Option Strict Off
Option Explicit On
Module Utils
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Copyright �1996-2001 VBnet, Randy Birch, All Rights Reserved.
	' Some pages may also contain other copyrights by the author.
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' You are free to use this code within your own applications,
	' but you are expressly forbidden from selling or otherwise
	' distributing this source code without prior written consent.
	' This includes both posting free demo projects made from this
	' code as well as reproducing the code in text or html format.
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	Public Const INVALID_HANDLE_VALUE As Short = -1
	Public Const MAX_PATH As Short = 260
	Public Const FILE_ATTRIBUTE_READONLY As Integer = &H1
	Public Const FILE_ATTRIBUTE_HIDDEN As Integer = &H2
	Public Const FILE_ATTRIBUTE_SYSTEM As Integer = &H4
	Public Const FILE_ATTRIBUTE_DIRECTORY As Integer = &H10
	Public Const FILE_ATTRIBUTE_ARCHIVE As Integer = &H20
	Public Const FILE_ATTRIBUTE_NORMAL As Integer = &H80
	Public Const FILE_ATTRIBUTE_TEMPORARY As Integer = &H100
	Public Const FILE_ATTRIBUTE_COMPRESSED As Integer = &H800
	
	Structure FILETIME
		Dim dwLowDateTime As Integer
		Dim dwHighDateTime As Integer
	End Structure
	
	Structure WIN32_FIND_DATA
		Dim dwFileAttributes As Integer
		Dim ftCreationTime As FILETIME
		Dim ftLastAccessTime As FILETIME
		Dim ftLastWriteTime As FILETIME
		Dim nFileSizeHigh As Integer
		Dim nFileSizeLow As Integer
		Dim dwReserved0 As Integer
		Dim dwReserved1 As Integer
		'UPGRADE_WARNING: Fixed-length string size must fit in the buffer. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
		<VBFixedString(MAX_PATH),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray,SizeConst:=MAX_PATH)> Public cFileName() As Char
		'UPGRADE_WARNING: Fixed-length string size must fit in the buffer. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
		<VBFixedString(14),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray,SizeConst:=14)> Public cAlternate() As Char
	End Structure
	
	Structure SECURITY_ATTRIBUTES
		Dim nLength As Integer
		Dim lpSecurityDescriptor As Integer
		Dim bInheritHandle As Integer
	End Structure
	
	'UPGRADE_WARNING: Structure SECURITY_ATTRIBUTES may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	Declare Function CreateDirectory Lib "kernel32"  Alias "CreateDirectoryA"(ByVal lpPathName As String, ByRef lpSecurityAttributes As SECURITY_ATTRIBUTES) As Integer
	
	Declare Function CopyFile Lib "kernel32"  Alias "CopyFileA"(ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Integer) As Integer
	
	'UPGRADE_WARNING: Structure WIN32_FIND_DATA may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	Declare Function FindFirstFile Lib "kernel32"  Alias "FindFirstFileA"(ByVal lpFileName As String, ByRef lpFindFileData As WIN32_FIND_DATA) As Integer
	
	'UPGRADE_WARNING: Structure WIN32_FIND_DATA may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	Declare Function FindNextFile Lib "kernel32"  Alias "FindNextFileA"(ByVal hFindFile As Integer, ByRef lpFindFileData As WIN32_FIND_DATA) As Integer
	
	Declare Function FindClose Lib "kernel32" (ByVal hFindFile As Integer) As Integer
	
	Declare Function GetFileAttributes Lib "kernel32"  Alias "GetFileAttributesA"(ByVal lpFileName As String) As Integer
	
	Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Integer) As Integer
	
	
	
	'Win32 API Constant Declarations
	Private Const WS_BORDER As Integer = &H800000
	Private Const WS_CAPTION As Integer = &HC00000
	Private Const WS_CHILD As Integer = &H40000000
	Private Const WS_CHILDWINDOW As Integer = (WS_CHILD)
	Private Const WS_CLIPCHILDREN As Integer = &H2000000
	Private Const WS_CLIPSIBLINGS As Integer = &H4000000
	Private Const WS_DISABLED As Integer = &H8000000
	Private Const WS_DLGFRAME As Integer = &H400000
	Private Const WS_EX_ACCEPTFILES As Integer = &H10
	Private Const WS_EX_DLGMODALFRAME As Integer = &H1
	Private Const WS_EX_NOPARENTNOTIFY As Integer = &H4
	Private Const WS_EX_TOPMOST As Integer = &H8
	Private Const WS_EX_TRANSPARENT As Integer = &H20
	Private Const WS_GROUP As Integer = &H20000
	Private Const WS_HSCROLL As Integer = &H100000
	Private Const WS_MINIMIZE As Integer = &H20000000
	Private Const WS_ICONIC As Integer = WS_MINIMIZE
	Private Const WS_MAXIMIZE As Integer = &H1000000
	Private Const WS_MAXIMIZEBOX As Integer = &H10000
	Private Const WS_MINIMIZEBOX As Integer = &H20000
	Private Const WS_OVERLAPPED As Integer = &H0
	Private Const WS_SYSMENU As Integer = &H80000
	Private Const WS_THICKFRAME As Integer = &H40000
	Private Const WS_OVERLAPPEDWINDOW As Boolean = (WS_OVERLAPPED Or WS_CAPTION Or WS_SYSMENU Or WS_THICKFRAME Or WS_MINIMIZEBOX Or WS_MAXIMIZEBOX)
	Private Const WS_POPUP As Integer = &H80000000
	Private Const WS_POPUPWINDOW As Boolean = (WS_POPUP Or WS_BORDER Or WS_SYSMENU)
	Private Const WS_SIZEBOX As Integer = WS_THICKFRAME
	Private Const WS_TABSTOP As Integer = &H10000
	Private Const WS_TILED As Integer = WS_OVERLAPPED
	Private Const WS_TILEDWINDOW As Boolean = WS_OVERLAPPEDWINDOW
	Private Const WS_VISIBLE As Integer = &H10000000
	Private Const WS_VSCROLL As Integer = &H200000
	
	'Win32 API Constant Declarations
	Private Const GWL_EXSTYLE As Short = (-20)
	Private Const GWL_HINSTANCE As Short = (-6)
	Private Const GWL_HWNDPARENT As Short = (-8)
	Private Const GWL_ID As Short = (-12)
	Private Const GWL_STYLE As Short = (-16)
	Private Const GWL_USERDATA As Short = (-21)
	Private Const GWL_WNDPROC As Short = (-4)
	
	
    Function FindFiles(ByRef sFilename As String) As String(,)

        Dim aDirList() As String
        'DOTNETT REDIM Dim aFileList(,) As String
        Dim aFileList(,) As String
        Dim aFileTempList(,) As String
        Dim iTempCount As Short
        Dim sSourcePath, sSourceFilename As String
        Dim bAsterixIncluded As Boolean
        Dim iStartName, iTotalLength As Short
        Dim iStartPos, iLength As Short
        Dim bSpecialCharacter As Boolean
        Dim sNameToFind, sTempPath, sTempName As String
        Dim sFirstPart As String
        Dim iFirstPartPos As Short
        Dim sLastPart As String
        Dim iLastPartPos As Short
        Dim iLengthNameToFind As Short
        Dim oFs As Scripting.FileSystemObject
        Dim sNewName As String
        Dim iCount, iYArrayNo As Short
        Dim i As Short

        Dim sTemp As String
        Dim sFixedTrailingPath As String
        Dim iClientStart As Short
        Dim iClientStop As Short

        oFs = New Scripting.FileSystemObject

        sFixedTrailingPath = ""
        sLastPart = ""

        sSourcePath = Left(sFilename, InStrRev(sFilename, "\"))
        sSourceFilename = Right(sFilename, Len(sFilename) - InStrRev(sFilename, "\"))
        iTotalLength = Len(sFilename)

        'FIX: We will not treat pathes like C:\kli*.��� correct
        'The function will return all files like C:\kli*.*
        If InStr(sFilename, "*") <> 0 Then
            bAsterixIncluded = True
        Else
            bAsterixIncluded = False
        End If

        If InStr(sSourcePath, "�") <> 0 Then
            sTempPath = Left(sSourcePath, Len(sSourcePath) - 1)
            sNameToFind = Right(sTempPath, Len(sTempPath) - InStrRev(sTempPath, "\"))
            sTempPath = Left(sTempPath, InStrRev(sTempPath, "\"))

            sFirstPart = UCase(Left(sNameToFind, InStr(sNameToFind, "�") - 1))
            iFirstPartPos = Len(sFirstPart)
            sLastPart = UCase(Right(sNameToFind, Len(sNameToFind) - InStrRev(sNameToFind, "�")))
            iLastPartPos = Len(sLastPart)
            'iLengthNameToFind = Len(sNameToFind)

            'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
            sTempName = Dir(sTempPath, FileAttribute.Directory - vbNormal) ' Retrieve the first entry.
            'Set oFs = CreateObject ("Scripting.FileSystemObject")
            iCount = 0
            Do While sTempName <> "" ' Start the loop.
                ' Ignore the current directory and the encompassing directory.
                If sTempName <> "." And sTempName <> ".." Then
                    ' NEW 21.01.2003 by JanP
                    ' Due to oFS.Filexists do not take * in search, must first find
                    ' directories, and fill an array with theese
                    ' Then search for files within each directory;
                    If GetAttr(sTempPath & "\" & sTempName) And FileAttribute.Directory Then
                        ReDim Preserve aDirList(iCount)
                        aDirList(iCount) = sTempPath & sTempName
                        iCount = iCount + 1
                    End If
                    ' Use bitwise comparison to make sure MyName is a directory.
                    '          If (GetAttr(sTempPath & sTempName) And vbDirectory) = vbDirectory Then
                    '                If UCase(Left$(sTempName, iFirstPartPos)) = sFirstPart Then
                    '                    If UCase(Right$(sTempName, iLastPartPos)) = sLastPart Then
                    '                        sNewName = sTempPath & sTempName & "\" & sSourceFilename
                    '                        If oFs.FileExists(sNewName) = True Then
                    '                            ReDim Preserve aFileList(1, iCount)
                    '                            aFileList(0, iCount) = sNewName
                    '                            aFileList(1, iCount) = Mid$(sTempName, iFirstPartPos + 1, Len(sTempName) - iLastPartPos)
                    '                            iCount = iCount + 1
                    '                        End If
                    '                        'Test om filnavnet eksisterer i s� fall add to list
                    '                    End If
                    '                End If
                    '             Debug.Print sTempName   ' Display entry only if it
                    '          End If   ' it represents a directory.
                End If
                sTempName = Dir() ' Get next entry.
            Loop

            ' NEW 21.01.2003 by JanP
            ' Has array with directorynames, search for files in each directory
            iCount = 0
            If Not Array_IsEmpty(aDirList) Then
                For i = 0 To UBound(aDirList)
                    sTempName = Dir(aDirList(i) & "\" & sSourceFilename)   ' Retrieve the first entry.
                    Do While sTempName <> ""   ' Start the loop.
                        ' XokNET 13.11.2013 - Check if directory, and do NOT add directories to filearray;
                        sNewName = aDirList(i) & "\" & sTempName
                        If Not (GetAttr(sNewName) And vbDirectory) Then
                            ReDim Preserve aFileList(1, iCount)
                            aFileList(0, iCount) = sNewName
                            'aFileList(1, iCount) = Mid$(sTempName, iFirstPartPos + 1, Len(sTempName) - iLastPartPos)
                            ' ClientNo
                            aFileList(1, iCount) = Mid$(aDirList(i), InStrRev(aDirList(i), "\") + 1)
                            iCount = iCount + 1
                        End If
                        sTempName = Dir()   ' Get next entry.
                    Loop
                Next i
            End If
            'Set oFs = Nothing

            FindFiles = VB6.CopyArray(aFileList)

            Exit Function

        ElseIf InStr(sSourceFilename, "�") <> 0 Then
            iStartPos = InStr(sSourceFilename, "�")
            iTotalLength = Len(sSourcePath) + Len(sSourceFilename) - 1
            sSourceFilename = Replace(sSourceFilename, "�", "*")
            aFileList = ListFiles(sSourcePath, sSourceFilename, iTotalLength, True)
            'Fill the second element in the array with the clientname.
            If Not Array_IsEmpty(aFileList) Then
                For iCount = 0 To UBound(aFileList, 2)
                    aFileList(1, iCount) = Mid(aFileList(0, iCount), Len(sSourcePath) + iStartPos, Len(aFileList(0, iCount)) - iTotalLength)
                Next iCount
            End If
            FindFiles = VB6.CopyArray(aFileList)
        ElseIf InStr(sSourceFilename, "�") <> 0 Then
            iStartPos = InStr(sSourceFilename, "�")
            iLength = 1
            bSpecialCharacter = True
            Do While bSpecialCharacter
                If Mid(sSourceFilename, iStartPos + iLength, 1) = "�" Then
                    iLength = iLength + 1
                Else
                    bSpecialCharacter = False
                End If
            Loop
            sSourceFilename = Replace(sSourceFilename, New String("�", iLength), "*")


            aFileList = ListFiles(sSourcePath, sSourceFilename, iTotalLength, bAsterixIncluded)
            'Fill the second element in the array with the clientno.
            If Not Array_IsEmpty(aFileList) Then
                For iCount = 0 To UBound(aFileList, 2)
                    aFileList(1, iCount) = Mid(aFileList(0, iCount), Len(sSourcePath) + iStartPos, iLength)
                Next iCount
            End If
            FindFiles = VB6.CopyArray(aFileList)

        ElseIf InStr(sSourcePath, "�") <> 0 Then

            sTempPath = Left(sSourcePath, Len(sSourcePath) - 1)


            ' added next lines 26.01.07
            ' Different actions dependent upon if there are subdirs under ���-path;
            ' test if there are two \ after ���
            sTemp = Mid(sFilename, InStr(sFilename, "�"))
            sTemp = Replace(sTemp, "�", "") ' Holds everything behind ���

            If Len(sTemp) - Len(Replace(sTemp, "\", "")) > 1 Then
                ' sSourcefilename must be last folder(s) + filename;
                sSourceFilename = Mid(sTemp, 2) ' Remove first \

                sNameToFind = Right(sTempPath, Len(sTempPath) - InStrRev(sTempPath, "\"))

                iClientStart = InStr(sFilename, "�")
                iClientStop = InStrRev(sFilename, "�")


                ' let sTemp be folders, not filename(s) after ���
                sTemp = Left(sTemp, InStrRev(sTemp, "\") - 1)
                ' remove last part (after ���) from sTempPath
                sTempPath = Replace(sTempPath, sTemp, "")
                ' At this stage, we must find the length of the folders we are trying to find, including the clientno ($) part
                ' ex, for c:\ccc\��� iLengthNameToFind is 3, but in c:\ccc\arkiv��� iLengthNameToFind=8
                iLengthNameToFind = Len(Mid(sTempPath, InStrRev(sTempPath, "\") + 1))


                ' New 26.01.07, for paths like c:\arkiv���\bankfiler\abc*.dat
                ' We must then also keep track of \bankfiler, and add it later on in ListFiles()
                If InStr(sTempPath, "\") > 0 Then
                    sFixedTrailingPath = Left(sSourceFilename, InStr(sSourceFilename, "\"))
                End If

                ' OK, more than one \ after $, thus it is a subdir between filename and clientnumber
                sNameToFind = Replace(sFilename, sTemp, "")
                sTempPath = Left(sTempPath, InStrRev(sTempPath, "\"))
                sTemp = Left(sFilename, InStr(sFilename, "�") - 1) ' upto clientpart in file, eg c:\arkiv���\bankfiler\abc*.dat to c:\arkiv
                sFirstPart = UCase(Mid(sTemp, InStrRev(sTemp, "\") + 1))
                iFirstPartPos = Len(sFirstPart)
            Else
                ' as before 26.01.07

                sNameToFind = Right(sTempPath, Len(sTempPath) - InStrRev(sTempPath, "\"))
                sTempPath = Left(sTempPath, InStrRev(sTempPath, "\"))

                sFirstPart = UCase(Left(sNameToFind, InStr(sNameToFind, "�") - 1))
                iFirstPartPos = Len(sFirstPart)
                sLastPart = UCase(Right(sNameToFind, Len(sNameToFind) - InStrRev(sNameToFind, "�")))
                iLastPartPos = Len(sLastPart)
                iLengthNameToFind = Len(sNameToFind)
            End If
            iYArrayNo = -1

            sTempName = Dir(sTempPath, FileAttribute.Directory) ' Retrieve the first entry.
            'oFs = CreateObject ("Scripting.FileSystemObject")
            iCount = 0
            Do While sTempName <> "" ' Start the loop.
                ' Ignore the current directory and the encompassing directory.
                If sTempName <> "." And sTempName <> ".." Then
                    ' Use bitwise comparison to make sure MyName is a directory.
                    If (GetAttr(sTempPath & sTempName) And FileAttribute.Directory) = FileAttribute.Directory Then
                        If Len(sTempName) = iLengthNameToFind Then
                            If UCase(Left(sTempName, iFirstPartPos)) = sFirstPart Then
                                If UCase(Right(sTempName, iLastPartPos)) = sLastPart Then
                                    sNewName = sTempPath & sTempName & "\" & sSourceFilename
                                    'New code 21.09.2004 by Kjell
                                    'We didn't find any files when the path included $-signs
                                    '   and the filename included an asterix
                                    If bAsterixIncluded Then
                                        aFileTempList = ListFiles(sTempPath & sTempName & "\", sSourceFilename, iTotalLength, bAsterixIncluded, sFixedTrailingPath)
                                        If Not Array_IsEmpty(aFileTempList) Then ' New 07.02.05 JanP
                                            ReDim Preserve aFileList(1, UBound(aFileTempList, 2) + 1 + iYArrayNo)
                                            For iTempCount = 0 To UBound(aFileTempList, 2)
                                                iYArrayNo = iYArrayNo + 1
                                                aFileList(0, iYArrayNo) = aFileTempList(0, iTempCount)
                                                ' added 26.01.07 to handle things like c:\arkiv���\bankfiler\abc*.dat
                                                If iClientStart > 0 Then
                                                    aFileList(1, iYArrayNo) = Mid(aFileTempList(0, iTempCount), iClientStart, (iClientStop - iClientStart) + 1)
                                                Else
                                                    ' as pre 26.01.07
                                                    aFileList(1, iYArrayNo) = sTempName
                                                End If
                                            Next iTempCount
                                        End If
                                    Else
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oFs.FileExists. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If oFs.FileExists(sNewName) = True Then
                                            ReDim Preserve aFileList(1, iCount)
                                            aFileList(0, iCount) = sNewName
                                            ' added 26.01.07 to handle things like c:\arkiv���\bankfiler\abc*.dat
                                            If iClientStart > 0 Then
                                                aFileList(1, iCount) = Mid(sNewName, iClientStart, (iClientStop - iClientStart) + 1)
                                            Else
                                                ' as pre 26.01.07
                                                aFileList(1, iCount) = Mid(sTempName, iFirstPartPos + 1, iLengthNameToFind - iFirstPartPos - iLastPartPos)
                                            End If
                                            iCount = iCount + 1
                                        End If
                                    End If
                                    'Test om filnavnet eksisterer i s� fall add to list
                                End If
                            End If
                        End If

                        Debug.Print(sTempName) ' Display entry only if it
                    End If ' it represents a directory.
                End If
                sTempName = Dir() ' Get next entry.
            Loop

            FindFiles = VB6.CopyArray(aFileList)

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

            Exit Function


        ElseIf InStr(sSourceFilename, "*") <> 0 Then
            'aFileList = ListFiles(sSourcePath, sSourceFilename, iTotalLength, bAsterixIncluded)
            aFileList = ListFiles(sSourcePath, sSourceFilename, iTotalLength, bAsterixIncluded)
            'Fill the second element in the array with an empty string.
            If Not Array_IsEmpty(aFileList) Then
                For iCount = 0 To UBound(aFileList, 2)
                    aFileList(1, iCount) = ""
                Next iCount
            End If
            FindFiles = VB6.CopyArray(aFileList)
        Else
            'XokNET - 29.06.2011 - Added next 2 lines, check with Jan-Petter (didn't return any filename when we had a fixed name (necessary when we check if any files are to be deleted before export.
            aFileList = ListFiles(sSourcePath, sSourceFilename, iTotalLength, False)
            FindFiles = aFileList

            'FIX: Babelerror No specialsigns found. Should be impossible, but you
            'never know.

        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        iStartName = 1

    End Function
    Public Function ListFiles_OLD(ByRef sSourcePath As String, ByRef sFileType As String, ByRef iTotalLength As Short, ByRef bAsterixIncluded As Boolean, Optional ByRef sFixedTrailingPath As String = "") As String(,)
        ' 04.11.2021 - Does not use this function anymore - no need to use API-files

        Dim WFD As WIN32_FIND_DATA
        'DOTNETT REDIM Dim aFileList(,) As String
        Dim aFileList(,) As String
        Dim r As Integer
        Dim hFile As Integer
        Dim bNext As Integer
        Dim iCount As Short
        Dim sCurrFile As String

        'Start searching for files in the Source directory.
        hFile = FindFirstFile(sSourcePath & sFileType, WFD)

        If (hFile = INVALID_HANDLE_VALUE) Then

            'FIX: Babelerror. No files to read!
            ListFiles_OLD = VB6.CopyArray(aFileList)
            Exit Function

        End If

        If hFile Then

            iCount = 0

            Do

                'trim trailing nulls, leaving one to terminate the string
                sCurrFile = Left(WFD.cFileName, InStr(WFD.cFileName, Chr(0)) - 1)
                ' See comment in function FindFiles()
                If bAsterixIncluded = False Then
                    If Len(sSourcePath & sFixedTrailingPath & sCurrFile) = iTotalLength Then
                        ' XokNET 13.11.2013 - must test if we have a foldername. needs filename
                        If Not WFD.dwFileAttributes = vbDirectory Then
                            ReDim Preserve aFileList(1, iCount)
                            aFileList(0, iCount) = UCase(sSourcePath & sFixedTrailingPath & sCurrFile)
                            'Will be filled out later, back in the FindFiles-function
                            aFileList(1, iCount) = ""
                            iCount = iCount + 1
                        End If
                    End If
                Else
                    If sCurrFile <> "." And sCurrFile <> ".." And sCurrFile.ToUpper <> "THUMBS.DB" Then '21.11.2017 - Added Thumbs.db
                        'XOKNET - 02.11.2010 - Kjell - Added next IF to check if it is a direcctory
                        If Not WFD.dwFileAttributes = vbDirectory Then
                            ReDim Preserve aFileList(1, iCount)
                            aFileList(0, iCount) = sSourcePath & sFixedTrailingPath & sCurrFile
                            'Will be filled out later, back in the FindFiles-function
                            aFileList(1, iCount) = ""
                            iCount = iCount + 1
                        End If
                    End If
                End If

                bNext = FindNextFile(hFile, WFD)

            Loop Until bNext = 0

        End If

        'Close the search handle
        r = FindClose(hFile)

        'return the complete path and name for files
        ListFiles_OLD = VB6.CopyArray(aFileList)

    End Function
    Public Function ListFiles(ByRef sSourcePath As String, ByRef sFileType As String, ByRef iTotalLength As Short, ByRef bAsterixIncluded As Boolean, Optional ByRef sFixedTrailingPath As String = "") As String(,)
        ' 04.11.2021 - Replaces old ListFiles function with this, so we do not need to use WinAPI
        Dim aFileList(,) As String
        Dim iCount As Short = 0
        'Start searching for files in the Source directory.
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(sSourcePath, FileIO.SearchOption.SearchTopLevelOnly, sFileType)
            ReDim Preserve aFileList(1, iCount)
            aFileList(0, iCount) = foundFile
            'Will be filled out later, back in the FindFiles-function
            aFileList(1, iCount) = ""
            iCount = iCount + 1
        Next
        'return the complete path and name for files
        ListFiles = VB6.CopyArray(aFileList)
    End Function

	
    Public Function DetectFormatInOut(ByVal eFormat As vbBabel.BabelFiles.FileType) As Boolean
        ' ------------------------------------------------
        ' True if outgoing payments (Telepay, Dirrem) False if incoming (OCR, Cremul etc)
        ' Public Enum Filetype
        '    Unknown = -1
        '    OCR = 1
        '    AutoGiro = 2
        '    Dirrem = 3
        '    Telepay = 4
        '    Coda = 5
        'End Enum
        ' --------------------------------------------------
        DetectFormatInOut = True

        Select Case eFormat
            Case 1
                DetectFormatInOut = False
            Case 2
                DetectFormatInOut = False
            Case 3
                DetectFormatInOut = True
            Case 4
                DetectFormatInOut = True
            Case 5
                DetectFormatInOut = True
        End Select

    End Function
	
	Function CheckFilename(ByRef sFilename As String, ByRef bOverWrite As Boolean, ByRef bWarning As Boolean) As Boolean
		
        Dim Fso As Scripting.FileSystemObject
		Dim iResponse As Short
		Dim bFilenameOK As Boolean
		'Dim sDrive As String
		'Dim sFolderName As String
		
		bFilenameOK = False

        Fso = New Scripting.FileSystemObject

		If Fso.FileExists(sFilename) = True Then
			If bOverWrite Then
				If bWarning Then
					'FIX: Babelerror Make a proper message
					'iResponse = MsgBox("Skal filen " & sFileName & " overskrives?", vbYesNo + vbQuestion, "Filnavn finnes fra f�r!")
					iResponse = MsgBox(LRS(60010) & " " & sFilename & " " & LRS(60011) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Filnavn finnes fra f�r!")
					If iResponse = MsgBoxResult.No Then
						'User doesn't want the file to be overwritten. Return false
						bFilenameOK = False
					Else
						'Delete (overwrite) the file and continue
						bFilenameOK = True
						Fso.DeleteFile((sFilename))
					End If
				Else
					'No warning just delete the file
					bFilenameOK = True
					Fso.DeleteFile((sFilename))
				End If
			Else
				'FIX: Babelerror Make a proper message
				'The program will be abandoned
				'                      Filen                  finnes fra f�r        Programmet avbrytes
				iResponse = MsgBox(LRS(60012) & " " & sFilename & " " & LRS(60013) & vbCrLf & LRS(60014) & vbCrLf & vbCrLf & LRS(60015), MsgBoxStyle.OKOnly, LRS(60016) & "!")
				'& vbCrLf & vbCrLf & "Gj�r endring i oppsettet hvis du �nsker at filene skal overskrives", vbOKOnly, "Filen finnes fra f�r!")
				bFilenameOK = False
			End If
		Else
			bFilenameOK = True
			
			'    sDrive = Fso.GetDriveName(sFileName)
			'
			'    If Fso.GetDrive(sDrive).IsReady Then
			'
			'
			'    End If
			'
			'    sDrive = Left$(sFileName, InStr(sFileName, "\"))
			'
			'    if fso.DriveExists
			'
			'
			'    sFolderName = Left$(sFileName, InStrRev(sFileName, "\"))
			'    If Fso.FolderExists(sFolderName) = True Then
			'        'No problem, continue
			'        bFilenameOK = True
			'    Else
			
		End If

        If Not Fso Is Nothing Then
            Fso = Nothing
        End If

		CheckFilename = bFilenameOK
		
	End Function
End Module
