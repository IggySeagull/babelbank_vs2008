Option Strict Off
Option Explicit On
Module resource
	
	Function RemoveSpaces(ByRef sInString As String) As String
		' remove all blanks within sInString
		Dim nStart As Short
		Dim nPos As Short
		nStart = 1
		nPos = 1
		Do While nPos > 0 And Len(sInString) > 1
			nPos = InStr(nStart, sInString, Space(1))
			If nPos > 0 Then
				' cut spaces:
				sInString = Left(sInString, nPos - 1) & Mid(sInString, nPos + 1)
			End If
		Loop 
		RemoveSpaces = sInString
	End Function
	
    Function PadLine(ByRef sVariable As String, ByRef iLength As Short, ByRef sType As String) As String
        ' Pad rest of line, without trimming
        'sVariable is the variable
        'iLength is the length of the string you want returned
        'sType is the key you want to use to pad the string i.e. " " or "0".

        If Len(sVariable) >= iLength Then
            PadLine = Right(sVariable, iLength)
        Else
            sVariable = sVariable & New String(sType, iLength)
            PadLine = Left(sVariable, iLength)
        End If

    End Function
	

    Function bbGetStateBankCode(ByRef sInCode As String, ByRef sCountryCode As String, Optional ByRef sFormat As String = "", Optional ByRef sPayType As String = "") As String
        ' Get a StateBankCode from BabelBanks generic StateBankCode
        'The standard used is Norwegian codes
        Dim sReturnCode As String
        ' Kodeforklaring: Se bbSetPayCode
        '-----------------------------------------------------------------------

        Select Case UCase(sCountryCode)

            Case "NO"

                Select Case sInCode

                    Case "10" ' Export/Import of ships
                        sReturnCode = "10"
                    Case "11" ' Export/Import of platforms/rigs
                        sReturnCode = "11"
                    Case "12" ' Export/Import of aeroplanes
                        sReturnCode = "12"
                    Case "13" ' Export of crude oil/natural gas and condensates
                        sReturnCode = "13"
                    Case "14" ' Export/Import of other goods
                        sReturnCode = "14"
                    Case "15" ' Other payments in connection with merchandise trade
                        sReturnCode = "15"
                    Case "20" ' Freight of passengers - income
                        sReturnCode = "20"
                    Case "21" ' Other freight income
                        sReturnCode = "21"
                    Case "22" ' Travel operations of natural persons or legal person domiciled in Norway
                        sReturnCode = "22"
                    Case "23" ' Traval operations of natural persons or legal person not domiciled in Norway
                        sReturnCode = "23"
                    Case "24" ' Export/Import of ships
                        sReturnCode = "24"
                    Case "25" ' Export/Import of platforms/rigs
                        sReturnCode = "25"
                    Case "40" ' Export/Import of aeroplanes
                        sReturnCode = "40"
                    Case "44" ' Export of crude oil/natural gas and condensates
                        sReturnCode = "44"
                    Case "46" ' License fees, patents, royalties
                        sReturnCode = "46"
                    Case "48" ' Other return on capital
                        sReturnCode = "48"
                    Case "60" ' Loans
                        sReturnCode = "60"
                    Case "61" ' Lending
                        sReturnCode = "61"
                    Case "62" ' Purchase/sale of Norwegian shares and units, referring to direct investment
                        sReturnCode = "62"
                    Case "63" ' Purchase/sale of foreing shares and units, referring to direct investment
                        sReturnCode = "63"
                    Case "64" ' Purchase/sale of Norwegian shares and units, referring to to portofolio investment
                        sReturnCode = "64"
                    Case "65" ' Purchase/sale of foreign shares and units, referring to to portofolio investment
                        sReturnCode = "65"
                    Case "66" ' Purchase/sale of Norwegian bonds
                        sReturnCode = "66"
                    Case "67" ' Purchase/sale of foreign bonds
                        sReturnCode = "67"
                    Case "68" ' Purchase/sale of Norwegian bonds
                        sReturnCode = "68"
                    Case "69" ' Purchase/sale of other foreing securities
                        sReturnCode = "69"
                    Case "70" ' Other capital transactions
                        sReturnCode = "70"
                    Case "90" ' Other transfers from/to natural persons domiciled in Norway
                        sReturnCode = "90"
                    Case "91" ' Other transfers on behalf of companies
                        sReturnCode = "91"

                    Case Else ' Export/Import of other goods
                        sReturnCode = "14"

                End Select

            Case "SE"
                'Payment Code/Central bank reports - the customer's responsibility
                'For certain orders, a report must be made to the central bank of the country involved. Handelsbanken
                'provides instructions as to how reporting of this kind is to be carried out. It is the customer's
                'responsibility to ensure that the instructions are followed on every transfer or payment.
                'If transfers are initiated from an account held with another bank regulations regarding central bank
                'reporting may apply. Please contact your local bank for further information regarding the
                'Note that the code you report here is the central bank code for the country where the debit account is
                'held. Central bank reporting for the incoming transfer must be done separately if necessary
                'Each individual transfers via a Swedish bank to a beneficiary domiciled outside Sweden (in a foreign
                'currency or in Swedish kronor) that exceeds a specific value determined by the National Tax Board
                'A list of the most frequently used payment codes is shown below. For a complete list of payment codes,
                'please refer to the Tax Board's publication 'Regler f�r kontrolluppgifter, bilaga 3' (availible in Swedish

                '*101- Imports/exports of goods
                '*122- Goods that have not passed the Swedish border; purchase of goods abroad with a view to selling them.
                '*130- Freight of goods imported to Sweden
                '*131- Freight of goods exported from Sweden
                '*173- Storage, shipping, transit, port, and terminal costs, etc.
                '*223- Other travel expenses (expenditure for hotels, courses, conferences, etc.)
                '*331- Fees and commission on bank-, broker and management services etc.
                '*410- Computing services (hardware and software consultancy and implementation, data processing,
                'etc.)
                '*423- Royalties and licence fees for the use of proprietary rights.
                '*440- Advertising, amrket research and public opinion polls
                '*442- Architectural, engineering and other technical services
                '*462- Other services
                '*473- Commission related to the intermediation of goods
                '*560- Other transfers (damages, alimony, membership, fees, and gifts incl. taxes, etc.)
                '*601- In Sweden (foreign owner). Loan to/from foreign owner.
                '*603- Outside Sweden (Swedish owner). Loan to/from foreign subsidiary/group company.
                '*683- Loan to party outside Sweden (asset). Loan to party outside Sweden/amortisation of loan to party
                'outside Sweden.
                '*793- Other capital transactions.
                '*940- Conversion outside Sweden of an amount that has been taken from Sweden, converted and then
                'brought into Sweden again. (For conversion abroad in connection with other payments, the code for the
                'nderlying transaction is used).

                Select Case sInCode

                    Case "10" ' Export/Import of ships
                        sReturnCode = "101" '- Imports/exports of goods
                    Case "11" ' Export/Import of platforms/rigs
                        sReturnCode = "101" '- Imports/exports of goods
                    Case "12" ' Export/Import of aeroplanes
                        sReturnCode = "101" '- Imports/exports of goods
                    Case "13" ' Export of crude oil/natural gas and condensates
                        sReturnCode = "101" '- Imports/exports of goods
                    Case "14" ' Export/Import of other goods
                        sReturnCode = "101" '- Imports/exports of goods
                    Case "15" ' Other payments in connection with merchandise trade
                        sReturnCode = "122" '- Goods that have not passed the Swedish border; purchase of goods abroad with a view to selling them.
                    Case "20" ' Freight of passengers - income
                        sReturnCode = "223" '- Other travel expenses (expenditure for hotels, courses, conferences, etc.)
                    Case "21" ' Other freight income
                        sReturnCode = "130" '- Freight of goods imported to Sweden (131- Freight of goods exported from Sweden)
                    Case "22" ' Travel operations of natural persons or legal person domiciled in Norway
                        sReturnCode = "223" '- Other travel expenses (expenditure for hotels, courses, conferences, etc.)
                    Case "23" ' Traval operations of natural persons or legal person not domiciled in Norway
                        sReturnCode = "223" '- Other travel expenses (expenditure for hotels, courses, conferences, etc.)
                    Case "24" ' Export/Import of ships
                        sReturnCode = "173" '- Storage, shipping, transit, port, and terminal costs, etc.
                    Case "25" ' Export/Import of platforms/rigs
                        sReturnCode = "101" '- Imports/exports of goods
                    Case "40" ' Export/Import of aeroplanes
                        sReturnCode = "101" '- Imports/exports of goods
                    Case "44" ' Export of crude oil/natural gas and condensates
                        sReturnCode = "101" '- Imports/exports of goods
                    Case "46" ' License fees, patents, royalties
                        sReturnCode = "423" '- Royalties and licence fees for the use of proprietary rights.
                    Case "48" ' Other return on capital
                        sReturnCode = "793" '- Other capital transactions.
                    Case "60" ' Loans
                        sReturnCode = "683" '- Loan to party outside Sweden (asset). Loan to party outside Sweden/amortisation of loan to party outside Sweden.
                    Case "61" ' Lending
                        sReturnCode = "793" '- Other capital transactions.
                    Case "62" ' Purchase/sale of Norwegian shares and units, referring to direct investment
                        sReturnCode = "793" '- Other capital transactions.
                    Case "63" ' Purchase/sale of foreing shares and units, referring to direct investment
                        sReturnCode = "793" '- Other capital transactions.
                    Case "64" ' Purchase/sale of Norwegian shares and units, referring to to portofolio investment
                        sReturnCode = "793" '- Other capital transactions.
                    Case "65" ' Purchase/sale of foreign shares and units, referring to to portofolio investment
                        sReturnCode = "793" '- Other capital transactions.
                    Case "66" ' Purchase/sale of Norwegian bonds
                        sReturnCode = "793" '- Other capital transactions.
                    Case "67" ' Purchase/sale of foreign bonds
                        sReturnCode = "793" '- Other capital transactions.
                    Case "68" ' Purchase/sale of Norwegian bonds
                        sReturnCode = "793" '- Other capital transactions.
                    Case "69" ' Purchase/sale of other foreing securities
                        sReturnCode = "793" '- Other capital transactions.
                    Case "70" ' Other capital transactions
                        sReturnCode = "793" '- Other capital transactions. (331- Fees and commission on bank-, broker and management services etc.)
                        '(940- Conversion outside Sweden of an amount that has been taken from Sweden, converted and then
                        'brought into Sweden again. (For conversion abroad in connection with other payments, the code for the
                        'underlying transaction is used).)
                    Case "90" ' Other transfers from/to natural persons domiciled in Norway
                        sReturnCode = "560" '- Other transfers (damages, alimony, membership, fees, and gifts incl. taxes, etc.)
                    Case "91" ' Other transfers on behalf of companies
                        sReturnCode = "560" '- Other transfers (damages, alimony, membership, fees, and gifts incl. taxes, etc.)
                        '(473- Commission related to the intermediation of goods)
                    Case "100" ' Other services
                        sReturnCode = "462" '- Other services
                    Case "410" ' Computing services (hardware and software consultancy and implementation, data processing,etc.)
                        sReturnCode = "410" '- Computing services (hardware and software consultancy and implementation, data processing, etc.)
                    Case "440" ' Advertising, amrket research and public opinion polls
                        sReturnCode = "440" '- Advertising, amrket research and public opinion polls
                    Case "442" ' Architectural, engineering and other technical services
                        sReturnCode = "442" '- Architectural, engineering and other technical services
                    Case Else ' Export/Import of other goods
                        sReturnCode = "101" '- Imports/exports of goods

                End Select

            Case Else
                sReturnCode = ""

        End Select

    End Function

    Function FilenameImported(Optional ByRef sFilename As String = "") As String
        Static sLocalFilename As String

        'Used to set or retrieve the index in the Filenames object in the Profile-collection

        If sFilename <> "" Then
            sLocalFilename = sFilename
        End If

        FilenameImported = sLocalFilename


    End Function


    Function FileSetup_ID(Optional ByRef iFileSetup_ID As Short = 0) As Short
        Static iId As Short

        'Used to set or retrieve the index in the Filenames object in the Profile-collection

        If iFileSetup_ID <> 0 Then
            iId = iFileSetup_ID
        End If

        'UPGRADE_WARNING: Couldn't resolve default property of object FileSetup_ID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        FileSetup_ID = iId

    End Function
    Function CheckAccountNo(ByRef sI_AccountNo As String, ByRef sOwnRef As String, ByRef oProfile As vbBabel.Profile, Optional ByRef bGetMultiFiles As Boolean = False) As Short

        Static sI_AccountNoOld As String
        Static bMultiExportFiles As Boolean
        'Dim oFormatsUsed As FormatsUsed
        'Dim oFormatUsed As FormatUsed,
        'Dim oFileSetup As FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim bNewAccount, bFirstTime As Boolean
        Static iClientIndexOld As Short
        Static bFoundAccount As Boolean
        Static iFormatOutOld, iFormatOut As Short
        Dim bIdenticalFormatOut As Boolean
        Dim sMessage As String
        Dim iFileSetupIndex As Short
        Dim iClientIndex, iAccountIndex As Short
        Dim bFromAccountingSystem As Boolean
        Dim iCounter As Short

        'This freak-accoutno is used to reset the static variable sI_AccountnoOld.
        'Used when BabelFiles are terminated.
        If sI_AccountNo = "@XYZ" Then
            sI_AccountNoOld = ""
            CheckAccountNo = 0
            Exit Function
        End If

        'Test if bGetMultiFiles = true. This variable is true if the fuction is called from
        'the babelfile-object. It's omitted when called from payment.
        'If true we'll return the bMultiExportFiles-static variable

        If bGetMultiFiles Then
            CheckAccountNo = Val(CStr(bMultiExportFiles))
            Exit Function
        End If

        'Check if it is the first Payment-object we add to this Babelfile-collection
        'The Static-variables will then be empty
        If sI_AccountNoOld = "" Then
            'Initializing some variables
            bMultiExportFiles = False
            bFirstTime = True
            iClientIndexOld = -1
            iFormatOut = -1
            bFoundAccount = False


            'Have to find and store the ClientNo for the Account
            'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups(FileSetup_ID()).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For Each oClient In oProfile.FileSetups(FileSetup_ID()).Clients
                For Each oaccount In oClient.Accounts
                    If oaccount.Account = sI_AccountNo Then
                        bFoundAccount = True
                    End If
                Next oaccount
                If bFoundAccount Then
                    'Store information
                    iClientIndexOld = oClient.Index
                    'Don't have to check anymore. Set old AccountNoOld identical to new
                    sI_AccountNoOld = sI_AccountNo
                    '11/10 - 2001 KI
                    'The next if-statement is added. If the filesetupOut is given use it
                    ' insted of get it from the clientformat-table.
                    ' Case: Skagerak Energi
                    'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups(FileSetup_ID()).FileSetupOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If oProfile.FileSetups(FileSetup_ID()).FileSetupOut <> -1 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups().FileSetupOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        iFormatOut = oProfile.FileSetups(FileSetup_ID()).FileSetupOut
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups(FileSetup_ID()).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If oProfile.FileSetups(FileSetup_ID()).FromAccountingSystem = True Then
                            iFormatOut = oClient.FormatOut
                        Else
                            iFormatOut = oClient.FormatIn
                        End If
                    End If
                    iFormatOutOld = iFormatOut
                    Exit For
                End If
            Next oClient
        End If



        'Check if this payment-object has an different internal accountno from the previous one.
        'If not, no action
        If sI_AccountNo <> sI_AccountNoOld Then
            'Resets and use the same variable
            bFoundAccount = False
            bNewAccount = True
            'Check if this FileSetup has a fixed exportformat.
            'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups(FileSetup_ID()).FileSetupOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If oProfile.FileSetups(FileSetup_ID()).FileSetupOut <> -1 Then
                'Check if the accountno. is known to Babelbank.
                'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups(FileSetup_ID()).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                For Each oClient In oProfile.FileSetups(FileSetup_ID()).Clients
                    For Each oaccount In oClient.Accounts
                        If sI_AccountNo = oaccount.Account Then
                            bFoundAccount = True
                            sI_AccountNoOld = sI_AccountNo
                            iClientIndexOld = oClient.Index
                            Exit For
                        End If
                    Next oaccount
                    If bFoundAccount Then
                        Exit For
                    End If
                Next oClient
                If Not bFoundAccount Then
                    'FIX: Add some userinterface and add new information to the profile
                    bFoundAccount = True
                    sI_AccountNoOld = sI_AccountNo
                    'FIX: Add the clientindex to the variable iClientindexOld =
                End If

                'Check if it is the same exportformat. If not bmultiexportfiles = true
                'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups().FileSetupOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                iFormatOut = oProfile.FileSetups(FileSetup_ID).FileSetupOut
                'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups(FileSetup_ID).FileSetupOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If oProfile.FileSetups(FileSetup_ID).FileSetupOut = iFormatOutOld Then
                    'check if exportfilename is client-dependent
                    'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups(FileSetup_ID).FileSetupOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If Not oProfile.FileSetups(FileSetup_ID).FileSetupOut = 0 Then 'FIX: 31052001 Added not tested
                        'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups().FileNameOut1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If InStr(oProfile.FileSetups(iFormatOut).FileNameOut1, "�") <> 0 Then
                            bMultiExportFiles = True
                        End If
                        iFormatOutOld = iFormatOut
                    Else 'FIX: 31052001 Added not tested everything under else
                        If bFirstTime Then
                            iFormatOutOld = iFormatOut
                            'OK, no problem
                        Else
                            iFormatOutOld = iFormatOut
                            'FIX: Immpossible, since we have only one export-format
                        End If
                    End If
                Else
                    If bFirstTime Then
                        iFormatOutOld = iFormatOut
                        'OK, no problem
                    Else
                        iFormatOutOld = iFormatOut
                        'FIX: Immpossible, since we have only one export-format
                    End If
                End If
            End If

            'We are finished treating the cases where we have a fixed exportformat


            'Let's check when it's not a fixed exportformat
            If Not bFoundAccount Then
                'Check if the new account is a known account.
                'We have to go through the client table to find all clients that uses this
                'importformat, and check if the account is registered.
                'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups(FileSetup_ID()).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                For Each oClient In oProfile.FileSetups(FileSetup_ID()).Clients
                    For Each oaccount In oClient.Accounts
                        If oaccount.Account = sI_AccountNo Then
                            bFoundAccount = True
                        End If
                    Next oaccount
                    If bFoundAccount Then
                        'Check if it is the same client
                        If oClient.Index = iClientIndexOld Then
                            'OK, export to same file.
                            'bMultiExportFiles doesn change value.
                            Exit For
                        Else
                            'Changes the values in the static variable.
                            iClientIndexOld = oClient.Index
                            'Check if the exportformat is identical for the two different clients.
                            If oClient.FormatOut <> iFormatOutOld Then
                                bMultiExportFiles = True
                                'Changes the values in the static variable.
                                iFormatOutOld = oClient.FormatOut
                                Exit For
                            Else
                                'Check if the exportfilename is fixed or depending of ClientNo.
                                'Check if $-sign exist in the filename
                                'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups().FileNameOut1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If InStr(oProfile.FileSetups(FileSetup_ID()).FileNameOut1, "�") = 0 Then
                                    'Fixed name
                                    'bMultiExportFiles doesn change value.
                                Else
                                    'Client-dependent name
                                    bMultiExportFiles = True
                                End If
                                Exit For
                            End If
                        End If
                    End If

                Next oClient
            End If

            If Not bFoundAccount Then
                sMessage = LRS(15107, sI_AccountNo)
                MsgBox(sMessage, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "PARSE ERROR")
                'Format=0 signifies end program!
                iFormatOutOld = 0
            End If

        End If


        ' Old code follows




        ''Check if it is the first Payment-object we add to this Babelfile-collection
        ''The Static-variables will then be empty
        'If sI_AccountNoOld = "" Then
        '    'Initializing some variables
        '    bMultiExportFiles = False
        '    bFirstTime = True
        '    sI_AccountNoOld = ""
        '    iClientIndexOld = -1
        '    iFormatOut = -1
        '    bFoundAccount = False
        '    'Have to find and store the ClientNo for the old Account
        '    For Each oClient In oProfile.FileSetups(FileSetup_ID())
        '    'For Each oClient In oProfile.FormatsUsed(FormatUsed_ID()).Filenames(FileName_ID()).Clients
        '        For Each oAccount In oClient.Accounts
        '            If oAccount.Account = sI_AccountNo Then
        '                bFoundAccount = True
        '            End If
        '        Next
        '        If bFoundAccount Then
        '            'Store information
        '            iClientIndexOld = oClient.Index
        '            'Don't have to check anymore. Set old AccountNoOld identical to new
        '            sI_AccountNoOld = sI_AccountNo
        '            iFormatOut = oClient.FormatOut
        '            iFormatOutOld = iFormatOut
        '            Exit For
        '        End If
        '    Next
        'End If
        '
        ''Check if this payment-object has an different internal accountno from the previous one.
        ''If not, no action
        'If sI_AccountNo <> sI_AccountNoOld Then
        '
        '    bNewAccount = True
        '    'Resets and use the same variable
        '    bFoundAccount = False
        '    'Check if the new account is a known account in the Profile-object,
        '    'for the filename-object that is used for import.
        '    For Each oClient In oProfile.FileSetups(FileSetup_ID())
        '    'For Each oClient In oProfile.FormatsUsed(FormatUsed_ID()).Filenames(FileName_ID()).Clients
        '        For Each oAccount In oClient.Accounts
        '            If oAccount.Account = sI_AccountNo Then
        '                bFoundAccount = True
        '            End If
        '        Next
        '        'Account known for this filename-object.
        '        If bFoundAccount Then
        '            'Check if it is the same client
        '            If oClient.Index = iClientIndexOld Then
        '                'OK, export to same file.
        '                'bMultiExportFiles doesn change value.
        '                Exit For
        '            Else
        '                'Changes the values in the static variable.
        '                iClientIndexOld = oClient.Index
        '                'Check if the exportformat/-filename is identical for the two different clients.
        '                If oClient.FormatOut <> iFormatOutOld Then
        '                    bMultiExportFiles = True
        '                    'Changes the values in the static variable.
        '                    iFormatOutOld = oClient.FormatOut
        '                    Exit For
        '                Else
        '                    'Check if the exportfilename is fixed or depending of ClientNo.
        '                    'Check if $-sign exist in the filename
        '                    If InStr(oProfile.FileSetups(FileSetup_ID()).FileNameOut1, "�") = 0 Then
        '                        'Fixed name
        '                        'bMultiExportFiles doesn change value.
        '                    Else
        '                        'Client-dependent name
        '                        bMultiExportFiles = True
        '                    End If
        '                    Exit For
        '                End If
        '            End If
        '        End If
        '    Next
        '    If Not bFoundAccount Then
        '        'iFormatUsedIndex = 0
        '        iFilenameIndex = 0
        '        iClientIndex = 0
        '        iAccountIndex = 0
        '
        '        'Check if the new account exist at all in the Profile-object.
        '        'Let's check the rest of the FormatsUsed.
        '        bIdenticalFormatOut = False
        '        For Each oFileSetup In oProfile.FileSetups
        '        'For Each oFormatUsed In oProfile.FormatsUsed
        '        '    iFormatUsedIndex = iFormatUsedIndex + 1
        '            'For Each oFileName In oFormatUsed.Filenames
        '            iFileSetupIndex = iFileSetupIndex + 1
        '            For Each oClient In oFileSetup.Clients
        '                iClientIndex = iClientIndex + 1
        '                For Each oAccount In oClient.Accounts
        '                    iAccountIndex = iAccountIndex + 1
        '                    If oAccount.Account = sI_AccountNo Then
        '                        bFoundAccount = True
        '                        Exit For
        '                    End If
        '                Next
        '                If bFoundAccount Then
        '                    'Even if it's a different filename-ID (In-format), It doesn't matter
        '                    ' as long as the Format Out is identical. Later we have to test
        '                    ' if the filename out a varable name (with ���), or not. See *1.
        '                    If oClient.FormatOut = iFormatOutOld Then
        '                        bIdenticalFormatOut = True
        '                        Exit For
        '                    Else
        '                        sMessage = "Kontonummer " & sI_AccountNo & " er registrert, men" _
        ''                            & " ikke for det formatet som importeres."
        '                        'FIX: Add more information here. Better message, and
        '                        'store the information in the collection
        '                        'Have to check the entered information to see if this
        '                        'new accountno shall be exported to the same file as the
        '                        'previously used one
        '                        'If not set bMultiExportFiles=True
        '                        'Have to store values in the static variables depending on the input.
        '                        bMultiExportFiles = True
        '                        Exit For
        '                    End If
        '                End If
        '                iAccountIndex = 0
        '            Next
        '            If bIdenticalFormatOut Then
        '                ' *1 Check if $-sign exist in the filename
        '                If InStr(oFileName.FileNameOut1, "�") = 0 Then
        '                    MsgBox ("Different in-format, but formats out identical, filename static")
        '                    'Fixed name
        '                    'bMultiExportFiles doesn change value.
        '                    Exit For
        '                Else
        '                    'Client-dependent name
        '                    MsgBox ("Different in-format, but formats out identical, filename client-dependent")
        '                    bMultiExportFiles = True
        '                    Exit For
        '                End If
        '            End If
        '            iClientIndex = 0
        ''            Next
        '        If bIdenticalFormatOut Then
        '            Exit For
        '        End If
        '        iFilenameIndex = 0
        '        Next
        '    End If
        '
        '    If Not bFoundAccount Then
        '        'If the exportfile-format is client-dependent on this importformat, then
        '        'assume that the new account shall be exported in the similar way.
        '        'We have to ask the user which clientno. to use. The export-file will follow
        '        'the standard client-dependent name.
        '        If oProfile.FormatsUsed(FormatUsed_ID()).Filenames.Count = 1 Then
        '            'If it is the first record we have to set the variable iFormatOutOld
        '            iFormatOutOld = oProfile.FormatsUsed(FormatUsed_ID()).Filenames(1).Clients(1).FormatOut
        '            If oProfile.FormatsUsed(iFormatOutOld).Filenames.Count = 1 Then
        '                If Not InStr(oProfile.FormatsUsed(iFormatOutOld).Filenames(1).FileNameOut1, "�") = 0 Then
        '                    frmUnknownAccount.lblDescription.Caption = Replace(LRS(15106), "%1", sI_AccountNo)
        '                    'frmUnknownAccount.txtClientNo.Text = oClient.Client_ID
        '                    'frmUnknownAccount.txtClientNo.Enabled = False
        '                    frmUnknownAccount.txtAccountNo.Text = sI_AccountNo
        '                    frmUnknownAccount.txtAccountNo.Enabled = False
        '                    frmUnknownAccount.txtFormatOut.Text = oProfile.FormatsUsed(iFormatOutOld).Description
        '                    frmUnknownAccount.txtFormatOut.Visible = False
        '                    frmUnknownAccount.txtFilename1Hidden.Text = oProfile.FormatsUsed(iFormatOutOld).Filenames(1).FileNameOut1
        '                    frmUnknownAccount.txtFileNameOut1.Visible = False
        '                    bFromAccountingSystem = frmUnknownAccount.SendProfile(oProfile)
        '                    frmUnknownAccount.Show 1
        '                Else
        '                    'Unknown accountno.
        '                    sMessage = Replace(LRS(15107), "%1", sI_AccountNo)
        '                    MsgBox (sMessage)
        '                End If
        '            Else
        '                'Unknown accountno.
        '                sMessage = Replace(LRS(15107), "%1", sI_AccountNo)
        '                MsgBox (sMessage)
        '            End If
        '        Else
        '            'Unknown accountno.
        '            sMessage = Replace(LRS(15107), "%1", sI_AccountNo)
        '            MsgBox (sMessage)
        '        End If
        '
        '
        '
        '        'The following code we'll be moved to another function
        '
        '        Dim iClientIndexNew As Integer
        '
        '        If oProfile.FormatsUsed(FormatUsed_ID()).Filenames(1).Clients.Count = 0 Then
        '            'FIX: Babelerror, At least 1 client must exist under this Filename_id
        '            'User have to run setup
        '        Else
        '            Set oClient = oProfile.FormatsUsed(FormatUsed_ID()).Filenames(1).Clients.Add
        '            'Set the properties in the new clinet-object
        '            'Input comes from user (the form frmUnknownAccount), or copied from
        '            'another client (with index 1) under the same filename_ID
        '            oClient.Client_ID = frmUnknownAccount.txtClientNo.Text
        '            oClient.FormatIn = oProfile.FormatsUsed(FormatUsed_ID()).Filenames(1).FileName_ID
        '            oClient.FormatOut = oProfile.FormatsUsed(FormatUsed_ID()).Filenames(1).Clients(1).FormatOut
        '    '        oClient.OwnRefText
        '            oClient.Outgoing = oProfile.FormatsUsed(FormatUsed_ID()).Filenames(1).Clients(1).Outgoing
        '    '        oClient.Name
        '    '        oClient.City
        '    '        oClient.CountryCode
        '    '        oClient.CompNo
        '            oClient.Changed = True
        '
        '            iClientIndexNew = oClient.Index
        '
        '            'Have to check the entered information to see if this
        '            'new accountno shall be exported to the same file as the
        '            'previously used one
        '            'If not set bMultiExportFiles=True
        '            'Have to store values in the static variables depending on the input.
        '            Set oAccount = oProfile.FormatsUsed(FormatUsed_ID()).Filenames(1).Clients(iClientIndexNew).Accounts.Add
        '            oAccount.Account = sI_AccountNo
        ''            oAccount.OCR_Id
        '            oAccount.Changed = True
        '
        '            bMultiExportFiles = True
        '        End If
        '    End If
        '
        '    sI_AccountNoOld = sI_AccountNo
        '
        'Else
        '
        'End If
        '
        ''If it's the first payment we enter in a Babelfile, the bMultiExportFiles is set to false
        'If bFirstTime = True Then
        '    bMultiExportFiles = False
        'End If

        'Returns the exportformat, now stored in iFormatOutOld
        CheckAccountNo = iFormatOutOld

    End Function
End Module
