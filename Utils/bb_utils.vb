Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports System.Threading

' ################ For KAR ################
Imports System.IO
Imports System.Net
Imports System.Security.Cryptography.X509Certificates

Module bb_utils
	' Used in reporting
    '31.07.2008 - KOKO added constant NOBREAK
	Public Const NOBREAK As Short = 0
	Public Const BREAK_ON_ACCOUNT As Short = 1 ': Break on productiondate + CreditAccount
	Public Const BREAK_ON_BATCH As Short = 2 ': Break on productiondate + CreditAccount + iBatches
	Public Const BREAK_ON_PAYMENT As Short = 3 ': Break on productiondate + CreditAccount + iBatches + iPayments
	Public Const BREAK_ON_ACCOUNTONLY As Short = 4 ': Break on CreditAccount
	
	Public Const DontAddVoucherNo As Short = 0
	Public Const AddVoucherNoFromERP As Short = 1
	Public Const AddVoucherNoFromBB As Short = 2
	
	'added next 02.02.2009, to be able to exit BB with errorcode
	Declare Function ExitProcess Lib "kernel32" (ByVal exitCode As Integer) As Integer
	'added next 02.02.2009, to be able to exit BB with errorcode
	Declare Sub CoUninitialize Lib "ole32" ()
	
	Public Declare Function ShellExecute Lib "shell32.dll"  Alias "ShellExecuteA"(ByVal hwnd As Integer, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Integer) As Integer
	Public Const SW_SHOW As Short = 5
	
	' TESTCODE RUNSHELL
	'*************************************************
	'TEST EXITCODE
	Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Integer, ByVal bInheritHandle As Integer, ByVal dwProcessId As Integer) As Integer
	
	Private Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Integer, ByRef lpExitCode As Integer) As Integer
	
	Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Integer) As Integer
	
	'*************************************************
	
	Private Const PROCESS_QUERY_INFORMATION As Integer = &H400
	Private Const STATUS_PENDING As Integer = &H103
    
    Public Function BB_DatabasePath(Optional ByVal bDEBUG As Boolean = False, Optional ByVal bTestLicense As Boolean = True) As String
        ' Find the path for BabelBanks database

        BB_DatabasePath = ""
        Try
            '22.12.2016 - Added next IF
            If BB_UseSQLServer() Then
                BB_DatabasePath = FindBBConnectionString()
            Else
                BB_DatabasePath = GetEnvironment("BabelBase")
            End If
        Catch ex As Exception
            MsgBox("Error in BB_DatabasePath - Please restart BabelBank")
            'End
        End Try

    End Function
    Public Function BB_LicensePath() As String
        ' Find the path for BabelBanks licensefile
        BB_LicensePath = ""
        Try
            BB_LicensePath = GetEnvironment("BabelLicense")
        Catch ex As Exception
            MsgBox("Error in BB_LicensePath - Please restart BabelBank")
            'End
        End Try

    End Function

    
    Public Function GetEnumFileType(ByRef iEnumNo As Object) As String
        Dim sFileType As String

        Select Case iEnumNo
            Case -1
                sFileType = "Unknown"
            Case 1
                sFileType = "OCR"
            Case 2
                sFileType = "Autogiro"
            Case 3
                sFileType = "Direkte remittering"
            Case 4
                sFileType = "Telepay ver. 1"
            Case 5
                sFileType = "Coda"
            Case 6
                sFileType = "Postbanken OCR Old"
            Case 7
                sFileType = "Autogiro engangsfullmakt"
            Case 8
                sFileType = "Telepay TBIO"
            Case 9
                sFileType = "ErhvervsGiro-Udbetaling"
            Case 10
                sFileType = "Leverant�rsbetalningar"
            Case 11
                sFileType = "Handelsbanken_DK_Domestic"
            Case 12
                sFileType = "OCR-Simulated"
            Case 13
                sFileType = "Hungarian Domestic Payments (HDP) - Citibank"
            Case 14
                sFileType = "Nordea Unitel PC"
            Case 15
                sFileType = "DnB TBI Payments"
            Case 16
                sFileType = "Bankgirot OCR"
            Case 17
                sFileType = "Postgirot OCR"
            Case 18
                sFileType = "Citibank Czech Republic"
            Case 19
                sFileType = "DnB TBIW Standard"
            Case 20
                sFileType = "DTAUS"
            Case 21
                sFileType = "ShipNet"
            Case 22
                sFileType = "CitiDirect US Flat File"
            Case 23
                sFileType = "DanskeBank TeleService"
            Case 24
                sFileType = "BACS for TBI"
            Case 25
                sFileType = "First Union National Bank - ACH"
            Case 26
                sFileType = "DnB Nattsafefil"
            Case 27
                sFileType = "Securitas Nattsafefil"
            Case 28
                sFileType = "Navision for SI_Data"
            Case 29
                sFileType = "MT 940"
            Case 30
                sFileType = "DanskeBank LokaleDanske"
            Case 31
                sFileType = "DnBTBUKIncoming"
            Case 32
                sFileType = "BabelBank Innbetaling"
            Case 33
                sFileType = "Telepay"
            Case 34
                sFileType = "DnB TBIW Danmark"
            Case 35
                sFileType = "LM Domestic"
            Case 36
                sFileType = "LUM International"
            Case 37
                sFileType = "PBS"
            Case 38
                sFileType = "BEC"
            Case 39
                sFileType = "Excel"
            Case 40
                sFileType = "Sydbank"
            Case 41
                sFileType = "SEK 2"
            Case 42
                sFileType = "DnB Kontoinformasjon"
            Case 43
                sFileType = "Bellin RAX CM"
            Case 44
                sFileType = "Nordea DK, EDI"
            Case 45
                sFileType = "Excel (XLS)"
            Case 46
                sFileType = "Bankdata 4.0 DK"
            Case 47
                sFileType = "SebScreen International, fullformat"
            Case 48
                sFileType = "SebScreen Denmark, fullformat"
            Case 49
                sFileType = "SebScreen Germany, fullformat"
            Case 50
                sFileType = "Bankdata 4.0/4.5 DK"
            Case 51
                sFileType = "Automatisk avprickning"
            Case 52
                sFileType = "Bankgirot OCR Simulated"
            Case 53
                sFileType = "Nordea Innbetaling Kto.inf. utland"
            Case 54
                sFileType = "Bankdata 4.5 Danmark"
            Case 55
                sFileType = "Teller - XML oppgj�rsfil"
            Case 56
                sFileType = "BOLS"
            Case 60
                sFileType = "SEB Screen"
            Case 61
                sFileType = "Handelsbanken GlobalOn-line (MT101)"

            Case 65
                sFileType = "Handelbanken MT940E Just Credit"
            Case 66
                sFileType = "BabelBank Mapping"
            Case 67
                sFileType = "K-LINK Excel-utbet"
            Case 71
                sFileType = "TelepayPlus"
            Case 61
                sFileType = "Handelsbanken MT940E Credit & Debit"
            Case 74
                sFileType = "BgMax"
            Case 78
                sFileType = "ANB Amro BTL91"
            Case 79
                sFileType = "Handelsbanken Bankavstemming"
                'XNET - 29.11.2010 - Added from here
            Case 84
                sFileType = "DTAZV - Cross border"
            Case 88
                sFileType = "Pain.001 (ISO 20022)"
            Case 1183
                sFileType = "Pain.001 (ISO 20022)"
            Case 90
                sFileType = "Pain.002 (ISO 20022)"
            Case 93
                sFileType = "Camt.054 Debit (ISO 20022)"
            Case 94
                sFileType = "Camt.054 Credit (ISO 20022)"
            Case 95
                sFileType = "Camt.054 (ISO 20022)"
            Case 99
                sFileType = "Generic Text"

            Case 102
                sFileType = "Avtalegiro"
                'XokNET 17.01.2013 - Added next case
            Case 104
                sFileType = "Nordea Fakturabetalningsservice"
            Case 107
                sFileType = "SEB Sisu"
            Case 111
                sFileType = "Nordea CorporateFilePayments"
            Case 128
                sFileType = "Corporate_Banking_Interbancario"

            Case vbBabel.BabelFiles.FileType.Camt053
                sFileType = "Camt.053"
                'XokNET 13.02.2014 added next
            Case vbBabel.BabelFiles.FileType.Total_IN
                sFileType = "TotalIn"


            Case 501
                sFileType = "CREMUL"
            Case 502
                sFileType = "PAYMUL"
            Case 503
                sFileType = "BANSTA"
            Case 504
                sFileType = "DEBMUL"
            Case 505
                sFileType = "CONTRL"
            Case 507
                sFileType = "COACSU"
            Case 901
                sFileType = "SG_CSV_OCR"
            Case 907
                sFileType = "Manuelt registret Nordea Liv"

            Case 995
                sFileType = "Bankgirot OCR-Simulated with KID"
            Case 996
                sFileType = "BB Auto Detect - incoming payments"
            Case 997
                sFileType = "One-time format"
            Case 998
                sFileType = "Database"
            Case 999
                sFileType = "Report only"
            Case 1001
                sFileType = "Capsy Autogiro-engangsfullmakt"
            Case 1002
                sFileType = "Excel utbetalinger for Orkla Media"
            Case 1003
                sFileType = "Excel utbetalinger for Oriflame"
            Case 1004
                sFileType = "Testfil for CREMUL"
            Case 1005
                sFileType = "Telepay BETFOR0099"
            Case 1006
                sFileType = "Telepay ISS Husleie"
            Case 1007
                sFileType = "LHL Ekstern"
            Case 1008
                sFileType = "ISS LonnsTrekk"
            Case 1009
                sFileType = "SRI Access"
            Case 1010
                sFileType = "SRI Access Retur"
            Case 1011
                sFileType = "e-Commerce Logistics"
            Case 1012
                sFileType = "Compuware"
            Case 1013
                sFileType = "SeaTruck"
            Case 1014
                sFileType = "WinOrg"
            Case 1015
                sFileType = "Telepay Unix"
            Case 1016
                sFileType = "VB Intern"
            Case 1017
                sFileType = "Elkj�p In-House"
            Case 1018
                sFileType = "Tazett Autogiro"
            Case 1019
                sFileType = "ISS P�leggstrekk"
            Case 1020
                sFileType = "3M Reiseregning"
            Case 1021
                sFileType = "Maritech Innbetalinger"
            Case 1022
                sFileType = "OCR for BKK"
            Case 1023
                sFileType = "Navision SIData Reskontro"
            Case 1024
                sFileType = "BACS for Agresso"
            Case 1025
                sFileType = "Control Consult Excel"
            Case 1026
                sFileType = "DnB Stockholm Excel"
            Case 1027
                sFileType = "QXL OCR"
            Case 1028
                sFileType = "First Nordic"
            Case 1029
                sFileType = "Kvaerner Singapore Salary"
            Case 1030
                sFileType = "File to DnB Hamburg"
            Case 1031
                sFileType = "Visma Global"
            Case 1032
                sFileType = "PGS"
            Case 1033
                sFileType = "Sandvik Tamrock"
            Case 1034
                sFileType = "Norwegian Cruise Line Report"
            Case 1035
                sFileType = "Creno XML"
            Case 1036, 902
                sFileType = "Rikstrygdeverket" 'This format is not added to formats in the DB.
            Case 1037
                sFileType = "PINK/LAAN"
            Case 1038
                sFileType = "Agresso_Lindorff"
            Case 1039
                sFileType = "Panfish_Scotland"
            Case 1040
                sFileType = "NextFinancial_Incoming"
            Case 1041
                sFileType = "Autocare_Norge"
            Case 1042
                sFileType = "StrommeSingapore"
            Case 1043
                sFileType = "PGSUK_Salary"
            Case 1044
                sFileType = "EltekSG_Salary"
            Case 1045
                sFileType = "Sudjaca"
            Case 1046
                sFileType = "Tandberg_ACH"
            Case 1047
                sFileType = "Visma_Business"
            Case 1048
                sFileType = "Sage_UK_Bacs"
            Case 1049
                sFileType = "DNV Germany"
            Case 1050
                sFileType = "Axapta - Columbus IT"
            Case 1051
                sFileType = "CyberCity"
            Case 1052
                sFileType = "Navision_KonicaMinolta"
            Case 1053
                sFileType = "NSA"
            Case 1054
                sFileType = "Aquarius Outgoing"
            Case 1055
                sFileType = "TV Aksjonen"
            Case 1056
                sFileType = "Kredinor K90"
            Case 1057
                sFileType = "SGFINANSBOLS"
            Case 1058
                sFileType = "Aquarius Incoming"
            Case 1059
                sFileType = "Aquarius Incoming OCR"
            Case 1060
                sFileType = "SG_ClientReporting"
            Case 1061
                sFileType = "SG_CSV_All"
            Case 1062
                sFileType = "SI_SIRI"
            Case 1063
                sFileType = "Politiets Fellesforbund CSV"
            Case 1064
                sFileType = "SGFinans Fakturafil"
            Case 1065
                sFileType = "AIG Kontoinfo"
            Case 1067
                sFileType = "Newphone fakturafiler"
            Case 1072
                sFileType = "Skagen fondene innbetalinger"
            Case 1078
                sFileType = "DNBUS_ACH_Incoming"
            Case 1079
                sFileType = "Eurosoft eiendom"
            Case 1080
                sFileType = "SG_0001327_StrandCoFactoring"
            Case 1082
                sFileType = "PGS_Sweden"
            Case 1083
                sFileType = "Bluewater_Retur = 1083"
            Case 1084
                sFileType = "Silver_XML"
            Case 1085
                sFileType = "S2000 Paymul"
            Case 1089
                sFileType = "SG Saldofil"
            Case 1090
                sFileType = "SG Saldofil SDV"

            Case 1104
                sFileType = "Nordea Liv bankavstemming"
            Case 1114
                sFileType = "DnV UK Sage"
            Case 1116
                sFileType = "OCR for Santander"
            Case 1120
                sFileType = "Gjensidige Utbytte"
            Case 1122
                sFileType = "DNBUS_ACH_Matching"
            Case 1123
                sFileType = "Ulefos Salary_FI"
            Case 1124
                sFileType = "INS2000"
            Case 1125
                sFileType = "Nemko Canada"
            Case 1185
                sFileType = "NHC Philippines"

            Case 1501
                sFileType = "TBI_General"
            Case 1502
                sFileType = "TBI_FI"
            Case 1503
                sFileType = "TBI_ACH"
            Case 1504
                sFileType = "TBI_Salary"
            Case 1505
                sFileType = "TBI_Reference"
            Case 1506
                sFileType = "TBI_Local"
            Case vbBabel.BabelFiles.FileType.KTL
                sFileType = "KTL"
            Case Else
                sFileType = LRS(60058)
        End Select
        GetEnumFileType = sFileType

    End Function
	
    Public Function GetEnumBank(ByVal iEnumNo As vbBabel.BabelFiles.Bank) As String
        Dim sBank As String

        Select Case iEnumNo
            Case -1
                sBank = "Unknown"
            Case 0
                sBank = "No_Bank_Specified"
            Case 1
                sBank = "Nordea eGateway"
            Case 1001
                sBank = "CitiDirect"
            Case 10001
                sBank = "DNB (USA)"
            Case 31101
                sBank = "ABN Amro (Holland)"
            Case 31102
                sBank = "ING Bank (Holland)"
            Case 31103
                sBank = "Rabobank (Netherlands)"
            Case 35102
                sBank = "Commerz Bank (Portugal)"
            Case 35801
                sBank = "Nordea Finland"
            Case 35802
                sBank = "DNB (Finland)"
            Case 35803
                sBank = "S|E|B (Finland)"
            Case 36000
                sBank = "A hungarian bank"
            Case 36001
                sBank = "Citibank (Hungary)"
            Case 42000
                sBank = "A czech bank"
            Case 42001
                sBank = "Citibank (Czech Republic)"
            Case 44000
                sBank = "A british bank"
            Case 44001
                sBank = "HSBC (UK)"
            Case 45000
                sBank = "A danish bank"
            Case 45001
                sBank = "Danske Bank (Danmark)"
            Case 45002
                sBank = "Nordea (Danmark)"
            Case 45003
                sBank = "BG Bank (Danmark)"
            Case 45004
                sBank = "Sydbank (Danmark)"
            Case 46000
                sBank = "A swedish bank"
            Case 46001
                sBank = "Svenska Handelsbanken"
            Case 46002
                sBank = "Nordea Sverige"
            Case 46003
                sBank = "SEB Sverige"
            Case 46004
                sBank = "Foreningssparebanken"
            Case 46005
                'XokNET 21.12.2011 Changed from DNB NOR
                sBank = "DnB Sverige"
            Case 47000
                sBank = "Norsk bank"
            Case 47001
                'XokNET 21.12.2011 Changed from DNB NOR
                sBank = "DNB (Norge)"
            Case 47002
                sBank = "Nordea (Norge)"
            Case 47003
                sBank = "Gjensidige (Norge)"
            Case 47004
                sBank = "Handelsbanken (Norge)"
            Case 47005
                sBank = "Fokus Bank (Norge)"
            Case 47006
                sBank = "SEB (Norge)"
            Case 47007
                sBank = "Danske Bank (Norge)"
            Case 47008
                sBank = "Sparebank1 (Norge)"
            Case 47009
                sBank = "Sparebanken Midt (Norge)"
            Case 47010
                sBank = "BBS (Norge)"
            Case 47011
                sBank = "Fellesdata (Norge)"
            Case 47012
                sBank = "DNB INPS (Norge)"
            Case 47013
                sBank = "Swedbank"
            Case 49000
                sBank = "A German bank"
            Case 49001
                sBank = "S|E|B (Deutschland)"
            Case 49002
                sBank = "DNB (Deutschland)"
            Case Else
                sBank = "Unknown"
        End Select
        GetEnumBank = sBank

    End Function
	Public Function FindExistingClient(ByRef oProfile As vbBabel.Profile, ByRef iFileSetup_ID As Short, ByRef sAccountNo As String) As vbBabel.Client
		Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client = Nothing
		Dim oaccount As vbBabel.Account
		Dim bFoundClient As Boolean
		Dim bExitLoop As Boolean
		
		bExitLoop = False
		
		Do Until bExitLoop
			bFoundClient = False
			For	Each oFilesetup In oProfile.FileSetups
				If Val(CStr(oFilesetup.FileSetup_ID)) = iFileSetup_ID Then
					For	Each oClient In oFilesetup.Clients
						For	Each oaccount In oClient.Accounts
							If oaccount.Account = sAccountNo Then
								'Account and client fount
								bFoundClient = True
								Exit For
							End If
						Next oaccount
						If bFoundClient Then
							bExitLoop = True
							Exit For
							
						End If
					Next oClient
					If Not bFoundClient Then
						'UPGRADE_NOTE: Object FindExistingClient may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
						FindExistingClient = Nothing
						bExitLoop = True
						Exit Function
					Else
						bExitLoop = True
					End If
					'Found the correct FileSetup_ID, no need to continue
					Exit For 'oFilesetup
				End If
			Next oFilesetup
		Loop 
		
		FindExistingClient = oClient
		
		
	End Function
	
	
	'Public Function UpdateProfileWithClient(oProfile As vbbabel.Profile, oFileSetup As vbbabel.FileSetup,
	Public Function UpdateProfileWithClient(ByRef oProfile As vbBabel.Profile, ByRef oFilesetup As vbBabel.FileSetup, ByRef sClientNo As String, ByRef sClientName As String, ByRef aNewAccounts() As String, ByRef sOwnRef As String, ByRef sDivision As String, ByRef sCompanyNo As String, ByRef sSequenceNo As String, ByRef iFilesetupOut As Short, ByRef bFirstUpdate As Boolean, Optional ByVal iNotification1 As Short = -1, Optional ByRef iNotification2 As Short = -1, Optional ByRef iNotification3 As Short = -1, Optional ByRef nOldClientID As Double = -1) As Object
		
		'Dim oClient As Object, oClientLocal As Object  'vbbabel.Client,
        Dim oClient As vbBabel.Client, oClientLocal As vbBabel.Client
		Dim oaccount As vbBabel.Account
		Dim nNextAccount_ID, nNextClient_ID, nNextClientFormat_ID As Double
		Dim iCountAccounts As Short
		Dim oFilesetupLocal As vbBabel.FileSetup
		Dim bClientExists, bNewClientFormatID As Boolean
		Dim sAccountString As String
		
		oProfile.Status = 2
		oFilesetup.Status = 2
		
		'If nOldClientID is <> -1 the call comes from the SETUP.DLL and signifies
		' that there has been a change on an existing client. Still however
		' the client may have been added in a new filesetup
		
		
		bClientExists = False
		If nOldClientID <> -1 Then
			For	Each oClient In oFilesetup.Clients
				If oClient.Client_ID = nOldClientID Then
					bClientExists = True
					Exit For
				End If
			Next oClient
		Else
			'Check if the clientno already exists. It can exist even if
			' the old clientID is set as -1. If so just do changes in the client.
			For	Each oClient In oFilesetup.Clients
				If sClientNo = oClient.ClientNo Then
					bClientExists = True
                    oClient.Status = vbBabel.Profile.CollectionStatus.Changed
					Exit For
				End If
			Next oClient
		End If
		
		bNewClientFormatID = True
		If iFilesetupOut <> oFilesetup.FileSetup_ID Then
			For	Each oClientLocal In oProfile.FileSetups(iFilesetupOut).Clients
				If sClientNo = oClientLocal.ClientNo Then
					If oClientLocal.FormatOut = oFilesetup.FileSetup_ID Then
						bNewClientFormatID = False
					End If
				End If
			Next oClientLocal
		End If
		
		If Not bClientExists Then
			'Find the last used Client_id and ClientFormat_id
			nNextClient_ID = 0
			nNextClientFormat_ID = 0
			For	Each oFilesetupLocal In oProfile.FileSetups
				For	Each oClient In oFilesetupLocal.Clients
					If oClient.Client_ID > nNextClient_ID Then
						nNextClient_ID = oClient.Client_ID
					End If
					If oClient.ClientFormat_ID > nNextClientFormat_ID Then
						nNextClientFormat_ID = oClient.ClientFormat_ID
					End If
				Next oClient
			Next oFilesetupLocal
			
			'If it is the first update we'll have to create a new ClientFormat_ID
			'The highest existing plus one, else use the highest existing
			If bFirstUpdate Then
				nNextClient_ID = nNextClient_ID + 1
			End If
			If bNewClientFormatID Then
				nNextClientFormat_ID = nNextClientFormat_ID + 1
			End If
			
			If nOldClientID <> -1 Then
				oClient = oFilesetup.Clients.Add(Str(nOldClientID))
				nNextClient_ID = nOldClientID
			Else
				'Add a new client to the oProfile object
				oClient = oFilesetup.Clients.Add(Str(nNextClient_ID))
			End If
			oClient.Client_ID = nNextClient_ID
			oClient.ClientFormat_ID = nNextClientFormat_ID
			If bFirstUpdate Then
				oClient.FormatIn = oFilesetup.FileSetup_ID
				oClient.FormatOut = iFilesetupOut 'oFileSetup.FileSetupOut
			Else
				oClient.FormatIn = iFilesetupOut
				oClient.FormatOut = oFilesetup.FileSetup_ID
			End If
			'FIX: What's this
			'oClient.Outgoing
			oClient.ClientNo = sClientNo
			oClient.Name = sClientName
			oClient.Division = sDivision
			If Len(Trim(sCompanyNo)) > 0 Then
				oClient.CompNo = sCompanyNo
			End If
			oClient.OwnRefText = sOwnRef
			'    If nOldClientID = -1 Then
            oClient.Status = vbBabel.Profile.CollectionStatus.NewCol
			'    Else
			'        oClient.Status = Changed
			'    End If
			oClient.SeqNoTotal = Val(sSequenceNo)
			oClient.Notification1 = iNotification1
			oClient.Notification2 = iNotification2
			oClient.Notification3 = iNotification3
			
		Else
			
			oClient.ClientNo = sClientNo
			oClient.Name = sClientName
			oClient.Division = sDivision
			If Len(Trim(sCompanyNo)) > 0 Then
				oClient.CompNo = sCompanyNo
			End If
			oClient.OwnRefText = sOwnRef
			oClient.Notification1 = iNotification1
			oClient.Notification2 = iNotification2
			oClient.Notification3 = iNotification3
            oClient.Status = vbBabel.Profile.CollectionStatus.Changed
			
		End If
		
		If nOldClientID <> -1 Then
			'UPGRADE_NOTE: Object oClient.Accounts may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
			oClient.Accounts = Nothing
		End If
		
		
		nNextAccount_ID = 0
		For	Each oaccount In oClient.Accounts
			If oaccount.Account_ID > nNextAccount_ID Then
				nNextAccount_ID = oaccount.Account_ID
			End If
		Next oaccount
		
		
		'FIX: test p� Array_ISEmpty(..) Lagt inn av JanP 18.06.01 - sjekk med Kjell
		If Not Array_IsEmpty(aNewAccounts) Then
			For iCountAccounts = 0 To UBound(aNewAccounts)
				' FIX: Nye tester lagt inn av JanP - Setup gir en array med alle konti,
				' ikke kun nye - derfor denne test
				If aNewAccounts(iCountAccounts) <> "" Then
					'If NewAccount(aNewAccounts(iCountAccounts), oProfile) Then
					nNextAccount_ID = nNextAccount_ID + 1
					oaccount = oClient.Accounts.Add(Str(nNextAccount_ID))
					oaccount.Client_ID = nNextClient_ID
					oaccount.Account_ID = nNextAccount_ID
					
					sAccountString = aNewAccounts(iCountAccounts)
					
					If InStr(sAccountString, "/") > 0 Then
						oaccount.Account = Left(sAccountString, InStr(sAccountString, "/") - 1)
						sAccountString = Mid(sAccountString, InStr(sAccountString, "/") + 1)
					Else
						oaccount.Account = sAccountString
						sAccountString = ""
					End If
					
					If InStr(sAccountString, "/") > 0 Then
						oaccount.GLAccount = Left(sAccountString, InStr(sAccountString, "/") - 1)
						sAccountString = Mid(sAccountString, InStr(sAccountString, "/") + 1)
					Else
						oaccount.GLAccount = sAccountString
						sAccountString = ""
					End If
					If InStr(sAccountString, "/") > 0 Then
						oaccount.GLResultsAccount = Left(sAccountString, InStr(sAccountString, "/") - 1)
						sAccountString = Mid(sAccountString, InStr(sAccountString, "/") + 1)
					Else
						oaccount.GLResultsAccount = sAccountString
						sAccountString = ""
					End If
					
					If InStr(sAccountString, "/") > 0 Then
						oaccount.ContractNo = Left(sAccountString, InStr(sAccountString, "/") - 1)
						sAccountString = Mid(sAccountString, InStr(sAccountString, "/") + 1)
					Else
						oaccount.ContractNo = sAccountString
						sAccountString = ""
					End If
					
					' New 27.05.2003 by JanP
					' We lost SWIFTAdress at this stage.
					' SWIFT-adress is not keyed in, it is imported from DnB Accountsfile
					' Must find SWIFT-adress in database, and add it here;
					oaccount.SWIFTAddress = dbSelectAccountSWIFT(oaccount.Account)
					' New 21.01.2004 by Kjell
					' We've lost AvtalegiroID at this stage.
					oaccount.AvtaleGiroID = dbSelectAvtaleGiroID(oaccount.Account)
					
					oaccount.ConvertedAccount = sAccountString
					
					
					'  End If
					'End If
					
					oaccount.Status = 4
					'End If
				End If
			Next iCountAccounts
		End If
		UpdateProfileWithClient = oProfile
		
	End Function
	Function NewAccount(ByRef sAccountNo As String, ByRef oProfile As vbBabel.Profile) As Boolean
		' Check if account is new in oProfile
		
		Dim oClient As vbBabel.Client
		Dim oaccount As vbBabel.Account
		Dim oFilesetup As vbBabel.FileSetup
		Dim bNewAccount As Boolean
		bNewAccount = True
		
		For	Each oFilesetup In oProfile.FileSetups
			For	Each oClient In oFilesetup.Clients
				For	Each oaccount In oClient.Accounts
					If oaccount.Account = sAccountNo Then
						bNewAccount = False
						Exit For
					End If
				Next oaccount
				If bNewAccount = False Then
					Exit For
				End If
			Next oClient
			If bNewAccount = False Then
				Exit For
			End If
		Next oFilesetup
		NewAccount = bNewAccount
		
    End Function
    ' 12.12.2016
    ' added function to recalculate exc.rate from unit 1 to unit 100 for currencies normally given in unit = 100 (like DKK, SEK)
    Public Function RecalculateExcRateTo100(ByVal nExcRate As Double, ByVal sCurrency As String) As Double

        If FindValutaUnit(sCurrency, 1) = 100 Then
            RecalculateExcRateTo100 = nExcRate * 100
        Else
            RecalculateExcRateTo100 = nExcRate * 1
        End If
    End Function

	Public Function FindValutaUnit(ByRef sValutaCode As String, ByRef iTableNo As Short) As Short
		
		Select Case iTableNo
			Case 1
				Select Case sValutaCode
					Case "EUR"
						FindValutaUnit = 1
					Case "SEK"
						FindValutaUnit = 100
					Case "DKK"
						FindValutaUnit = 100
					Case "USD"
						FindValutaUnit = 1
					Case "GBP"
						FindValutaUnit = 1
					Case "JPY"
						FindValutaUnit = 100
					Case "CAD"
						FindValutaUnit = 1
					Case "AUD"
						FindValutaUnit = 1
					Case "NZD"
						FindValutaUnit = 1
					Case "AED"
						FindValutaUnit = 1
					Case "ANG"
						FindValutaUnit = 1
					Case "ARS"
						FindValutaUnit = 1
					Case "AWG"
						FindValutaUnit = 1
					Case "BBD"
						FindValutaUnit = 1
					Case "BHD"
						FindValutaUnit = 1
					Case "BMD"
						FindValutaUnit = 1
					Case "BND"
						FindValutaUnit = 1
					Case "BOB"
						FindValutaUnit = 1
					Case "BOV"
						FindValutaUnit = 1
					Case "BRL"
						FindValutaUnit = 1
					Case "BSD"
						FindValutaUnit = 1
					Case "BWP"
						FindValutaUnit = 1
					Case "BZD"
						FindValutaUnit = 1
					Case "CUP"
						FindValutaUnit = 1
					Case "CYP"
						FindValutaUnit = 1
					Case "DOP"
						FindValutaUnit = 1
					Case "EGP"
						FindValutaUnit = 1
					Case "ETB"
						FindValutaUnit = 1
					Case "FJD"
						FindValutaUnit = 1
					Case "FKP"
						FindValutaUnit = 1
					Case "GIP"
						FindValutaUnit = 1
					Case "GTQ"
						FindValutaUnit = 1
					Case "HKD"
						FindValutaUnit = 1
					Case "HNL"
						FindValutaUnit = 1
					Case "HTG"
						FindValutaUnit = 1
					Case "INR"
						FindValutaUnit = 1
					Case "IQD"
						FindValutaUnit = 1
					Case "ILS"
						FindValutaUnit = 1
					Case "JMD"
						FindValutaUnit = 1
					Case "JOD"
						FindValutaUnit = 1
					Case "KPW"
						FindValutaUnit = 1
					Case "KWD"
						FindValutaUnit = 1
					Case "KYD"
						FindValutaUnit = 1
					Case "LRD"
						FindValutaUnit = 1
					Case "LSL"
						FindValutaUnit = 1
					Case "LTL"
						FindValutaUnit = 1
					Case "LVL"
						FindValutaUnit = 1
					Case "LYD"
						FindValutaUnit = 1
					Case "MDL"
						FindValutaUnit = 1
					Case "MMK"
						FindValutaUnit = 1
					Case "MOP"
						FindValutaUnit = 1
					Case "MTL"
						FindValutaUnit = 1
					Case "MWK"
						FindValutaUnit = 1
					Case "MYR"
						FindValutaUnit = 1
					Case "NAD"
						FindValutaUnit = 1
					Case "NIO"
						FindValutaUnit = 1
					Case "OMR"
						FindValutaUnit = 1
					Case "PAB"
						FindValutaUnit = 1
					Case "PEN"
						FindValutaUnit = 1
					Case "PGK"
						FindValutaUnit = 1
					Case "PKR"
						FindValutaUnit = 1
					Case "QAR"
						FindValutaUnit = 1
					Case "SAR"
						FindValutaUnit = 1
					Case "SBD"
						FindValutaUnit = 1
					Case "SCR"
						FindValutaUnit = 1
					Case "SDD"
						FindValutaUnit = 1
					Case "SGD"
						FindValutaUnit = 1
					Case "SHP"
						FindValutaUnit = 1
					Case "SVC"
						FindValutaUnit = 1
					Case "SZL"
						FindValutaUnit = 1
					Case "TND"
						FindValutaUnit = 1
					Case "TOP"
						FindValutaUnit = 1
					Case "TTD"
						FindValutaUnit = 1
					Case "UYU"
						FindValutaUnit = 1
					Case "UZS"
						FindValutaUnit = 1
					Case "VUV"
						FindValutaUnit = 1
					Case "WST"
						FindValutaUnit = 1
					Case "XCD"
						FindValutaUnit = 1
					Case "YER"
						FindValutaUnit = 1
					Case "ZAR"
						FindValutaUnit = 1
					Case "ZWD"
						FindValutaUnit = 1
						
					Case Else
						FindValutaUnit = 100
				End Select
			Case 2
				'All exchangerates are 1-1
				FindValutaUnit = 1
		End Select
		
	End Function
    Public Function FindDomesticCurrency(ByVal sCountryCode As String, Optional ByRef bIsSEPACountry As Boolean = False) As String
        ' XNET 21.10.2013 Take whole function
        Dim sReturnValue As String

        sReturnValue = vbNullString
        'XOKNET 29.10.2010
        ' IMPORTANT change, added
        bIsSEPACountry = False
        ' in case we are sending a parameter where bISSepaCountry = True into this function, and hits a country
        ' which is not a SEPA-country, where we do not set any value for bISSepaCountry

        'XOKNET 10.11.2010 added BG
        Select Case sCountryCode
            'SEPA countries
            Case "AT"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "BE"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "CY"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "DE"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "ES"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "FI"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "FR"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "GR"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "IE"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "IT"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "LU"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "MT"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "NO"
                sReturnValue = "NOK"
                bIsSEPACountry = True
            Case "NL"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "PT"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "SI"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "SK"
                sReturnValue = "EUR"
                bIsSEPACountry = True
                ' "new" EUR countries
            Case "LV"
                sReturnValue = "EUR"   '"LVL"
                bIsSEPACountry = True
            Case "LT"
                sReturnValue = "EUR"    '"LTL"
                bIsSEPACountry = True
            Case "EE"
                sReturnValue = "EUR"    '"EEK"
                bIsSEPACountry = True



                'Most used countries
            Case "AU"
                sReturnValue = "AUD"
                ' XOKNET added BG 10.11.2010
            Case "BG"
                sReturnValue = "BGN"
                bIsSEPACountry = True
            Case "CA"
                sReturnValue = "CADUSD"
            Case "CH"
                sReturnValue = "CHF"
                bIsSEPACountry = True
            Case "CZ"
                sReturnValue = "CZK"
                bIsSEPACountry = True
            Case "DK"
                sReturnValue = "DKK"
                bIsSEPACountry = True
            Case "GB"
                sReturnValue = "GBP"
                'bIsSEPACountry = True
                ' 22.09.2022 - GB is not a SEPA country anymore
                bIsSEPACountry = False
            Case "HK"
                sReturnValue = "HKD"
            Case "HR"
                ' Croatia
                sReturnValue = "HRK"
                bIsSEPACountry = True
            Case "HU"
                sReturnValue = "HUF"
                bIsSEPACountry = True
            Case "IS"
                sReturnValue = "ISK"
                bIsSEPACountry = True
            Case "LI"
                sReturnValue = "CHF"
                bIsSEPACountry = True
            Case "JP"
                sReturnValue = "JPY"
            Case "NZ"
                sReturnValue = "NZD"
            Case "PL"
                sReturnValue = "PLN"
                bIsSEPACountry = True
            Case "RO"
                sReturnValue = "RON"
                bIsSEPACountry = True
            Case "SE"
                sReturnValue = "SEK"
                bIsSEPACountry = True
            Case "SG"
                sReturnValue = "SGD"
            Case "US"
                sReturnValue = "USD"


                'Strange countries
            Case "AE"
                sReturnValue = "AED"
            Case "AN"
                sReturnValue = "ANG"
            Case "AR"
                sReturnValue = "ARS"
            Case "AW"
                sReturnValue = "AWG"
            Case "BB"
                sReturnValue = "BBD"
            Case "BL" 'Saint Barth�lemy BL FR EUR
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "BH"
                sReturnValue = "BHD"
            Case "BM"
                sReturnValue = "BMD"
            Case "GF"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "GG"
                sReturnValue = "GBP"
                ' 22.09.2022 Guernsey no longer SEPA country
                'bIsSEPACountry = True
                bIsSEPACountry = False
            Case "GI"
                sReturnValue = "GIP"
                bIsSEPACountry = True
            Case "GP"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "JE"
                sReturnValue = "GBP"
                ' 22.09.2022 Jersey no longer SEPA country
                'bIsSEPACountry = True
                bIsSEPACountry = False
            Case "IM"
                sReturnValue = "GBP"
                ' 22.09.2022 Isle of Man no longer SEPA country
                'bIsSEPACountry = True
                bIsSEPACountry = False
            Case "MC"  'Monaco MC MC EUR
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "MF" 'Saint Martin (French part) MF FR EUR
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "MQ"
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "PM" 'Saint Pierre and Miquelon PM FR EUR
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "YT"  'Mayotte YT FR EUR
                sReturnValue = "EUR"
                bIsSEPACountry = True
            Case "IN"   'India
                sReturnValue = "INR"  ' indian rupee
                bIsSEPACountry = False

            Case Else
                sReturnValue = "XXX"

        End Select

        FindDomesticCurrency = sReturnValue


    End Function
    Public Function IsSEPACurrency(ByRef sCurrencyCode As String) As Boolean
        'XNET: 10.11.2010 Added BGL

        If sCurrencyCode = "EUR" Or _
            sCurrencyCode = "NOK" Or _
            sCurrencyCode = "CHF" Or _
            sCurrencyCode = "CZK" Or _
            sCurrencyCode = "DKK" Or _
            sCurrencyCode = "EEK" Or _
            sCurrencyCode = "GBP" Or _
            sCurrencyCode = "HUF" Or _
            sCurrencyCode = "ISK" Or _
            sCurrencyCode = "LVL" Or _
            sCurrencyCode = "LTL" Or _
            sCurrencyCode = "PLN" Or _
            sCurrencyCode = "RON" Or _
            sCurrencyCode = "SEK" Or _
            sCurrencyCode = "GIP" Or _
            sCurrencyCode = "BGL" Then
            IsSEPACurrency = True
        Else
            IsSEPACurrency = False
        End If

    End Function
    ' XNET 09.11.2010 Added new function
    Public Function IsSEPACountry(ByVal sCountryCode As String) As Boolean
        ' Updated 09.11.2010 based on info at www.DnBNOR.no
        Dim bISIbanCountry As Boolean

        bISIbanCountry = False
        Select Case sCountryCode

            'Iban countries
            Case "AD"
                bISIbanCountry = True
            Case "AT"
                bISIbanCountry = True
            Case "BA"
                bISIbanCountry = True
            Case "BE"
                bISIbanCountry = True
            Case "BG"
                bISIbanCountry = True
            Case "CH"
                bISIbanCountry = True
            Case "CY"
                bISIbanCountry = True
            Case "CZ"
                bISIbanCountry = True
            Case "DE"
                bISIbanCountry = True
            Case "DK"
                bISIbanCountry = True
            Case "EE"
                bISIbanCountry = True
            Case "ES"
                bISIbanCountry = True
            Case "FI"
                bISIbanCountry = True
            Case "FR"
                bISIbanCountry = True
            Case "FO"
                bISIbanCountry = True

                ' 13.10.2021
                ' England is not a SEPA country anylonger after Brexit
                ' Have discussed this with H�kon Belt, DNB
            Case "GB"
                bISIbanCountry = False
            Case "GF"
                bISIbanCountry = True

                ' 13.10.2021
                ' England/Guernsey is not a SEPA country anylonger after Brexit
                ' Have discussed this with H�kon Belt, DNB
            Case "GG"
                bISIbanCountry = False
            Case "GI"
                bISIbanCountry = True
            Case "GL"
                bISIbanCountry = True
            Case "GP"
                bISIbanCountry = True
            Case "GR"
                bISIbanCountry = True
            Case "HU"
                bISIbanCountry = True
            Case "IE"
                bISIbanCountry = True

                ' 13.10.2021
                ' England/Isle of Man is not a SEPA country anylonger after Brexit
                ' Have discussed this with H�kon Belt, DNB
            Case "IM"
                bISIbanCountry = False
            Case "IS"
                bISIbanCountry = True
            Case "IT"
                bISIbanCountry = True
            Case "HR"
                bISIbanCountry = True
            Case "JE"
                ' 13.10.2021
                ' England/Jersey is not a SEPA country anylonger after Brexit
                ' Have discussed this with H�kon Belt, DNB
                bISIbanCountry = False
            Case "LU"
                bISIbanCountry = True
            Case "LI"
                bISIbanCountry = True
            Case "LV"
                bISIbanCountry = True
            Case "LT"
                bISIbanCountry = True
            Case "MC"
                bISIbanCountry = True
            Case "ME"
                bISIbanCountry = True
            Case "MK"
                bISIbanCountry = True
            Case "MT"
                bISIbanCountry = True
            Case "MU"
                ' 13.10.2021 - This is Mauritius, which is not a SEPA-country
                'bISIbanCountry = True
                bISIbanCountry = False
            Case "MQ"
                bISIbanCountry = True
            Case "NL"
                bISIbanCountry = True
            Case "NO"
                bISIbanCountry = True
            Case "PL"
                bISIbanCountry = True
            Case "PT"
                bISIbanCountry = True
            Case "RO"
                bISIbanCountry = True
            Case "RE"
                bISIbanCountry = True
                ' 13.11.2018 - Serbia er ikke i SEPA !!!
                'Case "RS"
                'bISIbanCountry = True
            Case "SI"
                bISIbanCountry = True
            Case "SK"
                bISIbanCountry = True
            Case "SE"
                bISIbanCountry = True
            Case "TN"
                bISIbanCountry = True
                ' 07.02.2020 - Tyrkia er ikke SEPA land. Det skulle tatt seg ut.
                'Case "TR"
                '   bISIbanCountry = True


        End Select
        IsSEPACountry = bISIbanCountry

    End Function
    ' XokNET 23.12.2013 Added new function
    Public Function IsEUCountry(ByVal sCountryCode As String) As Boolean
        Dim bIsEUCountry As Boolean

        bIsEUCountry = False
        Select Case sCountryCode
            Case "AT" 'Austria AT AT EUR
                bIsEUCountry = True
            Case "BE" 'Belgium BE BE EUR
                bIsEUCountry = True
            Case "BG" 'Bulgaria BG BG BGN
                bIsEUCountry = True
            Case "HR" 'Croatia HR HR HRK
                bIsEUCountry = True
            Case "CY" 'Cyprus CY CY EUR
                bIsEUCountry = True
            Case "CZ" 'Czech Republic CZ CZ CZK
                bIsEUCountry = True
            Case "DK" 'Denmark DK DK DKK
                bIsEUCountry = True
            Case "EE" 'Estonia EE EE EUR
                bIsEUCountry = True
            Case "FI" 'Finland FI FI EUR
                bIsEUCountry = True
            Case "FR" 'France FR FR EUR
                bIsEUCountry = True
            Case "GF" 'French Guiana GF FR EUR
                bIsEUCountry = True
            Case "DE" 'Germany DE DE EUR
                bIsEUCountry = True
            Case "GI" 'Gibraltar GI GI GIP
                bIsEUCountry = True
            Case "GR" 'Greece GR GR EUR
                bIsEUCountry = True
            Case "GP" 'Guadeloupe GP FR EUR
                bIsEUCountry = True
            Case "HU" 'Hungary HU HU HUF
                bIsEUCountry = True
            Case "IE" 'Ireland IE IE EUR
                bIsEUCountry = True
            Case "IT" 'Italy IT IT EUR
                bIsEUCountry = True
            Case "LV" 'Latvia LV LV LVL
                bIsEUCountry = True
            Case "LT" 'Lithuania LT LT LTL
                bIsEUCountry = True
            Case "LU" 'Luxembourg LU LU EUR
                bIsEUCountry = True
            Case "MT" 'Malta MT MT EUR
                bIsEUCountry = True
            Case "MQ" 'Martinique MQ FR EUR
                bIsEUCountry = True
            Case "YT" 'Mayotte YT FR EUR
                bIsEUCountry = True
            Case "MC" 'Monaco MC MC EUR
                bIsEUCountry = True
            Case "NL" 'Netherlands NL NL EUR
                bIsEUCountry = True
            Case "PL" 'Poland PL PL PLN
                bIsEUCountry = True
            Case "PT" 'Portugal PT PT EUR
                bIsEUCountry = True
            Case "RE" 'R�union RE FR EUR
                bIsEUCountry = True
            Case "RO" 'Romania RO RO RON
                bIsEUCountry = True
            Case "BL" 'Saint Barth�lemy BL FR E
                bIsEUCountry = True
            Case "MF" 'Saint Martin (French part) MF FR EUR
                bIsEUCountry = True
            Case "PM" 'Saint Pierre and Miquelon PM FR EUR
                bIsEUCountry = True
            Case "SK" 'Slovakia SK SK EUR
                bIsEUCountry = True
            Case "SI" 'Slovenia SI SI EUR
                bIsEUCountry = True
            Case "ES" 'Spain ES ES EUR
                bIsEUCountry = True
            Case "SE" 'Sweden SE SE SEK
                bIsEUCountry = True
            Case "GB" 'United Kingdom GB GB GBP
                bIsEUCountry = True
            Case "GG"   ' Guernsey
                bIsEUCountry = True
            Case "JE"   ' Jersey
                bIsEUCountry = True
            Case "IM"   ' Isle of Man
                bIsEUCountry = True

        End Select

        IsEUCountry = bIsEUCountry
    End Function

    Public Function SplitFI(ByRef sInputString As String, ByRef iElementNo As Short) As String
        ' Read a FI-string, complete with Kortart, BetalingsID and KreditorNo
        ' Return either
        ' 1) KreditorNo or
        ' 2) Kortartkode+BetalingsID
        ' Optional: Validate FI-card string

        ' How;
        ' Kortartkode  Betalingsid      Kreditorno
        '<blank>        19 pos, spesialbehandles
        '  44/48        16 pos          8 pos
        '  01           Empty           6 or 7 pos
        '  04           16 pos          6 or 7 or 8 pos
        '  15           16 pos
        '  44
        '  48
        '  71           15 pos          8 pos
        '  73           Empty           8 pos
        '  75           16 pos          8 pos

        ' Not complete! FIX: validation

        If iElementNo = 1 Then

            ' Return Kreditorno
            ' Special FI, 19 digits totalID + 8 digits Creditorno;
            If Len(sInputString) = 27 Then
                SplitFI = Mid(sInputString, 20, 8)
            Else
                Select Case Left(sInputString, 2)
                    Case "01"
                        SplitFI = Mid(sInputString, 3, 7)
                    Case "04"
                        SplitFI = Mid(sInputString, 19)
                    Case "15"
                        SplitFI = Mid(sInputString, 19)
                    Case "71"
                        SplitFI = Mid(sInputString, 18, 8)
                    Case "73"
                        SplitFI = Mid(sInputString, 3, 8)
                    Case "75"
                        SplitFI = Mid(sInputString, 19, 8)
                    Case Else
                        ' treat as blank
                        SplitFI = ""
                End Select
            End If
        Else
            ' return Kortart+BetalingsID
            ' Special FI, 19 digits totalID + 8 digits Creditorno;
            If Len(sInputString) = 27 Then
                SplitFI = Left(sInputString, 19)
            Else
                Select Case Left(sInputString, 2)
                    Case "01"
                        SplitFI = ""
                    Case "04"
                        SplitFI = Left(sInputString, 18)
                    Case "15"
                        SplitFI = Left(sInputString, 18)
                    Case "71"
                        SplitFI = Left(sInputString, 17)
                    Case "73"
                        SplitFI = "73"
                    Case "75"
                        SplitFI = Left(sInputString, 18)
                    Case Else
                        ' treat as blank
                        SplitFI = Left(sInputString, 19)
                End Select
            End If
        End If

    End Function
    Public Function ValidateFI(ByVal sFIString As String, ByRef sKortartKode As String, ByRef sBetalerIdent As String, ByRef sKreditorNo As String, ByRef eFormat As vbBabel.BabelFiles.FileType, ByRef sErrorString As String, Optional ByRef bReplaceHyphen As Boolean = False) As Boolean
        'Public Function ValidateFI(ByVal sFIString As String, eFormat As FileType, ByRef sErrorString As String, Optional bReplaceHyphen As Boolean = False) As Boolean
        Dim bReturnValue As Boolean

        If bReplaceHyphen Then
            sFIString = Replace(sFIString, "-", "")
        End If

        bReturnValue = True

        If Not vbIsNumeric(sFIString, "0123456789") Then
            sErrorString = LRSCommon(20006) & LRSCommon(20007) & sFIString
            'sErrorString = "The code on the 'indbetalingskort' must be numeric. Code stated: " & sFIString
        End If

        If Len(sFIString) = 27 Then
            ' Special FI, 19 digits totalID + 8 digits Creditorno;
            If eFormat = vbBabel.BabelFiles.FileType.TBIXML Then
                'Not allowed in TBI
                sErrorString = LRSCommon(20006) & LRSCommon(20007) & sFIString
                bReturnValue = False
                'sErrorString = "The FIK-code is not valid. Code stated: " & sFIString
            Else
                sKortartKode = Left(sFIString, 1)
                sBetalerIdent = Mid(sFIString, 4, 16)
                sKreditorNo = Mid(sFIString, 17)
            End If
        Else
            Select Case Left(sFIString, 2)
                'Kortarkcode
                Case "01"
                    sKortartKode = "01"
                    sKreditorNo = SplitFI(sFIString, 1)
                    sBetalerIdent = ""

                    '17.11.04
                    'In a comment from Torben in the TBI-project, 01-kortart can be followed
                    ' by either 7 or 8 digits. Changed code here according to this
                    'If Len(sFIString) <> 8 And Len(sFIString) <> 9 Then
                    If Len(sFIString) <> 9 And Len(sFIString) <> 10 Then
                        'sErrorString = LRSCommon(20008, "01", "either 8 or 9") & LRSCommon(20007) & sFIString
                        sErrorString = LRSCommon(20008, "01", " 9 / 10") & LRSCommon(20007) & sFIString
                        bReturnValue = False
                    Else
                        'sKreditorNo
                        'This is probably not a kreditornummer, but a Girokonto

                        '                If Not IsModulus11(sKreditorNo) Then
                        '                    sErrorString = LRSCommon(20009, "Kreditornummer") & sKreditorNo
                        '                    'sErrorString = "The 'kreditornummer' is not valid. The modulus-check reports an error. 'Kreditornummer' stated: " & SplitFI(sFIString, 1)
                        '                    bReturnValue = False
                        '                Else
                        '                    If Val(Left$(sKreditorNo, 1)) < 7 Then
                        '                        sErrorString = LRSCommon(20015, "Kreditornummer") & sKreditorNo
                        '                        '20015: The creditornumber is not valid. The startdigit should be either 7, 8 or 9. '%1' stated:
                        '                        bReturnValue = False
                        '                    End If
                        '                End If
                    End If
                Case "04"
                    sKortartKode = "04"
                    'Changed 29.06.2005 to allow accountno on 8 pos.
                    ' Length: Kortartcode (2) + Betalerident (16) + AccountNo (6, 7 or 8 ??????)
                    sKreditorNo = SplitFI(sFIString, 1)
                    sBetalerIdent = Mid(SplitFI(sFIString, 2), 3)
                    If Len(sFIString) <> 24 And Len(sFIString) <> 25 And Len(sFIString) <> 26 Then
                        sErrorString = LRSCommon(20008, "04", "either 24 or 25 or 26") & LRSCommon(20007) & sFIString
                        'sErrorString = "The length of a 'indbetalingskort' with 'kortartskode' 14, is either 24 or 25 digits long. Code stated: " & sFIString
                        bReturnValue = False
                    Else
                        'If Not IsModulus10(Mid$(SplitFI(sFIString, 2), 3)) Then
                        If Not IsModulus10(sBetalerIdent) Then
                            sErrorString = LRSCommon(20009, "Betaleridentifikation") & sBetalerIdent
                            'sErrorString = "The 'betaleridentifikation' is not valid. The modulus-check reports an error. 'Betaleridentifikation' stated: " & Mid$(SplitFI(sFIString, 2), 3)
                            bReturnValue = False
                        Else
                            'sKreditorNo
                            'This is probably not a kreditornummer, but a Girokonto

                            '                    If Not IsModulus11(sKreditorNo) Then
                            '                        sErrorString = LRSCommon(20009, "Kreditornummer") & sKreditorNo
                            '                        'sErrorString = "The 'kreditornummer' is not valid. The modulus-check reports an error. 'Kreditornummer' stated: " & SplitFI(sFIString, 1)
                            '                        bReturnValue = False
                            '                    Else
                            '                        If Val(Left$(sKreditorNo, 1)) < 7 Then
                            '                            sErrorString = LRSCommon(20015, "Kreditornummer") & sKreditorNo
                            '                            '20015: The creditornumber is not valid. The startdigit should be either 7, 8 or 9. '%1' stated:
                            '                            bReturnValue = False
                            '                        End If
                            '                    End If

                        End If
                    End If
                Case "15"
                    'What the fuck is this? Probably 2 + 16 + ???
                    sKortartKode = "15"
                    sKreditorNo = ""
                    sBetalerIdent = ""
                    If eFormat = vbBabel.BabelFiles.FileType.TBIXML Then
                        sErrorString = LRSCommon(20010, GetEnumFileType(eFormat)) & Left(sFIString, 2)
                        bReturnValue = False
                    End If

                Case "71"
                    'Length: Kortartcode (2) + Betalerident (15) + Creditorno. (8)
                    sKortartKode = "71"
                    sKreditorNo = SplitFI(sFIString, 1)
                    sBetalerIdent = Mid(SplitFI(sFIString, 2), 3)
                    If Len(sFIString) <> 25 Then
                        sErrorString = LRSCommon(20008, "71", "25") & LRSCommon(20007) & sFIString
                        'sErrorString = "The total length of an 'indbetalingskort' with 'kortartskode' 71, is 25 digits long. Code stated: " & sFIString
                        bReturnValue = False
                    Else
                        'If Not IsModulus10(Mid$(SplitFI(sFIString, 2), 3)) Then
                        If Not IsModulus10(sBetalerIdent) Then
                            sErrorString = LRSCommon(20009, "Betaleridentifikation") & sBetalerIdent
                            'sErrorString = "The 'betaleridentifikation' is not valid. The modulus-check reports an error. 'Betaleridentifikation' stated: " & Mid$(SplitFI(sFIString, 2), 3)
                            bReturnValue = False
                        Else
                            If Not IsModulus11(sKreditorNo) Then
                                sErrorString = LRSCommon(20009, "Kreditornummer") & sKreditorNo
                                'sErrorString = "The 'kreditornummer' is not valid. The modulus-check reports an error. 'Kreditornummer' stated: " & SplitFI(sFIString, 1)
                                bReturnValue = False
                            Else
                                If Val(Left(sKreditorNo, 1)) < 7 Then
                                    sErrorString = LRSCommon(20015, "Kreditornummer") & sKreditorNo
                                    '20015: The creditornumber is not valid. The startdigit should be either 7, 8 or 9. '%1' stated:
                                    bReturnValue = False
                                End If
                            End If
                        End If
                    End If
                Case "73"
                    'Length: Kortartcode (2) +  Creditorno. (8)
                    sKortartKode = "73"
                    sKreditorNo = SplitFI(sFIString, 1)
                    sBetalerIdent = ""
                    If Len(sFIString) <> 10 Then
                        sErrorString = LRSCommon(20008, "73", "10") & LRSCommon(20007) & sFIString
                        'sErrorString = "The total length of an 'indbetalingskort' with 'kortartskode' 73, is 10 digits long. Code stated: " & sFIString
                        bReturnValue = False
                    Else
                        'No betaleridentifikation
                        If Not IsModulus11(sKreditorNo) Then
                            sErrorString = LRSCommon(20009, "Kreditornummer") & sKreditorNo
                            'sErrorString = "The 'kreditornummer' is not valid. The modulus-check reports an error. 'Kreditornummer' stated: " & SplitFI(sFIString, 1)
                            bReturnValue = False
                        Else
                            If Val(Left(sKreditorNo, 1)) < 7 Then
                                sErrorString = LRSCommon(20015, "Kreditornummer") & sKreditorNo
                                '20015: The creditornumber is not valid. The startdigit should be either 7, 8 or 9. '%1' stated:
                                bReturnValue = False
                            End If
                        End If
                    End If
                Case "75"
                    'Length: Kortartcode (2) + Betalerident (16) + Creditorno. (8)
                    sKortartKode = "75"
                    sKreditorNo = SplitFI(sFIString, 1)
                    sBetalerIdent = Mid(SplitFI(sFIString, 2), 3)
                    If Len(sFIString) <> 26 Then
                        sErrorString = LRSCommon(20008, "75", "26") & LRSCommon(20007) & sFIString
                        'sErrorString = "The total length of an 'indbetalingskort' with 'kortartskode' 75, is 26 digits long. Code stated: " & sFIString
                        bReturnValue = False
                    Else
                        'If Not IsModulus10(Mid$(SplitFI(sFIString, 2), 3)) Then
                        If Not IsModulus10(sBetalerIdent) Then
                            sErrorString = LRSCommon(20009, "Betaleridentifikation") & sBetalerIdent
                            'sErrorString = "The 'betaleridentifikation' is not valid. The modulus-check reports an error. 'Betaleridentifikation' stated: " & Mid$(SplitFI(sFIString, 2), 3)
                            bReturnValue = False
                        Else
                            If Not IsModulus11(sKreditorNo) Then
                                sErrorString = LRSCommon(20009, "Kreditornummer") & sKreditorNo
                                'sErrorString = "The 'kreditornummer' is not valid. The modulus-check reports an error. 'Kreditornummer' stated: " & SplitFI(sFIString, 1)
                                bReturnValue = False
                            Else
                                If Val(Left(sKreditorNo, 1)) < 7 Then
                                    sErrorString = LRSCommon(20015, "Kreditornummer") & sKreditorNo
                                    '20015: The creditornumber is not valid. The startdigit should be either 7, 8 or 9. '%1' stated:
                                    bReturnValue = False
                                End If
                            End If
                        End If
                    End If
                Case Else
                    sKortartKode = ""
                    sKreditorNo = ""
                    sBetalerIdent = ""
                    sErrorString = LRSCommon(20010, GetEnumFileType(eFormat)) & Left(sFIString, 2)
                    bReturnValue = False
                    ' treat as blank
            End Select
        End If

        ValidateFI = bReturnValue

    End Function
    Public Function ValidateReference(ByRef sReference As String, ByRef sErrorString As String) As Boolean
        Dim bReturnValue As Boolean
        Dim iFactor As Short
        Dim lSum As Integer
        Dim iCDV As Short
        Dim l As Integer

        On Error GoTo ErrorValidateReference

        'sReference = "1234561"
        sReference = Trim(sReference)

        If Not vbIsNumeric(CObj(sReference), "0123456789") Then
            ValidateReference = False
            ''sErrorString = LRSCommon(20006) & LRSCommon(20007) & sReference
            'sErrorString = "The code on the 'indbetalingskort' must be numeric. Code stated: " & sFIString
            ' changed 20.03.2009, by JanP; This is not Indbetalingskort, this is reference !!!!
            sErrorString = LRSCommon(20039) & LRSCommon(20007) & sReference
            'sErrorString = "The referencecode must be numeric. Code stated: " & sFIString
            Exit Function
        End If

        If Len(sReference) < 4 Then
            ValidateReference = False
            sErrorString = LRSCommon(20020, "Reference", "4") & sReference
            '20020: The %1 must be minimum %2 numbers long
            Exit Function
        End If

        If Len(sReference) > 20 Then
            ValidateReference = False
            sErrorString = LRSCommon(20021, "Reference", "20") & sReference
            '20021: The %1 can't be more than %2 numbers long.
            Exit Function
        End If

        iFactor = 7
        lSum = 0

        'The digits in the reference to be verified, are multiplied by
        'the weights 7,3,1,7,3,1,7..... from the right to left.
        For l = Len(sReference) - 1 To 1 Step -1
            lSum = lSum + Val(Mid(sReference, l, 1)) * iFactor
            Select Case iFactor
                Case 1
                    iFactor = 7
                Case 3
                    iFactor = 1
                Case 7
                    iFactor = 3
            End Select
        Next

        iCDV = 10 - Val(Right(Trim(Str(lSum)), 1))

        If iCDV = 10 Then
            iCDV = 0
        End If

        If Val(Right(Trim(sReference), 1)) = iCDV Then
            bReturnValue = True
        Else
            sErrorString = LRSCommon(20009, "Reference") & sReference
            'sErrorString = "The 'Reference' is not valid. The modulus-check reports an error. 'Betaleridentifikation' stated: " & Mid$(SplitFI(sFIString, 2), 3)
            bReturnValue = False
        End If

        ValidateReference = bReturnValue

        Exit Function

ErrorValidateReference:

        sErrorString = Err.Description
        ValidateReference = False

    End Function


    Public Function FindAccSWIFTAddr(ByVal sAccount As String, ByVal oProfile As vbBabel.Profile, ByVal iFilesetupOut As Integer, ByRef sSWIFTAddr As String, Optional ByVal bThisIsTBIXML As Boolean = True, Optional ByRef sDebitAccountCountryCode As String = "", Optional ByRef sDebitAccountCurrencyCode As String = "") As Boolean
        'This function is called from both BabelBank and EDI2XML
        ' The LRS-strings is located in vbCommon
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim bReturnValue As Boolean
        Dim lCounter As Long, lAscCharacter As Long
        Dim bShowNoErrors As Boolean

        bReturnValue = False
        For Each oFilesetup In oProfile.FileSetups
            If oFilesetup.FileSetup_ID = iFilesetupOut Then
                For Each oClient In oFilesetup.Clients
                    For Each oaccount In oClient.Accounts
                        If Trim$(oaccount.Account) = Trim$(sAccount) Or Trim$(oaccount.ConvertedAccount) = Trim$(sAccount) Then
                            If Len(oaccount.SWIFTAddress) > 0 Then
                                sSWIFTAddr = Trim(oaccount.SWIFTAddress)
                                ' XokNET 08.09.2011 added sDebitAccountCountryCode
                                sDebitAccountCountryCode = oaccount.DebitAccountCountryCode
                                ' XNET 02.02.2012 - if not set oaccount.DebitAccountCountryCode, must find from Swift
                                If EmptyString(sDebitAccountCountryCode) And Len(sSWIFTAddr) > 5 Then
                                    sDebitAccountCountryCode = Mid$(sSWIFTAddr, 5, 2)
                                End If
                                ' XNET 05.01.2012 added sDebitAccountCurrencyCode
                                sDebitAccountCurrencyCode = Trim(oaccount.CurrencyCode)
                                bReturnValue = True
                            Else
                                If bThisIsTBIXML Then
                                    ' XNET 16.01.2012, removed 15006 in line below
                                    Err.Raise(20014, , LRSCommon(20014, sAccount) & vbCrLf & LRSCommon(20011, oProfile.AccountsFile) & vbCrLf) ' & vbCrLf & LRSCommon(15006)
                                    '17009: No SWIFT-address is registered on the account: %1 + Please transfer the accountfile from TBI Windows +++ description.
                                Else
                                    ' new test 09.09.04
                                    ' Warn only if outformat=PAYMUL in DanskeBank
                                    ' AND also if SEB and ISO20022
                                    If (oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.PAYMUL And (oFilesetup.Bank_ID = vbBabel.BabelFiles.Bank.Danske_Bank_NO Or oFilesetup.Bank_ID = vbBabel.BabelFiles.Bank.Danske_Bank_dk)) _
                                    Or (oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.Pain001 And (oFilesetup.Bank_ID = vbBabel.BabelFiles.Bank.SEB_NO Or oFilesetup.Bank_ID = vbBabel.BabelFiles.Bank.SEB_SE Or oFilesetup.Bank_ID = vbBabel.BabelFiles.Bank.SEB_DK)) Then
                                        '' XNET 16.01.2012, removed 15006 in line below
                                        Err.Raise(20014, , LRSCommon(20014, sAccount) & vbCrLf & LRSCommon(20019) & vbCrLf) ' & vbCrLf & LRSCommon(15006)
                                    Else
                                        bReturnValue = True
                                        bShowNoErrors = True
                                    End If
                                End If
                            End If
                        End If
                    Next oaccount
                Next oClient
            End If
        Next oFilesetup

        If bReturnValue = False Then
            If bThisIsTBIXML Then
                ' XNET 16.01.2012, removed 15006 in line below
                Err.Raise(20012, , LRSCommon(20012, sAccount) & vbCrLf & LRSCommon(20011, oProfile.AccountsFile) & vbCrLf) '& vbCrLf & LRSCommon(15006)
                '17011: The debitaccount %1 is unknown to BabelBank. + Please transfer the accountfile from TBI Windows +++ description.
            Else
                ' XNET 16.01.2012, removed 15006 in line below
                Err.Raise(20012, , LRSCommon(20012, sAccount) & vbCrLf & LRSCommon(20019) & vbCrLf) '& vbCrLf & LRSCommon(15006)
                '17011: The debitaccount %1 is unknown to BabelBank. + Please transfer the accountfile from TBI Windows +++ description.
            End If
        Else
            'Check if it is a valid SWIFT-adress
            If Len(sSWIFTAddr) = 8 Or Len(sSWIFTAddr) = 11 Then
                For lCounter = 1 To Len(sSWIFTAddr)
                    lAscCharacter = Asc(Mid$(sSWIFTAddr, lCounter, 1))
                    If (lAscCharacter > 47 And lAscCharacter < 58) Or (lAscCharacter > 64 And lAscCharacter < 91) Or (lAscCharacter > 96 And lAscCharacter < 123) Then
                        'OK
                    Else
                        bReturnValue = False
                        Exit For
                    End If
                Next lCounter
            Else
                bReturnValue = False
            End If
        End If

        If bReturnValue = False Then
            If bThisIsTBIXML Then
                ' XNET 16.01.2012, removed 15006 in line below
                Err.Raise(20013, , LRSCommon(20013, sSWIFTAddr, sAccount) & vbCrLf & LRSCommon(20011, oProfile.AccountsFile) & vbCrLf) '& vbCrLf & LRSCommon(15006)
                ' 17012: The SWIFT-address " %1 registered on the account: %2 is not valid. + Please transfer the accountfile from TBI Windows +++ description.
            Else
                If bShowNoErrors = False Then
                    ' XNET 16.01.2012, removed 15006 in line below
                    Err.Raise(20013, , LRSCommon(20013, sSWIFTAddr, sAccount) & vbCrLf & LRSCommon(20019) & vbCrLf) '& vbCrLf & LRSCommon(15006)
                End If
            End If
        End If

        oFilesetup = Nothing
        oClient = Nothing
        oaccount = Nothing

        FindAccSWIFTAddr = bReturnValue

    End Function

    Public Function ValidateBankID(ByRef eType As vbBabel.BabelFiles.BankBranchType, ByRef sCode As String, ByRef bIntermediaryBank As Boolean, ByRef sErrorString As String) As Boolean
        Dim bReturnValue As Boolean
        Dim lCounter As Integer
        Dim sChar As String

        On Error GoTo ValidateBankIDError

        'assume failure
        bReturnValue = False
        sCode = UCase(sCode)

        Select Case eType

            Case vbBabel.BabelFiles.BankBranchType.NoBranchType, vbBabel.BabelFiles.BankBranchType.SWIFT
                'Treat NoBranchType as SWIFT
                ' added 19.05.2015, ellers ender vi alltid opp med False
                bReturnValue = True
                If Len(sCode) = 8 Or Len(sCode) = 11 Then
                    For lCounter = 1 To Len(sCode)
                        sChar = Mid(sCode, lCounter, 1)

                        ' First 6 must be uppercase alphabetic chars
                        If lCounter < 7 And InStr(1, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", sChar) < 1 Then
                            bReturnValue = False
                            Exit For
                        End If

                        'o	*       7. karakter skal v�re enten A-Z eller 2-9 (alts� ikke '1')
                        If lCounter = 7 And InStr(1, "ABCDEFGHIJKLMNOPQRSTUVWXYZ23456789", sChar) < 1 Then
                            bReturnValue = False
                            Exit For
                        End If

                        'o	*       8. karakter skal v�re enten A-N / P-Z eller 0-9 (alts� ikke 'O')
                        If lCounter = 8 And InStr(1, "ABCDEFGHIJKLMNPQRSTUVWXYZ0123456789", sChar) < 1 Then
                            bReturnValue = False
                            Exit For
                        End If

                        'o	*       9. - 11. karakter kan alle v�re enten A-Z eller 0-9. Disse karakterer er dog 'optional'.
                        If lCounter > 8 And InStr(1, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", sChar) < 1 Then
                            bReturnValue = False
                            Exit For
                        End If

                    Next lCounter
                    If bReturnValue = False Then
                        sErrorString = LRSCommon(20001, "SWIFT-", sCode) & " " & LRSCommon(20002) 'The SWIFT-code %2 is not valid. The length of the field should be either 8 or 11 characters.
                        'The SWIFT-code %2 is not valid. Only letters and numbers are valid characters.
                    End If
                Else
                    sErrorString = LRSCommon(20001, "SWIFT-", sCode) & " " & LRSCommon(20003) 'The SWIFT-code %2 is not valid. The length of the field should be either 8 or 11 characters.
                End If

            Case vbBabel.BabelFiles.BankBranchType.Fedwire
                If Len(sCode) = 9 Then
                    If IsNumeric(sCode) Then
                        bReturnValue = True
                    Else
                        sErrorString = LRSCommon(20001, "Federal Wire ", sCode) & " " & LRSCommon(20004)
                        'The Federal Wire code %2 is not valid. Only numbers are allowed in the code.
                    End If
                Else
                    sErrorString = LRSCommon(20001, "Federal Wire ", sCode) & " " & LRSCommon(20005, "9")
                    'The Federal Wire code %2 is not valid. The length of the field should be exactly 9 digits.
                End If

            Case vbBabel.BabelFiles.BankBranchType.SortCode
                If Len(sCode) = 6 Then
                    If IsNumeric(sCode) Then
                        bReturnValue = True
                    Else
                        sErrorString = LRSCommon(20001, "Sort ", sCode) & " " & LRSCommon(20004)
                        'The SortCode %2 is not valid. Only numbers are allowed in the code.
                    End If
                Else
                    sErrorString = LRSCommon(20001, "Sort ", sCode) & " " & LRSCommon(20005, "6")
                    'The Federal Wire code %2 is not valid. The length of the field should be exactly 6 digits.
                End If

            Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                If Len(sCode) = 8 Then
                    If IsNumeric(sCode) Then
                        bReturnValue = True
                    Else
                        sErrorString = LRSCommon(20001, "Banklietzahl ", sCode) & " " & LRSCommon(20004)
                        'The Bankleitzahl %2 is not valid. Only numbers are allowed in the code.
                    End If
                Else
                    sErrorString = LRSCommon(20001, "Bankleitzahl ", sCode) & " " & LRSCommon(20005, "8")
                    'The Bankleitzahl %2 is not valid. The length of the field should be exactly 8 digits.
                End If

            Case vbBabel.BabelFiles.BankBranchType.Chips
                If Not bIntermediaryBank Then
                    If Len(sCode) = 6 Then
                        If IsNumeric(sCode) Then
                            bReturnValue = True
                        Else
                            sErrorString = LRSCommon(20001, "CHIPS-", sCode) & " " & LRSCommon(20004)
                            'The CHIPS-code %2 is not valid. Only numbers are allowed in the code.
                        End If
                    Else
                        sErrorString = LRSCommon(20001, "CHIPS-", sCode) & " " & LRSCommon(20005, "6")
                        'The CHIPS-code %2 is not valid. The length of the field should be exactly 6 digits.
                    End If
                Else
                    If Len(sCode) = 4 Then
                        If IsNumeric(sCode) Then
                            bReturnValue = True
                        Else
                            sErrorString = LRSCommon(20001, "intermediary banks CHIPS-", sCode) & " " & LRSCommon(20004)
                            'The intermediary banks CHIPS-code %2 is not valid. Only numbers are allowed in the code.
                        End If
                    Else
                        sErrorString = LRSCommon(20001, "intermediary banks CHIPS-", sCode) & " " & LRSCommon(20005, "4")
                        'The intermediary banks CHIPS-code %2 is not valid. The length of the field should be exactly 4 digits.
                    End If
                End If

            Case vbBabel.BabelFiles.BankBranchType.MEPS
                If Len(sCode) = 7 Then
                    If IsNumeric(sCode) Then
                        bReturnValue = True
                    Else
                        sErrorString = LRSCommon(20001, "MEPS ", sCode) & " " & LRSCommon(20004)
                        'The SortCode %2 is not valid. Only numbers are allowed in the code.
                    End If
                Else
                    ' 30.03.2010 changed in errormsg from 6 to 7 digits
                    sErrorString = LRSCommon(20001, "MEPS ", sCode) & " " & LRSCommon(20005, "7") 'LRSCommon(20005, "6")
                    'The MEPS Wire code %2 is not valid. The length of the field should be exactly 6 digits.
                End If

                ' added 30.03.2010
            Case vbBabel.BabelFiles.BankBranchType.BSB
                If Len(sCode) = 6 Then
                    If IsNumeric(sCode) Then
                        bReturnValue = True
                    Else
                        sErrorString = LRSCommon(20001, "BSB ", sCode) & " " & LRSCommon(20004)
                        'The SortCode %2 is not valid. Only numbers are allowed in the code.
                    End If
                Else
                    sErrorString = LRSCommon(20001, "BSB ", sCode) & " " & LRSCommon(20005, "6")
                    'The BSB Wire code %2 is not valid. The length of the field should be exactly 6 digits.
                End If

                ' XNET 19.05.2011 added CC
            Case vbBabel.BabelFiles.BankBranchType.CC
                If Len(sCode) = 9 Then
                    If IsNumeric(sCode) Then
                        bReturnValue = True
                    Else
                        sErrorString = LRSCommon(20001, "CC - Canada Clearing ", sCode) & " " & LRSCommon(20004)
                        'The CC code %2 is not valid. Only numbers are allowed in the code.
                    End If
                Else
                    sErrorString = LRSCommon(20001, "CC - Canada Clearing ", sCode) & " " & LRSCommon(20005, "9")
                    'The CC code %2 is not valid. The length of the field should be exactly 9 digits.
                End If


            Case Else
                bReturnValue = False

        End Select

        ValidateBankID = bReturnValue

        Exit Function

ValidateBankIDError:
        bReturnValue = False

    End Function
    Public Function vb_Translate_BranchType(ByRef eBranchType As Short) As String
        ' Translate from Enums to Stringtype BranchType
        ' "SW"    eBANK_BranchTypeCorrBank = 1
        ' "FW"    eBANK_BranchTypeCorrBank = 2
        ' "SC"    eBANK_BranchTypeCorrBank = 3
        ' "BL"    eBANK_BranchTypeCorrBank = 4
        ' "CH"    eBANK_BranchTypeCorrBank = 5
        ' "ME"    eBANK_BranchTypeCorrBank = 8
        ' "ZA"    eBANK_BranchTypeCorrBank = 11  ' SouthAfrica Sortcode
        ' Else: eBANK_BranchTypeCorrBank = 0
        Select Case eBranchType
            Case 0
                vb_Translate_BranchType = ""
            Case 1
                vb_Translate_BranchType = "SWIFT"
            Case 2
                vb_Translate_BranchType = "FedWire"
            Case 3
                vb_Translate_BranchType = "SortCode"
            Case 4
                vb_Translate_BranchType = "Bankleitzahl"
            Case 5
                vb_Translate_BranchType = "CHIPS"
            Case 8
                vb_Translate_BranchType = "MEPS"
                '30.03.2010 added BSB
            Case 9
                vb_Translate_BranchType = "BSB" 'BSB  Australia
            Case 10
                vb_Translate_BranchType = "CC" 'CC Canada Clearing
            Case 11
                vb_Translate_BranchType = "ZA" 'SouthAfrica sortcode
            Case Else
                vb_Translate_BranchType = ""
        End Select

    End Function

    Public Function vb_Translate_FromBranchType(ByRef sBranchType As String) As Short
        ' Translate from Stringtype BranchType to eNums in BabelBank
        ' "SW"    eBANK_BranchTypeCorrBank = 1
        ' "FW"    eBANK_BranchTypeCorrBank = 2
        ' "SC"    eBANK_BranchTypeCorrBank = 3
        ' "BL"    eBANK_BranchTypeCorrBank = 4
        ' "CH"    eBANK_BranchTypeCorrBank = 5
        ' eBANK_BranchTypeCorrBank = 6
        ' eBANK_BranchTypeCorrBank = 7
        ' "ME"    eBANK_BranchTypeCorrBank = 8

        ' Else: eBANK_BranchTypeCorrBank = 0
        Select Case sBranchType
            Case ""
                vb_Translate_FromBranchType = 0
            Case "SWIFT"
                vb_Translate_FromBranchType = 1
            Case "FedWire"
                vb_Translate_FromBranchType = 2
            Case "SortCode"
                vb_Translate_FromBranchType = 3
            Case "Bankleitzahl"
                vb_Translate_FromBranchType = 4
            Case "CHIPS"
                vb_Translate_FromBranchType = 5
            Case "US_ABA"
                vb_Translate_FromBranchType = 6
            Case "Bankleitzahl_Austria"
                vb_Translate_FromBranchType = 7
            Case "MEPS"
                vb_Translate_FromBranchType = 8
                '30.03.2010 added BSB, Australia
            Case "BSB", "AU"
                vb_Translate_FromBranchType = 9
            Case "CC"
                vb_Translate_FromBranchType = 10
            Case "ZA"
                vb_Translate_FromBranchType = 11 'southafrica sortcode

            Case Else
                vb_Translate_FromBranchType = 0
        End Select

    End Function
    Function dbSelectAccountSWIFT(ByRef sAccountNo As String) As String
        ' return accounts swiftadress
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String
        Dim sReturnValue As String = ""
        Dim sCompanyID As String = ""

        Try

            sCompanyID = "1"

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If


            'sMySQL = "SELECT * FROM Account WHERE Company_ID = " & sCompanyID & " AND Account = '" & sAccountNo & "'"
            ' XNET 22.10.2013 - changed SELECT to also reflect ConvertedAccount
            ' Kjell - consider this !
            sMySQL = "SELECT * FROM Account WHERE Company_ID = " & sCompanyID & " AND (Account = '" & sAccountNo & "' OR ConvertedAccount = '" & sAccountNo & "')"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                ' Will return only one Account, or blank for new accounts
                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord

                        sReturnValue = oMyDal.Reader_GetString("SWIFTAddr")

                        Exit Do

                    Loop
                Else
                    sReturnValue = ""
                End If
            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: dbSelectAccountSWIFT" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnValue

    End Function
    Function dbSelectAccountCurrency(ByRef sAccountNo As String) As String
        ' return accounts currencycode
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String
        Dim sReturnValue As String = ""
        Dim sCompanyID As String = ""

        Try

            sCompanyID = "1"

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If


            'sMySQL = "SELECT CurrencyCode FROM Account WHERE Company_ID = " & sCompanyID & " AND Account = '" & sAccountNo & "'"
            ' XNET 22.10.2013 - changed SELECT to also reflect ConvertedAccount
            ' Kjell - consider this !
            sMySQL = "SELECT * FROM Account WHERE Company_ID = " & sCompanyID & " AND (Account = '" & sAccountNo & "' OR ConvertedAccount = '" & sAccountNo & "')"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                ' Will return only one Account, or blank for new accounts
                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord

                        sReturnValue = oMyDal.Reader_GetString("CurrencyCode")

                        Exit Do

                    Loop
                Else
                    sReturnValue = ""
                End If
            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: dbSelectAccountCurrency" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnValue

    End Function
    Function dbSelectAvtaleGiroID(ByRef sAccountNo As String) As String
        ' return accounts AvtaleGiroID

        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String
        Dim sReturnValue As String = ""
        Dim sCompanyID As String = ""

        Try

            sCompanyID = "1"

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT AvtaleGiroID FROM Account WHERE Company_ID = " & sCompanyID & " AND Account = '" & sAccountNo & "'"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                ' Will return only one Account, or blank for new accounts
                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord

                        sReturnValue = oMyDal.Reader_GetString("AvtaleGiroID")

                        Exit Do

                    Loop
                Else
                    sReturnValue = ""
                End If
            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: dbSelectAvtaleGiroID" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnValue

    End Function
    Public Function db_NHST_KontoTilVisma(ByVal sTittel As String, ByVal sValuta As String, ByVal sRegion As String, ByVal sClientNo As String) As String
        ' Special for NHST - find values for special NHST-table in BB-database
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String
        Dim sReturnValue As String = ""
        Dim sCompanyID As String = ""

        Try

            sCompanyID = "1"

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ' Intrafish has "All" for Region and Valuta, search for Tittel only;
            If sClientNo = "F14015" Then
                sMySQL = "SELECT * FROM NHST_KontiTilVisma WHERE Company_ID = " & sCompanyID & " AND Tittel = '" & sTittel & "'"
                sMySQL = sMySQL & " AND ClientNo = '" & sClientNo & "'"
            Else
                sMySQL = "SELECT * FROM NHST_KontiTilVisma WHERE Company_ID = " & sCompanyID & " AND Tittel = '" & sTittel & "'"
                sMySQL = sMySQL & " AND Valuta = '" & sValuta & "' AND Region = '" & sRegion & "' AND ClientNo = '" & sClientNo & "'"
            End If

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord

                        sReturnValue = oMyDal.Reader_GetString("Konto")

                        Exit Do

                    Loop
                Else
                    sReturnValue = ""
                End If
            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: db_NHST_KontoTilVisma" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnValue

    End Function
    Public Function dbNoOfConnections() As Integer
        ' added 14.01.2015 - returns how many connections are setup against ERP-system
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String
        Dim iReturnValue As Integer = 0
        Dim sCompanyID As String = ""

        Try

            sCompanyID = "1"

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT Count(*) AS NoOfConnections FROM DB_Profile"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord

                        iReturnValue = oMyDal.Reader_GetString("NoOfConnections")

                        Exit Do

                    Loop
                End If
            Else

                Throw New Exception(LRSCommon(45002) & "dbNoOfConnections" & vbCrLf & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: dbNoOfConnections" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return iReturnValue
    End Function
    Public Function RestoreManualSetup(ByRef bJumpToNext As Boolean, ByRef sMatchedColumns As String, ByRef sColumnsToEnterData As String, ByRef bOmitPayments As Boolean, ByRef bDeductMatched As Boolean, ByRef bAllowDifferenceInCurrency As Boolean, Optional ByRef sERPPaymentID As String = "", Optional ByRef bManMatchFromERP As Boolean = False, Optional ByRef bExportOnlyMacthedPayments As Boolean = True, Optional ByRef bActivateStatistics As Boolean = False, Optional ByRef sPWStatistics As String = "", Optional ByRef seMailSubject As String = "", Optional ByRef seMailAddress As String = "", Optional ByRef beMailAttachment As Boolean = False, Optional ByRef bCalledFromSetup As Boolean = False, Optional ByRef bValidateMatching As Boolean = True, Optional ByRef bReportDeviations As Boolean = True, _
                                       Optional ByRef bUnDoDoubleMatched As Boolean = False, Optional ByRef bUnDoAllDoubleMatched As Boolean = False, Optional ByRef sHistoryDays As String = "") As Object
        'Added 27.03.2008 - Optional bExportOnlyMacthedPayments As Boolean = True
        ' 30.12.2009: added , Optional bActivateStatistics As Boolean = False

        ' Connect to database
        ' Find values for setup of manual payments
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String
        Dim sReturnValue As String = ""
        Dim sCompanyID As String = ""
        'Dim sTmp As String = ""

        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "Select * FROM MATCH_Manual"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                ' Will return only one Account, or blank for new accounts
                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord

                        bJumpToNext = CBool(oMyDal.Reader_GetString("JumpToNext"))
                        sMatchedColumns = oMyDal.Reader_GetString("MatchedColumns")
                        sColumnsToEnterData = oMyDal.Reader_GetString("ColumnsToEnterData")

                        ' For frmMATCH_ManualeMail
                        seMailSubject = oMyDal.Reader_GetString("eMailSubject")
                        If seMailSubject = "" Then
                            seMailSubject = LRS(61004) ' Melding om �pen post i BabelBank
                        End If
                        seMailAddress = oMyDal.Reader_GetString("eMailAddress")
                        beMailAttachment = CBool(oMyDal.Reader_GetString("eMailAttachment"))
                        bOmitPayments = CBool(oMyDal.Reader_GetString("ActivateOmit"))
                        bDeductMatched = CBool(oMyDal.Reader_GetString("DeductMatched"))
                        bAllowDifferenceInCurrency = CBool(oMyDal.Reader_GetString("AllowDifferenceInCurrency"))
                        ' added 30.12.2009
                        bActivateStatistics = CBool(oMyDal.Reader_GetString("ActivateStatistics"))

                        'XNET - Added next 2
                        bValidateMatching = CBool(oMyDal.Reader_GetString("ValidateMatching"))
                        bReportDeviations = CBool(oMyDal.Reader_GetString("ReportDeviations"))

                        ' New 10.03.2004
                        ' Transform from old to new string (Supplier included)
                        If Len(sMatchedColumns) = 8 Then
                            sMatchedColumns = Left(sMatchedColumns, 6) & "0" & Mid(sMatchedColumns, 7)
                        End If

                        ' New 07.10.2004
                        ' Transform from old to new string (ExchangeRate included)
                        If Len(sMatchedColumns) = 9 Then
                            sMatchedColumns = Left(sMatchedColumns, 6) & "0" & Mid(sMatchedColumns, 7)
                        End If

                        ' New 07.10.2004
                        ' Transform from old to new string (ExchangeRate included)
                        If Len(sMatchedColumns) = 10 Then
                            sMatchedColumns = sMatchedColumns & "00"
                        End If

                        ' New 25.01.2006
                        ' Transform from old to new string (MatchID, AkontoID and MyField included)
                        If Len(sMatchedColumns) = 12 Then
                            sMatchedColumns = sMatchedColumns & "000"
                            sColumnsToEnterData = sColumnsToEnterData & "000"
                        End If

                        ' New 12.07.2006
                        ' Transform from old to new string (ExchangeRate included)
                        If Len(sMatchedColumns) = 15 Then
                            sMatchedColumns = "0" & sMatchedColumns
                            sColumnsToEnterData = "0" & sColumnsToEnterData
                        End If

                        ' New 07.10.2004
                        ' Transform from old to new string (ExchangeRate included)
                        If Len(sMatchedColumns) = 16 Then
                            sMatchedColumns = Left(sMatchedColumns, 10) & "0" & Mid(sMatchedColumns, 11)
                        End If

                        ' New 18.10.2007
                        ' Transform from old to new string (DiscountAmount included)
                        If Len(sMatchedColumns) = 17 Then
                            sMatchedColumns = Left(sMatchedColumns, 11) & "0" & Mid(sMatchedColumns, 12)
                            sColumnsToEnterData = Left(sColumnsToEnterData, 11) & "0" & Mid(sColumnsToEnterData, 12)
                        End If

                        ' added 05.01.2010
                        sPWStatistics = oMyDal.Reader_GetString("PWStatistics")

                        ' New 11.12.2014
                        ' Added MON_CurrencyAmount, MON_Currency, Dim1 - Dim10
                        If Len(sMatchedColumns) = 18 Then
                            sMatchedColumns = sMatchedColumns & "000000000000000"
                            sColumnsToEnterData = sColumnsToEnterData & "000000000000000"
                        End If

                        'New 26.03.2015
                        'Added MyField2 and 3
                        If Len(sMatchedColumns) = 33 Then
                            sMatchedColumns = sMatchedColumns & "00"
                            sColumnsToEnterData = sColumnsToEnterData & "00"
                        End If

                        'New 06.07.2015
                        'Added BackPaymentAccount
                        If Len(sMatchedColumns) = 35 Then
                            sMatchedColumns = sMatchedColumns & "0"
                            sColumnsToEnterData = sColumnsToEnterData & "0"
                        End If

                        ' 27.01.2017 added HistoryDays
                        sHistoryDays = oMyDal.Reader_GetString("HistoryDays")
                        Exit Do

                    Loop
                Else
                    bJumpToNext = True
                    ' New 18.10.2007
                    sMatchedColumns = "0111111111001000000000000000000000"
                    sColumnsToEnterData = "0111111111001000000000000000000000"

                    ' For frmMATCH_ManualeMail
                    seMailSubject = "BabelBank"
                    seMailAddress = "@"
                    beMailAttachment = False
                    bOmitPayments = False
                    bDeductMatched = True
                    bAllowDifferenceInCurrency = False
                End If
            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage & vbCrLf & vbCrLf & "SQL:" & sMySQL)

            End If

            sMySQL = "Select ERPPaymentID, ManMatchFromERP, ExportOnlyMatchedPayments, UnDoDoubleMatched, UnDoAllDoubleMatched FROM Company"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then

                    Do While oMyDal.Reader_ReadRecord

                        sERPPaymentID = oMyDal.Reader_GetString("ERPPaymentID")
                        bManMatchFromERP = CBool(oMyDal.Reader_GetString("ManMatchFromERP"))
                        'not in use !! sTmp = oMyDal.Reader_GetString("ExportOnlyMatchedPayments")
                        bExportOnlyMacthedPayments = CBool(oMyDal.Reader_GetString("ExportOnlyMatchedPayments"))
                        bUnDoDoubleMatched = CBool(oMyDal.Reader_GetString("UnDoDoubleMatched"))
                        bUnDoAllDoubleMatched = CBool(oMyDal.Reader_GetString("UnDoAllDoubleMatched"))
                    Loop

                Else

                End If

            Else

                Throw New Exception("Feilmelding" & vbCrLf & LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: RestoreManualSetup" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

    End Function
    Public Function GetBBExportBackupInfo(ByRef bCreateBackup As Boolean, ByRef bBckBeforeImport As Boolean, ByRef bBckBeforeExport As Boolean, ByRef bBckAfterExport As Boolean, ByRef sExportBackuppath As String, ByRef iExportBackupGenerations As Short) As Object

        ' Connect to database
        ' Find values for setup of manual payments
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String = ""

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ' Fill the reader with all information about backup
            sMySQL = "Select CreateBackup, BckBeforeImport, BckBeforeExport, BckAfterExport, ExportBackuppath, ExportBackupGenerations FROM Company WHERE Company_ID = 1"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    bCreateBackup = CBool(oMyDal.Reader_GetString("CreateBackup"))
                    bBckBeforeImport = CBool(oMyDal.Reader_GetString("BckBeforeImport"))
                    bBckBeforeExport = CBool(oMyDal.Reader_GetString("BckBeforeExport"))
                    bBckAfterExport = CBool(oMyDal.Reader_GetString("BckAfterExport"))
                    sExportBackuppath = oMyDal.Reader_GetString("ExportBackuppath")
                    iExportBackupGenerations = Val(oMyDal.Reader_GetString("ExportBackupGenerations"))

                    Exit Do

                Loop

            Else

                Throw New Exception("Feilmelding" & vbCrLf & LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetBBExportBackupInfo" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

    End Function
    Public Function GetERPDBInfo(ByRef lCompanyNo As Integer, ByRef iMatchType As Short, Optional ByVal iDBProfile_ID As Integer = -1) As String(,,)

        'The aim is to make an 3 dimensional array like this
        'First dimension will be the DBProfile_ID

        'Second dimension is the data - either for the connection (third dimension 0
        '   or for the query (third dimension > 0). There will always be 5 elements
        'The data stored will be like this
        'Element 0 - Connectionstring/Query
        'Element 1 - UID/Type of query
        'Element 2 - PWD/Name for specialmatch
        'Element 3 - DBProfile_ID/DescriptionID
        'Element 4 - Name of DBProfile / Name of query
        'Element 5 - RetrieveRSWithOpen /ERPQuery_ID in the BB database
        'Element 6 - CursorType /Allowed roundings
        'Element 7 - LockType /IncludeOCR
        'Element 8 - CursorLocation /KIDStartCustomerNo
        'Element 9 - ProviderType /KIDLengthCustomerNo
        'Element10 - DatabaseType/UseAdjustments '19.10.2017 - Added databasetype several places in this function
        'Element11 -   /Propose match
        'Element12 -   /Include OCR
        'Element13 -   /Use AdjustmentsFinal (added 06.09.2016)
        'Element14 -  CustomerNoSearch (added 07.02.2018)

        'Third dimension will represent each query (matching rule).
        'NB! Except element 0 which will be the connection information

        'iMatchType signifies the type of matchingrule you want to retrieve
        ' MatchType = 1 - Automatic matching
        ' MatchType = 2 - Manual matching
        ' MatchType = 3 - After matching
        ' MatchType = 4 - Validation in manual matching
        ' MatchType = 5 - Other SQLs

        'Element 1 - Type of Query
        'The following is valid (as far as Kjell remeber)
        'Type 1 = Matching with identifier which uniquely identifies an invoice
        'Type 2 = Matching which identifies one or more customer (f.x. payers acc. no.)
        'Type 3 = Special match, the field 'Special' must be filled
        'Type 4 =
        'Type 5 = A query to be run for each payment in the aftermatching, updating the ERP
        '         f.x. update payers account no. in the ERP.
        'Type 6 = A query to run in the aftermatching, f.x. add voucher no.
        'Type 7 = A query to be run for each payment in the aftermatching, updating the collections
        '         f.x. add an exchangerate on the payment from the ERP
        'Type 8 = A query to run after finnishing treating a client in the aftermatching
        '         f.x. update the last used voucher no. in the ERP-system
        'Type 101 = As 1, but no restrictions on client
        'Type 103 = As 3, but no restrictions on client

        '        Dim myBBConnection As System.Data.Common.DbConnection
        '        Dim myDBProfileCommand As System.Data.Common.DbCommand
        '        Dim myERPQueryCommand As System.Data.Common.DbCommand
        '       Dim myDBProfileReader As System.Data.Common.DbDataReader
        '        Dim myERPQueryReader As System.Data.Common.DbDataReader

        Dim oMyDal As vbBabel.DAL = Nothing
        Dim oMyDal2 As vbBabel.DAL = Nothing
        Dim oMyDal3 As vbBabel.DAL = Nothing

        Dim sMySQL As String
        Dim sMySQL2 As String
        Dim sMySQL3 As String
        Dim aResultArray(,,) As String
        Dim aTempResultArray(,,) As String = Nothing
        Dim iXArrayCounter As Integer
        Dim iQueryCounter As Short
        Dim iThirdDimension As Short
        Dim sQuery As String
        Dim iElement1Counter As Integer
        Dim iElement2Counter As Integer
        Dim iElement3Counter As Integer

        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "Select Constring1, Constring2, UID, PWD, DBProfile_ID, Name, RetrieveRSWithOpen, CursorType, LockType, CursorLocation, ProviderType, DatabaseType FROM DB_Profile WHERE Company_ID = " & Trim(Str(lCompanyNo))
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                iXArrayCounter = -1

                If oMyDal.Reader_HasRows Then

                    Do While oMyDal.Reader_ReadRecord

                        iXArrayCounter = iXArrayCounter + 1

                        If iXArrayCounter = 0 Then
                            ReDim Preserve aResultArray(iXArrayCounter, 14, 0)
                        Else
                            'Have to copy all data to a temparray.
                            'It is not possible to Redim the first parameter in an array
                            If Not aTempResultArray Is Nothing Then
                                aTempResultArray = Nothing
                                ReDim aTempResultArray(aResultArray.GetUpperBound(0), aResultArray.GetUpperBound(1), aResultArray.GetUpperBound(2))
                            End If
                            aTempResultArray = aResultArray
                            aResultArray = Nothing
                            ReDim aResultArray(aTempResultArray.GetUpperBound(0) + 1, aTempResultArray.GetUpperBound(1), aTempResultArray.GetUpperBound(2))

                            For iElement1Counter = 0 To aTempResultArray.GetUpperBound(0)
                                For iElement2Counter = 0 To aTempResultArray.GetUpperBound(1)
                                    For iElement3Counter = 0 To aTempResultArray.GetUpperBound(2)
                                        aResultArray(iElement1Counter, iElement2Counter, iElement3Counter) = aTempResultArray(iElement1Counter, iElement2Counter, iElement3Counter)
                                    Next iElement3Counter
                                Next iElement2Counter
                            Next iElement1Counter
                        End If

                        iThirdDimension = 0

                        'Element 0 - ConnectionString
                        aResultArray(iXArrayCounter, 0, 0) = oMyDal.Reader_GetString("Constring1") & oMyDal.Reader_GetString("Constring2")

                        'Element 1 - UID
                        aResultArray(iXArrayCounter, 1, 0) = oMyDal.Reader_GetString("UID")
                        'Element 2 - PWD
                        aResultArray(iXArrayCounter, 2, 0) = BBDecrypt(oMyDal.Reader_GetString("PWD"), 3)
                        'Element 3 - DBProfile_ID
                        aResultArray(iXArrayCounter, 3, 0) = oMyDal.Reader_GetString("DBProfile_ID")
                        'Element 4 - Name
                        aResultArray(iXArrayCounter, 4, 0) = oMyDal.Reader_GetString("Name")
                        'Element 5 - RetrieveRSWithOpen
                        aResultArray(iXArrayCounter, 5, 0) = oMyDal.Reader_GetString("RetrieveRSWithOpen")

                        'Element 6 - CursorType
                        aResultArray(iXArrayCounter, 6, 0) = oMyDal.Reader_GetString("CursorType")
                        'Element 7 - LockType
                        aResultArray(iXArrayCounter, 7, 0) = oMyDal.Reader_GetString("LockType")

                        'Element 8 - CursorLocation
                        aResultArray(iXArrayCounter, 8, 0) = oMyDal.Reader_GetString("CursorLocation")

                        'Element 9 - ProviderType
                        aResultArray(iXArrayCounter, 9, 0) = oMyDal.Reader_GetString("ProviderType")
                        'Element 9 - DatabaseType
                        aResultArray(iXArrayCounter, 10, 0) = oMyDal.Reader_GetString("DatabaseType")

                        oMyDal2 = New vbBabel.DAL
                        oMyDal2.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                        If Not oMyDal2.ConnectToDB() Then
                            Err.Raise(Err.Number, "GetERPDBInfo", oMyDal.ErrorMessage)
                        End If

                        iQueryCounter = 0

                        If 1 = 2 Then 'RunTime() Then 'MANDARIN
                            'XNET - 08.05.2012 - Added Query13
                            sMySQL2 = "SELECT Query1, Query2, Query3, Query4, Query5, Query6, Query7, Query8, Query9, "
                            sMySQL2 = sMySQL2 & "Query10, Query11, Query12, Query13, Type, SpecialMatch, DescGroup, Name, ERPQuery_ID, "
                            sMySQL2 = sMySQL2 & "Roundings, IncludeOCR, KIDStartCustomerNo, KIDLengthCustomerNo, UseAdjustment, ProposeMatch, UseAdjustmentFinal, CustomerNoSearch "
                            sMySQL2 = sMySQL2 & "FROM ERPQuery WHERE Company_ID = " & Trim(Str(lCompanyNo)) & " AND DBProfile_ID = " & oMyDal.Reader_GetString("DBProfile_ID")
                            sMySQL2 = sMySQL2 & " AND MatchType = " & Trim(Str(iMatchType))
                            sMySQL2 = sMySQL2 & " AND Qorder > 0 ORDER BY Qorder"

                            oMyDal2.SQL = sMySQL2
                            If oMyDal2.Reader_Execute() Then

                                If oMyDal2.Reader_HasRows Then

                                    Do While oMyDal2.Reader_ReadRecord

                                        iQueryCounter = iQueryCounter + 1
                                        If iXArrayCounter > 0 Then
                                            If iQueryCounter > aResultArray.GetUpperBound(2) Then
                                                Err.Raise(20052, "GetERPDBInfo", LRSCommon(20052))
                                            Else
                                                'OK, the array is fit for adding this element
                                            End If
                                        Else
                                            ReDim Preserve aResultArray(iXArrayCounter, 14, iQueryCounter)
                                        End If

                                        sQuery = ""
                                        'Query1 - Query14
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query1", False)
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query2", False)
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query3", False)
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query4", False)
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query5", False)
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query6", False)
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query7", False)
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query8", False)
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query9", False)
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query10", False)
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query11", False)
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query12", False)
                                        sQuery = sQuery & oMyDal2.Reader_GetString("Query13", False)
                                        sQuery = BBDecrypt(Replace(sQuery, "@?@", "'"), 3)

                                        'Element 0 - Query 
                                        aResultArray(iXArrayCounter, 0, iQueryCounter) = sQuery

                                        'Element 1 - Type 
                                        aResultArray(iXArrayCounter, 1, iQueryCounter) = oMyDal2.Reader_GetString("Type")

                                        'Element 2 - SpecialMatch
                                        aResultArray(iXArrayCounter, 2, iQueryCounter) = oMyDal2.Reader_GetString("SpecialMatch")

                                        'Element 3 - DescGroup 
                                        aResultArray(iXArrayCounter, 3, iQueryCounter) = oMyDal2.Reader_GetString("DescGroup")

                                        'Element 4 - Name of query
                                        aResultArray(iXArrayCounter, 4, iQueryCounter) = oMyDal2.Reader_GetString("Name")

                                        'Element 5 - ERPQuery_ID in the BB database
                                        aResultArray(iXArrayCounter, 5, iQueryCounter) = oMyDal2.Reader_GetString("ERPQuery_ID")

                                        'Element 6 - Roundings
                                        aResultArray(iXArrayCounter, 6, iQueryCounter) = oMyDal2.Reader_GetString("Roundings") 'myERPQueryReader.GetDouble(17).ToString

                                        'Element 7 and 12 - IncludeOCR - For some reason IncludeOCR is stored in two different places
                                        aResultArray(iXArrayCounter, 7, iQueryCounter) = oMyDal2.Reader_GetString("IncludeOCR")
                                        aResultArray(iXArrayCounter, 12, iQueryCounter) = oMyDal2.Reader_GetString("IncludeOCR")

                                        'Element 8 - KIDStartCustomerNo
                                        aResultArray(iXArrayCounter, 8, iQueryCounter) = oMyDal2.Reader_GetString("KIDStartCustomerNo")

                                        'Element 9 - KIDLengthCustomerNo
                                        aResultArray(iXArrayCounter, 9, iQueryCounter) = oMyDal2.Reader_GetString("KIDLengthCustomerNo")

                                        'Element 10 - Use adjustments
                                        aResultArray(iXArrayCounter, 10, iQueryCounter) = oMyDal2.Reader_GetString("UseAdjustment")

                                        'Element 11 - ProposeMatch
                                        aResultArray(iXArrayCounter, 11, iQueryCounter) = oMyDal2.Reader_GetString("ProposeMatch")

                                        'Element 12 - IncludeOCR
                                        'Added above together with element 7

                                        'Element 13 - UseAdjustmentFinal
                                        aResultArray(iXArrayCounter, 13, iQueryCounter) = oMyDal2.Reader_GetString("UseAdjustmentFinal")

                                        'Element 14 - CustomerNoSearch - added 07.02.2018
                                        aResultArray(iXArrayCounter, 14, iQueryCounter) = oMyDal2.Reader_GetString("CustomerNoSearch")

                                    Loop
                                End If 'If oMyDal.Reader_HasRows Then
                            End If 'If oMyDal.Reader_Execute() Then

                            If Not oMyDal2 Is Nothing Then
                                oMyDal2.Close()
                                oMyDal2 = Nothing
                            End If

                        Else

                            '28.10.2014 - New structure of saving queries in BabelBank
                            sMySQL2 = "SELECT Type, SpecialMatch, DescGroup, Name, ERPQuery_ID, "
                            sMySQL2 = sMySQL2 & "Roundings, IncludeOCR, KIDStartCustomerNo, KIDLengthCustomerNo, UseAdjustment, ProposeMatch, UseAdjustmentFinal, CustomerNoSearch "
                            sMySQL2 = sMySQL2 & "FROM ERPQuery WHERE Company_ID = " & Trim(Str(lCompanyNo)) & " AND DBProfile_ID = " & oMyDal.Reader_GetString("DBProfile_ID")
                            sMySQL2 = sMySQL2 & " AND MatchType = " & Trim(Str(iMatchType))
                            sMySQL2 = sMySQL2 & " AND Qorder > 0 ORDER BY Qorder"

                            oMyDal2.SQL = sMySQL2
                            If oMyDal2.Reader_Execute() Then

                                If oMyDal2.Reader_HasRows Then

                                    oMyDal3 = New vbBabel.DAL
                                    oMyDal3.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                                    If Not oMyDal3.ConnectToDB() Then
                                        Err.Raise(Err.Number, "GetERPDBInfo", oMyDal.ErrorMessage)
                                    End If

                                    Do While oMyDal2.Reader_ReadRecord

                                        iQueryCounter = iQueryCounter + 1
                                        If iXArrayCounter > 0 Then
                                            If iQueryCounter > aResultArray.GetUpperBound(2) Then
                                                Err.Raise(20052, "GetERPDBInfo", LRSCommon(20052))
                                            Else
                                                'OK, the array is fit for adding this element
                                            End If
                                        Else
                                            ReDim Preserve aResultArray(iXArrayCounter, 14, iQueryCounter)
                                        End If

                                        sMySQL3 = "SELECT QueryLine "
                                        sMySQL3 = sMySQL3 & "FROM ERPQueryLine WHERE Company_ID = " & lCompanyNo.ToString & " AND DBProfile_ID = " & oMyDal.Reader_GetString("DBProfile_ID")
                                        sMySQL3 = sMySQL3 & " AND ERPQuery_ID = " & oMyDal2.Reader_GetString("ERPQuery_ID")
                                        sMySQL3 = sMySQL3 & " ORDER BY ERPQueryLine_ID"

                                        oMyDal3.SQL = sMySQL3
                                        If oMyDal3.Reader_Execute() Then

                                            If oMyDal3.Reader_HasRows Then
                                                sQuery = String.Empty
                                                Do While oMyDal3.Reader_ReadRecord
                                                    sQuery = sQuery & oMyDal3.Reader_GetString("QueryLine", False)
                                                Loop
                                                sQuery = BBDecrypt(Replace(sQuery, "@?@", "'"), 3)
                                            End If
                                        End If

                                        'If Not oMyDal3 Is Nothing Then
                                        '    oMyDal3.Close()
                                        '    oMyDal3 = Nothing
                                        'End If

                                        'Element 0 - Query 
                                        aResultArray(iXArrayCounter, 0, iQueryCounter) = sQuery

                                        'Element 1 - Type 
                                        aResultArray(iXArrayCounter, 1, iQueryCounter) = oMyDal2.Reader_GetString("Type")

                                        'Element 2 - SpecialMatch
                                        aResultArray(iXArrayCounter, 2, iQueryCounter) = oMyDal2.Reader_GetString("SpecialMatch")

                                        'Element 3 - DescGroup 
                                        aResultArray(iXArrayCounter, 3, iQueryCounter) = oMyDal2.Reader_GetString("DescGroup")

                                        'Element 4 - Name of query
                                        aResultArray(iXArrayCounter, 4, iQueryCounter) = oMyDal2.Reader_GetString("Name")

                                        'Element 5 - ERPQuery_ID in the BB database
                                        aResultArray(iXArrayCounter, 5, iQueryCounter) = oMyDal2.Reader_GetString("ERPQuery_ID")

                                        'Element 6 - Roundings
                                        aResultArray(iXArrayCounter, 6, iQueryCounter) = oMyDal2.Reader_GetString("Roundings") 'myERPQueryReader.GetDouble(17).ToString

                                        'Element 7 and 12 - IncludeOCR - For some reason IncludeOCR is stored in two different places
                                        aResultArray(iXArrayCounter, 7, iQueryCounter) = oMyDal2.Reader_GetString("IncludeOCR")
                                        aResultArray(iXArrayCounter, 12, iQueryCounter) = oMyDal2.Reader_GetString("IncludeOCR")

                                        'Element 8 - KIDStartCustomerNo
                                        aResultArray(iXArrayCounter, 8, iQueryCounter) = oMyDal2.Reader_GetString("KIDStartCustomerNo")

                                        'Element 9 - KIDLengthCustomerNo
                                        aResultArray(iXArrayCounter, 9, iQueryCounter) = oMyDal2.Reader_GetString("KIDLengthCustomerNo")

                                        'Element 10 - Use adjustments
                                        aResultArray(iXArrayCounter, 10, iQueryCounter) = oMyDal2.Reader_GetString("UseAdjustment")

                                        'Element 11 - ProposeMatch
                                        aResultArray(iXArrayCounter, 11, iQueryCounter) = oMyDal2.Reader_GetString("ProposeMatch")

                                        'Element 12 - IncludeOCR
                                        'Added above together with element 7

                                        'Element 13 - UseadjustmentsFinal
                                        '08.09.2016 - Added next
                                        aResultArray(iXArrayCounter, 13, iQueryCounter) = oMyDal2.Reader_GetString("UseAdjustmentFinal")

                                        'Element 14 - CustomerNoSearch - added 07.02.2018
                                        aResultArray(iXArrayCounter, 14, iQueryCounter) = oMyDal2.Reader_GetString("CustomerNoSearch")

                                    Loop
                                End If 'If oMyDal.Reader_HasRows Then
                            End If 'If oMyDal.Reader_Execute() Then

                            If Not oMyDal2 Is Nothing Then
                                oMyDal2.Close()
                                oMyDal2 = Nothing
                            End If
                        End If

                    Loop

                Else
                    '        'No entries in DB_Profile

                End If 'If oMyDal.Reader_HasRows Then

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If 'If oMyDal.Reader_Execute() Then

            '    myDBProfileReader.Close()
            '    myDBProfileReader = Nothing
            '    myDBProfileCommand.Dispose()

            '    myBBConnection.Close()
            '    myBBConnection.Dispose()

        Catch ex As Exception


            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If
            If Not oMyDal2 Is Nothing Then
                oMyDal2.Close()
                oMyDal2 = Nothing
            End If
            If Not oMyDal3 Is Nothing Then
                oMyDal3.Close()
                oMyDal3 = Nothing
            End If

            Throw New Exception("Function: <GetERPDBInfo>" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If
        If Not oMyDal2 Is Nothing Then
            oMyDal2.Close()
            oMyDal2 = Nothing
        End If
        If Not oMyDal3 Is Nothing Then
            oMyDal3.Close()
            oMyDal3 = Nothing
        End If

        Return aResultArray

    End Function
    Public Function GetSQLFromName(ByVal lCompanyNo As Long, ByRef sSQLName As String, ByRef iDBProfile_ID As Integer, ByRef iMatchType As Integer) As String

        'This function returns a SQL, based on the name of the SQL and the MatchType

        Dim oMyDal As vbBabel.DAL
        Dim sErrorString As String = ""
        Dim sMySQL As String
        Dim sQuery As String = ""


        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            If 1 = 2 Then 'RunTime() Then 'MANDARIN - 
                sMySQL = "SELECT * FROM ERPQuery WHERE Company_ID = " & Trim$(Str(lCompanyNo)) & " AND DBProfile_ID = " & Trim$(Str(iDBProfile_ID))
                sMySQL = sMySQL & " AND MatchType = " & Trim$(Str(iMatchType))
                sMySQL = sMySQL & " AND Name = '" & sSQLName & " '"

                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then

                    If oMyDal.Reader_HasRows Then
                        sQuery = ""
                        Do While oMyDal.Reader_ReadRecord
                            sQuery = oMyDal.Reader_GetString("Query1")
                            If Not EmptyString(oMyDal.Reader_GetString("Query2")) Then
                                sQuery = sQuery & oMyDal.Reader_GetString("Query2")
                            Else
                                Exit Do
                            End If
                            If Not EmptyString(oMyDal.Reader_GetString("Query3")) Then
                                sQuery = sQuery & oMyDal.Reader_GetString("Query3")
                            Else
                                Exit Do
                            End If
                            If Not EmptyString(oMyDal.Reader_GetString("Query4")) Then
                                sQuery = sQuery & oMyDal.Reader_GetString("Query4")
                            Else
                                Exit Do
                            End If
                            If Not EmptyString(oMyDal.Reader_GetString("Query5")) Then
                                sQuery = sQuery & oMyDal.Reader_GetString("Query5")
                            Else
                                Exit Do
                            End If
                            If Not EmptyString(oMyDal.Reader_GetString("Query6")) Then
                                sQuery = sQuery & oMyDal.Reader_GetString("Query6")
                            Else
                                Exit Do
                            End If
                            If Not EmptyString(oMyDal.Reader_GetString("Query7")) Then
                                sQuery = sQuery & oMyDal.Reader_GetString("Query7")
                            Else
                                Exit Do
                            End If
                            If Not EmptyString(oMyDal.Reader_GetString("Query8")) Then
                                sQuery = sQuery & oMyDal.Reader_GetString("Query8")
                            Else
                                Exit Do
                            End If
                            If Not EmptyString(oMyDal.Reader_GetString("Query9")) Then
                                sQuery = sQuery & oMyDal.Reader_GetString("Query9")
                            Else
                                Exit Do
                            End If
                            If Not EmptyString(oMyDal.Reader_GetString("Query10")) Then
                                sQuery = sQuery & oMyDal.Reader_GetString("Query10")
                            Else
                                Exit Do
                            End If
                            If Not EmptyString(oMyDal.Reader_GetString("Query11")) Then
                                sQuery = sQuery & oMyDal.Reader_GetString("Query11")
                            Else
                                Exit Do
                            End If
                            If Not EmptyString(oMyDal.Reader_GetString("Query12")) Then
                                sQuery = sQuery & oMyDal.Reader_GetString("Query12")
                            Else
                                Exit Do
                            End If
                            If Not EmptyString(oMyDal.Reader_GetString("Query13")) Then
                                sQuery = sQuery & oMyDal.Reader_GetString("Query13")
                            Else
                                Exit Do
                            End If

                        Loop

                    End If
                End If
            Else
                '28.10.2014 - New structure of saving queries in BabelBank
                sMySQL = "SELECT QueryLine FROM ERPQuery E, ERPQueryLine EL  WHERE E.Company_ID = " & lCompanyNo.ToString & " AND E.DBProfile_ID = " & iDBProfile_ID.ToString
                sMySQL = sMySQL & " AND E.MatchType = " & Trim$(Str(iMatchType))
                sMySQL = sMySQL & " AND E.Name = '" & sSQLName & " '"
                sMySQL = sMySQL & " AND E.Company_ID = EL.Company_ID AND E.DBProfile_ID = EL.DBProfile_ID AND E.ERPQuery_ID = EL.ERPQuery_ID"

                sQuery = ""
                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then

                    If oMyDal.Reader_HasRows Then

                        Do While oMyDal.Reader_ReadRecord

                            sQuery = sQuery & oMyDal.Reader_GetString("QueryLine")
                        Loop
                    End If 'If oMyDal.Reader_HasRows Then
                Else
                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                End If 'If oMyDal.Reader_Execute() Then

            End If

            If Not EmptyString(sQuery) Then
                sQuery = BBDecrypt(Replace(sQuery, "@?@", "'"), 3)
            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: <GetSQLFromName>" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sQuery

    End Function
    Public Function PrepareSQL(ByRef sMySQL As String, ByRef oPayment As vbBabel.Payment, ByRef sClientNo As String, Optional ByRef sAmount As String = "", Optional ByRef sCustomerNo As String = "", Optional ByRef sClientName As String = "") As String
        Dim sReturnString As String = ""
        Dim sTmp, sTmp2 As String
        Dim iPos, iPos2 As Integer

        'BB_Amount
        sReturnString = Replace(sMySQL, "BB_Amount", Trim(sAmount), , , CompareMethod.Text)
        ' added 29.04.2016
        sReturnString = Replace(sReturnString, "BB_OriginalAmount", Trim(sAmount), , , CompareMethod.Text)
        'BB_ClientNo
        sReturnString = Replace(sReturnString, "BB_ClientNo", sClientNo, , , CompareMethod.Text)
        'BB_Currency
        sReturnString = Replace(sReturnString, "BB_Currency", Trim(oPayment.MON_InvoiceCurrency), , , CompareMethod.Text)
        'BB_InvoiceIdentifier
        sReturnString = Replace(sReturnString, "BB_InvoiceIdentifier", Trim(oPayment.MATCH_InvoiceNumber), , , CompareMethod.Text)
        'BB_CustomerIdentifier
        sReturnString = Replace(sReturnString, "BB_CustomerIdentifier", Trim(oPayment.MATCH_InvoiceNumber), , , CompareMethod.Text)

        'BB_AccountNo
        sReturnString = Replace(sReturnString, "BB_AccountNo", oPayment.E_Account, , , CompareMethod.Text)
        'Internal account
        sReturnString = Replace(sReturnString, "BB_MyAccountNo", oPayment.I_Account, , , CompareMethod.Text)
        'BB_Name
        sReturnString = Replace(sReturnString, "BB_Name", Replace(oPayment.E_Name, "'", ""), , , CompareMethod.Text)
        'BB_CustomerNo
        sReturnString = Replace(sReturnString, "BB_CustomerNo", sCustomerNo, , , CompareMethod.Text)
        'BB_Zip
        sReturnString = Replace(sReturnString, "BB_Zip", oPayment.E_Zip, , , CompareMethod.Text)
        'BB_Adr1
        sReturnString = Replace(sReturnString, "BB_Adr1", oPayment.E_Adr1, , , CompareMethod.Text)
        'BB_Adr2
        sReturnString = Replace(sReturnString, "BB_Adr2", oPayment.E_Adr2, , , CompareMethod.Text)
        'BB_ValueDate
        sReturnString = Replace(sReturnString, "BB_ValueDate", oPayment.DATE_Value, , , CompareMethod.Text)
        'BB_ClientName, added 19.11.2007
        sReturnString = Replace(sReturnString, "BB_ClientName", Trim(sClientName), , , CompareMethod.Text)
        'BB_ExtraD1, added 15.01.2009
        sReturnString = Replace(sReturnString, "BB_ExtraD1", Trim(oPayment.ExtraD1), , , CompareMethod.Text)
        'BB_REFBank, added 09.03.2010 in VB6 - 27.09.2017 in .NET (for Odin)
        If Not oPayment.REF_Bank1 Is Nothing Then
            sReturnString = Replace(sReturnString, "BB_REFBank", oPayment.REF_Bank1.Trim, , , CompareMethod.Text)
        End If

        ' 08.12.2014 (Manchester Airport) 
        ' Created possibility to crypt info to be saved, like accountnumbers. First used for NHST
        ' Can use BB_EnCrypt(BB_AccountNo). Test on BB_EnCrypt, and decrypt info upto next )
        ' 20.01.2015 sReturnString can be blank
        If Not sReturnString Is Nothing Then
            If sReturnString.ToUpper.Contains("BB_ENCRYPT(") Then
                ' try to decrypt info;
                ' First save to "old" part
                iPos = sReturnString.ToUpper.IndexOf("BB_ENCRYPT(")   ' startpos of BB_Encrypt
                iPos2 = sReturnString.ToUpper.IndexOf(")", iPos)
                sTmp = sReturnString.Substring(iPos, iPos2 - iPos + 1)
                ' remove BB_ENCRYPT() from string
                sTmp2 = sTmp.ToUpper.Replace("BB_ENCRYPT(", "")
                sTmp2 = sTmp2.Replace(")", "")
                ' then decrypt the content;
                sTmp2 = BBEncrypt(sTmp2, 95)
                ' we may have problems with ' as part of the crypted string, replace them
                sTmp2 = sTmp2.Replace("'", "@?@")
                ' Then replace th content in the SQL with crypted content
                sReturnString = sReturnString.Replace(sTmp, sTmp2)
            End If
        End If


        Return sReturnString

    End Function
    Public Function GetCheckAgainstERPSQL(ByRef lCompanyNo As Integer, ByRef iDBProfile_ID As Short) As String
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String = ""
        Dim sReturnValue As String = ""
        Dim iCounter As Integer = 0

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            If 1 = 2 Then 'RunTime() Then 'MANDARIN

                sMySQL = "SELECT * FROM ERPQuery WHERE Company_ID = " & Trim(Str(lCompanyNo)) & " AND DBProfile_ID = " & Trim(Str(iDBProfile_ID))
                sMySQL = sMySQL & " AND Type = " & Trim(Str(8))
                sMySQL = sMySQL & " AND Qorder > 0 ORDER BY Qorder"

                oMyDal.SQL = sMySQL
                sReturnValue = ""

                If oMyDal.Reader_Execute() Then

                    Do While oMyDal.Reader_ReadRecord

                        sReturnValue = oMyDal.Reader_GetString("Query1")
                        sReturnValue = sReturnValue & oMyDal.Reader_GetString("Query2")
                        sReturnValue = sReturnValue & oMyDal.Reader_GetString("Query3")
                        sReturnValue = sReturnValue & oMyDal.Reader_GetString("Query4")
                        sReturnValue = sReturnValue & oMyDal.Reader_GetString("Query5")
                        sReturnValue = sReturnValue & oMyDal.Reader_GetString("Query6")
                        sReturnValue = sReturnValue & oMyDal.Reader_GetString("Query7")
                        sReturnValue = sReturnValue & oMyDal.Reader_GetString("Query8")
                        sReturnValue = sReturnValue & oMyDal.Reader_GetString("Query9")
                        sReturnValue = sReturnValue & oMyDal.Reader_GetString("Query10")
                        sReturnValue = sReturnValue & oMyDal.Reader_GetString("Query11")
                        sReturnValue = sReturnValue & oMyDal.Reader_GetString("Query12")
                        sReturnValue = sReturnValue & oMyDal.Reader_GetString("Query13")

                        iCounter = iCounter + 1
                        If iCounter > 1 Then
                            Throw New System.Exception(LRSCommon(20051))
                            Exit Do
                        End If

                    Loop

                Else

                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

                End If
            Else
                '28.10.2014 - New structure of saving queries in BabelBank
                sMySQL = "SELECT * FROM ERPQuery WHERE Company_ID = " & Trim(Str(lCompanyNo)) & " AND DBProfile_ID = " & Trim(Str(iDBProfile_ID))
                sMySQL = sMySQL & " AND Type = " & Trim(Str(8))
                sMySQL = sMySQL & " AND Qorder > 0 ORDER BY Qorder"

                sMySQL = "SELECT QueryLine FROM ERPQuery E, ERPQueryLine EL  WHERE E.Company_ID = " & lCompanyNo.ToString & " AND E.DBProfile_ID = " & iDBProfile_ID.ToString
                sMySQL = sMySQL & " AND E.Type = 8 AND Qorder > 0 "
                sMySQL = sMySQL & " AND E.Company_ID = EL.Company_ID AND E.DBProfile_ID = EL.DBProfile_ID AND E.ERPQuery_ID = EL.ERPQuery_ID ORDER BY Qorder"

                sReturnValue = ""
                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then

                    If oMyDal.Reader_HasRows Then

                        Do While oMyDal.Reader_ReadRecord

                            sReturnValue = sReturnValue & oMyDal.Reader_GetString("QueryLine")
                        Loop
                    End If 'If oMyDal.Reader_HasRows Then
                Else
                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                End If 'If oMyDal.Reader_Execute() Then

            End If
            sReturnValue = BBDecrypt(Replace(sReturnValue, "@?@", "'"), 3)

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetCheckAgainstERPSQL" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnValue

    End Function
    Public Function GetPattern(ByRef lCompanyNo As Integer, ByRef iDBProfile_ID As Integer) As String(,)

        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String = ""
        Dim aArray(,) As String = Nothing
        Dim iCounter As Integer

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT PatternGroup_ID, Name FROM PatternGroup WHERE Company_ID = " & Trim(Str(lCompanyNo)) & " AND DBProfile_ID = " & iDBProfile_ID.ToString
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                iCounter = 0

                Do While oMyDal.Reader_ReadRecord

                    ReDim Preserve aArray(1, iCounter)
                    aArray(0, iCounter) = oMyDal.Reader_GetString("PatternGroup_ID")
                    aArray(1, iCounter) = oMyDal.Reader_GetString("Name")
                    iCounter = iCounter + 1

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetPattern" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aArray

    End Function
    Public Function GetPatternPrClient(ByRef lCompanyNo As Integer, ByRef iClient_ID As Short) As String()
        ' Return patterns for one client in array, one dimension
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String = ""
        Dim aArray() As String = Nothing
        Dim iCounter As Integer

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT Description FROM Pattern WHERE Company_ID = " & Trim(Str(lCompanyNo)) & " AND Client_ID = " & iClient_ID.ToString
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                iCounter = 0

                Do While oMyDal.Reader_ReadRecord

                    ReDim Preserve aArray(iCounter)
                    aArray(iCounter) = oMyDal.Reader_GetString("Description")
                    iCounter = iCounter + 1

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetPatternPrClient" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aArray

    End Function
    Public Function GetArchiveInfo(ByRef lCompanyNo As Integer) As String()
        ' Return archiveInfo in array, one dimension
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String = ""
        Dim aArray() As String = Nothing

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT ArchiveConnString, ArchiveDatabasetype, ArchiveIncludeOCR, ArchiveUseArchive, ArchiveProvider FROM Company WHERE Company_ID = " & lCompanyNo.ToString
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    ReDim Preserve aArray(4)
                    aArray(0) = oMyDal.Reader_GetString("ArchiveConnString")
                    aArray(1) = oMyDal.Reader_GetString("ArchiveDatabasetype")
                    aArray(2) = oMyDal.Reader_GetString("ArchiveIncludeOCR")
                    aArray(3) = oMyDal.Reader_GetString("ArchiveUseArchive")
                    aArray(4) = oMyDal.Reader_GetString("ArchiveProvider")

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetPatternPrClient" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aArray

    End Function
    Public Function SaveArchiveInfo(ByRef lCompanyNo As Integer, ByRef sArchiveConnString As String, ByRef sArchiveDatabasetype As String, ByRef bArchiveIncludeOCR As Boolean, ByRef bArchiveUseArchive As Boolean) As Boolean
        ' Return archiveInfo in array, one dimension
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String = ""
        Dim sArchiveDatabaseTypeIndex As Short = 0
        Dim iRecordsAffected As Integer = 0
        Dim bReturnValue As Boolean = True

        Try

            Select Case UCase(sArchiveDatabasetype)
                'If changed, change also Private Sub ArchiveSetup_Click() in frmStart
                Case "MICROSOFT ACCESS"
                    sArchiveDatabaseTypeIndex = CShort("0")
                Case "ORACLE VER. ??"
                    sArchiveDatabaseTypeIndex = CShort("1")
                Case "SQL SERVER"
                    sArchiveDatabaseTypeIndex = CShort("2")
                Case "ORACLE FOR ODIN"
                    sArchiveDatabaseTypeIndex = CShort("3")
            End Select

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "UPDATE Company SET ArchiveConnString = '" & sArchiveConnString & "', ArchiveDatabasetype = " & sArchiveDatabaseTypeIndex
            sMySQL = sMySQL & ", ArchiveIncludeOCR = " & Str(bArchiveIncludeOCR) & ", ArchiveUseArchive = " & Str(bArchiveUseArchive) & " WHERE Company_ID = " & Trim(Str(lCompanyNo))

            oMyDal.SQL = sMySQL

            If oMyDal.ExecuteNonQuery Then

                If oMyDal.RecordsAffected = 1 Then
                    bReturnValue = True
                Else
                    MsgBox(LRS(48013) & vbCrLf & LRS(48015), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(48016))
                    'An unexcpected error occured.--- The changes were not saved.  ---- "Error during saving data"
                    bReturnValue = False
                End If

            Else

                MsgBox(LRS(48013) & vbCrLf & oMyDal.ErrorMessage, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(48016))

            End If

        Catch ex As Exception

            Throw New Exception("Function: SaveArchiveInfo" & vbCrLf & ex.Message)

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function

    Public Function GetAdjustments(ByRef lCompanyNo As Integer, ByRef sClientID As String) As String(,)

        'The aim is to make an array consisting of adjustments

        'Second dimension is the data -
        'Element 0 - Amount
        'Element 1 - Currency
        'Element 2 - Exact or Up to
        'Element 3 - AccountNo
        'Element 4 - AccountType
        'Element 5 - Text
        ' Added 03.10.06 VATCode
        'Element 6 - VATCode
        ' Added 13.07.06 VATCode
        'Element 7 - AdjustPerInovice
        'XNET - 27.03.2012  Added Name
        'Element 8 - Name
        ' 30.07.2015 added Dim1-10
        ' Element9 - Dim1
        ' Element10 - Dim2
        '.....
        ' Element 18 - Dim10
        'Element 19 - 

        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String
        Dim aResultArray(,) As String
        Dim iArrayCounter As Integer = -1

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            'XNET - 27.03.2012  Added A.Name
            'sMySQL = "Select M.Amount, M.Currency, M.Exact, A.Pattern, A.Type, A.Txt, A.VATCode, M.AdjustPerInvoice, A.Name FROM  MATCH_DiffAccounts A, MATCH_Diff M WHERE A.Company_ID = " & Trim(Str(lCompanyNo)) & " AND A.Client_ID = " & Trim(sClientID) & " AND A.DiffAccount_ID = M.MATCH_DiffAccountID ORDER BY M.Currency, M.Exact, M.Amount"
            ' 30.07.2015 - introduced Dimensions from MATCH_Diffaccounts
            sMySQL = "Select M.Amount, M.Currency, M.Exact, A.Pattern, A.Dim1, A.Dim2, A.Dim3, A.Dim4, A.Dim5, A.Dim6, A.Dim7, A.Dim8, A.Dim9, A.Dim10, A.Type, A.Txt, A.VATCode, M.AdjustPerInvoice, A.Name FROM  MATCH_DiffAccounts A, MATCH_Diff M WHERE A.Company_ID = " & Trim(Str(lCompanyNo)) & " AND A.Client_ID = " & Trim(sClientID) & " AND A.DiffAccount_ID = M.MATCH_DiffAccountID ORDER BY M.Currency, M.Exact, M.Amount"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    iArrayCounter = iArrayCounter + 1

                    ReDim Preserve aResultArray(18, iArrayCounter)

                    'Amount
                    aResultArray(0, iArrayCounter) = oMyDal.Reader_GetString("Amount")
                    'Currency
                    aResultArray(1, iArrayCounter) = oMyDal.Reader_GetString("Currency")
                    'Exact (or Up to)
                    aResultArray(2, iArrayCounter) = oMyDal.Reader_GetString("Exact")
                    'AccountNo
                    aResultArray(3, iArrayCounter) = oMyDal.Reader_GetString("Pattern")
                    'AccountType
                    aResultArray(4, iArrayCounter) = oMyDal.Reader_GetString("Type")
                    'Text
                    aResultArray(5, iArrayCounter) = oMyDal.Reader_GetString("Txt")
                    'VATCode - added 03.10.06
                    aResultArray(6, iArrayCounter) = oMyDal.Reader_GetString("VATCode")
                    'AdjustPerInovice - added 13.07.2007
                    aResultArray(7, iArrayCounter) = oMyDal.Reader_GetString("AdjustPerInvoice")
                    'XNET - 27.03.2012  Added Name
                    'Name
                    aResultArray(8, iArrayCounter) = oMyDal.Reader_GetString("Name")
                    ' 30.07.2015 - Dim1-10
                    aResultArray(9, iArrayCounter) = oMyDal.Reader_GetString("Dim1")
                    aResultArray(10, iArrayCounter) = oMyDal.Reader_GetString("Dim2")
                    aResultArray(11, iArrayCounter) = oMyDal.Reader_GetString("Dim3")
                    aResultArray(12, iArrayCounter) = oMyDal.Reader_GetString("Dim4")
                    aResultArray(13, iArrayCounter) = oMyDal.Reader_GetString("Dim5")
                    aResultArray(14, iArrayCounter) = oMyDal.Reader_GetString("Dim6")
                    aResultArray(15, iArrayCounter) = oMyDal.Reader_GetString("Dim7")
                    aResultArray(16, iArrayCounter) = oMyDal.Reader_GetString("Dim8")
                    aResultArray(17, iArrayCounter) = oMyDal.Reader_GetString("Dim9")
                    aResultArray(18, iArrayCounter) = oMyDal.Reader_GetString("Dim10")
                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetAdjustments" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aResultArray

    End Function
    Public Function AutobookingExist(ByRef lCompanyNo As Integer) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = False

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If


            sMySQL = "Select * FROM Autobook WHERE Company_ID = " & lCompanyNo.ToString & " AND Active = True"

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    bReturnValue = True
                Else
                    bReturnValue = False
                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: AutobookingExist" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function
    Public Function GetAutobookingsArray(ByRef lCompanyNo As Integer, ByRef sClientNo As String, ByRef bAutoBookingExists As Boolean) As String(,)
        ' NEW FUNCTION, where we can have more than one criteria pr autobooking
        'The aim is to make an array consisting of adjustments

        'Second dimension is the data -
        'Element 0 - ClientNo
        'Element 1 - SelectOnField
        'Element 2 - ValueInField
        'Element 3 - ToAccount
        'Element 4 - Booktext
        'Element 5 - Type
        'Element 6 - ProposeMatch
        'Element 7 - Name
        'Element 8 - AutoBook_ID
        'Element 9 - AutoBookDetail_ID
        'Element 10 - VATCode
        '18.02.2016 - Added the use of dimensions
        'Element 11 - Dim1
        'Element 12 - Dim2
        'Element 13 - Dim3
        'Element 14 - Dim4
        'Element 15 - Dim5
        'Element 16 - Dim6
        'Element 17 - Dim7
        'Element 18 - Dim8
        'Element 19 - Dim9
        'Element 20 - Dim10


        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim aResultArray(,) As String
        Dim iCounter As Integer = -1

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ReDim aResultArray(20, 0)

            sMySQL = "Select C.ClientNo, D.SelectOnField, D.ValueInField, A.ToAccount, A.BookText, A.Type, A.ProposeMatch, A.Name, A.VATCode, D.AutoBook_ID, D.AutoBookDetail_ID, A.Dim1, A.Dim2, A.Dim3, A.Dim4, A.Dim5, A.Dim6, A.Dim7, A.Dim8, A.Dim9, A.Dim10 "
            sMySQL = sMySQL & "FROM Autobook A, AutoBookDetail D, Client C WHERE C.Company_ID = " & Trim(Str(lCompanyNo)) & " AND C.ClientNo = '" & sClientNo & "' AND A.Client_ID = C.Client_ID AND A.Company_ID = C.Company_ID AND A.Client_ID = D.Client_ID AND A.AutoBook_ID = D.AutoBook_ID AND A.Active = True ORDER BY A.Name"
            oMyDal.SQL = sMySQL

            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then

                    bAutoBookingExists = True

                    Do While oMyDal.Reader_ReadRecord

                        iCounter = iCounter + 1

                        ReDim Preserve aResultArray(20, iCounter)
                        'ClientNo
                        aResultArray(0, iCounter) = oMyDal.Reader_GetString("ClientNo")
                        'SelectOnField
                        aResultArray(1, iCounter) = oMyDal.Reader_GetString("SelectOnField")
                        'ValueInField
                        aResultArray(2, iCounter) = oMyDal.Reader_GetString("ValueInField")
                        'ToAccount
                        aResultArray(3, iCounter) = oMyDal.Reader_GetString("ToAccount")
                        'Booktext
                        aResultArray(4, iCounter) = oMyDal.Reader_GetString("BookText")
                        'Type
                        aResultArray(5, iCounter) = oMyDal.Reader_GetString("Type")
                        'ProposeMatch
                        aResultArray(6, iCounter) = oMyDal.Reader_GetString("ProposeMatch")
                        'Name
                        aResultArray(7, iCounter) = oMyDal.Reader_GetString("Name")
                        ' AutoBook_ID
                        aResultArray(8, iCounter) = oMyDal.Reader_GetString("AutoBook_ID")
                        ' AutoBookDetail_ID
                        aResultArray(9, iCounter) = oMyDal.Reader_GetString("AutoBookDetail_ID")
                        'VATCode
                        aResultArray(10, iCounter) = oMyDal.Reader_GetString("VATCode")
                        'Dim1
                        aResultArray(11, iCounter) = oMyDal.Reader_GetString("Dim1")
                        'Dim2
                        aResultArray(12, iCounter) = oMyDal.Reader_GetString("Dim2")
                        'Dim3
                        aResultArray(13, iCounter) = oMyDal.Reader_GetString("Dim3")
                        'Dim4
                        aResultArray(14, iCounter) = oMyDal.Reader_GetString("Dim4")
                        'Dim5
                        aResultArray(15, iCounter) = oMyDal.Reader_GetString("Dim5")
                        'Dim6
                        aResultArray(16, iCounter) = oMyDal.Reader_GetString("Dim6")
                        'Dim7
                        aResultArray(17, iCounter) = oMyDal.Reader_GetString("Dim7")
                        'Dim8
                        aResultArray(18, iCounter) = oMyDal.Reader_GetString("Dim8")
                        'Dim9
                        aResultArray(19, iCounter) = oMyDal.Reader_GetString("Dim9")
                        'Dim10
                        aResultArray(20, iCounter) = oMyDal.Reader_GetString("Dim10")

                    Loop

                Else

                    bAutoBookingExists = False

                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetAutobookingsArray" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aResultArray

    End Function

    '    Public Function BB_RecordCount(ByRef rsMySet As ADODB.Recordset, ByRef bCheckIfExactOne As Boolean, Optional ByRef bMoveFirst As Boolean = True, Optional ByRef lMaxOccurances As Integer = 99999, Optional ByRef sSpecial As String = "") As Integer
    ''04.10.2006 Added sSpecial to the function
    'Dim lReturnValue, lCounter As Integer

    'If sSpecial = "KREDINOR_K90" Then
    '    If rsMySet.BOF And rsMySet.EOF Then
    '        BB_RecordCount = 0
    '        Exit Function
    '    End If
    'End If

    'lReturnValue = rsMySet.RecordCount

    'If lReturnValue = -1 Then
    '    If rsMySet.BOF And rsMySet.EOF Then
    '        lReturnValue = 0
    '    Else
    '        If bMoveFirst Then
    '            rsMySet.MoveFirst()
    '        End If
    '        lReturnValue = 0

    '        If bCheckIfExactOne Then
    '            Do Until rsMySet.EOF Or lReturnValue > 1
    '                lReturnValue = lReturnValue + 1
    '                rsMySet.MoveNext()
    '            Loop
    '        Else
    '            Do Until rsMySet.EOF Or lReturnValue = lMaxOccurances
    '                lReturnValue = lReturnValue + 1
    '                rsMySet.MoveNext()
    '            Loop
    '        End If

    '        If bMoveFirst Then
    '            rsMySet.MoveFirst()
    '        Else
    '            For lCounter = 1 To lReturnValue
    '                rsMySet.MovePrevious()
    '            Next lCounter
    '        End If
    '    End If
    'Else
    '    'return recordcount
    'End If

    'BB_RecordCount = lReturnValue

    '    End Function

    'XNET - 23.02.2012 - New function
    Public Function GetAutobookingsArrayRedCross(ByVal lCompanyNo As Long, ByVal sClientNo As String, ByRef bAutoBookingExists As Boolean, ByVal iImportFormat As Integer, ByVal sI_Account As String, ByVal oClient As vbBabel.Client) As String(,)
        'Used only for Norwegian Red Cross - They store this information in Axapta and we must retrieve the information from Ax.
        'Two possible fields, I_Account and Freetext
        ' NEW FUNCTION, where we can have more than one criteria pr autobooking
        'The aim is to make an array consisting of adjustments

        'Second dimension is the data -
        'Element 0 - ClientNo
        'Element 1 - SelectOnField
        'Element 2 - ValueInField
        'Element 3 - ToAccount
        'Element 4 - Booktext
        'Element 5 - Type
        'Element 6 - ProposeMatch
        'Element 7 - Name
        'Element 8 - AutoBook_ID
        'Element 9 - AutoBookDetail_ID
        'Element 10 - VATCode
        '18.02.2016 - Added the use of dimensions
        'Element 11 - Dim1
        'Element 12 - Dim2
        'Element 13 - Dim3
        'Element 14 - Dim4
        'Element 15 - Dim5
        'Element 16 - Dim6
        'Element 17 - Dim7
        'Element 18 - Dim8
        'Element 19 - Dim9
        'Element 20 - Dim10

        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = String.Empty
        Dim aQueryArray(,,) As String
        Dim aResultArray(,) As String
        Dim lCounter As Long
        Dim bx As Boolean
        Dim bFreetext As Boolean
        Dim bContinue As Boolean
        Dim iArrayCounter As Integer
        Dim sResponse As String

        Try

            aQueryArray = GetERPDBInfo(lCompanyNo, 5)

            For lCounter = 1 To UBound(aQueryArray, 3) 'XNET - 12.12.2012
                If aQueryArray(0, 2, lCounter) = "NRX_MALGRUPPE" Then
                    sMySQL = aQueryArray(0, 0, lCounter)
                    Exit For
                End If
            Next lCounter

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
            oMyDal.Company_ID = lCompanyNo
            oMyDal.DBProfile_ID = oClient.DBProfile_ID
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ReDim aResultArray(20, 0)

            oMyDal.SQL = sMySQL

            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then

                    iArrayCounter = -1
                    lCounter = 0
                    bAutoBookingExists = True

                    Do While oMyDal.Reader_ReadRecord
                        bFreetext = False
                        bContinue = True
                        lCounter = lCounter + 1

                        If Not EmptyString(oMyDal.Reader_GetString("BBRET_MyField")) Then
                            bFreetext = True
                        Else
                            If EmptyString(oMyDal.Reader_GetString("BBRET_Account")) Then
                                bContinue = False
                            End If
                        End If

                        If EmptyString(oMyDal.Reader_GetString("BBRET_MatchID")) Then
                            bContinue = False
                        End If

                        If bContinue Then
                            iArrayCounter = iArrayCounter + 1
                            ReDim Preserve aResultArray(20, iArrayCounter)
                            'ClientNo
                            aResultArray(0, iArrayCounter) = sClientNo
                            'SelectOnField
                            If bFreetext Then
                                aResultArray(1, iArrayCounter) = "Freetext"
                            Else
                                aResultArray(1, iArrayCounter) = "I_Account"
                            End If
                            'ValueInField
                            If bFreetext Then
                                aResultArray(2, iArrayCounter) = "*" & oMyDal.Reader_GetString("BBRET_MyField") & "*"
                            Else
                                aResultArray(2, iArrayCounter) = oMyDal.Reader_GetString("BBRET_Account")
                            End If
                            'ToAccount
                            aResultArray(3, iArrayCounter) = oMyDal.Reader_GetString("BBRET_MatchID")
                            'Booktext
                            aResultArray(4, iArrayCounter) = ""
                            'Type
                            aResultArray(5, iArrayCounter) = "1"
                            'ProposeMatch
                            aResultArray(6, iArrayCounter) = False
                            'Name
                            If bFreetext Then
                                aResultArray(7, iArrayCounter) = "Fritekst: " & oMyDal.Reader_GetString("BBRET_MyField")
                            Else
                                aResultArray(7, iArrayCounter) = "Kontonr.: " & oMyDal.Reader_GetString("BBRET_Account")
                            End If
                            ' AutoBook_ID
                            aResultArray(8, iArrayCounter) = lCounter
                            ' AutoBookDetail_ID
                            aResultArray(9, iArrayCounter) = lCounter
                            'VATCode
                            aResultArray(10, iArrayCounter) = ""
                            'Dim - leave them empty
                        End If

                    Loop

                Else

                    bAutoBookingExists = False

                End If

                'XNET - 06.09.2012 - Added next lines to Add m�lgruppe for Givertelefon
                If iImportFormat = vbBabel.BabelFiles.FileType.Givertelefon Then
                    For lCounter = 1 To UBound(aQueryArray, 3) 'XNET - 12.12.2012
                        If aQueryArray(0, 2, lCounter) = "NRX_VALIDMAAL" Then
                            sMySQL = aQueryArray(0, 0, lCounter)
                            Exit For
                        End If
                    Next lCounter

                    Do While True
                        sResponse = Trim$(InputBox("Vennligst legg inn m�lgruppen som skal benyttes p� alle posteringer i filen.", "M�LGRUPPE"))  '"Please input DebitAccount"))
                        If Len(sResponse) = 0 Then
                            ' cancel pressed
                            ' cut rest
                            Exit Do
                        Else
                            'Check if a valid m�lgruppe is stated in the inputbox

                            oMyDal.SQL = Replace(sMySQL, "BB_MyText", sResponse)

                            If oMyDal.Reader_Execute() Then

                                If oMyDal.Reader_HasRows Then

                                    Exit Do

                                Else
                                    MsgBox("Den angitte m�lgruppen er ikke gyldig." & "Fors�k p� nytt eller opprett m�lgruppen.", vbOKOnly, "Feil i m�lgruppe")
                                End If

                            Else

                                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                            End If
                        End If
                    Loop

                    iArrayCounter = iArrayCounter + 1
                    ReDim Preserve aResultArray(20, iArrayCounter)
                    'ClientNo
                    aResultArray(0, iArrayCounter) = sClientNo
                    'SelectOnField
                    aResultArray(1, iArrayCounter) = "I_Account"
                    'ValueInField
                    aResultArray(2, iArrayCounter) = sI_Account
                    'ToAccount
                    aResultArray(3, iArrayCounter) = sResponse
                    'Booktext
                    aResultArray(4, iArrayCounter) = ""
                    'Type
                    aResultArray(5, iArrayCounter) = "1"
                    'ProposeMatch
                    aResultArray(6, iArrayCounter) = False
                    'Name
                    aResultArray(7, iArrayCounter) = ""
                    ' AutoBook_ID
                    aResultArray(8, iArrayCounter) = "9999"
                    ' AutoBookDetail_ID
                    aResultArray(9, iArrayCounter) = "1"
                    'VATCode
                    aResultArray(10, iArrayCounter) = ""
                    'Dim - Leave them empty
                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetAutobookingArrayRedCross" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aResultArray

    End Function
    Public Function CheckForDifferenceInMatching(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim nSumPayTransferred As Double, nSumPayInvoice As Double
        Dim nSumInvInvoice As Double
        Dim bReturnValue As Boolean


        'Assume success
        bReturnValue = True

        For Each oBabelFile In oBabelFiles
            nSumPayTransferred = 0
            nSumPayInvoice = 0
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    nSumPayTransferred = nSumPayTransferred + oPayment.MON_TransferredAmount
                    nSumPayInvoice = nSumPayInvoice + oPayment.MON_InvoiceAmount
                    nSumInvInvoice = 0
                    For Each oInvoice In oPayment.Invoices
                        If oInvoice.MATCH_Final Then
                            nSumInvInvoice = nSumInvInvoice + oInvoice.MON_InvoiceAmount
                        End If
                    Next oInvoice
                    'New 15.08.2007
                    If oPayment.MATCH_UseoriginalAmountInMatching Then
                        'XNET - 01.12.2011 - Changed the test below
                        If Not IsEqualAmount(nSumInvInvoice, oPayment.MON_OriginallyPaidAmount) Then 'nSumInvInvoice <> oPayment.MON_OriginallyPaidAmount Then
                            bReturnValue = False
                            Exit For
                        End If
                    Else
                        'old code
                        'XNET - 01.12.2011 - Changed the test below
                        If Not IsEqualAmount(nSumInvInvoice, oPayment.MON_InvoiceAmount) Then 'nSumInvInvoice <> oPayment.MON_InvoiceAmount Then
                            bReturnValue = False
                            Exit For
                        End If
                    End If
                Next oPayment
                If Not bReturnValue Then
                    Exit For
                End If
            Next oBatch
            If Not bReturnValue Then
                Exit For
            End If
            'Changed 24/1-04 by Kjell. Commented the next to IFs.
            'This will cause a difference when we export OCR and/or Autogiro transactions
            '    If nSumPayTransferred <> oBabelFile.MON_TransferredAmount Then
            '        bReturnValue = False
            '        Exit For
            '    End If
            '    If nSumPayInvoice <> oBabelFile.MON_InvoiceAmount Then
            '        bReturnValue = False
            '        Exit For
            '    End If
        Next oBabelFile

        oInvoice = Nothing
        oPayment = Nothing
        oBatch = Nothing
        oBabelFile = Nothing

        CheckForDifferenceInMatching = bReturnValue

    End Function

    Public Function CheckForDifferenceInMatchingForSGFinance(ByRef oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim nSumPayTransferred, nSumPayInvoice As Double
        Dim nSumInvInvoice As Double
        Dim bReturnValue As Boolean


        'Assume success
        bReturnValue = True

        For Each oBabelFile In oBabelFiles
            nSumPayTransferred = 0
            nSumPayInvoice = 0
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    nSumPayTransferred = nSumPayTransferred + oPayment.MON_TransferredAmount
                    nSumPayInvoice = nSumPayInvoice + oPayment.MON_InvoiceAmount
                    nSumInvInvoice = 0
                    For Each oInvoice In oPayment.Invoices
                        If oInvoice.MATCH_Final Then
                            nSumInvInvoice = nSumInvInvoice + oInvoice.MON_InvoiceAmount
                        End If
                    Next oInvoice
                    'New 15.08.2007
                    If oPayment.MATCH_UseoriginalAmountInMatching Then
                        If oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.NotMatched Then
                            'No need to check for differences because the original amount is used (the invoice will be based upon the MON_InvoiceAmount
                        Else
                            If nSumInvInvoice <> oPayment.MON_OriginallyPaidAmount Then
                                bReturnValue = False
                                Exit For
                            End If
                        End If
                    Else
                        'old code
                        If nSumInvInvoice <> oPayment.MON_InvoiceAmount Then
                            bReturnValue = False
                            Exit For
                        End If
                    End If
                Next oPayment
                If Not bReturnValue Then
                    Exit For
                End If
            Next oBatch
            If Not bReturnValue Then
                Exit For
            End If
            'Changed 24/1-04 by Kjell. Commented the next to IFs.
            'This will cause a difference when we export OCR and/or Autogiro transactions
            '    If nSumPayTransferred <> oBabelFile.MON_TransferredAmount Then
            '        bReturnValue = False
            '        Exit For
            '    End If
            '    If nSumPayInvoice <> oBabelFile.MON_InvoiceAmount Then
            '        bReturnValue = False
            '        Exit For
            '    End If
        Next oBabelFile

        oInvoice = Nothing
        oPayment = Nothing
        oBatch = Nothing
        oBabelFile = Nothing

        CheckForDifferenceInMatchingForSGFinance = bReturnValue

    End Function
    Public Function DoesStoredPaymentsExists(ByRef iErrorOnExit As Short, ByRef iCompanyNo As Short, ByRef lFilesetup_ID As Integer) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = False
        Dim sTemp As String = ""

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "Select SavedPaymentsExists, ErrorOnExit FROM FileSetup WHERE Company_ID = " & iCompanyNo.ToString & " AND FileSetup_ID = " & lFilesetup_ID.ToString
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    sTemp = oMyDal.Reader_GetString("ErrorOnExit")
                    If sTemp = "" Then
                        iErrorOnExit = 0
                    Else
                        iErrorOnExit = CShort(sTemp)
                    End If
                    If oMyDal.Reader_GetString("SavedPaymentsExists") = "True" Then
                        sMySQL = "SELECT COUNT(*) AS Occurance FROM BabelFile WHERE FileSetup_ID = " & Trim(Str(lFilesetup_ID)) & " AND Company_ID = " & Trim(Str(iCompanyNo))
                        oMyDal.SQL = sMySQL
                        If oMyDal.Reader_Execute() Then
                            Do While oMyDal.Reader_ReadRecord
                                If Val(oMyDal.Reader_GetString("Occurance")) > 0 Then
                                    bReturnValue = True
                                End If
                                Exit Do
                            Loop
                        Else
                            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                        End If
                    End If

                    Exit Do
                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: DoesStoredPaymentsExists" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function

    Public Function CountMatchedItems(ByRef lCompanyNo As Integer, ByRef lTotalNumber As Integer, ByRef lMatchedItems As Integer, ByRef bPartlyOrProposedExists As Boolean) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            'Find total number of payments
            sMySQL = "SELECT COUNT(*) AS Occurance FROM Invoice I, Payment P WHERE I.Company_ID = P.Company_ID "
            sMySQL = sMySQL & "AND I.BabelFile_ID = P.BabelFile_ID AND I.Batch_ID = P.Batch_ID AND I.Payment_ID = P.Payment_ID "
            sMySQL = sMySQL & "AND I.MATCH_Final = True AND I.Company_ID = " & lCompanyNo.ToString
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    lTotalNumber = Val(oMyDal.Reader_GetString("Occurance"))
                    Exit Do

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

            'Find total number of matched payments
            sMySQL = "SELECT COUNT(*) AS OccuranceMatched FROM Invoice I, Payment P WHERE I.Company_ID = P.Company_ID "
            sMySQL = sMySQL & "AND I.BabelFile_ID = P.BabelFile_ID AND I.Batch_ID = P.Batch_ID AND I.Payment_ID = P.Payment_ID "
            sMySQL = sMySQL & "AND I.MATCH_Final = True AND I.MATCH_Matched = True AND I.Company_ID = " & lCompanyNo.ToString
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    lMatchedItems = Val(oMyDal.Reader_GetString("OccuranceMatched"))
                    Exit Do

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

            'Check if there are proposed or partly matched payments
            sMySQL = "SELECT COUNT(*) AS OccuranceProposed FROM Invoice I, Payment P WHERE I.Company_ID = P.Company_ID "
            sMySQL = sMySQL & "AND I.BabelFile_ID = P.BabelFile_ID AND I.Batch_ID = P.Batch_ID AND I.Payment_ID = P.Payment_ID "
            sMySQL = sMySQL & "AND I.MATCH_Final = True AND I.Company_ID = " & Trim(Str(lCompanyNo))
            sMySQL = sMySQL & " AND (P.MATCH_Matched = " & Trim(Str(vbBabel.BabelFiles.MatchStatus.ProposedMatched)) & " OR P.MATCH_Matched = " & Trim(Str(vbBabel.BabelFiles.MatchStatus.PartlyMatched)) & ")"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    If Val(oMyDal.Reader_GetString("OccuranceProposed")) > 0 Then
                        bPartlyOrProposedExists = True
                    Else
                        bPartlyOrProposedExists = False
                    End If
                    Exit Do

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: CountMatchedItems" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return True

    End Function
    Public Function UpdateProgressInBBDB(ByRef iStatus As Short, ByRef sFilesetup_ID As String, ByRef iCompany_ID As Short) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True

        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "UPDATE FileSetup SET ErrorOnExit = " & iStatus.ToString
            sMySQL = sMySQL & " WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & sFilesetup_ID

            oMyDal.SQL = sMySQL

            If oMyDal.ExecuteNonQuery Then

                If oMyDal.RecordsAffected = 0 Then
                    bReturnValue = False
                Else
                    bReturnValue = True
                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: UpdateProgressInBBDB" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function

    Public Function UpdateSavedPaymentsMark(ByRef iCompany_ID As Short, ByRef sFilesetup_ID As String, ByRef bPaymentsSaved As Boolean) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True

        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "UPDATE FileSetup SET SavedPaymentsExists = " & Trim(Str(bPaymentsSaved))
            sMySQL = sMySQL & " WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & sFilesetup_ID

            oMyDal.SQL = sMySQL

            If oMyDal.ExecuteNonQuery Then

                If oMyDal.RecordsAffected = 0 Then
                    bReturnValue = False
                Else
                    bReturnValue = True
                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: UpdateSavedPaymentsMark" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function
    Public Function UpdateExportMark(ByRef iCompany_ID As Short) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True

        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "UPDATE BabelFile SET Exported = " & Trim(Str(True)) & ", ExportDate = '" & Format(Now, "yyyyMMdd") 'TODO - Change the use of format maybe
            sMySQL = sMySQL & "' WHERE Company_ID = " & iCompany_ID.ToString & " AND Exported = False"

            oMyDal.SQL = sMySQL

            If oMyDal.ExecuteNonQuery Then

                If oMyDal.RecordsAffected = 0 Then
                    bReturnValue = False
                Else
                    bReturnValue = True
                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: UpdateExportMark" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function

    Public Function FindUniqueERPIDInfo(ByRef iCompany_ID As Short) As String
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sReturnValue As String = ""

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "Select ERPPaymentID FROM Company "
            sMySQL = sMySQL & " WHERE Company_ID = " & iCompany_ID.ToString
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    sReturnValue = oMyDal.Reader_GetString("ERPPaymentID")

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: FindUniqueERPIDInfo" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnValue

    End Function
    Public Function UpdateUniqueERPIDInfo(ByRef iCompany_ID As Short, ByRef sLastUsedID As String) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True

        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "UPDATE Company SET ERPPaymentID = '" & sLastUsedID
            sMySQL = sMySQL & "' WHERE Company_ID = " & iCompany_ID.ToString

            oMyDal.SQL = sMySQL

            If oMyDal.ExecuteNonQuery Then

                If oMyDal.RecordsAffected = 0 Then
                    bReturnValue = False
                Else
                    bReturnValue = True
                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: UpdateUniqueERPIDInfo" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function
    Public Function FindNextSubfieldMT940(ByRef sTxt As String, ByRef iPos As Short) As Short
        'The function returns the position of the next subfield
        Dim iReturnValue As Short
        Dim iMaxLength As Short

        On Error GoTo ErrorFindNextSubfieldMT940

        If Left(sTxt, 1) = "#" Then
            sTxt = Mid(sTxt, 2)
        ElseIf Mid(sTxt, 2, 1) = "#" Then
            sTxt = Left(sTxt, 1) & Mid(sTxt, 3)
        End If

        Select Case Left(sTxt, 2)

            Case "20"
                'Find either 21 or 30
                iMaxLength = 27
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 21", " 30")
                Else
                    iReturnValue = -1
                End If

            Case "21"
                'Find either 22 or 30
                iMaxLength = 27
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 22", " 30")
                Else
                    iReturnValue = -1
                End If

            Case "22"
                'Find either 23 or 30
                iMaxLength = 27
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 23", " 30")
                Else
                    iReturnValue = -1
                End If

            Case "23"
                'Find either 24 or 30
                iMaxLength = 27
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 24", " 30")
                Else
                    iReturnValue = -1
                End If

            Case "24"
                'Find either 25 or 30
                iMaxLength = 27
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 25", " 30")
                Else
                    iReturnValue = -1
                End If

            Case "25"
                'Find either 26 or 30
                iMaxLength = 27
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 26", " 30")
                Else
                    iReturnValue = -1
                End If

            Case "26"
                'Find either 27 or 30
                iMaxLength = 27
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 27", " 30")
                Else
                    iReturnValue = -1
                End If

            Case "27"
                'Find either 28 or 30
                iMaxLength = 27
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 28", " 30")
                Else
                    iReturnValue = -1
                End If

            Case "28"
                'Find either 29 or 30
                iMaxLength = 27
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 29", " 30")
                Else
                    iReturnValue = -1
                End If

            Case "29"
                'Find 30
                iMaxLength = 27
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 30")
                Else
                    iReturnValue = -1
                End If

            Case "30"
                'Find 31
                iMaxLength = 15
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 31")
                Else
                    iReturnValue = -1
                End If

            Case "31"
                'Find 32
                iMaxLength = 20
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 32")
                Else
                    If Len(sTxt) > 2 Then
                        iMaxLength = Len(sTxt)
                    Else
                        iReturnValue = -1
                    End If
                End If

            Case "32"
                'Find either 33 or 34
                iMaxLength = 27
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 33", " 34")
                Else
                    If Len(sTxt) > 2 Then
                        iMaxLength = Len(sTxt)
                        iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 33", " 34")
                    Else
                        iReturnValue = -1
                    End If
                End If

            Case "33"
                'Find 34
                iMaxLength = 27
                If Len(sTxt) > iMaxLength + 1 Then
                    iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 34")
                Else
                    If Len(sTxt) > 2 Then
                        iMaxLength = Len(sTxt)
                        iReturnValue = LocalFindNextSubfieldMT940(sTxt, iMaxLength, " 33", " 34")
                    Else
                        iReturnValue = -1
                    End If
                End If

            Case "34"
                'The end
                iReturnValue = Len(sTxt)


        End Select

        FindNextSubfieldMT940 = iReturnValue

        Exit Function

ErrorFindNextSubfieldMT940:

        FindNextSubfieldMT940 = -1

    End Function

    Private Function LocalFindNextSubfieldMT940(ByRef sTxt As String, ByRef iMaxLength As Short, ByRef sFirstPossibleSubfield As String, Optional ByRef sSecondPossibleSubfield As String = "") As Short
        Dim iNewLength1, iNewLength11 As Short
        Dim iNewLength2, iNewLength21 As Short
        Dim iReturnValue As Short
        Dim sPossibleText As String
        Dim iNoOfCrLF As Short

        On Error GoTo ErrorLocalFindNextSubfieldMT940
        If Len(sTxt) + 3 > iMaxLength Then
            sPossibleText = Mid(sTxt, 3, iMaxLength + 3)
            iNoOfCrLF = Len(sPossibleText) - Len(Replace(sPossibleText, "#", ""))
            sPossibleText = Mid(sTxt, 3, iMaxLength + 3 + iNoOfCrLF)
        Else
            sPossibleText = sTxt
        End If

        iNewLength11 = Len(sPossibleText) - Len(Replace(sPossibleText, "#" & Mid(sFirstPossibleSubfield, 2), "", , , CompareMethod.Text))
        If Len(sSecondPossibleSubfield) > 0 Then
            iNewLength21 = Len(sPossibleText) - Len(Replace(sPossibleText, "#" & Mid(sSecondPossibleSubfield, 2), "", , , CompareMethod.Text))
        End If

        sPossibleText = Replace(sPossibleText, "#", "")
        'sPossibleText = Replace(sPossibleText, sFirstPossibleSubfield, "", , vbTextCompare)
        iNewLength1 = iNewLength11 + Len(sPossibleText) - Len(Replace(sPossibleText, sFirstPossibleSubfield, "", , , CompareMethod.Text))
        iNewLength2 = iNewLength21 + Len(sPossibleText) - Len(Replace(sPossibleText, sSecondPossibleSubfield, "", , , CompareMethod.Text))

        'iNewLength1 = iMaxLength + 3 - Len(Replace(sPossibleText, sFirstPossibleSubfield, "", , vbTextCompare))
        'iNewLength2 = iMaxLength + 3 - Len(Replace(sPossibleText, sSecondPossibleSubfield, "", , vbTextCompare))
        If iNewLength1 + iNewLength2 = 0 Then
            iReturnValue = -1
        ElseIf iNewLength1 + iNewLength2 = 3 Then
            If iNewLength1 = 3 Then
                If iNewLength11 = 3 Then
                    iReturnValue = InStr(sTxt, "#" & Mid(sFirstPossibleSubfield, 2)) - 3
                Else
                    iReturnValue = InStr(sTxt, sFirstPossibleSubfield) - 3
                    If iReturnValue = -3 Then 'i.e  3#2
                        iReturnValue = InStr(sPossibleText, sFirstPossibleSubfield) - 1
                    End If
                End If
            ElseIf iNewLength2 = 3 Then
                If iNewLength21 = 3 Then
                    iReturnValue = InStr(sTxt, "#" & Mid(sSecondPossibleSubfield, 2)) - 3
                Else
                    iReturnValue = InStr(sTxt, sSecondPossibleSubfield) - 3
                    If iReturnValue = -3 Then 'i.e  3#2
                        iReturnValue = InStr(sPossibleText, sSecondPossibleSubfield) - 1
                    End If
                End If
                'iReturnValue = InStr(sTxt, sSecondPossibleSubfield) - 3
            Else
                iReturnValue = -1
            End If
        ElseIf iNewLength1 + iNewLength2 = 6 Then
            If iNewLength1 = 3 Then
                If Mid(sFirstPossibleSubfield, 2, 1) = "2" Then 'A messagefield
                    'Don't bother return the text field
                    iReturnValue = InStr(sTxt, sFirstPossibleSubfield) - 3
                End If
            End If

        Else
            If iNewLength1 = 3 Then

            End If



        End If

        LocalFindNextSubfieldMT940 = iReturnValue

        Exit Function

ErrorLocalFindNextSubfieldMT940:

        LocalFindNextSubfieldMT940 = -1

    End Function

    '14.05.2018 - Changed lSequenceNo As Long to nSequenceNo AS Double
    Public Function FindNumericPartOfString(ByRef sInString As String, ByRef nSequenceNo As Double, ByRef sAlphaPart As String) As Boolean
        'Used i.e. with Vouchers
        'Assumes that first part of string is alfabetic and last part numeric
        'This function returns the alphbetic and numeric part of the string
        Dim lDigitsLen As Integer
        Dim i As Short

        On Error GoTo ErrorFindNumericPartOfString

        If Len(sInString) > 0 Then
            lDigitsLen = 0
            sAlphaPart = ""
            For i = Len(sInString) To 1 Step -1
                If IsNumeric(Mid(sInString, i, 1)) Then
                    lDigitsLen = lDigitsLen + 1
                Else
                    Exit For
                End If
            Next i
            sAlphaPart = Left(sInString, Len(sInString) - lDigitsLen)
            nSequenceNo = Val(Right(sInString, lDigitsLen))
        Else
            Err.Raise(20018, "FindNumericPartOfString", LRSCommon(20018))
        End If

        FindNumericPartOfString = True

        Exit Function

ErrorFindNumericPartOfString:

        Err.Raise(20017, "FindNumericPartOfString", LRSCommon(20017, sInString))

        FindNumericPartOfString = False

    End Function
    ' XNET 23.10.2013 - added function
    Public Function IsPaymentStructured(ByVal oPayment As vbBabel.Payment) As Boolean
        ' All invoices has
        ' - UniqueID (OCR, Reference, etc), or
        ' - InvoiceNo
        Dim oInvoice As vbBabel.Invoice
        Dim bStructured As Boolean
        bStructured = True
        For Each oInvoice In oPayment.Invoices
            If EmptyString(oInvoice.Unique_Id) And EmptyString(oInvoice.InvoiceNo) Then
                bStructured = False
                Exit For
            End If
        Next oInvoice
        IsPaymentStructured = bStructured
    End Function

    Public Function IsPaymentDomestic(ByRef oPayment As vbBabel.Payment, Optional ByRef sSWIFTAdressDebitAccount As String = "", Optional ByRef bValidateOnE_CountryCode As Boolean = True, Optional ByRef bValidateOnE_BankCountryCode As Boolean = True, Optional ByRef bMustBeDomesticCurrency As Boolean = False, Optional ByRef sDebetAccountCurrency As String = "") As Boolean
        'Dim sSWIFTAdressDebitAccount As String
        Dim sCountryCodeDebitAccount As String
        Dim bx As Boolean
        Dim bReturnValue As Boolean
        Dim bReturnValueFound As Boolean '19.05.2009 - Added the use of this value

        bReturnValue = False
        bReturnValueFound = False

        If oPayment.PayTypeSetInImport Then
            If oPayment.PayType = "I" Then
                bReturnValue = False
            Else
                bReturnValue = True
            End If
            IsPaymentDomestic = bReturnValue
            Exit Function
        End If

        ' New tests 31.03.05, because we may lack profile
        If EmptyString(sSWIFTAdressDebitAccount) Then
            If Not oPayment.VB_Profile Is Nothing Then
                bx = FindAccSWIFTAddr(oPayment.I_Account, oPayment.VB_Profile, oPayment.VB_FilenameOut_ID, sSWIFTAdressDebitAccount, False, sCountryCodeDebitAccount)
            End If
        End If
        ' XokNET 08.09.2011 - added possibility to set debitaccounts countrycode in Account setup.
        ' This is vital for DnBAccounts in CA and AU, where BIC says US and SG.
        ' If countrycode set in Account, use this one, which is set in FindAccSWIFTAddr
        If EmptyString(sCountryCodeDebitAccount) Then
            sCountryCodeDebitAccount = Mid$(sSWIFTAdressDebitAccount, 5, 2)
        End If
        oPayment.BANK_SWIFTCode = Trim$(oPayment.BANK_SWIFTCode)

        'New test added 25.04.2007
        'If an IBAN-number is stated then, check the countrycode
        If IsIBANNumber(Trim(oPayment.E_Account), False) <> Scripting.Tristate.TristateFalse Then
            If sCountryCodeDebitAccount = Trim(Left(oPayment.E_Account, 2)) Then
                bReturnValue = True
            Else
                bReturnValue = False
            End If

            ' 25.03.2015 Added special test for Sweden - where account can start with BG for BankGiro and PG for Postgiro
            If sCountryCodeDebitAccount = "SE" And (Trim(Left(oPayment.E_Account, 2)) = "BG" Or Trim(Left(oPayment.E_Account, 2)) = "PG") And Len(Trim(oPayment.E_Account)) < 12 Then
                ' use test len<12 to not mix up with IBAN  BG to Bulgaria
                bReturnValue = True
            End If

            ''New, must check on BIC code
            'ElseIf Len(oPayment.BANK_SWIFTCode) = 8 Or Len(oPayment.BANK_SWIFTCode) = 11 Then
            '    If sCountryCodeDebitAccount = Mid$(oPayment.BANK_SWIFTCode, 5, 2) Then
            '        bReturnValue = True
            '    Else
            '        bReturnValue = False
            '    End If
        Else
            'Use the same test as before
            Select Case oPayment.BANK_BranchType

                Case vbBabel.BabelFiles.BankBranchType.SWIFT
                    If (oPayment.ImportFormat = vbBabel.BabelFiles.FileType.TelepayTBIO Or oPayment.ImportFormat = vbBabel.BabelFiles.FileType.Telepay2) And oPayment.BANK_SWIFTCode = "BANKGIRO" Then
                        If sCountryCodeDebitAccount = "SE" Then
                            bReturnValue = True
                        Else
                            bReturnValue = False
                        End If

                    ElseIf Len(oPayment.BANK_SWIFTCode) = 8 Or Len(oPayment.BANK_SWIFTCode) = 11 Then
                        'Use SWIFT-code to determine the receivers ountry
                        If sCountryCodeDebitAccount = Mid(oPayment.BANK_SWIFTCode, 5, 2) Then
                            bReturnValue = True
                        Else
                            bReturnValue = False
                        End If
                    Else
                        '18.10.2016 - Added the next IF, prior the function returned true at this stage
                        'We experienced problems with this when the SWIFT code was stated wrongly f.ex. 9 positions
                        If oPayment.I_CountryCode.Length > 0 Then
                            If FindDomesticCurrency(Trim(oPayment.I_CountryCode)).IndexOf(oPayment.MON_TransferCurrency) = -1 Then
                                bReturnValue = False
                            Else
                                bReturnValue = True
                            End If
                        ElseIf sCountryCodeDebitAccount.Length > 0 Then
                            If FindDomesticCurrency(Trim(sCountryCodeDebitAccount)).IndexOf(oPayment.MON_TransferCurrency) = -1 Then
                                bReturnValue = False
                            Else
                                bReturnValue = True
                            End If
                        End If
                    End If

                Case vbBabel.BabelFiles.BankBranchType.NoBranchType 'No help here, but
                    'New 17.04.2008 by Kjell
                    'If the receiver of the monew is in another country than the bankaccount this code
                    ' created problems for Gjensidige and DnBNOR Finans
                    'If neither a IBAN nor a SWIFT-address is stated we assume it's a domestic payment.
                    If bValidateOnE_BankCountryCode Then
                        '06.11.2009 - Cahnged from E_CountryCode to BANK_CountryCode in the line beneath
                        If Len(Trim(oPayment.BANK_CountryCode)) = 2 Then
                            If oPayment.BANK_CountryCode = sCountryCodeDebitAccount Then
                                bReturnValue = True
                                '19.05.2009 - Changed
                                'After validating on E_BankCountryCode and foun a Code we set the bValidateOnE_CountryCode to false (see some lines above)
                                ' When this is set to True we end up here where the ReturnValue is always set to true.

                                '06.11.2009 - Old code - moved som lines downwards outside the IF
                                'bReturnValueFound = True
                            Else
                                bReturnValue = False
                            End If
                            'No need to check E_CountryCode
                            bValidateOnE_CountryCode = False
                            '06.11.2009 - New code
                            bReturnValueFound = True
                        Else
                            bReturnValue = True
                        End If
                    End If
                    If bValidateOnE_CountryCode And Not bReturnValueFound Then
                        If Len(Trim(oPayment.E_CountryCode)) = 2 Then
                            If oPayment.E_CountryCode = sCountryCodeDebitAccount Then
                                bReturnValue = True
                            Else
                                bReturnValue = False
                            End If
                        Else
                            bReturnValue = True
                        End If
                    Else
                        '19.05.2009 - see above
                        If bReturnValueFound Then
                            'Nothing to do
                        Else
                            bReturnValue = True
                        End If
                    End If

                Case Else 'Use bankbranchtype
                    Select Case oPayment.BANK_BranchType

                        Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl
                            If sCountryCodeDebitAccount = "DE" Then
                                If oPayment.MON_TransferCurrency = "EUR" Then
                                    bReturnValue = True
                                End If
                            End If
                        Case vbBabel.BabelFiles.BankBranchType.Bankleitzahl_Austria
                            If sCountryCodeDebitAccount = "AT" Then
                                If oPayment.MON_TransferCurrency = "EUR" Then
                                    bReturnValue = True
                                End If
                            End If

                        Case vbBabel.BabelFiles.BankBranchType.Chips
                            If sCountryCodeDebitAccount = "US" Then
                                If oPayment.MON_TransferCurrency = "USD" Then
                                    bReturnValue = True
                                End If
                            End If

                        Case vbBabel.BabelFiles.BankBranchType.Fedwire
                            If sCountryCodeDebitAccount = "US" Then
                                If oPayment.MON_TransferCurrency = "USD" Then
                                    bReturnValue = True
                                End If
                            End If
                            ' 14.06.2016 Added  for Canada
                            If sCountryCodeDebitAccount = "CA" Then
                                If oPayment.MON_TransferCurrency = "USD" Or oPayment.MON_TransferCurrency = "CAD" Then
                                    bReturnValue = True
                                End If
                            End If

                        Case vbBabel.BabelFiles.BankBranchType.SortCode
                            If sCountryCodeDebitAccount = "GB" Then
                                If oPayment.MON_TransferCurrency = "GBP" Then
                                    bReturnValue = True
                                End If
                            End If

                        Case vbBabel.BabelFiles.BankBranchType.MEPS
                            If sCountryCodeDebitAccount = "SG" Then
                                If oPayment.MON_TransferCurrency = "SGD" Then
                                    bReturnValue = True
                                End If
                            End If

                        Case vbBabel.BabelFiles.BankBranchType.BSB ' added 30.03.2010
                            If sCountryCodeDebitAccount = "SG" Or sCountryCodeDebitAccount = "AU" Then
                                If oPayment.MON_TransferCurrency = "AUD" Then
                                    bReturnValue = True
                                End If
                            End If
                        Case vbBabel.BabelFiles.BankBranchType.CC
                            If sCountryCodeDebitAccount = "US" Or sCountryCodeDebitAccount = "CA" Then
                                If oPayment.MON_TransferCurrency = "CAD" Or oPayment.MON_TransferCurrency = "USD" Then
                                    bReturnValue = True
                                End If
                            End If

                        Case Else
                            bReturnValue = True

                    End Select

            End Select
        End If 'If IsIBANNumber(oPayment.E_Account) > 0 Then

        If Len(Trim(oPayment.I_CountryCode)) = 0 Then
            oPayment.I_CountryCode = sCountryCodeDebitAccount
        End If

        If bReturnValue = True Then
            If bMustBeDomesticCurrency Then
                'Test if the payment currency is identical to the domestic currency
                'If FindDomesticCurrency(Trim(oPayment.I_CountryCode)) <> oPayment.MON_TransferCurrency Then
                ' 14.06.2016 due to CA with USD and CAD, changed the If below
                If FindDomesticCurrency(Trim(oPayment.I_CountryCode)).IndexOf(oPayment.MON_TransferCurrency) = -1 Then
                    bReturnValue = False
                End If

                ' added 08.11.2016
                ' f.ex. in Norway, we must also check if there is exchange or not.
                ' we can f.ex. pay NOK from a USD account, then we need to classify as International-payment
                If Not EmptyString(sDebetAccountCurrency) Then
                    ' test if sDebetAccountCurrency different from transfercurrency
                    If oPayment.MON_TransferCurrency <> sDebetAccountCurrency Then
                        bReturnValue = False
                    End If
                End If
            End If
        End If


        ' 21.11.2014 - Special situation, where we in Norway have a transfer with priority
        If bReturnValue = True And oPayment.I_CountryCode = "NO" And oPayment.Priority = True Then
            bReturnValue = False  ' set as Int
        End If

        IsPaymentDomestic = bReturnValue

    End Function
    'XNET - 01.12.2010 - Added next function
    Public Function IsSEPAPayment(ByVal oPayment As vbBabel.Payment, Optional ByVal sSWIFTAdressDebitAccount As String = "", Optional ByVal bCheckCharges As Boolean = True, Optional ByVal bNewTypeOfCheck As Boolean = False) As Boolean
        '01.12.2010 - Poorly tested
        'EU standard payment, ie cross-border payment under Article 2 a) i) of Regulation (EC)No 2560/2001 of the European Parliament and of the Council of the
        'European Union on cross-border payments in euro, which are in euro up to an amount of EUR 50,000 and in which, pursuant to Article 5 (2),
        'the IBAN of the beneficiary and the BIC of the bank of the beneficiary are mentioned.

        'The parameter bCheckCharges is used like this
        'If set to true - oPayment.MON_ChargeMeDomestic must be True in the oPayment
        '   which is passed to this function , and oPayment.MON_ChargeMeAbroad to false.
        'If set to false - No test is done but oPayment.MON_ChargeMeDomestic is set to True
        '   in this function and oPayment.MON_ChargeMeAbroad is set to false.

        Dim sCountryCodeDebitAccount As String
        Dim sCountryCodeCreditAccount As String = ""
        Dim bx As Boolean
        Dim bReturnValue As Boolean

        '28.12.2014 - Added next test, check with Jan-P
        '29.04.2015 - Removed this IF, but we are still a bit unsure. SEB says it may be SEPA and urgent, DnB says no (from Jan-P memory)
        ' a search on the net seems to say it is OK
        ' 20.04.2016
        ' DNB (H�kon Belt + Sigrud Wiig) says that Priority, Urgent,  is NOT a SEPA payment
        If oPayment.Priority Then
            IsSEPAPayment = False
            Exit Function
        End If

        ' New tests 31.03.05, because we may lack profile
        If EmptyString(sSWIFTAdressDebitAccount) Then
            If Not oPayment.VB_Profile Is Nothing Then
                bx = FindAccSWIFTAddr(oPayment.I_Account, oPayment.VB_Profile, oPayment.VB_FilenameOut_ID, sSWIFTAdressDebitAccount, False)
            End If
        End If
        sCountryCodeDebitAccount = Mid$(sSWIFTAdressDebitAccount, 5, 2)
        oPayment.BANK_SWIFTCode = Trim$(oPayment.BANK_SWIFTCode)

        '09.03.2016 - KI - Added next IF
        If bNewTypeOfCheck Then

            bReturnValue = True
            If Not Trim$(oPayment.MON_InvoiceCurrency) = "EUR" Then
                bReturnValue = False
            End If

            If Not EmptyString(oPayment.MON_TransferCurrency) Then
                If Not Trim$(oPayment.MON_TransferCurrency) = "EUR" Then
                    bReturnValue = False
                End If
            End If

            If sCountryCodeDebitAccount.Length = 2 Then
                If Not IsSEPACountry(sCountryCodeDebitAccount) Then
                    bReturnValue = False
                End If
            Else
                bReturnValue = False
            End If

            'Find receivers countrycode
            If IsIBANNumber(oPayment.E_Account, False, False) <> Scripting.Tristate.TristateFalse Then
                sCountryCodeCreditAccount = oPayment.E_Account.Substring(0, 2)
            End If

            If sCountryCodeCreditAccount.Length = 2 Then
                If Not IsSEPACountry(sCountryCodeCreditAccount) Then
                    bReturnValue = False
                End If
            Else
                bReturnValue = False
            End If

            IsSEPAPayment = bReturnValue

        Else
            ' XNET 21.10.2013
            If Not IsSEPACountry(sCountryCodeDebitAccount) Then
                IsSEPAPayment = False
                Exit Function
            End If

            'An IBAN-number must be stated check the countrycode
            If IsIBANNumber(Trim$(oPayment.E_Account), False) = Scripting.Tristate.TristateFalse Then
                IsSEPAPayment = False
                Exit Function
            End If

            ' XNET 21.10.2013
            If Not IsSEPACountry(Left$(oPayment.E_Account, 2)) Then
                IsSEPAPayment = False
                Exit Function
            End If

            ' Her m� vi sannsynligvis ogs� teste p� blank SWIFT, da det er (snart?) tillatt � sende IBAN uten BIC
            '05.10.2016 - det er n� tillatt � kj�re uten SWIFT i SEPA, dersom det er IBAN
            '21.12.2017 - removed this test
            'If Not EmptyString(oPayment.BANK_SWIFTCode) Then
            '    If Len(oPayment.BANK_SWIFTCode) <> 8 And Len(oPayment.BANK_SWIFTCode) <> 11 Then
            '        IsSEPAPayment = False
            '        Exit Function
            '    End If
            'End If


            ' 25.01.2018
            ' har diskutert med H�kon Belt i DNB -
            ' betalinger OVERF�RT i EUR, som er vekslet fra annen valuta ER Sepa-betalinger
            ' det betyr at det er MON_TransferCurrency som ruler!!
            ' har derfor lagt inn neste If
            If EmptyString(oPayment.MON_TransferCurrency) Then
                ' If no TransferCurrency set, then test on InvoiceCurrency
                If Not Trim$(oPayment.MON_InvoiceCurrency) = "EUR" Then
                    IsSEPAPayment = False
                    Exit Function
                End If
            End If

            If Not EmptyString(oPayment.MON_TransferCurrency) Then
                If Not Trim$(oPayment.MON_TransferCurrency) = "EUR" Then
                    IsSEPAPayment = False
                    Exit Function
                End If
            End If

            If bCheckCharges Then
                If Not oPayment.MON_ChargeMeDomestic Then
                    IsSEPAPayment = False
                    Exit Function
                End If
                If oPayment.MON_ChargeMeAbroad Then
                    IsSEPAPayment = False
                    Exit Function
                End If
            Else
                oPayment.MON_ChargeMeDomestic = True
                oPayment.MON_ChargeMeAbroad = False
            End If

            IsSEPAPayment = True
            End If

    End Function
    Public Function IsEU_EEAPayment(ByVal oPayment As vbBabel.Payment, Optional ByVal sSWIFTAdressDebitAccount As String = "", Optional ByVal bCheckCharges As Boolean = True) As Boolean
        ' 05.01.2018 - In SEPA area, all currencies
        'EU standard payment, ie cross-border payment under Article 2 a) i) of Regulation (EC)No 2560/2001 of the European Parliament and of the Council of the
        'European Union on cross-border payments IN ALL CURRENCIES, which are in euro up to an amount of EUR 50,000 and in which, pursuant to Article 5 (2),
        'the IBAN of the beneficiary and the BIC of the bank of the beneficiary are mentioned.

        'The parameter bCheckCharges is used like this
        'If set to true - oPayment.MON_ChargeMeDomestic must be True in the oPayment
        '   which is passed to this function , and oPayment.MON_ChargeMeAbroad to false.
        'If set to false - No test is done but oPayment.MON_ChargeMeDomestic is set to True
        '   in this function and oPayment.MON_ChargeMeAbroad is set to false.

        Dim sCountryCodeDebitAccount As String
        Dim sCountryCodeCreditAccount As String = ""
        Dim bx As Boolean
        Dim bReturnValue As Boolean = True

        ' New tests 31.03.05, because we may lack profile
        If EmptyString(sSWIFTAdressDebitAccount) Then
            If Not oPayment.VB_Profile Is Nothing Then
                bx = FindAccSWIFTAddr(oPayment.I_Account, oPayment.VB_Profile, oPayment.VB_FilenameOut_ID, sSWIFTAdressDebitAccount, False)
            End If
        End If
        sCountryCodeDebitAccount = Mid$(sSWIFTAdressDebitAccount, 5, 2)
        oPayment.BANK_SWIFTCode = Trim$(oPayment.BANK_SWIFTCode)

        ' For CrossBorder payments only
        If oPayment.PayType = "I" Then

            If Not IsSEPACountry(sCountryCodeDebitAccount) Then
                bReturnValue = False
            End If

            'Find receivers countrycode
            If IsIBANNumber(oPayment.E_Account, False, False) <> Scripting.Tristate.TristateFalse Then
                sCountryCodeCreditAccount = oPayment.E_Account.Substring(0, 2)
            Else
                ' if no IBAN, then return false
                bReturnValue = False
            End If

            If Not IsSEPACountry(sCountryCodeCreditAccount) Then
                bReturnValue = False
            End If

            IsEU_EEAPayment = bReturnValue

        Else
            IsEU_EEAPayment = False
        End If


        If bCheckCharges Then
            If Not oPayment.MON_ChargeMeDomestic Then
                IsEU_EEAPayment = False
                Exit Function
            End If
            If oPayment.MON_ChargeMeAbroad Then
                IsEU_EEAPayment = False
                Exit Function
            End If
        End If


    End Function
    Public Function GetQuickPostingAccounts(ByRef iCompanyID As Short, ByRef iClientID As Short, Optional ByRef bOnlyObservationAccount As Boolean = False) As String(,)
        'aGetQuickPostingAccounts consists of:
        '0 = Name
        '1 = Pattern
        '2 = Type
        '3 = Text
        '4 = Function
        '5 = VATCode
        ' 29.07.2015 added Dim1-Dim10
        '6 - 15 =Dim1-10

        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim aLocalGetQuickPostingAccounts(,) As String = Nothing
        Dim iCounter As Integer = -1
        Dim bAddToArray As Boolean

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            '22.06.2009 - Added order by. Should not influence existing customers because there were no order by before
            '  and hopefully they will be picked by DiffAccount_ID anyway
            'sMySQL = "SELECT * FROM MATCH_DiffAccounts WHERE Company_ID = " & iCompanyID.ToString & " And Client_ID = " & iClientID.ToString & " ORDER BY DiffAccount_ID"
            ' 15.11.2017 - changed sort. Either sort alphabetically, or sort on new column SortOrder
            sMySQL = "SELECT * FROM MATCH_DiffAccounts WHERE Company_ID = " & iCompanyID.ToString & " And Client_ID = " & iClientID.ToString & " ORDER BY SortOrder, Name"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    If bOnlyObservationAccount Then
                        If oMyDal.Reader_GetString("Function") = "NOMATCH" Then
                            bAddToArray = True
                        Else
                            bAddToArray = False
                        End If
                    Else
                        If Val(oMyDal.Reader_GetString("Type")) = TypeDontPostSeparately Then
                            bAddToArray = False
                        Else
                            bAddToArray = True
                        End If
                    End If
                    If bAddToArray Then
                        iCounter = iCounter + 1
                        ReDim Preserve aLocalGetQuickPostingAccounts(15, iCounter)
                        aLocalGetQuickPostingAccounts(0, iCounter) = oMyDal.Reader_GetString("Name")
                        aLocalGetQuickPostingAccounts(1, iCounter) = oMyDal.Reader_GetString("Pattern")
                        aLocalGetQuickPostingAccounts(2, iCounter) = oMyDal.Reader_GetString("Type")
                        aLocalGetQuickPostingAccounts(3, iCounter) = oMyDal.Reader_GetString("Txt")
                        aLocalGetQuickPostingAccounts(4, iCounter) = oMyDal.Reader_GetString("Function")
                        aLocalGetQuickPostingAccounts(5, iCounter) = oMyDal.Reader_GetString("VATCode")
                        aLocalGetQuickPostingAccounts(6, iCounter) = oMyDal.Reader_GetString("Dim1")
                        aLocalGetQuickPostingAccounts(7, iCounter) = oMyDal.Reader_GetString("Dim2")
                        aLocalGetQuickPostingAccounts(8, iCounter) = oMyDal.Reader_GetString("Dim3")
                        aLocalGetQuickPostingAccounts(9, iCounter) = oMyDal.Reader_GetString("Dim4")
                        aLocalGetQuickPostingAccounts(10, iCounter) = oMyDal.Reader_GetString("Dim5")
                        aLocalGetQuickPostingAccounts(11, iCounter) = oMyDal.Reader_GetString("Dim6")
                        aLocalGetQuickPostingAccounts(12, iCounter) = oMyDal.Reader_GetString("Dim7")
                        aLocalGetQuickPostingAccounts(13, iCounter) = oMyDal.Reader_GetString("Dim8")
                        aLocalGetQuickPostingAccounts(14, iCounter) = oMyDal.Reader_GetString("Dim9")
                        aLocalGetQuickPostingAccounts(15, iCounter) = oMyDal.Reader_GetString("Dim10")
                    End If

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetQuickPostingsAccounts" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aLocalGetQuickPostingAccounts

    End Function

    Public Function ReplaceTextString(ByRef sString As String, ByRef oBatch As vbBabel.Batch, ByRef oPayment As vbBabel.Payment) As String
        'Legal variables
        'Payers name - BB_Name
        'Batch reference - BB_BatchReference
        'Payment reference - BB_Reference
        'Message from payer - BB_Freetext - Not implemented

        On Error GoTo ERR_ReplaceTextString

        If InStr(1, sString, "BB_NAME", CompareMethod.Text) > 0 Then
            sString = Replace(sString, "BB_NAME", Trim(oPayment.E_Name), , , CompareMethod.Text)
        End If
        If InStr(1, sString, "BB_BATCHREFERENCE", CompareMethod.Text) > 0 Then
            If Not EmptyString((oPayment.REF_Bank1)) Then
                sString = Replace(sString, "BB_BATCHREFERENCE", Trim(oPayment.REF_Bank1), , , CompareMethod.Text)
            Else
                sString = Replace(sString, "BB_BATCHREFERENCE", Trim(oPayment.REF_Bank2), , , CompareMethod.Text)
            End If
        End If
        If InStr(1, sString, "BB_REFERENCE", CompareMethod.Text) > 0 Then
            sString = Replace(sString, "BB_REFERENCE", Trim(oBatch.REF_Bank), , , CompareMethod.Text)
        End If
        If InStr(1, sString, "BB_DATE", CompareMethod.Text) > 0 Then
            sString = Replace(sString, "BB_DATE", DateToString(CDate(oPayment.DATE_Value)), , , CompareMethod.Text)
        End If

        ReplaceTextString = sString

        On Error GoTo 0
        Exit Function

ERR_ReplaceTextString:

        Err.Raise(Err.Number, "ReplaceTextString", Err.Description)

    End Function

    Public Function GetNoMatchAccount(ByRef iCompanyNo As Integer, ByRef iClient_ID As Integer, Optional ByRef sDim1 As String = "", Optional ByRef sDim2 As String = "", Optional ByRef sDim3 As String = "", Optional ByRef sDim4 As String = "", Optional ByRef sDim5 As String = "", Optional ByRef sDim6 As String = "", Optional ByRef sDim7 As String = "", Optional ByRef sDim8 As String = "", Optional ByRef sDim9 As String = "", Optional ByRef sDim10 As String = "") As String
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iCounter As Short = 0
        Dim sReturnValue As String = ""

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT Pattern, Dim1, Dim2, Dim3, Dim4, Dim5, Dim6, Dim7, Dim8, Dim9, Dim10 FROM Match_DiffAccounts WHERE Company_ID = "
            sMySQL = sMySQL & iCompanyNo.ToString & " AND Client_ID = "
            sMySQL = sMySQL & iClient_ID.ToString & " AND [Function] = 'NOMATCH'"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    iCounter = iCounter + 1

                    sReturnValue = Trim(oMyDal.Reader_GetString("Pattern"))
                    sDim1 = oMyDal.Reader_GetString("Dim1")
                    sDim2 = oMyDal.Reader_GetString("Dim2")
                    sDim3 = oMyDal.Reader_GetString("Dim3")
                    sDim4 = oMyDal.Reader_GetString("Dim4")
                    sDim5 = oMyDal.Reader_GetString("Dim5")
                    sDim6 = oMyDal.Reader_GetString("Dim6")
                    sDim7 = oMyDal.Reader_GetString("Dim7")
                    sDim8 = oMyDal.Reader_GetString("Dim8")
                    sDim9 = oMyDal.Reader_GetString("Dim9")
                    sDim10 = oMyDal.Reader_GetString("Dim10")

                Loop

                If iCounter = 0 Then
                    Throw New Exception(LRSCommon(45002) & vbCrLf & LRS(15704))
                    '15704: No observationaccount is stated. Enter an observationaccount in Client-setup.
                ElseIf iCounter = 1 Then
                    'OK
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & LRS(15705))
                    '15705: More than one observationaccount is stated. Delete the invalid observationaccount(s) in Client-setup.
                End If

            Else

                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetNoMatchAccount" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnValue

    End Function
    Public Function GetNoMatchAccountType(ByRef iCompanyNo As Integer, ByRef iClient_ID As Integer) As Integer
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iCounter As Short = 0
        Dim sReturnValue As String = ""

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT Type FROM Match_DiffAccounts WHERE Company_ID = "
            sMySQL = sMySQL & iCompanyNo.ToString & " AND Client_ID = "
            sMySQL = sMySQL & iClient_ID.ToString & " AND [Function] = 'NOMATCH'"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    iCounter = iCounter + 1

                    sReturnValue = oMyDal.Reader_GetString("Type")

                Loop

                If iCounter = 0 Then
                    Throw New Exception(LRSCommon(45002) & vbCrLf & LRS(15704))
                    '15704: No observationaccount is stated. Enter an observationaccount in Client-setup.
                ElseIf iCounter = 1 Then
                    'OK
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & LRS(15705))
                    '15705: More than one observationaccount is stated. Delete the invalid observationaccount(s) in Client-setup.
                End If

            Else

                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetNoMatchAccount" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnValue

    End Function
    Public Function GetDiscountAccount(ByRef lCompanyNo As Integer, ByRef lClient_ID As Integer) As String

        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String = ""
        Dim sReturnValue As String = ""
        Dim iCounter As Integer = 0

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT Pattern FROM Match_DiffAccounts WHERE Company_ID = "
            sMySQL = sMySQL & Trim(Str(lCompanyNo)) & " AND Client_ID = "
            sMySQL = sMySQL & Trim(Str(lClient_ID)) & " AND [Function] = 'CASHDISCOUNT'"

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    sReturnValue = oMyDal.Reader_GetString("Pattern")
                    iCounter = iCounter + 1
                    If iCounter > 1 Then
                        Throw New System.Exception(LRSCommon(20050))
                        Exit Do
                    End If
                Loop

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Return ""

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnValue



    End Function
    Public Function GetDiscountAccountType(ByRef lCompanyNo As Integer, ByRef lClient_ID As Integer) As Integer

        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String
        Dim iReturnValue As Integer = 0

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If


            sMySQL = "SELECT Type FROM Match_DiffAccounts WHERE Company_ID = "
            sMySQL = sMySQL & Trim(Str(lCompanyNo)) & " AND Client_ID = "
            sMySQL = sMySQL & Trim(Str(lClient_ID)) & " AND [Function] = 'CASHDISCOUNT'"

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then


                Do While oMyDal.Reader_ReadRecord

                    iReturnValue = Val(oMyDal.Reader_GetString("Type"))

                Loop

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Return 0

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return iReturnValue

    End Function
    Public Function GetDiscountAccountText(ByRef lCompanyNo As Integer, ByRef lClient_ID As Integer) As String

        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String = ""
        Dim sReturnValue As String = ""

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT Txt FROM Match_DiffAccounts WHERE Company_ID = "
            sMySQL = sMySQL & Trim(Str(lCompanyNo)) & " AND Client_ID = "
            sMySQL = sMySQL & Trim(Str(lClient_ID)) & " AND [Function] = 'CASHDISCOUNT'"

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    sReturnValue = oMyDal.Reader_GetString("Txt")

                Loop

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            sReturnValue = ""

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnValue

    End Function
    'XNET - 22.10.2012 - New function
    Public Function GetAccountFromName(ByVal lCompanyNo As Long, ByVal lClient_ID As Long, ByVal sAccountName As String, Optional ByRef sDim1 As String = "", Optional ByRef sDim2 As String = "", Optional ByRef sDim3 As String = "", Optional ByRef sDim4 As String = "", Optional ByRef sDim5 As String = "", Optional ByRef sDim6 As String = "", Optional ByRef sDim7 As String = "", Optional ByRef sDim8 As String = "", Optional ByRef sDim9 As String = "", Optional ByRef sDim10 As String = "") As String

        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String = ""
        Dim sReturnValue As String = ""
        Dim iCounter As Integer = 0

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT Pattern, Dim1, Dim2, Dim3, Dim4, Dim5, Dim6, Dim7, Dim8, Dim9, Dim10 FROM Match_DiffAccounts WHERE Company_ID = "
            sMySQL = sMySQL & Trim$(Str(lCompanyNo)) & " AND Client_ID = "
            sMySQL = sMySQL & Trim$(Str(lClient_ID)) & " AND ucase(Name) = '" & sAccountName & "'"

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord

                        iCounter = iCounter + 1

                        If iCounter = 1 Then
                            sReturnValue = oMyDal.Reader_GetString("Pattern")
                            sDim1 = oMyDal.Reader_GetString("Dim1")
                            sDim2 = oMyDal.Reader_GetString("Dim2")
                            sDim3 = oMyDal.Reader_GetString("Dim3")
                            sDim4 = oMyDal.Reader_GetString("Dim4")
                            sDim5 = oMyDal.Reader_GetString("Dim5")
                            sDim6 = oMyDal.Reader_GetString("Dim6")
                            sDim7 = oMyDal.Reader_GetString("Dim7")
                            sDim8 = oMyDal.Reader_GetString("Dim8")
                            sDim9 = oMyDal.Reader_GetString("Dim9")
                            sDim10 = oMyDal.Reader_GetString("Dim10")

                        Else
                            Err.Raise(15705, "GetAccountFromName", LRS(15705))
                        End If

                    Loop
                Else
                    Err.Raise(15704, "GetAccountFromName", LRS(15706))
                End If

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            sReturnValue = ""

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnValue

    End Function
    Public Function GetSwapDiffAccountFromName(ByVal lCompanyNo As Long, ByVal sAccountName As String) As String
        ' 01.12.2017 - Find a Swap DiffAccount from Name of Swapsetup
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String = ""
        Dim sReturnValue As String = ""
        Dim iCounter As Integer = 0

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT *  FROM SwapFiles WHERE Company_ID = " & Trim$(Str(lCompanyNo))
            sMySQL = sMySQL & " AND UCASE(Name) = '" & sAccountName & "'"

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord

                        iCounter = iCounter + 1

                        If iCounter = 1 Then
                            sReturnValue = oMyDal.Reader_GetString("DiffAccount")
                        Else
                            Err.Raise(15705, "GetSwapDiffAccountFromName", LRS(15705))
                        End If

                    Loop
                Else
                    'Err.Raise(15704, "GetSwapDiffAccountFromName", LRS(15704))
                    ' If we have no swap-account, just continue
                    sReturnValue = ""
                End If

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            sReturnValue = ""

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnValue

    End Function
    Public Function CheckIfPaymentIsOpenInERP(ByVal oPayment As vbBabel.Payment, ByVal oClient As vbBabel.Client, ByVal sCheckERPSQL As String, ByVal bCheckIfPaymentIsOpenInERP As Boolean, Optional ByVal bCalledFromFormMatch_Manual As Boolean = True, Optional ByVal sUnique_ERPID As String = vbNullString, Optional ByVal nMON_InvoiceAmount As Double = 0) As Boolean
        'New code 18.07.2007 because of changes in frmStoredPayments
        'Old code
        'Public Function CheckIfPaymentIsOpenInERP(oPayment As vbbabel.Payment, oClient As vbbabel.Client, sCheckERPSQL As String, bCheckIfPaymentIsOpenInERP As Boolean, Optional bCalledFromFormMatch_Manual = True) As Boolean

        ' XNET 07.02.2012 - her har det v�rt store endringer - uten XNETTING, og
        ' sannsynligvis flytting fra Start.bas.
        'VI M� SJEKKE DENNE, OG KANSKJE KOPIERE HELE DRITTEN; MEN DEN ER DALLET !!!

        Dim oMyDal As vbBabel.DAL
        Dim sLocalSQL As String
        Dim iCounter As Integer
        Dim bReturnValue As Boolean
        Dim bErrorInsideLoadSQLs As Boolean
        Dim sTmp As String = ""

        bErrorInsideLoadSQLs = False

        Try

            'New code 18.07.2007 because of changes in frmStoredPayments
            If sUnique_ERPID = vbNullString Then
                'XokNET 26.08.2011 Changed the next line
                sUnique_ERPID = Trim(oPayment.Unique_ERPID)
                'sUnique_ERPID = oPayment.Unique_ERPID
                nMON_InvoiceAmount = oPayment.MON_InvoiceAmount
            End If

            If bCheckIfPaymentIsOpenInERP Then
                'Check if a Unique_ERPID is stated, if not return false
                'XokNET 26.08.2011 Changed the next line
                If Not EmptyString(sUnique_ERPID) Then
                    'If Not EmptyString(oPayment.Unique_ERPID) Then
                    'Temp code, first part of IF-statement
                    oMyDal = New vbBabel.DAL
                    oMyDal.Company_ID = 1 'TOD: Hardcoded CompanyNo
                    oMyDal.DBProfile_ID = 1 'TOD: Hardcoded DBProfile_ID
                    oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                    If Not oMyDal.ConnectToDB() Then
                        Throw New System.Exception(oMyDal.ErrorMessage)
                    End If

                    bErrorInsideLoadSQLs = True
                    'If bCalledFromFormMatch_Manual Then
                    bReturnValue = True 'frmMATCH_Manual.LoadSQLs(oPayment.I_Account)

                    bErrorInsideLoadSQLs = False

                    'XokNET 26.08.2011 Changed the next line
                    sLocalSQL = Replace(sCheckERPSQL, "BB_ERPID", sUnique_ERPID, , , vbTextCompare)
                    sLocalSQL = Replace(sLocalSQL, "BB_ClientNo", oClient.ClientNo, , , vbTextCompare)
                    sLocalSQL = Replace(sLocalSQL, "BB_ClientName", Trim$(oClient.Name), , , vbTextCompare)

                    oMyDal.SQL = sLocalSQL
                    If oMyDal.Reader_Execute() Then

                        Do While oMyDal.Reader_ReadRecord
                            iCounter = iCounter + 1

                            If InStr(1, sCheckERPSQL, "BBRET_Open", CompareMethod.Text) > 0 Then
                                If oMyDal.Reader_GetString("BBRET_Open").ToUpper = "FALSE" Then
                                    bReturnValue = False
                                End If
                            End If

                            If InStr(1, sCheckERPSQL, "BBRET_Amount", CompareMethod.Text) > 0 Then
                                If Not IsEqualAmount(Val(oMyDal.Reader_GetString("BBRET_Amount")), nMON_InvoiceAmount) Then
                                    bReturnValue = False
                                End If
                            End If

                            If InStr(1, sCheckERPSQL, "BBRET_MATCHID", CompareMethod.Text) > 0 Then
                                sTmp = oMyDal.Reader_GetString("BBRET_MatchID")
                            End If

                        Loop

                        If iCounter <> 1 Then
                            bReturnValue = False 'Not exactly 1 row
                        End If

                        'If an matching-ID against the observation account is retrieved, store
                        '   the ID in the Payment-object.
                        If bReturnValue Then
                            If InStr(1, sCheckERPSQL, "BBRET_MATCHID", CompareMethod.Text) Then
                                'New code 18.07.2007 because of changes in frmStoredPayments
                                If Not oPayment Is Nothing Then
                                    oPayment.MATCH_MatchingIDAgainstObservationAccount = sTmp
                                End If
                            End If
                        End If

                    End If

                Else
                    bReturnValue = False
                End If
            Else
                bReturnValue = True
            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            If bErrorInsideLoadSQLs Then
                bReturnValue = False
                Return False
            End If

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function
    'Public Function CheckIfPaymentIsOpenInERP_Old(ByRef oPayment As vbBabel.Payment, ByRef oClient As vbBabel.Client, ByRef sCheckERPSQL As String, ByRef bCheckIfPaymentIsOpenInERP As Boolean, Optional ByRef bCalledFromFormMatch_Manual As Boolean = True, Optional ByRef sUnique_ERPID As String = "", Optional ByRef nMON_InvoiceAmount As Double = 0) As Boolean
    '    'TODO - THe use of MyDal is NOT TESTED YET 
    '    'New code 18.07.2007 because of changes in frmStoredPayments
    '    'Old code
    '    'Public Function CheckIfPaymentIsOpenInERP(oPayment As vbbabel.Payment, oClient As vbbabel.Client, sCheckERPSQL As String, bCheckIfPaymentIsOpenInERP As Boolean, Optional bCalledFromFormMatch_Manual = True) As Boolean

    '    Dim oMyDal As vbBabel.DAL
    '    Dim sMySQL As String
    '    Dim bReturnValue As Boolean = False
    '    Dim bErrorInsideLoadSQLs As Boolean = False
    '    Dim iCounter As Integer = 0

    '    Try

    '        'New code 18.07.2007 because of changes in frmStoredPayments
    '        If sUnique_ERPID = "" Then
    '            sUnique_ERPID = Trim(oPayment.Unique_ERPID)
    '            nMON_InvoiceAmount = oPayment.MON_InvoiceAmount
    '        End If

    '        If bCheckIfPaymentIsOpenInERP Then

    '            'Check if a Unique_ERPID is stated, if not return false
    '            If Not EmptyString(sUnique_ERPID) Then

    '                oMyDal = New vbBabel.DAL
    '                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
    '                If Not oMyDal.ConnectToDB() Then
    '                    Throw New System.Exception(oMyDal.ErrorMessage)
    '                End If

    '                bReturnValue = True 'frmMATCH_Manual.LoadSQLs(oPayment.I_Account)

    '                sMySQL = Replace(sCheckERPSQL, "BB_ERPID", sUnique_ERPID, , , CompareMethod.Text)
    '                sMySQL = Replace(sMySQL, "BB_ClientNo", oClient.ClientNo, , , CompareMethod.Text)
    '                'BB_ClientName - Added 19.11.2007
    '                sMySQL = Replace(sMySQL, "BB_ClientName", Trim(oClient.Name), , , CompareMethod.Text)

    '                oMyDal.SQL = sMySQL
    '                If oMyDal.Reader_Execute() Then

    '                    Do While oMyDal.Reader_ReadRecord

    '                        iCounter = iCounter + 1

    '                        If InStr(1, sCheckERPSQL, "BBRET_Open", CompareMethod.Text) > 0 Then
    '                            If oMyDal.Reader_GetString("BBRET_Open").ToUpper = "FALSE" Then
    '                                bReturnValue = False
    '                            End If
    '                        End If

    '                        If InStr(1, sCheckERPSQL, "BBRET_Amount", CompareMethod.Text) > 0 Then
    '                            If Not IsEqualAmount(Val(oMyDal.Reader_GetString("BBRET_Amount")), nMON_InvoiceAmount) Then
    '                                bReturnValue = False
    '                            End If
    '                        End If

    '                        'If an matching-ID against the observation account is retrieved, store
    '                        '   the ID in the Payment-object.
    '                        If bReturnValue Then
    '                            If InStr(1, sCheckERPSQL, "BBRET_MATCHID", CompareMethod.Text) Then
    '                                'New code 18.07.2007 because of changes in frmStoredPayments
    '                                If Not oPayment Is Nothing Then
    '                                    oPayment.MATCH_MatchingIDAgainstObservationAccount = oMyDal.Reader_GetString("BBRET_MatchID", True)
    '                                End If
    '                            End If
    '                        End If

    '                    Loop

    '                End If

    '            Else
    '                bReturnValue = False
    '            End If

    '        Else
    '            bReturnValue = True
    '        End If

    '    Catch ex As Exception

    '        If Not oMyDal Is Nothing Then
    '            oMyDal.Close()
    '            oMyDal = Nothing
    '        End If

    '        If bErrorInsideLoadSQLs Then
    '            Return False
    '        End If

    '    End Try

    '    If Not oMyDal Is Nothing Then
    '        oMyDal.Close()
    '        oMyDal = Nothing
    '    End If

    '    Return bReturnValue

    'End Function


    Public Function RemoveCharacters(ByRef sString As String, ByRef sCharactersToRemove As String, Optional ByVal bAllCharacters As Boolean = False) As String
        Dim sNewString As String
        Dim lCounter As Integer

        If bAllCharacters Then
            sCharactersToRemove = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        End If

        sNewString = sString

        For lCounter = 1 To Len(sCharactersToRemove)
            sNewString = Replace(sNewString, Mid(sCharactersToRemove, lCounter, 1), "", , , CompareMethod.Text)
        Next lCounter

        RemoveCharacters = sNewString

    End Function
    'XNET - 21.12.2010 - Added an optional parameter - Whole function!
    Public Function CheckForValidCharacters(ByVal sString As String, ByVal bAllowSmallChar As Boolean, ByVal bAllowCapitalizedChar As Boolean, ByVal bAllowNorwegianChar As Boolean, ByVal bAllowDigits As Boolean, ByVal sOtherValidCharacters As String, Optional ByVal bAddAnEOnSpecialConversion As Boolean = False, Optional ByRef sIllegalCharactersReplaced As String = "", Optional ByRef bTransformXMLCharacters As Boolean = False) As String
        'Invalid characters will either be changed or they will be removed
        ' Parmameters
        ' - sString:    Initial string
        ' -
        ' - sIllegalCharactersReplaced: Returns those characters that are illegal
        Dim sNewString As String = ""
        Dim lCharacter As Long
        Dim sCharacter As String = ""
        Dim bOK As Boolean
        Dim lCounter As Long, lCounter2 As Long

        sNewString = ""

        For lCounter = 1 To Len(sString)
            bOK = False
            Do While True
                sCharacter = Mid$(sString, lCounter, 1)
                lCharacter = Asc(sCharacter)
                If bAllowSmallChar Then
                    If lCharacter > 96 And lCharacter < 123 Then
                        bOK = True
                        Exit Do
                    End If
                End If
                If bAllowCapitalizedChar Then
                    If lCharacter > 64 And lCharacter < 91 Then
                        bOK = True
                        Exit Do
                    End If
                End If
                If bAllowDigits Then
                    If lCharacter > 47 And lCharacter < 58 Then
                        bOK = True
                        Exit Do
                    End If
                End If
                If bAllowNorwegianChar Then
                    If lCharacter = 230 Then '�
                        bOK = True
                        Exit Do
                    ElseIf lCharacter = 248 Then '�
                        bOK = True
                        Exit Do
                    ElseIf lCharacter = 229 Then '�
                        bOK = True
                        Exit Do
                    ElseIf lCharacter = 198 Then '�
                        bOK = True
                        Exit Do
                    ElseIf lCharacter = 216 Then '�
                        bOK = True
                        Exit Do
                    ElseIf lCharacter = 197 Then '�
                        bOK = True
                        Exit Do
                    End If
                End If
                If sCharacter = " " Then
                    bOK = True
                    Exit Do
                End If
                For lCounter2 = 1 To Len(sOtherValidCharacters)
                    If sCharacter = Mid$(sOtherValidCharacters, lCounter2, 1) Then
                        bOK = True
                        Exit Do
                    End If
                Next lCounter2
                Exit Do
            Loop
            If bOK Then
                sNewString = sNewString & sCharacter
            Else
                If bTransformXMLCharacters Then
                    If lCharacter = 38 Then '&
                        sNewString = sNewString & "&amp;"
                        bOK = True
                    ElseIf lCharacter = 60 Then '<
                        sNewString = sNewString & "&lt;"
                        bOK = True
                    ElseIf lCharacter = 62 Then '>
                        sNewString = sNewString & "&gt;"
                        bOK = True
                    ElseIf lCharacter = 34 Then '"
                        sNewString = sNewString & "&quot;"
                        bOK = True
                    ElseIf lCharacter = 39 Then ''
                        sNewString = sNewString & "&apos;"
                        bOK = True
                    End If
                End If

                If Not bOK Then
                    'Some characters will be changed
                    If lCharacter = 230 Then '�
                        sNewString = sNewString & "a"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter = 248 Then '�
                        sNewString = sNewString & "o"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter = 229 Then '�
                        sNewString = sNewString & "a"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter = 198 Then '�
                        'sNewString = sNewString & "AE"
                        ' 01.07.2015 Changed to return one character only - not to tamper with maxlength of fields
                        sNewString = sNewString & "A"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter = 216 Then '�
                        ' 01.07.2015 Changed to return one character only - not to tamper with maxlength of fields
                        'sNewString = sNewString & "OE"
                        sNewString = sNewString & "O"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter = 197 Then '�
                        sNewString = sNewString & "A"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter > 191 And lCharacter < 197 Then 'An A with some shit over
                        If bAddAnEOnSpecialConversion Then
                            If lCharacter > 194 And lCharacter < 197 Then
                                ' 01.07.2015 Changed to return one character only - not to tamper with maxlength of fields
                                'sNewString = sNewString & "AE"
                                sNewString = sNewString & "A"
                                sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                            Else
                                sNewString = sNewString & "A"
                                sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                            End If
                        Else
                            sNewString = sNewString & "A"
                            sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        End If
                    ElseIf lCharacter > 199 And lCharacter < 204 Then 'An E with some shit over
                        sNewString = sNewString & "E"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter > 203 And lCharacter < 208 Then 'An I with some shit over
                        sNewString = sNewString & "I"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter > 209 And lCharacter < 215 Then 'An O with some shit over
                        If bAddAnEOnSpecialConversion Then
                            If lCharacter > 212 And lCharacter < 215 Then
                                ' 01.07.2015 Changed to return one character only - not to tamper with maxlength of fields
                                'sNewString = sNewString & "OE"
                                sNewString = sNewString & "O"
                                sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                            Else
                                sNewString = sNewString & "O"
                                sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                            End If
                        Else
                            sNewString = sNewString & "O"
                            sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        End If
                    ElseIf lCharacter > 216 And lCharacter < 221 Then 'An U with some shit over
                        If bAddAnEOnSpecialConversion Then
                            If lCharacter = 220 Then
                                ' 01.07.2015 Changed to return one character only - not to tamper with maxlength of fields
                                'sNewString = sNewString & "UE"
                                sNewString = sNewString & "U"
                                sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                            Else
                                sNewString = sNewString & "U"
                                sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                            End If
                        Else
                            sNewString = sNewString & "U"
                            sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        End If
                    ElseIf lCharacter = 221 Then 'An �
                        sNewString = sNewString & "Y"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter > 223 And lCharacter < 229 Then 'An a with some shit over
                        sNewString = sNewString & "a"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter > 231 And lCharacter < 236 Then 'An e with some shit over
                        sNewString = sNewString & "e"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter > 235 And lCharacter < 240 Then 'An i with some shit over
                        sNewString = sNewString & "i"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter > 241 And lCharacter < 247 Then 'An o with some shit over
                        sNewString = sNewString & "o"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter > 248 And lCharacter < 253 Then 'An u with some shit over
                        sNewString = sNewString & "u"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    ElseIf lCharacter = 253 Then 'An � with some shit over
                        sNewString = sNewString & "y"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        ' 13.02.2012 added test for new characters, from here 

                        '138    �
                    ElseIf lCharacter = 138 Then
                        sNewString = sNewString & "S"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        '140    �
                    ElseIf lCharacter = 140 Then
                        sNewString = sNewString & "E"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        '142    �
                    ElseIf lCharacter = 142 Then
                        sNewString = sNewString & "Z"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        '154    �
                    ElseIf lCharacter = 154 Then
                        sNewString = sNewString & "s"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        '156    �
                    ElseIf lCharacter = 156 Then
                        sNewString = sNewString & "e"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        '158    �
                    ElseIf lCharacter = 158 Then
                        sNewString = sNewString & "z"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        '159    �
                    ElseIf lCharacter = 159 Then
                        sNewString = sNewString & "Y"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        '199    �
                    ElseIf lCharacter = 199 Then
                        sNewString = sNewString & "C"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        '209    �
                    ElseIf lCharacter = 209 Then
                        sNewString = sNewString & "N"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        '231    �
                    ElseIf lCharacter = 231 Then
                        sNewString = sNewString & "c"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        '241    �
                    ElseIf lCharacter = 241 Then
                        sNewString = sNewString & "n"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                        '255    �
                    ElseIf lCharacter = 255 Then
                        sNewString = sNewString & "y"
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)

                    Else
                        'Nothing to do, the character will be replaced with blank
                        sNewString = sNewString & " "
                        sIllegalCharactersReplaced = sIllegalCharactersReplaced & Chr(lCharacter)
                    End If
                End If
            End If
        Next lCounter

        CheckForValidCharacters = sNewString.Trim


    End Function
    Public Function IsPaymentLocked(ByRef lBabelFileIndex As Integer, ByRef lBatchIndex As Integer, ByRef lPaymentIndex As Integer, ByRef bManMatchFromERP As Boolean) As Boolean
        ' Check in table LockedPayments if current payment is in use
        ' Table LockedPayments is indexed unique on Company_ID+BabelFileIndex+BatchIndex+PaymentIndex
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True
        Dim sMachineID As String
        Dim sUserID As String
        Dim sCompanyID As String

        Try

            ' Brukes (forel�pig) kun ved lagring av BB-poster til ERP
            'If bManMatchFromERP Then

            sCompanyID = "1"
            sMachineID = "0"
            sUserID = Environ("Username")

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT BBUserID FROM LockedPayments WHERE "
            sMySQL = sMySQL & "Company_ID=" & sCompanyID & " AND "
            sMySQL = sMySQL & "BabelFileIndex=" & lBabelFileIndex.ToString & " AND "
            sMySQL = sMySQL & "BatchIndex=" & lBatchIndex.ToString & " AND "
            sMySQL = sMySQL & "PaymentIndex=" & lPaymentIndex.ToString ' & " AND "
            'sMySQL = sMySQL & "BBUserID<>'" & sUserID & "'"
            oMyDal.SQL = sMySQL

            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    If oMyDal.Reader_ReadRecord() Then
                        If oMyDal.Reader_GetString("BBUserID") = sUserID Then
                            bReturnValue = False ' Locked by the same user as the entering the post! That's OK.
                        Else
                            bReturnValue = True ' Locked by other user!
                        End If
                    End If
                Else
                    sMySQL = "INSERT INTO LockedPayments (Company_ID, BabelFileIndex, BatchIndex, PaymentIndex, MachineID, BBUserID, InDate, InTime) "
                    sMySQL = sMySQL & " VALUES("
                    sMySQL = sMySQL & sCompanyID
                    sMySQL = sMySQL & " ," & Trim(Str(lBabelFileIndex))
                    sMySQL = sMySQL & " ," & Trim(Str(lBatchIndex))
                    sMySQL = sMySQL & " ," & Trim(Str(lPaymentIndex))
                    sMySQL = sMySQL & " ,'" & sMachineID
                    sMySQL = sMySQL & "' ,'" & sUserID
                    sMySQL = sMySQL & "' ,'" & LTrim(Str(Year(Now))) & PadLeft(LTrim(Str(Month(Now))), 2, "0") & PadLeft(LTrim(Str(VB.Day(Now))), 2, "0")
                    sMySQL = sMySQL & "' ,'" & LTrim(VB6.Format(TimeOfDay, "HH:MM:SS"))
                    sMySQL = sMySQL & "')"

                    oMyDal.SQL = sMySQL

                    If oMyDal.ExecuteNonQuery Then
                        bReturnValue = False
                    End If

                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If




            '22.07.2014 - Old code
            'sMySQL = "INSERT INTO LockedPayments (Company_ID, BabelFileIndex, BatchIndex, PaymentIndex, MachineID, BBUserID, InDate, InTime) "
            'sMySQL = sMySQL & " VALUES("
            'sMySQL = sMySQL & sCompanyID
            'sMySQL = sMySQL & " ," & Trim(Str(lBabelFileIndex))
            'sMySQL = sMySQL & " ," & Trim(Str(lBatchIndex))
            'sMySQL = sMySQL & " ," & Trim(Str(lPaymentIndex))
            'sMySQL = sMySQL & " ,'" & sMachineID
            'sMySQL = sMySQL & "' ,'" & sUserID
            'sMySQL = sMySQL & "' ,'" & LTrim(Str(Year(Now))) & PadLeft(LTrim(Str(Month(Now))), 2, "0") & PadLeft(LTrim(Str(VB.Day(Now))), 2, "0")
            'sMySQL = sMySQL & "' ,'" & LTrim(VB6.Format(TimeOfDay, "HH:MM:SS"))
            'sMySQL = sMySQL & "')"

            'oMyDal.SQL = sMySQL

            'If oMyDal.ExecuteNonQuery Then

            '    If oMyDal.RecordsAffected = 1 Then
            '        bReturnValue = False
            '    Else
            '        bReturnValue = True
            '        'Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
            '    End If

            'Else

            '    If oMyDal.ErrorNumber = -2147467259 Then
            '        bReturnValue = True
            '    Else
            '        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
            '    End If
            'End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: IsPaymentLocked" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function
    Public Function IsPaymentLockedByAnotherUser(ByRef lBabelFileIndex As Integer, ByRef lBatchIndex As Integer, ByRef lPaymentIndex As Integer, ByRef bManMatchFromERP As Boolean) As Boolean
        ' Check in table LockedPayments if current payment is in use by other user
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sCompanyID As String = ""
        Dim sUserID As String = ""
        Dim bReturnValue As Boolean = False


        Try
            ' Brukes (forel�pig) kun ved lagring av BB-poster til ERP
            sCompanyID = "1"
            sUserID = Environ("Username")

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT * FROM LockedPayments WHERE "
            sMySQL = sMySQL & "Company_ID=" & sCompanyID & " AND "
            sMySQL = sMySQL & "BabelFileIndex=" & lBabelFileIndex.ToString & " AND "
            sMySQL = sMySQL & "BatchIndex=" & lBatchIndex.ToString & " AND "
            sMySQL = sMySQL & "PaymentIndex=" & lPaymentIndex.ToString & " AND "
            sMySQL = sMySQL & "BBUserID<>'" & sUserID & "'"
            oMyDal.SQL = sMySQL

            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    ' Locked by other user!
                    bReturnValue = True
                Else
                    bReturnValue = False
                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: IsPaymentLockedBuAnotherUser" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function
    Public Function IsFunctionPrintOmittedInProgress(ByRef iCompany_ID As Short) As Boolean
        ' Check in table Company if another user has started the function
        '  to print omitted payments
        ' If so, return Yes.
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT OmittedReportInProgress FROM Company WHERE "
            sMySQL = sMySQL & "Company_ID = " & iCompany_ID.ToString
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    bReturnValue = CBool(oMyDal.Reader_GetString("OmittedReportInProgress"))

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: IsFunctionPrintOmittedInProgress" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function

    Function dbUpDayTotals(ByRef bNew As Boolean, ByRef sDate As String, ByRef dAmount As Double, ByRef sType As String, ByRef sAccount As String, Optional ByRef sCompanyID As String = "1") As Boolean

        ' Update table DayTotals
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim oMyDalNonQuery As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sMySQlNonQuery As String = ""
        Dim sMySQlNonQuery2 As String = ""
        Dim bAlreadyUpdated As Boolean = False
        Dim bZeroAmount As Boolean = True
        Dim iErrNo As Short = -1
        Dim iClient_ID As Short = -1
        Dim sAccountNo As String
        Dim sTemp As String
        Dim bShowMessage As Boolean = False

        Try

            If dAmount = 0 Then
                bZeroAmount = True
            Else
                bZeroAmount = False
            End If

            If Len(sDate) <> 8 Or Left(sDate, 2) <> "20" Then
                iErrNo = 15017 ' Wrong dateformat
                Throw New Exception(LRS(15017))
            End If

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            If bNew Then
                'Retrieve the accountno
                '24.02.2017 - For SG Finans, use ConvertedAccount (is stated) instead of Account
                sMySQL = "SELECT C.Client_ID, C.ClientNo, A.Account, A.ConvertedAccount FROM Client C, Account A WHERE C.Client_ID = A.Client_ID AND C.Company_ID = A.Company_ID AND "

                If sAccount = "" Then
                    'No specific client stated
                Else
                    'A specific client stated
                    sMySQL = sMySQL & "A.Account = '" & Trim(sAccount) & "' AND "
                End If

                sMySQL = sMySQL & "A.Company_ID = " & sCompanyID

                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then

                    'Initiate a second connection to the DB
                    oMyDalNonQuery = New vbBabel.DAL
                    oMyDalNonQuery.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                    If Not oMyDalNonQuery.ConnectToDB() Then
                        Throw New System.Exception(oMyDalNonQuery.ErrorMessage)
                    End If

                    Do While oMyDal.Reader_ReadRecord

                        iClient_ID = CShort(oMyDal.Reader_GetString("Client_ID"))
                        If iClient_ID < 1 Then
                            Throw New System.Exception(LRSCommon(45003)) '"Feil under uthenting av klient_ID.")
                        End If
                        If oMyDal.Reader_GetString("ClientNo") = "" Then
                            Throw New System.Exception(LRSCommon(45004)) '"Feil under uthenting av klientnummer.")
                        End If
                        '24.02.2017 - Added next line and If
                        sTemp = ""
                        sTemp = oMyDal.Reader_GetString("ConvertedAccount")
                        If EmptyString(sTemp) Then
                            sAccountNo = oMyDal.Reader_GetString("Account")
                        Else
                            sAccountNo = sTemp
                        End If
                        If sAccountNo = "" Then
                            Throw New System.Exception(LRSCommon(45005)) '"Feil under uthenting av kontonummer.")
                        End If

                        sMySQlNonQuery = "DELETE FROM DayTotals WHERE AccountNo = '" & sAccountNo & "'" 'SQQLSERVER, fjernet * fra DELETE * FROM

                        oMyDalNonQuery.SQL = sMySQlNonQuery

                        If oMyDalNonQuery.ExecuteNonQuery Then

                            'OK, doesn't matter if no lines are deleted (may be the first time it's run)

                        Else

                            Throw New Exception(LRSCommon(45002) & oMyDalNonQuery.ErrorMessage)

                        End If

                        sMySQlNonQuery = "INSERT INTO DayTotals(Company_ID, ProdDate, "
                        Select Case UCase(sType)
                            Case "IB"
                                If bZeroAmount Then 'Retrieve IB from DB
                                    dAmount = GetDBTotal(sCompanyID, sAccountNo)
                                End If
                                sMySQlNonQuery = sMySQlNonQuery & "IB"
                            Case "IMPORTED"
                                sMySQlNonQuery = sMySQlNonQuery & "Imported"
                            Case "OCR"
                                sMySQlNonQuery = sMySQlNonQuery & "Ocr"
                            Case "MATCHED_BB"
                                sMySQlNonQuery = sMySQlNonQuery & "Matched_BB"
                            Case "UNMATCHED_BB"
                                If bZeroAmount Then 'TODO dbUpdayTotals - Why retrieve the UB when we are going to report unmatched? 'Retrieve IB from DB
                                    dAmount = GetDBTotal(sCompanyID, sAccountNo)
                                End If
                                sMySQlNonQuery = sMySQlNonQuery & "UnMatched_BB"
                            Case "MATCHED_GL"
                                sMySQlNonQuery = sMySQlNonQuery & "Matched_GL"
                            Case Else
                                iErrNo = 15016 ' Wrong type
                                Err.Raise(15016, "dbUpDayTotals", LRS(15016))
                        End Select
                        'XNET - 09.03.2012 - Changed nest line
                        '04.08.2020 - Changed next line - Added Imported with the value 0
                        sMySQlNonQuery = sMySQlNonQuery & ", AccountNo, Client_ID, Imported, Matched_BB, Ocr, Matched_GL) VALUES( " & sCompanyID & ", '" & sDate & "', " & dAmount.ToString & ", '" & sAccountNo & "', " & iClient_ID.ToString & ", 0, 0, 0, 0)"

                        'Insert information about the new account in DayTotals
                        oMyDalNonQuery.SQL = sMySQlNonQuery

                        If oMyDalNonQuery.ExecuteNonQuery Then

                            If oMyDalNonQuery.RecordsAffected = 0 Then
                                ' Probably wrong date - no update done
                                iErrNo = 20025 ' No records updated (Wrong date?)
                                Throw New Exception(LRSCommon(20025) & vbCrLf & oMyDalNonQuery.ErrorMessage)
                            End If

                        Else

                            Throw New Exception(LRSCommon(45002) & oMyDalNonQuery.ErrorMessage)

                        End If

                    Loop

                Else

                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

                End If

            Else
                'Initiate a second connection to the BB DB used for nonquery statements
                oMyDalNonQuery = New vbBabel.DAL
                oMyDalNonQuery.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDalNonQuery.ConnectToDB() Then
                    Throw New System.Exception(oMyDalNonQuery.ErrorMessage)
                End If

                ' Update
                sMySQlNonQuery = "UPDATE DayTotals SET "
                Select Case UCase(sType)
                    Case "IB"
                        sMySQlNonQuery = sMySQlNonQuery & "IB = "
                    Case "IMPORTED"
                        sMySQlNonQuery = sMySQlNonQuery & "Imported = "
                    Case "OCR"
                        sMySQlNonQuery = sMySQlNonQuery & "Ocr = Ocr + " 'XNET - 09.03.2012
                    Case "MATCHED_BB"
                        sMySQlNonQuery = sMySQlNonQuery & "Matched_BB = Matched_BB + "
                    Case "UNMATCHED_BB"

                        'MsgBox("Kommet til UNMATCHED BB")

                        'Retrieve the accountno, 'Initiate a new connection
                        sMySQL = "SELECT C.Client_ID, C.ClientNo, A.Account, A.ConvertedAccount FROM Client C, Account A WHERE C.Client_ID = A.Client_ID AND C.Company_ID = A.Company_ID AND "

                        If sAccount = "" Then
                            'No specific account stated
                        Else
                            'A specific account stated
                            sMySQL = sMySQL & "A.Account = '" & Trim(sAccount) & "' AND "
                            sMySQL = sMySQL & "(A.Account = '" & Trim(sAccount) & "' OR A.ConvertedAccount = '" & Trim(sAccount) & ") AND "
                        End If

                        sMySQL = sMySQL & "A.Company_ID = " & sCompanyID

                        oMyDal.SQL = sMySQL

                        'MsgBox("Skal kj�re SQL: " & sMySQL)

                        If oMyDal.Reader_Execute() Then

                            Do While oMyDal.Reader_ReadRecord
                                iClient_ID = CShort(oMyDal.Reader_GetString("Client_ID"))
                                If iClient_ID < 1 Then
                                    Throw New System.Exception(LRSCommon(45003)) '"Feil under uthenting av klient_ID.")
                                End If
                                If oMyDal.Reader_GetString("ClientNo") = "" Then
                                    Throw New System.Exception(LRSCommon(45004)) '"Feil under uthenting av klientnummer.")
                                End If
                                '03.03.2017 - Added next line and If
                                sTemp = ""
                                sTemp = oMyDal.Reader_GetString("ConvertedAccount")
                                If EmptyString(sTemp) Then
                                    sAccountNo = oMyDal.Reader_GetString("Account")
                                Else
                                    sAccountNo = sTemp
                                End If
                                'Old code
                                'sAccountNo = oMyDal.Reader_GetString("Account")
                                If sAccountNo = "" Then
                                    Throw New System.Exception(LRSCommon(45005)) '"Feil under uthenting av kontonummer.")
                                End If

                                'Update the table with the unmatched amount (use the nonquery-connection)
                                sMySQlNonQuery2 = "UPDATE DayTotals SET "
                                If bZeroAmount Then 'TODO dbUpdayTotals - Why retrieve the UB when we are going to report unmatched? - 'Retrieve IB from DB
                                    dAmount = GetDBTotal(sCompanyID, sAccountNo)
                                End If
                                sMySQlNonQuery2 = sMySQlNonQuery2 & "UnMatched_BB = "
                                sMySQlNonQuery2 = sMySQlNonQuery2 & dAmount.ToString
                                sMySQlNonQuery2 = sMySQlNonQuery2 & " WHERE Company_ID = " & sCompanyID & " AND "
                                '08.09.2010 - new code
                                sMySQlNonQuery2 = sMySQlNonQuery2 & "AccountNo = '" & Trim$(sAccountNo) & "'" 'Don't test against date anymore

                                oMyDalNonQuery.SQL = sMySQlNonQuery2

                                'MsgBox("Skal oppdatere db med: " & sMySQlNonQuery2)

                                If oMyDalNonQuery.ExecuteNonQuery Then

                                    If oMyDalNonQuery.RecordsAffected = 0 Then
                                        ' Probably wrong date - no update done
                                        iErrNo = 20025 ' No records updated (Wrong date?)
                                        Throw New Exception(LRSCommon(20025) & vbCrLf & oMyDalNonQuery.ErrorMessage)
                                    End If

                                Else

                                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)

                                End If

                            Loop
                        Else
                            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                        End If
                        bAlreadyUpdated = True
                    Case "MATCHED_GL"
                        sMySQlNonQuery = sMySQlNonQuery & "Matched_GL = Matched_GL + " 'XNET - 09.03.2012
                    Case Else
                        iErrNo = 15016 ' Wrong type
                        Throw New Exception(LRS(15016))
                        Err.Raise(15016, "dbUpDayTotals", LRS(15016))
                End Select
                sMySQlNonQuery = sMySQlNonQuery & dAmount.ToString
                sMySQlNonQuery = sMySQlNonQuery & " WHERE Company_ID = " & sCompanyID & " AND "
                'sMySQL = sMySQL & "Client_ID = " & Trim$(Str(iClient_ID)) & " AND "

                '02.06.2014 - Changed the update query
                'Removed the proddate parameter from the WHERE clause.
                'This will create a problem if an export is done on a day where no import has been done.
                'New code
                sMySQlNonQuery = sMySQlNonQuery & "AccountNo = '" & Trim(sAccount) & "'"
                'Old code
                'sMySQlNonQuery = sMySQlNonQuery & "AccountNo = '" & Trim(sAccount) & "' AND "
                'sMySQlNonQuery = sMySQlNonQuery & "ProdDate = '" & sDate & "'"

                If Not bAlreadyUpdated Then
                    oMyDalNonQuery.SQL = sMySQlNonQuery

                    If oMyDalNonQuery.ExecuteNonQuery Then

                        If oMyDalNonQuery.RecordsAffected = 0 Then
                            ' Probably wrong date - no update done
                            iErrNo = 20025 ' No records updated (Wrong date?)
                            Throw New Exception(LRSCommon(20025) & vbCrLf & oMyDalNonQuery.ErrorMessage)
                        End If

                    Else

                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)

                    End If

                End If

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            If Not oMyDalNonQuery Is Nothing Then
                oMyDalNonQuery.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: dbUpDayTotals" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        If Not oMyDalNonQuery Is Nothing Then
            oMyDalNonQuery.Close()
            oMyDalNonQuery = Nothing
        End If

    End Function
    Private Function GetDBTotal(ByRef sCompanyID As String, ByRef sAccountNo As String) As Double
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iRecordCounter As Integer = 0
        Dim dReturnValue As Double = 0

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT SUM(MON_InvoiceAmount) as TotalAmount FROM Payment WHERE Company_ID = " & sCompanyID & " AND I_Account = '" & sAccountNo & "'"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    iRecordCounter = iRecordCounter + 1
                    dReturnValue = CDbl(oMyDal.Reader_GetString("TotalAmount"))

                Loop

                If iRecordCounter <> 1 Then
                    dReturnValue = 0
                    Throw New Exception("Feil under uthenting av �pningsbalanse")
                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetDBTotal" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return dReturnValue

    End Function
    Public Function BackupDatabase(ByRef sBackUpPath As String) As Boolean
        ' Copy to backup database. Prefixi with date, etc as we do in backup normally
        Dim oBackup As vbBabel.BabelFileHandling
        Dim bOK As Boolean

        oBackup = New vbBabel.BabelFileHandling
        oBackup.BackupPath = sBackUpPath
        oBackup.SourceFile = BB_DatabasePath()
        oBackup.DeleteOriginalFile = False
        oBackup.FilesetupID = 0
        oBackup.InOut = "I"
        oBackup.FilenameNo = 0 'FilenameOut1 or 2 or 3
        oBackup.Client = ""
        oBackup.TheFilenameHasACounter = False
        oBackup.BankAccounting = "P"

        bOK = oBackup.CreateBackup()
        'UPGRADE_NOTE: Object oBackup may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oBackup = Nothing

        BackupDatabase = bOK

    End Function
    Public Function DetectFileFormat(ByRef sFilename As String, Optional ByVal bShowMessage As Boolean = True, Optional ByVal sSpecial As String = "") As vbBabel.BabelFiles.FileType
        ' ------------------------------------------------
        ' Opens the specified file
        ' Reads two lines
        ' Determines which fileformat
        ' Returns Filetype
        '
        ' Public Enum Filetype
        '    Unknown = -1
        '    OCR = 1
        '    AutoGiro = 2
        '    Dirrem = 3
        '    Telepay = 4
        '    Coda = 5
        '
        ' EDI:
        '   CREMUL  501
        '   PAYMUL  502
        '   BANSTA  503
        '   DEBMUL  504
        '
        'End Enum
        ' --------------------------------------------------
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine1 As String = ""
        Dim sLine2 As String = ""
        Dim sLine As String = ""
        Dim bMultipleLines As Boolean
        Dim iTemp As Short
        Dim sTemp As String


        oFs = New Scripting.FileSystemObject
        oFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForReading)


        'FIX - Slett kode, test NRX
        'Dim l As Long
        'For l = 1 To 1000
        '    sLine = oFile.ReadLine
        '    If InStr(1, sLine, "S�tre Einar") > 0 Then
        '        iTemp = 1
        '        Dim l2 As Long
        '        For l2 = 19 To 140
        '            If Asc(Mid(sLine, l2, 1)) <> 32 Then
        '                iTemp = Asc(Mid(sLine, l2, 1))
        '            End If
        '        Next l2
        '    End If
        'Next l

        If oFile.AtEndOfStream = False Then
            sLine = oFile.Read(2000)
            If InStr(sLine, vbLf) > 0 Then
                bMultipleLines = True
            Else
                bMultipleLines = False
            End If
        End If


        If bMultipleLines Then
            ' 19.03.07 - Added -1, hopefully this is ok for all formats with multiplelines
            ' 11.10.2011 added 2 x replace to remoce vbCR or vbLF
            sLine1 = Replace(Replace(Left(sLine, InStr(sLine, vbLf) - 1), vbCr, ""), vbLf, "")
            sLine2 = Mid(sLine, InStr(sLine, vbLf) + 1)
        Else
            sLine1 = sLine
            sLine2 = ""
        End If


        ' 13.10.2017
        ' Vipps Excel; How to catch this?
        ' Try to check first part of file char by char, to see if this can be used without opening Excel
        ' 16.01.2018 this start is common to all Excelfiles - not a good test
        'If Left(sLine1, 5) = "PK" Then
        '    DetectFileFormat = vbBabel.BabelFiles.FileType.Vipps_Excel
        '    oFile.Close()
        '    oFile = Nothing
        '    Exit Function
        'End If

        ' 07.01.2019 - Klarna

        'New 18.05.2022 - TO define a specific format
        If Not RunTime() Then
            If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20220518" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Fjorkraft_utbetaling
                Exit Function
            End If
        End If

        ' 16.11.2018 - Klarna
        If sLine1.IndexOf("<?xml version=") > -1 And sLine2.IndexOf("<settlement>") > -1 Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.Klarna_XML
            oFile.Close()
            oFile = Nothing
            Exit Function
        End If

        ' 16.11.2018 - Nordea Camt054 SecureEnvelope
        If sLine1.IndexOf("ApplicationResponse") > -1 And sLine1.IndexOf("NDCAPXMLD54O") > -1 Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.Camt054_Outgoing
            oFile.Close()
            oFile = Nothing
            Exit Function
        End If

        ' 11.11.2021 - Nordea Camt054 SecureEnvelope
        If sLine1.IndexOf("ApplicationResponse") > -1 And sLine1.IndexOf("NDARCRAXMLO") > -1 Then
            'Is it Camt.054C or just Camt.054?????
            DetectFileFormat = vbBabel.BabelFiles.FileType.Camt054_Incoming
            oFile.Close()
            oFile = Nothing
            Exit Function
        End If

        ' 10.01.2022 - Nordea Camt053 SecureEnvelope
        If sLine1.IndexOf("ApplicationResponse") > -1 And sLine1.IndexOf("NDARSTXMLO") > -1 Then
            'Is it Camt.054C or just Camt.054?????
            DetectFileFormat = vbBabel.BabelFiles.FileType.Camt053
            oFile.Close()
            oFile = Nothing
            Exit Function
        End If
        ' 15.05.2019 - Nordea pain002 SecureEnvelope
        If sLine1.IndexOf("ApplicationResponse") > -1 And sLine1.IndexOf("NDCAPXMLO") > -1 Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.Pain002
            oFile.Close()
            oFile = Nothing
            Exit Function
        End If

        ' 30.12.2020 - Nordea Pain.001 SecureEnvelope
        If sLine1.IndexOf("UploadFile") > -1 And sLine1.IndexOf("NDCAPXMLI") > -1 Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.Pain001
            oFile.Close()
            oFile = Nothing
            Exit Function
        End If

        If (Left$(sLine1, 4) = "MH00" Or Left$(sLine1, 4) = "MH01") Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.NordeaCorporateFilePayments
            ' 21.11.2016 - must first release file !!! added next 2 lines
            oFile.Close()
            oFile = Nothing
            Exit Function
        End If

        If Left$(sLine1, 14) = "<?xml version=" Or Left$(sLine1, 17) = "﻿<?xml version=" Then
            If sLine2.IndexOf("http://app.teller.no") > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.TellerXML
                oFile.Close()
                oFile = Nothing
                Exit Function
            End If
        End If

        If Left$(sLine1, 14) = "<?xml version=" Or Left$(sLine1, 17) = "﻿<?xml version=" Then
            If sLine2.Length > 50 Then
                If sLine2.IndexOf("EurolineReport") > 0 Then
                    DetectFileFormat = vbBabel.BabelFiles.FileType.Bambora_XML
                    oFile.Close()
                    oFile = Nothing
                    Exit Function
                End If
            End If
        End If

        'New 24.03.2010 - To be able to use the BB_Autodetect-format for Kredinor.
        ' They have some JCL-shit at the start of the file
        If Left(sLine1, 5) = "FMS01" Then
            If Mid(sLine, 18, 8) = "NOKRE101" Then
                'That's Kredinor
                sLine1 = Left(sLine2, InStr(sLine, vbLf) - 1)
                sLine2 = Mid(sLine2, InStr(sLine2, vbLf) + 1)
            End If
        End If

        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        ' Determine fileformat based on info from one or two lines:
        If Mid(sLine1, 41, 11) = "FIRST UNION" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.First_Union_ACH
            Exit Function
        End If
        'Added ISO-format
        ' 07.02.2018 corrected with >0 and added 2. line in If
        If InStr(1, sLine1, "<?xml version=" & Chr(34) & "1.0" & Chr(34), CompareMethod.Text) > 0 Or _
        InStr(1, sLine1, "<?xml version='1.0'", CompareMethod.Text) > 0 Then
            If InStr(Left(sLine1, 200), "20022:tech:xsd:camt.054.", CompareMethod.Text) > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Camt054
                Exit Function
            ElseIf InStr(sLine2, "20022:tech:xsd:camt.054.", CompareMethod.Text) > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Camt054
                Exit Function
            ElseIf InStr(Left(sLine1, 200), "20022:tech:xsd:camt.053.", CompareMethod.Text) > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Camt053
                Exit Function
            ElseIf InStr(sLine2, "20022:tech:xsd:camt.053.", CompareMethod.Text) > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Camt053
                Exit Function
            ElseIf InStr(Left(sLine1, 200), "20022:tech:xsd:pain.002.", CompareMethod.Text) > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Pain002
                Exit Function
            ElseIf InStr(sLine2, "20022:tech:xsd:pain.002.", CompareMethod.Text) > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Pain002
                Exit Function
            ElseIf InStr(Left(sLine1, 200), "20022:tech:xsd:pain.001.", CompareMethod.Text) > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Pain001
                Exit Function
            ElseIf InStr(Left(sLine1, 200), "pain.001.001.03", CompareMethod.Text) > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Pain001
                Exit Function
            ElseIf InStr(sLine2, "20022:tech:xsd:pain.001.", CompareMethod.Text) > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Pain001
                Exit Function
            End If
        End If
        If Left(sLine1, 43) = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "iso-8859-1" & Chr(34) & "?>" Then
            If Left(sLine2, 14) = "<e_settlement>" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.TellerXML
                Exit Function
            End If
        End If
        '21.04.2010 - Added new format from Teller
        If Left(sLine1, 12) = "<SchemaRoot>" Then
            If Left(LTrim(sLine2), 22) = "<Merchants MerchantNo=" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.TellerXML
                Exit Function
            End If
        End If
        '01.06.2010 - Added new format from Teller
        ' 11.06.2014 Added UCase
        'If InStr(1, sLine1, "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "UTF-8" & Chr(34) & "?>") Then
        If InStr(1, UCase(sLine1), "<?XML VERSION=" & Chr(34) & "1.0" & Chr(34) & " ENCODING=" & Chr(34) & "UTF-8" & Chr(34) & "?>") Then
            If InStr(1, sLine2, "http://teller.no/XMLSchema") Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.TellerXML
                Exit Function
            End If
        End If

        If Left(sLine1, 9) = "FILHEADER" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.DnBNattsafe
            Exit Function
        End If
        If Left(sLine1, 16) = "Posenr;Dato;Idnr" Then '11 "Posenr;Idnr" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.SecuritasNattsafe
            Exit Function
        End If

        If Left$(sLine1, 4) = ":20:" Then
            If Left$(sLine2, 4) = ":25:" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.MT940
                Exit Function
            End If
        End If

        If Left(sLine1, 9) = "AH100TBUK" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.DnBTBUKIncoming
            Exit Function
        End If

        If Left(sLine1, 9) = "AH100TBWK" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.DnBTBWK
            Exit Function
        End If

        ' added 07.07.2009
        ' XokNET 25.01.2012 - changed test, to read Nachas from other banks than DNB
        If Left$(sLine1, 3) = "101" And (Len(sLine1) = 95 Or Len(sLine1) = 94) Then   'PS including CR !!!!
            DetectFileFormat = vbBabel.BabelFiles.FileType.Nacha
            Exit Function
        End If

        If Left(sLine1, 3) = "AH1" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.Telepay
            Exit Function
        End If
        If Left(sLine1, 3) = "AH2" Then
            ' added 03.02.2009, for Telepay+
            If Mid(sLine, 6, 2) = "TK" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.TelepayPlus
                ' XNET 30.04.2013 - added test for TP+ returnfiles
            ElseIf Mid$(sLine, 39, 2) = "10" Then       ' 10 lines in TP+ records
                DetectFileFormat = vbBabel.BabelFiles.FileType.TelepayPlus
            ElseIf Mid$(sLine, 6, 4) = "TBIO" Then '25.02.2016 - Added
                DetectFileFormat = vbBabel.BabelFiles.FileType.Telepay
            Else
                DetectFileFormat = vbBabel.BabelFiles.FileType.Telepay2
            End If
            Exit Function
        End If

        If Left(sLine1, 4) = "NY00" Then
            ' BBS
            If Left(sLine2, 4) = "NY09" Or Left(sLine2, 4) = "NY21" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.OCR
                Exit Function
            End If
            If Left(sLine2, 4) = "NY04" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Dirrem
                Exit Function
            End If
            If Left(sLine2, 4) = "NY01" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.AutoGiro
                Exit Function
            End If
            If Left(sLine2, 4) = "NY02" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.AutogiroEngangs
                Exit Function
            End If

        End If
        If Left(sLine1, 2) = "PX" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.coda
            Exit Function
        End If
        If Left(sLine1, 3) = "UNA" Or Left(sLine1, 4) = "UNB+" Then
            ' EDI-format
            ' Which one ?
            'FIX: Find a better test; but good enough for now
            '12.09.2008 Moved CONTRL first in the IF, because a CONTRL also will contain the messagetype of the original message
            If InStr(sLine1 & sLine2, "CONTRL") > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.CONTRL
                ' and f.ex. PAY at en of line 1 and MUL at start of line 2
            ElseIf InStr(sLine1 & sLine2, "CREMUL") > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.CREMUL
            ElseIf InStr(sLine1 & sLine2, "PAYMUL") > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.PAYMUL
            ElseIf InStr(sLine1 & sLine2, "BANSTA") > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.BANSTA
            ElseIf InStr(sLine1 & sLine2, "DEBMUL") > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.DEBMUL
            ElseIf InStr(sLine1 & sLine2, "COACSU") > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.COACSU
            Else
                ' Error, but carry on
                DetectFileFormat = vbBabel.BabelFiles.FileType.CREMUL ' No better format to suggest!
            End If
            Exit Function
        End If

        If Left(sLine1, 2) = "00" And Mid$(sLine1, 37, 3) = "TL1" Then
            If Mid$(sLine1, 40, 9) = "TOTALIN-T" Or Mid$(sLine1, 40, 7) = "TOTALIN" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Total_IN
                Exit Function
            End If
        End If

        If Left(sLine1, 5) = "0128A" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.DTAUS
            Exit Function
        End If

        If Left(sLine1, 5) = "<?xml" Then
            If sLine2.Contains("SettlementReport") Then
                If sLine2.Contains("http://vipps.no/xmlschema") Then
                    DetectFileFormat = vbBabel.BabelFiles.FileType.Vipps_XML
                    Exit Function
                End If
            End If
        End If

        If Left(sLine1, 4) = "TBIW" Or Left(sLine1, 5) = "<?xml" Or Left(sLine1, 4) = "TBIR" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.TBIXML
            Exit Function
        End If

        If Left(sLine1, 4) = "#US#" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.CitiDirect_US_Flat_File
            Exit Function
        End If

        If Left(sLine1, 5) = Chr(34) & "CMM" & Chr(34) Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.DanskeBankTeleService
            Exit Function
        ElseIf Left(sLine1, 5) = "CMM  " Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.DanskeBankTeleService
            Exit Function
        End If

        If Left(sLine1, 6) = Chr(34) & "CMBO" & Chr(34) Or Left(sLine1, 6) = Chr(34) & "CMUO" & Chr(34) Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.DanskeBankLokaleDanske
            Exit Function
        End If

        If Left$(sLine1, 3) = "{1:" Then
            ' XokNET 31.08.2011 - added for Odin MT940 Finland
            If InStr(sLine1, "NDEAFIHH") > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.MT940E_Handelbanken
            Else
                ' 13.11.2020 - In MT940 we have a :25: tag as line 3
                If InStr(sLine2, ":25:") > 5 Then
                    DetectFileFormat = vbBabel.BabelFiles.FileType.MT940
                ElseIf InStr(sLine2, ":28D:") > 5 Then
                    ' MT101 has :28D: tag in line3
                    DetectFileFormat = vbBabel.BabelFiles.FileType.MT101
                Else
                    DetectFileFormat = vbBabel.BabelFiles.FileType.MT940
                End If
            End If
            Exit Function
        End If

        If Left(sLine1, 3) = "�4:" Then
            If Left(sLine2, 4) = ":20:" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.MT940E_Handelbanken
                Exit Function
            End If
        End If

        If Left(sLine1, 9) = "AH100TBUK" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.DnBTBUKIncoming
            Exit Function
        End If

        If Left(sLine1, 3) = "FI0" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.SEK2
            Exit Function
        End If

        If Left(sLine1, 27) = ";;;;;;;;;;;;;;;;;;;Nattsafe" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.ControlConsultExcel
            Exit Function
        End If

        'Test to find filetype OCR_Bankgirot
        If UCase(Mid(sLine1, 72, 9)) = "BANKGIROT" Then
            If Left(sLine1, 2) = "00" Then
                If Left(sLine2, 2) = "10" Then
                    DetectFileFormat = vbBabel.BabelFiles.FileType.OCR_Bankgirot
                    Exit Function
                End If
            End If
        End If

        If UCase(Left(Replace(Replace(sLine1, Chr(0), "", , , CompareMethod.Text), "��", "", , , CompareMethod.Text), 7)) = "01BGMAX" Then
            If Left(Replace(sLine2, Chr(0), "", , , CompareMethod.Text), 2) = "05" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.BgMax
                Exit Function
            End If
        End If

        'Test to find filetype OCR_Autogirot
        'If UCase(Mid(sLine1, 11, 12)) = "AUTOGIRO9900" Then
        ' changed 09.12.2021
        If UCase(Mid(sLine1, 11, 12)) = "AUTOGIRO9900" Or UCase(Mid(sLine1, 1, 10)) = "01AUTOGIRO" Then
            If Left(sLine1, 2) = "01" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Autogiro_Bankgirot
                Exit Function
            End If
        End If

        ' 11.04.2017 Moved to here from further down, must be before NordeaFakturabetalning and Leverat�rsbetalning
        If Mid$(sLine1, 25, 6) = "SESISU" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.SEB_Sisu
            Exit Function
        End If

        ' Added Nordea Fakturabetalningsservice return
        If Left$(sLine1, 1) = "0" Then
            'If Len(Replace(sLine1, vbCr, "")) = 80 Then
            'Second line of recordtype 2
            'If Left$(sLine2, 1) = "1" Then
            ' 06.07.2015 Changed test
            If Len(Replace(sLine1, vbCr, "")) = 80 And Left$(sLine2, 1) = "1" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Nordea_SE_Plusgirot_FakturabetalningService
                'End If



                ''Test to find filetype Leverand�rsbetalningar. This is international payments.
                'If Left$(sLine1, 1) = "0" Then
                'First line shows recordtype 0
                ' XOKNET 06.07.2015 changed to >= 80
            ElseIf Len(Replace(Replace(sLine1, Chr(10), vbNullString), Chr(13), vbNullString)) >= 80 Then
                'Linelength 80
                If Mid$(sLine1, 79, 1) = "2" Then  ' 02.03.2017 - rettet fra 2 til "2" - det er jo en string som kommer fra mid( !!!
                    'Position 79, always 2
                    'Test if the creationdate stated is 2 years or less earlier than today
                    iTemp = Val(Format(Now(), "yy"))
                    If (Val(Mid$(sLine1, 10, 2)) - iTemp > -3) And (Val(Mid$(sLine1, 10, 2)) - iTemp < 1) Then
                        'Second line of recordtype 2
                        If Left$(sLine2, 1) = "2" Then
                            DetectFileFormat = vbBabel.BabelFiles.FileType.Leverantorsbetalningar
                            Exit Function
                        End If
                    End If
                End If
            End If
        End If

        'Test to find filetype Leverand�rsbetalningar. This is international payments.
        If Left(sLine1, 1) = "0" Then
            'First line shows recordtype 0
            If Len(Replace(Replace(sLine1, Chr(10), ""), Chr(13), "")) = 80 Then
                'Linelength 80
                If Mid(sLine1, 79, 1) = "2" Then ' 02.03.2017 - endret fra CDbl(Mid(sLine1, 79, 1)) til Mid(sLine1, 79, 1)
                    'Position 79, always 2
                    'Test if the creationdate stated is 2 years or less earlier than today
                    iTemp = CShort(VB6.Format(Now, "YY"))
                    If (Val(Mid(sLine1, 10, 2)) - iTemp > -3) And (Val(Mid(sLine1, 10, 2)) - iTemp < 1) Then
                        'Second line of recordtype 2
                        If Left(sLine2, 1) = "2" Then
                            DetectFileFormat = vbBabel.BabelFiles.FileType.Leverantorsbetalningar
                            Exit Function
                        End If
                    End If
                End If
            End If
        End If

        ' Leverant�rsbetalingar og automatisk avprickning, innland
        If Left(sLine1, 2) = "11" And Mid(sLine1, 19, 8) = "LEVERANT" Then
            'Unsure if this is the correct way to ditinguish between avprickning and leverant�rswbetalningar
            If Mid(sLine1, 47, 1) = "4" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Avprickning
                Exit Function
            Else
                DetectFileFormat = vbBabel.BabelFiles.FileType.Leverantorsbetalningar
                Exit Function
            End If
        End If

        If Left(sLine1, 2) = "11" And Mid(sLine1, 19, 8) = "BLANKETT" Then
            'Unsure if this is the correct way to ditinguish between avprickning and leverant�rswbetalningar
            DetectFileFormat = vbBabel.BabelFiles.FileType.Avprickning
            Exit Function
        End If

        If Left(sLine1, 4) = Chr(34) & "ERH" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.Handelsbanken_DK_Domestic
            Exit Function
        End If

        If Left(sLine1, 9) = "000000010" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.ErhvervsGiro_Udbetaling
            Exit Function
        End If

        If Left(sLine1, 3) = "LM0" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.LM_Domestic_Finland
            Exit Function
        End If

        If Left(sLine1, 4) = "LUM2" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.LUM_International_Finland
            Exit Function
        End If

        If Len(sLine1) - Len(Replace(sLine1, ",", "")) > 50 Then
            sTemp = Replace(xDelim(sLine1, ",", 34, Chr(34)), Chr(34), "") 'Overf�rselsdato
            If Len(sTemp) = 8 Then 'Date always 8 positions
                If IsNumeric(sTemp) Then
                    If Year(Now) - Val(Left(sTemp, 4)) > -5 And Year(Now) - Val(Left(sTemp, 4)) < 5 Then
                        If Val(Mid(sTemp, 5, 2)) < 13 Then
                            If Val(Right(sTemp, 2)) < 32 Then
                                DetectFileFormat = vbBabel.BabelFiles.FileType.NordeaUnitel
                                Exit Function
                            End If
                        End If
                    End If
                End If
            End If
        End If
        'Old definition
        'If Left$(sLine1, 5) = Chr(34) & "0" & Chr(34) & "," & Chr(34) Then  ' Chr(34) = "
        '    DetectFileFormat = FileType.NordeaUnitel
        '    Exit Function
        'End If
        If Len(sLine) > 17 Then
            If Left(sLine1, 17) = Chr(34) & "IB000000000000" & Chr(34) & "," Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Sydbank
                Exit Function
            End If
        End If
        If Left(sLine1, 12) = "51040516542;" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.Autocare_Norge
            Exit Function
        End If

        If Left(sLine1, 26) = "Home Allotment Listing For" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.Sudjaca
            Exit Function
        End If

        '<Posible BEC ???
        If Left(sLine1, 3) = "200" And Left(sLine2, 2) = "35" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.bec
            Exit Function
        End If

        If Left(sLine1, 2) = "21" And Left(sLine2, 2) = "23" Then
            If vbIsNumeric(Mid(sLine2, 3, 2), "0123456789") Then
                If CDbl(VB6.Format(Now, "YY")) - CDbl(Mid(sLine2, 3, 2)) < 4 Then 'The bookvalue must be among the last 3 years
                    DetectFileFormat = vbBabel.BabelFiles.FileType.Nordea_Innbet_Ktoinf_utl
                    Exit Function
                End If
            End If
        End If

        If Left(sLine1, 9) = "110000001" And Len(sLine) = 60 Then
            ' BOLS
            DetectFileFormat = vbBabel.BabelFiles.FileType.BOLS
            Exit Function
        End If
        If Left(sLine1, 14) = "11000001FACTOR" Then
            ' BOLS Fokus/SG Finans
            DetectFileFormat = vbBabel.BabelFiles.FileType.BOLS
            Exit Function
        End If

        'New 15.12.2008
        'K-Link utbetalingsfil
        If InStr(1, sLine, "#BBMANUELL#") > 0 Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.KLINK_ExcelUtbetaling
            Exit Function
        End If

        If Left(sLine1, 4) = "BOLS" And InStr(sLine, ";") > 0 Then
            ' SG Finans BOLS
            DetectFileFormat = vbBabel.BabelFiles.FileType.SGFINANSBOLS
            Exit Function
        End If
        If Left(sLine1, 5) = "K9409" Then
            ' SG Finans Kundefil
            DetectFileFormat = vbBabel.BabelFiles.FileType.SGFinans_Kunde
            Exit Function
        End If
        If Left(sLine1, 1) = "K" And Mid(sLine1, 3, 4) = "9409" Then
            ' SG Finans Kundefil
            DetectFileFormat = vbBabel.BabelFiles.FileType.SGFinans_Kunde
            Exit Function
        End If

        ' added 04.06.2007
        If (Len(sLine) - Len(Replace(sLine, ";", "")) = 18) And Len(sLine1) = 147 Then
            ' Separated SG Outgoing payments. Two lines imported
            DetectFileFormat = vbBabel.BabelFiles.FileType.AquariusOutgoing
            Exit Function
        End If

        If InStr("1789", Left(sLine1, 1)) > 0 And Len(sLine1) = 129 Then
            ' SG Finans Fakturafil
            DetectFileFormat = vbBabel.BabelFiles.FileType.SGFinans_Faktura
            Exit Function
        End If
        If InStr("1789", Left(sLine1, 1)) > 0 And (Len(sLine) - Len(Replace(sLine, ";", "")) > 10) Or Len(sLine) - Len(Replace(sLine, Chr(9), "")) > 10 Then ' chr(9) = TAB
            ' Separated SG Factoring
            ' SG Finans Fakturafil
            DetectFileFormat = vbBabel.BabelFiles.FileType.SGFinans_Faktura
            Exit Function
        End If
        If Left(sLine, 5) = "OCR;0" Then
            ' SG Finans Kundereturfil
            DetectFileFormat = vbBabel.BabelFiles.FileType.SG_ClientReporting
            Exit Function
        End If

        'Test to find Newphone faktura and kundefil
        If Left(sLine1, 2) = "H;" Then
            'Second line starts with 981161408;
            If Left(sLine2, 10) = "981161408;" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.NewPhoneFactoring
                Exit Function
            End If
        End If

        If sLine = "00000008080 00088114 S2000" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.Gjensidige_S2000_PAYMUL
            Exit Function
        End If

        If Left(sLine, 7) = "AHDRACH" Or Left(sLine, 7) = "AHDRLBX" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.DnBNORUS_ACH_Matching
            Exit Function
        End If

        ' Moved here 26.11.2014, kills next If !!!!!!
        If Left$(sLine, 4) = ":20:" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.MT940
            Exit Function
        End If

        ' XokNET 03.11.2011 - added DnB Wires
        If Left$(sLine, 4) = ":20:" And Mid$(sLine, 12, 5) = "MT940" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.DnBNORUS_ACH_Matching
            Exit Function
        End If


        If Left(sLine1, 8) = "PAYMENT;" Then
            If Len(sLine1) - Len(Replace(sLine1, ";", "")) = 13 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.AquariusIncoming
                Exit Function
            End If
        End If

        ' XNET 04.11.2010 for Girodirekt SE (Plusgiro)
        If Left$(sLine1, 10) = "MH00PAYEXT" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.GiroDirekt_Plusgiro
            Exit Function
        End If

        'XNET 06.12.2010 - Added next format
        If Left$(sLine1, 9) = "11ABNAX01" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.ABNAmro_BTL91
            Exit Function
        End If

        'XNET 07.12.2010 - Added next format
        'XNET - 17.12.2010 - Changed next line
        If Left$(sLine1, 1) = "0" And Mid$(sLine1, 8, 12) = Space(12) And Mid$(sLine1, 23, 2) = "51" And Len(sLine1) = 129 Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.ISABEL_APS130_Foreign
            Exit Function
        End If

        ' XNET 06.01.2010 added SebScreen_Finland
        If Left$(sLine1, 5) = ":32V:" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.SebScreen_Finland
            Exit Function
        End If

        'XokNET - 11.08.2011
        If InStr(1, Mid$(sLine1, 266, 22), "Givertelefonen") > 0 Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.Givertelefon
            Exit Function
        End If

        If Mid$(sLine1, 1, 5) = "0256Q" Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.DTAZV
            Exit Function
        End If

        ' added 14.09.2010, to trap empty files:
        ' Fake a format EmptyFileFormat, so we do not receive warnings about empty files
        If EmptyString(sLine) Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.EmptyFileFormat
            Exit Function
        End If

        If Mid(sLine1, 2, 14) = "Account Number" Then
            If Mid(sLine1, 19, 8) = "Currency" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.SEB_CI_Incoming
                Exit Function
            End If
        End If

        'XNET 09.10.2012 - Added nettingfile from Vingcard
        If Left(sLine1, 8) = "NETTING," Then
            If InStr(1, sLine1, ",FINAL,", vbTextCompare) > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Vingcard_netting
                Exit Function
            End If
        End If

        'XNET 06.11.2012 - Added file with cardpayments from Elavon (for SISMO)
        If Left(sLine1, 5) = "OH;1;" Then
            If Left(sLine2, 5) = "SD;2;" Or Left(sLine2, 5) = "BD;2;" Or Left(sLine2, 5) = "TD;2;" Then
                'DetectFileFormat = vbBabel.BabelFiles.FileType.Elavon_Specification
                ' 15.06.2018 Changed to new Elavon_2017
                DetectFileFormat = vbBabel.BabelFiles.FileType.Elavon_2017
                Exit Function
            End If
        End If

        '03.06.2016 - Added this test
        If Left(sLine1, 16) = "F000;00000000001" Then
            If sLine1.IndexOf("Scs transact", 1) > 0 Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Swedbank_CardReconciliation
            End If
        End If

        '25.05.2018 - Added this test
        If Left(sLine1, 2) = "T;" Then
            If sLine1.Length - sLine1.Replace(";", "").Length = 5 Then '27.08.2018 - Changed from = 4 to = 5, because Lindorff has added accountnum.
                'T;A68000026;22;10.05.2018;350,00
                DetectFileFormat = vbBabel.BabelFiles.FileType.Lindorff_Gebyr
            End If
        End If

        If Left(sLine1, 5) = "PK" Then
            If sSpecial = "SGF_EQ" Then '27.08.2018 - Changed from = 4 to = 5, because Lindorff has added accountnum.
                'T;A68000026;22;10.05.2018;350,00
                DetectFileFormat = vbBabel.BabelFiles.FileType.Excel_Incoming_DIV
            End If
        End If

        If Not RunTime() Then
            If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20210615" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Vipps_XML
            End If
        End If

        If Left(sLine1, 1) = "0" And sLine1.Length = 90 Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.KTL
        End If
        If Left(sLine1, 3) = "T00" And sLine1.Length = 322 Then
            DetectFileFormat = vbBabel.BabelFiles.FileType.TITO
        End If

        If sLine1.Length > 30 Then
            If sLine1.Substring(0, 23) = "Sak_Nr;Sak_Ref2;Sak_ref" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Modhi_Internal
            End If
        End If

        If Not RunTime() Then
            If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20220920" Then
                DetectFileFormat = vbBabel.BabelFiles.FileType.Fjorkraft_utbetaling
            End If
        End If

        If DetectFileFormat = vbBabel.BabelFiles.FileType.Unknown Then
            If bShowMessage Then  ' added this parameter 09.12.2010
                MsgBox(LRSCommon(20026, sFilename), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "DetectFileFormat")
                'Err.Raise 15703, "BabelBank is unable to detect the format of the file" & vbCrLf & "Filename: " & sFilename
            End If
        End If


        ' At this point we do not know the filetype - ask the user:
        'FIX: DetectFileFormat = AskForFiletype

    End Function

    Public Function DetectVersion(ByRef eFormat As vbBabel.BabelFiles.FileType, ByRef sFilename As String) As String
        ' ------------------------------------------------
        Dim oFs As Scripting.FileSystemObject 'Object
        Dim oFile As Scripting.TextStream 'Object
        Dim sLine1 As String

        oFs = New Scripting.FileSystemObject ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")
        oFile = oFs.OpenTextFile(sFilename, 1)

        If oFile.AtEndOfStream = False Then
            'Read first line
            sLine1 = oFile.Read(500)
        End If
        'If oFile.AtEndOfStream = False Then
        '    'Read second line
        '    sLine2 = oFile.ReadLine()
        'End If
        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        If Left(sLine1, 3) = "UNA" Then
            ' EDI-format
            ' Which one ?
            'FIX: Find a better test; but good enough for now
            If InStr(sLine1, "CREMUL") > 0 Then
                ' Version i placed next to CREMUL:
                DetectVersion = Strip(Mid(sLine1, InStr(sLine1, "CREMUL") + 7, 5), ":")
            ElseIf InStr(sLine1, "PAYMUL") > 0 Then
                ' Version i placed next to PAYMUL:
                DetectVersion = Strip(Mid(sLine1, InStr(sLine1, "PAYMUL") + 7, 5), ":")
            ElseIf InStr(sLine1, "BANSTA") > 0 Then
                ' Version i placed next to BANSTA:
                DetectVersion = Strip(Mid(sLine1, InStr(sLine1, "BANSTA") + 7, 5), ":")
            ElseIf InStr(sLine1, "DEBMUL") > 0 Then
                ' Version i placed next to DEBMUL:
                DetectVersion = Strip(Mid(sLine1, InStr(sLine1, "DEBMUL") + 7, 5), ":")
            Else
                ' Error, but carry on
                DetectVersion = "D96A" ' No better version to suggest!
            End If
            Exit Function
        End If

        ' At this point we do not know the filetype - ask the user:
        'FIX: DetectFileFormat = AskForFiletype

    End Function

    Public Function DetectEDI(ByVal eFormat As vbBabel.BabelFiles.FileType) As Boolean
        ' ------------------------------------------------
        ' True if EDI-format
        ' Public Enum Filetype
        '    EDI-formats between 500 and 1000
        ' --------------------------------------------------
        If eFormat > 499 And eFormat < 1000 Then
            DetectEDI = True
        Else
            DetectEDI = False
        End If
    End Function
    'XNET - 26.11.2010 - Changed this function which appeared not to be in use
    Public Function CopyBabelFileObject(ByRef oOriginalBabelFile As vbBabel.BabelFile) As vbBabel.BabelFile
        'XNETOld function
        'Public Function CopyBabelFileObject(ByRef oOriginalBabelFile As vbBabel.BabelFile, ByRef oEmptyBabelFile As vbBabel.BabelFile) As vbBabel.BabelFile
        Dim oTempBatch As vbBabel.Batch
        Dim oBatch As vbBabel.Batch
        'XNET - added next 2 lines
        Dim oEmptyBabelFile As vbBabel.BabelFile

        'oEmptyBabelFile = CreateObject ("vbBabel.Babelfile")
        oEmptyBabelFile = New vbBabel.BabelFile

        oEmptyBabelFile.VB_Profile = oOriginalBabelFile.VB_Profile
        oEmptyBabelFile.BankByCode = oOriginalBabelFile.BankByCode
        oEmptyBabelFile.ClientNo = oOriginalBabelFile.ClientNo
        'oEmptyBabelFile.DATE_Production = oOriginalBabelFile.DATE_Production
        'oEmptyBabelFile.EDI_CreateCONTRLMessage = oOriginalBabelFile.EDI_CreateCONTRLMessage
        'oEmptyBabelFile.EDI_FilenameCONTRLMessage = oOriginalBabelFile.EDI_FilenameCONTRLMessage
        'oEmptyBabelFile.EDI_Format = oOriginalBabelFile.EDI_Format
        oEmptyBabelFile.EDI_MessageNo = oOriginalBabelFile.EDI_MessageNo
        'oEmptyBabelFile.ERPSystem = oOriginalBabelFile.ERPSystem
        'oEmptyBabelFile.ERR_Message = oOriginalBabelFile.ERR_Message
        'oEmptyBabelFile.ERR_Number = oOriginalBabelFile.ERR_Number
        'oEmptyBabelFile.ERR_UseErrorObject = oOriginalBabelFile.ERR_UseErrorObject
        oEmptyBabelFile.File_id = oOriginalBabelFile.File_id
        oEmptyBabelFile.FileFromBank = oOriginalBabelFile.FileFromBank
        oEmptyBabelFile.FilenameIn = oOriginalBabelFile.FilenameIn
        oEmptyBabelFile.IDENT_Receiver = oOriginalBabelFile.IDENT_Receiver
        oEmptyBabelFile.IDENT_Sender = oOriginalBabelFile.IDENT_Sender
        oEmptyBabelFile.ImportFormat = oOriginalBabelFile.ImportFormat
        oEmptyBabelFile.Index = oOriginalBabelFile.Index
        'oEmptyBabelFile.Language = oOriginalBabelFile.Language
        oEmptyBabelFile.MON_InvoiceAmount = oOriginalBabelFile.MON_InvoiceAmount
        oEmptyBabelFile.MON_TransferredAmount = oOriginalBabelFile.MON_TransferredAmount
        oEmptyBabelFile.No_Of_Records = oOriginalBabelFile.No_Of_Records
        oEmptyBabelFile.No_Of_Transactions = oOriginalBabelFile.No_Of_Transactions
        oEmptyBabelFile.RejectsExists = oOriginalBabelFile.RejectsExists
        oEmptyBabelFile.REPORT_Print = oOriginalBabelFile.REPORT_Print
        oEmptyBabelFile.REPORT_Screen = oOriginalBabelFile.REPORT_Screen
        oEmptyBabelFile.Special = oOriginalBabelFile.Special
        oEmptyBabelFile.StatusCode = oOriginalBabelFile.StatusCode
        'oEmptyBabelFile.VB_DBBabelFileIndex = oOriginalBabelFile.VB_DBBabelFileIndex
        oEmptyBabelFile.VB_FilenameInNo = oOriginalBabelFile.VB_FilenameInNo
        oEmptyBabelFile.VB_FileSetupID = oOriginalBabelFile.VB_FileSetupID
        oEmptyBabelFile.VB_ProfileInUse = oOriginalBabelFile.VB_ProfileInUse
        oEmptyBabelFile.Version = oOriginalBabelFile.Version

        ' then copy all Batchs;
        oEmptyBabelFile.Batches = New vbBabel.Batches
        For Each oTempBatch In oOriginalBabelFile.Batches
            oBatch = New vbBabel.Batch
            CopyBatchObject(oTempBatch, oBatch)
            oBatch = oEmptyBabelFile.Batches.VB_AddWithObject(oBatch)
        Next oTempBatch

        'XNET - added next line
        CopyBabelFileObject = oEmptyBabelFile

    End Function
    Public Function CopyBatchObject(ByRef oOriginalBatch As vbBabel.Batch, ByRef oEmptyBatch As vbBabel.Batch) As vbBabel.Batch
        Dim oTempPayment As vbBabel.Payment
        Dim oPayment As vbBabel.Payment
        'dim oEmptyBatch As vbbabel.Batch

        'Set oEmptyBatch = New vbbabel.Batch")
        oEmptyBatch.VB_Profile = oOriginalBatch.VB_Profile
        oEmptyBatch.Cargo = oOriginalBatch.Cargo
        oEmptyBatch.SequenceNoStart = oOriginalBatch.SequenceNoStart
        oEmptyBatch.SequenceNoEnd = oOriginalBatch.SequenceNoEnd
        oEmptyBatch.Batch_ID = oOriginalBatch.Batch_ID
        oEmptyBatch.StatusCode = oOriginalBatch.StatusCode
        oEmptyBatch.Version = oOriginalBatch.Version
        oEmptyBatch.OperatorID = oOriginalBatch.OperatorID
        oEmptyBatch.FormatType = oOriginalBatch.FormatType
        oEmptyBatch.I_EnterpriseNo = oOriginalBatch.I_EnterpriseNo
        oEmptyBatch.I_Branch = oOriginalBatch.I_Branch
        oEmptyBatch.DATE_Production = oOriginalBatch.DATE_Production
        oEmptyBatch.REF_Bank = oOriginalBatch.REF_Bank
        oEmptyBatch.REF_Own = oOriginalBatch.REF_Own
        oEmptyBatch.MON_TransferredAmount = oOriginalBatch.MON_TransferredAmount
        oEmptyBatch.MON_InvoiceAmount = oOriginalBatch.MON_InvoiceAmount
        oEmptyBatch.ImportFormat = oOriginalBatch.ImportFormat
        oEmptyBatch.MON_BalanceIN = oOriginalBatch.MON_BalanceIN
        oEmptyBatch.MON_BalanceOUT = oOriginalBatch.MON_BalanceOUT

        ' then copy all payments;
        oEmptyBatch.Payments = New vbBabel.Payments
        For Each oTempPayment In oOriginalBatch.Payments
            oPayment = New vbBabel.Payment
            ''''   Set oPayment = oEmptyBatch.Payments.VB_AddWithObject(oPayment)
            CopyPaymentObject(oTempPayment, oPayment)
            oPayment = oEmptyBatch.Payments.VB_AddWithObject(oPayment)
        Next oTempPayment

    End Function

    Public Function CopyPaymentObject(ByRef oOriginalPayment As vbBabel.Payment, ByRef oNewPayment As vbBabel.Payment, Optional ByVal nIndex As Double = -1) As vbBabel.Payment

        Dim oInvoice As vbBabel.Invoice
        Dim oTempInvoice As vbBabel.Invoice

        'Set oNewPayment = New vbbabel.Payment")
        oNewPayment.VB_Profile = oOriginalPayment.VB_Profile
        ' XNET 17.04.2013 Added nIndex
        If nIndex > 0 Then
            oNewPayment.Index = nIndex
        End If
        oNewPayment.StatusCode = oOriginalPayment.StatusCode
        oNewPayment.StatusText = oOriginalPayment.StatusText
        oNewPayment.Cancel = oOriginalPayment.Cancel
        oNewPayment.Payment_ID = oOriginalPayment.Payment_ID
        oNewPayment.DATE_Payment = oOriginalPayment.DATE_Payment
        oNewPayment.MON_TransferredAmount = oOriginalPayment.MON_TransferredAmount
        oNewPayment.MON_TransferCurrency = oOriginalPayment.MON_TransferCurrency
        oNewPayment.REF_Own = oOriginalPayment.REF_Own
        oNewPayment.REF_Bank1 = oOriginalPayment.REF_Bank1
        oNewPayment.REF_Bank2 = oOriginalPayment.REF_Bank2
        oNewPayment.E_AccountPrefix = oOriginalPayment.E_AccountPrefix
        oNewPayment.E_AccountSuffix = oOriginalPayment.E_AccountSuffix
        oNewPayment.E_Account = oOriginalPayment.E_Account
        oNewPayment.E_Name = oOriginalPayment.E_Name
        oNewPayment.E_Adr1 = oOriginalPayment.E_Adr1
        oNewPayment.E_Adr2 = oOriginalPayment.E_Adr2
        oNewPayment.E_Adr3 = oOriginalPayment.E_Adr3
        oNewPayment.E_Zip = oOriginalPayment.E_Zip
        oNewPayment.E_City = oOriginalPayment.E_City
        oNewPayment.E_CountryCode = oOriginalPayment.E_CountryCode
        oNewPayment.DATE_Value = oOriginalPayment.DATE_Value
        oNewPayment.Priority = oOriginalPayment.Priority
        oNewPayment.I_Client = oOriginalPayment.I_Client
        oNewPayment.Text_E_Statement = oOriginalPayment.Text_E_Statement
        oNewPayment.Text_I_Statement = oOriginalPayment.Text_I_Statement
        oNewPayment.Cheque = oOriginalPayment.Cheque
        oNewPayment.ToOwnAccount = oOriginalPayment.ToOwnAccount
        oNewPayment.PayCode = oOriginalPayment.PayCode
        oNewPayment.PayType = oOriginalPayment.PayType
        oNewPayment.PayTypeSetInImport = oOriginalPayment.PayTypeSetInImport
        oNewPayment.DnBNORTBIPayType = oOriginalPayment.DnBNORTBIPayType
        oNewPayment.I_Account = oOriginalPayment.I_Account
        oNewPayment.I_Name = oOriginalPayment.I_Name
        oNewPayment.I_Adr1 = oOriginalPayment.I_Adr1
        oNewPayment.I_Adr2 = oOriginalPayment.I_Adr2
        oNewPayment.I_Adr3 = oOriginalPayment.I_Adr3
        oNewPayment.I_Zip = oOriginalPayment.I_Zip
        oNewPayment.I_City = oOriginalPayment.I_City
        oNewPayment.I_CountryCode = oOriginalPayment.I_CountryCode
        oNewPayment.Structured = oOriginalPayment.Structured
        oNewPayment.VB_ClientNo = oOriginalPayment.VB_ClientNo
        oNewPayment.MON_LocalAmount = oOriginalPayment.MON_LocalAmount
        oNewPayment.MON_LocalCurrency = oOriginalPayment.MON_LocalCurrency
        oNewPayment.MON_AccountAmount = oOriginalPayment.MON_AccountAmount
        oNewPayment.MON_AccountCurrency = oOriginalPayment.MON_AccountCurrency
        oNewPayment.MON_InvoiceAmount = oOriginalPayment.MON_InvoiceAmount
        oNewPayment.MON_InvoiceCurrency = oOriginalPayment.MON_InvoiceCurrency
        oNewPayment.MON_EuroAmount = oOriginalPayment.MON_EuroAmount
        oNewPayment.MON_AccountExchRate = oOriginalPayment.MON_AccountExchRate
        oNewPayment.MON_LocalExchRate = oOriginalPayment.MON_LocalExchRate
        'oNewPayment.MON_EuroExchRate = oOriginalPayment.MON_EuroExchRate
        'oNewPayment.MON_ChargesAmount = oOriginalPayment.MON_ChargesAmount
        oNewPayment.MON_LocalExchRateSet = oOriginalPayment.MON_LocalExchRateSet
        'oNewPayment.MON_ChargesCurrency = oOriginalPayment.MON_ChargesCurrency
        oNewPayment.MON_ChargeMeDomestic = oOriginalPayment.MON_ChargeMeDomestic
        oNewPayment.MON_ChargeMeAbroad = oOriginalPayment.MON_ChargeMeAbroad
        oNewPayment.ERA_ExchRateAgreed = oOriginalPayment.ERA_ExchRateAgreed
        oNewPayment.ERA_DealMadeWith = oOriginalPayment.ERA_DealMadeWith
        oNewPayment.ERA_Date = oOriginalPayment.ERA_Date
        oNewPayment.FRW_ForwardContractRate = oOriginalPayment.FRW_ForwardContractRate
        oNewPayment.FRW_ForwardContractNo = oOriginalPayment.FRW_ForwardContractNo
        oNewPayment.NOTI_NotificationMessageToBank = oOriginalPayment.NOTI_NotificationMessageToBank
        oNewPayment.NOTI_NotificationType = oOriginalPayment.NOTI_NotificationType
        oNewPayment.NOTI_NotificationParty = oOriginalPayment.NOTI_NotificationParty
        oNewPayment.NOTI_NotificationAttention = oOriginalPayment.NOTI_NotificationAttention
        oNewPayment.NOTI_NotificationIdent = oOriginalPayment.NOTI_NotificationIdent
        oNewPayment.BANK_SWIFTCode = oOriginalPayment.BANK_SWIFTCode
        oNewPayment.BANK_BranchType = oOriginalPayment.BANK_BranchType
        oNewPayment.BANK_BranchNo = oOriginalPayment.BANK_BranchNo
        oNewPayment.BANK_Name = oOriginalPayment.BANK_Name
        oNewPayment.BANK_Adr1 = oOriginalPayment.BANK_Adr1
        oNewPayment.BANK_Adr2 = oOriginalPayment.BANK_Adr2
        oNewPayment.BANK_Adr3 = oOriginalPayment.BANK_Adr3
        oNewPayment.BANK_CountryCode = oOriginalPayment.BANK_CountryCode
        oNewPayment.BANK_SWIFTCodeCorrBank = oOriginalPayment.BANK_SWIFTCodeCorrBank
        oNewPayment.BANK_BranchTypeCorrBank = oOriginalPayment.BANK_BranchTypeCorrBank
        oNewPayment.BANK_BranchNoCorrBank = oOriginalPayment.BANK_BranchNoCorrBank
        oNewPayment.BANK_I_SWIFTCode = oOriginalPayment.BANK_I_SWIFTCode
        oNewPayment.BANK_I_BranchType = oOriginalPayment.BANK_I_BranchType
        oNewPayment.BANK_I_Domestic = oOriginalPayment.BANK_I_Domestic
        oNewPayment.BANK_NameAddressCorrBank1 = oOriginalPayment.BANK_NameAddressCorrBank1
        oNewPayment.BANK_NameAddressCorrBank2 = oOriginalPayment.BANK_NameAddressCorrBank2
        oNewPayment.BANK_NameAddressCorrBank3 = oOriginalPayment.BANK_NameAddressCorrBank3
        oNewPayment.BANK_NameAddressCorrBank4 = oOriginalPayment.BANK_NameAddressCorrBank4
        'XokNET 09.02.2012 - Added countrycode corrbank
        oNewPayment.BANK_CountryCodeCorrBank = oOriginalPayment.BANK_CountryCodeCorrBank
        oNewPayment.ImportFormat = oOriginalPayment.ImportFormat
        'oNewPayment.PaymentMethod = oOriginalPayment.PaymentMethod
        oNewPayment.VB_FilenameOut_ID = oOriginalPayment.VB_FilenameOut_ID
        oNewPayment.MATCH_Matched = oOriginalPayment.MATCH_Matched
        oNewPayment.VoucherNo = oOriginalPayment.VoucherNo
        oNewPayment.VoucherType = oOriginalPayment.VoucherType
        oNewPayment.Exported = oOriginalPayment.Exported
        oNewPayment.ToSpecialReport = oOriginalPayment.ToSpecialReport
        ' XNET 16.11.2010
        oNewPayment.REF_EndToEnd = oOriginalPayment.REF_EndToEnd

        oNewPayment.PurposeCode = oOriginalPayment.PurposeCode
        oNewPayment.UltimateE_Name = oOriginalPayment.UltimateE_Name
        oNewPayment.UltimateI_Name = oOriginalPayment.UltimateI_Name

        'XNET 20.12.2010 - Added next 3 lines
        oNewPayment.MATCH_HowMatched = oOriginalPayment.MATCH_HowMatched
        oNewPayment.MON_OriginallyPaidAmount = oOriginalPayment.MON_OriginallyPaidAmount
        oNewPayment.MON_OriginallyPaidCurrency = oOriginalPayment.MON_OriginallyPaidCurrency
        ' XNET 14.05.2013
        oNewPayment.MergeThis = oOriginalPayment.MergeThis
        oNewPayment.MergeCriteria = oOriginalPayment.MergeCriteria
        oNewPayment.MergeID = oOriginalPayment.MergeID
        ' XNET 19.06.2013 added 3 lines
        oNewPayment.ExtraD1 = oOriginalPayment.ExtraD1
        oNewPayment.ExtraD2 = oOriginalPayment.ExtraD2
        oNewPayment.ExtraD3 = oOriginalPayment.ExtraD3

        '' Then copy all invoices;
        '31.05.2071 - Added next IF, because sometimes we remove all invoices before we do the copy (to be more effective)
        If Not oOriginalPayment.Invoices Is Nothing Then
            For Each oTempInvoice In oOriginalPayment.Invoices
                oInvoice = New vbBabel.Invoice
                CopyInvoiceObject(oTempInvoice, oInvoice)
                oInvoice = oNewPayment.Invoices.VB_AddWithObject(oInvoice)
            Next oTempInvoice
        End If

        'Set from inside the class
        'ProfileInUse

        'Just local variables
        'sFormatType

        'Not included
        'oFile
        'oInvoices/Invoices
        'nIndex
        'oCargo

        ' XNET 19.06.2013 - changed next line
        'ExtraD4 and ExtraD5 not included

        'extraI1 -
        'sMATCH_MatchingIDAgainstObservationAccount As String
        'Private nMATCH_DifferenceAllowed As Double
        'Private bMATCH_DifferenceExact As Boolean
        'Private sMATCH_DifferenceAccount As String
        'Private iMATCH_DifferenceAccountType As Integer
        'Private sMATCH_DifferenceText As String
        'Private nMATCH_DiscountAllowed As Double
        'Private sMATCH_DiscountAccount As String
        'Private iMATCH_DiscountAccountType As Integer
        'Private sMATCH_DiscountText As String
        'Private sMATCH_ErrorText As String
        'Private bMATCH_ErrorInValuta As Boolean
        'Private lUnique_PaymentID As Long
        'Private sUnique_ERPID As String
        'Private bWritePaymentAmountIfOpen As Boolean
        'Private sVB_ObservationAccount As String 'This variable will not be stored!
        'Private sInvoiceDescription As String, iNoOfLeadingZeros As Integer
        ''Private bCheckAccounting As Boolean  ' Check invoiceNos against accounts receivable
        ''Private bSplitPayment As Boolean     ' Split payment into several inovices, using invoiceNo
        ''Private bStructureFreeText As Boolean  ' Structure FreeText with possible invoicenos
        'Private aPossibleInvoiceNos() As String, lInvoiceCounter As Long
        'Private sImportLineBuffer As String


    End Function
    Public Function CopyInvoiceObject(ByRef oOriginalInvoice As vbBabel.Invoice, ByRef oNewInvoice As vbBabel.Invoice) As Object

        Dim oFreeText As vbBabel.Freetext
        Dim oTempFreetext As vbBabel.Freetext
        ' XNET 01.07.2013 - must add ErrorObjects
        Dim oErrorObject As vbBabel.ErrorObject
        Dim oTempError As vbBabel.ErrorObject

        oNewInvoice.VB_Profile = oOriginalInvoice.VB_Profile
        oNewInvoice.StatusCode = oOriginalInvoice.StatusCode
        oNewInvoice.Cancel = oOriginalInvoice.Cancel
        oNewInvoice.MON_TransferredAmount = oOriginalInvoice.MON_TransferredAmount
        oNewInvoice.MON_InvoiceAmount = oOriginalInvoice.MON_InvoiceAmount
        oNewInvoice.MON_AccountAmount = oOriginalInvoice.MON_AccountAmount
        oNewInvoice.MON_LocalAmount = oOriginalInvoice.MON_LocalAmount
        oNewInvoice.REF_Own = oOriginalInvoice.REF_Own
        oNewInvoice.REF_Bank = oOriginalInvoice.REF_Bank
        oNewInvoice.Invoice_ID = oOriginalInvoice.Invoice_ID
        oNewInvoice.InvoiceNo = oOriginalInvoice.InvoiceNo
        oNewInvoice.CustomerNo = oOriginalInvoice.CustomerNo
        ' XNET 24.07.2013 added Supplierno
        oNewInvoice.SupplierNo = oOriginalInvoice.SupplierNo
        oNewInvoice.Unique_Id = oOriginalInvoice.Unique_Id
        'XNET - 13.12.2010 - Added next variable - Can contain different dateformats
        oNewInvoice.InvoiceDate = oOriginalInvoice.InvoiceDate
        oNewInvoice.STATEBANK_Code = oOriginalInvoice.STATEBANK_Code
        oNewInvoice.STATEBANK_Text = oOriginalInvoice.STATEBANK_Text
        oNewInvoice.STATEBANK_DATE = oOriginalInvoice.STATEBANK_DATE
        oNewInvoice.ImportFormat = oOriginalInvoice.ImportFormat
        oNewInvoice.MATCH_Original = oOriginalInvoice.MATCH_Original
        oNewInvoice.MATCH_Final = oOriginalInvoice.MATCH_Final
        oNewInvoice.MATCH_Matched = oOriginalInvoice.MATCH_Matched
        oNewInvoice.MATCH_MatchType = oOriginalInvoice.MATCH_MatchType
        oNewInvoice.MATCH_ID = oOriginalInvoice.MATCH_ID
        oNewInvoice.MATCH_PartlyPaid = oOriginalInvoice.MATCH_PartlyPaid
        'XNET 20.12.2010 - Added next 2 lines
        oNewInvoice.MATCH_ERPName = oOriginalInvoice.MATCH_ERPName
        oNewInvoice.MyField = oOriginalInvoice.MyField

        ' XNET 01.07.2013 - must add ErrorObjects
        For Each oTempError In oOriginalInvoice.ErrorObjects
            oErrorObject = New vbBabel.ErrorObject
            CopyErrorObject(oTempError, oErrorObject)
            oErrorObject = oNewInvoice.ErrorObjects.VB_AddWithObject(oErrorObject)
        Next

        '' Then copy all Freetexts;
        'Set oNewFreetext.Freetexts = New vbbabel.Freetexts
        For Each oTempFreetext In oOriginalInvoice.Freetexts
            oFreeText = New vbBabel.Freetext
            CopyFreetextObject(oTempFreetext, oFreeText)
            oFreeText = oNewInvoice.Freetexts.VB_AddWithObject(oFreeText)
        Next oTempFreetext


    End Function
    Public Function CopyFreetextObject(ByRef oOriginalFreetext As vbBabel.Freetext, ByRef oNewFreetext As vbBabel.Freetext) As Object


        oNewFreetext.Qualifier = oOriginalFreetext.Qualifier
        oNewFreetext.Col = oOriginalFreetext.Col
        oNewFreetext.Row = oOriginalFreetext.Row
        oNewFreetext.Text = oOriginalFreetext.Text

    End Function
    ' XNET 01.07.2013 - copy Errorobject
    Public Function CopyErrorObject(ByVal oOriginalError As vbBabel.ErrorObject, ByVal oNewError As vbBabel.ErrorObject)
        oNewError.CollectionLevel = oOriginalError.CollectionLevel
        oNewError.Errormessage = oOriginalError.Errormessage
        oNewError.ErrorType = oOriginalError.ErrorType
        oNewError.GridColumnName = oOriginalError.GridColumnName
        'oNewError.Index = oOriginalError.Index
        oNewError.PropertyName = oOriginalError.PropertyName
        oNewError.PropertyValue = oOriginalError.PropertyValue

    End Function
    '    Public Function ConnectToBBDB(ByRef bTest As Boolean, ByRef sErrorString As String, ByRef bUseLRS As Boolean) As ADODB.Connection
    '        Dim conERPDB As New ADODB.Connection
    '        Dim sConnectionString As String
    '        Dim sErrDescription As String
    '        Dim bErrorDuringConnection As Boolean

    '        On Error GoTo ERRHAndler

    '        bErrorDuringConnection = False
    '        ' Old sConnectionString = Trim$(oProfile.ConString)
    '        ' Old sConnectionString = Replace(sConnectionString, "=UID", "=" & Trim$(oProfile.ConUID))
    '        sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BB_DatabasePath() & ";"
    '        'If Not bTest Then
    '        '    ' Old sConnectionString = Replace(sConnectionString, "=PWD", "=" & pwDeCrypt(Trim$(oProfile.ConPWD)))
    '        '    ' 24.02.2003 Deleted PWDecrypt
    '        '    sConnectionString = Replace(sConnectionString, "=PWD", "=" & Trim$(sConPWD))
    '        'Else
    '        '    ' Old sConnectionString = Replace(sConnectionString, "=PWD", "=" & Trim$(oProfile.ConPWD))
    '        '    sConnectionString = Replace(sConnectionString, "=PWD", "=" & Trim$(sConPWD))
    '        'End If

    '        bErrorDuringConnection = True
    '        conERPDB.Open(sConnectionString)

    '        sErrorString = ""
    '        ConnectToBBDB = conERPDB

    '        Exit Function

    'ERRHAndler:

    '        If Not conERPDB Is Nothing Then
    '            'UPGRADE_NOTE: Object conERPDB may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            conERPDB = Nothing
    '        End If

    '        '25001: Couldn't connect to external database.
    '        If bUseLRS Then
    '            sErrDescription = LRS(25001)

    '            If bErrorDuringConnection Then
    '                'The connection reported the following error:
    '                sErrDescription = sErrDescription & vbCrLf & vbCrLf & LRS(25002) & vbCrLf & Err.Description
    '            Else
    '                'Error during the building of the connectionstring.
    '                sErrDescription = sErrDescription & vbCrLf & vbCrLf & LRS(25003)
    '            End If

    '            If bTest Then
    '                sErrorString = sErrDescription
    '            Else
    '                '25004: Connectionstring:
    '                '25005: To test/change the connectionstring, start BabelBank and choose Company under Setup in the menu.
    '                sErrDescription = sErrDescription & vbCrLf & vbCrLf & LRS(25004) & vbCrLf & sConnectionString & vbCrLf & vbCrLf & LRS(25005)

    '                Err.Raise(19502, , sErrDescription)
    '            End If
    '        Else
    '            sErrDescription = "25001  - Greier ikke � logge p� ekstern database"
    '            If bErrorDuringConnection Then
    '                'The connection reported the following error:
    '                sErrDescription = sErrDescription & vbCrLf & vbCrLf & "P�logging rapporterte f�lgende feil:" & vbCrLf & Err.Description
    '            Else
    '                'Error during the building of the connectionstring.
    '                sErrDescription = sErrDescription & vbCrLf & vbCrLf & "Feil under oppbygging av p�loggingsstreng"
    '            End If

    '            If bTest Then
    '                sErrorString = sErrDescription
    '            Else
    '                '25004: Connectionstring:
    '                '25005: To test/change the connectionstring, start BabelBank and choose Company under Setup in the menu.
    '                sErrDescription = sErrDescription & vbCrLf & vbCrLf & "P�loggingsstreng " & vbCrLf & sConnectionString & vbCrLf & vbCrLf & "Endre p�loggingsstreng i oppsett."

    '                Err.Raise(19502, , sErrDescription)
    '            End If

    '        End If

    '    End Function

    Public Function FindAmountToUseInMatching(ByRef oPayment As vbBabel.Payment, ByRef oInvoice As vbBabel.Invoice, ByRef aAdjustments(,) As String, ByRef bUseAdjustments As Boolean, ByRef iAdjustmentIndex As Short, ByRef nAdjustmentAmount As Double) As Double
        'The function always returns the invoice amount if it is a difference between the
        ' payment and the invoice amount
        Dim nReturnValue As Double
        Dim bAdjustmentFound As Boolean
        Dim lCounter As Integer

        bAdjustmentFound = False

        If bUseAdjustments Then
            If oPayment.Invoices.Count = 1 Then
                'New 06.12.2006 - The currencies must be equal as well (I think)
                If oPayment.MON_InvoiceCurrency = oPayment.MON_OriginallyPaidCurrency Then
                    If oInvoice.MON_InvoiceAmount = oPayment.MON_InvoiceAmount Then
                        If oPayment.MON_InvoiceAmount < oPayment.MON_OriginallyPaidAmount Then
                            If oPayment.MON_OriginallyPaidAmount > 0 Then

                                If Not Array_IsEmpty(aAdjustments) Then
                                    For lCounter = 0 To UBound(aAdjustments, 2)
                                        'Use adjustments only for the correct currency
                                        If aAdjustments(1, lCounter) = oPayment.MON_InvoiceCurrency Then
                                            If CBool(aAdjustments(2, lCounter)) = True Then '2 = Exact
                                                If System.Math.Abs(oPayment.MON_OriginallyPaidAmount - oPayment.MON_InvoiceAmount - Val(aAdjustments(0, lCounter))) < 0.0001 Then '0 = Amount
                                                    nReturnValue = oPayment.MON_OriginallyPaidAmount
                                                    nAdjustmentAmount = oPayment.MON_InvoiceAmount - oPayment.MON_OriginallyPaidAmount
                                                    iAdjustmentIndex = lCounter
                                                    bAdjustmentFound = True
                                                    Exit For
                                                End If
                                            Else
                                                If System.Math.Abs(oPayment.MON_OriginallyPaidAmount - oPayment.MON_InvoiceAmount) <= Val(aAdjustments(0, lCounter)) Then '0 = Amount
                                                    nReturnValue = oPayment.MON_OriginallyPaidAmount
                                                    nAdjustmentAmount = oPayment.MON_InvoiceAmount - oPayment.MON_OriginallyPaidAmount
                                                    iAdjustmentIndex = lCounter
                                                    bAdjustmentFound = True
                                                    Exit For
                                                End If
                                            End If
                                        End If
                                    Next lCounter
                                    If Not bAdjustmentFound Then
                                        'No valid adjustment found, use the invoice amount
                                        nReturnValue = oInvoice.MON_InvoiceAmount
                                        iAdjustmentIndex = -1
                                    End If
                                Else
                                    'No adjustments allowed, use the invoice amount
                                    nReturnValue = oInvoice.MON_InvoiceAmount
                                End If

                            Else
                                'Some shit is stored in the, return the invoiceamount
                                nReturnValue = oInvoice.MON_InvoiceAmount
                            End If
                        Else
                            'No charges or stuff, return the invoiceamount
                            nReturnValue = oInvoice.MON_InvoiceAmount
                        End If
                    Else
                        'OK, the amounts are equal
                        nReturnValue = oInvoice.MON_InvoiceAmount
                    End If
                Else
                    'OK, it's a difference in currency
                    nReturnValue = oInvoice.MON_InvoiceAmount
                End If
            Else
                'OK, there exist probably more than 1 invoice, return the invoiceamount
                nReturnValue = oInvoice.MON_InvoiceAmount
            End If
        Else
            'Don't bother to check
            nReturnValue = oInvoice.MON_InvoiceAmount
        End If

        FindAmountToUseInMatching = nReturnValue

    End Function
    Public Function ReplaceBBText(ByVal sString As String, ByRef oPayment As vbBabel.Payment) As String
        'Develop this function later

        sString = Replace(sString, "BB_Name", Trim(oPayment.E_Name), , , CompareMethod.Text)
        ReplaceBBText = sString

    End Function

    Public Function TranslateSignedAmount(ByRef sAmount As String, Optional ByRef eBank As vbBabel.BabelFiles.Bank = vbBabel.BabelFiles.Bank.No_Bank_Specified, Optional ByRef eERP As vbBabel.BabelFiles.FileType = vbBabel.BabelFiles.FileType.Unknown) As Double
        Dim nReturnAmount As Double

        On Error GoTo ERR_TranslateSignedAmount

        If eBank > vbBabel.BabelFiles.Bank.No_Bank_Specified Then

            Select Case eBank

            End Select

        ElseIf eERP > vbBabel.BabelFiles.FileType.Unknown Then

            Select Case eERP

                Case vbBabel.BabelFiles.FileType.OCR_Bankgirot


                    Select Case Asc(Right(sAmount, 1))

                        ' Added 29.01.08 by kjell - Used by bankgirot OCR
                        Case 45 ' -
                            nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10
                        Case 74, 106 'J, eller j
                            nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 1
                        Case 75, 107 'K, eller k
                            nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 2
                        Case 76, 108 'L, eller l
                            nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 3
                        Case 77, 109 'M, eller m
                            nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 4
                        Case 78, 110 'N, eller n
                            nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 5
                        Case 79, 111 'O, eller o
                            nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 6
                        Case 80, 112 'P, eller p
                            nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 7
                        Case 81, 113 'Q, eller q
                            nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 8
                        Case 82, 114 'R, eller r
                            nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 9
                    End Select


            End Select

        Else

            Select Case Asc(Right(sAmount, 1))

                ' Added 07.04.06 by JanP - Used by bankgirot
                Case 45 ' -
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10
                Case 48, 93, 123, 198, 230 '0, ], {, � eller �
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * 10
                Case 49, 65, 97 '1, A eller a
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * 10 + 1
                Case 50, 66, 98 '2, B eller b
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * 10 + 2
                Case 51, 67, 99 '3, C eller c
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * 10 + 3
                Case 52, 68, 100 '4, D eller d
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * 10 + 4
                Case 53, 69, 101 '5, E eller e
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * 10 + 5
                Case 54, 70, 102 '6, F eller f
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * 10 + 6
                Case 55, 71, 103 '7, G eller g
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * 10 + 7
                Case 56, 72, 104 '8, H eller h
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * 10 + 8
                Case 57, 73, 105 '9, I eller i
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * 10 + 9
                Case 125, 197, 229 '}, � eller �
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10
                Case 74, 106 'J, eller j
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 1
                Case 75, 107 'K, eller k
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 2
                Case 76, 108 'L, eller l
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 3
                Case 77, 109 'M, eller m
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 4
                Case 78, 110 'N, eller n
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 5
                Case 79, 111 'O, eller o
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 6
                Case 80, 112 'P, eller p
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 7
                Case 81, 113 'Q, eller q
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 8
                Case 82, 114 'R, eller r
                    nReturnAmount = Val(Left(sAmount, Len(sAmount) - 1)) * -10 - 9
            End Select

        End If

        TranslateSignedAmount = nReturnAmount

        On Error GoTo 0
        Exit Function

ERR_TranslateSignedAmount:

        Err.Raise(Err.Number, "TranslateSignedAmount", Err.Description)

    End Function
    Public Function TranslateToSignedAmount(ByRef nAmount As Double, Optional ByRef sSpecial As String = "") As String
        ' Translate from a "normal" amount, into a Signed amount
        ' NB! Amount must be passed without decimals, the same way as amounts are stored in BabelBank

        Dim sReturnAmount As String
        Dim sLast As String

        On Error GoTo ERR_TranslateToSignedAmount
        ' Based upon ���, and ABCDEFGH etc

        ' find last digit;
        sLast = Right(Str(nAmount), 1)

        Select Case sSpecial

            Case "DNBNORFINANS"
                If nAmount < 0 Then
                    'sAmountToUse = Replace(sAmountToUse, "-", "")
                    Select Case sLast
                        Case "0"
                            sLast = "p"
                        Case "1"
                            sLast = "q"
                        Case "2"
                            sLast = "r"
                        Case "3"
                            sLast = "s"
                        Case "4"
                            sLast = "t"
                        Case "5"
                            sLast = "u"
                        Case "6"
                            sLast = "v"
                        Case "7"
                            sLast = "w"
                        Case "8"
                            sLast = "x"
                        Case "9"
                            sLast = "y"
                    End Select
                    'sAmountToUse = Left(sAmountToUse, Len(sAmountToUse) - 1) & sLastCharacter
                Else
                    'No changes
                End If


            Case Else
                If nAmount < 0 Then
                    Select Case sLast
                        Case "0"
                            sLast = "�"
                        Case "1"
                            sLast = "J"
                        Case "2"
                            sLast = "K"
                        Case "3"
                            sLast = "L"
                        Case "4"
                            sLast = "M"
                        Case "5"
                            sLast = "N"
                        Case "6"
                            sLast = "O"
                        Case "7"
                            sLast = "P"
                        Case "8"
                            sLast = "Q"
                        Case "9"
                            sLast = "R"
                    End Select

                Else
                    ' Amount > 0
                    Select Case sLast
                        Case "0"
                            sLast = "�"
                        Case "1"
                            sLast = "A"
                        Case "2"
                            sLast = "B"
                        Case "3"
                            sLast = "C"
                        Case "4"
                            sLast = "D"
                        Case "5"
                            sLast = "E"
                        Case "6"
                            sLast = "F"
                        Case "7"
                            sLast = "G"
                        Case "8"
                            sLast = "H"
                        Case "9"
                            sLast = "I"
                    End Select
                End If

        End Select

        ' Now sLast holds last signed digit
        ' Must then merge with rest of amount;
        sReturnAmount = Left(Str(System.Math.Abs(nAmount)), Len(Str(nAmount)) - 1) & sLast

        TranslateToSignedAmount = sReturnAmount

        On Error GoTo 0
        Exit Function

ERR_TranslateToSignedAmount:

        Err.Raise(Err.Number, "TranslateToSignedAmount", Err.Description)

    End Function
    Public Function LoadNBCodes(Optional ByRef bJustCodes As Boolean = False, Optional ByRef bShowErrMessage As Boolean = True) As String()
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLanguageID As String
        Dim sLanguage As String
        Dim sString As String
        Dim bFirstCodeFound As Boolean
        Dim bLastCodeFound As Boolean
        Dim aNBCodes() As String
        Dim lCounter As Integer
        Dim bFileFound As Boolean
        Dim sFilename As String

        oFs = New Scripting.FileSystemObject  ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")
        ' 24.03.2020 - Flyttet fil fra "C:\Projects\BabelBank\BabelSetup\StateBankCodes_NO.txt", og
        ' dermed kuttet ut If Not Run Time() Then
        'If Not Run Time() Then 
        '    If Not oFs.FileExists("C:\Projects\BabelBank\BabelSetup\StateBankCodes_NO.txt") Then
        '        bFileFound = False
        '    Else
        '        bFileFound = True
        '        sFilename = "C:\Projects\BabelBank\BabelSetup\StateBankCodes_NO.txt"
        '    End If
        'Else
        If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\StateBankCodes_NO.txt") Then
            bFileFound = False
        Else
            bFileFound = True
            sFilename = My.Application.Info.DirectoryPath & "\StateBankCodes_NO.txt"
        End If
        'End If

        If Not bFileFound Then
            If bShowErrMessage Then
                MsgBox(LRSCommon(20023), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRSCommon(20024))
            End If
        Else
            oFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForReading)

            'sLanguageID = FindLanguageID()
            'If sLanguageID = "414" Or sLanguageID = "814" Or sLanguageID = "406" Or sLanguageID = "41D" Then
            '    sLanguage = "<NO>"
            'Else
            '    sLanguage = "<GB>"
            'End If
            sLanguageID = Thread.CurrentThread.CurrentCulture.ToString
            'If sLanguage = "da" Or sLanguage = "nb-NO" Or sLanguage = "nn-NO" Then
            ' 24.03.2020 rettet fra slanguage til sLanguageID
            If sLanguageID = "da" Or sLanguageID = "nb-NO" Or sLanguageID = "nn-NO" Then
                sLanguage = "<NO>"
            Else
                sLanguage = "<GB>"
            End If

            bFirstCodeFound = False
            Do Until bFirstCodeFound Or oFile.AtEndOfStream
                sString = oFile.ReadLine
                If sString = sLanguage Then
                    bFirstCodeFound = True
                End If
            Loop

            bLastCodeFound = False
            lCounter = 0
            If bFirstCodeFound Then
                ReDim Preserve aNBCodes(lCounter)
                If Not bJustCodes Then
                    aNBCodes(lCounter) = LRSCommon(20022)
                End If
                Do Until bLastCodeFound Or oFile.AtEndOfStream
                    sString = oFile.ReadLine
                    lCounter = lCounter + 1
                    If sString = "<END>" Then
                        bLastCodeFound = True
                    Else
                        ReDim Preserve aNBCodes(lCounter)
                        If bJustCodes Then
                            aNBCodes(lCounter) = Left(Trim(sString), 2)
                        Else
                            aNBCodes(lCounter) = sString
                        End If
                    End If
                Loop
            End If
        End If

        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        LoadNBCodes = VB6.CopyArray(aNBCodes)

    End Function
    Public Function GetAccounts(ByRef iCompany_ID As Short, Optional ByRef iClient_ID As Short = 0) As String(,)
        'Returns Client stored in the BBDB
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim aReturnArray(,) As String = Nothing
        Dim iCounter As Integer = -1
        Dim sTemp As String '24.02.2017

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            '24.02.2017 - For SG Finans - Need to use ConvertedAccount (if used) instead of Account
            sMySQL = "SELECT A.Account, C.Client_ID, A.ConvertedAccount FROM Client C, Account A WHERE C.Client_ID = A.Client_ID AND C.Company_ID = A.Company_ID AND A.Company_ID = " & iCompany_ID.ToString
            If iClient_ID > 0 Then
                'Retrieve for a specific client
                sMySQL = sMySQL & " AND A.Client_ID = " & iClient_ID.ToString
            End If
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord
                    iCounter = iCounter + 1
                    ReDim Preserve aReturnArray(2, iCounter)
                    aReturnArray(0, iCounter) = oMyDal.Reader_GetString("Client_ID")
                    sTemp = oMyDal.Reader_GetString("ConvertedAccount")
                    If EmptyString(sTemp) Then
                        'As prior to 24.02.2017 - No IF here
                        aReturnArray(1, iCounter) = oMyDal.Reader_GetString("Account")
                    Else
                        aReturnArray(1, iCounter) = sTemp
                    End If

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetAccounts" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aReturnArray

    End Function
    ' XNET 10.05.2013 - Get list of clients
    Public Function GetClients(ByVal iCompany_ID As Integer) As String(,)
        'Returns Clients stored in the BBDB
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim aReturnArray(,) As String = Nothing
        Dim lCounter As Long = 0

        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT Client_ID, ClientNo, Name FROM Client WHERE Company_ID = " & Trim$(Str(iCompany_ID))

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord
                        ReDim Preserve aReturnArray(2, lCounter)
                        aReturnArray(0, lCounter) = oMyDal.Reader_GetString("Client_ID")
                        aReturnArray(1, lCounter) = oMyDal.Reader_GetString("ClientNo")
                        aReturnArray(2, lCounter) = oMyDal.Reader_GetString("Name")

                        lCounter = lCounter + 1

                    Loop
                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: GetClients" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aReturnArray
    End Function
    Public Function FindBBConnectionString() As String
        Dim sReturnValue As String = ""

        If BB_UseSQLServer() Then
            ' Find connectionstring
            'sReturnValue = System.Configuration.ConfigurationSettings.AppSettings("BB_CONNECTIOSTRING")
            sReturnValue = System.Configuration.ConfigurationManager.AppSettings("BB_CONNECTIONSTRING")
        ElseIf BB_UseAccess2016() Then
            ' added 27.03.2016 - Use Access 2007-2016 as BB Database
            sReturnValue = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & BB_DatabasePath()
        Else
            sReturnValue = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BB_DatabasePath() '& ";"
            ' HER HAR VI KODE FOR ACCESS 2007 til 2016
            ' TODO ACCESS2016
            'sReturnValue = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Projects\Babelbank\Profiles\profile.accdb;Persist Security Info=False;"
        End If
        'Else
        'sReturnValue = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BB_DatabasePath() '& ";"
        'End If

        Return sReturnValue

    End Function
    Public Function BB_UseSQLServer() As Boolean
        Dim bReturnValue As Boolean = False
        Dim sSQLServerTest As String = ""

        sSQLServerTest = System.Configuration.ConfigurationManager.AppSettings("SQL_SERVER")
        If Not sSQLServerTest Is Nothing Then
            bReturnValue = CBool(sSQLServerTest)
        End If

        Return bReturnValue

    End Function
    Public Function BB_UseAccess2016() As Boolean
        Dim bReturnValue As Boolean = False
        Dim sAccess2016Test As String = ""

        sAccess2016Test = System.Configuration.ConfigurationManager.AppSettings("ACCESS2016")
        If Not sAccess2016Test Is Nothing Then
            bReturnValue = CBool(sAccess2016Test)
        End If

        Return bReturnValue

    End Function

    Public Function MakeACopyOfBBDB(ByRef oLog As vbLog.vbLogging, ByVal bLog As Boolean, ByVal eTypeOfBackup As vbBabel.BabelFiles.BackupType, Optional ByRef bShowError As Boolean = False, Optional ByRef sBackUpPath As String = "", Optional ByRef iGenerations As Short = 5, Optional ByRef bSilent As Boolean = False) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim sMyDatabasePath As String
        Dim iCounter As Short
        Dim iReturnValue As Short
        Dim oMyDal As vbBabel.DAL
        Dim sMySQL As String = ""
        Dim sConnectionString As String = ""
        Dim sTemp As String = ""
        Dim sDBName As String = ""
        Dim iPos As String
        ' 07.06.2018 - introduced Access2016 - extension = .accdb
        Dim sExtension As String = ".mdb"
        'Changed this function 06.12.2006, two new parameters in the function call

        Try

            oFs = New Scripting.FileSystemObject  ' 22.05.2017 CreateObject ("Scripting.Filesystemobject")

            sMyDatabasePath = BB_DatabasePath() ' Includes name of database, ex. Profile.mdb
            If BB_UseSQLServer() Then
                If EmptyString(sBackUpPath) Then
                    MakeACopyOfBBDB = True
                    Exit Function
                End If
            ElseIf Not EmptyString(sMyDatabasePath) Then
                sMyDatabasePath = Left(sMyDatabasePath, InStrRev(sMyDatabasePath, "\") - 1)

                ' added 08.06.2018:
                'If System.Configuration.ConfigurationManager.AppSettings("ACCESS2016") Then
                If BB_UseAccess2016() Then
                    sExtension = ".accdb"
                Else
                    sExtension = ".mdb"
                End If


                ' If db_ins path exists, then copy there, otherwise copy to same
                ' catalog as database
                If oFs.FolderExists(sMyDatabasePath & "\db_ins") Then
                    sMyDatabasePath = sMyDatabasePath & "\db_ins"
                End If
                If sBackUpPath = "" Then
                    sBackUpPath = sMyDatabasePath
                End If
            Else
                If EmptyString(sBackUpPath) Then
                    MakeACopyOfBBDB = True
                    Exit Function
                End If
            End If

            If Right(sBackUpPath, 1) <> "\" Then
                sBackUpPath = sBackUpPath & "\"
            End If

            'Delete old file if it exists

            If Not BB_UseSQLServer() Then
                '05.07.2018 - Added this If for Lindorff, doesn't make sense to check the folder.
                'The backup is run from SQL Server which usually is run on another server than BabelBank is running!
                If bSilent Then
                    iReturnValue = CheckFolder(sBackUpPath, vb_Utils.ErrorMessageType.Dont_Show, True, LRSCommon(15007), False, bSilent)
                Else
                    iReturnValue = CheckFolder(sBackUpPath, vb_Utils.ErrorMessageType.Use_MsgBox, True, LRSCommon(15007), False, bSilent)
                End If
            End If

            If Right(sBackUpPath, 1) = "\" Then
                sBackUpPath = Left(sBackUpPath, Len(sBackUpPath) - 1)
            End If

            If BB_UseSQLServer() Then
                If iReturnValue < 8 Or 1 = 1 Then
                    '05.07.2018 - Added 1 = 1 for Lindorff, doesn't make sense to check the folder.
                    'The backup is run from SQL Server which usually is run on another server than BabelBank is running!

                    'Find the name of the database
                    sConnectionString = FindBBConnectionString().ToUpper
                    iPos = sConnectionString.IndexOf("INITIAL CATALOG")
                    sDBName = ""
                    If iPos > 0 Then
                        sTemp = sConnectionString.Substring(iPos + ("INITIAL CATALOG").Length)
                        iPos = 0
                        Do While iPos < sTemp.Length
                            If sTemp.Substring(iPos, 1) = " " Or sTemp.Substring(iPos, 1) = "=" Then
                                'Ok, remove this character
                            Else
                                Exit Do
                                'Keep the rest of the string    
                            End If
                            iPos = iPos + 1
                        Loop
                        sTemp = sTemp.Substring(iPos)
                        iPos = sTemp.IndexOf(";")
                        If iPos > 0 Then
                            sDBName = sTemp.Substring(0, iPos)
                        End If
                    End If

                    '29.05.2019 - Added Next IF to try to find the db if it is stated as DATBASE=
                    If EmptyString(sDBName) Then
                        iPos = sConnectionString.IndexOf("DATABASE")
                        If iPos > 0 Then
                            sTemp = sConnectionString.Substring(iPos + ("DATABASE").Length)
                            iPos = 0
                            Do While iPos < sTemp.Length
                                If sTemp.Substring(iPos, 1) = " " Or sTemp.Substring(iPos, 1) = "=" Then
                                    'Ok, remove this character
                                Else
                                    Exit Do
                                    'Keep the rest of the string    
                                End If
                                iPos = iPos + 1
                            Loop
                            sTemp = sTemp.Substring(iPos)
                            iPos = sTemp.IndexOf(";")
                            If iPos > 0 Then
                                sDBName = sTemp.Substring(0, iPos)
                            End If
                        End If
                    End If

                    If Not EmptyString(sDBName) Then
                        'New 06.12.2006
                        '05.07 - Removed next For .. Next and then the next If for Lindorff
                        'Reason: See above


                        'For iCounter = iGenerations To 2 Step -1
                        '    If oFs.FileExists(sBackUpPath & "\x" & sDBName & Trim(Str(iCounter - 1)) & ".bak") Then
                        '        oFs.CopyFile(sBackUpPath & "\x" & sDBName & Trim(Str(iCounter - 1)) & ".bak", sBackUpPath & "\x" & sDBName & Trim(Str(iCounter)) & ".bak", True)
                        '    End If
                        'Next iCounter

                        ''Delete no.1 if it exist, because if not the backup wil be added to the existing
                        'If oFs.FileExists(sBackUpPath & "\x" & sDBName & "1.bak") Then
                        '    oFs.DeleteFile(sBackUpPath & "\x" & sDBName & "1.bak")
                        'End If

                        ' copy database to file
                        oMyDal = New vbBabel.DAL

                        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                        If Not oMyDal.ConnectToDB() Then
                            Throw New System.Exception(oMyDal.ErrorMessage)
                        End If

                        'sMySQL = "BACKUP DATABASE Profile TO DISK='C:\Slett\xBabelBank1.BAK'"
                        'sMySQL = "BACKUP DATABASE Profile TO DISK = '" & sBackUpPath & "\x" & sDBName & "1.bak'"
                        sMySQL = "BACKUP DATABASE " & sDBName & " TO DISK = '" & sBackUpPath & "\" & ReplaceDateTimeStamp(sDBName & "__%YMD_hms%.bak") & "'"

                        oMyDal.SQL = sMySQL

                        If oMyDal.ExecuteNonQuery Then

                            MakeACopyOfBBDB = True

                        Else

                            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                            MakeACopyOfBBDB = False
                        End If

                        oMyDal.Close()
                        oMyDal = Nothing

                        MakeACopyOfBBDB = True
                    Else
                        Throw New Exception(LRSCommon(25006, sConnectionString))
                        MakeACopyOfBBDB = False
                    End If
                Else
                    Throw New Exception(LRSCommon(25007, sBackUpPath))
                    MakeACopyOfBBDB = False
                End If
            Else
                If iReturnValue < 8 Then

                    'New 06.12.2006
                    For iCounter = iGenerations To 2 Step -1
                        If oFs.FileExists(sBackUpPath & "\xprofile_" & Trim(Str(iCounter - 1)) & sExtension) Then
                            oFs.CopyFile(sBackUpPath & "\xprofile_" & Trim(Str(iCounter - 1)) & sExtension, sBackUpPath & "\xprofile_" & Trim(Str(iCounter)) & sExtension, True)
                        End If
                    Next iCounter
                    ' copy Profile.mdb to 1
                    oFs.CopyFile(BB_DatabasePath, sBackUpPath & "\xprofile_1" & sExtension, True)
                    MakeACopyOfBBDB = True
                Else
                    Throw New Exception(LRSCommon(25007, sBackUpPath))
                    MakeACopyOfBBDB = False
                End If
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If


        Catch ex As Exception

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            sTemp = ""
            Select Case eTypeOfBackup

                Case vbBabel.BabelFiles.BackupType.AfterExport
                    sTemp = "AfterExport"

                Case vbBabel.BabelFiles.BackupType.BeforeExport
                    sTemp = "BeforeExport"

                Case vbBabel.BabelFiles.BackupType.BeforeImport
                    sTemp = "BeforeImport"

                Case vbBabel.BabelFiles.BackupType.FromSetup
                    sTemp = "FromSetup"

                Case Else
                    sTemp = ""

            End Select

            If bLog Then
                oLog.Heading = "BabelBank warning: "
                oLog.AddLogEvent("MakeACopyOfBBDB" & "_" & sTemp & " - " & ex.Message, System.Diagnostics.TraceEventType.Warning) 'vbLogEventTypeInformationDetail
            End If

            If bShowError Then
                Throw New Exception("MakeACopyOfBBDB" & "_" & sTemp & vbCrLf & ex.Message)
            Else
                MakeACopyOfBBDB = False
            End If

        End Try

    End Function
    Public Function BBdbTooLarge(Optional ByRef bShowMessage As Boolean = True, Optional ByRef sMessageString As String = "") As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iMaxSizeDB As Integer = 0   ' 17.11.2020 changed from Short = 0
        Dim bReturnValue As Boolean = False
        Dim iCompany_ID As Integer

        Try

            If Not BB_UseSQLServer() Then 'Do not test for SQL Server
                iCompany_ID = 1

                oMyDal = New vbBabel.DAL
                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDal.ConnectToDB() Then
                    Throw New System.Exception(oMyDal.ErrorMessage)
                End If

                sMySQL = "SELECT MaxSizeDB FROM Company WHERE Company_ID = " & iCompany_ID.ToString
                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then

                    Do While oMyDal.Reader_ReadRecord

                        'iMaxSizeDB = CShort(oMyDal.Reader_GetString("MaxSizeDB"))
                        iMaxSizeDB = CInt(oMyDal.Reader_GetString("MaxSizeDB"))  '17.11.2020
                        If iMaxSizeDB = 0 Then
                            iMaxSizeDB = 50  ' 17.11.2020 changed from 15
                        End If

                    Loop

                    'If FileLen(BB_DatabasePath) > iMaxSizeDB * 1000000 Then
                    ' 27.11.2020 - changed If, not to have overflow
                    If FileLen(BB_DatabasePath) / 1000000 > iMaxSizeDB Then
                        bReturnValue = True
                        If bShowMessage Then
                            'Give the user a warning about this
                            MsgBox(LRS(60218, LRSCommon(40001)), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRS(60219))
                            'BabelBank's database exceeds the maximum size that are stated under setup & vbcrlf
                            'To compress the database:
                            '1) Make sure that all other users are logged out of BabelBank.
                            '2) Choose 'File' -> 'Compress database' from the menu in BabelBank
                            '3) Confirm to start the process by pressing 'Yes'
                            '
                            'An alternative is to increase the maximum size of the database
                            'To increase the maximum size
                            '1) Choose 'Setup' -> 'Company' from the menu in BabelBank
                            '2) In the field '%1' set the desired size
                            '3) Save by clicking the 'OK'-button

                            'Compress BabelBank's database at the size of = lrs(60457) = %1
                        Else
                            sMessageString = LRS(60218, LRSCommon(40001))
                        End If
                    End If

                Else

                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

                End If
            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            If bShowMessage Then
                MsgBox(ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "BBdbTooLarge")
                bReturnValue = True
            Else
                sMessageString = Err.Description
                bReturnValue = True 'Too show messagestring
            End If

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function
    Public Function ReportName(ByRef iReportNO As Object) As String
        ' Translate from report number to reportname
        Select Case iReportNO



            Case 1
                ReportName = "Betalinger, sumrapport (001)" ' Payments, summary
            Case 2
                ReportName = "Betalinger, pr linje"
            Case 3
                ReportName = "Betalinger, fakturaniv�"
            Case 4
                ReportName = "Innbetalinger OCR"
            Case 5
                ReportName = "Innbetalinger FIK"
            Case 6
                ReportName = "Innbetalinger, UDUS"
            Case 503
                ReportName = "Innbetalinger, uavstemte"
            Case 504
                ReportName = "Innbetalinger, avstemte"
            Case 505
                ReportName = "Innbetalinger, kontrollrapport"
            Case 506
                ReportName = "Innbetalinger, sumrapporter"
            Case 507
                ReportName = "Innbetalinger, aKonto"
            Case 508
                ReportName = "Innbetalinger, til hovedbok"
            Case 509
                ReportName = "Innbetalinger, �pne poster"
            Case 510
                ReportName = "Innbetalinger, dagtotaler"
            Case 601
                ReportName = "Nattsafe, totalrapport"
            Case 602
                ReportName = "Nattsafe, differanser"
            Case 901
                ReportName = "Korreksjoner"
            Case 902
                ReportName = "Avvik"
            Case 911
                ReportName = "Importerte filer"
            Case 912
                ReportName = "Filtrerte og slettede"
            Case 950
                ReportName = "Spesialrapport"
            Case 990
                ReportName = "Klientoversikt"
            Case Else
                ReportName = "Ukjent rapportnavn"
        End Select

    End Function

    Public Function SaveExportstatus(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef sErrorText As String) As Boolean
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True
        Dim nErrors As Double = 0

        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.Exported Then
                            sMySQL = "UPDATE Payment SET Exported = " & Str(oPayment.Exported)
                            '13.07.2015 - Changed the way to move in manual matching
                            sMySQL = sMySQL & " WHERE PaymentCounter = "
                            '01.07.2010 - Critical change!!!!!! Done for Odin
                            'This may cause problems for SISMO, but maybe not.
                            sMySQL = sMySQL & oPayment.PaymentCounter.ToString & " AND BabelFile_ID = " & oBabelFile.VB_DBBabelFileIndex.ToString


                            'sMySQL = sMySQL & " WHERE Unique_PaymentID = "
                            ''01.07.2010 - Critical change!!!!!! Done for Odin
                            ''This may cause problems for SISMO, but maybe not.
                            'sMySQL = sMySQL & oPayment.Unique_PaymentID.ToString & " AND BabelFile_ID = " & oBabelFile.VB_DBBabelFileIndex.ToString
                            'Old code
                            'sMySQL = sMySQL & Trim$(Str(oPayment.Unique_PaymentID)) & " AND BabelFile_ID = " & Trim$(Str(oBabelFile.Index))

                            oMyDal.SQL = sMySQL

                            If oMyDal.ExecuteNonQuery Then

                                If oMyDal.RecordsAffected = 1 Then
                                    'OK
                                Else
                                    nErrors = nErrors + 1
                                    bReturnValue = False
                                End If

                            Else

                                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

                            End If

                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

            If nErrors > 0 Then
                sErrorText = LRSCommon(45006, nErrors.ToString) '"The exportflag was not updated correct on some payments." & vbCrLf & "This will affect the status in the archive database." & vbCrLf & vbCrLf & "Number of payments not correctly updated: " & Trim(Str(nErrors))
            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            sErrorText = LRS(48013) & vbCrLf & ex.Message
            bReturnValue = False

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function
    Public Function AskForFromAccount(Optional ByRef sDefaultAccount As String = "", Optional ByRef oFilesetup As vbBabel.FileSetup = Nothing, Optional ByRef iExactLength As Short = 0, Optional ByRef sCDV As String = "") As String
        Dim sResponse As String
        Dim bContinue As Boolean
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim sAccountInFilesetup As String

        ' if oFilesetup is passed, then check if there is one, and only one, account in Clients.Account
        For Each oClient In oFilesetup.Clients
            For Each oaccount In oClient.Accounts
                If EmptyString(sAccountInFilesetup) Then
                    sAccountInFilesetup = oaccount.Account
                Else
                    If sAccountInFilesetup <> oaccount.Account Then
                        ' more than one account specified in clientsetup, do not default to any account;
                        sAccountInFilesetup = "SeveralAccounts"
                        Exit For
                    End If
                End If
            Next oaccount
            If sAccountInFilesetup = "SeveralAccounts" Then
                Exit For
            End If
        Next oClient

        If sAccountInFilesetup <> "SeveralAccounts" And Not EmptyString(sAccountInFilesetup) Then
            sDefaultAccount = sAccountInFilesetup
        End If

        Do Until bContinue
            bContinue = True
            sResponse = Trim(InputBox(LRSCommon(20027), "BabelBank", sDefaultAccount)) '"Please input DebitAccount"))
            If Len(sResponse) = 0 Then
                ' cancel pressed
                ' cut rest
                Exit Do
            End If

            If iExactLength = 0 Then
                ' Not set fixed length for accountno. Assume at least 5 digits in accountno
                If Len(Trim(sResponse)) < 5 Then
                    bContinue = False
                    If MsgBox(LRSCommon(20028), MsgBoxStyle.OkCancel, "BabelBank") = MsgBoxResult.Cancel Then 'error in accounts length
                        sResponse = ""
                        Exit Do
                    End If
                End If
            ElseIf Len(Trim(sResponse)) <> iExactLength Then
                bContinue = False
                If MsgBox(LRSCommon(20028), MsgBoxStyle.OkCancel, "BabelBank") = MsgBoxResult.Cancel Then 'error in accounts length
                    sResponse = ""
                    Exit Do
                End If
            End If

            If sCDV = "10" Then
                ' check for cdv10
                If Not IsModulus10(sResponse) Then
                    bContinue = False
                    If MsgBox(LRSCommon(20029), MsgBoxStyle.OkCancel, "BabelBank") = MsgBoxResult.Cancel Then 'error in cdv-check
                        sResponse = ""
                        Exit Do
                    End If
                End If
            ElseIf sCDV = "11" Then
                ' check for cdv11
                If Not IsModulus11(sResponse) Then
                    bContinue = False
                    If MsgBox(LRSCommon(20029), MsgBoxStyle.OkCancel, "BabelBank") = MsgBoxResult.Cancel Then 'error in cdv-check
                        sResponse = ""
                        Exit Do
                    End If
                End If
            End If
        Loop

        AskForFromAccount = sResponse
    End Function
    Public Function AskForDate(ByRef sDatetextInInputBox As String, Optional ByRef bValidDateStated_ReturnValue As Boolean = False, Optional ByRef sDefaultDate As String = "") As Date
        'sDefaultDate must be on format YYYYMMDD
        Dim bOutOfLoop As Boolean
        Dim sDate As String
        Dim dReturnDate As Date
        Dim sResponse As String
        Dim sMonth, sDay, sYear As String

        On Error GoTo ERR_AskForDate

        bOutOfLoop = False
        ' Ask for paymentdate:
        Do While bOutOfLoop = False

            If Len(Trim(sDefaultDate)) = 8 Then
                sDefaultDate = Trim(sDefaultDate)
                sDate = Right(sDefaultDate, 2) & "." & Mid(sDefaultDate, 5, 2) & "." & Left(sDefaultDate, 4)
            Else
                sDate = Trim(Str(VB.Day(Now))) & "." & Trim(Str(Month(Now))) & "." & Trim(Str(Year(Now)))
            End If

            bOutOfLoop = True
            sResponse = InputBox(LRSCommon(20030), sDatetextInInputBox, sDate)
            If sResponse = "" Then
                ' user pressed Cancel
                bOutOfLoop = False
                dReturnDate = DateSerial(CInt("1990"), CInt("01"), CInt("01"))
                bValidDateStated_ReturnValue = False
                Exit Do
            End If

            If Len(sResponse) - Len(Replace(sResponse, ".", "")) = 2 Then
                ' OK, two . in datestring:
                ' Test date given;
                sDay = Left(sResponse, InStr(sResponse, ".") - 1)
                sMonth = Mid(sResponse, InStr(sResponse, ".") + 1, 2)
                If Right(sMonth, 1) = "." Then
                    ' only one digit for month
                    sMonth = Left(sMonth, 1)
                End If
                sYear = Right(sResponse, 4)
                If Val(sYear) < 2000 Or Val(sYear) > 2099 Then
                    MsgBox(LRSCommon(20032) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                    'Erroneus year stated - Please try again
                    bOutOfLoop = False
                End If
                If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
                    MsgBox(LRSCommon(20033) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                    'Erroneus month stated - Please try again
                    bOutOfLoop = False
                End If
                If Val(sDay) < 1 Or Val(sDay) > 31 Then
                    MsgBox(LRSCommon(20034) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                    'Erroneus day stated - Please try again
                    bOutOfLoop = False
                End If
                If Val(sMonth) = 4 Or Val(sMonth) = 6 Or Val(sMonth) = 9 Or Val(sMonth) = 11 Then
                    If Val(sDay) > 30 Then
                        MsgBox(LRSCommon(20034) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                        'Erroneus day stated - Please try again
                        bOutOfLoop = False
                    End If
                End If
                If Val(sMonth) = 2 Then
                    If Val(sDay) > 29 Then
                        MsgBox(LRSCommon(20034) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                        'Erroneus day stated - Please try again
                        bOutOfLoop = False
                    End If
                End If
            Else
                MsgBox(LRSCommon(20035, "dd.mm.yyyy"), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                'Erroneus date stated! vbcrlf Use the format dd.mm.yyyy, when You enter the date
                bOutOfLoop = False
            End If

            If bOutOfLoop Then
                dReturnDate = DateSerial(CInt(sYear), CInt(sMonth), CInt(sDay))
                bValidDateStated_ReturnValue = True
            End If
        Loop


        AskForDate = dReturnDate

        On Error GoTo 0
        Exit Function

ERR_AskForDate:

        Err.Raise(Err.Number, "AskForDate", Err.Description)

    End Function
    Public Function ValidateDate(ByRef sDate As String, ByRef sFormat As String, ByRef bShowErrorMessage As Boolean, Optional ByRef sErrorText As String = "", Optional ByRef dFirstValidDate As Date = #1/1/2000#, Optional ByRef dLastValidDate As Date = #12/31/2099#) As Boolean
        '11.07.2008
        'This is a function to validate a date
        'It is not yet by far complete
        Dim sStandardDate As String = ""
        Dim sDay As String = "01"
        Dim sMonth As String = "01"
        Dim sYear As String = "2000"
        Dim sMessage As String = ""

        'If dFirstValidDate = System.DateTime.FromOADate(0) Then
        '    dFirstValidDate = DateSerial(2000, 1, 1)
        'End If
        'If dLastValidDate = System.DateTime.FromOADate(0) Then
        '    dLastValidDate = DateSerial(2099, 1, 1)
        'End If

        sDate = Trim(sDate)
        sErrorText = ""
        sFormat = UCase(sFormat)
        sMessage = ""

        If sFormat = "DD.MM.YYYY" Then
            If Len(sDate) - Len(Replace(sDate, ".", "")) = 2 Then
                ' OK, two . in datestring:
                ' Test date given;
                sDay = Left(sDate, InStr(sDate, ".") - 1)
                sMonth = Mid(sDate, InStr(sDate, ".") + 1, 2)
                If Right(sMonth, 1) = "." Then
                    ' only one digit for month
                    sMonth = Left(sMonth, 1)
                End If
                sYear = Right(sDate, 4)

                sStandardDate = PadLeft(sDay, 2, "0") & "." & PadLeft(sMonth, 2, "0") & "." & sYear
            Else
                sMessage = LRSCommon(20035, sFormat, sDate)
                'Erroneus date stated! vbcrlf Use the format dd.mm.yyyy, when You enter the date vbcrlf Remember to use full stop as separator!
            End If
        ElseIf sFormat = "YYYY-MM-DD" Then 'Added 20.04.2020
            If Not EmptyString(sDate) Then
                If sDate.Length = 10 Then
                    If Len(sDate) - Len(Replace(sDate, "-", "")) = 2 Then
                        ' OK, two . in datestring:
                        ' Test date given;
                        sDay = sDate.Substring(8, 2)
                        sMonth = sDate.Substring(5, 2)
                        sYear = sDate.Substring(0, 4)

                        sStandardDate = PadLeft(sDay, 2, "0") & "." & PadLeft(sMonth, 2, "0") & "." & sYear
                    Else
                        sMessage = LRSCommon(20035, sFormat, sDate)
                        'Erroneus date stated! vbcrlf Use the format dd.mm.yyyy, when You enter the date vbcrlf Remember to use full stop as separator!
                    End If
                Else
                    sMessage = LRSCommon(20035, sFormat, sDate)
                    'Erroneus date stated! vbcrlf Use the format dd.mm.yyyy, when You enter the date vbcrlf Remember to use full stop as separator!
                End If
            Else
                sMessage = LRSCommon(20035, sFormat, sDate)
                'Erroneus date stated! vbcrlf Use the format dd.mm.yyyy, when You enter the date vbcrlf Remember to use full stop as separator!
            End If

            '29.04.2020 - added YYYYMMDD
        ElseIf sFormat = "YYYYMMDD" Then
            If Len(sDate) = 8 Then
                ' Test date given;
                sDay = Right(sDate, 2)
                sMonth = Mid(sDate, 5, 2)
                sYear = Left(sDate, 4)

                sStandardDate = PadLeft(sDay, 2, "0") & "." & PadLeft(sMonth, 2, "0") & "." & sYear
            Else
                sMessage = LRSCommon(20035, sFormat)
                'Erroneus date stated! vbcrlf Use the format YYYMMDD, when You enter the date vbcrlf Remember to use full stop as separator!
            End If

            'Add other formats if You like and create a standarddate on the correct format, including sDay, sMonth and sYear


        End If

        If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
            sMessage = LRSCommon(20033) & " " & sMonth
            'Erroneus month stated
        End If
        If Val(sDay) < 1 Or Val(sDay) > 31 Then
            sMessage = LRSCommon(20034) & " " & sDay
            'Erroneus day stated
        End If
        If Val(sMonth) = 4 Or Val(sMonth) = 6 Or Val(sMonth) = 9 Or Val(sMonth) = 11 Then
            If Val(sDay) > 30 Then
                sMessage = LRSCommon(20034) & " " & sDay
                'Erroneus day stated
            End If
        End If
        If Val(sMonth) = 2 Then
            If Val(sDay) > 29 Then
                sMessage = LRSCommon(20034) & " " & sDay
                'Erroneus day stated
            End If
        End If
        If Not IsDate(sStandardDate) Then
            sMessage = LRSCommon(20036)
            'Erroneus date stated
        End If
        If StringToDate(sYear & sMonth & sDay) < dFirstValidDate Then
            sMessage = LRSCommon(20037)
            'The date stated is outside the valid span
        End If
        If StringToDate(sYear & sMonth & sDay) > dLastValidDate Then
            sMessage = LRSCommon(20037)
            'The date stated is outside the valid span
        End If

        If Not EmptyString(sMessage) Then
            If bShowErrorMessage Then
                MsgBox(sMessage & vbCrLf & vbCrLf & LRSCommon(20038, sDate), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRSCommon(20036))
                '+Date stated:
            Else
                sErrorText = sMessage & vbCrLf & vbCrLf & LRSCommon(20038, sDate)
            End If
            ValidateDate = False
        Else
            ValidateDate = True
        End If


    End Function

    Public Function FindBankSenderInEDIFile(ByRef sEDIString As String, ByRef sImportFilename As String, ByRef bImport As Boolean, ByRef sFormat As String, ByRef sReturnEDIVersion As String, ByRef bReturnTransmittedFromFDByRJE As Boolean, ByRef sReturnCountry As String) As vbBabel.BabelFiles.Bank
        Dim sFilename As String = ""
        Dim l As Integer
        Dim s As String
        Dim lStartPos, lNextSegmentPos As Integer
        'Dim sVersion As String
        Dim sGroupSep As String
        Dim sElementSep As String
        Dim bExit As Boolean
        Dim iNoOfTagGroups As Short
        Dim sChar As String
        Dim sSenderID = "", sMappingName As String
        Dim eBankInterpreted As vbBabel.BabelFiles.Bank
        Dim eBank As vbBabel.BabelFiles.Bank

        If bImport Then
            sEDIString = Replace(sEDIString, vbCr, "")
            sEDIString = Replace(sEDIString, vbLf, "")

            'Find the version
            lStartPos = InStr(1, sEDIString, "UNH")
            lStartPos = InStr(lStartPos, sEDIString, sFormat)
            sReturnEDIVersion = Mid(sEDIString, lStartPos + Len(sFormat) + 1, 1)
            sReturnEDIVersion = Trim(sReturnEDIVersion) & Trim(Mid(sEDIString, lStartPos + Len(sFormat) + 3, 3))
            lNextSegmentPos = lStartPos

            'Find the senders ID
            If Left(sEDIString, 3) = "UNA" Then
                sElementSep = Mid(sEDIString, 4, 1)
                sGroupSep = Mid(sEDIString, 5, 1)
            Else
                sElementSep = ":"
                sGroupSep = "+"
            End If
            lStartPos = InStr(1, sEDIString, "UNB")

            l = 1
            iNoOfTagGroups = 0
            bExit = False
            Do While Not bExit
                If Mid(sEDIString, lStartPos + l, 1) = sGroupSep Then
                    iNoOfTagGroups = iNoOfTagGroups + 1
                    If iNoOfTagGroups = 2 Then
                        bExit = True
                    Else
                        l = l + 1
                    End If
                Else
                    l = l + 1
                End If
            Loop

            lStartPos = lStartPos + l

            l = 1
            bExit = False
            Do While Not bExit
                sChar = Mid(sEDIString, lStartPos + l, 1)
                If sChar <> sElementSep And sChar <> sGroupSep Then
                    sSenderID = sSenderID & sChar
                    l = l + 1
                Else
                    bExit = True
                End If
            Loop



            Select Case sSenderID
                Case "00810506482"
                    lNextSegmentPos = InStr(lNextSegmentPos, sEDIString, "BGM")
                    If Mid(sEDIString, lNextSegmentPos, 3) = "BGM" Then
                        Select Case Mid(sEDIString, lNextSegmentPos + 4, 3)
                            Case "435" 'Preadvice of a credit
                                sMappingName = "BBS"
                                eBankInterpreted = vbBabel.BabelFiles.Bank.BBS
                            Case "455" ' Extended credit advice
                                lNextSegmentPos = InStr(1, sEDIString, "UNOC")
                                ' 22.03.06 Removed next if (KI+JanP)
                                ''''If Mid$(sEDIString, lNextSegmentPos + 5, 1) = "1" Then
                                '    sMappingName = "DNB"
                                '    sReturnCountry = "SE"
                                '    eBankInterpreted = DnBNOR_SE
                                'Else
                                sMappingName = "DNB"
                                eBankInterpreted = vbBabel.BabelFiles.Bank.DnB
                                'End If
                            Case Else
                                eBankInterpreted = vbBabel.BabelFiles.Bank.No_Bank_Specified
                                Err.Raise(vbObjectError + 15002, , LRS(15002, sSenderID))
                                '"Unknown sender of file"
                        End Select
                    Else
                        eBankInterpreted = vbBabel.BabelFiles.Bank.No_Bank_Specified
                        Err.Raise(vbObjectError + 15002, , LRS(15002, sSenderID))
                        '"Unknown sender of file"
                    End If

                Case "012"
                    sMappingName = "FD"
                    eBankInterpreted = vbBabel.BabelFiles.Bank.Fellesdata
                    If Len(sEDIString) > 80 Then
                        'If InStr(12, sEDIString, "'") = Len(Trim$(Left$(sEDIString, 80))) Then
                        If InStr(12, sEDIString, "'") = Len(Trim(Left(sEDIString, 80))) Then
                            'OK, The file is trasmitted by RJE
                            bReturnTransmittedFromFDByRJE = True
                        End If
                    End If
                Case "00008080"
                    sMappingName = "BBS"
                    eBankInterpreted = vbBabel.BabelFiles.Bank.BBS
                Case "NDEANOKKXXX"
                    sMappingName = "Nord"
                    eBankInterpreted = vbBabel.BabelFiles.Bank.Nordea_NO
                Case "5790000243440"
                    sMappingName = "DBNO" 'Danske Bank - norske format
                    eBankInterpreted = vbBabel.BabelFiles.Bank.Danske_Bank_NO
                Case "XIANNOKKXXX"
                    sMappingName = "BBS"
                    eBankInterpreted = vbBabel.BabelFiles.Bank.BBS
                    'Customer-specific ID's
                Case "107388520701"
                    sFormat = "PAYMUL"
                    sReturnEDIVersion = "FPOAAB2912"
                    sReturnCountry = "DE"
                    sMappingName = "DNV"
                    eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified
                Case "Ignis Photonyx A/S"
                    sFormat = "PAYMUL"
                    sReturnEDIVersion = "D96A"
                    sMappingName = "IGNIS"
                    sReturnCountry = "DK"
                    eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified
                Case "5790000706815", "KILROY TRAVELS INTERNATIONAL"
                    sFormat = "PAYMUL"
                    sReturnEDIVersion = "D96A"
                    sMappingName = "Berlingske"
                    sReturnCountry = "DK"
                    eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified
                Case "KRAFT FOODS"
                    sFormat = "PAYMUL"
                    sReturnEDIVersion = "D96A"
                    sMappingName = "KRAFT"
                    sReturnCountry = "DK"
                    eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified

                Case "DNBANOKK" ' Added 04.01.07, possible DnBNOR INPS
                    If InStr(sEDIString, "CREMUL:D:96A:UN:INPS05") > 0 Then
                        sMappingName = "DNBNORINPS"
                        eBankInterpreted = vbBabel.BabelFiles.Bank.DnB
                    End If

                Case Else

                    eBankInterpreted = vbBabel.BabelFiles.Bank.No_Bank_Specified
                    sMappingName = sSenderID
                    'err.Raise vbObjectError + 15002, , Replace(LRS(15002), "%1", sSenderID)
                    '"Unknown sender of file"
            End Select

            'Added 05.01.2006 by Kjell to be able to used the bank from the profile
            If eBank > vbBabel.BabelFiles.Bank.No_Bank_Specified Then
                If eBank <> eBankInterpreted Then
                    ' 22.03.06: Changes made
                    If (eBank = vbBabel.BabelFiles.Bank.DnB And eBankInterpreted = vbBabel.BabelFiles.Bank.BBS) Then
                        'Special situation because we aren't able to distinguish between DnBNOR Internal/BBS
                        '  Keep the eBank from the profile
                        eBank = vbBabel.BabelFiles.Bank.BBS
                        'BGMAX - Remove next ElseIF - 6 lines - comment is wrong
                    ElseIf (eBank = vbBabel.BabelFiles.Bank.DnBNOR_SE And eBankInterpreted = vbBabel.BabelFiles.Bank.DnB) Then
                        'Special situation because we aren't able to distinguish between DnBNOR Internal/BBS
                        '  Keep the eBank from the profile
                        sMappingName = "DNB"
                        sReturnCountry = "NO"
                        eBankInterpreted = vbBabel.BabelFiles.Bank.DnBNOR_SE
                        'BGMAX - Uncomment next ElseIF - 6 lines
                        '            ElseIf (eBank = DnB And eBankInterpreted = DnBNOR_SE) Then
                        '                'Special situation if the user has choosen DnBNOR as bank when the
                        '                ' file origins from Sweden
                        '                sMappingName = "DNB"
                        '                sReturnCountry = "SE"
                        '                eBankInterpreted = DnBNOR_SE
                    Else
                        'Special case, does not seperate between DnBNOR Internal and DnBNOR BBS
                        Err.Raise(vbObjectError + 15091, "FindMappingFileName", Replace(LRS(15091, GetEnumBank(eBank), GetEnumBank(eBankInterpreted)), "%3", sImportFilename))
                    End If


                    ' 22.03.06 - old code
                    'Special case, does not seperate between DnBNOR Internal and DnBNOR BBS
                    'If Not (eBank = DnB And eBankInterpreted = BBS) Then
                    '    err.Raise vbObjectError + 15091, "FindMappingFileName", Replace(LRS(15091, GetEnumBank(eBank), GetEnumBank(eBankInterpreted)), "%3", sImportFilename)
                    'Else
                    '    'Special situation because we aren't able to distinguish between DnBNOR Internal/BBS
                    '    '  Keep the eBank from the profile
                    '    eBank = BBS
                    'End If
                Else
                    'OK
                End If
            Else
                eBank = eBankInterpreted
            End If
        Else
            Select Case eBank
                Case vbBabel.BabelFiles.Bank.No_Bank_Specified
                    sMappingName = "BBS"
                Case vbBabel.BabelFiles.Bank.A_Norwegian_bank
                    sMappingName = "BBS"
                Case vbBabel.BabelFiles.Bank.DnB
                    sMappingName = "DNB"
                Case vbBabel.BabelFiles.Bank.DnBNOR_SE
                    sMappingName = "DNB"
                    sReturnCountry = "SE"
                Case vbBabel.BabelFiles.Bank.Nordea_NO
                    sMappingName = "CBK"
                Case vbBabel.BabelFiles.Bank.Gjensidige_NOR
                    sMappingName = "NOR" 'Danske Bank - norske format
                Case vbBabel.BabelFiles.Bank.BBS
                    sMappingName = "BBS"
                Case vbBabel.BabelFiles.Bank.Danske_Bank_NO
                    sMappingName = "DBNO"
                Case vbBabel.BabelFiles.Bank.SEB_NO
                    sMappingName = "SEBNO"
                Case Else
                    Err.Raise(vbObjectError + 15002, , LRS(15002, sSenderID))
                    '"Unknown sender of file"
            End Select

        End If

        FindBankSenderInEDIFile = CShort(sFilename)

    End Function
    Public Function ValidateKIDMAsk(ByRef sStringToValidate As String, ByRef sMask As String) As Boolean
        Dim iNoOfFixedCharacters As Short 'Any defined characters in fixed places (ex. F#####)
        Dim iStringLen As Short
        Dim l As Integer
        Dim sTmp As String

        'The array consist of:
        '0, = InvoiceNo
        '1, = Original invoice-index in the collection
        '2, = CustomerNo
        '3, = Amount

        ' Analyse mask
        iStringLen = Len(sStringToValidate)
        iNoOfFixedCharacters = Len(Replace(Replace(sMask, "#", ""), "$", ""))

        If Len(sMask) <> iStringLen Then
            ValidateKIDMAsk = False
            Exit Function
        End If

        'Test Fixed Characters
        l = 1
        If iNoOfFixedCharacters > 0 Then
            Do While l < iStringLen
                sTmp = Mid(sMask, l, 1)
                If InStr("$#", sTmp) = 0 Then
                    ' Fixed char in invoicedescription
                    If Mid(sStringToValidate, l, 1) <> sTmp Then
                        ' No match
                        ValidateKIDMAsk = False
                        Exit Function
                    End If
                End If
                l = l + 1
            Loop
        End If

        'Test numeric characters
        l = 1
        Do While l < iStringLen
            If Mid(sMask, l, 1) = "#" Then
                ' We require a numeric here, is textstring a numeric ?
                If Not IsNumeric(Mid(sStringToValidate, l, 1)) Then
                    ' No match
                    ValidateKIDMAsk = False
                    Exit Do
                End If
            End If
            l = l + 1
        Loop

        ValidateKIDMAsk = True

    End Function

    Public Function BankgirotStatusTxts(ByRef sStatusCode As String) As String
        Dim sTxt As String = ""

        Select Case sStatusCode
            Case "0000"
                sTxt = "Se f�reg�ende post"
            Case "0013"
                sTxt = "Mottagarens bankkonto �r felaktigt."
            Case "0014"
                sTxt = "Valutakoden �r felaktig. Angiven valuta �r ej till�ten."
            Case "0015"
                sTxt = "Beloppet �r inte numeriskt. Beloppet inneh�ller annat �n siffror."
            Case "0025"
                sTxt = "Bankgironumret �r avregistrerat."
            Case "0035"
                sTxt = "Bankgironumret saknar mottagarkonto. Kontot �r avslutat hos banken."
            Case "0038"
                sTxt = "Mottagarens bankkonto �r inte numeriskt."
            Case "0040"
                sTxt = "Bankgironummer saknar avs�ndarkonto."
            Case "0041"
                sTxt = "Betalningsuppdraget har kommit in f�r sent."
            Case "0042"
                sTxt = "Mottagarens bankgironummer �r felaktigt."
            Case "0043"
                sTxt = "Mottagarens clearingnummer �r felaktigt."
            Case "0044"
                sTxt = "Mottagarens bankgironummer �r inte numeriskt."
            Case "0046"
                sTxt = "Bankkonto saknas f�r kontoins�ttning."
            Case "0050"
                sTxt = "Mottagarposten inneh�ller inget bankgironummer eller utbetalarnummer."
            Case "0051"
                sTxt = "Utbetalningsnumret �r felaktigt."
            Case "0052"
                sTxt = "Felaktig valuta f�r kontantutbetalning."
            Case "0055"
                sTxt = "Betalning godk�nd, �vertalig(a) informationspost(er) avvisas."
            Case "0056"
                sTxt = "Avdrag/kredifaktura g�r ej att skicka till bankgironummer som tillh�r Skatteverket."
            Case "0057"
                sTxt = "Avdragsdag felaktig. Avdragsdagen saknas eller �r fel."
            Case "0058"
                sTxt = "F�rsta bevakningsdag felaktig."
            Case "0059"
                sTxt = "Sista bevakningsdag felaktig."
            Case "0064"
                sTxt = "Orimligt datum. Datum finns ej i kalendern."
            Case "0082"
                sTxt = "Stoppad vid t�ckningskontroll."
            Case "0110"
                sTxt = "Mottagarens namn och/eller adress saknas."
            Case "0124"
                sTxt = "Fel i OCR-numret; felaktig l�ngd."
            Case "0126"
                sTxt = "Kontoins�ttning till bank som ej �r ansluten till Bankgirot."
            Case "0130"
                sTxt = "Fel i OCR-numret; felaktig checksiffra. OCR-referensnumret �r fel."
            Case "0148"
                sTxt = "Avtal saknas f�r angiven valuta."
            Case "0152"
                sTxt = "Avdragspost avvisad, sista betalningsdag passerad."
            Case "0153"
                sTxt = "Ursprungligt belopp. Sista bevakningsdag �r uppn�dd."
            Case "0155"
                sTxt = "Betalning avvisad pga att avdragsbeloppet �r st�rre �n betalning eller olika kontonummer/adresser."
            Case "0156"
                sTxt = "Avdragsbelopp �r st�rre �n betalning eller olika kontonr/adresser."
            Case "0302"
                sTxt = "Obligatorisk transkod saknas. Giltiga transaktionskoder saknas."
            Case "0303"
                sTxt = "Posterna inkom i fel ordning."

        End Select
        BankgirotStatusTxts = sTxt
    End Function
    Public Function TelepayStatusTxts(ByRef sStatusCode As String) As String
        Dim sTxt As String = ""

        Select Case sStatusCode
            Case "-1"
                sTxt = "Bruker har slettet betalingen" 'Used in manual matching
            Case "0", "00"
                sTxt = "Filen er avvist, men ingen feil i denne recorden.."
            Case "1", "01"
                sTxt = "Mottaksretur"
            Case "2", "02"
                sTxt = "Avregningsretur"
            Case "10"
                sTxt = "Feil  i foretaksnummer."
            Case "11"
                sTxt = "Endring/sletting ikke tillatt."
            Case "12"
                sTxt = "Totalsum fakturaer/kreditnotaer m� ikke v�re mindre en 0."
            Case "13"
                sTxt = "L�penummer finnes ikke eller ugyldig."
            Case "14"
                sTxt = "Transaksjonstype kan ikke endres i BETFOR21."
            Case "15"
                sTxt = "Manglende debet/kredit kode"
            Case "16" ' added 03.05.06
                sTxt = "Blanding av strukturert og ustrukturert meldingsinformasjon ikke mulig"
            Case "17"
                sTxt = "Feil bruk av KID / ugyldig KID"
            Case "18"
                sTxt = "Mottaker krever gyldig KID"
            Case "19"
                sTxt = "Kredit kontonummer ikke gyldig."
            Case "20"
                sTxt = "Debet kontonummer ugyldig."
            Case "21"
                sTxt = "Feil i betalingsdato."
            Case "22"
                sTxt = "Ref.nummer finnes ikke eller ugyldig."
            Case "25"
                sTxt = "Utl�pt passord (passord for gammelt)"
            Case "26"
                sTxt = "Operat�r sperret."
            Case "27"
                sTxt = "Feil passord."
            Case "28"
                sTxt = "Operat�r ikke autorisert."
            Case "29"
                sTxt = "Ugyldig operat�rnummer."
            Case "30"
                sTxt = "Ugyldig versjonsnummer i Betfor00."
            Case "34"
                sTxt = "Feil i navn/adresse felt."
            Case "35"
                sTxt = "Feil valutasort"
            Case "36"
                sTxt = "Feil i avtalt kurs/terminkurs"
            Case "37"
                sTxt = "Feil i sjekk kode"
            Case "38"
                sTxt = "Feil i omkostningskode"
            Case "39"
                sTxt = "Feil i varslingsanvisning"
            Case "40"
                sTxt = "Feil i hasteangivelse"
            Case "41"
                sTxt = "Feil i betalingsartkode"
            Case "42"
                sTxt = "Feil i bel�p"
            Case "43"
                sTxt = "Betalingsartkode 'Bel�pet gjelder' m� fylles ut"
            Case "44"
                sTxt = "Feil mottagers landkode."
            Case "45"
                sTxt = "SWIFT-adresse feil utfylt"
            Case "46"
                sTxt = "Bankkode feil utfyllt"
            Case "47"
                sTxt = "Feil i produksjonsdato"


                ' de neste 6 er spesielle koder laget i BabelBank for kansellerte/slettede betalinger
                'B Kansellert av banken.                        70
                'D Kansellert p� grunn av manglende dekning.    71
                'F Foreldet uhevet Giro Utbetaling              72
                'K Mottakers konto er oppgjort                  73
                'O Kansellert on-line av kunde.                 74
                'S Kanselleringsmelding innsendt av kunde       75
            Case "70"
                sTxt = "Kansellert av banken."
            Case "71"
                sTxt = "Kansellert p� grunn av manglende dekning."
            Case "72"
                sTxt = "Foreldet uhevet Giro Utbetaling"
            Case "73"
                sTxt = "Mottakers konto er oppgjort"
            Case "74"
                sTxt = "Kansellert on-line av kunde. "
            Case "75"
                sTxt = "Kanselleringsmelding innsendt av kunde"

            Case "80"
                sTxt = "Feil i sekvenskontrollfelt."
            Case "81"
                sTxt = "Feil oppbygd batch."
            Case "82"
                sTxt = "Ugyldig transaksjonskode."
            Case "83"
                sTxt = "Seglfeil."
            Case "84"
                sTxt = "Ny sigilln�kkel mangler"
            Case "85"
                sTxt = "Feil oppbygd oppdrag"
            Case "86"
                sTxt = "BETFOR etterf�lges ikke av tilstrekkelig antall records."
            Case "87"
                sTxt = "Mangler/feil i BETFOR99"
            Case "88"
                sTxt = "Mangler/feil i BETFOR00"

            Case "89"
                sTxt = "Summeringsfeil av antall records innenfor en batch (BETFOR99)."
            Case "90"
                sTxt = "Sekvensfeil i AH-sekvens (applikasjonsheader)"
            Case "91"
                sTxt = "Ukjent AH-RUTINE-ID."
            Case "92"
                sTxt = "Feil i AH-TRANS-DATO."
            Case "95"
                sTxt = "Ugyldig divisjon"
            Case "101"
                sTxt = "Ugyldig klient"
            Case "110"
                sTxt = "Innbetaling er slettet"
            Case Else
                sTxt = "Ukjent feil"
        End Select
        TelepayStatusTxts = sTxt

    End Function
    ' XokNET 28.11.2013
    Public Function LevBetStatusTxts(ByVal sStatusCode As String) As String
        Dim sTxt As String

        Select Case sStatusCode
            Case "0", "00", "000", "0000"
                sTxt = "Filen er avvist, men ingen feil i denne recorden.."
            Case "0025"
                sTxt = "Betalningen inneh�ller ett f�r stort belopp"
            Case "0013"
                sTxt = "Mottagarens bankkonto �r felaktigt."
            Case "0014"
                sTxt = "Valutakoden �r felaktig eller mottagarens bank kan inte hantera valutan"
            Case "0015"
                sTxt = "Beloppet �r inte numeriskt."
            Case "0018"
                sTxt = "Beloppet �r noll"
            Case "0025"
                sTxt = "Bankgironumret �r avregistrerat."
            Case "0035"
                sTxt = "Bankgironumret saknar mottagarkonto."
            Case "0038"
                sTxt = "Mottagarens bankkonto �r inte numeriskt."
            Case "0041"
                sTxt = "Betalningsuppdraget har kommit in f�r sent."
            Case "0042"
                sTxt = "Mottagarens bankgironummer �r felaktigt."
            Case "0043"
                sTxt = "Mottagarens clearingnummer �r felaktigt."
            Case "0044"
                sTxt = "Mottagarens bankgironummer �r inte numeriskt."
            Case "0046"
                sTxt = "Bankkonto saknas f�r kontoins�ttning."
            Case "0050"
                sTxt = "Mottagare saknas."
            Case "0051"
                sTxt = "Utbetalningsnumret �r felaktigt."
            Case "0052"
                sTxt = "Felaktig valuta f�r kontantutbetalning."
            Case "0055"
                sTxt = "Betalning godk�nd, �vertalig(a) informationspost(er) avvisas."
            Case "0056"
                sTxt = "Avdrag/kredifaktura g�r ej att skicka till bankgironummer som tillh�r Skatteverket."
            Case "0057"
                sTxt = "Avdragsdag felaktig."
            Case "0058"
                sTxt = "F�rsta bevakningsdag felaktig."
            Case "0059"
                sTxt = "Sista bevakningsdag felaktig."
            Case "0064"
                sTxt = "Orimligt datum."
            Case "0081"
                sTxt = "Mottgrens pgnr saknas i Bankgirots register."
            Case "0082"
                sTxt = "Stoppad vid t�ckningskontroll. Kontakta din bank."
            Case "0110"
                sTxt = "Mottagarens namn och/eller adress saknas."
            Case "0111"
                sTxt = "Mottagarens PGnr �r inte numerisk."
            Case "0113"
                sTxt = "Felaktig PlusGirobetalning."
            Case "0124"
                sTxt = "Fel i OCR-numret; felaktig l�ngd."
            Case "0126"
                sTxt = "Kontoins�ttning till bank som ej �r ansluten till Bankgirot."
            Case "0130"
                sTxt = "Fel i OCR-numret; felaktig checksiffra."
            Case "0147"
                sTxt = "Felaktig valuta f�r PlusGirobetalning."
            Case "0148"
                sTxt = "Avtal saknas f�r angiven valuta."
            Case "0149"
                sTxt = "Avs�ndande bank saknar avtal f�r PlusGirokonto"
            Case "0152"
                sTxt = "Avdragspost avvisad, sista betalningsdag passerad."
            Case "0153"
                sTxt = "Ursprungligt belopp. Sista bevakningsdag �r uppn�dd."
            Case "0155"
                sTxt = "Betalning avvisad, pga efterkommande post."
            Case "0156"
                sTxt = "Avdragsbelopp �r st�rre �n betalning eller olika kontonr/adresser."
            Case "0302"
                sTxt = "Obligatorisk transkod saknas."
            Case "0303"
                sTxt = "Posterna inkom i fel ordning."
            Case Else
                sTxt = "Ukjend fel"
        End Select
        LevBetStatusTxts = sTxt

    End Function
    Public Function BANSTAStatusTxts(ByRef sStatusCode As String, Optional ByRef eSendingBank As vbBabel.BabelFiles.Bank = vbBabel.BabelFiles.Bank.No_Bank_Specified) As String
        Dim sTxt As String

        Select Case eSendingBank

            Case vbBabel.BabelFiles.Bank.DnBNOR_INPS
                Select Case Trim(sStatusCode)
                    Case "12"
                        sTxt = "The sender is not allowed to send the message type which was sent."
                    Case "13"
                        sTxt = "The message type is not supported by the recipient."
                    Case "14"
                        sTxt = "Message conveys information on an error."
                    Case "15"
                        sTxt = "Response after correction; correction has been approved."
                    Case "16"
                        sTxt = "Response after correction; correction has not been approved."
                    Case "32"
                        sTxt = "The Instruction Is Cancelled."
                        sTxt = sTxt & vbCrLf & "The payment may be processed OK, please contact Your bank."
                    Case "33"
                        sTxt = "This transaction is rejected by the recipient application and is a single transaction within a multiple transaction message."
                    Case "45"
                        sTxt = "The account number of the beneficiary is unknown."
                    Case "47"
                        sTxt = "The account number of the payor is unknown."
                    Case "48"
                        sTxt = "The correspondent bank is not a possible one."
                    Case "49"
                        sTxt = "The execution date cannot be met."
                    Case "51"
                        sTxt = "The currency code does not exist."
                    Case "73"
                        sTxt = "Direct Debit service code error/missing"
                    Case "75"
                        sTxt = "Error in used exchange rate"
                    Case "77"
                        sTxt = "Action code error/missing"
                    Case "78"
                        sTxt = "Processing code error/missing"
                    Case "79"
                        sTxt = "Date Error / missing"
                    Case "83"
                        sTxt = "Instruction on hold. Not sufficient funds."
                    Case "84"
                        sTxt = "Instruction rejected due to the lack of sufficient funds."
                    Case "100"
                        sTxt = "Instruction is cancelled manually"
                    Case "101"
                        sTxt = "Not authorized to debit this account"
                    Case "102"
                        sTxt = "Error or uncomplete name or address of the beneficiary or ordering customer"
                    Case "103"
                        sTxt = "The total amount of invoices and credit notes is less than 0"
                    Case "104"
                        sTxt = "KID Error"
                    Case "105"
                        sTxt = "KID missing"
                    Case "106" 'Not documented in the mapping
                        sTxt = "Invalid currencycode"
                    Case "107"
                        sTxt = "Error or missing country code"
                    Case "108"
                        sTxt = "Error or missing central reporting information"
                    Case "109"
                        sTxt = "Instruction is a duplicate"
                    Case "110"
                        sTxt = "Authorization is missing"
                    Case "111"
                        sTxt = "Mandate number error/missing"
                    Case "115"
                        sTxt = "Amount to be transferred is not equal to the total invoice amount"
                    Case "116"
                        sTxt = "Amount receivable Or payable Is missing"
                    Case "122"
                        sTxt = "Missing valid reference"
                    Case "123"
                        sTxt = "Remittance information missing"
                    Case "130"
                        sTxt = "Interchange is a duplicate"
                    Case "131"
                        sTxt = "Authorization cancelled by the bank"
                    Case "132"
                        sTxt = "Authorization cancelled by the payer"
                    Case "133"
                        sTxt = "Account type not approved"
                    Case "135"
                        sTxt = "Incorrect information on account or account holder"
                    Case "136"
                        sTxt = "Authorization cancelled by the ACH"
                    Case "137"
                        sTxt = "Cancelled by the ACH due to unanswered bank query"
                    Case "140"
                        sTxt = "Authorization has already been entered into the ACH's directory"
                    Case "141"
                        sTxt = "Revocation of cancellation of an authorization"
                    Case "142"
                        sTxt = "Incorrect or missing personal identification no."
                    Case "143"
                        sTxt = "Incorrect or missing payer no."
                    Case "144"
                        sTxt = "Incorrect maximum amount"
                    Case "145"
                        sTxt = "Incorrect recipient bankgiro no."
                    Case "146"
                        sTxt = "Recipient bankgiro no. is missing"
                    Case "ZZZ"
                        sTxt = "Mutually defined indicator."
                    Case Else
                        sTxt = "Unknown error. Please contact the sender."

                End Select

            Case Else 'Code for DnBNOR_INPS
                Select Case Trim(sStatusCode)
                    Case "12"
                        sTxt = "The sender is not allowed to send the message type which was sent."
                    Case "13"
                        sTxt = "The message type is not supported by the recipient."
                    Case "14"
                        sTxt = "Message conveys information on an error."
                    Case "15"
                        sTxt = "Response after correction; correction has been approved."
                    Case "16"
                        sTxt = "Response after correction; correction has not been approved."
                    Case "32"
                        sTxt = "The Instruction Is Cancelled"
                    Case "33"
                        sTxt = "This transaction is rejected by the recipient application and is a single transaction within a multiple transaction message."
                    Case "45"
                        sTxt = "The account number of the beneficiary is unknown."
                    Case "47"
                        sTxt = "The account number of the payor is unknown."
                    Case "48"
                        sTxt = "The correspondent bank is not a possible one."
                    Case "49"
                        sTxt = "The execution date cannot be met."
                    Case "51"
                        sTxt = "The currency code does not exist."
                    Case "73"
                        sTxt = "Direct Debit service code error/missing"
                    Case "75"
                        sTxt = "Error in used exchange rate"
                    Case "77"
                        sTxt = "Action code error/missing"
                    Case "78"
                        sTxt = "Processing code error/missing"
                    Case "79"
                        sTxt = "Date Error / missing"
                    Case "83"
                        sTxt = "Instruction on hold. Not sufficient funds."
                    Case "84"
                        sTxt = "Instruction rejected due to the lack of sufficient funds."
                    Case "100"
                        sTxt = "Instruction is cancelled manually"
                    Case "101"
                        sTxt = "Not authorized to debit this account"
                    Case "102"
                        sTxt = "Error or uncomplete name or address of the beneficiary or ordering customer"
                    Case "103"
                        sTxt = "The total amount of invoices and credit notes is less than 0"
                    Case "104"
                        sTxt = "KID Error"
                    Case "105"
                        sTxt = "KID missing"
                    Case "106"
                        sTxt = "Invalid currencycode"
                    Case "107"
                        sTxt = "Error or missing country code"
                    Case "108"
                        sTxt = "Error or missing central reporting information"
                    Case "109"
                        sTxt = "Instruction is a duplicate"
                    Case "110"
                        sTxt = "Authorization is missing"
                    Case "111"
                        sTxt = "Mandate number error/missing"
                    Case "115"
                        sTxt = "Amount to be transferred is not equal to the total invoice amount"
                    Case "116"
                        sTxt = "Amount receivable Or payable Is missing"
                    Case "122"
                        sTxt = "Missing valid reference"
                    Case "123"
                        sTxt = "Remittance information missing"
                    Case "130"
                        sTxt = "Interchange is a duplicate"
                    Case "131"
                        sTxt = "Authorization cancelled by the bank"
                    Case "132"
                        sTxt = "Authorization cancelled by the payer"
                    Case "133"
                        sTxt = "Account type not approved"
                    Case "135"
                        sTxt = "Incorrect information on account or account holder"
                    Case "136"
                        sTxt = "Authorization cancelled by the ACH"
                    Case "137"
                        sTxt = "Cancelled by the ACH due to unanswered bank query"
                    Case "140"
                        sTxt = "Authorization has already been entered into the ACH's directory"
                    Case "141"
                        sTxt = "Revocation of cancellation of an authorization"
                    Case "142"
                        sTxt = "Incorrect or missing personal identification no."
                    Case "143"
                        sTxt = "Incorrect or missing payer no."
                    Case "144"
                        sTxt = "Incorrect maximum amount"
                    Case "145"
                        sTxt = "Incorrect recipient bankgiro no."
                    Case "146"
                        sTxt = "Recipient bankgiro no. is missing"
                    Case "ZZZ"
                        sTxt = "Mutually defined indicator."
                    Case Else
                        sTxt = "Unknown error. Please contact the sender."
                End Select
        End Select

        BANSTAStatusTxts = sTxt

    End Function
    Public Function ISOtoTelepayErrorCodes(ByVal sISOErrorCode As String, Optional ByVal sStatusText As String = "", Optional ByVal iImportformat As vbBabel.BabelFiles.FileType = vbBabel.BabelFiles.FileType.Unknown) As String

        ' MANGLER:
        ' bruke statustext til � bestemme kode
        ' sjekke hvilke koder vi kan f� p� batch og babelfile


        ' Translates to and returns Telepay errorcodes, based on Pain.002 errorcodes
        Dim sReturn As String

        ' 08.06.2015 added defaultvalues dependant on inputformat    
        If iImportformat = vbBabel.BabelFiles.FileType.Pain002 Or iImportformat = vbBabel.BabelFiles.FileType.Pain002_Rejected Then
            sReturn = "01"
        Else
            sReturn = "02"
        End If



        ' 04.10.2017
        ' Several errorcodes (Nordea) starting with "DS", like DS0A, DS0B, .. to mark error in datasignature (SecureEnvelope, ...)
        ' 30.03.2020 - reorganized so ISO codes are sorted alpahbetically
        '            - added several codes
        If sISOErrorCode.Substring(0, 2) = "DS" Then
            ' set all errorcode with DS to Telepays 83 - Seglfeil
            sReturn = "83"
        Else

            Select Case sISOErrorCode.ToUpper
                Case "AC02", "AC05", "AC13"
                    '20	Invalid debit account number	AC02	Invalid Debtor Account Number
                    sReturn = "20"
                Case "AC03", "AC04", "AC06", "AC07", "AC12", "AC14"
                    '19	Invalid credit account number	AC03	Invalid Creditor Account Number
                    sReturn = "19"
                Case "ACSP"
                    '1	Batch ok received (R1).	ACSP	Accepted Settlement In Process
                    sReturn = "01"
                Case "AG02", "AG03", "FF07"
                    'InvalidBankOperationCode
                    sReturn = "82"
                Case "AG04", "AG05", "AG06", "BE09", "BE10", "BE11"
                    sReturn = "44"  ' Error receivers countrycode
                Case "AG08"
                    '28 Operat�r ikke autorisert.
                    sReturn = "28"
                Case "AM01", "AM06"
                    '12	Total amount of invoices/credit notes cannot be less than 0	AM01	Zero Amount
                    sReturn = "12"
                Case "AM02", "AM04", "AM07", "AM09", "AM12", "AM13", "AM14", "AM15", "AM16", "AM17"
                    '42 Feil i bel�p
                    sReturn = "42"
                Case "AM03", "AC09", "AC10", "AM11", "CURR"
                    'Not allowed currency AM03
                    '35	Incorrect currency code	NARR	Incorrect Format Or Currency For Payment Product
                    sReturn = "35"
                Case "AM05", "DU01", "DU02", "DUPL"
                    ' Duplication
                    sReturn = "90"  'Sekvensfeil
                Case "AM10"
                    'InvalidControlSum
                    '87 Mangler/feil i BETFOR99
                    sReturn = "87"
                Case "AM18", "AM19", "AM20"
                    '89 Summeringsfeil av antall records innenfor en batch (BETFOR99).
                    sReturn = "89"
                Case "BE04", "BE07", "BE08", "BE21", "BE22"
                    '34 Feil i navn/adresse felt.
                    sReturn = "34"
                Case "BE16", "BE17", "BE18"
                    '10	Erroneous enterprise number/customer number	BE16	Debtor or Ultimate Debtor identification code missing or invalid
                    sReturn = "10"
                Case "BE19"
                    '38	Invalid charges codes	BE19	InvalidChargeBearerCode
                    sReturn = "38"
                Case "CH07"
                    ' ElementIsNotToBeUsedAtB-andC-Level
                    '85 Feil oppbygd oppdrag
                    sReturn = "85"
                Case "DT01", "DT02", "DT03", "DT04", "DT05"
                    '21 Feil i betalingsdato.
                    sReturn = "21"
                Case "DT06"
                    ' added 02.10.2017
                    ' Date has been modified in order for transaction to proceed (Nordea error, for PALS first time)
                    sReturn = "01"
                Case "DU04", "DU05"
                    '80 Feil i sekvenskontrollfelt.
                    sReturn = "80"
                Case "FF01"
                    '85 Feil oppbygd oppdrag
                    sReturn = "85"
                Case "FF02"
                    '93	Freetext information exceeds the limits in the format	FF02	Syntax Error
                    sReturn = "93"
                Case "NARR", "99" '31.10.2017 Sometimes the errorcode is set to 99, which I hope is done when it is NARR, ex from DnB with NARR-error for SISMO 
                    ' NARR can be used in several errorsituations, use errortext to see if we can be more specific;
                    If sStatusText.ToUpper.Replace(" ", "") = "DT01xExecutionDateOutsideSpecifiedInterval".ToUpper Then
                        '21 Payment date invalid
                        sReturn = "21"
                        'ElseIf sStatusText.ToUpper.Replace(" ", "") = "ErrorOnAllCounterParties".ToUpper Then
                        'ElseIf sStatusText.ToUpper.Replace(" ", "") = "IncorrectFormatOrCurrencyForPaymentProduct".ToUpper Then
                    ElseIf sStatusText.ToUpper.Replace(" ", "") = "ChequeOrderIncorrect".ToUpper Then
                        '37	Invalid cheque code	NARR	Cheque Order Incorrect
                        sReturn = "37"
                        'ElseIf sStatusText.ToUpper.Replace(" ", "") = "CreditNotesNotAllowed".ToUpper Then
                        'ElseIf sStatusText.ToUpper.Replace(" ", "") = "ManualInterventionNeeded".ToUpper Then
                        'ElseIf sStatusText.ToUpper.Replace(" ", "") = "CdtrBankNotReachableForSCT".ToUpper Then
                        'ElseIf sStatusText.ToUpper.Replace(" ", "") = "KIDRefNotAllowed".ToUpper Then
                    ElseIf sStatusText.ToUpper.Replace(" ", "") = "KIDRefRequired".ToUpper Then
                        '18	Use of KID is mandatory for this beneficiary	NARR	KID Ref Required
                        sReturn = "18"
                        'ElseIf sStatusText.ToUpper.Replace(" ", "") = "ChequesNotAllowed".ToUpper Then
                        'ElseIf sStatusText.ToUpper.Replace(" ", "") = "MixURGPandNURGNotAllowed".ToUpper Then
                    ElseIf sStatusText.ToUpper.Replace(" ", "") = "CreditorAccountrequiresOCR/StructuredCreditorReference.".ToUpper Then
                        'Added 31.10.2017 - Added for DnB, SISMO is the customer
                        sReturn = "18"
                    ElseIf sStatusText.ToUpper.Replace(" ", "") = "PY01UnknownBICinroutingtable".ToUpper Then
                        '45	Error in SWIFT code field	NARR	PY01 Unknown BIC in routing table
                        sReturn = "45"
                    Else
                        ' added Else 08.06.2015 to always return an errorcode when NARR
                        sReturn = "85"    ' unknown
                    End If

                Case "RC01", "RC02", "RC03", "RC05", "RC06", "RC07" '25.05.2016 - Creditor BIC identifier is invalid or missing
                    '45  SWIFT-adresse feil utfylt
                    sReturn = "45"
                Case "RC04", "AC08", "RC08", "RC09", "RC10"
                    '46	Error in bank code	RC04	Invalid Creditor Bank Identifier
                    sReturn = "46"
                Case "RJCT"
                    '0	Batch rejected, but no errors in this record.	RJCT	Batch/file/transaction rejected
                    sReturn = "00"
                Case "RR01", "RR02", "RR03", "RR04", "RR06", "RR07", "RR09", "RR12"
                    '41	Invalid authority report code	RR04	Regulatory Reason
                    sReturn = "41"
                Case "RR05"
                    '43	Missing mandatory authority report free text for authority report code	RR05	Regulatory Information Invalid        
                    sReturn = "43"
               
                Case Else
                    ' N�rmeste vi kommer en diverse-feilmelding
                    '85 Feil oppbygd oppdrag
                    sReturn = "85"    ' unknown
            End Select
        End If

        ISOtoTelepayErrorCodes = sReturn

    End Function
    Public Function CalculateCurrencyUnits(ByRef dInRate As Double, ByRef sCurrencyISO As String, ByRef iOutUnit As Short, Optional ByRef iInUnit As Short = 0) As Double
        '-----------------------------------------------------------
        ' Recalculate currencyrates between different units, e.g. from rates pr 100 to rates pr 1 unit, or other way.
        ' Parameters:
        ' - dInRate (double)       Original currencyrate. Use localized decimalseparator (, in Norway, . in US)
        ' - sCurrencyISO (string)  ISO currencycode (USD, EUR, SEK, NOK etc.)
        ' - iOutUnit (integer)     100 = return currencyrate for each 100, 1 = return currencyrate for each 1 of currency
        '                          0 = return currencyrate "natural" for actual currency, that is 1 for USD, CAD, EUR and
        '                          GBP, 100 for all other currencies
        ' - iInUnit (integer)      100 = incoming currencyrate is for each 100, 1 = incoming currencyrate is for each 1 of currency
        '                          0 = incoming currencyrate is "natural" for actual currency, that is 1 for USD, CAD, EUR and
        '                          GBP, 100 for all other currencies
        '                          Optional, if not passed defaults to 0
        '
        ' Returns:
        ' - CurrencyRate (double)   Recalculated according to incoming parameters.
        '                           Example:
        '                           CalculateCurrencyUnits(89,4567, "SEK", 1)  returns 0,8945
        '                           CalculateCurrencyUnits(5.8793, "USD", 0)  returns 5.8793
        '                           CalculateCurrencyUnits(5.87931, "USD", 100, 1)  returns 587.9310

        ' Start by calculating inRate to one unit of actual currency;
        If iInUnit = 100 Then
            dInRate = dInRate / 100
        ElseIf iInUnit = 1 Then
            dInRate = dInRate
        Else
            ' 0 as iInUnit, that is 100 for all currencies but USD, EUR, GBP or CAD
            If sCurrencyISO <> "USD" And sCurrencyISO <> "EUR" And sCurrencyISO <> "GBP" And sCurrencyISO <> "CAD" Then
                dInRate = dInRate / 100
            End If
        End If

        ' Then recalculate to the unit we will like to return
        If iOutUnit = 100 Then
            CalculateCurrencyUnits = dInRate * 100
        ElseIf iOutUnit = 1 Then
            CalculateCurrencyUnits = dInRate
        Else
            ' iOutUnit = 0, return "natural" currencyrate, that is 100 for all currencies but USD, EUR, GBP or CAD
            If sCurrencyISO <> "USD" And sCurrencyISO <> "EUR" And sCurrencyISO <> "GBP" And sCurrencyISO <> "CAD" Then
                CalculateCurrencyUnits = dInRate * 100 ' for each 100
            Else
                CalculateCurrencyUnits = dInRate ' for each 1
            End If
        End If

    End Function
    Public Function ExportOnlyMatchedPayments(ByRef iCompany_ID As Short) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = False

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "SELECT ExportOnlyMatchedPayments FROM Company WHERE Company_ID = " & iCompany_ID.ToString
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    bReturnValue = CBool(oMyDal.Reader_GetString("ExportOnlyMatchedPayments"))

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: ExportOnlyMatchedPayments" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function


    Public Function RetrieveZipAndCityFromTheAddress(ByRef oPayment As vbBabel.Payment, Optional ByRef bCheckForCountryCode As Boolean = True) As Boolean
        'bCheckForCountryCode is used to check for zip stated like DK-8900   RANDERS
        'Returns True if a Zip is retrieved or already exists

        Dim bReturnValue As Boolean
        Dim iZipLength As Short
        Dim iCounter As Short
        Dim sAdr As String
        Dim bZipFound As Boolean

        On Error GoTo ERR_RetrieveZipAndCityFromTheAddress

        bReturnValue = False

        If EmptyString((oPayment.E_Zip)) Then

            bZipFound = False
            For iCounter = 1 To 3
                If iCounter = 1 Then
                    sAdr = Trim(oPayment.E_Adr3)
                ElseIf iCounter = 2 Then
                    sAdr = Trim(oPayment.E_Adr2)
                Else
                    sAdr = Trim(oPayment.E_Adr1)
                End If

                If Not EmptyString(sAdr) Then

                    'If stated like 8900   RANDERS
                    If vbIsNumeric(Left(sAdr, 4), "0123456789 -") Then
                        If vbIsNumeric(Mid(sAdr, 5, 1), "0123456789 -") Then
                            If vbIsNumeric(Mid(sAdr, 6, 1), "0123456789 -") Then
                                iZipLength = 6
                            Else
                                iZipLength = 5
                            End If
                        Else
                            iZipLength = 4
                        End If
                        oPayment.E_Zip = Trim(Left(sAdr, iZipLength))
                        If EmptyString((oPayment.E_City)) Then
                            oPayment.E_City = Trim(Mid(sAdr, iZipLength + 1))
                        End If
                        bReturnValue = True
                        'XokNET - 06.04.2011 - Added last part of ElseIf below
                    ElseIf vbIsNumeric(Mid$(sAdr, 4, 4), "0123456789 -") And Not vbIsNumeric(Mid$(sAdr, 3, 1), "0123456789") Then
                        If vbIsNumeric(Mid(sAdr, 9, 1), "0123456789 -") Then
                            If vbIsNumeric(Mid(sAdr, 10, 1), "0123456789 -") Then
                                iZipLength = 6
                            Else
                                iZipLength = 5
                            End If
                        Else
                            iZipLength = 4
                        End If
                        oPayment.E_Zip = Trim(Mid(sAdr, 4, iZipLength))
                        If EmptyString((oPayment.E_City)) Then
                            oPayment.E_City = Trim(Mid(sAdr, 3 + iZipLength + 1))
                        End If
                        bReturnValue = True
                    End If


                    If bReturnValue Then
                        'OK, We've found a zip, remove the content of the address used.
                        If iCounter = 1 Then
                            oPayment.E_Adr3 = ""
                        ElseIf iCounter = 2 Then
                            oPayment.E_Adr2 = ""
                        Else
                            oPayment.E_Adr1 = ""
                        End If

                        Exit For
                    End If

                End If
            Next iCounter


            'Old code
            '    ElseIf vbIsNumeric(Left$(Replace(Left$(oPayment.E_Adr2, 10), " ", ""), 5), "0123456789") Then
            '        oPayment.E_Zip = Left$(Replace(Left$(oPayment.E_Adr2, 10), " ", ""), 5)
            '        If EmptyString(oPayment.E_City) Then
            '            If InStr(1, Left$(oPayment.E_Adr2, 6), " ") > 0 Then
            '                oPayment.E_City = Trim$(Mid$(oPayment.E_Adr2, 7))
            '            Else
            '                oPayment.E_City = Trim$(Mid$(oPayment.E_Adr2, 6))
            '            End If
            '            oPayment.E_Adr2 = ""
            '        End If
            '        bReturnValue = True

        Else
            bReturnValue = True
        End If

        RetrieveZipAndCityFromTheAddress = bReturnValue

        On Error GoTo 0
        Exit Function

ERR_RetrieveZipAndCityFromTheAddress:

        RetrieveZipAndCityFromTheAddress = False

    End Function
    Public Function ArrayFormat(ByRef bFromAccountingSystem As Boolean, ByRef sPaymentType As String, ByRef bIncludeSpecialFormats As Boolean, ByRef DnBNORTBI As Boolean) As String(,)
        ' --------------------------------------------------------
        ' Creates a recordset from format-table
        ' Uses only formats FromAccountingSystem true or false
        ' returns array with Name and Format_Id from format-table
        ' --------------------------------------------------------
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iCounter As Integer = -1
        Dim aFormatArray(,) As String = Nothing  ' changed 11.10.2010 from object
        Dim sFormatName As String = ""

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ' Retrieve all formats from formattable:
            sMySQL = "Select * from Format order by Name"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    ' Fill up array with formats of right type
                    sFormatName = oMyDal.Reader_GetString("Name")
                    'If (bFromAccountingSystem = CBool(rsFormat.Fields("FromAccountingSystem").Value)) Or InStr(rsFormat.Fields("Name").Value, "OCR") > 0 Or InStr(rsFormat.Fields("Name").Value, "CREMUL") > 0 Or (bFromAccountingSystem = True And (InStr(rsFormat.Fields("Name").Value, "BETFOR00/99") > 0 Or InStr(rsFormat.Fields("Name").Value, "Telepay UNIX") > 0 Or InStr(rsFormat.Fields("Name").Value, "SRI Access Retur") > 0)) Or (bFromAccountingSystem = True And rsFormat.Fields("EnumID").Value = 999) Then
                    'If (bFromAccountingSystem = CBool(oMyDal.Reader_GetString("FromAccountingSystem"))) Or InStr(sFormatName, "OCR") > 0 Or InStr(sFormatName, "CREMUL") > 0 Or (bFromAccountingSystem = True And (InStr(sFormatName, "BETFOR00/99") > 0 Or InStr(sFormatName, "Telepay UNIX") > 0 Or InStr(sFormatName, "SRI Access Retur") > 0)) Or (bFromAccountingSystem = True And rsFormat.Fields("EnumID").Value = 999) Then
                    If (bFromAccountingSystem = CBool(oMyDal.Reader_GetString("FromAccountingSystem"))) Or InStr(sFormatName, "OCR") > 0 Or InStr(sFormatName, "CREMUL") > 0 Or (bFromAccountingSystem = True And (InStr(sFormatName, "BETFOR00/99") > 0 Or InStr(sFormatName, "Telepay UNIX") > 0 Or InStr(sFormatName, "SRI Access Retur") > 0)) Or (bFromAccountingSystem = True And CInt(oMyDal.Reader_GetString("EnumID")) = 999) Then

                        ' ta alltid med OCR og Cremul, Telepay 00/99. Enum 999 = ReportOnly
                        If sPaymentType = "" Or (sPaymentType = "out" And CBool(oMyDal.Reader_GetString("PaymentOut")) = True) Or (sPaymentType = "in" And CBool(oMyDal.Reader_GetString("PaymentOut")) = False) Then

                            If bIncludeSpecialFormats = True Or CInt(oMyDal.Reader_GetString("EnumID")) < 1000 Then
                                ' New 24.05.05 - For DnBNOR TBI, include only formats for TBI
                                If Not DnBNORTBI Or (DnBNORTBI And CBool(oMyDal.Reader_GetString("DnBNORTBI")) = True) Then
                                    iCounter = iCounter + 1
                                    ReDim Preserve aFormatArray(26, iCounter)
                                    If CInt(oMyDal.Reader_GetString("EnumID")) = 999 Then
                                        aFormatArray(0, iCounter) = UCase(LRS(60122)) ' Report Only
                                    Else
                                        aFormatArray(0, iCounter) = oMyDal.Reader_GetString("Name")
                                    End If
                                    aFormatArray(1, iCounter) = oMyDal.Reader_GetString("Format_ID")
                                    aFormatArray(2, iCounter) = oMyDal.Reader_GetString("InfoNewClient")
                                    aFormatArray(3, iCounter) = oMyDal.Reader_GetString("KeepBatch")
                                    aFormatArray(4, iCounter) = oMyDal.Reader_GetString("CtrlNegative")
                                    aFormatArray(5, iCounter) = oMyDal.Reader_GetString("CtrlZip")
                                    aFormatArray(6, iCounter) = oMyDal.Reader_GetString("CtrlAccount")
                                    aFormatArray(7, iCounter) = oMyDal.Reader_GetString("Country")
                                    aFormatArray(8, iCounter) = oMyDal.Reader_GetString("TextFileImp1")
                                    aFormatArray(9, iCounter) = oMyDal.Reader_GetString("TextFileImp2")
                                    aFormatArray(10, iCounter) = oMyDal.Reader_GetString("TextFileImp3")
                                    aFormatArray(11, iCounter) = oMyDal.Reader_GetString("EnumID")
                                    aFormatArray(12, iCounter) = oMyDal.Reader_GetString("FromAccountingSystem")
                                    aFormatArray(13, iCounter) = oMyDal.Reader_GetString("PaymentOut")
                                    aFormatArray(14, iCounter) = oMyDal.Reader_GetString("EnumID")
                                    aFormatArray(15, iCounter) = oMyDal.Reader_GetString("LabelFileImp1")
                                    aFormatArray(16, iCounter) = oMyDal.Reader_GetString("LabelFileImp2")
                                    aFormatArray(17, iCounter) = oMyDal.Reader_GetString("LabelFileImp3")
                                    aFormatArray(18, iCounter) = oMyDal.Reader_GetString("TextFileExp1")
                                    aFormatArray(19, iCounter) = oMyDal.Reader_GetString("TextFileExp2")
                                    aFormatArray(20, iCounter) = oMyDal.Reader_GetString("TextFileExp3")
                                    aFormatArray(21, iCounter) = oMyDal.Reader_GetString("LabelFileExp1")
                                    aFormatArray(22, iCounter) = oMyDal.Reader_GetString("LabelFileExp2")
                                    aFormatArray(23, iCounter) = oMyDal.Reader_GetString("LabelFileExp3")
                                    aFormatArray(24, iCounter) = oMyDal.Reader_GetString("Version")
                                    aFormatArray(25, iCounter) = oMyDal.Reader_GetString("Type")
                                End If 'If Not DnBNORTBI Or (DnBNORTBI And rsFormat!DnBNORTBI = True)
                            End If
                        End If
                    End If

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: ArrayFormat" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aFormatArray

    End Function
    Public Function RestoreSwapFiles(ByRef sCompanyID As String, ByRef bEmptyArray As Boolean, ByRef bManualSwapsExists As Boolean) As String(,)
        'Returns an Array with information about the setup for Swapfiles
        'Doesn't matter what value the calling function sends for the variable bEmptyArray,
        '  it is a returnfield which states if at least 1 entry exists
        '  'Neither bManualSwapsExists

        'The returnArray consists of the following elements:
        '0 = Name of rule
        '1 = Filename to import
        '2 = FormatID of the format to import
        '3 = If deviations are allowed, boolean
        '4 = If the swap should be done automatically set to True, boolean
        '5 = If an deviation exists post against this account
        '6 = Type of deviationaccount
        '7 = Name of the SQL to run to match the data from the importfile
        'This SQL must be stated under the option Other in the SQL Setup
        '8 = Enum-ID of the importFormat
        '9 = Do not remove until succesful export ' 16.07.2019

        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim aReturnArray(,) As String = Nothing
        Dim iCounter As Integer = -1

        Try
            bManualSwapsExists = False
            bEmptyArray = True
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ' Fill a recordset from SwapFile
            ' Assume just one record. May be extended later!
            sMySQL = "SELECT S.Name, S.Filename, S.FormatID, S.AllowDiff, S.SwapAuto, S.DiffAccount, S.DiffAccountType, S.SQLName, F.EnumID, S.RemoveAtExport FROM SwapFiles S, Format F WHERE Company_ID = " & sCompanyID & " AND S.FormatID = F.Format_ID"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then

                    Do While oMyDal.Reader_ReadRecord

                        bEmptyArray = False
                        iCounter = iCounter + 1

                        ReDim Preserve aReturnArray(9, iCounter)
                        aReturnArray(0, iCounter) = oMyDal.Reader_GetString("Name")
                        aReturnArray(1, iCounter) = oMyDal.Reader_GetString("Filename")
                        ' find format
                        aReturnArray(2, iCounter) = oMyDal.Reader_GetString("FormatID")

                        aReturnArray(3, iCounter) = oMyDal.Reader_GetString("AllowDiff")
                        aReturnArray(4, iCounter) = oMyDal.Reader_GetString("SwapAuto")
                        If aReturnArray(4, iCounter) = "False" Then
                            bManualSwapsExists = True
                        End If
                        aReturnArray(5, iCounter) = oMyDal.Reader_GetString("DiffAccount")
                        aReturnArray(6, iCounter) = oMyDal.Reader_GetString("DiffAccountType")
                        aReturnArray(7, iCounter) = oMyDal.Reader_GetString("SQLName")
                        aReturnArray(8, iCounter) = oMyDal.Reader_GetString("EnumID")
                        aReturnArray(9, iCounter) = oMyDal.Reader_GetString("RemoveAtExport")

                    Loop

                Else

                    bEmptyArray = True

                End If
            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            bEmptyArray = True
            bManualSwapsExists = False

            Throw New Exception("Function: RestoreSwapFiles" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aReturnArray

    End Function
    'TEST EXITCODE
    Public Function RetrieveManualMenuItem(ByRef sCompanyID As String, ByRef bEmptyArray As Boolean) As String()
        'Returns an Array with information about Menu items to use when you rightclick in gridERP
        'The SQL sorts the array according to the Qorder-field

        'The returnArray consists of the following elements:
        '0 = Qorder + Text to show in the grid

        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim aReturnArray() As String = Nothing
        Dim iCounter As Integer = -1

        Try

            bEmptyArray = True

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ' Fill the reader from OmitAccounts
            sMySQL = "SELECT QOrder, Name FROM ManualMenuItem WHERE Company_ID = " & sCompanyID & " Order By QOrder"

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord

                        bEmptyArray = False
                        iCounter = iCounter + 1
                        ReDim Preserve aReturnArray(iCounter)
                        aReturnArray(iCounter) = oMyDal.Reader_GetString("QOrder") & " - " & oMyDal.Reader_GetString("Name")
                    Loop

                Else
                    bEmptyArray = True
                End If

            Else

                bEmptyArray = True
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: RetrieveManualMenuItem" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aReturnArray

    End Function
    Public Function RetrieveManualMenuItemLine(ByRef sCompanyID As String, ByRef bEmptyArray As Boolean, ByRef sName As String) As String(,,)
        'Returns an Array with information about the postings to be done when this item is selected in rightclickmenu in gridERP

        'The returnArray consists of the following elements:
        '0 = PostingLine (All fields with identical PostingLines is one posting in gridMatched
        '1 = FieldName
        '2 = FieldValue
        '3 = FieldSource

        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim aReturnArray(,,) As String = Nothing
        Dim sManualMenuItemID As String
        Dim iOldPostingLine As Integer = -1
        Dim iMaxX As Integer = -1
        Dim iMaxY As Integer = -1
        Dim ixCounter As Integer = -1
        Dim iyCounter As Integer = -1
        Dim izCounter As Integer = -1
        Try

            bEmptyArray = True

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sManualMenuItemID = sName.Substring(0, 2).Trim

            'Find some values to find the correct dimenions to create the array
            'First number of different postingline
            sMySQL = "SELECT Count(*) AS Occurance FROM (SELECT PostingLine FROM ManualMenuItemPostingLine  WHERE Company_ID = 1 GROUP BY PostingLine) AS OK"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord
                        'iMaxX = CInt(oMyDal.Reader_GetString("Occurance"))
                        iMaxX = 3
                    Loop
                Else
                    bEmptyArray = True
                End If
            Else
                bEmptyArray = True
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
            End If

            'Max nubers of variables defined
            'sMySQL = "SELECT MAX(Antall) AS Occurance FROM (SELECT Count(*) AS Antall, PostingLine FROM ManualMenuItemPostingLine WHERE Company_ID = 1 GROUP BY PostingLine)"
            sMySQL = "SELECT MAX(Antall) AS Occurance FROM (SELECT Count(*) AS Antall, ManualMenuItemID, PostingLine FROM ManualMenuItemPostingLine WHERE Company_ID = 1 GROUP BY ManualMenuItemID, PostingLine) AS OK"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord
                        iMaxY = CInt(oMyDal.Reader_GetString("Occurance")) - 1
                    Loop
                Else
                    bEmptyArray = True
                End If
            Else
                bEmptyArray = True
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
            End If

            ' Fill the reader from OmitAccounts
            sMySQL = "SELECT PostingLine, FieldName, FieldValue, FieldSource FROM ManualMenuItemPostingLine WHERE Company_ID = " & sCompanyID & " AND ManualMenuItemID = " & sManualMenuItemID & " Order By PostingLine ASC"

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord

                        If iOldPostingLine <> CInt(oMyDal.Reader_GetString("PostingLine")) Then
                            iOldPostingLine = CInt(oMyDal.Reader_GetString("PostingLine"))
                            izCounter = izCounter + 1
                            iyCounter = 0
                            ReDim Preserve aReturnArray(iMaxX, iMaxY, izCounter)
                        Else
                            iyCounter = iyCounter + 1
                        End If

                        bEmptyArray = False
                        aReturnArray(0, iyCounter, izCounter) = oMyDal.Reader_GetString("PostingLine")
                        aReturnArray(1, iyCounter, izCounter) = oMyDal.Reader_GetString("FieldName")
                        aReturnArray(2, iyCounter, izCounter) = oMyDal.Reader_GetString("FieldValue")
                        aReturnArray(3, iyCounter, izCounter) = oMyDal.Reader_GetString("FieldSource")

                    Loop

                Else
                    bEmptyArray = True
                End If

            Else

                bEmptyArray = True
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: RetrieveManualMenuItem" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aReturnArray

    End Function
    Public Function RestoreOmitAccounts(ByRef sCompanyID As String, ByRef bEmptyArray As Boolean) As String(,)
        'Returns an Array with information about the accounts to Omit
        'Doesn't matter what value the calling function sends for the variable bEmptyArray,
        '  it is a returnfield which states if at least 1 entry exists

        'The returnArray consists of the following elements:
        '0 = Accountnumber
        '1 = Automtaic matching - boolean value
        '2 = After matching - boolean value

        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim aReturnArray(,) As String = Nothing
        Dim iCounter As Integer = -1

        Try
            bEmptyArray = True

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ' Fill the reader from OmitAccounts
            sMySQL = "SELECT AccountNo, Automatic, After FROM OmitAccounts F WHERE Company_ID = " & sCompanyID

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord

                        bEmptyArray = False
                        iCounter = iCounter + 1
                        ReDim Preserve aReturnArray(2, iCounter)
                        aReturnArray(0, iCounter) = oMyDal.Reader_GetString("AccountNo")
                        aReturnArray(1, iCounter) = oMyDal.Reader_GetString("Automatic")
                        aReturnArray(2, iCounter) = oMyDal.Reader_GetString("After")

                    Loop

                Else
                    bEmptyArray = True
                End If

            Else

                bEmptyArray = True
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: RestoreOmitAccounts" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aReturnArray

    End Function
    Public Function RunShell(ByRef cmdline As String) As Short

        Dim hProcess As Integer
        Dim ProcessId As Integer
        Dim exitCode As Integer

        ProcessId = Shell(cmdline, 1)
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION, False, ProcessId)

        Do

            Call GetExitCodeProcess(hProcess, exitCode)
            If exitCode = 0 Then
                Exit Do
            End If
            System.Windows.Forms.Application.DoEvents()

        Loop While exitCode = STATUS_PENDING

        Call CloseHandle(hProcess)

        'MsgBox "The shelled process " & cmdline & " has ended."
        RunShell = exitCode

    End Function
    Public Function RetrieveInfoFromLindorffPayment(ByRef oPayment As vbBabel.Payment, ByRef sSaksnummer As String, ByRef sSaksnavn As String, ByRef sCL_AccountNo As String, ByRef sBY_AccountNo As String, ByRef sHovedstol As String, ByRef sRenter As String, ByRef sProvisjon As String, ByRef sGebyr As String, ByRef sMVA As String) As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim oFreeText As vbBabel.Freetext
        Dim iCounter, iCounter2 As Short
        Dim iPosition, iLength As Short
        Dim aMessage(10, 0) As String
        Dim sTemp, sTemp2 As String

        On Error GoTo errRetrieveInfoFromLindorffPayment

        If oPayment.Invoices.Count = 1 Then
            For Each oInvoice In oPayment.Invoices
                iCounter = 0
                For iCounter = 1 To oInvoice.Freetexts.Count

                    oFreeText = oInvoice.Freetexts.Item(iCounter)

                    aMessage(iCounter - 1, 0) = oFreeText.Text

                Next iCounter
            Next oInvoice

            For iCounter = 0 To UBound(aMessage)
                'First line may be added by BB as structured info
                If Left(aMessage(iCounter, 0), 6) = "Bel�p:" Then
                    'Nothing to do
                Else
                    If EmptyString(aMessage(iCounter, 0)) Then
                        'Nothing to do
                    ElseIf Left(aMessage(iCounter, 0), 3) = "SAK" Then
                        sTemp = Trim(Mid(aMessage(iCounter, 0), 4, 12))
                        sSaksnummer = ""
                        For iCounter2 = 1 To Len(sTemp)
                            If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789") Then
                                sSaksnummer = sSaksnummer & Mid(sTemp, iCounter2, 1)
                            Else
                                'Find the name of the skyldner. It's stated after the '-' after the saksnr.
                                sSaksnavn = Trim(Mid(aMessage(iCounter, 0), InStr(3 + Len(sSaksnummer), aMessage(iCounter, 0), "-") + 1))
                                Exit For
                            End If
                        Next iCounter2
                    ElseIf Left(aMessage(iCounter, 0), 3) = "REF" Then
                        iPosition = InStr(4, aMessage(iCounter, 0), "-")
                        sTemp = Mid(aMessage(iCounter, 0), 4, iPosition - 4)
                        For iCounter2 = 1 To Len(sTemp)
                            If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789") Then
                                sCL_AccountNo = sCL_AccountNo & Mid(sTemp, iCounter2, 1)
                            End If
                        Next iCounter2
                        sTemp = Trim(Mid(aMessage(iCounter, 0), iPosition + 1))
                        For iCounter2 = 1 To Len(sTemp)
                            If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789") Then
                                sBY_AccountNo = sBY_AccountNo & Mid(sTemp, iCounter2, 1)
                            Else
                                Exit For 'By_account must be numeric
                            End If
                        Next iCounter2
                    ElseIf Left(Trim(aMessage(iCounter, 0)), 6) = "AVDRAG" Or Left(Trim(aMessage(iCounter, 0)), 5) = "RETUR" Then
                        sTemp = Trim(aMessage(iCounter, 0)) & Trim(aMessage(iCounter + 1, 0))
                        'Remove the text AVDRAG and RETUR
                        sTemp = Replace(sTemp, "AVDRAG", "", , , CompareMethod.Text)
                        sTemp = Replace(sTemp, "RETUR", "", , , CompareMethod.Text)
                        iPosition = 0
                        'The downpayment of the hovedstol is marked with H.
                        iPosition = InStr(1, sTemp, "H.", CompareMethod.Binary)
                        If iPosition > 0 Then
                            sTemp2 = ""
                            For iCounter2 = iPosition + 2 To Len(sTemp)
                                If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789, -") Then
                                    sTemp2 = sTemp2 & Mid(sTemp, iCounter2, 1)
                                Else
                                    Exit For
                                End If
                            Next iCounter2
                            sHovedstol = Replace(sTemp2, " ", "")
                            'Remove hovedstol from the text
                            sTemp = Replace(sTemp, sHovedstol, "", iPosition + 1, 1, CompareMethod.Text)
                            sTemp = Replace(sTemp, "H.", "", , , CompareMethod.Binary)
                        End If
                        'The downpayment of the interest is marked with R.
                        iPosition = InStr(1, sTemp, "R.", CompareMethod.Binary)
                        If iPosition > 0 Then
                            sTemp2 = ""
                            For iCounter2 = iPosition + 2 To Len(sTemp)
                                If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789, -") Then
                                    sTemp2 = sTemp2 & Mid(sTemp, iCounter2, 1)
                                Else
                                    Exit For
                                End If
                            Next iCounter2
                            sRenter = Replace(sTemp2, " ", "")
                            'Remove interest from the text
                            sTemp = Replace(sTemp, sRenter, "", iPosition + 1, 1, CompareMethod.Text)
                            sTemp = Replace(sTemp, "R.", "", , , CompareMethod.Binary)
                        End If
                        'The downpayment of the provision is marked with P.
                        iPosition = InStr(1, sTemp, "P.", CompareMethod.Binary)
                        If iPosition > 0 Then
                            sTemp2 = ""
                            For iCounter2 = iPosition + 2 To Len(sTemp)
                                If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789, -") Then
                                    sTemp2 = sTemp2 & Mid(sTemp, iCounter2, 1)
                                Else
                                    Exit For
                                End If
                            Next iCounter2
                            sProvisjon = Replace(sTemp2, " ", "")
                            'Remove provision from the text
                            sTemp = Replace(sTemp, sProvisjon, "", iPosition + 1, 1, CompareMethod.Text)
                            sTemp = Replace(sTemp, "P.", "", , , CompareMethod.Binary)
                        End If
                        'The downpayment of the charges is marked with G.
                        iPosition = InStr(1, sTemp, "G.", CompareMethod.Binary)
                        If iPosition > 0 Then
                            sTemp2 = ""
                            For iCounter2 = iPosition + 2 To Len(sTemp)
                                If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789, -") Then
                                    sTemp2 = sTemp2 & Mid(sTemp, iCounter2, 1)
                                Else
                                    Exit For
                                End If
                            Next iCounter2
                            sGebyr = Replace(sTemp2, " ", "")
                            'Remove charges from the text
                            sTemp = Replace(sTemp, sGebyr, "", iPosition + 1, 1, CompareMethod.Text)
                            sTemp = Replace(sTemp, "G.", "", , , CompareMethod.Binary)
                        End If
                        'The downpayment of the VAT is marked with M.
                        iPosition = InStr(1, sTemp, "M.", CompareMethod.Binary)
                        If iPosition > 0 Then
                            sTemp2 = ""
                            For iCounter2 = iPosition + 2 To Len(sTemp)
                                If vbIsNumeric(Mid(sTemp, iCounter2, 1), "0123456789, -") Then
                                    sTemp2 = sTemp2 & Mid(sTemp, iCounter2, 1)
                                Else
                                    Exit For
                                End If
                            Next iCounter2
                            sMVA = Replace(sTemp2, " ", "")
                            'Remove VAT from the text
                            sTemp = Replace(sTemp, sMVA, "", iPosition + 1, 1, CompareMethod.Text)
                            sTemp = Replace(sTemp, "M.", "", , , CompareMethod.Binary)
                        End If

                        iCounter = iCounter + 1

                    End If
                End If

            Next iCounter


            '************** NB ****************************
            'The following code must be run for DnBNORFinans
            'If sSpecial = "DNBNORFINANS_CREMUL" then
            '        For Each oInvoice In oPayment.Invoices
            '            oInvoice.Extra1 = sSaksnummer & ";" & sSaksnavn & ";" & sCL_AccountNo & ";" & sBY_AccountNo & ";" & sHovedstol & ";" & sRenter & ";" & sProvisjon & ";" & sGebyr & ";" & sMVA
            '            oInvoice.CustomerNo = "LINDORFF"
            '        Next oInvoice
            'End if
            '************** NB ****************************

        End If

        RetrieveInfoFromLindorffPayment = True

        Exit Function

errRetrieveInfoFromLindorffPayment:

        '07.10.2009 - Don't raise an error
        'Just continue
        'Err.Raise Err.Number, "RetrieveInfoFromLindorffPayment", Err.Description
        On Error GoTo 0

        RetrieveInfoFromLindorffPayment = False

    End Function
    'New 05.01.2010
    Public Function CheckForStatistics(ByRef iCompany_ID As Short) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = False

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "Select ActivateStatistics FROM Match_Manual WHERE Company_ID = " & iCompany_ID.ToString
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    bReturnValue = CBool(oMyDal.Reader_GetString("ActivateStatistics"))

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            bReturnValue = False

            Throw New Exception("Function: CheckForStatistics" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function
    'New 05.01.2010
    Public Function UseCommit(ByRef iCompany_ID As Short) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "Select UseCommitTrans FROM Match_Manual WHERE Company_ID = " & iCompany_ID.ToString
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                If oMyDal.Reader_HasRows Then

                    Do While oMyDal.Reader_ReadRecord

                        bReturnValue = CBool(oMyDal.Reader_GetString("UseCommitTrans"))

                    Loop

                Else

                    bReturnValue = True

                End If

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            bReturnValue = True

            Throw New Exception("Function: UseCommit" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

        Exit Function

    End Function

    Public Function TransformCountryCodes(ByRef oBatches As vbBabel.Batches, ByRef bTransformE_Countrycode As Boolean, ByRef bTransformBankCountrycode As Boolean, ByRef iTypeOfConversion As Short) As Boolean
        '18.03.2010
        'This function transform countrycodes from one sort to another
        'The only one implmented is from three letter to two letter

        'iTypeOfConversion has the following values
        '1 = From three letter to two letter
        '2 = From two letter to three letter

        Dim docXMLCountrycodes As New System.Xml.XmlDocument
        'Dim nsMgr As System.Xml.XmlNamespaceManager
        'Dim root As System.Xml.XmlNode
        Dim nodCountryList As System.Xml.XmlNodeList
        Dim nodCountry As System.Xml.XmlNode
        Dim oFs As Scripting.FileSystemObject
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim sXPathCommand As String
        Dim sNewCode As String

        Try

            'First find the XML-mappingfile
            oFs = New Scripting.FileSystemObject  '22.05.2017 CreateObject ("Scripting.FileSystemObject")

            If RunTime() Then   'DISKUTERNOTRUNTIME flytte til felles mappe?
                If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\Countrycodes.xml") Then
                    Throw New Exception("TransformCountryCodes" & vbCrLf & "Kan ikke finne filen Countrycodes.xml, " & vbCrLf & "som benyttes til konvertering av landkoder.")
                Else
                    docXMLCountrycodes.Load(My.Application.Info.DirectoryPath & "\Mapping\Countrycodes.xml")
                End If
            Else
                If Not oFs.FileExists("c:\Projects\BabelBank\BabelBankEXE\Mapping\Countrycodes.xml") Then
                    Throw New Exception("TransformCountryCodes" & vbCrLf & "Kan ikke finne filen Countrycodes.xml, " & vbCrLf & "som benyttes til konvertering av landkoder.")
                Else
                    docXMLCountrycodes.Load("c:\Projects\babelbank\BabelBankEXE\Mapping\Countrycodes.xml")
                End If
            End If

            nodCountryList = docXMLCountrycodes.SelectNodes("//countries/country")
            'Set nodCountry = Nothing
            'Set nodCountry = docXMLCountrycodes.selectSingleNode("//countries/country")


            'UNB/TG010[T0001= 'UNOC']

            If nodCountryList.Count > 0 Then
                Select Case iTypeOfConversion

                    Case 1 'Convert from 3 letter to 2 letter
                        For Each oBatch In oBatches
                            For Each oPayment In oBatch.Payments
                                If bTransformE_Countrycode Then
                                    'XNET - 24.11.2010 - Added next if
                                    If Not EmptyString(oPayment.E_CountryCode) Then
                                        sXPathCommand = ""
                                        sXPathCommand = "//countries/country[alpha3 = '" & oPayment.E_CountryCode & "']"
                                        nodCountry = docXMLCountrycodes.SelectSingleNode(sXPathCommand)

                                        'XNET 02.10.2012 - Added next IF
                                        If nodCountry Is Nothing Then
                                            Throw New Exception("TransformCountryCodes" & vbCrLf & "Finner ikke landkoden " & oPayment.E_CountryCode & " i landkodemappingen." & vbCrLf & "Kontakt Visual Banking for ny landkodefil (\Mapping\Countrycodes.xml)")
                                        Else
                                            sNewCode = nodCountry.SelectSingleNode("child::*[name()='alpha2']").InnerText
                                        End If

                                        If Not nodCountry Is Nothing Then
                                            If Len(sNewCode) = 2 Then
                                                oPayment.E_CountryCode = sNewCode
                                            End If

                                            nodCountry = Nothing
                                        End If
                                    End If
                                End If
                                If bTransformBankCountrycode Then
                                    'XNET - 24.11.2010 - Added next if
                                    If Not EmptyString(oPayment.BANK_CountryCode) Then
                                        sXPathCommand = ""
                                        sXPathCommand = "//countries/country[alpha3 = '" & oPayment.BANK_CountryCode & "']"
                                        nodCountry = docXMLCountrycodes.SelectSingleNode(sXPathCommand)

                                        If Not nodCountry Is Nothing Then
                                            sNewCode = nodCountry.SelectSingleNode("child::*[name()='alpha2']").InnerText

                                            If Len(sNewCode) = 2 Then
                                                oPayment.BANK_CountryCode = sNewCode
                                            End If

                                            nodCountry = Nothing
                                        End If
                                    End If
                                End If
                            Next oPayment
                        Next oBatch

                    Case 2 'Convert from 2 letter to 3 letter
                        For Each oBatch In oBatches
                            For Each oPayment In oBatch.Payments
                                If bTransformE_Countrycode Then
                                    'XNET - 24.11.2010 - Added next if
                                    If Not EmptyString(oPayment.E_CountryCode) Then
                                        sXPathCommand = ""
                                        sXPathCommand = "//countries/country[alpha2 = '" & oPayment.E_CountryCode & "']"
                                        nodCountry = docXMLCountrycodes.SelectSingleNode(sXPathCommand)

                                        If nodCountry Is Nothing Then
                                            Throw New Exception("TransformCountryCodes" & vbCrLf & "Finner ikke landkode for " & oPayment.E_CountryCode & vbCrLf & "Kontakt Visual Banking for ny landkodefil (\Mapping\Countrycodes.xml)")
                                        Else
                                            sNewCode = nodCountry.SelectSingleNode("child::*[name()='alpha3']").InnerText
                                        End If

                                        If Not nodCountry Is Nothing Then
                                            If Len(sNewCode) = 3 Then
                                                oPayment.E_CountryCode = sNewCode
                                            End If

                                            nodCountry = Nothing
                                        End If
                                    End If
                                End If
                                If bTransformBankCountrycode Then
                                    'XNET - 24.11.2010 - Added next if
                                    If Not EmptyString(oPayment.BANK_CountryCode) Then
                                        sXPathCommand = ""
                                        sXPathCommand = "//countries/country[alpha2 = '" & oPayment.BANK_CountryCode & "']"
                                        nodCountry = docXMLCountrycodes.SelectSingleNode(sXPathCommand)

                                        If Not nodCountry Is Nothing Then
                                            sNewCode = nodCountry.SelectSingleNode("child::*[name()='alpha3']").InnerText

                                            If Len(sNewCode) = 3 Then
                                                oPayment.BANK_CountryCode = sNewCode
                                            End If

                                            nodCountry = Nothing
                                        End If
                                    End If

                                End If
                            Next oPayment
                        Next oBatch

                    Case Else


                End Select

            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: TransformCountryCodes" & vbCrLf & ex.Message, ex, oPayment, "", "")

        Finally

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If
            If Not nodCountryList Is Nothing Then
                nodCountryList = Nothing
            End If
            If Not nodCountry Is Nothing Then
                nodCountry = Nothing
            End If
            If Not docXMLCountrycodes Is Nothing Then
                docXMLCountrycodes = Nothing
            End If

        End Try

        TransformCountryCodes = True

    End Function

    Public Function UpdateLabels(ByRef oMyDal As vbBabel.DAL, ByVal sCompany_ID As String) As Boolean
        Dim bReturnValue As Boolean = False
        Dim sMySQL As String
        Dim sLRS_ID As String
        Dim sDefaultLabel As String
        Dim iCounter As Integer

        Try

            For iCounter = 1 To 27

                Select Case iCounter
                    Case 1 'Kundenummer
                        sLRS_ID = "41001"
                        sDefaultLabel = LRSCommon(41001)

                    Case 2 'Fakturanummer
                        sLRS_ID = "41002"
                        sDefaultLabel = LRSCommon(41002)

                    Case 3 'Eksternt klientnummer
                        sLRS_ID = "41011"
                        sDefaultLabel = LRSCommon(41011)

                    Case 4 'Leverand�rnummer
                        sLRS_ID = "41008"
                        sDefaultLabel = LRSCommon(41008)

                    Case 5 'Currency
                        sLRS_ID = "41005"
                        sDefaultLabel = LRSCommon(41005)

                    Case 6 'ExchangeRate
                        sLRS_ID = "41006"
                        sDefaultLabel = LRSCommon(41006)

                    Case 7 'General Ledger
                        sLRS_ID = "41007"
                        sDefaultLabel = LRSCommon(41007)

                    Case 8 'VoucherNo
                        sLRS_ID = "41009"
                        sDefaultLabel = LRSCommon(41009)

                    Case 9 'Name
                        sLRS_ID = "41012"
                        sDefaultLabel = LRSCommon(41012)

                    Case 10 'Freetext
                        sLRS_ID = "41010"
                        sDefaultLabel = LRSCommon(41010)

                    Case 11 'Currency amount
                        sLRS_ID = "41023"
                        sDefaultLabel = LRSCommon(41023)

                    Case 12 'Currency2
                        sLRS_ID = "41024"
                        sDefaultLabel = LRSCommon(41024)

                    Case 13 'Discount
                        sLRS_ID = "41020"
                        sDefaultLabel = LRSCommon(41020)

                    Case 14 'Backpayment
                        sLRS_ID = "41025"
                        sDefaultLabel = LRSCommon(41025)

                    Case 15 'MyField
                        sLRS_ID = "15"
                        sDefaultLabel = "MyField"

                    Case 16 'MyField2
                        sLRS_ID = "16"
                        sDefaultLabel = "MyField2"

                    Case 17 'MyField3
                        sLRS_ID = "17"
                        sDefaultLabel = "MyField3"

                    Case 18 'Dim1
                        sLRS_ID = "18"
                        sDefaultLabel = "Dim1"

                    Case 19 'Dim2
                        sLRS_ID = "19"
                        sDefaultLabel = "Dim2"

                    Case 20 'Dim3
                        sLRS_ID = "20"
                        sDefaultLabel = "Dim3"

                    Case 21 'Dim4
                        sLRS_ID = "21"
                        sDefaultLabel = "Dim4"

                    Case 22 'Dim5
                        sLRS_ID = "22"
                        sDefaultLabel = "Dim5"

                    Case 23 'Dim6
                        sLRS_ID = "23"
                        sDefaultLabel = "Dim6"

                    Case 24 'Dim7
                        sLRS_ID = "24"
                        sDefaultLabel = "Dim7"

                    Case 25 'Dim8
                        sLRS_ID = "25"
                        sDefaultLabel = "Dim8"

                    Case 26 'Dim9
                        sLRS_ID = "26"
                        sDefaultLabel = "Dim9"

                    Case 27 'Dim10
                        sLRS_ID = "27"
                        sDefaultLabel = "Dim10"

                        'Case 1 'Klientnavn
                        '    sLRS_ID = Klientnavn
                        '    sDefaultLabel = Klientnavn

                        'Case 2 'Klientnummer
                        '    sLRS_ID = Klientnummer
                        '    sDefaultLabel = Klientnummer

                        'Case 7 'KID
                        '    sLRS_ID = KID
                        '    sDefaultLabel = KID

                End Select

                sMySQL = String.Empty
                sMySQL = "INSERT INTO Labels(Company_ID, Label_LRSId, Label) VALUES("
                sMySQL = sMySQL & sCompany_ID & ", " & sLRS_ID & ", '" & sDefaultLabel & "')"
                oMyDal.SQL = sMySQL
                oMyDal.ExecuteNonQuery()
            Next iCounter

            bReturnValue = True

        Catch ex As Exception
            bReturnValue = False
            Throw New Exception(ex.Message)

        End Try

        Return bReturnValue

    End Function

    Public Function RetrieveLabels(ByRef oMyDal As vbBabel.DAL, ByVal sCompany_ID As String, ByRef sCustomerNo As String, ByRef sInvoiceNo As String, ByRef sExternalClientNumber As String, _
                                    ByRef sVendroNo As String, ByRef sCurrency As String, ByRef sExchangeRate As String, ByRef sGeneralLedger As String, _
                                    ByRef sVoucherNo As String, ByRef sName As String, ByRef sFreetext As String, ByRef sCurrencyAmount As String, _
                                    ByRef sCurrency2 As String, ByRef sDiscount As String, ByRef sBackPayment As String, ByRef sMyField As String, _
                                    ByRef sMyField2 As String, ByRef sMyField3 As String, ByRef sDim1 As String, ByRef sDim2 As String, _
                                    ByRef sDim3 As String, ByRef sDim4 As String, ByRef sDim5 As String, ByRef sDim6 As String, ByRef sDim7 As String, _
                                    ByRef sDim8 As String, ByRef sDim9 As String, ByRef sDim10 As String) As Boolean
        Dim bReturnValue As Boolean = False
        Dim sMySQL As String
        Dim bDALOpenedInFunction As Boolean = False

        Try

            If oMyDal Is Nothing Then
                oMyDal = New vbBabel.DAL
                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDal.ConnectToDB() Then
                    Throw New System.Exception(oMyDal.ErrorMessage)
                End If
                bDALOpenedInFunction = True
            End If
            sMySQL = "SELECT Label_LRSId, Label FROM Labels WHERE Company_ID = " & sCompany_ID
            oMyDal.SQL = sMySQL
            oMyDal.Reader_Execute()
            If oMyDal.Reader_HasRows Then
                Do While oMyDal.Reader_ReadRecord()

                    Select Case oMyDal.Reader_GetString("Label_LRSId")

                        Case "41001"
                            sCustomerNo = oMyDal.Reader_GetString("Label")
                        Case "41002"
                            sInvoiceNo = oMyDal.Reader_GetString("Label")
                        Case "41011"
                            sExternalClientNumber = oMyDal.Reader_GetString("Label")
                        Case "41008"
                            sVendroNo = oMyDal.Reader_GetString("Label")
                        Case "41005"
                            sCurrency = oMyDal.Reader_GetString("Label")
                        Case "41006"
                            sExchangeRate = oMyDal.Reader_GetString("Label")
                        Case "41007"
                            sGeneralLedger = oMyDal.Reader_GetString("Label")
                        Case "41009"
                            sVoucherNo = oMyDal.Reader_GetString("Label")
                        Case "41012"
                            sName = oMyDal.Reader_GetString("Label")
                        Case "41010"
                            sFreetext = oMyDal.Reader_GetString("Label")
                        Case "41023"
                            sCurrencyAmount = oMyDal.Reader_GetString("Label")
                        Case "41024"
                            sCurrency2 = oMyDal.Reader_GetString("Label")
                        Case "41020"
                            sDiscount = oMyDal.Reader_GetString("Label")
                        Case "41025"
                            sBackPayment = oMyDal.Reader_GetString("Label")
                        Case "15"
                            sMyField = oMyDal.Reader_GetString("Label")
                        Case "16"
                            sMyField2 = oMyDal.Reader_GetString("Label")
                        Case "17"
                            sMyField3 = oMyDal.Reader_GetString("Label")
                        Case "18"
                            sDim1 = oMyDal.Reader_GetString("Label")
                            If sDim1 = "" Then
                                sDim1 = "Dim1"
                            End If
                        Case "19"
                            sDim2 = oMyDal.Reader_GetString("Label")
                        Case "20"
                            sDim3 = oMyDal.Reader_GetString("Label")
                        Case "21"
                            sDim4 = oMyDal.Reader_GetString("Label")
                        Case "22"
                            sDim5 = oMyDal.Reader_GetString("Label")
                        Case "23"
                            sDim6 = oMyDal.Reader_GetString("Label")
                        Case "24"
                            sDim7 = oMyDal.Reader_GetString("Label")
                        Case "25"
                            sDim8 = oMyDal.Reader_GetString("Label")
                        Case "26"
                            sDim9 = oMyDal.Reader_GetString("Label")
                        Case "27"
                            sDim10 = oMyDal.Reader_GetString("Label")

                    End Select
                Loop

            End If

            If bDALOpenedInFunction Then
                oMyDal.Close()
                oMyDal = Nothing
            End If
            bReturnValue = True

        Catch ex As Exception
            bReturnValue = False
            Throw New Exception(ex.Message)

        End Try

        Return bReturnValue

    End Function
    Public Function FindDebitAccountInSetup(ByRef oProfile As vbBabel.Profile, ByRef iFileSetup_ID As Short, ByRef sFormat As String, Optional ByVal bAskForAccount As Boolean = False) As String
        Dim oFilesetup As vbBabel.FileSetup = Nothing
        Dim oClient As vbBabel.Client = Nothing
        Dim oaccount As vbBabel.Account = Nothing
        Dim sReturnValue As String = ""

        On Error GoTo ERR_FindDebitAccountInSetup

        sReturnValue = ""

        For Each oFilesetup In oProfile.FileSetups
            If oFilesetup.FileSetup_ID = iFileSetup_ID Then
                If oFilesetup.Clients.Count = 0 Then
                    ' as pre 27.07.2010
                    Err.Raise(11012, sFormat, LRS(11011) & LRS(11012))
                    'Err.Raise 11011, sFormat, "Can't find any debit accountnumber, because no client is found for this Profile."
                ElseIf oFilesetup.Clients.Count > 1 Then
                    Err.Raise(11013, sFormat, LRS(11011) & LRS(11013))
                    'Err.Raise 11011, sFormat, "Can't find any debit accountnumber, because more than 1 client uses this Profile."
                Else
                    For Each oClient In oFilesetup.Clients
                        If bAskForAccount And (oClient.Accounts.Count = 0 Or oClient.Accounts.Count > 1) Then
                            ' added 27.07.2010
                            ' if parameter bAskForAccount = True, then ask for it !
                            If bAskForAccount Then
                                Do While True
                                    'sReturnValue = InputBox("Please input fromacount", "DebitAccount")
                                    sReturnValue = InputBox(LRSCommon(64051), LRSCommon(64052))
                                    If Len(sReturnValue) > 3 Then  ' minimum 4 digits for account
                                        Exit Do
                                    Else
                                        If Len(sReturnValue) = 0 Then
                                            ' cancel pressed
                                            ' cut rest
                                            'Added 25.11.2010
                                            FindDebitAccountInSetup = ""
                                            Exit Function
                                        End If
                                    End If
                                Loop
                            End If
                        Else
                            ' as pre 27.07.2010
                            If oClient.Accounts.Count = 0 Then
                                Err.Raise(11011, sFormat, LRS(11011) & LRS(11014))
                                'Err.Raise 11011, sFormat, "Can't find any debit accountnumber, because no accountnumber is stated on the client."
                            ElseIf oClient.Accounts.Count > 1 Then
                                ' changed 27.07.2010
                                ' If more than one account, then return with blank string
                                FindDebitAccountInSetup = ""
                                Exit For

                                'Err.Raise 11011, sFormat, LRS(11011) & LRS(11015)
                                'Err.Raise 11011, sFormat, "Can't find any debit accountnumber, because more than 1 accountnumber is stated on the client."
                            Else
                                For Each oaccount In oClient.Accounts
                                    'Only 1 account per client
                                    sReturnValue = Trim$(oaccount.Account)
                                    Exit For
                                Next
                            End If
                            If Not EmptyString(sReturnValue) Then
                                Exit For
                            End If
                        End If
                    Next oClient
                End If
            End If  ' If oFilesetup.FileSetup_ID = iFileSetup_ID Then
            If Not EmptyString(sReturnValue) Then
                Exit For
            End If
        Next oFilesetup

        If Not oaccount Is Nothing Then
            oaccount = Nothing
        End If
        If Not oClient Is Nothing Then
            oClient = Nothing
        End If
        If Not oFilesetup Is Nothing Then
            oFilesetup = Nothing
        End If

        FindDebitAccountInSetup = sReturnValue

        Exit Function

ERR_FindDebitAccountInSetup:

        If Not oaccount Is Nothing Then
            oaccount = Nothing
        End If
        If Not oClient Is Nothing Then
            oClient = Nothing
        End If
        If Not oFilesetup Is Nothing Then
            'UPGRADE_NOTE: Object oFilesetup may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oFilesetup = Nothing
        End If

        Err.Raise(Err.Number, "FindDebitAccountInSetup", Err.Description)

    End Function
    Public Function FindClientName(ByVal sI_Account As String, Optional ByVal bOnlyName As Boolean = False) As String
        '30.08.2016 - Added the use of bOnlyName
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sReturnString As String = ""

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "Select C.ClientNo, C.Name FROM Client C INNER JOIN Account A ON C.Company_ID = A.Company_ID AND C.Client_ID = A.Client_ID WHERE A.Account = '" & sI_Account & "'"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    If bOnlyName Then
                        sReturnString = ""
                    Else
                        sReturnString = oMyDal.Reader_GetString("ClientNo")
                    End If
                    sReturnString = sReturnString & " " & oMyDal.Reader_GetString("Name")

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: FindClientName" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return sReturnString

    End Function
    '15.10.2013 - Added new function
    Public Function FindClientGroupName(ByRef bFoundAtLeastOne As Boolean) As String()
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim oMyDal2 As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim aReturnArray As String() = Nothing
        Dim iCounter As Integer

        Try
            bFoundAtLeastOne = False

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "Select ClientGroup FROM Client WHERE Company_ID = 1 GROUP BY ClientGroup" 'TODO Hardcoded Company_ID
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then
                iCounter = -1
                Do While oMyDal.Reader_ReadRecord

                    If Len(oMyDal.Reader_GetString("ClientGroup")) > 0 Then
                        'Added 20.08.2014 for Visma Collectors to show accounts within a client in addition to clientgroups
                        'This is used for clientaccounts, probably not useful for other customers.
                        If oMyDal.Reader_GetString("ClientGroup").ToUpper = "USEACCOUNTNUMBER" Then
                            If oMyDal2 Is Nothing Then
                                oMyDal2 = New vbBabel.DAL
                                oMyDal2.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                                If Not oMyDal2.ConnectToDB() Then
                                    Throw New System.Exception(oMyDal2.ErrorMessage)
                                End If
                            End If
                            sMySQL = "SELECT A.ACCOUNT AS KONTO FROM Account A, Client C WHERE C.ClientGroup = 'USEACCOUNTNUMBER' AND C.Company_ID = A.Company_ID AND C.Client_ID = A.Client_ID"
                            oMyDal2.SQL = sMySQL
                            If oMyDal2.Reader_Execute() Then
                                Do While oMyDal2.Reader_ReadRecord
                                    If Len(oMyDal2.Reader_GetString("KONTO")) > 0 Then
                                        iCounter = iCounter + 1
                                        bFoundAtLeastOne = True
                                        ReDim Preserve aReturnArray(iCounter)
                                        aReturnArray(iCounter) = oMyDal2.Reader_GetString("KONTO")
                                    End If
                                Loop
                            Else
                                Throw New Exception(LRSCommon(45002) & oMyDal2.ErrorMessage)
                            End If
                        Else
                            iCounter = iCounter + 1
                            bFoundAtLeastOne = True
                            ReDim Preserve aReturnArray(iCounter)
                            aReturnArray(iCounter) = oMyDal.Reader_GetString("ClientGroup")
                        End If
                    End If
                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: FindClientGroupName" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aReturnArray

    End Function
    Public Function RetrieveSQLForReports(ByVal bSQLServer As Boolean, ByVal sReportNo As String, ByVal iBreakLevel As Short, ByVal bShowOCR As Boolean, ByVal bSavebeforeReport As Boolean, Optional ByVal sClientNo As String = "", Optional ByRef sSELECTPart As String = "", Optional ByRef sWHEREPart As String = "", Optional ByRef sORDERBYPart As String = "", Optional ByRef sGROUPBYPart As String = "") As String
        'Returns the complete SQL to retrieve data from the Babel DB for reporting
        'It will also return the SELECT-part, WHERE-part and ORDERBY-part of the SQL
        'A bit unsure how to deal with clientbased reports. 
        '  When creating a report we need the actual clientno included in the SQL
        '  In SETUP we don't want to show a specific client
        Dim sMySQL As String
        Dim sMySQLSorting As String
        Dim sWhereClause As String
        Dim sAddToWhereClause As String
        Dim sReportWhere As String
        Dim sGroupByClause As String

        Try

            sMySQL = ""
            sAddToWhereClause = ""
            sWhereClause = ""
            sReportWhere = ""
            sGroupByClause = ""

            'Set the sorting of the report
            If sReportNo = "510" Or sReportNo = "990" Then
                'Some reports don't report 'Payment-tables' and this will affect the sorting
                If iBreakLevel = 5 Then
                    sMySQLSorting = "ORDER BY C.ClientNo ASC"
                Else
                    sMySQLSorting = ""
                End If
            ElseIf sReportNo = "505" Then
                If iBreakLevel = 5 Then
                    sMySQLSorting = "ORDER BY P.VB_ClientNo ASC"
                Else
                    sMySQLSorting = ""
                End If
            ElseIf sReportNo = "512" Then
                sMySQLSorting = ""
            Else
                Select Case iBreakLevel
                    Case vbBabel.BabelFiles.BreakLevel.NoBreak 'NOBREAK
                        sMySQLSorting = "ORDER BY P.Company_ID ASC, P.BabelFile_ID ASC, P.Batch_ID ASC, P.Payment_ID ASC, I.Invoice_ID ASC"

                    Case vbBabel.BabelFiles.BreakLevel.BreakOnAccountDay 'BREAK_ON_ACCOUNT (and per day)
                        'Must sort on account
                        'sMySQLSorting = "ORDER BY P.I_Account ASC, BA.DATE_Production ASC, P.Company_ID ASC, P.BabelFile_ID ASC, P.Batch_ID ASC, P.Payment_ID ASC, I.Invoice_ID ASC"
                        ' 03.02.2016 - changed to P.DATE_Payment
                        sMySQLSorting = "ORDER BY P.I_Account ASC, P.DATE_Payment ASC, P.Company_ID ASC, P.BabelFile_ID ASC, P.Batch_ID ASC, P.Payment_ID ASC, I.Invoice_ID ASC"

                    Case vbBabel.BabelFiles.BreakLevel.BreakOnBatch 'BREAK_ON_BATCH
                        sMySQLSorting = "ORDER BY P.Company_ID ASC, P.BabelFile_ID ASC, P.Batch_ID ASC, P.Payment_ID ASC, I.Invoice_ID ASC"

                    Case vbBabel.BabelFiles.BreakLevel.BreakOnPayment 'BREAK_ON_PAYMENT
                        sMySQLSorting = "ORDER BY P.Company_ID ASC, P.BabelFile_ID ASC, P.Batch_ID ASC, P.Payment_ID ASC, I.Invoice_ID ASC"

                    Case vbBabel.BabelFiles.BreakLevel.BreakOnAccount 'BREAK_ON_ACCOUNTONLY
                        sMySQLSorting = "ORDER BY P.I_Account ASC, P.Company_ID ASC, P.BabelFile_ID ASC, P.Batch_ID ASC, P.Payment_ID ASC, I.Invoice_ID ASC"

                    Case vbBabel.BabelFiles.BreakLevel.BreakOnClient 'BREAK_ON_CLIENT 'New
                        sMySQLSorting = "ORDER BY P.VB_ClientNo ASC, P.Company_ID ASC, P.BabelFile_ID ASC, P.Batch_ID ASC, P.Payment_ID ASC, I.Invoice_ID ASC"

                    Case Else 'NOBREAK
                        sMySQLSorting = "ORDER BY P.Company_ID ASC, P.BabelFile_ID ASC, P.Batch_ID ASC, P.Payment_ID ASC, I.Invoice_ID ASC"

                End Select
            End If


            'Check if it is a clientbased report
            If Not EmptyString(sClientNo) Then
                'Some reports don't report on the Payment-table and ClientNo must be retrieved from the Client-table.
                If sReportNo = "510" Then
                    sAddToWhereClause = "AND C.ClientNo = '" & Trim$(sClientNo) & "' "
                Else
                    sAddToWhereClause = "AND P.VB_ClientNo = '" & Trim$(sClientNo) & "' "
                End If

            End If
            If Not bShowOCR Then
                If sReportNo <> "510" And sReportNo <> "512" Then
                    If bSQLServer Then
                        sAddToWhereClause = sAddToWhereClause & "AND P.KIDPayment = 0 "
                    Else
                        sAddToWhereClause = sAddToWhereClause & "AND P.KIDPayment = False "
                    End If
                End If
            End If

            sReportNo = Microsoft.VisualBasic.Right("000" & sReportNo, 3)

            Select Case sReportNo

                Case "001"
                    '001 - Batches report

                    'sMySQL = "SELECT DISTINCT P.Company_ID, B.FilenameIn, BA.I_Branch, "
                    'sMySQL = sMySQL & "STR(BA.SequenceNoStart) + ' - ' + STR(BA.SequenceNoEnd) AS SequenceNo, "
                    'sMySQL = sMySQL & "P.BabelFile_ID AS BabelFile_ID, P.Batch_ID AS Batch_ID, BA.DATE_Production AS DATE_Production, "
                    'sMySQL = sMySQL & "IIf(BA.MON_TransferredAmount = 0, BA.MON_InvoiceAmount/100, BA.MON_TransferredAmount/100) AS MON_TransferredAmount, "
                    'sMySQL = sMySQL & "IIf(BA.MON_TransferredAmount = 0, P.MON_InvoiceCurrency, P.MON_TransferCurrency) AS MON_TransferCurrency, "
                    'sMySQL = sMySQL & "BA.REF_Bank, P.I_Account, "
                    'sMySQL = sMySQL & "BA.FormatType, P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment, "
                    'sMySQL = sMySQL & "(SELECT COUNT(*) FROM Payment P2 WHERE BA.Batch_ID = P2.Batch_ID AND BA.BabelFile_ID = P2.BabelFile_ID AND BA.Company_ID = P2.Company_ID) AS NoOfPayments "
                    'sMySQL = sMySQL & "FROM (BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                    'sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID "
                    'sWhereClause = "WHERE BA.MON_InvoiceAmount > 0 "

                    If bSQLServer Then

                        sMySQL = "SELECT BA.Company_ID, "
                        sMySQL = sMySQL & "CASE WHEN LEN(B.FilenameIn) > 60 THEN '...' + RIGHT(B.FilenameIn, 60) ELSE B.FilenameIn END AS Filename, BA.I_Branch, "
                        sMySQL = sMySQL & "STR(BA.SequenceNoStart) + ' - ' + STR(BA.SequenceNoEnd) AS SequenceNo, BA.BabelFile_ID AS BabelFile_ID, '20100505' AS DATE_Payment, BA.Batch_ID AS Batch_ID, BA.DATE_Production AS DATE_Production, "
                        sMySQL = sMySQL & "CASE WHEN BA.MON_TransferredAmount = 0 THEN BA.MON_InvoiceAmount/100 ELSE BA.MON_TransferredAmount/100 END AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "CASE WHEN BA.MON_TransferredAmount = 0 THEN "
                        sMySQL = sMySQL & "CASE WHEN (SELECT Count(*) AS Antall FROM (SELECT DISTINCT MON_InvoiceCurrency, BATCH_Id AS SubBatchID, Babelfile_Id AS SubBabelfileID, Company_ID AS SubCompanyID "
                        sMySQL = sMySQL & "FROM Payment P6) AS KOKO WHERE KOKO.SubBatchID = BA.Batch_ID AND KOKO.SubBabelfileID = BA.BabelFile_ID AND KOKO.SubCompanyID = BA.Company_ID) > 1 THEN 'Multippel' ELSE "
                        sMySQL = sMySQL & "(SELECT TOP 1  MON_InvoiceCurrency FROM Payment P3 WHERE BA.Batch_ID = P3.Batch_ID AND BA.BabelFile_ID = P3.BabelFile_ID AND BA.Company_ID = P3.Company_ID) END ELSE "
                        sMySQL = sMySQL & "CASE WHEN (SELECT Count(*) AS Antall FROM (SELECT DISTINCT MON_TransferCurrency, BATCH_Id AS SubBatchID, Babelfile_Id AS SubBabelfileID, Company_ID AS SubCompanyID FROM Payment P6) AS KOKO WHERE KOKO.SubBatchID = BA.Batch_ID AND KOKO.SubBabelfileID = BA.BabelFile_ID AND KOKO.SubCompanyID = BA.Company_ID) > 1 THEN 'Multippel' ELSE "
                        sMySQL = sMySQL & "(SELECT TOP 1  MON_TransferCurrency FROM Payment P4 WHERE BA.Batch_ID = P4.Batch_ID AND BA.BabelFile_ID = P4.BabelFile_ID AND BA.Company_ID = P4.Company_ID) END END AS MON_TransferCurrency, BA.REF_Bank, "
                        sMySQL = sMySQL & "CASE WHEN (SELECT Count(*) AS Antall FROM (SELECT DISTINCT I_Account, BATCH_Id AS SubBatchID, Babelfile_Id AS SubBabelfileID, Company_ID AS SubCompanyID FROM Payment P6) AS KOKO WHERE KOKO.SubBatchID = BA.Batch_ID AND KOKO.SubBabelfileID = BA.BabelFile_ID AND KOKO.SubCompanyID = BA.Company_ID) > 1 THEN 'Multippel' ELSE "
                        sMySQL = sMySQL & "(SELECT TOP 1 I_Account FROM Payment P4 WHERE BA.Batch_ID = P4.Batch_ID AND BA.BabelFile_ID = P4.BabelFile_ID AND BA.Company_ID = P4.Company_ID) END AS I_Account, BA.FormatType, (SELECT COUNT(*) FROM Payment P2 WHERE BA.Batch_ID = P2.Batch_ID AND BA.BabelFile_ID = P2.BabelFile_ID AND BA.Company_ID = P2.Company_ID) AS NoOfPayments "
                        sMySQL = sMySQL & "FROM (BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) WHERE BA.MON_InvoiceAmount > 0 "

                    Else

                        sMySQL = "SELECT BA.Company_ID, IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, BA.I_Branch, STR(BA.SequenceNoStart) + ' - ' + STR(BA.SequenceNoEnd) AS SequenceNo, BA.BabelFile_ID AS BabelFile_ID, "
                        sMySQL = sMySQL & "'20100505' AS DATE_Payment, "
                        sMySQL = sMySQL & "BA.Batch_ID AS Batch_ID, BA.DATE_Production AS DATE_Production, IIf(BA.MON_TransferredAmount = 0, BA.MON_InvoiceAmount/100, "
                        sMySQL = sMySQL & "BA.MON_TransferredAmount/100) AS MON_TransferredAmount, IIf(BA.MON_TransferredAmount = 0, IIF((SELECT Count(*) AS Antall FROM "
                        sMySQL = sMySQL & "(SELECT DISTINCT MON_InvoiceCurrency, BATCH_Id AS SubBatchID, Babelfile_Id AS SubBabelfileID, Company_ID AS SubCompanyID FROM Payment P6) AS KOKO "
                        sMySQL = sMySQL & "WHERE KOKO.SubBatchID = BA.Batch_ID AND KOKO.SubBabelfileID = BA.BabelFile_ID AND KOKO.SubCompanyID = BA.Company_ID) > 1, 'Multippel', "
                        sMySQL = sMySQL & "(SELECT TOP 1  MON_InvoiceCurrency FROM Payment P3 WHERE BA.Batch_ID = P3.Batch_ID AND BA.BabelFile_ID = P3.BabelFile_ID AND BA.Company_ID = P3.Company_ID)), "
                        sMySQL = sMySQL & "IIF((SELECT Count(*) AS Antall FROM (SELECT DISTINCT MON_TransferCurrency, BATCH_Id AS SubBatchID, Babelfile_Id AS SubBabelfileID, Company_ID AS SubCompanyID "
                        sMySQL = sMySQL & "FROM Payment P6) AS KOKO WHERE KOKO.SubBatchID = BA.Batch_ID AND KOKO.SubBabelfileID = BA.BabelFile_ID AND KOKO.SubCompanyID = BA.Company_ID) > 1"
                        sMySQL = sMySQL & ", 'Multippel', (SELECT TOP 1  MON_TransferCurrency FROM Payment P4 WHERE BA.Batch_ID = P4.Batch_ID AND BA.BabelFile_ID = P4.BabelFile_ID AND "
                        sMySQL = sMySQL & "BA.Company_ID = P4.Company_ID))) AS MON_TransferCurrency, BA.REF_Bank, IIF((SELECT Count(*) AS Antall FROM (SELECT DISTINCT I_Account, BATCH_Id AS SubBatchID, "
                        sMySQL = sMySQL & "Babelfile_Id AS SubBabelfileID, Company_ID AS SubCompanyID FROM Payment P6) AS KOKO WHERE "
                        sMySQL = sMySQL & "KOKO.SubBatchID = BA.Batch_ID AND KOKO.SubBabelfileID = BA.BabelFile_ID AND KOKO.SubCompanyID = BA.Company_ID) > 1, 'Multippel',"
                        sMySQL = sMySQL & "(SELECT TOP 1 I_Account FROM Payment P4 WHERE BA.Batch_ID = P4.Batch_ID AND BA.BabelFile_ID = P4.BabelFile_ID AND BA.Company_ID = P4.Company_ID)) AS I_Account"
                        sMySQL = sMySQL & ", BA.FormatType, (SELECT COUNT(*) FROM Payment P2 WHERE BA.Batch_ID = P2.Batch_ID AND BA.BabelFile_ID = P2.BabelFile_ID AND BA.Company_ID = P2.Company_ID) AS NoOfPayments "
                        sMySQL = sMySQL & "FROM (BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) WHERE BA.MON_InvoiceAmount > 0 "
                    End If

                    sAddToWhereClause = ""
                    sReportWhere = " AND B.Babelfile_ID > 11000000 "
                    sMySQLSorting = Replace(sMySQLSorting, ", I.Invoice_ID ASC", "", , vbTextCompare)
                    sMySQLSorting = Replace(sMySQLSorting, ", P.Payment_ID ASC", "", , vbTextCompare)
                    sMySQLSorting = Replace(sMySQLSorting, "P.I_Account ASC,", "", , vbTextCompare)
                    '25.07.2013 - The ORDER BY must refer to the Batch-table not the Payment (default), because the Payment-table isn't a part of the SQL
                    sMySQLSorting = Replace(sMySQLSorting, "P.", "BA.", , , vbTextCompare)


                Case "002"
                    '002 - Payment report (without details/invoices)

                    sMySQL = "SELECT str(P.Company_ID) + '-' + str(P.BabelFile_ID) + '-' +str(P.Batch_ID) + '-' + str(P.Payment_ID) AS Grouping, P.Company_ID AS Company_ID, "
                    sMySQL = sMySQL & "P.BabelFile_ID AS BabelFile_ID, P.Batch_ID AS Batch_ID, P.Payment_ID AS Payment_ID, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN LEN(B.FilenameIn) > 60 THEN '...' + RIGHT(B.FilenameIn, 60) ELSE B.FilenameIn END AS Filename, "
                    Else
                        sMySQL = sMySQL & "IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60), B.FilenameIn) AS Filename, "
                    End If
                    sMySQL = sMySQL & "P.VB_ClientNo AS Client, BA.DATE_Production AS DATE_Production, BA.REF_Bank AS BatchRef, BA.MON_TransferredAmount/100 AS StatementAmount, "
                    sMySQL = sMySQL & "P.I_Account AS I_Account, P.MON_InvoiceAmount/100 AS MON_InvoiceAmount, P.MON_InvoiceCurrency AS MON_InvoiceCurrency, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MON_TransferredAmount = 0 THEN P.MON_InvoiceAmount/100 ELSE P.MON_TransferredAmount/100 END AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "CASE WHEN P.MON_TransferredAmount = 0 THEN P.MON_InvoiceCurrency ELSE P.MON_TransferCurrency END AS MON_TransferCurrency, "
                    Else
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceAmount/100, P.MON_TransferredAmount/100) AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceCurrency, P.MON_TransferCurrency) AS MON_TransferCurrency, "
                    End If
                    sMySQL = sMySQL & "P.MON_OriginallyPaidAmount/100 AS MON_OriginallyPaidAmount, P.MON_OriginallyPaidCurrency AS MON_OriginallyPaidCurrency, "
                    sMySQL = sMySQL & "P.MON_AccountAmount/100 AS MON_AccountAmount, P.MON_AccountCurrency AS MON_AccountCurrency, P.MON_LocalExchRate AS MON_LocalExchRate, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MON_AccountAmount = 0 THEN 0 ELSE (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100 END AS ChargesAbroad, "
                    Else
                        sMySQL = sMySQL & "IIF(P.MON_AccountAmount = 0, 0, (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100) AS ChargesAbroad, "
                    End If
                    sMySQL = sMySQL & "P.MON_ChargesAmount / 100 AS ChargesDomestic, P.E_Name AS E_Name, "
                    sMySQL = sMySQL & "P.E_Account AS E_Account, P.Paycode AS Paycode, "
                    sMySQL = sMySQL & "P.REF_Bank1 AS REF_Bank1, P.REF_Bank2 AS REF_Bank2, P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment "
                    sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
                    '                sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID "
                    sWhereClause = "WHERE BA.MON_InvoiceAmount >= 0 "
                    sReportWhere = " AND B.Babelfile_ID > 11000000 "

                    sMySQLSorting = Replace(sMySQLSorting, ", I.Invoice_ID ASC", "", , vbTextCompare)

                Case "003"
                    '003 - Payment detail report (unregarding match, mathctype etc...

                    sMySQL = "SELECT str(I.Company_ID) + '-' + str(I.BabelFile_ID) + '-' +str(I.Batch_ID) + '-' + str(I.Payment_ID) AS Grouping, I.Company_ID AS Company_ID, "
                    sMySQL = sMySQL & "I.BabelFile_ID AS BabelFile_ID, I.Batch_ID AS Batch_ID, I.Payment_ID AS Payment_ID, I.Invoice_ID AS Invoice_ID, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN LEN(B.FilenameIn) > 60 THEN '...' + RIGHT(B.FilenameIn, 60) ELSE B.FilenameIn END AS Filename, B.Bank, "
                        sMySQL = sMySQL & "CASE WHEN BA.MON_TransferredAmount = 0 THEN BA.MON_InvoiceAmount/100 ELSE BA.MON_TransferredAmount/100 END AS StatementAmount, "
                    Else
                        sMySQL = sMySQL & "IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, B.Bank, "
                        sMySQL = sMySQL & "IIf(BA.MON_TransferredAmount = 0, BA.MON_InvoiceAmount/100, BA.MON_TransferredAmount/100) AS StatementAmount, "
                    End If
                    sMySQL = sMySQL & "P.VB_ClientNo AS Client, BA.DATE_Production AS DATE_Production, BA.REF_Bank AS BatchRef, "
                    sMySQL = sMySQL & "P.I_Account AS I_Account, P.MON_InvoiceAmount/100 AS MON_InvoiceAmount, P.MON_InvoiceCurrency AS MON_InvoiceCurrency, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MON_TransferredAmount = 0 THEN P.MON_InvoiceAmount/100 ELSE P.MON_TransferredAmount/100 END AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "CASE WHEN P.MON_TransferredAmount = 0 THEN P.MON_InvoiceCurrency ELSE P.MON_TransferCurrency END AS MON_TransferCurrency, "
                    Else
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceAmount/100, P.MON_TransferredAmount/100) AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceCurrency, P.MON_TransferCurrency) AS MON_TransferCurrency, "
                    End If
                    sMySQL = sMySQL & "P.MON_OriginallyPaidAmount/100 AS MON_OriginallyPaidAmount, P.MON_OriginallyPaidCurrency AS MON_OriginallyPaidCurrency, "
                    sMySQL = sMySQL & "P.MON_AccountAmount/100 AS MON_AccountAmount, P.MON_AccountCurrency AS MON_AccountCurrency, P.MON_LocalExchRate AS MON_LocalExchRate, "
                    '24.09.2015 - See comments in report
                    'sMySQL = sMySQL & "IIF(P.MON_AccountAmount = 0, 0, (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100) AS ChargesAbroad, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MON_AccountAmount = 0 THEN 0 ELSE P.MON_ChargesAmountAbroad / 100 END AS ChargesAbroad, "
                    Else
                        sMySQL = sMySQL & "IIF(P.MON_AccountAmount = 0, 0, P.MON_ChargesAmountAbroad / 100) AS ChargesAbroad, "
                    End If
                    sMySQL = sMySQL & "P.MON_ChargesAmount / 100 AS ChargesDomestic, P.StatusCode AS StatusCode, P.StatusText AS StatusText, P.E_Name AS E_Name, P.E_Adr1 AS E_Adr1, P.E_Adr2 AS E_Adr2, P.E_Adr3 AS E_Adr3, P.E_Zip AS E_Zip, "
                    '27.06.2017 - Added E_CountryCode
                    sMySQL = sMySQL & "P.E_City AS E_City, P.E_CountryCode AS E_CountryCode, P.E_Account AS E_Account, P.I_Name AS I_Name, P.I_Adr1 AS I_Adr1, P.I_Adr2 AS I_Adr2, P.I_Adr3 AS I_Adr3, P.I_Zip AS I_Zip, "
                    sMySQL = sMySQL & "P.I_City AS I_City, P.I_CountryCode AS I_CountryCode, P.REF_Bank1 AS REF_Bank1, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.Ref_Own_BabelBankGenerated <> '' THEN P.Ref_Own_BabelBankGenerated ELSE P.REF_Bank2 END AS REF_Bank2, "
                    Else
                        sMySQL = sMySQL & "IIF(P.Ref_Own_BabelBankGenerated <> '', P.Ref_Own_BabelBankGenerated, P.REF_Bank2) AS REF_Bank2, "
                    End If
                    sMySQL = sMySQL & "P.REF_Own AS REF_Own, P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment, P.PayCode AS Paycode, P.PayType AS Paytype, "
                    sMySQL = sMySQL & "P.BANK_SWIFTCode AS BANK_SWIFTCode, P.BANK_BranchNo AS BANK_BranchNo, P.BANK_BranchType AS BANK_BranchType, "
                    '04.01.2021 - Added - , P.Unique_PaymentID AS BabelBank_ID
                    sMySQL = sMySQL & "P.VoucherNo AS VoucherNo, P.Unique_PaymentID AS BabelBank_ID, P.ExtraD1 AS ExtraD, I.Extra1 AS Extra1, P.MATCH_UseOriginalAmountInMatching, P.ImportFormat AS ImportFormat, "
                    sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.StatusCode AS InvoiceStatusCode, "
                    'sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.MATCH_Type, I.Match_ID, "
                    sMySQL = sMySQL & "I.InvoiceNo AS InvoiceNumber, I.CustomerNo AS CustomerNumber, "
                    sMySQL = sMySQL & "I.MON_InvoiceAmount/100 AS InvoiceAmount "
                    sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID "

                    If bSQLServer Then
                        sWhereClause = "WHERE(BA.MON_InvoiceAmount >= 0 And I.MATCH_Final <> 0) "
                    Else
                        sWhereClause = "WHERE(BA.MON_InvoiceAmount >= 0 And I.MATCH_Final = True) "
                    End If
                    'sMySQL = sMySQL & "ORDER BY P.VB_ClientNo ASC, I.Company_ID ASC, I.BabelFile_ID ASC, I.Batch_ID ASC, I.Payment_ID ASC, I.Invoice_ID ASC"
                    sReportWhere = " AND B.Babelfile_ID > 11000000 "


                Case "004"
                    '004 - Used for reporting OCR incoming payments. Only! Not Autogiro

                    sMySQL = "SELECT str(P.Company_ID) + '-' + str(P.BabelFile_ID) + '-' +str(P.Batch_ID) + '-' + str(P.Payment_ID) AS Grouping, P.Company_ID AS Company_ID, "
                    sMySQL = sMySQL & "P.BabelFile_ID AS BabelFile_ID, P.Batch_ID AS Batch_ID, P.Payment_ID AS Payment_ID, I.Invoice_ID AS Invoice_ID, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN LEN(B.FilenameIn) > 60 THEN '...' + RIGHT(B.FilenameIn, 60) ELSE B.FilenameIn END AS Filename, "
                    Else
                        sMySQL = sMySQL & "IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, "
                    End If
                    sMySQL = sMySQL & "P.VB_ClientNo AS Client, BA.DATE_Production AS DATE_Production, BA.REF_Bank AS BatchRef, BA.MON_TransferredAmount/100 AS StatementAmount, "
                    sMySQL = sMySQL & "P.I_Account AS I_Account, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN I.MON_TransferredAmount = 0 THEN I.MON_InvoiceAmount/100 ELSE I.MON_TransferredAmount/100 END AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "CASE WHEN P.MON_TransferredAmount = 0 THEN P.MON_InvoiceCurrency ELSE P.MON_TransferCurrency END AS MON_TransferCurrency, "
                    Else
                        sMySQL = sMySQL & "IIf(I.MON_TransferredAmount = 0, I.MON_InvoiceAmount/100, I.MON_TransferredAmount/100) AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceCurrency, P.MON_TransferCurrency) AS MON_TransferCurrency, "
                    End If
                    sMySQL = sMySQL & "P.E_Name AS E_Name, "
                    sMySQL = sMySQL & "P.E_Account AS E_Account, P.Paycode AS Paycode, "
                    sMySQL = sMySQL & "P.REF_Bank1 AS REF_Bank1, P.DATE_Value AS DATE_Value, "
                    sMySQL = sMySQL & "I.MON_InvoiceAmount/100 AS MON_InvoiceAmount, I.Unique_ID AS Unique_ID "
                    sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID "

                    If bSQLServer Then
                        sWhereClause = "WHERE BA.MON_InvoiceAmount > 0 AND CAST(P.Paycode AS BIGINT) > 509 AND CAST(P.Paycode AS BIGINT) < 518 "
                    Else
                        sWhereClause = "WHERE BA.MON_InvoiceAmount > 0 AND VAL(P.Paycode) > 509 AND VAL(P.Paycode) < 518 "
                    End If
                    sReportWhere = " AND B.Babelfile_ID > 11000000 "

                Case "005"
                    '005 Report FIK incoming payments
                    If bSQLServer Then

                        Throw New Exception("The SQL-Server version of BabelBank is not adopted to create a report based on the database for reportno. " & sReportNo)

                    Else
                        sMySQL = "SELECT str(P.Company_ID) + '-' + str(P.BabelFile_ID) + '-' +str(P.Batch_ID) + '-' + str(P.Payment_ID) AS Grouping, P.Company_ID AS Company_ID, "
                        sMySQL = sMySQL & "P.BabelFile_ID AS BabelFile_ID, P.Batch_ID AS Batch_ID, P.Payment_ID AS Payment_ID, I.Invoice_ID AS Invoice_ID, IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, "
                        sMySQL = sMySQL & "P.VB_ClientNo AS Client, BA.DATE_Production AS DATE_Production, BA.REF_Bank AS BatchRef, BA.MON_TransferredAmount/100 AS StatementAmount, "
                        sMySQL = sMySQL & "P.I_Account AS I_Account, "
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceAmount/100, P.MON_TransferredAmount/100) AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceCurrency, P.MON_TransferCurrency) AS MON_TransferCurrency, "
                        sMySQL = sMySQL & "P.E_Name AS E_Name, "
                        sMySQL = sMySQL & "P.E_Account AS E_Account, P.Paycode AS Paycode, "
                        sMySQL = sMySQL & "P.REF_Bank1 AS REF_Bank1, P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment, "
                        sMySQL = sMySQL & "I.MON_InvoiceAmount/100 AS MON_InvoiceAmount, I.Unique_ID AS Unique_ID, LEFT(I.Unique_ID, 2) AS KortArtKode "
                        sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                        sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
                        sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID "

                        sWhereClause = "WHERE BA.MON_InvoiceAmount > 0 AND P.Paycode = '510' AND I.MATCH_Original = True "
                        sReportWhere = " AND B.Babelfile_ID > 11000000 "
                    End If

                Case "007"
                    '007 - Special report for US ACH/BAI 

                    If bSQLServer Then

                        Throw New Exception("The SQL-Server version of BabelBank is not adopted to create a report based on the database for reportno. " & sReportNo)

                    Else
                        sMySQL = "SELECT str(I.Company_ID) + '-' + str(I.BabelFile_ID) + '-' +str(I.Batch_ID) + '-' + str(I.Payment_ID) AS Grouping, I.Company_ID AS Company_ID, "
                        sMySQL = sMySQL & "I.BabelFile_ID AS BabelFile_ID, I.Batch_ID AS Batch_ID, I.Payment_ID AS Payment_ID, I.Invoice_ID AS Invoice_ID, IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, "
                        sMySQL = sMySQL & "IIf(BA.MON_TransferredAmount = 0, BA.MON_InvoiceAmount/100, BA.MON_TransferredAmount/100) AS StatementAmount, "
                        sMySQL = sMySQL & "P.VB_ClientNo AS Client, BA.DATE_Production AS DATE_Production, BA.REF_Bank AS BatchRef, "
                        sMySQL = sMySQL & "P.I_Account AS I_Account, P.MON_InvoiceAmount/100 AS MON_InvoiceAmount, P.MON_InvoiceCurrency AS MON_InvoiceCurrency, "
                        sMySQL = sMySQL & "P.MON_TransferredAmount/100 AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceCurrency, P.MON_TransferCurrency) AS MON_TransferCurrency, "
                        sMySQL = sMySQL & "P.MON_OriginallyPaidAmount/100 AS MON_OriginallyPaidAmount, P.MON_OriginallyPaidCurrency AS MON_OriginallyPaidCurrency, "
                        sMySQL = sMySQL & "P.MON_AccountAmount/100 AS MON_AccountAmount, P.MON_AccountCurrency AS MON_AccountCurrency, P.MON_LocalExchRate AS MON_LocalExchRate, "
                        sMySQL = sMySQL & "IIF(P.MON_AccountAmount = 0, 0, (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100) AS ChargesAbroad, "
                        sMySQL = sMySQL & "P.MON_ChargesAmount / 100 AS ChargesDomestic, P.StatusCode AS StatusCode, LEFT(P.E_Name, 30) AS E_Name, P.E_Adr1 AS E_Adr1, P.E_Adr2 AS E_Adr2, P.E_Adr3 AS E_Adr3, P.E_Zip AS E_Zip, "
                        sMySQL = sMySQL & "P.E_City AS E_City, P.E_Account AS E_Account, LEFT(P.I_Name, 30) AS I_Name, P.I_Adr1 AS I_Adr1, P.I_Adr2 AS I_Adr2, P.I_Adr3 AS I_Adr3, P.I_Zip AS I_Zip, "
                        sMySQL = sMySQL & "P.I_City AS I_City, P.REF_Bank1 AS REF_Bank1, P.REF_Bank2 AS REF_Bank2, P.REF_Own AS REF_Own, P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment, P.PayCode AS Paycode, P.PayType AS Paytype, "
                        sMySQL = sMySQL & "P.DnBNORTBIPayType, "
                        sMySQL = sMySQL & "P.BANK_SWIFTCode AS BANK_SWIFTCode, P.BANK_BranchNo AS BANK_BranchNo, P.BANK_BranchType AS BANK_BranchType, P.BANK_Name, "
                        sMySQL = sMySQL & "P.VoucherNo AS VoucherNo, P.VoucherType AS VoucherType, P.ExtraD1 AS ExtraD1, P.ExtraD2 AS ExtraD2, P.ExtraD3 AS ExtraD3, I.Extra1 AS Extra1, P.MATCH_UseOriginalAmountInMatching, P.ImportFormat AS ImportFormat, "
                        sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.StatusCode AS InvoiceStatusCode, I.MATCH_Original, "
                        'sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.MATCH_Type, I.Match_ID, "
                        sMySQL = sMySQL & "I.InvoiceNo AS InvoiceNumber, I.CustomerNo AS CustomerNumber, "
                        sMySQL = sMySQL & "I.MON_TransferredAmount/100 AS InvoiceAmount, "
                        sMySQL = sMySQL & "(SELECT COUNT(*) FROM [Freetext] F WHERE  I.Invoice_ID = F.Invoice_ID AND I.Payment_ID = F.Payment_ID AND I.Batch_ID = F.Batch_ID AND I.BabelFile_ID = F.BabelFile_ID AND I.Company_ID = F.Company_ID) AS NoOfFreetexts "
                        sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                        sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
                        sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID "

                        sWhereClause = "WHERE (BA.MON_InvoiceAmount > 0 And I.MATCH_Original = True) AND P.DnBNORTBIPayType <> 'MT942' "
                        'sMySQL = sMySQL & "ORDER BY P.VB_ClientNo ASC, I.Company_ID ASC, I.BabelFile_ID ASC, I.Batch_ID ASC, I.Payment_ID ASC, I.Invoice_ID ASC"
                        sReportWhere = " AND B.Babelfile_ID > 11000000 "
                    End If

                Case "011"
                    '011 - Rejected payments - Use report 003

                    If bSQLServer Then

                        Throw New Exception("The SQL-Server version of BabelBank is not adopted to create a report based on the database for reportno. " & sReportNo)

                    Else
                        sMySQL = "SELECT str(I.Company_ID) + '-' + str(I.BabelFile_ID) + '-' +str(I.Batch_ID) + '-' + str(I.Payment_ID) AS Grouping, I.Company_ID AS Company_ID, "
                        sMySQL = sMySQL & "I.BabelFile_ID AS BabelFile_ID, I.Batch_ID AS Batch_ID, I.Payment_ID AS Payment_ID, I.Invoice_ID AS Invoice_ID, IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, B.Bank, "
                        sMySQL = sMySQL & "IIf(BA.MON_TransferredAmount = 0, BA.MON_InvoiceAmount/100, BA.MON_TransferredAmount/100) AS StatementAmount, "
                        sMySQL = sMySQL & "P.VB_ClientNo AS Client, BA.DATE_Production AS DATE_Production, BA.REF_Bank AS BatchRef, "
                        sMySQL = sMySQL & "P.I_Account AS I_Account, P.MON_InvoiceAmount/100 AS MON_InvoiceAmount, P.MON_InvoiceCurrency AS MON_InvoiceCurrency, "
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceAmount/100, P.MON_TransferredAmount/100) AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceCurrency, P.MON_TransferCurrency) AS MON_TransferCurrency, "
                        sMySQL = sMySQL & "P.MON_OriginallyPaidAmount/100 AS MON_OriginallyPaidAmount, P.MON_OriginallyPaidCurrency AS MON_OriginallyPaidCurrency, "
                        sMySQL = sMySQL & "P.MON_AccountAmount/100 AS MON_AccountAmount, P.MON_AccountCurrency AS MON_AccountCurrency, P.MON_LocalExchRate AS MON_LocalExchRate, "
                        sMySQL = sMySQL & "IIF(P.MON_AccountAmount = 0, 0, (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100) AS ChargesAbroad, "
                        sMySQL = sMySQL & "P.MON_ChargesAmount / 100 AS ChargesDomestic, P.StatusCode AS StatusCode, P.StatusText AS StatusText, P.E_Name AS E_Name, P.E_Adr1 AS E_Adr1, P.E_Adr2 AS E_Adr2, P.E_Adr3 AS E_Adr3, P.E_Zip AS E_Zip, "
                        sMySQL = sMySQL & "P.E_City AS E_City, P.E_Account AS E_Account, P.I_Name AS I_Name, P.I_Adr1 AS I_Adr1, P.I_Adr2 AS I_Adr2, P.I_Adr3 AS I_Adr3, P.I_Zip AS I_Zip, "
                        sMySQL = sMySQL & "P.I_City AS I_City, P.REF_Bank1 AS REF_Bank1, P.REF_Bank2 AS REF_Bank2, P.REF_Own AS REF_Own, P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment, P.PayCode AS Paycode, P.PayType AS Paytype, "
                        sMySQL = sMySQL & "P.BANK_SWIFTCode AS BANK_SWIFTCode, P.BANK_BranchNo AS BANK_BranchNo, P.BANK_BranchType AS BANK_BranchType, "
                        sMySQL = sMySQL & "P.VoucherNo AS VoucherNo, P.ExtraD1 AS ExtraD, I.Extra1 AS Extra1, P.MATCH_UseOriginalAmountInMatching, P.ImportFormat AS ImportFormat, "
                        sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.StatusCode AS InvoiceStatusCode, "
                        'sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.MATCH_Type, I.Match_ID, "
                        sMySQL = sMySQL & "I.InvoiceNo AS InvoiceNumber, I.CustomerNo AS CustomerNumber, "
                        sMySQL = sMySQL & "I.MON_TransferredAmount/100 AS InvoiceAmount "
                        sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                        sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
                        sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID "

                        sWhereClause = "WHERE (BA.MON_InvoiceAmount > 0 And I.MATCH_Final = True) "
                        'sWhereClause = sWhereClause & "AND (VAL(P.Statuscode) > 2 OR VAL(I.Statuscode) > 2 OR P.Statuscode = 'ZZZ') "
                        ' Changed 04.05.2015 to reflect ISO20022 codes
                        sWhereClause = sWhereClause & "AND (P.Statuscode <> '00' AND I.Statuscode <> '01' AND P.Statuscode <> '02') "
                        sReportWhere = " AND B.Babelfile_ID > 11000000 "
                    End If

                Case "012"
                    '012 - CONTRL

                    If bSQLServer Then

                        Throw New Exception("The SQL-Server version of BabelBank is not adopted to create a report based on the database for reportno. " & sReportNo)

                    Else
                        '16.05.2017 - Changed the SQL to include Freetext
                        sMySQL = "SELECT str(BA.Company_ID) + '-' + str(BA.BabelFile_ID) + '-' +str(BA.Batch_ID) AS Grouping, BA.Company_ID AS Company_ID, "
                        sMySQL = sMySQL & "BA.BabelFile_ID AS BabelFile_ID, BA.Batch_ID AS Batch_ID, IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, "
                        sMySQL = sMySQL & "B.Bank, B.DATE_Production AS DATE_Production, B.StatusCode AS BatchStatusCode, B.File_ID AS File_ID, B.IDENT_Sender AS IDENT_Sender, "
                        sMySQL = sMySQL & "B.IDENT_Receiver AS IDENT_Receiver, BA.StatusCode AS BabelStatusCode, F.XText, P.StatusText FROM (((BabelFile AS B INNER JOIN Batch AS BA ON "
                        sMySQL = sMySQL & "B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID "
                        sMySQL = sMySQL & "AND BA.Company_ID = P.Company_ID) INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND "
                        sMySQL = sMySQL & "P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID) LEFT JOIN Freetext AS F ON I.Invoice_ID = F.Invoice_ID "
                        sMySQL = sMySQL & "AND I.Payment_ID = F.Payment_ID AND I.Batch_ID = F.Batch_ID AND I.BabelFile_ID = F.BabelFile_ID AND I.Company_ID = F.Company_ID "
                        'Old code
                        'sMySQL = "SELECT str(BA.Company_ID) + '-' + str(BA.BabelFile_ID) + '-' +str(BA.Batch_ID) AS Grouping, BA.Company_ID AS Company_ID, "
                        'sMySQL = sMySQL & "BA.BabelFile_ID AS BabelFile_ID, BA.Batch_ID AS Batch_ID, IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, B.Bank, B.DATE_Production AS DATE_Production, "
                        'sMySQL = sMySQL & "B.StatusCode AS BatchStatusCode, B.File_ID AS File_ID, B.IDENT_Sender AS IDENT_Sender, "
                        'sMySQL = sMySQL & "B.IDENT_Receiver AS IDENT_Receiver, BA.StatusCode AS BabelStatusCode "
                        'sMySQL = sMySQL & "FROM (BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "

                        sWhereClause = "WHERE B.Company_ID = 1 "
                        sReportWhere = " AND B.Babelfile_ID > 11000000 "

                        sMySQLSorting = "ORDER BY BA.Company_ID ASC, BA.BabelFile_ID ASC, BA.Batch_ID ASC "
                    End If

                Case "503"
                    '503 - CREMUL Unmatched, use report 504

                    sMySQL = "SELECT str(I.Company_ID) + '-' + str(I.BabelFile_ID) + '-' +str(I.Batch_ID) + '-' + str(I.Payment_ID) AS Grouping, I.Company_ID AS Company_ID, "
                    sMySQL = sMySQL & "I.BabelFile_ID AS BabelFile_ID, I.Batch_ID AS Batch_ID, I.Payment_ID AS Payment_ID, I.Invoice_ID AS Invoice_ID, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN LEN(B.FilenameIn) > 60 THEN '...' + RIGHT(B.FilenameIn, 60) ELSE B.FilenameIn END AS Filename, "
                    Else
                        sMySQL = sMySQL & "IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, "
                    End If
                    sMySQL = sMySQL & "P.VB_ClientNo AS Client, BA.DATE_Production AS DATE_Production, BA.REF_Bank AS BatchRef, BA.MON_TransferredAmount/100 AS StatementAmount, "
                    sMySQL = sMySQL & "P.I_Account AS I_Account, P.MON_InvoiceAmount/100 AS MON_InvoiceAmount, P.MON_InvoiceCurrency AS MON_InvoiceCurrency, "
                    sMySQL = sMySQL & "P.MON_OriginallyPaidAmount/100 AS MON_OriginallyPaidAmount, P.MON_OriginallyPaidCurrency AS MON_OriginallyPaidCurrency, "
                    sMySQL = sMySQL & "P.MON_AccountAmount/100 AS MON_AccountAmount, P.MON_AccountCurrency AS MON_AccountCurrency, P.MON_LocalExchRate AS MON_LocalExchRate, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MON_AccountAmount = 0 THEN 0 ELSE (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100 END AS ChargesAbroad, "
                    Else
                        sMySQL = sMySQL & "IIF(P.MON_AccountAmount = 0, 0, (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100) AS ChargesAbroad, "
                    End If
                    sMySQL = sMySQL & "P.MON_ChargesAmount / 100 AS ChargesDomestic, P.E_Name AS E_Name, P.E_Adr1 AS E_Adr1, P.E_Adr2 AS E_Adr2, P.E_Adr3 AS E_Adr3, P.E_Zip AS E_Zip, "
                    sMySQL = sMySQL & "P.E_City AS E_City, P.E_Account AS E_Account, P.I_Name AS I_Name, P.I_Adr1 AS I_Adr1, P.I_Adr2 AS I_Adr2, P.I_Adr3 AS I_Adr3, P.I_Zip AS I_Zip, "
                    sMySQL = sMySQL & "P.I_City AS I_City, P.REF_Bank1 AS REF_Bank1, P.REF_Bank2 AS REF_Bank2, P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment, P.PayCode AS Paycode, P.PayType AS Paytype, "
                    sMySQL = sMySQL & "P.VoucherNo AS VoucherNo, P.Unique_PaymentID AS BabelBank_ID, P.HowMatched AS HowMatched, P.ExtraD1 AS ExtraD, I.Extra1 AS Extra1, P.MATCH_UseOriginalAmountInMatching, P.MATCH_Matched AS PMATCH_Matched, "
                    sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.MATCH_Type, I.Match_ID, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN I.MATCH_Type = 2 THEN 'BB_GLLabel' ELSE 'BB_InvoiceLabel' END AS InvoiceLabel, "
                        sMySQL = sMySQL & "CASE WHEN I.Match_Type = 2 THEN I.Match_ID ELSE I.InvoiceNo END AS InvoiceNumber, 'BB_CustomerLabel' AS CustomerLabel, I.CustomerNo AS CustomerNumber, "
                        sMySQL = sMySQL & "I.MON_TransferredAmount/100 AS InvoiceAmount, "
                        sMySQL = sMySQL & "CASE WHEN P.MATCH_UseOriginalAmountInMatching <> 0 THEN P.MON_OriginallyPaidAmount/100 ELSE P.MON_InvoiceAmount/100 END AS TotalPayment, "
                        sMySQL = sMySQL & "P.ImportFormat AS ImportFormat, "
                        sMySQL = sMySQL & "CASE WHEN P.MATCH_Matched = 1 THEN 0 ELSE I.MATCH_Matched END AS Matched, "
                    Else
                        sMySQL = sMySQL & "IIF(I.MATCH_Type = 2, 'BB_GLLabel', 'BB_InvoiceLabel') AS InvoiceLabel, "
                        sMySQL = sMySQL & "IIF(I.Match_Type = 2, I.Match_ID, I.InvoiceNo) AS InvoiceNumber, 'BB_CustomerLabel' AS CustomerLabel, I.CustomerNo AS CustomerNumber, "
                        sMySQL = sMySQL & "I.MON_TransferredAmount/100 AS InvoiceAmount, IIF(P.MATCH_UseOriginalAmountInMatching = True,P.MON_OriginallyPaidAmount/100, "
                        sMySQL = sMySQL & "P.MON_InvoiceAmount/100) AS TotalPayment, P.ImportFormat AS ImportFormat, IIF(P.MATCH_Matched = 1, False, I.MATCH_Matched) AS Matched, "
                    End If
                    sMySQL = sMySQL & "I.MATCH_Final AS Final "
                    'sMySQL = sMySQL & "(SELECT SUM(II.MON_InvoiceAmount)/100 FROM Invoice II WHERE I.Payment_ID = II.Payment_ID AND I.Batch_ID = II.Batch_ID AND I.BabelFile_ID = II.BabelFile_ID AND I.Company_ID = II.Company_ID AND II.MATCH_Matched = False AND II.MATCH_Final = True) AS TotalUnmatched, "
                    'sMySQL = sMySQL & "(SELECT SUM(III.MON_InvoiceAmount)/100 FROM Invoice III WHERE I.Payment_ID = III.Payment_ID AND I.Batch_ID = III.Batch_ID AND I.BabelFile_ID = III.BabelFile_ID AND I.Company_ID = III.Company_ID AND III.MATCH_Matched = True AND III.MATCH_Final = True) AS TotalMatched "
                    sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID "

                    If bSQLServer Then
                        sWhereClause = "WHERE (BA.MON_InvoiceAmount > 0 And P.MATCH_Matched < 3 And I.MATCH_Final <> 0) AND P.StatusCode <> '-1' "
                    Else
                        sWhereClause = "WHERE (BA.MON_InvoiceAmount > 0 And P.MATCH_Matched < 3 And I.MATCH_Final = True) AND P.StatusCode <> '-1' "
                    End If
                    sReportWhere = " AND B.Babelfile_ID > 11000000 "

                Case "504"
                    '504 - CREMUL matched

                    sMySQL = "SELECT str(I.Company_ID) + '-' + str(I.BabelFile_ID) + '-' +str(I.Batch_ID) + '-' + str(I.Payment_ID) AS Grouping, I.Company_ID AS Company_ID, "
                    sMySQL = sMySQL & "I.BabelFile_ID AS BabelFile_ID, I.Batch_ID AS Batch_ID, I.Payment_ID AS Payment_ID, I.Invoice_ID AS Invoice_ID, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN LEN(B.FilenameIn) > 60 THEN '...' + RIGHT(B.FilenameIn, 60) ELSE B.FilenameIn END AS Filename, "
                    Else
                        sMySQL = sMySQL & "IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, "
                    End If
                    sMySQL = sMySQL & "P.VB_ClientNo AS Client, BA.DATE_Production AS DATE_Production, BA.REF_Bank AS BatchRef, BA.MON_TransferredAmount/100 AS StatementAmount, "
                    sMySQL = sMySQL & "P.I_Account AS I_Account, P.MON_InvoiceAmount/100 AS MON_InvoiceAmount, P.MON_InvoiceCurrency AS MON_InvoiceCurrency, "
                    sMySQL = sMySQL & "P.MON_OriginallyPaidAmount/100 AS MON_OriginallyPaidAmount, P.MON_OriginallyPaidCurrency AS MON_OriginallyPaidCurrency, "
                    sMySQL = sMySQL & "P.MON_AccountAmount/100 AS MON_AccountAmount, P.MON_AccountCurrency AS MON_AccountCurrency, P.MON_LocalExchRate AS MON_LocalExchRate, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MON_AccountAmount = 0 THEN 0 ELSE (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100 END AS ChargesAbroad, "
                    Else
                        sMySQL = sMySQL & "IIF(P.MON_AccountAmount = 0, 0, (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100) AS ChargesAbroad, "
                    End If
                    sMySQL = sMySQL & "P.MON_ChargesAmount / 100 AS ChargesDomestic, P.E_Name AS E_Name, P.E_Adr1 AS E_Adr1, P.E_Adr2 AS E_Adr2, P.E_Adr3 AS E_Adr3, P.E_Zip AS E_Zip, "
                    sMySQL = sMySQL & "P.E_City AS E_City, P.E_Account AS E_Account, P.I_Name AS I_Name, P.I_Adr1 AS I_Adr1, P.I_Adr2 AS I_Adr2, P.I_Adr3 AS I_Adr3, P.I_Zip AS I_Zip, "
                    sMySQL = sMySQL & "P.I_City AS I_City, P.REF_Bank1 AS REF_Bank1, P.REF_Bank2 AS REF_Bank2, P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment, P.PayCode AS Paycode, P.PayType AS Paytype, "
                    sMySQL = sMySQL & "P.VoucherNo AS VoucherNo, P.Unique_PaymentID AS BabelBank_ID, P.HowMatched AS HowMatched, P.ExtraD1 AS ExtraD, I.Extra1 AS Extra1, P.MATCH_UseOriginalAmountInMatching, "
                    sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.MATCH_Type, I.Match_ID, I.MATCH_Final AS Final, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN I.MATCH_Type = 2 THEN 'BB_GLLabel' ELSE 'BB_InvoiceLabel' END AS InvoiceLabel, " '25.04.2013 - Added I.MATCH_Final
                        sMySQL = sMySQL & "CASE WHEN I.Match_Type = 2 THEN I.Match_ID ELSE I.InvoiceNo END AS InvoiceNumber, 'BB_CustomerLabel' AS CustomerLabel, I.CustomerNo AS CustomerNumber, "
                    Else
                        sMySQL = sMySQL & "IIF(I.MATCH_Type = 2, 'BB_GLLabel', 'BB_InvoiceLabel') AS InvoiceLabel, " '25.04.2013 - Added I.MATCH_Final
                        sMySQL = sMySQL & "IIF(I.Match_Type = 2, I.Match_ID, I.InvoiceNo) AS InvoiceNumber, 'BB_CustomerLabel' AS CustomerLabel, I.CustomerNo AS CustomerNumber, "
                    End If
                    sMySQL = sMySQL & "I.MON_TransferredAmount/100 AS InvoiceAmount, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MATCH_UseOriginalAmountInMatching <> 0 THEN P.MON_OriginallyPaidAmount/100 ELSE P.MON_InvoiceAmount/100 END AS TotalPayment, "
                    Else
                        sMySQL = sMySQL & "IIF(P.MATCH_UseOriginalAmountInMatching = True,P.MON_OriginallyPaidAmount/100, P.MON_InvoiceAmount/100) AS TotalPayment, "
                    End If
                    sMySQL = sMySQL & "I.MATCH_Matched AS Matched "
                    sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID "

                    If bSQLServer Then
                        sWhereClause = "WHERE (BA.MON_InvoiceAmount > 0 And P.MATCH_Matched > 1 And I.MATCH_Final <> 0) "
                    Else
                        sWhereClause = "WHERE (BA.MON_InvoiceAmount > 0 And P.MATCH_Matched > 1 And I.MATCH_Final = True) "
                    End If

                    sReportWhere = " AND B.Babelfile_ID > 11000000 "

                Case "505"
                    '505 - Matching CONTRL
                    'sMySQL = "SELECT str(I.Company_ID) + '-' + str(I.BabelFile_ID) + '-' +str(I.Batch_ID) + '-' + str(I.Payment_ID) AS Grouping, BA.DATE_Production AS DATE_Production, P.VB_ClientNo AS Client, P.I_Account AS I_Account, P.E_Name AS Name, P.REF_Bank1 AS REF_Bank, P.MON_InvoiceAmount/100 AS PaymentAmount, (SELECT sum(MON_Invoiceamount) "
                    'sMySQL = sMySQL & "FROM Invoice IV WHERE IV.Invoice_ID = Invoice_ID AND IV.Payment_ID = I.Payment_ID AND IV.Batch_ID = I.Batch_ID AND IV.BabelFile_ID = I.BabelFile_ID AND IV.Company_ID = I.Company_ID AND IV.MATCH_Matched = True AND IV.MATCH_Final= True)/100 AS MATCHEDAmount, (SELECT sum(MON_Invoiceamount) FROM Invoice IVO "
                    'sMySQL = sMySQL & "WHERE IVO.Invoice_ID = Invoice_ID AND IVO.Payment_ID = I.Payment_ID AND IVO.Batch_ID = I.Batch_ID AND IVO.BabelFile_ID = I.BabelFile_ID AND IVO.Company_ID = I.Company_ID AND IVO.MATCH_Matched = False AND IVO.MATCH_Final= True)/100 AS UNMATCHEDAmount  "
                    'sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) INNER JOIN "
                    'sMySQL = sMySQL & "Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID WHERE I.MATCH_Final = True GROUP BY I.Company_ID, I.BabelFile_ID, I.Batch_ID, I.Payment_ID, BA.DATE_Production, P.I_Account, P.E_Name, P.REF_Bank1, P.MON_InvoiceAmount, P.VB_ClientNo "

                    sMySQL = "SELECT str(I.Company_ID) + '-' + str(I.BabelFile_ID) + '-' +str(I.Batch_ID) + '-' + str(I.Payment_ID) AS Grouping, BA.DATE_Production AS DATE_Production, P.VB_ClientNo AS Client, P.I_Account AS I_Account, P.E_Name AS Name, P.REF_Bank1 AS REF_Bank, P.MON_InvoiceAmount/100 AS PaymentAmount, (SELECT sum(IV.MON_Invoiceamount) "
                    sMySQL = sMySQL & "FROM Invoice IV  LEfT JOIN  Payment PA ON IV.Payment_ID = PA.Payment_ID AND IV.Batch_ID = PA.Batch_ID AND IV.BabelFile_ID = PA.BabelFile_ID AND IV.Company_ID = PA.Company_ID WHERE IV.Invoice_ID = Invoice_ID AND IV.Payment_ID = I.Payment_ID AND IV.Batch_ID = I.Batch_ID AND IV.BabelFile_ID = I.BabelFile_ID AND "
                    sMySQL = sMySQL & "IV.Company_ID = I.Company_ID "
                    If bSQLServer Then
                        sMySQL = sMySQL & "AND IV.MATCH_Matched <> 0 AND IV.MATCH_Final <> 0 AND PA.MATCH_Matched = 3)/100 AS MATCHEDAmount, "
                        sMySQL = sMySQL & "CASE WHEN P.Match_Matched < 3 THEN P.MON_InvoiceAmount/100 ELSE 0 END AS UNMATCHEDAmount "
                    Else
                        sMySQL = sMySQL & "AND IV.MATCH_Matched = True AND IV.MATCH_Final= True AND PA.MATCH_Matched = 3)/100 AS MATCHEDAmount, "
                        sMySQL = sMySQL & "IIF(P.Match_Matched < 3 ,P.MON_InvoiceAmount/100, 0) AS UNMATCHEDAmount "
                    End If
                    sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID WHERE "
                    If bSQLServer Then
                        sMySQL = sMySQL & "I.MATCH_Final <> 0 "
                    Else
                        sMySQL = sMySQL & "I.MATCH_Final = True "
                    End If
                    sMySQL = sMySQL & "GROUP BY I.Company_ID, I.BabelFile_ID, I.Batch_ID, I.Payment_ID, BA.DATE_Production, P.I_Account, P.E_Name, P.REF_Bank1, P.MON_InvoiceAmount, P.VB_ClientNo, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.Match_Matched < 3 THEN P.MON_InvoiceAmount/100 ELSE 0 END"
                    Else
                        sMySQL = sMySQL & "IIF(P.Match_Matched < 3 ,P.MON_InvoiceAmount/100, 0)"
                    End If

                    sWhereClause = ""
                    sReportWhere = ""

                Case "506"
                    '506 - Matching summary
                    If bSQLServer Then

                        Throw New Exception("The SQL-Server version of BabelBank is not adopted to create a report based on the database for reportno. " & sReportNo)

                    Else
                        sMySQL = "SELECT PDATE.DATE_Value AS Heading, COUNT(*) AS NoOfRecords, SUM(IDATE.MON_InvoiceAmount)/100 AS TotalAmount, "
                        sMySQL = sMySQL & "(SELECT COUNT(*) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND PP.KIDPayment = True AND PP.DATE_Value = PDATE.DATE_Value) AS NoOfOCR, "
                        sMySQL = sMySQL & "(SELECT SUM(II.MON_InvoiceAmount) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND PP.KIDPayment = True AND PP.DATE_Value = PDATE.DATE_Value)/100 AS SumOCR, "
                        sMySQL = sMySQL & "(SELECT COUNT(*) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND (II.MATCH_Matched = False OR PP.MATCH_Matched < 2) AND PP.DATE_Value = PDATE.DATE_Value) AS NoOfUnmatched, "
                        sMySQL = sMySQL & "(SELECT SUM(II.MON_InvoiceAmount) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND (II.MATCH_Matched = False OR PP.MATCH_Matched < 2) AND PP.DATE_Value = PDATE.DATE_Value)/100 AS SumUnmatched, "
                        sMySQL = sMySQL & "(SELECT COUNT(*) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND (II.MATCH_Matched = True AND PP.MATCH_Matched > 1) AND PP.DATE_Value = PDATE.DATE_Value) AS NoOfMatched, "
                        sMySQL = sMySQL & "(SELECT SUM(II.MON_InvoiceAmount) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND (II.MATCH_Matched = True AND PP.MATCH_Matched > 1) AND PP.DATE_Value = PDATE.DATE_Value)/100 AS SumMatched, "
                        sMySQL = sMySQL & "'DATE' AS Type FROM Payment PDATE INNER JOIN Invoice AS IDATE ON PDATE.Payment_ID = IDATE.Payment_ID AND PDATE.Batch_ID = IDATE.Batch_ID AND PDATE.BabelFile_ID = IDATE.BabelFile_ID AND PDATE.Company_ID = IDATE.Company_ID WHERE IDATE.MATCH_Final = True GROUP BY PDATE.DATE_Value ORDER BY  PDATE.DATE_Value "
                        sMySQL = sMySQL & "UNION "
                        sMySQL = sMySQL & "SELECT PACC.I_Account AS Heading, COUNT(*) AS NoOfRecords, SUM(IACC.MON_InvoiceAmount)/100 AS TotalAmount, "
                        sMySQL = sMySQL & "(SELECT COUNT(*) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND PP.KIDPayment = True AND PP.I_Account = PACC.I_Account) AS NoOfOCR, "
                        sMySQL = sMySQL & "(SELECT SUM(II.MON_InvoiceAmount) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND PP.KIDPayment = True AND PP.I_Account = PACC.I_Account)/100 AS SumOCR, "
                        sMySQL = sMySQL & "(SELECT COUNT(*) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND (II.MATCH_Matched = False OR PP.MATCH_Matched < 2) AND PP.I_Account = PACC.I_Account) AS NoOfUnmatched, "
                        sMySQL = sMySQL & "(SELECT SUM(II.MON_InvoiceAmount) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND (II.MATCH_Matched = False OR PP.MATCH_Matched < 2) AND PP.I_Account = PACC.I_Account)/100 AS SumUnmatched, "
                        sMySQL = sMySQL & "(SELECT COUNT(*) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND (II.MATCH_Matched = True AND PP.MATCH_Matched > 1) AND PP.I_Account = PACC.I_Account) AS NoOfMatched, "
                        sMySQL = sMySQL & "(SELECT SUM(II.MON_InvoiceAmount) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND (II.MATCH_Matched = True AND PP.MATCH_Matched > 1) AND PP.I_Account = PACC.I_Account)/100 AS SumMatched, "
                        sMySQL = sMySQL & "'ACCOUNT' AS Type FROM Payment PACC INNER JOIN Invoice AS IACC ON PACC.Payment_ID = IACC.Payment_ID AND PACC.Batch_ID = IACC.Batch_ID AND PACC.BabelFile_ID = IACC.BabelFile_ID AND PACC.Company_ID = IACC.Company_ID WHERE IACC.MATCH_Final = True GROUP BY PACC.I_Account ORDER BY Heading   "
                        sMySQL = sMySQL & "UNION "
                        sMySQL = sMySQL & "SELECT B.FilenameIn AS Heading, COUNT(*) AS NoOfRecords, SUM(I.MON_InvoiceAmount)/100 AS TotalAmount, "
                        sMySQL = sMySQL & "(SELECT COUNT(*) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND PP.KIDPayment = True AND PP.Babelfile_ID =  B.Babelfile_ID) AS NoOfOCR, "
                        sMySQL = sMySQL & "(SELECT SUM(II.MON_InvoiceAmount) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND PP.KIDPayment = True AND PP.Babelfile_ID =  B.Babelfile_ID)/100 AS SumOCR, "
                        sMySQL = sMySQL & "(SELECT COUNT(*) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND (II.MATCH_Matched = False OR PP.MATCH_Matched < 2) AND PP.Babelfile_ID =  B.Babelfile_ID) AS NoOfUnmatched, "
                        sMySQL = sMySQL & "(SELECT SUM(II.MON_InvoiceAmount) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND (II.MATCH_Matched = False OR PP.MATCH_Matched < 2) AND PP.Babelfile_ID =  B.Babelfile_ID)/100 AS SumUnmatched, "
                        sMySQL = sMySQL & "(SELECT COUNT(*) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND (II.MATCH_Matched = True AND PP.MATCH_Matched > 1) AND  PP.Babelfile_ID =  B.Babelfile_ID) AS NoOfMatched, "
                        sMySQL = sMySQL & "(SELECT SUM(II.MON_InvoiceAmount) FROM Payment PP INNER JOIN Invoice II ON PP.Payment_ID = II.Payment_ID AND PP.Batch_ID = II.Batch_ID AND PP.BabelFile_ID = II.BabelFile_ID AND PP.Company_ID = II.Company_ID WHERE II.MATCH_Final = True AND (II.MATCH_Matched = True AND PP.MATCH_Matched > 1) AND  PP.Babelfile_ID =  B.Babelfile_ID)/100 AS SumMatched, "
                        sMySQL = sMySQL & "'FILENAME' AS Type FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID "
                        sMySQL = sMySQL & "AND B.Company_ID = BA.Company_ID) INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID WHERE I.MATCH_Final = True GROUP BY B.FilenameIn, B.Babelfile_ID ORDER BY Heading"

                        sMySQLSorting = String.Empty
                        sAddToWhereClause = String.Empty
                        sReportWhere = String.Empty
                    End If

                Case "507"
                    '507 - CREMUL On-account postings, use report 504

                    sMySQL = "SELECT str(I.Company_ID) + '-' + str(I.BabelFile_ID) + '-' +str(I.Batch_ID) + '-' + str(I.Payment_ID) AS Grouping, I.Company_ID AS Company_ID, "
                    sMySQL = sMySQL & "I.BabelFile_ID AS BabelFile_ID, I.Batch_ID AS Batch_ID, I.Payment_ID AS Payment_ID, I.Invoice_ID AS Invoice_ID, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN LEN(B.FilenameIn) > 60 THEN '...' + RIGHT(B.FilenameIn, 60) ELSE B.FilenameIn END AS Filename, "
                    Else
                        sMySQL = sMySQL & "IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, "
                    End If
                    sMySQL = sMySQL & "P.VB_ClientNo AS Client, BA.DATE_Production AS DATE_Production, BA.REF_Bank AS BatchRef, BA.MON_TransferredAmount/100 AS StatementAmount, "
                    sMySQL = sMySQL & "P.I_Account AS I_Account, P.MON_InvoiceAmount/100 AS MON_InvoiceAmount, P.MON_InvoiceCurrency AS MON_InvoiceCurrency, "
                    sMySQL = sMySQL & "P.MON_OriginallyPaidAmount/100 AS MON_OriginallyPaidAmount, P.MON_OriginallyPaidCurrency AS MON_OriginallyPaidCurrency, "
                    sMySQL = sMySQL & "P.MON_AccountAmount/100 AS MON_AccountAmount, P.MON_AccountCurrency AS MON_AccountCurrency, P.MON_LocalExchRate AS MON_LocalExchRate, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MON_AccountAmount = 0 THEN 0 ELSE (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100 END AS ChargesAbroad, "
                    Else
                        sMySQL = sMySQL & "IIF(P.MON_AccountAmount = 0, 0, (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100) AS ChargesAbroad, "
                    End If
                    sMySQL = sMySQL & "P.MON_ChargesAmount / 100 AS ChargesDomestic, P.E_Name AS E_Name, P.E_Adr1 AS E_Adr1, P.E_Adr2 AS E_Adr2, P.E_Adr3 AS E_Adr3, P.E_Zip AS E_Zip, "
                    sMySQL = sMySQL & "P.E_City AS E_City, P.E_Account AS E_Account, P.I_Name AS I_Name, P.I_Adr1 AS I_Adr1, P.I_Adr2 AS I_Adr2, P.I_Adr3 AS I_Adr3, P.I_Zip AS I_Zip, "
                    sMySQL = sMySQL & "P.I_City AS I_City, P.REF_Bank1 AS REF_Bank1, P.REF_Bank2 AS REF_Bank2, P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment, P.PayCode AS Paycode, P.PayType AS Paytype, "
                    sMySQL = sMySQL & "P.VoucherNo AS VoucherNo, P.HowMatched AS HowMatched, P.ExtraD1 AS ExtraD, I.Extra1 AS Extra1, P.MATCH_UseOriginalAmountInMatching, "
                    sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.MATCH_Type AS MATCH_Type, I.Match_ID, I.MATCH_Final AS Final, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN I.MATCH_Type = 2 THEN 'BB_GLLabel' ELSE 'BB_InvoiceLabel' END AS InvoiceLabel, "
                        sMySQL = sMySQL & "CASE WHEN I.Match_Type = 2 THEN I.Match_ID ELSE I.InvoiceNo END AS InvoiceNumber, 'BB_CustomerLabel' AS CustomerLabel, I.CustomerNo AS CustomerNumber, "
                    Else
                        sMySQL = sMySQL & "IIF(I.MATCH_Type = 2, 'BB_GLLabel', 'BB_InvoiceLabel') AS InvoiceLabel, "
                        sMySQL = sMySQL & "IIF(I.Match_Type = 2, I.Match_ID, I.InvoiceNo) AS InvoiceNumber, 'BB_CustomerLabel' AS CustomerLabel, I.CustomerNo AS CustomerNumber, "
                    End If
                    sMySQL = sMySQL & "I.MON_TransferredAmount/100 AS InvoiceAmount, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MATCH_UseOriginalAmountInMatching <> 0 THEN P.MON_OriginallyPaidAmount/100 ELSE P.MON_InvoiceAmount/100 END AS TotalPayment, "
                    Else
                        sMySQL = sMySQL & "IIF(P.MATCH_UseOriginalAmountInMatching = True, P.MON_OriginallyPaidAmount/100, P.MON_InvoiceAmount/100) AS TotalPayment, "
                    End If
                    sMySQL = sMySQL & "I.MATCH_Matched AS Matched "
                    sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID "

                    If bSQLServer Then
                        sWhereClause = "WHERE (BA.MON_InvoiceAmount > 0 And P.MATCH_Matched > 1 And I.MATCH_Final <> 0) AND "
                        sWhereClause = sWhereClause & "(SELECT  COUNT(*) FROM Invoice II WHERE I.Payment_ID = II.Payment_ID AND I.Batch_ID = II.Batch_ID AND I.BabelFile_ID = II.BabelFile_ID AND I.Company_ID = II.Company_ID AND II.MATCH_Matched <> 0 AND II.MATCH_Final <> 0 AND II.MATCH_Type = 1) > 0 "
                    Else
                        sWhereClause = "WHERE (BA.MON_InvoiceAmount > 0 And P.MATCH_Matched > 1 And I.MATCH_Final = True) AND "
                        sWhereClause = sWhereClause & "(SELECT  COUNT(*) FROM Invoice II WHERE I.Payment_ID = II.Payment_ID AND I.Batch_ID = II.Batch_ID AND I.BabelFile_ID = II.BabelFile_ID AND I.Company_ID = II.Company_ID AND II.MATCH_Matched = True AND II.MATCH_Final = True AND II.MATCH_Type = 1) > 0 "
                    End If
                    sReportWhere = " AND B.Babelfile_ID > 11000000 "

                Case "508"
                    '508 - CREMUL GL postings, use report 504

                    sMySQL = "SELECT str(I.Company_ID) + '-' + str(I.BabelFile_ID) + '-' +str(I.Batch_ID) + '-' + str(I.Payment_ID) AS Grouping, I.Company_ID AS Company_ID, "
                    sMySQL = sMySQL & "I.BabelFile_ID AS BabelFile_ID, I.Batch_ID AS Batch_ID, I.Payment_ID AS Payment_ID, I.Invoice_ID AS Invoice_ID, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN LEN(B.FilenameIn) > 60 THEN '...' + RIGHT(B.FilenameIn, 60) ELSE B.FilenameIn END AS Filename, "
                    Else
                        sMySQL = sMySQL & "IIF(LEN(B.FilenameIn) > 60, '...' + RIGHT(B.FilenameIn, 60), B.FilenameIn) AS Filename, "
                    End If
                    sMySQL = sMySQL & "P.VB_ClientNo AS Client, BA.DATE_Production AS DATE_Production, BA.REF_Bank AS BatchRef, BA.MON_TransferredAmount/100 AS StatementAmount, "
                    sMySQL = sMySQL & "P.I_Account AS I_Account, P.MON_InvoiceAmount/100 AS MON_InvoiceAmount, P.MON_InvoiceCurrency AS MON_InvoiceCurrency, "
                    sMySQL = sMySQL & "P.MON_OriginallyPaidAmount/100 AS MON_OriginallyPaidAmount, P.MON_OriginallyPaidCurrency AS MON_OriginallyPaidCurrency, "
                    sMySQL = sMySQL & "P.MON_AccountAmount/100 AS MON_AccountAmount, P.MON_AccountCurrency AS MON_AccountCurrency, P.MON_LocalExchRate AS MON_LocalExchRate, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MON_AccountAmount = 0 THEN 0 ELSE (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100 END AS ChargesAbroad, "
                    Else
                        sMySQL = sMySQL & "IIF(P.MON_AccountAmount = 0, 0, (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100) AS ChargesAbroad, "
                    End If
                    sMySQL = sMySQL & "P.MON_ChargesAmount / 100 AS ChargesDomestic, P.E_Name AS E_Name, P.E_Adr1 AS E_Adr1, P.E_Adr2 AS E_Adr2, P.E_Adr3 AS E_Adr3, P.E_Zip AS E_Zip, "
                    sMySQL = sMySQL & "P.E_City AS E_City, P.E_Account AS E_Account, P.I_Name AS I_Name, P.I_Adr1 AS I_Adr1, P.I_Adr2 AS I_Adr2, P.I_Adr3 AS I_Adr3, P.I_Zip AS I_Zip, "
                    sMySQL = sMySQL & "P.I_City AS I_City, P.REF_Bank1 AS REF_Bank1, P.REF_Bank2 AS REF_Bank2, P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment, P.PayCode AS Paycode, P.PayType AS Paytype, "
                    sMySQL = sMySQL & "P.VoucherNo AS VoucherNo, P.HowMatched AS HowMatched, P.ExtraD1 AS ExtraD, I.Extra1 AS Extra1, P.MATCH_UseOriginalAmountInMatching, "
                    sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.MATCH_Type AS MATCH_Type, I.Match_ID, I.MATCH_Final AS Final, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN I.MATCH_Type = 2 THEN 'BB_GLLabel' ELSE 'BB_InvoiceLabel' END AS InvoiceLabel, "
                        sMySQL = sMySQL & "CASE WHEN I.Match_Type = 2 THEN I.Match_ID ELSE I.InvoiceNo END AS InvoiceNumber, "
                    Else
                        sMySQL = sMySQL & "IIF(I.MATCH_Type = 2, 'BB_GLLabel', 'BB_InvoiceLabel') AS InvoiceLabel, "
                        sMySQL = sMySQL & "IIF(I.Match_Type = 2, I.Match_ID, I.InvoiceNo) AS InvoiceNumber, "
                    End If
                    sMySQL = sMySQL & "'BB_CustomerLabel' AS CustomerLabel, I.CustomerNo AS CustomerNumber, "
                    sMySQL = sMySQL & "I.MON_TransferredAmount/100 AS InvoiceAmount, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MATCH_UseOriginalAmountInMatching <> 0 THEN P.MON_OriginallyPaidAmount/100 ELSE P.MON_InvoiceAmount/100 END AS TotalPayment, "
                    Else
                        sMySQL = sMySQL & "IIF(P.MATCH_UseOriginalAmountInMatching = True, P.MON_OriginallyPaidAmount/100, P.MON_InvoiceAmount/100) AS TotalPayment, "
                    End If
                    sMySQL = sMySQL & "I.MATCH_Matched AS Matched "
                    sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID "

                    If bSQLServer Then
                        sWhereClause = "WHERE (BA.MON_InvoiceAmount > 0 And P.MATCH_Matched > 1 And I.MATCH_Final <> 0) AND "
                        sWhereClause = sWhereClause & "(SELECT  COUNT(*) FROM Invoice II WHERE I.Payment_ID = II.Payment_ID AND I.Batch_ID = II.Batch_ID AND I.BabelFile_ID = II.BabelFile_ID AND I.Company_ID = II.Company_ID AND II.MATCH_Matched <> 0 AND II.MATCH_Final <> 0 AND II.MATCH_Type = 2) > 0 "
                    Else
                        sWhereClause = "WHERE (BA.MON_InvoiceAmount > 0 And P.MATCH_Matched > 1 And I.MATCH_Final = True) AND "
                        sWhereClause = sWhereClause & "(SELECT  COUNT(*) FROM Invoice II WHERE I.Payment_ID = II.Payment_ID AND I.Batch_ID = II.Batch_ID AND I.BabelFile_ID = II.BabelFile_ID AND I.Company_ID = II.Company_ID AND II.MATCH_Matched = True AND II.MATCH_Final = True AND II.MATCH_Type = 2) > 0 "
                    End If
                    sReportWhere = " AND B.Babelfile_ID > 11000000 "

                Case "510"
                    '510 - DayTotals

                    sMySQL = "SELECT D.ProdDate AS DATE_Production, D.IB/100 AS IB, D.Imported/100 AS Imported, D.OCR/100 AS OCR, D.Matched_BB/100 AS Matched_BB, "
                    sMySQL = sMySQL & "D.UnMatched_BB/100 AS UnMatched_BB, D.Matched_GL/100 AS Matched_GL, D.AccountNo AS AccountNo, C.ClientNo AS Client FROM DayTotals D, Client C "

                    sWhereClause = "WHERE D.Client_ID = C.Client_ID "
                    sAddToWhereClause = String.Empty

                Case "512" 'Added 24.04.2014 for Visma Collectors - Uses same layout as 510
                    '512 - DayTotals Aggregated

                    If bSQLServer Then

                        Throw New Exception("The SQL-Server version of BabelBank is not adopted to create a report based on the database for reportno. " & sReportNo)

                    Else
                        sMySQL = "SELECT D.Company_id, D.ProdDate AS DATE_Production, SUM(D.IB)/100 AS IB, SUM(D.Imported)/100 AS Imported, SUM(D.OCR)/100 AS OCR, "
                        sMySQL = sMySQL & "SUM(D.Matched_BB)/100 AS Matched_BB, SUM(D.UnMatched_BB)/100 AS UnMatched_BB, SUM(D.Matched_GL)/100 AS Matched_GL, '' AS AccountNo FROM DayTotals D, Client C "

                        sWhereClause = "WHERE D.Client_ID = C.Client_ID GROUP BY D.Company_id, D.ProdDate "
                        sAddToWhereClause = String.Empty
                    End If
                    '


                Case "603"
                    'Added 25.08.2017
                    '603 - AML report, based on 003 but added Ultimate deb/cred and bankdetails

                    sMySQL = "SELECT str(I.Company_ID) + '-' + str(I.BabelFile_ID) + '-' +str(I.Batch_ID) + '-' + str(I.Payment_ID) AS Grouping, I.Company_ID AS Company_ID, "
                    sMySQL = sMySQL & "I.BabelFile_ID AS BabelFile_ID, I.Batch_ID AS Batch_ID, I.Payment_ID AS Payment_ID, I.Invoice_ID AS Invoice_ID, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN LEN(B.FilenameIn) > 60 THEN '...' + RIGHT(B.FilenameIn, 60) ELSE B.FilenameIn END AS Filename, B.Bank, "
                        sMySQL = sMySQL & "CASE WHEN BA.MON_TransferredAmount = 0 THEN BA.MON_InvoiceAmount/100 ELSE BA.MON_TransferredAmount/100 END AS StatementAmount, "
                    Else
                        sMySQL = sMySQL & "IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, B.Bank, "
                        sMySQL = sMySQL & "IIf(BA.MON_TransferredAmount = 0, BA.MON_InvoiceAmount/100, BA.MON_TransferredAmount/100) AS StatementAmount, "
                    End If
                    sMySQL = sMySQL & "P.VB_ClientNo AS Client, BA.DATE_Production AS DATE_Production, BA.REF_Bank AS BatchRef, "
                    sMySQL = sMySQL & "P.I_Account AS I_Account, P.MON_InvoiceAmount/100 AS MON_InvoiceAmount, P.MON_InvoiceCurrency AS MON_InvoiceCurrency, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MON_TransferredAmount = 0 THEN P.MON_InvoiceAmount/100 ELSE P.MON_TransferredAmount/100 END AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "CASE WHEN P.MON_TransferredAmount = 0 THEN P.MON_InvoiceCurrency ELSE P.MON_TransferCurrency END AS MON_TransferCurrency, "
                    Else
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceAmount/100, P.MON_TransferredAmount/100) AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceCurrency, P.MON_TransferCurrency) AS MON_TransferCurrency, "
                    End If
                    sMySQL = sMySQL & "P.MON_OriginallyPaidAmount/100 AS MON_OriginallyPaidAmount, P.MON_OriginallyPaidCurrency AS MON_OriginallyPaidCurrency, "
                    sMySQL = sMySQL & "P.MON_AccountAmount/100 AS MON_AccountAmount, P.MON_AccountCurrency AS MON_AccountCurrency, P.MON_LocalExchRate AS MON_LocalExchRate, "
                    '24.09.2015 - See comments in report
                    'sMySQL = sMySQL & "IIF(P.MON_AccountAmount = 0, 0, (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100) AS ChargesAbroad, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MON_AccountAmount = 0 THEN 0 ELSE P.MON_ChargesAmountAbroad / 100 END AS ChargesAbroad, "
                    Else
                        sMySQL = sMySQL & "IIF(P.MON_AccountAmount = 0, 0, P.MON_ChargesAmountAbroad / 100) AS ChargesAbroad, "
                    End If
                    sMySQL = sMySQL & "P.MON_ChargesAmount / 100 AS ChargesDomestic, P.StatusCode AS StatusCode, P.StatusText AS StatusText, P.E_Name AS E_Name, P.E_Adr1 AS E_Adr1, P.E_Adr2 AS E_Adr2, P.E_Adr3 AS E_Adr3, P.E_Zip AS E_Zip, "
                    '27.06.2017 - Added E_CountryCode
                    sMySQL = sMySQL & "P.E_City AS E_City, P.E_CountryCode AS E_CountryCode, P.E_Account AS E_Account, P.I_Name AS I_Name, P.I_Adr1 AS I_Adr1, P.I_Adr2 AS I_Adr2, P.I_Adr3 AS I_Adr3, P.I_Zip AS I_Zip, "
                    sMySQL = sMySQL & "P.I_City AS I_City, P.I_CountryCode AS I_CountryCode, P.REF_Bank1 AS REF_Bank1, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.Ref_Own_BabelBankGenerated <> '' THEN P.Ref_Own_BabelBankGenerated ELSE P.REF_Bank2 END AS REF_Bank2, "
                    Else
                        sMySQL = sMySQL & "IIF(P.Ref_Own_BabelBankGenerated <> '', P.Ref_Own_BabelBankGenerated, P.REF_Bank2) AS REF_Bank2, "
                    End If
                    sMySQL = sMySQL & "P.REF_Own AS REF_Own, P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment, P.PayCode AS Paycode, P.PayType AS Paytype, "
                    sMySQL = sMySQL & "P.BANK_SWIFTCode AS BANK_SWIFTCode, P.BANK_BranchNo AS BANK_BranchNo, P.BANK_BranchType AS BANK_BranchType, "
                    sMySQL = sMySQL & "P.BANK_Name AS BANK_Name, P.BANK_Adr1 AS BANK_Adr1, P.BANK_Adr2 AS BANK_Adr2, P.BANK_Adr3 AS BANK_Adr3, "
                    sMySQL = sMySQL & "P.BANK_CountryCode AS BANK_CountryCode, P.BANK_SWIFTCodeCorrBank AS BANK_SWIFTCodeCorrBank, "
                    sMySQL = sMySQL & "P.VoucherNo AS VoucherNo, P.ExtraD1 AS ExtraD, I.Extra1 AS Extra1, P.MATCH_UseOriginalAmountInMatching, P.ImportFormat AS ImportFormat, "
                    '05.09.2017 - New fields for Report 603 - next 4 lines
                    sMySQL = sMySQL & "P.UltimateE_Name AS UltimateE_Name, P.UltimateE_Adr1 AS UltimateE_Adr1, P.UltimateE_Adr2 AS UltimateE_Adr2, P.UltimateE_Adr3 AS UltimateE_Adr3, "
                    sMySQL = sMySQL & "P.UltimateE_City AS UltimateE_City, P.UltimateE_Zip AS UltimateE_Zip, P.UltimateE_CountryCode AS UltimateE_CountryCode, "
                    sMySQL = sMySQL & "P.UltimateI_Name AS UltimateI_Name, P.UltimateI_Adr1 AS UltimateI_Adr1, P.UltimateI_Adr2 AS UltimateI_Adr2, P.UltimateI_Adr3 AS UltimateI_Adr3, "
                    sMySQL = sMySQL & "P.UltimateI_City AS UltimateI_City, P.UltimateI_Zip AS UltimateI_Zip, P.UltimateI_CountryCode AS UltimateI_CountryCode, "
                    sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.StatusCode AS InvoiceStatusCode, "
                    sMySQL = sMySQL & "I.InvoiceNo AS InvoiceNumber, I.CustomerNo AS CustomerNumber, "
                    sMySQL = sMySQL & "I.MON_InvoiceAmount/100 AS InvoiceAmount "
                    sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID "

                    If bSQLServer Then
                        sWhereClause = "WHERE(BA.MON_InvoiceAmount >= 0 And I.MATCH_Final <> 0) "
                    Else
                        sWhereClause = "WHERE(BA.MON_InvoiceAmount >= 0 And I.MATCH_Final = True) "
                    End If
                    'sMySQL = sMySQL & "ORDER BY P.VB_ClientNo ASC, I.Company_ID ASC, I.BabelFile_ID ASC, I.Batch_ID ASC, I.Payment_ID ASC, I.Invoice_ID ASC"
                    sReportWhere = " AND B.Babelfile_ID > 11000000 "


                Case "912"
                    '912 - Payment detail report (unregarding match, mathctype etc...

                    'NOT MADE FOR REPORTING ON STORED PAYMENTS, because ToSpecialReport isn't a part of the database (see sWhereClause)

                    sMySQL = "SELECT str(I.Company_ID) + '-' + str(I.BabelFile_ID) + '-' +str(I.Batch_ID) + '-' + str(I.Payment_ID) AS Grouping, I.Company_ID AS Company_ID, "
                    sMySQL = sMySQL & "I.BabelFile_ID AS BabelFile_ID, I.Batch_ID AS Batch_ID, I.Payment_ID AS Payment_ID, I.Invoice_ID AS Invoice_ID, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN LEN(B.FilenameIn) > 60 THEN '...' + RIGHT(B.FilenameIn, 60) ELSE B.FilenameIn END AS Filename, B.Bank, "
                        sMySQL = sMySQL & "CASE WHEN BA.MON_TransferredAmount = 0 THEN BA.MON_InvoiceAmount/100 ELSE BA.MON_TransferredAmount/100 END AS StatementAmount, "
                    Else
                        sMySQL = sMySQL & "IIF(LEN(B.FilenameIn) > 60,'...' + RIGHT(B.FilenameIn, 60),B.FilenameIn) AS Filename, B.Bank, "
                        sMySQL = sMySQL & "IIf(BA.MON_TransferredAmount = 0, BA.MON_InvoiceAmount/100, BA.MON_TransferredAmount/100) AS StatementAmount, "
                    End If
                    sMySQL = sMySQL & "P.VB_ClientNo AS Client, BA.DATE_Production AS DATE_Production, BA.REF_Bank AS BatchRef, "
                    sMySQL = sMySQL & "P.I_Account AS I_Account, P.MON_InvoiceAmount/100 AS MON_InvoiceAmount, P.MON_InvoiceCurrency AS MON_InvoiceCurrency, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MON_TransferredAmount = 0 THEN P.MON_InvoiceAmount/100 ELSE P.MON_TransferredAmount/100 END AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "CASE WHEN P.MON_TransferredAmount = 0 THEN P.MON_InvoiceCurrency ELSE P.MON_TransferCurrency END AS MON_TransferCurrency, "
                    Else
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceAmount/100, P.MON_TransferredAmount/100) AS MON_TransferredAmount, "
                        sMySQL = sMySQL & "IIf(P.MON_TransferredAmount = 0, P.MON_InvoiceCurrency, P.MON_TransferCurrency) AS MON_TransferCurrency, "
                    End If
                    sMySQL = sMySQL & "P.MON_OriginallyPaidAmount/100 AS MON_OriginallyPaidAmount, P.MON_OriginallyPaidCurrency AS MON_OriginallyPaidCurrency, "
                    sMySQL = sMySQL & "P.MON_AccountAmount/100 AS MON_AccountAmount, P.MON_AccountCurrency AS MON_AccountCurrency, P.MON_LocalExchRate AS MON_LocalExchRate, "
                    If bSQLServer Then
                        sMySQL = sMySQL & "CASE WHEN P.MON_AccountAmount = 0 THEN 0 ELSE (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100) END AS ChargesAbroad, "
                    Else
                        sMySQL = sMySQL & "IIF(P.MON_AccountAmount = 0, 0, (P.MON_OriginallyPaidAmount - P.MON_AccountAmount) / 100) AS ChargesAbroad, "
                    End If
                    sMySQL = sMySQL & "P.MON_ChargesAmount / 100 AS ChargesDomestic, P.StatusCode AS StatusCode, P.StatusText AS StatusText, P.E_Name AS E_Name, P.E_Adr1 AS E_Adr1, P.E_Adr2 AS E_Adr2, P.E_Adr3 AS E_Adr3, P.E_Zip AS E_Zip, "
                    sMySQL = sMySQL & "P.E_City AS E_City, P.E_Account AS E_Account, P.I_Name AS I_Name, P.I_Adr1 AS I_Adr1, P.I_Adr2 AS I_Adr2, P.I_Adr3 AS I_Adr3, P.I_Zip AS I_Zip, "
                    sMySQL = sMySQL & "P.I_City AS I_City, P.REF_Bank1 AS REF_Bank1, P.REF_Bank2 AS REF_Bank2, P.REF_Own AS REF_Own, P.DATE_Value AS DATE_Value, P.DATE_Payment AS DATE_Payment, P.PayCode AS Paycode, P.PayType AS Paytype, "
                    sMySQL = sMySQL & "P.BANK_SWIFTCode AS BANK_SWIFTCode, P.BANK_BranchNo AS BANK_BranchNo, P.BANK_BranchType AS BANK_BranchType, "
                    sMySQL = sMySQL & "P.VoucherNo AS VoucherNo, P.ExtraD1 AS ExtraD, I.Extra1 AS Extra1, P.MATCH_UseOriginalAmountInMatching, P.ImportFormat AS ImportFormat, "
                    sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.StatusCode AS InvoiceStatusCode, "
                    'sMySQL = sMySQL & "I.MyField AS MyField, I.Unique_ID AS Unique_ID, I.MATCH_Type, I.Match_ID, "
                    sMySQL = sMySQL & "I.InvoiceNo AS InvoiceNumber, I.CustomerNo AS CustomerNumber, "
                    sMySQL = sMySQL & "I.MON_InvoiceAmount/100 AS InvoiceAmount "
                    sMySQL = sMySQL & "FROM ((BabelFile AS B INNER JOIN Batch AS BA ON B.BabelFile_ID = BA.BabelFile_ID AND B.Company_ID = BA.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Payment AS P ON BA.Batch_ID = P.Batch_ID AND BA.BabelFile_ID = P.BabelFile_ID AND BA.Company_ID = P.Company_ID) "
                    sMySQL = sMySQL & "INNER JOIN Invoice AS I ON P.Payment_ID = I.Payment_ID AND P.Batch_ID = I.Batch_ID AND P.BabelFile_ID = I.BabelFile_ID AND P.Company_ID = I.Company_ID "

                    sWhereClause = "WHERE(BA.MON_InvoiceAmount > 0 And I.MATCH_Final = True) "
                    'sWhereClause = sWhereClause & "AND P.ToSpecialReport = True) "
                    sReportWhere = " AND B.Babelfile_ID > 11000000 "

                Case "950" 'Special
                    sMySQL = "" ' No specific SQL

                Case "952"
                    '952 - Statistics

                    Dim sStartDate As String
                    Dim sEndDate As String

                    sStartDate = "20100101"
                    sEndDate = "20100701"

                    If bSQLServer Then

                        'Throw New Exception("The SQL-Server version of BabelBank is not adopted to create a report based on the database for reportno. " & sReportNo)
                        sMySQL = "SELECT  SUM(NoOfPayments) AS TotalCount, SUM(Amount)/100 AS TotalAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'OCR') AS OCRCount,  "
                        sMySQL = sMySQL & "(SELECT SUM(Amount) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'OCR')/100 AS OCRAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'AG') AS AGCount, "
                        sMySQL = sMySQL & "(SELECT SUM(Amount) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'AG')/100 AS AGAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'AG' AND Type <> 'OCR' AND InfoType <> 'Manual') AS OtherCount, "
                        sMySQL = sMySQL & "(SELECT SUM(Amount) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'AG' AND Type <> 'OCR' AND InfoType <> 'Manual')/100 AS OtherAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'Open item' AND Infotype = 'Auto') AS AutoCount, "
                        sMySQL = sMySQL & "(SELECT SUM(Amount) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'Open item' AND Infotype = 'Auto')/100 AS AutoAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(Proposed) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'Open item' AND Infotype = 'Auto') AS AutoProposed, "
                        sMySQL = sMySQL & "(SELECT SUM(Finally) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'Open item' AND Infotype = 'Auto') AS AutoFinal, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'Open item' AND Infotype = 'Auto') AS UnPostedAutoCount, "
                        sMySQL = sMySQL & "(SELECT SUM(Amount) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'Open item' AND Infotype = 'Auto')/100 AS UnPostedAutoAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfInvoices) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'Open item' AND Infotype = 'Auto') AS UnPostedAutoFinal, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'Open item' AND Infotype = 'Manual') AS ManualCount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfInvoices) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'Open item' AND Infotype = 'Manual') AS ManualFinal, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'Open item' AND Infotype = 'Manual') AS UnMatchededCount, "
                        sMySQL = sMySQL & "(SELECT SUM(Amount) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'Open item' AND Infotype = 'Manual')/100 AS UnMatchedAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfInvoices) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'Open item' AND Infotype = 'Manual') AS UnMatchedFinal "
                        sMySQL = sMySQL & "FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND InfoType <> 'Manual'"

                        'sMySQL = "SELECT Client_ID, Type, SUM(NoOfPayments) as PaymentSum, SUM(NoOfInvoices) as InvoiceSum, Sum(Amount/100) as Amount100, Sum(Proposed) as ProposedSum, Sum(Finally) As FinallySum, InfoType FROM [Statistics] WHERE Date_Run >= '20100101' AND Date_Run <= '20100701' AND InfoType <> 'Manual' GROUP BY Type, Client_ID, InfoType"
                        sWhereClause = ""
                        sAddToWhereClause = ""
                        sReportWhere = String.Empty
                        sMySQLSorting = ""

                    Else
                        sMySQL = "SELECT  SUM(NoOfPayments) AS TotalCount, SUM(Amount)/100 AS TotalAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'OCR') AS OCRCount,  "
                        sMySQL = sMySQL & "(SELECT SUM(Amount) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'OCR')/100 AS OCRAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'AG') AS AGCount, "
                        sMySQL = sMySQL & "(SELECT SUM(Amount) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'AG')/100 AS AGAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'AG' AND Type <> 'OCR' AND InfoType <> 'Manual') AS OtherCount, "
                        sMySQL = sMySQL & "(SELECT SUM(Amount) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'AG' AND Type <> 'OCR' AND InfoType <> 'Manual')/100 AS OtherAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'Open item' AND Infotype = 'Auto') AS AutoCount, "
                        sMySQL = sMySQL & "(SELECT SUM(Amount) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'Open item' AND Infotype = 'Auto')/100 AS AutoAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(Proposed) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'Open item' AND Infotype = 'Auto') AS AutoProposed, "
                        sMySQL = sMySQL & "(SELECT SUM(Finally) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'Open item' AND Infotype = 'Auto') AS AutoFinal, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'Open item' AND Infotype = 'Auto') AS UnPostedAutoCount, "
                        sMySQL = sMySQL & "(SELECT SUM(Amount) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'Open item' AND Infotype = 'Auto')/100 AS UnPostedAutoAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfInvoices) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'Open item' AND Infotype = 'Auto') AS UnPostedAutoFinal, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'Open item' AND Infotype = 'Manual') AS ManualCount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfInvoices) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type <> 'Open item' AND Infotype = 'Manual') AS ManualFinal, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfPayments) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'Open item' AND Infotype = 'Manual') AS UnMatchededCount, "
                        sMySQL = sMySQL & "(SELECT SUM(Amount) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'Open item' AND Infotype = 'Manual')/100 AS UnMatchedAmount, "
                        sMySQL = sMySQL & "(SELECT SUM(NoOfInvoices) FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND Type = 'Open item' AND Infotype = 'Manual') AS UnMatchedFinal "
                        sMySQL = sMySQL & "FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND InfoType <> 'Manual'"

                        'sMySQL = "SELECT Client_ID, Type, SUM(NoOfPayments) as PaymentSum, SUM(NoOfInvoices) as InvoiceSum, Sum(Amount/100) as Amount100, Sum(Proposed) as ProposedSum, Sum(Finally) As FinallySum, InfoType FROM [Statistics] WHERE Date_Run >= '20100101' AND Date_Run <= '20100701' AND InfoType <> 'Manual' GROUP BY Type, Client_ID, InfoType"
                        sWhereClause = ""
                        sAddToWhereClause = ""
                        sReportWhere = String.Empty
                        sMySQLSorting = ""
                    End If

                Case "990"
                    '990 - Clients overview

                    If bSQLServer Then

                        Throw New Exception("The SQL-Server version of BabelBank is not adopted to create a report based on the database for reportno. " & sReportNo)

                    Else
                        sMySQL = "SELECT VB_ClientNo AS ClientNo, C.Name AS ClientName, SUM(P.MON_InvoiceAmount)/100 As TotalAmount, Count(*) AS NoOfRecords From Payment P, Client C "
                        sWhereClause = "WHERE P.VB_ClientNo = C.ClientNo "
                        sGroupByClause = "GROUP BY P.VB_ClientNo, C.Name"

                        sAddToWhereClause = String.Empty
                        sReportWhere = String.Empty
                    End If

                Case Else
                    Err.Raise(30050, "RetrieveSQLForReports", LRSCommon(30050, sReportNo))

            End Select

            If bSavebeforeReport Then
                sAddToWhereClause = sAddToWhereClause & sReportWhere
            End If

            sSELECTPart = sMySQL
            sWHEREPart = sWhereClause & sAddToWhereClause
            sORDERBYPart = sMySQLSorting
            sGROUPBYPart = sGroupByClause

            RetrieveSQLForReports = sMySQL & sWhereClause & sAddToWhereClause & sMySQLSorting & sGROUPBYPart


        Catch ex As Exception

            Throw New Exception("RetrieveSQLForReports" & vbCrLf & ex.Message)

        End Try


    End Function
    'XNET - 22.02.2011 - New function
    Public Function ConvertCharactersForNordeaeGateway_FI(ByVal sString As String) As String
        'Converting characters like this
        '[ - 5B = �   OK
        '\ - 5C = �   OK
        '] - 5D = �   OK
        '{ - 7B = �   OK
        '| - 7C = �   OK
        '} - 7D = �   ???



        sString = Replace(sString, Chr(CLng("&H" & "5B")), "�")
        sString = Replace(sString, Chr(CLng("&H" & "5C")), "�")
        sString = Replace(sString, Chr(CLng("&H" & "5D")), "�")
        sString = Replace(sString, Chr(CLng("&H" & "7B")), "�")
        sString = Replace(sString, Chr(CLng("&H" & "7C")), "�")
        sString = Replace(sString, Chr(CLng("&H" & "7D")), "�")

        ' 28.04.2021 added � to � (� used by DNB FI) 
        sString = Replace(sString, "�", "�")
        sString = Replace(sString, "�", "�")

        ConvertCharactersForNordeaeGateway_FI = sString

    End Function

    Public Function ConvertCharactersFromUTF8ToANSI_NO(ByVal sString As String) As String
        ' Converts Norwegian characters from a UTF-8 file to fit for ANSI export

        sString = Replace(sString, "æ", "�")
        sString = Replace(sString, "�", "�")
        sString = Replace(sString, "ä", "�")
        sString = Replace(sString, "ø", "�")
        sString = Replace(sString, "�", "�")
        sString = Replace(sString, "ö", "�")
        sString = Replace(sString, "å", "�")

        sString = Replace(sString, "Ø", "�")

        sString = Replace(sString, "�", "�")
        sString = Replace(sString, "�", "�")

        sString = Replace(sString, "Æ", "�")
        sString = Replace(sString, "�", "�")
        sString = Replace(sString, "Å", "�")

        ' div. special:
        sString = Replace(sString, "�", "�")
        sString = Replace(sString, "�", "�")
        sString = Replace(sString, "�", "u")
        sString = Replace(sString, "ü", "�")
        sString = Replace(sString, "é", "�")
        sString = Replace(sString, "è", "�")

        'Ô
        If InStr(sString, "Agircan") > 0 Then
            sString = sString
        End If
        sString = Replace(sString, "�" & Chr(148), "�")
        'ë
        sString = Replace(sString, "ë", "�")
        'Ö
        sString = Replace(sString, "Ö", "�")
        'á
        sString = Replace(sString, "á", "�")
        'ð
        sString = Replace(sString, "ð", "�")


        ConvertCharactersFromUTF8ToANSI_NO = sString

    End Function
    Public Function CheckForDoubleImport(ByRef iCompany_ID As Short, ByRef iFileSetup_ID As Short, ByRef sIdentifier As String, ByRef sBank As String, ByRef iFormat As Short, ByRef sFilename As String, ByRef bEndOfProfile As Boolean, ByRef bBBEndedSuccessfully As Boolean, ByRef sDate As String) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim oMyDalUpdate As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True
        Dim sTemp As String = ""
        Dim iOccurance As Double = 0
        Dim iMaxFileID As Double
        Dim sDeleteUntil As String = ""
        Dim iCounter As String = 0

        Try
            If Not EmptyString(sIdentifier) Then
                oMyDal = New vbBabel.DAL
                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDal.ConnectToDB() Then
                    Throw New System.Exception(oMyDal.ErrorMessage)
                End If

                oMyDalUpdate = New vbBabel.DAL
                oMyDalUpdate.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDalUpdate.ConnectToDB() Then
                    Throw New System.Exception(oMyDalUpdate.ErrorMessage)
                End If

                If bEndOfProfile Then

                    'Update the Succeeded-field in the table
                    sMySQL = "SELECT * FROM ImportedFiles WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString & " AND Identifier = '" & sIdentifier & "' AND Bank = '" & sBank & "' ORDER BY DateImported DESC"
                    oMyDal.SQL = sMySQL
                    If oMyDal.Reader_Execute() Then

                        If oMyDal.Reader_HasRows Then

                            Do While oMyDal.Reader_ReadRecord

                                sTemp = oMyDal.Reader_GetString("File_ID")

                                Exit Do

                            Loop

                            oMyDal.Reader_Close()

                            If bBBEndedSuccessfully Then
                                'Update succeeded field
                                sMySQL = "UPDATE ImportedFiles SET Succeeded = True WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString & " AND File_ID = " & sTemp & " AND Identifier = '" & sIdentifier & "' AND Bank = '" & sBank & "'"

                                oMyDalUpdate.SQL = sMySQL

                                If oMyDalUpdate.ExecuteNonQuery Then

                                    If oMyDalUpdate.RecordsAffected = 1 Then
                                        bReturnValue = True
                                    Else
                                        Throw New Exception("No records affected" & vbCrLf & oMyDalUpdate.ErrorMessage)
                                    End If

                                Else

                                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalUpdate.ErrorMessage)

                                End If

                            Else
                                'Delete entry
                                sMySQL = "DELETE FROM ImportedFiles WHERE Company_ID = " & Trim(Str(iCompany_ID)) & " AND FileSetup_ID = " & Trim(Str(iFileSetup_ID)) & " AND File_ID = " & sTemp & " AND Identifier = '" & sIdentifier & "' AND Bank = '" & sBank & "'"

                                oMyDalUpdate.SQL = sMySQL

                                If oMyDalUpdate.ExecuteNonQuery Then

                                    If oMyDalUpdate.RecordsAffected = 1 Then
                                        bReturnValue = True
                                    Else
                                        Throw New Exception("No records affected" & vbCrLf & oMyDalUpdate.ErrorMessage)
                                    End If

                                Else

                                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalUpdate.ErrorMessage)

                                End If

                            End If

                        End If 'If oMyDal.Reader_HasRows Then

                        ' 16.07.2019
                        ' COMMENTED as this part is never executed !!!!
                        '    'Check if we are going to delete some records
                        '    sMySQL = "SELECT COUNT(*) AS Occurance FROM ImportedFiles WHERE Company_ID = " & Trim(Str(iCompany_ID)) & " AND FileSetup_ID = " & Trim(Str(iFileSetup_ID)) & " AND Bank = '" & sBank & "'"
                        '    oMyDal.SQL = sMySQL
                        '    If oMyDal.Reader_Execute() Then
                        '        Do While oMyDal.Reader_ReadRecord
                        '            iOccurance = CDbl(oMyDal.Reader_GetString("Occurance"))
                        '        Loop
                        '    Else
                        '        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                        '    End If
                        '    'If iOccurance > 500 Then
                        '    If iOccurance > 500 Then
                        '        'If more than 500 entries in the table, delete 100 of them
                        '        'sMySQL = "SELECT DateImported FROM ImportedFiles WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString & " AND Bank = '" & sBank & "' ORDER BY DateImported ASC"
                        '        ' 05.01.2015 Fjernet Bank
                        '        sMySQL = "SELECT DateImported FROM ImportedFiles WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString & " ORDER BY DateImported ASC"
                        '        oMyDal.SQL = sMySQL
                        '        If oMyDal.Reader_Execute() Then
                        '            Do While oMyDal.Reader_ReadRecord And iCounter < 101
                        '                iCounter = iCounter + 1
                        '            Loop
                        '            sDeleteUntil = oMyDal.Reader_GetString("DateImported")
                        '        Else
                        '            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                        '        End If

                        '        sDeleteUntil = oMyDal.Reader_GetString("DateImported")

                        '        ' 05.01.2015 Fjernet Bank
                        '        'sMySQL = "DELETE FROM ImportedFiles WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString & " AND Bank = '" & sBank & "' AND DateImported < '" & sDeleteUntil & "'"
                        '        sMySQL = "DELETE FROM ImportedFiles WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString & " AND DateImported < '" & sDeleteUntil & "'"

                        '        oMyDalUpdate.SQL = sMySQL

                        '        If oMyDalUpdate.ExecuteNonQuery Then
                        '            'OK, the records are deleted
                        '        Else
                        '            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalUpdate.ErrorMessage)
                        '        End If
                        '    End If
                    Else
                        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                    End If

                Else 'If bEndOfProfile Then

                    ' 16.07.2019 -
                    ' Moved deleting for more than 500 records here
                    'Check if we are going to delete some records
                    sMySQL = "SELECT COUNT(*) AS Occurance FROM ImportedFiles WHERE Company_ID = " & Trim(Str(iCompany_ID)) & " AND FileSetup_ID = " & Trim(Str(iFileSetup_ID)) & " AND Bank = '" & sBank & "'"
                    oMyDal.SQL = sMySQL
                    If oMyDal.Reader_Execute() Then
                        Do While oMyDal.Reader_ReadRecord
                            iOccurance = CDbl(oMyDal.Reader_GetString("Occurance"))
                        Loop
                    Else
                        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                    End If

                    If iOccurance > 500 Then
                        'If more than 500 entries in the table, delete 100 of them
                        sMySQL = "SELECT DateImported FROM ImportedFiles WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString & " ORDER BY DateImported ASC"
                        oMyDal.SQL = sMySQL
                        If oMyDal.Reader_Execute() Then
                            Do While oMyDal.Reader_ReadRecord And iCounter < 101
                                iCounter = iCounter + 1
                            Loop
                            sDeleteUntil = oMyDal.Reader_GetString("DateImported")
                        Else
                            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                        End If

                        sDeleteUntil = oMyDal.Reader_GetString("DateImported")
                        sMySQL = "DELETE FROM ImportedFiles WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString & " AND DateImported < '" & sDeleteUntil & "'"
                        oMyDalUpdate.SQL = sMySQL

                        If oMyDalUpdate.ExecuteNonQuery Then
                            'OK, the records are deleted
                        Else
                            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalUpdate.ErrorMessage)
                        End If
                    End If

                    ' Find Max file_id
                    sMySQL = "SELECT MAX(File_ID) AS MaxFileID FROM ImportedFiles WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString

                    oMyDal.SQL = sMySQL
                    If oMyDal.Reader_Execute() Then
                        Do While oMyDal.Reader_ReadRecord
                            iMaxFileID = CDbl(oMyDal.Reader_GetString("MaxFileID"))
                        Loop
                    Else
                        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                    End If

                    'New code 15.11.2006, added field InProgress
                    '12.05.2022 - Removed form where: AND (Succeeded = True OR InProgress = True)
                    'Old code
                    'sMySQL = "SELECT * FROM ImportedFiles WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString & " AND Identifier = '" & sIdentifier & "' AND Bank = '" & sBank & "' AND (Succeeded = True OR InProgress = True) ORDER BY DateImported DESC"
                    'New code
                    sMySQL = "SELECT * FROM ImportedFiles WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString & " AND Identifier = '" & sIdentifier & "' AND Bank = '" & sBank & "' ORDER BY DateImported DESC"
                    oMyDal.SQL = sMySQL
                    If oMyDal.Reader_Execute() Then
                        If oMyDal.Reader_HasRows Then
                            'Retrieve the date from the last identidal file to show in the warning.
                            Do While oMyDal.Reader_ReadRecord
                                sDate = oMyDal.Reader_GetString("DateImported")
                                Exit Do 'To get the newest one
                            Loop

                            'Add a new entry, and return false
                            '15.11.2006 - Added InProgress = True to the SQL
                            sMySQL = "INSERT INTO ImportedFiles (Company_ID, FileSetup_ID, File_ID, Identifier, Bank, Format, Name, DateImported, TimeImported, Succeeded, InProgress, bbUser)" & " VALUES(" & iCompany_ID.ToString & ", " & iFileSetup_ID.ToString & ", " & (iMaxFileID + 1).ToString & ", '" & sIdentifier & "', '" & sBank & "', " & iFormat.ToString & ", '" & sFilename & "', '" & VB6.Format(Now, "YYYYMMDD") & "', '" & VB6.Format(Now, "hhmmss") & "', False, True, '" & Left(Environ("Username"), 40) & "')"
                            oMyDalUpdate.SQL = sMySQL

                            If oMyDalUpdate.ExecuteNonQuery Then
                                iOccurance = iOccurance + CDbl(oMyDalUpdate.RecordsAffected)
                            Else
                                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalUpdate.ErrorMessage)
                            End If

                            bReturnValue = False
                        Else
                            'OK, add a new entry
                            '15.11.2006 - Added InProgress = True to the SQL
                            sMySQL = "INSERT INTO ImportedFiles (Company_ID, FileSetup_ID, File_ID, Identifier, Bank, Format, Name, DateImported, TimeImported, Succeeded, InProgress, bbUser)" & " VALUES(" & Trim(Str(iCompany_ID)) & ", " & Trim(Str(iFileSetup_ID)) & ", " & Trim(Str(iMaxFileID + 1)) & ", '" & sIdentifier & "', '" & sBank & "', " & Trim(Str(iFormat)) & ", '" & sFilename & "', '" & VB6.Format(Now, "YYYYMMDD") & "', '" & VB6.Format(Now, "hhmmss") & "', False, True, '" & Left(Environ("Username"), 40) & "')"

                            oMyDalUpdate.SQL = sMySQL

                            If oMyDalUpdate.ExecuteNonQuery Then
                                iOccurance = iOccurance + CDbl(oMyDalUpdate.RecordsAffected)
                            Else
                                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalUpdate.ErrorMessage)
                            End If
                        End If
                    Else
                        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                    End If

                End If 'If bEndOfProfile Then

            End If 'If Not EmptyString(sIdentifier) Then

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If
            If Not oMyDalUpdate Is Nothing Then
                oMyDalUpdate.Close()
                oMyDalUpdate = Nothing
            End If

            Throw New Exception("Function: CheckForDoubleImport" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If
        If Not oMyDalUpdate Is Nothing Then
            oMyDalUpdate.Close()
            oMyDalUpdate = Nothing
        End If

        Return bReturnValue

    End Function
    Public Function AddToImportedfiles(ByRef iCompany_ID As Short, ByRef iFileSetup_ID As Short, ByRef iFormat As Short, ByRef sFilename As String, ByRef sDate As String) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim oMyDalUpdate As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True
        Dim sTemp As String = ""
        Dim iOccurance As Double = 0
        Dim iMaxFileID As Double
        Dim sDeleteUntil As String = ""
        Dim iCounter As String = 0

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            oMyDalUpdate = New vbBabel.DAL
            oMyDalUpdate.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDalUpdate.ConnectToDB() Then
                Throw New System.Exception(oMyDalUpdate.ErrorMessage)
            End If

            sMySQL = "SELECT MAX(File_ID) AS MaxFileID FROM ImportedFiles WHERE Company_ID = " & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then
                Do While oMyDal.Reader_ReadRecord
                    iMaxFileID = CDbl(oMyDal.Reader_GetString("MaxFileID"))
                Loop
            Else
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
            End If

            'OK, add a new entry
            '15.11.2006 - Added InProgress = True to the SQL
            sMySQL = "INSERT INTO ImportedFiles (Company_ID, FileSetup_ID, File_ID, Identifier, Bank, Format, Name, DateImported, TimeImported, Succeeded, InProgress, bbUser)" & " VALUES(" & Trim(Str(iCompany_ID)) & ", " & Trim(Str(iFileSetup_ID)) & ", " & Trim(Str(iMaxFileID + 1)) & ", 'SWAPFILEREMOVEATEXPORT', ''," & Trim(Str(iFormat)) & ", '" & sFilename & "', '" & VB6.Format(Now, "YYYYMMDD") & "', '" & VB6.Format(Now, "hhmmss") & "', False, True, '" & Left(Environ("Username"), 40) & "')"
            oMyDalUpdate.SQL = sMySQL

            If oMyDalUpdate.ExecuteNonQuery Then
                iOccurance = iOccurance + CDbl(oMyDalUpdate.RecordsAffected)
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalUpdate.ErrorMessage)
            End If


        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If
            If Not oMyDalUpdate Is Nothing Then
                oMyDalUpdate.Close()
                oMyDalUpdate = Nothing
            End If

            Throw New Exception("Function: AddToImportedFiles" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If
        If Not oMyDalUpdate Is Nothing Then
            oMyDalUpdate.Close()
            oMyDalUpdate = Nothing
        End If

        Return bReturnValue

    End Function
    Public Function SwapFileBackup(ByVal sBackupFolder As String) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim oMyDalUpdate As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True
        Dim oBackup As New vbBabel.BabelFileHandling
        Dim bBackup As Boolean

        Try
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            oMyDalUpdate = New vbBabel.DAL
            oMyDalUpdate.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDalUpdate.ConnectToDB() Then
                Throw New System.Exception(oMyDalUpdate.ErrorMessage)
            End If

            sMySQL = "SELECT Name AS FileName FROM ImportedFiles WHERE Identifier = 'SWAPFILEREMOVEATEXPORT'"

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then
                Do While oMyDal.Reader_ReadRecord

                    oBackup.SourceFile = oMyDal.Reader_GetString("FileName")
                    oBackup.BackupPath = sBackupFolder
                    oBackup.DeleteOriginalFile = True
                    oBackup.FilesetupID = 99
                    oBackup.InOut = "I"
                    oBackup.FilenameNo = 1
                    oBackup.Client = "0"
                    oBackup.BankAccounting = "S"

                    'Creates the backupfile, tests if exists, deletes the file if found
                    bBackup = oBackup.CreateBackup()
                Loop
            Else
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
            End If

            'Delete entries in ImportedFiles
            sMySQL = "DELETE FROM ImportedFiles WHERE Identifier = 'SWAPFILEREMOVEATEXPORT'"
            oMyDalUpdate.SQL = sMySQL

            If Not oMyDalUpdate.ExecuteNonQuery Then
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalUpdate.ErrorMessage)
            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If
            If Not oMyDalUpdate Is Nothing Then
                oMyDalUpdate.Close()
                oMyDalUpdate = Nothing
            End If

            Throw New Exception("Function: SwapFileBackup" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If
        If Not oMyDalUpdate Is Nothing Then
            oMyDalUpdate.Close()
            oMyDalUpdate = Nothing
        End If

        Return bBackup

    End Function
    ' XNET 11.03.2013 - added check for Visma Integerator license
    Public Function IsThisVismaIntegrator() As Boolean
        Dim oLicense As vbBabel.License
        'Dim oFS As New Scripting.FileSystemObject

        ' added next test 19.06.2017
        'If Not oFs.FileExists(BB_LicensePath) Then
        If Not Dir(BB_LicensePath) = "" Then
            oLicense = New vbBabel.License 'CreateObject ("vbbabel.license")
            If InStr(oLicense.LicServicesAvailable, "V") = 0 Then
                IsThisVismaIntegrator = False
            Else
                IsThisVismaIntegrator = True
            End If
            oLicense = Nothing
        Else
            ' no licensefile present, assume not Visma
            IsThisVismaIntegrator = False
        End If
        'oFS = Nothing


    End Function
    'Public Function IsThisVismagicIntegrator() As Boolean
    '    ' Vismagic is like VismaIntegrator, PLUS they have some added HSBC formats
    '    ' In the Licenseprogram, set Type = VISMAGIC
    '    Dim oLicense As vbBabel.License
    '    'Dim oFS As New Scripting.FileSystemObject

    '    ' added next test 19.06.2017
    '    'If Not oFs.FileExists(BB_LicensePath) Then
    '    If Not Dir(BB_LicensePath) = "" Then
    '        oLicense = New vbBabel.License 'CreateObject ("vbbabel.license")
    '        If InStr(oLicense.LicServicesAvailable, "VISMAGIC") = 0 Then
    '            IsThisVismagicIntegrator = False
    '        Else
    '            IsThisVismagicIntegrator = True
    '        End If
    '        oLicense = Nothing
    '    Else
    '        ' no licensefile present, assume not Visma
    '        IsThisVismagicIntegrator = False
    '    End If
    '    'oFS = Nothing


    'End Function
    Public Function IsThisSEBIntegrator() As Boolean
        Dim oLicense As vbBabel.License

        oLicense = New vbBabel.License 'CreateObject ("vbbabel.license")
        If InStr(oLicense.LicServicesAvailable, "SEB") = 0 Then
            IsThisSEBIntegrator = False
        Else
            IsThisSEBIntegrator = True
        End If
        oLicense = Nothing

    End Function
    ' 04.03.2016 - added check for DNB Connect Integerator (TBI) license
    Public Function IsThisDNBIntegrator() As Boolean
        Dim oLicense As vbBabel.License
        oLicense = New vbBabel.License 'CreateObject ("vbbabel.license")
        If InStr(oLicense.LicServicesAvailable, "T") = 0 And InStr(oLicense.LicServicesAvailable, "DNBX") = 0 Then
            IsThisDNBIntegrator = False
        Else
            IsThisDNBIntegrator = True
        End If
        oLicense = Nothing

    End Function
    ' 04.03.2016 - added check for DNB Connect Integerator (TBI) license
    Public Function IsThisDNBXMLIntegrator() As Boolean
        Dim oLicense As vbBabel.License
        oLicense = New vbBabel.License 'CreateObject ("vbbabel.license")
        If InStr(oLicense.LicServicesAvailable, "DNBX") > 0 Then
            IsThisDNBXMLIntegrator = True
        Else
            IsThisDNBXMLIntegrator = False
        End If
        oLicense = Nothing

    End Function
    'XNET - New function
    Public Function MonthFrom3Letters(ByVal sMonth As String) As String
        Dim sReturnMonth As String = ""

        Select Case UCase(sMonth)
            Case "JAN"
                sReturnMonth = "01"
            Case "FEB"
                sReturnMonth = "02"
            Case "MAR"
                sReturnMonth = "03"
            Case "APR"
                sReturnMonth = "04"
            Case "MAY", "MAI"
                sReturnMonth = "05"
            Case "JUN"
                sReturnMonth = "06"
            Case "JUL"
                sReturnMonth = "07"
            Case "AUG"
                sReturnMonth = "08"
            Case "SEP"
                sReturnMonth = "09"
            Case "OCT", "OKT"
                sReturnMonth = "10"
            Case "NOV"
                sReturnMonth = "11"
            Case "DEC", "DES"
                sReturnMonth = "12"
        End Select
        MonthFrom3Letters = sReturnMonth

    End Function
    ' XNET 11.06.2013 added function
    Public Function TreatKongsbergAutomotive(ByVal oProfile As vbBabel.Profile, ByRef sCurrency As String, ByRef sBranchNo As String, ByRef eBank_BranchType As vbBabel.BabelFiles.BankBranchType) As String
        Dim bAccountFound As Boolean
        Dim oaccount As vbBabel.Account
        Dim oClient As vbBabel.Client
        Dim oFilesetup As vbBabel.FileSetup
        Dim sI_Account As String = ""

        bAccountFound = False
        'Set oFilesetup = oPayment.VB_Profile.FileSetups(iFilesetup_ID)
        For Each oFilesetup In oProfile.FileSetups
            For Each oClient In oFilesetup.Clients
                For Each oaccount In oClient.Accounts
                    If sCurrency = "SEK" Or sCurrency = "EUR" Or sCurrency = "USD" Then
                        If oaccount.CurrencyCode = sCurrency Then
                            ' replace account with correct account from ClientSetup
                            sI_Account = oaccount.Account
                            bAccountFound = True
                            Exit For
                        End If
                    Else
                        ' for EUR and other currencies, debitaccount in DNB London
                        If oaccount.CurrencyCode = "GBP" Then
                            ' replace account with correct account from ClientSetup
                            sI_Account = oaccount.Account
                            bAccountFound = True
                            Exit For
                        End If
                    End If
                Next oaccount
                If bAccountFound Then Exit For
            Next oClient
        Next oFilesetup

        ' check sortcode
        If Left$(sBranchNo, 4) = "//SC" Then
            sBranchNo = Mid$(sBranchNo, 5)
            eBank_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode
        End If

        TreatKongsbergAutomotive = sI_Account

    End Function
    Public Function CreateMessageReference(ByRef oBabelFile As vbBabel.BabelFile, Optional ByVal nMsgIDVariable As Double = 0) As String
        'Dim oLocalBabelfile As vbBabel.BabelFile
        Dim oLocalBatch As vbBabel.Batch
        Dim oLocalPayment As vbBabel.Payment
        Dim nTotalAmount As Double
        Dim nTotalDate As Double
        Dim nTotalAccount As Double
        Dim sAccount As String

        nTotalAmount = 0
        nTotalDate = 0
        nTotalAccount = 0
        'For Each oBabelFile In oBabelFiles
        nTotalAmount = nTotalAmount + oBabelFile.MON_InvoiceAmount
        For Each oLocalBatch In oBabelFile.Batches
            For Each oLocalPayment In oLocalBatch.Payments
                nTotalDate = nTotalDate + Val(oLocalPayment.DATE_Payment)
                sAccount = RemoveCharacters(UCase(Trim(oLocalPayment.E_Account)), "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
                If vbIsNumeric(sAccount, "0123456789") Then
                    nTotalAccount = nTotalAccount + Val(Right(sAccount, 10))
                End If
            Next oLocalPayment
        Next oLocalBatch
        'Next oBabelFile

        CreateMessageReference = Right("BABELBANK" & Trim(Str(nTotalAmount + nTotalDate + nTotalAccount + nMsgIDVariable)), 35)

    End Function
    Public Function CreateUniqueReferenceFromBabelFiles(ByRef oBabelFiles As vbBabel.BabelFiles) As String
        'Dim oLocalBabelfile As vbBabel.BabelFile
        Dim oLocalBabelFile As vbBabel.BabelFile
        Dim oLocalBatch As vbBabel.Batch
        Dim oLocalPayment As vbBabel.Payment
        Dim nTotalAmount As Double
        Dim nTotalDate As Double
        Dim nTotalAccount As Double
        Dim sAccount As String

        nTotalAmount = 0
        nTotalDate = 0
        nTotalAccount = 0
        'For Each oBabelFile In oBabelFiles
        For Each oLocalBabelFile In oBabelFiles
            nTotalAmount = nTotalAmount + oLocalBabelFile.MON_InvoiceAmount
            For Each oLocalBatch In oLocalBabelFile.Batches
                For Each oLocalPayment In oLocalBatch.Payments
                    nTotalDate = nTotalDate + Val(oLocalPayment.DATE_Payment)
                    sAccount = RemoveCharacters(UCase(Trim(oLocalPayment.E_Account)), "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
                    If vbIsNumeric(sAccount, "0123456789") Then
                        nTotalAccount = nTotalAccount + Val(Right(sAccount, 10))
                    End If
                Next oLocalPayment
            Next oLocalBatch
        Next oLocalBabelFile

        CreateUniqueReferenceFromBabelFiles = Right("BABELBANK" & Trim(Str(nTotalAmount + nTotalDate + nTotalAccount)), 35)

    End Function
    Public Sub RearrangeBankAddress(ByRef oPayment As vbBabel.Payment, Optional ByVal bE_Bank As Boolean = True, Optional ByVal bCorr_Bank As Boolean = False)
        'This function rearrange the address info to ensure that it is filled out correct
        ' If only one line the first entry is filled
        ' If two lines the two first and so on ....
        Dim sLine1 As String = ""
        Dim sLine2 As String = ""
        Dim sLine3 As String = ""
        Dim sLine4 As String = ""

        If bE_Bank Then
            sLine1 = oPayment.BANK_Adr1
            sLine2 = oPayment.BANK_Adr2
            sLine3 = oPayment.BANK_Adr3
            If Not EmptyString(sLine1 & sLine2 & sLine3) Then
                If EmptyString(sLine1) Then
                    If EmptyString(sLine2) Then
                        sLine1 = sLine3
                        sLine3 = ""
                    Else
                        sLine1 = sLine2
                        sLine2 = sLine3
                        sLine3 = ""
                    End If
                Else
                    If EmptyString(sLine2) Then
                        sLine2 = sLine3
                        sLine3 = ""
                    End If
                End If
            End If
            oPayment.BANK_Adr1 = sLine1
            oPayment.BANK_Adr2 = sLine2
            oPayment.BANK_Adr3 = sLine3
        End If
        If bCorr_Bank Then
            sLine1 = oPayment.BANK_NameAddressCorrBank1
            sLine2 = oPayment.BANK_NameAddressCorrBank2
            sLine3 = oPayment.BANK_NameAddressCorrBank3
            sLine4 = oPayment.BANK_NameAddressCorrBank4
            If Not EmptyString(sLine1 & sLine2 & sLine3 & sLine4) Then
                If EmptyString(sLine1) Then
                    If EmptyString(sLine2) Then
                        If EmptyString(sLine3) Then
                            sLine1 = sLine4
                            sLine4 = ""
                        Else
                            sLine1 = sLine3
                            sLine2 = sLine4
                            sLine3 = ""
                            sLine4 = ""
                        End If
                        sLine1 = sLine3
                        sLine3 = ""
                    Else
                        sLine1 = sLine2
                        If EmptyString(sLine3) Then
                            sLine2 = sLine4
                            sLine3 = ""
                            sLine4 = ""
                        Else
                            sLine2 = sLine3
                            sLine3 = sLine4
                            sLine4 = ""
                        End If
                    End If
                Else
                    If EmptyString(sLine2) Then
                        If EmptyString(sLine3) Then
                            sLine2 = sLine4
                            sLine3 = ""
                            sLine4 = ""
                        Else
                            sLine2 = sLine3
                            sLine3 = sLine4
                            sLine4 = ""
                        End If
                    Else
                        If EmptyString(sLine3) Then
                            sLine3 = sLine4
                            sLine4 = ""
                        End If
                    End If
                End If
            End If
            oPayment.BANK_NameAddressCorrBank1 = sLine1
            oPayment.BANK_NameAddressCorrBank2 = sLine2
            oPayment.BANK_NameAddressCorrBank3 = sLine3
            oPayment.BANK_NameAddressCorrBank4 = sLine4
        End If

    End Sub
    ' XokNET 13.12.2013
    Public Function StripSpecialChars(ByVal sInString As String) As String
        ' Strip a string for special characters
        ' chr(9) (tab)
        ' chr(13) CR
        ' chr(10) LF
        ' chr(27) Esc
        '----
        Dim sReturn As String
        'sReturn = Replace(Replace(Replace(Replace(sInString, Chr(9), ""), Chr(13), ""), Chr(10), ""), Chr(27), "")
        If sInString Is Nothing Then
            sInString = ""
        End If
        sReturn = sInString.Replace(Chr(9), "")
        sReturn = sInString.Replace(Chr(13), "")
        sReturn = sInString.Replace(Chr(10), "")
        sReturn = sInString.Replace(Chr(27), "")

        StripSpecialChars = sReturn
    End Function
    Public Function HaveToShowLoginForm(ByVal sConnString As String, ByVal sUID As String, ByVal sPWD As String) As Boolean
        Dim bShowLoginForm As Boolean

        bShowLoginForm = False
        If InStr(1, sConnString, "=UID", vbTextCompare) > 0 Then
            If EmptyString(sUID) Then
                bShowLoginForm = True
            End If
        End If
        If InStr(1, sConnString, "=PWD", vbTextCompare) > 0 Then
            If EmptyString(sPWD) Then
                bShowLoginForm = True
            End If
        End If
    End Function
    Public Function FindCountryNameFromSWIFT(ByVal sSWIFT As String) As vbBabel.BabelFiles.CountryName
        'The CountryName is defined in vbBabel.BabelFiles as Enum
        Dim eReturnValue As vbBabel.BabelFiles.CountryName = vbBabel.BabelFiles.CountryName.Unknown

        sSWIFT = Trim(sSWIFT)
        ' 23.12.2015 - added posibilty to use countrycode as inputparameter
        If Len(sSWIFT) = 2 Then
            sSWIFT = "XXXX" & sSWIFT & "XX"
        End If

        If Len(sSWIFT) = 8 Or Len(sSWIFT) = 11 Then

            Select Case Mid(sSWIFT, 5, 2)

                Case "NO"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Norway
                Case "SE"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Sweden
                Case "DK"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Denmark
                Case "FI"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Finland
                Case "IS"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Iceland
                Case "EE"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Estonia
                Case "LV"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Latvia
                Case "LT"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Lithuania
                Case "GB"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Great_Britain
                Case "IE"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Ireland
                Case "DE"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Germany
                Case "NL"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Netherlands
                Case "BE"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Belgium
                Case "LU"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Luxemburg
                Case "FR"
                    eReturnValue = vbBabel.BabelFiles.CountryName.France
                Case "PL"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Poland
                Case "HK"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Hong_Kong
                Case "SG"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Singapore
                Case "US"
                    eReturnValue = vbBabel.BabelFiles.CountryName.USA
                Case "AU"
                    eReturnValue = vbBabel.BabelFiles.CountryName.Australia
                Case Else
                    eReturnValue = vbBabel.BabelFiles.CountryName.Unknown

            End Select

        Else
            eReturnValue = vbBabel.BabelFiles.CountryName.Unknown
        End If

        Return eReturnValue

    End Function
    Public Function IsBaltic(ByVal e As vbBabel.BabelFiles.CountryName) As Boolean
        Dim bReturnValue As Boolean = False

        If e = vbBabel.BabelFiles.CountryName.Estonia Then
            bReturnValue = True
        ElseIf e = vbBabel.BabelFiles.CountryName.Lithuania Then
            bReturnValue = True
        ElseIf e = vbBabel.BabelFiles.CountryName.Latvia Then
            bReturnValue = True
        Else
            bReturnValue = False
        End If


    End Function

    Public Function FindTypeOfSwedishAccount(ByRef oPayment As vbBabel.Payment, ByRef sErrorMessage As String) As Boolean
        Dim bReturnValue As Boolean = True

        If Not EmptyString((oPayment.BANK_SWIFTCode)) Then
            Select Case oPayment.BANK_SWIFTCode
                Case "BANKGIRO"
                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                    oPayment.BANK_BranchNo = Left(oPayment.E_Account, 4)
                Case "NDEASESS"
                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_PlusGiro
                    oPayment.E_Account = Trim(oPayment.E_Account)
                Case Else
                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount
                    oPayment.E_Account = Trim(oPayment.E_Account)
                    oPayment.BANK_BranchNo = Left(oPayment.E_Account, 4)
                    'If bankaccountnumber starts with 8 then clearingcode is 5 positions
            End Select
        Else
            oPayment.E_Account = Trim(oPayment.E_Account)
            Select Case Len(oPayment.E_Account)
                Case 7, 8
                    'This may also be Plusgiro as far as I understand, but 
                    '  we take an assumption that it's not
                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                    oPayment.BANK_BranchNo = ""
                Case 11, 12, 13, 14, 15, 16
                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount
                    oPayment.BANK_BranchNo = Left(oPayment.E_Account, 4)
                    'If bankaccountnumber starts with 8 then clearingcode is 5 positions
                Case Else
                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                    sErrorMessage = LRS(35024, oPayment.E_Account)
                    bReturnValue = False

            End Select
            'oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
        End If

        Return bReturnValue

    End Function
    Public Function ValidateBBANAccount89(ByVal sAccount As String, ByVal eCountry As vbBabel.BabelFiles.CountryName) As Boolean
        'Canada: Bank account number must be maximum 10 digits
        'Denmark: Bank account number must be 14 digits. 
        '   The first 4 digits of the bank account number must be the bank registration number. 
        '   The last 10 dig-its must be the account number. 
        '   If the bank registration number is shorter than 4 digits or the account number is shorter than 10 digits, 
        '   it must be right aligned and padded with leading zeroes.
        'Estonia: Bank account number must be 11 digits
        'Finland: No BBAN
        'Germany: Bank account number must consist of 10 digits. 
        '   If the account number is shorter than 10 digits it must be right aligned and padded with leading zeroes
        'Latvia: Bank account number must be 8 digits
        'Lithuania: Bank account number must be 11 digits
        'Norway: Bank account number must be 11 digits
        'Poland: No BBAN
        'Russia: Bank account number consist of 20 digits
        'Sweden:
        '   Bankgiro: Bankgiro number minimum 7 and maximum 8 digits. Clearing code is not used.
        '       Note: International and high value payments cannot be made from/to bankgiro number.
        '   PlusGiro: Bank account number minimum 2 and maximum 8 digits. Clearing code is not used.
        '   Nordea(from Nordea documentation): For international and high value payments, only Nordea bank and/or PlusGiro account may be used. 
        '       Nordea bank account number must be 11 digits. The first 4 digits of the bank account number must be the clearing code. Account currency must be stated
        'United Kingdom: Bank account number must consist of 10 digits. If the account number is shorter than 10 digits it must be right aligned and padded with leading zeroes.
        'U.S.A.: Bank account number must be maximum 10 digits
        Dim bReturnValue As Boolean = False




    End Function

    Public Function GetInnerText(ByRef node As System.Xml.XmlNode, ByRef sXPath As String, Optional ByRef nsMgr As System.Xml.XmlNamespaceManager = Nothing) As String
        Dim sReturnValue As String = ""
        Dim nodeNew As System.Xml.XmlNode

        nodeNew = node.SelectSingleNode(sXPath, nsMgr)
        If nodeNew Is Nothing Then
            sReturnValue = ""
        Else
            sReturnValue = nodeNew.InnerText
        End If

        Return sReturnValue
    End Function
    Public Function Date2Julian(ByVal vDate As Date) As Long
        ' return Julian date, as YYddd

        Date2Julian = CLng(Right(Format(Year(vDate), "0000"), 2) _
                      + Format(DateDiff("d", CDate("01/01/" _
                      + Format(Year(vDate), "0000")), vDate) _
                      + 1, "000"))

    End Function
    Function bbSetPayCode(ByRef sInCode As String, ByRef sType As String, ByRef sFormat As String, Optional ByVal bCredits As Boolean = True, Optional ByRef sInCode2 As String = "", Optional ByRef sInCode3 As String = "", Optional ByRef eCountryName As vbBabel.BabelFiles.CountryName = vbBabel.BabelFiles.CountryName.Unknown, Optional ByVal sMessageType As String = "", Optional ByVal eBank As vbBabel.BabelFiles.Bank = vbBabel.BabelFiles.Bank.No_Bank_Specified) As String
        ' Send in a PayCode from one format, translate to BabelBank generic PayCode
        Dim sReturnCode As String = ""

        ' Type              BabelBank Telepay Telep.ny Dirrem Datadialog Autogiro  OCR  ErhvervsGiro  EDI       PAY/DEBMUL DTAUS     ShipNet  MT940      PeriodiskaBetalningar(FI)
        '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        ' UTBETALINGER
        ' ------------
        ' (Med melding)
        ' Unknown              100                                                                                                             MSC
        ' ISO Other, Other  PMNT OTHER OTHR             100 
        ' Faste oppdrag        101      030                                                                                      8/108
        ' Husleie              102      033
        ' Kontingent           103      034
        ' AB-kontrakt          104      035
        ' Skatt/avgift         105      036   609                                                                                9/109
        ' Moms                 106      037
        ' Melding sendt fra ..
        '  ..annen institusjon 108            608

        ' Egenprod. giro utbet.130            630
        ' Overf�ring m/melding 150      150   602      03/66                                410/411              21        51    10/110       TRF + the rest
        ' ISO Issued Credit trans, Automatic trans  PMNT ICDT AUTT
        ' m/melding+BANKkonto  151      150   602      03/66                                410/411              21        51    10/110       TRF + the rest
        ' Utbet uten mott.konto160            603      04                                   630                  23              12/112       CHK/CLR/COL/ECK/TCK
        ' Statens konsernkonto 170                               50
        ' Hastebetaling        175                                                                               ZZ               5/105
        ' 17.10.2011 - New "urgent" for ISO, SameDayValue
        ' SameDayValue         176                                                                               ??               5/105
        ' Betaling p� vent     177                                                                                8
        ' FasterPayment GB     178
        ' UDUS (Denmark)       180
        ' Postgiro Sweden      190
        ' (Massetrans)
        ' Unknown mass         200
        ' ISO Issued Credit trans, ACH Trans    PMNT ICDT ATXN
        ' Pensjon              201      157   607       65       41                                                                1/101 (utland)           20
        ' Barnetrygd           202      160   607                40                                                                                         19
        ' L�nn                 203      165   604       01                                  605                            53      2/102 (utland)           10
        ' Hyretrekk            204      166   605
        ' Landbruksoppgj�r     205      167   606       62                                                                         3/103                    60-68
        ' L�nn (2dgs. forl�p)  206                                                          600
        ' Transf by public fund207                                                                                         56
        ' Capital-building fringe benefit (CBFB) (with
        '   savings premium)   208                                                                                         54 (?)
        ' Fritekst massebet.   221            621
        ' Fritekst(egenref 22) 222            622
        ' Div overf. u melding 250            600       02
        ' ISO Issued Credit trans, ACH Debit    PMNT ICDT ACDT
        ' Statenskonsernk.l�nn 270                               51
        ' Wire USA              281

        ' (KID)
        ' Bet. med KID,FIK etc 301            601       12                                  910 +++                                 11/111
        ' KID og underspesif.  302                      16
        ' KID og kreditnota    303                      17
        ' 06.02.2015 - Added FI codes
        ' FIK04                304            601
        ' FIK01                311            601
        ' FIK15                315            601
        ' FIK71                371            601
        ' FIK73                373            601
        ' FIK75                375            601

        ' (Diverse)
        ' Visa                 401                      39                                                                         4/104
        ' Renter               402                                                                                                 6/106
        ' Til egen konto       403                                                                                                 7/107
        ' ISO Issued Cash Concentration, Intra Company trans PMNT ICCN ICCT
        ' Meddelelse p� mod-
        ' tagers kontoutskrift 404
        ' 19.10.2011 Added Treasury
        ' Treasury              405
        ' XNET 27.11.2012 Added Finansielle betalinger
        ' Finansielle betalinger 406
        ' Financial (CORT)      406
        ' ISO Issued Credit Trans, Cross-Border Credit PMNT ICDT XBCT           407
        ' ISO Issued Direct Debits, Direct Debit Under Reserve PMNT IDDT URDD   408
        ' ISO Autogiro privat, Sweden   XAP                                     409
        ' ISO Autogiro F�retag, Sweden  XAF                                     410
        ' ISO Autogiro Nordea, Sweden   XAN                                     411
        ' ISO Autogiro several countrues                                        412
        ' ISO Merchant Card Trans, POS  PMNT MCRD POSP                          413
        ' ISO Cash payments (Danske bank, Bits) used by Loomis                  414

        'SWEDISH DOMESTIC PAYMENTS (Range 331-360)
        'FROM BANKGIRONUMBER
        'to Bankgiro including an OCR reference
        '                      331      
        'to Bankgiro including an OCR reference, an OCR credit note
        '                      332      
        'to Bankgiro including a none OCR reference, a none OCR credit note
        '                      333      
        'to Bankgiro including free text remittance info
        '                      334      
        'to a bank account including free text remittance info populated by the element <Ustrd>
        '                      335      
        'to a bank account including free text remittance info populated by the element <Purp><Prtry>
        '                      336      
        'to money order (payment advise)
        '                      337      
        'to a bank account
        '                      338      

        'FROM PLUSGIRONUMBER
        'to PlusGirot including an OCR reference
        '                      341      
        'to PlusGirot including two none OCR references, none OCR credit note (Structured??)
        '                      342      
        'to PlusGirot including free text remittance info, and ultimate(debtor)
        '                      343      
        'to Bankgiro including an OCR reference
        '                      344      
        'to Bankgiro including free text remittance info
        '                      345      
        'to a bank account including free text remittanceinfo
        '                      346      
        'to money order (payment advise)
        '                      347      
        'Salary payment to a bank account
        '                      348      
        'Pension payment to a bank account
        '                      349      
        'Intercompany payment from PlusGirot to a bank account including freetext remittance info (domestic SWIFT) (high value)
        '                      350      

        'FROM BANKACCOUNTNUMBER
        'Same day value payment from a Nordea account to a bank account including freetext remittance info (via STOK) (high value)
        '                      351      
        'Financial payment from a Nordea account to a bank account including freetext remittance info (domestic SWIFT) (high value)
        '                      352      

        ' INNBETALINGER
        ' -------------
        ' Unknown              600                                                                    ZZZ
        ' ISO Other Other   PMNT OTHR OTHR

        'Transactions without KID
        ' .. Electronic paym   601                                                                    233/IBB/UBB 'XOKNET 19.08.2013 Added IBB and UBB
        '*ISO: Received Credit Transfer, Automatc Transfer, PMNT RCDT AUTT, and node Strd doesn't exists see *
        ' .. Manual payments   602                                                                    234
        ' .. Bank internal tra 603                                                                    BKT
        ' .. Advanced Payment  604                                                                    ADV
        '    (Forh�ndsbetaling)
        ' .. Bankgiro          605                                                                    BGI
        ' .. Bank Draft        606                                                                    BKD
        ' .. Cash Management Transfer
        '  (Likv.styringstrans)607                                                                    CAS
        ' .. Cheque Internat.  608                                                                    CHI
        ' .. Cheque National   609                                                                    CHN/26 'XOKNET 19.08.2013 Added 26
        ' .. Commercial Credit 610 '                                                                   COC
        ' .. Structured paym.  629                                                                    240
        '*ISO: Received Credit Transfer, Automatc Transfer, PMNT RCDT AUTT, and node Strd exists see *
        ' .. Cash Payment
        '    by post           630                                                                    CPP
        ' .. Buying or selling
        '    Foreign Notes     631                                                                    CUX
        ' .. Dividend          632                                                                    DIV
        ' .. Foreign Exchange  633                                                                    FEX
        ' .. Purchase and sale
        '    of goods          634                                                                    GDS
        ' .. Government Payment635                                                                    GVT
        ' .. Insurance Premium 636                                                                    INS
        ' .. Interest          637                                                                    INT
        ' .. License Fees      638                                                                    LIF
        ' .. Loan              639                                                                    LOA
        ' .. Loan Repayment    640                                                                    LOR
        ' .. Pension           641                                                                    PEN
        ' .. Rent              642                                                                    REN
        ' .. Salary            643                                                                    SAL
        ' .. Purchase and Sale
        '    of Services       644                                                                    SCV
        ' .. Social Secutiry
        '    Benefit           645                                                                    SSB
        ' .. Tax Payment       646                                                                    TAX
        ' .. Value Added Tax   647                                                                    VAT
        ' .. Commision, Gebyr  648                                                                    COM
        ' .. Telefonbetaling   649
        ' Derivatives          652
        ' .. ForwardContractRate 670 - Retrieved from specific conditions in CREMUL
        ' ISO Merchant card POS (pmnt mcrd posp) PMNT MRCD POSP                 671
        ' ISO Received Credit, Cross Border PMNT RCDT XBCT                      672
        ' ISO Received Cash Concentration Trans, Intra Company PMNT RCCN ICCT   673
        ' ISO Misc Credit Operations Other PMNT MCOP OTHR                       674
        ' ISO Received Credit, Domestic Credit  PMNT RCDT DMCT                  675 
        ' ISO Received Credit, ACH Credit   PMNT RCDT ACDT                      676
        ' ISO Received Credit, ACH Trans.   PMNT RCDT ATXN                      677
        ' ISO Plusgiro. IS      PIS                                             678
        ' ISO Plusgiro. TIPS    PTI                                             679
        ' ISO Plusgiro. Girodirekt(CR1) PGD                                     680

        ' ISO Doktolk with IS   PDI                                             682
        ' ISO Topping          683

        '-- some US-payments
        ' Lockbox              660
        ' ACH CTX              661
        ' ACH CCD              662
        ' ACH PPD              663
        ' DnBNOR MT940         665

        ' Nattsafe;
        '    Nattsafe          650
        '    Veksel nattsafe   651

        ' .. Autogiro u/meld.  611                                        02                          232
        ' ISO Direct Debit PMNT RDDT PMDD       
        ' ISO Autogiro/DirectDebit Norway,Denmark, Finland, Poland

        ' .. Autogiro m/meld.  612                                        03                                               04
        ' .. Autog. authorization of
        '    payee to collect  613                                                                                         05000
        ' .. Direct debit from POS drawing
        '    - electronic cash 614                                                                                         05005
        ' .. Direct debit from POS drawing
        '    with foreign card 615                                                                                         05006
        ' .. Direct debit from POS drawing
        '    - POZ             616                                                                                         05015
        ' ISO Autogiro privat, Sweden   XAP     617
        ' ISO Autogiro F�retag, Sweden  XAF     618
        ' ISO Autogiro Nordea, Sweden   XAN     619
        ' XNET 30.10.2012
        ' Avtalegiro trekk     670


        'SEPA -Payments
        'SEPA                  500

        ' (Transactions with KID)
        ' Trans belastet konto 510                                               10                   230/YT8
        ' .. fra faste oppdrag 511                                               11
        ' .. fra Dir.rem       512                                               12
        ' .. fra BTG           513                                               13
        ' .. fra skrankegiro   514                                               14
        ' .. fra avtalegiro    515                                               15
        ' .. fra Telegiro      516                                               16
        ' .. fra giro-bet cash 517                                               17
        ' ISO: Credttransfer with agreed commercial info PMNT RCDT VCOM, set to 510
        ' .. wrong KID         518                                                                    231
        ' .. Danish Inpaymentform (FIK?) 519                                                          IBK 'XOKNET 19.08.2013 Added 519
        ' ISO Doktolk with TIPS PDT 520

        ' (Specialtransaction for AvtaleGiro, FBOs)
        ' .. all FBOs - info   530                                                0
        ' .. New FBO           531                                                1
        ' .. Deleted FBO       532                                                2

        ' Factoring
        '-----------
        ' Faktura               701
        ' Fakturahistorikk      705
        ' Kontant faktura       707
        ' Kontant kreditnota    708
        ' Kreditnota            709
        ' Kundepost             710
        ' Saldoposter           720 '12.05.2020 - Used for transactions marked "KONTOT�MMING" for Danske Bank SG Finans (set in oBatch.ReadCamt054)
        '
        ' Inkasso
        ' -------
        ' Inkassoinnbetaling    751

        ' Mobile payments
        ' ----------------
        ' Vipps fra DNB         801

        ' PayPal                851

        'XNET 10.10.2012 Added next 3 lines
        ' Spesialkoder for kunder
        ' Vingcard_netting_ut   901
        ' Vingcard_netting_inn  902
        '-----------------------------------------------------------------------

        Select Case sFormat

            ' XNET 16.12.2010 added TELEPAY+
            Case "TELEPAY", "TELEPAY2", "TELEPAY+"

                Select Case sInCode
                    Case "030" ' Faste oppdrag
                        sReturnCode = "101"
                    Case "033" ' Husleie
                        sReturnCode = "102"
                    Case "034" ' Kontingent
                        sReturnCode = "103"
                    Case "035" ' AB Kontrakt
                        sReturnCode = "104"
                    Case "036", "609" ' Skatt/avgift
                        sReturnCode = "105"
                    Case "037" ' Moms
                        sReturnCode = "106"
                    Case "150", "602" 'Overf�ring med melding
                        sReturnCode = "150"
                    Case "600" 'Diverse overf�ring uten melding
                        sReturnCode = "250"
                    Case "603" ' Utbet. uten mottakerkonto
                        sReturnCode = "160"
                    Case "608" ' Melding sendt fra annen institusjon
                        sReturnCode = "108"
                    Case "630" ' Egenprodusert giro
                        sReturnCode = "130"

                        ' massetrans
                    Case "157", "607" ' Pensjon
                        sReturnCode = "201"
                    Case "160" ' Barnetrygd
                        sReturnCode = "202"
                    Case "165", "604" ' L�nn
                        sReturnCode = "203"
                    Case "166", "605" ' Hyretrekk
                        sReturnCode = "204"
                    Case "167", "606" ' Landbruksoppgj�r
                        sReturnCode = "205"
                    Case "621" ' Fritekst massebet. fra egenref i betfor21
                        sReturnCode = "221"
                    Case "622" ' Fritekst massebet. fra egenref i betfor22
                        sReturnCode = "222"

                        'KID
                    Case "601" ' Bet med KID
                        sReturnCode = "301"

                    Case Else
                        ' something rotten - but give it a standard number:
                        If sType = "M" Or sType = "L" Then
                            ' massetrans
                            sReturnCode = "165" 'l�nn
                        Else
                            ' fakturatrans
                            sReturnCode = "150" 'overf�ring med melding
                        End If

                End Select

            Case "DIRREM"
                Select Case sInCode
                    Case "03" ' Overf�ring med melding
                        sReturnCode = "150"
                    Case "66" ' Overf�ring med melding
                        sReturnCode = "150"
                    Case "04" ' Utbet. uten mottakerkonto
                        sReturnCode = "160"

                        ' massetrans
                    Case "01" ' L�nn
                        sReturnCode = "203"
                    Case "65" ' Pensjon
                        sReturnCode = "201"
                    Case "62" ' Landbruksoppgj�r
                        sReturnCode = "205"
                    Case "02" ' Div overf�ring uten melding
                        sReturnCode = "250"

                        ' kid
                    Case "12" ' Bet. med KID
                        sReturnCode = "301"
                    Case "16" ' KID med underspesifikasjoner
                        sReturnCode = "302"
                    Case "17" ' Kid og kreditnota
                        sReturnCode = "303"

                        ' diverse
                    Case Is = "39" ' Visa
                        sReturnCode = "401"


                    Case Else
                        ' fakturatrans
                        sReturnCode = "150" 'overf�ring med melding

                End Select

            Case "Autogiro", "AutogiroEngangs"

                Select Case sInCode
                    Case "02" ' Trans. uten melding
                        sReturnCode = "611"
                    Case "03" ' Trans med melding
                        sReturnCode = "612"
                End Select

            Case "OCR"

                Select Case sInCode
                    Case "10" ' Trans. fra giro belastet konto
                        sReturnCode = "510"
                    Case "11" ' Trans fra Faste Oppdrag
                        sReturnCode = "511"
                    Case "12" ' Trans fra Dir rem
                        sReturnCode = "512"
                    Case "13" ' Trans fra BTG
                        sReturnCode = "513"
                    Case "14" ' Trans fra skrankegiro
                        sReturnCode = "514"
                    Case "15" ' Trans fra avtalegiro
                        sReturnCode = "515"
                    Case "16" ' Trans fra Telegiro
                        sReturnCode = "516"
                    Case "17" ' Trans fra giro - betalt kontant
                        sReturnCode = "517"

                    Case Else
                        ' something rotten - but give it a standard number:
                        sReturnCode = "510" ' Trans. fra giro belastet konto

                End Select

            Case "OCR_FBO", "AVTALEGIRO_FBO"
                Select Case sInCode
                    Case "0"
                        sReturnCode = "530" 'All FBOs - just info
                    Case "1"
                        sReturnCode = "531" 'New FBO
                    Case "2"
                        sReturnCode = "532" 'Deleted FBO
                    Case Else
                        ' something rotten - but give it a standard number:
                        sReturnCode = "530" ' Trans. fra giro belastet konto

                End Select


            Case "ErhvervsGiro"

                Select Case sInCode
                    Case "410" ' Transfer to a known account. Freetext on bankstatement.
                        'If long freetext a seperate letter is sent
                        sReturnCode = "150"
                    Case "411" ' Transfer to a known account. Freetext on bank-statement.
                        sReturnCode = "150"
                    Case "600" ' Salary, 2-days ????
                        sReturnCode = "206"
                    Case "605" ' Salary, day by day ????
                        sReturnCode = "203"
                    Case "630" ' Check (receivers accountno. is unknown)
                        sReturnCode = "160"
                    Case "910" ' Indbetalingskort - Danish OCR
                        sReturnCode = "301"

                    Case Else
                        ' something rotten - but give it a standard number:
                        If sType = "M" Or sType = "L" Then
                            ' massetrans
                            sReturnCode = "200" 'l�nn
                        Else
                            ' fakturatrans
                            sReturnCode = "100" 'overf�ring med melding
                        End If

                End Select

            Case "CREMUL"

                Select Case sInCode
                    Case "230", "YT8", "AAE"   ' 10.11.2017 YT7 er fjernet av JPS - , "YT7" ' KID-transaction (AAE - Nordea Sweden) - 08.12.2014 - Added YT7 for Visma Collectors DnBNOR INPS CREMUL
                        sReturnCode = "510"
                    Case "231" ' Wrong KID
                        sReturnCode = "518"
                    Case "232", "DDT" ' Autogiro (DDT - Nordea Sweden)
                        sReturnCode = "611"
                    Case "233", "AAW", "IBB", "UBB"     ' Electronic payment (AAW - Nordea Sweden) ' Electronic payment (AAW - Nordea Sweden)
                        sReturnCode = "601"
                    Case "234", "AAH" ' Manual payment (AAH - Nordea Sweden)
                        sReturnCode = "602"
                        'Maybe we should create a specific code this type
                    Case "240" ' Electronic payment
                        sReturnCode = "629"
                    Case "ADV" ' Advanced Payment
                        sReturnCode = "604"
                    Case "BGI" ' Bankgiro
                        sReturnCode = "605"
                    Case "BKD" ' Bank Draft
                        sReturnCode = "606"
                    Case "BKT" ' Bank Internal transaction
                        sReturnCode = "603"
                    Case "CAS" ' Cash Management Transfer
                        sReturnCode = "607"
                    Case "CHI" ' Cheque Internat.
                        sReturnCode = "608"
                    Case "CHN", "26"     ' Cheque National
                        sReturnCode = "609"
                    Case "COC" ' Commercial Credit
                        sReturnCode = "610"
                    Case "CPP" ' Cash Payment by post
                        sReturnCode = "630"
                    Case "CUX" ' Buying or selling Foreign Notes
                        sReturnCode = "631"
                    Case "DIV" ' Dividend
                        sReturnCode = "632"
                    Case "FEX" ' Foreign Exchange
                        sReturnCode = "633"
                    Case "GDS" ' Purchase and sale of goods
                        sReturnCode = "634"
                    Case "GVT" ' Government Payment
                        sReturnCode = "635"
                    Case "IBK"     ' Insurance Premium
                        sReturnCode = "519"
                    Case "INS" ' Insurance Premium
                        sReturnCode = "636"
                    Case "INT" ' Interest
                        sReturnCode = "637"
                    Case "LIF" ' License Fees
                        sReturnCode = "638"
                    Case "LOA" ' Loan
                        sReturnCode = "639"
                    Case "LOR" ' Loan Repayment
                        sReturnCode = "640"
                    Case "PEN" ' Pension
                        sReturnCode = "641"
                    Case "REN" ' Rent
                        sReturnCode = "642"
                    Case "SAL" ' Salary
                        sReturnCode = "643"
                    Case "SCV" ' Purchase and Sale of Services
                        sReturnCode = "644"
                    Case "SSB" ' Benefit
                        sReturnCode = "645"
                    Case "TAX" ' Tax Payment
                        sReturnCode = "646"
                    Case "VAT" ' Value Added Tax
                        sReturnCode = "647"
                    Case "COM" ' Commision (Gebyr)
                        sReturnCode = "648"
                    Case "ZZZ"
                        sReturnCode = "600"

                    Case Else
                        ' something rotten - but give it a standard number:
                        sReturnCode = "601" ' Trans. fra giro belastet konto

                End Select

            Case "PAYMUL", "DEBMUL"

                Select Case sInCode
                    Case "ADV" ' Advanced Payment
                        sReturnCode = "604"
                    Case "CAS" ' Cash Management Transfer
                        sReturnCode = "607"
                    Case "COC" ' Commercial Credit
                        sReturnCode = "610"
                    Case "COM" ' Commision (Gebyr)
                        sReturnCode = "648"
                    Case "DIV" ' Dividend
                        sReturnCode = "632"
                    Case "FEX" ' Foreign Exchange
                        sReturnCode = "633"
                    Case "GDS" ' Purchase and sale of goods
                        sReturnCode = "634"
                    Case "GVT" ' Government Payment
                        sReturnCode = "635"
                    Case "INS" ' Insurance Premium
                        sReturnCode = "636"
                    Case "INT" ' Interest
                        sReturnCode = "637"
                    Case "LIF" ' License Fees
                        sReturnCode = "638"
                    Case "LOA" ' Loan
                        sReturnCode = "639"
                    Case "LOR" ' Loan Repayment
                        sReturnCode = "640"
                    Case "PEN" ' Pension
                        sReturnCode = "641"
                    Case "REN" ' Rent
                        sReturnCode = "642"
                    Case "SAL" ' Salary
                        sReturnCode = "643"
                    Case "SCV" ' Purchase and Sale of Services
                        sReturnCode = "644"
                    Case "SSB" ' Benefit
                        sReturnCode = "645"
                    Case "TAX" ' Tax Payment
                        sReturnCode = "646"
                    Case "VAT" ' Value Added Tax
                        sReturnCode = "647"
                    Case "ZZZ"
                        sReturnCode = "600"

                    Case Else
                        ' something rotten - but give it a standard number:
                        sReturnCode = "601" ' Trans. fra giro belastet konto

                End Select

                '    Select Case sInCode
                '    Case "8"     'Hold
                '        sReturnCode = "177"
                '    Case "21"     ' Banker's draft, domestic payment
                '        sReturnCode = "160"
                '    Case "23"     ' Bank cheque issued by payors bank
                '        sReturnCode = "160"
                '    Case "26"     ' Bank cheque issued by recipients bank
                '        sReturnCode = "160"
                '    Case "ZZ"     ' Urgent payment
                '        sReturnCode = "175"
                '    Case Else
                '        ' something rotten - but give it a standard number:
                '         sReturnCode = "100"   ' Trans. fra giro belastet konto
                '
                '
                '    End Select

            Case "DTAUS"

                Select Case Left(sInCode, 2)
                    Case "04"
                        sReturnCode = "612"
                    Case "05"
                        Select Case Right(sInCode, 3)
                            Case "000"
                                sReturnCode = "613"
                            Case "005"
                                sReturnCode = "614"
                            Case "006"
                                sReturnCode = "615"
                            Case "015"
                                sReturnCode = "616"
                            Case Else
                                Err.Raise(10025, "vbBabel", LRS(10025), sInCode)
                        End Select
                    Case "51"
                        sReturnCode = "150"
                    Case "53"
                        sReturnCode = "203"
                    Case "54"
                        sReturnCode = "208"
                    Case "56"
                        sReturnCode = "207"
                    Case Else
                        Err.Raise(10025, "vbBabel", LRS(10025), Left(sInCode, 2))
                End Select

            Case "ShipNet"

                Select Case sInCode
                    Case "1", "101"
                        sReturnCode = "201"
                    Case "2", "102"
                        sReturnCode = "203"
                    Case "3", "103"
                        sReturnCode = "205"
                    Case "4", "104"
                        sReturnCode = "401"
                    Case "5", "105"
                        sReturnCode = "175"
                    Case "6", "106"
                        sReturnCode = "402"
                    Case "7", "107"
                        sReturnCode = "403"
                    Case "8", "108"
                        sReturnCode = "101"
                    Case "9", "109"
                        sReturnCode = "105"
                    Case "10", "110"
                        sReturnCode = "150"
                    Case "11", "111"
                        sReturnCode = "301"
                    Case "12", "112"
                        sReturnCode = "160"
                    Case Else
                        Err.Raise(10025, "vbBabel", LRS(10025), sInCode)
                End Select

            Case "MT940" ' Also used for ReadNordea_Innbet_Ktoinf_utl
                Select Case Left(sInCode, 1)
                    Case "S"
                        sReturnCode = "601" 'Is this salary?
                    Case "N", "F" 'What's the difference National, Foreign?
                        Select Case Right(sInCode, 3)
                            Case "TRF"
                                sReturnCode = "601"
                            Case "MSC"
                                sReturnCode = "601" 'Wrong code?
                            Case "CHK"
                                sReturnCode = "609" 'Can't distinguish between domestic and international payments
                            Case "CLR", "COL", "ECK", "TCK"
                                sReturnCode = "601" 'Could be more precise, but we don't know the meaning of the different codes
                            Case "INT"
                                sReturnCode = "637"
                            Case "CMI"
                                ' For DnBNOR US, Incoming. CMI is used for ACH-transactions
                                sReturnCode = "661" ' ACH (CTX)
                            Case "LOC" '- Local payment, may include other types of payment, must recheck in the calling function
                                sReturnCode = "629"
                            Case "LBX"  ' Lockbox  - added 19.04.2017 (StormGeo)
                                sReturnCode = "660"
                            Case Else
                                sReturnCode = "601"
                        End Select
                    Case Else
                        sReturnCode = "601"

                End Select

            Case "PERIODISKABETALNINGAR"

                Select Case sInCode

                    ' massetrans
                    Case "20" ' Pensjon
                        sReturnCode = "201"
                    Case "19" ' Underh�ll
                        sReturnCode = "202"
                    Case "10" ' L�nn
                        sReturnCode = "203"
                    Case "60", "61", "62", "63", "64", "68" ' Landbruksoppgj�r
                        sReturnCode = "205"
                    Case Else
                        sReturnCode = "200" ' Other mass.
                End Select

                ' XOKNET 16.08.2013
            Case "LEVERANTORSBETALNINGAR"
                Select Case sInCode
                    Case "A"     ' L�nn
                        sReturnCode = "203"
                    Case "B"    ' Skatt/avgift
                        sReturnCode = "105"
                    Case "C"    ' Leverant�rbetalingar
                        sReturnCode = "150"
                    Case "D"    ' Til egen konto
                        sReturnCode = "150"
                    Case "E"    ' Plusgirobet
                        sReturnCode = "150"

                    Case Else
                        sReturnCode = "150" 'overf�ring med melding

                End Select

                ' XNET 25.09.2013 Added UNITELPC
            Case "UNITELPC"

                '-35  Husleje
                '-100 [Fri tekst]
                '-151 Overskydende skat
                '- 161 Kontingent

                ' XokNET 12.12.2013
                '102 Info -overf�rsel

                Select Case sInCode
                    Case "033"     ' Husleie
                        sReturnCode = "035"
                    Case "034"     ' Kontingent
                        sReturnCode = "161"
                    Case "036", "609"    ' Skatt/avgift
                        sReturnCode = "151"
                    Case "150", "602" 'Overf�ring med melding
                        'sReturnCode = "100"
                        sReturnCode = "102" '102 Info -overf�rsel

                        ' XokNET 12.12.2013
                        ' massetrans
                        '099 L�n*
                        '103 Pension*
                        '151 Overskydende skat
                        '156 L�noverf�rsel
                        '157 14 dages l�n



                    Case "157", "607"      ' Pensjon
                        sReturnCode = "103"
                    Case "165", "604"     ' L�nn
                        sReturnCode = "099"
                    Case "166", "605"     ' Hyretrekk
                        sReturnCode = "099"


                    Case Else
                        ' all others;
                        If sType = "M" Or sType = "L" Then
                            ' massetrans
                            sReturnCode = "099" 'l�nn
                        Else
                            ' fakturatrans
                            ' XokNET 12.12.2013
                            'sReturnCode = "100" 'overf�ring med melding
                            sReturnCode = "102" '102 Info -overf�rsel
                        End If

                End Select

            Case "ISO20022"
                If bCredits Then
                    ' Credits - Incoming payments
                    ' ---------------------------
                    If UCase(sInCode) = "PMNT" Then
                        Select Case UCase(sInCode2)
                            Case "ICCN"
                                '27.04.2022 - For Odin, internal transfer
                                Select Case UCase(sInCode3)
                                    Case "COAT"
                                        sReturnCode = "603" ' .. Bank internal tra 603
                                        sType = "D"
                                End Select

                            Case "IDDT"
                                '29.03.2021 - Iflg. mail fra Visma  har Nordea endret dette. 
                                'Direct debit transactions credited on your account (issued collections) The Bank Transaction Code will change from PMNT/RDDT/PMDD to PMNT/IDDT/PMDD
                                'Direct debit transactions debited on your account (received collections) The Bank Transaction Code will change from PMNT/IDDT/PMDD to PMNT/RDDT/PMDD
                                Select Case UCase(sInCode3)
                                    Case "DMCT"
                                        sReturnCode = "515" 'Avtalegiro
                                        sType = "D"
                                    Case "PMDD" '28.11.2019 - Added for SGF_EQ, defined as Direct Debit
                                        sReturnCode = "515" 'Avtalegiro
                                        sType = "D"

                                End Select
                            Case "MCRD"
                                Select Case UCase(sInCode3)
                                    Case "POSP"
                                        sReturnCode = "671"
                                        sType = "D"
                                End Select
                            Case "RCDT"
                                Select Case UCase(sInCode3)
                                    Case "AUTT"
                                        sReturnCode = "601"
                                        sType = "D"
                                    Case "VCOM"
                                        sReturnCode = "510" '?? For Norge ihvertfall?
                                    Case "XBCT"
                                        sReturnCode = "672"
                                        sType = "I"
                                    Case "DMCT"
                                        '24.01.2017 - Added the first IF in the IF .. Else below for ODIN, This are a sort of KID transactions
                                        If eBank = vbBabel.BabelFiles.Bank.Handelsbanken Then
                                            sReturnCode = "510"
                                        ElseIf eCountryName = vbBabel.BabelFiles.CountryName.Norway Then
                                            sReturnCode = "602"
                                            ' 26.09.2019 -
                                            ' 602 er feil. ihvertfall iflg Nordeas MIG.
                                            ' Electronic payments with/without advice - PMNT/RCDT/DMCT
                                            ' endrer derfor til
                                            sReturnCode = "601"
                                        Else
                                            sReturnCode = "675"
                                        End If
                                        sType = "D"
                                    Case "ACDT"
                                        sReturnCode = "676"
                                        sType = "M"
                                    Case "ATXN"
                                        If eCountryName = vbBabel.BabelFiles.CountryName.Sweden Or eCountryName = vbBabel.BabelFiles.CountryName.Denmark Then
                                            ' 04.09.2019 - this is not OCR from DNB Sweden
                                            ' 25.09.2019 - and not "ocr" from DNB Denmark
                                            If IsDnB(eBank) Then
                                                sReturnCode = "677"
                                            Else
                                                sReturnCode = "510"
                                            End If
                                            sType = "D"
                                        Else
                                            sReturnCode = "677"
                                            sType = "M"
                                        End If
                                    Case "ESCT"
                                        ' added by JanP 23.03.2020
                                        'ESCT = SEPA Credit Transfer
                                        sReturnCode = "150"
                                        sType = "D"  ' Vi vet ikke om I eller D

                                End Select
                            Case "NTAV"
                                Select Case UCase(sInCode3)
                                    Case "NTAV"
                                        'NBNBNB - This is not correct. This codes signifies NOT AVAILABLEand should probably then return 601 (Payment with message)
                                        'Gjensidige needed this to be OCR. I know this was necessary for GPF prior to 01.04.2013 because Autogiro transactions then were marked this way.
                                        'This was changed 01.04.2013 and is not necessary anymore for GPF, but I have no idea if it is necessary for other comanies/countries in Gjensidige.
                                        'sReturnCode = "515"
                                        '19.05.2017 - Changed this for SG Finans Blockfactoring, now setting it to "601". Gjensidige will very soon need a new version, check if this creates problems.
                                        sReturnCode = "601"
                                End Select
                            Case "RDDT"
                                '29.03.2021 - Iflg. mail fra Visma  har Nordea endret dette. 
                                'Direct debit transactions credited on your account (issued collections) The Bank Transaction Code will change from PMNT/RDDT/PMDD to PMNT/IDDT/PMDD
                                'Direct debit transactions debited on your account (received collections) The Bank Transaction Code will change from PMNT/IDDT/PMDD to PMNT/RDDT/PMDD
                                Select Case UCase(sInCode3)
                                    Case "PMDD"
                                        sReturnCode = "611"
                                        sType = "D"
                                End Select
                            Case "RCCN"
                                Select Case UCase(sInCode3)
                                    Case "ICCT"
                                        sReturnCode = "673"
                                End Select
                            Case "MCOP"
                                Select Case UCase(sInCode3)
                                    Case "OTHR"
                                        sReturnCode = "674"
                                End Select
                            Case "OTHR"
                                Select Case UCase(sInCode3)
                                    Case "OTHR"
                                        sReturnCode = "600"
                                End Select
                        End Select
                    Else
                        ' special service codes
                        Select Case sInCode
                            Case "CAMT" 'Added 26.06.2017 - To identify charges for SG Finans aginst Danske Bank
                                If IsDanskeBank(eBank) Then
                                    If sInCode2 = "ACCB" Then
                                        If sInCode3 = "ZABA" Then
                                            sReturnCode = "648"
                                        ElseIf sInCode3 = "TOPG" Then
                                            sReturnCode = "683" '12.05.2020 for SG Finans filter - vital for solution if they still uses Danske Bank
                                        Else
                                            sReturnCode = "510"
                                        End If
                                    Else
                                        sReturnCode = "510"
                                    End If
                                Else
                                    sReturnCode = "510"
                                End If
                            Case "DERV"
                                If sInCode2 = "NTAV" Then
                                    If sInCode3 = "NTAV" Then
                                        sReturnCode = "652" '12.05.2020 for SG Finans filter - vital for solution if they still uses Danske Bank
                                        sType = "I"
                                    End If
                                End If
                            Case "FORX" 'Added 25.11.2019 for Nordea eGateway
                                If sInCode2 = "MDOP" Or sInCode2 = "MCOP" Then
                                    If sInCode3 = "NTAV" Then
                                        sReturnCode = "601"
                                        sType = "I"
                                    Else
                                        sReturnCode = "601"
                                        sType = "I"
                                    End If
                                Else
                                    ' 30.09.2021 - i dok. fra Nordea er FORX =Foreign Exchange, = BB paycode 633
                                    If eBank = vbBabel.BabelFiles.Bank.Nordea_NO Then
                                        sReturnCode = "633"
                                    Else
                                        sReturnCode = "601"
                                    End If
                                sType = "I"
                                End If
                            Case "XAP"
                                sReturnCode = "617"
                            Case "XAF"
                                sReturnCode = "618"
                            Case "XAN"
                                sReturnCode = "619"
                            Case "XAU", "XBB", "XDD", "XDP"
                                sReturnCode = "611"
                            Case "XLS"
                                sReturnCode = "601"
                            Case "XBT"
                                sReturnCode = "510"
                            Case "SECU"
                                If sInCode2 = "NTAV" Then
                                    If sInCode3 = "NTAV" Then
                                        sReturnCode = "633" '07.05.2020 for SG Finans filter - vital for solution if they still uses Danske Bank
                                        sType = "D" '??????
                                    Else
                                        sReturnCode = 600
                                        sType = "D" '??????
                                    End If
                                Else
                                    sReturnCode = 600
                                    sType = "D" '??????
                                End If
                            Case Else
                                ' added case else - DNB Hamburg is using GENERAL GENERAL !!!
                                sReturnCode = 600
                                sType = "D" '??????
                        End Select
                    End If
                Else
                    ' Debits - Outgoing payments
                    ' --------------------------
                    If UCase(sInCode) = "PMNT" Then
                        Select Case UCase(sInCode2)
                            Case "MCRD"
                                Select Case UCase(sInCode3)
                                    Case "POSP"
                                        sReturnCode = "413"
                                        sType = "D"
                                End Select
                            Case "ICDT"
                                Select Case UCase(sInCode3)
                                    Case "ATXN"
                                        'What is this? For Gjensidige One World, regular domestic payments are returned with this code.
                                        'Documentation says ACH Transaction
                                        'sReturnCode = "200"

                                        'Changed again, because it created an error for Gjensidige Forsikring Camt.053

                                        'New code
                                        If sMessageType = "Camt.053" Then
                                            sType = "I"
                                        Else
                                            sType = "D"
                                        End If

                                        'Old code (with comments)
                                        'sReturnCode = "150"
                                        ''Changed 10.04.2013 - CRITICAL - Done for One World Camt.054 debet. May influence other Gjensidige cases or other formats (Camt.053 and 054) 
                                        'sType = "D"
                                        ''Old code
                                        ''sType = "I" '? - According to Camt.053 Nordea

                                        ' JP 24.05.2016 - Vi m� jo ha en eller annen returkode, ikke blank-
                                        ' For Ler�y US Inc, fra norsk konto, f�r vi disse kodene for vanlig norsk innlandstran
                                        sReturnCode = "150"


                                    Case "ACDT"
                                        sReturnCode = "250"
                                        sType = "M"
                                    Case "XBCT"
                                        sReturnCode = "150" '"407"
                                        sType = "I" '? - According to Camt.053 Nordea
                                    Case "AUTT"
                                        sReturnCode = "150"
                                    Case "ESCT"
                                        ' added by JanP 08.05.2015
                                        'ESCT = SEPA Credit Transfer
                                        sReturnCode = "150"
                                        sType = "I"
                                    Case "SALA"
                                        ' added by JanP 08.05.2015
                                        'SALA = Salary,
                                        sReturnCode = "203"
                                        sType = "S"
                                    Case "BCHQ"
                                        ' added by JanP 08.05.2015
                                        'BCHQ = cheque
                                        sReturnCode = "608"
                                        sType = "I"
                                    Case "DMCT"
                                        ' added 03.11.2016
                                        ' Domestic Credit Transfer
                                        sReturnCode = "150"
                                        sType = "D"

                                End Select
                            Case "IDDT"
                                Select Case UCase(sInCode3)
                                    Case "URDD"
                                        sReturnCode = "408"
                                End Select
                            Case "ICCN"
                                Select Case UCase(sInCode3)
                                    Case "ICCT"
                                        sReturnCode = "403"
                                End Select
                                'Case "ICDT" 'Camt.053 - Nordea eGateway - International Payments - Sweden/Norway
                                '    Select Case UCase(sInCode3)
                                '        Case "ATXN"
                                '            sReturnCode = "150"
                                '            sType = "I"
                                '        Case "XBCT"
                                '            sReturnCode = "150"
                                '            sType = "I"
                                '    End Select
                            Case "ICHQ" 'Camt.053 - Nordea eGateway - International Payments - Sweden/Norway
                                Select Case UCase(sInCode3)
                                    Case "XBCQ"
                                        sReturnCode = "150"
                                        sType = "I"
                                    Case "BCHQ"
                                        sReturnCode = "150"
                                        sType = "D"
                                End Select
                            Case "OTHR"
                                Select Case UCase(sInCode3)
                                    Case "OTHR"
                                        sReturnCode = "100"
                                End Select
                            Case "RCDT" 'Camt.053 - Nordea eGateway - Domestic Payments - Sweden/Norway
                                Select Case UCase(sInCode3)
                                    Case "AUTT"
                                        sReturnCode = "150"
                                        sType = "D"
                                End Select
                        End Select
                    Else
                        ' special service codes
                        Select Case sInCode
                            Case "FORX" 'Added 25.11.2019 for Nordea eGateway
                                If sInCode2 = "MDOP" Or sInCode2 = "MCOP" Then
                                    If sInCode3 = "NTAV" Then
                                        sReturnCode = "150"
                                        sType = "I"
                                    Else
                                        sReturnCode = "150"
                                        sType = "I"
                                    End If
                                Else
                                    sReturnCode = "150"
                                    sType = "I"
                                End If
                            Case "XAP"
                                sReturnCode = "409"
                            Case "XAF"
                                sReturnCode = "410"
                            Case "XAN"
                                sReturnCode = "411"
                            Case "XAU", "XBB", "XDD", "XDP"
                                sReturnCode = "412"
                            Case "XLS"
                                sReturnCode = "150"
                            Case "XBT"
                                sReturnCode = "301"
                            Case "PDT"  ' Doktolk with TIPS - 
                                sReturnCode = "520" ' type OCR
                            Case "XTND"
                                If eCountryName = vbBabel.BabelFiles.CountryName.Norway Then
                                    sType = "D"
                                    sReturnCode = "150" 'This is not correct but what is it?
                                End If
                            Case Else
                                ' don't know - default to
                                sType = "D"
                                sReturnCode = "150" 'This is not correct but what is it?
                        End Select

                    End If

                End If

                ' 30.04.2020 -
                ' vi m� jo ha en "default" dersom vi ikke treffer p� noen av kodene over
                ' MEN - varsle i utviklingsmilj�et
                If Not RunTime() Then
                    If sType = "" Then
                        'MsgBox("bbSetPayCode: Empty PayType")
                    End If
                    If sReturnCode = "" Then
                        'MsgBox("bbSetPayCode: Empty ReturnCode")
                    End If
                End If
                If EmptyString(sType) Then
                    sType = "D"
                End If
                If EmptyString(sReturnCode) Then
                    If bCredits Then
                        sReturnCode = "601"
                    Else
                        sReturnCode = "150"
                    End If

                End If

            Case Else
                ' Div. No translation:

                Select Case sInCode
                    Case "180" ' UDUS
                        sReturnCode = "180"
                        ' 17.08.2010 added Plusgiro Sweden 190
                    Case "190" ' Plusgiro Sweden
                        sReturnCode = "190"
                End Select
        End Select



        bbSetPayCode = sReturnCode

    End Function
    Function bbGetPayCodeTelepayPlus(ByVal oPayment As vbBabel.Payment, ByVal sTP_PayType As String) As String
        '-----------------------------------------
        ' XOKNET
        ' 18.10.2010 - and forward - major changes !!!!!
        ' 24.04.2014 - major changes
        '-----------------------------------------

        ' Get a TelepayPlus extended paymentcode from BabelBanks generic PayCode
        Dim sReturnCode As String = ""
        'XOKNET 10.11.2010 added next 4 lines
        Dim bIsSEPACountry As Boolean
        Dim bISSEPADebitCountry As Boolean  ' Added 17.02.2014
        Dim bIsEUCountry As Boolean
        Dim sDebetCountryCode As String
        Dim sDebitAccountCurrency As String
        Dim sCreditAccountCurrency As String
        Dim sErrMsg As String
        Dim sTmp As String

        bIsSEPACountry = False
        sDebetCountryCode = Mid$(oPayment.BANK_I_SWIFTCode, 5, 2)
        'FindDomesticCurrency oPayment.BANK_CountryCode, bIsSEPACountry  ' find if SEPA Country
        bIsSEPACountry = IsSEPACountry(oPayment.BANK_CountryCode)
        bIsEUCountry = IsEUCountry(oPayment.BANK_CountryCode)

        ' Kodeforklaring: Se bbSetPayCode
        '-----------------------------------------------------------------------
        ' Extract payers banks countrycode from payers banks swift

        '- M� HUSKE AT PAYTYPE = D n� kan v�re betalinger innen SEPA

        ' 06.02.2014 -
        ' Some tests can be set here, nt dependant of country;
        If (oPayment.BANK_I_Domestic Or oPayment.BANK_CountryCode = oPayment.BANK_AccountCountryCode) And Left$(oPayment.BANK_I_SWIFTCode, 4) <> "DNBA" Then
            ' -51  IBAN/BBAN   Request for Transfer (determined by product id) Domestic  (TKIO in TelepayPlus)
            sReturnCode = "51"

        Else
            ' countryspecific tests

            Select Case Mid$(oPayment.BANK_I_SWIFTCode, 5, 2)
                Case "NO"
                    ' TelepayPlus from Norway uses ordinary paymentcodes (Telepay BETFOR21, pos 264-266
                    ' xoknet 14.12.2011 - added text, and changed paycodes below
                    ' We will also add the TP+ paycodes;
                    ' - 01  NO  Domestic    A
                    ' - 02  NO  Non-Domestic    B
                    ' - 52  NO  Domestic    C   massebetaling



                    ' XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                    ' the 05.04.2019 changes are just for testing so far, and removed 03.05.2019

                    ' XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                    ' 16.05.2019 - MAJOR changes, because DNB Singapore is closing down.
                    '              The accounts are moved to Norway, with countrycode NO
                    If Left(oPayment.DnBNORTBIPayType, 3) = "IPB" And (oPayment.I_CountryCode = "NO" Or oPayment.BANK_AccountCountryCode = "NO") And oPayment.MON_InvoiceCurrency = "AUD" And _
                            (oPayment.MON_InvoiceCurrency = oPayment.MON_AccountCurrency) And oPayment.BANK_CountryCode = "AU" Then
                        ' Australia
                        ' ----------
                        '97      AU  IPBLow 
                        '98      AU  IPBHigh

                        ' We may have marked as IPBLOW, IPBHIGH in setup
                        If oPayment.DnBNORTBIPayType = "IPBLOW" Then
                            '- 97     AU  Domestic        
                            sReturnCode = 97
                        ElseIf oPayment.DnBNORTBIPayType = "IPBHIGH" Then
                            '-98      AU  Domestic        
                            sReturnCode = 98
                        End If
                    ElseIf Left(oPayment.DnBNORTBIPayType, 3) = "IPB" And (oPayment.I_CountryCode = "NO" Or oPayment.BANK_AccountCountryCode = "NO") And oPayment.MON_InvoiceCurrency = "SGD" And _
                            (oPayment.MON_InvoiceCurrency = oPayment.MON_AccountCurrency) And oPayment.BANK_CountryCode = "SG" Then

                        ' Singapore
                        ' ----------
                        '93      SG  IPB Low value
                        '94      SG  IPB High value
                        If oPayment.DnBNORTBIPayType = "IPBLOW" Then
                            sReturnCode = 93
                        ElseIf oPayment.DnBNORTBIPayType = "IPBHIGH" Then
                            sReturnCode = 94
                        End If

                    Else

                        ' the 05.04.2019 changes are just for testing so far, and removed 03.05.2019

                        'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                        ' For NO as before we introduces Australian IPBLow and IPBHigh

                        If oPayment.PayType = "I" Then
                            If oPayment.ToOwnAccount Then  '' added 17.08.2009
                                sReturnCode = "54"   '54  ALL Non-Domestic    F   Intra Company Non-Domestic (determined by product id)
                            Else
                                sReturnCode = "02"
                            End If
                            ' xoknet 14.12.2011
                        ElseIf oPayment.PayType = "M" Or oPayment.PayType = "S" And sTP_PayType = "M" Then
                            sReturnCode = "52"
                        Else
                            sReturnCode = "01"
                        End If
                    End If

                Case "SE"
                    If oPayment.PayType = "D" And sTP_PayType <> "M" Then
                        If oPayment.Priority Then
                            ' XOKNET 27.11.2012 added test for Finansielle betalinger
                            If oPayment.PayCode = "406" Then
                                sReturnCode = "13"  '13  SE Domestic             Financiell payments
                            Else
                                sReturnCode = "09"  '09  SE  Domestic            Urgent payments (only NBB SE)
                            End If
                        Else

                            'Select Case oPayment.PayCode
                            If oPayment.PayCode = "105" Then
                                sReturnCode = "11" '11  SE  Domestic    A   Tax payments to BankGiro
                            ElseIf (oPayment.PayCode = "403" Or oPayment.ToOwnAccount) And Left$(oPayment.BANK_SWIFTCode, 6) = "DNBASE" Then
                                sDebitAccountCurrency = dbSelectAccountCurrency(oPayment.I_Account)
                                sCreditAccountCurrency = dbSelectAccountCurrency(oPayment.E_Account)
                                If Len(sCreditAccountCurrency) > 0 Then
                                    ' Check if debitaccountcurrency, creditaccountcurrency and transfercurrency are the same;
                                    If sDebitAccountCurrency = sCreditAccountCurrency And sDebitAccountCurrency = oPayment.MON_InvoiceCurrency Then
                                        ' currency for debitacc and creditacc and transer are all the same, no exchange
                                        sReturnCode = "10"  '10  SE  Domestic        Intracompany payment within DnBNOR SE (only NBB SE), no exchange
                                    Else
                                        ' with exchange
                                        sReturnCode = "17"  '17  SE  Domestic        Intracompany payment within DnBNOR SE (only NBB SE), with exchange
                                    End If
                                Else
                                    ' errormsg
                                    'Add errorinformation, which helps the user to identify the payment in matter
                                    sErrMsg = LRSCommon(20040) & " " & oPayment.I_Account & vbCrLf   '"From account: " & oPayment.I_Account & vbCrLf
                                    sErrMsg = sErrMsg & LRSCommon(20041) & " " & StringToDate(oPayment.DATE_Payment) & vbCrLf       '"Due date: " & StringToDate(oPayment.DATE_Payment) & vbCrLf
                                    sErrMsg = sErrMsg & LRSCommon(20042) & " " & oPayment.E_Name & vbCrLf                    '"To: " & oPayment.E_Name & vbCrLf
                                    sErrMsg = sErrMsg & LRSCommon(20043) & " " & oPayment.E_Account & vbCrLf                 '"To account: " & oPayment.E_Account & vbCrLf
                                    sErrMsg = sErrMsg & LRSCommon(20044) & " " & Format(oPayment.MON_InvoiceAmount / 100, "##,##0.00")                 '"Amount: " & Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00")
                                    'MsgBox "Betaling til egen konto krever at mottakerkonto er innmeldt i klientoppsettet i BabelBank." & vbCrLf & sErrMsg, vbExclamation, "BabelBank avbrytes"
                                    MsgBox(LRSCommon(20045) & vbCrLf & sErrMsg, vbExclamation, "BabelBank")
                                    '- hva gj�r vi n�r det oppdages at konto ikke er satt opp ? Bare avslutter ?
                                    sReturnCode = "-1"
                                    Err.Raise(20045, , sErrMsg)
                                End If
                                ' XOKNET 29.06.2015
                            ElseIf (oPayment.PayCode = "403" Or oPayment.ToOwnAccount) Then
                                ' Intracompany to other banks
                                sDebitAccountCurrency = dbSelectAccountCurrency(oPayment.I_Account)
                                sCreditAccountCurrency = dbSelectAccountCurrency(oPayment.E_Account)
                                ' All intracompany SE is now 48
                                sReturnCode = "48"  'Intracompany to other banks 48

                            ElseIf oPayment.PayCode = "160" Then
                                sReturnCode = "05"  '05  SE  Domestic    A   Payments without beneficiary account
                                ' xoknet 17.10.2011
                                ' New "urgent" for ISO, SameDayValue
                                ' SameDayValue         176                                                                               ??               5/105
                            ElseIf oPayment.PayCode = "175" Or oPayment.PayCode = "176" Then
                                sReturnCode = "09"  '09  SE  Domestic            Urgent payments (only NBB SE)

                            Else
                                If EmptyString(oPayment.E_Account) Then
                                    sReturnCode = "05"  '05  SE  Domestic    A   Payments without beneficiary account
                                Else
                                    ' with account
                                    If oPayment.Invoices.Count > 0 Then
                                        If oPayment.Invoices.Item(1).Freetexts.Count > 0 Or Len(Trim(oPayment.Invoices.Item(1).Unique_Id)) > 0 Then
                                            'We have no marker for PlusGiro !! 12  SE  Domestic    A   Tax payments to PlusGiro
                                            ' 09.08.2010, test on length of account to decide if to bankaccount, or to bankgiro/plusgiro
                                            If oPayment.PayCode = "190" Or oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_PlusGiro Or Mid$(oPayment.E_Account, Len(oPayment.E_Account) - 1, 1) = "-" Then
                                                sReturnCode = "04"  '04  SE  Domestic    Postgiro
                                            ElseIf Len(oPayment.E_Account) < 9 Or oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro Then
                                                sReturnCode = "03"  '03  SE  Domestic    Bankgiro - (NB! we do not know if bankgiro or plsugiro !!!!)
                                            Else
                                                sReturnCode = "06"  '06  SE  Domestic    A   Payments to account with remittance information
                                            End If
                                        Else
                                            sReturnCode = "07"  '07  SE  Domestic    A   Payments to account without remittance information
                                        End If
                                    Else
                                        sReturnCode = "07"  '07  SE  Domestic    A   Payments to account without remittance information

                                    End If

                                End If
                            End If
                        End If    'If oPayment.Priority Then
                        '13  SE  Domestic    A   Finance Payments
                        'This is actually a sort of treasury payment. At design stage it is unclear if this type will be used in the INPS project. For now this type has been put on hold, but should be implemented as described in this document.

                    ElseIf oPayment.PayType = "S" Then
                        sReturnCode = "08"  '08  SE  Domestic    C   Salary Payments
                    ElseIf oPayment.PayType = "I" Then
                        ' international
                        'bIsEUCountry = IsEUCountry(oPayment.BANK_CountryCode)

                        ' XOKNET 27.11.2012 added test for Finansielle betalinger
                        If oPayment.PayCode = "406" Then
                            'sReturnCode = "59"  '59  -SE International           Financiell payments
                            ' XNET 06.03.2015 - After discussion with H�kon Belt we change code to 13
                            sReturnCode = "13"  '13  -SE International           Financiell payments

                        ElseIf EmptyString(oPayment.E_Account) Then
                            sReturnCode = "14"    '14  SE  Non-Domestic    B   Bank drafts/cheque to beneficiary
                            '15  SE  Non-Domestic    B   Bank drafts/cheque to ordering party (not applicable in SE)
                        Else
                            If oPayment.Priority Then
                                sReturnCode = "16"   '16  SE      Non-Domestic        Urgent cross border(only NBB SE)
                            Else
                                If (oPayment.PayCode = "403" Or oPayment.ToOwnAccount) And Left$(oPayment.BANK_SWIFTCode, 6) = "DNBASE" Then
                                    sDebitAccountCurrency = dbSelectAccountCurrency(oPayment.I_Account)
                                    sCreditAccountCurrency = dbSelectAccountCurrency(oPayment.E_Account)
                                    If Len(sCreditAccountCurrency) > 0 Then
                                        ' Check if debitaccountcurrency, creditaccountcurrency and transfercurrency are the same;
                                        If sDebitAccountCurrency = sCreditAccountCurrency And sDebitAccountCurrency = oPayment.MON_InvoiceCurrency Then
                                            ' currency for debitacc and creditacc and transer are all the same, no exchange
                                            sReturnCode = "10"  '10  SE  Domestic        Intracompany payment within DnBNOR SE (only NBB SE), no exchange
                                        Else
                                            ' with exchange
                                            sReturnCode = "17"  '17  SE  Domestic        Intracompany payment within DnBNOR SE (only NBB SE), with exchange
                                        End If
                                    Else
                                        ' errormsg
                                        'Add errorinformation, which helps the user to identify the payment in matter
                                        sErrMsg = LRSCommon(20040) & " " & oPayment.I_Account & vbCrLf   '"From account: " & oPayment.I_Account & vbCrLf
                                        sErrMsg = sErrMsg & LRSCommon(20041) & " " & StringToDate(oPayment.DATE_Payment) & vbCrLf       '"Due date: " & StringToDate(oPayment.DATE_Payment) & vbCrLf
                                        sErrMsg = sErrMsg & LRSCommon(20042) & " " & oPayment.E_Name & vbCrLf                    '"To: " & oPayment.E_Name & vbCrLf
                                        sErrMsg = sErrMsg & LRSCommon(20043) & " " & oPayment.E_Account & vbCrLf                 '"To account: " & oPayment.E_Account & vbCrLf
                                        sErrMsg = sErrMsg & LRSCommon(20044) & " " & Format(oPayment.MON_InvoiceAmount / 100, "##,##0.00")                 '"Amount: " & Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00")
                                        'MsgBox "Betaling til egen konto krever at mottakerkonto er innmeldt i klientoppsettet i BabelBank." & vbCrLf & sErrMsg, vbExclamation, "BabelBank avbrytes"
                                        MsgBox(LRSCommon(20045) & vbCrLf & sErrMsg, vbExclamation, "BabelBank")
                                        '- hva gj�r vi n�r det oppdages at konto ikke er satt opp ? Bare avslutter ?
                                        sReturnCode = "-1"
                                        Err.Raise(20045, , sErrMsg)
                                    End If
                                ElseIf (oPayment.PayCode = "403" Or oPayment.ToOwnAccount) Then
                                    ' XOKNET 29.06.2015
                                    'sReturnCode = "17"  '17     Intracompany payment other countries
                                    ' All intracompany other banks SE is now 48
                                    sReturnCode = "48"  'Intracompany to other banks 48


                                Else
                                    ' 23.12.2013 added IsEUCountry and SEK
                                    If IsIBANNumber(oPayment.E_Account, True, False) And Not EmptyString(oPayment.BANK_SWIFTCode) And _
                                    (oPayment.MON_TransferCurrency = "EUR" Or oPayment.MON_TransferCurrency = "SEK") And bIsEUCountry Then
                                        sReturnCode = "19"  '19  SE  Non-Domestic    B   EU Payments
                                    End If
                                End If
                            End If
                        End If
                        If EmptyString(sReturnCode) Then
                            ' not hit any of the tests above, give it 20
                            sReturnCode = "20"    '20  SE  Non-Domestic    B   Normal Payments
                        End If

                    Else
                        ' Mass? no other?
                        sReturnCode = "07"   '07  SE  Domestic    A   Payments to account without remittance information
                    End If

                Case "DK"
                    ' XNET 03.04.2014 TAKE WHOLE "DK" SECTION
                    '----------------------------------------
                    If oPayment.PayType = "D" And sTP_PayType <> "M" Then
                        If oPayment.Priority And oPayment.MON_InvoiceAmount > 500000000 And Not EmptyString(oPayment.BANK_SWIFTCode) Then
                            ' Urgent domestic payment, > 5.000.000,00 and with BIC
                            sReturnCode = "35"
                        ElseIf oPayment.PayCode = "301" Then  'KID/FIK
                            ' must control what kind of Giro/FIK by checking oInvoice.UniqueID, 2 first chars
                            If oPayment.Invoices.Count > 0 Then
                                Select Case Left$(oPayment.Invoices.Item(1).Unique_Id, 2)
                                    Case "71"
                                        sReturnCode = "21" '21  DK  Domestic    A   FI kort 71
                                    Case "73"
                                        sReturnCode = "22" '22  DK  Domestic    A   FI kort 73
                                    Case "75"
                                        sReturnCode = "23" '23  DK  Domestic    A   FI kort 75
                                    Case "01"
                                        sReturnCode = "24" '24  DK  Domestic    A   Girokort 01
                                    Case "04"
                                        sReturnCode = "25" '25  DK  Domestic    A   Girokort 04
                                    Case "15"
                                        sReturnCode = "26" '26  DK  Domestic    A   Girokort 15
                                    Case Else
                                        ' should not happen, but give it 71
                                        sReturnCode = "21"
                                End Select

                            Else
                                ' no invoice - should not happen, but give it 71
                                sReturnCode = "21"
                            End If
                        ElseIf Left$(oPayment.BANK_SWIFTCode, 6) = "DNBADK" And (oPayment.PayCode = "403" Or oPayment.ToOwnAccount) Then
                            sDebitAccountCurrency = dbSelectAccountCurrency(oPayment.I_Account)
                            sCreditAccountCurrency = dbSelectAccountCurrency(oPayment.E_Account)
                            If Len(sCreditAccountCurrency) > 0 Then
                                ' Check if debitaccountcurrency, creditaccountcurrency and transfercurrency are the same;
                                If sDebitAccountCurrency = sCreditAccountCurrency And sDebitAccountCurrency = oPayment.MON_InvoiceCurrency Then
                                    ' currency for debitacc and creditacc and transer are all the same, no exchange
                                    sReturnCode = "52"
                                Else
                                    ' with exchange
                                    sReturnCode = "53"
                                End If
                            Else
                                ' errormsg
                                'Add errorinformation, which helps the user to identify the payment in matter
                                sErrMsg = LRSCommon(20040) & " " & oPayment.I_Account & vbCrLf   '"From account: " & oPayment.I_Account & vbCrLf
                                sErrMsg = sErrMsg & LRSCommon(20041) & " " & StringToDate(oPayment.DATE_Payment) & vbCrLf       '"Due date: " & StringToDate(oPayment.DATE_Payment) & vbCrLf
                                sErrMsg = sErrMsg & LRSCommon(20042) & " " & oPayment.E_Name & vbCrLf                    '"To: " & oPayment.E_Name & vbCrLf
                                sErrMsg = sErrMsg & LRSCommon(20043) & " " & oPayment.E_Account & vbCrLf                 '"To account: " & oPayment.E_Account & vbCrLf
                                sErrMsg = sErrMsg & LRSCommon(20044) & " " & Format(oPayment.MON_InvoiceAmount / 100, "##,##0.00")                 '"Amount: " & Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00")
                                'MsgBox "Betaling til egen konto krever at mottakerkonto er innmeldt i klientoppsettet i BabelBank." & vbCrLf & sErrMsg, vbExclamation, "BabelBank avbrytes"
                                MsgBox(LRSCommon(20045) & vbCrLf & sErrMsg, vbExclamation, "BabelBank")
                                '- hva gj�r vi n�r det oppdages at konto ikke er satt opp ? Bare avslutter ?
                                sReturnCode = "-1"
                                Err.Raise(20045, , sErrMsg)
                            End If

                        ElseIf oPayment.PayCode = "403" Or oPayment.ToOwnAccount Then
                            ' XOKNET 29.06.2015
                            ' Intracompany other banks
                            'sReturnCode = "34"
                            sReturnCode = "48"  ' Intracompany other banks = 48
                        Else

                            Select Case oPayment.PayCode

                                Case "150"   ' 25.08.2015 changed from Case "404". "Normal" DK payments are msg on statement
                                    sReturnCode = "27"  '27  DK  Domestic    A   Bank transfer Long UDUS (melding p� kontoutdrag ???)
                                Case "151"   ' 25.08.2015 changed from Case "150"
                                    sReturnCode = "28"  '28  DK  Domestic    A   Bank transfer Extended UDUS (melding pr brev)
                                Case "160"
                                    sReturnCode = "30"  '30  DK  Domestic    A   Payments without beneficiary account (cheque)
                                Case Else
                                    If EmptyString(oPayment.E_Account) Then
                                        sReturnCode = "30"  '30  DK  Domestic    A   Payments without beneficiary account (cheque)
                                    Else
                                        sReturnCode = "28"  '28  DK  Domestic    A   Bank transfer Long UDUS (melding p� kontoutdrag ???)
                                    End If

                            End Select
                        End If

                    ElseIf oPayment.PayType = "S" Then
                        sReturnCode = "29"  '29  DK  Domestic    C   Salary Payments
                    ElseIf oPayment.PayType = "M" Then
                        sReturnCode = "29"  '29  DK  Domestic    C   Salary Payments

                    ElseIf oPayment.PayType = "I" Then
                        If oPayment.PayCode = "406" Then
                            ' Financial payment, International
                            sReturnCode = "33"  '33  DK  Int    Financial payment
                        ElseIf EmptyString(oPayment.E_Account) Then
                            sReturnCode = "31"  '31  DK  Non-Domestic    B   Bank drafts/cheque to beneficiary
                            '32  DK  Non-Domestic    B   Bank drafts/cheque to ordering party (not applicable in DK)
                        ElseIf oPayment.Priority Then
                            sReturnCode = "50"  '50  DK  Non-Domestic    Urgent
                        ElseIf bIsSEPACountry And IsIBANNumber(oPayment.E_Account, True, False) And oPayment.MON_TransferCurrency = "EUR" Then
                            ' XNET 25.08.2014
                            '                If oPayment.Structured Or IsPaymentStructured(oPayment) Then
                            '                    '- 60      Domestic/Non Domestic       SEPA Structured/reference
                            '                    sReturnCode = "60"
                            '                Else
                            '                    '- 61      Domestic/Non Domestic       SEPA Non Structured
                            '                    sReturnCode = "61"
                            '                End If
                            sReturnCode = "36"
                        ElseIf bIsEUCountry And IsIBANNumber(oPayment.E_Account, True, False) And (oPayment.MON_TransferCurrency = "EUR" Or oPayment.MON_TransferCurrency = "SEK") Then
                            '- 36      Domestic/Non Domestic       EU Payment
                            sReturnCode = "36"

                        Else
                            sReturnCode = "50"  '50  DK  Non-Domestic    B   Normal Payments
                        End If
                    Else
                        ' should not happen, but give it normal, 50
                        sReturnCode = "50"  '50  DK  Non-Domestic    B   Normal Payments
                    End If

                Case "FI"
                    ' Finland
                    '- 44 FI  Non-Domestic    B   Bank drafts/cheque to beneficiary
                    '- 50 FI  Non-Domestic        Cross border urgent
                    '- 48  FI  Non-Domestic        Intra company other banks
                    '- 49  FI  Non-Domestic    B   EU payment
                    '- 50  FI  Non-Domestic    B   Normal payments
                    '- 51  -   -   D   Request for Transfer (determined by product id)

                    '- 52  -   Domestic        Intra Company Domestic without exchange
                    '- 53  -   Domestic    E   Intra Company Domestic with exchange
                    '- 54  -   Non-Domestic    F   Intra Company Non-Domestic (DnB NOR)
                    '- 59  -   (all)               Financiell payments
                    '- 60      Domestic/Non Domestic       SEPA Structured/reference
                    '- 61      Domestic/Non Domestic       SEPA Non Structured
                    '- 62      Domestic/Non Domestic       SEPA Salary
                    '- 63  FI  Domestic        Domestic Urgent (POPS)
                    '- 65  Fi  Domestic        SEPA Structured/referencewithin DNBAFI
                    '- 66  Fi  Domestic        SEPA Non Structured within DNBAFI
                    ' xoknet 27.06.2011 added 67
                    ' 67 is no longer in use (22.10.2013)
                    ' -67       Domestic/Non Domestic       SEPA Salary, DnBNOR

                    ' XOKNET 27.11.2012 added test for Finansielle betalinger
                    If oPayment.PayCode = "406" Then
                        sReturnCode = "59"  '- 59  -   (all)               Financiell payments

                    ElseIf oPayment.PayType = "D" And sTP_PayType <> "M" Then
                        If Left$(oPayment.BANK_SWIFTCode, 6) = "DNBAFI" And (oPayment.PayCode = "403" Or oPayment.ToOwnAccount) Then
                            ' XOKNET 17.01.2012 - changed for IntraCompany
                            ' for Domestic, always 52
                            ' XOKNET 17.10.2013 - Do a control to check if the account (ownaccount, receivers acccount) is set registered in BabelBanks accountssetup
                            sDebitAccountCurrency = dbSelectAccountCurrency(oPayment.I_Account)
                            sCreditAccountCurrency = dbSelectAccountCurrency(oPayment.E_Account)
                            If Len(sCreditAccountCurrency) > 0 Then
                                ' Check if debitaccountcurrency, creditaccountcurrency and transfercurrency are the same;
                                If sDebitAccountCurrency = sCreditAccountCurrency And sDebitAccountCurrency = oPayment.MON_InvoiceCurrency Then
                                    ' currency for debitacc and creditacc and transer are all the same, no exchange
                                    sReturnCode = "52"
                                Else
                                    ' with exchange
                                    sReturnCode = "53"
                                End If
                            Else
                                ' errormsg
                                'Add errorinformation, which helps the user to identify the payment in matter
                                sErrMsg = LRSCommon(20040) & " " & oPayment.I_Account & vbCrLf   '"From account: " & oPayment.I_Account & vbCrLf
                                sErrMsg = sErrMsg & LRSCommon(20041) & " " & StringToDate(oPayment.DATE_Payment) & vbCrLf       '"Due date: " & StringToDate(oPayment.DATE_Payment) & vbCrLf
                                sErrMsg = sErrMsg & LRSCommon(20042) & " " & oPayment.E_Name & vbCrLf                    '"To: " & oPayment.E_Name & vbCrLf
                                sErrMsg = sErrMsg & LRSCommon(20043) & " " & oPayment.E_Account & vbCrLf                 '"To account: " & oPayment.E_Account & vbCrLf
                                sErrMsg = sErrMsg & LRSCommon(20044) & " " & Format(oPayment.MON_InvoiceAmount / 100, "##,##0.00")                 '"Amount: " & Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00")
                                'MsgBox "Betaling til egen konto krever at mottakerkonto er innmeldt i klientoppsettet i BabelBank." & vbCrLf & sErrMsg, vbExclamation, "BabelBank avbrytes"
                                MsgBox(LRSCommon(20045) & vbCrLf & sErrMsg, vbExclamation, "BabelBank")
                                '- hva gj�r vi n�r det oppdages at konto ikke er satt opp ? Bare avslutter ?
                                sReturnCode = "-1"
                                Err.Raise(20045, , sErrMsg)
                            End If

                            ' 17.10.2013 removed code 51 after agreement with DNB

                            ' test if SEPA-payment:
                            ' 24.02.2014 added test for Len(oPayment.BANK_SWIFTCode) = 0
                        ElseIf oPayment.MON_AccountCurrency = "EUR" And oPayment.MON_InvoiceCurrency = "EUR" And _
                            IsIBANNumber(oPayment.E_Account, True, False) And (Len(oPayment.BANK_SWIFTCode) = 8 Or _
                            Len(oPayment.BANK_SWIFTCode) = 11 Or Len(oPayment.BANK_SWIFTCode) = 0) And oPayment.MON_ChargeMeDomestic And _
                            Not oPayment.MON_ChargeMeAbroad Then
                            ' SEPA
                            If oPayment.Priority Then
                                '- 63  FI  Domestic        Domestic Urgent (POPS)
                                sReturnCode = "63"
                            Else
                                If oPayment.PayCode = "301" Or IsPaymentStructured(oPayment) Then  'Reference
                                    If Left$(oPayment.BANK_SWIFTCode, 6) = "DNBAFI" Then
                                        '- 65  Fi  Domestic        SEPA Structured/referencewithin DNBAFI
                                        sReturnCode = "65"
                                    Else
                                        '- 60      Domestic/Non Domestic       SEPA Structured/reference
                                        sReturnCode = "60"
                                    End If
                                Else
                                    If Left$(oPayment.BANK_SWIFTCode, 6) = "DNBAFI" Then
                                        '- 66  Fi  Domestic        SEPA Non Structured within DNBAFI
                                        sReturnCode = "66"
                                    Else
                                        '- 61      Domestic/Non Domestic       SEPA Non Structured
                                        sReturnCode = "61"
                                    End If
                                End If
                            End If

                        Else
                            ' No Default (?), but set 61 if not passed tests above
                            '- 61      Domestic/Non Domestic       SEPA Non Structured
                            sReturnCode = "61"
                        End If

                        ' 24.02.2014 added test for Len(oPayment.BANK_SWIFTCode) = 0
                    ElseIf oPayment.PayType = "S" Or sTP_PayType = "M" Then
                        If oPayment.MON_AccountCurrency = "EUR" And oPayment.MON_InvoiceCurrency = "EUR" And _
                            IsIBANNumber(oPayment.E_Account, True, False) And (Len(oPayment.BANK_SWIFTCode) = 8 Or _
                            Len(oPayment.BANK_SWIFTCode) = 11 Or Len(oPayment.BANK_SWIFTCode) = 0) And oPayment.MON_ChargeMeDomestic And _
                            Not oPayment.MON_ChargeMeAbroad And bIsSEPACountry Then ' XOKNET 17.12.2010 added Not at start of line

                            ' 17.10.2013 Only one code, 62, for SEPA Salary
                            '' xoknet 27.06.2011 - added Salary DnBNOR 67
                            'If Left$(oPayment.BANK_SWIFTCode, 6) = "DNBAFI" Then
                            '    sReturnCode = "67"   22.10.2013 Kode 67 er d�d !
                            'Else
                            '- 62      Domestic/Non Domestic       SEPA Salary
                            sReturnCode = "62"
                            'End If
                        Else
                            '- 50  FI  Non-Domestic    B   Normal payments
                            sReturnCode = "50"
                        End If

                    ElseIf oPayment.PayType = "I" Then

                        'XOKNET 20.12.2010, several lines - added code 60/61, which can also be used International
                        If oPayment.MON_AccountCurrency = "EUR" And oPayment.MON_InvoiceCurrency = "EUR" And _
                            IsIBANNumber(oPayment.E_Account, True, False) And (Len(oPayment.BANK_SWIFTCode) = 8 Or _
                            Len(oPayment.BANK_SWIFTCode) = 11) And oPayment.MON_ChargeMeDomestic And _
                            Not oPayment.MON_ChargeMeAbroad Then  ' XOKNET 17.12.2010 added Not at start of line
                            ' SEPA
                            If bIsSEPACountry Then
                                If oPayment.Structured Or IsPaymentStructured(oPayment) Then
                                    '- 60      Domestic/Non Domestic       SEPA Structured/reference
                                    sReturnCode = "60"
                                Else
                                    '- 61      Domestic/Non Domestic       SEPA Non Structured
                                    sReturnCode = "61"
                                End If
                            Else
                                sReturnCode = "common"
                            End If
                        Else
                            ' bring all others to common routine
                            sReturnCode = "common"
                        End If
                    Else
                        ' bring all others to common routine
                        sReturnCode = "common"
                    End If

                Case "GB"
                    ' United Kingdom
                    ' -69          GB  Domestic        Credit DNBAGB different customer
                    ' -70          GB  Domestic        Bacs payment (ACH Payment)
                    ' -71          GB  Domestic        Same day transfer (CHAPS pmt) STP
                    ' -72          GB  Domestic        Salary
                    ' -73          GB  Domestic        High Value (CHAPS pmt)>200.000
                    ' xoknet 16.06.2011 Added comment for 79
                    ' -79          GB  Domestic        High Value (CHAPS pmt) with exchange
                    ' Common
                    ' -51  IBAN/BBAN   Request for Transfer (determined by product id)
                    ' -52  All <> SE   Domestic    E   Intra Company Domestic DNBA (determined by product id)(NBB Without exchange)
                    ' -53  All Domestic    E   Intra Company Domestic DNBA  (determined by product id) (NBB) With Exchange)
                    ' -55  IBAN        GB  Non-Domestic    B   EU Payments STP

                    If oPayment.PayType = "D" And sTP_PayType <> "M" Then
                        sDebitAccountCurrency = dbSelectAccountCurrency(oPayment.I_Account)
                        If Not oPayment.BANK_I_Domestic And Left$(oPayment.BANK_I_SWIFTCode, 4) <> "DNBA" Then
                            ' -51  IBAN/BBAN   Request for Transfer (determined by product id)  (TKIO in TelepayPlus)
                            sReturnCode = "51"
                        ElseIf oPayment.ToOwnAccount And Left$(oPayment.BANK_SWIFTCode, 6) = "DNBAGB" Then
                            ' 23.12.2013 Changes for code 52 and 53
                            sCreditAccountCurrency = dbSelectAccountCurrency(oPayment.E_Account)
                            If Len(sCreditAccountCurrency) > 0 Then
                                ' Check if debitaccountcurrency, creditaccountcurrency and transfercurrency are the same;
                                If sDebitAccountCurrency = sCreditAccountCurrency And sDebitAccountCurrency = oPayment.MON_InvoiceCurrency Then
                                    ' currency for debitacc and creditacc and transer are all the same, no exchange
                                    sReturnCode = "52"
                                Else
                                    ' with exchange
                                    sReturnCode = "53"
                                End If
                            Else
                                ' errormsg
                                'Add errorinformation, which helps the user to identify the payment in matter
                                sErrMsg = LRSCommon(20040) & " " & oPayment.I_Account & vbCrLf   '"From account: " & oPayment.I_Account & vbCrLf
                                sErrMsg = sErrMsg & LRSCommon(20041) & " " & StringToDate(oPayment.DATE_Payment) & vbCrLf       '"Due date: " & StringToDate(oPayment.DATE_Payment) & vbCrLf
                                sErrMsg = sErrMsg & LRSCommon(20042) & " " & oPayment.E_Name & vbCrLf                    '"To: " & oPayment.E_Name & vbCrLf
                                sErrMsg = sErrMsg & LRSCommon(20043) & " " & oPayment.E_Account & vbCrLf                 '"To account: " & oPayment.E_Account & vbCrLf
                                sErrMsg = sErrMsg & LRSCommon(20044) & " " & Format(oPayment.MON_InvoiceAmount / 100, "##,##0.00")                 '"Amount: " & Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00")
                                'MsgBox "Betaling til egen konto krever at mottakerkonto er innmeldt i klientoppsettet i BabelBank." & vbCrLf & sErrMsg, vbExclamation, "BabelBank avbrytes"
                                MsgBox(LRSCommon(20045) & vbCrLf & sErrMsg, vbExclamation, "BabelBank")
                                '- hva gj�r vi n�r det oppdages at konto ikke er satt opp ? Bare avslutter ?
                                sReturnCode = "-1"
                                Err.Raise(20045, , sErrMsg)
                            End If
                        ElseIf Left$(oPayment.BANK_SWIFTCode, 6) = "DNBAGB" Or (oPayment.BANK_BranchNo = "405114" And oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode) Then
                            ' -69          GB  Domestic        Credit DNBAGB different customer
                            ' Sortcode DnBNOR London: 405114
                            sReturnCode = "69"
                            ' xoknet 16.06.2011 -
                            '   Added next test for Chaps and debitaccountcurrency other than GBP, and GBP payment
                            'XokNET - changed 03.03.2014
                            'ElseIf oPayment.Priority And oPayment.MON_InvoiceCurrency <> "GBP" And sDebitAccountCurrency = "GBP" Then
                            ' 24.03.2021 - changed test below
                            'ElseIf oPayment.MON_InvoiceCurrency = "GBP" And (oPayment.Priority Or sDebitAccountCurrency <> "GBP") Then
                        ElseIf oPayment.Priority And oPayment.MON_InvoiceCurrency = "GBP" And sDebitAccountCurrency <> "GBP" Then
                            ' -79          GB  Domestic        Chaps, debitaccountcurrency other than GBP
                            sReturnCode = "79"
                            ' xoknet 16.06.2011 Added currency GBP
                        ElseIf oPayment.MON_InvoiceAmount > 20000000 And oPayment.MON_InvoiceCurrency = "GBP" Then
                            ' -73          GB  Domestic        High Value (CHAPS pmt)>200.000
                            sReturnCode = "73"
                            ' xoknet 16.06.2011 Added currency GBP
                        ElseIf oPayment.Priority And oPayment.MON_InvoiceCurrency = "GBP" Then
                            ' -71          GB  Domestic        Same day transfer (CHAPS pmt) STP
                            sReturnCode = "71"
                        ElseIf oPayment.DnBNORTBIPayType = "ACH" Then
                            ' -70          GB  Domestic        Bacs payment (ACH Payment)
                            sReturnCode = "70"
                            'Else
                            ' xoknet 16.06.2011 Added currency GBP
                        ElseIf oPayment.MON_InvoiceCurrency = "GBP" Then
                            ' otherwise, BACS
                            ' -70          GB  Domestic        Bacs payment (ACH Payment)
                            ' XokNET 30.03.2011 - added test for default to 70 or 71
                            If oPayment.DnBNORTBIPayType = "ACH" Or _
                                (oPayment.PayType = "M" Or oPayment.PayType = "S") Then
                                sReturnCode = "70"
                            Else
                                sReturnCode = "71"
                            End If
                            ' xoknet 16.06.2011 added an Else to cope with "leftovers" (should be none!)
                        Else
                            sReturnCode = "common"
                        End If

                    ElseIf oPayment.PayType = "S" And sTP_PayType = "M" Then
                        ' -72          GB  Domestic        Salary
                        sReturnCode = "72"

                    ElseIf oPayment.PayType = "M" And sTP_PayType = "M" Then
                        '  -70          GB  Domestic        Bacs payment (ACH Payment)
                        sReturnCode = "70"

                    ElseIf sTP_PayType = "M" And oPayment.MON_InvoiceCurrency = "GBP" Then
                        ' mass (BETFOR21/22), but not discovered by paymenttype, should be BACS
                        sReturnCode = "70"

                        'ElseIf oPayment.PayType = "I" Then
                    Else
                        If oPayment.PayType = "I" Then
                            bIsEUCountry = IsEUCountry(oPayment.BANK_CountryCode)
                            If IsIBANNumber(oPayment.E_Account, True, False) And (Len(oPayment.BANK_SWIFTCode) = 8 Or Len(oPayment.BANK_SWIFTCode) = 11) And _
                            bIsEUCountry And oPayment.MON_InvoiceCurrency = "EUR" And oPayment.MON_ChargeMeDomestic = True And Not oPayment.MON_ChargeMeAbroad Then
                                ' -55  IBAN        GB  Non-Domestic    B   EU Payments STP
                                sReturnCode = "55"
                            Else
                                ' treat rest as common for all countries, at the bottom of this function
                                sReturnCode = "common"
                            End If
                        Else
                            ' treat rest as common for all countries, at the bottom of this function
                            sReturnCode = "common"
                        End If
                    End If

                Case "DE"
                    ' Germany
                    ' - 52  -   Domestic        Intra Company Domestic without exchange
                    ' - 53  -   Domestic    E   Intra Company Domestic with exchange
                    ' - 61      All             SEPA Non Structured/message

                    ' XNET 29.04.2014 Next lines lifted outside If/Endif
                    sDebitAccountCurrency = dbSelectAccountCurrency(oPayment.I_Account)

                    ' New 22.10.2013
                    If Left$(oPayment.BANK_SWIFTCode, 6) = "DNBADE" And (oPayment.PayCode = "403" Or oPayment.ToOwnAccount) Then

                        sCreditAccountCurrency = dbSelectAccountCurrency(oPayment.E_Account)

                        ' for Domestic, always 52
                        If Len(sCreditAccountCurrency) > 0 Then
                            ' Check if debitaccountcurrency, creditaccountcurrency and transfercurrency are the same;
                            If sDebitAccountCurrency = sCreditAccountCurrency And sDebitAccountCurrency = oPayment.MON_InvoiceCurrency Then
                                ' currency for debitacc and creditacc and transer are all the same, no exchange
                                sReturnCode = "52"
                            Else
                                ' with exchange
                                sReturnCode = "53"
                            End If
                        Else
                            ' errormsg
                            'Add errorinformation, which helps the user to identify the payment in matter
                            sErrMsg = LRSCommon(20040) & " " & oPayment.I_Account & vbCrLf   '"From account: " & oPayment.I_Account & vbCrLf
                            sErrMsg = sErrMsg & LRSCommon(20041) & " " & StringToDate(oPayment.DATE_Payment) & vbCrLf       '"Due date: " & StringToDate(oPayment.DATE_Payment) & vbCrLf
                            sErrMsg = sErrMsg & LRSCommon(20042) & " " & oPayment.E_Name & vbCrLf                    '"To: " & oPayment.E_Name & vbCrLf
                            sErrMsg = sErrMsg & LRSCommon(20043) & " " & oPayment.E_Account & vbCrLf                 '"To account: " & oPayment.E_Account & vbCrLf
                            sErrMsg = sErrMsg & LRSCommon(20044) & " " & Format(oPayment.MON_InvoiceAmount / 100, "##,##0.00")                 '"Amount: " & Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00")
                            'MsgBox "Betaling til egen konto krever at mottakerkonto er innmeldt i klientoppsettet i BabelBank." & vbCrLf & sErrMsg, vbExclamation, "BabelBank avbrytes"
                            MsgBox(LRSCommon(20045) & vbCrLf & sErrMsg, vbExclamation, "BabelBank")
                            '- hva gj�r vi n�r det oppdages at konto ikke er satt opp ? Bare avslutter ?
                            sReturnCode = "-1"
                            Err.Raise(20045, , sErrMsg)
                        End If

                        ' XOKNET 12.08.2013 - added new test from Kjell Christian at DNB, based on new set of rules from 01.02.2014
                        ' 3.    SEPA reg code 60 and 61 requires IBAN and BIC. Exception:  IBAN (only) shall be required if the IBAN is located in Germany. The BIC will be added in 811.
                    ElseIf IsIBANNumber(oPayment.E_Account, True, False) And oPayment.BANK_CountryCode = "DE" Then
                        If Not oPayment.Priority Then
                            If oPayment.Structured Or IsPaymentStructured(oPayment) Then
                                sReturnCode = "60"
                            Else
                                sReturnCode = "61"
                            End If
                        Else
                            ' XokNET 12.08.2013 - added new test from Kjell Christian at DNB, based on new set of rules from 01.02.2014
                            '4.    Urgent payment can also be accepted with IBAN with Country code DE. Then the reg code 68 shall be used!
                            sReturnCode = "68"
                        End If

                    ElseIf IsIBANNumber(oPayment.E_Account, True, False) And (Len(oPayment.BANK_SWIFTCode) = 8 Or Len(oPayment.BANK_SWIFTCode) = 11) And _
                    bIsSEPACountry And oPayment.MON_InvoiceCurrency = "EUR" And oPayment.MON_ChargeMeDomestic = True And _
                    Not oPayment.MON_ChargeMeAbroad Then '
                        ' XOKNET 24.01.2011 - removed test for < 5000000
                        'And oPayment.MON_InvoiceAmount < 5000000 Then  '' XOKNET 17.12.2010 added Not at start of line
                        ' - 61      All             SEPA Non Structured/message
                        ' XNET 29.04.2014 Prepare for structured as well
                        If oPayment.Structured Or IsPaymentStructured(oPayment) Then
                            sReturnCode = "60"
                        Else
                            sReturnCode = "61"
                        End If


                        ' XNET 29.04.2014
                        ' The DNB-problems with code 60/61 is only for "International" (oPayment.Paytype="I")
                        ' XNET - No more need for this problemsolution
                        '            If oPayment.PayType = "I" Then
                        '                ' XokNET 28.01.2014 - problems at DNB with code 60/61
                        '                ' For version 1.88.41
                        '                sReturnCode = "50"
                        '            End If


                    Else
                        If Not oPayment.BANK_I_Domestic Then
                            ' -51, 52, 53
                            sReturnCode = "common"

                        ElseIf oPayment.PayType = "D" Then
                            sReturnCode = "68"          'DE  Domestic        DTA payment"

                        ElseIf oPayment.PayType = "S" Then
                            sReturnCode = "68"  '68          DE  Domestic        DTA payment

                        ElseIf oPayment.PayType = "M" Then
                            sReturnCode = "68"  '68          DE  Domestic        DTA payment

                        ElseIf oPayment.PayType = "I" Then
                            sReturnCode = "common"
                        End If
                    End If

                    'xoknet 12.09.2011 Lagt til Case "US"
                Case "US"
                    ' XOKNET 27.11.2012 added test for Finansielle betalinger
                    If oPayment.PayCode = "406" Then
                        sReturnCode = "59"  '- 59  -   (all)               Financiell payments

                    ElseIf oPayment.PayType = "D" Then

                        ' Can be either US domestic or CA domestic !
                        If oPayment.I_CountryCode = "US" And oPayment.MON_InvoiceCurrency = "USD" Then
                            ' USA
                            '----
                            ' USA - except Nacha, which is handled in specific files, not TP+
                            '77      US  Domestic        Wire
                            '78      US  Domestic        Wire DNBAUS/DNBAUS
                            '30      US  Domestic        Check sent beneficiary
                            '44      US  Non-Domestic        Check sent beneficiary

                            ' XOKNET 16.10.2012 added "A CHECK" and "ACHECK"
                            If oPayment.E_Account = "" Or _
                            oPayment.E_Account = "A CHEQUE" Or _
                            oPayment.E_Account = "CHECK" Or _
                            oPayment.E_Account = "A CHECK" Or _
                            oPayment.E_Account = "ACHECK" _
                            Then
                                If oPayment.E_CountryCode = "US" Or oPayment.E_CountryCode = "CA" Or oPayment.E_CountryCode = "" Then
                                    ' checks to US or CA have code 30 (also default blank countrycode to US or CA
                                    ' -30      US  Domestic        Check sent beneficiary
                                    sReturnCode = "30"
                                Else
                                    ' international check probably not possible here, as the payment is marked "D"
                                    sReturnCode = "44"
                                End If
                                ' Next ElseIf for Intracompany domestics added 17.02.2014
                            ElseIf (oPayment.ToOwnAccount Or oPayment.PayCode = "403") And Left$(oPayment.BANK_SWIFTCode, 4) = "DNBA" Then
                                If Left$(oPayment.BANK_SWIFTCode, 8) = Left$(oPayment.BANK_I_SWIFTCode, 8) Then
                                    ' Inside same DNB subcidiary ! And when International, we have in TreatTBI established that
                                    ' this is a payment with exchange

                                    ' added next lines and test 17.02.2014
                                    sDebitAccountCurrency = dbSelectAccountCurrency(oPayment.I_Account)
                                    sCreditAccountCurrency = dbSelectAccountCurrency(oPayment.E_Account)
                                    If Len(sCreditAccountCurrency) > 0 And sDebitAccountCurrency = sCreditAccountCurrency And sDebitAccountCurrency = oPayment.MON_InvoiceCurrency Then
                                        ' currency for debitacc and creditacc and transer are all the same, no exchange
                                        sReturnCode = "52"
                                    Else
                                        ' -53  All Domestic    E   Intra Company Domestic DNBA  (determined by product id) (NBB) With Exchange)
                                        sReturnCode = "53"
                                    End If
                                Else
                                    ' Not inside DNB-bank, or other than DNB
                                    ' -54  Non Domestic    E   Intra Company DNBA Cross Border  (determined by product id) (NBB) With Exchange)
                                    sTmp = dbSelectAccountSWIFT(oPayment.E_Account)
                                    ' we need to know that the account exist!
                                    If Len(sTmp) > 0 Then
                                        sReturnCode = "54"
                                    Else
                                        ' errormsg
                                        'Add errorinformation, which helps the user to identify the payment in matter
                                        sErrMsg = LRSCommon(20040) & " " & oPayment.I_Account & vbCrLf   '"From account: " & oPayment.I_Account & vbCrLf
                                        sErrMsg = sErrMsg & LRSCommon(20041) & " " & StringToDate(oPayment.DATE_Payment) & vbCrLf       '"Due date: " & StringToDate(oPayment.DATE_Payment) & vbCrLf
                                        sErrMsg = sErrMsg & LRSCommon(20042) & " " & oPayment.E_Name & vbCrLf                    '"To: " & oPayment.E_Name & vbCrLf
                                        sErrMsg = sErrMsg & LRSCommon(20043) & " " & oPayment.E_Account & vbCrLf                 '"To account: " & oPayment.E_Account & vbCrLf
                                        sErrMsg = sErrMsg & LRSCommon(20044) & " " & Format(oPayment.MON_InvoiceAmount / 100, "##,##0.00")                 '"Amount: " & Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00")
                                        'MsgBox "Betaling til egen konto krever at mottakerkonto er innmeldt i klientoppsettet i BabelBank." & vbCrLf & sErrMsg, vbExclamation, "BabelBank avbrytes"
                                        MsgBox(LRSCommon(20045) & vbCrLf & vbCrLf & sErrMsg, vbExclamation, "BabelBank")
                                        '- hva gj�r vi n�r det oppdages at konto ikke er satt opp ?
                                        sReturnCode = "-1"
                                        Err.Raise(20045, , sErrMsg)
                                    End If
                                End If

                            ElseIf Left$(oPayment.BANK_SWIFTCode, 6) = "DNBAUS" Or oPayment.BANK_BranchNo = "026005610" Then
                                ' -78      US  Domestic        Wire DNBAUS/DNBAUS
                                sReturnCode = 78
                            Else
                                '-77      US  Domestic        Wire
                                sReturnCode = 77
                            End If

                        ElseIf oPayment.I_CountryCode = "CA" And _
                        (oPayment.MON_InvoiceCurrency = "CAD" Or oPayment.MON_InvoiceCurrency = "USD") And _
                        (oPayment.MON_InvoiceCurrency = oPayment.MON_AccountCurrency) _
                        Then
                            '84      CA  Domestic        ACH (Low value pmt AFT-ACH pmt)
                            '85      CA  Domestic        Wire (High value pmt LVTS)
                            '86      CA  Domestic        Cheque pmt (Local bank draft)
                            ' We may have marked as LOCALLOW, LOCALHIGH, LOCALCHECK in setup
                            If oPayment.DnBNORTBIPayType = "LOCALLOW" Then
                                '- 84      CA  Domestic        ACH (Low value pmt AFT-ACH pmt)
                                sReturnCode = 84
                            ElseIf oPayment.DnBNORTBIPayType = "LOCALHIGH" Then
                                '-85      CA  Domestic        Wire (High value pmt LVTS)
                                sReturnCode = 85
                            ElseIf oPayment.DnBNORTBIPayType = "LOCALCHECK" Then
                                If oPayment.MON_InvoiceCurrency = "CAD" Then
                                    '-86      CA  Domestic        Cheque pmt (Local bank draft)
                                    sReturnCode = 86
                                Else
                                    ' check US
                                    '-30      US  Domestic        Check sent beneficiary
                                    sReturnCode = 30
                                End If

                                ' OR we have not set LOCAL... in setup
                                ' ------------------------------------
                            ElseIf oPayment.E_Account = "" Or _
                            oPayment.E_Account = "A CHEQUE" Or _
                            oPayment.E_Account = "CHECK" _
                            Then
                                If oPayment.MON_InvoiceCurrency = "CAD" Then
                                    '-86      CA  Domestic        Cheque pmt (Local bank draft)
                                    sReturnCode = 86
                                Else
                                    ' check US
                                    '-30      US  Domestic        Check sent beneficiary
                                    sReturnCode = 30
                                End If
                            ElseIf oPayment.Priority Then
                                '-85      CA  Domestic        Wire (High value pmt LVTS)
                                sReturnCode = 85
                            Else
                                '- 84      CA  Domestic        ACH (Low value pmt AFT-ACH pmt)
                                sReturnCode = 84
                            End If
                        Else
                            ' can happen for domestics in other currency than USD, but if so, bring it over to Common Int treating
                            sReturnCode = "common"
                        End If
                    Else
                        sReturnCode = "common"
                    End If

                    'xoknet 12.09.2011 Lagt til Case "SG"
                Case "SG"
                    If oPayment.PayType = "D" Then
                        ' Can be either SG domestic or AU domestic !
                        ' XOKNET 17.01.2013
                        ' Domestics can be in other currencies than SGD !
                        'If oPayment.I_CountryCode = "SG" And oPayment.MON_InvoiceCurrency = "SGD" Then
                        If oPayment.I_CountryCode = "SG" Then
                            ' Singapore
                            '----------
                            ' XOKNET 26.11.2012 added cpde 69
                            '69      SG  Domestic        Credit DNBASG different customer
                            ' MEN vent til DNB er klare, bruk forel�pig kode 90
                            '70      SG  Domestic        eGIRO (ACH)
                            '72      SG  Domestic        Salary
                            '90      SG  Domestic        Meps
                            '30      SG  Domestic        Cheque pmt (Local bank draf - Benef)
                            If oPayment.E_Account = "" Then
                                sDebitAccountCurrency = dbSelectAccountCurrency(oPayment.I_Account)
                                ' 17.02.2014 Added test for debbitaccounts currency for checkpayments
                                If sDebitAccountCurrency = "SGD" And (oPayment.E_CountryCode = "SG" Or oPayment.E_CountryCode = "") Then
                                    ' -30      SG  Domestic        Cheque pmt (Local bank draf - Benef)
                                    sReturnCode = "30"
                                Else
                                    ' -44      SG  Int        Cheque pmt (Local bank draf - Benef)
                                    sReturnCode = "44"
                                End If

                            ElseIf oPayment.Priority Then
                                ' -'90      SG  Domestic        Meps
                                sReturnCode = 90
                                ' XOKNET 17.01.2013 - added IntraCompany for domestic SG
                            ElseIf Left$(oPayment.BANK_SWIFTCode, 6) = "DNBASG" And (oPayment.PayCode = "403" Or oPayment.ToOwnAccount) Then
                                ' for Domestic, always 52
                                sReturnCode = "52"

                                ' xoknet 19.09.2011 - added test for DNBASG
                            ElseIf Left$(oPayment.BANK_SWIFTCode, 6) <> "DNBASG" Then
                                ' XOKNET 24.09.2012 - eGiro only when ACH
                                If oPayment.DnBNORTBIPayType = "ACH" And sTP_PayType = "M" Then
                                    '-70      SG  Domestic        eGIRO (ACH)
                                    sReturnCode = 70
                                ElseIf oPayment.DnBNORTBIPayType = "SAL" And sTP_PayType = "M" Then
                                    '-72      SG  Domestic        Salary, SG
                                    sReturnCode = 72
                                Else
                                    ' -'90      SG  Domestic        Meps
                                    sReturnCode = 90
                                End If
                                ' XOKNET 26.11.2012 added code 69
                            ElseIf Left$(oPayment.BANK_SWIFTCode, 6) = "DNBASG" Then
                                ' 69          SG  Domestic        Credit DNBASG different customer
                                sReturnCode = "69"
                                ' MEN vent til DNB er klare, bruk forel�pig kode 90
                                sReturnCode = "90"

                            Else
                                sReturnCode = "common"
                            End If
                            ' 16.05.2019 - NOT POSSIBLE ANYMORE with Local Australia
                            ' --------------------------------------------------------------------------------------------------------------------------
                        ElseIf (oPayment.I_CountryCode = "AU" Or oPayment.BANK_AccountCountryCode = "AU") And oPayment.MON_InvoiceCurrency = "AUD" And _
                            (oPayment.MON_InvoiceCurrency = oPayment.MON_AccountCurrency) Then
                            ' Australia
                            ' ----------
                            '84      AU  Domestic        Low value pmt (BECS/CS2-ACH pmt)
                            '85      AU  Domestic        High value pmt (HVCS/CS4)
                            ' xoknet 01.02.2012 changed code 91 to 86
                            '86  Domestic    Cheque pmt (Local bank draft)   11.00

                            ' We may have marked as LOCALLOW, LOCALHIGH, LOCALCHECK in setup
                            If oPayment.DnBNORTBIPayType = "LOCALLOW" Then
                                '-84      AU  Domestic        Low value pmt (BECS/CS2-ACH pmt)
                                sReturnCode = 84
                            ElseIf oPayment.DnBNORTBIPayType = "LOCALHIGH" Then
                                '-85      AU  Domestic        High value pmt (HVCS/CS4)
                                sReturnCode = 85
                            ElseIf oPayment.DnBNORTBIPayType = "LOCALCHECK" Then
                                ' xoknet 01.02.2012 changed code 91 to 86
                                '86  Domestic    Cheque pmt (Local bank draft)   11.00
                                sReturnCode = 86

                                ' OR we have not set LOCAL... in setup
                                ' ------------------------------------
                            ElseIf oPayment.E_Account = "" Or _
                            oPayment.E_Account = "A CHEQUE" Or _
                            oPayment.E_Account = "CHECK" _
                            Then
                                ' xoknet 01.02.2012 changed code 91 to 86
                                '86  Domestic    Cheque pmt (Local bank draft)   11.00
                                sReturnCode = 86
                            ElseIf oPayment.Priority Then
                                '-85      AU  Domestic        High value pmt (HVCS/CS4)
                                sReturnCode = 85
                            Else
                                '-84      AU  Domestic        Low value pmt (BECS/CS2-ACH pmt)
                                sReturnCode = 84
                            End If

                        Else
                            ' should not happen, but if so, bring it over to Common Int treating
                            sReturnCode = "common"
                        End If
                    ElseIf oPayment.PayType = "S" And sTP_PayType = "M" Then
                        '-72      SG  Domestic        Salary, SG
                        sReturnCode = 72
                    ElseIf oPayment.PayType = "M" And sTP_PayType = "M" Then
                        '-70      SG  Domestic        eGIRO (ACH), SG
                        sReturnCode = 70

                    Else
                        sReturnCode = "common"
                    End If

            End Select

            ' ==================================================================
            ' Treat common Non-domestics in common
            ' Incorporated for UK, GE ......
            ' ==================================================================
            If sReturnCode = "common" Then
                ' -44  N/A ALL     Non-Domestic    B   Bank drafts/cheque to beneficiary
                ' -45  N/A NO/GB   Non-Domestic    B   Bank drafts/cheque to ordering party
                ' -48      All     Non Domestic        Intra company other banks (INTC)
                ' FJERNET 25.02.2014 -49      All <> GB   Non-Domestic    B   EU Payments
                ' -50      All <> SE DK    Non-Domestic    B   Normal Payments
                ' -50      All <> SE DK    Non-Domestic    B   Urgent Payment: Marked with flag in Telepay + ( "J" in BETFOR01 Pos159 )
                ' -51  IBAN/BBAN   Request for Transfer (determined by product id)  (TKIO in TelepayPlus)
                ' -52  All <> SE   Domestic    E   Intra Company Domestic DNBA (determined by product id)(NBB Without exchange)
                ' -53  All Domestic    E   Intra Company Domestic DNBA  (determined by product id) (NBB) With Exchange)
                ' -54  Non Domestic    E   Intra Company DNBA Corss Border  (determined by product id) (NBB) With Exchange)
                ' 25.02.2014 - added 60/61, replaces 49
                '- 60      Domestic/Non Domestic       SEPA Structured/reference
                '- 61      Domestic/Non Domestic       SEPA Non Structured

                ' XOKNET 06.01.2011 removed comment about 55 below

                'sDebetCountryCode = Mid$(oPayment.BANK_I_SWIFTCode, 5, 2)  moved up
                ' find out if "SEPA-country"
                'FindDomesticCurrency sDebetCountryCode, bISSepaCountry  ' moved up, also used for Domestic
                ' 17.10.13: Her mener jeg det m� ha v�rt en alvorlig blunder. Det er ikke DebetCountryCode som avgj�r om det er SEPA-land, det er KreditCountrycode ! Se �verst i denne funksjonen!

                ' added 17.02.2014
                bISSEPADebitCountry = IsSEPACountry(sDebetCountryCode)

                If oPayment.E_Account = vbNullString Or oPayment.PayCode = "160" Then
                    ' -44  N/A ALL     Non-Domestic    B   Bank drafts/cheque to beneficiary
                    ' XOKNET 03.01.2011 Changed next lines about 44/45  - WRONG ---> unable to determine by BabelBankif 44 or 45, choose 44
                    'sReturnCode = "44"

                    ' "45" only for NO, GB, SG
                    If oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToPayer And (sDebetCountryCode = "NO" Or sDebetCountryCode = "GB" Or sDebetCountryCode = "SG") Then
                        sReturnCode = "45"
                    Else
                        sReturnCode = "44"
                    End If

                ElseIf Not oPayment.BANK_I_Domestic And Left$(oPayment.BANK_I_SWIFTCode, 4) <> "DNBA" Then
                    ' -51  IBAN/BBAN   Request for Transfer (determined by product id)  (TKIO in TelepayPlus)
                    sReturnCode = "51"

                    ' XOKNET 17.01.2012 - changed for IntraCompany International (which may be Domestic, with Exchange!)

                ElseIf (oPayment.ToOwnAccount Or oPayment.PayCode = "403") And Left$(oPayment.BANK_SWIFTCode, 4) = "DNBA" Then
                    If Left$(oPayment.BANK_SWIFTCode, 8) = Left$(oPayment.BANK_I_SWIFTCode, 8) Then
                        ' Inside same DNB subcidiary ! And when International, we have in TreatTBI established that
                        ' this is a payment with exchange

                        ' added next lines and test 17.02.2014
                        sDebitAccountCurrency = dbSelectAccountCurrency(oPayment.I_Account)
                        sCreditAccountCurrency = dbSelectAccountCurrency(oPayment.E_Account)
                        If Len(sCreditAccountCurrency) > 0 And sDebitAccountCurrency = sCreditAccountCurrency And sDebitAccountCurrency = oPayment.MON_InvoiceCurrency Then
                            ' currency for debitacc and creditacc and transer are all the same, no exchange
                            sReturnCode = "52"
                        Else
                            ' -53  All Domestic    E   Intra Company Domestic DNBA  (determined by product id) (NBB) With Exchange)
                            sReturnCode = "53"
                        End If
                    Else
                        ' Not inside DNB-bank, or other than DNB
                        ' -54  Non Domestic    E   Intra Company DNBA Cross Border  (determined by product id) (NBB) With Exchange)
                        sTmp = dbSelectAccountSWIFT(oPayment.E_Account)
                        ' we need to know that the account exist!
                        If Len(sTmp) > 0 Then
                            sReturnCode = "54"
                        Else
                            ' errormsg
                            'Add errorinformation, which helps the user to identify the payment in matter
                            sErrMsg = LRSCommon(20040) & " " & oPayment.I_Account & vbCrLf   '"From account: " & oPayment.I_Account & vbCrLf
                            sErrMsg = sErrMsg & LRSCommon(20041) & " " & StringToDate(oPayment.DATE_Payment) & vbCrLf       '"Due date: " & StringToDate(oPayment.DATE_Payment) & vbCrLf
                            sErrMsg = sErrMsg & LRSCommon(20042) & " " & oPayment.E_Name & vbCrLf                    '"To: " & oPayment.E_Name & vbCrLf
                            sErrMsg = sErrMsg & LRSCommon(20043) & " " & oPayment.E_Account & vbCrLf                 '"To account: " & oPayment.E_Account & vbCrLf
                            sErrMsg = sErrMsg & LRSCommon(20044) & " " & Format(oPayment.MON_InvoiceAmount / 100, "##,##0.00")                 '"Amount: " & Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00")
                            'MsgBox "Betaling til egen konto krever at mottakerkonto er innmeldt i klientoppsettet i BabelBank." & vbCrLf & sErrMsg, vbExclamation, "BabelBank avbrytes"
                            MsgBox(LRSCommon(20045) & vbCrLf & vbCrLf & sErrMsg, vbExclamation, "BabelBank")
                            '- hva gj�r vi n�r det oppdages at konto ikke er satt opp ?
                            sReturnCode = "-1"
                            Err.Raise(20045, , sErrMsg)
                        End If
                    End If
                ElseIf ((oPayment.ToOwnAccount Or oPayment.PayCode = "403") Or InStr(oPayment.NOTI_NotificationMessageToBank, "INTC") > 0) And Left$(oPayment.BANK_SWIFTCode, 4) <> "DNBA" Then
                    ' -48      All <> SE   Non Domestic        Intra company other banks (INTC)
                    sReturnCode = "48"

                ElseIf IsIBANNumber(oPayment.E_Account, True, False) And (Len(oPayment.BANK_SWIFTCode) = 8 Or Len(oPayment.BANK_SWIFTCode) = 11) And _
                bIsSEPACountry And oPayment.MON_InvoiceCurrency = "EUR" And oPayment.MON_ChargeMeDomestic = True And Not oPayment.MON_ChargeMeAbroad _
                And bISSEPADebitCountry _
                Then ' 17.02.2014 added bISSEPADebitCountry

                    'and XOKNET 24.01.2011 - no amountlimit (changed again from DnB !!!!)
                    ' this taken away If/End if under
                    'If oPayment.MON_InvoiceAmount < 5000000 Then
                    ' 25.02.2014 REMOVED 49
                    ' -49      All <> GB   Non-Domestic    B   EU Payments
                    'ReturnCode = "49"
                    'else
                    ' -50      All <> SE DK    Non-Domestic    B   Normal Payments
                    ' -50      All <> SE DK    Non-Domestic    B   Urgent Payment: Marked with flag in Telepay + ( "J" in BETFOR01 Pos159 )
                    '    sReturnCode = "50"
                    'End If

                    ' 25.02.2014 Added 60/61
                    If oPayment.Structured Or IsPaymentStructured(oPayment) Then
                        sReturnCode = "60"
                    Else
                        sReturnCode = "61"
                    End If


                Else
                    '50          All <> SE DK    Non-Domestic    B   Normal Payments
                    sReturnCode = "50"
                End If


            End If
        End If

        bbGetPayCodeTelepayPlus = sReturnCode

    End Function
    ' added next function 06.03.2017
    Public Function IsThisMeps(ByVal oPayment As vbBabel.Payment) As Boolean
        IsThisMeps = False
        If oPayment.Priority = True Then
            IsThisMeps = True
        End If
        If oPayment.BANK_SWIFTCode <> "DNBASG" Then
            If oPayment.PayType = "D" And oPayment.E_Account <> "" Then
                IsThisMeps = True
            End If
        End If

    End Function
    Public Function IsThisNachaDNB(ByVal oPayment As vbBabel.Payment) As Boolean
        IsThisNachaDNB = False
        If Left(oPayment.BANK_I_SWIFTCode, 6) = "DNBAUS" And oPayment.PayType <> "I" And oPayment.MON_InvoiceCurrency = "USD" Then
            IsThisNachaDNB = True
            ' Cheque is not Nacha
            If oPayment.Cheque > vbBabel.BabelFiles.ChequeType.noCheque Or oPayment.E_Account = "" Then
                IsThisNachaDNB = False
            ElseIf oPayment.Priority Then   ' Priority payments, wire, can be exported to Pain.001 for DNB
                IsThisNachaDNB = False
            ElseIf oPayment.BANK_CountryCode = "CA" Then ' OK to export Canada payments
                IsThisNachaDNB = False
            ElseIf oPayment.PayCode = "281" Then
                ' 10.12.2018 - Added a new PaymentType =281 Wire US
                IsThisNachaDNB = False
            End If
        End If

    End Function

    'Function bbGetPayCode(ByRef sInCode As String, ByRef sFormat As String, Optional ByRef sPayType As String = "") As String
    Function bbGetPayCode(ByRef sInCode As String, ByRef sFormat As String, Optional ByRef sPayType As String = "", Optional ByVal bCredits As Boolean = False, Optional ByRef sCountry As String = "", Optional ByVal bToOwnAccount As Boolean = False) As String
        ' Get a PayCode from BabelBanks generic PayCode
        Dim sReturnCode As String = ""

        ' Kodeforklaring: Se bbSetPayCode
        '-----------------------------------------------------------------------

        Select Case sFormat

            Case "TELEPAY", "TELEPAY2"

                Select Case sInCode
                    Case "101" ' Faste oppdrag
                        sReturnCode = "030"
                    Case "102" ' Husleie
                        sReturnCode = "033"
                    Case "103" ' Kontingent
                        sReturnCode = "034"
                    Case "104" ' AB Kontrakt
                        sReturnCode = "035"
                    Case "105" ' Skatt/avgift
                        sReturnCode = "609"
                    Case "106" ' Moms
                        sReturnCode = "037"
                    Case "108" ' Melding fra annen institusjon
                        sReturnCode = "608"
                    Case "130" ' Egenprodusert giro
                        sReturnCode = "630"
                    Case "150" ' Overf�ring med melding
                        sReturnCode = "602"
                    Case "151" ' Overf�ring med melding, og med kontoinnsetting (added 24.08.06, because for Leverant�rsbetalingar, NSA
                        sReturnCode = "602"
                    Case "160" ' Utbetaling uten mottakers konto
                        sReturnCode = "603"
                        ' 17.08.2010 added 190
                    Case "190" ' Plusgiro Sweden
                        sReturnCode = "602"
                    Case "250" 'Diverse overf�ring uten melding
                        sReturnCode = "600"
                    Case "304", "311", "315", "371", "373", "375"
                        sReturnCode = "601"

                        ' massetrans
                    Case "200" ' Pensjon
                        sReturnCode = "600"
                    Case "201" ' Pensjon
                        sReturnCode = "607"
                    Case "202" ' Barnetrygd
                        sReturnCode = "607"
                    Case "203" ' L�nn
                        sReturnCode = "604"
                    Case "204" ' Hyretrekk
                        sReturnCode = "605"
                    Case "205" ' Landbruksoppgj�r
                        sReturnCode = "606"
                    Case "206" ' L�nn - 2 dgs. forl�p
                        sReturnCode = "604"
                    Case "221" ' Fritekst massebet, fra egenref betfor21
                        sReturnCode = "621"
                    Case "222" ' Fritekst massebet, fra egenref betfor22
                        sReturnCode = "622"
                    Case "270" ' Statens konsernkonto l�nn
                        sReturnCode = "604"
                    Case "333" 'From Bankgiro to Bankgiro including a none OCR reference, a none OCR credit note
                        sReturnCode = "602"
                    Case "334" 'From Bankgiro to Bankgiro including free text remittance info
                        sReturnCode = "602"
                    Case "335" 'From Bankgiro to a bank account including free text remittance info populated by the element <Ustrd>
                        sReturnCode = "602"
                    Case "336" 'From Bankgiro to a bank account including free text remittance info populated by the element <Purp><Prtry>
                        sReturnCode = "602"
                    Case "337" 'From Bankgiro to money order (payment advise)
                        sReturnCode = "603"
                    Case "338" 'From Bankgiro to a bank account
                        sReturnCode = "602"
                    Case "343" 'From Plusgirot to PlusGirot including free text remittance info, and ultimate(debtor)
                        sReturnCode = "602"
                    Case "345" 'From Plusgirot to Bankgiro including free text remittance info
                        sReturnCode = "602"
                    Case "346" 'From Plusgirot to a bank account including free text remittanceinfo
                        sReturnCode = "602"
                    Case "347" 'From Plusgirot to money order (payment advise)
                        sReturnCode = "603"
                    Case "348" 'From Plusgirot Salary payment to a bank account
                        sReturnCode = "604"
                    Case "349" 'From Plusgirot Pension payment to a bank account
                        sReturnCode = "607"
                    Case "350" 'Intercompany payment from PlusGirot to a bank account including freetext remittance info (domestic SWIFT) (high value)
                        sReturnCode = "602"
                    Case "351" 'Same day value payment from a Nordea account to a bank account including freetext remittance info (via STOK) (high value)
                        sReturnCode = "602"
                    Case "352" 'Financial payment from a Nordea account to a bank account including freetext remittance info (domestic SWIFT) (high value)
                        sReturnCode = "602"

                        ' KID
                    Case "301" 'Betaling med KID
                        sReturnCode = "601"
                    Case "331" 'From Bankgiro to Bankgiro including an OCR reference
                        sReturnCode = "601"
                    Case "332" 'From Bankgiro to Bankgiro including an OCR reference, an OCR credit note
                        sReturnCode = "601"
                    Case "341" 'From Plusgirot to PlusGirot including an OCR reference
                        sReturnCode = "601"
                    Case "342" 'From Plusgirot to PlusGirot including two none OCR references, none OCR credit note (Structured??)
                        sReturnCode = "601"
                    Case "344" 'From Plusgirot to Bankgiro including an OCR reference
                        sReturnCode = "601"

                    Case "403" ' Til egen konto
                        ' vi har ikke en slik i Telepay, men det er betaling uten melding
                        sReturnCode = "600"

                        ' XokNET
                    Case Else
                        If sPayType = "M" Then
                            sReturnCode = "604" 'L�nn
                        Else
                            ' fakturatrans
                            sReturnCode = "602" 'overf�ring med melding
                        End If

                End Select


            Case "DIRREM"

                Select Case sInCode

                    Case "150" ' Overf�ring med melding
                        sReturnCode = "03"
                    Case "151" ' Overf�ring med melding, og med kontoinnsetting (added 24.08.06, because for Leverant�rsbetalingar, NSA
                        sReturnCode = "03"
                    Case "160" ' Utbet. uten mottakerkonto
                        sReturnCode = "04"
                        ' changed 15.04.2009 - according to BBS docs it is 05 on returnfiles - wait !!!
                        'sReturnCode = "05"
                    Case "170" ' Statens konsernkonto
                        sReturnCode = "50"

                        ' massetrans
                    Case "203" ' L�nn
                        sReturnCode = "01"
                    Case "201" ' Pensjon
                        sReturnCode = "65"
                    Case "202" ' Barnetrygd
                        sReturnCode = "02" 'no code in dirrem for for barnetrygd
                    Case "204" ' Hyretrekk
                        sReturnCode = "02" 'no code in dirrem for for hyretrekk
                    Case "205" ' Landbruksoppgj�r
                        sReturnCode = "62"
                    Case "206" ' L�nn - 2 dgs. forl�p
                        sReturnCode = "01"
                    Case "250" ' Div overf�ring uten melding
                        sReturnCode = "02"
                    Case "270" ' Statens konsernkonto l�nn
                        sReturnCode = "01" 'no code in dirrem for for statens konsernkonto


                        ' kid
                    Case "301" ' Bet. med KID
                        sReturnCode = "12"
                    Case "302" ' KID med underspesifikasjoner
                        sReturnCode = "16"
                    Case "303" ' Kid og kreditnota
                        sReturnCode = "17"

                        ' diverse
                    Case Is = "401" ' Visa
                        sReturnCode = "39"

                    Case Else

                        If sPayType = "M" Then
                            sReturnCode = "01" 'L�nn
                        Else
                            sReturnCode = "03" ' covers several codes for transfer with advice
                        End If
                End Select

            Case "Autogiro"

                Select Case sInCode
                    Case "611" ' Trans. fra giro belastet konto
                        sReturnCode = "02"
                    Case "612" ' Trans fra Faste Oppdrag
                        sReturnCode = "03"
                End Select

            Case "OCR"

                Select Case sInCode
                    Case "510" ' Trans. fra giro belastet konto
                        sReturnCode = "10"
                    Case "511" ' Trans fra Faste Oppdrag
                        sReturnCode = "11"
                    Case "512" ' Trans fra Dir rem
                        sReturnCode = "12"
                    Case "513" ' Trans fra BTG
                        sReturnCode = "13"
                    Case "514" ' Trans fra skrankegiro
                        sReturnCode = "14"
                    Case "515" ' Trans fra avtalegiro
                        sReturnCode = "15"
                    Case "516" ' Trans fra Telegiro
                        sReturnCode = "16"
                    Case "517" ' Trans fra giro - betalt kontant
                        sReturnCode = "17"

                    Case Else
                        ' something rotten - but give it a standard number:
                        sReturnCode = "10" ' Trans. fra giro belastet konto

                End Select

            Case "OCR_FBO"
                Select Case sInCode
                    Case "530"
                        sReturnCode = "0" 'All FBOs - just info
                    Case "531"
                        sReturnCode = "1" 'New FBO
                    Case "532"
                        sReturnCode = "2" 'Deleted FBO
                    Case Else
                        ' something rotten - but give it a standard number:
                        sReturnCode = "0" ' Trans. fra giro belastet konto

                End Select


            Case "ErhvervsGiro"

                Select Case sInCode
                    Case "100"
                        sReturnCode = "411"
                    Case "150" ' Transfer to a known account. Freetext on bankstatement.
                        'If long freetext a seperate letter is sent
                        sReturnCode = "411"
                    Case "151" ' Overf�ring med melding, og med kontoinnsetting (added 24.08.06, because for Leverant�rsbetalingar, NSA
                        sReturnCode = "411"

                    Case "200"
                        sReturnCode = "605"
                    Case "206" ' Salary, 2-days ????
                        sReturnCode = "600"
                    Case "203" ' Salary, day by day ????
                        sReturnCode = "605"
                    Case "160" ' Check (receivers accountno. is unknown)
                        sReturnCode = "630"
                    Case "301" ' Indbetalingskort - Danish OCR
                        sReturnCode = "910"

                    Case Else
                        ' something rotten - but give it a standard number:
                        If sPayType = "M" Or sPayType = "S" Then
                            ' massetrans
                            sReturnCode = "605" 'l�nn
                        Else
                            ' fakturatrans
                            sReturnCode = "411" 'overf�ring med melding
                        End If

                End Select

            Case "CREMUL"

                Select Case sInCode
                    Case "180" 'UDUS
                        sReturnCode = "233"
                    Case "510" ' Trans. fra giro belastet konto
                        sReturnCode = "230"
                    Case "511" ' Trans fra Faste Oppdrag
                        sReturnCode = "230"
                    Case "512" ' Trans fra Dir rem
                        sReturnCode = "230"
                    Case "513" ' Trans fra BTG
                        sReturnCode = "230"
                    Case "514" ' Trans fra skrankegiro
                        sReturnCode = "230"
                    Case "515" ' Trans fra avtalegiro
                        sReturnCode = "230"
                    Case "516" ' Trans fra Telegiro
                        sReturnCode = "230"
                    Case "517" ' Trans fra giro - betalt kontant
                        sReturnCode = "230"
                    Case "518" ' Wrong KID
                        sReturnCode = "231"
                    Case "601" ' Electronic payment
                        sReturnCode = "233"
                    Case "602" ' Manual payment
                        sReturnCode = "234"
                    Case "603" ' Internal transaction
                        sReturnCode = "BKT"
                    Case "604" ' Advanced Payment
                        sReturnCode = "ADV"
                    Case "605" ' Bankgiro
                        sReturnCode = "BGI"
                    Case "606" ' Bank Draft
                        sReturnCode = "BKD"
                    Case "607" ' Cash Management Transfer
                        sReturnCode = "CAS"
                    Case "608" ' Cheque Internat.
                        sReturnCode = "CHI"
                    Case "609" ' Cheque National
                        sReturnCode = "CHN"
                    Case "610" ' Commercial Credit
                        sReturnCode = "COC"
                    Case "629"     ' Structured payment
                        sReturnCode = "240"
                    Case "630" ' Cash Payment by post
                        sReturnCode = "CPP"
                    Case "631" ' Buying or selling Foreign Notes
                        sReturnCode = "CUX"
                    Case "632" ' Dividend
                        sReturnCode = "DIV"
                    Case "633" ' Foreign Exchange
                        sReturnCode = "FEX"
                    Case "634" ' Purchase and sale of goods
                        sReturnCode = "GDS"
                    Case "635" ' Government Payment
                        sReturnCode = "GVT"
                    Case "636" ' Insurance Premium
                        sReturnCode = "INS"
                    Case "637" ' Interest
                        sReturnCode = "INT"
                    Case "638" ' License Fees
                        sReturnCode = "LIF"
                    Case "639" ' Loan
                        sReturnCode = "LOA"
                    Case "640" ' Loan Repayment
                        sReturnCode = "LOR"
                    Case "641" ' Pension
                        sReturnCode = "PEN"
                    Case "642" ' Rent
                        sReturnCode = "REN"
                    Case "643" ' Salary
                        sReturnCode = "SAL"
                    Case "644" ' Purchase and Sale of Services
                        sReturnCode = "SCV"
                    Case "645" ' Benefit
                        sReturnCode = "SSB"
                    Case "646" ' Tax Payment
                        sReturnCode = "TAX"
                    Case "647" ' Value Added Tax
                        sReturnCode = "VAT"
                    Case "648" ' Commision (Gebyr)
                        sReturnCode = "COM"
                    Case "600"
                        sReturnCode = "ZZZ"
                    Case "611" ' Autogiro
                        sReturnCode = "232"
                    Case Else
                        sReturnCode = "233"
                End Select

            Case "PAYMUL"

                Select Case sInCode
                    Case "101" ' Faste oppdrag
                        sReturnCode = ""
                    Case "102" ' Husleie
                        sReturnCode = ""
                    Case "103" ' Kontingent
                        sReturnCode = ""
                    Case "104" ' AB Kontrakt
                        sReturnCode = ""
                    Case "105" ' Skatt/avgift
                        sReturnCode = ""
                    Case "106" ' Moms
                        sReturnCode = ""
                    Case "108" ' Melding fra annen institusjon
                        sReturnCode = ""
                    Case "130" ' Egenprodusert giro
                        sReturnCode = ""
                    Case "150" ' Overf�ring med melding
                        sReturnCode = ""
                    Case "151" ' Overf�ring med melding, og med kontoinnsetting (added 24.08.06, because for Leverant�rsbetalingar, NSA
                        sReturnCode = ""
                    Case "160" ' Utbetaling uten mottakers konto
                        If sPayType = "I" Then
                            sReturnCode = "23"
                        Else
                            sReturnCode = "21"
                        End If
                    Case "250" 'Diverse overf�ring uten melding
                        sReturnCode = ""

                        ' massetrans
                    Case "201" ' Pensjon
                        sReturnCode = ""
                    Case "202" ' Barnetrygd
                        sReturnCode = ""
                    Case "203" ' L�nn
                        sReturnCode = ""
                    Case "204" ' Hyretrekk
                        sReturnCode = ""
                    Case "205" ' Landbruksoppgj�r
                        sReturnCode = ""
                    Case "206" ' L�nn - 2 dgs. forl�p
                        sReturnCode = ""
                    Case "221" ' Fritekst massebet, fra egenref betfor21
                        sReturnCode = ""
                    Case "222" ' Fritekst massebet, fra egenref betfor22
                        sReturnCode = ""
                    Case "270" ' Statens konsernkonto l�nn
                        sReturnCode = ""

                        ' KID
                    Case "301" 'Betaling med KID
                        sReturnCode = ""


                    Case Else
                        sReturnCode = ""

                End Select

            Case "DTAUS"
                'Codes marked with *, should return '888' as the last 3 digits if
                ' the originator of the payment/payee is a non-resident

                Select Case Left(sInCode, 2)
                    Case "612"
                        sReturnCode = "04000"
                    Case "613"
                        sReturnCode = "05000"
                    Case "614"
                        sReturnCode = "05005"
                    Case "615"
                        sReturnCode = "05006"
                    Case "616"
                        sReturnCode = "05015"
                    Case "150"
                        sReturnCode = "51000"
                    Case "203"
                        sReturnCode = "53000"
                    Case "208"
                        'Pos 3-4 are either 00, or the respective savings premium percentage
                        'Pos 5 are last digit of the year to which the benefit applies
                        sReturnCode = "5400" & Right(Str(Year(Now)), 1)
                    Case "207"
                        sReturnCode = "56000"
                    Case Else
                        Err.Raise(10025, "vbBabel", LRS(10025), Left(sInCode, 2))
                End Select


            Case "ShipNet"
                If sPayType = "I" Then
                    ' International
                    Select Case sInCode
                        Case "201"
                            sReturnCode = "101"
                        Case "203"
                            sReturnCode = "102"
                        Case "205"
                            sReturnCode = "103"
                        Case "401"
                            sReturnCode = "104"
                        Case "175"
                            sReturnCode = "105"
                        Case "402"
                            sReturnCode = "106"
                        Case "403"
                            sReturnCode = "107"
                        Case "101"
                            sReturnCode = "108"
                        Case "105"
                            sReturnCode = "109"
                        Case "150"
                            sReturnCode = "110"
                        Case "151" 'Added 24.08.06
                            sReturnCode = "110"
                        Case "301"
                            sReturnCode = "111"
                        Case "160"
                            sReturnCode = "112"
                        Case Else
                            If sPayType = "M" Or sPayType = "S" Then
                                sReturnCode = "102" 'L�nn
                            Else
                                ' fakturatrans
                                sReturnCode = "110" 'overf�ring med melding
                            End If
                    End Select
                Else
                    ' Domestic, Mass or Salary
                    ' International
                    Select Case sInCode
                        Case "201"
                            sReturnCode = "1"
                        Case "203"
                            sReturnCode = "2"
                        Case "205"
                            sReturnCode = "3"
                        Case "401"
                            sReturnCode = "4"
                        Case "175"
                            sReturnCode = "5"
                        Case "402"
                            sReturnCode = "6"
                        Case "403"
                            sReturnCode = "7"
                        Case "101"
                            sReturnCode = "8"
                        Case "105"
                            sReturnCode = "9"
                        Case "150"
                            sReturnCode = "10"
                        Case "151"
                            sReturnCode = "10" ' Added 24.08.06
                        Case "301"
                            sReturnCode = "11"
                        Case "160"
                            sReturnCode = "12"
                        Case Else
                            If sPayType = "M" Or sPayType = "S" Then
                                sReturnCode = "2" 'L�nn
                            Else
                                ' fakturatrans
                                sReturnCode = "10" 'overf�ring med melding
                            End If
                    End Select
                End If
            Case "PERIODISKABETALNINGAR"

                Select Case sInCode

                    ' massetrans
                    Case "201" ' Pensjon
                        sReturnCode = "20"
                    Case "202" ' Underh�ll
                        sReturnCode = "19"
                    Case "203" ' L�nn
                        sReturnCode = "10"
                    Case "205" ' Landbruksoppgj�r
                        sReturnCode = "60"
                    Case Else
                        sReturnCode = "90" ' Other mass.
                End Select

            Case "SGFINANS_FAKTURA"
                Select Case sInCode
                    Case "701" ' Faktura
                        sReturnCode = "1"
                    Case "707" ' Kontant faktura
                        sReturnCode = "7"
                    Case "708" ' Kontant kreditnota
                        sReturnCode = "8"
                    Case "709" ' Kreditnota
                        sReturnCode = "9"
                End Select

                ' XNET 25.09.2013 Added UnitelPC
            Case "UNITELPC"

                '8   Opgjort konto
                '18  Omkostninger
                '35  Husleje
                '68  overf�rsel
                '86  Tilbagef�rsel
                '100 [Fri tekst]
                '102 Info -overf�rsel
                '139 Klientkontomidler
                '145 Tilskud
                '151 Overskydende skat
                '158 Indbetaling
                '159 Regning
                '160 Faktura
                '161 Kontingent
                '163 Udbytte
                '307 Udl�g
                '

                Select Case sInCode
                    Case "035"     ' Husleie
                        sReturnCode = "033"
                    Case "161"     ' Kontingent
                        sReturnCode = "034"
                    Case "151"     ' Skatt/avgift
                        sReturnCode = "609"
                    Case "100"     ' Overf�ring med melding
                        sReturnCode = "602"

                        '099 L�n*
                        '103 Pension*
                        '156 L�noverf�rsel*
                        '157 14 dages l�n*

                        ' massetrans
                    Case "103", "201"    ' Pensjon
                        sReturnCode = "600"
                    Case "099", "156", "157"   ' L�nn
                        sReturnCode = "604"

                    Case Else
                        If sPayType = "M" Then
                            sReturnCode = "604" 'L�nn
                        Else
                            ' fakturatrans
                            sReturnCode = "602" 'overf�ring med melding
                        End If

                End Select

            Case "ISO20022"
                If bCredits Then
                    ' Credit Transfers - Incoming payments
                    ' ------------------------------------
                    Select Case UCase(sInCode)
                        Case "671"
                            sReturnCode = "PMNT&MCRD&POSP"
                            sPayType = "D"
                        Case "601"
                            sReturnCode = "PMNT&RCDT&AUTT"
                            sPayType = "D"
                        Case "610"
                            sReturnCode = "PMNT&RCDT&VCOM"
                        Case "672"
                            sReturnCode = "PMNT&RCDT&XBCT"
                            sPayType = "I"
                        Case "675"
                            sReturnCode = "PMNT&RCDT&DMCT"
                            sPayType = "D"
                        Case "676"
                            sReturnCode = "PMNT&RCDT&ACDT"
                            sPayType = "M"
                        Case "677"
                            sReturnCode = "PMNT&RCDT&ATXN"
                            sPayType = "M"
                        Case "611"
                            sReturnCode = "PMNT&RDDT&PMDD"
                        Case "673"
                            sReturnCode = "PMNT&RCCN&ICCT"
                        Case "674"
                            sReturnCode = "PMNT&MCOP&OTHR"
                        Case "600"
                            sReturnCode = "PMNT&OTHR&OTHR"
                        Case "520"  ' Doktolk with TIPS - 
                            sReturnCode = "PDT" ' type OCR
                        Case "629"
                            'Added 15.10.2020 - Correct for DnB (this code is used on a file from the bank), Case BAMA
                            sReturnCode = "PMNT&RCDT&VCOM"

                            ' From here comes the "guesswork" - how to translate codes not set sepcific as ISO20022
                        Case Else
                            If sPayType = "I" Then
                                ' only one International type;
                                sReturnCode = "PMNT&RCDT&XBCT"
                            ElseIf sPayType = "M" Or sPayType = "S" Then
                                Select Case sCountry
                                    Case "SE"
                                        ' two types - ACDT and ATXN - which one to pick ???
                                        sReturnCode = "PMNT&RCDT&ATXN"
                                    Case Else
                                        sReturnCode = "PMNT&RCDT&AUTT"
                                End Select
                            Else
                                ' paytype="D"
                                If bToOwnAccount Then
                                    If sCountry <> "DK" And sCountry <> "PL" Then
                                        sReturnCode = "PMNT&RCCN&ICCT"
                                    Else
                                        ' No intracompanycodes for Denmark and Poland
                                        sReturnCode = "PMNT&RCDT&AUTT"
                                    End If
                                Else
                                    Select Case sCountry
                                        Case "DK", "EE", "FI", "LV", "LT", "PL"
                                            sReturnCode = "PMNT&RCDT&AUTT"
                                        Case "NO", "SE"
                                            sReturnCode = "PMNT&RCDT&DMCT"
                                        Case Else
                                            sReturnCode = "PMNT&RCDT&AUTT"
                                    End Select
                                End If

                            End If
                    End Select
                Else
                    ' Debits - outgoing payments
                    ' --------------------------
                    Select Case UCase(sInCode)
                        Case "100"
                            sReturnCode = "PMNT&OTHR&OTHR"
                        Case "150"
                            sReturnCode = "PMNT&ICDT&AUTT"
                        Case "200"
                            sReturnCode = "PMNT&ICDT&ATXN"
                            sPayType = "M"
                        Case "250"
                            sReturnCode = "PMNT&ICDT&ACDT"
                            sPayType = "M"
                        Case "403"
                            sReturnCode = "PMNT&ICCN&ICCT"
                        Case "407"
                            sReturnCode = "PMNT&ICDT&XBCT"
                            sPayType = "I"
                        Case "408"
                            sReturnCode = "PMNT&IDDT&URDD"
                        Case "413"
                            sReturnCode = "PMNT&MCRD&POSP"
                            sPayType = "D"
                        Case "629" ' - Should be incoming payments so I do not know why we get this code (imports Camt.054)
                            sReturnCode = "PMNT&ICDT&DMCT"

                            ' From here comes the "guesswork" - how to translate codes not set sepcific as ISO20022
                        Case Else
                            If sPayType = "I" Then
                                ' only one International type;
                                sReturnCode = "PMNT&ICDT&XBCT"
                            ElseIf sPayType = "M" Or sPayType = "S" Then
                                sReturnCode = "PMNT&ICDT&ATXN"
                            Else
                                ' paytype="D"
                                If bToOwnAccount Then
                                    If sCountry = "FI" Or sCountry <> "NO" Or sCountry = "SE" Then
                                        sReturnCode = "PMNT&ICCN&ICCT"
                                    ElseIf sCountry = "DK" Then
                                        sReturnCode = "XLS"
                                    ElseIf sCountry = "EE" Then
                                        sReturnCode = "PMNT&ICDT&ATXN"
                                        sPayType = "M"
                                    Else
                                        ' No intracompanycodes for other countries
                                        sReturnCode = "PMNT&RCDT&AUTT"
                                    End If
                                Else
                                    Select Case sCountry
                                        Case "DK"
                                            sReturnCode = "XLS"
                                        Case "EE"
                                            sReturnCode = "PMNT&RCDT&ATXN"
                                        Case Else
                                            sReturnCode = "PMNT&RCDT&AUTT"
                                    End Select
                                End If

                            End If
                    End Select
                End If
                ' if we have not been able to establish PayType for ISO20022, give it a default D
                If EmptyString(sPayType) Then
                    sPayType = "D"
                End If
        End Select

        bbGetPayCode = sReturnCode

    End Function
    Function bbGetPayCodeString(ByRef sInCode As String) As String
        ' Get a translated string from BabelBanks generic PayCode
        Dim sReturnText As String = ""


        Select Case sInCode
            Case "101" ' Faste oppdrag
                sReturnText = LRS(64006)
            Case "102" ' Husleie
                sReturnText = LRS(64007)
            Case "103" ' Kontingent
                sReturnText = LRS(64008)
            Case "104" ' AB Kontrakt
                sReturnText = LRS(64009)
            Case "105" ' Skatt/avgift
                sReturnText = LRS(64010)
            Case "106" ' Moms
                sReturnText = LRS(64011)
            Case "150" ' Overf�ring med melding
                sReturnText = LRS(64012)
            Case "160" ' Utbet. uten mottakerkonto
                sReturnText = LRS(64013)
            Case "170" ' Statens konsernkonto
                sReturnText = LRS(64014)
            Case "175" ' Hastebetaling
                sReturnText = LRS(64039)
            Case "177" ' Vent p� bekreftelse
                sReturnText = LRS(64040)

                ' massetrans
            Case "201" ' Pensjon
                sReturnText = LRS(64015)
            Case "202" ' Barnetrygd
                sReturnText = LRS(64016)
            Case "203" ' L�nn
                sReturnText = LRS(64017)
            Case "204" ' Hyretrekk
                sReturnText = LRS(64018)
            Case "205" ' Landbruksoppgj�r
                sReturnText = LRS(64019)
            Case "250" ' Div overf�ring uten melding
                sReturnText = LRS(64020)
            Case "270" ' Statens konsernkonto l�nn
                sReturnText = LRS(64021)
                ' kid
            Case "301" ' Bet. med KID
                sReturnText = LRS(64022)
            Case "302" ' KID med underspesifikasjoner
                sReturnText = LRS(64023)
            Case "303" ' Kid og kreditnota
                sReturnText = LRS(64024)
                ' diverse
            Case Is = "401" ' Visa
                sReturnText = LRS(64025)

            Case "510" ' Trans. fra giro belastet konto
                sReturnText = LRS(64026)
            Case "511" ' Trans fra Faste Oppdrag
                sReturnText = LRS(64027)
            Case "512" ' Trans fra Dir rem
                sReturnText = LRS(64028)
            Case "513" ' Trans fra BTG
                sReturnText = LRS(64029)
            Case "514" ' Trans fra skrankegiro
                sReturnText = LRS(64030)
            Case "515" ' Trans fra avtalegiro
                sReturnText = LRS(64031)
            Case "516" ' Trans fra Telegiro
                sReturnText = LRS(64032)
            Case "517" ' Trans fra giro - betalt kontant
                sReturnText = LRS(64033)
            Case "518" ' Wrong KID
                sReturnText = LRS(64034)
            Case "530" ' Info FBO
                sReturnText = LRS(64042)
            Case "531" ' New FBO
                sReturnText = LRS(64043)
            Case "532" ' Deleted FBO
                sReturnText = LRS(64044)
            Case "601" ' Electronic payment
                sReturnText = LRS(64035)
            Case "602" ' Manual payment
                sReturnText = LRS(64036)
            Case "603" ' Internal transaction
                sReturnText = LRS(64037)
            Case "611" ' Autogiro
                sReturnText = LRS(64038)
            Case "612" ' Autogiro m/melding
                sReturnText = LRS(64041)

                ' US ACH/Lockbox
                ' Lockbox              660
                ' ACH CTX              661
                ' ACH CCD              662
                ' ACH PPD              663
            Case "660" ' Autogiro m/melding
                sReturnText = "Lockbox"
            Case "661" ' Autogiro m/melding
                sReturnText = "CTX"
            Case "662" ' Autogiro m/melding
                sReturnText = "CCD"
            Case "663" ' Autogiro m/melding
                sReturnText = "PPD"

        End Select

        bbGetPayCodeString = sReturnText

    End Function
    'okXNET - 11.11.2013 - New function (moved from WriteSpecificERP, delete the function there as well!!!!!!!!!!!!!!!!!)
    Public Function WriteAgresso_BKKAgresso_GL(ByVal sBuntNummer As String, ByVal sPreSystem As String, ByVal sVoucherType As String, ByVal sCompanyCode As String, ByVal sGLAccount As String, ByVal sFreeTextVariable As String, ByVal sPeriod As String, ByVal sCurrency As String, ByVal nAmount As Double, ByVal sDate As String, ByVal bPostAgainstObservationAccount As Boolean, ByVal sDim1 As String, ByVal sDim2 As String, ByVal sDim3 As String, ByVal sDim4 As String, ByVal sDim5 As String, ByVal sDim6 As String, ByVal sDim7 As String) As String
        Dim sLineToWrite As String

        On Error GoTo ErrWriteAgresso_BKKAgresso_GL

        '26.02.2008
        sLineToWrite = PadRight(sBuntNummer, 25, " ") 'Buntnummer, 1-25, 25
        sLineToWrite = sLineToWrite & PadRight(sPreSystem, 25, " ") 'Interface (forsystem), 26-50, 25
        sLineToWrite = sLineToWrite & PadRight(sVoucherType, 25, " ") 'Bilagsart, 51-75, 25
        sLineToWrite = sLineToWrite & "GL" 'Transtype, 76-77, 2
        sLineToWrite = sLineToWrite & PadRight(sCompanyCode, 25, " ") 'Firmakode, 78-102, 25

        '15.03.2018 - Added next IF to write Dimensions using the new setup
        If sGLAccount.Contains("-") Then
            'Old code
            sLineToWrite = sLineToWrite & PadRight(xDelim(sGLAccount, "-", 1), 25, " ") 'Konto, 103-127, 25
            sLineToWrite = sLineToWrite & PadRight(xDelim(sGLAccount, "-", 2), 25, " ") 'Dim 1 (Ansvar/Koststed), 128-152, 25
            sLineToWrite = sLineToWrite & PadRight(xDelim(sGLAccount, "-", 3), 25, " ") 'Dim 2 (H.Prosess), 153-177, 25
            sLineToWrite = sLineToWrite & PadRight(xDelim(sGLAccount, "-", 4), 25, " ") 'Dim 3 (Anl/res), 178-202, 25
            sLineToWrite = sLineToWrite & PadRight(xDelim(sGLAccount, "-", 5), 25, " ") 'Dim 4 (Prosess/Prosjekt), 203-227, 25
            sLineToWrite = sLineToWrite & PadRight(xDelim(sGLAccount, "-", 6), 25, " ") 'Dim 5 (Segmentfordeling), 228-252, 25
            sLineToWrite = sLineToWrite & PadRight(xDelim(sGLAccount, "-", 7), 25, " ") 'Dim 6 (Konsernkode), 253-277, 25
        Else
            'New code
            sLineToWrite = sLineToWrite & PadRight(sGLAccount, 25, " ") 'Konto, 103-127, 25
            sLineToWrite = sLineToWrite & PadRight(sDim1, 25, " ") 'Dim 1 (Ansvar/Koststed), 128-152, 25
            sLineToWrite = sLineToWrite & PadRight(sDim2, 25, " ") 'Dim 2 (H.Prosess), 153-177, 25
            sLineToWrite = sLineToWrite & PadRight(sDim3, 25, " ") 'Dim 3 (Anl/res), 178-202, 25
            sLineToWrite = sLineToWrite & PadRight(sDim4, 25, " ") 'Dim 4 (Prosess/Prosjekt), 203-227, 25
            sLineToWrite = sLineToWrite & PadRight(sDim5, 25, " ") 'Dim 5 (Segmentfordeling), 228-252, 25
            sLineToWrite = sLineToWrite & PadRight(sDim6, 25, " ") 'Dim 6 (Konsernkode), 253-277, 25
        End If

        sLineToWrite = sLineToWrite & Space(25) ' 278-302, 25
        'XNET - 08.01.2013 - Added next IF
        If Not EmptyString(xDelim(sGLAccount, "-", 8)) Then
            sLineToWrite = sLineToWrite & PadRight(xDelim(sGLAccount, "-", 8), 25, " ") 'Tax_code, 303-327, 25 (bruk dimensjon 7)
        ElseIf Not EmptyString(sDim7) Then
            sLineToWrite = sLineToWrite & PadRight(sDim7, 25, " ") ' Tax_code, 303-327, 25
        Else
            sLineToWrite = sLineToWrite & PadRight("0", 25, " ") ' Tax_code, 303-327, 25
        End If
        sLineToWrite = sLineToWrite & Space(25) ' 328-352, 25 Tax system
        If Len(Trim$(sCurrency)) = 3 Then
            sLineToWrite = sLineToWrite & PadRight(Trim$(sCurrency), 25, " ") 'Valutakode, 353-377, 25
        Else
            sLineToWrite = sLineToWrite & PadRight("NOK", 25, " ") 'Valutakode, 353-377, 25
        End If
        'XOKNET 21.10.2013 - Added ABS to 4 lines in the next IF
        If nAmount > -0.0000001 Then
            sLineToWrite = sLineToWrite & " 1" 'Debit, 378-379, 2
            sLineToWrite = sLineToWrite & PadLeft("+" & Trim$(Str(Math.Abs(nAmount))), 20, " ") 'Valutabel�p, 380-399, 20
            sLineToWrite = sLineToWrite & PadLeft("+" & Trim$(Str(Math.Abs(nAmount))), 20, " ") 'Bel�p i firmavaluta (NOK), 400-419, 20
        Else
            sLineToWrite = sLineToWrite & "-1" 'Credit, 378-379, 2
            sLineToWrite = sLineToWrite & PadLeft("-" & Trim$(Str(Math.Abs(nAmount))), 20, " ") 'Valutabel�p, 380-399, 20
            sLineToWrite = sLineToWrite & PadLeft("-" & Trim$(Str(Math.Abs(nAmount))), 20, " ") 'Bel�p i firmavaluta (NOK), 400-419, 20
        End If
        sLineToWrite = sLineToWrite & Space(71) 'Feil i formatbeskrivelse?, 420-490
        'XOKNET - 08.11.2012 - Changed next line
        sLineToWrite = sLineToWrite & PadRight(sBuntNummer & " - " & RTrim$(sFreeTextVariable), 255, " ") 'Tekst, 491-745, 255
        sLineToWrite = sLineToWrite & Right(sDate, 8) 'Transaksjonsdato, 746-753, 8 - YYYYMMDD
        sLineToWrite = sLineToWrite & Right(sDate, 8) 'Bilagsdato, 754-761, 8 - YYYYMMDD
        sLineToWrite = sLineToWrite & Space(15) 'Feil i formatbeskrivelse?
        sLineToWrite = sLineToWrite & sPeriod 'Periode, 777-782, 6 - YYYYMM
        sLineToWrite = sLineToWrite & Space(1278)

        WriteAgresso_BKKAgresso_GL = sLineToWrite

        Exit Function

ErrWriteAgresso_BKKAgresso_GL:

        Err.Raise(Err.Number, "WriteAgresso_BKKAgresso_GL", Err.Description)

    End Function
    Public Function CheckPath(ByVal sPath As String, ByRef bFilenameIncluded As Boolean, Optional ByRef bMustBeStated As Boolean = True) As Boolean
        Dim oBackup As vbBabel.BabelFileHandling
        Dim bContinue As Boolean
        Dim iResponse As Short
        Dim bReturnValue As Boolean

        bReturnValue = True

        'Check if some text are entered in the control
        If Not EmptyString(sPath) Then
            'Check if a folder and file is stated
            bContinue = False
            If bFilenameIncluded Then
                If InStrRev(sPath, "\") > 0 Then
                    sPath = VB.Left(sPath, InStrRev(sPath, "\"))
                    bContinue = True
                Else
                    'MsgBox("You must state the full path, not only the filename.", MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "AN ERRONUEUS PATH STATED")
                    MsgBox(LRSCommon(64080), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRSCommon(64081))
                    bContinue = False
                    bReturnValue = False
                End If
            Else
                bContinue = True
            End If

            If bContinue Then
                'Check if the path exists:
                oBackup = New vbBabel.BabelFileHandling  ' CreateObject ("vbbabel.BabelFileHandling")

                oBackup.BackupPath = Trim(sPath)
                If Not oBackup.BackupPathExists Then
                    iResponse = MsgBox(oBackup.Message, MsgBoxStyle.YesNo + MsgBoxStyle.Question, LRS(60010))
                    If iResponse = MsgBoxResult.Yes Then
                        If oBackup.CreateFolderOK > 0 Then
                            '03.04.2007 JanP: Her var det tidligere mulighet for feil, pga retur True eller False
                            'Error in creating folder
                            MsgBox(oBackup.Message & vbCrLf & LRSCommon(64082) & " " & sPath, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRSCommon(64081))
                            'MsgBox oBackup.Message & vbCrLf & LRS(60011), vbOKOnly, LRS(60012)
                            bReturnValue = False
                        Else
                            bReturnValue = True
                        End If
                    Else
                        'No backup taken
                        bReturnValue = False
                    End If

                End If

            End If 'If bContinue Then
        Else
            If bMustBeStated Then
                'MsgBox("No path stated", MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, lrscommon(64081))
                MsgBox(LRSCommon(64083), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRSCommon(64081))
                bReturnValue = False
            Else
                bReturnValue = True
            End If
        End If

    End Function

    Private Function TranslatePackedDecimal(ByVal pd As String, ByVal pScale As Integer) As Double

        Dim i As Integer
        Dim Work As String = ""
        Dim b As Byte
        Dim v As Object

        For i = 1 To Len(pd)
            b = Asc(Mid$(pd, i, 1))
            v = Mid$(pd, i, 1)
            If i = Len(pd) Then
                If ((b And 240) / 16) = 13 Then ' Negative Sign nibble
                    Work = "-" & Work
                End If
            Else
                Work = Work & CInt((b And 240) / 16)
            End If
            Work = Work & (b And 15)
        Next i

        ' Try changing this:
        'TranslatePackedDecimal = CDbl(Work)
        'If pScale <> 0 Then
        '    TranslatePackedDecimal = TranslatePackedDecimal / (10 ^ pScale)
        'End If

        ' To
        Dim dblTemp As Double
        dblTemp = CDbl(Work)
        If pScale <> 0 Then
            dblTemp = dblTemp / (10 ^ pScale)
        End If

        TranslatePackedDecimal = dblTemp

    End Function
    Public Function VISMA_CollectorsRetriveClients(ByVal oERPDal As vbBabel.DAL, ByVal oClient As vbBabel.Client, ByVal sAccount As String, ByVal sCompanyID As String) As String
        Dim sMySQL As String
        Dim sMySQL2 As String
        Dim aSpecialQueryArray(,,) As String
        Dim bFound As Boolean
        Dim lCounter As Long
        Dim sReturnString As String = String.Empty

        'We need to retrieve which clients that utilies this BB-ClientNumber
        aSpecialQueryArray = GetERPDBInfo(sCompanyID, 5)

        sMySQL = ""
        sMySQL2 = ""
        For lCounter = 0 To UBound(aSpecialQueryArray, 3)
            If UCase(aSpecialQueryArray(0, 4, lCounter)) = "RETRIEVE_CLIENTS" Then
                sMySQL = aSpecialQueryArray(0, 0, lCounter)
            End If
            If UCase(aSpecialQueryArray(0, 4, lCounter)) = "RETRIEVE_CLIENTS2" Then
                sMySQL2 = aSpecialQueryArray(0, 0, lCounter)
            End If
        Next lCounter

        bFound = True
        If EmptyString(sMySQL) Then
            bFound = False
        End If
        If EmptyString(sMySQL2) Then
            bFound = False
        End If


        If Not bFound Then
            'Can't find the SQL to retrieve which clients are connected to this bankaccount for client oClient.Client_ID
            Err.Raise(19508, "LoadSQLs", LRS(19508, oClient.Client_ID.ToString))
            'As above
        Else
            sMySQL = Replace(sMySQL, "BB_AccountNo", sAccount, , , CompareMethod.Text)
            oERPDal.SQL = sMySQL
            If oERPDal.Reader_Execute() Then
                If oERPDal.Reader_HasRows Then
                    Do While oERPDal.Reader_ReadRecord
                        sReturnString = oERPDal.Reader_GetString("Client")
                    Loop
                Else
                    sMySQL2 = Replace(sMySQL2, "BB_AccountNo", sAccount, , , CompareMethod.Text)
                    oERPDal.SQL = sMySQL2

                    If oERPDal.Reader_Execute Then
                        If oERPDal.Reader_HasRows Then
                            Do While oERPDal.Reader_ReadRecord
                                sReturnString = oERPDal.Reader_GetString("Client")
                            Loop
                        Else
                            If Not RunTime() Then  'KJELLNOTRUNTIME
                                sReturnString = "280"
                            Else
                                Err.Raise(15708, "VISMA_CollectorsRetrieveClients", LRS(15708, sAccount))
                            End If
                        End If
                    Else
                        Err.Raise(45002, , oERPDal.ErrorMessage)
                    End If
                End If
            End If
        End If

        If EmptyString(sReturnString) Then
            Err.Raise(15708, "VISMA_CollectorsRetrieveClients", LRS(15708, sAccount))
        End If

        Return sReturnString

    End Function
    Public Sub Validate_VismaCollectorsPostingBabelFiles(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal bShowErrormessage As Boolean, Optional ByVal bCalledFromExport As Boolean = False)
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment

        Try
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        Validate_VismaCollectorsPostingPayment(oPayment, bShowErrormessage, bCalledFromExport)
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception
            Throw New Exception("Function: Validate_VismaCollectorsPostingBabelFiles" & vbCrLf & ex.Message)
        End Try

    End Sub
    Public Function Validate_VismaCollectorsPostingPayment(ByVal oPayment As vbBabel.Payment, ByVal bShowErrormessage As Boolean, Optional ByVal bCalledFromExport As Boolean = False) As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim oFreetext As vbBabel.Freetext
        Dim sClientNo As String
        Dim bAddClientToGLPosting As Boolean
        Dim sCustomerNo As String
        Dim bAddCustomerToGLPosting As Boolean
        Dim sErrorText As String
        Dim sAccountReceiver As String
        Dim sMessage As String
        Dim bReturnValue As Boolean = True
        Dim bAtLeastOneINKPayment As Boolean = False
        Dim bAllINKPayments As Boolean = True

        Try

            sClientNo = String.Empty
            bAddClientToGLPosting = False
            sCustomerNo = String.Empty
            bAddCustomerToGLPosting = False
            bAtLeastOneINKPayment = False
            bAllINKPayments = True
            For Each oInvoice In oPayment.Invoices
                If oInvoice.MATCH_Final Then
                    If oInvoice.MATCH_Matched Then
                        'It should not be � backpayment
                        If oInvoice.MATCH_ID <> "RET" Then
                            If oInvoice.MATCH_ID = "INK" Then
                                bAtLeastOneINKPayment = True
                            Else
                                bAllINKPayments = False
                            End If
                            '19.05.2015 - Added next IF
                            If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                                If EmptyString(oInvoice.InvoiceNo) Then
                                    sErrorText = CreateStandardErrorText(oPayment, oInvoice)
                                    sErrorText = "Du kan ikke postere en post som ikke har et fakturanummer." & vbCrLf & vbCrLf & sErrorText
                                    Throw New System.Exception(sErrorText)
                                End If
                            End If
                            If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Or oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                                If sClientNo = String.Empty Then
                                    sClientNo = oInvoice.MATCH_ClientNo
                                    sCustomerNo = oInvoice.CustomerNo
                                Else
                                    If sClientNo = oInvoice.MATCH_ClientNo Then
                                        'OK
                                    Else
                                        'Poted on two different client. THIS IS NOT ALLOWED
                                        sErrorText = CreateStandardErrorText(oPayment, oInvoice)
                                        sErrorText = "Innbetalingen er krysset mot to forskjellige klienter. Det er ikke en gyldig f�ring." & vbCrLf & _
                                        "Klient 1: " & sClientNo & vbCrLf & "Klient 2: " & oInvoice.MATCH_ClientNo & vbCrLf & vbCrLf & sErrorText
                                        Throw New System.Exception(sErrorText)
                                    End If
                                End If
                                If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                                    If oInvoice.MON_InvoiceAmount < 0 Then
                                        'It is not alowed to post a negative OnAccount amount
                                        sErrorText = CreateStandardErrorText(oPayment, oInvoice)
                                        sErrorText = "Det er ikke mulig � postere en negative aKonto f�ring." & vbCrLf & vbCrLf & sErrorText
                                        Throw New System.Exception(sErrorText)
                                    End If
                                End If
                            ElseIf oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                                If EmptyString(oInvoice.MATCH_ClientNo) Then
                                    If sClientNo = String.Empty Then
                                        bAddClientToGLPosting = True
                                    Else
                                        oInvoice.MATCH_ClientNo = sClientNo
                                    End If
                                End If
                                If EmptyString(oInvoice.CustomerNo) Then
                                    If sCustomerNo = String.Empty Then
                                        bAddCustomerToGLPosting = True
                                    Else
                                        oInvoice.CustomerNo = sCustomerNo
                                    End If
                                End If
                            End If
                        Else
                            If oInvoice.MON_InvoiceAmount < 0 Then
                                'It is not alowed to post a negative OnAccount amount
                                sErrorText = CreateStandardErrorText(oPayment, oInvoice)
                                sErrorText = "Det er ikke mulig � utbetale en negativ betaling." & vbCrLf & vbCrLf & sErrorText
                                Throw New System.Exception(sErrorText)
                            End If
                            'Check if a valid accountnumber is stated
                            sAccountReceiver = String.Empty
                            If Not EmptyString(oInvoice.MyField) Then
                                sAccountReceiver = Trim(oInvoice.MyField)
                            Else
                                sAccountReceiver = Trim(oPayment.E_Account)
                            End If
                            If Len(sAccountReceiver) <> 11 Or Mid(sAccountReceiver, 5, 1) = "9" Or Not CheckAccountNoNOR(sAccountReceiver) Then
                                'The stated accountnumber is not valid
                                sErrorText = CreateStandardErrorText(oPayment, oInvoice)
                                sErrorText = "Mottakers kontonummer som er angitt er ikke gyldig." _
                                & vbCrLf & "Nummeret m� v�re 11 posisjoner langt, 5. posisjon ulik 9 og med gydig kontrollsiffer." & vbCrLf & vbCrLf & sErrorText
                                Throw New System.Exception(sErrorText)
                            End If
                            sMessage = String.Empty
                            For Each oFreetext In oInvoice.Freetexts
                                If oFreetext.Qualifier = 4 Then
                                    sMessage = sMessage & Trim(oFreetext.Text)
                                End If
                            Next oFreetext
                            If EmptyString(sMessage) Then
                                sErrorText = CreateStandardErrorText(oPayment, oInvoice)
                                sErrorText = "Det er ikke angitt noen meldingstekst p� tilbakebetalingen." _
                                 & vbCrLf & vbCrLf & sErrorText
                                Throw New System.Exception(sErrorText)
                            End If
                            If oPayment.MON_InvoiceCurrency <> "NOK" Then
                                sErrorText = CreateStandardErrorText(oPayment, oInvoice)
                                sErrorText = "Ved tilbakebetaling m� valutakoden v�re NOK." _
                                 & vbCrLf & vbCrLf & sErrorText
                                Throw New System.Exception(sErrorText)
                            End If
                        End If 'If oInvoice.MATCH_ID <> "RET" Then
                    End If
                End If
            Next oInvoice

            If bAtLeastOneINKPayment Then
                If bAllINKPayments Then
                    'OK - all invoices are marked as INK
                Else
                    'Raise an error. Either or all or none paymeents must be markes as INK (Collecting/Inkasso)
                    sErrorText = CreateStandardErrorText(oPayment)
                    sErrorText = "Det finnes minst en postering mot en 'vanlig' faktura og minst en merket som 'Overf�res til inkasso'." _
                     & vbCrLf & "Ved overf�ring til inkasso m� hele det betalte bel�pet overf�res." _
                     & vbCrLf & vbCrLf & sErrorText
                    Throw New System.Exception(sErrorText)
                End If
            End If

            If bAddClientToGLPosting Then
                For Each oInvoice In oPayment.Invoices
                    If oInvoice.MATCH_Final Then
                        If oInvoice.MATCH_Matched Then
                            'It should not be � backpayment
                            If oInvoice.MATCH_ID <> "RET" Then
                                If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                                    If EmptyString(oInvoice.MATCH_ClientNo) Then
                                        If sClientNo = String.Empty Then
                                            'Poted on two different client. THIS IS NOT ALLOWED
                                            sErrorText = CreateStandardErrorText(oPayment, oInvoice)
                                            sErrorText = "Finner ingen klient � f�re hoveboksposteringen(e) p�." & vbCrLf & vbCrLf & sErrorText
                                            Throw New System.Exception(sErrorText)
                                        Else
                                            oInvoice.MATCH_ClientNo = sClientNo
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next oInvoice
            End If
            If bAddCustomerToGLPosting Then
                For Each oInvoice In oPayment.Invoices
                    If oInvoice.MATCH_Final Then
                        If oInvoice.MATCH_Matched Then
                            'It should not be � backpayment
                            If oInvoice.MATCH_ID <> "RET" Then
                                If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                                    If EmptyString(oInvoice.CustomerNo) Then
                                        If sCustomerNo = String.Empty Then
                                            ''Poted on two different client. THIS IS NOT ALLOWED
                                            'sErrorText = CreateStandardErrorText(oPayment, oInvoice)
                                            'sErrorText = "Finner ingen klient � f�re hoveboksposteringen(e) p�." & vbCrLf & vbCrLf & sErrorText
                                            'Throw New System.Exception(sErrorText)
                                        Else
                                            oInvoice.CustomerNo = sCustomerNo
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next oInvoice
            End If

            Return True

        Catch ex As Exception
            If bShowErrormessage Then
                Throw New Exception("Function: Validate_VismaCollectorsPostingPayment" & vbCrLf & ex.Message)
            Else
                MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "BabelBank kan ikke avstemme betalingen!")
                Return False
            End If

        End Try

    End Function
    Friend Function VismaCollectors_AddCurrencyAmounts(ByRef oLocalPayment As vbBabel.Payment) As Boolean
        Dim oLocalInvoice As vbBabel.Invoice
        Dim nMatchedAccountsReceivableAmount As Double = 0
        Dim nMatchedAccountsReceivableAmountInCurrency As Double
        Dim nCalculatedExchangeRate As Double = 0
        Dim iCounter As Integer = 0

        If oLocalPayment.MON_OriginallyPaidCurrency <> oLocalPayment.MON_InvoiceCurrency Then
            If oLocalPayment.MON_OriginallyPaidCurrency.Length = 3 Then
                iCounter = 0
                nMatchedAccountsReceivableAmount = 0
                For Each oLocalInvoice In oLocalPayment.Invoices
                    If oLocalInvoice.MATCH_Final Then
                        If oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Or oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                            iCounter = iCounter + 1
                            nMatchedAccountsReceivableAmount = nMatchedAccountsReceivableAmount + oLocalInvoice.MON_InvoiceAmount
                        End If
                    End If
                Next oLocalInvoice
            End If
        End If
        If iCounter = 1 Then
            For Each oLocalInvoice In oLocalPayment.Invoices
                If oLocalInvoice.MATCH_Final Then
                    If oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Or oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                        oLocalInvoice.MON_TransferredAmount = oLocalPayment.MON_OriginallyPaidAmount
                    End If
                End If
            Next oLocalInvoice
        ElseIf iCounter > 1 Then
            nMatchedAccountsReceivableAmountInCurrency = 0
            nCalculatedExchangeRate = Math.Round(oLocalPayment.MON_OriginallyPaidAmount / nMatchedAccountsReceivableAmount, 4)
            For Each oLocalInvoice In oLocalPayment.Invoices
                If oLocalInvoice.MATCH_Final Then
                    If oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Or oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                        oLocalInvoice.MON_TransferredAmount = Math.Round(oLocalInvoice.MON_InvoiceAmount * nCalculatedExchangeRate, 0)
                        nMatchedAccountsReceivableAmountInCurrency = nMatchedAccountsReceivableAmountInCurrency + oLocalInvoice.MON_TransferredAmount
                    End If
                End If
            Next oLocalInvoice
            If IsEqualAmount(oLocalPayment.MON_OriginallyPaidAmount, nMatchedAccountsReceivableAmountInCurrency) Then
                'OK, the whole OriginalAmount is posted
            Else
                'Correct the first Invoice with the deviation
                For Each oLocalInvoice In oLocalPayment.Invoices
                    If oLocalInvoice.MATCH_Final Then
                        If oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Or oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                            oLocalInvoice.MON_TransferredAmount = oLocalInvoice.MON_TransferredAmount + (oLocalPayment.MON_OriginallyPaidAmount - nMatchedAccountsReceivableAmountInCurrency)
                            Exit For
                        End If
                    End If
                Next oLocalInvoice
            End If

        Else
            'Nothing to do

        End If

        'Old code
        'If oLocalPayment.MON_OriginallyPaidAmount <> oLocalPayment.MON_InvoiceCurrency Then
        '    iCounter = 0
        '    nMatchedAccountsReceivableAmount = 0
        '    For Each oLocalInvoice In oLocalPayment.Invoices
        '        If oLocalInvoice.MATCH_Final Then
        '            If oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Or oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
        '                iCounter = iCounter + 1
        '                nMatchedAccountsReceivableAmount = nMatchedAccountsReceivableAmount + oLocalInvoice.MON_InvoiceAmount
        '            End If
        '        End If
        '    Next oLocalInvoice
        'End If
        'If iCounter = 1 Then
        '    For Each oLocalInvoice In oLocalPayment.Invoices
        '        If oLocalInvoice.MATCH_Final Then
        '            If oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Or oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
        '                oLocalInvoice.MON_TransferredAmount = oLocalPayment.MON_OriginallyPaidAmount
        '            End If
        '        End If
        '    Next oLocalInvoice
        'ElseIf iCounter > 1 Then
        '    nCalculatedExchangeRate = Math.Round(oLocalPayment.MON_OriginallyPaidAmount / nMatchedAccountsReceivableAmount, 4)
        '    For Each oLocalInvoice In oLocalPayment.Invoices
        '        If oLocalInvoice.MATCH_Final Then
        '            If oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Or oLocalInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
        '                oLocalInvoice.MON_TransferredAmount = oLocalPayment.MON_InvoiceAmount * nCalculatedExchangeRate
        '                nMatchedAccountsReceivableAmountInCurrency = nMatchedAccountsReceivableAmountInCurrency + oLocalInvoice.MON_TransferredAmount
        '            End If
        '        End If
        '    Next oLocalInvoice

        'Else
        '    'Nothing to do

        'End If

    End Function
    Public Function CreateStandardErrorText(ByVal oPayment As vbBabel.Payment, Optional ByVal oInvoice As vbBabel.Invoice = Nothing) As String
        Dim sReturnValue As String

        sReturnValue = LRSCommon(20053) & oPayment.E_Name & vbCrLf
        sReturnValue = sReturnValue & LRSCommon(20054) & oPayment.E_Account & vbCrLf
        sReturnValue = sReturnValue & LRSCommon(41021) & " " & oPayment.MON_InvoiceCurrency & " " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, ".", ",") & vbCrLf
        sReturnValue = sReturnValue & LRSCommon(20055) & oPayment.I_Account
        If Not oInvoice Is Nothing Then
            sReturnValue = sReturnValue & vbCrLf & vbCrLf
            sReturnValue = sReturnValue & LRSCommon(20056) & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, ".", ",")
        End If

        Return sReturnValue

    End Function

    Public Function SQLToRetrievePaymentFromDatabase() As String
        Dim sMySQL As String

        sMySQL = "SELECT Payment_ID, StatusCode, StatusText, Cancel, DATE_Payment, MON_TransferredAmount, AmountSetInvoice, AmountSetTransferred, "
        sMySQL = sMySQL & "MON_TransferCurrency, MON_OriginallyPaidAmount, MON_OriginallyPaidCurrency, REF_Own, REF_Bank1, REF_Bank2, "
        sMySQL = sMySQL & "FormatType, E_Account, E_AccountPrefix, E_AccountSuffix, E_Name, E_Adr1, E_Adr2, E_Adr3, e_OrgNo, "
        sMySQL = sMySQL & "E_Zip, E_City, E_CountryCode, DATE_Value, Priority, I_Client, Text_E_Statement, Text_I_Statement, "
        sMySQL = sMySQL & "Cheque, ToOwnAccount, PayCode, PayType, I_Account, I_Name, I_Adr1, I_Adr2, I_Adr3, I_Zip, I_City, "
        sMySQL = sMySQL & "I_CountryCode, Structured, VB_ClientNo, ExtraD1, ExtraD2, ExtraD3, ExtraD4, ExtraD5, "
        sMySQL = sMySQL & "MON_LocalAmount, MON_LocalCurrency, MON_AccountAmount, MON_AccountCurrency, MON_InvoiceAmount, MON_InvoiceCurrency, "
        sMySQL = sMySQL & "MON_EuroAmount, MON_AccountExchRate, MON_LocalExchRate, MON_LocalExchRateSet, MON_EuroExchRate, MON_ChargesAmount, "
        sMySQL = sMySQL & "MON_ChargesCurrency, MON_ChargeMeDomestic, MON_ChargeMeAbroad, ERA_ExchRateAgreed, ERA_DealMadeWith, "
        sMySQL = sMySQL & "FRW_ForwardContractRate, FRW_ForwardContractNo, NOTI_NotificationMessageToBank, NOTI_NotificationType, "
        sMySQL = sMySQL & "NOTI_NotificationParty, NOTI_NotificationAttention, NOTI_NotificationIdent, BANK_SWIFTCode, BANK_BranchNo, "
        sMySQL = sMySQL & "BANK_Name, BANK_Adr1, BANK_Adr2, BANK_Adr3, BANK_CountryCode, BANK_SWIFTCodeCorrBank, "
        sMySQL = sMySQL & "ExtraI1, ExtraI2, ExtraI3, ImportFormat, FilenameOut_ID, StatusSplittedFreetext, "
        sMySQL = sMySQL & "Exported, VoucherNo, VoucherType, MATCH_Matched, Unique_PaymentID, Unique_ERPID, MATCH_MatchingIDAgainstObservationAccount, "
        sMySQL = sMySQL & "UserID, HowMatched, MATCH_UseoriginalAmountInMatching, SpecialMark, DoNotShow, BANK_BranchType, "
        sMySQL = sMySQL & "DnBNORTBIPayType, REF_EndToEnd, PurposeCode, UltimateE_Name, UltimateI_Name, CategoryPurposeCode, Match_Note, Ref_Own_BabelBankGenerated, IsSEPA, ToSpecialReport, PaymentCounter, "
        sMySQL = sMySQL & "MON_ChargesAmountAbroad, DATE_Initiated, "
        sMySQL = sMySQL & "UltimateE_Adr1, UltimateE_Adr2, UltimateE_Adr3, UltimateE_City, UltimateE_Zip, UltimateE_CountryCode, "
        sMySQL = sMySQL & "UltimateI_Adr1, UltimateI_Adr2, UltimateI_Adr3, UltimateI_City, UltimateI_Zip, UltimateI_CountryCode, "
        sMySQL = sMySQL & "E_Social_Security_Number "

        Return sMySQL

    End Function
    Public Function TranslateFromNorgesBankCodesToRiksbankCodes(ByVal sNBCode As String) As String
        ' Translate a NorgesBank declarationcode into a Swedish Riksbank declarationcode
        ' Pr 22.07.2015 only a limited set of codes are translated
        ' We will need to expand this function
        Dim sReturn As String = "101"

        ' If we have 3-digitcodes sent into this function, do NOT translate, as we then assume that we already has a Swedish declarationcode:
        If sNBCode.Length = 3 Then
            sReturn = sNBCode
        Else
            Select Case sNBCode
                Case "14"
                    sReturn = "101"
                Case "29"
                    sReturn = "122"
                Case "81"
                    sReturn = "470"
            End Select
        End If
        TranslateFromNorgesBankCodesToRiksbankCodes = sReturn
    End Function
    Public Function TranslateFromRiksbankCodesToNorgesBankCodes(ByVal sRBCode As String) As String
        ' 24.01.2020
        ' Translate a Swedish declarationcode into a NorgesBank declarationcode
        ' Only a limited set of codes are translated
        ' We will need to expand this function
        Dim sReturn As String = "14"

        ' If we have 2-digitcodes sent into this function, do NOT translate, as we then assume that we already has a Swedish declarationcode:
        If sRBCode.Length = 2 Then
            sReturn = sRBCode
        Else
            Select Case sRBCode
                Case "101"
                    sReturn = "14"
                Case "122"
                    sReturn = "29"
                Case "470"
                    sReturn = "81"
            End Select
        End If
        TranslateFromRiksbankCodesToNorgesBankCodes = sReturn
    End Function
    Public Function IsDnB(ByVal eBank As vbBabel.BabelFiles.Bank) As Boolean
        Dim bReturnValue As Boolean = False

        If eBank = vbBabel.BabelFiles.Bank.DnB Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.DNB_FI Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.DNB_US Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.DnBNOR_D Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.DnBNOR_SE Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.DNB_DK Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.DNB_GB Then
            bReturnValue = True
        End If

        Return bReturnValue

    End Function
    Public Function IsCharges(ByVal sPayCode As String) As Boolean
        Dim bReturnValue As Boolean = False

        If sPayCode = "648" Then
            bReturnValue = True
        End If

        Return bReturnValue

    End Function


    Public Function IsDanskeBank(ByVal eBank As vbBabel.BabelFiles.Bank) As Boolean
        Dim bReturnValue As Boolean = False

        If eBank = vbBabel.BabelFiles.Bank.Danske_Bank_NO Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.Danske_Bank_dk Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.Danske_Bank_SF Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.DanskeBank_SE Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.Danske_Bank_UK Then
            bReturnValue = True

        End If

        Return bReturnValue

    End Function
    Public Function IsHandelsbanken(ByVal eBank As vbBabel.BabelFiles.Bank) As Boolean
        Dim bReturnValue As Boolean = False

        If eBank = vbBabel.BabelFiles.Bank.Handelsbanken Then
            bReturnValue = True
        End If

        Return bReturnValue

    End Function

    Public Function IsNordea(ByVal eBank As vbBabel.BabelFiles.Bank) As Boolean
        Dim bReturnValue As Boolean = False

        If eBank = vbBabel.BabelFiles.Bank.Nordea_CorporateAccess Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.Nordea_DK Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.Nordea_FI Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.Nordea_NO Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.Nordea_SE Then
            bReturnValue = True
            '26.10.2016 - So far excluded eGateway,will probably cease to exist anyway 
            'ElseIf eBank = vbBabel.BabelFiles.Bank.Nordea_eGateway Then
            '    bReturnValue = True
        End If

        Return bReturnValue

    End Function
    Public Function IsSEB(ByVal eBank As vbBabel.BabelFiles.Bank) As Boolean
        Dim bReturnValue As Boolean = False

        If eBank = vbBabel.BabelFiles.Bank.SEB_DK Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.SEB_Finland Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.SEB_NO Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.SEB_SE Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.SEB_PL Then
            bReturnValue = True
        End If

        Return bReturnValue

    End Function
    Public Function IsNorwegianBank(ByVal eBank As vbBabel.BabelFiles.Bank) As Boolean
        Dim bReturnValue As Boolean = False

        If eBank = vbBabel.BabelFiles.Bank.DnB Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.SEB_NO Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.Danske_Bank_NO Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.A_Norwegian_bank Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.BBS Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.Handelsbanken Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.Nordea_NO Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.Sparebank1 Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.Sparebanken_Midt_Norge Then
            bReturnValue = True
        ElseIf eBank = vbBabel.BabelFiles.Bank.BITS_NorwegianStandard Then
            bReturnValue = True
        End If

        Return bReturnValue

    End Function
    Public Function IsNordicCountry(ByVal eCountry As vbBabel.BabelFiles.CountryName) As Boolean

        Dim bReturnValue As Boolean = False

        If eCountry = vbBabel.BabelFiles.CountryName.Denmark Then
            bReturnValue = True
        ElseIf eCountry = vbBabel.BabelFiles.CountryName.Finland Then
            bReturnValue = True
        ElseIf eCountry = vbBabel.BabelFiles.CountryName.Norway Then
            bReturnValue = True
        ElseIf eCountry = vbBabel.BabelFiles.CountryName.Sweden Then
            bReturnValue = True
        ElseIf eCountry = vbBabel.BabelFiles.CountryName.Nordic Then
            bReturnValue = True
        End If

        Return bReturnValue

    End Function

    Public Function ValutaRegistreringsTekst(ByVal sCode As String, ByVal sCountryCode As String) As String
        ' Returns a standardtext from Toll & Avgiftsdirektoratet (NO) or
        ' Sveriges Riksbank (SE) for each different registrationscode

        Dim sText As String = ""
        If sCountryCode = "NO" Then

            Select Case sCode
                Case "14"
                    sText = "Kj�p/salg av varer"
                Case "26"
                    sText = "Leie"
                Case "29"
                    sText = "Annet kj�p/salg av tjenester"
                Case "31"
                    sText = "Renter"
                Case "35"
                    sText = "Utbytte.Omfatter utbytte p� all eier-/egenkapital. Omfatter ogs� driftstilskudd til utenlandske konsernselskaper"
                Case "38"
                    sText = "Annen kapitalavkastning"
                Case "41"
                    sText = "Kj�p/salg av fast eiendom og aktiverte rettigheter i utlandet"
                Case "43"
                    sText = "Direkteinvestering i aksjer mm."
                Case "45"
                    sText = "Direkteinvestering i annen kapital"
                Case "51"
                    sText = "Portef�ljeinvestering i aksjer og verdipapirfondsandeler"
                Case "52"
                    sText = "Portef�ljeinvestering i obligasjoner og sertifikater"
                Case "53"
                    sText = "Portef�ljeinvestering i derivater"
                Case "71"
                    sText = "Livsforsikring/pensjon"
                Case "79"
                    sText = "Andre finansinvesteringer"
                Case "81"
                    sText = "L�nn"
                Case "82"
                    sText = "Arv, gave med mere"
            End Select
        ElseIf sCountryCode = "SE" Then
            ' 18.07.2016 added list of Swedish codes and texts
            Select Case sCode
                ' Varor 
                Case "101"
                    sText = "Varuexport/import "
                Case "122"
                    sText = "Vara som ej passerat sv gr�ns, k�p av varor i utlandet i avsikt att vidaref�rs�lja dessa. "
                Case "473"
                    sText = "Provision vid varuf�rmedling "

                    'Leasing � hyra av kapitalvara 
                Case "701"
                    sText = "Finansiell leasing "
                Case "401"
                    sText = "Operationell leasing "

                    'Frakter i samband med export/import av varor 
                Case "130"
                    sText = "Importfrakt "
                Case "131"
                    sText = "Exportfrakt "

                    'Frakter vid transitohandel 
                Case "140"
                    sText = "Fartygsfrakt "
                Case "141"
                    sText = "Flygfrakt "
                Case "142"
                    sText = "J�rnv�gsfrakt "
                Case "143"
                    sText = "V�gfrakt "
                Case "150"
                    sText = "�vriga transportmedel "

                    'Charter/hyra av transportmedel med bes�ttning i samband med varufrakter 
                Case "160"
                    sText = "Fartyg "
                Case "161"
                    sText = "Flygplan "
                Case "162"
                    sText = "Landfordon "

                    'Varor och tj�nster i samband med frakter 
                Case "163"
                    sText = "Bunkring och proviantering "
                Case "173"
                    sText = "Lagrings-, speditions- och transiteringskostnader mm "

                    'Personbefordran � resor 
                Case "200"
                    sText = "B�t "
                Case "201"
                    sText = "Flyg "
                Case "202"
                    sText = "T�g och buss "

                    'Resor, �vrigt 
                Case "221"
                    sText = "Kontokortsbetalningar "
                Case "223"
                    sText = "�vriga reseutgifter "

                    'F�rs�krings, bank- och finansiella tj�nster 
                Case "300"
                    sText = "Fraktf�rs�kring vid varuimport/export "
                Case "301"
                    sText = "�vrigt fraktf�rs�kring "
                Case "302"
                    sText = "Liv- och pensionsf�rs�kring "
                Case "303"
                    sText = "�vrig direktf�rs�kring "
                Case "310"
                    sText = "�terf�rs�kring "
                Case "321"
                    sText = "Tj�nster i samband med f�rs�kringar "
                Case "331"
                    sText = "Andra finansiella f�rmedlingstj�nster "
                    'Post- och teletj�nster 
                Case "402"
                    sText = "Postbefordran, kurirtj�nster "
                Case "403"
                    sText = "Telekommunikation, �verf�ring via tele-,datan�t och satellit inkl radio och TV-s�ndning "

                    'Data- och informationstj�nster 
                Case "410"
                    sText = "Datatj�nster "
                Case "411"
                    sText = "Informationstj�nster "

                    'Byggnads- och installationstj�nster 
                Case "412"
                    sText = "I utlandet "
                Case "413"
                    sText = "I Sverige "

                    'Licenser och andra r�ttigheter 
                Case "423"
                    sText = "Ers�ttning/royalty i samband med uppl�telse av r�ttigheter "
                Case "723"
                    sText = "�verl�telse � k�p/f�rs�ljning � av r�ttigheter "

                    'L�ner 
                Case "470"
                    sText = "L�ner och ers�ttning till egna anst�llda "

                    '�vriga tj�nster 
                Case "430"
                    sText = "Juridiska tj�nster "
                Case "431"
                    sText = "Bokf�ring, revision, administration "
                Case "440"
                    sText = "Reklam och marknadsunders�kning "
                Case "441"
                    sText = "Forskning och utveckling "
                Case "442"
                    sText = "Arkitekt-, ingenj�rs- och andra tekniska tj�nster "
                Case "450"
                    sText = "Jord- och skogsbruk "
                Case "451"
                    sText = "Mineral, olja och gas "
                Case "452"
                    sText = "Milj�tj�nster "
                Case "460"
                    sText = "Produktion av film, ljudinspeln, radio- och TVprogram m.m. "
                Case "461"
                    sText = "�vrig kultur, rekreation "
                Case "462"
                    sText = "�vriga tj�nster "
                Case "473"
                    sText = "Provision vid varuf�rmedling "

                    'Transfereringar � bist�nd och bidrag 
                Case "510"
                    sText = "U-l�nder "
                Case "520"
                    sText = "Europeiska Unionen "
                Case "540"
                    sText = "�vrig intl organisation "

                    '�vriga transfereringar 
                Case "550"
                    sText = "Arbetsinkomst intj�nad utanf�r hemlandet "
                Case "551"
                    sText = "Emigration/immigration "
                Case "552"
                    sText = "Merv�rdesskatt "
                Case "553"
                    sText = "Arv "
                Case "560"
                    sText = "�vriga transfereringar "

                    'Direkt investering i Sverige (utl �gare) 
                Case "600"
                    sText = "Aktier, andelar mm "
                Case "601"
                    sText = "L�netransaktion inom koncern "
                Case "801"
                    sText = "R�nta p� l�n inom koncern "
                Case "900"
                    sText = "Utdelning till utl �gare "

                    'Direkt investering i utlandet (svensk �gare) 
                Case "602"
                    sText = "Aktier, andelar, aktie�gartillskott mm. "
                Case "603"
                    sText = "L�netransaktion inom koncern "
                Case "803"
                    sText = "R�nta p� l�n inom koncern "
                Case "902"
                    sText = "Utdelning fr�n direktinvesteringsf�retag i utlandet"

                    'Svenska aktier 
                Case "620"
                    sText = "K�p och f�rs�ljning "
                Case "910"
                    sText = "Utdelning "

                    'Utl�ndska aktier 
                Case "622"
                    sText = "K�p och f�rs�ljning "
                Case "912"
                    sText = "Utdelning "

                    'R�nteb�rande v�rdepapper, utgivna av svensk 
                Case "631"
                    sText = "K�p och f�rs�ljning "
                Case "831"
                    sText = "R�nta "

                    'R�nteb�rande v�rdepapper, utgivna av utl�nning 
                Case "633"
                    sText = "K�p och f�rs�ljning "
                Case "833"
                    sText = "R�nta "

                    'Finansiella derivat 
                Case "670"
                    sText = "Optionspremier och marginalbetalningar f�r finansiella derivat samt realiserade v�rdet av derivatkontrakt vid l�sen/f�rfall "

                    '�vriga l�n, fr�n utlandet 
                Case "681"
                    sText = "Uppl�ning/amortering "
                Case "881"
                    sText = "R�nta "

                    '�vriga l�n, till utlandet 
                Case "683"
                    sText = "Uppl�ning/amortering "
                Case "883"
                    sText = "R�nta "

                    'Finansiell leasing 
                Case "701"
                    sText = "L�ngtidshyra av kapitalvara "

                    'K�p och f�rs�ljning av bostad f�r eget bruk 
                Case "710"
                    sText = "Bostad i Sverige "
                Case "712"
                    sText = "Bostad i utlandet "

                    'R�ttigheter - k�p och f�rs�ljning 
                Case "723"
                    sText = "Patent, licenser, franchising mm "

                    'Konton 
                Case "751"
                    sText = "Svenskt f�retags konto f�r utl�ndsk mot parts r�kning. "

                    '�vriga kapitaltransaktioner 
                Case "793"
                    sText = "�vriga kapitaltransaktioner "

                    'R�ntor 
                Case "801"
                    sText = "R�nta p� l�n inom koncernen (direkt investering i Sverige) "
                Case "803"
                    sText = "R�nta p� l�n inom koncernen (direkt investering i utlandet) "
                Case "831"
                    sText = "R�nta p� r�nteb�rande v�rdepapper utgivna av svensk "
                Case "833"
                    sText = "R�nta p� v�rdepapper utgivna av utl�nning "
                Case "881"
                    sText = "R�nta p� �vriga l�n fr�n utlandet "
                Case "883"
                    sText = "R�nta p� �vriga l�n till utlandet "
                Case "893"
                    sText = "�vrig r�nta "

                    'Utdelningar 
                Case "900"
                    sText = "Utdelning till utl�ndsk �gare p� direkt investering i Sverige "
                Case "902"
                    sText = "Utdelning fr�n direktinvesteringsf�retag i utlandet "
                Case "910"
                    sText = "Utdelning p� svenska aktier "
                Case "912"
                    sText = "Utdelning p� utl�ndska aktier "

                    '�vriga koder. V�xling 
                Case "940"
                    sText = "V�xling i utlandet av belopp som f�rst f�rts ut, och efter v�xling �ter tagits hem Nettning, betalning av nettobelopp efter kvittning"
                Case Else
                    sText = "Varuexport/import"
            End Select

        End If

        ValutaRegistreringsTekst = sText
    End Function
    Public Function Visma_AlphaNumString(ByVal sInstring As String, ByVal iNoOfSignsToReturn As Integer) As String
        ' take inputpart in digits, and return as alphanumeric with iNoOfSignsToReturn number of signs
        Dim sReturn As String = ""
        sReturn = AlphaEncode(sInstring)

        Visma_AlphaNumString = sReturn.PadLeft(iNoOfSignsToReturn, "0")

    End Function
    Private Function AlphaEncode(ByRef Value As String) As String
        'Defines the digit sequence for the alphadecimal numbering
        Dim AlphaDecimalCharacters As Char() = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray
        Dim Radix As Long = 36

        Dim ResultValue As String = String.Empty
        Dim DecimalValue As Long = Value
        Dim Negative As Boolean = False

        If DecimalValue < 0 Then
            Negative = True
            DecimalValue = Math.Abs(DecimalValue)
        End If

        If (DecimalValue < Radix) Then
            ResultValue = AlphaDecimalCharacters(Convert.ToInt32(DecimalValue))
        Else
            While (DecimalValue <> 0)
                Dim Remainder As Long = DecimalValue Mod Radix
                ResultValue = AlphaDecimalCharacters(Convert.ToInt32(Remainder)) & ResultValue
                DecimalValue = Convert.ToInt64(Math.Truncate(DecimalValue / Radix))
            End While
        End If

        If Negative Then ResultValue = "-" & ResultValue

        Return ResultValue
    End Function
    ''' <summary>
    ''' Decodes an alphadecimal (base 36) value to its equivalent decimal (base 10) value.
    ''' Supports any whole number up to a 64-bit signed value.
    ''' Filters out commas, plus sign, or dollar sign.
    ''' </summary>
    ''' <param name="AlphaDecimal">Base 36 whole number string value</param>
    ''' <returns>Base 10 decimal equvalent whole number</returns>
    ''' <remarks>From Visual Studio Magazine On VB column by Joe Kunk</remarks>
    Public Function AlphaDecode(ByVal AlphaDecimal As String) As Long
        Dim ReturnValue As Long = 0
        Dim Negative As Boolean = False
        Dim AlphaDecimalCharacters As Char() = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray
        Dim Radix As Long = 36

        'Make sure all values have only uppercase letters to properly
        'locate them in the AlphaDecimalCharacters array
        Dim AlphaDecimalValue As String = AlphaDecimal.ToUpper.Trim

        'Handle negative number
        If (AlphaDecimalValue.Contains("-")) AndAlso (AlphaDecimalValue.Length > 1) Then
            Negative = True
        End If

        'Clean valid non-numeric characters from input value
        If AlphaDecimalValue.IndexOfAny("$,+-".ToCharArray) > -1 Then
            AlphaDecimalValue = CleanValue(AlphaDecimal)
        End If

        'Trim leading zero from input value for efficiency
        AlphaDecimalValue = TrimLeadingZeros(AlphaDecimalValue)

        'For clarity, more steps than I would do in a production application
        'Decode one digit at a time from left to right
        For i As Integer = 0 To AlphaDecimalValue.Length - 1
            'Get the digit
            Dim Digit As Char = Convert.ToChar(AlphaDecimalValue.Substring(i, 1))

            'Determine which position it is in the array defined at top of class
            Dim index As Integer = Array.IndexOf(AlphaDecimalCharacters, Digit)

            'The leftmost digit of the value is string positon 0
            'PlaceValue is its proper "power of 36" value based on its position
            Dim PlaceValue As Long = AlphaDecimalValue.Length - i - 1


            'Numeric value of the digits is its position in AlphaDecimalCharcters array
            'times 36 raised to the PlaceValue power.
            Dim DigitValue As Long = Convert.ToInt64(index * (Radix ^ PlaceValue))

            'Add its numeric value to the accumulated total
            ReturnValue += DigitValue
        Next

        'Set the response negative if came in negative
        If Negative Then ReturnValue = ReturnValue * -1

        Return ReturnValue
    End Function
    ''' <summary>
    ''' Removes any leading zeros from the numeric value prior to conversion
    ''' </summary>
    ''' <param name="Value"></param>
    ''' <returns>Eqivalent value without leading zeros</returns>
    ''' <remarks></remarks>
    Private Function TrimLeadingZeros(ByVal Value As String) As String
        Dim ResultValue As String = Value
        While (ResultValue.Length > 1) AndAlso (Left(ResultValue, 0) = "0")
            ResultValue = Mid(ResultValue, 1)
        End While
        Return ResultValue
    End Function
    Public Function CleanValue(ByVal DecimalValue As String) As String
        Dim ResultValue As String = DecimalValue

        If (ResultValue.IndexOfAny("$,+-".ToCharArray) > -1) Then
            ResultValue = ResultValue.Replace("$", "")
            ResultValue = ResultValue.Replace(",", "")
            ResultValue = ResultValue.Replace("+", "")
            ResultValue = ResultValue.Replace("-", "")
        End If
        Return ResultValue
    End Function
    Public Function ClientSavesData(ByVal oFileSetups As vbBabel.FileSetups) As Boolean
        ' 08.09.2016
        ' Check if clients (at least one client) is set up to save data
        ' -------------------------------------------------------------
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim bSaveData As Boolean = False

        For Each oFilesetup In oFileSetups
            For Each oClient In oFilesetup.Clients
                If oClient.SavePaymentData Then
                    bSaveData = True
                    ''Exit For
                End If
            Next
        Next
        ClientSavesData = bSaveData

    End Function

    Public Function IsISO20022ReturnFile(ByVal eFormat As vbBabel.BabelFiles.FileType) As Boolean
        Dim bReturnValue As Boolean = False

        Select Case eFormat
            Case vbBabel.BabelFiles.FileType.Camt054, vbBabel.BabelFiles.FileType.Camt054_Outgoing
                bReturnValue = True
            Case vbBabel.BabelFiles.FileType.Camt053, vbBabel.BabelFiles.FileType.Camt053_Outgoing
                bReturnValue = True

            Case vbBabel.BabelFiles.FileType.Pain002, vbBabel.BabelFiles.FileType.Pain002_Rejected
                bReturnValue = True
        End Select

        Return bReturnValue

    End Function
    Public Function IsTelepay(ByVal eFormat As vbBabel.BabelFiles.FileType) As Boolean
        ' Is this a Telepayformat?
        Dim bReturnValue As Boolean = False

        Select Case eFormat
            Case vbBabel.BabelFiles.FileType.Telepay
                bReturnValue = True
            Case vbBabel.BabelFiles.FileType.Telepay_BETFOR0099
                bReturnValue = True
            Case vbBabel.BabelFiles.FileType.Telepay_Split_TBMITBRI
                bReturnValue = True
            Case vbBabel.BabelFiles.FileType.Telepay2
                bReturnValue = True
            Case vbBabel.BabelFiles.FileType.TelepayDOIN
                bReturnValue = True
            Case vbBabel.BabelFiles.FileType.TelepayPlus
                bReturnValue = True
            Case vbBabel.BabelFiles.FileType.TelepayTBIO
                bReturnValue = True
            Case vbBabel.BabelFiles.FileType.TelepayUNIX
                bReturnValue = True
            Case vbBabel.BabelFiles.FileType.Telepay_Split_TBMITBRI_AND_DOIN 'Added 09.01.2018
                bReturnValue = True

        End Select

        Return bReturnValue

    End Function
    Public Function Visma_BankNameToeBank(ByVal sBank As String) As vbBabel.BabelFiles.Bank
        ' translate from bankname in text to enum bank from Visma setup
        Select Case sBank.ToUpper
            Case "ABN_AMRO_NL"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.ABN_Amro_NL
            Case "BG_BANK"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.BG_Bank
            Case "BITS STANDARD"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.BITS_NorwegianStandard
            Case "CITIBANK_CZ"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Citibank_CZ
            Case "CITIBANK_H"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Citibank_H
            Case "DANSKE BANK"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Danske_Bank_NO
            Case "DANSKE BANK_DK"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Danske_Bank_dk
            Case "DANSKE BANK_NO"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Danske_Bank_NO
            Case "DANSKE BANK_SF"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Danske_Bank_SF
            Case "DNB"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.DnB
            Case "DNB_DE"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.DnBNOR_D
            Case "DNB_FI"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.DNB_FI
            Case "DNB_INPS"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS
            Case "DNB_SE"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.DnBNOR_SE
            Case "DNB_US"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.DNB_US
            Case "EVRY"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Fellesdata
            Case "FORENINGSSPAREBANKEN"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Foreningssparebanken
            Case "HANDELSBANKEN"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Handelsbanken
            Case "HANDELSBANKEN_SE"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Svenska_Handelsbanken
            Case "HSBC_UK"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.HSBC_UK
            Case "ING_BANK_NL"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Ing_Bank_NL
            Case "NETS"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.BBS
            Case "NORDEA"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Nordea_CorporateAccess
            Case "NORDEA_DK"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Nordea_DK
            Case "NORDEA_eGateway"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Nordea_eGateway
            Case "NORDEA_FI"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Nordea_FI
            Case "NORDEA_NO"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Nordea_NO
            Case "NORDEA_SE"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Nordea_SE
            Case "RABOBANK_NL"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Rabobank_NL
            Case "S|E|B"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.SEB_NO
            Case "SEB_FINLAND"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.SEB_Finland
            Case "SEB_DK"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.SEB_DK
            Case "SEB_NO"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.SEB_NO
            Case "SEB_SE"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.SEB_SE
            Case "SPAREBANK1"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Sparebank1
            Case "SPAREBANKEN_MIDT_NORGE"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Sparebanken_Midt_Norge
            Case "SYDBANK"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Sydbank
            Case "SWEDBANK"
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Swedbank
            Case Else
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.No_Bank_Specified
        End Select

        ' 05.10.2016
        ' If no bank found, then we need to test further. Can be from BankName in BankPartner.
        ' "Dechiper" from fritextname, but need to have the banks name first, like DNB_Innland, DNB_Utland, SEBsepa, NordeaLokale, etc
        If Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.No_Bank_Specified Or Visma_BankNameToeBank = Nothing Then
            sBank = sBank.ToUpper
            If sBank.Length > 2 Then
                sBank = sBank.Replace(" ", "")  ' remove blanks
            End If

            If Left(sBank, 10) = "DANSKEBANK" Then
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Danske_Bank_NO
            ElseIf Left(sBank, 4) = "BITS" Then
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.BITS_NorwegianStandard
            ElseIf Left(sBank, 3) = "DNB" Then
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.DnB
            ElseIf Left(sBank, 13) = "HANDELSBANKEN" Or Left(sBank, 20) = "SVENSKAHANDELSBANKEN" Then
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Handelsbanken
            ElseIf Left(sBank, 6) = "NORDEA" Then
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Nordea_CorporateAccess
            ElseIf Left(sBank, 3) = "SEB" Or Left(sBank, 5) = "S|E|B" Then
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.SEB_NO
            ElseIf Left(sBank, 8) = "SWEDBANK" Then
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Swedbank
            ElseIf Left(sBank, 7) = "SYDBANK" Then
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.Sydbank
            Else
                Visma_BankNameToeBank = vbBabel.BabelFiles.Bank.No_Bank_Specified
            End If

        End If

    End Function
    'Public Class FileTooLargeException ' - Testing Try ... Catch
    '    Inherits ApplicationException
    '    Private mlngFileSize As Long

    '    Public Sub New(ByVal Message As String)
    '        MyBase.New(Message)
    '    End Sub

    '    Public Sub New(ByVal Message As String, _
    '     ByVal Inner As Exception)
    '        MyBase.New(Message, Inner)
    '    End Sub

    '    Public Sub New(ByVal Message As String, _
    '     ByVal Inner As Exception, ByVal FileSize As Long)
    '        MyBase.New(Message, Inner)
    '        mlngFileSize = FileSize
    '    End Sub

    '    Public ReadOnly Property FileSize() As Long
    '        Get
    '            Return mlngFileSize
    '        End Get
    '    End Property
    'End Class
    Public Function GetExceptionInfo(ByVal ex As Exception) As String ' - Testing Try ... Catch
        Dim sResult As String = ""
        'Dim hr As Integer = Runtime.InteropServices.Marshal.GetHRForException(ex)
        'Result = ex.GetType.ToString & "(0x" & hr.ToString("X8") & "): " & ex.Message & Environment.NewLine & ex.StackTrace & Environment.NewLine
        Dim st As StackTrace = New StackTrace(ex, True)
        For Each sf As StackFrame In st.GetFrames
            If sf.GetFileLineNumber() > 0 Then
                sResult = sResult & "Line:" & sf.GetFileLineNumber() & " Method: " & sf.GetMethod.Name() & " File: " & IO.Path.GetFileName(sf.GetFileName) & Environment.NewLine
            End If
        Next
        Return sResult

    End Function
    Public Function IsIPBAustralia(ByRef oPayment As vbBabel.Payment) As Boolean
        If Left(oPayment.DnBNORTBIPayType, 3) = "IPB" And oPayment.BANK_CountryCode = "AU" And oPayment.MON_AccountCurrency = "AUD" And oPayment.MON_TransferCurrency = "AUD" Then
            IsIPBAustralia = True
        Else
            IsIPBAustralia = False
        End If
    End Function
    Public Function IsIPBSingapore(ByRef oPayment As vbBabel.Payment) As Boolean
        If Left(oPayment.DnBNORTBIPayType, 3) = "IPB" And oPayment.BANK_CountryCode = "SG" And oPayment.MON_AccountCurrency = "SGD" And oPayment.MON_TransferCurrency = "SGD" Then
            IsIPBSingapore = True
        Else
            IsIPBSingapore = False
        End If
    End Function
    Public Function MergePaymentsForISO20022(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        'This function returns true if a merger has taken place, else it will return False

        'The merged payments MUST be marked as follows
        'oPayment.Exported = True
        'oPayment.SpecialMark = True
        'Store the Ref_Own_BabelBankGenerated from the payment it is merged into in REF_Own of the payment that is merged (and will be deleted later)
        'oPaymentToBeDeleted.REF_Own = oPaymentToBeMergedInto.Ref_Own_BabelBankGenerated

        Dim bReturnValue As Boolean = False
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPaymentToBeDeleted As vbBabel.Payment
        Dim oPayment As vbBabel.Payment
        Dim bTryToMerge As Boolean
        Dim sMergeCriteria As String
        Dim oPaymentToBeMergedInto As vbBabel.Payment
        Dim oLocalInvoice As vbBabel.Invoice
        Dim oNewInvoice As vbBabel.Invoice
        Dim nLocalAmount As Double
        Dim bMergeDone As Boolean
        Dim sErr_Message As String = ""

        Try

            For Each oBabelFile In oBabelFiles
                Select Case oBabelFile.Special

                    Case "GJENSIDIGE_OW_TOBANK"
                        'In this case we will try to merge negative payments within a batch (NOT within a babelfile).
                        'It will merge with the first identical payment according to the MergeCriteria
                        'It will NOT try to merge with identical amount first, then to merge with a higher amount.


                        For Each oBatch In oBabelFile.Batches
                            For Each oPayment In oBatch.Payments
                                ' add mergeinfo (like we do for Visma)
                                oPayment.MergeThis = True
                                oPayment.MergeCriteria = oPayment.I_Account & oPayment.MON_TransferCurrency & oPayment.E_Account & oPayment.BANK_SWIFTCode & oPayment.BANK_BranchNo & CStr(oPayment.MON_ChargeMeDomestic) & CStr(oPayment.MON_ChargeMeAbroad) & oPayment.DATE_Payment
                            Next
                        Next

                        bTryToMerge = True
                        Do While bTryToMerge = True
                            'Find creditnotes
                            sMergeCriteria = ""
                            For Each oBatch In oBabelFile.Batches
                                For Each oPaymentToBeMergedInto In oBatch.Payments
                                    If oPaymentToBeMergedInto.MON_InvoiceAmount < 0 And Not oPaymentToBeMergedInto.Exported = True And Not oPaymentToBeMergedInto.SpecialMark = True Then
                                        sMergeCriteria = oPaymentToBeMergedInto.MergeCriteria

                                        Exit For
                                    End If
                                Next
                                If Not EmptyString(sMergeCriteria) Then
                                    Exit For
                                End If
                            Next 'FJERN?

                            'Merge all payments with the same mergecriteria
                            If Not EmptyString(sMergeCriteria) Then
                                For Each oBatch In oBabelFile.Batches 'FJERN?
                                    bMergeDone = False
                                    For Each oPaymentToBeDeleted In oBatch.Payments
                                        If Not oPaymentToBeDeleted.Exported Then
                                            If oPaymentToBeDeleted.MergeCriteria = oPaymentToBeMergedInto.MergeCriteria Then
                                                If Not oPaymentToBeDeleted.Index = oPaymentToBeMergedInto.Index Then
                                                    For Each oLocalInvoice In oPaymentToBeDeleted.Invoices
                                                        ' append invoice to payment with the first creditnote
                                                        oNewInvoice = New vbBabel.Invoice
                                                        CopyInvoiceObject(oLocalInvoice, oNewInvoice)
                                                        oLocalInvoice = oPaymentToBeMergedInto.Invoices.VB_AddWithObject(oNewInvoice)
                                                    Next oLocalInvoice
                                                    oPaymentToBeDeleted.Exported = True
                                                    oPaymentToBeDeleted.SpecialMark = True 'Remove these payments
                                                    oPaymentToBeDeleted.REF_Own = oPaymentToBeMergedInto.Ref_Own_BabelBankGenerated
                                                    nLocalAmount = 0
                                                    For Each oLocalInvoice In oPaymentToBeMergedInto.Invoices
                                                        nLocalAmount = nLocalAmount + oLocalInvoice.MON_InvoiceAmount
                                                    Next
                                                    oPaymentToBeMergedInto.MON_InvoiceAmount = nLocalAmount
                                                    oPaymentToBeMergedInto.MON_TransferredAmount = nLocalAmount
                                                    oPaymentToBeMergedInto.AmountSetTransferred = False
                                                    If oPaymentToBeMergedInto.MON_InvoiceAmount > -0.000001 Then
                                                        bMergeDone = True
                                                    End If
                                                Else
                                                    'This is the creditnote itself
                                                    bMergeDone = bMergeDone
                                                End If
                                            End If
                                        End If
                                    Next
                                    If bMergeDone Then
                                        bReturnValue = True
                                        Exit For
                                    Else
                                        'The merge failed, the amount is still negative.
                                        'An error will be created later

                                        '05.09.2019 - commented next two lines
                                        ' traverse through the next batc as well 
                                        'bTryToMerge = False
                                        'Exit For
                                    End If
                                Next 'FJERN?

                                If bMergeDone Then
                                    bReturnValue = True
                                Else
                                    'The merge failed, the amount is still negative.
                                    'An error will be created later

                                    bTryToMerge = False
                                    'Exit For
                                End If

                            Else
                                bTryToMerge = False
                            End If

                            'Next 'ADD?
                        Loop

                        For Each oBatch In oBabelFile.Batches
                            For Each oPayment In oBatch.Payments
                                If oPayment.MON_InvoiceAmount < 0 And Not oPayment.Exported = True And Not oPayment.SpecialMark = True Then
                                    If sErr_Message = "" Then

                                    Else
                                        sErr_Message = sErr_Message & vbCrLf
                                    End If
                                    sErr_Message = sErr_Message & "BabelBank is unaible to merge this creditnote with an invoice(s)." & vbCrLf & CreateStandardErrorText(oPayment)
                                    Exit For 'Just show the first negative payment, other creditnotes have not yet been merged.
                                End If
                            Next
                        Next

                        If Not EmptyString(sErr_Message) Then
                            Throw New Exception(sErr_Message)
                        End If

                    Case Else
                        bReturnValue = False

                End Select

            Next oBabelFile


        Catch ex As Exception
            If Not EmptyString(sErr_Message) Then
                Throw New vbBabel.Payment.PaymentException("Function: MergePaymentsForISO20022" & vbCrLf & sErr_Message, ex)
            Else
                Throw New vbBabel.Payment.PaymentException("Function: MergePaymentsForISO20022" & vbCrLf & ex.Message, ex)
            End If

        Finally


        End Try

        MergePaymentsForISO20022 = bReturnValue

    End Function

    Public Function RemoveMergedPaymentsForISO20022(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim lBabelFileCount As Long
        Dim lBatchCount As Long
        Dim lPaymentCount As Long
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch

        'Remove used payments
        For lBabelFileCount = oBabelFiles.Count To 1 Step -1
            oBabelFile = oBabelFiles.Item(lBabelFileCount)
            For lBatchCount = oBabelFile.Batches.Count To 1 Step -1
                oBatch = oBabelFile.Batches.Item(lBatchCount)
                ' Delete marked payments
                For lPaymentCount = oBatch.Payments.Count To 1 Step -1
                    If oBatch.Payments.Item(lPaymentCount).SpecialMark = True Then
                        If oBatch.Payments.Item(lPaymentCount).Exported = True Then
                            oBatch.Payments.Remove(lPaymentCount)
                        End If
                    End If
                Next lPaymentCount
                ' Must check if any payments left in this batch
                If oBatch.Payments.Count = 0 Then
                    oBabelFile.Batches.Remove(lBatchCount)
                End If
            Next lBatchCount
            ' Must check if any batches left in this batch
            If oBabelFile.Batches.Count = 0 Then
                oBabelFiles.Remove(lBabelFileCount)
            End If
        Next lBabelFileCount


    End Function
    Public Function FromUTF8ToANSIFile(ByVal sFileNameIn As String, ByVal sFileNameOut As String) As Boolean
        Dim sFileNameInRenamed As String = ""

        ' does importfile exist?
        If System.IO.File.Exists(sFileNameIn) Then

            ' if same filenameout and in, the rename in-file
            If sFileNameIn = sFileNameOut Then
                ' if already present, delete "_renamed"
                If System.IO.File.Exists(sFileNameIn & "_renamed") Then
                    System.IO.File.Delete(sFileNameIn & "_renamed")
                End If

                My.Computer.FileSystem.RenameFile(sFileNameIn, System.IO.Path.GetFileName(sFileNameIn) & "_renamed")
                sFileNameIn = sFileNameIn & "_renamed"
                sFileNameInRenamed = sFileNameIn
            End If

            ' import utf-8 file
            Dim bData As Byte()
            Dim br As System.IO.BinaryReader = New System.IO.BinaryReader(System.IO.File.OpenRead(sFileNameIn))
            bData = br.ReadBytes(br.BaseStream.Length)
            br.Close()



            Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(bData, 0, bData.Length)
            'Dim myStr As String = System.Text.Encoding.ASCII.GetString(ms.ToArray())
            Dim myStr As String = System.Text.Encoding.UTF8.GetString(ms.ToArray())  ' ikke gj�r om tegn
            ' s� gj�r om med interne funksjoner
            myStr = ReplaceUTF8Characters(myStr)
            ' linjen over funker ikke s�rlig bra, den bytter ut utf-8 tegn oer asc127 til ?
            ' og det blir igjen tre ??? i starten.
            ' men vi f�r leve med dette som en start
            ' m� da fjerne innledende ?
            myStr = Mid(myStr, 2)

            ' delete outputfile if already exists
            If System.IO.File.Exists(sFileNameOut) Then
                System.IO.File.Delete(sFileNameOut)
            End If

            ' export to ascii file
            Dim file As System.IO.StreamWriter
            file = My.Computer.FileSystem.OpenTextFileWriter(sFileNameOut, True, System.Text.Encoding.ASCII)
            file.WriteLine(myStr)
            file.Close()

            ' if same filenameout and in, the delete rename in-file
            If System.IO.File.Exists(sFileNameInRenamed) Then
                System.IO.File.Delete(sFileNameInRenamed)
            End If

        End If

        FromUTF8ToANSIFile = True

    End Function
    Sub KARTest(ByRef oProfile As vbBabel.Profile, ByRef iFileSetup_ID As Short)
        Dim sAccount As String = ""
        Dim sCustomer As String = ""
        Dim sReturnCode As String = ""
        Dim sReturnComment As String = ""
        Dim sOK As Boolean = False
        Dim oFilesetup As vbBabel.FileSetup
        Dim sCertificateFileName As String = ""
        Dim sPW As String = "w5fH9IJZD2cx"

        ' Find certificate file
        For Each oFilesetup In oProfile.FileSetups
            If Val(CStr(oFilesetup.FileSetup_ID)) = iFileSetup_ID Then
                sCertificateFileName = oFilesetup.secFileNameBankPublicKey
                sPW = oFilesetup.secPassword
                Exit For
            End If
        Next

        If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20220929" Then
            MsgBox("Sertifikatilnavn er satt til: " & sCertificateFileName)
        End If

        ' Disse testdata skal finnes i KAR-test
        sCustomer = "00996543218"
        sAccount = "99994411116"

        If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20201008" Then
            MsgBox("Skal starte testen")
        End If

        ' kj�r test 1000 ganger
        'Dim i As Integer = 0
        'Dim starttime As DateTime = Now
        'For i = 1 To 1000

        If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20201020" Then
            sCertificateFileName = ""
            sPW = ""
        End If
        sOK = verifyAccountOwner(sCertificateFileName, sPW, sCustomer, sAccount, sReturnCode, sReturnComment)

        If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20201008" Then
            MsgBox("Har kj�rt test!" & vbCrLf & "sOK = " & sOK & vbCrLf & "sReturnCode = " & sReturnCode & vbCrLf & "sReturnComment = " & sReturnComment)
        End If
        'Next i
        'Dim stoptime As DateTime = Now
        'Dim elapsed_time As TimeSpan = stoptime.Subtract(starttime)

        sAccount = ""

        '00981234561 99994411124 02&Ja&req=karVerifyAccountOwner&customer=00981234561&account=99994411124&newaccount = 99994422231
        'Ja, konto 99994411124 er omnummerert til ny konto 99994422231, og nykonto eies av denne kunden.

        '00996543218 99994422223 03&Nei&req=karVerifyAccountOwner&customer=00996543218&account=99994422223
        'Nei, konto 99994422223 finnes, men eies ikke av denne kunden.

        '00996543218 99996677776 04&Nei&req=karVerifyAccountOwner&customer=00996543218&account=99996677776
        'Nei, konto 99996677776 finnes ikke.

        '00996543218 99991688885 05&Kan ikke verifiseres&req=karVerifyAccountOwner&customer=00996543218&account=99991688885
        '&newaccount=99998800001 Nei, konto 99991688885 er omnummerert til ny konto 99998800001, og ny konto finnes ikke i KAR.

        '00971234563 99994411124 06&Nei&req=karVerifyAccountOwner&customer=00971234563&account=99994411124& NewAccount = 99994422231
        'Nei, konto 99994411124 er omnummerert til ny konto 99994422231. Ny konto finnes, men eies ikke av denne kunden.

        If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20201008" Then
            MsgBox("STOPP KJ�RING")
        End If

    End Sub
    Private Function verifyAccountPayment(ByRef sCertificateFileName As String, ByVal sAccount As String, Optional ByRef sReturnCode As String = "", Optional ByRef sReturnComment As String = "") As Boolean
        ' Open connection.
        'Dim sCertificateFileName As String = "C:\Slett\SG\KAR\ajour-sgfinans-kt.p12"
        Dim sPW As String = "w5fH9IJZD2cx"
        Dim sProgress As String = "100"
        Dim cert1 As X509Certificate2
        Dim cert2 As X509Certificate2
        Dim cert3 As X509Certificate2
        Dim sBaseUri As String = ""
        Dim bProduction As Boolean = True

        '#################### T E S T I N G ######################
        If EmptyString(sCertificateFileName) Then
            sCertificateFileName = "C:\Slett\SG\KAR\ajour-sgfinans-kt.p12"
        End If
        ' HVIS sertifikatets navn starter med TEST, s� g�r vi mot KAR test.
        '       Dersom ikke, s� sp�r vi mot produksjon
        If Left(System.IO.Path.GetFileName(sCertificateFileName), 4).ToUpper = "TEST" Then
            bProduction = False
        End If
        '#################### T E S T I N G ######################

        If bProduction Then
            sBaseUri = "https://ajour.nets.no/kar-direct/"
        Else
            sBaseUri = "https://ajour-test.nets.no/kar-direct/"
        End If

        Try
            Dim request As HttpWebRequest = HttpWebRequest.Create(sBaseUri & "accounts/" + sAccount + "/karVerifyAccountPayment")
            request.Method = "GET"
            request.Credentials = CredentialCache.DefaultCredentials
            sProgress = "200"

            Dim Collection As New X509Certificate2Collection
            Collection.Import(sCertificateFileName, sPW, X509KeyStorageFlags.PersistKeySet)
            ' vi har tre sertifikater i sertifikatfilen
            cert1 = Collection(0)
            cert2 = Collection(1)
            cert3 = Collection(2)

            ' Etter melding fra Arild Endresen, Centurion NO AS, aendr@nets.no for Nets, ser det ut som vi skal bruke
            ' to sertifikater, og vi pr�ver da de to siste av tre.
            request.ClientCertificates.Add(cert2)  ' hvilket sertifikat skal vi bruke?
            request.ClientCertificates.Add(cert3)  ' hvilket sertifikat skal vi bruke?

            sProgress = "300"
            ' Get the response.
            Dim response As WebResponse = request.GetResponse()
            'sFeedback = CType(response, HttpWebResponse).StatusDescription

            sProgress = "400"
            If Not response Is Nothing Then
                ' Get the stream containing content returned by the server.
                Dim dataStream As Stream = response.GetResponseStream()
                sProgress = "500"
                ' Open the stream using a StreamReader for easy access.
                Dim reader As New StreamReader(dataStream)
                ' Read the content.
                Dim responseFromServer As String = reader.ReadToEnd()
                sProgress = "600"
                ' Clean up the streams and the response.
                reader.Close()
                response.Close()

                ' f�r vi tilbake hele str�mmen, som f.eks. slik:
                '02&Ja, men omnummerert&req=karVerifyAccountPayment&account=70010012345&newaccount=60019900000
                If xDelim(responseFromServer, "&", 1) = "02" Then
                    verifyAccountPayment = True
                Else
                    verifyAccountPayment = False
                End If
            Else
                MsgBox("verifyAccountPayment - Feil i sp�rring mot KAR")
            End If

        Catch
            Throw New System.Exception("KAR: Feil i verifyAccountPayment. Progress = " & sProgress)
        End Try

    End Function
    Public Function verifyAccountOwner(ByRef sCertificateFileName As String, ByRef sPW As String, ByVal sCustomer As String, ByVal sAccount As String, Optional ByRef sReturnCode As String = "", Optional ByRef sReturnComment As String = "", Optional ByVal bShowMessage As Boolean = False) As Boolean

        ' Open connection.
        'Dim sCertificateFileName As String '= "C:\Slett\SG\KAR\ajour-sgfinans-kt.p12"
        'Dim sPW As String = "w5fH9IJZD2cx"  ' <===== Passord b�r hentes fra oppsett !!!!
        'PWTest = "w5fH9IJZD2cx"
        'PWProd = "FbHVRTzXq2tJ"
        Dim sProgress As String = "100"
        Static cert1 As X509Certificate2
        Dim cert2 As X509Certificate2
        Dim cert3 As X509Certificate2
        Dim sBaseUri As String = ""
        Dim bProduction As Boolean = True
        'Dim bShowMessage As Boolean = False
        Static Collection As X509Certificate2Collection

        '#################### T E S T I N G ######################
        If EmptyString(sCertificateFileName) Then
            'sCertificateFileName = "C:\Slett\SG\KAR\TESTajour-sgfinans-kt.p12"
            ' 20.10.2020 - if no certificate filename given, find certificate from certificate store
            ' SETT SOM TEST FOREL�PIG
            'If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20201020" Then
            '    bProduction = False     '<------ FJERNES I PRODUKSJON
            '    '####################################################
            'End If
        End If
        ' HVIS sertifikatets navn starter med TEST, s� g�r vi mot KAR test.
        '       Dersom ikke, s� sp�r vi mot produksjon
        If Left(System.IO.Path.GetFileName(sCertificateFileName), 4).ToUpper = "TEST" Then
            bProduction = False
        End If
        '#################### T E S T I N G ######################

        If bProduction Then
            sBaseUri = "https://ajour.nets.no/kar-direct/"
        Else
            sBaseUri = "https://ajour-test.nets.no/kar-direct/"
        End If

        If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20221002" Then
            bShowMessage = True
            MsgBox("Skal benytte URI: " & sBaseUri)
        End If

        'System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12

        If bShowMessage Then
            MsgBox("Har satt securityprotocol til Tls12")
        End If

        ' Find certificate (p12) file
        ' Must be setup in profile, as Banks Public key

        'System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls 'Tls12

        ' Prod:
        ' https://host:port/kar-ws/api/v1/customers/{customer}/accounts/{account}/karVerifyAccountOwner
        ' Test:
        ' https://ajour-test.nets.no/kar-direct/{customer}/accounts/{account}/karVerifyAccountOwner
        Dim request As HttpWebRequest = HttpWebRequest.Create(sBaseUri & "customers/" & sCustomer & "/accounts/" + sAccount + "/karVerifyAccountOwner")

        If bShowMessage Then
            MsgBox("Dimming gikk bra.")
        End If
        request.Method = "GET"
        request.Credentials = CredentialCache.DefaultCredentials

        If bShowMessage Then
            MsgBox("Har satt defaultcredentials")
        End If
        sProgress = "200"

        ' new if 20.10.2020
        If EmptyString(sCertificateFileName) Then
            ' Find certificate from certificate store;
            ' Use the X509Store class to get a handle to the local certificate stores. "My" is the "Personal" store.
            Dim store As X509Store = New X509Store(StoreName.My, StoreLocation.LocalMachine)

            ' Open the store to be able to read from it.
            store.Open(OpenFlags.ReadOnly)

            If bShowMessage Then
                MsgBox("Har �pnet store.")
            End If

            ' Use the X509Certificate2Collection class to get a list of certificates that match our criteria (in this case, we should only pull back one).
            'X509Certificate2Collection(Collection = store.Certificates.Find(X509FindType.FindBySubjectName, "ajour-sgfinans-kt", True))
            Dim storeCollection As X509Certificate2Collection = store.Certificates.Find(X509FindType.FindByIssuerName, "NETS AS Intermediate CA", False)


            If bShowMessage Then
                MsgBox("Antall i StoreCollection" & storeCollection.Count.ToString)
            End If

            If bShowMessage Then
                MsgBox("Item(0): " & storeCollection.Item(0).ToString())
            End If

            'If bShowMessage Then
            '    MsgBox("Item(1): " & storeCollection.Item(1).ToString())
            'End If

            ' Associate the certificates with the request
            request.ClientCertificates = storeCollection


            If bShowMessage Then
                MsgBox("Har lagt inn i request.")
            End If


        Else

            If cert1 Is Nothing Then
                Collection = New X509Certificate2Collection
                Collection.Import(sCertificateFileName, sPW, X509KeyStorageFlags.PersistKeySet)
            End If

            If bShowMessage Then
                MsgBox("Har laget en Collection.")
            End If

            If bShowMessage Then
                MsgBox("Collection(0) eksisterer")
            End If

            ' vi har tre sertifikater i sertifikatfilen
            cert1 = Collection(0)
            If bShowMessage Then
                MsgBox("Collection(0) eksisterer")
            End If
            cert2 = Collection(1)
            If bShowMessage Then
                MsgBox("Collection(1) eksisterer")
            End If
            cert3 = Collection(2)
            If bShowMessage Then
                MsgBox("Collection(2) eksisterer")
            End If

            If bShowMessage Then
                MsgBox("Har hentet ut de tre sertifikatene i filen.")
            End If

            ' Etter melding fra Arild Endresen, Centurion NO AS, aendr@nets.no for Nets, ser det ut som vi skal bruke
            ' to sertifikater, og vi pr�ver da de to siste av tre.
            request.ClientCertificates.Add(cert2)  ' hvilket sertifikat skal vi bruke?
            request.ClientCertificates.Add(cert3)  ' hvilket sertifikat skal vi bruke?
        End If

        sProgress = "300"
        ' Get the response.
        Dim response As WebResponse = request.GetResponse()
        'sFeedback = CType(response, HttpWebResponse).StatusDescription

        If bShowMessage Then
            MsgBox("Har f�tt en response.")
        End If

        If Not response Is Nothing Then
            sProgress = "400"
            ' Get the stream containing content returned by the server.
            Dim dataStream As Stream = response.GetResponseStream()
            ' Open the stream using a StreamReader for easy access.

            If bShowMessage Then
                MsgBox("Har dimmet DataStream")
            End If

            Dim reader As New StreamReader(dataStream)
            sProgress = "500"
            ' Read the content.
            Dim responseFromServer As String = reader.ReadToEnd()
            sProgress = "600"
            ' Clean up the streams and the response.
            reader.Close()
            response.Close()

            If bShowMessage Then
                MsgBox("Har lest ut svaret og putta det i en variabel.")
            End If

            ' Vi f�r tilbake hele str�mmen, som f.eks. slik:
            '02&Ja, men omnummerert&req=karVerifyAccountPayment&account=70010012345&newaccount=60019900000
            If xDelim(responseFromServer, "&", 2) = "Ja" Then
                verifyAccountOwner = True
            Else
                verifyAccountOwner = False
            End If

            ' Tolke svaret;
            sReturnCode = xDelim(responseFromServer, "&", 1)
            Select Case sReturnCode
                Case "01"
                    sReturnComment = "Ja, denne kunden eier denne kontoen."
                Case "02"
                    sReturnComment = "Ja, konto " & sAccount & " er omnummerert til ny konto, og ny konto eies av denne kunden."
                Case "03"
                    sReturnComment = "Nei, konto " & sAccount & " finnes, men eies ikke av denne kunden."
                Case "04"
                    sReturnComment = "Nei, konto " & sAccount & " finnes ikke."
                Case "05"
                    sReturnComment = "Nei, konto " & sAccount & " er omnummerert til ny konto, og ny konto finnes ikke i KAR."
                Case "06"
                    sReturnComment = "Nei, konto " & sAccount & " er omnummerert til ny konto. Ny konto finnes, men eies ikke av denne kunden"
                Case "07"
                    sReturnComment = "Bank ikke i KAR."
                Case "08"
                    sReturnComment = "Konto " & sAccount & " er omnummerert til konto. Ny konto tilh�rer bank som ikke er aktiv i KAR."
                Case "09"
                    sReturnComment = "Reg.nr. finnes ikke."
                Case "10"
                    sReturnComment = "Konto ikke CDVgyldig."
                Case "11"
                    sReturnComment = "Ugyldig foretaksnummer."
                Case "13"
                    sReturnComment = "Innsikt forbudt."
                Case "14"
                    sReturnComment = "Konto " & sAccount & " finnes, men eies av en kunde med ugyldig kundenummer."
                Case "15"
                    sReturnComment = "Konto " & sAccount & " er omnummerert til ny konto, og ny konto eies av kunden med ugyldig kundenummer"
                Case Else
                    sReturnComment = "Ukjent svar - " & responseFromServer
            End Select

            If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20221003" Then
                MsgBox("Returkommentar: " & sReturnCode & " - " & sReturnComment)
            End If

        Else
            'MsgBox("verifyAccountOwner - Feil i sp�rring mot KAR")
            sReturnCode = "-1"
            sReturnComment = "Feil i sp�rring mot KAR"
        End If

    End Function
    Public Sub ExportDecryptedFile(ByVal sFilenameIn As String, ByVal sFilenameOut As String, Optional ByRef bDecryptedFileAlreadyExported As Boolean = False)
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim stmp As String = ""
        Dim sTmp2 As String = ""
        Dim bSecureEnvelopeInUse As Boolean = False
        Dim bt64 As Byte()
        Dim bGZIP As Boolean = False
        Dim iPos As Long
        Dim iPos2 As Long
        Dim bPain001 As Boolean = False

        oFs = New Scripting.FileSystemObject
        oFile = oFs.OpenTextFile(sFilenameIn, Scripting.IOMode.ForReading)
        stmp = ""
        stmp = oFile.Read(500000)  ' Is 500000 encough???
        sTmp2 = stmp.Substring(0, 500)  ' to check compression
        ' close file
        oFile.Close()
        oFile = Nothing
        ' ------------------------------------------------------------------

        iPos = InStr(stmp, "NDCAPXMLI", CompareMethod.Text)   ' Pain.001 with SecureEnvelope 
        If iPos > 0 Then
            bPain001 = True
        Else
            iPos = InStr(stmp, "NDCAPXMLO", CompareMethod.Text)   ' Pain.002 with SecureEnvelope 
            If iPos <= 1 Then
                iPos = InStr(stmp, "NDCAPXML", CompareMethod.Text)
                ' Also test for NDCAPXMLD54O
                If iPos <= 1 Then
                    If iPos < 1 Then
                        iPos = InStr(stmp, "NDCAPXMLD54O", CompareMethod.Text)
                    End If
                End If
            End If
        End If

        iPos2 = InStr(stmp, "<Content>", CompareMethod.Text)
        If iPos > 0 And iPos2 > 0 Then
            ' this file is with SecureEnvelope
            bSecureEnvelopeInUse = True
            ' find the actual filecontent
            iPos = iPos2 + 9
            ' find the end of content
            iPos2 = InStr(stmp, "</Content>", CompareMethod.Text) - 1
            ' find base64 string;
            'sTmp = Mid(sTmp, iPos, iPos2 - iPos)
            stmp = Mid(stmp, iPos, iPos2 - iPos + 1)
            bt64 = System.Convert.FromBase64String(stmp)
        End If

        ' --------------------------------------------------
        If bSecureEnvelopeInUse Then
            iPos = InStr(sTmp2, "<Compression>", CompareMethod.Text)   ' Pain.001 with SecureEnvelope
            If iPos > 0 Then
                iPos = iPos + 13
                iPos2 = iPos + 5
                stmp = sTmp2.Substring(iPos - 1, iPos2 - iPos - 1).ToUpper
            Else
                ' Pain.002/Camt.054
                iPos = InStr(sTmp2, "<Compressed>", CompareMethod.Text) ' Pain.002 or Camt.054
                iPos = iPos + 12
                iPos2 = iPos + 5
                stmp = sTmp2.Substring(iPos - 1, iPos2 - iPos - 1).ToUpper
            End If

            If stmp = "TRUE" Then
                ' Compressed with GZIP. We take the assumption that if compression = true, then it's GZIP
                bGZIP = True
            Else
                bGZIP = False
            End If
            If bGZIP Then
                bt64 = GZIPDeCompress(bt64)
                ' unzip via string
                'bt64 = System.Text.Encoding.Unicode.GetBytes(GZ_UnZip(System.Text.Encoding.Unicode.GetString(bt64)))
            End If
            ' read from base64 string
            stmp = System.Text.Encoding.ASCII.GetString(bt64)
            If bPain001 Then
                ' for Pain.001 files we sometimes have UTF-8 charset, and we then have 3 characters in front which creates problems
                If VB.Left(stmp, 1) <> "<" Then
                    stmp = Mid(stmp, 4)
                End If
            End If
        End If

        If bSecureEnvelopeInUse Then
            ' Export to file;
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForWriting, True)
            oFile.Write(stmp)
            oFile.Close()
            oFile = Nothing
            oFs = Nothing
            bDecryptedFileAlreadyExported = True
        End If
    End Sub

End Module
