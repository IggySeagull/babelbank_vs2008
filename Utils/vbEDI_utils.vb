Option Strict Off
Option Explicit On
Module vbEDI_utils
	
	Private sSegmentSep, sElementSep, sGroupSep As String
	Private sEscapeChar, sDecimalSep, sSegmentLen As String
	
	'UPGRADE_WARNING: Lower bound of array aTagsettings was changed from 1,1 to 0,0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
	Private aTagsettings(2, 9999) As Short
    Private aSegmentInfo(,) As String
	
	Public Function InitiateSeperators(ByRef sElement As String, ByRef sSegment As String, ByRef sGroup As String, ByRef sDecimal As String, ByRef sEscape As String, ByRef sSegmentLength As String) As Boolean
		
		sElementSep = sElement
		sSegmentSep = sSegment
		sGroupSep = sGroup
		sDecimalSep = sDecimal
		sEscapeChar = sEscape
		sSegmentLen = sSegmentLength
		
		InitiateSeperators = True
		
	End Function
	
	Public Function GetPropValue(ByRef sVariable As String, ByVal oSegment As MSXML2.IXMLDOMElement, ByRef oEDIMapping As MSXML2.IXMLDOMElement, Optional ByRef lNoOfAppearence As Integer = 0) As String
		
		Dim oMappingElement As MSXML2.IXMLDOMElement
		'Dim oSegmentOriginal As MSXML2.IXMLDOMElement
		Dim lCountLoops, i, lChangeLevel, l, lMax As Integer
		Dim iNoOfFTX As Integer
		Dim sFTX As String
		Dim oPossibleNodes As MSXML2.IXMLDOMNodeList
		Dim oFoundElement As MSXML2.IXMLDOMElement ', oFoundElements As MSXML2.IXMLDOMNodeList
		Dim sXPathCommand, sLocalVariable As String
		Dim oDateFormatElement As MSXML2.IXMLDOMElement
		Dim sItemNo As String
		Dim iOldGroupNo As Short
		
		
		
		sLocalVariable = sVariable
		
		Dim sSegment, sGroup, sQTagGroup As String
		Dim sTagGroup, sQTag, sTag As String
		Dim sLine As String
		Dim sXpath1, sXpath2 As String
		
		sItemNo = "1"
		
		'Set oSegmentOriginal = oSegment
		
		
		For i = 0 To lNoOfAppearence
			
			'Set oMappingElement = oEDIMapping.selectSingleNode(sLocalVariable)
			
			sXpath1 = Left(sLocalVariable, InStr(1, sLocalVariable, "/") - 1)
			sXpath2 = Right(sLocalVariable, Len(sLocalVariable) - InStr(1, sLocalVariable, "/"))
			
			oMappingElement = oEDIMapping.selectSingleNode(sXpath1)
			oMappingElement = oMappingElement.selectSingleNode(sXpath2)
			
			If oMappingElement Is Nothing Then
                GetPropValue = ""
                Exit Function
            Else
                'Sometimes the information are at a higher "level" in the EDI-file. This is
                ' stored in an attribute in the mapping called level. Then we
                ' we have to climb up in the mapping tree.
                'UPGRADE_WARNING: Couldn't resolve default property of object oMappingElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lChangeLevel = CInt(oMappingElement.getAttribute("level"))
                For l = 1 To lChangeLevel
                    If oSegment.nodeName = "GROUP" Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object oSegment.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sItemNo = oSegment.getAttribute("index")
                        'UPGRADE_WARNING: Couldn't resolve default property of object oSegment.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        iOldGroupNo = CShort(oSegment.getAttribute("group"))
                    End If
                    oSegment = oSegment.parentNode
                Next l
                'Set oPossibleNodes = oSegment.selectNodes("GROUP[@group = ' 3']")

                'UPGRADE_WARNING: Couldn't resolve default property of object oMappingElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sGroup = oMappingElement.getAttribute("group")
                'UPGRADE_WARNING: Couldn't resolve default property of object oMappingElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sSegment = oMappingElement.getAttribute("segment")
                'UPGRADE_WARNING: Couldn't resolve default property of object oMappingElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTagGroup = oMappingElement.getAttribute("taggroup")
                'UPGRADE_WARNING: Couldn't resolve default property of object oMappingElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTag = oMappingElement.getAttribute("tag")
                'UPGRADE_WARNING: Couldn't resolve default property of object oMappingElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sLine = oMappingElement.getAttribute("line")
                'UPGRADE_WARNING: Couldn't resolve default property of object oMappingElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sQTagGroup = oMappingElement.getAttribute("qtaggroup")
                'UPGRADE_WARNING: Couldn't resolve default property of object oMappingElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sQTag = oMappingElement.getAttribute("qtag")

                'Loop through all possible values in the mapping-file, in prioritated order
                ' until you find a segment in the EDI-file that fits the mapping.
                'We use lcountloops to count the number of times we go through the loop, cause
                ' we always have to go through the loop even if the length is 0
                lCountLoops = -1
                'New code by Kjell 12/2-2004, cahnged the IF-statement
                For l = 0 To oMappingElement.childNodes(0).childNodes.length
                    'For l = 0 To oMappingElement.childNodes(0).childNodes.length
                    If l = 0 Then l = 1
                    lCountLoops = lCountLoops + 1

                    ' Find the node fitting the description (correct value in the qualificator).
                    sXPathCommand = ""
                    If lChangeLevel > 0 Then
                        If Val(sGroup) = iOldGroupNo Then
                            sXPathCommand = "GROUP[@group = ' "
                            sXPathCommand = sXPathCommand & sGroup & "' and @index = '" & sItemNo & "']/"
                            'sXPathCommand = "GROUP[@group = ' 3' and @index = ' 1']/"
                        Else
                            sXPathCommand = "GROUP[@group = ' "
                            sXPathCommand = sXPathCommand & sGroup & "']/"
                        End If
                    ElseIf lChangeLevel = -2 Then
                        sXPathCommand = "GROUP[@group = ' "
                        'UPGRADE_WARNING: Couldn't resolve default property of object oMappingElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sXPathCommand = sXPathCommand & oMappingElement.getAttribute("parentgroup") & "']/"
                        sXPathCommand = sXPathCommand & "GROUP[@group = ' "
                        sXPathCommand = sXPathCommand & sGroup & "']/"
                    ElseIf lChangeLevel <> 0 Then
                        sXPathCommand = "GROUP[@group = ' "
                        sXPathCommand = sXPathCommand & sGroup & "']/"
                    End If

                    sXPathCommand = sXPathCommand & sSegment & "/TG"
                    If oMappingElement.childNodes(0).hasChildNodes Then
                        sXPathCommand = sXPathCommand & sQTagGroup & "[T" & sQTag & "= '" & oMappingElement.childNodes(0).childNodes(lCountLoops).nodeTypedValue & "']"
                    Else
                        'Sometimes we don't have a qualifier, then test if the tag containing the value exists
                        sXPathCommand = sXPathCommand & sTagGroup
                        '& "[T" & _
                        ''oMappingElement.getAttribute ("tag") & "= '" & _
                        ''oMappingElement.childNodes(0).childNodes(l).nodeTypedValue & "']"
                    End If

                    'sXPathCommand = "NAD/TG010[T3035= 'MS']"
                    'sXPathCommand = "//BGM/TG020"
                    'sXPathCommand = "UNB/UNG/GROUP/BGM/TG020"
                    oFoundElement = oSegment.selectSingleNode(sXPathCommand)

                    If Not oFoundElement Is Nothing Then
                        '12.09.2006 - Added the next IF
                        If sXpath1 = "batch" And sXpath2 = "ref_bank" Then
                            'Sometimes the first priority of references AII is stated but without value.
                            ' If so try the next qualificator
                            If oFoundElement.childNodes.length > 1 Then
                                Exit For
                            End If
                        ElseIf sXPathCommand = "GROUP[@group = ' 11']/RFF/TG010[T1153= 'ACD']" Then
                            '06.02.2016 - Added this ElseIf for REMA. THE ACD-reference is stated in the CREMUL-file but empty.
                            '    we need to continue the search to find a reference
                            'I think it is a good idea to always test if childnodes.length > 1. That prevent us from importing empty valuea when other valid info is stated.
                            If oFoundElement.childNodes.length > 1 Then
                                Exit For
                            End If
                        Else
                            Exit For
                        End If
                    End If

                Next l


                '***************  OLD CODE ************

                '        If Not oFoundElement Is Nothing Then
                '        'If oFoundElements.length > 0 Then
                '            'We've found the correct node according to the mapping. Go to the parent node
                '            ' and find the correct element containing the wanted data
                '            If oMappingElement.getAttribute("segment") = "FTX" Then
                '                'Set oFoundElement = oFoundElements(0).parentNode
                '                Set oFoundElement = oFoundElement.parentNode
                '                Set oFoundElement = oFoundElement.parentNode
                '                sXPathCommand = ".//"
                '            Else
                '                'Set oFoundElement = oFoundElements(0).parentNode
                '                Set oFoundElement = oFoundElement.parentNode
                '                sXPathCommand = ""
                '            End If
                '            sXPathCommand = sXPathCommand & "TG" & oMappingElement.getAttribute("taggroup") & "/T" & oMappingElement.getAttribute("tag")
                '            Set oPossibleNodes = oFoundElement.selectNodes(sXPathCommand)
                '            If oPossibleNodes.length = 0 Then
                '                If i >= lNoOfAppearence Then
                '                    'FIX: Babelerror
                '                    'MsgBox "The variable value was not found according to the qualificator/mapping" & vbCrLf _
                ''                     '& "Segment: " & oMappingElement.getAttribute("segment") & vbCrLf _
                ''                     '& "TagGroup: " & oMappingElement.getAttribute("taggroup") & vbCrLf _
                ''                     '& "Tag: " & oMappingElement.getAttribute("tag")
                '                Else
                '                    GetPropValue = ""
                '                End If
                '            Else
                '                'Check if the specified line in the mapping exceeds the actual number of "lines"
                '                ' in the node-list, if so return an empty-string.
                '                'If oPossibleNodes.length < Val(oMappingElement.getAttribute("line")) Then
                '                If oPossibleNodes.length < CLng(oMappingElement.getAttribute("line")) Then
                '                    GetPropValue = ""
                '                ElseIf oMappingElement.getAttribute("segment") = "FTX" Then
                '                    sFTX = ""
                '                    lMax = oPossibleNodes.length - 1
                '                    For iNoOfFTX = 0 To lMax   'oPossibleNodes.length - 1
                ''                        sFTX = CVar(oPossibleNodes(iNoOfFTX).nodeTypedValue) _
                '''                           & Space(70 - Len(oPossibleNodes(iNoOfFTX).nodeTypedValue))
                '                        sFTX = sFTX & CVar(oPossibleNodes(iNoOfFTX).nodeTypedValue) & Space$(70 - Len(oPossibleNodes(iNoOfFTX).nodeTypedValue))
                ''                        sFTX = oPossibleNodes(1).xml
                ''                        sFTX = Replace(Mid$(sFTX, 8, Len(sFTX) - 7), "</T4440>", "")
                ''                        sFTX = Replace(sFTX, "</T4440>", "")
                '                    Next iNoOfFTX
                '                    GetPropValue = sFTX
                '                Else
                '                    GetPropValue = oPossibleNodes(Val(oMappingElement.getAttribute("line")) - 1).nodeTypedValue
                '                    If oMappingElement.getAttribute("segment") = "DTM" Then
                '                        sXPathCommand = ""
                '                        If lChangeLevel > 0 Then
                '                            sXPathCommand = "GROUP[@group = ' "
                '                            sXPathCommand = sXPathCommand & oMappingElement.getAttribute("group") & "']/"
                '                        End If
                '                        sXPathCommand = sXPathCommand & oMappingElement.getAttribute("segment") & "/TG010/T2379"
                '                        Set oDateFormatElement = oSegment.selectSingleNode(sXPathCommand)
                '                        GetPropValue = FindDateFormat(GetPropValue, oDateFormatElement.nodeTypedValue)
                '                    End If
                '                End If
                '            End If
                '        Else
                '            GetPropValue = ""
                '        End If


                '*************** NEW CODE ***********

                If Not oFoundElement Is Nothing Then
                    'If oFoundElements.length > 0 Then
                    'We've found the correct node according to the mapping. Go to the parent node
                    ' and find the correct element containing the wanted data
                    If sSegment = "FTX" Then
                        'Set oFoundElement = oFoundElements(0).parentNode
                        oFoundElement = oFoundElement.parentNode
                        oFoundElement = oFoundElement.parentNode
                        sXPathCommand = ".//"
                    Else
                        'Set oFoundElement = oFoundElements(0).parentNode
                        oFoundElement = oFoundElement.parentNode
                        sXPathCommand = ""
                    End If
                    sXPathCommand = sXPathCommand & "TG" & sTagGroup & "/T" & sTag
                    oPossibleNodes = oFoundElement.selectNodes(sXPathCommand)
                    If oPossibleNodes.length = 0 Then
                        If i >= lNoOfAppearence Then
                            'FIX: Babelerror
                            'MsgBox "The variable value was not found according to the qualificator/mapping" & vbCrLf _
                            ''& "Segment: " & oMappingElement.getAttribute("segment") & vbCrLf _
                            ''& "TagGroup: " & oMappingElement.getAttribute("taggroup") & vbCrLf _
                            ''& "Tag: " & oMappingElement.getAttribute("tag")
                            'New codeline 23.12.2002 by Kjell
                            'GetPropValue = ""
                        Else
                            GetPropValue = ""
                        End If
                    Else
                        'Check if the specified line in the mapping exceeds the actual number of "lines"
                        ' in the node-list, if so return an empty-string.
                        'If oPossibleNodes.length < Val(oMappingElement.getAttribute("line")) Then
                        If oPossibleNodes.length < CInt(sLine) Then
                            GetPropValue = ""
                        ElseIf sSegment = "FTX" Then
                            sFTX = ""
                            lMax = oPossibleNodes.length - 1
                            For iNoOfFTX = 0 To lMax 'oPossibleNodes.length - 1
                                '                        sFTX = CVar(oPossibleNodes(iNoOfFTX).nodeTypedValue) _
                                ''                           & Space(70 - Len(oPossibleNodes(iNoOfFTX).nodeTypedValue))
                                If Len(oPossibleNodes(iNoOfFTX).nodeTypedValue) > 69 Then
                                    sFTX = sFTX & Left(CObj(oPossibleNodes(iNoOfFTX).nodeTypedValue), 70)
                                Else
                                    sFTX = sFTX & CObj(oPossibleNodes(iNoOfFTX).nodeTypedValue) & Space(70 - Len(oPossibleNodes(iNoOfFTX).nodeTypedValue))
                                End If
                                '                        sFTX = oPossibleNodes(1).xml
                                '                        sFTX = Replace(Mid$(sFTX, 8, Len(sFTX) - 7), "</T4440>", "")
                                '                        sFTX = Replace(sFTX, "</T4440>", "")
                            Next iNoOfFTX
                            GetPropValue = sFTX
                        Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object oPossibleNodes().nodeTypedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            GetPropValue = oPossibleNodes(Val(sLine) - 1).nodeTypedValue
                            If sSegment = "DTM" Then
                                sXPathCommand = ""
                                If lChangeLevel > 0 Then
                                    sXPathCommand = "GROUP[@group = ' "
                                    sXPathCommand = sXPathCommand & sGroup & "']/"
                                End If
                                sXPathCommand = sXPathCommand & sSegment & "/TG010/T2379"
                                oDateFormatElement = oSegment.selectSingleNode(sXPathCommand)
                                'New 21.02.2008 for SEB BANSTA this code did'nt work so we have to (probably because lchangelevel = -1)
                                If oDateFormatElement Is Nothing Then
                                    sXPathCommand = "TG010/T2379"
                                    oDateFormatElement = oFoundElement.selectSingleNode(sXPathCommand)
                                End If
                                GetPropValue = FindDateFormat(GetPropValue, oDateFormatElement.nodeTypedValue)
                            End If
                        End If
                    End If
                Else
                    GetPropValue = ""
                End If



            End If

            'If GetPropValue <> "" Then
            If Len(GetPropValue) <> 0 Then
                Exit For
            Else
                'Changed 30.04.2009 - because for DnBNOR INPS E_Adr may be stated in 3 different ways (max earlier was 2)
                If i = 0 Then
                    sLocalVariable = sLocalVariable & Trim(Str(i + 1))
                Else
                    sLocalVariable = Left(sLocalVariable, Len(sLocalVariable) - 1) & Trim(Str(i + 1))
                End If
                'Old code
                'sLocalVariable = sLocalVariable & Trim$(Str$(lNoOfAppearence))
            End If

        Next i

        'vbTimer ("GetPropValue")

    End Function

    Private Function FindDateFormat(ByRef sDate As String, ByRef sFormat As String) As String
        'The returnvalue is always on format YYYYMMDD

        Select Case sFormat
            Case "102"
                FindDateFormat = sDate
            Case "203"
                FindDateFormat = Left(sDate, 8)
            Case Else
                'FIX: BABELERROR
                MsgBox("Unknown date/time-format: " & sFormat & vbCrLf & "Date/time = " & sDate, MsgBoxStyle.OKOnly + MsgBoxStyle.Critical, "DATE ERROR")
        End Select

    End Function
    Public Function InitTagSettingsArray(ByRef mrXMLTagSettings As MSXML2.DOMDocument40) As Boolean
        'Public Function InitTagSettingsArray(sXMLTagSettingsFile As String) As Boolean

        ' Initiate an array with all possible tagsettings,
        ' TagNo (As index in the array)
        ' Element 1:  AlfaNumeric = 0,Numeric = 1, Alfa (only) = 2
        ' Element 2: Max. length of tag
        'Dim mrXMLTagSettings As New MSXML2.DOMDocument40
        Dim eleTagNode As MSXML2.IXMLDOMElement
        'Dim aRetArray(1 To 2, 1 To 9999) As Integer
        Dim lIndex As Integer
        Dim bReturnValue As Boolean

        bReturnValue = False

        'mrXMLTagSettings.Load (sXMLTagSettingsFile)
        eleTagNode = mrXMLTagSettings.documentElement.selectSingleNode("//TAGS").firstChild


        Do
            lIndex = Val(Right(eleTagNode.nodeName, 4))
            'UPGRADE_WARNING: Couldn't resolve default property of object eleTagNode.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aTagsettings(1, lIndex) = Val(eleTagNode.getAttribute("numeric"))
            'UPGRADE_WARNING: Couldn't resolve default property of object eleTagNode.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aTagsettings(2, lIndex) = Val(eleTagNode.getAttribute("maxlength"))
            eleTagNode = eleTagNode.nextSibling
            If eleTagNode Is Nothing Then
                bReturnValue = True
                Exit Do
            End If
        Loop

        InitTagSettingsArray = bReturnValue

    End Function
    Public Function CheckSegment(ByRef sSegString As String, ByRef mrCurrentElement As MSXML2.IXMLDOMElement, ByRef lLINCounter As Integer, ByRef lSEQCounter As Integer, ByRef sFilename As String, ByRef nodValidCurrencyCodes As MSXML2.IXMLDOMElement, ByRef bRemoveEscapeChar As Boolean) As Boolean
        Dim bReturnValue, bTagValueFound As Boolean
        Dim aSegmentArray(,) As String
        'DOTNETT REDIM Dim aNewSegmentArray(,) As String
        Dim aNewSegmentArray(,) As String
        Dim lTagCounter, lTagGroupCounter, lTagValueCounter As Integer
        Dim sTagGroupName, sTagName As String
        Dim iArrayCounter As Short
        Dim bSettingsOK As Boolean
        Dim eleTagGroupElement As MSXML2.IXMLDOMElement
        Dim eleTagElement As MSXML2.IXMLDOMElement
        Dim sSegmentName As String
        Dim sErrorString, sTempSeg As String
        Dim eleCurrencyElement As MSXML2.IXMLDOMElement

        '*************************
        'Dim aSegmentInfo() As String

        Dim sGroupNumber As String

        If mrCurrentElement.parentNode.nodeName = "GROUP" Then
            'UPGRADE_WARNING: Couldn't resolve default property of object mrCurrentElement.parentNode.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sGroupNumber = mrCurrentElement.parentNode.Attributes.getNamedItem("number").nodeValue
        Else
            sGroupNumber = "??"
        End If

        If Len(Trim(sSegString)) = 0 Then
            CheckSegment = True
            Exit Function
        End If

        sErrorString = ""
        sSegmentName = Left(sSegString, 3)

        aSegmentArray = BuildArray(sSegString, bRemoveEscapeChar)

        'Assume success as always
        bReturnValue = True
        iArrayCounter = 0
        For lTagGroupCounter = 0 To mrCurrentElement.childNodes.length - 1
            eleTagGroupElement = mrCurrentElement.childNodes(lTagGroupCounter)
            If Val(aSegmentArray(0, iArrayCounter)) - 1 = lTagGroupCounter Then
                'If Not eleTagGroupElement.Attributes.Item(1).nodeValue = "1" Then
                If Not eleTagGroupElement.getAttribute("inuse") = 1 Then
                    sTempSeg = sSegString
                    'Old code - Remove old replace 
                    'sErrorString = LRS(15027) & vbCrLf & LRS(15030, sFilename) & vbCrLf & LRS(15038, sGroupNumber) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSEQCounter)) & vbCrLf & Replace(LRS(15036), "%1", sSegmentName) & vbCrLf & Replace(LRS(15033), "%1", eleTagGroupElement.nodeName) & vbCrLf & LRS(15035, aSegmentArray(2, iArrayCounter)) & vbCrLf & LRS(15037) & vbCrLf
                    sErrorString = LRS(15027) & vbCrLf & LRS(15030, sFilename) & vbCrLf & LRS(15038, sGroupNumber) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSEQCounter)) & vbCrLf & LRS(15036, sSegmentName) & vbCrLf & LRS(15033, eleTagGroupElement.nodeName) & vbCrLf & LRS(15035, aSegmentArray(2, iArrayCounter)) & vbCrLf & LRS(15037) & vbCrLf
                    Do
                        If Len(sTempSeg) > 50 Then
                            sErrorString = sErrorString & Left(sTempSeg, 50)
                            sTempSeg = Right(sTempSeg, Len(sTempSeg) - 50)
                        Else
                            sErrorString = sErrorString & sTempSeg
                            Exit Do
                        End If
                    Loop
                    Err.Raise(vbObjectError + 15027, , sErrorString)
                    '"Unexpected element"
                    bReturnValue = False
                    Exit For
                Else
                    'Store the TagGroup-name
                    sTagGroupName = eleTagGroupElement.nodeName
                    For lTagCounter = 0 To eleTagGroupElement.childNodes.length - 1
                        eleTagElement = eleTagGroupElement.childNodes(lTagCounter)
                        If Val(aSegmentArray(1, iArrayCounter)) - 1 = lTagCounter And Val(aSegmentArray(0, iArrayCounter)) - 1 = lTagGroupCounter Then
                            'If Not mrCurrentElement.childNodes(lTagGroupCounter).childNodes(lTagCounter).Attributes.Item(1).nodeValue = "1" Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object eleTagElement.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If Not eleTagElement.getAttribute("inuse") = 1 Then
                                sTempSeg = sSegString
                                sErrorString = LRS(15027) & vbCrLf & LRS(15030, sFilename) & vbCrLf & LRS(15038, sGroupNumber) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSEQCounter)) & vbCrLf & LRS(15036, sSegmentName) & vbCrLf & LRS(15033, eleTagGroupElement.nodeName) & vbCrLf & LRS(15034, eleTagElement.nodeName) & vbCrLf & LRS(15035, aSegmentArray(2, iArrayCounter)) & vbCrLf & LRS(15037) & vbCrLf
                                Do
                                    If Len(sTempSeg) > 50 Then
                                        sErrorString = sErrorString & Left(sTempSeg, 50)
                                        sTempSeg = Right(sTempSeg, Len(sTempSeg) - 50)
                                    Else
                                        sErrorString = sErrorString & sTempSeg
                                        Exit Do
                                    End If
                                Loop
                                Err.Raise(vbObjectError + 15027, , sErrorString)
                                '"Unexpected element"
                                bReturnValue = False
                                Exit For
                            Else
                                'Store the Tag-name
                                sTagName = eleTagElement.nodeName
                                'Save the names and value in an array, to be used for writing the XML-file
                                ReDim Preserve aNewSegmentArray(2, iArrayCounter)
                                aNewSegmentArray(0, iArrayCounter) = sTagGroupName
                                aNewSegmentArray(1, iArrayCounter) = sTagName
                                aNewSegmentArray(2, iArrayCounter) = aSegmentArray(2, iArrayCounter)
                                bSettingsOK = CheckSettings(Val(Right(sTagName, 4)), aSegmentArray(2, iArrayCounter), sSegmentName, lLINCounter, lSEQCounter, sFilename, eleTagElement, Val(aSegmentArray(3, iArrayCounter)), sGroupNumber)
                                If eleTagElement.hasChildNodes Then
                                    'Dim a As MSXML2.IXMLDOMElement
                                    'Set a = mrCurrentElement.childNodes(lTagGroupCounter).childNodes(lTagCounter)
                                    bTagValueFound = False
                                    For lTagValueCounter = 0 To eleTagElement.childNodes(0).childNodes.length - 1
                                        'Treat Tag 6345 special
                                        'This is valutacodes, Check it in Values.xml
                                        If sTagName = "T6345" Then
                                            eleCurrencyElement = nodValidCurrencyCodes.selectSingleNode("//" & aSegmentArray(2, iArrayCounter))
                                            If Not eleCurrencyElement Is Nothing Then
                                                If eleCurrencyElement.nodeName = aSegmentArray(2, iArrayCounter) Then
                                                    bTagValueFound = True
                                                Else
                                                    bTagValueFound = True
                                                End If
                                                'UPGRADE_NOTE: Object eleCurrencyElement may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                                eleCurrencyElement = Nothing
                                            Else
                                                bTagValueFound = True
                                            End If
                                        Else
                                            If aSegmentArray(2, iArrayCounter) = eleTagElement.childNodes(0).childNodes(lTagValueCounter).nodeTypedValue Then
                                                bTagValueFound = True
                                            End If
                                        End If
                                        If bTagValueFound Then
                                            Exit For
                                        End If
                                    Next lTagValueCounter
                                    If Not bTagValueFound Then
                                        sTempSeg = sSegString
                                        sErrorString = LRS(15028) & vbCrLf & LRS(15030, sFilename) & vbCrLf & LRS(15038, sGroupNumber) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSEQCounter)) & vbCrLf & LRS(15036, sSegmentName) & vbCrLf & LRS(15033, eleTagGroupElement.nodeName) & vbCrLf & LRS(15034, eleTagElement.nodeName) & vbCrLf & LRS(15035, aSegmentArray(2, iArrayCounter)) & vbCrLf & LRS(15037) & vbCrLf
                                        Do
                                            If Len(sTempSeg) > 50 Then
                                                sErrorString = sErrorString & Left(sTempSeg, 50)
                                                sTempSeg = Right(sTempSeg, Len(sTempSeg) - 50)
                                            Else
                                                sErrorString = sErrorString & sTempSeg
                                                Exit Do
                                            End If
                                        Loop
                                        Err.Raise(15028, , sErrorString)
                                    End If
                                End If
                                iArrayCounter = iArrayCounter + 1
                            End If
                        Else
                            'If mrCurrentElement.childNodes(lTagGroupCounter).childNodes(lTagCounter).Attributes.Item(0).nodeValue = "1" Then
                            If Mandatory(eleTagElement) Then
                                sTempSeg = sSegString
                                sErrorString = LRS(15029) & vbCrLf & LRS(15030, sFilename) & vbCrLf & LRS(15038, sGroupNumber) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSEQCounter)) & vbCrLf & LRS(15036, sSegmentName) & vbCrLf & LRS(15033, eleTagGroupElement.nodeName) & vbCrLf & LRS(15034, eleTagElement.nodeName) & vbCrLf & LRS(15037) & vbCrLf
                                Do
                                    If Len(sTempSeg) > 50 Then
                                        sErrorString = sErrorString & Left(sTempSeg, 50)
                                        sTempSeg = Right(sTempSeg, Len(sTempSeg) - 50)
                                    Else
                                        sErrorString = sErrorString & sTempSeg
                                        Exit Do
                                    End If
                                Loop
                                Err.Raise(vbObjectError + 15029, , sErrorString)
                                '"Mandatory tag missing"
                                bReturnValue = False
                                Exit For
                            End If
                        End If
                    Next lTagCounter
                End If
            Else
                'If mrCurrentElement.childNodes(lTagGroupCounter).Attributes.Item(0).nodeValue = "1" Then
                If Mandatory(eleTagGroupElement) Then
                    sTempSeg = sSegString
                    sErrorString = LRS(15040) & vbCrLf & LRS(15030, sFilename) & vbCrLf & LRS(15038, sGroupNumber) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSEQCounter)) & vbCrLf & LRS(15036, sSegmentName) & vbCrLf & LRS(15033, eleTagGroupElement.nodeName) & vbCrLf & LRS(15037) & vbCrLf
                    Do
                        If Len(sTempSeg) > 50 Then
                            sErrorString = sErrorString & Left(sTempSeg, 50)
                            sTempSeg = Right(sTempSeg, Len(sTempSeg) - 50)
                        Else
                            sErrorString = sErrorString & sTempSeg
                            Exit Do
                        End If
                    Loop
                    Err.Raise(vbObjectError + 15040, , sErrorString)
                    '"Mandatory TagGroup missing"
                    bReturnValue = False
                    Exit For
                End If
            End If
            If bReturnValue = False Then
                Exit For
            End If

        Next lTagGroupCounter

        If UBound(aSegmentArray, 2) - 1 = UBound(aNewSegmentArray, 2) Then
            'nothing to do
        Else
            sTempSeg = sSegString
            sErrorString = LRS(15046) & vbCrLf & LRS(15030, sFilename) & vbCrLf & LRS(15038, sGroupNumber) & vbCrLf & LRS(15031, Str(lLINCounter)) & vbCrLf & LRS(15032, Str(lSEQCounter)) & vbCrLf & LRS(15036, sSegmentName) & vbCrLf & LRS(15037) & vbCrLf
            Do
                If Len(sTempSeg) > 50 Then
                    sErrorString = sErrorString & Left(sTempSeg, 50)
                    sTempSeg = Right(sTempSeg, Len(sTempSeg) - 50)
                Else
                    sErrorString = sErrorString & sTempSeg
                    Exit Do
                End If
            Loop
            Err.Raise(vbObjectError + 15046, , sErrorString)
            '"Mandatory TagGroup missing"
        End If

        eleTagGroupElement = Nothing
        eleTagElement = Nothing

        aSegmentInfo = VB6.CopyArray(aNewSegmentArray)

        CheckSegment = bReturnValue

    End Function

    Private Function CheckSettings(ByRef iTagNumber As Short, ByRef sValue As String, ByRef sSegmentName As String, ByRef lLINCounter As Integer, ByRef lSEQCounter As Integer, ByRef sFilename As String, ByRef eleTagElement As MSXML2.IXMLDOMElement, ByRef iEscapeCharAdded As Short, ByRef sGroupNumber As String) As Boolean
        'iEscapeCharAdded is used when writing an EDI-file. The element may contain
        ' escapecharacters which will influence the allowed length
        Dim bReturnValue As Boolean
        Dim lCounter As Integer
        Dim bRaiseError As Boolean
        Dim sLocalLength As String
        Dim bLocalLengthSet As Boolean

        If aTagsettings(2, iTagNumber) = 0 Then
            Err.Raise(vbObjectError + 15041, , LRS(15041, "T" & Str(iTagNumber)) _
  & vbCrLf & LRS(15038, sGroupNumber) _
  & vbCrLf & LRS(15030, sFilename) _
  & vbCrLf & LRS(15031, Str(lLINCounter)) _
  & vbCrLf & LRS(15032, Str(lSEQCounter)) _
  & vbCrLf & LRS(15036, sSegmentName) _
  & vbCrLf & LRS(15035, sValue))

            '"The element " & sTagName & " is not defined  i mappingen."
        End If

        bReturnValue = True
        'Check if the representation is OK
        'If nodTemp.getAttribute("numeric") = 1 Then
        If aTagsettings(1, iTagNumber) = 1 Then
            'New code 04.02.2008 to deal with amounts like 0.8752
            '12.01.2008 - added minus (not sure if that's correct
            If Not vbIsNumeric(sValue, "01234567890,.-") Then
                'Old code
                'If Not IsNumeric(sValue) Then
                Err.Raise(vbObjectError + 15042, , LRS(15042) _
  & vbCrLf & LRS(15038, sGroupNumber) _
  & vbCrLf & LRS(15030, sFilename) _
  & vbCrLf & LRS(15031, Str(lLINCounter)) _
  & vbCrLf & LRS(15032, Str(lSEQCounter)) _
  & vbCrLf & LRS(15036, sSegmentName) _
  & vbCrLf & LRS(15034, "T" & Str(iTagNumber)) _
  & vbCrLf & LRS(15035, sValue))

                '"The element should be numeric! " & vbCrLf & sTagName & ", " & sValue
                bReturnValue = False
            End If
        End If

        'Check if the length of the value is valid
        If bReturnValue Then
            'If Len(sValue) > Val(nodTemp.getAttribute("maxlength")) Then
            If Len(sValue) > aTagsettings(2, iTagNumber) + iEscapeCharAdded Then
                bRaiseError = False
                'Check if we have some companyspecific settings regarding allowed length
                ' for instance Alpharma
                sLocalLength = ""
                bLocalLengthSet = False
                For lCounter = 0 To eleTagElement.Attributes.length - 1
                    If eleTagElement.Attributes.Item(lCounter).nodeName = "allowedlength" Then
                        bLocalLengthSet = True
                        Exit For
                    End If
                Next
                If bLocalLengthSet Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object eleTagElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sLocalLength = eleTagElement.getAttribute("allowedlength")
                    If Len(sValue) > Val(sLocalLength) + iEscapeCharAdded Then
                        aTagsettings(2, iTagNumber) = Val(sLocalLength)
                        bRaiseError = True
                    End If
                Else
                    bRaiseError = True
                End If
                If bRaiseError Then
                    Err.Raise(vbObjectError + 15043, , LRS(15043, Str(aTagsettings(2, iTagNumber))) _
  & vbCrLf & LRS(15038, sGroupNumber) _
  & vbCrLf & LRS(15030, sFilename) _
  & vbCrLf & LRS(15031, Str(lLINCounter)) _
  & vbCrLf & LRS(15032, Str(lSEQCounter)) _
  & vbCrLf & LRS(15036, sSegmentName) _
  & vbCrLf & LRS(15034, "T" & Str(iTagNumber)) _
  & vbCrLf & LRS(15035, sValue))

                    bReturnValue = False
                Else
                    bReturnValue = True
                End If

                ' "The elementvalue is too long! Maxlength is nodTemp.getAttribute("maxlength") & vbCrLf & sTagName & ", " & sValue

            Else
                bReturnValue = True
            End If
        End If

        CheckSettings = bReturnValue

    End Function
    Private Function BuildArray(ByRef sSegString As String, ByRef bRemoveEscapeChar As Boolean) As String(,)
        Dim iTagGroupCounter As Short
        Dim iTagCounter As Short
        Dim iArrayCounter As Short
        Dim iPosition As Short
        'DOTNETT REDIM Dim aSegmentArray(,) As String
        Dim aSegmentArray(,) As String
        Dim sSign As String
        Dim iStartElement, iLengthElement As Short
        Dim bNewElement, bMOA As Boolean
        Dim iDecimalPosition As Short
        Dim iNoOfEscapeChar As Short

        iTagGroupCounter = 0
        iTagCounter = 1
        iArrayCounter = 0
        iStartElement = 0
        iLengthElement = 0
        iNoOfEscapeChar = 0
        bNewElement = False
        sSign = ""
        bMOA = False

        If Left(sSegString, 3) = "MOA" Then
            bMOA = True
        End If

        For iPosition = CDbl(sSegmentLen) + 1 To Len(sSegString)
            sSign = Mid(sSegString, iPosition, 1)
            iLengthElement = iLengthElement + 1
            Select Case sSign
                Case sEscapeChar '?
                    If bRemoveEscapeChar Then
                        sSegString = Left(sSegString, iPosition - 1) & Right(sSegString, Len(sSegString) - iPosition)
                    Else
                        iPosition = iPosition + 1
                        iLengthElement = iLengthElement + 1
                        iNoOfEscapeChar = iNoOfEscapeChar + 1
                    End If
                Case sElementSep ':
                    If bNewElement Then
                        If Not iLengthElement = 1 Then
                            aSegmentArray = AppendItem(iArrayCounter, iTagGroupCounter, iTagCounter, Mid(sSegString, iStartElement, iLengthElement - 1), aSegmentArray, iNoOfEscapeChar)
                            iArrayCounter = iArrayCounter + 1
                            iNoOfEscapeChar = 0
                        End If
                    End If
                    bNewElement = True
                    iStartElement = iPosition + 1
                    iLengthElement = 0
                    iTagCounter = iTagCounter + 1
                    '        sElement = GetElement(Right(sSegString, Len(sSegString) - iPosition))
                    '        If Not Len(sElement) = 0 Then
                    '            aSegmentArray() = AppendItem(iArrayCounter, iTagGroupCounter, iTagCounter, sElement, aSegmentArray())
                    '            iArrayCounter = iArrayCounter + 1
                    '        End If
                Case sGroupSep '+
                    If bNewElement Then
                        If Not iLengthElement = 1 Then
                            aSegmentArray = AppendItem(iArrayCounter, iTagGroupCounter, iTagCounter, Mid(sSegString, iStartElement, iLengthElement - 1), aSegmentArray, iNoOfEscapeChar)
                            iArrayCounter = iArrayCounter + 1
                            iNoOfEscapeChar = 0
                        End If
                    End If
                    bNewElement = True
                    iStartElement = iPosition + 1
                    iLengthElement = 0
                    iTagCounter = 1
                    iTagGroupCounter = iTagGroupCounter + 1
                    'sElement = GetElement(Right(sSegString, Len(sSegString) - iPosition))
                    '        If Not Len(sElement) = 0 Then
                    '            aSegmentArray() = AppendItem(iArrayCounter, iTagGroupCounter, iTagCounter, sElement, aSegmentArray())
                    '        iArrayCounter = iArrayCounter + 1
                    '        End If
                Case sSegmentSep ''
                    If bNewElement Then
                        If Not iLengthElement = 1 Then
                            aSegmentArray = AppendItem(iArrayCounter, iTagGroupCounter, iTagCounter, Mid(sSegString, iStartElement, iLengthElement - 1), aSegmentArray, iNoOfEscapeChar)
                            iArrayCounter = iArrayCounter + 1
                            iNoOfEscapeChar = 0
                        End If
                    End If
                    Exit For
                Case Else
            End Select
        Next iPosition

        'Add an dummyelement at the end of the array
        ReDim Preserve aSegmentArray(3, UBound(aSegmentArray, 2) + 1)
        aSegmentArray(0, UBound(aSegmentArray, 2)) = "-1"
        aSegmentArray(1, UBound(aSegmentArray, 2)) = "-1"
        aSegmentArray(2, UBound(aSegmentArray, 2)) = "-1"

        If bMOA Then
            For iPosition = 0 To UBound(aSegmentArray, 2)
                If aSegmentArray(1, iPosition) = "2" Then
                    iDecimalPosition = InStr(1, aSegmentArray(2, iPosition), sDecimalSep)
                    If iDecimalPosition = 0 Then
                        aSegmentArray(2, iPosition) = aSegmentArray(2, iPosition) & "00"
                    ElseIf Len(aSegmentArray(2, iPosition)) - iDecimalPosition = 2 Then
                        aSegmentArray(2, iPosition) = Replace(aSegmentArray(2, iPosition), sDecimalSep, "")
                    ElseIf Len(aSegmentArray(2, iPosition)) - iDecimalPosition = 1 Then
                        aSegmentArray(2, iPosition) = Replace(aSegmentArray(2, iPosition), sDecimalSep, "") & "0"
                        'New 28.02.2006 DNV Germany delivers amount with 3 numbers after the decimal
                    ElseIf Len(aSegmentArray(2, iPosition)) - iDecimalPosition = 3 Then
                        If Right(aSegmentArray(2, iPosition), 1) = "0" Then
                            aSegmentArray(2, iPosition) = Left(aSegmentArray(2, iPosition), Len(aSegmentArray(2, iPosition)) - 1)
                            aSegmentArray(2, iPosition) = Replace(aSegmentArray(2, iPosition), sDecimalSep, "")
                        Else
                            MsgBox("ERROR in amount", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "PARSE ERROR")
                        End If
                    Else
                        MsgBox("ERROR in amount", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "PARSE ERROR")
                    End If
                End If
            Next iPosition
        End If

        BuildArray = VB6.CopyArray(aSegmentArray)

    End Function
    Public Function GetSegmentInfo() As String(,)

        GetSegmentInfo = VB6.CopyArray(aSegmentInfo)

    End Function
	
	'DOTNETT REDIM Private Function AppendItem(iArrayCounter As Integer, iTagGroupCounter As Integer, iTagCounter As Integer, sElement As String, aSegmentArray(,) As String, iNoOfEscapeChar As Integer) As String()
    Private Function AppendItem(ByRef iArrayCounter As Short, ByRef iTagGroupCounter As Short, ByRef iTagCounter As Short, ByRef sElement As String, ByRef aSegmentArray(,) As String, ByRef iNoOfEscapeChar As Short) As String(,)
        'Dim aSegmentArray() As String

        ReDim Preserve aSegmentArray(3, iArrayCounter)
        aSegmentArray(0, iArrayCounter) = Trim(Str(iTagGroupCounter))
        aSegmentArray(1, iArrayCounter) = Trim(Str(iTagCounter))
        aSegmentArray(2, iArrayCounter) = sElement
        aSegmentArray(3, iArrayCounter) = Trim(Str(iNoOfEscapeChar))
        AppendItem = VB6.CopyArray(aSegmentArray)

    End Function
	Public Function InUse(ByRef nodElement As MSXML2.IXMLDOMElement) As Boolean
		'Check if a node is in use according to the mapping
		
		'UPGRADE_WARNING: Couldn't resolve default property of object nodElement.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If nodElement.getAttribute("inuse") = "1" Then
			InUse = True
		Else
			InUse = False
		End If
		
		'If nodElement.Attributes.Item(1).nodeValue = "1" Then
		
		
	End Function
	
	Public Function Mandatory(ByRef nodElement As MSXML2.IXMLDOMElement) As Boolean
		'Check if a node is mandatory an in use according to the mapping
		Dim bReturnValue As Boolean
		
		'check if it's mandatory
		'UPGRADE_WARNING: Couldn't resolve default property of object nodElement.getAttribute(mandatory). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If nodElement.getAttribute("mandatory") = "1" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object nodElement.getAttribute(inuse). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If nodElement.getAttribute("inuse") = "1" Then
				bReturnValue = True
			Else
				bReturnValue = False
			End If
		Else
			bReturnValue = False
		End If
		
		Mandatory = bReturnValue
		
	End Function
End Module
