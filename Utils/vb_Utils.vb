Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports System.IO
'Imports IWshRuntimeLibrary
Imports System.Threading

Module vb_Utils
    ' utils, some  collected from vb-net, some selfmade

    Dim aValidationError As New System.Collections.ArrayList
    Dim bValidationErrorExists As Boolean
    Dim bValidationWarningExists As Boolean

    Public Const SGRounding As Short = 5003
    Public Const SGAdjCharges As Short = 5017
    Public Const SGCreditNoteUsedFromAnotherDebtorAccount As Short = 5020
    'Public Const SGCreditNoteUsedOnAnotherAccount = 5020
    Public Const SGCreditNoteTakenByDebtorForAnotherAccount As Short = 5021
    'Public Const SGCreditNoteCreatedFromAnotherAccount = 5021
    Public Const SGCreditRounding As Short = 5022 'Paid to little
    Public Const SGDebitRounding As Short = 5027 'Paid to much
    Public Const SGInkassoInterest As Short = 5040 '5022
    Public Const SGDiscount As Short = 5041 '5023
    Public Const SGInkassoVAT As Short = 5042

    'Declarations for BrowseForFolders
    Public Structure BROWSEINFO
        Dim hOwner As Integer
        Dim pidlRoot As Integer
        Dim pszDisplayName As String
        Dim lpszTitle As String
        Dim ulFlags As Integer
        Dim lpfn As Integer
        Dim lParam As Integer
        Dim iImage As Integer
    End Structure
    Public Structure VS_FIXEDFILEINFO
        Dim dwSignature As Integer
        Dim dwStrucVersion As Integer 'e.g. 0x00000042 = "0.42"
        Dim dwFileVersionMS As Integer 'e.g. 0x00030075 = "3.75"
        Dim dwFileVersionLS As Integer 'e.g. 0x00000031 = "0.31"
        Dim dwProductVersionMS As Integer 'e.g. 0x00030010 = "3.10"
        Dim dwProductVersionLS As Integer 'e.g. 0x00000031 = "0.31"
        Dim dwFileFlagsMask As Integer '= 0x3F for version "0.42"
        Dim dwFileFlags As Integer 'e.g. VFF_DEBUG Or VFF_PRERELEASE
        Dim dwFileOS As Integer 'e.g. VOS_DOS_WINDOWS16
        Dim dwFileType As Integer 'e.g. VFT_DRIVER
        Dim dwFileSubtype As Integer 'e.g. VFT2_DRV_KEYBOARD
        Dim dwFileDateMS As Integer 'e.g. 0
        Dim dwFileDateLS As Integer 'e.g. 0
    End Structure
    Declare Function GetLongPathName Lib "kernel32" Alias "GetLongPathNameA" (ByVal lpShortPath As String, ByVal lpLongPath As String, ByRef ByValnBufferLength As Integer) As Integer
    'UPGRADE_WARNING: Structure BROWSEINFO may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    'Public Declare Function SHBrowseForFolder Lib "shell32" Alias "SHBrowseForFolderA" (ByRef lpBrowseInfo As BROWSEINFO) As Integer

    Public Declare Function SHGetPathFromIDList Lib "shell32" Alias "SHGetPathFromIDListA" (ByVal pidl As Integer, ByVal pszPath As String) As Integer

    'UPGRADE_NOTE: pv was upgraded to pv_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Public Declare Sub CoTaskMemFree Lib "ole32" (ByVal pv_Renamed As Integer)

    'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    'ChangeDone - As Any 
    'Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Integer, ByVal wMsg As Integer, ByVal wParam As Integer, ByRef lParam As Any) As Integer
    Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Integer, ByVal wMsg As Integer, ByVal wParam As Integer, ByRef lParam As Integer) As Integer

    'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    'ChangeDone - As Any - Created to declaration because we pass different types 
    'Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (ByRef pDest As Any, ByRef pSource As Any, ByVal dwLength As Integer)
    Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (ByRef pDest As Integer, ByRef pSource As String, ByVal dwLength As Integer)
    Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (ByRef pDest As VS_FIXEDFILEINFO, ByRef pSource As Integer, ByVal dwLength As Integer)

    Public Const MAX_PATH As Short = 260
    Public Const WM_USER As Integer = &H400
    Public Const BFFM_INITIALIZED As Short = 1

    'Constants ending in 'A' are for Win95 ANSI
    'calls; those ending in 'W' are the wide Unicode
    'calls for NT.

    'Sets the status text to the null-terminated
    'string specified by the lParam parameter.
    'wParam is ignored and should be set to 0.
    Public Const BFFM_SETSTATUSTEXTA As Integer = (WM_USER + 100)
    Public Const BFFM_SETSTATUSTEXTW As Integer = (WM_USER + 104)

    'If the lParam  parameter is non-zero, enables the
    'OK button, or disables it if lParam is zero.
    '(docs erroneously said wParam!)
    'wParam is ignored and should be set to 0.
    Public Const BFFM_ENABLEOK As Integer = (WM_USER + 101)

    'Selects the specified folder. If the wParam
    'parameter is FALSE, the lParam parameter is the
    'PIDL of the folder to select , or it is the path
    'of the folder if wParam is the C value TRUE (or 1).
    'Note that after this message is sent, the browse
    'dialog receives a subsequent BFFM_SELECTIONCHANGED
    'message.
    Public Const BFFM_SETSELECTIONA As Integer = (WM_USER + 102)
    Public Const BFFM_SETSELECTIONW As Integer = (WM_USER + 103)


    'specific to the PIDL method
    'Undocumented call for the example. IShellFolder's
    'ParseDisplayName member function should be used instead.
    Public Declare Function SHSimpleIDListFromPath Lib "shell32" Alias "#162" (ByVal szPath As String) As Integer


    'specific to the STRING method
    Public Declare Function LocalAlloc Lib "kernel32" (ByVal uFlags As Integer, ByVal uBytes As Integer) As Integer

    Public Declare Function LocalFree Lib "kernel32" (ByVal hMem As Integer) As Integer

    Public Const LMEM_FIXED As Integer = &H0
    Public Const LMEM_ZEROINIT As Integer = &H40
    Public Const lPtr As Boolean = (LMEM_FIXED Or LMEM_ZEROINIT)

    'windows-defined type OSVERSIONINFO
    Public Structure OSVERSIONINFO
        Dim OSVSize As Integer
        Dim dwVerMajor As Integer
        Dim dwVerMinor As Integer
        Dim dwBuildNumber As Integer
        Dim PlatformID As Integer
        'szCSDVersion    As String * 129   ' Must be changed from 128 to 129 in to vb.net
        'UPGRADE_WARNING: Fixed-length string size must fit in the buffer. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
        <VBFixedString(128), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=128)> Public szCSDVersion() As Char
    End Structure

    Public Const VER_PLATFORM_WIN32_NT As Short = 2

    'UPGRADE_WARNING: Structure OSVERSIONINFO may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Public Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (ByRef lpVersionInformation As OSVERSIONINFO) As Integer


    Public Declare Function GetLogicalDriveStrings Lib "kernel32" Alias "GetLogicalDriveStringsA" (ByVal nBufferLength As Integer, ByVal lpBuffer As String) As Integer

    '----------
    Public Enum ErrorMessageType
        Dont_Show = 0
        Use_ERR_Raise = 1
        Use_MsgBox = 2
    End Enum

    '**********************************************************************
    ' Constants for helpfile
    'Run the Help File
    'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    'Declare Function WinHelp Lib "user32"  Alias "WinHelpA"(ByVal hwndApp As Integer, ByVal lpHelpFile As String, ByVal wCommand As Integer, ByRef dwData As Any) As Integer

    'WinHelp structures
    Public Const HELPINFO_WINDOW As Integer = &H1
    Public Const HELPINFO_MENUITEM As Integer = &H2
    Structure POINTAPI
        Dim x As Integer
        Dim y As Integer
    End Structure
    Structure HELPINFO
        Dim cbSize As Integer
        Dim iContextType As Integer
        Dim iCtrlId As Integer
        Dim hItemHandle As Integer
        Dim dwContextId As Integer
        Dim MousePos As POINTAPI
    End Structure
    Structure HELPWININFO
        Dim wStructSize As Integer
        Dim x As Integer
        Dim y As Integer
        Dim dx As Integer
        Dim dy As Integer
        Dim wMax As Integer
        'UPGRADE_WARNING: Fixed-length string size must fit in the buffer. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
        <VBFixedString(2), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=2)> Public rgchMember() As Char
    End Structure

    'WinHelp API constants
    Public Const HELP_COMMAND As Integer = &H102
    Public Const HELP_CONTENTS As Integer = &H3
    Public Const HELP_CONTEXT As Integer = &H1
    Public Const HELP_CONTEXTMENU As Integer = &HA
    Public Const HELP_CONTEXTPOPUP As Integer = &H8
    Public Const HELP_CONTEXTNOFOCUS As Integer = &H108
    Public Const HELP_POPUPID As Integer = &H104
    Public Const HELP_FINDER As Integer = &HB
    Public Const HELP_FORCEFILE As Integer = &H9
    Public Const HELP_HELPONHELP As Integer = &H4
    Public Const HELP_INDEX As Integer = &H3
    Public Const HELP_KEY As Integer = &H101
    Public Const HELP_MULTIKEY As Integer = &H201
    Public Const HELP_PARTIALKEY As Integer = &H105
    Public Const HELP_CLOSEWINDOW As Integer = &H107
    Public Const HELP_QUIT As Integer = &H2
    Public Const HELP_SETCONTENTS As Integer = &H5
    Public Const HELP_SETINDEX As Integer = &H5
    Public Const HELP_SETWINPOS As Integer = &H203
    Public Const HELP_SETPOPUP_POS As Integer = &HD
    Public Const HELP_TCARD As Integer = &H8000
    Public Const HELP_TCARD_DATA As Integer = &H10
    Public Const HELP_TCARD_OTHER_CALLER As Integer = &H11
    Public Const HELP_WM_HELP As Integer = &HC

    Public Const HELPMSGSTRING As String = "commdlg_help"



    ' End of Helpfile constants

    'Private Declare Function GetTickCount& Lib "kernel32" ()

    '------------ For use with FolderPicker --------------------



    ' Div. constants for SHBrowseForFolders
    Public Const BIF_RETURNONLYFSDIRS As Integer = &H1 'Only return file system directories.
    'If the user selects folders that are not
    'part of the file system, the OK button is grayed.

    Public Const BIF_DONTGOBELOWDOMAIN As Integer = &H2 'Do not include network folders below the
    'domain level in the tree view control

    Public Const BIF_STATUSTEXT As Integer = &H4 'Include a status area in the dialog box.
    'The callback function can set the status text
    'by sending messages to the dialog box.

    Public Const BIF_RETURNFSANCESTORS As Integer = &H8 'Only return file system ancestors. If the user selects
    'anything other than a file system ancestor, the OK button is grayed

    Public Const BIF_EDITBOX As Integer = &H10 'Version 4.71. The browse dialog includes an edit control
    'in which the user can type the name of an item.

    Public Const BIF_VALIDATE As Integer = &H20 'Version 4.71. If the user types an invalid name into the
    'edit box, the browse dialog will call the application's
    'BrowseCallbackProc with the BFFM_VALIDATEFAILED
    ' message. This flag is ignored if BIF_EDITBOX is not specified

    Public Const BIF_NEWDIALOGSTYLE As Integer = &H40 'Version 5.0. New dialog style with context menu and resizability

    Public Const BIF_BROWSEINCLUDEURLS As Integer = &H80 'Version 5.0. Allow URLs to be displayed or entered. Requires BIF_USENEWUI.

    Public Const BIF_BROWSEFORCOMPUTER As Integer = &H1000 'Only return computers. If the user selects anything
    'other than a computer, the OK button is grayed

    Public Const BIF_BROWSEFORPRINTER As Integer = &H2000 'Only return printers. If the user selects anything
    'other than a printer, the OK button is grayed.

    Public Const BIF_BROWSEINCLUDEFILES As Integer = &H4000 'The browse dialog will display files as well as folders

    Public Const BIF_SHAREABLE As Integer = &H8000 'Version 5.0.  Allow display of remote shareable resources.  Requires BIF_USENEWUI.

    '--------------- For use with language DLL's--------------------------
    Public Declare Function GetUserDefaultLCID Lib "kernel32" () As Integer

    ' Object reference to the DLL that contains the resources
    ' to be loaded.
    'Private clsSatellite As Object
    'TODO remove clsSatellite when new LRS is OK
    'Public clsSatellite As Object

    'Private clsCommonSatellite As Object

    '' For createShortcut
    'Private Declare Function CreateShellLink Lib "vb6stkit.dll" Alias "fCreateShellLink" (ByVal lpstrFolderName As String, ByVal lpstrLinkName As String, ByVal lpstrLinkPath As String, ByVal lpstrLinkArguments As String, ByVal fPrivate As Integer, ByVal sParent As String) As Integer

    'Private Declare Function SHGetSpecialFolderLocation Lib "shell32.dll" (ByVal hwndOwner As Integer, ByVal Folder As Integer, ByRef idl As Integer) As Integer

    'Public Enum CSIDL_clsFiles
    '    CSIDL_DESKTOP_cF = &H0
    '    CSIDL_PROGRAMS_cF = &H2
    '    CSIDL_CONTROLS_cF = &H3
    '    CSIDL_PRINTERS_cF = &H4
    '    CSIDL_PERSONAL_cF = &H5
    '    CSIDL_FAVORITES_cF = &H6
    '    CSIDL_STARTUP_cF = &H7
    '    CSIDL_RECENT_cF = &H8
    '    CSIDL_SENDTO_cF = &H9
    '    CSIDL_BITBUCKET_cF = &HA
    '    CSIDL_STARTMENU_cF = &HB
    '    CSIDL_DESKTOPDIRECTORY_cF = &H10
    '    CSIDL_DRIVES_cF = &H11
    '    CSIDL_NETWORK_cF = &H12
    '    CSIDL_NETHOOD_cF = &H13
    '    CSIDL_FONTS_cF = &H14
    '    CSIDL_TEMPLATES_cF = &H15
    '    CSIDL_COMMON_STARTMENU_cF = &H16
    '    CSIDL_COMMON_PROGRAMS_cF = &H17
    '    CSIDL_COMMON_STARTUP_cF = &H18
    '    CSIDL_COMMON_DESKTOPDIRECTORY_cF = &H19
    '    CSIDL_APPDATA_cF = &H1A
    '    CSIDL_PRINTHOOD_cF = &H1B
    'End Enum

    'Private Declare Function GetWindowsDirectory Lib "kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Integer) As Integer

    'Private Declare Function GetSystemDirectory Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Integer) As Integer

    ' For use with connecting a file extension in Explorer to f.ex. BabelBank
    'Public Const REG_SZ As Integer = &H1
    'Public Const REG_DWORD As Integer = &H4
    'Public Const HKEY_CLASSES_ROOT As Integer = &H80000000
    'Public Const HKEY_CURRENT_USER As Integer = &H80000001
    'Public Const HKEY_LOCAL_MACHINE As Integer = &H80000002
    'Public Const HKEY_USERS As Integer = &H80000003

    'Public Const ERROR_SUCCESS As Integer = 0
    'Public Const ERROR_BADDB As Integer = 1009
    'Public Const ERROR_BADKEY As Integer = 1010
    'Public Const ERROR_CANTOPEN As Integer = 1011
    'Public Const ERROR_CANTREAD As Integer = 1012
    'Public Const ERROR_CANTWRITE As Integer = 1013
    'Public Const ERROR_OUTOFMEMORY As Integer = 14
    'Public Const ERROR_INVALID_PARAMETER As Integer = 87
    'Public Const ERROR_ACCESS_DENIED As Integer = 5
    'Public Const ERROR_MORE_DATA As Integer = 234
    'Public Const ERROR_NO_MORE_ITEMS As Integer = 259

    'Public Const KEY_ALL_ACCESS As Integer = &H3F
    'Public Const REG_OPTION_NON_VOLATILE As Integer = 0

    'Public Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Integer) As Integer

    'Public Declare Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" (ByVal hKey As Integer, ByVal lpSubKey As String, ByVal RESERVED As Integer, ByVal lpClass As String, ByVal dwOptions As Integer, ByVal samDesired As Integer, ByVal lpSecurityAttributes As Integer, ByRef phkResult As Integer, ByRef lpdwDisposition As Integer) As Integer

    'Public Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Integer, ByVal lpSubKey As String, ByVal ulOptions As Integer, ByVal samDesired As Integer, ByRef phkResult As Integer) As Integer

    'Public Declare Function RegSetValueExString Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Integer, ByVal lpValueName As String, ByVal RESERVED As Integer, ByVal dwType As Integer, ByVal lpvalue As String, ByVal cbData As Integer) As Integer

    'Public Declare Function RegSetValueExLong Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Integer, ByVal lpValueName As String, ByVal RESERVED As Integer, ByVal dwType As Integer, ByRef lpvalue As Integer, ByVal cbData As Integer) As Integer

    'Public Declare Function DeleteKey Lib "advapi32" Alias "RegDeleteKeyA" (ByVal hKey As Integer, ByVal lpszSubKey As String) As Integer

    '*************API used to retreive the memory-status
    Private Structure MEMORYSTATUS
        Dim dwLength As Integer
        Dim dwMemoryLoad As Integer
        Dim dwTotalPhys As Integer
        Dim dwAvailPhys As Integer
        Dim dwTotalPageFile As Integer
        Dim dwAvailPageFile As Integer
        Dim dwTotalVirtual As Integer
        Dim dwAvailVirtual As Integer
    End Structure
    'UPGRADE_WARNING: Structure MEMORYSTATUS may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Private Declare Sub GlobalMemoryStatus Lib "kernel32" (ByRef lpBuffer As MEMORYSTATUS)
    '************End of API to retreive memory
    ' ----------- Color on  progressbar -------------
    Private Const CCM_FIRST As Integer = &H2000
    Private Const CCM_SETBKCOLOR As Integer = (CCM_FIRST + 1)

    'set progressbar backcolor in IE3 or later
    Private Const PBM_SETBKCOLOR As Integer = CCM_SETBKCOLOR

    'set progressbar barcolor in IE4 or later
    Private Const PBM_SETBARCOLOR As Integer = (WM_USER + 9)


    '---------- end color on progressbar ----

    'Changed 27.04.2005
    'Before
    'Public Const TypeGLAccount = 1
    'Public Const TypeCustomerNoAccount = 2
    'New
    Public Const TypeCustomerNoAccount As Short = 1
    Public Const TypeGLAccount As Short = 2
    Public Const TypeKIDNumber As Short = 3
    Public Const TypeFromERP As Short = 4
    Public Const TypeDontPostSeparately As Short = 5
    Public Const TypeSupplierAccount As Short = 6
    'See also in BabelFiles
    'Public Enum MatchType
    '    OpenInvoice = -1
    '    MatchedOnInvoice = 0
    '    MatchedOnCustomer = 1
    '    MatchedOnGL = 2
    '    MatchedOnSupplier = 3
    'End Enum
    '-------------------------------------------------------------------------------------------
    '---------------- FUNCTIONS TO READ/WRITE FROM/TO INI-FILES --------------------------------
    '-------------------------------------------------------------------------------------------
    ' Check link http://www.mvps.org/vbnet/index.html?code/file/pprofilebasic.htm
    ' Copyright �1996-2002 VBnet, Randy Birch, All Rights Reserved.
    ' Some pages may also contain other copyrights by the author.
    'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    'ChangeDone - As Any 
    'Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpSectionName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpSectionName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer

    'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    'ChangeDone - As Any 
    'Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpSectionName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Integer
    Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpSectionName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer

    'DECLARATIONS USED TO MANIPULATE THE CLIPBOARD FROM VB
    Declare Function EmptyClipboard Lib "user32" () As Integer
    Declare Function SetClipboardData Lib "user32" (ByVal wFormat As Integer, ByVal hMem As Integer) As Integer
    Declare Function OpenClipboard Lib "user32" (ByVal hwnd As Integer) As Integer
    Declare Function CloseClipboard Lib "user32" () As Integer
    Declare Function GetClipboardData Lib "user32" (ByVal wFormat As Integer) As Integer
    Declare Function GlobalAlloc Lib "kernel32" (ByVal wFlags As Integer, ByVal dwBytes As Integer) As Integer
    Declare Function GlobalLock Lib "kernel32" (ByVal hMem As Integer) As Integer
    Declare Function GlobalUnlock Lib "kernel32" (ByVal hMem As Integer) As Integer
    Declare Function GlobalSize Lib "kernel32" (ByVal hMem As Integer) As Integer
    'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    'ChangeDone - As Any - Different types used in BB.EXE
    'Declare Function lstrcpy Lib "kernel32" (ByVal lpString1 As Any, ByVal lpString2 As Any) As Integer
    Declare Function lstrcpy Lib "kernel32" (ByVal lpString1 As Integer, ByVal lpString2 As String) As Integer
    Declare Function lstrcpy Lib "kernel32" (ByVal lpString1 As String, ByVal lpString2 As Integer) As Integer

    Public Const GHND As Integer = &H42
    Public Const CF_TEXT As Short = 1
    Public Const MAXSIZE As Short = 4096

    'Declarations use to change the menubar on forms starts here -------

    Public Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Integer, ByVal hWndInsertAfter As Integer, ByVal x As Integer, ByVal y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer) As Integer
    ' Declarations for changing the menubar on forms ends after next line -----------
    Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Integer, ByVal nIndex As Integer, ByVal dwNewLong As Integer) As Integer
    Private Declare Function DrawMenuBar Lib "user32" (ByVal hwnd As Integer) As Integer
    'Win32 API Function Declarations
    Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Integer, ByVal nIndex As Integer) As Integer


    '   Private Declare Function PathIsUNC Lib "shlwapi" _
    ''   Alias "PathIsUNCA" _
    ''  (ByVal pszPath As String) As Long
    'Public Function IsUNCPathValid(ByVal sPath As String) As Boolean
    '
    '  'Determines if the string is a valid UNC
    '  '(universal naming convention) for a server
    '  'and share path. Returns True if the string
    '  'is a valid UNC path, or False otherwise.
    '
    '   IsUNCPathValid = PathIsUNC(sPath) = 1
    '
    'End Function
    Private Declare Function SetEnvironmentVariable Lib "kernel32" Alias "SetEnvironmentVariableA" (ByVal lpName As String, ByVal lpvalue As String) As Integer
    Private Declare Function GetEnvironmentVariable Lib "kernel32" Alias "GetEnvironmentVariableA" (ByVal lpName As String, ByVal lpBuffer As String, ByVal nSize As Integer) As Integer
    Public Sub ProfileSaveItem(ByRef lpSectionName As String, ByRef lpKeyName As String, ByRef lpvalue As String, ByRef inifile As String)

        'This function saves the passed value to the file,
        'under the section and key names specified.
        'If the ini file does not exist, it is created.
        'If the section does not exist, it is created.
        'If the key name does not exist, it is created.
        'If the key name exists, it's value is replaced.

        Call WritePrivateProfileString(lpSectionName, lpKeyName, lpvalue, inifile)

    End Sub
    Public Function ProfileGetItem(ByRef lpSectionName As String, ByRef lpKeyName As String, ByRef defaultValue As String, ByRef inifile As String) As String

        'Retrieves a value from an ini file corresponding
        'to the section and key name passed.

        Dim success As Integer
        Dim nSize As Integer
        Dim ret As String

        'call the API with the parameters passed.
        'The return value is the length of the string
        'in ret, including the terminating null. If a
        'default value was passed, and the section or
        'key name are not in the file, that value is
        'returned. If no default value was passed (""),
        'then success will = 0 if not found.

        'Pad a string large enough to hold the data.
        ret = Space(2048)
        nSize = Len(ret)
        success = GetPrivateProfileString(lpSectionName, lpKeyName, defaultValue, ret, nSize, inifile)

        If success Then
            ProfileGetItem = Left(ret, success)
        End If

    End Function


    Public Sub ProfileDeleteItem(ByRef lpSectionName As String, ByRef lpKeyName As String, ByRef inifile As String)

        'this call will remove the keyname and its
        'corresponding value from the section specified
        'in lpSectionName. This is accomplished by passing
        '"" as the lpValue parameter. For example,
        'assuming that an ini file had:
        '  [Colours]
        '  Colour1=Red
        '  Colour2=Blue
        '  Colour3=Green
        '
        'and this sub was called passing "Colour2"
        'as lpKeyName, the resulting ini file
        'would contain:
        '  [Colours]
        '  Colour1=Red
        '  Colour3=Green

        Call WritePrivateProfileString(lpSectionName, lpKeyName, "", inifile)

    End Sub


    Public Sub ProfileDeleteSection(ByRef lpSectionName As String, ByRef inifile As String)

        'this call will remove the entire section
        'corresponding to lpSectionName. This is
        'accomplished by passing ""
        'as both the lpKeyName and lpValue parameters.
        'For example, assuming that an ini file had:
        '  [Colours]
        '  Colour1=Red
        '  Colour2=Blue
        '  Colour3=Green
        '
        'and this sub was called passing "Colours"
        'as lpSectionName, the resulting Colours
        'section in the ini file would be deleted.

        Call WritePrivateProfileString(lpSectionName, "", "", inifile)

    End Sub
    '---------------- End of block with Functions for ini-files --------------------------------



    'Public Declare Function GetSystemDirectory Lib "Kernel" (ByVal lpBuffer As String, ByVal nSize As Integer) As Integer
    'Public Declare Function GetWindowsDirectory Lib "Kernel" (ByVal lpBuffer As String, ByVal nSize As Integer) As Integer
    'Public Function GetSpecialFolder(ByRef CSIDL As CSIDL_clsFiles) As String

    '    ' to find the desktop folder, past in the CSIDL_DESKTOP_cF value as the param (it is = to &H0)
    '    'a few local variables needed
    '    Dim r As Integer
    '    Dim sPath As String
    '    Dim pidl As Integer

    '    Const NOERROR As Short = 0
    '    Const MAX_LENGTH As Short = 260

    '    'fill pidl with the specified folder item
    '    r = SHGetSpecialFolderLocation(0, CSIDL, pidl)

    '    If r = NOERROR Then

    '        'Of the structure is filled, initialize and
    '        'retrieve the path from the id list, and return
    '        'the folder with a trailing slash appended.
    '        sPath = Space(MAX_LENGTH)
    '        r = SHGetPathFromIDList(pidl, sPath)

    '        If r Then
    '            GetSpecialFolder = Left(sPath, InStr(sPath, Chr(0)) - 1) & "\"
    '        End If

    '    End If

    'End Function


    Sub Pause(ByRef iSeconds As Double)
        Dim nStart As Double

        ' wait for n seconds
        nStart = VB.Timer() ' Set start time.
        Do While VB.Timer() < nStart + iSeconds
            System.Windows.Forms.Application.DoEvents() ' Yield to other processes.
        Loop

    End Sub
    Function xDelim(ByRef sInString As String, ByRef sDelimiter As String, ByRef iFieldNo As Short, Optional ByRef sSurround As String = "", Optional ByRef sFilename As String = "", Optional ByRef sFieldname As String = "") As String

        ' Return a portion from a delimited string
        ' sInString:    Delimited string
        ' sDelimiter:   The character used as delimiter (e.g. , or ;)
        ' iFieldNo:     Number of column or field to extract
        ' sSurround:    (optional) If fields are surrounded with a character, (e.g. "),
        '               strip returnfield from that character
        '
        ' 12.02.01      Jan-Petter Skogvang, Visual Banking
        '--------------------------------------------------------------------------------

        Dim iFirstDelimPos, iLastDelimPos As Short
        Dim i As Short
        Dim iStartPos As Short
        Dim sRetvalue As String
        Dim bWithinField As Boolean
        Dim iCurrentField As Short
        Dim sErrorMessage As String

        On Error GoTo xDelimError

        If Len(Trim(sInString)) > 0 Then
            iStartPos = 0
            If iFieldNo = 1 Then
                'return start of string
                If InStr(1, sInString, sDelimiter) > 0 Then
                    sRetvalue = Left(sInString, InStr(1, sInString, sDelimiter) - 1)
                Else
                    ' New 15.09.05 JanP
                    sRetvalue = sInString
                End If
            Else
                ' find pos for delimiter before desired field

                'New 21.06.02 JanP
                ' When using surroundchars (f.ex. "), there is a possible problemsituation:
                ' the delimitersign may be a part of the string tp be returned.
                ' f.ex. "Adresse1, Address2","City",.... is possible
                ' Use then surroundchar as last pos:
                If Len(sSurround) > 0 Then
                    'New code 08.04-2003. Set iStartPos = 1, because the expression
                    '   Mid$(sInString, iStartPos, 1) is not valid when iStartPos = 0
                    iStartPos = 1
                    bWithinField = False 'Not inside " " or '  ', etc
                    For i = 1 To Len(sInString) ' start with delimiter before field
                        ' Must read one char at a time, until we have found our field;
                        If Mid(sInString, iStartPos, 1) = sSurround Then
                            If bWithinField Then
                                bWithinField = False
                            Else
                                bWithinField = True
                            End If
                            'iStartPos = iStartPos + 1
                        End If

                        ' find pos for delimiters, until we reach the one before our field:
                        If Mid(sInString, iStartPos, 1) = sDelimiter Then
                            If Not bWithinField Then
                                iCurrentField = iCurrentField + 1
                                If iCurrentField = iFieldNo - 1 Then
                                    ' found startpos for our field
                                    Exit For
                                End If
                            End If
                        End If

                        iStartPos = iStartPos + 1
                    Next  'i
                    ' iStartPos is now the pos for delimiter before our field
                    ' If iStartpos = 0 then the number of Delimiters are fewer than the
                    ' number found in the string. Return ""
                    'Changed 31.03.2005 from returning "DELIMERROR" to ""
                    If iStartPos = 0 Then
                        'xDelim = "DELIMERROR"
                        xDelim = ""
                        Exit Function
                    End If
                    iFirstDelimPos = iStartPos

                    ' New by JanP 28.06.02;
                    ' Current field can be surrounded by sSurround or not.
                    If Mid(sInString, iStartPos + 1, 1) = sSurround Then
                        ' Surrounded by sSurroundPos for delimiter after our field
                        iLastDelimPos = InStr(iStartPos + 2, sInString, sSurround) + 1
                    Else
                        ' This field has no surroundchars
                        ' JANP 29.04.2003:
                        ' This must be wrong!
                        ' In a case with empty fields, and separators close to each other
                        ' we jump one char to far (ex. ;;;;;NOK;;123,23;;;;)
                        ' The nonused fields will return ; instead of ""
                        'iLastDelimPos = InStr(iStartPos + 2, sInString, sDelimiter)
                        ' Changed to
                        iLastDelimPos = InStr(iStartPos + 1, sInString, sDelimiter)
                    End If
                Else

                    For i = 1 To iFieldNo - 1 ' start with delimiter before field
                        ' find pos for delimiters, until we reach the one before our field:
                        iStartPos = InStr(iStartPos + 1, sInString, sDelimiter)
                        If iStartPos = 0 Then Exit For
                    Next  'i
                    ' iStartPos is now the pos for delimiter before our field
                    ' If iStartpos = 0 then the number of Delimiters are fewer than the
                    ' number found in the string. Return ""
                    'Changed 31.03.2005 from returning "DELIMERROR" to ""
                    If iStartPos = 0 Then
                        'xDelim = "DELIMERROR"
                        xDelim = ""
                        Exit Function
                    End If
                    iFirstDelimPos = iStartPos

                    ' Pos for delimiter after our field
                    iLastDelimPos = InStr(iStartPos + 1, sInString, sDelimiter)
                End If

                'Sometimes the last field doesn't end with the delimiter.
                'If so read the rest of the line
                If iLastDelimPos < iFirstDelimPos Then
                    iLastDelimPos = Len(sInString) + 1
                End If
                If iLastDelimPos > iFirstDelimPos + 1 Then
                    sRetvalue = Mid(sInString, iFirstDelimPos + 1, iLastDelimPos - iFirstDelimPos - 1)
                Else
                    sRetvalue = ""
                End If
            End If

            ' Strip characters around fields;
            If Len(sSurround) > 0 Then
                'sRetvalue = Strip(sRetvalue, sSurround)
                ' 03.10.2008 - by JanP
                ' if we f.ex have , "80273891" , in the file, then we pass the leading and ending spaces
                ' when surround char is used, I assume that we are not interested in spaces before and after ""
                ' thus, we add trim/rtrim of space OUTSIDE surround
                sRetvalue = Strip(RTrim(Trim(sRetvalue)), sSurround)
            End If
            ' wait with sSurroundstripping ...
            xDelim = sRetvalue
        Else
            xDelim = ""
        End If

        Exit Function

xDelimError:
        sErrorMessage = LRS(11002) & vbCrLf & vbCrLf
        sErrorMessage = sErrorMessage & LRS(11003) & sInString
        If Len(sFilename) > 0 Then
            sErrorMessage = sErrorMessage & vbCrLf & LRS(20007) & sFilename
        End If
        sErrorMessage = sErrorMessage & vbCrLf & LRS(11005) & Trim(Str(iFieldNo))
        If Len(sFieldname) > 0 Then
            sErrorMessage = sErrorMessage & vbCrLf & LRS(11004) & sFieldname
        End If

        Err.Raise(11002, , sErrorMessage)

    End Function

    'Public Function FolderPicker(frm As Form, Optional sTitle As String = "", Optional bPickFiles As Boolean = True)
    '' 31.03.2009 DOTTNET - removed, not in use any longer
    '' Source from VBNet
    '' Folder-select
    '
    '  Dim BI As BROWSEINFO
    '  Dim pidl As Long
    '  Dim path As String
    ''  Dim pos As Integer
    '
    '' Dim lblSelected As String
    '
    ' 'Fill the BROWSEINFO structure with the
    ' 'needed data. To accomodate comments, the
    ' 'With/End With sytax has not been used, though
    ' 'it should be your 'final' version.
    '
    ' 'hwnd of the window that receives messages
    ' 'from the call. Can be your application
    ' 'or the handle from GetDesktopWindow().
    '  'bi.hOwner = Me.hWnd
    'BI.hOwner = frm.hwnd
    ' 'Pointer to the item identifier list specifying
    ' 'the location of the "root" folder to browse from.
    ' 'If NULL, the desktop folder is used.
    '  BI.pidlRoot = 0&
    '
    ' 'message to be displayed in the Browse dialog
    '  BI.lpszTitle = sTitle '"Velg katalog"
    '
    ' 'the type of folder to return.
    ' If bPickFiles Then
    '    BI.ulFlags = BIF_BROWSEINCLUDEFILES '+ BIF_EDITBOX  '+ BIF_NEWDIALOGSTYLE
    ' Else
    '    BI.ulFlags = BIF_RETURNONLYFSDIRS ' Original !
    ' End If
    '
    ' 'show the browse for folders dialog
    '  pidl = SHBrowseForFolder(BI)
    '
    ' 'the dialog has closed, so parse & display the
    ' 'user's returned folder selection contained in pidl
    ' path = Space$(MAX_PATH)
    '
    '  If SHGetPathFromIDList(ByVal pidl, ByVal path) Then
    '     'pos = InStr(path, Chr$(0))
    '     'lblSelected = Left(path, pos - 1)
    '  End If
    '
    '  Call CoTaskMemFree(pidl)
    '
    '  FolderPicker = path
    '
    'End Function

    Public Function Filenamepicker(ByVal dlgFileNameOpen As System.Windows.Forms.OpenFileDialog, ByVal bMultiSelect As Boolean, Optional ByVal sTitle As String = "", Optional ByVal sInitialString As String = "", Optional ByVal sFilter As String = "") As String()
        'sFilter settes p� f�lgende m�te
        ' sFilter = "Text files (*.txt)|*.txt|" & "All files|*.*"

        Dim aFilenames As String()
        Dim sFilename As String
        Dim sDirectory As String
        Dim bExit As Boolean
        Dim iCounter As Integer

        bExit = False
        iCounter = 0
        If Not EmptyString(sInitialString) Then
            sDirectory = sInitialString
            Do Until bExit
                iCounter = iCounter + 1
                If DetermineIfDirectoryExists(sDirectory) Then
                    bExit = True
                Else
                    sDirectory = Mid$(sDirectory, 1, InStrRev(sDirectory, "\", , Microsoft.VisualBasic.CompareMethod.Text) - 1)
                    'If InStr(Mid$(sDirectory, InStrRev(sDirectory, "\", , CompareMethod.Text)), ".", CompareMethod.Text) > 0 Then
                End If
                If iCounter = 20 Then
                    sDirectory = ""
                    bExit = True
                End If
            Loop
        End If

        '.DefaultExt 'Get/Set default filename extension
        '.FileName 'Get and set the filename - flytt til Array aFileNames
        '.Filenames
        '.InitialDirectory 'Get or sets initial directory - sInitialString
        '.Multiselect 'Multiple files to be selected - bMultiSelect

        With dlgFileNameOpen

            .AutoUpgradeEnabled = True 'Enables Vista-look on Vista platforms

            If Not EmptyString(sDirectory) Then
                .InitialDirectory = sDirectory
            End If
            If Not EmptyString(sTitle) Then
                .Title = sTitle
            End If
            If Not EmptyString(sFilter) Then
                .Filter = sFilter
            End If
            .Multiselect = bMultiSelect

            'setting filters so that Text files and All Files choice appears in the Files of Type box
            'in the dialog
            If .ShowDialog() = DialogResult.OK Then
                'showDialog method makes the dialog box visible at run time
                If bMultiSelect Then
                    aFilenames = .FileNames
                Else
                    sFilename = .FileName
                    ReDim Preserve aFilenames(0)
                    aFilenames(0) = sFilename
                End If
            End If

        End With

        Filenamepicker = aFilenames

    End Function
    Private Function DetermineIfDirectoryExists(ByVal sDirName As String) As Boolean
        'NBNBNBNBNB Imports System.IO must be set in the Declaration part of the module

        Dim dDir As New DirectoryInfo(sDirName)

        If dDir.Exists Then
            DetermineIfDirectoryExists = True
        Else
            DetermineIfDirectoryExists = False
        End If
    End Function
    'CONVERTED FUNCTION - Old
    'Public Function FilenamePicker(ByRef CommonDialog1 As Object, Optional ByRef sFilename As String = "") As Object
    '    ' From VB Net:
    '    ' Show Common Dialog for finding files,
    '    ' return one or more files, in an array

    '    'working variables
    '    Dim c As Short
    '    Dim y As Short
    '    Dim sFile As String
    '    Dim startStrg As String
    '    Dim sPath As String

    '    'dim an array to hold the files selected
    '    Dim filearray() As String

    '    'set the max buffer large enough to retrieve multiple files
    '    'UPGRADE_WARNING: Couldn't resolve default property of object CommonDialog1.MaxFileSize. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    CommonDialog1.MaxFileSize = 4096
    '    'UPGRADE_WARNING: Couldn't resolve default property of object CommonDialog1.Filename. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    CommonDialog1.Filename = sFilename
    '    'UPGRADE_WARNING: Couldn't resolve default property of object CommonDialog1.Filter. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    CommonDialog1.Filter = "All Files|*.*"
    '    'UPGRADE_WARNING: Couldn't resolve default property of object CommonDialog1.Flags. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    'UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
    '    'UPGRADE_ISSUE: Constant cdlOFNAllowMultiselect was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
    '    CommonDialog1.Flags = MSComDlg.FileOpenConstants.cdlOFNAllowMultiselect Or MSComDlg.FileOpenConstants.cdlOFNExplorer
    '    'UPGRADE_WARNING: Couldn't resolve default property of object CommonDialog1.CancelError. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    CommonDialog1.CancelError = True
    '    On Error Resume Next

    '    'UPGRADE_WARNING: Couldn't resolve default property of object CommonDialog1.ShowOpen. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    CommonDialog1.ShowOpen() ' = 1

    '    If Err.Number <> 0 Then
    '        ' user pressed cancel ?
    '        ' just quit, with empty array. Test in calling routine!
    '    Else

    '        'assign the string returned from the
    '        'common dialog to startStrg for further processing.
    '        'Note that two "nulls" are appended to the
    '        'end of the string.  This is for use in the StripItem
    '        'routine below.

    '        'UPGRADE_WARNING: Couldn't resolve default property of object CommonDialog1.Filename. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '        startStrg = CommonDialog1.Filename & Chr(0) & Chr(0)

    '        'Extract each returned filename.
    '        'If only 1 file was selected, then the string
    '        'contains the fully-qualified path to the file.

    '        'If more than 1 string file was selected, the
    '        'string contains the path as the first item,
    '        'and the FileArray as the rest of the string,
    '        'each separated by a space.
    '        ' Must test for canceled dialog by user;
    '        'UPGRADE_WARNING: Couldn't resolve default property of object CommonDialog1.Filename. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '        If Len(CommonDialog1.Filename) > 0 Then
    '            'UPGRADE_WARNING: Couldn't resolve default property of object CommonDialog1.Filename. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            For c = 1 To Len(CommonDialog1.Filename)

    '                'extract 1 item from the string
    '                sFile = StripItem(startStrg, Chr(0))

    '                'if nothing's there, we're done
    '                If sFile = "" Then Exit For

    '                'ReDim the filename array
    '                'to add the new file. FileArray(0) is either the
    '                'path (if more than 1 file selected), or the
    '                'fully qualified filename (if only 1 file selected).
    '                ReDim Preserve filearray(y)
    '                If y = 0 Then
    '                    filearray(y) = LCase(sFile)
    '                    sPath = LCase(sFile) ' extract pathname
    '                ElseIf y = 1 Then
    '                    filearray(y - 1) = sPath & "\" & LCase(sFile)
    '                Else
    '                    filearray(y - 1) = sPath & "\" & LCase(sFile)
    '                End If


    '                'increment y by 1 for the next pass
    '                y = y + 1

    '            Next

    '            If y > 1 Then
    '                ReDim Preserve filearray(y - 2)
    '            End If
    '            If UBound(filearray) = 0 Then
    '                ' only one file, do a litle trick to ensure that we always
    '                ' have filename, with full path from FileArray(1) and upwards:
    '                ReDim Preserve filearray(0)
    '                filearray(0) = sPath
    '            End If

    '            'return array with one or more filename, including path
    '            'FileArray(0) should not be used, only fraom 1 an upwards
    '        End If
    '    End If ' err = 0
    '    'UPGRADE_WARNING: Couldn't resolve default property of object FilenamePicker. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    FilenamePicker = VB6.CopyArray(filearray)

    'End Function


    Function StripItem(ByRef startStrg As String, ByRef parser As String) As String
        'this takes a string separated by the chr passed in Parser,
        'splits off 1 item, and shortens startStrg so that the next
        'item is ready for removal.

        Dim c As Short
        Dim Item As String

        c = 1

        Do

            If Mid(startStrg, c, 1) = parser Then

                Item = Mid(startStrg, 1, c - 1)
                startStrg = Mid(startStrg, c + 1, Len(startStrg))
                StripItem = Item
                Exit Function
            End If

            c = c + 1

        Loop

    End Function
    Public Function Array_IsEmpty(ByRef x As Object) As Boolean
        ' test for empty arrays

        Dim DummyVAL As Integer '// Dummy Variable

        '// Set to Trap Error, if Any
        On Error Resume Next

        '08.12.2010
        If x Is Nothing Then
            Array_IsEmpty = True
            Exit Function
        End If

        '// Try and get the Lower Bound of the Array
        DummyVAL = LBound(x)

        '// If an Error is thrown off then the array is not dimmed
        If (Err.Number > 0) Then
            Array_IsEmpty = True
        Else
            Array_IsEmpty = False
        End If

    End Function

    Public Function DateToDatabase(ByRef dDate As Date) As String
        'Function is used to convert a date to format that we can use against a database
        ' The returnformat is #YYYY/MM/DD#

        DateToDatabase = "#" & VB6.Format(dDate, "YYYY") & "/" & VB6.Format(dDate, "MM") & "/" & VB6.Format(dDate, "DD") & "#"

    End Function

    ' This part module contains all the code necessary for the application
    ' to use a satellite DLL for localization purposes.

    ' Please refer to the MSDN for more information regarding the APIs
    ' used in this example.

    '    Public Function LoadLocalizedResources(ByRef sDLLPrefix As String) As Boolean
    '        Dim lLocalID As String

    '        If Len(Dir(My.Application.Info.DirectoryPath & "\language.txt")) > 0 Then
    '            lLocalID = GetLanguageCodeFromFile()
    '        Else
    '            ' Find the LocalID.
    '            lLocalID = Hex(GetUserDefaultLCID)
    '        End If

    '        ' Load the Satellite DLL that contains the local
    '        ' resource object to be used. If CreateObject
    '        ' fails, there is no local version of the
    '        ' resources.

    '        On Error GoTo NoLocalResource

    '        ' Create a local object containing resources.
    '        ' So far, with only norwegian end english dll's,
    '        ' if Norwegain Nynorsk or Danish, then load NorwgeianDLL
    '        ' otherwise, English
    '        If lLocalID = "414" Or lLocalID = "814" Or lLocalID = "406" Or lLocalID = "41D" Then
    '            clsSatellite = CreateObject (sDLLPrefix & "414" & ".clsResources")
    '            clsCommonSatellite = CreateObject ("vbCommon414" & ".clsResources")
    '        Else
    '            'Set clsSatellite = CreateObject (sDLLPrefix & lLocalID & ".clsResources")
    '            clsSatellite = CreateObject (sDLLPrefix & "809" & ".clsResources")
    '            clsCommonSatellite = CreateObject ("vbCommon809" & ".clsResources")
    '        End If
    '        ' Return true, then read local resources.
    '        LoadLocalizedResources = True
    '        Exit Function

    'NoLocalResource:

    '        ' There is no local satellite DLL. As a result, false is returned.
    '        LoadLocalizedResources = False

    '    End Function
    'Public Function FindLanguageID() As String

    '    FindLanguageID = Hex(GetUserDefaultLCID)

    'End Function
    ' LRS will access the object and return the string
    ' resources specific to the region. For this example, only
    ' basic error handling is implemented.
    '    Public Function LRS(ByRef StringIndex As Integer, Optional ByRef sString1 As String = "", Optional ByRef sString2 As String = "") As String
    '        Dim sReturnString As String

    '        On Error GoTo LRSError

    '        ' Make sure there is a resource object.
    '        If Not (clsSatellite Is Nothing) Then
    '            ' Get the resource from the resource object.
    '            If StringIndex > 0 Then
    '                'UPGRADE_WARNING: Couldn't resolve default property of object clsSatellite.GetResourceString. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                sReturnString = clsSatellite.GetResourceString(StringIndex)
    '                If sString1 <> "" Then
    '                    sReturnString = Replace(sReturnString, "%1", Trim(sString1))
    '                    If sString2 <> "" Then
    '                        sReturnString = Replace(sReturnString, "%2", Trim(sString2))
    '                    End If
    '                End If
    '            Else
    '                ' Passed with index 0, illegal, but to avoid runtimerror:
    '                sReturnString = "Text missing"
    '            End If
    '            LRS = sReturnString
    '        Else
    '            ' For this example, if there is no resource
    '            ' object something is still returned.
    '            LRS = "Error : No Local Data"
    '        End If
    '        Exit Function

    'LRSError:
    '        LRS = "Did not find " & Str(StringIndex) & " in LRS()"

    '    End Function

    ' LRS will access the object and return the string
    ' resources specific to the region. For this example, only
    ' basic error handling is implemented.
    Public Function LRSCommon(ByVal StringIndex As Long, Optional ByVal sString1 As String = vbNullString, Optional ByVal sString2 As String = vbNullString, Optional ByVal sString3 As String = vbNullString) As String
        Dim sReturnString As String
        Dim sLanguage As String
        Dim sNorwegian As String = ""
        Dim sEnglish As String = ""
        Dim sSwedish As String = ""
        Dim sDanish As String = ""
        Dim bUseNorwegian As Boolean = False
        Dim bUseDanish As Boolean = False
        Dim bUseSwedish As Boolean = False
        Dim sTmp As String
        Dim oFS As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Static sLanguageFromFile As String = ""

        sReturnString = ""
        'sLanguage = Thread.CurrentThread.CurrentCulture.ToString

        sTmp = System.Configuration.ConfigurationManager.AppSettings("Language")

        If sTmp = "" Then
            sLanguage = Thread.CurrentThread.CurrentCulture.ToString
        Else
            sLanguage = sTmp
        End If
        ' Check if we have "Language.txt" file with Language code set in file.
        If sLanguageFromFile = "" Then
            ' check languagefile
            oFS = New Scripting.FileSystemObject
            If oFS.FileExists(My.Application.Info.DirectoryPath & "\language.txt") Then
                ' file found, import
                oFile = oFS.OpenTextFile(My.Application.Info.DirectoryPath & "\language.txt", Scripting.IOMode.ForReading)
                sLanguageFromFile = oFile.ReadAll
                oFile.Close()
                oFile = Nothing
                oFS = Nothing
            Else
                sLanguageFromFile = "no file"
            End If
        End If
        If sLanguageFromFile <> "no file" Then
            If sLanguageFromFile = "406" Then
                sLanguage = "da"
            ElseIf sLanguageFromFile = "41d" Then
                sLanguage = "se"
            ElseIf sLanguageFromFile = "414" Then
                sLanguage = "nn-NO"
            ElseIf sLanguageFromFile = "809" Then
                sLanguage = "en"
            End If
        End If


        If sLanguage = "nb-NO" Or sLanguage = "nn-NO" Then
            bUseNorwegian = True
        ElseIf sLanguage = "da" Then
            bUseDanish = True
        ElseIf sLanguage = "se" Then
            bUseSwedish = True
        Else
            bUseNorwegian = False
        End If

        Select Case StringIndex
            Case 10000
                sNorwegian = "-------- LRS for vb_Utils --------"
                sEnglish = "-------- LRS for vb_Utils --------"
                sSwedish = "-------- LRS for vb_Utils --------"
                sDanish = "-------- LRS for vb_Utils --------"
            Case 10015
                sNorwegian = "BabelBank avbrytes."
                sEnglish = "BabelBank terminates."
                sSwedish = "BabelBank avbryts."
                sDanish = "BabelBank afbrydes."
            Case 10016
                sNorwegian = "Kontakt din leverand�r."
                sEnglish = "Contact Your dealer."
                sSwedish = "Kontakta din leverant�r."
                sDanish = "Kontakt din leverand�r."
            Case 15006
                sNorwegian = "BabelBank avsluttes"
                sEnglish = "BabelBank terminates"
                sSwedish = "BabelBank avslutas"
                sDanish = "BabelBank afsluttes"
            Case 15007
                sNorwegian = "Feil i filsti for databasekopi"
                sEnglish = "Error in filepath for copy of database"
                sSwedish = "Fel i filbana f�r databasekopi"
                sDanish = "Feil i filsti for databasekopi"
            Case 20000
                sNorwegian = "-------- LRS for BB_Utils -------"
                sEnglish = "-------- LRS for BB_Utils -------"
                sSwedish = "-------- LRS for BB_Utils -------"
                sDanish = "-------- LRS for BB_Utils -------"
            Case 20001
                sNorwegian = "%1koden, %2, er ikke gyldig"
                sEnglish = "The %1code, %2, is not valid"
                sSwedish = "%1koden %2 �r ogiltig"
                sDanish = "%1koden, %2, er ikke gyldig"
            Case 20002
                sNorwegian = "Kun bokstaver og tall er gyldige karakterer."
                sEnglish = "Only letters and numbers are valid characters."
                sSwedish = "Endast bokst�ver och siffror �r giltiga tecken."
                sDanish = "Kun bogstaver og tal er gyldige karakterer."
            Case 20003
                sNorwegian = "Lengden p� feltet m� v�re p� enten 8 eller 11 posisjoner."
                sEnglish = "The length of the field should be either 8 or 11 characters."
                sSwedish = "F�ltl�ngden m�ste vara antingen 8 eller 11 teckenpositioner."
                sDanish = "L�ngden p� feltet skal v�re enten 8 eller 11 tegn."
            Case 20004
                sNorwegian = "Kun tall er tillatt i koden."
                sEnglish = "Only numbers are allowed in the code."
                sSwedish = "Endast siffror till�ts i koden."
                sDanish = "Kun tal er tilladt i koden."
            Case 20005
                sNorwegian = "Lengden p� koden m� v�re p� eksakt %1 posisjoner."
                sEnglish = "The length of the code should be exactly %1 digits."
                sSwedish = "Kodl�ngden m�ste vara exakt %1 teckenpositioner."
                sDanish = "L�ngden p� koden skal v�re p� n�jagtigt %1 tegn."
            Case 20006
                sNorwegian = "20006: Koden p� 'indbetalingskortet' m� v�re nummerisk."
                sEnglish = "20006: The code on the 'indbetalingskort' must be numeric."
                sSwedish = "20006: Koden p� ''inbetalningskortet'' m�ste vara numerisk."
                sDanish = "20006: Koden p� 'indbetalingskortet' skal v�re numerisk."
            Case 20007
                sNorwegian = "Oppgitt kode:"
                sEnglish = "Code stated:"
                sSwedish = "Angiven kod:"
                sDanish = "Angivet kode:"
            Case 20008
                sNorwegian = "20008: Lengden p� et 'indbetalingskort' med 'kortartskode' %1, er p� %2 tall."
                sEnglish = "20008: The length of a 'indbetalingskort' with 'kortartskode' %1, is %2 digits long."
                sSwedish = "20008: L�ngden p� ett inbetalningskort med ''referensnummer'' %1 �r %2 siffror."
                sDanish = "20008: L�ngden p� et 'indbetalingskort' med 'kortartkode' %1, er p� %2 tal."
            Case 20009
                sNorwegian = "20009: '%1' er ikke gyldig. Modulus-kontrollen gir en feil. Angitt '%1':"
                sEnglish = "20009: The '%1' is not valid. The modulus-check reports an error." & vbLf & "'%1' stated:"
                sSwedish = "20009: ''%1'' �r ogiltigt. Modulkontrollen genererar ett fel. Angivet ''%1''':"
                sDanish = "20009: '%1' er ikke gyldig. Modulus-kontrollen giver en fejl. Angivet '%1':"
            Case 20010
                sNorwegian = "20010: 'Kortartkoden' er ikke implementert i %1. Kortartkode:"
                sEnglish = "20010: The 'kortartkode' is not implemented in TBI. Kortartkode:"
                sSwedish = "20010: ''Referensnumret'' �r inte implementerat i %1. Referensnummer:"
                sDanish = "20010: 'Kortartkoden' er ikke implementeret i %1. Kortartkode:"
            Case 20011
                'sNorwegian = "Vennligst overf�r ny kontofil fra TBI Windows." & vbLf & "Logg p� www.dnb.no - Velg Betaling" & vbLf & "Klikk p� Payment Module og velg 'Last ned kontolistefil' fra Hjemmesiden" & vbLf & "Lagre filen i %1 med filenavn DEBACCLIST.DAT" & vbLf & "Start deretter Babel Bank p� vanlig m�te."
                'sEnglish = "Please transfer the accountfile from TBI Windows." & vbLf & "Please logg on to www.DnB.no - Click on Payment module" & vbLf & "Select 'Download Accounlist file' from Home." & vbLf & "Save the file in %1 with filename DEBACCLIST.DAT" & vbLf & "Start Babel Bank as usual."
                ' Changed 16.01.2012 to reflect move to DNB Connect
                sNorwegian = "Vennligst overf�r ny kontofil fra TBI Windows." & vbLf & "Logg p� www.dnb.no - Velg menyen �Administrasjon� I DNB Connect." & vbLf & "Velg �Generelt� og klikk p� linken �Last ned kontoliste� " & vbLf & "Lagre filen i %1 med filnavn DEBACCLIST.DAT" & vbLf & "Start deretter Babel Bank p� vanlig m�te."
                sEnglish = "Please transfer the accountfile from TBI Windows." & vbLf & "Please logg on to www.DnB.no - Select the �Personal settings� menu in DNB Connect." & vbLf & "Choose 'General', and click on the link �Download account list'" & vbLf & "Save the file in %1 with filename DEBACCLIST.DAT" & vbLf & "Start Babel Bank as usual."
                sSwedish = "�verf�r en ny kontofil fr�n DNB Connect." & vbLf & "" & vbLf & "" & vbLf & "Logga in p� www.dnb.se - V�lj menyn ''Administration'' i DNB Connect, v�lj ''Generellt'', klicka p� l�nken f�r att h�mta kontolistan och spara filen i %1 med namnet DEBACCLIST.DAT" & vbLf & "" & vbLf & "" & vbLf & "Starta sedan BabelBank som vanligt.	"
                sDanish = "Overf�r ny kontofil fra DNB Connect." & vbLf & "Log p� www.dnb.no - V�lg menuen 'Administration' i DNB Connect, V�lg 'Generelt', Klik p� linket 'Download kontoliste', og gem filen i %1 med filnavn DEBACCLIST.DAT" & vbLf & "Start derefter Babel Bank p� normal vis.	"
            Case 20012
                sNorwegian = "20012: Debet kontonummer %1 er ukjent for BabelBank."
                sEnglish = "20012: The debitaccount %1 is unknown to BabelBank."
                sSwedish = "20012: Debetkontonummer %1 �r ok�nt f�r BabelBank."
                sDanish = "20012: Debet kontonummer %1 er ukendt for BabelBank."
            Case 20013
                sNorwegian = "20013: SWIFT-adressen %1 registrert p� konto: %2 er ugyldig."
                sEnglish = "20013: The SWIFT-address %1 registered on the account: %2 is not valid."
                sSwedish = "20013: SWIFT-adressen %1 som registrerats f�r konto: %2 �r ogiltigt."
                sDanish = "20013: SWIFT-adressen %1 registreret p� konto: %2 er ugyldig."
            Case 20014
                sNorwegian = "20014: Det er ikke registrert en SWIFT-adresse p� konto: %1."
                sEnglish = "20014: No SWIFT-address is registered on the account: %1"
                sSwedish = "20014: Det finns ingen registrerad SWIFT-adress f�r konto: %1."
                sDanish = "20014: Der er ikke registreret en SWIFT-adresse p� konto: %1."
            Case 20015
                sNorwegian = "20015: Kreditornummeret er ikke gyldig." & vbLf & "Startsifferet er alltid 7, 8 eller 9." & vbLf & "Angitt '%1':"
                sEnglish = "20015: The creditornumber is not valid." & vbLf & "The startdigit should be either 7, 8 or 9." & vbLf & "'%1' stated:"
                sSwedish = "20015: Kreditnumret �r ogiltigt." & vbLf & "Startsiffran �r alltid 7, 8 eller 9." & vbLf & "Angivet ''%1'': 	"
                sDanish = "20015: Kreditornummeret er ikke gyldigt." & vbLf & "Starttallet er altid 7, 8 eller 9." & vbLf & "Angivet '%1': 	"
            Case 20016
                sNorwegian = "20016: FIK-koden er ikke gyldig for formatet %1. Oppgitt kode:"
                sEnglish = "20016: The FIK-code is not valid in the format %1. Code stated:"
                sSwedish = "20016: Gironumret �r inte giltig f�r formatet %1. Angivet nummer:"
                sDanish = "20016: Gironummeret er ikke gyldigt for formatet %1. Angivet kode:"
            Case 20017
                sNorwegian = "20017: En uventet feil oppstod under behandling av bilagsnummeret. Bilagsnummer: %1"
                sEnglish = "20017: An unexpected error occured when adding the voucherno. Voucherno.: %1"
                sSwedish = "20017: Ett ov�ntat fel uppstod under behandling av verifikationsnumret. Verifikationsnummer: %1"
                sDanish = "20017: Der opstod en uventet fejl under behandling af bilagsnummeret. Bilagsnummer: %1"
            Case 20018
                sNorwegian = "20018: BabelBank fors�ker � legge p� et bilagsnummer p� betalingen," & vbLf & "men det er ikke angitt noe bilagsnummer."
                sEnglish = "20018: BabelBank tries to add a vouchernumber to the payment," & vbLf & "but no voucherno is stated."
                sSwedish = "20018: BabelBank f�rs�ker l�gga till ett verifikationsnummer f�r betalningen" & vbLf & "men inget verifikationsnummer har angetts.	"
                sDanish = "20018: BabelBank fors�ger at tilf�je et bilagsnummer p� betalingen," & vbLf & "men der er ikke angivet noget bilagsnummer.	"
            Case 20019
                sNorwegian = "Vennligst g� inn p� Oppsett i BabelBank, og legg inn korrekte data p� klienten."
                sEnglish = "Please enter Setup in BabelBank, and add the necessary data on the client."
                sSwedish = "G� till Vyer i BabelBank och ange r�tt data f�r klienten."
                sDanish = "G� ind p� Ops�tning i BabelBank, og angiv de korrekte data p� klienten."
            Case 20020
                sNorwegian = "20020: %1-ID m� v�re minimum %2 siffer lang. " & vbLf & "Oppgitt %1-ID:"
                sEnglish = "20020: The %1 must be minimum %2 numbers long. " & vbLf & "%1 stated:"
                sSwedish = "20020: %1-ID m�ste vara minst %2 siffror l�ngt." & vbLf & "Angivet %1-ID: 	"
                sDanish = "20020: %1-ID m� v�re minimum %2cifre langt." & vbLf & "Angivet %1-ID: 	"
            Case 20021
                sNorwegian = "20021: %1-ID kan ikke v�re lenger enn %2 siffer lang." & vbLf & "Oppgitt %1-ID:"
                sEnglish = "20021: The %1 can't be more than %2 numbers long." & vbLf & "%1 stated:"
                sSwedish = "20021: %1-ID f�r inte vara �ver %2 siffror l�ngt." & vbLf & "Angivet %1-ID: 	"
                sDanish = "20021: %1-ID kan ikke v�re l�ngere end %2 cifre langt." & vbLf & "Angivet %1-ID: 	"
            Case 20022
                sNorwegian = "<Ikke legg p� kode>"
                sEnglish = "<Don't add a standard code>"
                sSwedish = "<Ikke legg p� kode>"
                sDanish = "<Ikke legg p� kode>"
            Case 20023
                sNorwegian = "BabelBank kan ikke finne filen som inneholder gyldige koder til Norges Bank." & vbLf & "Kontakt din forhandler, eller skriv inn korrekt kode hvis du kjenner den."
                sEnglish = "BabelBank can't localize the file containing valid codes to the Central Bank of Norway." & vbLf & "Contact Your dealer, or enter the correct code if You know it."
                sSwedish = "BabelBank kan inte hitta filen med giltiga koder till Riksbanken." & vbLf & "" & vbLf & "Kontakta din �terf�rs�ljare eller ange r�tt kod om du k�nner till den.	"
                sDanish = "BabelBank kan ikke finde den fil, der indeholder gyldige koder til Nationalbanken." & vbLf & "Kontakt din forhandler, eller skriv korrekt kode, hvis du kender den.	"
            Case 20024
                sNorwegian = "FINNER IKKE FIL"
                sEnglish = "NO FILE FOUND"
                sSwedish = "HITTAR INTE FIL"
                sDanish = "FIL IKKE FUNDET"
            Case 20025
                sNorwegian = "Ingen poster oppdatert (Feil dato?)" & vbLf & "" & vbLf & "SQL:" & vbLf & "%1"
                sEnglish = "No records updated (Wrong date?)" & vbLf & "" & vbLf & "SQL:" & vbLf & "%1"
                sSwedish = "Inga poster har uppdaterats (fel datum?)" & vbLf & "" & vbLf & "SQL:" & vbLf & "%1	"
                sDanish = "Ingen poster opdateret (Forkert dato?)" & vbLf & "" & vbLf & "SQL:" & vbLf & "%1	"
            Case 20026
                sNorwegian = "BabelBank kjenner ikke formatet p� filen." & vbCrLf & "Filnavn: %1"
                sEnglish = "BabelBank is unable to detect the format of the file." & vbCrLf & "Filename: %1"
                sSwedish = "BabelBank kan inte identifiera formatet p� filen."
                sDanish = "BabelBank kender ikke formatet p� filen."
            Case 20027
                sNorwegian = "Legg inn belastningskonto"
                sEnglish = "Please input fromaccount"
                sSwedish = "Ange debiteringskonto"
                sDanish = "Angiv frakonto"
            Case 20028
                sNorwegian = "Feil i kontolengde"
                sEnglish = "Error in accountlength"
                sSwedish = "Fel i kontol�ngd"
                sDanish = "Fejl i kontol�ngde"
            Case 20029
                sNorwegian = "Feil i CDV-kontroll"
                sEnglish = "Error in CDV-control"
                sSwedish = "Fel i CDV-kontroll"
                sDanish = "Fejl i CDV-kontrol"
            Case 20030
                sNorwegian = "Legg inn dato (dd.mm.yyyy)"
                sEnglish = "Enter date (dd.mm.yyyy)"
                sSwedish = "Ange datum (dd.mm.����)"
                sDanish = "Angiv dato (dd.mm.yyyy)"
            Case 20031
                sNorwegian = "Vennligst pr�v igjen!"
                sEnglish = "Please try again!"
                sSwedish = "F�rs�k igen!"
                sDanish = "Pr�v igen!"
            Case 20032
                sNorwegian = "Feil i �rstall -"
                sEnglish = "Erroneus year stated -"
                sSwedish = "Fel i �rtal -"
                sDanish = "Fejl i �rstal -"
            Case 20033
                sNorwegian = "Feil i m�ned -"
                sEnglish = "Erroneus month stated -"
                sSwedish = "Fel i m�nad -"
                sDanish = "Fejl i m�ned -"
            Case 20034
                sNorwegian = "Feil i dag -"
                sEnglish = "Erroneus day stated -"
                sSwedish = "Fel i dag -"
                sDanish = "Fejl i dag -"
            Case 20035
                sNorwegian = "Feil i dato: %2" & vbLf & "Legg inn dato p� formatet %1"
                sEnglish = "Erroneus date stated: %2" & vbLf & "Use the format %1, when You enter the date."
                sSwedish = "Fel i datum: &2" & vbLf & "Ange datum i formatet %1	"
                sDanish = "Fejl i dato: %2" & vbLf & "Angiv dato p� formatet %1	"
            Case 20036
                sNorwegian = "Ugyldig dato!"
                sEnglish = "Erroneus date stated!"
                sSwedish = "Ogiltigt datum!"
                sDanish = "Ugyldig dato!"
            Case 20037
                sNorwegian = "Den angitte datoen er utenfor det gyldige tidsrommet."
                sEnglish = "The date stated is outside the valid span."
                sSwedish = "Det angivna datumet �r utanf�r den giltiga tidsperioden."
                sDanish = "Den angivne dato er uden for det gyldige tidsrum."
            Case 20038
                sNorwegian = "Angitt dato: %1"
                sEnglish = "Date stated: %1"
                sSwedish = "Angivet datum: %1"
                sDanish = "Angivet dato: %1"
            Case 20039
                sNorwegian = "20039 - Referencekode m� v�re numerisk."
                sEnglish = "20039 - The Referencecode must be numeric."
                sSwedish = "20039 - Referenskod m�ste anges med siffror."
                sDanish = "20039 - Referencekode skal v�re numerisk."
            Case 20040
                sNorwegian = "Fra konto:"
                sEnglish = "From account:"
                sSwedish = "Fr�n konto:"
                sDanish = "Fra konto:"
            Case 20041
                sNorwegian = "Forfallsdato:"
                sEnglish = "Duedate:"
                sSwedish = "F�rfallodatum:"
                sDanish = "Forfaldsdato:"
            Case 20042
                sNorwegian = "Til:"
                sEnglish = "To:"
                sSwedish = "Till:"
                sDanish = "Til:"
            Case 20043
                sNorwegian = "Til konto:"
                sEnglish = "To account:"
                sSwedish = "Till konto:"
                sDanish = "Til konto:"
            Case 20044
                sNorwegian = "Bel�p:"
                sEnglish = "Amount:"
                sSwedish = "Belopp:"
                sDanish = "Bel�b:"
            Case 20045
                sNorwegian = "Betaling til egen konto krever at mottakerkonto er innmeldt i klientoppsettet i BabelBank."
                sEnglish = "Payment to own account requires receivers account set in BabelBank's clientsetup."
                sSwedish = "Betalning till eget konto kr�ver att mottagarkontot har angetts i klientvyn i BabelBank."
                sDanish = "Betaling til egen konto kr�ver, at modtagerkonto er oprettet i klientops�tningen i BabelBank."
            Case 20046
                sNorwegian = "BabelBank avbrytes."
                sEnglish = "BabelBank terminates !"
                sSwedish = "BabelBank avbryts."
                sDanish = "BabelBank afbrydes."
            Case 20050
                sNorwegian = "20050 - Det er angitt mer enn en konto for kontantrabatt."
                sEnglish = "20050 - More than one account is marked as dicount account."
                sSwedish = "20050 - Det er angeven flera enn ett konto f�r kontantrabattering."
                sDanish = "20050 - Det er angitt mer enn en konto for kontantrabatt."
            Case 20051
                sNorwegian = "20051 - Det er angitt mer enn en SQL for � validere innbetalingene mot ERP systemet."
                sEnglish = "20051 - More than one SQL is stated to verify the payments against the ERP-system."
                sSwedish = "20051 - Mer �n en SQL anges f�r att verifiera betalningarna mot ERP-systemet."
                sDanish = "20051 - Flere end en SQL er angivet for at verificere betalingerne mod ERP-systemet."
            Case 20052
                sNorwegian = "20052 - Det m� angis like mange sp�rringer for alle tilkoblingene mot ERP-systemet i BabelBank."
                sEnglish = "20052 - The same number of SQL's must be stated for all connections in BabelBank."
                sSwedish = "20052 - Samma antal SQL m�ste anges f�r alla anslutningar i BabelBank."
                sDanish = "20052 - Det samme antal SQL'er skal angives for alle forbindelser i BabelBank"
            Case 20053
                sNorwegian = "Debitors navn: "
                sEnglish = "Debtor's name: "
                sSwedish = "G�lden�rens namn: "
                sDanish = "Debitors navn: "
            Case 20054
                sNorwegian = "Debitors konto: "
                sEnglish = "Debtor's account: "
                sSwedish = "G�lden�rens konto: "
                sDanish = "Debitors konto: "
            Case 20055
                sNorwegian = "Kreditors konto: "
                sEnglish = "Creditor's account: "
                sSwedish = "Borgen�rs konto: "
                sDanish = "Kreditors konto: "
            Case 20056
                sNorwegian = "Bel�p postering: "
                sEnglish = "Amount posting: "
                sSwedish = "Belopp bokf�ring: "
                sDanish = "Bel�p postering: "
            Case 25001
                sNorwegian = "25001: Kunne ikke koble til ekstern database."
                sEnglish = "25001: Couldn't connect to external database."
                sSwedish = "25001: Kunde inte ansluta till extern databas."
                sDanish = "25001: Kunne ikke koble til ekstern database."
            Case 25002
                sNorwegian = "P�loggingen til databasen rapporterte f�lgende feil:"
                sEnglish = "The connection reported the following error:"
                sSwedish = "Inloggningen p� databasen genererade f�ljande fel:"
                sDanish = "P�logningen til databasen rapporterede f�lgende fejl:"
            Case 25003
                sNorwegian = "Feil under oppbygning av p�loggingsstreng."
                sEnglish = "Error during the building of the connectionstring."
                sSwedish = "Fel under konstruktion av inloggningsstr�ng."
                sDanish = "Fejl under opbygning af p�logningsstreng."
            Case 25004
                sNorwegian = "P�loggingsstreng:"
                sEnglish = "Connectionstring:"
                sSwedish = "Inloggningsstr�ng:"
                sDanish = "P�logningsstreng:"
            Case 25005
                sNorwegian = "For � teste/endre p�loggingsstreng, g� inn p� Firmaopplysninger under Oppsett i BabelBank."
                sEnglish = "To test/change the connectionstring, start BabelBank and select Company under Setup in the menu."
                sSwedish = "Testa/�ndra inloggningsstr�ngen i f�retagsinformationen under Vyer i BabelBank."
                sDanish = "For at teste/�ndre p�logningsstreng, g� ind p� Firmaoplysninger under Ops�tning i BabelBank."
            Case 25006
                sNorwegian = "Klarer ikke � finne databasenavn i p�logginsstrengen: %1."
                sEnglish = "BabelBank is not able to dedect the database name from the connectionstring: %1"
                sSwedish = "Det g�r inte att hitta databasens namn i inloggningsstr�ngen %1"
                sDanish = "Kan ikke finde databasens navn i p�loggingsstrengen: %1"
            Case 25007
                sNorwegian = "Kan ikke ta backup av database. Feil rettigheter p� katalog %1."
                sEnglish = "BabelBank s not able to make a backup. The folder %1, has wrong permissions."
                sSwedish = "Det g�r inte att skapa s�kerhetskopior av databasen. Fel beh�righeter p� katalogen %1"
                sDanish = "Kan ikke oprette backup af databasen. Forkerte tilladelser p� katalog &1"
            Case 30000
                sNorwegian = "-------- LRS for vb_Utils --------"
                sEnglish = "------- LRS for VB_Utils -------"
                sSwedish = "-------- LRS for vb_Utils --------"
                sDanish = "-------- LRS for vb_Utils --------"
            Case 30001
                sNorwegian = "30001: Tegnet, %1, kan ikke benyttes i et IBAN-nummer."
                sEnglish = "30001: The character, %1, is not valid in an IBAN-number."
                sSwedish = "30001: Tecknet %1 kan inte anv�ndas i IBAN-nummer."
                sDanish = "30001: Tegnet, %1, kan ikke benyttes i et IBAN-nummer."
            Case 30002
                sNorwegian = "30002: Posisjon 3 og 4 m� v�re nummeriske i et IBAN-nummer."
                sEnglish = "30002: Position 3 and 4 in an IBAN-number must be numeric."
                sSwedish = "30002: Position 3 och 4 m�ste vara siffror i ett IBAN-nummer."
                sDanish = "30002: Position 3 og 4 skal v�re numeriske i et IBAN-nummer."
            Case 30003
                sNorwegian = "30003: Validering av IBAN-nummere rapporterer en feil." & vbLf & "Utregningen av kontrollsifferene, stemmer ikke overens med de angitte kontrollsifferene."
                sEnglish = "30003: The IBAN-validation reports an error." & vbLf & "The calculation of the check-digits, doesn't match the check-digits stated."
                sSwedish = "30003: Ett fel rapporteras under valideringen av IBAN-numret." & vbLf & "De angivna kontrollsiffrorna st�mmer inte.	"
                sDanish = "30003: Validering af IBAN-nummer rapporterer en fejl." & vbLf & "Udregningen af kontrolcifrene, stemmer ikke overens med de angivne kontrolcifre.	"
            Case 30008
                sNorwegian = "30008: Feil i innlesningsmodul." & vbLf & "Det m� angis mulige desimalskilletegn p� bel�p."
                sEnglish = "30008: Error in the import module." & vbLf & "Possible decimal seperators for amount must be stated."
                sSwedish = "30008: Fel i inl�sningsmodul." & vbLf & "Antal till�tna decimaler m�ste anges f�r belopp.	"
                sDanish = "30008: Fejl i indl�sningsmodul." & vbLf & "Der skal angives mulige decimalskilletegn i bel�b.	"
            Case 30009
                sNorwegian = "30009: Feil i bel�p." & vbLf & "Bel�pet %1 er ikke gyldig."
                sEnglish = "30009: Error in amount." & vbLf & "Amount %1 is not valid."
                sSwedish = "30009: Fel i belopp." & vbLf & "Beloppet %1 �r ogiltigt.	"
                sDanish = "30009: Fejl i bel�b." & vbLf & "Bel�bet %1 er ikke gyldigt.	"
            Case 30011
                sNorwegian = "Kan ikke opprette katalogen %1." & vbLf & "" & vbLf & "Feil spesifikajon av katalogen."
                sEnglish = "Can't create the folder %1." & vbLf & "" & vbLf & "Wrong specification of folder."
                sSwedish = "Kan inte skapa katalogen %1." & vbLf & "" & vbLf & "Fel specifikation av katalogen.	"
                sDanish = "Kan ikke oprette kataloget %1." & vbLf & "" & vbLf & "Forkert specifikation for kataloget.	"
            Case 30012
                sNorwegian = "Katalogen %1 er skrivebeskyttet."
                sEnglish = "The folder %1 is read-only."
                sSwedish = "Katalogen %1 �r skrivskyddad."
                sDanish = "Katalog %1 er skrivebeskyttet."
            Case 30013
                sNorwegian = "Legg inn en annen katalog, eller endre rettighetene."
                sEnglish = "Change the folder, or change the attributes of the folder."
                sSwedish = "Amge en annan katalog eller annan beh�righet."
                sDanish = "Angiv et andet katalog, eller �ndr rettighederne."
            Case 30014
                sNorwegian = "Katalogen %1 finnes ikke."
                sEnglish = "Folder %1 does not exist."
                sSwedish = "Katalogen %1 finns inte."
                sDanish = "Kataloget %1 findes ikke."
            Case 30015
                sNorwegian = "�nsker du � opprette katalogen?"
                sEnglish = "Do You want to create the folder?"
                sSwedish = "Vill du skapa katalogen?"
                sDanish = "Vil du oprette kataloget?"
            Case 30016
                sNorwegian = "Opprett katalogen, eller endre stien under oppsett."
                sEnglish = "Create the folder or change the path in setup."
                sSwedish = "Skapa katalogen eller �ndra s�kv�g i inst�llningen."
                sDanish = "Opret kataloget, eller �ndr stien under ops�tning."
            Case 30017
                sNorwegian = "Stasjon %1 finnes ikke."
                sEnglish = "The drive %1 does not exist."
                sSwedish = "Station %1 finns inte."
                sDanish = "Drev %1 findes ikke."
            Case 30018
                sNorwegian = "Filen %1 er skrivebeskyttet. " & vbLf & "Kan ikke slette filen."
                sEnglish = "The file %1 is read-only. " & vbLf & "Can't delete the file."
                sSwedish = "Filen %1 �r skrivskyddad." & vbLf & "Kan inte ta bort filen.	"
                sDanish = "Filen %1 er skrivebeskyttet." & vbLf & "Kan ikke slette filen.	"
            Case 30019
                sNorwegian = "Filen %1 kan ikke slettes."
                sEnglish = "The file %1 can't be deleted."
                sSwedish = "Filen %1 kan inte tas bort."
                sDanish = "Filen %1 kan ikke slettes."
            Case 30020
                sNorwegian = "Filen %1 er skrivebeskyttet. Sjekk at bruker har de n�dvendige rettigheter."
                sEnglish = "The file %1 is read-only. Grant the user the appropriate permissions."
                sSwedish = "Filen %1 �r skrivskyddad. Kontrollera att anv�ndaren har r�tt beh�righet."
                sDanish = "Filen %1 er skrivebeskyttet. Kontroller, at brugeren har de n�dvendige rettigheder."
            Case 30021
                sNorwegian = "Filen %1 er i bruk av en annen applikasjon, eller av en annen BabelBank-kj�ring." & vbLf & "" & vbLf & "Kontroller om en annen applikasjon har filen �pen, eller fors�k � restarte din PC."
                sEnglish = "The file %1 is locked by another application, or by another instance of BabelBank." & vbLf & "" & vbLf & "Check if another application has opened the file, or try to restart Your computer."
                sSwedish = "Filen %1 anv�nds i en annan applikation eller i en annan BabelBank-k�rning." & vbLf & "" & vbLf & "Kontrollera om filen �r �ppen i en annan applikation eller prova med att starta om datorn.	"
                sDanish = "Filen %1 bruges af en anden applikation, eller af en anden BabelBank-k�rsel." & vbLf & "" & vbLf & "Kontroller om filen er �bnet i en anden applikation, eller pr�v at genstarte din PC.	"
            Case 30022
                sNorwegian = "Det er ikke spesifisert noe filnavn i filstien %1."
                sEnglish = "No filename specified in the path %1."
                sSwedish = "Du har inte specificerat n�got filnamn i s�kv�gen %1."
                sDanish = "Der er ikke specificeret noget filnavn i filstien %1."
            Case 30023
                sNorwegian = "En uventet feil oppstod under validering av filstien:" & vbLf & "%1"
                sEnglish = "An unexpected error occured during the validation" & vbLf & "of the path: %1"
                sSwedish = "Ett ov�ntat fel uppstod under validering av s�kv�gen:" & vbLf & "%1	"
                sDanish = "Der opstod en uventet fejl under validering af filstien:" & vbLf & "%1	"
            Case 30024
                sNorwegian = "BabelBank klarer ikke � utlede en gyldig dato fra feltet!" & vbLf & "" & vbLf & "Dato: %1"
                sEnglish = "BabelBank can't retrieve a valid date from the field!" & vbLf & "" & vbLf & "Date: %1"
                sSwedish = "BabelBank kan inte h�rleda n�got giltigt datum fr�n f�ltet!" & vbLf & "" & vbLf & "Datum: %1	"
                sDanish = "BabelBank kan ikke udlede en gyldig dato fra feltet!" & vbLf & "" & vbLf & "Dato: %1	"
            Case 30025
                sNorwegian = "Feil dato!"
                sEnglish = "Wrong date!"
                sSwedish = "Fel datum!"
                sDanish = "Forkert dato!"
            Case 30026
                sNorwegian = "Filen %1 er l�st og kan ikke flyttes til backup. Kan den v�re ibruk av et annet program ?"
                sEnglish = "The file %1 is locked. Check if it is in use by another program."
                sSwedish = "Filen %1 �r l�st och kan inte flyttas till en s�kerhetskopia. Den kanske anv�nds i ett annat program ?"
                sDanish = "Filen %1 er l�st og kan ikke flyttes til backup. Kan den v�re i brug af et andet program ?"
            Case 30027
                sNorwegian = "Filen %1 er skrivebeskyttet."
                sEnglish = "The file %1 is read-only."
                sSwedish = "Filen %1 �r skrivskyddad."
                sDanish = "Filen %1 er skrivebeskyttet."
            Case 30028
                sNorwegian = "Filen %1 er en UTF-8 fil, skal v�re ASCII."
                sEnglish = "The file %1 is a UTF-8 file, should be ASCII."
                sSwedish = "Filen% 1 �r en UTF-8-fil, m�ste vara ASCII."
                sDanish = "Filen %1 er skrivebeskyttet."
            Case 30029
                sNorwegian = "SQL-en skal oppdatere eksakt 1 rad, denne oppdaterer %1 rad(er)."
                sEnglish = "The query should affect exactly 1 row, this query updates %1 row(s)."""
                sSwedish = "SQL-en ska uppdatera exakt 1 rad, denna uppdaterar %1 rad(er)."
                sDanish = "SQL-en skal opdatere n�jagtigt 1 r�kke, denne opdaterer %1 r�kk(er)."

                '--------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 30050

            Case 30050
                sNorwegian = "Rapport nummer %1 er ikke en gyldig rapport i BabelBank."
                sEnglish = "Reportnumber %1 is not a valid report in BabelBank."
                sSwedish = "Rapport nummer %1 �r inte en giltig rapport i BabelBank."
                sDanish = "Rapport nummer %1 er ikke en gyldig rapport i BabelBank."
            Case 30051
                sNorwegian = "BabelBank kan ikke konvertere strengen til en gyldig dato." & vbCrLf & "Streng: %1" & vbCrLf & "Format: %2"
                sEnglish = "BabelBank is not able to convert the string to a valid date." & vbCrLf & "String: %1" & vbCrLf & "Format: %2"
                sSwedish = "BabelBank kan inte konvertera str�ngen till ett giltigt datum." & vbCrLf & "Str�ng: %1" & vbCrLf & "Format: %2"
                sDanish = "BabelBank kan ikke konvertere streng til en gyldig dato." & vbCrLf & "Streng: %1" & vbCrLf & "Format: %2"
            Case 30052
                sNorwegian = "Det er ikke angitt noen p�loggingsstreng, kan ikke kj�re rapport: %1."
                sEnglish = "No connectionstring stated, can't create report: %1"
                sSwedish = "Der �r inte specificerad en inloggningsstr�ng, kan inte skapa rapport: %1"
                sDanish = "Der er ikke specificeret p�logningsstreng, kan ikke k�re rapport: %1."

            Case 35001
                sNorwegian = "35001: Kunne ikke koble til Visma sin database."
                sEnglish = "35001: Could'nt connect to the Visma database."
                sSwedish = "35001: Kunde inte ansluta Visma till databasen."
                sDanish = "35001: Kunne ikke forbinde til Vismas database."
            Case 35002
                sNorwegian = "P�loggingen til databasen rapporterte f�lgende feil:"
                sEnglish = "The connection reported the following error:"
                sSwedish = "Inloggningen p� databasen genererade f�ljande fel:"
                sDanish = "P�logning til databasen rapporterede f�lgende fejl:"
            Case 35003
                sNorwegian = "Feil under oppbygning av p�loggingsstreng."
                sEnglish = "Error during the building of the connectionstring."
                sSwedish = "Fel under konstruktion av inloggningsstr�ng."
                sDanish = "Fejl under opbygning af p�logningsstreng."
            Case 35004
                sNorwegian = "P�loggingsstreng:"
                sEnglish = "Connectionstring:"
                sSwedish = "Inloggningsstr�ng:"
                sDanish = "P�logningsstreng:"

            Case 40000
                sNorwegian = "-------- LRS for BB_Setup --------"
                sEnglish = "------- LRS for BB_Setup -------"
                sSwedish = "-------- LRS for BB_Setup --------"
                sDanish = "-------- LRS for BB_Setup --------"
            Case 40001
                sNorwegian = "Komprimer BabelBank sin database n�r den er p�"
                sEnglish = "Compress BabelBank's database at the size of"
                sSwedish = "Komprimera BabelBanks-databasen n�r den n�r"
                sDanish = "Komprimer BabelBanks database n�r den er p�"
            Case 41000
                sNorwegian = "---- LRS for BB_Setup/EXE ----"
                sEnglish = "---- LRS for BB_Setup/EXE ----"
                sSwedish = "---- LRS for BB_Setup/EXE ----"
                sDanish = "---- LRS for BB_Setup/EXE ----"
            Case 41001
                sNorwegian = "Kundenr."
                sEnglish = "Customerno."
                sSwedish = "Kundnr."
                sDanish = "Kundenr."
            Case 41002
                sNorwegian = "Fakturanr."
                sEnglish = "Invoiceno."
                sSwedish = "Fakturanr."
                sDanish = "Fakturanr."
            Case 41003
                sNorwegian = "Debet"
                sEnglish = "Debit"
                sSwedish = "Debet"
                sDanish = "Debet"
            Case 41004
                sNorwegian = "Kredit"
                sEnglish = "Credit"
                sSwedish = "Kredit"
                sDanish = "Kredit"
            Case 41005
                sNorwegian = "Valuta"
                sEnglish = "Currency"
                sSwedish = "Valuta"
                sDanish = "Valuta"
            Case 41006
                sNorwegian = "Val.kurs"
                sEnglish = "Exch.rate"
                sSwedish = "Val.kurs"
                sDanish = "Val.kurs"
            Case 41007
                sNorwegian = "H.Bok"
                sEnglish = "GL Account"
                sSwedish = "H.bok"
                sDanish = "H.Bog"
            Case 41008
                sNorwegian = "Levnr."
                sEnglish = "Supplierno."
                sSwedish = "Levnr."
                sDanish = "Leverand�r"
            Case 41009
                sNorwegian = "Bilagsnr."
                sEnglish = "Voucherno."
                sSwedish = "Verifikationsnr."
                sDanish = "Bilagsnr."
            Case 41010
                sNorwegian = "Fritekst"
                sEnglish = "Freetext"
                sSwedish = "Fritext"
                sDanish = "Fritekst"
            Case 41011
                sNorwegian = "Klientnr."
                sEnglish = "Clientno."
                sSwedish = "Klientnr."
                sDanish = "Klientnr."
            Case 41012
                sNorwegian = "Navn"
                sEnglish = "Name"

                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 41050

                sSwedish = "Namn"
                sDanish = "Navn"
            Case 41019
                sNorwegian = "Validering"
                sEnglish = "Validation"
                sSwedish = "Validering"
                sDanish = "Validering"
            Case 41020
                sNorwegian = "Rabatt"
                sEnglish = "Discount"
                sSwedish = "Rabatt"
                sDanish = "Rabat"
            Case 41021
                sNorwegian = "Bel�p:"
                sEnglish = "Amount:"
                sSwedish = "Belopp:"
                sDanish = "Bel�b:"
            Case 41022
                sNorwegian = "Kreditnota:"
                sEnglish = "Creditnote:"
                sSwedish = "Kreditnota:"
                sDanish = "Kreditnota:"
            Case 41023
                sNorwegian = "Valutabel�p"
                sEnglish = "CurrencyAmount"
                sSwedish = "Valutabelopp"
                sDanish = "Valutabel�p"
            Case 41024
                sNorwegian = "Valuta2"
                sEnglish = "Currency2"
                sSwedish = "Valuta2"
                sDanish = "Valuta2"
            Case 41025
                sNorwegian = "Tilbakebetalingskonto"
                sEnglish = "RepaymentAccount"
                sSwedish = "�terbetalningskonto"
                sDanish = "Tilbagebetalingskonto"
            Case 41026
                sNorwegian = "Ant. dager s�kehistorikk"
                sEnglish = "No. of days searchhistory"
                sSwedish = "Ant. dagers s�khistorikk"
                sDanish = "Ant. dager s�gehistorikk"

            Case 45000
                sNorwegian = "---- LRS for vbBabel/EXE ----"
                sEnglish = "---- LRS for vbBabel/EXE ----"
                sSwedish = "---- LRS for vbBabel/EXE ----"
                sDanish = "---- LRS for vbBabel/EXE ----"
            Case 45001
                sNorwegian = "45001: Kan ikke finne n�dvendig informasjon for � hente betalingsdata fra database." & vbLf & "" & vbLf & "Spsifiser 'importfilen' p� denne m�ten DB=Navn p� p�logging;SQL=Navn p� SQL for � hente ut data;SQL2=Navn p� SQL for � oppdatere data" & vbLf & "Navn p� p�logging og SQL kan du sette opp under menypunktet Avstemming' -> 'Avstemmingsregler'."
                sEnglish = "45001: Can't find necessary information to retrieve data from the database." & vbLf & "" & vbLf & "Specify the 'importfile' like this DB=Name of connection;SQL1=Name of SQL to retrieve data;SQL2=Name of SQL to update data" & vbLf & "The name of the connection and the SQL are stated under the menu 'Matching' -> 'Matching rule'"
                sSwedish = "45001: Kan inte hitta n�dv�ndig information f�r h�mtning av betalningsdata fr�n databasen." & vbLf & "" & vbLf & "Specificera ''importfilen'' s� h�r: DB=Inloggningsnamn;SQL=Namn p� SQL f�r h�mtning av data;SQL2=Namn p� SQL f�r uppdatering av data" & vbLf & "Inloggningsnamn och SQL kan st�llas in p� menyn ''Avst�mning'' -> ''Avst�mningsregler''.	"
                sDanish = "45001: Kan ikke finde n�dvendig information for at hente betalingsdata fra database." & vbLf & "" & vbLf & "Specificer 'importfilen' p� denne m�de DB=Navn p� p�logning;SQL=Navn p� SQL for at hente data ud;SQL2=Navn p� SQL for at opdatere data" & vbLf & "Navn p� p�logning og SQL kan du s�tte op under menupunktet Afstemning' -> 'Afstemningsregler'.	"
            Case 45002
                sNorwegian = "Feil: "
                sEnglish = "Error: "
                sSwedish = "Fel: "
                sDanish = "Fejl: "
            Case 45003
                sNorwegian = "45003: Feil under uthenting av klient_ID."
                sEnglish = "45003: An unexpected error occurred when retrieving the client-id."
                sSwedish = "45003: Ett ov�ntat fel uppstod vid h�mtning av klient-id."
                sDanish = "45003: Der opstod en uventet fejl ved hentning af klient-id."
            Case 45004
                sNorwegian = "45004: Feil under uthenting av klientnummer"
                sEnglish = "45004: An unexpected error occurred when retrieving the client number."
                sSwedish = "45004: Ett ov�ntat fel uppstod vid h�mtning av klientnumret."
                sDanish = "45004: Der opstod en uventet fejl ved hentning af klientnummeret."
            Case 45005
                sNorwegian = "45005: Feil under uthenting av kontonummer"
                sEnglish = "45005: An unexpected error occurred when retrieving the account number."
                sSwedish = "45005: Ett ov�ntat fel uppstod n�r du h�mtade kontonummeret."
                sDanish = "45005: Der opstod en uventet fejl ved hentning af kontonummeret."
                'sErrorText = "The exportflag was not updated correct on some payments." & vbCrLf & "This will affect the status in the archive database." & vbCrLf & vbCrLf & "Number of payments not correctly updated: " & Trim(Str(nErrors))
            Case 45006
                sNorwegian = "45006: Eksportflagget ble ikke oppdatert korrekt p� enkelte betalinger." & vbCrLf & "Dette vil p�virke statusen i databasen." & vbCrLf & vbCrLf & "Antall betalinger som ikke ble korrekt oppdatert: %1"
                sEnglish = "45006: The export flag was not updated correctly on some payments." & vbCrLf & "This will affect the status in the archive database." & vbCrLf & vbCrLf & "Number of payments not correctly updated: %1"
                sSwedish = "45006: Exportflaggan uppdaterades inte korrekt p� vissa betalningar." & vbCrLf & "Detta p�verkar statusen i arkivdatabasen." & vbCrLf & vbCrLf & "Antal betalningar som inte uppdaterats korrekt: % 1"
                sDanish = "45006: Eksportflagget blev ikke opdateret korrekt p� nogle betalinger." & vbCrLf & "Dette vil p�virke status i arkivdatabasen." & vbCrLf & vbCrLf & "Antal betalinger, der ikke blev opdateret korrekt: % 1"
            Case 50000
                sNorwegian = "--------- LRS for felles forms --------------"
                sEnglish = "------- LRS for common forms ------"
                sSwedish = "--------- LRS for felles forms --------------"
                sDanish = "--------- LRS for felles forms --------------"
            Case 50001
                sNorwegian = "&Avbryt"
                sEnglish = "&Cancel"
                sSwedish = "&Avbryt"
                sDanish = "&Afbryd"
            Case 64001
                sNorwegian = "Melding:"
                sEnglish = "Message:"
                sSwedish = "Meddelande:"
                sDanish = "Meddelelse:"
            Case 64002
                sNorwegian = "Format:"
                sEnglish = "Format:"
                sSwedish = "Format:"
                sDanish = "Format:"
            Case 64003
                sNorwegian = "Filnavn:"
                sEnglish = "Filename:"
                sSwedish = "Filnamn:"
                sDanish = "Filnavn:"
            Case 64004
                sNorwegian = "Linje:"
                sEnglish = "Record:"
                sSwedish = "Rad:"
                sDanish = "Linje:"
            Case 64005
                sNorwegian = "Kodelinje:"
                sEnglish = "Line in kode:"
                sSwedish = "Kodlinje:"
                sDanish = "Kodelinje:"
            Case 64006
                sNorwegian = "Betaling:"
                sEnglish = "Payment:"
                sSwedish = "Betalning:"
                sDanish = "Betaling:"
            Case 64007
                sNorwegian = "Versjon: "
                sEnglish = "Version: "
                sSwedish = "Version: "
                sDanish = "Versjon: "
            Case 64045
                sNorwegian = "Endre melding til mottaker"
                sEnglish = "Change message to beneficiary"
                sSwedish = "�ndra meddelande till mottagare"
                sDanish = "�ndr meddelelse til modtager"
            Case 64046
                sNorwegian = "Tekst som kommer med p� betaling:"
                sEnglish = "Message to beneficiary:"
                sSwedish = "Text som visas p� betalning:"
                sDanish = "Tekst som kommer med p� betaling:"
            Case 64047
                sNorwegian = "Tekst som utelates p� betaling:"
                sEnglish = "Message that will be removed:"
                sSwedish = "Text som utel�mnas p� betalning:"
                sDanish = "Tekst som udelades p� betaling:"
            Case 64048
                sNorwegian = "Informasjon om betaling:"
                sEnglish = "Payment information:"
                sSwedish = "Information om betalning:"
                sDanish = "Information om betaling:"
            Case 64049
                sNorwegian = "Melding til mottager er for lang. Maksimal lengde er %1 posisjoner." & vbLf & "Overskytende tekst vil ikke sendes mottaker."
                sEnglish = "The message to beneficiary is too long. Maximum length is %1 positions." & vbLf & "Additional text will not be sent to beneficiary."
                sSwedish = "Meddelandet till mottagaren �r f�r l�ngt. Maxl�ngden �r %1 tecken." & vbLf & "�verskottstext skickas inte till mottagaren.	"
                sDanish = "Meddelelse til modtager er for lang. Maksimal l�ngde er %1 tegn." & vbLf & "Overskydende tekst vil ikke blive sendt til modtager.	"
            Case 64050
                sNorwegian = "Dato angitt: %1"
                sEnglish = "Date given: %1"
                sSwedish = "Angivet datum: %1"
                sDanish = "Dato angivet: %1"
            Case 64051
                sNorwegian = "Legg inn frakonto"
                sEnglish = "Please input fromaccount"
                sSwedish = "Ange fr�n-konto"
                sDanish = "Angiv frakonto"
            Case 64052
                sNorwegian = "Frakonto"
                sEnglish = "From account"
                sSwedish = "Fr�n konto"
                sDanish = "Fra konto"
                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 64080
            Case 64080
                sNorwegian = "Du m� angi hele mappe og filnavn, ikke kun filnavnet."
                sEnglish = "You must state the full path, not only the file name."
                sSwedish = "Du m�ste ange hela s�kv�gen, inte bara filnamnet."
                sDanish = "Du skal angive hele s�gev�gen, ikke kun filnavn."
            Case 64081
                sNorwegian = "Feil i oppgitt filsti"
                sEnglish = "Error in specified file path"
                sSwedish = "Fel i den angivna s�kv�gen"
                sDanish = "Fejl i den angivne filsti"
            Case 64082
                sNorwegian = "Feil ved opprettelse av filmappe "
                sEnglish = "Error when creating folder "
                sSwedish = "Fel vid skapande av mapp "
                sDanish = "Fejl ved oprettelse af mappe "
            Case 64083
                sNorwegian = "Det er ikke angitt noen filsti"
                sEnglish = "No filepath stated"
                sSwedish = "Ingen filv�g angiven"
                sDanish = "Ingen filsti angivet"
            Case 64084
                sNorwegian = "Bel�pet p� betalingen avviker fra summen av bel�p p� fakturaene i databasen." & vbCrLf & "Bel�p betaling: %1" & vbCrLf & "Sum fakturaer: %2"
                sEnglish = "The amount on the payment differs from the sum of amounts on the invoices in the database." & vbCrLf & "Amount payment: %1" & vbCrLf & "Sum invoices: %2"
                sSwedish = "Beloppet p� betalningen skiljer sig fr�n summan av beloppen p� fakturorna i databasen." & vbCrLf & "Belopp betalning: %1" & vbCrLf & "Summa fakturor: %2"
                sDanish = "Bel�pet p� betalingen er forskellig fra summen af bel�bene p� fakturaer i databasen." & vbCrLf & "Bel�b betaling: %1" & vbCrLf & "Sum fakturaer: %2"
            Case 64085
                sNorwegian = "Dersom du fortsetter, brukes de opprinnelige bel�p importert inn i BabelBanks database." & vbCrLf & "Vil du avbryte BabelBank?"
                sEnglish = "If you continue, the original amount imported into the BabelBanks database will be used." & vbCrLf & "Do you want to cancel BabelBank?"
                sSwedish = "Om du forts�tter, anv�nda det ursprungliga beloppet som ble importerad till Babylon Bank databas." & vbCrLf & "Vill du avbryta BabelBank?"
                sDanish = "Hvis du forts�tter, bruges det oprindelige bel�b importerert til BabelBanks database." & vbCrLf & "Vil du annulere BabelBank?"

        End Select

        If bUseDanish Then
            sReturnString = sDanish
        ElseIf bUseSwedish Then
            sReturnString = sSwedish
        ElseIf bUseNorwegian Then
            sReturnString = sNorwegian
        Else
            sReturnString = sEnglish
        End If
        ' If no hit, then use English;
        If EmptyString(sReturnString) Then
            sReturnString = sEnglish
        End If
        ' If still not found
        If EmptyString(sReturnString) Then
            sReturnString = StringIndex & " languagestring is missing."
        End If

        sReturnString = Replace(sReturnString, "%1", Trim$(sString1))
        sReturnString = Replace(sReturnString, "%2", Trim$(sString2))
        sReturnString = Replace(sReturnString, "%3", Trim$(sString3))

        LRSCommon = sReturnString


    End Function
    Function RunTime() As Boolean
        Static bReturnValue As Boolean
        Static bCalledBefore As Boolean ' No need to test each time

        ' 27.09.2018
        ' Changed form using oFS to System.IO.
        ' ====================================
        If Not bCalledBefore Then
            bReturnValue = True
            If System.IO.File.Exists("C:\vb_Debug.lsk") Then
                bReturnValue = False
            End If
            bCalledBefore = True
        End If

        '  --- OLD CODE before 27.09.2018 ---
        'Dim oFs As Scripting.FileSystemObject

        'If Not bCalledBefore Then

        '    bReturnValue = True

        '    oFs = New Scripting.FileSystemObject  ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")

        '    If oFs.FolderExists("C:\") Then
        '        If oFs.FileExists("C:\vb_Debug.lsk") Then
        '            bReturnValue = False
        '        End If
        '    End If
        '    bCalledBefore = True
        'End If

        'If Not oFs Is Nothing Then
        '    oFs = Nothing
        'End If

        RunTime = bReturnValue

    End Function
    Function CheckFolder(ByRef sPath As String, ByRef iErrorMessageType As ErrorMessageType, Optional ByRef bCreateFolder As Boolean = True, Optional ByRef sMessageHeader As String = "", Optional ByRef bCheckFileAlso As Boolean = False, Optional ByRef bSilent As Boolean = False) As Short
        'Function CheckFolder(sPath As String, Optional bCreateFolder As Boolean = True, Optional bShowErrorMessage As Bank = False, Optional sMessageHeader As String = "") As Integer
        '
        'NB NB NB NB NB NB NB NB NB
        'A passed folder should always end with an \

        'This function checks if the folder exists, if the folder is readonly, and gives
        ' a possibility to create it
        ' Should also test for available space on disk (more than 1 Mb free).
        ' Set the last parameter to .true. if you want to use the errormessages(remember the LRS-ids) in this function
        ' Returnvalues less than 10 are non-critical
        'FIX: We should receive the first LRS-ID in the function. Errormessages should then be created
        ' like this If LRS-id = 0 then msgbox "Errormessage" else msgbox lrs(receivedLRS-id + fixed number)
        'The returnvalues
        ' 0 - Everything OK, the folder already exists
        ' 1 - OK, the folder has been created, and thus exists
        ' 5 - The folder exists or has been created and the file exists (if bCheckFileAlso = True)
        '     This is a common and OK returnvalue when the file is checked as well.
        ' 8 - Under 1 Mb free space on the drive. Not in use
        '
        ' 11 - Folder exist but is read-only
        ' 13 - Folder doesn't exists, and are not created
        ' 14 - Folder doesn't exists, and can't be created because of read-only
        ' 17 - The drive doesn't exists
        ' 51 - File is read-only (if bCheckFileAlso = True)
        ' 90 - No folder specified
        ' 91 - Can't create the folder spath. Wrong specification of folder
        ' 92 - No file specified (if bCheckFileAlso = True)
        ' 99 - Unspecified error


        Dim oFs As Scripting.FileSystemObject
        Dim oFolder As Scripting.Folder
        Dim oDirectory As System.IO.DirectoryInfo
        Dim oDrive As Scripting.Drive
        Dim sFolderName As String
        Dim iReturnValue As Short
        Dim iPos As Short
        Dim sTempPath1, sTempPath2 As String
        Dim bContinueLoop As Boolean
        Dim iResponse As Short

        On Error GoTo ERR_CheckFolder

        oFs = New Scripting.FileSystemObject

        iReturnValue = 0

        If Not sPath = "" Then
            If InStr(1, sPath, "\") = 0 Then
                Select Case iErrorMessageType
                    Case ErrorMessageType.Dont_Show
                        'Do nothing
                    Case ErrorMessageType.Use_ERR_Raise
                        Err.Raise(30011, sMessageHeader, LRSCommon(30011, sPath))
                    Case ErrorMessageType.Use_MsgBox
                        'Can't create the folder %spath%. Wrong specification of folder
                        MsgBox(LRSCommon(30011, sPath), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, sMessageHeader)
                End Select
                iReturnValue = 91
            Else
                sFolderName = Left(sPath, InStrRev(sPath, "\") - 1)
                'Check if folder already exists
                If oFs.FolderExists(sFolderName) Then
                    'oFolder = oFs.GetFolder(sFolderName)
                    'If oFolder.Attributes And Scripting.__MIDL___MIDL_itf_scrrun_0000_0000_0001.ReadOnly Then
                    oDirectory = My.Computer.FileSystem.GetDirectoryInfo(sFolderName)
                    If (oDirectory.Attributes And System.IO.FileAttributes.ReadOnly) = System.IO.FileAttributes.ReadOnly Then
                        'The folder %sfoldername% is read-only. & Change the folder, or change the attributes of the folder.
                        Select Case iErrorMessageType
                            Case ErrorMessageType.Dont_Show
                                'Do nothing
                            Case ErrorMessageType.Use_ERR_Raise
                                Err.Raise(30012, sMessageHeader, LRSCommon(30012, sPath) & vbCrLf & LRSCommon(30013))
                            Case ErrorMessageType.Use_MsgBox
                                MsgBox(LRSCommon(30012, sFolderName) & vbCrLf & LRSCommon(30013), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, sMessageHeader)
                        End Select
                        iReturnValue = 11
                    Else
                        iReturnValue = 0
                    End If
                Else
                    If bCreateFolder Then
                        Select Case iErrorMessageType
                            Case ErrorMessageType.Dont_Show
                                'Just create the folder, no messages
                                bCreateFolder = True
                            Case ErrorMessageType.Use_ERR_Raise
                                'Just create the folder, no messages
                                'UPGRADE_WARNING: Couldn't resolve default property of object bSilent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If bSilent Then
                                    bCreateFolder = True
                                Else
                                    '"Folder " & sFolderName & "doesn't exist." & "Do You want to create the folder?"
                                    iResponse = MsgBox(LRSCommon(30014, sFolderName) & vbCrLf & LRSCommon(30015), MsgBoxStyle.YesNo + MsgBoxStyle.Question, sMessageHeader)
                                    If iResponse = MsgBoxResult.Yes Then
                                        bCreateFolder = True
                                    Else
                                        bCreateFolder = False
                                    End If
                                End If
                            Case ErrorMessageType.Use_MsgBox
                                '"Folder " & sFolderName & "doesn't exist." & "Do You want to create the folder?"
                                iResponse = MsgBox(LRSCommon(30014, sFolderName) & vbCrLf & LRSCommon(30015), MsgBoxStyle.YesNo + MsgBoxStyle.Question, sMessageHeader)
                                If iResponse = MsgBoxResult.Yes Then
                                    bCreateFolder = True
                                Else
                                    bCreateFolder = False
                                End If
                        End Select
                    Else
                        '"Folder " & sFolderName & "doesn't exist." & "Create the folder or change the path in setup."
                        Select Case iErrorMessageType
                            Case ErrorMessageType.Dont_Show
                                'Do nothing
                            Case ErrorMessageType.Use_ERR_Raise
                                Err.Raise(30014, sMessageHeader, LRSCommon(30014, sFolderName) & vbCrLf & LRSCommon(30016))
                            Case ErrorMessageType.Use_MsgBox
                                MsgBox(LRSCommon(30014, sFolderName) & vbCrLf & LRSCommon(30016), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
                        End Select
                        bCreateFolder = False
                    End If
                    If bCreateFolder Then
                        'Tries to create the folder. If possible, do it. If not find out why
                        If Not MakeFolder(sFolderName) Then
                            iPos = 1
                            iPos = InStr(iPos, sPath, "\")
                            sTempPath1 = Left(sPath, iPos - 1)
                            If Not oFs.DriveExists(sTempPath1) Then
                                'Can't create the folder %sfoldername%. The drive %stemppath1% does not exist
                                Select Case iErrorMessageType
                                    Case ErrorMessageType.Dont_Show
                                        'Do nothing
                                    Case ErrorMessageType.Use_ERR_Raise
                                        Err.Raise(30011, sMessageHeader, LRSCommon(30011, sFolderName) & vbCrLf & LRSCommon(30017, sTempPath1))
                                    Case ErrorMessageType.Use_MsgBox
                                        MsgBox(LRSCommon(30011, sFolderName) & vbCrLf & LRSCommon(30017, sTempPath1), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, sMessageHeader)
                                End Select
                                iReturnValue = 17
                            Else
                                bContinueLoop = True
                                Do While bContinueLoop
                                    'Assume failure
                                    bContinueLoop = False
                                    iPos = InStr(iPos + 2, sPath, "\")
                                    sTempPath2 = sTempPath1
                                    If iPos = 0 Then
                                        sTempPath1 = sPath
                                        'Can't create the folder %spath%. Wrong specification of folder
                                        Select Case iErrorMessageType
                                            Case ErrorMessageType.Dont_Show
                                                'Do nothing
                                            Case ErrorMessageType.Use_ERR_Raise
                                                Err.Raise(30011, sMessageHeader, LRSCommon(30011, sPath))
                                            Case ErrorMessageType.Use_MsgBox
                                                MsgBox(LRSCommon(30011, sPath), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
                                        End Select
                                        iReturnValue = 91
                                        Exit Do
                                    Else
                                        sTempPath1 = Left(sPath, iPos - 1)
                                    End If
                                    If Not oFs.FolderExists(sTempPath1) Then
                                        oFolder = oFs.GetFolder(sTempPath2)
                                        If oFolder.Attributes And Scripting.__MIDL___MIDL_itf_scrrun_0000_0000_0001.ReadOnly Then
                                            bContinueLoop = False
                                            'Can't create the folder %sfoldername%. The folder %sTempPath2% is read-only
                                            Select Case iErrorMessageType
                                                Case ErrorMessageType.Dont_Show
                                                    'Do nothing
                                                Case ErrorMessageType.Use_ERR_Raise
                                                    Err.Raise(30011, sMessageHeader, LRSCommon(30011, sFolderName) & vbCrLf & LRSCommon(30012, sTempPath2))
                                                Case ErrorMessageType.Use_MsgBox
                                                    MsgBox(LRSCommon(30011, sFolderName) & vbCrLf & LRSCommon(30012, sTempPath2), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
                                            End Select
                                            iReturnValue = 14
                                        Else
                                            oFs.CreateFolder(sTempPath1)
                                            bContinueLoop = True
                                        End If
                                    Else
                                        bContinueLoop = True
                                    End If
                                    If sTempPath1 = sFolderName Then
                                        iReturnValue = 1
                                        bContinueLoop = False
                                    End If
                                Loop
                            End If
                        Else
                            'Folder created
                            iReturnValue = 1
                        End If
                    Else
                        'The folder will not be created
                        iReturnValue = 13
                    End If
                End If
                'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFs = Nothing
                'UPGRADE_NOTE: Object oFolder may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFolder = Nothing
                'UPGRADE_NOTE: Object oDrive may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oDrive = Nothing
            End If
        Else
            'Can't create the folder %spath%. Wrong specification of folder
            Select Case iErrorMessageType
                Case ErrorMessageType.Dont_Show
                    'Do nothing
                Case ErrorMessageType.Use_ERR_Raise
                    Err.Raise(30011, sMessageHeader, LRSCommon(30011, sPath))
                Case ErrorMessageType.Use_MsgBox
                    MsgBox(LRSCommon(30011, sPath), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
            End Select
            iReturnValue = 90
        End If

        'If iReturnValue = 0 Then ' Doesn't work for disks larger than 2 Giga
        '    Set oDrive = oFs.GetDrive(oFs.GetDriveName(oFs.GetAbsolutePathName(sFilename)))
        '    d = FormatNumber(oDrive.FreeSpace / 1024, 0)  'oDrive.FreeSpace
        '    Set oDrive = oFs.GetDrive(oFs.GetDriveName(oFs.GetAbsolutePathName("j:\")))
        '    d = oDrive.FreeSpace
        'End If

        If bCheckFileAlso And iReturnValue < 10 Then
            iReturnValue = CheckFile(sPath, iErrorMessageType, sMessageHeader)
        End If

        CheckFolder = iReturnValue

        If Not oFolder Is Nothing Then
            oFolder = Nothing
        End If
        If Not oDrive Is Nothing Then
            oDrive = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        On Error GoTo 0
        Exit Function

ERR_CheckFolder:

        If Not oFolder Is Nothing Then
            oFolder = Nothing
        End If
        If Not oDrive Is Nothing Then
            oDrive = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If
        'An unexpected error occured during the validation vbcrlf of the path %1
        Select Case iErrorMessageType
            Case ErrorMessageType.Dont_Show
                'Do nothing
            Case ErrorMessageType.Use_ERR_Raise
                If Err.Number > 30000 And Err.Number < 30200 Then
                    Err.Raise(Err.Number, sMessageHeader, Err.Description)
                Else
                    Err.Raise(30023, sMessageHeader, LRSCommon(30023, sPath))
                End If
            Case ErrorMessageType.Use_MsgBox
                If Err.Number > 30000 And Err.Number < 30200 Then
                    MsgBox(Err.Description, MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
                Else
                    MsgBox(LRSCommon(30023, sPath), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
                End If
                Err.Clear()
        End Select
        If iReturnValue = 0 Then
            iReturnValue = 99
        End If

        CheckFolder = iReturnValue

    End Function
    Function CheckFile(ByRef sPath As String, ByRef iErrorMessageType As ErrorMessageType, Optional ByRef sMessageHeader As String = "") As Short

        'This function checks if the file exists and if the folder is readonly.

        ' Returnvalues less than 10 are non-critical
        'The returnvalues
        ' 5 - The file exists
        ' 51 - File is read-only (if bCheckFileAlso = True)
        ' 52 - The file is locked by another process
        ' 92 - No file specified (if bCheckFileAlso = True)

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.File
        Dim sFile As String
        Dim iReturnValue As Short
        Dim iPos As Short
        Dim sTempPath1, sTempPath2 As String
        Dim bContinueLoop As Boolean
        Dim iResponse As Short
        Dim i As Short
        Dim bFileIsLocked As Boolean


        On Error GoTo ERR_CheckFile

        oFs = New Scripting.FileSystemObject

        iReturnValue = 0
        sPath = Trim(sPath)
        sFile = Right(sPath, InStrRev(sPath, "\") + 1)


        If Not EmptyString(sFile) Then
            'Check if file already exists
            If oFs.FileExists(sPath) Then
                oFile = oFs.GetFile(sPath)
                'UPGRADE_WARNING: Couldn't resolve default property of object oFile.Attributes. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If oFile.Attributes And Scripting.__MIDL___MIDL_itf_scrrun_0000_0000_0001.ReadOnly Then
                    'The file %sfile% is read-only. & Grant the user the appropriate permissions. The file may also be locked by an other application.
                    Select Case iErrorMessageType
                        Case ErrorMessageType.Dont_Show
                            'Do nothing
                        Case ErrorMessageType.Use_ERR_Raise
                            Err.Raise(30020, sMessageHeader, LRSCommon(30020, sPath))
                        Case ErrorMessageType.Use_MsgBox
                            MsgBox(LRSCommon(30020, sPath), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
                    End Select
                    iReturnValue = 51
                Else
                    'Check if the file is being use by another app
                    bFileIsLocked = False


                    i = FreeFile()

                    If (Dir(sPath) <> "") Then
                        On Error Resume Next
                        FileOpen(i, sPath, OpenMode.Binary, OpenAccess.Read, OpenShare.LockReadWrite)
                        If (Err.Number <> 0) Then
                            bFileIsLocked = True
                            Err.Clear()
                        Else
                            Lock(i)
                            If (Err.Number <> 0) Then
                                bFileIsLocked = True
                                Err.Clear()
                            Else
                                Unlock(i)
                            End If
                            FileClose(i)
                        End If
                    End If

                    If bFileIsLocked Then
                        'The file %sfile% is locked by another application or by another instance of BabelBank.
                        'Check if another application has opened the file, or try to restart Your computer.

                        ' added 17.09.2010, otherwise did not go to errroutine
                        On Error GoTo ERR_CheckFile

                        Select Case iErrorMessageType
                            Case ErrorMessageType.Dont_Show
                                'Do nothing
                            Case ErrorMessageType.Use_ERR_Raise
                                Err.Raise(30021, sMessageHeader, LRSCommon(30021, sPath))
                            Case ErrorMessageType.Use_MsgBox
                                MsgBox(LRSCommon(30021, sPath), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
                        End Select
                        iReturnValue = 52
                    Else
                        iReturnValue = 5
                    End If
                End If
            Else
                iReturnValue = 0
            End If
        Else
            'No filename specified in the path %1.
            Select Case iErrorMessageType
                Case ErrorMessageType.Dont_Show
                    'Do nothing
                Case ErrorMessageType.Use_ERR_Raise
                    Err.Raise(30022, sMessageHeader, LRSCommon(30022, sPath))
                Case ErrorMessageType.Use_MsgBox
                    MsgBox(LRSCommon(30022, sPath), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, sMessageHeader)
            End Select
            iReturnValue = 92
        End If

        If Not oFile Is Nothing Then
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        CheckFile = iReturnValue

        On Error GoTo 0
        Exit Function

ERR_CheckFile:

        If Not oFile Is Nothing Then
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        'An unexpected error occured during the validation vbcrlf of the path %1
        Select Case iErrorMessageType
            Case ErrorMessageType.Dont_Show
                'Do nothing
            Case ErrorMessageType.Use_ERR_Raise
                If Err.Number > 30000 And Err.Number < 30200 Then
                    Err.Raise(Err.Number, sMessageHeader, Err.Description)
                Else
                    Err.Raise(30023, sMessageHeader, LRSCommon(30023, sPath))
                End If
            Case ErrorMessageType.Use_MsgBox
                If Err.Number > 30000 And Err.Number < 30200 Then
                    MsgBox(Err.Description, MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
                Else
                    MsgBox(LRSCommon(30023, sPath), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, sMessageHeader)
                End If
        End Select
        If iReturnValue = 0 Then
            iReturnValue = 99
        End If

        CheckFile = iReturnValue

    End Function
    Function OLD_CheckFile(ByRef sPath As String, ByRef iErrorMessageType As ErrorMessageType, Optional ByRef sMessageHeader As String = "") As Short

        'This function checks if the file exists and if the folder is readonly.

        ' Returnvalues less than 10 are non-critical
        'The returnvalues
        ' 5 - The file exists
        ' 51 - File is read-only (if bCheckFileAlso = True)
        ' 52 - The file is locked by another process
        ' 92 - No file specified (if bCheckFileAlso = True)

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.File
        Dim sFile As String
        Dim iReturnValue As Short
        Dim iPos As Short
        Dim sTempPath1, sTempPath2 As String
        Dim bContinueLoop As Boolean
        Dim iResponse As Short
        Dim i As Short
        Dim bFileIsLocked As Boolean


        On Error GoTo ERR_CheckFile

        oFs = New Scripting.FileSystemObject

        iReturnValue = 0
        sPath = Trim(sPath)
        sFile = Right(sPath, InStrRev(sPath, "\") + 1)


        If Not EmptyString(sFile) Then
            'Check if file already exists
            If oFs.FileExists(sPath) Then
                oFile = oFs.GetFile(sPath)
                'UPGRADE_WARNING: Couldn't resolve default property of object oFile.Attributes. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If oFile.Attributes And Scripting.__MIDL___MIDL_itf_scrrun_0000_0000_0001.ReadOnly Then
                    'The file %sfile% is read-only. & Grant the user the appropriate permissions. The file may also be locked by an other application.
                    Select Case iErrorMessageType
                        Case ErrorMessageType.Dont_Show
                            'Do nothing
                        Case ErrorMessageType.Use_ERR_Raise
                            Err.Raise(30020, sMessageHeader, LRSCommon(30020, sPath))
                        Case ErrorMessageType.Use_MsgBox
                            MsgBox(LRSCommon(30020, sPath), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
                    End Select
                    iReturnValue = 51
                Else
                    'Check if the file is being use by another app
                    bFileIsLocked = False


                    i = FreeFile()

                    If (Dir(sPath) <> "") Then
                        On Error Resume Next
                        FileOpen(i, sPath, OpenMode.Binary, OpenAccess.Read, OpenShare.LockReadWrite)
                        If (Err.Number <> 0) Then
                            bFileIsLocked = True
                            Err.Clear()
                        Else
                            Lock(i)
                            If (Err.Number <> 0) Then
                                bFileIsLocked = True
                                Err.Clear()
                            Else
                                Unlock(i)
                            End If
                            FileClose(i)
                        End If
                    End If

                    If bFileIsLocked Then
                        'The file %sfile% is locked by another application or by another instance of BabelBank.
                        'Check if another application has opened the file, or try to restart Your computer.

                        ' added 17.09.2010, otherwise did not go to errroutine
                        On Error GoTo ERR_CheckFile

                        Select Case iErrorMessageType
                            Case ErrorMessageType.Dont_Show
                                'Do nothing
                            Case ErrorMessageType.Use_ERR_Raise
                                Err.Raise(30021, sMessageHeader, LRSCommon(30021, sPath))
                            Case ErrorMessageType.Use_MsgBox
                                MsgBox(LRSCommon(30021, sPath), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
                        End Select
                        iReturnValue = 52
                    Else
                        iReturnValue = 5
                    End If
                End If
            Else
                iReturnValue = 0
            End If
        Else
            'No filename specified in the path %1.
            Select Case iErrorMessageType
                Case ErrorMessageType.Dont_Show
                    'Do nothing
                Case ErrorMessageType.Use_ERR_Raise
                    Err.Raise(30022, sMessageHeader, LRSCommon(30022, sPath))
                Case ErrorMessageType.Use_MsgBox
                    MsgBox(LRSCommon(30022, sPath), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, sMessageHeader)
            End Select
            iReturnValue = 92
        End If

        If Not oFile Is Nothing Then
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        OLD_CheckFile = iReturnValue

        On Error GoTo 0
        Exit Function

ERR_CheckFile:

        If Not oFile Is Nothing Then
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        'An unexpected error occured during the validation vbcrlf of the path %1
        Select Case iErrorMessageType
            Case ErrorMessageType.Dont_Show
                'Do nothing
            Case ErrorMessageType.Use_ERR_Raise
                If Err.Number > 30000 And Err.Number < 30200 Then
                    Err.Raise(Err.Number, sMessageHeader, Err.Description)
                Else
                    Err.Raise(30023, sMessageHeader, LRSCommon(30023, sPath))
                End If
            Case ErrorMessageType.Use_MsgBox
                If Err.Number > 30000 And Err.Number < 30200 Then
                    MsgBox(Err.Description, MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
                Else
                    MsgBox(LRSCommon(30023, sPath), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, sMessageHeader)
                End If
        End Select
        If iReturnValue = 0 Then
            iReturnValue = 99
        End If

        OLD_CheckFile = iReturnValue

    End Function
    Private Function MakeFolder(ByRef sFolderName As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim sTmpFolder As String = ""
        Dim sFoldersToCheck As String = ""
        Dim iStartPos As Integer = 4  ' co beyond c:\
        On Error Resume Next

        oFs = New Scripting.FileSystemObject

        sFoldersToCheck = sFolderName  ' start with all possible folders

        ' 13.11.2012 - changed to be able to create folders also when there are several levels of subfolders missing;
        Do
            ' check if "topmost" folder exists; and carry on like that for each level of folders
            If InStr(iStartPos, sFoldersToCheck, "\") > 0 Then
                ' more folders to check
                sTmpFolder = Left(sFoldersToCheck, InStr(iStartPos, sFoldersToCheck, "\") - 1)
            Else
                ' "last" folder
                sTmpFolder = sFoldersToCheck
            End If
            ' does this folder exist?
            If Not oFs.FolderExists(sTmpFolder) Then
                ' then create it
                oFs.CreateFolder(sTmpFolder)

                If (Err.Number > 0) Then
                    MakeFolder = False
                    Exit Function
                End If
            End If

            ' completed ?
            iStartPos = InStr(iStartPos, sFoldersToCheck, "\") + 1
            If iStartPos > 1 And iStartPos < Len(sFoldersToCheck) Then
                ' more folders to check
            Else
                ' "last" folder
                Exit Do
            End If

        Loop


        'The above code doesn't function properly so we have to add this
        If oFs.FolderExists(sFolderName) Then
            MakeFolder = True
        Else
            MakeFolder = False
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

    End Function
    Function DeleteFilename(ByRef sFilename As String, Optional ByRef bShowErrorMessage As Boolean = False, Optional ByRef sMessageHeader As String = "") As Short
        Dim Fso As Scripting.FileSystemObject
        Dim oFile As Scripting.File
        Dim iReturnValue As Short

        'This function is used to delete files.
        ' Checks if the file exists, if the file is readonly
        ' Set the last parameter to .true. if you want to use the errormessages(remember the LRS-ids) in this function
        ' Returnvalues less than 10 are non-critical
        'FIX: We should receive the first LRS-ID in the function. Errormessages should then be created
        ' like this If LRS-id = 0 then msgbox "Errormessage" else msgbox lrs(receivedLRS-id + fixed number)
        'The returnvalues
        ' 0 - Everything OK, the file is deleted
        ' 1 - The file didn't exist
        '
        ' 11 - File is read-only
        ' 90 - No file specified
        ' 99 - Unspecified error

        On Error GoTo Error_Renamed

        Fso = New Scripting.FileSystemObject

        If Not Len(sFilename) = 0 Then
            If Fso.FileExists(sFilename) = True Then
                oFile = Fso.GetFile(sFilename)
                If oFile.Attributes And Scripting.__MIDL___MIDL_itf_scrrun_0000_0000_0001.ReadOnly Then
                    'The file %sfilename% is read-only. & Can't delete the file.
                    If bShowErrorMessage Then
                        MsgBox(LRS(30008, sFilename) & vbCrLf & LRS(15001), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
                    End If
                    iReturnValue = 11
                Else
                    Kill(sFilename)
                    iReturnValue = 0
                End If
            Else
                iReturnValue = 1
            End If
        Else
            iReturnValue = 90
        End If

        If Not oFile Is Nothing Then
            oFile = Nothing
        End If

        If Not Fso Is Nothing Then
            Fso = Nothing
        End If

        DeleteFilename = iReturnValue
        Exit Function

Error_Renamed:

        'The file %sfilename% can't be deleted.
        If bShowErrorMessage Then
            MsgBox(LRS(30009, sFilename) & vbCrLf & LRS(15001), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, sMessageHeader)
        End If

        If Not oFile Is Nothing Then
            oFile = Nothing
        End If

        If Not Fso Is Nothing Then
            Fso = Nothing
        End If

        DeleteFilename = 99

    End Function

    Public Function CheckAccountNoNOR(ByRef sAccountNo As String) As Boolean
        Dim bReturnValue As Boolean
        Dim iSumOfDigits As Short
        Dim iControlDigit As Short

        bReturnValue = False
        iSumOfDigits = 0

        If Not IsNumeric(sAccountNo) Then
            bReturnValue = False
            'Some accountno. has a different check.
        ElseIf Mid(sAccountNo, 5, 2) = "00" Then
            bReturnValue = True
        Else
            'New test added 04.02.2005
            If Len(Trim(sAccountNo)) = 11 Then
                iSumOfDigits = Val(Mid(sAccountNo, 1, 1)) * 5
                iSumOfDigits = iSumOfDigits + Val(Mid(sAccountNo, 2, 1)) * 4
                iSumOfDigits = iSumOfDigits + Val(Mid(sAccountNo, 3, 1)) * 3
                iSumOfDigits = iSumOfDigits + Val(Mid(sAccountNo, 4, 1)) * 2
                iSumOfDigits = iSumOfDigits + Val(Mid(sAccountNo, 5, 1)) * 7
                iSumOfDigits = iSumOfDigits + Val(Mid(sAccountNo, 6, 1)) * 6
                iSumOfDigits = iSumOfDigits + Val(Mid(sAccountNo, 7, 1)) * 5
                iSumOfDigits = iSumOfDigits + Val(Mid(sAccountNo, 8, 1)) * 4
                iSumOfDigits = iSumOfDigits + Val(Mid(sAccountNo, 9, 1)) * 3
                iSumOfDigits = iSumOfDigits + Val(Mid(sAccountNo, 10, 1)) * 2
                iControlDigit = System.Math.Abs(11 - iSumOfDigits + Int(iSumOfDigits / 11) * 11)
                If iControlDigit = 11 Then
                    iControlDigit = 0
                End If
                If iControlDigit = Val(Mid(sAccountNo, 11, 1)) Then
                    bReturnValue = True
                Else
                    bReturnValue = False
                End If
            Else
                bReturnValue = False
            End If
        End If

        CheckAccountNoNOR = bReturnValue

    End Function
    Function Strip(ByRef vPhrase As Object, ByRef sBadChars As String) As String
        ' Purpose: remove any of the characters in sBadChars from vPhrase
        Dim sPhrase As String
        Dim i As Short

        If IsDBNull(vPhrase) Or IsNothing(vPhrase) Or Len(sBadChars) = 0 Then
            'UPGRADE_WARNING: Couldn't resolve default property of object vPhrase. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Strip = vPhrase
        Else
            'UPGRADE_WARNING: Couldn't resolve default property of object vPhrase. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sPhrase = vPhrase
            i = 1
            Do Until i > Len(sPhrase)
                Do Until InStr(sBadChars, Mid(sPhrase, i, 1)) = 0 Or i > Len(sPhrase)
                    sPhrase = Left(sPhrase, i - 1) & Mid(sPhrase, i + 1)
                Loop
                i = i + 1
            Loop
            Strip = sPhrase
        End If
    End Function

    Public Function LStrip(ByRef sString As String, ByRef sBadChar As String) As String
        Dim lLength As Integer
        Dim l As Integer

        lLength = Len(sString)

        Do While True
            If Left(sString, 1) = sBadChar Then
                sString = Right(sString, lLength - l - 1)
            Else
                Exit Do
            End If
            l = l + 1
            If l = lLength Then
                sString = ""
                Exit Do
            End If
        Loop

        LStrip = sString

    End Function


    '***************************************************************
    ' Name: Get Windows and System Directories
    ' Description:Not all users have Windows neatly tucked away under
    '     a C:\Windows directory. What if they run two versions of Windows
    '     (more common right now than ever)? Hard-coding C:\Windows into yo
    '     ur programs to work with files in the Windows directory can be da
    '     ngerous at best. This tip will show you how to instruct Windows i
    '     tself to tell you exactly where it is (and it's system subdirecto
    '     ry as well).
    ' By: VB Pro
    '
    '
    ' Inputs:None
    '
    ' Returns:None
    '
    'Assumes:None
    '
    'Side Effects:None
    '
    'Code provided by Planet Source Code(tm) (http://www.Planet-Sourc
    '     e-Code.com) 'as is', without warranties as to performance, fitnes
    '     s, merchantability,and any other warranty (whether expressed or i
    '     mplied).
    'This code is for personal private or personal business use only
    '     and may not be redistributed or duplicated in any format without
    '     express written consent from Planet Source Code or Exhedra Soluti
    '     ons, Inc.
    '***************************************************************

    'Public Function GetWindowsDir() As String
    '    Dim Temp As New VB6.FixedLengthString(256)
    '    Dim x As Short
    '    x = GetWindowsDirectory(Temp.Value, Len(Temp.Value)) ' Make API Call (Temp will hold return value)
    '    GetWindowsDir = Left(Temp.Value, x) ' Trim Buffer and return String
    'End Function
    'Public Function GetSystemDir() As String
    '    Dim Temp As New VB6.FixedLengthString(256)
    '    Dim x As Short
    '    x = GetSystemDirectory(Temp.Value, Len(Temp.Value)) ' Make API Call (Temp will hold return value)
    '    GetSystemDir = Left(Temp.Value, x) ' Trim Buffer and return String
    'End Function

    'Public Function SetValueEx(ByVal hKey As Integer, ByRef sValueName As String, ByRef lType As Integer, ByRef vValue As Object) As Integer

    '    Dim nValue As Integer
    '    Dim sValue As String

    '    Select Case lType
    '        Case REG_SZ
    '            'UPGRADE_WARNING: Couldn't resolve default property of object vValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            sValue = vValue & Chr(0)
    '            SetValueEx = RegSetValueExString(hKey, sValueName, 0, lType, sValue, Len(sValue))

    '        Case REG_DWORD
    '            'UPGRADE_WARNING: Couldn't resolve default property of object vValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            nValue = vValue
    '            SetValueEx = RegSetValueExLong(hKey, sValueName, 0, lType, nValue, 4)

    '    End Select

    'End Function

    ' 31.03.2009 DOTTNET, fjernet, trenger ikke denne
    'Public Sub CreateNewKey(sNewKeyName As String, _
    ''                        lPredefinedKey As Long)
    '
    '  'handle to the new key
    '   Dim hKey As Long
    '   Dim result As Long
    '
    '   Call RegCreateKeyEx(lPredefinedKey, _
    ''                       sNewKeyName, 0&, _
    ''                       "", _
    ''                       REG_OPTION_NON_VOLATILE, _
    ''                       KEY_ALL_ACCESS, 0&, hKey, result)
    '
    '   Call RegCloseKey(hKey)
    '
    'End Sub

    ' 31.03.2009 DOTTNET, fjernet, trenger ikke denne
    'Public Sub SetKeyValue(sKeyName As String, _
    ''                       sValueName As String, _
    ''                       vValueSetting As Variant, _
    ''                       lValueType As Long)
    '
    '  'handle of opened key
    '   Dim hKey As Long
    '
    '  'open the specified key
    '   Call RegOpenKeyEx(HKEY_CLASSES_ROOT, _
    ''                    sKeyName, 0, _
    ''                    KEY_ALL_ACCESS, hKey)
    '
    '   Call SetValueEx(hKey, _
    ''                   sValueName, _
    ''                   lValueType, _
    ''                   vValueSetting)
    '
    '   Call RegCloseKey(hKey)
    '
    'End Sub
    'Public Function vbTimer(ByRef sText As String) As Object
    '    ' Used to clock in time for different procedures in vbBabel
    '    ' Stores total used time for each calling function into an array,
    '    ' When stopped, writes to a logfile, vbTimer.log, and writes arraycontent into it

    '    '######## not in use p.t. Remove next line in test!
    '    Exit Function

    '    Dim oFs As NewScripting.FileSystemObject
    '    Dim sLogFile As String
    '    Dim oFile As Scripting.TextStream
    '    Dim i, j As Integer
    '    Dim bFound As Boolean
    '    'Static sStartTime As Single
    '    'DOTNETT REDIM Static aResults(,) As Variant
    '    Static aResults(,) As Object

    '    If RunTime() Then
    '        ' do nothing when run in "RealWorld"    sLogFile = App.path + "\vbTimer.Log"
    '    Else
    '        If StrComp(sText, "start") = 0 Then
    '            ' reset array and timers;
    '            ReDim aResults(3, 0)
    '            If oFs.FileExists("c:\projects\babelbank\vbTimer.Log") Then
    '                ' Delete previous logfile;
    '                '------oFs.DeleteFile ("c:\projects\babelbank\vbTimer.Log")
    '            End If
    '        ElseIf StrComp(sText, "stop") = 0 Then
    '            ' Write to logfile;
    '            sLogFile = "c:\projects\babelbank\vbTimer.Log"
    '            oFile = oFs.OpenTextFile(sLogFile, Scripting.IOMode.ForAppending, True) 'Creates file if not present
    '            ' One line for each element in array
    '            For i = 0 To UBound(aResults, 2) - 1
    '                'UPGRADE_WARNING: Couldn't resolve default property of object aResults(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                oFile.WriteLine(PadRight(Left(aResults(0, i), 30), 30, " ") & "   " & VB6.Format(aResults(1, i), "###0.00") & "  " & Str(aResults(3, i)))
    '            Next i
    '            oFile.WriteLine(CStr(Now))

    '            'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            oFile = Nothing ' Close file
    '        Else
    '            ' look up function name in array, and set or reset timer, and calculate result
    '            bFound = False
    '            ' find arrayelement;

    '            i = 0
    '            Do While True
    '                'UPGRADE_WARNING: Couldn't resolve default property of object aResults(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                If StrComp(aResults(0, i), sText) = 0 Then
    '                    'UPGRADE_WARNING: Couldn't resolve default property of object aResults(2, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                    If aResults(2, i) = 0 Then
    '                        ' start of timing for this function
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResults(2, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        aResults(2, i) = VB.Timer()
    '                    Else
    '                        ' reached end of function, stop timing, and add to total
    '                        ' starttime is in aResults(2,i)
    '                        ' add to totaltime (aResults(1,i)
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResults(2, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResults(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResults(1, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        aResults(1, i) = aResults(1, i) + (VB.Timer() - aResults(2, i))
    '                        ' add to counter for this function
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResults(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResults(3, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        aResults(3, i) = aResults(3, i) + 1
    '                        ' Reset starttime
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResults(2, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        aResults(2, i) = 0
    '                    End If
    '                    bFound = True
    '                    Exit Do
    '                End If
    '                If i = UBound(aResults, 2) Then
    '                    Exit Do
    '                Else
    '                    i = i + 1
    '                End If
    '            Loop


    '            If Not bFound Then
    '                ' function not found in array, create;
    '                ReDim Preserve aResults(3, UBound(aResults, 2) + 1)
    '                'ReDim Preserve aPossibleInvoiceNos(3, UBound(aPossibleInvoiceNos, 2) + 1)
    '                'UPGRADE_WARNING: Couldn't resolve default property of object aResults(0, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                aResults(0, i) = sText
    '                ' start of timing for this function
    '                'UPGRADE_WARNING: Couldn't resolve default property of object aResults(2, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                aResults(2, i) = VB.Timer()
    '                'UPGRADE_WARNING: Couldn't resolve default property of object aResults(3, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                aResults(3, i) = 0 ' Counter
    '            End If
    '        End If
    '    End If

    'End Function
    Public Function AddCDV11digit(ByRef s As String) As String

        Dim l As Integer
        Dim lFactor As Integer
        Dim lSum As Integer 'added as Long 05.08.2008
        Dim iReturnValue As Short

        '0052001002759
        's = "005200100275"
        lFactor = 2
        lSum = 0
        iReturnValue = 99

        For l = Len(s) To 1 Step -1
            lSum = lSum + Val(Mid(s, l, 1)) * lFactor
            If lFactor = 7 Then
                lFactor = 2
            Else
                lFactor = lFactor + 1
            End If
        Next

        iReturnValue = (11 - (lSum Mod 11))

        Select Case iReturnValue
            Case 10
                AddCDV11digit = "-"
            Case 11
                AddCDV11digit = "0"
            Case Else
                AddCDV11digit = Trim(Str(iReturnValue))
        End Select

    End Function
    Public Function IsModulus11(ByRef sTestString As String) As Boolean
        ' Test if string passes an modulus-11 test
        ' Use AddCDV11Digit, and see if last digit is equal

        'New 22.03.2007 ' Must be numeric to be valid or '-'
        If vbIsNumeric(sTestString, "0123456789-") Then
            If Right(sTestString, 1) = Trim(AddCDV11digit(Left(sTestString, Len(sTestString) - 1))) Then
                IsModulus11 = True
            Else
                IsModulus11 = False
            End If
        Else
            IsModulus11 = False
        End If

    End Function
    Public Function AddCDVReferenceDigit(ByRef s As String) As String

        Dim l As Integer
        Dim lSum As Integer
        Dim iReturnValue As Short
        Dim sWeightsString As String

        'Grunnreferencen numbers must be mulitplied with the weights 7,3,1,7,3,1... starting from the right.
        'Grunnreferencen:    1  2  3  4  5  6
        'Weight:             1  3  7  1  3  7
        'Add the products:   1+ 6+21+ 4+15+42= 89
        'Deduct the sum from nearest 10      - 90
        '                                   -------
        'The difference is the controldigit     1

        'maximumlength is 20 (including the controldigit)

        sWeightsString = Right("7137137137137137137", Len(s))

        lSum = 0

        For l = Len(s) To 1 Step -1
            lSum = lSum + Val(Mid(s, l, 1)) * Val(Mid(sWeightsString, l, 1))
        Next

        iReturnValue = (10 - (lSum Mod 10))

        If iReturnValue = 10 Then
            iReturnValue = 0
        End If

        AddCDVReferenceDigit = Trim(Str(iReturnValue))

    End Function
    Public Function IsModulusReference(ByRef sTestString As String) As Boolean
        ' Test if string passes an modulus-11 test
        ' Use AddCDV11Digit, and see if last digit is equal

        'sTestString = "1590000010200820"
        'New 22.03.2007 ' Must be numeric to be valid 
        If vbIsNumeric(sTestString, "0123456789") Then
            If Right(sTestString, 1) = Trim(AddCDVReferenceDigit(Left(sTestString, Len(sTestString) - 1))) Then
                IsModulusReference = True
            Else
                IsModulusReference = False
            End If
        Else
            IsModulusReference = False
        End If

    End Function
    Public Function IsModulusBirthnumberNO(ByRef sTestString As String) As Boolean
        ' Check if sTestString passes moduluscontrol for Norwegian Birthnumbers
        ' De to siste sifre er kontrollsifre. Disse beregnes slik;
        'Kontrollsifrene beregnes av de ni f�rste sifrene i f�dselsnummeret.
        'For � finne det f�rste kontrollsifferet, K1, multipliseres de ni f�rste
        'sifrene i
        'f�dselsnummeret med faste vekter. Produktsummen divideres med 11 ved
        'heltallsdivisjon. Resttallet ved denne divisjonen trekkes fra 11 for �
        'finne K1.
        '
        'Vektene er:
        '3, 7, 6, 1, 8, 9, 4, 5 og 2
        'k1 = 11 - ((3*D1 + 7*D2 + 6*M1 + 1*M2 + 8*�1 + 9*�2 + 4*I1 + 5*I2 +
        '2*I3) - 11*Q1)
        '
        'Q1 st�r for heltallskvotienten for produktsummen dividert med 11, dvs det
        'h�yeste tallet som multiplisert med 11 gir produktsummen eller mindre.
        'Det blir ikke tildelt f�dselsnummer som gir k1 = 10.
        'Dersom k1 = 11, settes K1 lik 0, ellers er K1 lik k1.
        '
        'Det andre kontrollsifferet, K2, beregnes p� tilsvarende m�te av de 9
        'f�rste
        'sifrene og det f�rste kontrollsifferet.
        '
        'Vektene her er:
        '5, 4, 3, 2, 7, 6, 5, 4, 3 og 2
        'k2 = 11 - ((5*D1 + 4*D2 + 3*M1 + 2*M2 + 7*�1 + 6*�2 + 5*I1 + 4*I2 +
        '3*I3 + 2*K1) - 11*Q2)
        'Her st�r Q2 for heltallskvotienten for produktsummen dividert med 11, dvs
        'det h�yeste tallet som multiplisert med 11 gir produktsummen eller mindre.
        'Det blir ikke tildelt f�dselsnummer som gir k2 = 10.
        'Dersom k2 = 11, settes K2 lik 0, ellers er K2 lik k2.
        Dim bOK As Boolean
        Dim k1, k2 As Short

        bOK = True

        ' Start by calculating first checksum (k1)
        k1 = 11 - (3 * CDbl(Mid(sTestString, 1, 1)) + 7 * CDbl(Mid(sTestString, 2, 1)) + 6 * CDbl(Mid(sTestString, 3, 1)) + 1 * CDbl(Mid(sTestString, 4, 1)) + 8 * CDbl(Mid(sTestString, 5, 1)) + 9 * CDbl(Mid(sTestString, 6, 1)) + 4 * CDbl(Mid(sTestString, 7, 1)) + 5 * CDbl(Mid(sTestString, 8, 1)) + 2 * CDbl(Mid(sTestString, 9, 1)))
        k1 = k1 - (Int(k1 / 11) * 11)
        If k1 = 11 Then
            k1 = 0
        End If

        If k1 = CDbl(Mid(sTestString, 10, 1)) Then
            ' Calculate second checksum (k2)
            k2 = 11 - (5 * CDbl(Mid(sTestString, 1, 1)) + 4 * CDbl(Mid(sTestString, 2, 1)) + 3 * CDbl(Mid(sTestString, 3, 1)) + 2 * CDbl(Mid(sTestString, 4, 1)) + 7 * CDbl(Mid(sTestString, 5, 1)) + 6 * CDbl(Mid(sTestString, 6, 1)) + 5 * CDbl(Mid(sTestString, 7, 1)) + 4 * CDbl(Mid(sTestString, 8, 1)) + 3 * CDbl(Mid(sTestString, 9, 1)) + 2 * CDbl(Mid(sTestString, 10, 1)))
            k2 = k2 - (Int(k2 / 11) * 11)
            If k2 = 11 Then
                k2 = 0
            End If
            If k2 <> CDbl(Mid(sTestString, 11, 1)) Then
                bOK = False
            End If
        Else
            bOK = False
        End If

        IsModulusBirthnumberNO = bOK

    End Function
    Public Function AddCDV10digit(ByRef s As String) As String

        Dim lCount As Integer
        Dim lFactor As Integer
        Dim lSum As Integer
        Dim lProduct As Integer
        Dim iReturnValue As Short

        '     0052001002759
        's = "005200100275"
        lFactor = 2
        lSum = 0
        iReturnValue = 99

        For lCount = Len(s) To 1 Step -1
            lProduct = Val(Mid(s, lCount, 1)) * lFactor
            If lProduct > 9 Then
                ' add digits if multipication gives > 9
                lSum = lSum + Val(Left(CStr(lProduct), 1)) + Val(Right(CStr(lProduct), 1))
            Else
                ' less than 10, one digit only
                lSum = lSum + lProduct
            End If
            If lFactor = 1 Then
                lFactor = 2
            Else
                lFactor = 1
            End If
        Next
        iReturnValue = (10 - (lSum Mod 10))
        If iReturnValue = 10 Then
            iReturnValue = 0
        End If
        AddCDV10digit = Trim(Str(iReturnValue))

    End Function

    Public Function IsModulus10(ByRef sTestString As String) As Boolean
        ' Test if string passes an modulus-10 test
        ' Use AddCDV10Digit, and see if last digit is equal

        'sTestString = "10048110349880"

        'New 22.03.2007 ' Must be numeric to be valid
        If vbIsNumeric(sTestString, "0123456789") Then
            If Right(sTestString, 1) = Trim(AddCDV10digit(Left(sTestString, Len(sTestString) - 1))) Then
                IsModulus10 = True
            Else
                IsModulus10 = False
            End If
        Else
            IsModulus10 = False
        End If

    End Function
    Public Function AddCDV7Digit(ByRef s As String) As String
        Dim lCounter As Integer
        Dim lFactor As Integer
        Dim lSum As Integer
        Dim lNumber As Integer
        Dim lReturnValue As Integer

        's = "04403620" gives 5

        lFactor = 3
        lSum = 0
        lReturnValue = 99

        For lCounter = 1 To Len(s)
            ' Factor = 3 for 1, 4, 7 etc
            ' Factor = 7 for 2, 5, 8 etc
            ' Factor = 1 for 3, 6, 9 etc
            lNumber = Val(Mid(s, lCounter, 1))

            If System.Math.Abs((lCounter + 2) / 3 - Int((lCounter + 2) / 3)) < 0.001 Then
                lFactor = 3
            ElseIf System.Math.Abs((lCounter + 1) / 3 - Int((lCounter + 1) / 3)) < 0.001 Then
                lFactor = 7
            Else
                lFactor = 1
            End If
            lSum = lSum + (lNumber * lFactor)
        Next

        lReturnValue = (10 - Val(Right(Str(lSum), 1)))
        ' added 2008.03.21 IS THIS CORRECT
        ' If calculates to 10, return 0
        If lReturnValue = 10 Then
            lReturnValue = 0
        End If
        AddCDV7Digit = Trim(Str(lReturnValue))

    End Function



    Function PadLeft(ByVal sVariable As String, ByRef iLength As Short, ByRef sType As String) As String

        'sVariable is the variable
        'iLength is the length of the string you want returned
        'sType is the key you want to use to pad the string i.e. " " or "0".

        If Len(Trim(sVariable)) > iLength Then
            PadLeft = Left(Trim(sVariable), iLength)
        Else
            sVariable = New String(sType, iLength) & Trim(sVariable)
            PadLeft = Right(sVariable, iLength)
        End If
        'PadLeft = String(iLength - Len(Trim(sVariable)), sType) & trim(sVariable)


    End Function
    Function PadRight(ByVal sVariable As String, ByRef iLength As Short, ByRef sType As String) As String

        'sVariable is the variable
        'iLength is the length of the string you want returned
        'sType is the key you want to use to pad the string i.e. " " or "0".

        If Len(Trim(sVariable)) > iLength Then
            PadRight = Left(LTrim(sVariable), iLength)
        Else
            sVariable = Trim(sVariable) & New String(sType, iLength)
            PadRight = Left(sVariable, iLength)
        End If
        'PadRight = trim(sVariable) & String(iLength - Len(Trim(sVariable)), sType)

    End Function
    Function PadRightWithoutTrim(ByVal sVariable As String, ByRef iLength As Short, ByRef sType As String) As String
        ' 14.11.2007
        ' A bug in Padright trims the start of the string, which gives errors when the strings start with blank
        'sVariable is the variable
        'iLength is the length of the string you want returned
        'sType is the key you want to use to pad the string i.e. " " or "0".

        If Len(sVariable) > iLength Then
            PadRightWithoutTrim = Left(sVariable, iLength)
        Else
            sVariable = sVariable & New String(sType, iLength) ' Has no Trim$ here, which we have in Function PadRight
            PadRightWithoutTrim = Left(sVariable, iLength)
        End If

    End Function

    Friend Function CheckISO20022Amount(ByRef sAmount As String, ByVal sPossibleDecimalSep As String) As String
        Dim sReturnValue As String = String.Empty
        Dim iPos As Integer
        Dim nDecimalAmount As Double = 0


        If EmptyString(sAmount) Then
            sReturnValue = 0
        Else
            If EmptyString(sPossibleDecimalSep) Then
                sReturnValue = sAmount
            Else
                iPos = sAmount.IndexOf(sPossibleDecimalSep)
                If iPos > 0 Then
                    iPos = sAmount.Length - iPos - 1
                    If iPos > 2 Then
                        nDecimalAmount = CDbl(Decimal.Parse(sAmount, System.Globalization.CultureInfo.InvariantCulture))
                        sAmount = Math.Round(nDecimalAmount, 2).ToString
                        sReturnValue = sAmount.Replace(",", ".")
                        'nDecimalAmount = Math.Round(Convert.ToDouble(sAmount), 2)
                        'nDecimalAmount = CDbl(sAmount.Substring(sAmount.Length - iPos + 2)) 'Including decimals
                        'nDecimalAmount = nDecimalAmount + CDbl(sAmount.Substring(sAmount.Length - iPos + 3)) ' Math.Round(nDecimalAmount / (10 ^ iPos - 2), 0)
                        'sReturnValue = sAmount.Substring(0, sAmount.Length - iPos) & sPossibleDecimalSep & nDecimalAmount.ToString
                    Else
                        sReturnValue = sAmount
                    End If
                Else
                    sReturnValue = sAmount
                End If
            End If
        End If

        Return sReturnValue

    End Function
    Function ConvertToAmount(ByRef sAmount As String, ByRef sPossibleThousandSep As String, ByRef sPossibleDecimalSep As String, Optional ByRef bSigned As Boolean = True, Optional ByVal bCheckISO20022Amount As Boolean = False) As String
        Dim sDecimalPart As String
        'Dim iStringCounter As Integer
        'Dim iStartPosDecimal As Integer
        Dim sDecimalAmount As String
        Dim sFirstPartofAmount, sThousandSep As String
        Dim bNegative As Boolean
        Dim bError As Boolean
        Dim i, j As Short

        If bCheckISO20022Amount Then '19.12-2017 - Added this for Swedbank (Visma-customer) where the may be several decimals stated in the amount
            sAmount = CheckISO20022Amount(sAmount, sPossibleDecimalSep)
        End If

        sDecimalAmount = ""
        bNegative = False
        bError = False

        sAmount = Trim(sAmount)

        If Len(sAmount) = 0 Then
            sAmount = "0"
        End If

        If bSigned Then
            If Left(sAmount, 1) = "-" Then
                bNegative = True
                sAmount = Trim(Right(sAmount, Len(sAmount) - 1))
            ElseIf Left(sAmount, 1) = "+" Then
                bNegative = False
                sAmount = Trim(Right(sAmount, Len(sAmount) - 1))
            ElseIf Right(sAmount, 1) = "-" Then
                bNegative = True
                sAmount = Trim(Left(sAmount, Len(sAmount) - 1))
            ElseIf Right(sAmount, 1) = "+" Then
                bNegative = True
                sAmount = Trim(Left(sAmount, Len(sAmount) - 1))
            End If
        End If

        'Maybe we shall remove leading zeros here, but it should not be necessary


        'Find the part where the decimalseperator may be.
        If Len(sAmount) > 3 Then
            sDecimalPart = Right(sAmount, 3)
        Else
            sDecimalPart = sAmount
        End If

        If sPossibleDecimalSep = "" Then
            Err.Raise(30008, , LRSCommon(30008) & vbCrLf & vbCrLf & LRSCommon(10016))
            'Kontakt din leverand�r. "Det m� angis mulige desimalskilletegn p� bel�p." & vbCrLf & vbCrLf & _
            '"", vbCritical, "Feil i innlesningsmodul!"
            bError = True
        Else
            'Find the decimalpart
            For i = 1 To Len(sPossibleDecimalSep)
                If InStr(1, sDecimalPart, Mid(sPossibleDecimalSep, i, 1)) Then
                    'Changed 04.01.2006 to master the amount "0,"
                    sDecimalAmount = Right(sDecimalPart, Len(sDecimalPart) + 1 - InStrRev(sDecimalPart, Mid(sPossibleDecimalSep, i, 1)))
                    'Old code
                    'sDecimalAmount = Right(sDecimalPart, 4 - InStrRev(sDecimalPart, Mid$(sPossibleDecimalSep, i, 1)))
                    Select Case Len(sDecimalAmount)
                        Case 1
                            'The amount ends with the decimalsep
                            sDecimalAmount = "00"
                            sFirstPartofAmount = Left(sAmount, Len(sAmount) - 1)
                        Case 2
                            'The amount ends with decimalsep & digit
                            sDecimalAmount = Right(sDecimalAmount, 1) & 0
                            sFirstPartofAmount = Left(sAmount, Len(sAmount) - 2)
                        Case 3
                            'The amount ends with decimalsep & 2 digits
                            'sDecimalAmount = "," & Right(sDecimalAmount, 2)
                            sDecimalAmount = Right(sDecimalAmount, 2)
                            sFirstPartofAmount = Left(sAmount, Len(sAmount) - 3)

                    End Select
                End If
            Next i
            'No decimalsep found.
            If Len(sDecimalAmount) = 0 Then
                sDecimalAmount = "00"
                sFirstPartofAmount = sAmount
            End If

            'Added 04.01.2006 to master the amount ",0"
            If EmptyString(sFirstPartofAmount) Then
                sFirstPartofAmount = "0"
            End If

            If sPossibleThousandSep = "" Then
                'No Thousendseps are allowed
                If Not IsNumeric(sFirstPartofAmount) Or InStr(1, sFirstPartofAmount, " ") > 0 Then
                    Err.Raise(30009, , LRSCommon(30009, sAmount) & vbCrLf & vbCrLf & LRSCommon(10015))
                    'MsgBox "Error in amount" "The amount " & sAmount & " is not valid" & vbCrLf & vbCrLf & _
                    '"BabelBank avbrytes"
                    bError = True
                End If
            Else
                'First test if the rest is numeric, then its OK
                'If Not IsNumeric(sFirstPartofAmount) Or InStr(1, sFirstPartofAmount, " ") > 0 Then
                For i = 1 To Len(sPossibleThousandSep)
                    sThousandSep = Mid(sPossibleThousandSep, i, 1)
                    If InStr(1, sFirstPartofAmount, sThousandSep) > 0 Then
                        For j = 4 To Len(sFirstPartofAmount) Step 4
                            If Left(Right(sFirstPartofAmount, j), 1) <> sThousandSep Then
                                Err.Raise(30009, , LRSCommon(30009, sAmount) & vbCrLf & vbCrLf & LRSCommon(10015))
                                'MsgBox "Error in amount" "The amount " & sAmount & " is not valid" & vbCrLf & vbCrLf & _
                                '"BabelBank avbrytes"
                                bError = True
                            End If
                        Next j
                        sFirstPartofAmount = Replace(sFirstPartofAmount, sThousandSep, "")
                        Exit For
                    End If
                Next i
                If Not IsNumeric(sFirstPartofAmount) Or InStr(sFirstPartofAmount, ",") > 0 Or InStr(sFirstPartofAmount, ".") > 0 Or InStr(sFirstPartofAmount, " ") > 0 Then
                    Err.Raise(30009, , LRSCommon(30009, sAmount) & vbCrLf & vbCrLf & LRSCommon(10015))
                    'MsgBox "Error in amount" "The amount " & sAmount & " is not valid" & vbCrLf & vbCrLf & _
                    '"BabelBank avbrytes"
                    bError = True
                End If
                'End If
            End If
        End If

        If bError Then
            ConvertToAmount = "ERROR"
        Else
            ConvertToAmount = Trim(sFirstPartofAmount) & Trim(sDecimalAmount)
            If bNegative Then
                ConvertToAmount = "-" & ConvertToAmount
            End If
        End If

    End Function
    Function ConvertToAmountNet(ByRef sAmount As String, ByRef sPossibleThousandSep As String, ByRef sPossibleDecimalSep As String, Optional ByRef bSigned As Boolean = True) As String
        Dim sDecimalPart As String
        'Dim iStringCounter As Integer
        'Dim iStartPosDecimal As Integer
        Dim sDecimalAmount As String
        Dim sFirstPartofAmount, sThousandSep As String
        Dim bNegative As Boolean
        Dim bError As Boolean
        Dim i, j As Short
        Dim sDecimalSign As String = ""

        sDecimalAmount = ""
        bNegative = False
        bError = False

        If sAmount = "" Or sAmount Is Nothing Then
            ConvertToAmountNet = 0
        Else
            sAmount = sAmount.Trim

            If bSigned Then
                If sAmount.Substring(0, 1) = "-" Then
                    bNegative = True
                    sAmount = sAmount.Substring(1)
                ElseIf sAmount.Substring(0, 1) = "+" Then
                    bNegative = False
                    sAmount = sAmount = sAmount.Substring(1)
                ElseIf Right(sAmount, 1) = "-" Then
                    bNegative = True
                    sAmount = sAmount.Trim.Substring(0, sAmount.Trim.Length - 1) ''  Trim(Left(sAmount, Len(sAmount) - 1))
                ElseIf Right(sAmount, 1) = "+" Then
                    bNegative = True
                    sAmount = sAmount.Trim.Substring(0, sAmount.Trim.Length - 1)  ''Trim(Left(sAmount, Len(sAmount) - 1))
                End If
            End If

            'Maybe we shall remove leading zeros here, but it should not be necessary


            'Find the part where the decimalseperator may be.
            'If Len(sAmount) > 3 Then
            '    sDecimalPart = Right(sAmount, 3)
            'Else
            '    sDecimalPart = sAmount
            'End If

            If sPossibleDecimalSep = "" Then
                Err.Raise(30008, , LRSCommon(30008) & vbCrLf & vbCrLf & LRSCommon(10016))
                'Kontakt din leverand�r. "Det m� angis mulige desimalskilletegn p� bel�p." & vbCrLf & vbCrLf & _
                '"", vbCritical, "Feil i innlesningsmodul!"
                bError = True
            Else
                'Find the decimalpart
                For i = 1 To Len(sPossibleDecimalSep)
                    sDecimalSign = sPossibleDecimalSep.Substring(i - 1, 1)
                    If InStr(1, sAmount, Mid(sPossibleDecimalSep, i, 1)) Then
                        'Changed 04.01.2006 to master the amount "0,"
                        sDecimalAmount = Right(sAmount, Len(sAmount) + 1 - InStrRev(sAmount, Mid(sPossibleDecimalSep, i, 1)))
                        'Old code
                        'sDecimalAmount = Right(sDecimalPart, 4 - InStrRev(sDecimalPart, Mid$(sPossibleDecimalSep, i, 1)))
                        Select Case Len(sDecimalAmount)
                            Case 1
                                'The amount ends with the decimalsep
                                sDecimalAmount = "00"
                                sFirstPartofAmount = Left(sAmount, Len(sAmount) - 1)
                            Case 2
                                'The amount ends with decimalsep & digit
                                sDecimalAmount = Right(sDecimalAmount, 1) & 0
                                sFirstPartofAmount = Left(sAmount, Len(sAmount) - 2)
                            Case 3
                                'The amount ends with decimalsep & 2 digits
                                'sDecimalAmount = "," & Right(sDecimalAmount, 2)
                                sDecimalAmount = Right(sDecimalAmount, 2)
                                sFirstPartofAmount = Left(sAmount, Len(sAmount) - 3)
                            Case Else
                                ' there are more than two decimaldigits
                                ' take 2 digits after decimalsep as decimalamount
                                ' 21,123456
                                sDecimalAmount = sAmount.Substring(sAmount.IndexOf(sDecimalSign))   ' like ,123456
                                ' reduce to two decimals
                                sDecimalAmount = sDecimalAmount.Substring(1, 2)   ' like 12
                                sFirstPartofAmount = sAmount.Substring(0, sAmount.IndexOf(sDecimalSign))

                        End Select
                    End If
                Next i
                'No decimalsep found.
                If Len(sDecimalAmount) = 0 Then
                    sDecimalAmount = "00"
                    sFirstPartofAmount = sAmount
                End If

                'Added 04.01.2006 to master the amount ",0"
                If EmptyString(sFirstPartofAmount) Then
                    sFirstPartofAmount = "0"
                End If

                If sPossibleThousandSep = "" Then
                    'No Thousendseps are allowed
                    If Not IsNumeric(sFirstPartofAmount) Or InStr(1, sFirstPartofAmount, " ") > 0 Then
                        Err.Raise(30009, , LRSCommon(30009, sAmount) & vbCrLf & vbCrLf & LRSCommon(10015))
                        'MsgBox "Error in amount" "The amount " & sAmount & " is not valid" & vbCrLf & vbCrLf & _
                        '"BabelBank avbrytes"
                        bError = True
                    End If
                Else
                    'First test if the rest is numeric, then its OK
                    'If Not IsNumeric(sFirstPartofAmount) Or InStr(1, sFirstPartofAmount, " ") > 0 Then
                    For i = 1 To Len(sPossibleThousandSep)
                        sThousandSep = Mid(sPossibleThousandSep, i, 1)
                        If InStr(1, sFirstPartofAmount, sThousandSep) > 0 Then
                            For j = 4 To Len(sFirstPartofAmount) Step 4
                                If Left(Right(sFirstPartofAmount, j), 1) <> sThousandSep Then
                                    Err.Raise(30009, , LRSCommon(30009, sAmount) & vbCrLf & vbCrLf & LRSCommon(10015))
                                    'MsgBox "Error in amount" "The amount " & sAmount & " is not valid" & vbCrLf & vbCrLf & _
                                    '"BabelBank avbrytes"
                                    bError = True
                                End If
                            Next j
                            sFirstPartofAmount = Replace(sFirstPartofAmount, sThousandSep, "")
                            Exit For
                        End If
                    Next i
                    If Not IsNumeric(sFirstPartofAmount) Or InStr(sFirstPartofAmount, ",") > 0 Or InStr(sFirstPartofAmount, ".") > 0 Or InStr(sFirstPartofAmount, " ") > 0 Then
                        Err.Raise(30009, , LRSCommon(30009, sAmount) & vbCrLf & vbCrLf & LRSCommon(10015))
                        'MsgBox "Error in amount" "The amount " & sAmount & " is not valid" & vbCrLf & vbCrLf & _
                        '"BabelBank avbrytes"
                        bError = True
                    End If
                    'End If
                End If
            End If

            If bError Then
                ConvertToAmountNet = "ERROR"
            Else
                ConvertToAmountNet = Trim(sFirstPartofAmount) & Trim(sDecimalAmount)
                If bNegative Then
                    ConvertToAmountNet = "-" & ConvertToAmountNet
                End If
            End If
        End If

    End Function
    Public Function ConvertFromAmountToString(ByRef nAmount As Double, Optional ByRef sThousandSep As String = "", Optional ByRef sDecimalSep As String = "") As String
        Dim sAmount As String
        Dim bNegative As Boolean

        sAmount = Trim(Str(nAmount))
        If Left(sAmount, 1) = "-" Then
            bNegative = True
            sAmount = Mid(sAmount, 2)
        Else
            bNegative = False
        End If

        Select Case Len(sAmount)
            Case 1
                sAmount = "00" & sAmount
            Case 2
                sAmount = "0" & sAmount
            Case Else
                'OK
        End Select

        If Len(sThousandSep) > 0 Then
            If Len(sAmount) > 5 Then
                sAmount = Left(sAmount, Len(sAmount) - 5) & sThousandSep & Right(sAmount, 5)
                If Len(sAmount) > 9 Then
                    sAmount = Left(sAmount, Len(sAmount) - 9) & sThousandSep & Right(sAmount, 9)
                    If Len(sAmount) > 13 Then
                        sAmount = Left(sAmount, Len(sAmount) - 13) & sThousandSep & Right(sAmount, 13)
                        If Len(sAmount) > 17 Then
                            sAmount = Left(sAmount, Len(sAmount) - 17) & sThousandSep & Right(sAmount, 17)
                        End If
                    End If
                End If
            End If
        End If

        If Len(sDecimalSep) > 0 Then
            sAmount = Left(sAmount, Len(sAmount) - 2) & sDecimalSep & Right(sAmount, 2)
        End If

        If bNegative Then
            sAmount = "-" & sAmount
        End If

        ConvertFromAmountToString = sAmount

    End Function
    Public Function vbIsNumeric(ByRef vTest As Object, Optional ByRef sAllowedChars As String = "") As Boolean
        ' A subsistute for vbs erroneous IsNumeric!
        ' Including decimalseps !
        ' Changed 23.05.03 by JanP, added sAllowedChars, to be able to set which chars/digits are allowed
        Dim i, iLen As Integer
        If Len(Trim(sAllowedChars)) = 0 Then
            ' XNET 23.05.2012 - also added - because we use this one to test amounts
            'sAllowedChars = "0123456789.,"
            sAllowedChars = "0123456789.,-"
        End If
        vbIsNumeric = True
        'Changed 14.06.2005 by Kjell - returned true when the string " " was passed and <blank> wasn't an allowed character
        'From
        'iLen = Len(Trim$(vTest))
        'To
        iLen = Len(vTest)
        For i = 1 To iLen
            'UPGRADE_WARNING: Couldn't resolve default property of object vTest. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If InStr(sAllowedChars, Mid(vTest, i, 1)) = 0 Then
                ' at least one char is not numeric/allowed
                vbIsNumeric = False
                Exit For
            End If
        Next i
        ' added 03.06.2008 by JanP
        ' We can't treat emptystrings as valid numeric strings!
        If Len(vTest) = 0 Then
            vbIsNumeric = False
        End If
    End Function
    Public Function KeepNumericsOnly(ByRef vTest As Object) As String
        Dim sAllowedChars As String = "0123456789"
        Dim iLen As Integer = Len(vTest)
        Dim i As Integer
        Dim sReturnString As String = ""

        For i = 1 To iLen
            If InStr(sAllowedChars, Mid(vTest, i, 1)) > 0 Then
                ' OK, keep
                sReturnString = sReturnString & Mid(vTest, i, 1)
            End If
        Next i
        KeepNumericsOnly = sReturnString
    End Function
    Public Function KeepNumericsAndSignsOnly(ByRef vTest As Object) As String
        Dim sAllowedChars As String = "0123456789.,-+"
        Dim iLen As Integer = Len(vTest)
        Dim i As Integer
        Dim sReturnString As String = ""

        For i = 1 To iLen
            If InStr(sAllowedChars, Mid(vTest, i, 1)) > 0 Then
                ' OK, keep
                sReturnString = sReturnString & Mid(vTest, i, 1)
            End If
        Next i
        KeepNumericsAndSignsOnly = sReturnString
    End Function
    Public Function DateToString(ByRef dDate As Date) As String

        DateToString = CStr(VB6.Format(dDate, "YYYYMMDD"))

    End Function
    Public Function DateStringToString(ByRef sDate As String) As String
        ' Input a string like 29.04.2016, retur a string like 20160429
        DateStringToString = CStr(VB6.Format(sDate, "YYYYMMDD"))

    End Function
    Public Function StringToDate(ByRef sString As String) As Date

        StringToDate = CDate(DateSerial(CInt(Left(sString, 4)), CInt(Mid(sString, 5, 2)), CInt(Right(sString, 2))))

    End Function
    Public Function StringContainsValidDate(ByRef sString As String) As Boolean
        Dim bReturnValue As Boolean

        bReturnValue = False

        If Len(sString) <> 8 Then
            bReturnValue = False
        Else
            If Val(Left(sString, 4)) < 1990 Then
                bReturnValue = False
            Else
                If Val(Left(sString, 4)) > 2100 Then
                    bReturnValue = False
                Else
                    sString = Left(sString, 4) & "/" & Mid(sString, 5, 2) & "/" & Right(sString, 2)
                    bReturnValue = IsDate(sString)
                End If
            End If
        End If

        StringContainsValidDate = bReturnValue

    End Function
    Public Function StringToDateAdvanced(ByRef sString As String, Optional ByRef sSpecial As String = "", Optional ByRef bUSDate As Boolean = False, Optional ByRef bTimeIncluded As Boolean = False) As Date
        '03.03.2010 - This function is created when You don't know the format of the date You got
        'It is created to be used for payEx where the dateformat differs from file to file.
        'The function is by no means complete
        'Year must be 4 positions

        'Theese variants are taken care of.
        'Skogvang! - Please don't mess'em up
        'Tested
        's = "13.02.2010 00:11"
        's = "3.02.2010 00:11"
        's = "13.2.2010 00:11"
        's = "3.2.2010 00:11"

        's = "2009-12-23 00:00:25"
        's = "2009-12-3 00:00:25"
        's = "2009-2-23 00:00:25"
        's = "2009-2-3 00:00:25"

        's = "04/24/2009 0:00"
        's = "4/24/2009 0:00"
        's = "04/4/2009 0:00"
        's = "4/5/2009 0:00"

        Dim sDate As String
        Dim iCounter As Short
        Dim iPosition As Short
        Dim sDelimiter As String
        Dim bYearInfront As Boolean
        Dim bError As Boolean
        Dim sYear As String
        Dim sMonth As String
        Dim sDay As String

        bYearInfront = False
        bError = False
        sDelimiter = ""
        sYear = ""
        sMonth = ""
        sDay = ""

        If bTimeIncluded Then
            'Assume space between date and time
            For iCounter = 1 To Len(sString)
                If Mid(sString, iCounter, 1) = " " Then
                    iPosition = iCounter
                    Exit For
                End If
            Next iCounter
            If iPosition > 0 Then
                'Check if part 1 contains a ":". If so assume that this is the timepart of the date
                'If not the last part is seen as the timepart
                If InStr(1, Mid(sString, 1, iCounter), ":") > 0 Then
                    sDate = Mid(sString, iCounter + 1)
                Else
                    sDate = Left(sString, iCounter - 1)
                End If
            End If
        End If

        'sDate now contains the date (at least I hope so)

        'Try to find which separator that is used
        For iCounter = 1 To Len(sDate)
            Select Case Mid(sDate, iCounter, 1)
                Case "."
                    sDelimiter = "."
                    Exit For

                Case "-"
                    sDelimiter = "-"
                    Exit For

                Case "/"
                    'When / is used, assume it's a yankeedate
                    bUSDate = True
                    sDelimiter = "/"
                    Exit For

            End Select

        Next iCounter

        If iCounter < 4 Then
            bYearInfront = False
        Else
            bYearInfront = True
        End If

        'Check if 2 delimiters
        If Len(sDate) - Len(Replace(sDate, sDelimiter, "", , , Microsoft.VisualBasic.CompareMethod.Text)) = 2 Then
            bError = False
        Else
            bError = True
        End If

        If Not bError Then
            If bYearInfront Then
                If Mid(sDate, 5, 1) = sDelimiter Then
                    sYear = Left(sDate, 4)
                    If Mid(sDate, 7, 1) = sDelimiter Then
                        sMonth = "0" & Mid(sDate, 6, 1)
                    ElseIf Mid(sDate, 8, 1) = sDelimiter Then
                        sMonth = Mid(sDate, 6, 2)
                    Else
                        bError = True
                    End If
                    If Not bError Then
                        If Left(Right(sDate, 3), 1) = sDelimiter Then
                            sDay = Right(sDate, 2)
                        ElseIf Left(Right(sDate, 2), 1) = sDelimiter Then
                            sDay = "0" & Right(sDate, 1)
                        Else
                            bError = True
                        End If
                    End If
                Else
                    bError = True
                End If
            Else
                If Mid(sDate, Len(sDate) - 4, 1) = sDelimiter Then
                    sYear = Right(sDate, 4)
                    If Mid(sDate, 2, 1) = sDelimiter Then
                        If bUSDate Then
                            sMonth = "0" & Left(sDate, 1)
                        Else
                            sDay = "0" & Left(sDate, 1)
                        End If
                        If Mid(sDate, 4, 1) = sDelimiter Then
                            If bUSDate Then
                                sDay = "0" & Mid(sDate, 3, 1)
                            Else
                                sMonth = "0" & Mid(sDate, 3, 1)
                            End If
                        ElseIf Mid(sDate, 5, 1) = sDelimiter Then
                            If bUSDate Then
                                sDay = Mid(sDate, 3, 2)
                            Else
                                sMonth = Mid(sDate, 3, 2)
                            End If
                        Else
                            bError = True
                        End If
                    ElseIf Mid(sDate, 3, 1) = sDelimiter Then
                        If bUSDate Then
                            sMonth = Left(sDate, 2)
                        Else
                            sDay = Left(sDate, 2)
                        End If
                        If Mid(sDate, 5, 1) = sDelimiter Then
                            If bUSDate Then
                                sDay = "0" & Mid(sDate, 4, 1)
                            Else
                                sMonth = "0" & Mid(sDate, 4, 1)
                            End If
                        ElseIf Mid(sDate, 6, 1) = sDelimiter Then
                            If bUSDate Then
                                sDay = Mid(sDate, 4, 2)
                            Else
                                sMonth = Mid(sDate, 4, 2)
                            End If
                        Else
                            bError = True
                        End If
                    Else
                        bError = True
                    End If
                Else
                    bError = True
                End If
            End If

        End If

        If bError Then
            Err.Raise(30023, "StringToDateAdvanced", LRSCommon(30023, sString))
        Else
            StringToDateAdvanced = StringToDate(sYear & sMonth & sDay)
        End If



    End Function
    Public Function StringToDateConversion(ByRef sString As String, ByRef sFormat As String) As Date
        '03.03.2010 - This function is used to convert from a given format to a date

        'Theese variants are taken care of.
        Dim sDate As String
        Dim sYear As String
        Dim iYearStart As Integer
        Dim iYearLength As Integer
        Dim sMonth As String
        Dim iMonthStart As Integer
        Dim iMonthLength As Integer
        Dim sDay As String
        Dim iDayStart As Integer
        Dim iDayLength As Integer
        Dim iCounter As Integer
        Dim s As String
        Dim dReturnValue As Date

        Try

            For iCounter = 1 To Len(sFormat)
                s = Mid(sFormat, iCounter, 1)
                Select Case s

                    Case "Y"
                        If iYearStart = 0 Then
                            iYearStart = iCounter
                        End If
                        iYearLength = iYearLength + 1

                    Case "M"
                        If iMonthStart = 0 Then
                            iMonthStart = iCounter
                        End If
                        iMonthLength = iMonthLength + 1

                    Case "D"
                        If iDayStart = 0 Then
                            iDayStart = iCounter
                        End If
                        iDayLength = iDayLength + 1

                End Select
            Next iCounter

            sYear = Mid(sString, iYearStart, iYearLength)
            sMonth = Mid(sString, iMonthStart, iMonthLength)
            sDay = Mid(sString, iDayStart, iDayLength)

            dReturnValue = StringToDate(sYear & sMonth & sDay)

        Catch ex As Exception
            Throw New Exception(LRSCommon(30051, sString, sFormat) & vbCrLf & ex.Message)

        End Try


        StringToDateConversion = dReturnValue


    End Function
    Public Function StringContainsValidNOShortDate(ByRef sString As String) As Boolean
        ' Check dates on form DDMMYY
        ' Validates dates from 010104 to 311290
        Dim bReturnValue As Boolean

        bReturnValue = True

        If Len(sString) <> 6 Then
            bReturnValue = False
        Else
            If Val(Right(sString, 2)) < 4 And Val(Right(sString, 2)) > 90 Then
                bReturnValue = False
            Else
                If Val(Left(sString, 2)) > 31 Then
                    bReturnValue = False
                Else
                    If Val(Mid(sString, 3, 2)) > 12 Then
                        bReturnValue = False
                    End If
                End If
            End If
        End If

        StringContainsValidNOShortDate = bReturnValue

    End Function

    Public Sub FormvbStyle(ByRef frm As System.Windows.Forms.Form, ByRef sPic As String, Optional ByRef lHeight As Integer = 0, Optional ByVal lStartHeight As Long = 110)
        'Koden er kun testet p� frmStart
        'Skalaene som brukes for � angi hvor p� formen kontrollen skal ligge er endret!
        On Error Resume Next

        Dim ctl As System.Windows.Forms.Control
        Dim sCtlType As String
        Dim sPicture As String
        Dim ctlImage() As System.Windows.Forms.PictureBox
        Dim iImgCnt As Short

        If lHeight = 0 Then
            lHeight = 55 '650
        Else
            lHeight = lHeight * 0.085 'Other scale in .net. This is hust testet for frmStart
        End If

        frm.BackColor = System.Drawing.Color.White   'LightYellow

        For Each ctl In frm.Controls
            'UPGRADE_WARNING: TypeName has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            sCtlType = TypeName(ctl)
            If sCtlType = "Label" Then
                If ctl.Name.IndexOf("lblLine") = -1 Then   ' 21.07.2016
                    ' do not change color for the Orange labels we use in the forms as "design" separators
                    ctl.BackColor = System.Drawing.Color.White
                End If
                'UPGRADE_ISSUE: Control method ctl.BackStyle was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                'Change done - use the default
                'ctl.BackStyle = 0
            ElseIf sCtlType = "CheckBox" Then
                ctl.BackColor = System.Drawing.Color.White
                'UPGRADE_ISSUE: Control method ctl.BackStyle was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                'Change done - use the default
                'ctl.BackStyle = 0
            ElseIf sCtlType = "TextBox" Then
                ' Exceptions;
                If ctl.Name = "txtlblInvoiceSeek" Then
                    ' do nothing
                ElseIf ctl.Name = "txtLblWhoLocked" Then
                    ' do nothing
                ElseIf ctl.Name = "txtInvoiceSeek" Then
                    ' do nothing
                Else
                    ctl.BackColor = System.Drawing.Color.White
                End If

            ElseIf sCtlType = "OptionButton" Then
                ctl.BackColor = System.Drawing.Color.White
            ElseIf sCtlType = "CommandButton" Then
                'The following setting creats an error, Commented 29/8 by Kjell
                'ctl.Style = 1 ' Graphical
                'Must be set to Style=Graphical when form is created
                ctl.BackColor = System.Drawing.Color.White
            ElseIf sCtlType = "Menu" Then
                ctl.BackColor = System.Drawing.Color.White
            ElseIf sCtlType = "Toolbar" Then
                ctl.BackColor = System.Drawing.Color.White
            ElseIf sCtlType = "ListView" Then
                ctl.BackColor = System.Drawing.Color.White
            ElseIf sCtlType = "Frame" Then
                If ctl.Name = "frameLabels" Then
                    ctl.BackColor = System.Drawing.ColorTranslator.FromOle(RGB(255, 240, 180))
                Else
                    ctl.BackColor = System.Drawing.Color.White
                End If
                'ElseIf sCtlType = "TabStrip" Then
                'ctl.Parent.BackColor = vbWhite

            End If
        Next ctl

        If Len(sPic) > 0 Then
            ' Display "productimage" only when a filename is specified
            ' In some forms we do not want images
            iImgCnt = iImgCnt + 1
            ' Add both a "product-image", to the left, and a "vb net-a-like image on top:
            ReDim Preserve ctlImage(iImgCnt)

            ' Add image to left
            ctlImage(iImgCnt) = New System.Windows.Forms.PictureBox
            ctlImage(iImgCnt).Name = "ctlImage" & CStr(iImgCnt)
            'ctlImage(iImgCnt).SetBounds(15, 110, 200, lHeight + 85) '1100
            ' 28.12.2010 must add a StartHeight to fit all forms
            ctlImage(iImgCnt).SetBounds(15, lStartHeight, 200, lHeight + 85) '1100
            If RunTime() Then   'DISKUTERNOTRUNTIME flytte til felles mappe?
                sPicture = My.Application.Info.DirectoryPath & "\" & sPic
            Else
                sPicture = "C:\Projects\Babelbank\Graphics\" & sPic
            End If
            ctlImage(iImgCnt).Image = System.Drawing.Image.FromFile(sPicture)
            ctlImage(iImgCnt).Visible = True
            'The control must be created by using NEW before it is added
            frm.Controls.Add(ctlImage(iImgCnt))

        End If

        ' "vb.net" look-a-like
        iImgCnt = iImgCnt + 1
        ReDim Preserve ctlImage(iImgCnt)
        ' Add image on top of picture;
        ctlImage(iImgCnt) = New System.Windows.Forms.PictureBox
        ctlImage(iImgCnt).SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        ' 23.07.2015 changed from 1200 to 1500 - can cause errors, f.ex. in resizable forms ????
        'ctlImage(iImgCnt).SetBounds(0, 0, 1200, lHeight)
        ctlImage(iImgCnt).SetBounds(0, 0, 1500, lHeight)
        If RunTime() Then  'DISKUTERNOTRUNTIME flytte til felles mappe?
            sPicture = My.Application.Info.DirectoryPath & "\background.jpg"
        Else
            sPicture = "C:\Projects\Babelbank\Graphics\background.jpg"
        End If
        ' Load from file if file Background.jpg is present.
        ' We can then have different looks based on content of Background.jpg
        ctlImage(iImgCnt).Image = System.Drawing.Image.FromFile(sPicture)
        ctlImage(iImgCnt).Visible = True
        ctlImage(iImgCnt).SendToBack() ' Bring to back
        '.NET
        'The control must be created by using NEW before it is added
        frm.Controls.Add(ctlImage(iImgCnt))

    End Sub
    
    Public Sub FormLRSCaptions(ByRef frm As System.Windows.Forms.Form)
        ' Automatic fill in of languagestrings in control caption
        ' if caption or text starts with 5 numerics, then Convert to LRS(.....)
        On Error Resume Next

        Dim ctl As System.Windows.Forms.Control
        Dim ctlToolbar As ToolBar
        Dim ctlToolStrip As System.Windows.Forms.ToolStrip
        Dim ctlMenuStrip As System.Windows.Forms.MenuStrip
        Dim ctlMenuStripItem As System.Windows.Forms.ToolStripMenuItem
        Dim ctlMenuDropDownStripItem As System.Windows.Forms.ToolStripMenuItem
        Dim ctlMenuDropDownItem As Object
        Dim iCounter As Integer
        Dim obj As Object
        Dim sCtlType As String
        Dim subctl As System.Windows.Forms.Control

        ' Captions must be put as 60188 - Text to display
        'set the form's caption
        If vbIsNumeric(Left(frm.Text, 5)) And Left(frm.Text, 5) <> "00000" Then
            frm.Text = LRS(CInt(Left(frm.Text, 5)))
        End If

        'set the controls' captions using the caption
        'property for menu items and the Tag property
        'for all other controls
        'TODO for Panel (Frame) we must test each label under a Panel?
        For Each ctl In frm.Controls 'frm.Controls

            ' 20.01.2011 - problems with f.ex. listboxes with value 1 or 11, etc
            ' thus added next if;
            If vbIsNumeric(Left(ctl.Text, 5)) And Val(Left(ctl.Text, 5)) < 10000 Then
                ' just leave as is
                ctl.Text = ctl.Text
            Else

                'Set ctl.Font = fnt
                'UPGRADE_WARNING: TypeName has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
                sCtlType = TypeName(ctl)

                If sCtlType = "Label" Then
                    If vbIsNumeric(Left(ctl.Text, 5)) And Left(ctl.Text, 5) <> "00000" Then
                        ctl.Text = LRS(CInt(Left(ctl.Text, 5)))
                    ElseIf Left(ctl.Text, 5) = "00000" Then  ' added 18.01.2011 to easier see missing LRS-es
                        ctl.Text = "HUSK LRS!" & Mid(ctl.Text, 6)
                    End If
                ElseIf sCtlType = "Panel" Or sCtlType = "TableLayoutPanel" Then
                    If vbIsNumeric(Left(ctl.Text, 5)) And Left(ctl.Text, 5) <> "00000" Then
                        ctl.Text = LRS(CInt(Left(ctl.Text, 5)))
                    End If
                    If ctl.Name = "frame_Seek" Then
                        iCounter = iCounter
                    End If
                    ControlsLRSCaptions(ctl)

                ElseIf sCtlType = "GroupBox" Or sCtlType = "ComboBox" Then
                    If vbIsNumeric(Left(ctl.Text, 5)) And Left(frm.Text, 5) <> "00000" Then
                        ctl.Text = LRS(CInt(Left(ctl.Text, 5)))
                    End If
                    ControlsLRSCaptions(ctl)
                ElseIf sCtlType = "Button" And Left(ctl.Text, 5) <> "00000" Then
                    If vbIsNumeric(Left(ctl.Text, 5)) Then
                        ctl.Text = LRS(CInt(Left(ctl.Text, 5)))
                    ElseIf Left(ctl.Text, 5) = "00000" Then  ' added 18.01.2011 to easier see missing LRS-es
                        ctl.Text = "HUSK LRS!" & Mid(ctl.Text, 6)
                    End If
                ElseIf sCtlType = "CheckBox" And Left(ctl.Text, 5) <> "00000" Then
                    If vbIsNumeric(Left(ctl.Text, 5)) Then
                        ctl.Text = LRS(CInt(Left(ctl.Text, 5)))
                    ElseIf Left(ctl.Text, 5) = "00000" Then  ' added 18.01.2011 to easier see missing LRS-es
                        ctl.Text = "HUSK LRS!" & Mid(ctl.Text, 6)
                    End If
                ElseIf (sCtlType = "OptionButton" Or sCtlType = "RadioButton") And Left(ctl.Text, 5) <> "00000" Then
                    If vbIsNumeric(Left(ctl.Text, 5)) Then
                        ctl.Text = LRS(CInt(Left(ctl.Text, 5)))
                    ElseIf Left(ctl.Text, 5) = "00000" Then  ' added 18.01.2011 to easier see missing LRS-es
                        ctl.Text = "HUSK LRS!" & Mid(ctl.Text, 6)
                    End If
                ElseIf sCtlType = "TextBox" Then
                    ' Extra test here, due to potential problems with numeric textboxes
                    If vbIsNumeric(Left(ctl.Text, 5)) And (Mid(ctl.Text, 6, 3) = " - " Or Mid(ctl.Text, 6, 1) = "-") Then
                        ctl.Text = LRS(CInt(Left(ctl.Text, 5)))
                    End If

                ElseIf sCtlType = "Menu" And Left(ctl.Text, 5) <> "00000" Then
                    If vbIsNumeric(Left(ctl.Text, 5)) Then
                        ctl.Text = LRS(CInt(Left(ctl.Text, 5)))
                    End If
                ElseIf sCtlType = "MenuStrip" And Left(ctl.Text, 5) <> "00000" Then
                    ctlMenuStrip = ctl
                    For iCounter = 0 To ctlMenuStrip.Items.Count - 1
                        ctlMenuStripItem = ctlMenuStrip.Items(iCounter)
                        If vbIsNumeric(Left(ctlMenuStripItem.Text, 5)) Then
                            ctlMenuStripItem.Text = LRS(Left(ctlMenuStripItem.Text, 5))
                        End If
                        For Each ctlMenuDropDownItem In ctlMenuStripItem.DropDownItems
                            If TypeName(ctlMenuDropDownItem) = "ToolStripMenuItem" Then
                                ctlMenuDropDownStripItem = ctlMenuDropDownItem
                                If vbIsNumeric(Left(ctlMenuDropDownStripItem.Text, 5)) Then
                                    ctlMenuDropDownStripItem.Text = LRS(Left(ctlMenuDropDownStripItem.Text, 5))
                                End If
                            End If
                        Next ctlMenuDropDownItem
                    Next iCounter
                ElseIf sCtlType = "AxTabStrip" Then

                    For Each obj In CObj(ctl).Tabs
                        If Not obj Is Nothing Then
                            obj.Caption = LRS(CInt(Left(obj.Caption, 5)))
                            If vbIsNumeric(Left(obj.ToolTipText, 5)) Then
                                obj.ToolTipText = LRS(CInt(Left(obj.ToolTipText, 5)))
                            End If
                        End If
                    Next obj

                    ElseIf sCtlType = "ToolStrip" Or sCtlType = "TabStrip" Then
                        ctlToolStrip = ctl
                        'Change done
                        For Each obj In ctlToolStrip.Items
                            If vbIsNumeric(Left(obj.ToolTipText, 5)) Then
                                obj.ToolTipText = LRS(CInt(Left(obj.ToolTipText, 5)))
                            End If
                        Next obj
                        ControlsLRSCaptions(ctl)
                        'For Each subctl In ctl.Controls
                        ' ControlsLRSCaptions(subctl)
                        ' Next

                    ElseIf sCtlType = "Toolbar" Then
                        ctlToolbar = ctl
                        'Change done
                        For Each obj In ctlToolbar.Buttons
                            If vbIsNumeric(Left(obj.ToolTipText, 5)) Then
                                obj.ToolTipText = LRS(CInt(Left(obj.ToolTipText, 5)))
                            End If
                        Next obj
                        ControlsLRSCaptions(ctl)

                ElseIf sCtlType = "Frame" And Left(ctl.Text, 5) <> "00000" Then
                    If vbIsNumeric(Left(ctl.Text, 5)) Then
                        ctl.Text = LRS(CInt(Left(ctl.Text, 5)))
                    End If
                    ControlsLRSCaptions(ctl)
                    'For Each subctl In ctl.Controls
                    ' ControlsLRSCaptions(subctl)
                    ' Next

                    Else
                        iCounter = iCounter
                        'nVal = 0
                        '            nVal = Val(ctl.Tag)
                        '            If nVal > 0 Then ctl.Caption = LRS(nVal)
                        '            nVal = 0
                        '            nVal = Val(ctl.ToolTipText)
                        '            If nVal > 0 Then ctl.ToolTipText = LRS(nVal)
                    End If
            End If
        Next ctl

        Exit Sub

    End Sub
    Public Sub ControlsLRSCaptions(ByRef MainCtl As System.Windows.Forms.Control)
        ' In vb.net, controls like Panel and Controlbox may have subcontrols,
        ' which we have to translate with LRS, which is done here ..

        ' Automatic fill in of languagestrings in control caption
        ' if caption or text starts with 5 numerics, then Convert to LRS(.....)
        On Error Resume Next

        Dim ctl As Control
        Dim iCounter As Integer
        Dim sCtlType As String
        Dim subctl As System.Windows.Forms.Control

        'set the controls' captions using the caption
        'property for menu items and the Tag property
        'for all other controls
        For Each ctl In MainCtl.Controls 'frm.Controls
            sCtlType = TypeName(ctl)

            If sCtlType = "Label" Or sCtlType = "RichTextBox" Or sCtlType = "ComboBox" Or sCtlType = "ListBox" Or sCtlType = "PictureBox" Or _
                sCtlType = "RadioButton" Or sCtlType = "CommandButton" Or sCtlType = "Button" Or sCtlType = "CheckBox" Or _
                sCtlType = "OptionButton" Then
                If vbIsNumeric(Left(ctl.Text, 5)) And (Mid(ctl.Text, 6, 3) = " - " Or Mid(ctl.Text, 6, 1) = "-" Or Mid(ctl.Text, 6, 1) = " ") Then
                    ctl.Text = LRS(CInt(Left(ctl.Text, 5)))
                End If
            ElseIf sCtlType = "GroupBox" Or sCtlType = "Panel" Or sCtlType = "Frame" Or sCtlType = "Menu" Or sCtlType = "MenuStrip" Or _
            sCtlType = "ToolStripMenuItem" Or sCtlType = "Toolstrip" Or sCtlType = "AxTabStrip" Or sCtlType = "TabStrip" Then
                If vbIsNumeric(Left(ctl.Text, 5)) And (Mid(ctl.Text, 6, 3) = " - " Or Mid(ctl.Text, 6, 1) = "-" Or Mid(ctl.Text, 6, 1) = "-") Then
                    ctl.Text = LRS(CInt(Left(ctl.Text, 5)))
                End If
                ControlsLRSCaptions(ctl)
            ElseIf sCtlType = "TextBox" Then
                ' Extra test here, due to potential problems with numeric textboxes
                If vbIsNumeric(Left(ctl.Text, 5)) And (Mid(ctl.Text, 6, 3) = " - " Or Mid(ctl.Text, 6, 1) = "-" Or Mid(ctl.Text, 6, 1) = "-") Then
                    ctl.Text = LRS(CInt(Left(ctl.Text, 5)))
                End If

            Else
                ctl.Text = ctl.Text

            End If
        Next ctl
        Exit Sub

    End Sub

    Public Sub FormSelectAllTextboxs(ByRef frm As System.Windows.Forms.Form)
        ' Automatic set Selstart=0 and Sellength = len(control.text)
        On Error Resume Next

        Dim ctl As System.Windows.Forms.Control
        Dim obj As Object
        Dim sCtlType As String
        Dim nVal As Short
        'Change done
        Dim ctlTextBox As System.Windows.Forms.TextBox

        For Each ctl In frm.Controls 'frm.Controls
            'Set ctl.Font = fnt
            'UPGRADE_WARNING: TypeName has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            sCtlType = TypeName(ctl)
            If sCtlType = "TextBox" Then

                ' ChangeDone
                ctlTextBox = ctl
                ctlTextBox.SelectionStart = 0
                ctlTextBox.SelectionLength = Len(Trim$(ctlTextBox.Text))

            End If
        Next ctl


    End Sub
    Public Sub SelectAllText()
        Dim ctlTextBox As System.Windows.Forms.TextBox

        If TypeName(VB6.GetActiveControl) = "TextBox" Then
            ' call from control.gotfocus, to highlight current text
            ctlTextBox = VB6.GetActiveControl()

            ctlTextBox.SelectionStart = 0
            ctlTextBox.SelectionLength = Len(Trim$(ctlTextBox.Text))

        End If
    End Sub

    '    Public Function ConnectToERPDB(ByVal oProfile As vbBabel.Profile, ByVal bTest As Boolean, ByRef sErrorString As String) As ADODB.Connection
    'Public Function ConnectToERPDB(ByRef sConnectionString As String, ByRef sConUID As String, ByRef sConPWD As String, ByRef bTest As Boolean, ByRef sErrorString As String, ByRef bUseLRS As Boolean) As Integer
    'Public Function ConnectToERPDB(ByRef sConnectionString As String, ByRef sConUID As String, ByRef sConPWD As String, ByRef bTest As Boolean, ByRef sErrorString As String, ByRef bUseLRS As Boolean) As ADODB.Connection
    '        Dim conERPDB As New ADODB.Connection
    '        'Dim sConnectionString As String
    '        Dim sErrDescription As String
    '        Dim bErrorDuringConnection As Boolean
    '        'New 07.08.2007 - To not show the password in the connectionstring. Use this new variable in the errormessage
    '        Dim sOrigConnectionString As String

    '        On Error GoTo ERRHAndler

    '        bErrorDuringConnection = False
    '        ' Old sConnectionString = Trim$(oProfile.ConString)
    '        ' Old sConnectionString = Replace(sConnectionString, "=UID", "=" & Trim$(oProfile.ConUID))

    '        sOrigConnectionString = sConnectionString

    '        sConnectionString = Trim(Replace(sConnectionString, "=UID", "=" & Trim(sConUID)))
    '        If Not bTest Then
    '            ' Old sConnectionString = Replace(sConnectionString, "=PWD", "=" & pwDeCrypt(Trim$(oProfile.ConPWD)))
    '            ' 24.02.2003 Deleted PWDecrypt
    '            sConnectionString = Replace(sConnectionString, "=PWD", "=" & Trim(sConPWD))
    '        Else
    '            ' Old sConnectionString = Replace(sConnectionString, "=PWD", "=" & Trim$(oProfile.ConPWD))
    '            sConnectionString = Replace(sConnectionString, "=PWD", "=" & Trim(sConPWD))
    '        End If

    '        bErrorDuringConnection = True
    '        'sConnectionString = "Provider=MSDASQL.1;Extended Properties='DSN=NC_Test;CSF=No;Database=C:\Nor-Cargo\NC_test_babel.fdb;PPath=C:\Programfiler\Navision Attain\Client;CS=256;OPT=Text;IT=a-z,A-Z,0-9,_;QTYesNo=Yes;RO=No;CC=Yes;BE=Yes;CD=No;ML=1044;RD=No;UID=babel;PWD=bank;CN=Nor-Cargo AS'"
    '        conERPDB.Open(sConnectionString)

    '        sErrorString = ""
    '        ConnectToERPDB = conERPDB

    '        Exit Function

    'ERRHAndler:

    '        If Not conERPDB Is Nothing Then
    '            'UPGRADE_NOTE: Object conERPDB may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            conERPDB = Nothing
    '        End If

    '        '25001: Couldn't connect to external database.
    '        If bUseLRS Then

    '            ' ISINGRUD - Denne b�r vel ligge i lrsCommon???
    '            sErrDescription = LRS(25001)

    '            If bErrorDuringConnection Then
    '                'The connection reported the following error:
    '                sErrDescription = sErrDescription & vbCrLf & LRS(25002) & vbCrLf & Err.Description
    '            Else
    '                'Error during the building of the connectionstring.
    '                sErrDescription = sErrDescription & vbCrLf & LRS(25003)
    '            End If

    '            If bTest Then
    '                sErrorString = sErrDescription
    '            Else
    '                '25004: Connectionstring:
    '                '25005: To test/change the connectionstring, start BabelBank and choose Company under Setup in the menu.
    '                sErrDescription = sErrDescription & vbCrLf & LRS(25004) & sOrigConnectionString & vbCrLf & LRS(25005)

    '                Err.Raise(19502, , sErrDescription)
    '            End If
    '        Else
    '            sErrDescription = "25001  - Greier ikke � logge p� ekstern database"
    '            If bErrorDuringConnection Then
    '                'The connection reported the following error:
    '                sErrDescription = sErrDescription & vbCrLf & "P�logging rapporterte f�lgende feil:" & vbCrLf & Err.Description
    '            Else
    '                'Error during the building of the connectionstring.
    '                sErrDescription = sErrDescription & vbCrLf & vbCrLf & "Feil under oppbygging av p�loggingsstreng"
    '            End If

    '            If bTest Then
    '                sErrorString = sErrDescription
    '            Else
    '                '25004: Connectionstring:
    '                '25005: To test/change the connectionstring, start BabelBank and choose Company under Setup in the menu.
    '                sErrDescription = sErrDescription & vbCrLf & vbCrLf & "P�loggingsstreng " & vbCrLf & sOrigConnectionString & vbCrLf & vbCrLf & "Endre p�loggingsstreng i oppsett."

    '                Err.Raise(19502, , sErrDescription)
    '            End If

    '        End If

    '    End Function

    'Public Function ConnectToERPDB(oProfile As vbbabel.Profile, bTest As Boolean, ByRef sErrorString As String) As ADODB.Connection
    Public Function ConnectToERPDBNew(ByRef sProviderName As String, ByRef sConnectionString As String, ByRef sConUID As String, ByRef sConPWD As String, ByRef bTest As Boolean, ByRef sErrorString As String, ByRef bUseLRS As Boolean) As System.Data.Common.DbConnection 'DING
        'Assume failure
        Dim conERPDB As System.Data.Common.DbConnection = Nothing
        Dim sErrDescription As String
        Dim bErrorDuringConnection As Boolean
        'New 07.08.2007 - To not show the password in the connectionstring. Use this new variable in the errormessage
        Dim sOrigConnectionString As String

        ' Create the DbProviderFactory and DbConnection.
        If Not sConnectionString Is Nothing Then
            Try
                bErrorDuringConnection = False
                sOrigConnectionString = sConnectionString
                If Not bTest Then
                    sConnectionString = Replace(sConnectionString, "=PWD", "=" & Trim(sConPWD))
                Else
                    sConnectionString = Replace(sConnectionString, "=PWD", "=" & Trim(sConPWD))
                End If

                Dim factory As System.Data.Common.DbProviderFactory = System.Data.Common.DbProviderFactories.GetFactory(sProviderName)
                'System.Data.Odbc
                'System.Data.OleDb
                'System.Data.OracleClient
                'System.Data.SqlClient
                'System.Data.SqlServerCe.3.5


                conERPDB = factory.CreateConnection()
                conERPDB.ConnectionString = sConnectionString
                bErrorDuringConnection = True
                conERPDB.Open()

            Catch ex As Exception
                ' Set the connection to Nothing if it was created.
                If Not conERPDB Is Nothing Then
                    conERPDB = Nothing
                End If

                '25001: Couldn't connect to external database.
                If bUseLRS Then

                    ' ISINGRUD - Denne b�r vel ligge i lrsCommon???
                    sErrDescription = LRSCommon(25001)

                    If bErrorDuringConnection Then
                        'The connection reported the following error:
                        sErrDescription = sErrDescription & vbCrLf & LRSCommon(25002) & vbCrLf & Err.Description
                    Else
                        'Error during the building of the connectionstring.
                        sErrDescription = sErrDescription & vbCrLf & LRSCommon(25003)
                    End If

                    If bTest Then
                        sErrorString = sErrDescription
                    Else
                        '25004: Connectionstring:
                        '25005: To test/change the connectionstring, start BabelBank and choose Company under Setup in the menu.
                        sErrDescription = sErrDescription & vbCrLf & LRSCommon(25004) & sOrigConnectionString & vbCrLf & LRSCommon(25005)

                        Err.Raise(19502, , sErrDescription)
                    End If
                Else
                    sErrDescription = "25001  - Greier ikke � logge p� ekstern database"
                    If bErrorDuringConnection Then
                        'The connection reported the following error:
                        sErrDescription = sErrDescription & vbCrLf & "P�logging rapporterte f�lgende feil:" & vbCrLf & Err.Description
                    Else
                        'Error during the building of the connectionstring.
                        sErrDescription = sErrDescription & vbCrLf & vbCrLf & "Feil under oppbygging av p�loggingsstreng"
                    End If

                    If bTest Then
                        sErrorString = sErrDescription
                    Else
                        '25004: Connectionstring:
                        '25005: To test/change the connectionstring, start BabelBank and choose Company under Setup in the menu.
                        sErrDescription = sErrDescription & vbCrLf & vbCrLf & "P�loggingsstreng " & vbCrLf & sOrigConnectionString & vbCrLf & vbCrLf & "Endre p�loggingsstreng i oppsett."

                        Err.Raise(19502, , sErrDescription)
                    End If

                End If

            End Try
        End If

        sErrorString = ""
        ' Return the connection.
        Return conERPDB

    End Function

    Public Function pwCrypt(ByRef pw As String) As String
        ' Simple crypting of password:
        Dim i As Short
        Dim sRetPW As String

        sRetPW = ""

        For i = 1 To Len(pw)
            Select Case Asc(Mid(pw, i, 1))
                Case Is < 81
                    ' deduct 10
                    sRetPW = sRetPW & Chr(Asc(Mid(pw, i, 1)) - 10)
                Case Is < 91
                    ' deduct 8
                    sRetPW = sRetPW & Chr(Asc(Mid(pw, i, 1)) - 8)
                Case Is < 101
                    ' deduct 5
                    sRetPW = sRetPW & Chr(Asc(Mid(pw, i, 1)) - 5)
                Case Is < 121
                    ' deduct 3
                    sRetPW = sRetPW & Chr(Asc(Mid(pw, i, 1)) - 3)
                Case Else
                    sRetPW = sRetPW & Chr(Asc(Mid(pw, i, 1)))
            End Select
        Next i
        pwCrypt = sRetPW

    End Function
    Public Function pwDeCrypt(ByRef pw As String) As String
        ' Simple DEcrypting of password:
        Dim i As Short
        Dim sRetPW As String

        sRetPW = ""

        For i = 1 To Len(pw)
            Select Case Asc(Mid(pw, i, 1))
                Case Is < 71
                    ' add 10
                    sRetPW = sRetPW & Chr(Asc(Mid(pw, i, 1)) + 10)
                Case Is < 83
                    ' add 8
                    sRetPW = sRetPW & Chr(Asc(Mid(pw, i, 1)) + 8)
                Case Is < 96
                    ' add 5
                    sRetPW = sRetPW & Chr(Asc(Mid(pw, i, 1)) + 5)
                Case Is < 118
                    ' add 3
                    sRetPW = sRetPW & Chr(Asc(Mid(pw, i, 1)) + 3)
                Case Else
                    sRetPW = sRetPW & Chr(Asc(Mid(pw, i, 1)))
            End Select
        Next i
        pwDeCrypt = sRetPW

    End Function

    Public Function EasyEncrypt(ByVal sDecryptedPassword As String) As String
        Const Number As Short = 7
        Dim sPassword As String
        Dim iTemp, iCounter As Short

        On Error GoTo ErrTrap

        sPassword = ""
        iCounter = 1

        Do Until iCounter = Len(sDecryptedPassword) + 1
            iTemp = Asc(Mid(sDecryptedPassword, iCounter, 1)) Xor (10 - Number)
            If iCounter Mod 2 = 0 Then ' See if even
                iTemp = iTemp + Number
            Else
                iTemp = iTemp - Number
            End If
            sPassword = sPassword & Chr(iTemp)
            iCounter = iCounter + 1
        Loop

        sPassword = pwCrypt(sPassword)

        EasyEncrypt = sPassword

        Exit Function

ErrTrap:

        Err.Raise(CInt("10000"), , "Error in the encrypting-routine")

    End Function


    Public Function EasyDecrypt(ByVal sEncryptedPassword As String) As String
        Const Number As Short = 7
        Dim sPassword As String
        Dim iTemp, iCounter As Short

        On Error GoTo ErrTrap

        sPassword = ""
        iCounter = 1

        sEncryptedPassword = pwDeCrypt(sEncryptedPassword)

        Do Until iCounter = Len(sEncryptedPassword) + 1
            iTemp = Asc(Mid(sEncryptedPassword, iCounter, 1))
            If iCounter Mod 2 = 0 Then ' See if even
                iTemp = iTemp - Number
            Else
                iTemp = iTemp + Number
            End If
            iTemp = iTemp Xor (10 - Number)
            sPassword = sPassword & Chr(iTemp)
            iCounter = iCounter + 1
        Loop

        EasyDecrypt = sPassword

        Exit Function

ErrTrap:

        Err.Raise(CInt("10000"), , "Error in the decrypting-routine")

    End Function

    Public Function BBEncrypt(ByRef sString As String, ByRef iEncryptionKey As Short) As String
        Dim sEncryptedString As String
        Dim lCounter As Integer
        Dim sNewCharacter As String
        Dim sWorkingString As String 'work string
        Dim sTemp As String
        Dim lCorrect1 As Integer
        Dim lCorrect2 As Integer



        'BBEncrypt = sString
        'Exit Function


        sEncryptedString = ""
        sWorkingString = Trim(sString)

        'sWorkingString = Replace(sworkingsstring, Chr(10), "")

        For lCounter = 1 To Len(sWorkingString)
            sTemp = Trim(Str(System.Math.Round(lCounter / 7, 4)))
            If Len(sTemp) = 1 Then
                sTemp = "25"
            End If
            lCorrect1 = Val(Mid(sTemp, Len(sTemp) - 1, 1))

            sTemp = Trim(Str(System.Math.Round(lCounter - iEncryptionKey / 7, 4)))
            If Len(sTemp) = 1 Then
                sTemp = "22"
            End If
            lCorrect2 = Val(Mid(sTemp, Len(sTemp) - 1, 1))

            sNewCharacter = Chr(Asc(Mid(sWorkingString, lCounter, 1)) - 4 - lCorrect1 + lCorrect2)
            'sNewCharacter = Chr(Asc(Mid(sWorkingString, lCounter, 1)) + 40 + lCounter + iEncryptionKey)
            sEncryptedString = sEncryptedString & sNewCharacter
        Next lCounter

        'e = EasyEncrypt(e)

        ' 11.05.2017
        ' we have problems when ' is part of crypted string.
        ' replace ' with QQQQQ
        ' test f�rst for not runtime
        sEncryptedString = Replace(sEncryptedString, "'", "QQQQQ")

        BBEncrypt = sEncryptedString

    End Function

    Public Function BBDecrypt(ByRef sString As String, ByRef iEncryptionKey As Short) As String
        Dim sDecryptedString As String
        Dim lCounter As Integer
        Dim sNewCharacter As String
        Dim sWorkingString As String
        Dim sTemp As String
        Dim lCorrect1 As Integer
        Dim lCorrect2 As Integer

        's = EasyDecrypt(s)

        'BBDecrypt = sString
        'Exit Function

        ' 11.05.2017
        ' we have problems when ' is part of crypted string.
        ' replace ' with QQQQQ
        ' test f�rst for not runtime
        sString = Replace(sString, "QQQQQ", "'")

        sDecryptedString = ""
        'sWorkingString = Trim(sString)
        sWorkingString = sString
        If Len(sWorkingString) > 0 Then
            For lCounter = 1 To Len(sWorkingString)
                sTemp = Trim(Str(System.Math.Round(lCounter / 7, 4)))
                If Len(sTemp) = 1 Then
                    sTemp = "25"
                End If
                lCorrect1 = Val(Mid(sTemp, Len(sTemp) - 1, 1))

                sTemp = Trim(Str(System.Math.Round(lCounter - iEncryptionKey / 7, 4)))
                If Len(sTemp) = 1 Then
                    sTemp = "22"
                End If
                lCorrect2 = Val(Mid(sTemp, Len(sTemp) - 1, 1))

                sNewCharacter = Chr(Asc(Mid(sWorkingString, lCounter, 1)) + 4 + lCorrect1 - lCorrect2)
                'sNewCharacter = Chr(Asc(Mid(sWorkingString, lCounter, 1)) - 40 - lCounter - iEncryptionKey)
                sDecryptedString = sDecryptedString & sNewCharacter
            Next lCounter
        End If

        BBDecrypt = sDecryptedString

    End Function

    Public Function CheckAndDecrypt(sLoginPassword As String) As String
        Dim sReturnValue As String = ""

        sReturnValue = sLoginPassword
        If Not EmptyString(sReturnValue) Then
            If sReturnValue.Substring(0, 1) = "j" Then
                If sReturnValue.Substring(sReturnValue.Length - 1, 1) = "B" Then
                    sReturnValue = BBDecrypt(sReturnValue.Substring(1, sReturnValue.Length - 2), 49)
                End If
            End If
        End If

        Return sReturnValue

    End Function

    '--------------------------------------
    Function CalculateEasterDay(ByRef iYear As Short) As Date
        '--------------------------------------
        ' modDato
        ' Udregner p�skedag for et givet �rstal
        ' Beregningsmetode ifl. Gauss
        ' Rettet 19-03-2001
        '--------------------------------------
        'Skrevet af Stephen Biering-S�rensen, DTI Center for IT
        '--------------------------------------
        Dim a As Short
        Dim b As Short
        Dim c As Short
        Dim d As Short
        Dim e As Short
        Dim k As Short
        Dim p As Short
        Dim q As Short
        Dim m As Short
        Dim n As Short
        Dim intDay As Short
        Dim intMonth As Short

        k = iYear \ 100
        p = (13 + 8 * k) \ 25
        q = k \ 4
        m = (15 - p + k - q) Mod 30
        n = (4 + k - q) Mod 7
        a = iYear Mod 19
        b = iYear Mod 4
        c = iYear Mod 7
        d = (19 * a + m) Mod 30
        e = (2 * b + 4 * c + 6 * d + n) Mod 7

        If d + e <= 9 Then
            intDay = 22 + d + e
            intMonth = 3
        ElseIf (d = 29) And (e = 6) Then
            intDay = 19
            intMonth = 4
        ElseIf (d = 28) And (e = 6) And (a > 10) Then
            intDay = 18
            intMonth = 4
        Else
            intDay = d + e - 9
            intMonth = 4
        End If
        CalculateEasterDay = DateSerial(iYear, intMonth, intDay)
    End Function

    '--------------------------------------
    Function IsHoliday(ByRef dDate As Date, ByRef sISOCountry As String, ByRef bWeekendsAsWell As Boolean) As Boolean
        '--------------------------------------
        ' Returns TRUE if the date is a holiday
        ' If bWeekendsAsWell = true, it will also return true on all Sundays and Saturdays
        ' sCountry needs the ISO-standard
        '--------------------------------------

        Dim iYear As Short
        Dim dEasterDay As Date
        Dim iDifference As Short
        Dim iDay As Short

        IsHoliday = False

        If bWeekendsAsWell Then
            iDay = Weekday(dDate, FirstDayOfWeek.Monday)
            If iDay > 5 Then '= "Saturday" Or sDay = "Sunday" Then
                IsHoliday = True
                Exit Function
            End If
        End If

        iYear = Year(dDate)
        dEasterDay = CalculateEasterDay(iYear)
        'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
        iDifference = DateDiff(Microsoft.VisualBasic.DateInterval.Day, dEasterDay, dDate, FirstDayOfWeek.System, FirstWeekOfYear.System)

        Select Case UCase(sISOCountry)
            Case "NO"
                Select Case iDifference
                    Case -3 : IsHoliday = True 'Sk�rtorsdag
                    Case -2 : IsHoliday = True 'Langfredag
                    Case 0 : IsHoliday = True 'P�skedag *Udgangspunkt
                    Case 1 : IsHoliday = True '2 P�skedag
                    Case 39 : IsHoliday = True 'Kristi himmelfart
                    Case 49 : IsHoliday = True 'Pinsedag
                    Case 50 : IsHoliday = True '2 Pinsedag
                    Case Else
                        If VB.Month(dDate) = 1 And VB.Day(dDate) = 1 Then
                            IsHoliday = True ' New Years day
                        ElseIf VB.Month(dDate) = 5 And VB.Day(dDate) = 1 Then
                            IsHoliday = True ' 1st of may
                        ElseIf VB.Month(dDate) = 5 And VB.Day(dDate) = 17 Then
                            IsHoliday = True ' 17th of may
                        ElseIf VB.Month(dDate) = 12 And VB.Day(dDate) = 25 Then
                            IsHoliday = True ' Christemas day
                        ElseIf VB.Month(dDate) = 12 And VB.Day(dDate) = 26 Then
                            IsHoliday = True ' 2. Christemas day
                        End If
                End Select

            Case "DK"
                Select Case iDifference
                    Case -3 : IsHoliday = True 'Sk�rtorsdag
                    Case -2 : IsHoliday = True 'Langfredag
                    Case 0 : IsHoliday = True 'P�skedag *Udgangspunkt
                    Case 1 : IsHoliday = True '2 P�skedag
                    Case 26 : IsHoliday = True 'Bededag
                    Case 39 : IsHoliday = True 'Kristi himmelfart
                    Case 49 : IsHoliday = True 'Pinsedag
                    Case 50 : IsHoliday = True '2 Pinsedag
                    Case Else
                        If VB.Month(dDate) = 1 And VB.Day(dDate) = 1 Then
                            IsHoliday = True ' New Year day
                            'ElseIf Month(dDate) = 5 And Day(dDate) = 1 Then
                            'IsHoliday = True '  1 maj
                            '28.04.06 Etter samtale ned H�kon Belt, Thomas Olesen, DnBNOR DK er dette bankdag i DK
                        ElseIf VB.Month(dDate) = 6 And VB.Day(dDate) = 5 Then
                            IsHoliday = True ' 5th of june, Liberation day
                        ElseIf VB.Month(dDate) = 12 And VB.Day(dDate) = 25 Then
                            IsHoliday = True ' Christemas day
                        ElseIf VB.Month(dDate) = 12 And VB.Day(dDate) = 26 Then
                            IsHoliday = True ' 2. Christemas day
                        End If
                End Select

            Case "GB"
                Select Case iDifference
                    'Case -3: IsHoliday = True 'Sk�rtorsdag - Not holiday in UK
                    Case -2 : IsHoliday = True 'Langfredag
                    Case 0 : IsHoliday = True 'P�skedag *Udgangspunkt
                    Case 1 : IsHoliday = True '2 P�skedag
                    Case Else
                        Select Case VB.Month(dDate)
                            Case 1
                                'If the 1/1 is in a weekend, the first non-weekend day is a holiday
                                Select Case VB.Day(dDate)
                                    Case 1
                                        IsHoliday = True
                                    Case 2
                                        If Weekday(System.DateTime.FromOADate(dDate.ToOADate - 1), FirstDayOfWeek.Monday) > 5 Then ' > Friday
                                            'The 1 is in the weekend
                                            IsHoliday = True
                                        End If
                                    Case 3
                                        If Weekday(System.DateTime.FromOADate(dDate.ToOADate - 2), FirstDayOfWeek.Monday) = 6 Then ' = Saturday
                                            'The 1 is on a Saturday
                                            IsHoliday = True
                                        End If
                                    Case Else
                                End Select
                            Case 5
                                If VB.Day(dDate) < 8 Then
                                    If Weekday(dDate, FirstDayOfWeek.Monday) = 1 Then '= Monday
                                        IsHoliday = True 'First Monday in may
                                    End If
                                ElseIf VB.Day(dDate) > 24 Then
                                    If Weekday(dDate, FirstDayOfWeek.Monday) = 1 Then ' = Monday
                                        IsHoliday = True 'Last Monday in may
                                    End If
                                End If
                            Case 8
                                If VB.Day(dDate) > 24 Then
                                    If Weekday(dDate, FirstDayOfWeek.Monday) = 1 Then ' = Monday
                                        IsHoliday = True 'Last Monday in may
                                    End If
                                End If

                            Case 12
                                Select Case VB.Day(dDate)
                                    Case 25, 26
                                        IsHoliday = True
                                    Case 27
                                        'If the 25th or 26 is Saturday and/or Sunday then 27th is a holiday
                                        If Weekday(System.DateTime.FromOADate(dDate.ToOADate - 2), FirstDayOfWeek.Monday) > 4 Then '25th > Thursday
                                            IsHoliday = True
                                        End If
                                    Case 28
                                        If Weekday(System.DateTime.FromOADate(dDate.ToOADate - 3), FirstDayOfWeek.Monday) = 5 Or Weekday(System.DateTime.FromOADate(dDate.ToOADate - 3), FirstDayOfWeek.Monday) = 6 Then '25th = Friday or Saturday
                                            IsHoliday = True
                                        End If
                                End Select
                            Case Else 'Month(dDate)
                                'Not an holiday
                        End Select 'Month(dDate)


                        'Case Else 'iDifference
                        'Return as not-holiday

                End Select 'iDifference

            Case "US"
                Select Case iDifference
                    'Case -3: IsHoliday = True 'Skj�rtorsdag - Not holiday in US
                    'Case -2: IsHoliday = True 'Langfredag -  Not Holiday in US
                    Case 0 : IsHoliday = True 'P�skedag *Udgangspunkt
                        'Case 1:  IsHoliday = True '2 P�skedag Not Holiday in US
                    Case Else

                        ' Fixed dates
                        If VB.Month(dDate) = 1 And VB.Day(dDate) = 1 Then
                            IsHoliday = True ' New Years day
                        ElseIf VB.Month(dDate) = 7 And VB.Day(dDate) = 4 Then
                            IsHoliday = True ' July 4th, Independence Day
                        ElseIf VB.Month(dDate) = 11 And VB.Day(dDate) = 11 Then
                            IsHoliday = True ' November 11th, Veterans Day
                            ' XNET 09.11.2012 - added next for Veteransday on a sunday, then moved to monday 12. nov
                        ElseIf VB.Month(dDate) = 11 And VB.Day(dDate) = 12 And Weekday(dDate, vbMonday) = 1 Then
                            IsHoliday = True ' November 12th, Veterans Day, on a Monday if 11. november is a Sunday !
                        ElseIf VB.Month(dDate) = 12 And VB.Day(dDate) = 25 Then
                            IsHoliday = True ' December 25th, Christmas Day

                        Else
                            ' Holidays where date is dependent on weekday
                            Select Case VB.Month(dDate)
                                Case 1
                                    If VB.Day(dDate) > 14 And VB.Day(Date.Today) < 22 Then
                                        If Weekday(dDate, FirstDayOfWeek.Monday) = 1 Then '= Monday
                                            IsHoliday = True 'Third Monday in January, Birthday of Martin Luther King
                                        End If
                                    End If
                                Case 2
                                    If VB.Day(dDate) > 14 And VB.Day(Date.Today) < 22 Then
                                        If Weekday(dDate, FirstDayOfWeek.Monday) = 1 Then '= Monday
                                            IsHoliday = True 'Third Monday in February, Presidents day
                                        End If
                                    End If
                                Case 5
                                    If VB.Day(dDate) > 24 Then
                                        If Weekday(dDate, FirstDayOfWeek.Monday) = 1 Then ' = Monday
                                            IsHoliday = True 'Last Monday in may, Memorial Day
                                        End If
                                    End If
                                Case 9
                                    If VB.Day(dDate) < 8 Then
                                        If Weekday(dDate, FirstDayOfWeek.Monday) = 1 Then '= Monday
                                            IsHoliday = True 'First Monday in September, Labor Day
                                        End If
                                    End If
                                Case 10
                                    If VB.Day(dDate) > 7 And VB.Day(Date.Today) < 15 Then
                                        If Weekday(dDate, FirstDayOfWeek.Monday) = 1 Then '= Monday
                                            IsHoliday = True 'Second Monday in October, Columbus Day
                                        End If
                                    End If
                                Case 11
                                    If VB.Day(dDate) > 22 And VB.Day(Date.Today) < 29 Then
                                        'If Weekday(dDate, vbMonday) = vbThursday Then ' = Thursday
                                        ' changed 27.11.2008, from vbThursday to 4
                                        If Weekday(dDate, FirstDayOfWeek.Monday) = 4 Then ' = Thursday
                                            IsHoliday = True 'Fourth Thursday in November, Thanksgiving
                                        End If
                                    End If
                            End Select 'Month(dDate)
                        End If
                End Select 'iDifference

            Case "CA"  ' Canada
                Select Case iDifference
                    'Case -3: IsHoliday = True 'Sk�rtorsdag - Not holiday in CA
                    Case -2 : IsHoliday = True 'Langfredag
                    Case 0 : IsHoliday = True 'P�skedag *Udgangspunkt
                    Case 1 : IsHoliday = True '2 P�skedag
                    Case Else

                        ' Fixed dates
                        If VB.Month(dDate) = 1 And VB.Day(dDate) = 1 Then
                            IsHoliday = True ' New Years day
                        ElseIf VB.Month(dDate) = 7 And VB.Day(dDate) = 1 Then
                            IsHoliday = True ' July 1th, Canada Day
                        ElseIf VB.Month(dDate) = 11 And VB.Day(dDate) = 11 Then
                            IsHoliday = True ' November 11th, Remembrance Day
                        ElseIf VB.Month(dDate) = 12 And VB.Day(dDate) = 25 Then
                            IsHoliday = True ' December 25th, Christmas Day
                        ElseIf VB.Month(dDate) = 12 And VB.Day(dDate) = 26 Then
                            IsHoliday = True ' Boxing day
                        Else
                            ' Holidays where date is dependent on weekday
                            Select Case VB.Month(dDate)
                                Case 9
                                    If VB.Day(dDate) < 8 Then
                                        If Weekday(dDate, vbMonday) = 1 Then '= Monday
                                            IsHoliday = True 'First Monday in September, Labor Day
                                        End If
                                    End If
                                Case 5
                                    If VB.Day(dDate) > 17 And VB.Day(dDate) < 25 Then
                                        If Weekday(dDate, vbMonday) = 1 Then ' = Monday
                                            IsHoliday = True 'Monday on or before May 24th, Victoria Day
                                        End If
                                    End If
                                Case 10
                                    If VB.Day(dDate) > 7 And VB.Day(dDate) < 15 Then
                                        If Weekday(dDate, vbMonday) = 1 Then '= Monday
                                            IsHoliday = True 'Second Monday in October, Thanksgiving
                                        End If
                                    End If
                            End Select 'Month(dDate)
                        End If
                End Select 'iDifference

                'XNET 16.11.2010 Lagt inn Finland
            Case "FI"   'Finland
                'New Year's Day 1 January 1 January 1 January 1 January
                'Good Friday 2 April 22 April 6 April 29 March
                'Easter Monday 5 April 25 April 9 April 1 April
                'May Day 1 May 1 May 1 May 1 May
                'Christmas Day 25 December 25 December 25 December 25 December
                'Boxing Day 26 December 26 December 26 December 26 December

                Select Case iDifference
                    Case -2 : IsHoliday = True 'Langfredag
                    Case 0 : IsHoliday = True 'P�skedag *Udgangspunkt
                    Case 1 : IsHoliday = True '2 P�skedag
                    Case Else
                        If VB.Month(dDate) = 1 And VB.Day(dDate) = 1 Then
                            IsHoliday = True ' New Years day
                        ElseIf VB.Month(dDate) = 5 And VB.Day(dDate) = 1 Then
                            IsHoliday = True ' 1st of may
                        ElseIf VB.Month(dDate) = 12 And VB.Day(dDate) = 25 Then
                            IsHoliday = True ' Christemas day
                        ElseIf VB.Month(dDate) = 12 And VB.Day(dDate) = 26 Then
                            IsHoliday = True ' 2. Christemas day
                        End If
                End Select

                ' XNET 16.11.2010 Lagt inn Tyskland
            Case "DE"   'Germany
                Select Case iDifference
                    Case -2 : IsHoliday = True 'Langfredag
                    Case 0 : IsHoliday = True 'P�skedag *Udgangspunkt
                    Case 1 : IsHoliday = True '2 P�skedag
                    Case Else
                        If VB.Month(dDate) = 1 And VB.Day(dDate) = 1 Then
                            IsHoliday = True ' New Years day
                        ElseIf VB.Month(dDate) = 5 And VB.Day(dDate) = 1 Then
                            IsHoliday = True ' 1st of may
                        ElseIf VB.Month(dDate) = 5 And VB.Day(dDate) = 13 Then
                            IsHoliday = True ' Ascension
                        ElseIf VB.Month(dDate) = 5 And VB.Day(dDate) = 24 Then
                            IsHoliday = True ' Whit Monday
                        ElseIf VB.Month(dDate) = 10 And VB.Day(dDate) = 3 Then
                            IsHoliday = True ' German unity day
                        ElseIf VB.Month(dDate) = 12 And VB.Day(dDate) = 25 Then
                            IsHoliday = True ' Christemas day
                        ElseIf VB.Month(dDate) = 12 And VB.Day(dDate) = 26 Then
                            IsHoliday = True ' 2. Christemas day
                        End If
                End Select


                ' XNET 08.09.2011 - added Singapore bank holidays
            Case "SG"

                Select Case iDifference
                    Case -2 : IsHoliday = True 'Langfredag

                    Case Else
                        ' Chineese new year ????
                        ' Difficult to calculate, so lets set some fixed dates for some years
                        'Dragon January 23, 2012
                        'Snake February 10, 2013
                        'Horse January 31, 2014
                        'Ram/Sheep February 19, 2015
                        'Monkey February 9, 2016
                        'Rooster January 28, 2017
                        'Dog February 16, 2018
                        'Boar February 5, 2019
                        'Rat January 25, 2020
                        '2021 February 12th (Friday) Ox
                        '2022: February 1st (Tuesday) Tiger
                        '2023: January 22nd (Wednesday) Rabbit
                        '2024: February 10th (Saturday) Dragon
                        '2025: January 29th (Wednesday) Snake

                        ' Also next day is holiday
                        If Month(dDate) = 1 And (VB.Day(dDate) = 23 Or VB.Day(dDate) = 24) And Year(dDate) = 2012 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 2 And (VB.Day(dDate) = 10 Or VB.Day(dDate) = 11) And Year(dDate) = 2013 Then
                            IsHoliday = True
                        ElseIf (Month(dDate) = 1 And VB.Day(dDate) = 31) Or (Month(dDate) = 2 And VB.Day(dDate) = 1) And Year(dDate) = 2014 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 2 And (VB.Day(dDate) = 19 Or VB.Day(dDate) = 20) And Year(dDate) = 2015 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 2 And (VB.Day(dDate) = 9 Or VB.Day(dDate) = 10) And Year(dDate) = 2016 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 1 And (VB.Day(dDate) = 28 Or VB.Day(dDate) = 29) And Year(dDate) = 2017 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 2 And (VB.Day(dDate) = 16 Or VB.Day(dDate) = 17) And Year(dDate) = 2018 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 2 And (VB.Day(dDate) = 5 Or VB.Day(dDate) = 6) And Year(dDate) = 2019 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 1 And (VB.Day(dDate) = 25 Or VB.Day(dDate) = 26) And Year(dDate) = 2020 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 2 And (VB.Day(dDate) = 12 Or VB.Day(dDate) = 13) And Year(dDate) = 2021 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 2 And (VB.Day(dDate) = 1 Or VB.Day(dDate) = 2) And Year(dDate) = 2022 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 1 And (VB.Day(dDate) = 22 Or VB.Day(dDate) = 23) And Year(dDate) = 2023 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 2 And (VB.Day(dDate) = 10 Or VB.Day(dDate) = 11) And Year(dDate) = 2024 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 1 And (VB.Day(dDate) = 29 Or VB.Day(dDate) = 30) And Year(dDate) = 2025 Then
                            IsHoliday = True



                            ' Buddha day - full moon on the third month
                            ' End of Ramadan (ID) ' Cannot be calculated in advance !

                            ' Deepavali - hinduism
                        ElseIf Month(dDate) = 10 And VB.Day(dDate) = 26 And Year(dDate) = 2011 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 11 And VB.Day(dDate) = 13 And Year(dDate) = 2012 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 11 And VB.Day(dDate) = 3 And Year(dDate) = 2013 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 10 And VB.Day(dDate) = 23 And Year(dDate) = 2014 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 11 And VB.Day(dDate) = 11 And Year(dDate) = 2015 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 10 And VB.Day(dDate) = 30 And Year(dDate) = 2016 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 10 And VB.Day(dDate) = 19 And Year(dDate) = 2017 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 11 And VB.Day(dDate) = 7 And Year(dDate) = 2018 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 10 And VB.Day(dDate) = 27 And Year(dDate) = 2019 Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 11 And VB.Day(dDate) = 14 And Year(dDate) = 2020 Then
                            IsHoliday = True

                            ' Hari Raya Haji, ID ul  hada  ' Cannot be calculated in advance !

                            ' If holiday on Sunday, move to Monday !
                        ElseIf Month(dDate) = 1 And VB.Day(dDate) = 1 Then
                            IsHoliday = True ' New Years day
                        ElseIf Month(dDate) = 1 And VB.Day(dDate) = 2 And iDay = vbMonday Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 5 And VB.Day(dDate) = 1 Then
                            IsHoliday = True ' 1st of may
                        ElseIf Month(dDate) = 5 And VB.Day(dDate) = 2 And iDay = vbMonday Then
                            ' 1. of May on Sunday, then Monday May 2nd is holiday
                            IsHoliday = True
                        ElseIf Month(dDate) = 8 And VB.Day(dDate) = 9 Then
                            IsHoliday = True ' ' National day august 9
                        ElseIf Month(dDate) = 8 And VB.Day(dDate) = 10 And iDay = vbMonday Then
                            IsHoliday = True
                        ElseIf Month(dDate) = 12 And VB.Day(dDate) = 25 Then
                            IsHoliday = True ' Christmas day
                        ElseIf Month(dDate) = 12 And VB.Day(dDate) = 26 And iDay = vbMonday Then
                            IsHoliday = True ' Christmas day on Dec 26 if Dec 25 is Sunday
                        ElseIf Month(dDate) = 12 And VB.Day(dDate) = 27 And iDay = vbMonday Then
                            IsHoliday = True


                        End If
                End Select


                ' XNET 08.09.2011 Added Australia
            Case "AU"   'Australia

                Select Case iDifference
                    Case 0 : IsHoliday = True 'Easter day
                    Case 40 : IsHoliday = True 'Ascension Day: Celebrated on May 17, the Ascension Day comes after 30 days from Easter. It is celebrated on the 40th day, which is always a Sunday. Christ is believed to have accented to heaven on this day.

                    Case Else

                        If Month(dDate) = 1 And VB.Day(dDate) = 1 Then
                            'New Year's Day: In 2012, the New Year�s Day is on a Sunday. The day is celebrated following the modern Gregorian calendar. Church bells in all parts of Australia starts ringing at the stroke of midnight. People go on a mass followed by a grand celebration throughout the nation.
                            IsHoliday = True ' New Years day
                        ElseIf Month(dDate) = 1 And VB.Day(dDate) = 6 Then
                            'Epiphany: Celebrated mainly by the Christians, this is an important festival of Australia. Epiphany is celebrated on January 6. It is regarded as a public holiday when all schools, colleges, private and public offices and banks remain closed. Being one of the oldest Christian celebrations the festivities include religious rituals and grand feasts.
                            IsHoliday = True ' 6st of Jan
                        ElseIf Month(dDate) = 1 And VB.Day(dDate) = 26 Then
                            IsHoliday = True 'Australia Day on 26 January - Australia is a national holiday and one of the bank holidays in Australia. Australia Day is an annual celebration. It falls on the 26th January in all the states and territories of Australia. It is the day of the establishment of the first English settlement at Port Jackson by Captain Arthur Phillip in 1788. Since 1960, on the Australia day the Prime Minister announces the winner of the Australia of the Year award.
                        ElseIf Month(dDate) = 4 And VB.Day(dDate) = 25 Then
                            IsHoliday = True 'Anzac Day on 25 April - On the Anzac Day, Australia pays homage to the citizens who served the country in wars. Anzac Day is celebrated on the 25th April every year. On the Anzac Day war memorials are held around the country.
                        ElseIf Month(dDate) = 12 And VB.Day(dDate) = 25 Then
                            IsHoliday = True 'Christmas Day on 25 December - Christmas is celebrated worldwide on 25 December on a magnanimous scale. The Christmas carnival commemorates the birth of Jesus. On the Christmas Eve people offer prayers, deck up Christmas tree, decorate their houses with lights and wreath, exchange gifts with their loved ones, sing Christmas carols and enjoy feasting.
                        ElseIf Month(dDate) = 12 And VB.Day(dDate) = 26 Then
                            IsHoliday = True 'Boxing Day on 26 December - The day after Christmas is observed as Boxing Day. The day enunciates the start of the post-Christmas sale season. It is a significant sporting day, and in various places Boxing Day test matches are organized.
                        End If
                End Select

            Case "XX"
                'Meaning no specific country

            Case Else 'UCase(sISOCountry)

        End Select 'UCase(sISOCountry)

    End Function

    Public Function GetBankday(ByVal dDate As Date, ByRef sISOCountry As String, ByRef iDaysDifference As Short) As Date
        ' 13.05.2020 changed from ByRef dDate to ByVal
        Dim bFoundBankday As Boolean
        Dim iBankDaysFound As Short
        Dim iDayCounter As Short

        bFoundBankday = False
        iBankDaysFound = 0
        iDayCounter = 0

        If iDaysDifference = 0 Then
            'GetBankday = dDate
            ' Changed 04.01.2008
            ' Must also be able to see if dDate is a bankdate. If not, add one day
            iDayCounter = -1
            Do
                iDayCounter = iDayCounter + 1
                If Not IsHoliday(System.DateTime.FromOADate(dDate.ToOADate + iDayCounter), sISOCountry, True) Then
                    GetBankday = System.DateTime.FromOADate(dDate.ToOADate + iDayCounter)
                    Exit Do
                End If
            Loop

        Else
            Do Until bFoundBankday
                iDayCounter = iDayCounter + 1
                If iDaysDifference < 0 Then
                    If Not IsHoliday(System.DateTime.FromOADate(dDate.ToOADate - iDayCounter), sISOCountry, True) Then
                        iBankDaysFound = iBankDaysFound + 1
                    End If
                    If iBankDaysFound = iDaysDifference * -1 Then
                        GetBankday = System.DateTime.FromOADate(dDate.ToOADate - iDayCounter)
                        bFoundBankday = True
                    End If
                Else
                    If Not IsHoliday(System.DateTime.FromOADate(dDate.ToOADate + iDayCounter), sISOCountry, True) Then
                        iBankDaysFound = iBankDaysFound + 1
                    End If
                    If iBankDaysFound = iDaysDifference Then
                        GetBankday = System.DateTime.FromOADate(dDate.ToOADate + iDayCounter)
                        bFoundBankday = True
                    End If
                End If
            Loop
        End If

    End Function
    Public Function ReplaceDateTimeStamp(ByRef sFilenamePathOut As String, Optional ByRef bDontUseBankdays As Boolean = False) As String
        ' New by JanP 12.08.02
        ' Allow timestamp in exportfilename
        ' Starts and ends with %
        ' Use
        ' Y Full Year (2002)
        ' y last two digits of year (02)
        ' M for Months
        ' D for Days
        ' h for Hours
        ' m for minutes
        ' s for seconds
        ' Changes 22.05.2009
        ' Allow to add or reduce from day, month or year, by setting f.ex. M-1 or M+1 or D-1 or D+1

        Dim sFilenameOut, sPathOut As String
        Dim sStart, Send As Short
        Dim sReplacePart As String
        Dim iDaysDifference, sStartOfMacro, iStartDaysDifference As Short
        Dim dDateToUse As Date
        Dim iYearsToAddorReduce As Short
        Dim iMonthsToAddorReduce As Short
        Dim iDaysToAddorReduce As Short

        ' CHanged to also accept dates/time in folderpart of filename, not only in filename
        ' Has changed from testing sFilenameout to sFilenamePathOut in some lines

        sPathOut = Left(sFilenamePathOut, InStrRev(sFilenamePathOut, "\"))
        sFilenameOut = Right(sFilenamePathOut, Len(sFilenamePathOut) - InStrRev(sFilenamePathOut, "\"))
        sStartOfMacro = 1
        ' Replace codes with real date and time
        ' There may be more than one timestamp, so lets loop through the filename:
        ' (ex. Out%ymd%_%hms%.dat
        Do
            'sStart = InStr(sStartOfMacro, sFilenameOut, "%")  ' first %
            sStart = InStr(sStartOfMacro, sFilenamePathOut, "%") ' first %
            If sStart = 0 Then
                ' no more macros, get out
                Exit Do
            End If
            sStart = sStart + 1 ' First char after %
            'Send = InStr(sStart, sFilenameOut, "%") - 1  ' last %
            Send = InStr(sStart, sFilenamePathOut, "%") - 1 ' last %

            'sReplacePart = Mid$(sFilenameOut, sStart, (Send - sStart) + 1) ' String with datetimestamp macro
            sReplacePart = Mid(sFilenamePathOut, sStart, (Send - sStart) + 1) ' String with datetimestamp macro

            'See if we have to correct the date
            iStartDaysDifference = InStr(1, sReplacePart, ",")
            If iStartDaysDifference > 0 Then
                iDaysDifference = Val(Mid(sReplacePart, iStartDaysDifference + 1))
                sReplacePart = Left(sReplacePart, iStartDaysDifference - 1)
                If iDaysDifference <> 0 Then
                    If bDontUseBankdays Then
                        dDateToUse = GetBankday(Now, "NO", iDaysDifference)
                    Else
                        dDateToUse = System.DateTime.FromOADate(Now.ToOADate + iDaysDifference)
                    End If
                End If
            Else
                iDaysDifference = 0
                dDateToUse = Now
            End If
            '4/1-04 Kjell has deleted the following IF-statement
            'If iDaysDifference = 0 And bDontUseBankdays = True And IsHoliday(Now(), "NO", True) Then
            'sPathOut = ""
            'sFilenameOut = ""
            'Else
            ' Date
            ' added 22.05.2009, changes to add or reduce day, month or year
            ' Assume max 9 years, months or days to add or reduce (one position in filestring)
            If InStr(sReplacePart, "Y-") > 0 Then
                iYearsToAddorReduce = Val(Mid(sReplacePart, InStr(sReplacePart, "Y-") + 2, 1)) * -1
                dDateToUse = DateAdd(Microsoft.VisualBasic.DateInterval.Year, iYearsToAddorReduce, dDateToUse)
                ' remove the -1 or -2 or -3, etc from filestring
                sReplacePart = Left(sReplacePart, InStr(sReplacePart, "Y-")) & Mid(sReplacePart, InStr(sReplacePart, "Y-") + 2)
            ElseIf InStr(sReplacePart, "Y+") > 0 Then
                iYearsToAddorReduce = Val(Mid(sReplacePart, InStr(sReplacePart, "Y+") + 2, 1))
                dDateToUse = DateAdd(Microsoft.VisualBasic.DateInterval.Year, iYearsToAddorReduce, dDateToUse)
                ' remove the +1 or +2 or +3, etc from filestring
                sReplacePart = Left(sReplacePart, InStr(sReplacePart, "Y+")) & Mid(sReplacePart, InStr(sReplacePart, "Y+") + 2)
            End If
            If InStr(sReplacePart, "M-") > 0 Then
                iMonthsToAddorReduce = Val(Mid(sReplacePart, InStr(sReplacePart, "M-") + 2, 1)) * -1
                dDateToUse = DateAdd(Microsoft.VisualBasic.DateInterval.Month, iMonthsToAddorReduce, dDateToUse)
                ' remove the -1 or -2 or -3, etc from filestring
                sReplacePart = Left(sReplacePart, InStr(sReplacePart, "M-")) & Mid(sReplacePart, InStr(sReplacePart, "M-") + 3)
            ElseIf InStr(sReplacePart, "M+") > 0 Then
                iMonthsToAddorReduce = Val(Mid(sReplacePart, InStr(sReplacePart, "M+") + 2, 1))
                dDateToUse = DateAdd(Microsoft.VisualBasic.DateInterval.Month, iMonthsToAddorReduce, dDateToUse)
                ' remove the +1 or +2 or +3, etc from filestring
                sReplacePart = Left(sReplacePart, InStr(sReplacePart, "M+")) & Mid(sReplacePart, InStr(sReplacePart, "M+") + 2)
            End If
            If InStr(sReplacePart, "D-") > 0 Then
                iDaysToAddorReduce = Val(Mid(sReplacePart, InStr(sReplacePart, "D-") + 2, 1))
                dDateToUse = System.DateTime.FromOADate(dDateToUse.ToOADate - iDaysToAddorReduce)
                ' remove the -1 or -2 or -3, etc from filestring
                sReplacePart = Left(sReplacePart, InStr(sReplacePart, "D-")) & Mid(sReplacePart, InStr(sReplacePart, "D-") + 3)
            ElseIf InStr(sReplacePart, "D+") > 0 Then
                iDaysToAddorReduce = Val(Mid(sReplacePart, InStr(sReplacePart, "D+") + 2, 1))
                dDateToUse = System.DateTime.FromOADate(dDateToUse.ToOADate + iDaysToAddorReduce)
                ' remove the +1 or +2 or +3, etc from filestring
                sReplacePart = Left(sReplacePart, InStr(sReplacePart, "D+")) & Mid(sReplacePart, InStr(sReplacePart, "D+") + 2)
            End If
            sReplacePart = Replace(sReplacePart, "Y", Trim(Str(Year(dDateToUse)))) ' four digits (2002)
            sReplacePart = Replace(sReplacePart, "y", Right(CStr(Year(dDateToUse)), 2)) ' last two digits (02)
            sReplacePart = Replace(sReplacePart, "M", PadLeft(Str(Month(dDateToUse)), 2, "0"))
            sReplacePart = Replace(sReplacePart, "D", PadLeft(Str(VB.Day(dDateToUse)), 2, "0"))

            ' Time
            sReplacePart = Replace(sReplacePart, "h", PadLeft(Str(Hour(Now)), 2, "0"))
            sReplacePart = Replace(sReplacePart, "m", PadLeft(Str(Minute(Now)), 2, "0"))
            sReplacePart = Replace(sReplacePart, "s", PadLeft(Str(Second(Now)), 2, "0"))

            'sFilenameOut = Left$(sFilenameOut, sStart - 2) & sReplacePart & Mid$(sFilenameOut, Send + 2)
            sFilenamePathOut = Left(sFilenamePathOut, sStart - 2) & sReplacePart & Mid(sFilenamePathOut, Send + 2)
            sStartOfMacro = Send + 1
            'End If
        Loop

        'ReplaceDateTimeStamp = sPathOut & sFilenameOut
        ReplaceDateTimeStamp = sFilenamePathOut



    End Function

    Public Function ClipBoard_GetData(ByRef sErrorString As String) As String
        Dim hClipMemory As Integer
        Dim lpClipMemory As Integer
        Dim MyString As String
        Dim RetVal As Integer

        If OpenClipboard(0) = 0 Then
            sErrorString = "Cannot open Clipboard. Another app. may have it open"
            Exit Function
        End If

        ' Obtain the handle to the global memory
        ' block that is referencing the text.
        hClipMemory = GetClipboardData(CF_TEXT)
        If IsDBNull(hClipMemory) Then
            sErrorString = "Could not allocate memory"
            GoTo OutOfHere
        End If

        ' Lock Clipboard memory so we can reference
        ' the actual data string.
        lpClipMemory = GlobalLock(hClipMemory)

        If Not IsDBNull(lpClipMemory) Then
            MyString = Space(MAXSIZE)
            RetVal = lstrcpy(MyString, lpClipMemory)
            RetVal = GlobalUnlock(hClipMemory)

            ' Peel off the null terminating character.
            MyString = Mid(MyString, 1, InStr(1, MyString, Chr(0), 0) - 1)
        Else
            sErrorString = "Could not lock memory to copy string from."
        End If

OutOfHere:

        RetVal = CloseClipboard()
        ClipBoard_GetData = MyString

    End Function

    Public Function ClipBoard_SetData(ByRef MyString As String, ByRef sErrorString As String) As Boolean
        Dim hGlobalMemory, lpGlobalMemory As Integer
        Dim hClipMemory, x As Integer
        Dim bReturnValue As Boolean

        bReturnValue = True 'Assume success

        ' Allocate moveable global memory.
        '-------------------------------------------
        hGlobalMemory = GlobalAlloc(GHND, Len(MyString) + 1)

        ' Lock the block to get a far pointer
        ' to this memory.
        lpGlobalMemory = GlobalLock(hGlobalMemory)

        ' Copy the string to this global memory.
        lpGlobalMemory = lstrcpy(lpGlobalMemory, MyString)

        ' Unlock the memory.
        If GlobalUnlock(hGlobalMemory) <> 0 Then
            sErrorString = "Could not unlock memory location. Copy aborted."
            bReturnValue = False
            GoTo OutOfHere2
        End If

        ' Open the Clipboard to copy data to.
        If OpenClipboard(0) = 0 Then
            sErrorString = "Could not open the Clipboard. Copy aborted."
            bReturnValue = False
            Exit Function
        End If

        ' Clear the Clipboard.
        x = EmptyClipboard()

        ' Copy the data to the Clipboard.
        hClipMemory = SetClipboardData(CF_TEXT, hGlobalMemory)

OutOfHere2:

        If CloseClipboard() = 0 Then
            sErrorString = "Could not close Clipboard."
            bReturnValue = False
        End If

    End Function


    Public Sub StayOnTop(ByRef frmForm As System.Windows.Forms.Form, ByRef fOnTop As Boolean)
        Const HWND_TOPMOST As Short = -1
        Const HWND_NOTOPMOST As Short = -2
        Dim lState As Integer
        Dim iwidth, ileft, itop, iheight As Short

        With frmForm
            ileft = VB6.PixelsToTwipsX(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width) / VB6.TwipsPerPixelX - VB6.PixelsToTwipsX(.Width) / VB6.TwipsPerPixelX
            itop = VB6.PixelsToTwipsY(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height) / VB6.TwipsPerPixelY - VB6.PixelsToTwipsY(.Height) / VB6.TwipsPerPixelY - 400 / VB6.TwipsPerPixelY
            iwidth = VB6.PixelsToTwipsX(.Width) / VB6.TwipsPerPixelX
            iheight = VB6.PixelsToTwipsY(.Height) / VB6.TwipsPerPixelY
        End With

        lState = HWND_NOTOPMOST
        If fOnTop Then lState = HWND_TOPMOST

        Call SetWindowPos(frmForm.Handle.ToInt32, lState, ileft, itop, iwidth, iheight, 0)



    End Sub

    Public Sub HideCaptionBar(ByRef frmForm As System.Windows.Forms.Form)

        Dim Style As Integer

        Const WS_CAPTION As Integer = &HC00000
        Const GWL_STYLE As Short = (-16)

        Style = GetWindowLong(frmForm.Handle.ToInt32, GWL_STYLE)

        Call SetWindowLong(frmForm.Handle.ToInt32, GWL_STYLE, Style And Not WS_CAPTION)
        Call DrawMenuBar(frmForm.Handle.ToInt32)


    End Sub

    'Public Function ExecuteTheSQL(ByRef oConnection As Integer, ByRef sMySQL As String, ByRef sErrorString As String, Optional ByRef bUseCommit As Boolean = True) As Boolean
    '    Public Function ExecuteTheSQL(ByRef oConnection As ADODB.Connection, ByRef sMySQL As String, ByRef sErrorString As String, Optional ByRef bUseCommit As Boolean = True) As Boolean
    '        Dim cmdUpdate As New ADODB.Command
    '        Dim bReturnValue As Boolean

    '        On Error GoTo ErrorExecuteTheSQL

    '        'If cnSlett.State = adStateClosed Then
    '        '    Set oDatabase = New Database
    '        '    sMyFile = BB_DatabasePath
    '        '
    '        '
    '        '    'Set cnProfile = New ADODB.Connection
    '        '
    '        '    cnSlett.ConnectionString = oDatabase.ConnectToProfile(sMyFile)
    '        '    cnSlett.Open
    '        'End If



    '        'If oConnection.State = adStateClosed Then
    '        '    sErrorString = "The connection to the DB is not open."
    '        '    bReturnValue = False
    '        'End If

    '        '20.10.2009
    '        '06.01.2010 - Added the use of bUseCommit
    '        If bUseCommit Then
    '            oConnection.BeginTrans()
    '            cmdUpdate.CommandText = sMySQL
    '            cmdUpdate.CommandType = ADODB.CommandTypeEnum.adCmdText
    '            cmdUpdate.let_ActiveConnection(oConnection)
    '            cmdUpdate.Execute()
    '            '20.10.2009
    '            oConnection.CommitTrans()
    '        Else
    '            cmdUpdate.CommandText = sMySQL
    '            cmdUpdate.CommandType = ADODB.CommandTypeEnum.adCmdText
    '            cmdUpdate.let_ActiveConnection(oConnection)
    '            cmdUpdate.Execute()
    '        End If

    '        ExecuteTheSQL = True
    '        Exit Function

    'ErrorExecuteTheSQL:
    '        bReturnValue = False
    '        sErrorString = Err.Description

    '        'If cnProfile.State = adStateClosed Then
    '        '    Set oDatabase = New Database
    '        '    sMyFile = BB_DatabasePath
    '        '
    '        '
    '        '    'Set cnProfile = New ADODB.Connection
    '        '
    '        '    cnProfile.ConnectionString = oDatabase.ConnectToProfile(sMyFile)
    '        '    cnProfile.Open
    '        'End If

    '        'If bClose Then
    '        '    cnProfile.Close
    '        '    Set oDatabase = Nothing
    '        '    Exit Function
    '        'Else
    '        '    cmdUpdate.CommandText = sMySQL
    '        '    cmdUpdate.CommandType = adCmdText
    '        '    cmdUpdate.ActiveConnection = cnProfile
    '        '    cmdUpdate.Execute
    '        'End If

    '    End Function

    Public Function bVal(ByVal bBool As Boolean) As Short
        ' Converts from Boolean to Integer
        ' False -> 0
        ' True ->  1
        bVal = IIf(bBool, 1, 0)
    End Function
    'Public Sub SetProgressBarColour(ByRef hwndProgBar As Integer, ByVal clrref As Integer)
    '    Call SendMessage(hwndProgBar, PBM_SETBARCOLOR, 0, clrref)
    'End Sub

    Public Function SetEnvironment(ByRef sName As String, ByRef sValue As String) As String
        'Set Environment variable
        SetEnvironment = CStr(SetEnvironmentVariable(sName, sValue))

    End Function
    Public Function GetEnvironment(ByRef sName As String) As String
        'Read Environmen variable
        Dim sValue As String
        Dim lSize As Integer
        Dim sRetVal As Integer

        'Get the size of the buffer needed to store the value
        lSize = GetEnvironmentVariable(sName, sValue, 0)

        If lSize > 0 Then
            'The variable exists

            'Allocate buffer for the value
            sValue = New String(" ", lSize)

            sRetVal = GetEnvironmentVariable(sName, sValue, lSize)

            'Remove the terminating Null
            sValue = Left(sValue, sRetVal)
        End If
        GetEnvironment = sValue

    End Function
    Public Function ValidateIBAN(ByRef sIBANNo As String, Optional ByRef bShowErrorMessage As Boolean = True) As Boolean
        Dim lCounter As Integer
        Dim sCharacter As String
        Dim iCharacter As Short
        Dim lNoOfLoops As Integer
        Dim sCopyOfIBANNo As String
        Dim sNewIBANNo As String
        Dim lControlDigit As Integer
        Dim lCalculatedDigit As Integer
        Dim bReturnValue As Boolean '14.12.2017 - Added the use of bReturnValue, to avoid stop with Err.Raise in debug-mode

        On Error GoTo ErrorValidateIBAN

        bReturnValue = True

        'After new standards, always use uppercase
        sCopyOfIBANNo = UCase(sIBANNo)

        If Not IsNumeric(Mid(sIBANNo, 3, 2)) Then
            'Not an valid character
            Err.Raise(30002, "ValidateIBAN", LRSCommon(30002))
            '30002: Position 3 and 4 in an IBAN-number must be numeric.
        Else
            lControlDigit = Val(Mid(sIBANNo, 3, 2))
        End If

        'Move countrycode and controldigits to the end
        sCopyOfIBANNo = Mid(sCopyOfIBANNo, 5) & Left(sCopyOfIBANNo, 4)

        'Convert alphabetical characters into numbers, and validate that no other signs are used
        lNoOfLoops = Len(sCopyOfIBANNo) - 2
        sNewIBANNo = ""
        For lCounter = 1 To lNoOfLoops
            sCharacter = Mid(sCopyOfIBANNo, lCounter, 1)
            iCharacter = Asc(sCharacter)
            If iCharacter > 47 And iCharacter < 58 Then
                'OK, This is a digit
                sNewIBANNo = sNewIBANNo & sCharacter
            ElseIf iCharacter > 64 And iCharacter < 91 Then
                'This is an alphabetical charater, convert it
                sNewIBANNo = sNewIBANNo & Trim(Str(iCharacter - 55))
            Else
                'Not an valid character
                If bShowErrorMessage Then '14.12.2017 - Added the use of bReturnValue, to avoid stop with Err.Raise in debug-mode
                    Err.Raise(30001, "ValidateIBAN", LRSCommon(30001, sCharacter))
                Else
                    bReturnValue = False
                End If
                '30001: The character, %1, is not valid in an IBAN-number.
            End If
        Next lCounter

        'Add 00 to the new IBANno. According to the rules
        sNewIBANNo = sNewIBANNo & "00"

        'BEcause of calculation trouble with a long digit, use blocks of 9 and digits

        Do While Len(sNewIBANNo) > 0
            If Len(sNewIBANNo) > 9 Then
                lCalculatedDigit = GetModulus97Digits(CDbl(Left(sNewIBANNo, 9)))
                sNewIBANNo = Trim(Str(lCalculatedDigit)) & Mid(sNewIBANNo, 10)
            Else
                lCalculatedDigit = GetModulus97Digits(CDbl(sNewIBANNo))
                sNewIBANNo = ""
            End If
        Loop

        lCalculatedDigit = 98 - lCalculatedDigit

        If lCalculatedDigit <> lControlDigit Then
            If bShowErrorMessage Then
                Err.Raise(30003, "ValidateIBAN", LRSCommon(30003))
            Else
                bReturnValue = False
            End If
            '30003: The IBAN-validation reports an error. The calculation of the check-digits, doesn't match the check-digits stated..
        End If

        ValidateIBAN = bReturnValue

        Exit Function

ErrorValidateIBAN:

        If bShowErrorMessage Then
            ValidateIBAN = False
            Err.Raise(Err.Number, Err.Source, Err.Description)
        Else
            ValidateIBAN = False
        End If

    End Function

    Public Function GetModulus97Digits(ByRef nNumberToCalculate As Double) As Integer

        GetModulus97Digits = nNumberToCalculate - (Int(nNumberToCalculate / 97)) * 97

    End Function
    Public Function CalculateIBAN(ByRef sBBAN As String, ByRef sCountry As String) As String
        ' pass BBAN and two letter country string (FI , BE, NO etc)
        ' returns full IBAN account number
        ' IBAN calculated by taking BBAN + numeric reference of Countrys letters + 10 (A=10, B=11, C=12 etc) + "00"
        ' Take MOD97 of this, and subtract that from 98
        Dim sCountryPart As String
        sCountryPart = (Asc(sCountry.Substring(0, 1)) - 55).ToString("00") & (Asc(sCountry.Substring(1, 1)) - 55).ToString("00") & "00"
        CalculateIBAN = sCountry & PadLeft((98 - GetModulusWithLargeNumbers(sBBAN + sCountryPart, 97)).ToString, 2, "00") & sBBAN

    End Function
    Public Function GetModulusWithLargeNumbers(ByVal sInString As String, ByVal iModulus As Integer) As Integer

        'Const iModulus As Integer = 97
        'Dim sinstring As String = "654654321984981624196846213"

        Dim done As Boolean
        Dim result As Integer
        Do Until done

            Dim l As Long
            If Long.TryParse(sInString, l) Then

                result = l Mod iModulus
                done = True

            Else

                'its still too big so treat it like a string
                Dim s3 As String = sInString.Substring(0, 3)
                Dim i As Integer = CInt(s3)

                Dim m As Integer = i Mod iModulus
                sInString = String.Concat(m.ToString, sInString.Substring(3, sInString.Length - 3))

            End If

        Loop

        GetModulusWithLargeNumbers = result

    End Function
    Public Function IsIBANNumber(ByRef sAccountNo As String, ByRef bValidateAsWell As Boolean, Optional ByRef bShowErrorMessage As Boolean = True, Optional ByRef sErrorMessage As String = "") As Scripting.Tristate
        'Returnvalues
        '0 = Ikke IBAN-Nummer
        '1 = IBAN-Nummer, hvis Validert, ikke korrekt
        '2 = Korrekt IBAN-nummer
        Dim iCharacter As Short
        Dim bIsIBAN As Boolean
        Dim bValidationStarted As Boolean

        On Error GoTo ErrorISIBANNumber

        bValidationStarted = False

        'If EmptyString(sAccountNo) Then
        ' 16.07.2009 changed
        If Len(sAccountNo) < 3 Then
            bIsIBAN = False
        Else
            bIsIBAN = True
            iCharacter = Asc(UCase(Left(sAccountNo, 1)))
            If iCharacter > 64 And iCharacter < 91 Then ' a character
                bIsIBAN = True
            Else
                bIsIBAN = False
            End If

            iCharacter = Asc(UCase(Mid(sAccountNo, 2, 1)))
            If bIsIBAN And (iCharacter > 64 And iCharacter < 91) Then ' a character
                bIsIBAN = True
            Else
                bIsIBAN = False
            End If

            If bIsIBAN And IsNumeric(Mid(sAccountNo, 3, 2)) Then
                bIsIBAN = True
            Else
                bIsIBAN = False
            End If

            If bIsIBAN Then
                'Verify length
                Select Case UCase(Left(sAccountNo, 2))

                    Case "DE" ' Germany
                        If Len(sAccountNo) = 22 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "DK" ' Denmark
                        If Len(sAccountNo) = 18 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "FI" ' Finland
                        If Len(sAccountNo) = 18 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "GB" ' Great Britain
                        ' United Kingdom (22) IBAN format: GBkk BBBB SSSS SSCC CCCC CC
                        ' B = alphabetical bank code, S = sort code (often a specific branch), C = account No.
                        If Len(sAccountNo) = 22 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "BE" ' Belgium
                        If Len(sAccountNo) = 16 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "FR" ' France
                        If Len(sAccountNo) = 27 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "GR" ' Greece
                        If Len(sAccountNo) = 27 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If
                    Case "IE" ' Ireland
                        If Len(sAccountNo) = 22 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "IT" ' Italy
                        If Len(sAccountNo) = 27 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "LU" ' Luxemburg
                        If Len(sAccountNo) = 20 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "NL" ' Netherlands
                        If Len(sAccountNo) = 18 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "PT" ' Portugal
                        If Len(sAccountNo) = 25 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "SI" ' Slovenia
                        If Len(sAccountNo) = 19 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "ES" ' Spain
                        If Len(sAccountNo) = 24 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "AT" ' Austria
                        If Len(sAccountNo) = 20 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If
                    Case "CY" ' Cyprus
                        If Len(sAccountNo) = 28 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case "MT" ' Malta
                        If Len(sAccountNo) = 31 Then
                            bIsIBAN = True
                        Else
                            bIsIBAN = False
                        End If

                    Case Else
                        bIsIBAN = True
                End Select
            End If

            bValidationStarted = True

            If bIsIBAN And bValidateAsWell Then
                If ValidateIBAN(sAccountNo, bShowErrorMessage) Then
                    IsIBANNumber = Scripting.Tristate.TristateTrue
                Else
                    IsIBANNumber = Scripting.Tristate.TristateMixed
                End If
            Else
                If bIsIBAN Then
                    IsIBANNumber = Scripting.Tristate.TristateTrue
                Else
                    IsIBANNumber = Scripting.Tristate.TristateFalse
                End If
            End If
        End If

        Exit Function

ErrorISIBANNumber:
        If bShowErrorMessage Then
            Err.Raise(Err.Number, "ISIBANNumber", Err.Description)
        Else
            sErrorMessage = Err.Description
            If bValidationStarted Then
                IsIBANNumber = Scripting.Tristate.TristateMixed
            Else
                IsIBANNumber = Scripting.Tristate.TristateFalse
            End If
        End If

    End Function
    'XNET - 16.11.2010 - Added new function
    Public Function ExtractAccountFromIBAN(ByVal sAccount As String, Optional ByVal sCountryCode As String = "", Optional ByVal bShowErrorMessage As Boolean = True, Optional ByVal sErrorMessage As String = "") As String
        Dim sReturnValue As String

        On Error GoTo ErrorExtractAccountFromIBAN

        ' 25.08.2015
        ' If nothing found below, return original account passed to function !!!
        sReturnValue = sAccount

        If EmptyString(sCountryCode) Then
            sCountryCode = Left$(sAccount, 2)
        End If
        Select Case sCountryCode

            Case "DE"
                ' Germany (22) IBAN format: DEkk BBBB BBBB CCCC CCCC CC
                ' B = sort code (Bankleitzahl/BLZ), C = account No.
                ' last ten digits are accountno
                sReturnValue = RemoveLeadingCharacters(Mid$(sAccount, 13), "0")

            Case "DK"
                'Denmark (18) IBAN format: DKkk BBBB CCCC CCCC CC
                'B = bank No., C = account No.
                'Last 14 digits are accountno
                If sAccount.Length > 4 Then
                    sReturnValue = sAccount.Substring(4)
                End If

            Case "FI"
                'Finland (18) IBAN format: FIkk BBBB BBCC CCCC CK
                'B = bank code, branch number and account type, C = account No.,
                '     K = check digit of the Finnish account numbering scheme.
                'Last 14 digits are accountno
                sReturnValue = Mid$(sAccount, 5)

            Case "GB"
                'United Kingdom (22) IBAN format: GBkk BBBB SSSS SSCC CCCC CC
                'B = alphabetical bank code, S = sort code (often a specific branch), C = account No
                ' last eight digits are accountno
                sReturnValue = Right$(sAccount, 8)

            Case "NL"
                'NLkk BBBB CCCC CCCC CC
                'The first 4 alphanumeric characters represent a bank
                '   and the last 10 digits an account
                'NL98FTSB0244721734
                sReturnValue = RemoveLeadingCharacters(Mid$(sAccount, 9), "0")

                'XNET - Added "NO"
            Case "NO"
                'NOkk BBBB CCCC CCK
                'B = National bank code
                'C = account No.
                'K = modulo-11 check digit
                sReturnValue = Mid$(sAccount, 5)

        End Select

        ExtractAccountFromIBAN = sReturnValue

        On Error GoTo 0

        Exit Function

ErrorExtractAccountFromIBAN:
        If bShowErrorMessage Then
            Err.Raise(Err.Number, "ErrorExtractAccountFromIBAN", Err.Description)
        Else
            sErrorMessage = Err.Description
            ExtractAccountFromIBAN = False
        End If

    End Function
    'XNET - 16.11.2010 - Changed function name from ExtractAccountFromIBANN to ExtractInfoFromIBAN
    'XNET - Created a new function to extract only accountnumber
    Public Function ExtractInfoFromIBAN(ByVal oPayment As vbBabel.Payment, Optional ByVal bShowErrorMessage As Boolean = True, Optional ByVal sErrorMessage As String = "") As Boolean
        'XNET - Lots of changes in this function - copy all
        'Public Function ExtractAccountFromIBAN(oPayment As Payment, Optional bShowErrorMessage As Boolean = True, Optional sErrorMessage As String = "") As Boolean

        ' Extract accountnumber, Branch, Branchtype and BankCountrycode from IBANN,
        ' and FILL INTO PAYMENT!!!
        Dim sClearingCode As String
        Dim sAccount As String

        On Error GoTo ErrorExtractInfoFromIBAN

        sAccount = oPayment.E_Account
        ' If account starts with /, remove
        If Left$(sAccount, 1) = "/" Then
            sAccount = Mid$(sAccount, 2)
        End If

        ' How to extract is dependant on country
        ' Country always first two chars of IBANN
       
        ' Det riktige her mener jeg er BANK_CountryCode, som er mottakers landkode
        'oPayment.BANK_AccountCountryCode = Left$(sAccount, 2)
        oPayment.BANK_CountryCode = Left$(sAccount, 2)
        'Select Case oPayment.BANK_AccountCountryCode
        Select Case oPayment.BANK_CountryCode

            Case "DE"
                ' Germany (22) IBAN format: DEkk BBBB BBBB CCCC CCCC CC
                ' B = sort code (Bankleitzahl/BLZ), C = account No.
                ' last ten digits are accountno
                oPayment.E_Account = ExtractAccountFromIBAN(sAccount, oPayment.BANK_AccountCountryCode, bShowErrorMessage, sErrorMessage)
                oPayment.BANK_BranchNo = Mid$(sAccount, 5, 8)
                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.Bankleitzahl

            Case "DK"
                'Denmark (18) IBAN format: DKkk BBBB CCCC CCCC CC
                'B = bank No., C = account No.
                'Last 14 digits are accountno
                oPayment.E_Account = ExtractAccountFromIBAN(sAccount, oPayment.BANK_AccountCountryCode, bShowErrorMessage, sErrorMessage)

            Case "FI"
                'Finland (18) IBAN format: FIkk BBBB BBCC CCCC CK
                'B = bank code, branch number and account type, C = account No.,
                '     K = check digit of the Finnish account numbering scheme.
                'Last 14 digits are accountno
                oPayment.E_Account = ExtractAccountFromIBAN(sAccount, oPayment.BANK_AccountCountryCode, bShowErrorMessage, sErrorMessage)

            Case "GB"
                'United Kingdom (22) IBAN format: GBkk BBBB SSSS SSCC CCCC CC
                'B = alphabetical bank code, S = sort code (often a specific branch), C = account No
                ' last eight digits are accountno
                oPayment.E_Account = ExtractAccountFromIBAN(sAccount, oPayment.BANK_AccountCountryCode, bShowErrorMessage, sErrorMessage)
                oPayment.BANK_BranchNo = Mid$(sAccount, 9, 6)
                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode

            Case "NL"
                'NLkk BBBB CCCC CCCC CC
                'The first 4 alphanumeric characters represent a bank
                '   and the last 10 digits an account
                'NL98FTSB0244721734
                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT
                oPayment.E_Account = ExtractAccountFromIBAN(sAccount, oPayment.BANK_AccountCountryCode, bShowErrorMessage, sErrorMessage)

                'XNET - Added "NO"
            Case "NO"
                'NOkk BBBB CCCC CCK
                'B = National bank code
                'C = account No.
                'K = modulo-11 check digit
                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT
                oPayment.E_Account = ExtractAccountFromIBAN(sAccount, oPayment.BANK_AccountCountryCode, bShowErrorMessage, sErrorMessage)


            Case "SE"
                'IBAN format: SEkk BBBB CCCC CCCC CCCC CCCC
                'The Bs represent the bank code and the Cs the account number.

                'Clearingnummer(Bank)
                '1100-1199	Nordea
                '1200-1399	Danske Bank
                '1400-2099	Nordea
                '2300-2309	JP Nordiska
                '2310:   �landsbanken()
                '2311-2399	JP Nordiska
                '2400-2499	Danske Bank
                '2950:   Sambox()
                '3000-3299	Nordea
                '3300:   Nordea(Personkonto)
                '3301-3399	Nordea
                '3400-3409	L�nsf�rs�kringar Bank
                '3410-3781	Nordea
                '3782:   Nordea(Personkonton)
                '3783-4999	Nordea
                '5000-5999	SEB
                '6000-6999	Handelsbanken
                '7000-7120	Swedbank
                '7121-7122	Sparbanken i Enk�ping
                '7123-8104	Swedbank
                '7980:   Swedbank()
                '8079-6	F�rs & Frosta Sparbank
                '8105:   Swedbank()
                '8106-8999	Swedbank
                '8239-6	Snapphanebygdens Sparbank
                '8257:   S�rmlands(Sparbank)
                '8264:   Sparbanken(Nord)
                '8284:   Sala(Sparbank)
                '8304-8	Sparbanken Alings�s
                '8305-5	Sparbanken i Enk�ping
                '8313-9	Sparbanken 1826
                '8383-2	Vadstena Sparbank
                '8393-1	L�nneberga-Tuna-Vena Sparbank
                '8431-9	Westra Wermlands Sparbank
                '9020-9029	L�nsf�rs�kringar Bank
                '9040-9049	Citibank
                '9050-9059	HSB Bank
                '9060-9069	L�nsf�rs�kringar Bank
                '9080:   Calyon(Bank)
                '9090-9099	ABN AMRO
                '9100:   Nordnet(Bank)
                '9120-9124	SEB
                '9130-9149	SEB
                '9150-9169	Skandiabanken
                '9170-9179	Ikano Bank
                '9190-9199	Den Norske Bank
                '9200-9209	Stadshypotek Bank
                '9230:   Bank2()
                '9231-9239	SalusAnsvar Bank
                '9260-9269	Gjensidige NOR Sparebank
                '9270-9279	ICA Banken
                '9280-9289	Resurs Bank
                '9290-9299	Coop Bank
                '9300:   Sparbanken(�resund)
                '9400-9449	Forex Bank
                '9460	GE Money Bank
                '9469	GE Money Bank
                '9500-9547	Plusgirot Bank
                '9548:   Ekobanken()
                '9549:   JAK(Medlemsbank)
                '9550:   Avanza(Bank)
                '9559:   Avanza(Bank)
                '9960-9969	Plusgirot Bank

                'From the EDIFACT documentation from Nordea
                'Bankgiro number minimum 7 and maximum 8 digits. 
                '    Clearing code is not used.
                'PlusGiro Bank account number minimum 2 and maximum 8 digits.
                '    Clearing code is not used.
                'Bank account number Must include a 4 digit clearing code. Minimum
                '    length 11 digits (including clearing code). Maximum length 14 digits
                '    (Payments from PlusGiro) or 16 digits (Payments from Bankgiro). (For
                '    NB Personal accounts (code Personkonto), the clearing is always
                '    �3300�).
                'Note: Exception for payment from PlusGiro to a Swedbank account
                '    where the clearing code starts with 8. Then the clearing code must be 5
                '    digits. Maximum 15 digits including the clearing code.

                oPayment.E_Account = Trim$(oPayment.E_Account)
                sClearingCode = Mid$(oPayment.E_Account, 5, 4)
                If sClearingCode = "6000" Then
                    'Treat Handelsbanken in a special way. Clearing code is NOT stated as a part of the AccountNo in IBAN
                    oPayment.E_Account = sClearingCode & RemoveLeadingCharacters(Mid$(oPayment.E_Account, 9), "0")
                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount
                ElseIf sClearingCode = "8000" Then
                    'Treat Swedbank in a special way. Use the accountno from the IBAN,
                    '  but remove the 5th position.
                    oPayment.E_Account = RemoveLeadingCharacters(Mid$(oPayment.E_Account, 9), "0")
                    '04.12.2019 - linjen to hakk under var feil, ihvertfall for 11 sifrede konti i Swedbank - legger derfor inn en IF
                    If Len(oPayment.E_Account) <> 11 Then
                        'oPayment.E_Account = Left$(oPayment.E_Account, 4) & Mid$(oPayment.E_Account, 6)
                        ' 27.12.2019 - ser ut som om vi skal fjerne ledende 0-er fra kontonr delen, ref case ReiseServiceDeutschland (RSD)
                        oPayment.E_Account = Left$(oPayment.E_Account, 4) & RemoveLeadingCharacters(Mid$(oPayment.E_Account, 6), "0")
                        ' for 11 siffer s� gj�r ingenting
                    End If

                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount
                Else
                    oPayment.E_Account = RemoveLeadingCharacters(Mid$(oPayment.E_Account, 9), "0")
                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount
                End If

                oPayment.E_Account = oPayment.E_Account.Trim
                'If oPayment.PayType = "S" Then
                '    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount
                'Else
                '    If oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.NoAccountType And Not EmptyString(oPayment.E_Account) Then
                '        'Something is stated in the E_Account but what?
                '        'Let's do some qualified guessing
                '        If (InStr(1, oPayment.E_Account, "-", vbTextCompare) > 0) And (Len(oPayment.E_Account) = 9 Or Len(oPayment.E_Account) = 10) Then
                '            oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                '            oPayment.E_Account = Replace(oPayment.E_Account, "-", "", , , vbTextCompare)
                '        ElseIf Len(oPayment.E_Account) = 8 Or Len(oPayment.E_Account) = 9 Then
                '            oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                '        ElseIf Len(oPayment.E_Account) > 8 And Len(oPayment.E_Account) < 15 Then
                '            oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount
                '        Else
                '            oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_PlusGiro
                '        End If
                '    End If
                'End If

        End Select

        ExtractInfoFromIBAN = True
        Exit Function

ErrorExtractInfoFromIBAN:
        If bShowErrorMessage Then
            Err.Raise(Err.Number, "ExtractInfoFromIBAN", Err.Description)
        Else
            sErrorMessage = Err.Description
            ExtractInfoFromIBAN = False
        End If

    End Function
    Sub RaiseInvalidObject()
        'Error # 10001
        Err.Raise(vbObjectError + 10001, , LRS(10001))
    End Sub

    Sub RaiseUnknownErr()
        'Error # 10004
        Err.Raise(vbObjectError + 10004, , LRS(10004))
    End Sub

    'Public Sub MultiDQuickSort(vec, loBound, hiBound, SortField, SortDir)
    ''' ADO Array Sort

    Sub QuickSortADO(ByRef vec As Object, ByRef loBound As Object, ByRef hiBound As Object, ByRef SortField As Object, ByRef SortDir As Object)
        '==--------------------------------------------------------==
        '== Sort a multi dimensional array on SortField            ==
        '==                                                        ==
        '== This procedure is adapted from the algorithm given in: ==
        '==    ~ Data Abstractions & Structures using C++ by ~     ==
        '==    ~ Mark Headington and David Riley, pg. 586    ~     ==
        '== Quicksort is the fastest array sorting routine for     ==
        '== unordered arrays.  Its big O is n log n                ==
        '==                                                        ==
        '== Parameters:                                            ==
        '== vec       - array to be sorted                         ==
        '== SortField - The field to sort on (1st dimension value) ==
        '== loBound and hiBound are simply the upper and lower     ==
        '==   bounds of the array's "row" dimension. It's probably ==
        '==   easiest to use the LBound and UBound functions to    ==
        '==   set these.                                           ==
        '== SortDir   - ASC, ascending; DESC, Descending           ==
        '==--------------------------------------------------------==

        'UPGRADE_WARNING: Couldn't resolve default property of object loBound. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Couldn't resolve default property of object hiBound. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Dim pivot() As Object
        Dim loSwap, hiSwap, counter As Object
        If Not (hiBound - loBound = 0) Then
            ReDim pivot(UBound(vec, 1))
            'UPGRADE_WARNING: Couldn't resolve default property of object SortDir. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            SortDir = UCase(SortDir)

            '== Two items to sort
            'UPGRADE_WARNING: Couldn't resolve default property of object loBound. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object hiBound. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If hiBound - loBound = 1 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object SortDir. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If (SortDir = "ASC") Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare(vec(SortField, hiBound), vec(SortField, loBound)). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare(vec(SortField, loBound), vec(SortField, hiBound)). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If FormatCompare(vec(SortField, loBound), vec(SortField, hiBound)) > FormatCompare(vec(SortField, hiBound), vec(SortField, loBound)) Then Call SwapRowsADO(vec, hiBound, loBound)
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare(vec(SortField, hiBound), vec(SortField, loBound)). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare(vec(SortField, loBound), vec(SortField, hiBound)). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If FormatCompare(vec(SortField, loBound), vec(SortField, hiBound)) < FormatCompare(vec(SortField, hiBound), vec(SortField, loBound)) Then Call SwapRowsADO(vec, hiBound, loBound)
                End If
            End If

            '== Three or more items to sort
            For counter = 0 To UBound(vec, 1)
                'UPGRADE_WARNING: Couldn't resolve default property of object counter. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object hiBound. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object loBound. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object vec(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object pivot(counter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                pivot(counter) = vec(counter, Int((loBound + hiBound) / 2))
                'UPGRADE_WARNING: Couldn't resolve default property of object hiBound. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object loBound. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object vec(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                vec(counter, Int((loBound + hiBound) / 2)) = vec(counter, loBound)
                'UPGRADE_WARNING: Couldn't resolve default property of object counter. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object pivot(counter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object vec(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                vec(counter, loBound) = pivot(counter)
            Next

            'UPGRADE_WARNING: Couldn't resolve default property of object loBound. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object loSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            loSwap = loBound + 1
            'UPGRADE_WARNING: Couldn't resolve default property of object hiBound. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object hiSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            hiSwap = hiBound

            Do
                '== Find the right loSwap
                'UPGRADE_WARNING: Couldn't resolve default property of object SortDir. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If (SortDir = "ASC") Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object SortField. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare(pivot(SortField), vec(SortField, loSwap)). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare(vec(SortField, loSwap), pivot(SortField)). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object hiSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object loSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    While loSwap < hiSwap And FormatCompare(vec(SortField, loSwap), pivot(SortField)) <= FormatCompare(pivot(SortField), vec(SortField, loSwap))
                        'UPGRADE_WARNING: Couldn't resolve default property of object loSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        loSwap = loSwap + 1
                    End While
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object SortField. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare(pivot(SortField), vec(SortField, loSwap)). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare(vec(SortField, loSwap), pivot(SortField)). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object hiSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object loSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    While loSwap < hiSwap And FormatCompare(vec(SortField, loSwap), pivot(SortField)) >= FormatCompare(pivot(SortField), vec(SortField, loSwap))
                        'UPGRADE_WARNING: Couldn't resolve default property of object loSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        loSwap = loSwap + 1
                    End While
                End If
                '== Find the right hiSwap
                'UPGRADE_WARNING: Couldn't resolve default property of object SortDir. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If (SortDir = "ASC") Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object SortField. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare(pivot(SortField), vec(SortField, hiSwap)). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare(vec(SortField, hiSwap), pivot(SortField)). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    While FormatCompare(vec(SortField, hiSwap), pivot(SortField)) > FormatCompare(pivot(SortField), vec(SortField, hiSwap))
                        'UPGRADE_WARNING: Couldn't resolve default property of object hiSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        hiSwap = hiSwap - 1
                    End While
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object SortField. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare(pivot(SortField), vec(SortField, hiSwap)). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare(vec(SortField, hiSwap), pivot(SortField)). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    While FormatCompare(vec(SortField, hiSwap), pivot(SortField)) < FormatCompare(pivot(SortField), vec(SortField, hiSwap))
                        'UPGRADE_WARNING: Couldn't resolve default property of object hiSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        hiSwap = hiSwap - 1
                    End While
                End If
                '== Swap values if loSwap is less then hiSwap
                'UPGRADE_WARNING: Couldn't resolve default property of object hiSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object loSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If loSwap < hiSwap Then Call SwapRowsADO(vec, loSwap, hiSwap)
                'UPGRADE_WARNING: Couldn't resolve default property of object hiSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object loSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Loop While loSwap < hiSwap

            For counter = 0 To UBound(vec, 1)
                'UPGRADE_WARNING: Couldn't resolve default property of object vec(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                vec(counter, loBound) = vec(counter, hiSwap)
                'UPGRADE_WARNING: Couldn't resolve default property of object counter. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object pivot(counter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object vec(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                vec(counter, hiSwap) = pivot(counter)
            Next

            '== Recursively call function .. the beauty of Quicksort
            '== 2 or more items in first section
            'UPGRADE_WARNING: Couldn't resolve default property of object hiSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object hiSwap - 1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object loBound. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object hiSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If loBound < (hiSwap - 1) Then Call QuickSortADO(vec, loBound, hiSwap - 1, SortField, SortDir)
            '== 2 or more items in second section
            'UPGRADE_WARNING: Couldn't resolve default property of object hiSwap. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If hiSwap + 1 < hiBound Then Call QuickSortADO(vec, hiSwap + 1, hiBound, SortField, SortDir)
        End If
    End Sub 'QuickSortADO

    Sub SwapRowsADO(ByRef ary As Object, ByRef row1 As Object, ByRef row2 As Object)
        '==------------------------------------------==
        '== This proc swaps two rows of an array     ==
        '==------------------------------------------==

        Dim x, tempvar As Object
        For x = 0 To UBound(ary, 1)
            'UPGRADE_WARNING: Couldn't resolve default property of object ary(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object tempvar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            tempvar = ary(x, row1)
            'UPGRADE_WARNING: Couldn't resolve default property of object ary(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ary(x, row1) = ary(x, row2)
            'UPGRADE_WARNING: Couldn't resolve default property of object tempvar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object ary(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ary(x, row2) = tempvar
        Next
    End Sub 'SwapRowsADO


    Function FormatCompare(ByRef sOne As Object, ByRef sTwo As Object) As Object
        '==------------------------------------------==
        '==  Checks sOne & sTwo, returns sOne as a   ==
        '==  Numeric if both pass isNumeric, if not  ==
        '==  returns sOne as a string.               ==
        '==------------------------------------------==

        'UPGRADE_WARNING: Couldn't resolve default property of object sTwo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'UPGRADE_WARNING: Couldn't resolve default property of object sOne. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If (IsNumeric(Trim(sOne)) And IsNumeric(Trim(sTwo))) Then
            'UPGRADE_WARNING: Couldn't resolve default property of object sOne. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object FormatCompare. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            FormatCompare = CDbl(Trim(sOne))
        Else
            'UPGRADE_WARNING: Couldn't resolve default property of object sOne. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            FormatCompare = Trim(sOne)
        End If
    End Function

    '---- BrowseForFolderRoutines ----



    Public Function BrowseCallbackProcStr(ByVal hwnd As Integer, ByVal uMsg As Integer, ByVal lParam As Integer, ByVal lpData As Integer) As Integer

        'Callback for the Browse STRING method.

        'On initialization, set the dialog's
        'pre-selected folder from the pointer
        'to the path allocated as bi.lParam,
        'passed back to the callback as lpData param.

        Select Case uMsg
            Case BFFM_INITIALIZED

                Call SendMessage(hwnd, BFFM_SETSELECTIONA, 1, lpData)

            Case Else

        End Select

    End Function


    Public Function BrowseCallbackProc(ByVal hwnd As Integer, ByVal uMsg As Integer, ByVal lParam As Integer, ByVal lpData As Integer) As Integer

        'Callback for the Browse PIDL method.

        'On initialization, set the dialog's
        'pre-selected folder using the pidl
        'set as the bi.lParam, and passed back
        'to the callback as lpData param.
        Select Case uMsg
            Case BFFM_INITIALIZED

                Call SendMessage(hwnd, BFFM_SETSELECTIONA, 0, lpData)

            Case Else

        End Select

    End Function


    Public Function FARPROC(ByRef pfn As Integer) As Integer

        'A dummy procedure that receives and returns
        'the value of the AddressOf operator.

        'This workaround is needed as you can't
        'assign AddressOf directly to a member of a
        'user-defined type, but you can assign it
        'to another long and use that instead!
        FARPROC = pfn

    End Function
    'TODO - REMOVE Old converted function - BrowseForFolder ************************
    'Public Function BrowseForFolderByPath(ByRef sSelpath As String) As String

    '    Dim BI As BROWSEINFO
    '    Dim pidl As Integer
    '    Dim lpSelPath As Integer
    '    Dim sPath As New VB6.FixedLengthString(MAX_PATH)


    '    With BI
    '        '.hOwner = Me.hwnd
    '        .pidlRoot = 0
    '        .lpszTitle = "Pre-selecting folder using the folder's string."
    '        'UPGRADE_WARNING: Add a delegate for AddressOf BrowseCallbackProcStr Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="E9E157F7-EF0C-4016-87B7-7D7FBBC6EE08"'
    '        .lpfn = FARPROC(AddressOf BrowseCallbackProcStr)

    '        lpSelPath = LocalAlloc(lPtr, Len(sSelpath) + 1)
    '        CopyMemory(lpSelPath, sSelpath, Len(sSelpath) + 1)
    '        .lParam = lpSelPath

    '    End With

    '    pidl = SHBrowseForFolder(BI)

    '    If pidl Then

    '        If SHGetPathFromIDList(pidl, sPath.Value) Then
    '            BrowseForFolderByPath = Left(sPath.Value, InStr(sPath.Value, vbNullChar) - 1)
    '        Else
    '            BrowseForFolderByPath = ""
    '        End If

    '        Call CoTaskMemFree(pidl)

    '    Else
    '        BrowseForFolderByPath = ""
    '    End If

    '    Call LocalFree(lpSelPath)

    'End Function

    'OK 31.12.2010 - Browse for Folder - see Endringer.doc, commented function
    'Public Function browseforfilesorfolders(ByRef sSelpath As String, ByRef frm As System.Windows.Forms.Form, Optional ByRef sTitle As String = "", Optional ByRef bPickFiles As Boolean = True) As String
    ' Replaced with common function below:
    Public Function BrowseForFilesOrFolders(ByRef sSelpath As String, ByRef frm As System.Windows.Forms.Form, Optional ByRef sTitle As String = "", Optional ByRef bPickFiles As Boolean = True) As String
        Dim sReturnPath As String
        Dim sSelFile As String = ""

        ' If Open Folders only, then use FolderOpenDialog:
        ' ------------------------------------------------
        If Not bPickFiles Then
            Dim FolderBrowserDialog1 As New FolderBrowserDialog
            FolderBrowserDialog1.SelectedPath = sSelpath
            FolderBrowserDialog1.Description = sTitle
            If FolderBrowserDialog1.ShowDialog = DialogResult.OK Then
                sReturnPath = FolderBrowserDialog1.SelectedPath
            Else
                sReturnPath = ""
            End If
        Else
            ' or if open files, use OpenFileDialog:
            ' -------------------------------------
            Dim openFileDialog1 As New OpenFileDialog()
            ' if passed with both folder & filename, remove filename
            ' Assume that a filename always has ., and a fioldername has not
            If InStr(sSelpath, ".") > 0 Then
                ' Creates problem, by filtering just this one file - sSelFile = System.IO.Path.GetFileName(sSelpath)
                sSelpath = System.IO.Path.GetDirectoryName(sSelpath)
            End If
            openFileDialog1.InitialDirectory = sSelpath
            'openFileDialog1.FileName = sSelFile
            openFileDialog1.Multiselect = False
            openFileDialog1.Title = sTitle
            openFileDialog1.Filter = sSelFile & "|" & sSelFile & "| All files (*.*)|*.*"
            openFileDialog1.FilterIndex = 1
            openFileDialog1.RestoreDirectory = True
            openFileDialog1.CheckFileExists = False
            openFileDialog1.ValidateNames = False

            If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                sReturnPath = openFileDialog1.FileName
            Else
                sReturnPath = ""
            End If
        End If

        BrowseForFilesOrFolders = sReturnPath
        'sSelPath As String) As String

        'Dim BI As BROWSEINFO
        'Dim pidl As Integer
        'Dim sPath As New VB6.FixedLengthString(MAX_PATH)

        'With BI
        '    .hOwner = frm.Handle.ToInt32
        '    .pidlRoot = 0
        '    .lpszTitle = sTitle '"Pre-selecting a folder using the folder's pidl."
        '    'UPGRADE_WARNING: Add a delegate for AddressOf BrowseCallbackProc Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="E9E157F7-EF0C-4016-87B7-7D7FBBC6EE08"'
        '    .lpfn = FARPROC(AddressOf BrowseCallbackProc)
        '    .lParam = GetPIDLFromPath(sSelpath)
        'End With
        ''the type of folder to return.
        'If bPickFiles Then
        '    BI.ulFlags = BIF_BROWSEINCLUDEFILES '+ BIF_EDITBOX  '+ BIF_NEWDIALOGSTYLE
        'Else
        '    BI.ulFlags = BIF_RETURNONLYFSDIRS ' Original !
        'End If

        'pidl = SHBrowseForFolder(BI)

        'If pidl Then
        '    If SHGetPathFromIDList(pidl, sPath.Value) Then
        '        browseforfilesorfolders = Left(sPath.Value, InStr(sPath.Value, vbNullChar) - 1)
        '    Else
        '        browseforfilesorfolders = ""
        '    End If

        '    'free the pidl from SHBrowseForFolder call
        '    Call CoTaskMemFree(pidl)
        'Else
        '    browseforfilesorfolders = ""
        'End If

        ''free the pidl (lparam) from GetPIDLFromPath call
        'Call CoTaskMemFree(BI.lParam)

    End Function


    Private Function GetPIDLFromPath(ByRef sPath As String) As Integer

        'return the pidl to the path supplied by calling the
        'undocumented API #162 (our name for this undocumented
        'function is "SHSimpleIDListFromPath").
        'This function is necessary as, unlike documented APIs,
        'the API is not implemented in 'A' or 'W' versions.

        If IsWinNT() Then
            'UPGRADE_ISSUE: Constant vbUnicode was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
            'NOSA - vbUnicode is constant in VB6, but doesn't exist in .NET
            GetPIDLFromPath = SHSimpleIDListFromPath(StrConv(sPath, VbStrConv.None))
            'GetPIDLFromPath = SHSimpleIDListFromPath(StrConv(sPath, vbUnicode))
        Else
            GetPIDLFromPath = SHSimpleIDListFromPath(sPath)
        End If

    End Function


    Private Function IsWinNT() As Boolean

#If Win32 Then

        Dim osv As OSVERSIONINFO

        osv.OSVSize = Len(osv)

        'API returns 1 if a successful call
        If GetVersionEx(osv) = 1 Then

            'PlatformId contains a value representing
            'the OS; if VER_PLATFORM_WIN32_NT,
            'return true
            IsWinNT = osv.PlatformID = VER_PLATFORM_WIN32_NT
        End If

#End If

    End Function
    ' TODO 31.12.2010 REMOVE not in use
    'Private Function IsValidDrive(ByRef sPath As String) As Boolean

    '    Dim buff As String
    '    Dim nBuffsize As Integer

    '    'Call the API with a buffer size of 0.
    '    'The call fails, and the required size
    '    'is returned as the result.
    '    nBuffsize = GetLogicalDriveStrings(0, buff)

    '    'pad a buffer to hold the results
    '    buff = Space(nBuffsize)
    '    nBuffsize = Len(buff)

    '    'and call again
    '    If GetLogicalDriveStrings(nBuffsize, buff) Then

    '        'if the drive letter passed is in
    '        'the returned logical drive string,
    '        'return True.
    '        IsValidDrive = InStr(1, buff, sPath, CompareMethod.Text) > 0

    '    End If

    'End Function
    ' TODO REMOVE - do not need this 
    'Public Function FixPath(ByRef sPath As String) As String

    'The Browse callback requires the path string
    'in a specific format - trailing slash if a
    'drive only, or minus a trailing slash if a
    'file system path. This routine assures the
    'string is formatted correctly.
    '
    'In addition, because the calls to LocalAlloc
    'requires a valid path for the call to succeed,
    'the path defaults to C:\ if the passed string
    'is empty.

    'Test 1: check for empty string. Since
    'we're setting it we can assure it is
    'formatted correctly, so can bail.
    '   If Len(sPath) = 0 Then
    '      FixPath = "C:\"
    '     Exit Function
    'End If

    'Test 2: is path a valid drive?
    'If this far we did not set the path,
    'so need further tests. Here we ensure
    'the path is properly terminated with
    'a trailing slash as needed.
    '
    'Drives alone require the trailing slash;
    'file system paths must have it removed.
    ''If IsValidDrive(sPath) Then

    'IsValidDrive only determines if the
    'path provided is contained in
    'GetLogicalDriveStrings. Since
    'IsValidDrive() will return True
    'if either C: or C:\ is passed, we
    'need to ensure the string is formatted
    'with the trailing slash.
    'FixPath = QualifyPath(sPath)
    'Else
    'The string passed was not a drive, so
    'assume it's a path and ensure it does
    'not have a trailing space.
    '   FixPath = UnqualifyPath(sPath)
    'End If

    'End Function


    Private Function QualifyPath(ByRef sPath As String) As String

        If Len(sPath) > 0 Then

            If Right(sPath, 1) <> "\" Then
                QualifyPath = sPath & "\"
            Else
                QualifyPath = sPath
            End If

        Else
            QualifyPath = ""
        End If

    End Function


    Private Function UnqualifyPath(ByRef sPath As String) As String

        'Qualifying a path involves assuring that its format
        'is valid, including a trailing slash, ready for a
        'filename. Since SHBrowseForFolder will not pre-select
        'the path if it contains the trailing slash, it must be
        'removed, hence 'unqualifying' the path.
        If Len(sPath) > 0 Then

            If Right(sPath, 1) = "\" Then

                UnqualifyPath = Left(sPath, Len(sPath) - 1)
                Exit Function

            End If

        End If

        UnqualifyPath = sPath

    End Function
    '-------- End of new BrowseForFolders functions -----------------------
    '
    '
    ' Gj�r om fra kort til langt filnavn, brukes ved H�yreklikk, �pen i Babelbank
    'Place in a Module
    'Declare Function GetLongPathName Lib "kernel32" Alias "GetLongPathNameA" (ByVal lpShortPath As String, ByVal lpLongPath As String, ByValnBufferLength As Long) As Long
    Function getlongpath(ByVal CmdLineIn As String) As Object
        If CmdLineIn <> "" Then
            If Left(CmdLineIn, 1) = "'" Or Left(CmdLineIn, 1) = Chr(34) Then CmdLineIn = Right(CmdLineIn, Len(CmdLineIn) - 1)
            If Right(CmdLineIn, 1) = "'" Or Right(CmdLineIn, 1) = Chr(34) Then CmdLineIn = Left(CmdLineIn, Len(CmdLineIn) - 1)
        End If
        ' Get Long Path
        Dim lBuffLen As Integer
        Dim LongPathFile As String
        LongPathFile = Space(260) ' Prepare the long path buffer
        lBuffLen = GetLongPathName(CmdLineIn & Chr(0), LongPathFile, Len(LongPathFile)) ' Get the long path

        If lBuffLen > 0 Then ' If so, then we have a path! If not, the
            'path/file does Not exist
            If lBuffLen > Len(LongPathFile) Then ' Buffer ain't big enough
                LongPathFile = Space(lBuffLen) ' Make buffer large enough
                lBuffLen = GetLongPathName(CmdLineIn & Chr(0), LongPathFile, Len(LongPathFile)) ' And get it again
            End If
            LongPathFile = Trim(Left(LongPathFile, lBuffLen)) ' Trim trailing nulls
        End If


        'UPGRADE_WARNING: Couldn't resolve default property of object getlongpath. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        getlongpath = LongPathFile
    End Function

    Public Function TransformDnBNORReference(ByRef sREF As String) As String
        Dim sChar As String
        Dim iAsc As Short

        On Error GoTo ERR_TransformDnBNORReference

        If EmptyString(sREF) Then
            TransformDnBNORReference = ""
        Else
            sREF = UCase(sREF)

            iAsc = Asc(Left(sREF, 1)) 'A = 65, B = 66

            If iAsc > 64 And iAsc < 91 Then
                sREF = Right(Trim(Str(iAsc - 4)), 1) & Mid(sREF, 2)
            End If

            TransformDnBNORReference = sREF
        End If

        On Error GoTo 0
        Exit Function

ERR_TransformDnBNORReference:

        Err.Raise(Err.Number, "TransformDnBNORReference", Err.Description)
    End Function
    Public Function EmptyString(ByRef sString As String) As Boolean

        If sString Is Nothing Then
            EmptyString = True
        ElseIf sString.Trim.Length = 0 Then
            EmptyString = True
        Else
            EmptyString = False
        End If

        'If Len(Trim(sString)) = 0 Then
        '    EmptyString = True
        'Else
        '    EmptyString = False
        'End If

    End Function
    Public Function EmptyOrNullString(ByRef sString As String) As Boolean

        If Len(Trim(sString)) = 0 Or UCase(sString) = "NULL" Then
            EmptyOrNullString = True
        Else
            EmptyOrNullString = False
        End If

    End Function
    Public Function IsEqualAmount(ByRef d1 As Double, ByRef d2 As Double) As Boolean
        ' Check if to amounts are equal within a fraction of 0.01
        ' 10.10.2008 added = to > below
        If System.Math.Abs(d1 - d2) >= 0.01 Then
            IsEqualAmount = False
        Else
            IsEqualAmount = True
        End If
    End Function
    Public Function IsOCR(ByRef sPayCode As String) As Boolean

        If sPayCode > "509" And sPayCode < "521" Then 'Incoming payments
            IsOCR = True
        Else
            IsOCR = False
        End If

    End Function
    Public Function IsFIK(ByRef sPayCode As String) As Boolean
        Dim bReturnValue As Boolean = False

        Select Case sPayCode
            Case "304", "311", "315", "371", "373", "375"
                bReturnValue = True

            Case Else
                bReturnValue = False

        End Select
        '08.06.2016 - Det ble aldri returnert noen verdi annet enn False !!!
        IsFIK = bReturnValue

    End Function
    Public Function IsAutogiro(ByRef sPayCode As String) As Boolean

        If sPayCode > "610" And sPayCode < "617" Then
            IsAutogiro = True
        Else
            IsAutogiro = False
        End If

    End Function
    Public Function IsKIDPayment(ByRef sPayCode As String) As Boolean

        If IsOCR(sPayCode) Then
            IsKIDPayment = True
        ElseIf IsAutogiro(sPayCode) Then
            IsKIDPayment = True
        ElseIf sPayCode = "180" Then
            IsKIDPayment = True
        Else
            IsKIDPayment = False
        End If

    End Function
    Public Function IsOutgoingKIDPayment(ByRef sPayCode As String) As Boolean
        Dim bReturnValue As Boolean = False

        Select Case sPayCode
            Case "301", "302", "303" 'With KID NO
                bReturnValue = True
            Case "304", "311", "315", "371", "373", "375"
                bReturnValue = True
            Case "331", "332" 'From Bankgiro SE
                bReturnValue = True
            Case "341", "344" 'From Plusgiro SE
                bReturnValue = True

            Case Else
                bReturnValue = False

        End Select

        Return bReturnValue

    End Function
    Public Function IsOutgoingPayment(ByRef sPayCode As String) As Boolean
        ' V�re Paycodes er forh�pentlig slik;
        '   - alt under 500 er utbetalinger
        '   - alt fra 500 og opp er innbetalinger
        Dim bReturnValue As Boolean = False

        If Val(sPayCode) < 500 Then
            IsOutgoingPayment = True
        Else
            IsOutgoingPayment = False
        End If

    End Function
    Public Function IsStructuredPayment(ByRef oPayment As vbBabel.Payment) As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim bReturnValue As Boolean = True

        For Each oInvoice In oPayment.Invoices
            If Not EmptyString(oInvoice.InvoiceNo) Then
                'OK, struct
            ElseIf Not EmptyString(oInvoice.Unique_Id) Then
                'OK, struct
            Else
                bReturnValue = False
                Exit For
            End If
        Next oInvoice

        Return bReturnValue

    End Function

    Public Function IsSwedishDomesticPayType(ByRef sPayCode As String) As Boolean

        If sPayCode > "330" And sPayCode < "361" Then
            IsSwedishDomesticPayType = True
        Else
            IsSwedishDomesticPayType = False
        End If

    End Function
    Public Function ReplaceIllegalCharactersInString(ByRef sString As String, Optional ByRef bExternal As Boolean = False) As String
        Dim sNewString As String

        If Not sString Is Nothing Then
            sNewString = sString

            If bExternal Then
                sNewString = Replace(sNewString, "'", "")
            Else
                '21.08.2017 - Changed the function so we do not add extra characters
                sNewString = Replace(sNewString, "'", Chr(94)) 'CHR(94) = ^
                'Old code
                'sNewString = Replace(sNewString, "'", "@�Q")
            End If

            If bExternal Then
                sNewString = Replace(sNewString, Chr(34), "")
            Else
                '21.08.2017 - Changed the function so we do not add extra characters
                sNewString = Replace(sNewString, Chr(34), Chr(126)) 'CHR(126) = ~
                'Old code
                'sNewString = Replace(sNewString, Chr(34), "�@Q")
            End If
        Else
            sNewString = ""
        End If

        ReplaceIllegalCharactersInString = sNewString

    End Function

    Public Function RetrieveIllegalCharactersInString(ByRef sString As String) As String
        Dim sNewString As String


        If EmptyString(sString) Then
            sNewString = ""
        Else
            sNewString = sString

            '21.08.2017 - Changed the function so we do not add extra characters (see function above)
            sNewString = Replace(sNewString, Chr(94), "'")
            sNewString = Replace(sNewString, Chr(126), Chr(34))
            'TODO - May be deleted in 2019
            sNewString = Replace(sNewString, "@�Q", "'")
            sNewString = Replace(sNewString, "�@Q", Chr(34))
        End If


        RetrieveIllegalCharactersInString = sNewString

    End Function
    Public Function SGFinansCreateAllocArray(ByRef oPayment As vbBabel.Payment, ByRef sErrorMessage As String, ByRef bCalledFromAutoMatch As Boolean, ByRef bCalledFromManualMatch As Boolean, ByRef bCalledFromExport As Boolean, ByRef oMyERPConnection As vbBabel.DAL, ByVal sSGValidateCurrencySQL As String, Optional ByRef bErrorExists As Boolean = False, Optional ByRef iErrorType As Short = 0, Optional ByRef sNegativeClient As String = "") As String(,)
        'DOTNETT REDIM Dim aArray(,) As String
        Dim aArray(,) As String
        Dim aClientArray(,) As String
        Dim oInvoice As vbBabel.Invoice
        Dim bAddNew As Boolean
        Dim j, i, jIndex As Short
        Dim bErrorPossibleToAdjust As Boolean
        Dim sClientNo As String
        Dim sOldClientNo As String
        Dim nAmount As Double
        Dim bx As Boolean
        Dim bShowMessage As Boolean
        Dim sCreditNote As String
        Dim nMatchedInvoices As Double
        'New 14.01.2008
        Dim nGLAmount As Double
        Dim bNegativeAmountExist As Boolean
        Dim sClientNoNegativerAmount As String
        Dim sCustomerNoNegatvieAmount As String
        Dim sTempADAccountNumber As String
        Dim sCurrencyToUse As String
        Dim sMySQL As String

        Dim sDeleteResponse As String

        Try

            '0 - ClientNo
            '1 - MyField 'AD.AgreementNumber
            '2 - CustomerNo (debtor)
            '3 - Amount (ALLOC)
            '4 - Amount (only matched invoices)
            '5 - Undo this matching (1 = Undo all amtching for this client/debtor)
            ' Value 1 = Posted negative amount and not matched against an invoice (On-account)
            ' Value 2 = Negative amount per debtor/client (ALLOC)
            ' Value 3 = Negative total on the items inside an ALLOC. The total of creditnotes is larger than invoices posted.
            '6 -'Amount (only matched invoices, corrected for adjustments)
            'The amount adjusted for adjustments used (when we use a invoice to cover up a creditnote this amount is adjusted
            ' This element of the array is then adjusted
            '7 - SACAmount (ALLOC)
            'New 03.06.2008
            '8 - AD.AccountNumber, If not present, default 001

            'IF bCalledFromAutoMatch = True
            ' - If errors exist, set the payment to proposed matched
            'If bCalledFromManualMatch = True
            ' - If errors exist (type 1 or 3) show message, don't match.
            ' - If errors of type 2 exist, check if OK but don't create any adjustments
            'IF bCalledFromExport = True
            ' - If errors exist (type 1 or 3) undo the payment
            ' - If errors of type 2 exist, check if OK and create adjustments

            'ErrorTypes
            ' 1 = Negativ A-konto
            ' (2 = Negative amounts per debtor/client)
            ' 3 = Summen av posterte kreditnotaer er st�rre enn summen av posterte fakturaer.
            ' 4 = Sum av posterte fakturaer er st�rre en inbetalt bel�p.
            ' 5 = Kan ikke inne et gyldig serviceagreement avtalenummer (001, 002 .....)
            ' 6 = Zero amount
            ' 7 = Error in currency, different between payment currency (Account), and Agreement currency

            j = -1
            ReDim aArray(8, 0)

            'Commented 21.08.2008 next line - not in use
            'ReDim aCreditNotes(1, 0)
            nGLAmount = 0

            sErrorMessage = ""
            bErrorExists = False
            bErrorPossibleToAdjust = False

            If bCalledFromManualMatch Then
                bShowMessage = True 'LSK-Haugesund 1-0
            End If

            For Each oInvoice In oPayment.Invoices
                'New to prevent posting of negative amounts -  - DOKUMENTERT TIL SG
                If bCalledFromManualMatch Then
                    If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount = 0 Then
                        sErrorMessage = sErrorMessage & "Kan ikke postere nullbel�p - Klient: " & oInvoice.MATCH_ClientNo & "  Engasjement: " & oInvoice.CustomerNo
                        'New 03.06.2008
                        If Not EmptyString(oInvoice.InvoiceNo) Then
                            sErrorMessage = sErrorMessage & "  Faktura: " & oInvoice.InvoiceNo & vbCrLf
                        Else
                            sErrorMessage = sErrorMessage & vbCrLf
                        End If
                        bErrorExists = True
                        iErrorType = 6
                    End If
                End If
                'New 27.11.2007 regarding charges added oInvoice.Qualifier <> InvoiceQualifier.NotToBeExported Then
                If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount <> 0 Then 'And oInvoice.Qualifier <> InvoiceQualifier.NotToBeExported Then 'And oInvoice.MATCH_MatchType <> MatchType.MatchedOnGL Then
                    'If the invoice is unmatched, add the unmatched customerNo to the invoice
                    'Removed 28/9-2007 because this information is now set in the function SpecialAfterMatching
                    'If oInvoice.MATCH_Matched = False Then
                    '    ' NoMatchAccount is like 9999999/001/9999999999
                    '    oInvoice.MATCH_ClientNo = xDelim(oInvoice.MATCH_ID, "/", 1)
                    '    oInvoice.CustomerNo = xDelim(oInvoice.MATCH_ID, "/", 3)
                    '    oInvoice.MyField = xDelim(oInvoice.MATCH_ID, "/", 2) & "-"  'AgreementNumber
                    'End If

                    '

                    'New 03.06.2008
                    sTempADAccountNumber = ""
                    If EmptyString(xDelim(oInvoice.MyField, "-", 3)) Then
                        sTempADAccountNumber = "001"
                    Else
                        sTempADAccountNumber = Trim(xDelim(oInvoice.MyField, "-", 3))
                    End If

                    bAddNew = True
                    For i = 0 To UBound(aArray, 2)
                        If oInvoice.CustomerNo = aArray(2, i) Then
                            If oInvoice.MATCH_ClientNo = aArray(0, i) Then
                                If xDelim(oInvoice.MyField, "-", 1) = aArray(1, i) Then
                                    If sTempADAccountNumber = aArray(8, i) Then
                                        bAddNew = False
                                        Exit For
                                    End If
                                End If
                            End If
                        End If
                    Next i
                    If bAddNew Then
                        j = j + 1
                        ReDim Preserve aArray(8, j)
                        aArray(0, j) = oInvoice.MATCH_ClientNo
                        aArray(1, j) = xDelim(oInvoice.MyField, "-", 1)
                        aArray(2, j) = oInvoice.CustomerNo
                        'New 03.06.2008 - DOKUMENTERT TIL SG
                        aArray(8, j) = sTempADAccountNumber
                        If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Or oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                            If EmptyString(xDelim(oInvoice.MyField, "-", 1)) Or Left(Trim(xDelim(oInvoice.MyField, "-", 1)), 1) <> "0" Then
                                If Left(Trim(oInvoice.CustomerNo), 7) <> "9999999" Then
                                    sErrorMessage = sErrorMessage & "Ingen avtaletype (001,002 ...) angitt - Klient: " & aArray(0, j) & "   Engasjement: " & aArray(2, j) & "   Bel�p: " & VB6.Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00") & vbCrLf
                                    bErrorExists = True
                                    iErrorType = 5
                                    aArray(5, j) = "5"
                                End If
                            End If
                        End If
                        If oInvoice.MATCH_MatchType <> vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                            'Don't add invoices matched on GL (adjustments) to the ALLOC-amount
                            aArray(3, j) = Str(oInvoice.MON_InvoiceAmount)
                            If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                                aArray(4, j) = Str(oInvoice.MON_InvoiceAmount)
                            Else
                                aArray(4, j) = "0"
                            End If
                        Else
                            nGLAmount = nGLAmount + oInvoice.MON_InvoiceAmount
                            aArray(3, j) = "0"
                            aArray(4, j) = "0"
                        End If
                    Else
                        If oInvoice.MATCH_MatchType <> vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                            'Don't add invoices matched on GL (adjustments) to the ALLOC-amount
                            aArray(3, i) = Str(oInvoice.MON_InvoiceAmount + Val(aArray(3, i)))
                            If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                                aArray(4, i) = Str(oInvoice.MON_InvoiceAmount + Val(aArray(4, i)))
                            Else
                                aArray(4, i) = Trim(Str(0 + Val(aArray(4, i))))
                            End If
                        Else
                            nGLAmount = nGLAmount + oInvoice.MON_InvoiceAmount
                        End If
                    End If

                    If oInvoice.MATCH_Matched And oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                        If oInvoice.MON_InvoiceAmount < 0 Then

                            If bShowMessage Then
                                '30.10.2019 - Changed from aArray(0, i) to oInvoice.MATCH_ClientNo and aArray(2, i) to oInvoice.CustomerNo
                                'Because we don not have a correct value in the variable i
                                sErrorMessage = sErrorMessage & "Negativ A-konto - Klient: " & oInvoice.MATCH_ClientNo & "   Engasjement: " & oInvoice.CustomerNo & "   Bel�p: " & VB6.Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00") & vbCrLf
                                bErrorExists = True
                                iErrorType = 1
                                '30.10.2019 - Changed from aArray(5, i) to aArray(5, j)
                                'Because we don not have a correct value in the variable i
                                'This is not a good solution because it probably is not the correct Client/Customer
                                aArray(5, j) = "1"
                            Else
                                'New 24.07.2008
                                'Prior to this change no action was taken after the validation.
                                'Now the user is shown a message, and asked to correct the payment
                                sErrorMessage = sErrorMessage & "Negativ A-konto - Klient: " & oInvoice.MATCH_ClientNo & "   Engasjement: " & oInvoice.CustomerNo & "   Bel�p: " & VB6.Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00") & vbCrLf
                                bErrorExists = True
                                iErrorType = 1
                                '30.10.2019 - Changed from aArray(5, i) to aArray(5, j)
                                'Because we don not have a correct value in the variable i
                                'This is not a good solution because it probably is not the correct Client/Customer
                                aArray(5, j) = "1"
                            End If
                        End If
                    End If

                End If
            Next oInvoice

            nMatchedInvoices = 0

            'Dim sMsg As String = ""
            'If oPayment.E_Name.Trim.ToUpper = "TIGER AS" Then
            'MsgBox("Skal presentere array.", MsgBoxStyle.OkOnly)
            'For i = 0 To aArray.GetUpperBound(1)
            'sMsg = sMsg & vbCrLf & aArray(0, i) & " - " & aArray(1, i) & " - " & aArray(2, i) & " - " & aArray(3, i) & " - " & aArray(4, i) & " - " & aArray(5, i) & " - " & aArray(6, i) & " - " & aArray(7, i) & " - " & aArray(8, i) & " - "
            'Next i
            'MsgBox("I CreateAllocArray" & vbCrLf & sMsg, MsgBoxStyle.OkOnly)
            'End If

            'Don't allow negative amounts per client 
            sClientNo = ""
            sOldClientNo = ""
            ReDim aClientArray(1, 0)
            For i = 0 To UBound(aArray, 2)

                bAddNew = True
                If i = 0 Then
                    aClientArray(0, 0) = aArray(0, 0) 'The client
                    aClientArray(1, 0) = aArray(4, 0) 'The amount
                    bAddNew = False
                    'MsgBox("F�rste element =" & vbCrLf & aClientArray(0, 0) & " - " & aClientArray(1, 0), MsgBoxStyle.OkOnly)

                Else
                    For j = 0 To aClientArray.GetUpperBound(1)
                        If aArray(0, i) = aClientArray(0, j) Then
                            aClientArray(1, j) = (Val(aClientArray(1, j)) + Val(aArray(4, i))).ToString 'The amount
                            'MsgBox("Nytt element, verdiene er n� =" & vbCrLf & aClientArray(0, j) & " - " & aClientArray(1, j), MsgBoxStyle.OkOnly)
                            bAddNew = False
                        End If
                    Next j
                End If

                If bAddNew Then
                    j = aClientArray.GetUpperBound(1) + 1
                    ReDim Preserve aClientArray(1, j)
                    aClientArray(0, j) = aArray(0, i) 'The client
                    aClientArray(1, j) = aArray(4, i) 'The amount
                End If

                ''Don't allow a negative total on the items inside an ALLOC
                'If Val(aArray(4, i)) < 0 Then
                '    sErrorMessage = sErrorMessage & "Akonto st�rre enn sum Debtitor/Klient - Klient: " & aArray(0, i) & " Debitor: " & aArray(2, i) & " Bel�p: " &VB6.Format((Val(aArray(3, i)) - Val(aArray(4, i))) / 100, "##,##0.00") & vbCrLf
                '    aArray(5, i) = "3"
                'End If
            Next i
            'Finally, check if some clients have a negative posting
            sNegativeClient = ""
            For i = 0 To aClientArray.GetUpperBound(1)
                If Val(aClientArray(1, i)) < -0.1 Then

                    'MsgBox("Funnet negativ klient, verdier =" & vbCrLf & aClientArray(0, i) & " - " & aClientArray(1, i), MsgBoxStyle.OkOnly)


                    'Write the errormessage later if it isn't possible to adjust this negative client

                    '***********************************************************************************
                    'The errormessage should be removed later!!!!!!!!!!!!!!!!!
                    sErrorMessage = sErrorMessage & vbCrLf & "Kreditnota tilh�rer annen kunde - Kunde: " & aClientArray(0, i) & "   Bel�p: " & VB6.Format(Val(aClientArray(1, i)) / 100, "##,##0.00") & vbCrLf
                    iErrorType = 2
                    bErrorExists = True
                    bErrorPossibleToAdjust = True
                    sNegativeClient = aClientArray(0, i)
                End If
            Next i

            For i = 0 To UBound(aArray, 2)
                'Don't allow negative amounts per debtor/client (ALLOC)
                If Val(aArray(4, i)) < -0.1 Then
                    'Old code
                    'If Val(aArray(3, i)) < -0.1 Then
                    'Write the errormessage later if it isn't possible to adjust this negative client/debtor

                    '***********************************************************************************
                    'The errormessage should be removed later!!!!!!!!!!!!!!!!!
                    'It will be added later on in the function if necassary
                    'sErrorMessage = sErrorMessage & "Negativ Debitor/Klient - Klient: " & aArray(0, i) & "   Debitor: " & aArray(2, i) & "   Bel�p: " &VB6.Format(Val(aArray(3, i)) / 100, "##,##0.00") & vbCrLf
                    aArray(5, i) = "2"
                    iErrorType = 2
                    bErrorExists = True
                    bErrorPossibleToAdjust = True
                End If
                nMatchedInvoices = nMatchedInvoices + Val(aArray(4, i))
                'End If
                aArray(6, i) = aArray(4, i)
            Next i

            'Don't allow a negative total on the invoices matched inside an ALLOC
            If nMatchedInvoices < -0.01 Then
                sErrorMessage = sErrorMessage & "Summen av posterte kreditnotaer er st�rre enn summen av posterte fakturaer."
                'sErrorMessage = sErrorMessage & "Akonto st�rre enn sum Debtitor/Klient - Klient: " & aArray(0, i) & "   Debitor: " & aArray(2, i) & "   Bel�p: " &VB6.Format((Val(aArray(3, i)) - Val(aArray(4, i))) / 100, "##,##0.00") & vbCrLf


                'Must deal with this problem if called from export

                iErrorType = 3
                bErrorExists = True
                'bErrorPossibleToAdjust = True ?
            ElseIf nMatchedInvoices + nGLAmount > oPayment.MON_InvoiceAmount + 0.0001 And bCalledFromAutoMatch Then
                'If nothing more are done then BB will create a negative ALLOC against the observation account
                ' This is not valid
                sErrorMessage = sErrorMessage & "Sum av posterte fakturaer er st�rre en inbetalt bel�p." & vbCrLf & "Dette vil medf�re en negative postering p� observasjonskonto, noe som ikke er lovlig."
                bErrorExists = True
                iErrorType = 4
            End If

            'Added 29.04.2019
            'Validate accountcurrency against currency in AgreementDetails_CA
            '0 - ClientNo
            '4 - Amount (only matched invoices)
            If Not EmptyString(oPayment.MON_InvoiceCurrency) Then
                sCurrencyToUse = oPayment.MON_InvoiceCurrency.Trim
            Else
                sCurrencyToUse = "NOK"
            End If


            '23.04.2020 - Removed this kode - because this is solved in through SQL
            'If sCurrencyToUse <> "NOK" Then
            '    For i = 0 To UBound(aArray, 2)
            '        'Don't allow negative amounts per debtor/client (ALLOC)

            '        If bCalledFromManualMatch Then
            '            sDeleteResponse = InputBox("Skal sjekke f�lgende innbetaling. " & vbCrLf & "Navn: " & oPayment.E_Name & vbCrLf & "Bel�p: " & oPayment.MON_InvoiceAmount.ToString, "TEST", aArray(0, i))
            '            If sDeleteResponse = "" Then
            '                ' user pressed Cancel
            '                Err.Raise(1234, "CreateALLOC", "Program avbrytes")
            '            End If
            '        End If

            '        If Val(aArray(4, i)) > 0 Then

            '            If bCalledFromManualMatch Then
            '                sDeleteResponse = InputBox("Element 4 er st�rre enn 0", "TEST", "KOKO")
            '                If sDeleteResponse = "" Then
            '                    ' user pressed Cancel
            '                    Err.Raise(1234, "CreateALLOC", "Program avbrytes")
            '                End If
            '            End If

            '            If Not EmptyString(aArray(0, i)) Then

            '                If bCalledFromManualMatch Then
            '                    sDeleteResponse = InputBox("Element 0 har verdi: " & aArray(0, i), "TEST", aArray(0, i))

            '                    If sDeleteResponse = "" Then
            '                        ' user pressed Cancel
            '                        Err.Raise(1234, "CreateALLOC", "Program avbrytes")
            '                    End If
            '                End If

            '                sMySQL = sSGValidateCurrencySQL.Replace("BB_MyText", aArray(0, i))
            '                sMySQL = sMySQL.Replace("BB_AgreementNumber", aArray(1, i))
            '                oMyERPConnection.SQL = sMySQL

            '                If bCalledFromManualMatch Then
            '                    sDeleteResponse = InputBox("Skal kj�re f�lgende SQL: " & vbCrLf & sSGValidateCurrencySQL.Replace("BB_MyText", aArray(0, i)), "TEST", "KOKO")
            '                    If sDeleteResponse = "" Then
            '                        ' user pressed Cancel
            '                        Err.Raise(1234, "CreateALLOC", "Program avbrytes")
            '                    End If
            '                End If

            '                If oMyERPConnection.Reader_Execute() Then
            '                    If oMyERPConnection.Reader_HasRows Then
            '                        Do While oMyERPConnection.Reader_ReadRecord
            '                            If oMyERPConnection.Reader_GetString("CurrencyCode") <> sCurrencyToUse Then

            '                                If bCalledFromManualMatch Then
            '                                    sDeleteResponse = InputBox("Avvik i valuta. " & vbCrLf & "Innbetalingsvaluta: " & sCurrencyToUse & vbCrLf & "Avtalevaluta: " & oMyERPConnection.Reader_GetString("CurrencyCode"), "TEST", "KOKO")
            '                                    If sDeleteResponse = "" Then
            '                                        ' user pressed Cancel
            '                                        Err.Raise(1234, "CreateALLOC", "Program avbrytes")
            '                                    End If
            '                                End If

            '                                sErrorMessage = sErrorMessage & "Innbetalingens valuta stemmer ikke overens med CurrencyCode i AgreementDetails_CA."
            '                                bErrorExists = True
            '                                iErrorType = 7

            '                            Else

            '                                If bCalledFromManualMatch Then
            '                                    sDeleteResponse = InputBox("Valuta er OK. " & vbCrLf & "Innbetalingsvaluta: " & sCurrencyToUse & vbCrLf & "Avtalevaluta: " & oMyERPConnection.Reader_GetString("CurrencyCode"), "TEST", "KOKO")
            '                                    If sDeleteResponse = "" Then
            '                                        ' user pressed Cancel
            '                                        Err.Raise(1234, "CreateALLOC", "Program avbrytes")
            '                                    End If
            '                                End If

            '                            End If
            '                        Loop
            '                    Else

            '                        If bCalledFromManualMatch Then
            '                            sDeleteResponse = InputBox("Sp�rringen returnerer ingen rader.", "TEST", "KOKO")
            '                            If sDeleteResponse = "" Then
            '                                ' user pressed Cancel
            '                                Err.Raise(1234, "CreateALLOC", "Program avbrytes")
            '                            End If
            '                        End If

            '                    End If
            '                Else

            '                    If bCalledFromManualMatch Then
            '                        sDeleteResponse = InputBox("Sp�rringen tryner med f�lgende feil: " & vbCrLf & Err.Description, "TEST", "KOKO")
            '                        If sDeleteResponse = "" Then
            '                            ' user pressed Cancel
            '                            Err.Raise(1234, "CreateALLOC", "Program avbrytes")
            '                        End If

            '                    End If
            '                End If
            '            End If
            '        End If
            '    Next i
            'End If

            'If we have errors, try to move creditnotes to an OK debtor/client
            If bErrorExists And bErrorPossibleToAdjust Then
                If bCalledFromAutoMatch Then
                    'Don't do anything
                Else
                    For i = 0 To UBound(aArray, 2)
                        'Check if there is an errormark in the array
                        If aArray(5, i) = "2" Then ' - "2" = Negative posting on this client/debtor
                            nAmount = Val(aArray(4, i)) * -1
                            'sClientNo = aArray(0, i)
                            jIndex = -1
                            'Check if we have some other entries in the array for the same client which is positive
                            For j = 0 To UBound(aArray, 2)
                                'Changed 08.10.2007 -No longer necessary to adjust inside a client
                                'If aArray(0, j) = sClientNo Then
                                'Check if the value is larger than the negative amount in aArray(?, i)
                                If Val(aArray(6, j)) > 0 Then
                                    If i <> j Then
                                        If Val(aArray(6, j)) > nAmount Then 'Val(aArray(3, i)) * -1 Then
                                            'OK, use this Debtor, exit for
                                            jIndex = j
                                            Exit For
                                        Else
                                            nAmount = nAmount - Val(aArray(6, j))
                                        End If
                                    End If
                                End If


                                'End If 'If aArray(0, j) = sClientNo Then
                            Next j
                            If jIndex > -1 Then
                                'Have to adjust this client/debtor aginst another client/debtors

                                'First create adjustments against the original creditnotes

                                If bCalledFromExport Then
                                    sCreditNote = ""
                                    bx = SGFinanceAddInvoice(oPayment, aArray, i, jIndex, 1, sCreditNote)
                                    bx = SGFinanceAddInvoice(oPayment, aArray, i, jIndex, 2, sCreditNote)
                                    'Remove errormark
                                    aArray(5, i) = ""
                                End If
                                'ElseIf nAmount > Val(aArray(3, i)) * -1 Then
                                'Possible to adjust this client/debtor aginst several other client/debtors
                                '    If bCalledFromExport Then
                                '        bx = SGFinanceAddInvoice(oPayment, aArray(), i, jIndex, 1, sCreditNote)
                                '        bx = SGFinanceAddInvoice(oPayment, aArray(), i, jIndex, 2, sCreditNote)
                                '        'Remove errormark
                                '        aArray(5, i) = ""
                                '    End If
                            Else
                                'Can't adjust the payment
                                sErrorMessage = sErrorMessage & "Summen av posterte kreditnotaer er st�rre enn summen av posterte fakturaer."
                                'sErrorMessage = sErrorMessage & "Negativ Debitor/Klient - Klient: " & aArray(0, i) & "   Debitor: " & aArray(2, i) & "   Bel�p: " &VB6.Format(Val(aArray(3, i)) / 100, "##,##0.00") & vbCrLf
                            End If

                        End If
                    Next i
                End If 'IF bCalledFromAutoMatch Then
            End If 'if bErrorExists then

            If Not EmptyString(sErrorMessage) And bCalledFromManualMatch Then
                sErrorMessage = "Det ble funnet ugyldige posteringer:" & vbCrLf & vbCrLf & sErrorMessage
            End If

            '03.11.2009 - Removed Or bCalledFromAutoMatch Then
            If bCalledFromExport Then 'Or bCalledFromAutoMatch Then
                For i = 0 To UBound(aArray, 2)
                    'Check if there is an errormark in the array
                    If aArray(5, i) = "1" Or aArray(5, i) = "2" Or aArray(5, i) = "3" Then ' - "1" = Undo all matching for this client/debtor
                        If bCalledFromExport Then
                            '24.07.2008 - Changed by Kjell
                            'Removed by Kjell, raise an error instead
                            'oPayment.MATCH_Undo
                        Else
                            '24.07.2008 - Don't undo the matching if the payment is proposed matched
                            If oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched Then
                                oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.PartlyMatched
                                For Each oInvoice In oPayment.Invoices
                                    If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount <> 0 Then
                                        If oInvoice.CustomerNo = aArray(2, i) Then
                                            If oInvoice.MATCH_ClientNo = aArray(0, i) Then
                                                If xDelim(oInvoice.MyField, "-", 1) = aArray(1, i) Then
                                                    oInvoice.MATCH_Matched = False
                                                    oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.OpenInvoice
                                                End If
                                            End If
                                        End If
                                    End If
                                Next oInvoice
                            End If
                        End If
                    End If
                Next i
            End If

        Catch ex As Exception

            If bShowMessage Then
                Err.Raise(Err.Number, "SGFinansCreateAllocArray", Err.Description)
            Else
                sErrorMessage = "En uventet feil oppstod i rutinen SGFinansCreateAllocArray: " & vbCrLf & ex.Message
            End If

        End Try

        SGFinansCreateAllocArray = VB6.CopyArray(aArray)

    End Function
    Public Function ValidateNordeaFinance_APTIC_Postings(ByRef oPayment As vbBabel.Payment, ByRef sErrorMessage As String, ByRef bCalledFromAutoMatch As Boolean, ByRef bCalledFromManualMatch As Boolean, ByRef bCalledFromExport As Boolean) As Boolean
        Dim aClientArray(,) As String
        Dim oInvoice As vbBabel.Invoice
        Dim bAddNew As Boolean
        Dim j, i, jIndex As Short
        Dim bPostingOK As Boolean
        Dim bShowMessage As Boolean
        Dim nUnmatchedAmount As Double = 0
        Dim nMatchedAmount As Double = 0

        Try

            'MsgBox("Kommet inn i funksjonen!")

            bPostingOK = True

            If bCalledFromManualMatch Then
                bShowMessage = True
            End If

            i = 0
            ReDim aClientArray(1, 0)
            For Each oInvoice In oPayment.Invoices

                If oInvoice.MATCH_Final Then
                    If oInvoice.MATCH_Matched = True Then
                        nMatchedAmount = nMatchedAmount + oInvoice.MON_InvoiceAmount

                        'MsgBox("Totalsum er n� p�: " & nMatchedAmount.ToString)

                        If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Or oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                            bAddNew = True
                            If i = 0 Then
                                aClientArray(0, 0) = oInvoice.MATCH_ClientNo.Trim 'The client
                                aClientArray(1, 0) = oInvoice.MON_InvoiceAmount.ToString 'The amount
                                bAddNew = False
                                i = i + 1
                                'MsgBox("F�rste element =" & vbCrLf & aClientArray(0, 0) & " - " & aClientArray(1, 0), MsgBoxStyle.OkOnly)

                            Else
                                For j = 0 To aClientArray.GetUpperBound(1)
                                    If oInvoice.MATCH_ClientNo.Trim = aClientArray(0, j) Then
                                        'aClientArray(1, j) = (Val(aClientArray(1, j)) + Val(aArray(4, i))).ToString 'The amount
                                        aClientArray(1, j) = (oInvoice.MON_InvoiceAmount + Val(aClientArray(1, j))).ToString
                                        'MsgBox("Nytt element, verdiene er n� =" & vbCrLf & aClientArray(0, j) & " - " & aClientArray(1, j), MsgBoxStyle.OkOnly)
                                        bAddNew = False
                                    End If
                                Next j
                            End If

                            If bAddNew Then
                                j = aClientArray.GetUpperBound(1) + 1
                                ReDim Preserve aClientArray(1, j)
                                aClientArray(0, j) = oInvoice.MATCH_ClientNo.Trim 'The client
                                aClientArray(1, j) = oInvoice.MON_InvoiceAmount.ToString 'The amount
                                'MsgBox("Heisan, enda et nytt element, verdiene er n� =" & vbCrLf & aClientArray(0, j) & " - " & aClientArray(1, j), MsgBoxStyle.OkOnly)
                            End If

                        End If
                    Else
                        nUnmatchedAmount = nUnmatchedAmount + oInvoice.MON_InvoiceAmount
                    End If
                End If
            Next oInvoice

            'Finally, check if some clients have a negative posting
            'sNegativeClient = ""
            For i = 0 To aClientArray.GetUpperBound(1)
                If Val(aClientArray(1, i)) < -0.1 Then

                    'MsgBox("Funnet negativ klient, verdier =" & vbCrLf & aClientArray(0, i) & " - " & aClientArray(1, i), MsgBoxStyle.OkOnly)


                    'Write the errormessage later if it isn't possible to adjust this negative client

                    '***********************************************************************************
                    'The errormessage should be removed later!!!!!!!!!!!!!!!!!
                    sErrorMessage = sErrorMessage & vbCrLf & "Totalt posteringsbel�p p� en kreditor er negativt." & vbCrLf & "Kreditor: " & aClientArray(0, i) & "   Bel�p: " & VB6.Format(Val(aClientArray(1, i)) / 100, "##,##0.00") & vbCrLf
                    bPostingOK = False
                End If
            Next i

            'MsgBox("TOTALSUM ENDTE P�: " & nMatchedAmount.ToString & vbCrLf & "INNBETALINGEN ER P�: " & oPayment.MON_InvoiceAmount.ToString)
            If nUnmatchedAmount < 0 Then
                sErrorMessage = sErrorMessage & vbCrLf & "Det er ikke lov � f�re et negativt bel�p mot OBS-konto." & vbCrLf & "Kreditor: " & aClientArray(0, i) & "   Bel�p: " & VB6.Format(nUnmatchedAmount / 100, "##,##0.00") & vbCrLf
                bPostingOK = False
            ElseIf nMatchedAmount > oPayment.MON_InvoiceAmount Then
                sErrorMessage = sErrorMessage & vbCrLf & "Det er ikke lov � f�re et negativt bel�p mot OBS-konto." & vbCrLf & "Kreditor: " & aClientArray(0, i) & "   Bel�p: " & VB6.Format(nUnmatchedAmount / 100, "##,##0.00") & vbCrLf
                bPostingOK = False
            End If

            If Not EmptyString(sErrorMessage) And bCalledFromManualMatch Then
                sErrorMessage = "Det ble funnet ugyldige posteringer:" & vbCrLf & vbCrLf & sErrorMessage
            End If


        Catch ex As Exception

            If bShowMessage Then
                Err.Raise(Err.Number, "ValidateNordeaFinance_APTIC_Postings", Err.Description)
            Else
                sErrorMessage = "En uventet feil oppstod i rutinen ValidateNordeaFinance_APTIC_Postings: " & vbCrLf & ex.Message
            End If

            bPostingOK = False

        End Try

        'If bPostingOK Then
        '    MsgBox("bPostingsOK = True!")
        'Else
        '    MsgBox("bPostingsOK = False!")
        'End If
        ValidateNordeaFinance_APTIC_Postings = bPostingOK

    End Function

    Public Function ValidateConecto_APTIC_Postings(ByRef oPayment As vbBabel.Payment, ByRef sErrorMessage As String, ByRef bCalledFromAutoMatch As Boolean, ByRef bCalledFromManualMatch As Boolean, ByRef bCalledFromExport As Boolean) As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim bPostingOK As Boolean
        Dim bShowMessage As Boolean

        Try

            bPostingOK = True

            If bCalledFromManualMatch Then
                bShowMessage = True
            End If

            For Each oInvoice In oPayment.Invoices

                If oInvoice.MATCH_Final Then 'And oInvoice.MATCH_Matched = True Then

                    If oInvoice.MON_InvoiceAmount < 0 Then
                        If Not EmptyString(oInvoice.InvoiceNo) Then
                            If Not EmptyString(oInvoice.CustomerNo) Then
                                sErrorMessage = sErrorMessage & vbCrLf & "Negativ posteringsbel�p er ikke lov." & vbCrLf & "Kundenr.: " & oInvoice.CustomerNo & "Fakturanr.: " & oInvoice.InvoiceNo & vbCrLf & "Bel�p: " & VB6.Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00") & vbCrLf
                            Else
                                sErrorMessage = sErrorMessage & vbCrLf & "Negativ posteringsbel�p er ikke lov." & vbCrLf & "Fakturanr.: " & oInvoice.InvoiceNo & "   Bel�p: " & VB6.Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00") & vbCrLf
                            End If
                        Else
                            If Not EmptyString(oInvoice.CustomerNo) Then
                                sErrorMessage = sErrorMessage & vbCrLf & "Negativ posteringsbel�p er ikke lov." & vbCrLf & "Kundenr.: " & oInvoice.CustomerNo & "   Bel�p: " & VB6.Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00") & vbCrLf
                            Else
                                sErrorMessage = sErrorMessage & vbCrLf & "Negativ posteringsbel�p er ikke lov." & vbCrLf & "Bel�p: " & VB6.Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00") & vbCrLf
                            End If
                        End If
                        bPostingOK = False
                    End If

                End If
            Next oInvoice

            If Not EmptyString(sErrorMessage) And bCalledFromManualMatch Then
                sErrorMessage = "Det ble funnet ugyldige posteringer:" & vbCrLf & vbCrLf & sErrorMessage
            End If


        Catch ex As Exception

            If bShowMessage Then
                Err.Raise(Err.Number, "ValidateConecto_APTIC_Postings", Err.Description)
            Else
                sErrorMessage = "En uventet feil oppstod i rutinen ValidateConecto_APTIC_Postings: " & vbCrLf & ex.Message
            End If

        End Try

        ValidateConecto_APTIC_Postings = bPostingOK

    End Function
    'New function 28.07.2008
    Private Function SGFinanceAddInvoice(ByRef oPayment As vbBabel.Payment, ByRef aArray(,) As String, ByRef iIndexInArrayForNegativeAmount As Short, ByRef iIndexInArrayForPositiveAmount As Short, ByRef iTypeOfInvoiceToAdd As Short, ByRef sCreditNote As String) As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim oNewInvoice As vbBabel.Invoice
        Dim oNewFreetext As vbBabel.Freetext
        Dim nNegativeAmount, nAmountToAdjust As Double
        Dim nRestAmount As Double
        Dim bGetTheFuckOut As Boolean
        Dim iLoopCounter As Short
        Dim lCounter As Integer
        Dim bAdjustTheInvoice As Boolean
        Dim sTempADAccountNumber As String

        'PAY 1000
        '
        'Before - logical way
        'ALLOC -1000
        'KRECON -400
        'KRECON -2100
        'FRECON 1500
        'ALLOC 2000
        'FRECON 2000
        '
        ' After - Aquaius way
        'ALLOC 0
        'KRECON -400
        'KRECON -2100
        'FRECON 1500
        '*ARECON +1000 Case 1 - (both creditnotes adjusted in 1 adjustment)
        'ALLOC 1000
        'FRECON 1000 Case 2
        '*ARECON -1000 Case 2


        Select Case iTypeOfInvoiceToAdd

            Case 1
                'Counterpost against creditnotes

                'Total amount to counterpost
                nRestAmount = Val(aArray(4, iIndexInArrayForNegativeAmount))
                bGetTheFuckOut = False

                For Each oInvoice In oPayment.Invoices

                    If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount < -0.1 Then
                        'It must be matched against a creditnote
                        If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                            'Check if it is the correct debtor
                            If oInvoice.MATCH_ClientNo = aArray(0, iIndexInArrayForNegativeAmount) Then
                                If xDelim(oInvoice.MyField, "-", 1) = aArray(1, iIndexInArrayForNegativeAmount) Then
                                    If oInvoice.CustomerNo = aArray(2, iIndexInArrayForNegativeAmount) Then
                                        'New 03.06.2008
                                        sTempADAccountNumber = ""
                                        If EmptyString(xDelim(oInvoice.MyField, "-", 3)) Then
                                            sTempADAccountNumber = "001"
                                        Else
                                            sTempADAccountNumber = Trim(xDelim(oInvoice.MyField, "-", 3))
                                        End If
                                        If sTempADAccountNumber = aArray(8, iIndexInArrayForNegativeAmount) Then

                                            'OK, we have found a matched creditnote to adjust
                                            'Create an adjustment for this creditnote

                                            'We have to adjust each invoice not as before the total of all creditnotes on this clientdebtor.

                                            If IsEqualAmount(oInvoice.MON_InvoiceAmount, nRestAmount) Then
                                                'We must adjust use the whole invoiceamount
                                                nAmountToAdjust = nRestAmount
                                            ElseIf oInvoice.MON_InvoiceAmount < nRestAmount Then  ' Remember that the amounts we compare is negative
                                                'We must adjust just a part of the invoice
                                                nAmountToAdjust = nRestAmount
                                            Else 'Invoiceamount larger than net for debtorclient
                                                'We must adjust the whole invoice
                                                nAmountToAdjust = oInvoice.MON_InvoiceAmount
                                            End If

                                            If EmptyString(sCreditNote) Then
                                                sCreditNote = "KRE" & oInvoice.InvoiceNo
                                            End If

                                            oNewInvoice = oPayment.Invoices.Add
                                            oNewInvoice.MON_InvoiceAmount = nAmountToAdjust * -1
                                            oNewInvoice.MON_TransferredAmount = nAmountToAdjust * -1
                                            oNewInvoice.MATCH_Final = True
                                            oNewInvoice.MATCH_Matched = True
                                            oNewInvoice.MATCH_Original = False
                                            oNewInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice
                                            oNewInvoice.MATCH_ID = oInvoice.MATCH_ID
                                            oNewInvoice.StatusCode = "02"
                                            oNewInvoice.MATCH_ClientNo = oInvoice.MATCH_ClientNo
                                            oNewInvoice.CustomerNo = oInvoice.CustomerNo
                                            oNewInvoice.InvoiceNo = sCreditNote
                                            oNewInvoice.MyField = oInvoice.MyField & "-" & Trim(Str(SGCreditNoteTakenByDebtorForAnotherAccount))
                                            oNewInvoice.VB_Profile = oPayment.VB_Profile
                                            oNewFreetext = oInvoice.Freetexts.Add
                                            oNewFreetext.Qualifier = 3
                                            oNewFreetext.Text = "Kreditnota brukt mot " & aArray(0, iIndexInArrayForPositiveAmount) & "/" & aArray(2, iIndexInArrayForPositiveAmount)

                                            nRestAmount = nRestAmount - nAmountToAdjust

                                            If nRestAmount > -0.0001 Then

                                                bGetTheFuckOut = True

                                                aArray(6, iIndexInArrayForNegativeAmount) = "0"

                                            End If

                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If

                    If bGetTheFuckOut Then
                        Exit For
                    End If


                Next oInvoice

                bGetTheFuckOut = True

            Case 2
                'Use creditnote against invoice

                'Total amount to counterpost
                nNegativeAmount = Val(aArray(4, iIndexInArrayForNegativeAmount))
                nAmountToAdjust = nNegativeAmount
                bGetTheFuckOut = False

                For iLoopCounter = 1 To 4
                    'First if iLoopCounter = 1 then try to adjust invoices for the same client and the whole amount against 1 invoice
                    ' If iLoopCounter = 2 adjust any invoice for the same client
                    'iLoopCounter = 3 then try to adjust invoices for an other client and the whole amount against 1 invoice
                    ' If iLoopCounter = 2 adjust any invoice for any client

                    For Each oInvoice In oPayment.Invoices

                        If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount > 0.9 Then
                            'It must be matched against an invoice
                            If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                                'Check if it is the correct client
                                If oInvoice.MATCH_ClientNo = aArray(0, iIndexInArrayForNegativeAmount) Or iLoopCounter > 2 Then
                                    If xDelim(oInvoice.MyField, "-", 1) = aArray(1, iIndexInArrayForNegativeAmount) Or iLoopCounter > 2 Then
                                        'Don't adjust invoices which is matched against a clientdebtor where the ALLOC (totalamount)
                                        '  is zero or negative (f.x. the clientdebtor which originally was negative, iIndexInArrayForNegativeAmount in the array)

                                        For lCounter = 0 To UBound(aArray, 2)
                                            If oInvoice.MATCH_ClientNo = aArray(0, lCounter) Then
                                                If xDelim(oInvoice.MyField, "-", 1) = aArray(1, lCounter) Then
                                                    If oInvoice.CustomerNo = aArray(2, lCounter) Then
                                                        'New 03.06.2008
                                                        sTempADAccountNumber = ""
                                                        If EmptyString(xDelim(oInvoice.MyField, "-", 3)) Then
                                                            sTempADAccountNumber = "001"
                                                        Else
                                                            sTempADAccountNumber = Trim(xDelim(oInvoice.MyField, "-", 3))
                                                        End If
                                                        If sTempADAccountNumber = aArray(8, lCounter) Then

                                                            If Val(aArray(6, lCounter)) > 0.1 Then
                                                                bAdjustTheInvoice = True

                                                                If IsEqualAmount(oInvoice.MON_InvoiceAmount, Val(aArray(6, lCounter))) Then
                                                                    'We may use the whole invoiceamount
                                                                    If oInvoice.MON_InvoiceAmount >= nAmountToAdjust * -1 Then
                                                                        ''We are able to post all nAmountToAdjust
                                                                    Else
                                                                        If iLoopCounter = 1 Or iLoopCounter = 3 Then
                                                                            'Must be able to adjust whole amount
                                                                            bAdjustTheInvoice = False
                                                                        Else
                                                                            nAmountToAdjust = oInvoice.MON_InvoiceAmount * -1
                                                                        End If
                                                                    End If
                                                                ElseIf oInvoice.MON_InvoiceAmount < Val(aArray(6, lCounter)) Then
                                                                    'We may use the whole invoiceamount
                                                                    If oInvoice.MON_InvoiceAmount >= nAmountToAdjust * -1 Then
                                                                        ''We are able to post all nAmountToAdjust
                                                                    Else
                                                                        If iLoopCounter = 1 Or iLoopCounter = 3 Then
                                                                            'Must be able to adjust whole amount
                                                                            bAdjustTheInvoice = False
                                                                        Else
                                                                            nAmountToAdjust = oInvoice.MON_InvoiceAmount * -1
                                                                        End If
                                                                    End If
                                                                Else 'Invoiceamount larger than net for debtorclient
                                                                    'We may only use the use the aArray(6, lCounter)-part of the invoiceamount
                                                                    'If we use more, this clientdebtor will be negative

                                                                    If iLoopCounter = 1 Or iLoopCounter = 3 Then
                                                                        'Must be able to adjust whole amount
                                                                        bAdjustTheInvoice = False
                                                                    Else
                                                                        If Val(aArray(6, lCounter)) >= nAmountToAdjust * -1 Then
                                                                            ''We are able to post all nAmountToAdjust
                                                                        Else
                                                                            nAmountToAdjust = Val(aArray(6, lCounter)) * -1
                                                                        End If
                                                                    End If
                                                                End If
                                                                If bAdjustTheInvoice Then
                                                                    Exit For
                                                                End If
                                                            End If
                                                            'We've found the correct clientdebtor, but the net is already zero or negative
                                                            If bAdjustTheInvoice Then
                                                                Exit For
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        Next lCounter

                                        If bAdjustTheInvoice Then
                                            'OK, we have found a matched invoice to adjust
                                            'First reduce matched amount on the original invoice

                                            If IsEqualAmount(oInvoice.MON_InvoiceAmount, nAmountToAdjust * -1) Then
                                                oInvoice.MON_InvoiceAmount = 0
                                                oInvoice.MON_TransferredAmount = 0
                                            Else
                                                oInvoice.MON_InvoiceAmount = oInvoice.MON_InvoiceAmount + nAmountToAdjust '(remember the amount is already negativ, thus +)
                                                oInvoice.MON_TransferredAmount = oInvoice.MON_TransferredAmount + nAmountToAdjust '(remember the amount is already negativ, thus +)
                                            End If
                                            'Create an adjustment
                                            oNewInvoice = oPayment.Invoices.Add
                                            oNewInvoice.MON_InvoiceAmount = nAmountToAdjust
                                            oNewInvoice.MON_TransferredAmount = nAmountToAdjust
                                            oNewInvoice.MATCH_Final = True
                                            oNewInvoice.MATCH_Matched = True
                                            oNewInvoice.MATCH_Original = True
                                            'oNewInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice
                                            oNewInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL
                                            oNewInvoice.MATCH_ID = oInvoice.MATCH_ID
                                            oNewInvoice.StatusCode = "02"
                                            oNewInvoice.MATCH_ClientNo = oInvoice.MATCH_ClientNo
                                            oNewInvoice.CustomerNo = oInvoice.CustomerNo
                                            oNewInvoice.InvoiceNo = sCreditNote
                                            oNewInvoice.MyField = oInvoice.MyField & "-" & Trim(Str(SGCreditNoteUsedFromAnotherDebtorAccount))
                                            oNewInvoice.VB_Profile = oPayment.VB_Profile
                                            oNewFreetext = oInvoice.Freetexts.Add
                                            oNewFreetext.Qualifier = 3
                                            oNewFreetext.Text = "Kreditnota brukt " & aArray(0, iIndexInArrayForNegativeAmount) & "/" & aArray(2, iIndexInArrayForNegativeAmount)

                                            'Reduce the net amount for the clientarray
                                            aArray(6, lCounter) = Trim(Str(Val(aArray(6, lCounter)) + nAmountToAdjust)) 'nAmountToAdjust is already negative, thus +

                                            nNegativeAmount = nNegativeAmount - nAmountToAdjust 'nAmountToAdjust is already negative, thus -
                                            nAmountToAdjust = nNegativeAmount

                                            'Reset value
                                            bAdjustTheInvoice = False

                                            If nAmountToAdjust > -0.1 Then
                                                'That�'s it!
                                                bGetTheFuckOut = True
                                            Else
                                                'We have to find more invoices
                                            End If
                                        End If

                                    End If
                                End If
                            End If
                        End If

                        If bGetTheFuckOut Then
                            Exit For
                        End If

                    Next oInvoice

                    If bGetTheFuckOut Then
                        Exit For
                    End If

                Next iLoopCounter

        End Select

    End Function

    'New function 28.07.2008
    Private Function OLD_SGFinanceAddInvoice(ByRef oPayment As vbBabel.Payment, ByRef aArray(,) As String, ByRef iIndexInArrayForNegativeAmount As Short, ByRef iIndexInArrayForPositiveAmount As Short, ByRef iTypeOfInvoiceToAdd As Short, ByRef sCreditNote As String) As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim oNewInvoice As vbBabel.Invoice
        Dim oNewFreetext As vbBabel.Freetext
        Dim nNegativeAmount, nAmountToAdjust As Double
        Dim nRestAmount As Double
        Dim bGetTheFuckOut As Boolean
        Dim iLoopCounter As Short
        Dim lCounter As Integer
        Dim bAdjustTheInvoice As Boolean
        Dim sTempADAccountNumber As String

        'PAY 1000
        '
        'Before - logical way
        'ALLOC -1000
        'KRECON -400
        'KRECON -2100
        'FRECON 1500
        'ALLOC 2000
        'FRECON 2000
        '
        ' After - Aquaius way
        'ALLOC 0
        'KRECON -400
        'KRECON -2100
        'FRECON 1500
        '*ARECON +1000 Case 1 - (both creditnotes adjusted in 1 adjustment)
        'ALLOC 1000
        'FRECON 1000 Case 2
        '*ARECON -1000 Case 2


        Select Case iTypeOfInvoiceToAdd

            Case 1
                'Counterpost against creditnotes

                'Total amount to counterpost
                nRestAmount = Val(aArray(4, iIndexInArrayForNegativeAmount))
                bGetTheFuckOut = False

                For Each oInvoice In oPayment.Invoices

                    If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount < -0.1 Then
                        'It must be matched against a creditnote
                        If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                            'Check if it is the correct debtor
                            If oInvoice.MATCH_ClientNo = aArray(0, iIndexInArrayForNegativeAmount) Then
                                If xDelim(oInvoice.MyField, "-", 1) = aArray(1, iIndexInArrayForNegativeAmount) Then
                                    If oInvoice.CustomerNo = aArray(2, iIndexInArrayForNegativeAmount) Then
                                        'New 03.06.2008
                                        sTempADAccountNumber = ""
                                        If EmptyString(xDelim(oInvoice.MyField, "-", 3)) Then
                                            sTempADAccountNumber = "001"
                                        Else
                                            sTempADAccountNumber = Trim(xDelim(oInvoice.MyField, "-", 3))
                                        End If
                                        If sTempADAccountNumber = aArray(8, iIndexInArrayForNegativeAmount) Then

                                            'OK, we have found a matched creditnote to adjust
                                            'Create an adjustment for this creditnote

                                            'We have to adjust each invoice not as before the total of all creditnotes on this clientdebtor.

                                            If IsEqualAmount(oInvoice.MON_InvoiceAmount, nRestAmount) Then
                                                'We must adjust use the whole invoiceamount
                                                nAmountToAdjust = nRestAmount
                                            ElseIf oInvoice.MON_InvoiceAmount < nRestAmount Then  ' Remember that the amounts we compare is negative
                                                'We must adjust just a part of the invoice
                                                nAmountToAdjust = nRestAmount
                                            Else 'Invoiceamount larger than net for debtorclient
                                                'We must adjust the whole invoice
                                                nAmountToAdjust = oInvoice.MON_InvoiceAmount
                                            End If

                                            If EmptyString(sCreditNote) Then
                                                sCreditNote = "KRE" & oInvoice.InvoiceNo
                                            End If

                                            oNewInvoice = oPayment.Invoices.Add
                                            oNewInvoice.MON_InvoiceAmount = nAmountToAdjust * -1
                                            oNewInvoice.MON_TransferredAmount = nAmountToAdjust * -1
                                            oNewInvoice.MATCH_Final = True
                                            oNewInvoice.MATCH_Matched = True
                                            oNewInvoice.MATCH_Original = False
                                            oNewInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice
                                            oNewInvoice.MATCH_ID = oInvoice.MATCH_ID
                                            oNewInvoice.StatusCode = "02"
                                            oNewInvoice.MATCH_ClientNo = oInvoice.MATCH_ClientNo
                                            oNewInvoice.CustomerNo = oInvoice.CustomerNo
                                            oNewInvoice.InvoiceNo = sCreditNote
                                            oNewInvoice.MyField = oInvoice.MyField & "-" & Trim(Str(SGCreditNoteTakenByDebtorForAnotherAccount))
                                            oNewInvoice.VB_Profile = oPayment.VB_Profile
                                            oNewFreetext = oInvoice.Freetexts.Add
                                            oNewFreetext.Qualifier = 3
                                            oNewFreetext.Text = "Kreditnota brukt mot " & aArray(0, iIndexInArrayForPositiveAmount) & "/" & aArray(2, iIndexInArrayForPositiveAmount)

                                            nRestAmount = nRestAmount - nAmountToAdjust

                                            If nRestAmount > -0.0001 Then

                                                bGetTheFuckOut = True

                                                aArray(6, iIndexInArrayForNegativeAmount) = "0"

                                            End If

                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If

                    If bGetTheFuckOut Then
                        Exit For
                    End If


                Next oInvoice

                bGetTheFuckOut = True

            Case 2
                'Use creditnote against invoice

                'Total amount to counterpost
                nNegativeAmount = Val(aArray(4, iIndexInArrayForNegativeAmount))
                nAmountToAdjust = nNegativeAmount
                bGetTheFuckOut = False

                For iLoopCounter = 1 To 2
                    'First if iLoopCounter = 1 then try to adjust invoices for the same client
                    ' If iLoopCounter = 2 adjust any invoice
                    For Each oInvoice In oPayment.Invoices

                        If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount > 0.9 Then
                            'It must be matched against an invoice
                            If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                                'Check if it is the correct client
                                If oInvoice.MATCH_ClientNo = aArray(0, iIndexInArrayForNegativeAmount) Or iLoopCounter = 2 Then
                                    If xDelim(oInvoice.MyField, "-", 1) = aArray(1, iIndexInArrayForNegativeAmount) Or iLoopCounter = 2 Then
                                        'Don't adjust invoices which is matched against a clientdebtor where the ALLOC (totalamount)
                                        '  is zero or negative (f.x. the clientdebtor which originally was negative, iIndexInArrayForNegativeAmount in the array)

                                        For lCounter = 0 To UBound(aArray, 2)
                                            If oInvoice.MATCH_ClientNo = aArray(0, lCounter) Then
                                                If xDelim(oInvoice.MyField, "-", 1) = aArray(1, lCounter) Then
                                                    If oInvoice.CustomerNo = aArray(2, lCounter) Then
                                                        'New 03.06.2008
                                                        sTempADAccountNumber = ""
                                                        If EmptyString(xDelim(oInvoice.MyField, "-", 3)) Then
                                                            sTempADAccountNumber = "001"
                                                        Else
                                                            sTempADAccountNumber = Trim(xDelim(oInvoice.MyField, "-", 3))
                                                        End If
                                                        If sTempADAccountNumber = aArray(8, lCounter) Then

                                                            If Val(aArray(6, lCounter)) > 0.1 Then
                                                                If IsEqualAmount(oInvoice.MON_InvoiceAmount, Val(aArray(6, lCounter))) Then
                                                                    'We may use the whole invoiceamount
                                                                    If oInvoice.MON_InvoiceAmount >= nAmountToAdjust * -1 Then
                                                                        ''We are able to post all nAmountToAdjust
                                                                    Else
                                                                        nAmountToAdjust = oInvoice.MON_InvoiceAmount * -1
                                                                    End If
                                                                ElseIf oInvoice.MON_InvoiceAmount < Val(aArray(6, lCounter)) Then
                                                                    'We may use the whole invoiceamount
                                                                    If oInvoice.MON_InvoiceAmount >= nAmountToAdjust * -1 Then
                                                                        ''We are able to post all nAmountToAdjust
                                                                    Else
                                                                        nAmountToAdjust = oInvoice.MON_InvoiceAmount * -1
                                                                    End If
                                                                Else 'Invoiceamount larger than net for debtorclient
                                                                    'We may only use the use the aArray(6, lCounter)-part of the invoiceamount
                                                                    'If we use more, this clientdebtor will be negative
                                                                    If Val(aArray(6, lCounter)) >= nAmountToAdjust * -1 Then
                                                                        ''We are able to post all nAmountToAdjust
                                                                    Else
                                                                        nAmountToAdjust = Val(aArray(6, lCounter)) * -1
                                                                    End If
                                                                End If
                                                                bAdjustTheInvoice = True
                                                                Exit For
                                                            End If
                                                            'We've found the correct clientdebtor, but the net is already zero or negative
                                                            Exit For
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        Next lCounter

                                        If bAdjustTheInvoice Then
                                            'OK, we have found a matched invoice to adjust
                                            'First reduce matched amount on the original invoice

                                            If IsEqualAmount(oInvoice.MON_InvoiceAmount, nAmountToAdjust * -1) Then
                                                oInvoice.MON_InvoiceAmount = 0
                                                oInvoice.MON_TransferredAmount = 0
                                            Else
                                                oInvoice.MON_InvoiceAmount = oInvoice.MON_InvoiceAmount + nAmountToAdjust '(remember the amount is already negativ, thus +)
                                                oInvoice.MON_TransferredAmount = oInvoice.MON_TransferredAmount + nAmountToAdjust '(remember the amount is already negativ, thus +)
                                            End If
                                            'Create an adjustment
                                            oNewInvoice = oPayment.Invoices.Add
                                            oNewInvoice.MON_InvoiceAmount = nAmountToAdjust
                                            oNewInvoice.MON_TransferredAmount = nAmountToAdjust
                                            oNewInvoice.MATCH_Final = True
                                            oNewInvoice.MATCH_Matched = True
                                            oNewInvoice.MATCH_Original = True
                                            oNewInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL
                                            oNewInvoice.MATCH_ID = oInvoice.MATCH_ID
                                            oNewInvoice.StatusCode = "02"
                                            oNewInvoice.MATCH_ClientNo = oInvoice.MATCH_ClientNo
                                            oNewInvoice.CustomerNo = oInvoice.CustomerNo
                                            oNewInvoice.InvoiceNo = sCreditNote
                                            oNewInvoice.MyField = oInvoice.MyField & "-" & Trim(Str(SGCreditNoteUsedFromAnotherDebtorAccount))
                                            oNewInvoice.VB_Profile = oPayment.VB_Profile
                                            oNewFreetext = oInvoice.Freetexts.Add
                                            oNewFreetext.Qualifier = 3
                                            oNewFreetext.Text = "Kreditnota brukt " & aArray(0, iIndexInArrayForNegativeAmount) & "/" & aArray(2, iIndexInArrayForNegativeAmount)

                                            'Reduce the net amount for the clientarray
                                            aArray(6, lCounter) = Trim(Str(Val(aArray(6, lCounter)) + nAmountToAdjust)) 'nAmountToAdjust is already negative, thus +

                                            nNegativeAmount = nNegativeAmount - nAmountToAdjust 'nAmountToAdjust is already negative, thus -
                                            nAmountToAdjust = nNegativeAmount

                                            'Reset value
                                            bAdjustTheInvoice = False

                                            If nAmountToAdjust > -0.1 Then
                                                'That�'s it!
                                                bGetTheFuckOut = True
                                            Else
                                                'We have to find more invoices
                                            End If
                                        End If

                                    End If
                                End If
                            End If
                        End If

                        If bGetTheFuckOut Then
                            Exit For
                        End If

                    Next oInvoice

                    If bGetTheFuckOut Then
                        Exit For
                    End If

                Next iLoopCounter

        End Select

    End Function

    'Old function
    '''''Private Function SGFinanceAddInvoice(oPayment As vbbabel.Payment, aArray() As String, iIndexInArrayForNegativeAmount As Integer, iIndexInArrayForPositiveAmount As Integer, iTypeOfInvoiceToAdd As Integer, sCreditNote As String) As Boolean
    '''''Dim oInvoice As vbbabel.Invoice
    '''''Dim oNewInvoice As vbbabel.Invoice
    '''''Dim oNewFreetext As vbbabel.FreeText
    '''''Dim nNegativeAmount As Double, nAmountToAdjust As Double
    '''''Dim bGetTheFuckOut As Boolean
    '''''Dim iLoopCounter As Integer
    '''''Dim bContinue As Boolean
    '''''Dim lCounter As Long
    '''''Dim bAdjustTheInvoice As Boolean
    '''''Dim sTempADAccountNumber As String
    '''''
    ''''''PAY 1000
    ''''''
    ''''''Before - logical way
    ''''''ALLOC -1000
    ''''''KRECON -400
    ''''''KRECON -2100
    ''''''FRECON 1500
    ''''''ALLOC 2000
    ''''''FRECON 2000
    ''''''
    '''''' After - Aquaius way
    ''''''ALLOC 0
    ''''''KRECON -400
    ''''''KRECON -2100
    ''''''FRECON 1500
    ''''''*ARECON +1000 Case 1 - (both creditnotes adjusted in 1 adjustment)
    ''''''ALLOC 1000
    ''''''FRECON 1000 Case 2
    ''''''*ARECON -1000 Case 2
    '''''
    '''''
    '''''Select Case iTypeOfInvoiceToAdd
    '''''
    '''''Case 1
    '''''    'Counterpost against creditnotes
    '''''
    '''''    'Total amount to counterpost
    '''''    nAmountToAdjust = Val(aArray(4, iIndexInArrayForNegativeAmount))
    '''''    bGetTheFuckOut = False
    '''''
    '''''    For Each oInvoice In oPayment.Invoices
    '''''
    '''''        If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount < -0.1 Then
    '''''            'It must be matched against a creditnote
    '''''            If oInvoice.MATCH_MatchType = MatchType.MatchedOnInvoice Then
    '''''                'Check if it is the correct debtor
    '''''                If oInvoice.MATCH_ClientNo = aArray(0, iIndexInArrayForNegativeAmount) Then
    '''''                    If xDelim(oInvoice.MyField, "-", 1) = aArray(1, iIndexInArrayForNegativeAmount) Then
    '''''                        If oInvoice.CustomerNo = aArray(2, iIndexInArrayForNegativeAmount) Then
    '''''                            'New 03.06.2008
    '''''                            sTempADAccountNumber = ""
    '''''                            If EmptyString(xDelim(oInvoice.MyField, "-", 3)) Then
    '''''                                sTempADAccountNumber = "001"
    '''''                            Else
    '''''                                sTempADAccountNumber = Trim$(xDelim(oInvoice.MyField, "-", 3))
    '''''                            End If
    '''''                            If sTempADAccountNumber = aArray(8, iIndexInArrayForNegativeAmount) Then
    '''''
    '''''                                'OK, we have found a matched creditnote to adjust
    '''''                                'Create only 1 adjustment for this creditnote and
    '''''                                '  any other for this clientdebtor
    '''''
    '''''                                'We don't need to adjust each invoice, just use the
    '''''                                '  net negative amount on the clientdebtor stored in nAmountToAdjust
    '''''
    '''''
    '''''
    '''''
    '''''    '''''                            'Code if we need to adjust each invoice
    '''''    '''''                            If IsEqualAmount(oInvoice.MON_InvoiceAmount, nAmountToAdjust) Then
    '''''    '''''                                'Use any amount
    '''''    '''''                                nAmountToAdjust = oInvoice.MON_InvoiceAmount
    '''''    '''''                            ElseIf oInvoice.MON_InvoiceAmount > nAmountToAdjust Then 'The invoice amount 'less' negative than the AmountToAdjust
    '''''    '''''                                'Use the invoiceamount
    '''''    '''''                                nAmountToAdjust = oInvoice.MON_InvoiceAmount
    '''''    '''''                            Else
    '''''    '''''                                'Use the nAmountToAdjust
    '''''    '''''
    '''''    '''''                                'NOT NECESSARY ??????
    '''''    ''''''''                                'First, reduce the matched creditnote amount
    '''''    ''''''''                                oInvoice.MON_InvoiceAmount = oInvoice.MON_InvoiceAmount - nAmountToAdjust
    '''''    ''''''''                                oInvoice.MON_TransferredAmount = oInvoice.MON_TransferredAmount - nAmountToAdjust
    '''''    '''''                            End If
    '''''
    '''''                                If EmptyString(sCreditNote) Then
    '''''                                    sCreditNote = "KRE" & oInvoice.InvoiceNo
    '''''                                End If
    '''''
    '''''                                Set oNewInvoice = oPayment.Invoices.Add
    '''''                                oNewInvoice.MON_InvoiceAmount = nAmountToAdjust * -1
    '''''                                oNewInvoice.MON_TransferredAmount = nAmountToAdjust * -1
    '''''                                oNewInvoice.MATCH_Final = True
    '''''                                oNewInvoice.MATCH_Matched = True
    '''''                                oNewInvoice.MATCH_Original = False
    '''''                                oNewInvoice.MATCH_MatchType = MatchType.MatchedOnInvoice
    '''''                                oNewInvoice.MATCH_ID = oInvoice.MATCH_ID
    '''''                                oNewInvoice.StatusCode = "02"
    '''''                                oNewInvoice.MATCH_ClientNo = oInvoice.MATCH_ClientNo
    '''''                                oNewInvoice.CustomerNo = oInvoice.CustomerNo
    '''''                                oNewInvoice.InvoiceNo = sCreditNote
    '''''                                oNewInvoice.MyField = oInvoice.MyField & "-" & Trim$(Str(SGCreditNoteTakenByDebtorForAnotherAccount))
    '''''                                Set oNewInvoice.VB_Profile = oPayment.VB_Profile
    '''''                                Set oNewFreetext = oInvoice.Freetexts.Add
    '''''                                oNewFreetext.Qualifier = 3
    '''''                                oNewFreetext.Text = "Kreditnota brukt mot " & aArray(0, iIndexInArrayForPositiveAmount) & "/" & aArray(2, iIndexInArrayForPositiveAmount)
    '''''
    '''''                                bGetTheFuckOut = True
    '''''
    '''''                                aArray(6, iIndexInArrayForNegativeAmount) = "0"
    '''''
    '''''                                'If iNumberofCreditnotesAdjusted > 1 Then
    '''''                                '    ReDim Preserve aCreditNotes(1, iNumberofCreditnotesAdjusted - 1)
    '''''                                'End If
    '''''
    '''''    '''''                            'Code if we need to adjust each invoice
    '''''    '''''                            aArray(6, iIndexInArrayForNegativeAmount) = Trim$(Str(Val(aArray(6, iIndexInArrayForNegativeAmount)) - nAmountToAdjust))
    '''''    '''''                            nAmountToAdjust = Val(aArray(6, iIndexInArrayForNegativeAmount))
    '''''    '''''
    '''''    '''''                            If nAmountToAdjust > -0.1 Then
    '''''    '''''                                'That�'s it!
    '''''    '''''                                bGetTheFuckOut = True
    '''''    '''''                            End If
    '''''                            End If
    '''''                        End If
    '''''                    End If
    '''''                End If
    '''''            End If
    '''''        End If
    '''''
    '''''    If bGetTheFuckOut Then
    '''''        Exit For
    '''''    End If
    '''''
    '''''
    '''''    Next oInvoice
    '''''
    '''''    bGetTheFuckOut = True
    '''''
    '''''Case 2
    '''''    'Use creditnote against invoice
    '''''
    '''''    'Total amount to counterpost
    '''''    nNegativeAmount = Val(aArray(4, iIndexInArrayForNegativeAmount))
    '''''    nAmountToAdjust = nNegativeAmount
    '''''    bGetTheFuckOut = False
    '''''
    '''''    For iLoopCounter = 1 To 2
    '''''        'First if iLoopCounter = 1 then try to adjust invoices for the same client
    '''''        ' If iLoopCounter = 2 adjust any invoice
    '''''        For Each oInvoice In oPayment.Invoices
    '''''
    '''''            If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount > 0.9 Then
    '''''                'It must be matched against an invoice
    '''''                If oInvoice.MATCH_MatchType = MatchType.MatchedOnInvoice Then
    '''''                    'Check if it is the correct client
    '''''                    If oInvoice.MATCH_ClientNo = aArray(0, iIndexInArrayForNegativeAmount) Or iLoopCounter = 2 Then
    '''''                        If xDelim(oInvoice.MyField, "-", 1) = aArray(1, iIndexInArrayForNegativeAmount) Or iLoopCounter = 2 Then
    '''''                            'Don't adjust invoices which is matched against a clientdebtor where the ALLOC (totalamount)
    '''''                            '  is zero or negative (f.x. the clientdebtor which originally was negative, iIndexInArrayForNegativeAmount in the array)
    '''''
    '''''                            For lCounter = 0 To UBound(aArray(), 2)
    '''''                                If oInvoice.MATCH_ClientNo = aArray(0, lCounter) Then
    '''''                                    If xDelim(oInvoice.MyField, "-", 1) = aArray(1, lCounter) Then
    '''''                                        If oInvoice.CustomerNo = aArray(2, lCounter) Then
    '''''                                            'New 03.06.2008
    '''''                                            sTempADAccountNumber = ""
    '''''                                            If EmptyString(xDelim(oInvoice.MyField, "-", 3)) Then
    '''''                                                sTempADAccountNumber = "001"
    '''''                                            Else
    '''''                                                sTempADAccountNumber = Trim$(xDelim(oInvoice.MyField, "-", 3))
    '''''                                            End If
    '''''                                            If sTempADAccountNumber = aArray(8, lCounter) Then
    '''''
    '''''                                                If Val(aArray(6, lCounter)) > 0.1 Then
    '''''                                                    If IsEqualAmount(oInvoice.MON_InvoiceAmount, Val(aArray(6, lCounter))) Then
    '''''                                                        'We may use the whole invoiceamount
    '''''                                                        If oInvoice.MON_InvoiceAmount >= nAmountToAdjust * -1 Then
    '''''                                                            ''We are able to post all nAmountToAdjust
    '''''                                                        Else
    '''''                                                            nAmountToAdjust = oInvoice.MON_InvoiceAmount * -1
    '''''                                                        End If
    '''''                                                    ElseIf oInvoice.MON_InvoiceAmount < Val(aArray(6, lCounter)) Then
    '''''                                                        'We may use the whole invoiceamount
    '''''                                                        If oInvoice.MON_InvoiceAmount >= nAmountToAdjust * -1 Then
    '''''                                                            ''We are able to post all nAmountToAdjust
    '''''                                                        Else
    '''''                                                            nAmountToAdjust = oInvoice.MON_InvoiceAmount * -1
    '''''                                                        End If
    '''''                                                    Else 'Invoiceamount larger than net for debtorclient
    '''''                                                        'We may only use the use the aArray(6, lCounter)-part of the invoiceamount
    '''''                                                        'If we use more, this clientdebtor will be negative
    '''''                                                        If Val(aArray(6, lCounter)) >= nAmountToAdjust * -1 Then
    '''''                                                            ''We are able to post all nAmountToAdjust
    '''''                                                        Else
    '''''                                                            nAmountToAdjust = Val(aArray(6, lCounter)) * -1
    '''''                                                        End If
    '''''                                                    End If
    '''''                                                    bAdjustTheInvoice = True
    '''''                                                    Exit For
    '''''                                                End If
    '''''                                                'We've found the correct clientdebtor, but the net is already zero or negative
    '''''                                                Exit For
    '''''                                            End If
    '''''                                        End If
    '''''                                    End If
    '''''                                End If
    '''''                            Next lCounter
    '''''
    '''''                            If bAdjustTheInvoice Then
    '''''                                'OK, we have found a matched invoice to adjust
    '''''                                'First reduce matched amount on the original invoice
    '''''
    '''''                                If IsEqualAmount(oInvoice.MON_InvoiceAmount, nAmountToAdjust * -1) Then
    '''''                                    oInvoice.MON_InvoiceAmount = 0
    '''''                                    oInvoice.MON_TransferredAmount = 0
    '''''                                Else
    '''''                                    oInvoice.MON_InvoiceAmount = oInvoice.MON_InvoiceAmount + nAmountToAdjust '(remember the amount is already negativ, thus +)
    '''''                                    oInvoice.MON_TransferredAmount = oInvoice.MON_TransferredAmount + nAmountToAdjust '(remember the amount is already negativ, thus +)
    '''''                                End If
    '''''                                'Create an adjustment
    '''''                                Set oNewInvoice = oPayment.Invoices.Add
    '''''                                oNewInvoice.MON_InvoiceAmount = nAmountToAdjust
    '''''                                oNewInvoice.MON_TransferredAmount = nAmountToAdjust
    '''''                                oNewInvoice.MATCH_Final = True
    '''''                                oNewInvoice.MATCH_Matched = True
    '''''                                oNewInvoice.MATCH_Original = True
    '''''                                oNewInvoice.MATCH_MatchType = MatchType.MatchedOnGL
    '''''                                oNewInvoice.MATCH_ID = oInvoice.MATCH_ID
    '''''                                oNewInvoice.StatusCode = "02"
    '''''                                oNewInvoice.MATCH_ClientNo = oInvoice.MATCH_ClientNo
    '''''                                oNewInvoice.CustomerNo = oInvoice.CustomerNo
    '''''                                oNewInvoice.InvoiceNo = sCreditNote
    '''''                                oNewInvoice.MyField = oInvoice.MyField & "-" & Trim$(Str(SGCreditNoteUsedFromAnotherDebtorAccount))
    '''''                                Set oNewInvoice.VB_Profile = oPayment.VB_Profile
    '''''                                Set oNewFreetext = oInvoice.Freetexts.Add
    '''''                                oNewFreetext.Qualifier = 3
    '''''                                oNewFreetext.Text = "Kreditnota brukt " & aArray(0, iIndexInArrayForNegativeAmount) & "/" & aArray(2, iIndexInArrayForNegativeAmount)
    '''''
    '''''                                'Reduce the net amount for the clientarray
    '''''                                aArray(6, lCounter) = Trim$(Str(Val(aArray(6, lCounter)) + nAmountToAdjust)) 'nAmountToAdjust is already negative, thus +
    '''''
    '''''                                nNegativeAmount = nNegativeAmount - nAmountToAdjust 'nAmountToAdjust is already negative, thus -
    '''''                                nAmountToAdjust = nNegativeAmount
    '''''
    '''''                                'Reset value
    '''''                                bAdjustTheInvoice = False
    '''''
    '''''                                If nAmountToAdjust > -0.1 Then
    '''''                                    'That�'s it!
    '''''                                    bGetTheFuckOut = True
    '''''                                Else
    '''''                                    'We have to find more invoices
    '''''                                End If
    '''''                            End If
    '''''
    '''''                        End If
    '''''                    End If
    '''''                End If
    '''''            End If
    '''''
    '''''        If bGetTheFuckOut Then
    '''''            Exit For
    '''''        End If
    '''''
    '''''        Next oInvoice
    '''''
    '''''    If bGetTheFuckOut Then
    '''''        Exit For
    '''''    End If
    '''''
    '''''    Next iLoopCounter
    '''''
    '''''End Select
    '''''
    '''''End Function
    'XokNET - 27.10.2011 - New function
    Public Function ValidateCreditnotes(ByVal oPayment As vbBabel.Payment, ByVal sErrorMessage As String, ByVal bCalledFromAutoMatch As Boolean, ByVal bCalledFromManualMatch As Boolean, ByVal bCalledFromExport As Boolean, Optional ByVal bErrorExists As Boolean = False, Optional ByVal iErrorType As Integer = 0) As String(,)
        Dim aArray(,) As String
        Dim oInvoice As vbBabel.Invoice
        Dim bAddNew As Boolean
        Dim i As Integer, j As Integer, jIndex As Integer
        Dim bErrorPossibleToAdjust As Boolean
        Dim sClientNo As String
        Dim bClientSet As Boolean
        Dim nAmount As Double
        Dim bx As Boolean
        Dim bShowMessage As Boolean
        Dim sCreditNote As String
        Dim nMatchedInvoices As Double
        'New 14.01.2008
        Dim nGLAmount As Double
        Dim bNegativeAmountExist As Boolean
        Dim sClientNoNegativerAmount As String
        Dim sCustomerNoNegatvieAmount As String
        Dim sTempADAccountNumber As String
        Dim bAllEntriesTested As Boolean
        Dim nTotalAmountForClient As Double

        On Error GoTo ERR_ValidateCreditnotes

        '0 - CustomerNo (debtor)
        '1 - Amount (ALLOC)
        '2 - Amount (only matched invoices)
        '3 - Undo this matching (1 = Undo all amtching for this client/debtor)
        ' Value 1 = Posted negative amount and not matched against an invoice (On-account)
        ' Value 2 = Negative amount per debtor/client (ALLOC)
        ' Value 3 = Negative total on the items inside an ALLOC. The total of creditnotes is larger than invoices posted.
        '4 -'Amount (only matched invoices, corrected for adjustments)
        'The amount adjusted for adjustments used (when we use a invoice to cover up a creditnote this amount is adjusted
        ' This element of the array is then adjusted
        '5 - ClientNo (used by TDC)
        '6 - For different purposes

        'IF bCalledFromAutoMatch = True
        ' - If errors exist, set the payment to proposed matched
        'If bCalledFromManualMatch = True
        ' - If errors exist (type 1 or 3) show message, don't match.
        ' - If errors of type 2 exist, check if OK but don't create any adjustments
        'IF bCalledFromExport = True
        ' - If errors exist (type 1 or 3) undo the payment
        ' - If errors of type 2 exist, check if OK and create adjustments

        'ErrorTypes
        ' 1 = Negativ A-konto
        ' 2 = Summen av posterte kreditnotaer er st�rre enn summen av posterte fakturaer.
        ' 3 = Summen av posteringer er negativ for denne kunden.
        '       Dette er ikke en feil da kreditnotaene kan spise av fakturaer p� andre kunder.
        ' 4 = Zero amount
        ' 5 = Summen av alle posteringer p� en kreditor er negativ

        j = -1
        ReDim aArray(6, 0)

        nGLAmount = 0

        sErrorMessage = vbNullString
        bErrorExists = False
        bErrorPossibleToAdjust = False

        If bCalledFromManualMatch Then
            bShowMessage = True
        End If

        For Each oInvoice In oPayment.Invoices
            'New to prevent posting of negative amounts
            If bCalledFromManualMatch Then
                If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount = 0 Then
                    sErrorMessage = sErrorMessage & "Kan ikke postere nullbel�p - Kunde: " & oInvoice.CustomerNo
                    If Not EmptyString(oInvoice.InvoiceNo) Then
                        sErrorMessage = sErrorMessage & "  Faktura: " & oInvoice.InvoiceNo & vbCrLf
                    Else
                        sErrorMessage = sErrorMessage & vbCrLf
                    End If
                    bErrorExists = True
                    iErrorType = 4
                End If
            End If
            If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount <> 0 Then
                bAddNew = True
                For i = 0 To UBound(aArray, 2)
                    If oInvoice.CustomerNo = aArray(0, i) Then
                        If oInvoice.MATCH_ClientNo = aArray(5, i) Then
                            bAddNew = False
                            Exit For
                        End If
                    End If
                Next i
                If bAddNew Then
                    j = j + 1
                    ReDim Preserve aArray(6, j)
                    aArray(0, j) = oInvoice.CustomerNo
                    If oInvoice.MATCH_MatchType <> vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                        'Don't add invoices matched on GL (adjustments) to the ALLOC-amount
                        aArray(1, j) = Str(oInvoice.MON_InvoiceAmount)
                        If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                            aArray(2, j) = Str(oInvoice.MON_InvoiceAmount)
                        Else
                            aArray(2, j) = "0"
                        End If
                    Else
                        nGLAmount = nGLAmount + oInvoice.MON_InvoiceAmount
                        aArray(1, j) = "0"
                        aArray(2, j) = "0"
                    End If
                    aArray(5, j) = oInvoice.MATCH_ClientNo
                Else
                    If oInvoice.MATCH_MatchType <> vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                        'Don't add invoices matched on GL (adjustments) to the ALLOC-amount
                        aArray(1, i) = Str(oInvoice.MON_InvoiceAmount + Val(aArray(1, i)))
                        If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                            aArray(2, i) = Str(oInvoice.MON_InvoiceAmount + Val(aArray(2, i)))
                        Else
                            'aArray(2, i) = Trim$(Str(0 + Val(aArray(2, i))))
                        End If
                    Else
                        nGLAmount = nGLAmount + oInvoice.MON_InvoiceAmount
                    End If
                End If

                If oInvoice.MATCH_Matched And oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                    If oInvoice.MON_InvoiceAmount < 0 Then
                        If bShowMessage Then
                            sErrorMessage = sErrorMessage & "Negativ A-konto - Kunde: " & aArray(0, i) & "   Bel�p: " & Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00") & vbCrLf
                            bErrorExists = True
                            iErrorType = 1
                            aArray(3, i) = "1"
                        Else
                            'New 24.07.2008
                            'Prior to this change no action was taken after the validation.
                            'Now the user is shown a message, and asked to correct the payment
                            sErrorMessage = sErrorMessage & "Negativ A-konto - Kunde: " & aArray(0, i) & "   Bel�p: " & Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00") & vbCrLf
                            bErrorExists = True
                            iErrorType = 1
                            aArray(3, i) = "1"
                        End If
                    End If
                End If

            End If
        Next oInvoice

        nMatchedInvoices = 0

        'Don't allow a negative total on the invoices matched
        If nMatchedInvoices < -0.01 Then
            sErrorMessage = sErrorMessage & vbCrLf & "Summen av posterte kreditposter er st�rre enn summen av posterte fakturaer."

            iErrorType = 2
            bErrorExists = True
            'bErrorPossibleToAdjust = True ?

            '''Kan bli aktuelt
            ''ElseIf nMatchedInvoices + nGLAmount > oPayment.MON_InvoiceAmount + 0.0001 And bCalledFromAutoMatch Then
            ''    'If nothing more are done then BB will create a negative ALLOC against the observation account
            ''    ' This is not valid
            ''    sErrorMessage = sErrorMessage & "Sum av posterte fakturaer er st�rre en inbetalt bel�p." & vbCrLf & "Dette vil medf�re en negative postering p� observasjonskonto, noe som ikke er lovlig."
            ''    bErrorExists = True
            ''    iErrorType = 4
        End If


        'New tests 25.01.2011 per client (for TDC)
        Do While bAllEntriesTested = False
            nTotalAmountForClient = 0
            bClientSet = False
            bAllEntriesTested = True
            For i = 0 To UBound(aArray, 2)
                If aArray(6, i) <> "True" Then
                    bAllEntriesTested = False
                    If Not bClientSet Then
                        sClientNo = aArray(5, i)
                        bClientSet = True
                    End If

                    If aArray(5, i) = sClientNo Then
                        nTotalAmountForClient = nTotalAmountForClient + Val(aArray(2, i))
                        aArray(6, i) = "True"
                    End If

                End If
            Next i
            If nTotalAmountForClient < -0.01 Then
                sErrorMessage = sErrorMessage & vbCrLf & "Summen av posteringer p� kreditor " & sClientNo & " er negativ." & vbCrLf & "Innbetalingen m� omposteres"
                iErrorType = 5
                bErrorExists = True
            End If
        Loop

        'XNET - 29.02.2012 - Added next IF - ASHLEY BARNES
        If Not TreatConectoGLPostings(oPayment, False) Then
            sErrorMessage = sErrorMessage & vbCrLf & "Det finnes ingen fakturaer � f�re hovedbokspostene mot." & vbCrLf & "Innbetalingen m� omposteres"
        End If

        If Not EmptyString(sErrorMessage) Then
            If bCalledFromManualMatch Then
                sErrorMessage = "Det ble funnet ugyldige posteringer:" & vbCrLf & vbCrLf & sErrorMessage
            ElseIf bCalledFromAutoMatch Then
                oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.ProposedMatched
            ElseIf bCalledFromExport Then
                sErrorMessage = "Det ble funnet ugyldige posteringer:" & vbCrLf & vbCrLf & "Betalers navn: " & oPayment.E_Name & _
                  vbCrLf & "Bel�p: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, " ", ",") & sErrorMessage
            End If
        End If


        ValidateCreditnotes = aArray

        Exit Function

ERR_ValidateCreditnotes:

        If bShowMessage Then
            Err.Raise(Err.Number, "ValidateCreditnotes", Err.Description)
        Else
            sErrorMessage = "En uventet feil oppstod i rutinen ValidateCreditnotes: " & vbCrLf & Err.Description
        End If


    End Function

    Public Function Santander_CheckNegativeAmounts(ByRef oPayment As vbBabel.Payment) As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim bReturnValue As Boolean

        On Error GoTo ERR_Santander_CheckNegativeAmounts

        bReturnValue = True

        For Each oInvoice In oPayment.Invoices

            'Find Final invoices with negative amounts
            If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount < -0.1 Then
                'It must be matched as an invoice
                If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Or oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                    bReturnValue = False
                    Exit For
                End If
            End If
        Next oInvoice


        Santander_CheckNegativeAmounts = bReturnValue

        Exit Function

ERR_Santander_CheckNegativeAmounts:

        MsgBox("En uventet feil oppstod i rutinen Santander_CheckNegativeAmounts: " & vbCrLf & Err.Description)
        Santander_CheckNegativeAmounts = False

    End Function
    Public Function Matched_CheckNegativeAmounts(ByRef oPayment As vbBabel.Payment) As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim bReturnValue As Boolean

        On Error GoTo ERR_Matched_CheckNegativeAmounts

        bReturnValue = True

        For Each oInvoice In oPayment.Invoices

            'Find Final invoices with negative amounts
            If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount < -0.1 Then
                'It must be matched as an invoice
                bReturnValue = False
                Exit For
            End If
        Next oInvoice


        Matched_CheckNegativeAmounts = bReturnValue

        Exit Function

ERR_Matched_CheckNegativeAmounts:

        MsgBox("En uventet feil oppstod i rutinen Matched_CheckNegativeAmounts: " & vbCrLf & Err.Description)

        Matched_CheckNegativeAmounts = False

    End Function
    Public Function ACH_Interpret_RMR(ByVal sImportLineBuffer As String, ByRef sInvoiceNo As String, ByRef nAmount As Double, ByRef nAmount2 As Double, ByRef nAmount3 As Double, ByRef nAmount4 As Double, ByRef sText As String, ByRef sText2 As String)
        ' XOKNET 23.05.2012 SEVERAL CHANGES, take whole function
        ' We are checking amounts more thorough, by testing if they are numeric, before continuing.
        ' Then we do not crash when the RMR is illegally setup

        ' Try to extract invoiceno and amount(s) from freetext where the label \RMR is included
        Dim sTmp As String, sTmp2 As String
        Dim otxtFile As Scripting.TextStream
        Dim oFs As Scripting.FileSystemObject
        Dim sFilename As String

        On Error GoTo RMRErr

        ' Startvalues, indicating that we have not receiced amount1-4
        nAmount = -1
        nAmount2 = -1
        nAmount3 = -1
        nAmount4 = -1

        ' RMR Remittance Advice Accounts Receivable Open Item Reference
        ' Try to find invoicenumber
        sTmp = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "\RMR") + 4)
        ' Upto next segment, if more segments in sTmp
        ' added next line 12..08.2008
        If InStr(sTmp, "\") > 0 Then
            sTmp = Left$(sTmp, InStr(sTmp, "\") - 1)
        End If
        'If Left$(sTmp, 4) = "*IV*" Or Left$(sTmp, 4) = "*OI*" Then
        If Left$(sTmp, 1) = "*" And Mid$(sTmp, 4, 1) = "*" Then ' ex *IV* or *OI*

            'If Left$(sTmp, 4) = "*IV*" Then
            '    sText2 = "Invoice No"
            'ElseIf Left$(sTmp, 4) = "*OI*" Then
            '    sText2 = "Original Invoice No"
            'Else
            '    sText2 = "Invoice No"
            'End If

            ' Find code in REFfile;
            oFs = New Scripting.FileSystemObject  '22.05.2017 CreateObject ("Scripting.Filesystemobject")
            sFilename = My.Application.Info.DirectoryPath & "\ACH_Map_REF.sdv"
            If oFs.FileExists(sFilename) Then
                otxtFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForReading, False)
                Do While otxtFile.AtEndOfStream = False
                    sTmp2 = otxtFile.ReadLine()  ' reads one line at a time
                    If Mid$(sTmp, 2, 2) = xDelim(sTmp2, ";", 1) Then
                        ' found the correct code
                        sText2 = xDelim(sTmp2, ";", 2) ' Find the corresponding text for the code, e.g. SW = Seller's Sale Number
                        Exit Do
                    End If
                Loop
            End If
            otxtFile.Close()
            otxtFile = Nothing
            oFs = Nothing

            sTmp = Mid$(sTmp, 5)
            ' 17.08.2009: changed from  > 0 to > 1, and a special blank invoice for < 2,
            ' because there may be no invoiceno (like RMR*IV**PO\ )
            'If InStr(sTmp, "*") > 0 Then
            'If InStr(sTmp, "*") < 2 Then
            ' 13.08.2019 added  InStr(sTmp, "*") > 0 Then
            If InStr(sTmp, "*") < 2 And InStr(sTmp, "*") > 0 Then
                sInvoiceNo = ""
            ElseIf InStr(sTmp, "*") > 1 Then
                ' 17.08.2009: changed from  > 0 to > 1, because there may be no invoiceno (like RMR*IV**PO\ )
                'If InStr(sTmp, "*") > 0 Then
                '1776885*PI*455.97
                'sInvoiceNo = Trim$(Left$(sTmp, InStr(5, sTmp, "*") - 1))
                sInvoiceNo = Trim$(Left$(sTmp, InStr(4, sTmp, "*") - 1))   ' 08.07.2010, changed from 5 to 4, because of exampl ENT*000003 which crashed
            Else
                ' 1776885
                sInvoiceNo = Trim$(sTmp)
            End If
        End If
        ' Try to find paid amount *PI*, *AJ*, etc
        sTmp = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "\RMR") + 4)
        ' Upto next segment
        ' added next line 12..08.2008
        If InStr(sTmp, "\") > 0 Then
            sTmp = Left$(sTmp, InStr(sTmp, "\") - 1)
        End If
        ' Next element is after third *, after InvoicenNo PaymentActionCodes. This may be blank, like **
        sTmp = Mid$(sTmp, InStr(sTmp, sInvoiceNo) + Len(sInvoiceNo))


        ' Payment Action Codes
        ' --------------------
        'AJ Adjustment
        'ER Evaluated Receipts Settlement
        'FL final
        'NS Not Specified (Unknown as to Type of Payment)
        'PA Payment in Advance
        'PI Pay Item
        'PO Payment on Account
        'PP Partial Payment
        'PR Progress Payment
        'OI - unknown !!!

        ' XokNET 16.09.2011 Several places in this function, for all nAmount = ...
        ' From Val( etc to ConvertToAmount( etc
        '
        ' Added 22.01.2008
        ' Can be **, without no Payment Action Code
        If Left$(sTmp, 2) = "**" Then
            sText = ""
            ' 455.97
            sTmp = Mid$(sTmp, InStr(sTmp, "**") + 2)
            If InStr(sTmp, "*") > 0 Then
                '455.97*XX*\REF*6W*E2606J1694104I\SE*10*605600001\GE*1*60560\IEA00061419702
                sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)
                'nAmount = Val(Replace(Replace(sTmp, ".", ""), ",", ""))
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            Else
                ' 455.97
                'nAmount = Val(Replace(Replace(sTmp, ".", ""), ",", ""))
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            End If
        End If
        If InStr(sTmp, "*AJ*") > 0 Then
            sText = "Adjustment"
            ' 455.97
            sTmp = Mid$(sTmp, InStr(sTmp, "*AJ*") + 4)
            If InStr(sTmp, "*") > 0 Then
                '455.97*XX*\REF*6W*E2606J1694104I\SE*10*605600001\GE*1*60560\IEA00061419702
                sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If

            Else
                ' 455.97
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            End If
        End If
        If InStr(sTmp, "*ER*") > 0 Then
            sText = "Evaluated Receipts Settlement"
            ' 455.97
            sTmp = Mid$(sTmp, InStr(sTmp, "*ER*") + 4)
            If InStr(sTmp, "*") > 0 Then
                '455.97*XX*\REF*6W*E2606J1694104I\SE*10*605600001\GE*1*60560\IEA00061419702
                sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            Else
                ' 455.97
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            End If
        End If
        If InStr(sTmp, "*FL*") > 0 Then
            sText = "Final"
            ' 455.97
            sTmp = Mid$(sTmp, InStr(sTmp, "*FL*") + 4)
            If InStr(sTmp, "*") > 0 Then
                '455.97*XX*\REF*6W*E2606J1694104I\SE*10*605600001\GE*1*60560\IEA00061419702
                sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            Else
                ' 455.97
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            End If
        End If
        If InStr(sTmp, "*NS*") > 0 Then
            sText = "Not Specified (Unknown as to Type of Payment)"
            ' 455.97
            sTmp = Mid$(sTmp, InStr(sTmp, "*NS*") + 4)
            If InStr(sTmp, "*") > 0 Then
                '455.97*XX*\REF*6W*E2606J1694104I\SE*10*605600001\GE*1*60560\IEA00061419702
                sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            Else
                ' 455.97
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            End If
        End If
        If InStr(sTmp, "*PA*") > 0 Then
            sText = "Payment in Advance"
            ' 455.97
            sTmp = Mid$(sTmp, InStr(sTmp, "*PA*") + 4)
            If InStr(sTmp, "*") > 0 Then
                '455.97*XX*\REF*6W*E2606J1694104I\SE*10*605600001\GE*1*60560\IEA00061419702
                sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            Else
                ' 455.97
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            End If
        End If
        If InStr(sTmp, "*PI*") > 0 Then
            sText = "Pay Item"
            ' 455.97
            sTmp = Mid$(sTmp, InStr(sTmp, "*PI*") + 4)
            If InStr(sTmp, "*") > 0 Then
                '455.97*XX*\REF*6W*E2606J1694104I\SE*10*605600001\GE*1*60560\IEA00061419702
                sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            Else
                ' 455.97
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            End If
        End If
        If InStr(sTmp, "*PO*") > 0 Then
            sText = "Payment On Account"
            ' 455.97
            sTmp = Mid$(sTmp, InStr(sTmp, "*PO*") + 4)
            If InStr(sTmp, "*") > 0 Then
                '455.97*XX*\REF*6W*E2606J1694104I\SE*10*605600001\GE*1*60560\IEA00061419702
                sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            Else
                ' 455.97
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            End If
        End If
        If InStr(sTmp, "*PP*") > 0 Then
            sText = "Partial Payment"
            ' 455.97
            sTmp = Mid$(sTmp, InStr(sTmp, "*PP*") + 4)
            If InStr(sTmp, "*") > 0 Then
                '455.97*XX*\REF*6W*E2606J1694104I\SE*10*605600001\GE*1*60560\IEA00061419702
                sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            Else
                ' 455.97
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            End If
        End If
        If InStr(sTmp, "*PR*") > 0 Then
            sText = "Progress Payment"
            ' 455.97
            sTmp = Mid$(sTmp, InStr(sTmp, "*PR*") + 4)
            If InStr(sTmp, "*") > 0 Then
                '455.97*XX*\REF*6W*E2606J1694104I\SE*10*605600001\GE*1*60560\IEA00061419702
                sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            Else
                ' 455.97
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,") '
                Else
                    nAmount = 0
                End If
            End If
        End If
        ' xoknet 24.11.2011 ADDED AI
        If InStr(sTmp, "*AI*") > 0 Then
            sText = "AI"
            ' 455.97
            sTmp = Mid$(sTmp, InStr(sTmp, "*AI*") + 4)
            If InStr(sTmp, "*") > 0 Then
                '455.97*XX*\REF*6W*E2606J1694104I\SE*10*605600001\GE*1*60560\IEA00061419702
                sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            Else
                ' 455.97
                ' 23.05.2012 Make it more robust by checking if amountpart is numeric here;
                If vbIsNumeric(sTmp) Then
                    nAmount = ConvertToAmount(sTmp, vbNullString, ".,")
                Else
                    nAmount = 0
                End If
            End If
        End If
        ' XokNET 24.11.2011 - added next comment;
        ' nAmount is the invoice amount. Use this one if specified.
        ' This is RMR04
        ' If NOT RMR04 is specified, then use the amounts below;
        ' nAmount2 = RMR05, Amount of Invoice
        ' nAmount3 = RMR06, Deduction amount (discount or adjustment)
        ' nAmount4 = RMR07, Interest penalty amount

        ' added 13.12.2007
        ' there may be upto 4 amounts specified


        ' Upto next segment
        sTmp = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "\RMR") + 4)
        ' added next line 12..08.2008
        If InStr(sTmp, "\") > 0 Then
            sTmp = Left$(sTmp, InStr(sTmp, "\") - 1)  ' Extracts the complete RMR, upto next segment
        End If

        ' Amount2, Amount of Invoice , after fith *
        If Not EmptyString(xDelim(sTmp, "*", 6)) Then
            'nAmount2 = Val(Replace(Replace(xDelim(sTmp, "*", 6), ".", ""), ",", ""))
            ' 23.05.2012
            ' Make it more robust by checking if amountpart is numeric here;
            If vbIsNumeric(xDelim(sTmp, "*", 6)) Then
                nAmount2 = ConvertToAmount(xDelim(sTmp, "*", 6), vbNullString, ".,")
            Else
                nAmount2 = 0 ' to be able to continue without bombing
            End If
        End If

        ' Amount3, Amount of Discount or Adjustment , after sixth *
        If Not EmptyString(xDelim(sTmp, "*", 7)) Then
            ' 23.05.2012
            ' Make it more robust by checking if amountpart is numeric here;
            If vbIsNumeric(xDelim(sTmp, "*", 7)) Then
                nAmount3 = ConvertToAmount(xDelim(sTmp, "*", 7), vbNullString, ".,")
            Else
                nAmount3 = 0
            End If
        End If

        ' Amount4, Interest Penality Amount , after seventh *
        If Not EmptyString(xDelim(sTmp, "*", 8)) Then
            ' 23.05.2012
            ' Make it more robust by checking if amountpart is numeric here;
            If vbIsNumeric(xDelim(sTmp, "*", 8)) Then
                nAmount4 = ConvertToAmount(xDelim(sTmp, "*", 8), vbNullString, ".,")
            Else
                nAmount4 = 0
            End If
        End If



        ' Consider extracting specialcase with only ** ahead of amount.
        ' OK,lets do it
        'If InStr(sTmp, "**") > 0 Then
        '    ' 455.97
        '    sTmp = Mid$(sTmp, InStr(sTmp, "**") + 2)
        '    If InStr(sTmp, "*") > 0 Then
        '        '455.97*XX*\REF*6W*E2606J1694104I\SE*10*605600001\GE*1*60560\IEA00061419702
        '        sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)
        '        nAmount = Val(Replace(Replace(sTmp, ".", ""), ",", ""))
        '    Else
        '        ' 455.97
        '        nAmount = Val(Replace(Replace(sTmp, ".", ""), ",", ""))
        '    End If
        'End If
        Exit Function

RMRErr:
        sInvoiceNo = ""
        nAmount = 0
        nAmount2 = 0
        nAmount3 = 0
        nAmount4 = 0
        sText = ""
        sText2 = ""

    End Function
    Public Function ACH_Interpret_REF(ByVal sImportLineBuffer As String, ByRef sCode As String, ByRef sREF As String)
        ' Try to extact a special Code qualifying the Reference Identification (sTmp) and the reference itself (sTmp2)
        ' \REF*6W*E2651J3413290I
        Dim sTmp As String, sTmp2 As String
        Dim otxtFile As Scripting.TextStream
        Dim oFs As Scripting.FileSystemObject
        Dim sFilename As String

        ' XOKNET 23.05.2012
        On Error GoTo REFErr

        ' Try to find code. Must lookup in a .csv-file
        sTmp = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "\REF") + 5) ' gets 6W*E2651J3413290I
        ' Upto next *

        'new code NYRANGERS 11.09.2008 added next IF, sometimes there are no asterix in the string, and that will cause an error
        If InStr(sTmp, "*") > 0 Then
            sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)  ' gives SW
        End If

        ' sometimes, there are no codes in a REF, thus
        If Not EmptyString(sTmp) Then
            ' Find code in file;
            oFs = New Scripting.FileSystemObject  '22.05.2017 CreateObject ("Scripting.Filesystemobject")
            sFilename = My.Application.Info.DirectoryPath & "\ACH_Map_REF.sdv"
            If oFs.FileExists(sFilename) Then
                otxtFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForReading, False)
                Do While otxtFile.AtEndOfStream = False
                    sTmp2 = otxtFile.ReadLine()  ' reads one line at a time
                    If sTmp = xDelim(sTmp2, ";", 1) Then
                        ' found the correct code
                        sCode = xDelim(sTmp2, ";", 2) ' Find the corresponding text for the code, e.g. SW = Seller's Sale Number
                        Exit Do
                    End If
                Loop
                otxtFile.Close()
            End If
            otxtFile = Nothing
            oFs = Nothing
            ' Try to find the reference itself, E2651J3413290I in the example
            sTmp = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "\REF") + 5) ' gets 6W*E2651J3413290I ++ a lot of other stuff
            ' find after *
            sREF = Mid$(sTmp, InStr(sTmp, "*") + 1)
            ' find upto next \ or next *
            If InStr(sREF, "\") > 0 Then
                sREF = Left$(sREF, InStr(sREF, "\") - 1)
            End If
            If InStr(sREF, "*") > 0 Then
                sREF = Left$(sREF, InStr(sREF, "*") - 1)
            End If
            If EmptyString(sREF) Then
                ' may be no code, but a description, like
                ' \REF*ZZ**00000000000000000�BNR (DO NOT REINVOICE)\
                sREF = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "\REF") + 5) ' gets ZZ**00000000000000000�BNR (DO NOT REINVOICE)\ ++ a lot of other stuff
                If InStr(sREF, "\") > 0 Then
                    sREF = Left$(sREF, InStr(sREF, "\") - 1)
                End If
                If InStr(sREF, "**") > 0 Then
                    sREF = Mid$(sREF, InStr(sREF, "**") + 2)
                End If
            End If




        Else
            ' ref only
            sREF = sTmp
        End If

        ' XOKNET 23.05.2012
        Exit Function

REFErr:
        sREF = ""
        sCode = ""

    End Function
    Public Function ACH_Interpret_NTE(ByVal sImportLineBuffer As String, ByRef sCode As String, ByRef sText As String)
        ' Try to extact a special Code and the freetext itself (sTmp2)
        ' NTE*ALL*ACCOUNT [00117489*\
        Dim sTmp As String, sTmp2 As String
        Dim otxtFile As Scripting.TextStream
        Dim oFs As Scripting.FileSystemObject
        Dim sFilename As String

        ' XOKNET 23.05.2012
        On Error GoTo NTEErr

        ' Try to find code. Must lookup in a .csv-file
        sTmp = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "NTE*") + 4) ' gets ALL*ACCOUNT: 00117489 *\
        If InStr(sTmp, "*") > 0 Then
            ' Upto next *
            sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)  ' gives ALL

            ' Try to find the freetext itself, ACCOUNT: 00117489 in the example
            sTmp2 = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "NTE*") + 4) ' gets ALL*ACCOUNT: 00117489  ++ a lot of other stuff
            ' find after *
            sText = Mid$(sTmp2, InStr(sTmp2, "*") + 1)
            ' find upto next \ or next *
            If InStr(sText, "\") > 0 Then
                sText = Left$(sText, InStr(sText, "\") - 1)
            End If
            If InStr(sText, "*") > 0 Then
                sText = Left$(sText, InStr(sText, "*") - 1)
            End If

            ' added 2008.03.21, have a file which reads NTE*INV[ 235320\, then check for [
        ElseIf InStr(sTmp, "[") > 0 Then
            ' Upto next [
            sTmp = Left$(sTmp, InStr(sTmp, "[") - 1)  ' gives INV
            ' Try to find the freetext itself, ACCOUNT: 00117489 in the example
            sTmp2 = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "NTE*") + 4) ' gets ALL*ACCOUNT: 00117489  ++ a lot of other stuff

            ' find after [
            sText = Mid$(sTmp2, InStr(sTmp2, "[") + 1)
            ' find upto next \ or next [
            If InStr(sText, "\") > 0 Then
                sText = Left$(sText, InStr(sText, "\") - 1)
            End If
            If InStr(sText, "[") > 0 Then
                sText = Left$(sText, InStr(sText, "[") - 1)
            End If
        Else
            ' no * and no [, take next three after NTE*
            sTmp = Left$(sTmp, 3)
            ' Try to find the freetext itself, ACCOUNT: 00117489 in the example
            sTmp2 = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "NTE*") + 4) ' gets ALL*ACCOUNT: 00117489  ++ a lot of other stuff

            ' find after INV etc
            sText = Mid$(sTmp2, 4)
            ' find upto next \
            If InStr(sText, "\") > 0 Then
                sText = Left$(sText, InStr(sText, "\") - 1)
            End If
        End If
        ' sometimes, there are no codes in a NTE, thus
        If Not EmptyString(sTmp) Then
            ' Find code in file;
            oFs = New Scripting.FileSystemObject  '22.05.2017 CreateObject ("Scripting.Filesystemobject")
            sFilename = My.Application.Info.DirectoryPath & "\ACH_Map_NTE.sdv"
            If oFs.FileExists(sFilename) Then
                otxtFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForReading, False)
                Do While otxtFile.AtEndOfStream = False
                    sTmp2 = otxtFile.ReadLine()  ' reads one line at a time
                    If sTmp = xDelim(sTmp2, ";", 1) Then
                        ' found the correct code
                        sCode = xDelim(sTmp2, ";", 2) ' Find the corresponding text for the code, e.g. ALL = All Documents
                        Exit Do
                    End If
                Loop
            End If
            otxtFile.Close()
            otxtFile = Nothing
            oFs = Nothing
        Else
            ' ref only
            sText = sTmp
        End If

        ' XOKNET 23.05.2012
        Exit Function

NTEErr:
        sCode = ""
        sText = ""

    End Function
    Public Function ACH_Interpret_DTM(ByVal sImportLineBuffer As String, ByRef sCode As String, ByRef sText As String)
        ' the DTM comes in many different flavours, but from the one below
        ' we try to extract code=097 and date=20071008
        ' DTM*097*200708*0706
        Dim sTmp As String, sTmp2 As String

        Dim otxtFile As Scripting.TextStream
        Dim oFs As Scripting.FileSystemObject
        Dim sFilename As String

        ' XOKNET 23.05.2012
        On Error GoTo DTMErr

        ' Try to find code. Must lookup in a .csv-file
        sTmp = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "DTM*") + 4) ' 097*200708*0706
        ' Upto next *
        sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)  ' gives 097

        ' sometimes, there are no codes in a DTM, thus
        If Not EmptyString(sTmp) Then
            ' Find code in file;
            oFs = New Scripting.FileSystemObject  '22.05.2017 CreateObject ("Scripting.Filesystemobject")
            sFilename = My.Application.Info.DirectoryPath & "\ACH_Map_DTM.sdv"
            If oFs.FileExists(sFilename) Then
                otxtFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForReading, False)
                Do While otxtFile.AtEndOfStream = False
                    sTmp2 = otxtFile.ReadLine()  ' reads one line at a time

                    ' remove leading 0 from sTmp
                    Do
                        If Left$(sTmp, 1) = "0" Then
                            sTmp = Mid$(sTmp, 2)
                        Else
                            Exit Do
                        End If
                    Loop

                    If sTmp = xDelim(sTmp2, ";", 1) Then
                        ' found the correct code
                        sCode = "Date - " & xDelim(sTmp2, ";", 2) ' Find the corresponding text for the code, e.g. ALL = All Documents
                        Exit Do
                    End If
                Loop
            End If
            otxtFile.Close()
            otxtFile = Nothing
            oFs = Nothing
            ' Try to find the Date itself, 200708 in the example
            sTmp = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "DTM*") + 4) ' gets 097*200708*0706  ++ a lot of other stuff
            ' find after *
            sText = Mid$(sTmp, InStr(sTmp, "*") + 1)
            ' find upto next \ or next *
            If InStr(sText, "\") > 0 Then
                sText = Left$(sText, InStr(sText, "\") - 1)
            End If
            If InStr(sText, "*") > 0 Then
                sText = Left$(sText, InStr(sText, "*") - 1)
            End If


        Else
            ' ref only
            sText = sTmp
        End If

        ' XOKNET 23.05.2012
        Exit Function

DTMErr:
        sText = ""
        sCode = ""

    End Function
    ' XOKNET 25.05.2012 added sText
    Public Function ACH_Interpret_ADX(ByVal sImportLineBuffer As String, ByRef sCode As String, ByRef sAmount As String, ByRef sText As String)
        ' Try to extact a special Code and the amount (sTmp2)
        ' \ADX*.00*54

        ' XOKNET 25.05.2012
        ' More info about ADX
        'ADX*0*ZZ*BP*50916000HC4HXYQ0000
        '
        'ADX01-The 0 is the amount
        'ADX02-The ZZ is the adjustment reason code.  I could give you the table.
        'ADX03-BP is the Reference ID Qualifier.  Another table.
        'ADX04-Reference identification - the actual information


        Dim sTmp As String, sTmp2 As String
        Dim otxtFile As Scripting.TextStream
        Dim oFs As Scripting.FileSystemObject
        Dim sFilename As String

        ' XOKNET 23.05.2012
        On Error GoTo ADX_err

        ' Try to find code. Must lookup in a .csv-file
        sTmp = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "ADX*") + 4) ' gets .00*54\
        ' From next *
        sTmp = Mid$(sTmp, InStr(sTmp, "*") + 1) ' gets 54\
        ' Upto next * or \
        ' XOKNET 22.05.2012
        If InStr(sTmp, "*") > 0 Then
            sTmp = Left$(sTmp, InStr(sTmp, "*") - 1)  ' gives 54\ , but in case we have more *, we try
        End If
        ' XOKNET 22.05.2012
        If InStr(sTmp, "\") Then
            sTmp = Left$(sTmp, InStr(sTmp, "\") - 1)  ' gives 54
        End If
        ' Might have leading zero
        If Left$(sTmp, 1) = "0" Then
            sTmp = Mid$(sTmp, 2)
        End If

        ' in case there are no codes in a ADX, then
        If Not EmptyString(sTmp) Then
            ' Find code in file;
            oFs = New Scripting.FileSystemObject  '22.05.2017CreateObject ("Scripting.Filesystemobject")
            sFilename = My.Application.Info.DirectoryPath & "\ACH_Map_ADX.sdv"
            If oFs.FileExists(sFilename) Then
                otxtFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForReading, False)
                Do While otxtFile.AtEndOfStream = False
                    sTmp2 = otxtFile.ReadLine()  ' reads one line at a time
                    If sTmp = xDelim(sTmp2, ";", 1) Then
                        ' found the correct code
                        sCode = xDelim(sTmp2, ";", 2) ' Find the corresponding text for the code, e.g. 54 = Freight Deducted
                        Exit Do
                    End If
                Loop
            End If
            otxtFile.Close()
            otxtFile = Nothing
            oFs = Nothing

            ' Try to find the amount, 0.00 in this example
            sTmp = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "ADX*") + 4) ' gets .00*54\
            sTmp = Left$(sTmp, InStr(sTmp, "*") - 1) ' gets 0.00
            sAmount = sTmp

        Else
            ' amount only
            sAmount = sTmp
        End If

        ' XOKNET 25.05.2012 added sText, after 4th * if properly formatted
        ' 'ADX*0*ZZ*BP*50916000HC4HXYQ0000
        sTmp = Mid$(sImportLineBuffer, InStr(sImportLineBuffer, "ADX*"))  ' gets ADX*0*ZZ*BP*50916000HC4HXYQ0000\(next segment..)
        ' keep only the ADX-info, up to next segment if there are anymore
        If InStr(sTmp, "\") > 0 Then
            sTmp = Left$(sTmp, InStr(sTmp, "\") - 1) 'gets ADX*0*ZZ*BP*50916000HC4HXYQ0000
        End If
        If Len(sTmp) - Len(Replace(sTmp, "*", "")) >= 4 Then
            sText = Mid$(sTmp, InStrRev(sTmp, "*") + 1)
        End If

        ' XOKNET 23.05.2012
        Exit Function

ADX_err:
        sAmount = 0
        sCode = ""

    End Function

    Public Function RemoveLeadingCharacters(ByRef sString As String, ByRef sCharactersToRemove As String) As String
        Dim sNewString As String
        Dim lCounter, lCounter2 As Integer
        Dim bRemoveCharacter As Boolean

        sNewString = sString

        For lCounter = 1 To Len(sString)
            bRemoveCharacter = False
            For lCounter2 = 1 To Len(sCharactersToRemove)
                If Left(sNewString, 1) = Mid(sCharactersToRemove, lCounter2, 1) Then
                    bRemoveCharacter = True
                    Exit For
                End If
            Next lCounter2
            If bRemoveCharacter Then
                sNewString = Mid(sNewString, 2)
            Else
                Exit For
            End If
        Next lCounter

        RemoveLeadingCharacters = sNewString

    End Function
    Public Function ExtractBirthNumber(ByRef oPayment As vbBabel.Payment) As String
        ' Extract F�dselsnr fra Freetext
        Dim i As Short
        Dim bOK As Boolean
        Dim sPossible As String

        ' Use methods from vbbabel.Payment to extract possible BirthNumbers
        ' Check 11 digits e.g. 09095839794

        sPossible = ""

        oPayment.MATCH_InvoiceDescription = "###########"
        oPayment.MATCH_ExtractInvoiceNo()
        For i = 1 To oPayment.MATCH_PossibleNumberOfInvoices
            sPossible = Replace(oPayment.MATCH_InvoiceNumber, " ", "")
            ' Check if valid f�dselsnummer
            If CheckBirthnumber_NO(sPossible) Then
                bOK = True
                Exit For
            End If
            oPayment.MATCH_NextInvoice()
        Next
        oPayment.MATCH_Reset()

        If Not bOK Then

            ' Check with - between date and number e.g. 090958-39794
            oPayment.MATCH_InvoiceDescription = "######-#####"
            oPayment.MATCH_ExtractInvoiceNo()
            For i = 1 To oPayment.MATCH_PossibleNumberOfInvoices
                sPossible = Replace(Replace(oPayment.MATCH_InvoiceNumber, "-", ""), " ", "")
                ' Check if valid f�dselsnummer
                If CheckBirthnumber_NO(sPossible) Then
                    bOK = True
                    Exit For
                End If
                oPayment.MATCH_NextInvoice()
            Next
            oPayment.MATCH_Reset()
        End If

        If Not bOK Then
            ' Check with space between date and number e.g. 090958 39794
            oPayment.MATCH_InvoiceDescription = "###### #####"
            oPayment.MATCH_ExtractInvoiceNo()
            For i = 1 To oPayment.MATCH_PossibleNumberOfInvoices
                sPossible = Replace(oPayment.MATCH_InvoiceNumber, " ", "")
                ' Check if valid f�dselsnummer
                If CheckBirthnumber_NO(sPossible) Then
                    bOK = True
                    Exit For
                End If
                oPayment.MATCH_NextInvoice()
            Next
            oPayment.MATCH_Reset()
        End If
        If bOK Then
            ExtractBirthNumber = sPossible
        Else
            ExtractBirthNumber = ""
        End If
    End Function
    Public Function CheckBirthnumber_NO(ByRef sNo As String) As Boolean
        ' Check validity of norwegian birthnumers
        'Hvordan er f�dselsnummeret bygget opp?
        'F�dselsnummer best�r av elleve siffer. De seks f�rste sifrene angir personens f�dselsdato i rekkef�lge: f�dselsdag, m�ned og �r.
        '
        'De fem siste sifrene kalles personnummer. De tre f�rste sifrene i personnummeret kalles individnummer og tildeles fortl�pende innen den enkelte f�dselsdato.
        '
        'Fra 750-500 omfatter personer f�dt fra 1850-1899
        '499-000 omfatter personer f�dt 1900-1999
        '999-500 omfatter personer f�dt 2000-2049
        '
        'Det tredje sifferet i personnummeret forteller noe om kj�nn.. Kvinner har like tall (partall), mens menn har ulike tall (oddetall). For n�rmere informasjon om f�dselsnummeret kan du se p� Skattetatens hjemmesider
        ' De to siste sifre er kontrollsifre. Disse beregnes slik;
        'Kontrollsifrene beregnes av de ni f�rste sifrene i f�dselsnummeret.
        'For � finne det f�rste kontrollsifferet, K1, multipliseres de ni f�rste
        'sifrene i
        'f�dselsnummeret med faste vekter. Produktsummen divideres med 11 ved
        'heltallsdivisjon. Resttallet ved denne divisjonen trekkes fra 11 for �
        'finne K1.
        '
        'Vektene er:
        '3, 7, 6, 1, 8, 9, 4, 5 og 2
        'k1 = 11 - ((3*D1 + 7*D2 + 6*M1 + 1*M2 + 8*�1 + 9*�2 + 4*I1 + 5*I2 +
        '2*I3) - 11*Q1)
        '
        'Q1 st�r for heltallskvotienten for produktsummen dividert med 11, dvs det
        'h�yeste tallet som multiplisert med 11 gir produktsummen eller mindre.
        'Det blir ikke tildelt f�dselsnummer som gir k1 = 10.
        'Dersom k1 = 11, settes K1 lik 0, ellers er K1 lik k1.
        '
        'Det andre kontrollsifferet, K2, beregnes p� tilsvarende m�te av de 9
        'f�rste
        'sifrene og det f�rste kontrollsifferet.
        '
        'Vektene her er:
        '5, 4, 3, 2, 7, 6, 5, 4, 3 og 2
        'k2 = 11 - ((5*D1 + 4*D2 + 3*M1 + 2*M2 + 7*�1 + 6*�2 + 5*I1 + 4*I2 +
        '3*I3 + 2*K1) - 11*Q2)
        'Her st�r Q2 for heltallskvotienten for produktsummen dividert med 11, dvs
        'det h�yeste tallet som multiplisert med 11 gir produktsummen eller mindre.
        'Det blir ikke tildelt f�dselsnummer som gir k2 = 10.
        'Dersom k2 = 11, settes K2 lik 0, ellers er K2 lik k2.


        Dim bOK As Boolean
        bOK = True
        'Test to first for correct day
        If Val(Left(sNo, 2)) > 31 Then
            bOK = False
        End If
        ' Test if month is bellow limit (12)
        If Val(Mid(sNo, 3, 2)) > 12 Then
            bOK = False
        End If
        ' Test two last digits, checksums
        If Not IsModulusBirthnumberNO(sNo) Then
            bOK = False
        End If
        CheckBirthnumber_NO = bOK

    End Function
    Public Function WidthFromSpreadToGrid(ByVal iIn As Integer) As Integer
        ' New function 15.07.2009
        ' Recalculate from columnwidths in Farpoint Spread to vb.net's DataGridView

        ' So far, just return invalue. Later, learn how to recalculate
        WidthFromSpreadToGrid = VB6.PixelsToTwipsX(iIn)
    End Function
    Public Function CleanInputAlphabet(ByVal str As String) As String
        Return System.Text.RegularExpressions.Regex.Replace(str, "[0-9\b\s-]", "")
    End Function
    Public Function CleanInputNumber(ByVal str As String) As String
        Return System.Text.RegularExpressions.Regex.Replace(str, "[a-zA-Z\b\s-.]", "")
    End Function
    'XOKNET 23.03.2012
    Public Function TreatEurosoftGLPostings(ByVal oPayment As vbBabel.Payment, ByVal bCalledFromManualmatching As Boolean, ByVal sErrorText As String) As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim iNoOfGLPostings As Integer
        Dim iNoOfInvoicesPosted As Integer
        Dim bReturnValue As Boolean
        Dim sInvoiceNo As String

        On Error GoTo ERR_TreatEurosoftGLPostings

        bReturnValue = False

        'First check if we have any GL-postings
        iNoOfGLPostings = 0
        For Each oInvoice In oPayment.Invoices
            If oInvoice.MATCH_Final Then
                If oInvoice.MATCH_Matched Then
                    If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                        iNoOfGLPostings = iNoOfGLPostings + 1
                    End If
                End If
            End If
        Next oInvoice

        If iNoOfGLPostings > 0 Then

            'Mark the payment that GL-postings exist (used in the function WriteEurosoft_DetailRecord
            oPayment.ExtraD1 = "GL"

            'First check how many invoices that are posted
            iNoOfInvoicesPosted = 0
            For Each oInvoice In oPayment.Invoices
                If oInvoice.MATCH_Final Then
                    If oInvoice.MATCH_Matched Then
                        If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                            sInvoiceNo = oInvoice.InvoiceNo
                            iNoOfInvoicesPosted = iNoOfInvoicesPosted + 1
                        End If
                    End If
                End If
            Next oInvoice

            If iNoOfInvoicesPosted = 0 Then
                sErrorText = "Det er f�rt minst en hovedbokspost, men det er ingen fakturaer � koble den mot."
                bReturnValue = False
            ElseIf iNoOfInvoicesPosted = 1 Then
                'OK - Attach the GL-Postings against this invoice

                'Find the GL-invoice(s) and and add the InvoiceNo
                For Each oInvoice In oPayment.Invoices
                    If oInvoice.MATCH_Final Then
                        If oInvoice.MATCH_Matched Then
                            If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                                oInvoice.InvoiceNo = sInvoiceNo
                            End If
                        End If
                    End If
                Next oInvoice
                bReturnValue = True
            Else
                'Check if an invoiceno is stated on the GL-postings
                bReturnValue = True
                For Each oInvoice In oPayment.Invoices
                    If oInvoice.MATCH_Final Then
                        If oInvoice.MATCH_Matched Then
                            If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                                If Not EmptyString(oInvoice.InvoiceNo) Then
                                    'OK
                                Else
                                    bReturnValue = False
                                    sErrorText = "Det er f�rt minst en hovedbokspost og flere fakturaer." & vbCrLf & "Du m� angi hvilken faktura den skal krysses mot."
                                    Exit For
                                End If
                            End If
                        End If
                    End If
                Next oInvoice

            End If

        Else
            bReturnValue = True
        End If

        TreatEurosoftGLPostings = bReturnValue

        Exit Function

ERR_TreatEurosoftGLPostings:

        TreatEurosoftGLPostings = False

    End Function
    'XNET - 11.10.2010 - New function
    ' XNET 16.04.2012 lagt inn parameter bAddExtraInformation
    Public Function TreatConectoGLPostings(ByVal oPayment As vbBabel.Payment, ByVal bAddExtraInformation As Boolean) As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim bGLPostingsExists As Boolean
        Dim sArray(,) As String
        Dim lCounter As Long
        Dim bReturnValue As Boolean
        'Array
        '0 - Index of the invoice which keeps the GL-Posting
        '1 - The Amount of the GL-Posting
        '2 - The Match-ID of the invoice used to cover the GL-Posting
        '3 - The restamount of the GL-posting (if the amount has to be used against more than 1 invoice

        On Error GoTo ERR_TreatConectoGLPostings

        bReturnValue = True
        bGLPostingsExists = False

        'Check if there are any GL-postings in the payment. If so build the array
        For Each oInvoice In oPayment.Invoices
            If oInvoice.MATCH_Final Then
                If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                    If bGLPostingsExists Then
                        lCounter = UBound(sArray, 2)
                        ReDim Preserve sArray(3, lCounter)
                    Else
                        lCounter = 0
                        ReDim Preserve sArray(3, 0)
                        bGLPostingsExists = True
                    End If
                    sArray(0, lCounter) = Trim$(Str(oInvoice.Index))
                    sArray(1, lCounter) = Trim$(Str(oInvoice.MON_InvoiceAmount))
                    sArray(3, lCounter) = sArray(1, lCounter)
                End If
            End If
        Next oInvoice

        If bGLPostingsExists Then
            'If there are any GL postings then find the invoic(es) to post the GL-amount against
            lCounter = 0
            For Each oInvoice In oPayment.Invoices
                If oInvoice.MATCH_Final Then
                    If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                        If (Val(sArray(3, lCounter)) * -1 <= oInvoice.MON_InvoiceAmount) Or Val(sArray(3, lCounter)) > -0.000001 Then
                            'Whole amount may be deducted from this invoice
                            'XNET - 13.04.2012 - Added next IF
                            If bAddExtraInformation Then
                                oInvoice.Extra1 = oInvoice.Extra1 & sArray(0, lCounter) & "&" & sArray(3, lCounter)
                            End If
                            sArray(2, lCounter) = oInvoice.MATCH_ID & "&" & sArray(3, lCounter)
                            sArray(3, lCounter) = "0"
                            lCounter = lCounter + 1
                        Else
                            'XNET - 13.04.2012 - Added next IF
                            If bAddExtraInformation Then
                                'XNET - 28.06.2012 - Changed next line
                                oInvoice.Extra1 = oInvoice.Extra1 & sArray(0, lCounter) & "&" & Trim$(Str(oInvoice.MON_InvoiceAmount * -1)) & "&" 'Trim$(Str(Val(sArray(3, lCounter)) + oInvoice.MON_InvoiceAmount)) & "&"
                                'Old code
                                'oInvoice.Extra1 = oInvoice.Extra1 & Trim$(Str(Val(sArray(3, lCounter)) + oInvoice.MON_InvoiceAmount)) & "&" & sArray(3, lCounter) & "&"
                            End If
                            sArray(2, lCounter) = oInvoice.MATCH_ID & "&" & Trim$(Str(oInvoice.MON_InvoiceAmount)) & "&"
                            sArray(3, lCounter) = Trim$(Str(Val(sArray(3, lCounter)) + oInvoice.MON_InvoiceAmount))
                        End If
                        If lCounter > UBound(sArray, 2) Then
                            Exit For
                        End If
                    End If
                End If
            Next oInvoice

            'Check that all GL-postings are reduced to zero
            For lCounter = 0 To UBound(sArray, 2)
                If Not IsEqualAmount(0, Val(sArray(3, lCounter))) Then
                    bReturnValue = False
                    'XNET - 16.04.2012 - Raise an error
                    Err.Raise(25312, "TreatConectoGLPostings", "Ugyldig postering" & vbCrLf & "Det totalt postert hovedboksbel�pet er h�yere en posterte fakturabel�p." & vbCrLf _
                        & "Betalers navn: " & oPayment.E_Name & vbCrLf & "Bel�p: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, "", ",") & vbCrLf & "Referanse: " & oPayment.REF_Bank1)
                End If
            Next lCounter

        End If

        TreatConectoGLPostings = bReturnValue

        Exit Function

ERR_TreatConectoGLPostings:

        'MsgBox "En uventet feil oppstod i rutinen Matched_CheckNegativeAmounts: " & vbCrLf & Err.Description

        TreatConectoGLPostings = False

    End Function
    'XNET - 01.12.2010 - New function
    Public Function AddTextToFilename(ByVal sOriginalFilename As String, ByVal sTextToAdd As String, ByVal bAddBeforeExtension As Boolean, ByVal bAddBeforeFilename As Boolean) As String
        'This function may be used to alter the filename passed.
        'Added so far:
        'sTextToAdd = "NEW"
        'bAddBeforeExtension = True changes from C:\Slett\MyFile.txt to C:\Slett\MyFileNEW.txt
        'bAddBeforeFilename = true changes from C:\Slett\MyFile.txt to C:\Slett\NEWMyFile.txt

        Dim sReturnString As String
        Dim iStartPosFilename As Integer
        Dim iPosForFilenameSuffix As Integer

        On Error GoTo ERR_AddTextToFilename

        If bAddBeforeExtension Then
            iStartPosFilename = InStrRev(sOriginalFilename, "\", , vbTextCompare)
            iPosForFilenameSuffix = InStrRev(Mid$(sOriginalFilename, iStartPosFilename), ".", , vbTextCompare)
            If iPosForFilenameSuffix > 1 Then
                sReturnString = Left$(sOriginalFilename, iStartPosFilename + iPosForFilenameSuffix - 2) & sTextToAdd & Mid$(sOriginalFilename, iStartPosFilename + iPosForFilenameSuffix - 1)
            Else
                sReturnString = sOriginalFilename & sTextToAdd
            End If


        ElseIf bAddBeforeFilename Then
            iStartPosFilename = InStrRev(sOriginalFilename, "\", , vbTextCompare)
            sReturnString = Left$(sOriginalFilename, iStartPosFilename) & sTextToAdd & Mid$(sOriginalFilename, iStartPosFilename + 1)

        Else
            sReturnString = sOriginalFilename
        End If

        AddTextToFilename = sReturnString

        On Error GoTo 0

        Exit Function

ERR_AddTextToFilename:

        'Return original filename if an error occure

        AddTextToFilename = sOriginalFilename

    End Function
    'Public Function NotNull(Of T)(ByVal Value As T, ByVal DefaultValue As T) As T
    '    If Value Is Nothing OrElse IsDBNull(Value) Then
    '        Return DefaultValue
    '    Else
    '        Return Value
    '    End If
    'End Function
    Public Function NotNull(Of T)(ByVal Value As T, ByVal DefaultValue As T) As T
        If Value Is Nothing OrElse IsDBNull(Value) Then
            'If Value.GetType = SystemTypeName(var
            Return DefaultValue
        Else
            Return Value
        End If
    End Function
    Public Function TranslatePackedDecimal(ByVal pd As String, ByVal pScale As Integer) As Double

        Dim i As Integer
        Dim Work As String
        Dim b As Byte
        Dim v As Object

        For i = 1 To Len(pd)
            b = Asc(Mid$(pd, i, 1))
            v = Mid$(pd, i, 1)
            If i = Len(pd) Then
                If ((b And 240) / 16) = 13 Then ' Negative Sign nibble
                    Work = "-" & Work
                End If
            Else
                Work = Work & CInt((b And 240) / 16)
            End If
            Work = Work & (b And 15)
        Next i

        ' Try changing this:
        'TranslatePackedDecimal = CDbl(Work)
        'If pScale <> 0 Then
        '    TranslatePackedDecimal = TranslatePackedDecimal / (10 ^ pScale)
        'End If

        ' To
        Dim dblTemp As Double
        dblTemp = CDbl(Work)
        If pScale <> 0 Then
            dblTemp = dblTemp / (10 ^ pScale)
        End If

        TranslatePackedDecimal = dblTemp
    End Function
    Public Function ValidateXMLFile(ByVal Schema As String, ByVal sNameSpace As String, ByVal XMLDoc As String, ByRef bErrorExists As Boolean, ByRef bWarningExists As Boolean) As ArrayList

        Dim objWorkingXML As New System.Xml.XmlDocument
        Dim objValidateXML As System.Xml.XmlValidatingReader
        Dim objSchemasColl As New System.Xml.Schema.XmlSchemaCollection

        aValidationError.Clear()
        '23.03.2015 - Added reset these values
        bValidationErrorExists = False
        bValidationWarningExists = False

        objSchemasColl.Add(sNameSpace, Schema)

        'This loads XML string 
        objValidateXML = New System.Xml.XmlValidatingReader(New System.Xml.XmlTextReader(XMLDoc))

        objValidateXML.Schemas.Add(objSchemasColl)

        'This is how you CATCH the errors (with a handler function)
        AddHandler objValidateXML.ValidationEventHandler, AddressOf XMLValidationCallBack

        'This is WHERE the validation occurs.. WHEN the XML Document READS throughthe validating reader
        Try
            objWorkingXML.Load(objValidateXML)

        Catch ex As Exception
            aValidationError.Add(ex.Message)
            If Not objValidateXML Is Nothing Then
                objValidateXML.Close()
                objValidateXML = Nothing
            End If
            If Not objWorkingXML Is Nothing Then
                objWorkingXML = Nothing
            End If
            If Not objSchemasColl Is Nothing Then
                objSchemasColl = Nothing
            End If
        Finally
            objValidateXML.Close()
            objValidateXML = Nothing
            objWorkingXML = Nothing
            objSchemasColl = Nothing

        End Try

        bErrorExists = bValidationErrorExists
        bWarningExists = bValidationWarningExists

        Return aValidationError

    End Function

    Private Sub XMLValidationCallBack(ByVal sender As Object, ByVal e As System.Xml.Schema.ValidationEventArgs)
        Select Case e.Severity
            ' TODO - �pne filen og vise data der hvor det er feil
            Case System.Xml.Schema.XmlSeverityType.Error
                bValidationErrorExists = True
                aValidationError.Add("Error: " & vbCrLf & "LineNo: " & e.Exception.LineNumber.ToString & "      LinePos: " & e.Exception.LinePosition & vbCrLf & e.Message & vbCrLf)
            Case System.Xml.Schema.XmlSeverityType.Warning
                bValidationWarningExists = False
                aValidationError.Add("Warning: " & vbCrLf & "LineNo: " & e.Exception.LineNumber.ToString & "      LinePos: " & e.Exception.LinePosition & vbCrLf & e.Message & vbCrLf)
        End Select
        'aValidationError.Add(e.Message)
    End Sub
    Public Function ReplaceUTF8Characters(ByVal s As String) As String

        s = s.Replace("�", "�")   ' �
        s = s.Replace((Chr("142")).ToString, "�")   '�
        s = s.Replace("�", "�")   ' �

        s = s.Replace((Chr("195") & Chr("166")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("134")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("184")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("152")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("165")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("133")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("188")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("156")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("164")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("132")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("182")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("150")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("169")).ToString, "�")
        s = s.Replace((Chr("195") & Chr("137")).ToString, "�")

        ' svenske
        s = s.Replace((Chr("128")).ToString, "�")   ' �
        s = s.Replace((Chr("190")).ToString, "�")   ' �  
        s = s.Replace((Chr("142")).ToString, "�")   ' �  
        s = s.Replace((Chr("220")).ToString, "�")   ' �  
        s = s.Replace((Chr("136")).ToString, "�")   ' '�  



        ReplaceUTF8Characters = s

    End Function
    Public Function GetLanguageCodeFromFile() As String
        'This function is used to retrieve the language code from a file in the app.path directory
        Dim fs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sString As String

        fs = New Scripting.FileSystemObject '22.05.2017 CreateObject ("Scripting.FileSystemObject")
        ' Find databasepath, filename not included
        oFile = fs.OpenTextFile(My.Application.Info.DirectoryPath & "\language.txt", Scripting.IOMode.ForReading)
        sString = oFile.ReadLine
        oFile.Close()
        fs = Nothing
        GetLanguageCodeFromFile = sString
    End Function
    Public Function DetectEncoding(ByVal filename As String) As String

        'Returns "ASCII" if no identifiable byte order mark
        'Returns "Unicode" if an identifiable unicode byte order mark is found
        'Returns "" if file does not exist or no random access

        Dim enc As String = ""
        If System.IO.File.Exists(filename) Then
            Dim filein As New System.IO.FileStream(filename, IO.FileMode.Open, IO.FileAccess.Read)
            If (filein.CanSeek) Then
                Dim bom(4) As Byte
                filein.Read(bom, 0, 5)
                'EF BB BF       = utf-8
                'FF FE          = ucs-2le, ucs-4le, and ucs-16le
                'FE FF          = utf-16 and ucs-2
                '00 00 FE FF    = ucs-4
                'If (((bom(0) = &HEF) And (bom(1) = &HBB) And (bom(2) = &HBF)) Or _
                '    ((bom(0) = &HFF) And (bom(1) = &HFE)) Or _
                '    ((bom(0) = &HFE) And (bom(1) = &HFF)) Or _
                '    ((bom(0) = &H0) And (bom(1) = &H0) And (bom(2) = &HFE) And (bom(3) = &HFF))) Then
                '    enc = "Unicode"
                If Chr(bom(0)) = "<" Or Chr(bom(3)) = "<" Then
                    enc = "XML"
                ElseIf ((bom(0) = &HEF) And (bom(1) = &HBB) And (bom(2) = &HBF)) Then
                    enc = "UTF-8"
                ElseIf ((bom(0) = &HFF) And (bom(1) = &HFE)) Then
                    enc = "ucs-21e, ucs-41e or ucs-161e"
                ElseIf ((bom(0) = &HFE) And (bom(1) = &HFF)) Then
                    enc = "utf-16 or ucs-2"
                ElseIf ((bom(0) = &H0) And (bom(1) = &H0) And (bom(2) = &HFE) And (bom(3) = &HFF)) Then
                    enc = "ucs-4"
                Else
                    enc = "ASCII"
                End If
                'Position the file cursor back to the start of the file
                filein.Seek(0, System.IO.SeekOrigin.Begin)
                ' Do more stuff
            End If
            filein.Close()
        End If
        Return enc

    End Function
    Public Sub FormVismaStyle(ByVal frm As Form, Optional ByVal sPic As String = "", Optional ByVal lHeight As Long = 650)
        On Error Resume Next

        Dim ctl As Control
        Dim sCtlType As String
        Dim sPicture As String
        Dim iImgCnt As Integer

        If lHeight = 0 Then
            lHeight = 650
        End If

        frm.BackColor = Color.LightGray  '&HF6F6F6

        For Each ctl In frm.Controls
            'ctl.Font = "Segoe UI"
            'ctl.FontSize = 8

            sCtlType = TypeName(ctl)
            If sCtlType = "Label" Then
                ctl.BackColor = Color.LightGray  '&HF6F6F6
                'VISMA_TODO
                'ctl.BackStyle = 1
            ElseIf sCtlType = "CheckBox" Then
                ctl.BackColor = Color.LightGray  '&HF6F6F6
                'VISMA_TODO
                'ctl.BackStyle = 1
            ElseIf sCtlType = "TextBox" Then
                ctl.BackColor = Color.LightGray  '&HF6F6F6

            ElseIf sCtlType = "OptionButton" Then
                ctl.BackColor = Color.LightGray  '&HF6F6F6
            ElseIf sCtlType = "CommandButton" Then
                'The following setting creats an error, Commented 29/8 by Kjell
                'ctl.Style = 1 ' Graphical
                'Must be set to Style=Graphical when form is created
                ctl.BackColor = Color.LightGray  '&HF6F6F6
                'VISMA_TODO
                'ctl.Appearance = 0
                'ctl.Style = 0

            ElseIf sCtlType = "Menu" Then
                'VISMA_TODO
                'ctl.Font = "Tahoma"
                'ctl.FontSize = 14

                ctl.BackColor = Color.LightGray  '&HF6F6F6
            ElseIf sCtlType = "Toolbar" Then
                ctl.BackColor = Color.LightGray  '&HF6F6F6
            ElseIf sCtlType = "ListView" Then
                ctl.BackColor = Color.LightGray  '&HF6F6F6
            ElseIf sCtlType = "Frame" Then
                If ctl.Name = "frameLabels" Then
                    ctl.BackColor = Color.LightGray  ' VISMA_TODO Hvilken farge er dette?  RGB(255, 240, 180)
                Else
                    ctl.BackColor = Color.LightGray  '&HF6F6F6
                End If
                'ElseIf sCtlType = "TabStrip" Then
                'ctl.Parent.BackColor = vbWhite

            End If
        Next

    End Sub
    ' 30.03.2016 added function
    Public Function vb_LastDayOfMonth(ByVal aDate As DateTime) As Date
        Return New DateTime(aDate.Year, _
                            aDate.Month, _
                            DateTime.DaysInMonth(aDate.Year, aDate.Month))
    End Function
    Public Sub RemoveLastCRLFInFile(ByVal sFilename As String)

        Dim crString = Environment.NewLine '= vbCrLf
        Dim crBytes = System.Text.Encoding.ASCII.GetBytes(crString)
        Dim bytesRead(crBytes.Length - 1) As Byte
        Dim iOffset As Integer = 0
        Dim stringRead As String

        ' first, test if we have access to the file
        If CheckFile(sFilename, vb_Utils.ErrorMessageType.Dont_Show, "") < 50 Then
            Using fs = System.IO.File.Open(sFilename, FileMode.Open, FileAccess.ReadWrite)
                While iOffset < fs.Length
                    fs.Seek(-(crBytes.Length + iOffset), SeekOrigin.End)
                    fs.Read(bytesRead, 0, crBytes.Length)
                    stringRead = System.Text.Encoding.ASCII.GetString(bytesRead)
                    If stringRead = crString Then
                        fs.SetLength(fs.Length - (crBytes.Length * iOffset + 1))
                        Exit While
                    End If
                    iOffset += 1
                End While
            End Using
        End If

    End Sub
    Public Function GZ_UnZip(ByVal compressedText As String) As String
        'Dim gzBuffer As Byte() = Convert.FromBase64String(compressedText)
        'Using ms As New MemoryStream()
        '    Dim msgLength As Integer = BitConverter.ToInt32(gzBuffer, 0)
        '    ' her skrives fra pos 4 - kan det v�re noe "bom"-messig?
        '    'ms.Write(gzBuffer, 4, gzBuffer.Length - 4)
        '    ' pr�v "uten bom"
        '    ms.Write(gzBuffer, 0, gzBuffer.Length)

        '    Dim buffer As Byte() = New Byte(msgLength - 1) {}

        '    ms.Position = 0
        '    Using zipStream As New System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Decompress)
        '        zipStream.Read(buffer, 0, buffer.Length)
        '    End Using

        '    Return System.Text.Encoding.Unicode.GetString(buffer, 0, buffer.Length)
        'End Using

        Dim str As String = ""
        Dim bytes As Byte() = System.Convert.FromBase64String(compressedText)

        'Standard GZipStream code for decompression
        Using ms As New IO.MemoryStream(bytes)
            Using gzs As New System.IO.Compression.GZipStream(ms, IO.Compression.CompressionMode.Decompress)
                Using rdr As New IO.StreamReader(gzs)
                    'Console.WriteLine(rdr.ReadToEnd)
                    str = rdr.ReadToEnd
                End Using 'rdr
            End Using 'gzs
        End Using 'ms
        Return str
    End Function
    Public Function GZIPUnCompress(ByVal compressed() As Byte) As Byte()
        ' uncompress a gzipped bytearray
        ' Clean up memory with Using-statements.
        Using memory As System.IO.MemoryStream = New System.IO.MemoryStream(compressed)
            ' Create uncompressed stream.
            Using gzip As System.IO.Compression.GZipStream = New System.IO.Compression.GZipStream(memory, System.IO.Compression.CompressionMode.Decompress, False)
                ' Reset variable to collect uncompressed result
                'Dim uncompressed As Byte() = New Byte(compressed.Length)
                'Dim iOutputSize As Integer = compressed.Length  'BitConverter.ToInt32(compressed, 0) - 3
                Dim iOutputSize As Integer = BitConverter.ToInt32(compressed, 0) - 3
                ' read
                gzip.Read(GZIPUnCompress, 0, memory.Length)
            End Using
            ' Return array.
            Return memory.ToArray()
        End Using
    End Function

    Function GZIPDeCompress(ByRef compress() As Byte) As Byte()
        Using output As MemoryStream = New MemoryStream()
            Using memory As MemoryStream = New MemoryStream(compress)
                Using gzip As System.IO.Compression.GZipStream = New System.IO.Compression.GZipStream(memory, System.IO.Compression.CompressionMode.Decompress)
                    CopyStreamToStream(gzip, output)
                End Using
                Return output.ToArray()
            End Using
        End Using
    End Function


    Public Sub CopyStreamToStream(ByVal input As Stream, ByVal output As Stream)
        Dim buffer As Byte() = New Byte(16 * 1024 - 1) {}
        ' Fairly arbitrary size
        Dim bytesRead As Integer

        While (InlineAssignHelper(bytesRead, input.Read(buffer, 0, buffer.Length))) > 0
            output.Write(buffer, 0, bytesRead)
        End While
    End Sub
    Private Function InlineAssignHelper(Of T)(ByRef target As T, ByVal value As T) As T
        target = value
        Return value
    End Function
    Public Class _MyListBoxItem
        '11.06.2019 add New overloaded methods to accept parameters on creation of object for simplicity

        Dim m_ItemData As Integer
        Dim m_ItemString As String = ""
        Public Sub New(Optional ByVal sItemString As String = "", Optional ByVal iItemData As Integer = 0)
            m_ItemString = sItemString
            m_ItemData = iItemData
        End Sub
        Public Property ItemData() As Integer

            Get
                Return m_ItemData
            End Get

            Set(ByVal value As Integer)
                m_ItemData = value
            End Set

        End Property


        Public Property ItemString() As String

            Get
                Return m_ItemString
            End Get

            Set(ByVal value As String)
                m_ItemString = value
            End Set

        End Property


        'override the ToString to display the ItemString value in the combo box
        Public Overrides Function ToString() As String
            Return m_ItemString
        End Function


        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            'check to see if the objects to string value matches the ItemString
            Return obj.ToString = m_ItemString
        End Function

    End Class
    Public Function IsUnicode(ByVal input As String) As Boolean
        Dim asciiBytesCount = System.Text.Encoding.ASCII.GetByteCount(input)
        Dim unicodBytesCount = System.Text.Encoding.UTF8.GetByteCount(input)
        Return asciiBytesCount <> unicodBytesCount
    End Function
End Module
