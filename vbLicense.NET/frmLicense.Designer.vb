<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmLicense
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdMailCode As System.Windows.Forms.Button
	Public WithEvents cmbSupport As System.Windows.Forms.ComboBox
	Public WithEvents txtFormatsLimit As System.Windows.Forms.TextBox
    Public WithEvents cmdMail As System.Windows.Forms.Button
	Public WithEvents txtEMail As System.Windows.Forms.TextBox
	Public WithEvents txtPreviousRecords As System.Windows.Forms.TextBox
	Public WithEvents cmdFullLicense As System.Windows.Forms.Button
	Public WithEvents txtDateResets As System.Windows.Forms.TextBox
	Public WithEvents txtDateLastUsed As System.Windows.Forms.TextBox
	Public WithEvents txtRecordsUsed As System.Windows.Forms.TextBox
	Public WithEvents txtServices As System.Windows.Forms.TextBox
	Public WithEvents txtDateLimit As System.Windows.Forms.TextBox
	Public WithEvents txtRecordsLimit As System.Windows.Forms.TextBox
	Public WithEvents txtName As System.Windows.Forms.TextBox
	Public WithEvents txtNumber As System.Windows.Forms.TextBox
	Public WithEvents cmdReadFile As System.Windows.Forms.Button
	Public WithEvents cmdCreateFile As System.Windows.Forms.Button
	Public WithEvents Label14 As System.Windows.Forms.Label
    Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents lblEMail As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents lblLisensfil As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLicense))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdMailCode = New System.Windows.Forms.Button
        Me.cmbSupport = New System.Windows.Forms.ComboBox
        Me.txtFormatsLimit = New System.Windows.Forms.TextBox
        Me.cmdMail = New System.Windows.Forms.Button
        Me.txtEMail = New System.Windows.Forms.TextBox
        Me.txtPreviousRecords = New System.Windows.Forms.TextBox
        Me.cmdFullLicense = New System.Windows.Forms.Button
        Me.txtDateResets = New System.Windows.Forms.TextBox
        Me.txtDateLastUsed = New System.Windows.Forms.TextBox
        Me.txtRecordsUsed = New System.Windows.Forms.TextBox
        Me.txtServices = New System.Windows.Forms.TextBox
        Me.txtDateLimit = New System.Windows.Forms.TextBox
        Me.txtRecordsLimit = New System.Windows.Forms.TextBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtNumber = New System.Windows.Forms.TextBox
        Me.cmdReadFile = New System.Windows.Forms.Button
        Me.cmdCreateFile = New System.Windows.Forms.Button
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.lblEMail = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblLisensfil = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmdCodeOneYearLicense = New System.Windows.Forms.Button
        Me.cmdCode5DayLicense = New System.Windows.Forms.Button
        Me.cmdCodeThreeYearsLicense = New System.Windows.Forms.Button
        Me.cmdCodeSQLDatabase = New System.Windows.Forms.Button
        Me.txtLicenseCode = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'cmdMailCode
        '
        Me.cmdMailCode.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMailCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdMailCode.Enabled = False
        Me.cmdMailCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdMailCode.Location = New System.Drawing.Point(466, 344)
        Me.cmdMailCode.Name = "cmdMailCode"
        Me.cmdMailCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdMailCode.Size = New System.Drawing.Size(107, 33)
        Me.cmdMailCode.TabIndex = 32
        Me.cmdMailCode.Text = "Mail lisenskode"
        Me.cmdMailCode.UseVisualStyleBackColor = False
        '
        'cmbSupport
        '
        Me.cmbSupport.BackColor = System.Drawing.SystemColors.Window
        Me.cmbSupport.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbSupport.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbSupport.Items.AddRange(New Object() {"Standard", "None"})
        Me.cmbSupport.Location = New System.Drawing.Point(168, 216)
        Me.cmbSupport.Name = "cmbSupport"
        Me.cmbSupport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbSupport.Size = New System.Drawing.Size(145, 21)
        Me.cmbSupport.TabIndex = 6
        Me.cmbSupport.Text = "Standard"
        '
        'txtFormatsLimit
        '
        Me.txtFormatsLimit.AcceptsReturn = True
        Me.txtFormatsLimit.BackColor = System.Drawing.SystemColors.Window
        Me.txtFormatsLimit.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFormatsLimit.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFormatsLimit.Location = New System.Drawing.Point(168, 184)
        Me.txtFormatsLimit.MaxLength = 0
        Me.txtFormatsLimit.Name = "txtFormatsLimit"
        Me.txtFormatsLimit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFormatsLimit.Size = New System.Drawing.Size(89, 20)
        Me.txtFormatsLimit.TabIndex = 5
        Me.txtFormatsLimit.Text = "1"
        '
        'cmdMail
        '
        Me.cmdMail.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdMail.Enabled = False
        Me.cmdMail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdMail.Location = New System.Drawing.Point(353, 345)
        Me.cmdMail.Name = "cmdMail"
        Me.cmdMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdMail.Size = New System.Drawing.Size(107, 33)
        Me.cmdMail.TabIndex = 15
        Me.cmdMail.Text = "Mail lisensfil"
        Me.cmdMail.UseVisualStyleBackColor = False
        '
        'txtEMail
        '
        Me.txtEMail.AcceptsReturn = True
        Me.txtEMail.BackColor = System.Drawing.SystemColors.Window
        Me.txtEMail.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtEMail.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtEMail.Location = New System.Drawing.Point(168, 248)
        Me.txtEMail.MaxLength = 0
        Me.txtEMail.Name = "txtEMail"
        Me.txtEMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtEMail.Size = New System.Drawing.Size(377, 20)
        Me.txtEMail.TabIndex = 11
        '
        'txtPreviousRecords
        '
        Me.txtPreviousRecords.AcceptsReturn = True
        Me.txtPreviousRecords.BackColor = System.Drawing.SystemColors.Window
        Me.txtPreviousRecords.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPreviousRecords.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPreviousRecords.Location = New System.Drawing.Point(456, 184)
        Me.txtPreviousRecords.MaxLength = 0
        Me.txtPreviousRecords.Name = "txtPreviousRecords"
        Me.txtPreviousRecords.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPreviousRecords.Size = New System.Drawing.Size(89, 20)
        Me.txtPreviousRecords.TabIndex = 10
        Me.txtPreviousRecords.Text = "0"
        '
        'cmdFullLicense
        '
        Me.cmdFullLicense.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFullLicense.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFullLicense.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFullLicense.Location = New System.Drawing.Point(240, 345)
        Me.cmdFullLicense.Name = "cmdFullLicense"
        Me.cmdFullLicense.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFullLicense.Size = New System.Drawing.Size(107, 33)
        Me.cmdFullLicense.TabIndex = 14
        Me.cmdFullLicense.Text = "Lag korrigert lisensfil"
        Me.cmdFullLicense.UseVisualStyleBackColor = False
        '
        'txtDateResets
        '
        Me.txtDateResets.AcceptsReturn = True
        Me.txtDateResets.BackColor = System.Drawing.SystemColors.Window
        Me.txtDateResets.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDateResets.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDateResets.Location = New System.Drawing.Point(456, 152)
        Me.txtDateResets.MaxLength = 0
        Me.txtDateResets.Name = "txtDateResets"
        Me.txtDateResets.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDateResets.Size = New System.Drawing.Size(89, 20)
        Me.txtDateResets.TabIndex = 9
        Me.txtDateResets.Text = "0"
        '
        'txtDateLastUsed
        '
        Me.txtDateLastUsed.AcceptsReturn = True
        Me.txtDateLastUsed.BackColor = System.Drawing.SystemColors.Window
        Me.txtDateLastUsed.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDateLastUsed.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDateLastUsed.Location = New System.Drawing.Point(456, 120)
        Me.txtDateLastUsed.MaxLength = 0
        Me.txtDateLastUsed.Name = "txtDateLastUsed"
        Me.txtDateLastUsed.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDateLastUsed.Size = New System.Drawing.Size(89, 20)
        Me.txtDateLastUsed.TabIndex = 8
        Me.txtDateLastUsed.Text = "01.01.01"
        '
        'txtRecordsUsed
        '
        Me.txtRecordsUsed.AcceptsReturn = True
        Me.txtRecordsUsed.BackColor = System.Drawing.SystemColors.Window
        Me.txtRecordsUsed.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRecordsUsed.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRecordsUsed.Location = New System.Drawing.Point(456, 88)
        Me.txtRecordsUsed.MaxLength = 0
        Me.txtRecordsUsed.Name = "txtRecordsUsed"
        Me.txtRecordsUsed.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRecordsUsed.Size = New System.Drawing.Size(89, 20)
        Me.txtRecordsUsed.TabIndex = 7
        Me.txtRecordsUsed.Text = "0"
        '
        'txtServices
        '
        Me.txtServices.AcceptsReturn = True
        Me.txtServices.BackColor = System.Drawing.SystemColors.Window
        Me.txtServices.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtServices.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtServices.Location = New System.Drawing.Point(168, 152)
        Me.txtServices.MaxLength = 0
        Me.txtServices.Name = "txtServices"
        Me.txtServices.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtServices.Size = New System.Drawing.Size(89, 20)
        Me.txtServices.TabIndex = 4
        '
        'txtDateLimit
        '
        Me.txtDateLimit.AcceptsReturn = True
        Me.txtDateLimit.BackColor = System.Drawing.SystemColors.Window
        Me.txtDateLimit.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDateLimit.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDateLimit.Location = New System.Drawing.Point(168, 120)
        Me.txtDateLimit.MaxLength = 8
        Me.txtDateLimit.Name = "txtDateLimit"
        Me.txtDateLimit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDateLimit.Size = New System.Drawing.Size(89, 20)
        Me.txtDateLimit.TabIndex = 3
        Me.txtDateLimit.Text = "31.12.99"
        '
        'txtRecordsLimit
        '
        Me.txtRecordsLimit.AcceptsReturn = True
        Me.txtRecordsLimit.BackColor = System.Drawing.SystemColors.Window
        Me.txtRecordsLimit.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRecordsLimit.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRecordsLimit.Location = New System.Drawing.Point(168, 88)
        Me.txtRecordsLimit.MaxLength = 0
        Me.txtRecordsLimit.Name = "txtRecordsLimit"
        Me.txtRecordsLimit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRecordsLimit.Size = New System.Drawing.Size(89, 20)
        Me.txtRecordsLimit.TabIndex = 2
        Me.txtRecordsLimit.Text = "5000"
        '
        'txtName
        '
        Me.txtName.AcceptsReturn = True
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtName.Location = New System.Drawing.Point(168, 56)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtName.Size = New System.Drawing.Size(377, 20)
        Me.txtName.TabIndex = 1
        Me.txtName.Text = "Firmanavn"
        '
        'txtNumber
        '
        Me.txtNumber.AcceptsReturn = True
        Me.txtNumber.BackColor = System.Drawing.SystemColors.Window
        Me.txtNumber.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNumber.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNumber.Location = New System.Drawing.Point(168, 24)
        Me.txtNumber.MaxLength = 5
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNumber.Size = New System.Drawing.Size(89, 20)
        Me.txtNumber.TabIndex = 0
        Me.txtNumber.Text = "31000"
        '
        'cmdReadFile
        '
        Me.cmdReadFile.BackColor = System.Drawing.SystemColors.Control
        Me.cmdReadFile.CausesValidation = False
        Me.cmdReadFile.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdReadFile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdReadFile.Location = New System.Drawing.Point(16, 345)
        Me.cmdReadFile.Name = "cmdReadFile"
        Me.cmdReadFile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdReadFile.Size = New System.Drawing.Size(107, 33)
        Me.cmdReadFile.TabIndex = 12
        Me.cmdReadFile.Text = "Les lisensfil"
        Me.cmdReadFile.UseVisualStyleBackColor = False
        '
        'cmdCreateFile
        '
        Me.cmdCreateFile.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCreateFile.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCreateFile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCreateFile.Location = New System.Drawing.Point(128, 345)
        Me.cmdCreateFile.Name = "cmdCreateFile"
        Me.cmdCreateFile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCreateFile.Size = New System.Drawing.Size(107, 33)
        Me.cmdCreateFile.TabIndex = 13
        Me.cmdCreateFile.Text = "Lag lisensfil upgrade"
        Me.cmdCreateFile.UseVisualStyleBackColor = False
        Me.cmdCreateFile.Visible = False
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.SystemColors.Control
        Me.Label14.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label14.Location = New System.Drawing.Point(25, 310)
        Me.Label14.Name = "Label14"
        Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label14.Size = New System.Drawing.Size(129, 17)
        Me.Label14.TabIndex = 31
        Me.Label14.Text = "N�kkel:"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.SystemColors.Control
        Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label12.Location = New System.Drawing.Point(24, 216)
        Me.Label12.Name = "Label12"
        Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label12.Size = New System.Drawing.Size(137, 17)
        Me.Label12.TabIndex = 29
        Me.Label12.Text = "Supportavtale"
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.SystemColors.Control
        Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label11.Location = New System.Drawing.Point(24, 184)
        Me.Label11.Name = "Label11"
        Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label11.Size = New System.Drawing.Size(137, 17)
        Me.Label11.TabIndex = 28
        Me.Label11.Text = "Antall formater"
        '
        'lblEMail
        '
        Me.lblEMail.BackColor = System.Drawing.SystemColors.Control
        Me.lblEMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblEMail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblEMail.Location = New System.Drawing.Point(24, 248)
        Me.lblEMail.Name = "lblEMail"
        Me.lblEMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblEMail.Size = New System.Drawing.Size(121, 17)
        Me.lblEMail.TabIndex = 27
        Me.lblEMail.Text = "E-Mail adresse"
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.SystemColors.Control
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(304, 184)
        Me.Label10.Name = "Label10"
        Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label10.Size = New System.Drawing.Size(145, 17)
        Me.Label10.TabIndex = 26
        Me.Label10.Text = "Forrige antall records"
        '
        'lblLisensfil
        '
        Me.lblLisensfil.BackColor = System.Drawing.SystemColors.Control
        Me.lblLisensfil.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLisensfil.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLisensfil.Location = New System.Drawing.Point(176, 280)
        Me.lblLisensfil.Name = "lblLisensfil"
        Me.lblLisensfil.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLisensfil.Size = New System.Drawing.Size(369, 17)
        Me.lblLisensfil.TabIndex = 25
        Me.lblLisensfil.Text = "Lisensfilnavn"
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.SystemColors.Control
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(24, 281)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(129, 17)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "Lisensfil:"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(304, 152)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(145, 17)
        Me.Label8.TabIndex = 23
        Me.Label8.Text = "Dato resatt antall ganger"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(304, 120)
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label7.Size = New System.Drawing.Size(145, 17)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "Sist brukt dato (dd.mm.yy)"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(304, 88)
        Me.Label6.Name = "Label6"
        Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label6.Size = New System.Drawing.Size(153, 17)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Antall records - Brukt"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.Control
        Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(24, 152)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(137, 33)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "Tjenester (T=TBI, DNBX, V=Visma, SEB)"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(24, 120)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(145, 17)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Utl�psdato (dd.mm.yy)"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(24, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(121, 17)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Antall records - Grense"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(24, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(121, 17)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Navn"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(24, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(121, 17)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Lisensnummer"
        '
        'cmdCodeOneYearLicense
        '
        Me.cmdCodeOneYearLicense.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCodeOneYearLicense.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCodeOneYearLicense.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCodeOneYearLicense.Location = New System.Drawing.Point(353, 384)
        Me.cmdCodeOneYearLicense.Name = "cmdCodeOneYearLicense"
        Me.cmdCodeOneYearLicense.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCodeOneYearLicense.Size = New System.Drawing.Size(107, 33)
        Me.cmdCodeOneYearLicense.TabIndex = 35
        Me.cmdCodeOneYearLicense.Text = "Ett�rs lisenskode"
        Me.cmdCodeOneYearLicense.UseVisualStyleBackColor = False
        '
        'cmdCode5DayLicense
        '
        Me.cmdCode5DayLicense.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCode5DayLicense.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCode5DayLicense.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCode5DayLicense.Location = New System.Drawing.Point(238, 384)
        Me.cmdCode5DayLicense.Name = "cmdCode5DayLicense"
        Me.cmdCode5DayLicense.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCode5DayLicense.Size = New System.Drawing.Size(107, 33)
        Me.cmdCode5DayLicense.TabIndex = 36
        Me.cmdCode5DayLicense.Text = "5 dagers lisenskode"
        Me.cmdCode5DayLicense.UseVisualStyleBackColor = False
        '
        'cmdCodeThreeYearsLicense
        '
        Me.cmdCodeThreeYearsLicense.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCodeThreeYearsLicense.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCodeThreeYearsLicense.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCodeThreeYearsLicense.Location = New System.Drawing.Point(466, 384)
        Me.cmdCodeThreeYearsLicense.Name = "cmdCodeThreeYearsLicense"
        Me.cmdCodeThreeYearsLicense.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCodeThreeYearsLicense.Size = New System.Drawing.Size(107, 33)
        Me.cmdCodeThreeYearsLicense.TabIndex = 37
        Me.cmdCodeThreeYearsLicense.Text = "Tre�rs lisenskode"
        Me.cmdCodeThreeYearsLicense.UseVisualStyleBackColor = False
        '
        'cmdCodeSQLDatabase
        '
        Me.cmdCodeSQLDatabase.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCodeSQLDatabase.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCodeSQLDatabase.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCodeSQLDatabase.Location = New System.Drawing.Point(128, 384)
        Me.cmdCodeSQLDatabase.Name = "cmdCodeSQLDatabase"
        Me.cmdCodeSQLDatabase.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCodeSQLDatabase.Size = New System.Drawing.Size(107, 33)
        Me.cmdCodeSQLDatabase.TabIndex = 38
        Me.cmdCodeSQLDatabase.Text = "Kode for lage SQL"
        Me.cmdCodeSQLDatabase.UseVisualStyleBackColor = True
        '
        'txtLicenseCode
        '
        Me.txtLicenseCode.Location = New System.Drawing.Point(173, 303)
        Me.txtLicenseCode.Name = "txtLicenseCode"
        Me.txtLicenseCode.ReadOnly = True
        Me.txtLicenseCode.Size = New System.Drawing.Size(371, 20)
        Me.txtLicenseCode.TabIndex = 39
        '
        'frmLicense
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(583, 429)
        Me.Controls.Add(Me.txtLicenseCode)
        Me.Controls.Add(Me.cmdCodeSQLDatabase)
        Me.Controls.Add(Me.cmdCodeThreeYearsLicense)
        Me.Controls.Add(Me.cmdCode5DayLicense)
        Me.Controls.Add(Me.cmdCodeOneYearLicense)
        Me.Controls.Add(Me.cmdMailCode)
        Me.Controls.Add(Me.cmbSupport)
        Me.Controls.Add(Me.txtFormatsLimit)
        Me.Controls.Add(Me.cmdMail)
        Me.Controls.Add(Me.txtEMail)
        Me.Controls.Add(Me.txtPreviousRecords)
        Me.Controls.Add(Me.cmdFullLicense)
        Me.Controls.Add(Me.txtDateResets)
        Me.Controls.Add(Me.txtDateLastUsed)
        Me.Controls.Add(Me.txtRecordsUsed)
        Me.Controls.Add(Me.txtServices)
        Me.Controls.Add(Me.txtDateLimit)
        Me.Controls.Add(Me.txtRecordsLimit)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.txtNumber)
        Me.Controls.Add(Me.cmdReadFile)
        Me.Controls.Add(Me.cmdCreateFile)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.lblEMail)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblLisensfil)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmLicense"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Lisensopplysninger"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents cmdCodeOneYearLicense As System.Windows.Forms.Button
    Public WithEvents cmdCode5DayLicense As System.Windows.Forms.Button
    Public WithEvents cmdCodeThreeYearsLicense As System.Windows.Forms.Button
    Public WithEvents cmdCodeSQLDatabase As System.Windows.Forms.Button
    Friend WithEvents txtLicenseCode As System.Windows.Forms.TextBox
#End Region 
End Class