Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmLicense
	Inherits System.Windows.Forms.Form
	Private nLicUsedRecords As Double 'No of used paymentrecords
	Private nLicPreviousRecords As Double ' No of records used before last update
	Private nLicLimitRecords As Double 'No of available paymentrecords in license
	Private nLicLimitFormats As Short ' No of available formats
	Private sLicServicesAvailable As String 'Which of BabelBanks service is user licensed to use ?
	Private sLicDateLastUsed As String 'Date last time records were imported
	Private sLicLimitDate As String 'Timeoutdate if limited license
	Private sLicNumber As String 'License number
	Private sLicName As String 'License registered to
	Private sLicSupport As String 'Supporttype (Standard, eller None)
	Private nLicDateResets As Short ' No of times date is reset (for testlicenses)
	'Fully qualified path
	Private sLicFileName As String ' Name of file (app.path +  "\babel.lic")
	Private sKey As String
	
	Private Sub cmdMailCode_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdMailCode.Click
		' Lag en mail best�ende av lisenskoden, som bruker skal punche
		
		Dim oFs As Scripting.FileSystemObject 'Object
		'Dim oFile As TextStream
		Dim bOK As Boolean
		Dim sMsgNote As String
        Dim SendMail As String
        Dim myMail As New vbMail
		
		bOK = True
		
		If Len(Me.txtEMail.Text) = 0 Or InStr(Me.txtEMail.Text, "@") = 0 Then
			MsgBox("Please specify email-adress!")
			bOK = False
			Me.txtEMail.Focus()
		End If
		
		If bOK Then
            myMail.EMail_SMTP = False

			On Error GoTo MAPIFail
			
            myMail.AddReceivers(Me.txtEMail.Text)
            myMail.Subject = "Lisenskode for BabelBank fra Visual Banking AS"

            ' 06.01.2017 added mail of licensecode for SQL server database
            If Len(sKey) < 6 Then
                sMsgNote = "Her f�lger en lisenskode for � kunne lage SQL server database i BabelBank." & vbCr & vbCr
                sMsgNote = sMsgNote & "Denne koden skal du registrere i BabelBank." & vbCr
                sMsgNote = sMsgNote & "1. Start BabelBank (Vanligvis med Start-knappen, Programmer, BabelBank, BabelBank)" & vbCr
                sMsgNote = sMsgNote & "2. G� til menyen 'Fil', og velg 'Lag SQL Server DB'" & vbCr
                sMsgNote = sMsgNote & "3. Skriv inn den koden som vises i denne mailen n�r du blir spurt om dette. " & vbCr & vbCr & vbCr
                sMsgNote = sMsgNote & "LISENSKODE:          " & sKey & vbCr & vbCr & vbCr
                sMsgNote = sMsgNote & "Med vennlig hilsen" & vbCr
                sMsgNote = sMsgNote & "Visual Banking AS"

            Else
                sMsgNote = "Her f�lger en lisenskode for BabelBank." & vbCr & vbCr
                sMsgNote = sMsgNote & "Denne koden skal du registrere i BabelBank." & vbCr
                sMsgNote = sMsgNote & "1. Start BabelBank (Vanligvis med Start-knappen, Programmer, BabelBank, BabelBank)" & vbCr
                sMsgNote = sMsgNote & "2. G� til menyen 'Oppsett', og velg 'Registrer lisenskode'" & vbCr
                sMsgNote = sMsgNote & "3. Skriv inn den koden som vises i denne mailen. V�r n�ye med sm� og store bokstaver! " & vbCr & vbCr & vbCr
                sMsgNote = sMsgNote & "LISENSKODE:          " & sKey & vbCr & vbCr & vbCr
                sMsgNote = sMsgNote & "Med vennlig hilsen" & vbCr
                sMsgNote = sMsgNote & "Visual Banking AS"
            End If
            myMail.Body = sMsgNote

            myMail.Send()

        End If

        Exit Sub

MAPIFail:
        SendMail = Err.Description
        
	End Sub
	
	Private Sub frmLicense_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Me.txtDateLastUsed.Text = VB.Right("0" & Trim(Str(VB.Day(Now))), 2) & "." & VB.Right("0" & Trim(Str(Month(Now))), 2) & "." & Mid(CStr(Year(Now)), 3, 2)
		
	End Sub
	Private Sub cmdCreateFile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCreateFile.Click
		' Create an license UPGRADEfile
		Dim oFs As Object 'Scripting
		Dim oFile As Object
		Dim sLine As String
		
		Me.cmdMail.Enabled = True
		Me.cmdMailCode.Enabled = True
		
		sLicFileName = My.Application.Info.DirectoryPath & "\babel.upgrade"
		Me.lblLisensfil.Text = sLicFileName
		
		oFs = CreateObject("Scripting.Filesystemobject")
		' Erase the licensefile, we're creating a new one:
        If oFs.FileExists(sLicFileName) Then
            oFs.DeleteFile(sLicFileName, True)
            If oFs.FileExists(My.Application.Info.DirectoryPath & "\babel.lic") Then
                ' Also delete babel.lic, to avoid problems when sending mail:
                oFs.DeleteFile(My.Application.Info.DirectoryPath & "\babel.lic", True)
            End If
        End If
		' Open licensefile:
        oFile = oFs.OpenTextFile(sLicFileName, Scripting.IOMode.ForAppending, True)
		
		
		If Len(Me.txtNumber.Text) > 0 Then
			sLicNumber = Me.txtNumber.Text
            oFile.WriteLine(LicCrypt("NUMBER") & " = " & LicCrypt(sLicNumber, True))
		End If
		If Len(Me.txtName.Text) > 0 Then
			sLicName = Me.txtName.Text
            oFile.WriteLine(LicCrypt("NAME") & " = " & LicCrypt(sLicName))
		End If
		If Len(Trim(Me.cmbSupport.Text)) > 0 Then
			sLicSupport = Me.cmbSupport.Text
            oFile.WriteLine(LicCrypt("SUPPORT") & " = " & LicCrypt(sLicSupport))
		End If
		
		If Val(Me.txtRecordsLimit.Text) > 0 Then
			nLicLimitRecords = Val(Me.txtRecordsLimit.Text)
            oFile.WriteLine(LicCrypt("LIMITRECORDS") & " = " & LicCrypt(Str(nLicLimitRecords), True))
		End If
		If Val(Me.txtFormatsLimit.Text) > 0 Then
			nLicLimitFormats = Val(Me.txtFormatsLimit.Text)
            oFile.WriteLine(LicCrypt("LIMITFORMATS") & " = " & LicCrypt(Str(nLicLimitFormats), True))
		End If
		
		If Val(Me.txtRecordsUsed.Text) > 0 Then
			nLicUsedRecords = Val(Me.txtRecordsUsed.Text)
            oFile.WriteLine(LicCrypt("USEDRECORDS") & " = " & LicCrypt(Str(nLicUsedRecords), True))
		End If
		If Len(Me.txtDateLimit.Text) > 0 Then
			' dd.mm.yy
			sLicLimitDate = "20" & VB.Right(Me.txtDateLimit.Text, 2) & Mid(Me.txtDateLimit.Text, 4, 2) & VB.Left(Me.txtDateLimit.Text, 2)
            oFile.WriteLine(LicCrypt("LIMITDATE") & " = " & LicCrypt(sLicLimitDate, True))
		End If
        If Len(Me.txtServices.Text) > 0 Then
            If InStr(Me.txtServices.Text, "P") > 0 Then
                sLicServicesAvailable = Me.txtServices.Text
            Else
                sLicServicesAvailable = Me.txtServices.Text & "P"  ' always add P for BabelBank Pro
            End If
            'sLicServicesAvailable = Me.txtServices.Text & "P"  ' always add P for BabelBank Pro
            oFile.WriteLine(LicCrypt("SERVICESAVAILABLE") & " = " & LicCrypt(sLicServicesAvailable))
        End If
        If Val(Me.txtDateResets.Text) > 0 Then
            nLicDateResets = Val(Me.txtDateResets.Text)
            oFile.WriteLine(LicCrypt("DATERESETS") & " = " & LicCrypt(Str(0), True))
        End If
        ' Set datelastused to today
        ' -  this one is used to test that the upgradefile is no more than 60 days old
        sLicDateLastUsed = Trim(Str(Year(Now)) & VB.Right("0" & Trim(Str(Month(Now))), 2) & VB.Right("0" & Trim(Str(VB.Day(Now))), 2))
        oFile.WriteLine(LicCrypt("DATELASTUSED") & " = " & LicCrypt(sLicDateLastUsed, True))

        oFile.Close()

        'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFile = Nothing
        'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFs = Nothing

        ' --- Create a licensekey, which may be keyed in by the enduser
        ' 2 parts;  LIMITRECORDS + LIMITDATE
        ' Show crypted, splitted in 4 parts with - between
        sKey = LicCrypt(Str(nLicLimitRecords), True) & LicCrypt(sLicLimitDate, True)
        ' Add uncrypted as digit 4 LIMITFORMATS
        sKey = VB.Left(sKey, 3) & VB.Right(Str(nLicLimitFormats), 1) & Mid(sKey, 4)
        ' Add uncrypted as digits 5 SUPPORT (0 or 1)
        If sLicSupport = "None" Then
            sKey = VB.Left(sKey, 4) & "0" & Mid(sKey, 5)
        Else
            sKey = VB.Left(sKey, 4) & "1" & Mid(sKey, 5)
        End If
        ' Tilslutt, legg p� to posisjoner for SERVICESAVAILABLE, uncrypted
        sKey = sKey & VB.Right(sLicServicesAvailable, 2)
        ' split in sections of seven
        sKey = VB.Left(sKey, 7) & " " & Mid(sKey, 8, 7) & " " & Mid(sKey, 15, 7) & " " & Mid(sKey, 22, 7)
        txtLicenseCode.Text = sKey

    End Sub
    Private Sub cmdInitialFile_Click()
        ' Create a limited licensefile, to be a part of ordinary setup
        ' This way the user can test BabelBank for 40 days

        Dim oFs As Object 'Scripting
        Dim oFile As Object
        Dim sLine As String

        sLicNumber = "0"
        sLicName = "Midlertidig lisens"
        nLicLimitRecords = 99999
        nLicUsedRecords = 1
        sLicFileName = My.Application.Info.DirectoryPath & "\babel.lic"
        Me.lblLisensfil.Text = sLicFileName

        oFs = CreateObject("Scripting.Filesystemobject")
        ' Erase the licensefile, we're creating a new one:
        If oFs.FileExists(sLicFileName) Then
            oFs.DeleteFile(sLicFileName, True)
        End If

        ' Open licensefile:
        oFile = oFs.OpenTextFile(sLicFileName, 8, True)
        oFile.WriteLine(LicCrypt("NAME") & " = " & LicCrypt(sLicName))
        oFile.WriteLine(LicCrypt("NUMBER") & " = " & LicCrypt(sLicNumber, True))
        oFile.WriteLine(LicCrypt("LIMITRECORDS") & " = " & LicCrypt(Str(nLicLimitRecords), True))
        oFile.WriteLine(LicCrypt("USEDRECORDS") & " = " & LicCrypt(Str(nLicUsedRecords), True))
        ' Set datelastused to today
        sLicDateLastUsed = Trim(Str(Year(Now)) & VB.Right("0" & Trim(Str(Month(Now))), 2) & VB.Right("0" & Trim(Str(VB.Day(Now))), 2))
        oFile.WriteLine(LicCrypt("DATELASTUSED") & " = " & LicCrypt(sLicDateLastUsed, True))

        oFile.Close()
        'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFile = Nothing
        'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFs = Nothing


    End Sub
    Private Sub cmdFullLicense_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFullLicense.Click
        ' Create a complete licensfile, f.ex. to substitute a broken one

        Dim oFs As Object 'Scripting
        Dim oFile As Object
        Dim sLine As String

        Me.cmdMail.Enabled = True
        Me.cmdMailCode.Enabled = True

        sLicNumber = Me.txtNumber.Text
        sLicName = Me.txtName.Text
        sLicSupport = Me.cmbSupport.Text
        nLicLimitRecords = Val(Me.txtRecordsLimit.Text)
        nLicLimitFormats = Val(Me.txtFormatsLimit.Text)
        nLicUsedRecords = Val(Me.txtRecordsUsed.Text)
        ' Set datelastused to today
        sLicDateLastUsed = Trim(Str(Year(Now)) & VB.Right("0" & Trim(Str(Month(Now))), 2) & VB.Right("0" & Trim(Str(VB.Day(Now))), 2))
        ''''sLicLimitDate = frmLicense.txtDateLimit
        sLicLimitDate = "20" & VB.Right(Me.txtDateLimit.Text, 2) & Mid(Me.txtDateLimit.Text, 4, 2) & VB.Left(Me.txtDateLimit.Text, 2)
        'sLicServicesAvailable = Me.txtServices.Text
        If InStr(Me.txtServices.Text, "P") > 0 Then
            sLicServicesAvailable = Me.txtServices.Text
        Else
            sLicServicesAvailable = Me.txtServices.Text & "P"  ' always add P for BabelBank Pro
        End If
        nLicDateResets = Val(Me.txtDateResets.Text)
        sLicFileName = My.Application.Info.DirectoryPath & "\babel.lic"
        Me.lblLisensfil.Text = sLicFileName

        oFs = CreateObject("Scripting.Filesystemobject")
        ' Erase the licensefile, we're creating a new one:
        If oFs.FileExists(sLicFileName) Then
            oFs.DeleteFile(sLicFileName, True)
            ' Also delete babel.update, to avoid problems when sending mail:
            If oFs.FileExists(My.Application.Info.DirectoryPath & "\babel.update") Then
                oFs.DeleteFile(My.Application.Info.DirectoryPath & "\babel.update", True)
            End If
        End If

        ' Open licensefile:
        oFile = oFs.OpenTextFile(sLicFileName, 8, True)
        oFile.WriteLine(LicCrypt("NAME") & " = " & LicCrypt(sLicName))
        oFile.WriteLine(LicCrypt("NUMBER") & " = " & LicCrypt(sLicNumber, True))
        oFile.WriteLine(LicCrypt("SUPPORT") & " = " & LicCrypt(sLicSupport))
        oFile.WriteLine(LicCrypt("LIMITRECORDS") & " = " & LicCrypt(Str(nLicLimitRecords), True))
        oFile.WriteLine(LicCrypt("LIMITFORMATS") & " = " & LicCrypt(Str(nLicLimitFormats), True))
        ' Set datelastused to today
        'sLicDateLastUsed = Str(Year(Now)) + Right$("0" + Trim(Str(Month(Now))), 2) + Right$("0" + Trim(Str(Day(Now))), 2)
        oFile.WriteLine(LicCrypt("LIMITDATE") & " = " & LicCrypt(sLicLimitDate, True))
        oFile.WriteLine(LicCrypt("USEDRECORDS") & " = " & LicCrypt(Str(nLicUsedRecords), True))
        oFile.WriteLine(LicCrypt("DATELASTUSED") & " = " & LicCrypt(sLicDateLastUsed, True))
        oFile.WriteLine(LicCrypt("SERVICESAVAILABLE") & " = " & LicCrypt(sLicServicesAvailable))
        oFile.WriteLine(LicCrypt("DATERESETS") & " = " & LicCrypt(Str(0), True))

        oFile.Close()
        oFile = Nothing
        oFs = Nothing

        ' --- Create a licensekey, which may be keyed in by the enduser
        ' 2 parts;  LIMITRECORDS + LIMITDATE
        ' Show crypted, splitted in 4 parts with - between
        sKey = LicCrypt(Str(nLicLimitRecords), True) & LicCrypt(sLicLimitDate, True)
        ' Add uncrypted as digit 4 LIMITFORMATS
        sKey = VB.Left(sKey, 3) & VB.Right(Str(nLicLimitFormats), 1) & Mid(sKey, 4)
        ' Add uncrypted as digits 5 SUPPORT (0 or 1)
        If sLicSupport = "None" Then
            sKey = VB.Left(sKey, 4) & "0" & Mid(sKey, 5)
        Else
            sKey = VB.Left(sKey, 4) & "1" & Mid(sKey, 5)
        End If
        ' Tilslutt, legg p� to posisjoner for SERVICESAVAILABLE, uncrypted
        sKey = sKey & VB.Right(sLicServicesAvailable, 2)
        ' split in sections of seven
        sKey = VB.Left(sKey, 7) & " " & Mid(sKey, 8, 7) & " " & Mid(sKey, 15, 7) & " " & Mid(sKey, 22, 7)
        txtLicenseCode.Text = sKey


    End Sub
    Private Sub cmdReadFile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdReadFile.Click
        Dim oFs As Scripting.FileSystemObject 'Object
        Dim oFile As Scripting.TextStream
        Dim sLine As String
        Dim sType As String
        Dim sArgument As String
        Dim bOK As Boolean

        sLicFileName = My.Application.Info.DirectoryPath & "\babel.lic"

        sLicNumber = ""
        sLicName = ""
        nLicLimitRecords = 0
        nLicUsedRecords = 0
        sLicDateLastUsed = ""
        sLicLimitDate = ""
        sLicServicesAvailable = ""
        nLicDateResets = 0

        oFs = CreateObject("Scripting.Filesystemobject")
        If Not oFs.FileExists(sLicFileName) Then
            MsgBox("Can't find licensefile", MsgBoxStyle.Critical, "License")
            bOK = False
        Else
            bOK = True
        End If

        If bOK Then
            ' Open licensefile:
            oFile = oFs.OpenTextFile(sLicFileName, Scripting.IOMode.ForReading, False)

            Do While Not oFile.AtEndOfStream = True
                sLine = oFile.ReadLine()
                sType = LicDecrypt(VB.Left(sLine, InStr(sLine, "=") - 2)) ' License property
                sArgument = Trim(LicDecrypt(Mid(sLine, InStr(sLine, "=") + 1))) ' Value in property

                Select Case sType
                    Case "USEDRECORDS" 'No of used paymentrecords
                        nLicUsedRecords = CDbl(sArgument)
                    Case "PREVIOUSRECORDS" 'No of used paymentrecords
                        nLicPreviousRecords = CDbl(sArgument)
                    Case "LIMITRECORDS" 'No of available paymentrecords in license
                        nLicLimitRecords = CDbl(sArgument)
                    Case "LIMITFORMATS" 'No of available formats in license
                        nLicLimitFormats = CShort(sArgument)
                    Case "SERVICESAVAILABLE" 'Which of BabelBanks service is user licensed to use ?
                        sLicServicesAvailable = sArgument
                    Case "DATELASTUSED" 'Date last time records were imported
                        sLicDateLastUsed = sArgument
                    Case "LIMITDATE" 'Timeoutdate if limited license
                        sLicLimitDate = sArgument
                    Case "NUMBER" 'License number
                        sLicNumber = sArgument
                    Case "NAME" 'License registered to
                        sLicName = sArgument
                    Case "SUPPORT" 'Supportcontract
                        sLicSupport = sArgument
                    Case "DATERESETS" ' No of times date is reset (for testlicenses)
                        nLicDateResets = CShort(sArgument)

                End Select

            Loop

            oFile.Close()
            'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oFile = Nothing

            ' Show on screen;
            Me.txtNumber.Text = sLicNumber
            Me.txtName.Text = sLicName
            Me.txtRecordsLimit.Text = Str(nLicLimitRecords)
            Me.txtFormatsLimit.Text = Str(nLicLimitFormats)
            Me.cmbSupport.Text = sLicSupport
            Me.txtRecordsUsed.Text = Str(nLicUsedRecords)
            Me.txtPreviousRecords.Text = Str(nLicPreviousRecords)
            If IsNothing(sLicDateLastUsed) Then
                Me.txtDateLastUsed.Text = VB.Right("0" & Trim(Str(VB.Day(Now))), 2) & "." & VB.Right("0" & Trim(Str(Month(Now))), 2) & "." & Mid(CStr(Year(Now)), 3, 2)
            Else
                Me.txtDateLastUsed.Text = VB.Right(sLicDateLastUsed, 2) & "." & Mid(sLicDateLastUsed, 5, 2) & "." & Mid(sLicDateLastUsed, 3, 2)
            End If

            ' date: dd.mm.yy in form, yyyymmdd in licensefile
            Me.txtDateLimit.Text = VB.Right(sLicLimitDate, 2) & "." & Mid(sLicLimitDate, 5, 2) & "." & Mid(sLicLimitDate, 3, 2)
            Me.txtServices.Text = sLicServicesAvailable
            Me.txtDateResets.Text = Str(nLicDateResets)
        Else
            Me.txtName.Text = "Error opening licensefile"
        End If
        Me.lblLisensfil.Text = sLicFileName
        'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oFs = Nothing
        Me.txtNumber.Focus()


    End Sub
    Private Sub cmdMail_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdMail.Click

        Dim oFs As Scripting.FileSystemObject 'Object
        Dim oFile As Scripting.TextStream
        Dim bOK As Boolean
        Dim sMsgNote As String
        Dim SendMail As String
        Dim myMail As New vbMail

        bOK = True


        If Len(Me.txtEMail.Text) = 0 Or InStr(Me.txtEMail.Text, "@") = 0 Then
            MsgBox("Please specify email-adress!")
            bOK = False
            Me.txtEMail.Focus()
        End If
        oFs = CreateObject("Scripting.Filesystemobject")
        If oFs.FileExists(My.Application.Info.DirectoryPath & "\babel.lic") Then
            sLicFileName = My.Application.Info.DirectoryPath & "\babel.lic"
        ElseIf oFs.FileExists(My.Application.Info.DirectoryPath & "\babel.upgrade") Then
            sLicFileName = My.Application.Info.DirectoryPath & "\babel.upgrade"
        Else
            MsgBox("Can't find licensefile", MsgBoxStyle.Critical, "License")
            bOK = False
        End If

        If bOK Then

            On Error GoTo MAPIFail

            myMail.AddReceivers(Me.txtEMail.Text)

            myMail.Subject = "Lisensfil for BabelBank fra Visual Banking AS"

            sMsgNote = "Vedlagt f�lger lisensfil for BabelBank." & vbCr & vbCr
            sMsgNote = sMsgNote & "Vennligst lagre denne filen med navn BABEL.LIC til den samme katalogen som BabelBanks database er plassert." & vbCr & vbCr
            sMsgNote = sMsgNote & "(Se i menyvalget 'Hjelp', 'Om BabelBank'. Der st�r det nederst 'Database:' - der ser du hvor databasen er plassert.)" & vbCr & vbCr
            sMsgNote = sMsgNote & "I Microsoft Outlook g�r du til menyvalget 'Fil', og velger 'Lagre vedlegg'." & vbCr
            sMsgNote = sMsgNote & "Angi etter dette katalogen hvor BabelBank er installert." & vbCr & vbCr
            sMsgNote = sMsgNote & "Og dersom du ikke forsto s� mye av dette - RING OSS! Tlf. 2318 3600" & vbCr & vbCr

            sMsgNote = sMsgNote & "Med vennlig hilsen" & vbCr
            sMsgNote = sMsgNote & "Visual Banking AS"
            myMail.Body = sMsgNote



            'Add the Attachment at the end of the message
            '.AttachmentPosition = Len(MAPIMessages1.MsgNoteText) (gives error)
            '
            myMail.AddAttachment(sLicFileName)
            myMail.Send()

        End If

        Exit Sub

MAPIFail:
        SendMail = Err.Description

    End Sub

    Friend Function LicDecrypt(ByRef sString As String) As String
        ' Decrypt a string from licenstable
        ' Numerics run through a cdv-control as well
        Dim sResult As String
        Dim i As Short
        Dim bNumeric As Boolean
        Dim nSquarePart As Short
        Dim sFirstPart As String
        Dim sBaseNumber As String
        Dim sChar As String
        Dim nResult As Double


        If VB.Right(sString, 1) = "-" Then
            ' Numeric, special crypting
            bNumeric = True
            sString = VB.Left(sString, Len(sString) - 1)
        End If

        If IsDBNull(sString) Then
            sResult = ""
        Else

            For i = 1 To Len(sString)
                sChar = Mid(sString, i, 1) ' One character at a time
                Select Case sChar
                    Case "i"
                        sResult = sResult & "A"
                    Case "A"
                        sResult = sResult & "B"
                    Case "Q"
                        sResult = sResult & "C"
                    Case "Z"
                        sResult = sResult & "D"
                    Case "y"
                        sResult = sResult & "E"
                    Case "r"
                        sResult = sResult & "F"
                    Case "H"
                        sResult = sResult & "G"
                    Case "E"
                        sResult = sResult & "H"
                    Case "M"
                        sResult = sResult & "I"
                    Case "G"
                        sResult = sResult & "J"
                    Case "N"
                        sResult = sResult & "K"
                    Case "c"
                        sResult = sResult & "L"
                    Case "9"
                        sResult = sResult & "M"
                    Case "F"
                        sResult = sResult & "N"
                    Case "2"
                        sResult = sResult & "O"
                    Case "b"
                        sResult = sResult & "P"
                    Case "5"
                        sResult = sResult & "Q"
                    Case "o"
                        sResult = sResult & "R"
                    Case "L"
                        sResult = sResult & "S"
                    Case "7"
                        sResult = sResult & "T"
                    Case "4"
                        sResult = sResult & "U"
                    Case "S"
                        sResult = sResult & "V"
                    Case "T"
                        sResult = sResult & "W"
                    Case "8"
                        sResult = sResult & "X"
                    Case "6"
                        sResult = sResult & "Y"
                    Case "u"
                        sResult = sResult & "Z"
                    Case "-"
                        sResult = sResult & " "
                    Case "X"
                        sResult = sResult & "0"
                    Case "D"
                        sResult = sResult & "1"
                    Case "j"
                        sResult = sResult & "2"
                    Case "P"
                        sResult = sResult & "3"
                    Case "3"
                        sResult = sResult & "4"
                    Case "1"
                        sResult = sResult & "5"
                    Case "0"
                        sResult = sResult & "6"
                    Case "W"
                        sResult = sResult & "7"
                    Case "V"
                        sResult = sResult & "8"
                    Case "K"
                        sResult = sResult & "9"
                    Case "+"
                        sResult = sResult & "�"
                    Case "/"
                        sResult = sResult & "�"
                    Case "."
                        sResult = sResult & "�"

                End Select
            Next i
        End If

        If bNumeric Then
            ' Have to CONTROL numerics, to ensure that they are not changed

            ' split in two, three last digits is square-part of crypting
            ' that is, the first three digits after square of result
            nSquarePart = Val(VB.Right(sResult, 3))
            sFirstPart = VB.Left(sResult, 8) ' always 8 digits

            ' Check if anything have been changed:
            If Val(VB.Right(Str(Int(System.Math.Sqrt(Val(sFirstPart)) * 1000)), 3)) <> nSquarePart Then
                MsgBox("Error: Noe galt med lisensfilen !", MsgBoxStyle.Critical, "License")
            End If

            ' then subtract 8 from first digit, 7 from second, etc:
            sBaseNumber = ""
            For i = 1 To 8
                sBaseNumber = sBaseNumber & VB.Right(Str(Val(Mid(sFirstPart, i, 1)) + i + 1), 1)
            Next i
            nResult = Val(sBaseNumber)
            sResult = Str(Int(nResult))

        End If
        LicDecrypt = sResult

    End Function
    Friend Function LicCrypt(ByVal sString As String, Optional ByRef bNumeric As Boolean = False) As String
        ' crypt a string to licenstable
        ' Numerics run through a cdv-control as well
        Dim i As Short
        Dim sChar As String
        Dim sResult As String
        Dim sTemp As String
        Dim nSquare As Double

        sResult = ""
        sString = Trim(sString)

        If bNumeric Then
            ' extra control for numerics

            ' Always 8 digits
            sResult = VB.Right("00000000" & Trim(sString), 8)
            ' scramble, to make it tougher to read
            ' 1. digit is added 8, 2. digit is added 7, etc, last is added 1
            sTemp = sResult
            sResult = ""
            For i = 1 To 8
                sResult = sResult & VB.Right(Str(Val(CStr(CDbl(Mid(sTemp, i, 1)) + (9 - i)))), 1) ' last digit
            Next i
            ' Square root of scrambled 8-digit string
            nSquare = System.Math.Sqrt(Val(sResult))
            ' take first three decimaldigits og square root:
            nSquare = Int(nSquare * 1000)
            ' place these three digits as last three in numeric-result
            sResult = sResult & VB.Right(Str(nSquare), 3)
            sString = sResult
        End If

        sResult = ""

        For i = 1 To Len(sString)
            sChar = UCase(Mid(sString, i, 1)) ' One character at a time
            Select Case sChar
                Case "A"
                    sResult = sResult & "i"
                Case "B"
                    sResult = sResult & "A"
                Case "C"
                    sResult = sResult & "Q"
                Case "D"
                    sResult = sResult & "Z"
                Case "E"
                    sResult = sResult & "y"
                Case "F"
                    sResult = sResult & "r"
                Case "G"
                    sResult = sResult & "H"
                Case "H"
                    sResult = sResult & "E"
                Case "I"
                    sResult = sResult & "M"
                Case "J"
                    sResult = sResult & "G"
                Case "K"
                    sResult = sResult & "N"
                Case "L"
                    sResult = sResult & "c"
                Case "M"
                    sResult = sResult & "9"
                Case "N"
                    sResult = sResult & "F"
                Case "O"
                    sResult = sResult & "2"
                Case "P"
                    sResult = sResult & "b"
                Case "Q"
                    sResult = sResult & "5"
                Case "R"
                    sResult = sResult & "o"
                Case "S"
                    sResult = sResult & "L"
                Case "T"
                    sResult = sResult & "7"
                Case "U"
                    sResult = sResult & "4"
                Case "V"
                    sResult = sResult & "S"
                Case "W"
                    sResult = sResult & "T"
                Case "X"
                    sResult = sResult & "8"
                Case "Y"
                    sResult = sResult & "6"
                Case "Z"
                    sResult = sResult & "u"
                Case "�"
                    sResult = sResult & "+"
                Case "�"
                    sResult = sResult & "/"
                Case "�"
                    sResult = sResult & "."
                Case " "
                    sResult = sResult & "-"
                Case "0"
                    sResult = sResult & "X"
                Case "1"
                    sResult = sResult & "D"
                Case "2"
                    sResult = sResult & "j"
                Case "3"
                    sResult = sResult & "P"
                Case "4"
                    sResult = sResult & "3"
                Case "5"
                    sResult = sResult & "1"
                Case "6"
                    sResult = sResult & "0"
                Case "7"
                    sResult = sResult & "W"
                Case "8"
                    sResult = sResult & "V"
                Case "9"
                    sResult = sResult & "K"
            End Select
        Next i

        ' Put a sign on numerics, to tell the decrypt function that we have a numeric
        If bNumeric Then
            sResult = sResult & "-"
        End If
        LicCrypt = sResult


    End Function


    Private Sub txtNumber_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumber.Enter
        txtNumber.SelectionStart = 0
        txtNumber.SelectionLength = Len(txtNumber.Text)
    End Sub
    Private Sub txtName_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtName.Enter
        txtName.SelectionStart = 0
        txtName.SelectionLength = Len(txtName.Text)
    End Sub
    Private Sub txtrecordslimit_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtRecordsLimit.Enter
        txtRecordsLimit.SelectionStart = 0
        txtRecordsLimit.SelectionLength = Len(txtRecordsLimit.Text)
    End Sub
    Private Sub txtformatslimit_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFormatsLimit.Enter
        txtFormatsLimit.SelectionStart = 0
        txtFormatsLimit.SelectionLength = Len(txtFormatsLimit.Text)
    End Sub

    Private Sub txtrecordsused_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtRecordsUsed.Enter
        txtRecordsUsed.SelectionStart = 0
        txtRecordsUsed.SelectionLength = Len(txtRecordsUsed.Text)
    End Sub
    Private Sub txtdatelimit_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDateLimit.Enter
        txtDateLimit.SelectionStart = 0
        txtDateLimit.SelectionLength = Len(txtDateLimit.Text)
    End Sub
    Private Sub txtservices_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServices.Enter
        txtServices.SelectionStart = 0
        txtServices.SelectionLength = Len(txtServices.Text)
    End Sub
    Private Sub txtservices_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServices.Leave
        txtServices.Text = UCase(txtServices.Text)
        If InStr(" APTVSEBPDNBXVISMAGIC", Trim(txtServices.Text)) = 0 Then
            MsgBox("Du kan kun bruke kodene SEB, T, V, DNBX eller blank")
            Me.txtServices.Focus()
        End If
    End Sub
    Private Sub txtdateresets_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDateResets.Enter
        txtDateResets.SelectionStart = 0
        txtDateResets.SelectionLength = Len(txtDateResets.Text)
    End Sub
    Private Sub txtemail_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEMail.Enter
        txtEMail.SelectionStart = 0
        txtEMail.SelectionLength = Len(txtEMail.Text)
    End Sub
    Private Sub txtDatelimit_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDateLimit.Validating
        Dim KeepFocus As Boolean = eventArgs.Cancel
        ' If the value is not a date, keep the focus, unless the user
        ' clicks Help.
        'If Not IsDate(txtDateLimit.Text) Then
        '    KeepFocus = True
        '    MsgBox("Wrong dateformat")
        'End If
        If Not IsNumeric(Replace(txtDateLimit.Text, ".", "")) And Len(Replace(txtDateLimit.Text, ".", "")) <> 6 Then
            KeepFocus = True
            MsgBox("Wrong dateformat")
        End If
        If Val(VB.Left(txtDateLimit.Text, 2)) > 31 Then
            KeepFocus = True
            MsgBox("Wrong dateformat")
        End If
        If Val(Mid(txtDateLimit.Text, 4, 2)) > 12 Then
            KeepFocus = True
            MsgBox("Wrong dateformat")
        End If
        If Val(Mid(txtDateLimit.Text, 7, 2)) < 1 Then
            KeepFocus = True
            MsgBox("Wrong dateformat")
        End If
        'sString = txtDateLimit.Text
        'StringToDate = CDate(DateSerial(Left$(sString, 4), Mid$(sString, 5, 2), Right$(sString, 2)))
        If CDate(DateSerial(CInt("20" & VB.Right(txtDateLimit.Text, 2)), CInt(Mid(txtDateLimit.Text, 4, 2)), CInt(VB.Left(txtDateLimit.Text, 2)))) <= Now Then
            KeepFocus = True
            MsgBox("Date must be forward in time")
        End If
        eventArgs.Cancel = KeepFocus
    End Sub
    Private Sub txtDatelastused_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDateLastUsed.Validating
        Dim KeepFocus As Boolean = eventArgs.Cancel
        ' If the value is not a date, keep the focus, unless the user
        ' clicks Help.
        If Not IsDate(txtDateLastUsed.Text) Then
            KeepFocus = True
            MsgBox("Wrong dateformat")
        End If
        If Not IsNumeric(Replace(txtDateLimit.Text, ".", "")) And Len(Replace(txtDateLimit.Text, ".", "")) <> 6 Then
            KeepFocus = True
            MsgBox("Wrong dateformat")
        End If
        If Val(VB.Left(txtDateLimit.Text, 2)) > 31 Then
            KeepFocus = True
            MsgBox("Wrong dateformat")
        End If
        If Val(Mid(txtDateLimit.Text, 4, 2)) > 12 Then
            KeepFocus = True
            MsgBox("Wrong dateformat")
        End If
        If Val(Mid(txtDateLimit.Text, 7, 2)) < 1 Then
            KeepFocus = True
            MsgBox("Wrong dateformat")
        End If

        eventArgs.Cancel = KeepFocus
    End Sub
    Private Sub txtnumber_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtNumber.Validating
        Dim KeepFocus As Boolean = eventArgs.Cancel
        ' Digits only
        If Not IsNumeric(txtNumber.Text) Then
            KeepFocus = True
            MsgBox("Numbers only")
        End If
        If Len(txtNumber.Text) <> 5 Then
            MsgBox("5 digits in licensenumber")
        End If
        eventArgs.Cancel = KeepFocus
    End Sub
    Private Sub txtname_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtName.Validating
        Dim KeepFocus As Boolean = eventArgs.Cancel
        If Len(txtName.Text) < 1 Then
            KeepFocus = True
            MsgBox("Must specify a name")
        End If
        eventArgs.Cancel = KeepFocus
    End Sub
    Private Sub txtFormatsLimit_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtFormatsLimit.Validating
        Dim KeepFocus As Boolean = eventArgs.Cancel
        ' Digits only
        If Not IsNumeric(txtFormatsLimit.Text) Then
            KeepFocus = True
            MsgBox("Numbers only")
        End If
        If Len(txtFormatsLimit.Text) > 2 Then
            MsgBox("Max 99 formats")
        End If
        eventArgs.Cancel = KeepFocus
    End Sub

    Private Sub cmdCodeSQLDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCodeSQLDatabase.Click
        ' Beregn en kode som skal brukes n�r man fra menyen Fil, Opprett SQL server database oppretter en ny, tom SQL server database
        Dim nMonth As Integer = Microsoft.VisualBasic.DateAndTime.Month(Today)
        Dim nDay As Integer = Microsoft.VisualBasic.DateAndTime.Day(Today)
        Dim nYear As Integer = Microsoft.VisualBasic.DateAndTime.Year(Today)
        sKey = Math.Truncate(nYear / nDay * nMonth * 3.14)
        ' show key 
        Me.txtLicenseCode.Text = sKey
        Me.cmdMailCode.Enabled = True

    End Sub

    Private Sub cmdCodeThreeYearsLicense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCodeThreeYearsLicense.Click
        ' beregn en kode som skal brukes for � forlenge gjeldende lisens slik at den gjelder til 10. mars om 3 �r
        Dim nYear As Integer = Microsoft.VisualBasic.DateAndTime.Year(Today) + 3
        Dim sLicLastRegDate As String = ""
        ' tar hensyn til at det skal v�re oppgitt et lisensnr (h�yere enn 31000)
        ' lisensen m� brukes innen 5 dager
        sLicNumber = Me.txtNumber.Text.Trim
        If Val(sLicNumber) < 31001 Then
            MsgBox("Licensenumber must be higher than 31000!")
            Me.txtNumber.Focus()

        Else
            sLicLastRegDate = Mid(CStr(Year(DateAdd(DateInterval.Day, 5, Now))), 1, 4) & VB.Right("0" & Trim(Str(Month(DateAdd(DateInterval.Day, 5, Now)))), 2) & VB.Right("0" & Trim(Str(VB.Day(DateAdd(DateInterval.Day, 5, Now)))), 2)
            sLicLimitDate = nYear.ToString & "0310"
            sKey = LicCrypt(sLicNumber, False) & LicCrypt(sLicLastRegDate, False) & LicCrypt(sLicLimitDate, False)

            ' split in sections of seven
            sKey = VB.Left(sKey, 7) & " " & Mid(sKey, 8, 7) & " " & Mid(sKey, 15, 7) & " " & Mid(sKey, 22, 7)

            ' show key 
            Me.txtLicenseCode.Text = sKey
            Me.cmdMailCode.Enabled = True
        End If
    End Sub

    Private Sub cmdCodeOneYearLicense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCodeOneYearLicense.Click
        ' beregn en kode som skal brukes for � forlenge gjeldende lisens slik at den gjelder til 10. mars om 1 �r
        Dim nYear As Integer = Microsoft.VisualBasic.DateAndTime.Year(Today) + 1
        Dim sLicLastRegDate As String = ""
        ' tar hensyn til at det skal v�re oppgitt et lisensnr (h�yere enn 31000)
        ' lisensen m� brukes innen 5 dager
        sLicNumber = Me.txtNumber.Text.Trim
        If Val(sLicNumber) < 31001 Then
            MsgBox("Licensenumber must be higher than 31000!")
            Me.txtNumber.Focus()

        Else
            sLicLastRegDate = Mid(CStr(Year(DateAdd(DateInterval.Day, 5, Now))), 1, 4) & VB.Right("0" & Trim(Str(Month(DateAdd(DateInterval.Day, 5, Now)))), 2) & VB.Right("0" & Trim(Str(VB.Day(DateAdd(DateInterval.Day, 5, Now)))), 2)
            sLicLimitDate = nYear.ToString & "0310"
            sKey = LicCrypt(sLicNumber, False) & LicCrypt(sLicLastRegDate, False) & LicCrypt(sLicLimitDate, False)

            ' split in sections of seven
            sKey = VB.Left(sKey, 7) & " " & Mid(sKey, 8, 7) & " " & Mid(sKey, 15, 7) & " " & Mid(sKey, 22, 7)

            ' show key 
            Me.txtLicenseCode.Text = sKey
            Me.cmdMailCode.Enabled = True
        End If

    End Sub

    Private Sub cmdCode5DayLicense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCode5DayLicense.Click
        ' beregn en kode som skal brukes for � forlenge gjeldende lisens slik at den gjelder til 5 dager fram i tid
        Dim dLimitDate As Date = DateAdd(DateInterval.Day, 5, Now)

        Dim sLicLastRegDate As String = ""
        ' tar hensyn til at det skal v�re oppgitt et lisensnr (h�yere enn 31000)
        ' lisensen m� brukes innen 5 dager
        sLicNumber = Me.txtNumber.Text.Trim
        If Val(sLicNumber) < 31001 Then
            MsgBox("Licensenumber must be higher than 31000!")
            Me.txtNumber.Focus()

        Else
            sLicLastRegDate = Mid(CStr(Year(DateAdd(DateInterval.Day, 5, Now))), 1, 4) & VB.Right("0" & Trim(Str(Month(DateAdd(DateInterval.Day, 5, Now)))), 2) & VB.Right("0" & Trim(Str(VB.Day(DateAdd(DateInterval.Day, 5, Now)))), 2)
            sLicLimitDate = Mid(CStr(Year(dLimitDate)), 1, 4) & VB.Right("0" & Trim(Str(Month(dLimitDate))), 2) & VB.Right("0" & Trim(Str(VB.Day(dLimitDate))), 2)
            sKey = LicCrypt(sLicNumber, False) & LicCrypt(sLicLastRegDate, False) & LicCrypt(sLicLimitDate, False)

            ' split in sections of seven
            sKey = VB.Left(sKey, 7) & " " & Mid(sKey, 8, 7) & " " & Mid(sKey, 15, 7) & " " & Mid(sKey, 22, 7)

            ' show key 
            Me.txtLicenseCode.Text = sKey
            Me.cmdMailCode.Enabled = True
        End If

    End Sub
End Class