﻿Public Class vbMail
    ' Send Smtp OR Mapi mails from BabelBank with this class
    Private s_eMailSender As String
    Private s_eMailSenderDisplayName As String
    Private s_eMailReplyTo As String

    Private aMailDisplayName() As String
    Private aMailReceiver() As String

    Private aMailCC() As String
    Private aMailCCDisplayName() As String

    Private aMailBcc() As String
    Private aMailBccDisplayName() As String

    Private s_eMailSMTPHost As String
    Private s_EmailSMTPPort As Boolean

    Private s_Subject As String
    Private s_Body As String
    Private aAttachments() As String
    Private b_Email_SMTP As Boolean
    Private s_UserName As String
    Private s_PassWord As String
    Private bSuccess As Boolean

    ' MAPI-constants
    Const mapOrigList = 0 'The message originator. 
    Const mapToList = 1 'The recipient is a primary recipient. 
    Const mapCcList = 2 'The recipient is a copy recipient. 
    Const mapBccList = 3 'The recipient is a blind copy recipient. 
    Public Sub New()
        s_eMailSender = ""
        s_eMailSenderDisplayName = ""
        aMailDisplayName = Nothing
        aMailCC = Nothing
        aMailCCDisplayName = Nothing
        aMailBcc = Nothing
        aMailBccDisplayName = Nothing
        s_eMailReplyTo = ""
        s_eMailSMTPHost = ""
        s_eMailSMTPHost = 0  '25
        b_Email_SMTP = False  ' default to MAPI
        aMailReceiver = Nothing
        s_Subject = ""
        aAttachments = Nothing
        s_UserName = ""
        s_PassWord = ""
        bSuccess = True
    End Sub
    Public Property EMail_SMTP() As Boolean
        ' SMTP or MAPI ?
        Get
            EMail_SMTP = b_Email_SMTP
        End Get
        Set(ByVal Value As Boolean)
            b_Email_SMTP = Value
        End Set
    End Property
    Public Property Subject() As String
        Get
            Subject = s_Subject
        End Get
        Set(ByVal Value As String)
            s_Subject = Value
        End Set
    End Property
    Public Property Body() As String
        Get
            Body = s_Body
        End Get
        Set(ByVal Value As String)
            s_Body = Value
        End Set
    End Property
    Public Property eMailSender() As String
        Get
            eMailSender = s_eMailSender
        End Get
        Set(ByVal Value As String)
            s_eMailSender = Value
        End Set
    End Property
    Public Property eMailSenderDisplayName() As String
        Get
            eMailSenderDisplayName = s_eMailSenderDisplayName
        End Get
        Set(ByVal Value As String)
            s_eMailSenderDisplayName = Value
        End Set
    End Property
    Public Property eMailReplyTo() As String
        Get
            eMailReplyTo = s_eMailReplyTo
        End Get
        Set(ByVal Value As String)
            s_eMailReplyTo = Value
        End Set
    End Property
    Public Property eMailSMTPHost() As String
        Get
            eMailSMTPHost = s_eMailSMTPHost
        End Get
        Set(ByVal Value As String)
            s_eMailSMTPHost = Value
        End Set
    End Property
    Public Property eMailSMTPPort() As String
        Get
            eMailSMTPPort = s_EmailSMTPPort
        End Get
        Set(ByVal Value As String)
            s_EmailSMTPPort = Value
        End Set
    End Property
    Public Property UserName() As String
        Get
            s_UserName = s_UserName
        End Get
        Set(ByVal Value As String)
            s_UserName = Value
        End Set
    End Property
    Public Property Password() As String
        Get
            Password = s_PassWord
        End Get
        Set(ByVal Value As String)
            s_PassWord = Value
        End Set
    End Property
    Public ReadOnly Property Success() As Boolean
        Get
            Success = bSuccess
        End Get
    End Property
    Public Sub AddReceivers(ByVal sEmail As String, Optional ByVal sDisplayName As String = "")
        If sEmail <> "" Then
            If sDisplayName = "" Then
                sDisplayName = sEmail
            End If

            If Array_IsEmpty(aMailReceiver) Then
                ReDim aMailReceiver(0)
                ReDim aMailDisplayName(0)
                aMailReceiver(0) = sEmail
                aMailDisplayName(0) = sDisplayName
            Else
                ReDim Preserve aMailReceiver(UBound(aMailReceiver) + 1)
                ReDim Preserve aMailDisplayName(UBound(aMailDisplayName) + 1)
                aMailReceiver(UBound(aMailReceiver)) = sEmail
                aMailDisplayName(UBound(aMailDisplayName)) = sDisplayName
            End If
        End If

    End Sub
    Public Sub AddCCReceivers(ByVal sEmail As String, Optional ByVal sDisplayName As String = "")
        If sEmail <> "" Then
            If sDisplayName = "" Then
                sDisplayName = sEmail
            End If
            If Array_IsEmpty(aMailCC) Then
                ReDim aMailCC(0)
                ReDim aMailCCDisplayName(0)
                aMailCC(0) = sEmail
                aMailCCDisplayName(0) = sDisplayName
            Else
                ReDim Preserve aMailCC(UBound(aMailCC) + 1)
                ReDim Preserve aMailCCDisplayName(UBound(aMailCCDisplayName) + 1)
                aMailCC(UBound(aMailCC)) = sEmail
                aMailCCDisplayName(UBound(aMailCCDisplayName)) = sDisplayName
            End If
        End If
    End Sub
    Public Sub AddBCCReceivers(ByVal sEmail As String, Optional ByVal sDisplayName As String = "")
        If sEmail <> "" Then
            If sDisplayName = "" Then
                sDisplayName = sEmail
            End If
            If Array_IsEmpty(aMailBcc) Then
                ReDim aMailBcc(0)
                ReDim aMailBccDisplayName(0)
                aMailBcc(0) = sEmail
                aMailBccDisplayName(0) = sDisplayName
            Else
                ReDim Preserve aMailBcc(UBound(aMailBcc) + 1)
                ReDim Preserve aMailBccDisplayName(UBound(aMailBccDisplayName) + 1)
                aMailBcc(UBound(aMailBcc)) = sEmail
                aMailBccDisplayName(UBound(aMailBccDisplayName)) = sDisplayName
            End If
        End If
    End Sub
    Public Sub AddAttachment(ByVal sFilename As String)
        If sFilename <> "" Then
            If Array_IsEmpty(aAttachments) Then
                ReDim aAttachments(0)
                aAttachments(0) = sFilename
            Else
                ReDim Preserve aAttachments(UBound(aAttachments) + 1)
                aAttachments(UBound(aAttachments)) = sFilename
            End If
        End If
    End Sub
    Public Sub Send()
        ' send the message
        Dim sTmpTo As String
        Dim i As Integer
        Dim AttachmentCollection As System.Net.Mail.AttachmentCollection
        Dim attachment As System.Net.Mail.Attachment
        Dim iReturn As Integer

        bSuccess = True
        ' is it SMPT og MAPI ?
        If b_Email_SMTP Then

            '-----------------
            ' SMTP-mail
            '-----------------

            Dim msg As New System.Net.Mail.MailMessage
            Dim smtp As New System.Net.Mail.SmtpClient

            Try
                If s_eMailSender <> "" Then
                    msg.From = New System.Net.Mail.MailAddress(s_eMailSender, s_eMailSenderDisplayName)
                End If
                If Not Array_IsEmpty(aMailReceiver) Then
                    For i = 0 To UBound(aMailReceiver)
                        msg.To.Add(New System.Net.Mail.MailAddress(aMailReceiver(i), aMailDisplayName(i)))
                    Next i
                End If

                If Not Array_IsEmpty(aMailCC) Then
                    For i = 0 To UBound(aMailCC)
                        msg.CC.Add(New System.Net.Mail.MailAddress(aMailCC(i), aMailCCDisplayName(i)))
                    Next i
                End If

                If Not Array_IsEmpty(aMailBcc) Then
                    For i = 0 To UBound(aMailBcc)
                        msg.Bcc.Add(New System.Net.Mail.MailAddress(aMailBcc(i), aMailBccDisplayName(i)))
                    Next i
                End If

                msg.Subject = s_Subject
                msg.Body = s_Body
                If s_eMailSMTPHost <> "" Then
                    smtp.Host = s_eMailSMTPHost
                End If
                If s_EmailSMTPPort > 0 Then
                    smtp.Port = s_EmailSMTPPort
                End If
                smtp.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
                smtp.Credentials = New System.Net.NetworkCredential(s_UserName, s_PassWord)

                ' Attachments
                If Not Array_IsEmpty(aAttachments) Then
                    For i = 0 To UBound(aAttachments)
                        ' add attachmentfiles, one by one
                        If Len(Dir(aAttachments(i))) > 0 Then
                            AttachmentCollection = msg.Attachments()
                            attachment = New System.Net.Mail.Attachment(aAttachments(i))
                            AttachmentCollection.Add(attachment)
                        End If

                    Next i
                End If

                smtp.Send(msg)
                ' 06.11.2015 added next if
                If Not Array_IsEmpty(aAttachments) Then
                    attachment.Dispose()
                    attachment = Nothing
                    AttachmentCollection.Clear()
                    AttachmentCollection.Dispose()
                    AttachmentCollection = Nothing
                End If

                msg = Nothing
                smtp = Nothing

            Catch ex As Exception
                bSuccess = False
                MessageBox.Show(ex.Message)
                msg = Nothing
                smtp = Nothing
            End Try

        Else
            '----------------
            ' MAPI
            '----------------
            Dim myMapi As New MAPI

            Try
                If Not Array_IsEmpty(aMailReceiver) Then
                    For i = 0 To UBound(aMailReceiver)
                        myMapi.AddRecipientTo(aMailReceiver(i))
                    Next i
                End If

                If Not Array_IsEmpty(aMailCC) Then
                    For i = 0 To UBound(aMailCC)
                        myMapi.AddRecipientCC(aMailCC(i))
                    Next i
                End If

                If Not Array_IsEmpty(aMailBcc) Then
                    For i = 0 To UBound(aMailBcc)
                        myMapi.AddRecipientBCC(aMailBcc(i))
                    Next i
                End If


                ' Attachments
                If Not Array_IsEmpty(aAttachments) Then
                    For i = 0 To UBound(aAttachments)
                        ' add attachmentfiles, one by one
                        If Len(Dir(aAttachments(i))) > 0 Then
                            myMapi.AddAttachment(aAttachments(i))
                        End If
                    Next i
                End If
                iReturn = myMapi.SendMailPopup(s_Subject, s_Body)
                If iReturn <> 0 Then
                    bSuccess = False
                End If

                myMapi = Nothing

            Catch ex As Exception
                bSuccess = False
                MessageBox.Show(myMapi.GetLastError & vbCrLf & ex.Message)
                myMapi = Nothing
            End Try


        End If
    End Sub
    Public Function Array_IsEmpty(ByRef x As Object) As Boolean
        ' test for empty arrays

        Dim DummyVAL As Integer '// Dummy Variable

        '// Set to Trap Error, if Any
        On Error Resume Next

        '08.12.2010
        If x Is Nothing Then
            Array_IsEmpty = True
            Exit Function
        End If

        '// Try and get the Lower Bound of the Array
        DummyVAL = LBound(x)

        '// If an Error is thrown off then the array is not dimmed
        If (Err.Number > 0) Then
            Array_IsEmpty = True
        Else
            Array_IsEmpty = False
        End If

    End Function
End Class
