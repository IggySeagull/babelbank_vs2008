Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("Correct_NET.Correct")> Public Class Correct
	' Class for displaying information about one payment,
	' with functionality for changing and deleting
    Private oBabelFiles As vbBabel.BabelFiles
    Private oBatch As vbBabel.Batch
    Private oPayment As vbBabel.Payment
    Private sFilename As String
    Private aInfoArray(,) As Object
    Private iCorrectReturn As Integer  ' 1-4 depending on user exit from frmCorrect
    ' XNET 21.08.2012 - new option; Show all payments in frmCorrect, with possibility to change info. Some fields locked
    Private bShowAllPayments As Boolean
    ' XNET 31.01.2013 - added possibilty to edit almost all info in frmCorrect
    Private bEditAll As Boolean
    '********* START PROPERTY SETTINGS ***********************
	Public WriteOnly Property BabelFiles() As vbbabel.BabelFiles
		Set(ByVal Value As vbbabel.BabelFiles)
			oBabelFiles = Value
		End Set
	End Property
	Public WriteOnly Property Filename() As String
		Set(ByVal Value As String)
			sFilename = Value
		End Set
	End Property
    Public WriteOnly Property InfoArray() As Object(,)
        Set(ByVal Value(,) As Object)
            ' Element 0: Index pointing to errounous payment; 0,0,0,0,0
            '            BabelFile , Batch, Payment, Invoice, FreeText
            ' Element 1: Name of errorfields
            ' Element 2: Errorstring for actual field with error
            ' Element 3: ErrorType; 0 = Warning  1 = Error
            ' Element 4: 0 = Do not allow edit   1 = Allow edit
            ' Element 5: Additional text. If not empty, use this text in field. Use for payType

            aInfoArray = VB6.CopyArray(Value)
        End Set
    End Property
	Public ReadOnly Property CorrectReturn() As Short
		Get
			CorrectReturn = iCorrectReturn
		End Get
	End Property
    ' XNET 21.08.2012 - added method to set ShowAll payments
    Public WriteOnly Property ShowAll() As Boolean
        Set(ByVal Value As Boolean)
            bShowAllPayments = Value
        End Set
    End Property
    ' XNET 31.01.2013 - added possibilty to edit almost all info in frmCorrect
    Public WriteOnly Property EditAll() As Boolean
        Set(ByVal Value As Boolean)
            bEditAll = Value
        End Set
    End Property
    '********* END PROPERTY SETTINGS ***********************

    Public Sub Class_Terminate_Renamed()
    End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
	Public Sub Show()
		Dim rp_901_Corrections As Object

        Dim i, j As Integer
		Dim sErrField As String
		Dim sErrString As String
		Dim bAllowEdit As Boolean
		Dim bError As Boolean
		Dim oBabelFile As BabelFile
		Dim oInvoice As Invoice
		Dim oFreeText As Freetext
		Dim iNoOfErrors As Integer
		Dim iNoOfWarnings As Integer
		Dim sNameAdress As String
		Dim sFreetextKID As String
		Dim bEditNameAddress As Boolean
		Dim lInvoiceCounter, lFreetextCounter As Integer
		Dim lBatchIndex, lBabelIndex, lPaymentIndex As Integer
		Dim lInvoiceIndex As Integer
		Dim aErrorArray(,) As String
        Dim sTmp As String
		Dim iNoOfHyphens As Short

        Dim frmCorrect As New frmCorrect
        Dim frmCorrectNameAddress As New frmCorrectNameAddress
        Dim frmCorrectBank As New frmCorrectBank
        Dim frmCorrectOther As New frmCorrectOther

        lBabelIndex = CInt(xDelim(CStr(aInfoArray(0, 0)), ",", 1))
        lBatchIndex = CInt(xDelim(CStr(aInfoArray(0, 0)), ",", 2))
        lPaymentIndex = CInt(xDelim(CStr(aInfoArray(0, 0)), ",", 3))
        lInvoiceIndex = CInt(xDelim(CStr(aInfoArray(0, 0)), ",", 4))
		'lFreetextIndex
		
		oBabelFile = oBabelFiles(lBabelIndex)
		' Find batch
		oBatch = oBabelFile.Batches(lBatchIndex)
		' Find payment;
		oPayment = oBatch.Payments(lPaymentIndex)
		
        ' Display payment on screen, frmCorrect
        frmCorrect.txtFilename.Text = sFilename
        frmCorrect.txtTotalAmount.Text = Format(oBabelFile.MON_InvoiceAmount / 100, "##,##0.00")
        frmCorrect.txtNoOfPayments.Text = oBabelFile.No_Of_Transactions
        frmCorrect.txtI_Name.Text = oPayment.I_Name
        frmCorrect.txtI_Account.Text = oPayment.I_Account
        frmCorrect.txtCompanyNo.Text = oBabelFile.IDENT_Sender
        frmCorrect.txtPayOwnRef.Text = oPayment.REF_Own
        If aInfoArray(5, 0) = "FI Payment" Or aInfoArray(5, 0) = "General Payment" Or aInfoArray(5, 0) = "ACH Payment" Or aInfoArray(5, 0) = "Salary" Then
            sNameAdress = oPayment.E_Name & vbCrLf & oPayment.E_Adr1 & vbCrLf & oPayment.E_Adr2
            sNameAdress = sNameAdress & vbCrLf & oPayment.E_Adr3 & " " & oPayment.E_Zip
            sNameAdress = sNameAdress & " " & oPayment.E_City ' & vbCrLf & oPayment.E_CountryCode
        Else
            sNameAdress = oPayment.E_Name & vbCrLf & oPayment.E_Adr1 & vbCrLf & oPayment.E_Adr2
            sNameAdress = sNameAdress & vbCrLf & oPayment.E_Adr3 & vbCrLf & oPayment.E_Zip
            sNameAdress = sNameAdress & " " & oPayment.E_City ' & vbCrLf & oPayment.E_CountryCode
        End If
        frmCorrect.txtE_Name_Adress.Text = sNameAdress
        frmCorrect.txtE_Account.Text = oPayment.E_Account
        frmCorrect.txtDATE_Payment.Text = StringToDate(oPayment.DATE_Payment)
        'frmCorrect.DTDate_Payment.Value = StringToDate(oPayment.DATE_Payment)
        frmCorrect.txtDate_Payment.Text = StringToDate(oPayment.DATE_Payment)
        frmCorrect.txtPayMON_InvoiceAmount.Text = Format(oPayment.MON_InvoiceAmount / 100, "##,##0.00")
        frmCorrect.txtMON_InvoiceCurrency.Text = oPayment.MON_InvoiceCurrency
        frmCorrect.txtPayType.Text = oPayment.PayType
        If Trim(CStr(oPayment.Cheque)) = "" Or CStr(oPayment.Cheque) = "0" Then
            frmCorrect.chkCheque.CheckState = System.Windows.Forms.CheckState.Unchecked
        Else
            frmCorrect.chkCheque.CheckState = System.Windows.Forms.CheckState.Checked
        End If

        ' Fill info from first invoice into txtControls
        'frmCorrect.txtInvMON_InvoiceAmount.Text = Format(oPayment.Invoices(1).MON_InvoiceAmount / 100, "##,##0.00")
        ' changed 13.08.03
        ' XokNET 07.05.2014 Changed If test, because sometimes when invoices are deleted by enduser
        If oPayment.Invoices.Count > 0 And Not (oPayment.Invoices.Count < lInvoiceIndex) Then
            frmCorrect.txtInvMON_InvoiceAmount.Text = Format(oPayment.Invoices(lInvoiceIndex).MON_InvoiceAmount / 100, "##,##0.00")
            frmCorrect.txtInvOwnRef.Text = oPayment.Invoices(1).REF_Own
            'If oPayment.Invoices(1).Freetexts.Count = 0 Then
            ' 20.03.2009 changed regarding Reference Finland
            If oPayment.Invoices(1).Freetexts.Count = 0 Or _
                (oPayment.DnBNORTBIPayType = "REF" And Not EmptyString(oPayment.Invoices(1).Unique_Id)) Then
                ' KID
                sFreetextKID = oPayment.Invoices(1).Unique_Id
            Else
                ' Freetext
                sFreetextKID = vbNullString
                For Each oFreeText In oPayment.Invoices(1).Freetexts
                    sFreetextKID = sFreetextKID & oFreeText.Text
                Next
            End If
        End If
        frmCorrect.txtFreetext.Text = sFreetextKID

        ' Fill frmCorrectBank
        ' Label for txtBank_Info dependant on noti_notificationparty
        If oPayment.NOTI_NotificationParty = 1 Then
            ' Info to payers bank
            frmCorrectBank.lblBank_Info.Text = LRS(60127) 'Info to payers bank
        Else
            frmCorrectBank.lblBank_Info.Text = LRS(60144) 'Info to ben.bank
        End If

        frmCorrectBank.txtBank_Info.Text = oPayment.NOTI_NotificationMessageToBank
        If oPayment.BANK_BranchType > vbBabel.BabelFiles.BankBranchType.SWIFT Then
            frmCorrectBank.txtBank_ID.Text = oPayment.BANK_BranchNo
        Else
            frmCorrectBank.txtBank_ID.Text = oPayment.BANK_SWIFTCode
        End If
        'frmCorrectBank.cmbBank_IDType.AddItem vbNullString
        frmCorrectBank.cmbBank_IDType.Items.Clear()
        frmCorrectBank.cmbBank_IDType.Items.Add("SWIFT")
        frmCorrectBank.cmbBank_IDType.Items.Add("FedWire")
        frmCorrectBank.cmbBank_IDType.Items.Add("SortCode")
        frmCorrectBank.cmbBank_IDType.Items.Add("Bankleitzahl")
        frmCorrectBank.cmbBank_IDType.Items.Add("CHIPS")
        frmCorrectBank.cmbBank_IDType.Items.Add("US_ABA")
        frmCorrectBank.cmbBank_IDType.Items.Add("Bankleitzahl_Austria")
        frmCorrectBank.cmbBank_IDType.Items.Add("MEPS")
        frmCorrectBank.cmbBank_IDType.Items.Add("BSB")  ' added 30.03.2010, Australia
        frmCorrectBank.cmbBank_IDType.Items.Add("CC")  ' XOKNET added 19.05.2011 Canada Clearing
        frmCorrectBank.cmbBank_IDType.Items.Add("ZA bankcode") ' added 27.02.2017
        If oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.NoBranchType Then
            If EmptyString(oPayment.BANK_SWIFTCode) Then
                frmCorrectBank.cmbBank_IDType.Items.Add("")
                '07.02.2005 - changed from 5 to 8 (also for CorrBank)
                frmCorrectBank.cmbBank_IDType.SelectedIndex = 9  ' 30.03.2010 changed from 8 to 9
            Else
                frmCorrectBank.cmbBank_IDType.SelectedIndex = 0
            End If
        Else
            frmCorrectBank.cmbBank_IDType.SelectedIndex = CDbl(oPayment.BANK_BranchType) - 1
        End If
        frmCorrectBank.txtBank_Adr1.Text = oPayment.BANK_Name
        frmCorrectBank.txtBank_Adr2.Text = oPayment.BANK_Adr1
        frmCorrectBank.txtBank_Adr3.Text = oPayment.BANK_Adr2
        frmCorrectBank.txtBank_Adr4.Text = oPayment.BANK_Adr3


        frmCorrectBank.txtCorrBank_ID.Text = oPayment.BANK_SWIFTCodeCorrBank
        frmCorrectBank.cmbCorrBank_IDType.Items.Clear()
        frmCorrectBank.cmbCorrBank_IDType.Items.Add("SWIFT")
        frmCorrectBank.cmbCorrBank_IDType.Items.Add("FedWire")
        frmCorrectBank.cmbCorrBank_IDType.Items.Add("SortCode")
        frmCorrectBank.cmbCorrBank_IDType.Items.Add("Bankleitzahl")
        frmCorrectBank.cmbCorrBank_IDType.Items.Add("CHIPS")
        frmCorrectBank.cmbCorrBank_IDType.Items.Add("US_ABA")
        frmCorrectBank.cmbCorrBank_IDType.Items.Add("Bankleitzahl_Austria")
        frmCorrectBank.cmbCorrBank_IDType.Items.Add("MEPS")
        frmCorrectBank.cmbCorrBank_IDType.Items.Add("BSB")  ' 30.03.2010 Australia
        frmCorrectBank.cmbCorrBank_IDType.Items.Add("ZA bankcode")
        If oPayment.BANK_BranchTypeCorrBank = vbBabel.BabelFiles.BankBranchType.NoBranchType Then
            If oPayment.BANK_SWIFTCodeCorrBank = vbNullString Then
                frmCorrectBank.cmbCorrBank_IDType.Items.Add("-")
                frmCorrectBank.cmbCorrBank_IDType.SelectedIndex = 9 ' 30.03.2010 from 8 to 9
            Else
                frmCorrectBank.cmbCorrBank_IDType.SelectedIndex = 0
            End If
        Else
            frmCorrectBank.cmbCorrBank_IDType.SelectedIndex = oPayment.BANK_BranchTypeCorrBank - 1
        End If
        frmCorrectBank.txtCorrBank_Adr1.Text = oPayment.BANK_NameAddressCorrBank1
        frmCorrectBank.txtCorrBank_Adr2.Text = oPayment.BANK_NameAddressCorrBank2
        frmCorrectBank.txtCorrBank_Adr3.Text = oPayment.BANK_NameAddressCorrBank3
        frmCorrectBank.txtCorrBank_Adr4.Text = oPayment.BANK_NameAddressCorrBank4

        ' added 08.09.2009, charges
        frmCorrectBank.chkChargeMeDomestic.CheckState = bVal(oPayment.MON_ChargeMeDomestic)
        frmCorrectBank.chkChargeMeAbroad.CheckState = bVal(oPayment.MON_ChargeMeAbroad)
        ' have to send frmCorrectBank to frmCorrect
        frmCorrect.PassfrmCorrectBank(frmCorrectBank)

        ' Fill frmCorrectOther
        If oPayment.ERA_Date <> "19900101" Then
            frmCorrectOther.txtDate_Contract.Text = StringToDate(oPayment.ERA_Date)
        End If
        frmCorrectOther.txtERA_DealMadeWith.Text = oPayment.ERA_DealMadeWith
        If oPayment.ERA_ExchRateAgreed <> 0 Then
            frmCorrectOther.txtERA_ExchangeRateAgreed.Text = oPayment.ERA_ExchRateAgreed
        Else
            frmCorrectOther.txtERA_ExchangeRateAgreed.Text = vbNullString
        End If
        frmCorrectOther.txtERA_ContractRef.Text = oPayment.FRW_ForwardContractNo

        If oPayment.Invoices.Count > 0 Then
            ' Show statebank info for first invoice;
            'frmCorrectOther.txtStateBank_Code = oPayment.Invoices(1).STATEBANK_Code
            frmCorrectOther.cmbStateBank_Code.Items.Add(oPayment.Invoices(1).STATEBANK_Code)
            frmCorrectOther.cmbStateBank_Code.SelectedIndex = 0
            frmCorrectOther.txtStateBank_Text.Text = oPayment.Invoices(1).STATEBANK_Text
        End If

        If oPayment.Priority = True Then
            frmCorrectOther.chkUrgent.CheckState = System.Windows.Forms.CheckState.Checked
        Else
            frmCorrectOther.chkUrgent.CheckState = System.Windows.Forms.CheckState.Unchecked
        End If
        frmCorrectOther.txtE_Countrycode.Text = oPayment.E_CountryCode
        frmCorrect.PassfrmCorrectOther(frmCorrectOther)

        ' Changed 13.08.03
        ' Makes sense only for ACH and SAL
        ' Otherwise, only one invoice!
        'TODO - Spreadstuff
        'If aInfoArray(5, 0) <> "FI Payment" And aInfoArray(5, 0) <> "General Payment" Then
        '    '-----------------------------------------------
        '    ' Fill up spread-browser with Invoice/Freetext
        '    '-----------------------------------------------
        '    With frmCorrect.sprInvoices
        '        .MaxRows = 1

        '        .ScrollBars = SS_SCROLLBAR_V_ONLY
        '        .ScrollBarExtMode = True  ' show scrollbars only when needed
        '        .OperationMode = SS_OP_MODE_ROWMODE   'SINGLE_SELECT
        '        .MaxCols = 3
        '        .SetOddEvenRowColor(RGB(249, 253, 169), &H0&, &HFFFFFF, &H0&)
        '        .BackColorStyle = BackColorStyleUnderGrid
        '        '.SelBackColor = RGB(0, 0, 128)

        '        .GridShowHoriz = False
        '        .GridShowVert = False
        '        .GrayAreaBackColor = vbWhite
        '        .BorderStyle = BorderStyleNone
        '        ' Col headings and widths:
        '        .Row = 0
        '        .Col = 1
        '        .TypeHAlign = SS_CELL_H_ALIGN_RIGHT
        '        .Text = LRS(60060)  '"Bel�p"
        '        .ColWidth(-2) = 12

        '        .Col = 2
        '        .Text = LRS(60072)   '"Egenreferanse"
        '        .ColWidth(-2) = 30

        '        .Col = 3
        '        .Text = LRS(60073)  '"KID/Melding"
        '        .ColWidth(-2) = 40

        '        '.Col = 4
        '        '.Text = "Inv.Inx"
        '        '.ColWidth(-2) = 10

        '        .Row = 0
        '        .MaxRows = 0

        '        i = 0
        '        For Each oInvoice In oPayment.Invoices

        '            i = i + 1

        '            .Row = .Row + 1
        '            .MaxRows = .MaxRows + 1
        '            .Col = 1
        '            .Text = Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00")
        '            .CellType = SS_CELL_TYPE_FLOAT
        '            .TypeFloatSeparator = True

        '            .Col = 2
        '            .Text = oInvoice.REF_Own

        '            .Col = 3
        '            If oBabelFile.ImportFormat = AutoGiro Then
        '                .Text = oInvoice.REF_Bank

        '            ElseIf aInfoArray(5, 0) = "FI Payment" Then
        '                frmCorrect.lblE_Account.Caption = "Indbetalingskort ref.:"
        '                If Len(oPayment.E_Account) > 0 Then
        '                    'For some types there are no Betaleridentifikation
        '                    If Len(oInvoice.Unique_Id) > 2 Then
        '                        frmCorrect.txtE_Account.Text = Left$(oInvoice.Unique_Id, 2) & "-" & Mid$(oInvoice.Unique_Id, 3) & "-" & Trim$(oPayment.E_Account)
        '                    Else
        '                        frmCorrect.txtE_Account.Text = Left$(oInvoice.Unique_Id, 2) & "-" & Trim$(oPayment.E_Account)
        '                    End If
        '                Else
        '                    If Len(oInvoice.Unique_Id) > 0 Then
        '                        frmCorrect.txtE_Account.Text = Left$(oInvoice.Unique_Id, 2) & "-" & Mid$(oInvoice.Unique_Id, 3)
        '                    Else
        '                        frmCorrect.txtE_Account.Text = vbNullString
        '                    End If
        '                End If
        '                j = 0
        '                For Each oFreeText In oInvoice.Freetexts
        '                    j = j + 1
        '                    If j = 1 Then
        '                        .Text = oFreeText.Text
        '                    Else
        '                        .MaxRows = .MaxRows + 1
        '                        .Row = .Row + 1
        '                        .MaxRows = .MaxRows + 1
        '                        .Text = oFreeText.Text
        '                    End If
        '                Next

        '                ' Sjekk om KID eller melding:
        '            ElseIf Len(Trim(oInvoice.Unique_Id)) > 0 Then
        '                j = 1
        '                ' 20.03.2009 Added test for REF (Finland
        '                If oPayment.DnBNORTBIPayType = "REF" Then
        '                    .Text = "Reference : " & oInvoice.Unique_Id
        '                Else
        '                    ' show KID if used:
        '                    .Text = "KID : " & oInvoice.Unique_Id
        '                End If
        '            Else
        '                j = 0
        '                For Each oFreeText In oInvoice.Freetexts
        '                    j = j + 1
        '                    If j = 1 Then
        '                        .Text = oFreeText.Text
        '                    Else
        '                        .MaxRows = .MaxRows + 1
        '                        .Row = .Row + 1
        '                        .MaxRows = .MaxRows + 1
        '                        .Text = oFreeText.Text
        '                    End If
        '                Next
        '            End If

        '            '    .Col = 4
        '            '    .txt = Trim$(Str(i))

        '        Next 'oInvoice
        '        .MaxRows = .Row


        '        'cell formatting
        '        .BlockMode = True
        '        .Row = 1
        '        .row2 = .MaxRows

        '        ' Amount
        '        .Col = 1
        '        .Col2 = 1
        '        .TypeHAlign = SS_CELL_H_ALIGN_RIGHT

        '        .Col = 3
        '        .Col2 = 3
        '        .TypeEditMultiLine = True


        '        .BlockMode = False

        '        ' adjust size of spread, acording to no of invoice
        '        .UnitType = UnitTypeTwips
        '        .Height = (245 * (.MaxRows + 1)) + 50
        '        If .Height > 1400 Then
        '            .Height = 1400
        '        End If
        '        .UnitType = UnitTypeNormal
        '        .Refresh()

        '    End With

        '    ' do not show spread if only one invoice;
        '    If oPayment.Invoices.Count = 1 Then
        '        'frmCorrect.sprInvoices.Visible = False
        '    End If
        '    ' Fill first invoice into separate txtFields;
        '    'frmCorrect.txt
        'Else
        '    frmCorrect.sprInvoices.Visible = False
        'End If

        '------------------------------------------------

        frmCorrect.BabelFiles_Renamed = oBabelFiles
        frmCorrect.Filename = sFilename


        ' Mark errorfields with red, warnings also red
        ' If editable, enable

        ' test
        'For i = 0 To 0
        '    sErrField = "PAYMENT_DATE_PAYMENT"
        '    bError = True
        '    sErrString = "EKUKK med datoen"
        '    bAllowEdit = True

        For i = 0 To UBound(aInfoArray, 2)
            sErrField = aInfoArray(1, i)
            'bError = IIf(aInfoArray(3, i) = "0", False, True)
            If aInfoArray(3, i) = "0" Or aInfoArray(3, i) = Nothing Then
                bError = False
            Else
                bError = True
            End If
            sErrString = IIf(bError, "E" & aInfoArray(2, i), "W" & aInfoArray(2, i))
            bAllowEdit = IIf(aInfoArray(4, i) = "0", False, True)

            ' XNET 21.08.2012
            ' XNET 31.01.2013 - commented out (set in Edi2XML), and added next
            If bEditAll Then
                bAllowEdit = True
            End If
            '    If bShowAllPayments Then
            '        bAllowEdit = True
            '    End If


            If Len(Trim$(aInfoArray(5, i))) > 0 Then
                ' Override value for PaymentType
                frmCorrect.txtPayType.Text = Trim$(aInfoArray(5, i))
                '20.03.2009 added Reference and FI/Girokort if in use
            ElseIf oPayment.DnBNORTBIPayType = "REF" Then
                frmCorrect.txtPayType.Text = "Reference"
            ElseIf oPayment.DnBNORTBIPayType = "FIK" Then
                frmCorrect.txtPayType.Text = "FI-/Girokort"
            Else
                Select Case oPayment.PayType
                    'MISSING
                    Case "D"
                        frmCorrect.txtPayType.Text = LRS(60139)
                    Case "I"
                        frmCorrect.txtPayType.Text = LRS(60140)
                    Case "M"
                        frmCorrect.txtPayType.Text = LRS(60141)
                    Case "S"
                        frmCorrect.txtPayType.Text = LRS(60142)
                    Case Else
                        'Unknown paytype"
                        frmCorrect.txtPayType.Text = LRS(60143)
                End Select

            End If


            If bError Then
                iNoOfErrors = iNoOfErrors + 1
            Else
                ' added 20.06.2016
                If bError = False And bAllowEdit Then
                    ' do not count warnings
                Else
                    iNoOfWarnings = iNoOfWarnings + 1
                End If
            End If


            Select Case UCase(sErrField)

                ' BABELBANK-collection
                '---------------------
                Case "BABEL_COMPANYNO"
                    ' Checked
                    If bError Then
                        frmCorrect.txtTotalAmount.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(5).Visible = True
                    Else
                        frmCorrect.txtTotalAmount.ForeColor = System.Drawing.Color.Red  'System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(5).Visible = True
                    End If
                    If Not bAllowEdit Then
                        frmCorrect.txtCompanyNo.ReadOnly = True
                    End If
                    frmCorrect.txtCompanyNo.Enabled = True
                    frmCorrect.txtCompanyNo.Tag = sErrString

                Case "BABEL_MON_INVOICEAMOUNT"
                    ' Checked
                    If bError Then
                        frmCorrect.txtTotalAmount.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(1).Visible = True
                    Else
                        frmCorrect.txtTotalAmount.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(1).Visible = True
                    End If
                    frmCorrect.txtTotalAmount.Enabled = True
                    frmCorrect.txtTotalAmount.ReadOnly = True
                    frmCorrect.txtTotalAmount.Tag = sErrString

                    ' PAYMENT-collection
                    '-------------------
                    ' NOt in use Case "PAYMENT_PAYCODE"

                Case "PAYMENT_I_NAME"
                    ' Checked
                    If bError Then
                        frmCorrect.txtI_Name.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(3).Visible = True
                    Else
                        frmCorrect.txtI_Name.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(3).Visible = True
                    End If
                    frmCorrect.txtI_Name.Enabled = True
                    If Not bAllowEdit Then
                        frmCorrect.txtI_Name.ReadOnly = True
                    End If
                    frmCorrect.txtI_Name.Tag = sErrString

                Case "PAYMENT_I_ACCOUNT"
                    'Checked
                    If bError Then
                        frmCorrect.txtI_Account.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(4).Visible = True
                    Else
                        frmCorrect.txtI_Account.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(4).Visible = True
                    End If
                    ' NEWER allow edit of I_Account
                    frmCorrect.txtI_Account.ReadOnly = True
                    frmCorrect.txtI_Account.Enabled = True
                    frmCorrect.txtI_Account.Tag = sErrString


                Case "PAYMENT_REF_OWN"
                    'Checked
                    If bError Then
                        frmCorrect.txtPayOwnRef.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(6).Visible = True
                    Else
                        frmCorrect.txtPayOwnRef.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(6).Visible = True
                    End If
                    If Not bAllowEdit Then
                        frmCorrect.txtPayOwnRef.ReadOnly = True
                    End If
                    frmCorrect.txtPayOwnRef.Enabled = True
                    frmCorrect.txtPayOwnRef.Tag = sErrString

                Case "PAYMENT_E_NAME", "PAYMENT_E_ADR1", "PAYMENT_E_ADR2", "PAYMENT_E_ADR3", "PAYMENT_E_ZIP", "PAYMENT_E_CITY"
                    'Checked
                    If bError Then
                        frmCorrect.txtE_Name_Adress.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(7).Visible = True
                    Else
                        frmCorrect.txtE_Name_Adress.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(7).Visible = True
                    End If
                    If bAllowEdit Then
                        ' XNET 21.08.2012 added If
                        If bShowAllPayments = False Then
                            ' Must edit name/address in subform frmCorrectNameAddress
                            frmCorrect.cmdENameAdress.Enabled = True
                        End If

                        frmCorrect.txtE_Name_Adress.ReadOnly = True
                        frmCorrect.txtE_Name_Adress.Enabled = True
                        ' Fill frmCorrectNameAddress;
                        frmCorrectNameAddress.txtName.Text = oPayment.E_Name
                        frmCorrectNameAddress.txtAdr1.Text = oPayment.E_Adr1
                        frmCorrectNameAddress.txtAdr2.Text = oPayment.E_Adr2
                        frmCorrectNameAddress.txtAdr3.Text = oPayment.E_Adr3
                        frmCorrectNameAddress.txtZIP.Text = oPayment.E_Zip
                        frmCorrectNameAddress.txtCity.Text = oPayment.E_City
                    End If
                    frmCorrect.txtE_Name_Adress.Tag = sErrString

                Case "PAYMENT_E_ACCOUNT"
                    ' Checked
                    If bError Then
                        frmCorrect.txtE_Account.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(8).Visible = True
                    Else
                        frmCorrect.txtE_Account.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(8).Visible = True
                    End If
                    If Not bAllowEdit Then
                        frmCorrect.txtE_Account.ReadOnly = True
                    End If
                    ' XNET 21.08.2012 added If
                    If bShowAllPayments = False Then
                        frmCorrect.txtE_Account.ReadOnly = True
                    End If
                    frmCorrect.txtE_Account.Enabled = True
                    If Trim$(aInfoArray(5, i)) = "FI Payment" Then
                        ' Must treat special if DnB TBI, and FI-Payment.
                        sTmp = oPayment.E_Account
                        If oPayment.Invoices.Count > 0 Then
                            If Len(sTmp) > 0 Then
                                'For some types there are no Betaleridentifikation
                                If Len(oPayment.Invoices(lInvoiceIndex).Unique_Id) > 2 Then
                                    sTmp = Left$(oPayment.Invoices(lInvoiceIndex).Unique_Id, 2) & "-" & Mid$(oPayment.Invoices(lInvoiceIndex).Unique_Id, 3) & "-" & sTmp
                                Else
                                    sTmp = Left$(oPayment.Invoices(lInvoiceIndex).Unique_Id, 2) & "-" & sTmp
                                End If
                            Else
                                If Len(oPayment.Invoices(lInvoiceIndex).Unique_Id) > 0 Then
                                    sTmp = Left$(oPayment.Invoices(lInvoiceIndex).Unique_Id, 2) & "-" & Mid$(oPayment.Invoices(lInvoiceIndex).Unique_Id, 3)
                                Else
                                    sTmp = vbNullString
                                End If
                            End If
                        End If
                        frmCorrect.txtE_Account.Text = sTmp

                    End If
                    frmCorrect.txtE_Account.Tag = sErrString

                Case "PAYMENT_DATE_PAYMENT"
                    'Checked
                    If bError Then
                        frmCorrect.txtDate_Payment.ForeColor = System.Drawing.Color.Red
                        'frmCorrect.DTDate_Payment.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(9).Visible = True
                    Else
                        frmCorrect.txtDate_Payment.ForeColor = System.Drawing.Color.Red  'vbGreen
                        'frmCorrect.DTDate_Payment.ForeColor = System.Drawing.Color.Red
                        frmCorrect.imgExclamation(9).Visible = True
                    End If
                    If Not bAllowEdit Then
                        frmCorrect.txtDate_Payment.ReadOnly = True
                    End If
                    frmCorrect.txtDate_Payment.Enabled = True
                    frmCorrect.txtDate_Payment.Tag = sErrString

                Case "PAYMENT_MON_INVOICEAMOUNT"
                    'Checked
                    If bError Then
                        frmCorrect.txtPayMON_InvoiceAmount.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(10).Visible = True
                    Else
                        frmCorrect.txtPayMON_InvoiceAmount.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(10).Visible = True
                    End If
                    ' Never edit payments amount, must edit each invoices amount!
                    frmCorrect.txtPayMON_InvoiceAmount.Enabled = False
                    frmCorrect.txtPayMON_InvoiceAmount.Tag = sErrString

                Case "PAYMENT_MON_INVOICECURRENCY"
                    'Checked
                    If bError Then
                        frmCorrect.txtMON_InvoiceCurrency.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(11).Visible = True
                    Else
                        frmCorrect.txtMON_InvoiceCurrency.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(11).Visible = True
                    End If
                    ' XNET 21.08.2012 Never edit currency
                    'If Not bAllowEdit Then
                    frmCorrect.txtMON_InvoiceCurrency.ReadOnly = True
                    'End If
                    frmCorrect.txtMON_InvoiceCurrency.Enabled = True
                    frmCorrect.txtMON_InvoiceCurrency.Tag = sErrString

                Case "PAYMENT_PAYTYPE"
                    ' Checked
                    If bError Then
                        frmCorrect.txtPayType.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(12).Visible = True
                    Else
                        frmCorrect.txtPayType.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(12).Visible = True
                    End If
                    If Not bAllowEdit Then
                        frmCorrect.txtPayType.ReadOnly = True
                    End If
                    frmCorrect.txtPayType.Enabled = True
                    frmCorrect.txtPayType.Tag = sErrString

                Case "INVOICE_MON_INVOICEAMOUNT"
                    'Checked
                    If bError Then
                        frmCorrect.txtInvMON_InvoiceAmount.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(13).Visible = True
                    Else
                        frmCorrect.txtInvMON_InvoiceAmount.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(13).Visible = True
                    End If
                    ' XNET 21.08.2012 Never edit amount
                    'If Not bAllowEdit Then
                    frmCorrect.txtInvMON_InvoiceAmount.ReadOnly = True
                    'End If
                    'TODO - Spreadstuff
                    'frmCorrect.sprInvoices.Enabled = True
                    frmCorrect.txtInvMON_InvoiceAmount.Enabled = True
                    frmCorrect.txtInvMON_InvoiceAmount.Tag = sErrString

                Case "INVOICE_REF_OWN"
                    'Checked
                    If bError Then
                        frmCorrect.txtInvOwnRef.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(14).Visible = True
                    Else
                        frmCorrect.txtInvOwnRef.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(14).Visible = True
                    End If
                    If Not bAllowEdit Then
                        frmCorrect.txtInvOwnRef.ReadOnly = True
                    End If
                    'TODO - Spreadstuff
                    'frmCorrect.sprInvoices.Enabled = True
                    frmCorrect.txtInvOwnRef.Enabled = True
                    frmCorrect.txtInvOwnRef.Tag = sErrString

                    ' added 11.03.2017 to be able to change receivers countrycode, ref Danish checks
                Case "E_COUNTRYCODE"
                    'Checked
                    If bError Then
                        frmCorrectOther.txtE_Countrycode.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(7).Visible = True
                    Else
                        frmCorrect.txtInvOwnRef.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(7).Visible = True
                    End If
                    If Not bAllowEdit Then
                        frmCorrectOther.txtE_Countrycode.ReadOnly = True
                    End If
                    'TODO - Spreadstuff
                    'frmCorrect.sprInvoices.Enabled = True
                    frmCorrectOther.txtE_Countrycode.Enabled = True
                    frmCorrectOther.txtE_Countrycode.Tag = sErrString

                Case "FREETEXT"
                    'Checked
                    If bError Then
                        frmCorrect.txtFreetext.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(15).Visible = True
                    Else
                        frmCorrect.txtFreetext.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(15).Visible = True
                    End If
                    If Not bAllowEdit Then
                        frmCorrect.txtFreetext.ReadOnly = True
                    End If
                    frmCorrect.txtFreetext.Enabled = True
                    'TODO - Spreadstuff
                    'frmCorrect.sprInvoices.Enabled = True
                    frmCorrect.txtFreetext.Tag = sErrString

                Case "PAYMENT_BANK_SWIFTCODE", "PAYMENT_BANK_BRANCHNO", "PAYMENT_BANK_BRANCHTYPE"
                    'Changed 07.02.2005, added PAYMENT_BANK_BRANCHTYPE, removed next case
                    ' Test
                    If bError Then
                        frmCorrect.Image2(16).Visible = True
                        frmCorrectBank.txtBank_ID.ForeColor = System.Drawing.Color.Red
                        frmCorrectBank.Image2(0).Visible = True
                        frmCorrectBank.Image2(1).Visible = True
                    Else
                        frmCorrect.imgExclamation(16).Visible = True
                        frmCorrectBank.txtBank_ID.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectBank.imgExclamation(0).Visible = True
                        frmCorrectBank.imgExclamation(1).Visible = True
                    End If
                    frmCorrectBank.txtBank_ID.Enabled = True
                    frmCorrectBank.cmbBank_IDType.Enabled = True
                    If Not bAllowEdit Then
                        frmCorrectBank.txtBank_ID.ReadOnly = True
                        'TODO - LOCKING COMBOBOX -Several places in Correct.vb, including one with an explaination
                        'frmCorrectBank.cmbBank_IDType.Locked = True
                    End If
                    frmCorrectBank.txtBank_ID.Tag = sErrString

                 

                Case "PAYMENT_BANK_NAME", "PAYMENT_BANK_ADR1", "PAYMENT_BANK_ADR2", "PAYMENT_BANK_ADR3"
                    'Added PAYMNET_BANK_ADR1, 2 and 3 to this case. Removed seperate cases.
                    ' Test
                    If bError Then
                        frmCorrect.Image2(16).Visible = True
                        frmCorrectBank.txtBank_Adr1.ForeColor = System.Drawing.Color.Red
                        frmCorrectBank.Image2(3).Visible = True
                    Else
                        frmCorrect.imgExclamation(16).Visible = True
                        frmCorrectBank.txtBank_Adr1.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectBank.imgExclamation(3).Visible = True
                    End If
                    If Not bAllowEdit Then
                        frmCorrectBank.txtBank_Adr1.ReadOnly = True
                        frmCorrectBank.txtBank_Adr2.ReadOnly = True
                        frmCorrectBank.txtBank_Adr3.ReadOnly = True
                        frmCorrectBank.txtBank_Adr4.ReadOnly = True
                    End If
                    frmCorrectBank.txtBank_Adr1.Enabled = True
                    frmCorrectBank.txtBank_Adr1.Tag = sErrString
                    frmCorrectBank.txtBank_Adr2.Enabled = True
                    frmCorrectBank.txtBank_Adr2.Tag = sErrString
                    frmCorrectBank.txtBank_Adr3.Enabled = True
                    frmCorrectBank.txtBank_Adr3.Tag = sErrString
                    frmCorrectBank.txtBank_Adr4.Enabled = True
                    frmCorrectBank.txtBank_Adr4.Tag = sErrString


                 
                Case "PAYMENT_BANK_SWIFTCODECORRBANK", "PAYMENT_BANK_BRANCHNO_CORRBANK", "PAYMENT_BANK_BRANCHTYPECORRBANK"
                    'Changed 07.02.2005, added PAYMENT_BANK_BRANCHTYPECORRBANK, removed next case
                    ' Test
                    If bError Then
                        frmCorrect.Image2(16).Visible = True
                        frmCorrectBank.txtCorrBank_ID.ForeColor = System.Drawing.Color.Red
                        frmCorrectBank.Image2(8).Visible = True
                        frmCorrectBank.Image2(9).Visible = True
                    Else
                        frmCorrect.imgExclamation(16).Visible = True
                        frmCorrectBank.txtCorrBank_ID.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectBank.imgExclamation(8).Visible = True
                        frmCorrectBank.imgExclamation(9).Visible = True
                    End If
                    If Not bAllowEdit Then
                        frmCorrectBank.txtCorrBank_ID.ReadOnly = True
                        'TODO - LOCKING COMBOBOX -Several places in Correct.vb, including one with an explaination
                        'frmCorrectBank.cmbCorrBank_IDType.Locked = True
                    End If
                    frmCorrectBank.txtCorrBank_ID.Enabled = True
                    frmCorrectBank.cmbCorrBank_IDType.Enabled = True
                    frmCorrectBank.txtCorrBank_ID.Tag = sErrString

                    '    Case "PAYMENT_BANK_BRANCHTYPECORRBANK"
                    '        ' Test
                    '        If bError Then
                    '            frmCorrect.Image2(16).Visible = True
                    '            frmCorrectBank.cmbCorrBank_IDType.ForeColor = System.Drawing.Color.Red
                    '            'frmCorrectBank.txtCorrBank_IDType.ForeColor = System.Drawing.Color.Red
                    '            frmCorrectBank.Image2(9).Visible = True
                    '        Else
                    '            frmCorrect.imgExclamation(16).Visible = True
                    '            frmCorrectBank.cmbCorrBank_IDType.ForeColor = System.Drawing.Color.Red
                    '            'frmCorrectBank.txtCorrBank_IDType.ForeColor = System.Drawing.Color.Red  'vbGreen
                    '            frmCorrectBank.imgExclamation(9).Visible = True
                    '        End If
                    '        If Not bAllowEdit Then
                    '            frmCorrectBank.cmbCorrBank_IDType.Locked = True
                    '        End If
                    '        frmCorrectBank.cmbCorrBank_IDType.Enabled = True
                    '        frmCorrectBank.cmbCorrBank_IDType.Tag = sErrString


                Case "PAYMENT_BANK_NAMEADDRESSCORRBANK1", "PAYMENT_BANK_NAMEADDRESSCORRBANK2", "PAYMENT_BANK_NAMEADDRESSCORRBANK3", "PAYMENT_BANK_NAMEADDRESSCORRBANK4"
                    'Added PAYMENT_BANK_NAMEADDRESSCORRBANK1, 2, 3 and 4 to this case. Removed seperate cases.
                    ' Test
                    If bError Then
                        frmCorrect.Image2(16).Visible = True
                        frmCorrectBank.txtCorrBank_Adr1.ForeColor = System.Drawing.Color.Red
                        frmCorrectBank.Image2(11).Visible = True
                    Else
                        frmCorrect.imgExclamation(16).Visible = True
                        frmCorrectBank.txtCorrBank_Adr1.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectBank.imgExclamation(11).Visible = True
                    End If
                    If Not bAllowEdit Then
                        frmCorrectBank.txtCorrBank_Adr1.ReadOnly = True
                        frmCorrectBank.txtCorrBank_Adr2.ReadOnly = True
                        frmCorrectBank.txtCorrBank_Adr3.ReadOnly = True
                        frmCorrectBank.txtCorrBank_Adr4.ReadOnly = True
                    End If
                    frmCorrectBank.txtCorrBank_Adr1.Enabled = True
                    frmCorrectBank.txtCorrBank_Adr1.Tag = sErrString
                    frmCorrectBank.txtCorrBank_Adr2.Enabled = True
                    frmCorrectBank.txtCorrBank_Adr2.Tag = sErrString
                    frmCorrectBank.txtCorrBank_Adr3.Enabled = True
                    frmCorrectBank.txtCorrBank_Adr3.Tag = sErrString
                    frmCorrectBank.txtCorrBank_Adr4.Enabled = True
                    frmCorrectBank.txtCorrBank_Adr4.Tag = sErrString


                    '    Case "PAYMENT_BANK_NAMEADDRESSCORRBANK2"
                    '        ' Test
                    '        If bError Then
                    '            frmCorrect.Image2(16).Visible = True
                    '            frmCorrectBank.txtCorrBank_Adr2.ForeColor = System.Drawing.Color.Red
                    '            frmCorrectBank.Image2(12).Visible = True
                    '        Else
                    '            frmCorrect.imgExclamation(16).Visible = True
                    '            frmCorrectBank.txtCorrBank_Adr2.ForeColor = System.Drawing.Color.Red  'vbGreen
                    '            frmCorrectBank.imgExclamation(12).Visible = True
                    '        End If
                    '        If Not bAllowEdit Then
                    '            frmCorrectBank.txtCorrBank_Adr2.ReadOnly = True
                    '        End If
                    '        frmCorrectBank.txtCorrBank_Adr2.Enabled = True
                    '        frmCorrectBank.txtCorrBank_Adr2.Tag = sErrString
                    '
                    '    Case "PAYMENT_BANK_NAMEADDRESSCORRBANK3"
                    '            ' Test
                    '        If bError Then
                    '            frmCorrect.Image2(16).Visible = True
                    '            frmCorrectBank.txtCorrBank_Adr3.ForeColor = System.Drawing.Color.Red
                    '            frmCorrectBank.Image2(13).Visible = True
                    '        Else
                    '            frmCorrect.imgExclamation(16).Visible = True
                    '            frmCorrectBank.txtCorrBank_Adr3.ForeColor = System.Drawing.Color.Red  'vbGreen
                    '            frmCorrectBank.imgExclamation(13).Visible = True
                    '        End If
                    '        If Not bAllowEdit Then
                    '            frmCorrectBank.txtCorrBank_Adr3.ReadOnly = True
                    '        End If
                    '        frmCorrectBank.txtCorrBank_Adr3.Enabled = True
                    '        frmCorrectBank.txtCorrBank_Adr3.Tag = sErrString
                    '
                    '    Case "PAYMENT_BANK_NAMEADDRESSCORRBANK4"
                    '            ' Test
                    '        If bError Then
                    '            frmCorrect.Image2(16).Visible = True
                    '            frmCorrectBank.txtCorrBank_Adr4.ForeColor = System.Drawing.Color.Red
                    '            frmCorrectBank.Image2(14).Visible = True
                    '        Else
                    '            frmCorrect.imgExclamation(16).Visible = True
                    '            frmCorrectBank.txtCorrBank_Adr4.ForeColor = System.Drawing.Color.Red  'vbGreen
                    '            frmCorrectBank.imgExclamation(14).Visible = True
                    '        End If
                    '        If Not bAllowEdit Then
                    '            frmCorrectBank.txtCorrBank_Adr4.ReadOnly = True
                    '        End If
                    '        frmCorrectBank.txtCorrBank_Adr4.Enabled = True
                    '        frmCorrectBank.txtCorrBank_Adr4.Tag = sErrString

                Case "PAYMENT_NOTI_NOTIFICATIONMESSAGETOBANK"
                    ' Test
                    If bError Then
                        frmCorrect.Image2(16).Visible = True
                        frmCorrectBank.txtBank_Info.ForeColor = System.Drawing.Color.Red
                        'frmCorrectBank.Image2(14).Visible = True
                        frmCorrectBank.Image2(7).Visible = True  ' endret fra 14 til 7 03.02.2009
                    Else
                        frmCorrect.imgExclamation(16).Visible = True
                        frmCorrectBank.txtBank_Info.ForeColor = System.Drawing.Color.Red  'vbGreen
                        'frmCorrectBank.imgExclamation(14).Visible = True
                        frmCorrectBank.imgExclamation(7).Visible = True ' endret fra 14 til 7 03.02.2009
                    End If
                    If Not bAllowEdit Then
                        frmCorrectBank.txtBank_Info.ReadOnly = True
                    End If
                    frmCorrectBank.txtBank_Info.Enabled = True
                    frmCorrectBank.txtBank_Info.Tag = sErrString

                Case "PAYMENT_MON_CHARGEMEABROAD"   'Missing
                    If bError Then
                        frmCorrect.Image2(16).Visible = True
                        frmCorrectBank.chkChargeMeDomestic.ForeColor = System.Drawing.Color.Red
                        frmCorrectBank.Image2(16).Visible = True
                    Else
                        frmCorrect.imgExclamation(16).Visible = True
                        frmCorrectBank.chkChargeMeAbroad.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectBank.imgExclamation(16).Visible = True
                    End If
                    If Not bAllowEdit Then
                        'Can't lock the control
                    End If
                    frmCorrectBank.chkChargeMeAbroad.Enabled = True
                    frmCorrectBank.chkChargeMeAbroad.Tag = sErrString

                Case "PAYMENT_MON_CHARGEMEDOMESTIC"  'Missing
                    If bError Then
                        frmCorrect.Image2(16).Visible = True
                        frmCorrectBank.chkChargeMeAbroad.ForeColor = System.Drawing.Color.Red
                        frmCorrectBank.Image2(10).Visible = True
                    Else
                        frmCorrect.imgExclamation(16).Visible = True
                        frmCorrectBank.chkChargeMeDomestic.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectBank.imgExclamation(10).Visible = True
                    End If
                    If Not bAllowEdit Then
                        'Can't lock the control
                    End If
                    frmCorrectBank.chkChargeMeDomestic.Enabled = True
                    frmCorrectBank.chkChargeMeDomestic.Tag = sErrString


                Case "PAYMENT_CHEQUE"
                    ' Test
                    If bError Then
                        frmCorrect.chkCheque.ForeColor = System.Drawing.Color.Red
                        frmCorrect.Image2(18).Visible = True
                    Else
                        frmCorrect.chkCheque.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrect.imgExclamation(18).Visible = True
                    End If
                    If Not bAllowEdit Then
                        'Can't lock the control, always editable
                    End If
                    frmCorrect.chkCheque.Enabled = True
                    frmCorrect.chkCheque.Tag = sErrString


                Case "PAYMENT_ERA_DATE"
                    ' Test
                    If bError Then
                        frmCorrect.Image2(17).Visible = True
                        frmCorrectOther.txtDate_Contract.ForeColor = System.Drawing.Color.Red
                        frmCorrectOther.Image2(1).Visible = True
                    Else
                        frmCorrect.imgExclamation(17).Visible = True
                        frmCorrectOther.txtDate_Contract.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectOther.imgExclamation(1).Visible = True
                    End If
                    If Not bAllowEdit Then
                        ' All ERA-fields must be seen in accordance to each other,
                        ' therefore, enable them all!
                        frmCorrectOther.txtDate_Contract.ReadOnly = True
                        frmCorrectOther.txtERA_ContractRef.ReadOnly = True
                        frmCorrectOther.txtERA_DealMadeWith.ReadOnly = True
                        frmCorrectOther.txtERA_ExchangeRateAgreed.ReadOnly = True
                    End If
                    frmCorrectOther.txtDate_Contract.Enabled = True
                    frmCorrectOther.txtERA_ContractRef.Enabled = True
                    frmCorrectOther.txtERA_DealMadeWith.Enabled = True
                    frmCorrectOther.txtERA_ExchangeRateAgreed.Enabled = True
                    'frmCorrectOther.DTERA_Date.Tag = sErrString

                Case "PAYMENT_ERA_DEALMADEWITH"
                    ' Test
                    If bError Then
                        frmCorrect.Image2(17).Visible = True
                        frmCorrectOther.txtERA_DealMadeWith.ForeColor = System.Drawing.Color.Red
                        frmCorrectOther.Image2(2).Visible = True
                    Else
                        frmCorrect.imgExclamation(17).Visible = True
                        frmCorrectOther.txtERA_DealMadeWith.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectOther.imgExclamation(2).Visible = True
                    End If
                    If Not bAllowEdit Then
                        ' All ERA-fields must be seen in accordance to each other,
                        ' therefore, enable them all!
                        frmCorrectOther.txtDate_Contract.ReadOnly = True
                        frmCorrectOther.txtERA_ContractRef.ReadOnly = True
                        frmCorrectOther.txtERA_DealMadeWith.ReadOnly = True
                        frmCorrectOther.txtERA_ExchangeRateAgreed.ReadOnly = True
                    End If
                    frmCorrectOther.txtDate_Contract.Enabled = True
                    frmCorrectOther.txtERA_ContractRef.Enabled = True
                    frmCorrectOther.txtERA_DealMadeWith.Enabled = True
                    frmCorrectOther.txtERA_ExchangeRateAgreed.Enabled = True
                    frmCorrectOther.txtERA_DealMadeWith.Tag = sErrString

                Case "PAYMENT_ERA_EXCHRATEAGREED"
                    ' Test
                    If bError Then
                        frmCorrect.Image2(17).Visible = True
                        frmCorrectOther.txtERA_ExchangeRateAgreed.ForeColor = System.Drawing.Color.Red
                        frmCorrectOther.Image2(3).Visible = True
                    Else
                        frmCorrect.imgExclamation(17).Visible = True
                        frmCorrectOther.txtERA_ExchangeRateAgreed.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectOther.imgExclamation(3).Visible = True
                    End If
                    If Not bAllowEdit Then
                        ' All ERA-fields must be seen in accordance to each other,
                        ' therefore, enable them all!
                        frmCorrectOther.txtERA_ContractRef.ReadOnly = True
                        frmCorrectOther.txtERA_DealMadeWith.ReadOnly = True
                        frmCorrectOther.txtERA_ExchangeRateAgreed.ReadOnly = True
                    End If
                    frmCorrectOther.txtDate_Contract.Enabled = True
                    frmCorrectOther.txtERA_ContractRef.Enabled = True
                    frmCorrectOther.txtERA_DealMadeWith.Enabled = True
                    frmCorrectOther.txtERA_ExchangeRateAgreed.Enabled = True
                    frmCorrectOther.txtERA_ExchangeRateAgreed.Tag = sErrString

                Case "PAYMENT_FRW_FORWARDCONTRACTNO"
                    ' Test
                    If bError Then
                        frmCorrect.Image2(17).Visible = True
                        frmCorrectOther.txtERA_ContractRef.ForeColor = System.Drawing.Color.Red
                        frmCorrectOther.Image2(4).Visible = True
                    Else
                        frmCorrect.imgExclamation(17).Visible = True
                        frmCorrectOther.txtERA_ContractRef.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectOther.imgExclamation(4).Visible = True
                    End If
                    If Not bAllowEdit Then
                        ' All ERA-fields must be seen in accordance to each other,
                        ' therefore, enable them all!
                        frmCorrectOther.txtERA_ContractRef.ReadOnly = True
                        frmCorrectOther.txtERA_DealMadeWith.ReadOnly = True
                        frmCorrectOther.txtERA_ExchangeRateAgreed.ReadOnly = True
                    End If
                    frmCorrectOther.txtDate_Contract.Enabled = True
                    frmCorrectOther.txtERA_ContractRef.Enabled = True
                    frmCorrectOther.txtERA_DealMadeWith.Enabled = True
                    frmCorrectOther.txtERA_ExchangeRateAgreed.Enabled = True
                    frmCorrectOther.txtERA_ContractRef.Tag = sErrString

                Case "INVOICE_STATEBANK_CODE"   'Missing
                    ' Test
                    If bError Then
                        frmCorrect.Image2(17).Visible = True
                        'frmCorrectOther.txtStateBank_Code.ForeColor = System.Drawing.Color.Red
                        frmCorrectOther.cmbStateBank_Code.ForeColor = System.Drawing.Color.Red
                        frmCorrectOther.Image2(5).Visible = True
                    Else
                        frmCorrect.imgExclamation(17).Visible = True
                        'frmCorrectOther.txtStateBank_Code.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectOther.cmbStateBank_Code.ForeColor = System.Drawing.Color.Red
                        frmCorrectOther.imgExclamation(5).Visible = True
                    End If
                    If Not bAllowEdit Then
                        'frmCorrectOther.txtStateBank_Code.ReadOnly = True
                        'frmCorrectOther.txtStateBank_Code.Locked = True

                        'TODO - LOCKING COMBOBOX -Several places in Correct.vb, including one with an explaination
                        'In Visual Basic 6.0, the Locked property of a ComboBox control determines whether the text-box portion of the control can be edited. 
                        'In Visual Basic 2008, the Locked property prevents a control from being moved at design time. There is no direct equivalent for the Visual Basic 6.0 Locked property; however, you can achieve the same effect by setting the DropDownStyle property of the ComboBox control to DropDownList.
                        'Note   In Visual Basic 6.0, setting the Locked property to True also prevents the selection from being changed. 
                        '
                        'THIS IS WHAT WE ARE GOING TO DO!!!!!!
                        'You can duplicate this behavior by canceling the selection in the MouseDown event.
                        'frmCorrectOther.cmbStateBank_Code.Locked = True
                    Else
                        frmCorrectOther.txtStateBank_Text.Enabled = True
                        frmCorrectOther.txtStateBank_Text.ReadOnly = False 'Allow editing the text as well
                        PopulateNBCodes(frmCorrectOther)
                    End If
                    'frmCorrectOther.txtStateBank_Code.Enabled = True
                    frmCorrectOther.cmbStateBank_Code.Enabled = True
                    'frmCorrectOther.txtStateBank_Code.Tag = sErrString
                    frmCorrectOther.cmbStateBank_Code.Tag = sErrString

                Case "INVOICE_STATEBANK_TEXT"
                    ' Test
                    If bError Then
                        frmCorrect.Image2(17).Visible = True
                        frmCorrectOther.txtStateBank_Text.ForeColor = System.Drawing.Color.Red
                        frmCorrectOther.Image2(6).Visible = True
                    Else
                        frmCorrect.imgExclamation(17).Visible = True
                        frmCorrectOther.txtStateBank_Text.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectOther.imgExclamation(6).Visible = True
                    End If
                    If Not bAllowEdit Then
                        frmCorrectOther.txtStateBank_Text.ReadOnly = True
                    Else
                        'frmCorrectOther.txtStateBank_Code.Enabled = True
                        'frmCorrectOther.txtStateBank_Code.ReadOnly = False 'Allow editing the code as well
                        frmCorrectOther.cmbStateBank_Code.Enabled = True
                        'TODO - LOCKING COMBOBOX -Several places in Correct.vb, including one with an explaination
                        'frmCorrectOther.cmbStateBank_Code.Locked = False 'Allow editing the code as well
                        PopulateNBCodes(frmCorrectOther)
                    End If
                    frmCorrectOther.txtStateBank_Text.Enabled = True
                    frmCorrectOther.txtStateBank_Text.Tag = sErrString

                Case "PAYMENT_PRIORITY"
                    ' Test
                    If bError Then
                        frmCorrect.Image2(17).Visible = True
                        frmCorrectOther.chkUrgent.ForeColor = System.Drawing.Color.Red
                        frmCorrectOther.Image2(8).Visible = True
                    Else
                        frmCorrect.imgExclamation(17).Visible = True
                        frmCorrectOther.chkUrgent.ForeColor = System.Drawing.Color.Red  'vbGreen
                        frmCorrectOther.imgExclamation(8).Visible = True
                    End If
                    If Not bAllowEdit Then
                        'Can't lock the control
                    End If
                    frmCorrectOther.chkUrgent.Enabled = True
                    frmCorrectOther.chkUrgent.Tag = sErrString


                    '    Not in use
                    '    Case "PAYMENT_E_COUNTRYCODE"
                    '        If bError Then
                    '            frmCorrect.txtE_CountryCode.ForeColor = System.Drawing.Color.Red
                    '            frmCorrect.Image2(9).Visible = True
                    '        Else
                    '            frmCorrect.txtE_CountryCode.ForeColor = System.Drawing.Color.Red  'vbGreen
                    '            frmCorrect.imgExclamation(9).Visible = True
                    '        End If
                    '        If bAllowEdit Then
                    '            frmCorrect.txtE_CountryCode.Enabled = True
                    '        End If
                    '        frmCorrect.txtE_CountryCode.Tag = sErrString



                Case Else
                    ' Display errors for non-defined fields
                    frmCorrect.lblErrMessage.Text = Mid$(sErrString, 2) ' is preceeded with "E" or "W"
                    ' 20.06.2016 added next If
                    If iNoOfErrors > 0 Then
                        frmCorrect.Image2(19).Visible = True
                        frmCorrect.imgExclamation(19).Visible = False
                        frmCorrect.lblErrMessage.ForeColor = Color.Red
                    End If

            End Select

        Next i

        ' Show No of errors an warnings:
        frmCorrect.lblNoOfErrors.Text = LRS(60105) & Str(iNoOfErrors) & "   " & LRS(60106) & Str(iNoOfWarnings)


        ' XNET 21.08.2012 - open most fields for editing if ShowAll is selected;
        ' XNET 31.01.2013
        If bShowAllPayments Or bEditAll Then

            ' XNET 31.01.2013
            frmCorrect.txtE_Account.Enabled = True
            frmCorrect.cmdENameAdress.Enabled = True  ' cmd Button to bring up form for change of name/address

            frmCorrectNameAddress.txtName.Text = oPayment.E_Name
            frmCorrectNameAddress.txtAdr1.Text = oPayment.E_Adr1
            frmCorrectNameAddress.txtAdr2.Text = oPayment.E_Adr2
            frmCorrectNameAddress.txtAdr3.Text = oPayment.E_Adr3
            frmCorrectNameAddress.txtZIP.Text = oPayment.E_Zip
            frmCorrectNameAddress.txtCity.Text = oPayment.E_City
            frmCorrect.txtMON_InvoiceCurrency.Enabled = True

            ' end XNET

            frmCorrect.txtI_Name.Enabled = True
            frmCorrect.txtPayOwnRef.Enabled = True
            frmCorrect.txtDate_Payment.Enabled = True
            'frmCorrect.DTDate_Payment.Enabled = True
            'TODO - Spreadstuff
            'frmCorrect.sprInvoices.Enabled = True
            frmCorrect.txtInvOwnRef.Enabled = True
            frmCorrect.txtFreeText.Enabled = True
            ' XNET 31.01.2013 - do not enable edits in spread - too dangerous
            'TODO - Spreadstuff
            'frmCorrect.sprInvoices.Enabled = False

            frmCorrectBank.txtBank_ID.Enabled = True
            frmCorrectBank.cmbBank_IDType.Enabled = True
            frmCorrectBank.txtBank_Adr1.Enabled = True
            frmCorrectBank.txtBank_Adr1.Tag = sErrString
            frmCorrectBank.txtBank_Adr2.Enabled = True
            frmCorrectBank.txtBank_Adr2.Tag = sErrString
            frmCorrectBank.txtBank_Adr3.Enabled = True
            frmCorrectBank.txtBank_Adr3.Tag = sErrString
            frmCorrectBank.txtBank_Adr4.Enabled = True
            frmCorrectBank.txtBank_Adr4.Tag = sErrString
            frmCorrectBank.txtCorrBank_ID.Enabled = True
            frmCorrectBank.cmbCorrBank_IDType.Enabled = True
            frmCorrectBank.txtCorrBank_Adr1.Enabled = True
            frmCorrectBank.txtCorrBank_Adr1.Tag = sErrString
            frmCorrectBank.txtCorrBank_Adr2.Enabled = True
            frmCorrectBank.txtCorrBank_Adr2.Tag = sErrString
            frmCorrectBank.txtCorrBank_Adr3.Enabled = True
            frmCorrectBank.txtCorrBank_Adr3.Tag = sErrString
            frmCorrectBank.txtCorrBank_Adr4.Enabled = True
            frmCorrectBank.txtCorrBank_Adr4.Tag = sErrString
            frmCorrectBank.txtBank_Info.Enabled = True
            frmCorrectBank.chkChargeMeAbroad.Enabled = True
            frmCorrectBank.chkChargeMeDomestic.Enabled = True
            frmCorrect.chkCheque.Enabled = True
            frmCorrectOther.txtDate_Contract.Enabled = True
            frmCorrectOther.txtERA_ContractRef.Enabled = True
            frmCorrectOther.txtERA_DealMadeWith.Enabled = True
            frmCorrectOther.txtERA_ExchangeRateAgreed.Enabled = True
            frmCorrectOther.txtERA_ContractRef.Enabled = True
            frmCorrectOther.txtERA_DealMadeWith.Enabled = True
            frmCorrectOther.txtERA_ExchangeRateAgreed.Enabled = True
            frmCorrectOther.txtERA_ContractRef.Enabled = True
            frmCorrectOther.txtERA_DealMadeWith.Enabled = True
            frmCorrectOther.txtERA_ExchangeRateAgreed.Enabled = True
            frmCorrectOther.txtERA_ContractRef.Enabled = True
            frmCorrectOther.txtERA_DealMadeWith.Enabled = True
            frmCorrectOther.txtERA_ExchangeRateAgreed.Enabled = True
            frmCorrectOther.cmbStateBank_Code.Enabled = True
            frmCorrectOther.cmbStateBank_Code.Enabled = True
            'TODO - LOCKING COMBOBOX -Several places in Correct.vb, including one with an explaination
            'frmCorrectOther.cmbStateBank_Code.Locked = False 'Allow editing the code as well
            PopulateNBCodes(frmCorrectOther)
            frmCorrectOther.txtStateBank_Text.Enabled = True
            frmCorrectOther.chkUrgent.Enabled = True
            frmCorrectOther.txtE_Countrycode.Enabled = True

        End If
        ' End XNET 21.08.2012


        Do
            frmCorrect.PassfrmCorrectNameAddress(frmCorrectNameAddress)
            frmCorrect.ShowDialog()
            iCorrectReturn = frmCorrect.iReturnValue


            ' FIX: Run through aInfoArray, test for errField and AllowEdit, and save only
            '      editable fields!!!
            Select Case iCorrectReturn
                Case 1   ' &Next: Save values from form into oPayment

                    For i = 0 To UBound(aInfoArray, 2)
                        sErrField = aInfoArray(1, i)

                        Select Case UCase(sErrField)

                            Case "PAYMENT_E_NAME", "PAYMENT_E_ADR1", "PAYMENT_E_ADR2", "PAYMENT_E_ADR3", "PAYMENT_E_ZIP", "PAYMENT_E_CITY"
                                bEditNameAddress = IIf(aInfoArray(4, i) = "0", False, True)
                                ' Must split info from txtE_Name_Adress
                                If bEditNameAddress Then
                                    oPayment.E_Name = frmCorrectNameAddress.txtName.Text
                                    oPayment.E_Adr1 = frmCorrectNameAddress.txtAdr1.Text
                                    oPayment.E_Adr2 = frmCorrectNameAddress.txtAdr2.Text
                                    oPayment.E_Adr3 = frmCorrectNameAddress.txtAdr3.Text
                                    oPayment.E_Zip = frmCorrectNameAddress.txtZIP.Text
                                    oPayment.E_City = frmCorrectNameAddress.txtCity.Text
                                End If

                            Case "PAYMENT_DATE_PAYMENT"
                                ' Date as 25.04.2003 in textbox
                                oPayment.DATE_Payment = DateToString(frmCorrect.txtDate_Payment.Text)
                                'oPayment.DATE_Payment = DateToString(frmCorrect.DTDate_Payment.Value)

                            Case "PAYMENT_MON_INVOICECURRENCY"
                                oPayment.MON_InvoiceCurrency = frmCorrect.txtMON_InvoiceCurrency.Text

                            Case "PAYMENT_I_NAME"
                                oPayment.I_Name = frmCorrect.txtI_Name.Text

                            Case "PAYMENT_I_ACCOUNT"
                                oPayment.I_Account = frmCorrect.txtI_Account.Text

                            Case "BABEL_COMPANYNO"
                                oBabelFile.IDENT_Sender = frmCorrect.txtCompanyNo.Text

                            Case "PAYMENT_REF_OWN"
                                oPayment.REF_Own = frmCorrect.txtPayOwnRef.Text

                            Case "PAYMENT_E_ACCOUNT"
                                If aInfoArray(5, 0) = "FI Payment" Then


                                    'oPayment.E_Account = SplitFI(Replace(frmCorrect.txtE_Account.Text, "-", ""), 1)
                                    'oPayment.Invoices(lInvoiceIndex).Unique_Id = SplitFI(Replace(frmCorrect.txtE_Account.Text, "-", ""), 2)
                                    iNoOfHyphens = Len(frmCorrect.txtE_Account.Text) - Len(Replace(frmCorrect.txtE_Account.Text, "-", ""))
                                    If iNoOfHyphens > 0 And iNoOfHyphens < 3 Then
                                        ' Must have 1 or 2 -
                                        oPayment.E_Account = Mid$(frmCorrect.txtE_Account.Text, InStrRev(frmCorrect.txtE_Account.Text, "-") + 1)
                                        If oPayment.Invoices.Count > 0 Then
                                            oPayment.Invoices(lInvoiceIndex).Unique_Id = Replace(Left$(frmCorrect.txtE_Account.Text, InStrRev(frmCorrect.txtE_Account.Text, "-") - 1), "-", "")
                                        End If
                                    End If
                                Else
                                    oPayment.E_Account = frmCorrect.txtE_Account.Text
                                End If

                            Case "PAYMENT_CHEQUE"
                                oPayment.Cheque = IIf(frmCorrect.chkCheque.CheckState = 1, 2, 0)

                            Case "PAYMENT_NOTI_NOTIFICATIONMESSAGETOBANK"
                                oPayment.NOTI_NotificationMessageToBank = frmCorrectBank.txtBank_Info.Text

                            Case "PAYMENT_BANK_SWIFTCODE", "PAYMENT_BANK_BRANCHTYPE", "PAYMENT_BANK_BRANCHNO"
                                ' Changed 17.11.04
                                ' Must test if Swift or Other, because the user may have changed branchtype
                                'oPayment.BANK_SWIFTCode = frmCorrectBank.txtBank_ID.Text
                                'oPayment.BANK_BranchType = vb_Translate_FromBranchType(frmCorrectBank.cmbBank_IDType.Text)
                                oPayment.BANK_BranchType = vb_Translate_FromBranchType(frmCorrectBank.cmbBank_IDType.Text)
                                If oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT Then
                                    oPayment.BANK_SWIFTCode = frmCorrectBank.txtBank_ID.Text
                                    oPayment.BANK_BranchNo = vbNullString
                                Else
                                    oPayment.BANK_BranchNo = frmCorrectBank.txtBank_ID.Text
                                    oPayment.BANK_SWIFTCode = vbNullString
                                End If

                                'Case "PAYMENT_BANK_BRANCHNO"
                                ' Changed 17.11.04 - Moved to previous Case
                                'oPayment.BANK_BranchNo = frmCorrectBank.txtBank_ID.Text
                                'oPayment.BANK_BranchType = vb_Translate_FromBranchType(frmCorrectBank.cmbBank_IDType.Text)

                            Case "PAYMENT_BANK_NAME", "PAYMENT_BANK_ADR1", "PAYMENT_BANK_ADR2", "PAYMENT_BANK_ADR3"
                                'Added PAYMENT_BANK_ADR1, 2 and 3 to this case. Removed seperate cases.
                                oPayment.BANK_Name = frmCorrectBank.txtBank_Adr1.Text
                                oPayment.BANK_Adr1 = frmCorrectBank.txtBank_Adr2.Text
                                oPayment.BANK_Adr2 = frmCorrectBank.txtBank_Adr3.Text
                                oPayment.BANK_Adr3 = frmCorrectBank.txtBank_Adr4.Text

                                '         Case "PAYMENT_BANK_ADR1"
                                '             oPayment.BANK_Adr1 = frmCorrectBank.txtBank_Adr2.Text
                                '
                                '         Case "PAYMENT_BANK_ADR2"
                                '             oPayment.BANK_Adr2 = frmCorrectBank.txtBank_Adr3.Text
                                '
                                '         Case "PAYMENT_BANK_ADR3"
                                '             oPayment.BANK_Adr3 = frmCorrectBank.txtBank_Adr4.Text

                            Case "PAYMENT_BANK_SWIFTCODECORRBANK", "PAYMENT_BANK_BRANCHTYPECORRBANK", "PAYMENT_BANK_BRANCHNOCODECORRBANK"
                                ' Changed 07.02.2005
                                ' Must test if Swift or Other, because the user may have changed branchtype
                                ' and "PAYMENT_BANK_BRANCHNOCODECORRBANK" added in this case
                                'oPayment.BANK_SWIFTCodeCorrBank = frmCorrectBank.txtCorrBank_ID.Text
                                'oPayment.BANK_BranchTypeCorrBank = vb_Translate_FromBranchType(frmCorrectBank.cmbCorrBank_IDType.Text)
                                oPayment.BANK_BranchTypeCorrBank = vb_Translate_FromBranchType(frmCorrectBank.cmbCorrBank_IDType.Text)
                                If oPayment.BANK_BranchTypeCorrBank = vbBabel.BabelFiles.BankBranchType.SWIFT Then
                                    oPayment.BANK_SWIFTCodeCorrBank = frmCorrectBank.txtCorrBank_ID.Text
                                    oPayment.BANK_BranchNoCorrBank = vbNullString
                                Else
                                    oPayment.BANK_BranchNoCorrBank = frmCorrectBank.txtCorrBank_ID.Text
                                    oPayment.BANK_SWIFTCodeCorrBank = vbNullString
                                End If

                                '         Case "PAYMENT_BANK_BRANCHNOCODECORRBANK", "PAYMENT_BANK_BRANCHTYPECORRBANK"
                                '             oPayment.BANK_BranchNoCorrBank = frmCorrectBank.txtCorrBank_ID.Text
                                '             oPayment.BANK_BranchTypeCorrBank = vb_Translate_FromBranchType(frmCorrectBank.cmbCorrBank_IDType.Text)
                                '
                            Case "PAYMENT_BANK_NAMEADDRESSCORRBANK1", "PAYMENT_BANK_NAMEADDRESSCORRBANK2", "PAYMENT_BANK_NAMEADDRESSCORRBANK3", "PAYMENT_BANK_NAMEADDRESSCORRBANK4"
                                'Added PAYMENT_BANK_NAMEADDRESSCORRBANK1, 2, 3 and 4 to this case. Removed seperate cases.
                                oPayment.BANK_NameAddressCorrBank1 = frmCorrectBank.txtCorrBank_Adr1.Text
                                oPayment.BANK_NameAddressCorrBank2 = frmCorrectBank.txtCorrBank_Adr2.Text
                                oPayment.BANK_NameAddressCorrBank3 = frmCorrectBank.txtCorrBank_Adr3.Text
                                oPayment.BANK_NameAddressCorrBank4 = frmCorrectBank.txtCorrBank_Adr4.Text
                                '         Case "PAYMENT_BANK_NAMEADDRESSCORRBANK2"
                                '             oPayment.BANK_NameAddressCorrBank2 = frmCorrectBank.txtCorrBank_Adr2.Text
                                '         Case "PAYMENT_BANK_NAMEADDRESSCORRBANK3"
                                '             oPayment.BANK_NameAddressCorrBank3 = frmCorrectBank.txtCorrBank_Adr3.Text
                                '         Case "PAYMENT_BANK_NAMEADDRESSCORRBANK4"
                                '             oPayment.BANK_NameAddressCorrBank4 = frmCorrectBank.txtCorrBank_Adr4.Text

                                ' added 28.09.2009
                            Case "PAYMENT_MON_CHARGEMEABROAD"
                                oPayment.MON_ChargeMeAbroad = bVal(frmCorrectBank.chkChargeMeAbroad.CheckState)

                                ' added 28.09.2009
                            Case "PAYMENT_MON_CHARGEMEDOMESTIC"  'Missing
                                oPayment.MON_ChargeMeDomestic = bVal(frmCorrectBank.chkChargeMeDomestic.CheckState)

                            Case "PAYMENT_ERA_DATE", "PAYMENT_ERA_DEALMADEWITH", "PAYMENT_ERA_EXCHRATEAGREED", "PAYMENT_FRW_FORWARDCONTRACTNO"
                                If Not IsDBNull(frmCorrectOther.txtDate_Contract.Text) Then    'vbNullString Then
                                    oPayment.ERA_Date = DateToString(frmCorrectOther.txtDate_Contract.Text)
                                Else
                                    oPayment.ERA_Date = "19900101"
                                End If
                                oPayment.ERA_DealMadeWith = frmCorrectOther.txtERA_DealMadeWith.Text
                                If Len(Trim$(frmCorrectOther.txtERA_ExchangeRateAgreed.Text)) > 0 Then
                                    oPayment.ERA_ExchRateAgreed = CDbl(frmCorrectOther.txtERA_ExchangeRateAgreed.Text)
                                Else
                                    oPayment.ERA_ExchRateAgreed = 0
                                End If
                                oPayment.FRW_ForwardContractNo = frmCorrectOther.txtERA_ContractRef.Text

                            Case "INVOICE_STATEBANK_CODE", "INVOICE_STATEBANK_TEXT"
                                ' Save same statebankinfo for all invoices!
                                ' FIX: Not a perfect solution!
                                For Each oInvoice In oPayment.Invoices
                                    'oInvoice.STATEBANK_Code = frmCorrectOther.txtStateBank_Code
                                    oInvoice.STATEBANK_Code = Left$(frmCorrectOther.cmbStateBank_Code.Text, 2)
                                    oInvoice.STATEBANK_Text = frmCorrectOther.txtStateBank_Text.Text
                                Next oInvoice

                            Case "PAYMENT_PRIORITY"
                                oPayment.Priority = frmCorrectOther.chkUrgent.CheckState

                                'oPayment.E_CountryCode = frmCorrectOther.txtE_Countrycode

                            Case "E_COUNTRYCODE"
                                ' added 11.03.2016
                                oPayment.E_CountryCode = frmCorrectOther.txtE_Countrycode.Text


                            Case "INVOICE_MON_INVOICEAMOUNT", "INVOICE_REF_OWN", "FREETEXT"
                                'TODO - Spreadstuff
                                'If frmCorrect.sprInvoices.Visible Then  ' New 13.08.03
                                '    With frmCorrect.sprInvoices
                                '        lInvoiceCounter = 0
                                '        lFreetextCounter = 0
                                '        For j = 1 To .MaxRows
                                '            .Row = j
                                '            .Col = 1
                                '            If Len(.Text) > 0 Then
                                '                lFreetextCounter = 0
                                '                lInvoiceCounter = lInvoiceCounter + 1
                                '                'oPayment.Invoices(i).MON_InvoiceAmount = Val(.Text) * 100
                                '                oPayment.Invoices(lInvoiceCounter).MON_InvoiceAmount = CDbl(.Text) * 100
                                '                .Col = 2
                                '                oPayment.Invoices(lInvoiceCounter).REF_Own = .Text
                                '            End If
                                '            .Col = 3
                                '            lFreetextCounter = lFreetextCounter + 1
                                '            If aInfoArray(5, 0) = "FI Payment" Then
                                '                oPayment.Invoices.Item(lInvoiceCounter).Unique_Id = SplitFI(frmCorrect.txtE_Account.Text, 2)
                                '                If oPayment.Invoices.Item(lInvoiceCounter).Freetexts.Count >= lFreetextCounter Then
                                '                    oPayment.Invoices(lInvoiceCounter).Freetexts(lFreetextCounter).Text = .Text
                                '                End If
                                '            Else
                                '                If oPayment.Invoices(lInvoiceCounter).Unique_Id <> vbNullString Then
                                '                    oPayment.Invoices(lInvoiceCounter).Unique_Id = .Text
                                '                ElseIf oPayment.Invoices(lInvoiceCounter).Freetexts.Count >= lFreetextCounter Then
                                '                    'If oPayment.Invoices(lInvoiceCounter).Freetexts(lFreetextCounter).Text = vbNullString Then
                                '                    ' What about freetext????
                                '                    oPayment.Invoices(lInvoiceCounter).Freetexts(lFreetextCounter).Text = .Text
                                '                    'End If
                                '                Else
                                '                    oFreeText = oPayment.Invoices(lInvoiceCounter).Freetexts.Add
                                '                    oFreeText.Text = .Text
                                '                    'oPayment.Invoices(lInvoiceCounter).Freetexts(1).Text = .Text
                                '                End If
                                '            End If
                                '        Next j
                                '    End With
                                'Else
                                '    ' New 28.04.05
                                '    ' To be able to handle userinput in ACHPAYERSREF
                                '    If UCase(sErrField) = "INVOICE_REF_OWN" Then
                                '        oPayment.Invoices(1).REF_Own = frmCorrect.txtInvOwnRef.Text
                                '    End If
                                'End If

                        End Select
                    Next i

                    ' XNET 21.08.2012 - Save all editable fields
                    ' XNET 31.01.2013 added bEditAll
                    If bShowAllPayments Or bEditAll Then

                        ' XNET 31.01.2103 - addes some saves for bEditAll
                        If bEditAll Then
                            oPayment.E_Name = frmCorrectNameAddress.txtName.Text
                            oPayment.E_Adr1 = frmCorrectNameAddress.txtAdr1.Text
                            oPayment.E_Adr2 = frmCorrectNameAddress.txtAdr2.Text
                            oPayment.E_Adr3 = frmCorrectNameAddress.txtAdr3.Text
                            oPayment.E_Zip = frmCorrectNameAddress.txtZIP.Text
                            oPayment.E_City = frmCorrectNameAddress.txtCity.Text
                            oPayment.E_Account = Mid$(frmCorrect.txtE_Account.Text, InStrRev(frmCorrect.txtE_Account.Text, "-") + 1)

                        End If
                        ' end XNET
                        'oPayment.DATE_Payment = DateToString(frmCorrect.DTDate_Payment.Value)
                        oPayment.DATE_Payment = DateToString(frmCorrect.txtDate_Payment.Text)
                        oPayment.MON_InvoiceCurrency = frmCorrect.txtMON_InvoiceCurrency.Text
                        oPayment.I_Name = frmCorrect.txtI_Name.Text
                        oBabelFile.IDENT_Sender = frmCorrect.txtCompanyNo.Text
                        oPayment.REF_Own = frmCorrect.txtPayOwnRef.Text
                        oPayment.Cheque = IIf(frmCorrect.chkCheque.CheckState = 1, 2, 0)
                        oPayment.NOTI_NotificationMessageToBank = frmCorrectBank.txtBank_Info.Text
                        oPayment.BANK_BranchType = vb_Translate_FromBranchType(frmCorrectBank.cmbBank_IDType.Text)
                        If oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT Then
                            oPayment.BANK_SWIFTCode = frmCorrectBank.txtBank_ID.Text
                            oPayment.BANK_BranchNo = vbNullString
                        Else
                            oPayment.BANK_BranchNo = frmCorrectBank.txtBank_ID.Text
                            oPayment.BANK_SWIFTCode = vbNullString
                        End If
                        oPayment.BANK_Name = frmCorrectBank.txtBank_Adr1.Text
                        oPayment.BANK_Adr1 = frmCorrectBank.txtBank_Adr2.Text
                        oPayment.BANK_Adr2 = frmCorrectBank.txtBank_Adr3.Text
                        oPayment.BANK_Adr3 = frmCorrectBank.txtBank_Adr4.Text
                        oPayment.BANK_BranchTypeCorrBank = vb_Translate_FromBranchType(frmCorrectBank.cmbCorrBank_IDType.Text)
                        If oPayment.BANK_BranchTypeCorrBank = vbBabel.BabelFiles.BankBranchType.SWIFT Then
                            oPayment.BANK_SWIFTCodeCorrBank = frmCorrectBank.txtCorrBank_ID.Text
                            oPayment.BANK_BranchNoCorrBank = vbNullString
                        Else
                            oPayment.BANK_BranchNoCorrBank = frmCorrectBank.txtCorrBank_ID.Text
                            oPayment.BANK_SWIFTCodeCorrBank = vbNullString
                        End If
                        oPayment.BANK_NameAddressCorrBank1 = frmCorrectBank.txtCorrBank_Adr1.Text
                        oPayment.BANK_NameAddressCorrBank2 = frmCorrectBank.txtCorrBank_Adr2.Text
                        oPayment.BANK_NameAddressCorrBank3 = frmCorrectBank.txtCorrBank_Adr3.Text
                        oPayment.BANK_NameAddressCorrBank4 = frmCorrectBank.txtCorrBank_Adr4.Text
                        oPayment.MON_ChargeMeAbroad = bVal(frmCorrectBank.chkChargeMeAbroad.CheckState)
                        oPayment.MON_ChargeMeDomestic = bVal(frmCorrectBank.chkChargeMeDomestic.CheckState)
                        If Not EmptyString(frmCorrectOther.txtDate_Contract.Text) Then    'vbNullString Then
                            oPayment.ERA_Date = DateToString(frmCorrectOther.txtDate_Contract.Text)
                        Else
                            oPayment.ERA_Date = "19900101"
                        End If
                        oPayment.ERA_DealMadeWith = frmCorrectOther.txtERA_DealMadeWith.Text
                        If Len(Trim$(frmCorrectOther.txtERA_ExchangeRateAgreed.Text)) > 0 Then
                            ' XNET 21.08.2012, changed from cDbl to Val
                            oPayment.ERA_ExchRateAgreed = Val(frmCorrectOther.txtERA_ExchangeRateAgreed.Text)
                        Else
                            oPayment.ERA_ExchRateAgreed = 0
                        End If
                        oPayment.FRW_ForwardContractNo = frmCorrectOther.txtERA_ContractRef.Text
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.STATEBANK_Code = Left$(frmCorrectOther.cmbStateBank_Code.Text, 2)
                            oInvoice.STATEBANK_Text = frmCorrectOther.txtStateBank_Text.Text
                        Next oInvoice
                        oPayment.Priority = bVal(frmCorrectOther.chkUrgent.CheckState)
                        oPayment.E_CountryCode = frmCorrectOther.txtE_Countrycode.Text

                        ' find freetext in txtFreetext
                        'For Each oInvoice In oPayment.Invoices
                        '    For i = oInvoice.Freetexts.Count To 1 Step -1
                        '        oInvoice.Freetexts.Remove(i)
                        '    Next i
                        '    oFreeText = oInvoice.Freetexts.Add()
                        '    oFreeText.Text = frmCorrect.txtFreetext.Text
                        'Next oInvoice

                        oPayment.Invoices(1).REF_Own = frmCorrect.txtInvOwnRef.Text
                    End If

                    Exit Do

                Case 2   ' &Delete: Delete payment in concern
                    ' Update payments for this batch
                    ' Must run from last payment and up;
                    For i = oBatch.Payments.Count To 1 Step -1
                        'If i = Val(Mid$(aInfoArray(0, 0), 3, 1)) Then
                        sTmp = aInfoArray(0, 0)
                        If i = Val(xDelim(sTmp, ",", 3)) Then

                            '----- 21.12.04
                            '----- This must be done different if we are going to report correct!!!
                            '----- F.ex. mark for deletion, but not delete ?
                            'oBatch.Payments.Remove (Val(Mid$(aInfoArray(0, 0), 3, 1)))
                            oBatch.Payments.Remove(Val(xDelim(sTmp, ",", 3)))
                            Exit For
                        End If
                    Next i
                    ' XokNET 16.01.2015 - When we have deleted, we need to reindex current batch to have indexes properly sequenced
                    oBatch.ReIndexPayments()

                    Exit Do

                Case 3   ' &Cancel: Cancel paymentprocess
                    Exit Do
                Case 4   ' Cancel This; Cancel changes for this payment only
                    ' Refresh screen with previous values
                    Exit Do

                Case 5  ' Print current payment
                    ' call ActiveReport object
                    ' gjenta alle tester som under Case 1, men legg inn verdier i array
                    ' som skal sendes til rapporten;
                    ' (0) Error:            Feilmelding
                    ' (1) Field:            Feltnavn
                    ' (2) Original value:   Gammel verdi
                    ' (3) New value:        Nytt feltinnhold


                    For i = 0 To UBound(aInfoArray, 2)
                        ReDim Preserve aErrorArray(3, UBound(aInfoArray, 2))
                        sErrField = aInfoArray(1, i)
                        aErrorArray(0, i) = aInfoArray(2, i)
                        aErrorArray(1, i) = sErrField

                        Select Case UCase(sErrField)

                            Case "PAYMENT_E_NAME"
                                aErrorArray(2, i) = oPayment.E_Name
                                aErrorArray(3, i) = frmCorrectNameAddress.txtName.Text
                            Case "PAYMENT_E_ADR1"
                                aErrorArray(2, i) = oPayment.E_Adr1
                                aErrorArray(3, i) = frmCorrectNameAddress.txtAdr1.Text
                            Case "PAYMENT_E_ADR2"
                                aErrorArray(2, i) = oPayment.E_Adr2
                                aErrorArray(3, i) = frmCorrectNameAddress.txtAdr2.Text
                            Case "PAYMENT_E_ADR3"
                                aErrorArray(2, i) = oPayment.E_Adr3
                                aErrorArray(3, i) = frmCorrectNameAddress.txtAdr3.Text
                            Case "PAYMENT_E_ZIP"
                                aErrorArray(2, i) = oPayment.E_Zip
                                aErrorArray(3, i) = frmCorrectNameAddress.txtZIP.Text
                            Case "PAYMENT_E_CITY"
                                aErrorArray(2, i) = oPayment.E_City
                                aErrorArray(3, i) = frmCorrectNameAddress.txtCity.Text
                            Case "PAYMENT_DATE_PAYMENT"
                                aErrorArray(2, i) = oPayment.DATE_Payment
                                'aErrorArray(3, i) = DateToString(frmCorrect.DTDate_Payment.Value)
                                aErrorArray(3, i) = DateToString(frmCorrect.txtDate_Payment.Text)
                            Case "PAYMENT_MON_INVOICECURRENCY"
                                aErrorArray(2, i) = oPayment.MON_InvoiceCurrency
                                aErrorArray(3, i) = frmCorrect.txtMON_InvoiceCurrency.Text
                            Case "PAYMENT_I_NAME"
                                aErrorArray(2, i) = oPayment.I_Name
                                aErrorArray(3, i) = frmCorrect.txtI_Name.Text
                            Case "PAYMENT_I_ACCOUNT"
                                aErrorArray(2, i) = oPayment.I_Account
                                aErrorArray(3, i) = frmCorrect.txtI_Account.Text
                            Case "BABEL_COMPANYNO"
                                aErrorArray(2, i) = oBabelFile.IDENT_Sender
                                aErrorArray(3, i) = frmCorrect.txtCompanyNo.Text
                            Case "PAYMENT_REF_OWN"
                                aErrorArray(2, i) = oPayment.REF_Own
                                aErrorArray(3, i) = frmCorrect.txtPayOwnRef.Text
                            Case "PAYMENT_E_ACCOUNT"
                                aErrorArray(2, i) = oPayment.E_Account
                                aErrorArray(3, i) = frmCorrect.txtE_Account.Text
                            Case "PAYMENT_CHEQUE"
                                aErrorArray(2, i) = oPayment.Cheque
                                aErrorArray(3, i) = frmCorrect.chkCheque.CheckState
                            Case "PAYMENT_NOTI_NOTIFICATIONMESSAGETOBANK"
                                aErrorArray(2, i) = oPayment.NOTI_NotificationMessageToBank
                                aErrorArray(3, i) = frmCorrectBank.txtBank_Info.Text
                                '         Case "PAYMENT_BANK_SWIFTCODE"
                                '            aErrorArray(2, i) = oPayment.BANK_SWIFTCode
                                '            aErrorArray(3, i) = frmCorrectBank.txtBank_ID.Text
                            Case "PAYMENT_BANK_BRANCHTYPE", "PAYMENT_BANK_BRANCHNO", "PAYMENT_BANK_SWIFTCODE"
                                '07.02.2005 - Treating BrachType, branchno and SWIFTcode as one
                                aErrorArray(1, i) = "Payment_BANK_BranchType - Payment_BANK_SWIFTCode - Payment_BANK_BranchNo"
                                aErrorArray(2, i) = oPayment.BANK_BranchType & " - " & oPayment.BANK_SWIFTCode & " - " & oPayment.BANK_BranchNo
                                If vb_Translate_FromBranchType(frmCorrectBank.cmbBank_IDType.Text) <= 1 Then 'SWIFT
                                    aErrorArray(3, i) = vb_Translate_FromBranchType(frmCorrectBank.cmbBank_IDType.Text) & " - " & frmCorrectBank.txtBank_ID.Text & " - "
                                Else
                                    aErrorArray(3, i) = vb_Translate_FromBranchType(frmCorrectBank.cmbBank_IDType.Text) & " -    - " & frmCorrectBank.txtBank_ID.Text
                                End If

                                '         Case "PAYMENT_BANK_BRANCHNO"
                                '             aErrorArray(2, i) = oPayment.BANK_BranchNo
                                '             aErrorArray(3, i) = frmCorrectBank.txtBank_ID.Text
                            Case "PAYMENT_BANK_NAME", "PAYMENT_BANK_ADR1", "PAYMENT_BANK_ADR2", "PAYMENT_BANK_ADR3"
                                '07.02.2005 - Treating BankName and adresses as one
                                aErrorArray(1, i) = "Payment_BANK_Name - Payment_BANK_Adr1 - Payment_BANK_Adr2 - Payment_BANK_Adr3"
                                aErrorArray(2, i) = oPayment.BANK_Name & " - " & oPayment.BANK_Adr1 & " - " & oPayment.BANK_Adr2 & " - " & oPayment.BANK_Adr3
                                aErrorArray(3, i) = frmCorrectBank.txtBank_Adr1.Text & " - " & frmCorrectBank.txtBank_Adr2.Text & " - " & frmCorrectBank.txtBank_Adr3.Text & " - " & frmCorrectBank.txtBank_Adr4.Text

                                '         Case "PAYMENT_BANK_ADR1"
                                '             aErrorArray(2, i) = oPayment.BANK_Adr1
                                '             aErrorArray(3, i) = frmCorrectBank.txtBank_Adr2.Text
                                '
                                '         Case "PAYMENT_BANK_ADR2"
                                '             aErrorArray(2, i) = oPayment.BANK_Adr2
                                '             aErrorArray(3, i) = frmCorrectBank.txtBank_Adr3.Text
                                '
                                '         Case "PAYMENT_BANK_ADR3"
                                '             aErrorArray(2, i) = oPayment.BANK_Adr3
                                '             aErrorArray(3, i) = frmCorrectBank.txtBank_Adr4.Text

                            Case "PAYMENT_BANK_SWIFTCODECORRBANK", "PAYMENT_BANK_BRANCHTYPECORRBANK", "PAYMENT_BANK_BRANCHNOCODECORRBANK"
                                '07.02.2005 - Treating BrachType, branchno and SWIFTcode for CorrBank as one
                                aErrorArray(1, i) = "Payment_BANK_BranchTypeCorrBank - Payment_BANK_SWIFTCodeCorrBank - Payment_BANK_BranchNoCodeCorrBank"
                                aErrorArray(2, i) = oPayment.BANK_BranchTypeCorrBank & " - " & oPayment.BANK_SWIFTCodeCorrBank & " - " & oPayment.BANK_BranchNoCorrBank
                                If vb_Translate_FromBranchType(frmCorrectBank.cmbCorrBank_IDType.Text) <= 1 Then 'SWIFT
                                    aErrorArray(3, i) = vb_Translate_FromBranchType(frmCorrectBank.cmbCorrBank_IDType.Text) & " - " & frmCorrectBank.txtCorrBank_ID.Text & " - "
                                Else
                                    aErrorArray(3, i) = vb_Translate_FromBranchType(frmCorrectBank.cmbCorrBank_IDType.Text) & " -    - " & frmCorrectBank.txtCorrBank_ID.Text
                                End If
                                'aErrorArray(2, i) = oPayment.BANK_SWIFTCodeCorrBank
                                'aErrorArray(3, i) = frmCorrectBank.txtCorrBank_ID.Text
                                '         Case "PAYMENT_BANK_BRANCHTYPECORRBANK"
                                '             aErrorArray(2, i) = oPayment.BANK_BranchTypeCorrBank
                                '             aErrorArray(3, i) = vb_Translate_FromBranchType(frmCorrectBank.cmbCorrBank_IDType.Text)
                                '         Case "PAYMENT_BANK_BRANCHNOCODECORRBANK"
                                '             aErrorArray(2, i) = oPayment.BANK_BranchNoCorrBank
                                '             aErrorArray(3, i) = frmCorrectBank.txtCorrBank_ID.Text
                                '         Case "PAYMENT_BANK_BRANCHTYPECORRBANK"
                                '             aErrorArray(2, i) = oPayment.BANK_BranchTypeCorrBank
                                '             aErrorArray(3, i) = vb_Translate_FromBranchType(frmCorrectBank.cmbCorrBank_IDType.Text)
                            Case "PAYMENT_BANK_NAMEADDRESSCORRBANK1", "PAYMENT_BANK_NAMEADDRESSCORRBANK2", "PAYMENT_BANK_NAMEADDRESSCORRBANK3", "PAYMENT_BANK_NAMEADDRESSCORRBANK4"
                                '07.02.2005 - Treating BankName and adresses for CorrBank as one
                                aErrorArray(1, i) = "Payment_BANK_NameAddressCorrBank1 - Payment_BANK_NameAddressCorrBank2 - Payment_BANK_NameAddressCorrBank3 - Payment_NameAddressCorrBank4"
                                aErrorArray(2, i) = oPayment.BANK_NameAddressCorrBank1 & " - " & oPayment.BANK_NameAddressCorrBank2 & " - " & oPayment.BANK_NameAddressCorrBank3 & " - " & oPayment.BANK_NameAddressCorrBank4
                                aErrorArray(3, i) = frmCorrectBank.txtCorrBank_Adr1.Text & " - " & frmCorrectBank.txtCorrBank_Adr2.Text & " - " & frmCorrectBank.txtCorrBank_Adr3.Text & " - " & frmCorrectBank.txtCorrBank_Adr4.Text
                                'aErrorArray(2, i) = oPayment.BANK_NameAddressCorrBank1
                                'aErrorArray(3, i) = frmCorrectBank.txtCorrBank_Adr1.Text
                                '         Case "PAYMENT_BANK_NAMEADDRESSCORRBANK2"
                                '             aErrorArray(2, i) = oPayment.BANK_NameAddressCorrBank2
                                '             aErrorArray(3, i) = frmCorrectBank.txtCorrBank_Adr2.Text
                                '         Case "PAYMENT_BANK_NAMEADDRESSCORRBANK3"
                                '             aErrorArray(2, i) = oPayment.BANK_NameAddressCorrBank3
                                '             aErrorArray(3, i) = frmCorrectBank.txtCorrBank_Adr3.Text
                                '         Case "PAYMENT_BANK_NAMEADDRESSCORRBANK4"
                                '             aErrorArray(2, i) = oPayment.BANK_NameAddressCorrBank4
                                '             aErrorArray(3, i) = frmCorrectBank.txtCorrBank_Adr4.Text
                            Case "PAYMENT_ERA_DATE"
                                aErrorArray(2, i) = oPayment.ERA_Date
                                aErrorArray(3, i) = DateToString(frmCorrectOther.txtDate_Contract.Text)
                            Case "PAYMENT_ERA_DEALMADEWITH"
                                aErrorArray(2, i) = oPayment.ERA_DealMadeWith
                                aErrorArray(3, i) = frmCorrectOther.txtERA_DealMadeWith.Text
                            Case "PAYMENT_ERA_EXCHRATEAGREED"
                                aErrorArray(2, i) = oPayment.ERA_ExchRateAgreed
                                aErrorArray(3, i) = CDbl(frmCorrectOther.txtERA_ExchangeRateAgreed.Text)
                            Case "PAYMENT_FRW_FORWARDCONTRACTNO"
                                aErrorArray(2, i) = oPayment.FRW_ForwardContractNo
                                aErrorArray(3, i) = frmCorrectOther.txtERA_ContractRef.Text
                            Case "INVOICE_STATEBANK_CODE"
                                aErrorArray(2, i) = oInvoice.STATEBANK_Code
                                'aErrorArray(3, i) = frmCorrectOther.txtStateBank_Code
                                aErrorArray(3, i) = Left$(frmCorrectOther.cmbStateBank_Code.Text, 2)
                            Case "INVOICE_STATEBANK_TEXT"
                                aErrorArray(2, i) = oInvoice.STATEBANK_Text
                                aErrorArray(3, i) = frmCorrectOther.txtStateBank_Text.Text
                            Case "PAYMENT_PRIORITY"
                                aErrorArray(2, i) = oPayment.Priority
                                aErrorArray(3, i) = frmCorrectOther.chkUrgent.CheckState
                        End Select
                    Next i
                    '
                    ' Send denne arrayen til rapport
                    ' Rapporten viser en details del, men en (to?) linjer pr element i arrayen
                    ' i tillegg til en "standarddel", som viser de mest aktuelle felt for betalingen

                    '----------------
                    ' Send to printer
                    '----------------
                    'TODO - RAPPORT
                    'Load(rp_901_Corrections)
                    'Unload(rp_901_Corrections)  ' test
                    'rp_901_Corrections.Payment(oPayment)   ' Needs payment only!
                    'rp_901_Corrections.ErrorArray(aErrorArray)
                    'rp_901_Corrections.PrintReport(True)
                    'Unload(rp_901_Corrections)

                    ''' Testing
                    'frmViewer.iReportType = 901
                    'frmViewer.Show 1   'Modal
                    '------------------------------

            End Select

        Loop  ' Loops only when printing, to show frmCorrect again

        frmCorrect.Close()
        frmCorrect = Nothing

    End Sub
    Private Sub PopulateNBCodes(ByVal frmCorrectOther As frmCorrectOther)
        Dim aCodes() As String
        Dim lCounter As Long

        'Remove entries from the cmbbox
        For lCounter = frmCorrectOther.cmbStateBank_Code.Items.Count - 1 To 0 Step -1
            frmCorrectOther.cmbStateBank_Code.Items.RemoveAt(lCounter)
        Next lCounter

        aCodes = LoadNBCodes()

        frmCorrectOther.cmbStateBank_Code.Items.Add("   ")
        If Not Array_IsEmpty(aCodes) Then
            For lCounter = 1 To UBound(aCodes, 1)
                frmCorrectOther.cmbStateBank_Code.Items.Add(aCodes(lCounter))
                If Trim$(oPayment.Invoices(1).STATEBANK_Code) = Left$(aCodes(lCounter), 2) Then
                    frmCorrectOther.cmbStateBank_Code.SelectedIndex = lCounter
                End If
            Next lCounter
        End If

    End Sub
End Class
