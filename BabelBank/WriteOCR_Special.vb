Option Strict Off
Option Explicit On
Module WriteOCR_Special
	
	'Dim oFs As Object
	'Dim oFile As TextStream
	Dim sLine As String ' en output-linje
	Dim i As Double
	Dim oBatch As Batch
	' Vi m� dimme noen variable for
	' - batchniv�: sum bel�p, antall records, antall transer, f�rste dato, siste dato
	Dim nBatchNoRecords, nBatchSumAmount, nBatchNoTransactions As Double
	Dim dBatchLastDate, dBatchFirstDate As Date
	' - fileniv�: sum bel�p, antall records, antall transer, siste dato
	Dim nFileNoRecords, nFileSumAmount, nFileNoTransactions As Double
	'Dim dFileFirstDate As Date
	
    Function WriteOCRFile_Special(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sAdditionalID As String, ByRef sClientNo As String, ByRef bFalseOCR As Boolean, ByRef bOCRTransactions As Boolean, ByRef sSpecial As String) As Object
        'bFalseOCR, False = not an original OCR-file, i.e. CREMUL
        'bOCRTransactions, False = not original OCR-transaction. i.e. transaction from CREMUL which is not of type 233.
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        'Dim op As Payment
        Dim nTransactionNo As Double
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim nExistingLines As Double
        Dim sTempFilename As String
        Dim iCount As Short
        Dim bFoundRecord89 As Boolean
        Dim oFileWrite, oFileRead As Scripting.TextStream
        Dim bAppendFile, bStartRecordWritten As Boolean
        Dim dProductionDate As Date
        Dim bKeepOppdragsNr, bFoundContractNo As Boolean
        Dim sContractNo As String
        Dim sGroupNumber As String 'Used to create a false, Sentral-ID + Dagkode +
        ' Delavregningsnr + l�penummer (for grouping on the bankstatement).
        Dim bFBOsExist, bBatchStartWritten As Boolean
        Dim nFBOTransactionNo As Double
        Dim iCounter As Short

        'For BKK
        Dim aBKKArray(,) As String
        'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
        'DOTNETT REDIM Dim aBKKArray() As String
        Dim bDateFound As Boolean
        Dim sNoMatchAccount As String
        Dim bFirstPayment As Boolean
        Dim bNegativeInvoiceExists As Boolean
        Dim sTempText, sTempText2 As String
        Dim lInvoiceCount As Integer

        Try

            oFs = New Scripting.FileSystemObject

            ' new 19.12.02, due to problems running to OCR-export connescutive (without leaving BB.EXE)
            nBatchSumAmount = 0
            nBatchNoRecords = 0
            nBatchNoTransactions = 0
            dBatchLastDate = CDate(Nothing)
            dBatchFirstDate = CDate(Nothing)
            nFileSumAmount = 0
            nFileNoRecords = 0
            nFileNoTransactions = 0

            'BKK - IS Customer9
            'ReDim aBKKArray(1, 0)

            '----------
            bKeepOppdragsNr = True

            bAppendFile = False
            bStartRecordWritten = False
            dProductionDate = DateSerial(CInt("1990"), CInt("01"), CInt("01"))
            sContractNo = ""
            sGroupNumber = ""
            bFBOsExist = False

            'Check if we are appending to an existing file.
            ' If so BB has to delete the last 89-record, and save the counters
            'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If oFs.FileExists(sFilenameOut) Then
                bAppendFile = True
                bFoundRecord89 = False
                nExistingLines = 0
                'Set oFile = oFs.OpenTextFile(FilenameIn, 1)
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTempFilename = Left(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\BBFile.tmp"
                'First count the number of lines in the existing file
                Do While Not oFile.AtEndOfStream = True
                    nExistingLines = nExistingLines + 1
                    oFile.SkipLine()
                Loop
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'Then check if one of the 5 last lines are a 89-line
                ' Store the number of lines until the 89-line
                Do Until oFile.Line = nExistingLines - 1
                    oFile.SkipLine()
                Loop
                For iCount = 1 To 0 Step -1
                    sLine = oFile.ReadLine()
                    If Len(sLine) > 8 Then
                        If Left(sLine, 8) = "NY000089" Then
                            nExistingLines = nExistingLines - iCount
                            bFoundRecord89 = True
                            Exit For
                        End If
                    End If
                Next iCount
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                If Not bFoundRecord89 Then
                    'FIX: Babelerror Noe feil med fila som ligger der fra f�r.
                    MsgBox("The existing file is not an OCR-file")
                    'UPGRADE_WARNING: Couldn't resolve default property of object WriteOCRFile_Special. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    WriteOCRFile_Special = False
                    Exit Function
                    'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFs = Nothing
                Else
                    'Copy existing file to a temporary file and delete existing file
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFs.CopyFile(sFilenameOut, sTempFilename, True)
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFs.DeleteFile(sFilenameOut)
                    'Coopy the content of the temporary file to the new file,
                    ' excluding the 89-record
                    oFileRead = oFs.OpenTextFile(sTempFilename, Scripting.IOMode.ForReading, False, 0)
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFileWrite = oFs.CreateTextFile(sFilenameOut, True, 0)
                    Do Until oFileRead.Line > nExistingLines - 1
                        sLine = oFileRead.ReadLine
                        oFileWrite.WriteLine((sLine))
                    Loop
                    'Store the neccesary counters and totalamount
                    sLine = oFileRead.ReadLine
                    nFileNoTransactions = Val(Mid(sLine, 9, 8))
                    'Subtract 1 because we have deleted the 89-line
                    nFileNoRecords = Val(Mid(sLine, 17, 8)) - 1
                    nFileSumAmount = Val(Mid(sLine, 25, 17))
                    'Kill'em all
                    oFileRead.Close()
                    'UPGRADE_NOTE: Object oFileRead may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFileRead = Nothing
                    oFileWrite.Close()
                    'UPGRADE_NOTE: Object oFileWrite may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFileWrite = Nothing
                    oFs.DeleteFile(sTempFilename)
                End If
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                        If oBatch.FormatType <> "OCR_FBO" Then
                            '05.09.2008 - Changed due to error from "510" to IsOCR
                            If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And IsOCR((oPayment.PayCode))) Or (bFalseOCR And Not bOCRTransactions And Not IsOCR((oPayment.PayCode))) Then
                                'Old code
                                'If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And oPayment.PayCode = "510") Or (bFalseOCR And Not bOCRTransactions And oPayment.PayCode <> "510") Then
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'If Not (sSpecial = "BKK" And oPayment.MATCH_Matched <> MatchStatus.Matched) Then
                                    If Not bMultiFiles Then
                                        bExportoBabel = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoBabel = True
                                                End If
                                            Else
                                                bExportoBabel = True
                                            End If
                                        End If
                                    End If
                                    'End If
                                End If
                            Else
                                Exit For
                            End If 'bFalseOCR
                            If bExportoBabel Then
                                Exit For
                            End If
                        Else
                            bFBOsExist = True
                            Exit For
                        End If ' FormatType
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    If Not bAppendFile And Not bStartRecordWritten Then
                        bStartRecordWritten = True
                        sLine = Wr_OCRFileStart(oBabel, sAdditionalID, bFalseOCR, bOCRTransactions, sSpecial)
                        oFile.WriteLine((sLine))
                    End If
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                            If oBatch.FormatType <> "OCR_FBO" Then

                                '05.09.2008 - Changed due to error from "510" to IsOCR
                                If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And IsOCR((oPayment.PayCode))) Or (bFalseOCR And Not bOCRTransactions And Not IsOCR((oPayment.PayCode))) Then
                                    'Old code
                                    'If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And oPayment.PayCode = "510") Or (bFalseOCR And Not bOCRTransactions And oPayment.PayCode <> "510") Then
                                    'If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    ' Changed by JanP 07.01.04
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                        'If Not (sSpecial = "BKK" And oPayment.MATCH_Matched <> MatchStatus.Matched) Then
                                        If Not bMultiFiles Then
                                            bExportoBatch = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                If Len(oPayment.VB_ClientNo) > 0 Then
                                                    bKeepOppdragsNr = False ' Construct oppdragsnr
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        bExportoBatch = True
                                                    End If
                                                Else
                                                    bExportoBatch = True
                                                End If
                                            End If
                                        End If
                                        'End If
                                    End If
                                Else
                                    Exit For
                                End If 'falseOCR
                            Else
                                bFBOsExist = True
                                Exit For
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                'BKK - IS Customer9
                                '                    If sSpecial = "BKK" Then
                                '                        If oBabel.Bank <> Bank.DnB Then
                                '                            ReDim aBKKArray(1, 0)
                                '                        End If
                                '                    End If
                                If bFalseOCR Then 'If FalseOCR then calculate the Oppdragsnr.
                                    bKeepOppdragsNr = False
                                    'Have to find the Avtale_ID when the import-format isn't OCR
                                    sContractNo = ""
                                    bFoundContractNo = False
                                    For Each oFilesetup In oBatch.VB_Profile.FileSetups
                                        For Each oClient In oFilesetup.Clients
                                            For Each oaccount In oClient.Accounts
                                                If oaccount.Account = oBatch.Payments.Item(1).I_Account Then
                                                    sContractNo = oaccount.ContractNo
                                                    If sSpecial = "BKK" Then
                                                        sNoMatchAccount = GetNoMatchAccount(1, (oClient.Client_ID))
                                                    End If
                                                    If Len(sContractNo) > 0 Then
                                                        bFoundContractNo = True
                                                        Exit For
                                                    End If
                                                End If
                                                If bFoundContractNo Then
                                                    Exit For
                                                End If
                                            Next oaccount
                                            If bFoundContractNo Then
                                                Exit For
                                            End If
                                        Next oClient
                                        If bFoundContractNo Then
                                            Exit For
                                        End If
                                    Next oFilesetup
                                    If Not bFoundContractNo Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        Err.Raise(35023, , LRS(35023, oBatch.Payments.Item(1).I_Account, "Avtale-ID") & vbCrLf & LRS(35004, "OCR") & vbCrLf & LRS(35005, sFilenameOut) & vbCrLf & vbCrLf & LRS(10017))
                                        '35023: No Contract-number (Avtale-ID) stated for account: %1.
                                        'Unable to create a correct OCR-file.
                                    End If
                                    'Find the GroupingNo, see declaration for explanation
                                    sGroupNumber = "99" & VB6.Format(StringToDate((oBatch.DATE_Production)), "DD") & "9" & PadLeft(Trim(Str(oBatch.Index)), 5, "0")

                                    ' New by JanP 08.01.04
                                    ' Do not set ContractNo here if we already have filled it
                                    ' in f.ex. in TreatSpecial (ref BAMA)
                                    If Len(oBatch.REF_Own) = 9 Then
                                        sContractNo = ""
                                    End If
                                End If
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then
                            nTransactionNo = 0

                            bFirstPayment = False
                            bBatchStartWritten = False


                            j = 0
                            For Each oPayment In oBatch.Payments
                                '12.01.2017 - Removed this code for BKK
                                'If sSpecial = "BKK" Then
                                '    'Can't export negative amounts. Loop through the invoices
                                '    ' and check for negatvie amounts. If neg amounts exists
                                '    ' create a new invoice and delete old ones.
                                '    bNegativeInvoiceExists = False
                                '    For Each oInvoice In oPayment.Invoices
                                '        If oInvoice.MATCH_Final Then
                                '            If oInvoice.MON_InvoiceAmount < 0 Then
                                '                bNegativeInvoiceExists = True
                                '                Exit For
                                '            End If
                                '        End If
                                '    Next oInvoice

                                '    'New 07.10.2008
                                '    ''BKK - IS Customer9
                                '    'If the payment is partly matched or proposed matched Undo the matching
                                '    'If status then are unmatched and there are more than
                                '    ' 1 invoice, treat the payment the same way as for the one with negative amounts
                                '    If oPayment.MATCH_Matched = BabelFiles.MatchStatus.PartlyMatched Or oPayment.MATCH_Matched = BabelFiles.MatchStatus.ProposedMatched Then
                                '        bNegativeInvoiceExists = True
                                '    End If

                                '    'New 09.10.2008
                                '    ''BKK - IS Customer9
                                '    'If the payment is unmatched but more than one invoice that is final
                                '    ' , treat the payment the same way as for the one with negative amounts
                                '    lInvoiceCount = 0
                                '    If oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched Then
                                '        For Each oInvoice In oPayment.Invoices
                                '            If oInvoice.MATCH_Final = True Then
                                '                lInvoiceCount = lInvoiceCount + 1
                                '                If lInvoiceCount > 1 Then
                                '                    bNegativeInvoiceExists = True
                                '                    Exit For
                                '                End If
                                '            End If
                                '        Next oInvoice
                                '    End If

                                '    If bNegativeInvoiceExists Then
                                '        sTempText = ""
                                '        For Each oInvoice In oPayment.Invoices
                                '            If oInvoice.MATCH_Final Then
                                '                sTempText2 = "Bel�p: " & ConvertFromAmountToString((oInvoice.MON_InvoiceAmount), ".", ",") & " "
                                '                If Len(oInvoice.InvoiceNo) > 0 Then
                                '                    sTempText2 = sTempText2 & "F.nr.: " & oInvoice.InvoiceNo
                                '                ElseIf Len(oInvoice.Unique_Id) > 0 Then
                                '                    sTempText2 = sTempText2 & "F.nr.: " & oInvoice.Unique_Id
                                '                End If
                                '                If Len(sTempText2) < 41 Then
                                '                    sTempText2 = PadRight(sTempText2, 40, " ")
                                '                Else
                                '                    sTempText2 = PadRight(sTempText2, 80, " ")
                                '                End If
                                '                sTempText = sTempText & sTempText2
                                '                '                                    For Each oFreeText In oInvoice.Freetexts
                                '                '                                        sTempText = sTempText & PadRight(oFreeText.Text, 40, " ")
                                '                '                                    Next oFreeText
                                '            End If
                                '        Next oInvoice
                                '        'Remove the old invoices
                                '        For lInvoiceCount = oPayment.Invoices.Count To 1 Step -1
                                '            oInvoice = oPayment.Invoices.Item(lInvoiceCount)
                                '            If oInvoice.MATCH_Final Then
                                '                If oInvoice.MATCH_Original Then
                                '                    oInvoice.MATCH_Final = False
                                '                    oInvoice.MATCH_Matched = False
                                '                    oInvoice.MATCH_ID = ""
                                '                Else
                                '                    oPayment.Invoices.Remove((oInvoice.Index))
                                '                End If
                                '            End If
                                '        Next lInvoiceCount
                                '        'Add the new invoice
                                '        oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched
                                '        oInvoice = oPayment.Invoices.Add
                                '        oInvoice.MON_InvoiceAmount = oPayment.MON_InvoiceAmount
                                '        oInvoice.MON_TransferredAmount = oPayment.MON_InvoiceAmount
                                '        oInvoice.REF_Bank = oPayment.REF_Bank1
                                '        oInvoice.REF_Own = oPayment.REF_Own
                                '        oInvoice.MATCH_Final = True
                                '        oInvoice.MATCH_Original = False
                                '        oInvoice.MATCH_Matched = False
                                '        oInvoice.MATCH_ID = sNoMatchAccount
                                '        oInvoice.ImportFormat = oPayment.ImportFormat
                                '        ' added for security 22.11.06
                                '        If Not oPayment.Cargo Is Nothing Then
                                '            oInvoice.Cargo = oPayment.Cargo
                                '        End If
                                '        oInvoice.StatusCode = oPayment.StatusCode
                                '        Do While Len(sTempText) > 0
                                '            If Len(sTempText) < 41 Then
                                '                oFreeText = oInvoice.Freetexts.Add
                                '                oFreeText.Text = sTempText
                                '                sTempText = ""
                                '            Else
                                '                oFreeText = oInvoice.Freetexts.Add
                                '                oFreeText.Text = Left(sTempText, 40)
                                '                sTempText = Mid(sTempText, 41)
                                '            End If
                                '        Loop
                                '    End If
                                'End If
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile

                                '05.09.2008 - Changed due to error from "510" to IsOCR
                                If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And IsOCR((oPayment.PayCode))) Or (bFalseOCR And Not bOCRTransactions And Not IsOCR((oPayment.PayCode))) Then
                                    'Old code
                                    'If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And oPayment.PayCode = "510") Or (bFalseOCR And Not bOCRTransactions And oPayment.PayCode <> "510") Then
                                    'If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    ' Changed by JanP 07.01.04
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then

                                        '''If Not (sSpecial = "BKK" And oPayment.MATCH_Matched <> MatchStatus.Matched) Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                If Len(oPayment.VB_ClientNo) > 0 Then
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        bExportoPayment = True
                                                    End If
                                                Else
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                        '''                            ElseIf sSpecial = "BKK" Then
                                        '''                                If oPayment.MATCH_Matched = MatchStatus.PartlyMatched Then
                                        '''                                    For Each oInvoice In oPayment.Invoices
                                        '''                                        oInvoice.MATCH_Matched = False 'MatchStatus.NotMatched
                                        '''                                        oInvoice.MATCH_ID = ""
                                        '''                                    Next oInvoice
                                        '''                                    bDateFound = False
                                        '''
                                        '''                                    '*************** REMOVED ********************
                                        '''                                    'Check if the array is empty
                                        '''                                    If aBKKArray(0, 0) = "" Then
                                        '''                                        aBKKArray(0, 0) = oPayment.DATE_Payment
                                        '''                                        aBKKArray(1, 0) = Str(oPayment.MON_InvoiceAmount)
                                        '''                                        bDateFound = True
                                        '''                                    Else
                                        '''                                        'Traverse through the array and check if the date exists
                                        '''                                        For iCounter = 0 To UBound(aBKKArray(), 2)
                                        '''                                            If aBKKArray(0, iCounter) = oPayment.DATE_Payment Then
                                        '''                                                aBKKArray(1, iCounter) = Str(Val(aBKKArray(1, iCounter) + oPayment.MON_InvoiceAmount))
                                        '''                                                bDateFound = True
                                        '''                                            End If
                                        '''                                        Next iCounter
                                        '''                                    End If
                                        '''                                    'Check if we have found the date, if not add a new element
                                        '''                                    If Not bDateFound Then
                                        '''                                        iCounter = UBound(aBKKArray(), 2) + 1
                                        '''                                        ReDim Preserve aBKKArray(1, iCounter)
                                        '''                                        aBKKArray(0, iCounter) = oPayment.DATE_Payment
                                        '''                                        aBKKArray(1, iCounter) = Str(oPayment.MON_InvoiceAmount)
                                        '''                                    End If
                                        ''''                                    If Len(sPaymentDate) = 0 Then
                                        ''''                                        sPaymentDate =VB6.Format(StringToDate(oPayment.DATE_Payment), "ddmmyy")
                                        ''''                                    End If
                                        '''                                    oPayment.MATCH_Matched = MatchStatus.NotMatched
                                        '''                                    oPayment.Exported = True
                                        '''                                    '*************** REMOVED ********************
                                        '''                                    'bExportoPayment = True
                                        '''                                Else 'Not matched
                                        '''                                    bDateFound = False
                                        '''                                    'Check if the array is empty
                                        '''                                    If aBKKArray(0, 0) = "" Then
                                        '''                                        aBKKArray(0, 0) = oPayment.DATE_Payment
                                        '''                                        aBKKArray(1, 0) = Str(oPayment.MON_InvoiceAmount)
                                        '''                                        bDateFound = True
                                        '''                                    Else
                                        '''                                        'Traverse through the array and check if the date exists
                                        '''                                        For iCounter = 0 To UBound(aBKKArray(), 2)
                                        '''                                            If aBKKArray(0, iCounter) = oPayment.DATE_Payment Then
                                        '''                                                aBKKArray(1, iCounter) = Str(Val(aBKKArray(1, iCounter) + oPayment.MON_InvoiceAmount))
                                        '''                                                bDateFound = True
                                        '''                                            End If
                                        '''                                        Next iCounter
                                        '''                                    End If
                                        '''                                    'Check if we have found the date, if not add a new element
                                        '''                                    If Not bDateFound Then
                                        '''                                        iCounter = UBound(aBKKArray(), 2) + 1
                                        '''                                        ReDim Preserve aBKKArray(1, iCounter)
                                        '''                                        aBKKArray(0, iCounter) = oPayment.DATE_Payment
                                        '''                                        aBKKArray(1, iCounter) = Str(oPayment.MON_InvoiceAmount)
                                        '''                                    End If
                                        ''''                                    If Len(sPaymentDate) = 0 Then
                                        ''''                                        sPaymentDate =VB6.Format(StringToDate(oPayment.DATE_Payment), "ddmmyy")
                                        ''''                                    End If
                                        '''                                    oPayment.Exported = True
                                        '''                                End If
                                        '''                            End If
                                    End If
                                End If 'falseOCR
                                If bExportoPayment Then
                                    If Not bBatchStartWritten Then
                                        i = i + 1
                                        sLine = Wr_OCRBatchStart(oBatch, bKeepOppdragsNr, sContractNo, False)
                                        oFile.WriteLine((sLine))
                                        bBatchStartWritten = True
                                    End If
                                    j = j + 1

                                    For Each oInvoice In oPayment.Invoices
                                        If (bFalseOCR And Not bOCRTransactions And Not oInvoice.MATCH_Final) Then
                                            'Nothing to do. This is not a final transaction, and should therefore not be exported.
                                        Else
                                            If Not (sSpecial = "BKK" And oInvoice.MON_InvoiceAmount = 0) Then
                                                ' Add to transactionno
                                                nTransactionNo = nTransactionNo + 1
                                                sLine = Wr_OCRPayOne(oPayment, oInvoice, nTransactionNo, sGroupNumber, bFalseOCR, bOCRTransactions, sSpecial)
                                                oFile.WriteLine((sLine))
                                                sLine = Wr_OCRPayTwo(oPayment, nTransactionNo, bFalseOCR, sSpecial)
                                                oFile.WriteLine((sLine))
                                                oPayment.Exported = True
                                            Else
                                                oPayment.Exported = True
                                            End If
                                        End If
                                    Next oInvoice 'invoice
                                End If

                            Next oPayment ' payment

                            '''                If sSpecial = "BKK" Then
                            '''                    If oBabel.Bank <> Bank.DnB Then
                            '''                        'Add an extra record for unmatched items, and add batch_end
                            '''
                            '''                        If aBKKArray(0, 0) <> "" Then
                            '''                            If UBound(aBKKArray(), 2) > 1 Then
                            '''                                Err.Raise 1, "WriteOCRFile_Special_BKK", "Det ble funnet to forskjellige valuteringsdatoer p� en BBS-post" & vbCrLf & _
                            ''''                                  "Det er ikke tatt h�yde for dette i eksportfilen til BKK."
                            '''                            End If
                            '''                            For iCounter = 0 To UBound(aBKKArray(), 2)
                            '''                                'If no payments are matched, then the batchstart is not written
                            '''                                ' We need to write it no
                            '''                                If Not bBatchStartWritten Then
                            '''                                    i = i + 1
                            '''                                    sLine = Wr_OCRBatchStart(oBatch, bKeepOppdragsNr, sContractNo, False)
                            '''                                    oFile.WriteLine (sLine)
                            '''                                    bBatchStartWritten = True
                            '''                                End If
                            '''                                ' Add to Batch-totals:
                            '''                                nBatchSumAmount = nBatchSumAmount + Val(aBKKArray(1, iCounter))
                            '''                                nBatchNoRecords = nBatchNoRecords + 2  'both 30 and 31, payment1 and 2
                            '''                                nBatchNoTransactions = nBatchNoTransactions + 1
                            '''                                'Write 30-record
                            '''                                sLine = "NY091030"
                            '''                                sLine = sLine & PadLeft(Str$(nTransactionNo), 7, "0") ' transnr
                            '''                                sLine = sLine &VB6.Format(StringToDate(aBKKArray(0, iCounter)), "ddmmyy")
                            '''                                sLine = sLine & sGroupNumber
                            '''                                sLine = sLine & "0"              ' Filler
                            '''                                sLine = sLine & PadLeft(Str(aBKKArray(1, iCounter)), 17, "0") ' Amount
                            '''                                sLine = sLine & PadLeft(sNoMatchAccount, 25, " ") ' KID
                            '''
                            '''                                sLine = PadLine(sLine, 80, "0")
                            '''                                oFile.WriteLine (sLine)
                            '''
                            '''                                'Write 31-record
                            '''                                sLine = "NY091031"
                            '''                                sLine = sLine & PadLeft(Str(nTransactionNo), 7, "0") ' transnr
                            '''
                            '''                                sLine = sLine & String(10, "0") ' Form no (Blankettnr.)
                            '''                                sLine = sLine & String(9, "0") ' ContractID/Archiveref
                            '''                                sLine = sLine & String(7, "0") ' Postgiro account
                            '''                                sLine = sLine &VB6.Format(StringToDate(aBKKArray(0, iCounter)), "ddmmyy")
                            '''                                sLine = sLine & String(11, "0") 'E_Account
                            '''
                            '''                                sLine = PadLine(sLine, 80, "0")
                            '''                                oFile.WriteLine (sLine)
                            '''
                            '''                                'Write Batch end
                            '''                                sLine = Wr_OCRBatchEnd(oBatch, False)
                            '''                                oFile.WriteLine (sLine)
                            '''                            Next iCounter
                            '''                            ReDim aBKKArray(1, 0)
                            '''                        Else
                            '''                            'Write Batch end
                            '''                            sLine = Wr_OCRBatchEnd(oBatch, False)
                            '''                            oFile.WriteLine (sLine)
                            '''                        End If
                            '''
                            '''                    Else
                            '''                        If bBatchStartWritten Then
                            '''                            'This is DnB so the payment was matched, and therefore
                            '''                            '   we have to add a batch-end
                            '''                            'Write Batch end
                            '''                            sLine = Wr_OCRBatchEnd(oBatch, False)
                            '''                            oFile.WriteLine (sLine)
                            '''                        Else
                            '''                            'No transactions written, don't write the batch end
                            '''                        End If
                            '''                    End If
                            '''                Else
                            sLine = Wr_OCRBatchEnd(oBatch, False)
                            oFile.WriteLine((sLine))
                            '''                End If

                            If StringToDate((oBatch.DATE_Production)) > dProductionDate Then
                                dProductionDate = StringToDate((oBatch.DATE_Production))
                            End If

                        End If
                    Next oBatch 'batch

                End If

                '''    If sSpecial = "BKK" Then
                '''        If aBKKArray(0, 0) <> "" Then
                '''            For iCounter = 0 To UBound(aBKKArray(), 2)
                '''                'Ok, we have at least 1 unmatched item. Write a new batch with this info
                '''                sLine = "NY090020"
                '''                sLine = sLine & PadLeft(sContractNo, 9, "0") ' avtaleid
                '''                sLine = sLine & "0000001" ' oppdragsnr
                '''                sLine = sLine & PadLeft(sI_Account, 11, "0") ' oppdragskonto
                '''                sLine = PadLine(sLine, 80, "0")
                '''                oFile.WriteLine sLine
                '''
                '''                'Write 30-record
                '''                sLine = "NY091030"
                '''                sLine = sLine & PadLeft(Str$(nTransactionNo), 7, "0") ' transnr
                '''                sLine = sLine &VB6.Format(StringToDate(aBKKArray(0, iCounter)), "ddmmyy")
                '''                sLine = sLine & sGroupNumber
                '''                sLine = sLine & "0"              ' Filler
                '''                sLine = sLine & PadLeft(Trim$(aBKKArray(1, iCounter)), 17, "0") ' Amount
                '''                sLine = sLine & PadLeft(sNoMatchAccount, 25, " ") ' KID
                '''
                '''                sLine = PadLine(sLine, 80, "0")
                '''                oFile.WriteLine (sLine)
                '''
                '''                'Write 31-record
                '''                sLine = "NY091031"
                '''                sLine = sLine & PadLeft(Str(nTransactionNo), 7, "0") ' transnr
                '''
                '''                sLine = sLine & String(10, "0") ' Form no (Blankettnr.)
                '''                sLine = sLine & String(9, "0") ' ContractID/Archiveref
                '''                sLine = sLine & String(7, "0") ' Postgiro account
                '''                sLine = sLine &VB6.Format(StringToDate(aBKKArray(0, iCounter)), "ddmmyy")
                '''                sLine = sLine & String(11, "0") 'E_Account
                '''
                '''                sLine = PadLine(sLine, 80, "0")
                '''                oFile.WriteLine (sLine)
                '''
                '''                'Write Batch end
                '''
                '''                'nBatchNoRecords = nBatchNoRecords + 1  'Also count 88-recs
                '''        '        ' Add to File-totals:
                '''        '        nFileSumAmount = nFileSumAmount + nBatchSumAmount
                '''        '        nFileNoRecords = nFileNoRecords + nBatchNoRecords
                '''        '        nFileNoTransactions = nFileNoTransactions + nBatchNoTransactions
                '''
                '''                sLine = "NY090088"
                '''
                '''                sLine = sLine & "00000001" ' No of transactions in batch (bel�pspost1 + 2 = 1)
                '''                sLine = sLine & "00000004" ' No of records in batch (including 20 and 88)
                '''
                '''                sLine = sLine & PadLeft(Trim$(aBKKArray(1, iCounter)), 17, "0") ' Total amount in batch
                '''                sLine = sLine &VB6.Format(StringToDate(aBKKArray(0, iCounter)), "ddmmyy")
                '''                sLine = sLine &VB6.Format(StringToDate(aBKKArray(0, iCounter)), "ddmmyy") ' First date in batch,DDMMYY
                '''                sLine = sLine &VB6.Format(StringToDate(aBKKArray(0, iCounter)), "ddmmyy") ' First date in batch, DDMMYY
                '''                sLine = PadLine(sLine, 80, "0")
                '''
                '''                oFile.WriteLine (sLine)
                '''
                '''                'To update counters
                '''                ' Add to File-totals:
                '''                nFileSumAmount = nFileSumAmount + Val(aBKKArray(1, iCounter))
                '''                nFileNoRecords = nFileNoRecords + 4
                '''                nFileNoTransactions = nFileNoTransactions + 1
                '''                sLine = ""
                '''            Next iCounter
                '''            ReDim aBKKArray(1, 0)
                '''        End If
                '''    End If

            Next oBabel

            'New code 07.11.2002 by Iggy
            If bFBOsExist Then
                'FBOs exist, and we have to write a new Oppdrag (Batch).
                For Each oBabel In oBabelFiles
                    For Each oBatch In oBabel.Batches
                        bBatchStartWritten = False
                        nFBOTransactionNo = 0
                        If oBatch.FormatType = "OCR_FBO" Then
                            For Each oPayment In oBatch.Payments
                                If sI_Account = oPayment.I_Account Then
                                    If Not bStartRecordWritten Then
                                        sLine = Wr_OCRFileStart(oBabel, sAdditionalID, bFalseOCR, bOCRTransactions, sSpecial)
                                        oFile.WriteLine((sLine))
                                        bStartRecordWritten = True
                                    End If
                                    'Write the start Batch
                                    ' Only 1 batch per account, and new
                                    '   batch for a new account.
                                    If Not bBatchStartWritten Then
                                        sLine = Wr_OCRBatchStart(oBatch, bKeepOppdragsNr, sContractNo, True)
                                        oFile.WriteLine((sLine))
                                        bBatchStartWritten = True
                                    End If
                                    nFBOTransactionNo = nFBOTransactionNo + 1
                                    sLine = Wr_FBO(oPayment, nFBOTransactionNo)
                                    oFile.WriteLine((sLine))
                                    oPayment.Exported = True
                                End If
                            Next oPayment
                            'If the start is written, write the end of the batch
                            If bBatchStartWritten Then
                                sLine = Wr_OCRBatchEnd(oBatch, True)
                                oFile.WriteLine((sLine))
                            End If
                        End If
                    Next oBatch
                Next oBabel
            End If

            'End new code 07.11.2002 by Iggy
            'Changed 17/10 by Kjell
            'This code is taken out of the For each oBabel.Babelfiles loop (at the end)
            'Changed 28/11 by Kjell
            'That was not a good idea. If there are no records to export, don't write the OCRFileEnd

            If nFileNoRecords > 0 Then
                sLine = Wr_OCRFileEnd(dProductionDate)
                oFile.WriteLine((sLine))
                oFile.Close()
                oFile = Nothing
            Else
                oFile.Close()
                oFile = Nothing
                oFs.DeleteFile((sFilenameOut))
            End If

        Catch ex As Exception

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            Throw New vbBabel.Payment.PaymentException("Function: WriteOCRFile_Special" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteOCRFile_Special = True

    End Function
    Function Wr_OCRFileStart(ByRef oBabelFile As BabelFile, ByRef sAdditionalID As String, ByRef bFalseOCR As Boolean, ByRef bOCRTransactions As Boolean, Optional ByRef sSpecial As String = "") As String

        Dim sLine As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileNoRecords = 1
        nFileNoTransactions = 0

        sLine = "NY000010"
        ' husk evt ledende 0-er!

        'Changed 16/06-2003, Kjell
        'From
        'Changed Sender and receiver, as in the import of OCR

        If bFalseOCR Then
            '24/1-04 Added the next IF. No special reason, but it  seemed logical to
            '  write the BBS-ID on real OCR-transactions
            If bOCRTransactions Then
                sLine = sLine & "00008080"
            Else
                sLine = sLine & "82735532"
            End If
        Else
            If Len(oBabelFile.IDENT_Sender) = 0 Then
                sLine = sLine & "00008080"
            Else
                sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Sender, 1, 8), 8, "0") ' dataavsender
            End If
        End If

        '05.11.2008 - New for BKK - added sSpecial and the next IF
        If sSpecial = "BKK" Then
            sLine = sLine & Right(Right(VB6.Format(Now, "mmdd"), 3) & VB6.Format(Now, "NnSs"), 7) ' forsendelsesnr
        ElseIf Len(oBabelFile.File_id) < 8 Then
            sLine = sLine & PadLeft(Mid(oBabelFile.File_id, 1, 7), 7, "0") ' forsendelsesnr
        Else
            sLine = sLine & Right(oBabelFile.File_id, 7)
        End If
        If sAdditionalID <> "" Then
            sLine = sLine & PadLeft(sAdditionalID, 8, "0")
        Else
            sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Receiver, 1, 8), 8, "0") ' datamottaker
        End If

        sLine = PadLine(sLine, 80, "0")
        Wr_OCRFileStart = sLine

    End Function
    Private Function Wr_OCRPayOne(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef nTransactionNo As Double, ByRef sGroupNumber As String, ByRef bFalseOCR As Boolean, ByRef bOCRTransactions As Boolean, ByRef sSpecial As String) As String

        Dim sLine As String

        ' Add to Batch-totals:
        nBatchSumAmount = nBatchSumAmount + oInvoice.MON_TransferredAmount
        nBatchNoRecords = nBatchNoRecords + 2 'both 30 and 31, payment1 and 2
        nBatchNoTransactions = nBatchNoTransactions + 1

        If StringToDate((oPayment.DATE_Payment)) > dBatchLastDate Then
            dBatchLastDate = StringToDate((oPayment.DATE_Payment))
        End If
        If StringToDate((oPayment.DATE_Payment)) < dBatchFirstDate Then
            dBatchFirstDate = StringToDate((oPayment.DATE_Payment))
        End If

        sLine = "NY09"
        ' trekker transtype fra Babelbanks interne kodesett
        '07.10.2008 added next IF
        If sSpecial = "BKK" Then
            sLine = sLine & "10"
        Else
            sLine = sLine & bbGetPayCode(Trim(oPayment.PayCode), "OCR")
        End If
        sLine = sLine & "30"
        sLine = sLine & PadLeft(Str(nTransactionNo), 7, "0") ' transnr

        If StringToDate((oPayment.DATE_Payment)) > (System.Date.FromOADate(Now.ToOADate - 1000)) Then
            ' hvis vi har lagret en dato, bruk denne
            sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Payment)), "ddmmyy")
        Else
            ' vi har ikke lagret dato, sett inn dagens dato
            sLine = sLine & VB6.Format(Now, "ddmmyy")
        End If
        'Possible error since REF_Own consists of 4 different fields in the format. If some are
        'omitted it may affect the others

        If Len(sGroupNumber) > 0 Then
            sLine = sLine & sGroupNumber
        Else
            sLine = sLine & oPayment.REF_Own ' First 2pos. Sentral ID, 3-4 Dagkode
            ' 5 Delavregningsnr., 6-10 L�penummer
        End If
        sLine = sLine & "0" ' Filler
        sLine = sLine & PadLeft(Str(oInvoice.MON_TransferredAmount), 17, "0") ' Amount
        '20.11-03, KID ligger i Match_MatchID for avstemte transaksjoner
        If bFalseOCR And Not bOCRTransactions Then
            sLine = sLine & PadLeft(Trim(oInvoice.MATCH_ID), 25, " ") ' KID
        Else
            sLine = sLine & PadLeft(oInvoice.Unique_Id, 25, " ") ' KID
        End If

        sLine = PadLine(sLine, 80, "0")
        Wr_OCRPayOne = sLine


    End Function

    Private Function Wr_OCRPayTwo(ByRef oPayment As Payment, ByRef nTransactionNo As Double, ByRef bFalseOCR As Boolean, ByRef sSpecial As String) As String

        Dim sLine, sTempString As String
        Dim lCounter As Integer

        sLine = "NY09"
        ' trekker transtype fra Babelbank sitt interne kodesett
        '07.10.2008 added next IF
        If sSpecial = "BKK" Then
            sLine = sLine & "10"
        Else
            sLine = sLine & bbGetPayCode(Trim(oPayment.PayCode), "OCR")
        End If
        sLine = sLine & "31"
        sLine = sLine & PadLeft(Str(nTransactionNo), 7, "0") ' transnr

        sLine = sLine & PadLeft(oPayment.REF_Bank2, 10, "0") ' Form no (Blankettnr.)
        If bFalseOCR Then
            'This field is numeric, so when we create a false-OCR-file we replace alfasigns with 0.
            If IsNumeric(oPayment.REF_Bank1) Then
                sLine = sLine & PadLeft(Mid(oPayment.REF_Bank1, 1, 9), 9, "0") ' ContractID/Archiveref
            Else
                sTempString = Trim(oPayment.REF_Bank1)
                For lCounter = 1 To Len(sTempString)
                    If Not IsNumeric(Mid(sTempString, lCounter, 1)) Then
                        If Not lCounter = Len(sTempString) Then
                            sTempString = Left(sTempString, lCounter - 1) & "0" & Mid(sTempString, lCounter + 1)
                        Else
                            sTempString = Left(sTempString, lCounter - 1) & "0"
                        End If
                    End If
                Next lCounter
                sLine = sLine & PadLeft(sTempString, 9, "0") ' ContractID/Archiveref
            End If
        Else
            sLine = sLine & PadLeft(Mid(oPayment.REF_Bank1, 1, 9), 9, "0") ' ContractID/Archiveref
        End If
        sLine = sLine & PadLeft(Mid(oPayment.ExtraD5, 1, 7), 7, "0") ' Postgiro account
        If oPayment.DATE_Value = "19900101" Then
            sLine = sLine & New String("0", 6)
        Else
            sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Value)), "DDMMYY") ' Bankdate
        End If

        sLine = sLine & PadLeft(Mid(oPayment.E_Account, 1, 11), 11, "0")

        sLine = PadLine(sLine, 80, "0")

        Wr_OCRPayTwo = sLine

    End Function
    Private Function Wr_FBO(ByRef oPayment As Payment, ByRef nFBOTransactionNo As Double) As String

        Dim sLine As String

        ' Add to Batch-totals:
        nBatchNoRecords = nBatchNoRecords + 1 'just a 70-record
        nBatchNoTransactions = nBatchNoTransactions + 1

        'If StringToDate(oPayment.DATE_Payment) > dBatchLastDate Then
        '    dBatchLastDate = StringToDate(oPayment.DATE_Payment)
        'End If
        'If StringToDate(oPayment.DATE_Payment) < dBatchFirstDate Then
        '    dBatchFirstDate = StringToDate(oPayment.DATE_Payment)
        'End If

        sLine = "NY219470"
        sLine = sLine & PadLeft(Str(nFBOTransactionNo), 7, "0") ' transnr
        ' trekker transtype fra Babelbanks interne kodesett
        sLine = sLine & bbGetPayCode(Trim(oPayment.PayCode), "OCR_FBO")
        sLine = sLine & PadLeft(oPayment.Invoices.Item(1).Unique_Id, 25, " ") ' KID
        If oPayment.NOTI_NotificationType = "PAPER" Then
            sLine = sLine & "J"
        Else
            sLine = sLine & "N"
        End If

        sLine = PadLine(sLine, 80, "0")
        Wr_FBO = sLine


    End Function

    Private Function Wr_OCRFileEnd(ByRef dProductionDate As Date) As String

        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1 ' also count 89-recs

        sLine = "NY000089"

        sLine = sLine & PadLeft(Str(nFileNoTransactions), 8, "0") ' No of transactions in file (bel�pspost1 + 2 = 1)
        sLine = sLine & PadLeft(Str(nFileNoRecords), 8, "0") ' No of records in file (including 20 and 88)
        sLine = sLine & PadLeft(Str(nFileSumAmount), 17, "0") ' Total amount in file

        If dProductionDate > DateSerial(CInt("1999"), CInt("12"), CInt("01")) Then
            sLine = sLine & VB6.Format(dProductionDate, "DDMMYY")
        Else
            sLine = sLine & VB6.Format(Now, "ddmmyy") ' BBS date (Productiondate of file)
        End If

        ' pad with 0 to 80
        sLine = PadLine(sLine, 80, "0")

        Wr_OCRFileEnd = sLine

    End Function
    Function Wr_OCRBatchStart(ByRef oBatch As Batch, ByRef bKeepOppdragsNr As Boolean, ByRef sContractNo As String, ByRef bFBO As Boolean) As String
        Dim sLine As String
        Static lTaskNumber As Integer

        ' Reset Batch-totals:
        nBatchSumAmount = 0
        nBatchNoRecords = 1
        nBatchNoTransactions = 0
        dBatchLastDate = System.Date.FromOADate(Now.ToOADate - 1000)
        dBatchFirstDate = System.Date.FromOADate(Now.ToOADate + 1000)

        If bFBO Then
            sLine = "NY212420"
        Else
            sLine = "NY090020"
        End If
        If Len(sContractNo) > 0 Then 'Only when FalseOCR
            sLine = sLine & PadLeft(sContractNo, 9, "0") ' avtaleid
        Else
            sLine = sLine & PadLeft(Mid(oBatch.REF_Own, 1, 9), 9, "0") ' avtaleid
        End If
        If bKeepOppdragsNr Then
            sLine = sLine & PadLeft(Mid(oBatch.Batch_ID, 1, 7), 7, "0") ' oppdragsnr
        Else
            ' To be used when we split on other than accountno, and for FalseOCR
            lTaskNumber = lTaskNumber + 1
            sLine = sLine & PadLeft(Str(lTaskNumber), 7, "0") ' oppdragsnr
        End If
        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sLine = sLine & PadLeft(Mid(oBatch.Payments(1).I_Account, 1, 11), 11, "0") ' oppdragskonto

        sLine = PadLine(sLine, 80, "0")

        Wr_OCRBatchStart = sLine

    End Function

    Function Wr_OCRBatchEnd(ByRef oBatch As Batch, ByRef bFBO As Boolean) As String
        Dim sLine As String

        nBatchNoRecords = nBatchNoRecords + 1 'Also count 88-recs

        ' Add to File-totals:
        nFileSumAmount = nFileSumAmount + nBatchSumAmount
        nFileNoRecords = nFileNoRecords + nBatchNoRecords
        nFileNoTransactions = nFileNoTransactions + nBatchNoTransactions

        sLine = ""
        If bFBO Then
            sLine = "NY212488"
        Else
            sLine = "NY090088"
        End If

        sLine = sLine & PadLeft(Str(nBatchNoTransactions), 8, "0") ' No of transactions in batch (bel�pspost1 + 2 = 1)
        sLine = sLine & PadLeft(Str(nBatchNoRecords), 8, "0") ' No of records in batch (including 20 and 88)

        If bFBO Then
            sLine = sLine & "00000000000000000" 'Total amount
            sLine = sLine & "000000" ' BBS date (todays date)
            sLine = sLine & "000000" ' First date in batch,DDMMYY
            sLine = sLine & "000000" ' First date in batch, DDMMYY
        Else
            sLine = sLine & PadLeft(Str(nBatchSumAmount), 17, "0") ' Total amount in batch
            If oBatch.DATE_Production <> "19900101" Then
                sLine = sLine & VB6.Format(StringToDate((oBatch.DATE_Production)), "DDMMYY")
            Else
                sLine = sLine & VB6.Format(Now, "ddmmyy") ' BBS date (todays date)
            End If
            sLine = sLine & VB6.Format(dBatchFirstDate, "ddmmyy") ' First date in batch,DDMMYY
            sLine = sLine & VB6.Format(dBatchLastDate, "ddmmyy") ' First date in batch, DDMMYY
        End If

        sLine = PadLine(sLine, 80, "0")

        Wr_OCRBatchEnd = sLine

    End Function
End Module
