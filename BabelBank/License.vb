Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
<System.Runtime.InteropServices.ProgId("License_NET.License")> Public Class License
	'This class handles license information
	
	
	'Parameters for license:
	'- nLicUsedRecords      No of used paymentrecords
	'- nLicLimitRecords     No of available paymentrecords in license
	'- (nLicNoOfUsers)
	'- (nLicNoOfComputers)
	'- nFormatsKey          Key calculated out of which formats allowed
    ' XNET 15.04.2013 added comment V = Visma
    '- sLicServicesAvailable Which of BabelBanks service is user licensed to use ?
    '                        "E"=EDI "F"=FileDoubleControl "T" = DnBNOR TBI V = Visma P=BabelBank Pro
    '- sLicDateLastUsed     Date last time records were imported
	'- nDateReset           Number of times systemdate have been adjusted (back in time)
	'- sLicLimitDate        Timeoutdate if limited license
	'
	'- sLicensNo            License number
	'- sLicenseName         License registered to
	'- nLicLimitFormats     No of registered formats
	'- sLicSupport             "T" if user have support-contract with VisualBanking, else "F"
	
	' Methods:
	'=========
	'-v LicRead()          Read license data from license.dat into static variables
	'-v LicWrite()         Write licensedata to license.dat
	'-v LicCrypt()         Crypt data before writing to database
	'-v LicDecrypt()       Decrypt data after reading from database
	
	'-v LicFreeRecords()   Returns no of available payments
	'-v LicWarning()       Warning if few records available
	'-v LicOkToContinue()  Any free records, not past limitdate ?
	'-v LicDaysRemaining() No of days remaining if testlicense
	'-v LicUpdate()        Update used records
	'-v LicUpgrade()       Read external licensefile, or mail, and upgrade licensfile
	
	' Wait:
	'-LicenseMailUpgrade()  Produce mail for order of upgrade of license
	'-LicenseLetterUpgrade()Produce letter for order of upgrade of license
	'-LicenseFormatAllowed()Is actual format licensed?
	
	' Store licensdata in file babel.lic
	' Crypt some of the info in table
	'====================================================================================
	
	Private nLicUsedRecords As Double 'No of used paymentrecords
	Private nLicPreviousRecords As Double ' No of used payments, until last time
	Private nLicLimitRecords As Double 'No of available paymentrecords in license
	Private nLicNoOfUsers As Short
	Private nLicNoOfComputers As Short
	Private sLicServicesAvailable As String 'Which of BabelBanks service is user licensed to use ?
	Private sLicDateLastUsed As String 'Date last time records were imported
	Private sLicLimitDate As String 'Timeoutdate if limited license
	Private sLicNumber As String 'License number
	Private sLicName As String 'License registered to
	Private nLicDateResets As Short ' No of times date is reset (for testlicenses)
	'Fully qualified path
	Private sLicFileName As String ' Name of file (app.path +  "\babel.lic")
    Private nLicLimitFormats As Integer ' No of available formats
	Private sLicSupport As String ' "Ordinary" if supportcontract, else "None" or blank
	
	'Determines whether the lic file has been opened or not
	Private bOK As Boolean
	Private bAnythingChanged As Boolean
	
	'********* START PROPERTY SETTINGS ***********************
	Public ReadOnly Property LicUsedRecords() As Object
		Get
            LicUsedRecords = nLicUsedRecords
		End Get
	End Property
    Public ReadOnly Property LicLimitRecords() As Double
        Get
            LicLimitRecords = nLicLimitRecords
        End Get
    End Property
    Public ReadOnly Property LicLimitFormats() As Integer
        Get
            LicLimitFormats = nLicLimitFormats
        End Get
    End Property
    Public ReadOnly Property LicServicesAvailable() As String
        Get
            LicServicesAvailable = sLicServicesAvailable
        End Get
    End Property
    Public ReadOnly Property LicDateLastUsed() As String
        Get
            LicDateLastUsed = sLicDateLastUsed
        End Get
    End Property
    Public ReadOnly Property LicLimitDate() As String
        Get
            LicLimitDate = sLicLimitDate
        End Get
    End Property
    Public Property LicNumber() As String
        Get
            LicNumber = sLicNumber
        End Get
        Set(ByVal Value As String)
            sLicNumber = Value
            bAnythingChanged = True
        End Set
    End Property
    Public Property LicName() As String
        Get
            LicName = sLicName
        End Get
        Set(ByVal Value As String)
            sLicName = Value
            bAnythingChanged = True
        End Set
    End Property
    Public ReadOnly Property LicSupport() As String
        Get
            LicSupport = sLicSupport
        End Get
    End Property
	'********* END PROPERTY SETTINGS ***********************
	
	Public Function LicCode(ByRef sCode As String) As Boolean
        ' Check licensecode keyed in by user
        ' either a 21 digitcode for 1 or 3 years license to 10.03,.., or 5 days extension
        ' A 30-digit code holds info about
		' - limitnumber of records
		' - limitdate
		' - no of formats
		' - support or not
		' - services available
        ' 06.01.2017
        ' added 3 new possible keys;
        ' extend licdate to
        ' 10.03 + 3 years - licensecode starts with 3-
        ' 10.03 + 1 year - licensecode starts with 1-
        ' 5 days from licensecode is released - licensecode starts with 5-

        Dim bRetValue As Boolean
        Dim sTmp As String
		
		bRetValue = False

		'No need to set bAnythingChanged = True because the change is written anyway
		
		' Interpred given code;
		' There is a special code, a one time quickcode, which adds 14 days to limitdate,
		' and increases LimitRecords by 10 percent.
		' Starts with s, followed by second digit in todays daynumber of month, then 5
		' then 3, then first digit in todays daynumber of month
		If UCase(Left(sCode, 1)) = "S" Then
			' Only "quickcode" can start with S
			If Mid(sCode, 3, 2) = "53" Then
				' check day of month;
				If Int(CDbl(Mid(sCode, 5, 1) & Mid(sCode, 2, 1))) = VB.Day(Now) Then
					bRetValue = True
					' add 10 days to limitdate
					sLicLimitDate = Trim(Str(Year(System.Date.FromOADate(Now.ToOADate + 10))) & Right("0" & Trim(Str(Month(System.Date.FromOADate(Now.ToOADate + 10)))), 2) & Right("0" & Trim(Str(VB.Day(System.Date.FromOADate(Now.ToOADate + 10)))), 2))
					
					' add 10 percent to limitrecords
					nLicLimitRecords = Int(nLicLimitRecords * 1.1)
				End If
            End If

        ElseIf sCode.Length = 21 Then
            ' License extended to 10.03 + 1 or 3 years, or 5 days - always length = 17
            ' Check licensenumber, from pos 1 to 5 in code
            sTmp = LicDecrypt(Mid(sCode, 1, 5))

            If sTmp <> sLicNumber Then
                Err.Raise(10055, , LRS(10063)) ' 10063: Lisencenumber in the licensecodeis different from the licensecode in the licensefile.
                bOK = False
            End If

            ' Check licensecode release limitdate, from pos 6 to 13 in code YYYYMMDD
            sTmp = LicDecrypt(Mid(sCode, 6, 8))

            ' Check the release limit date
            If Left(sTmp, 2) <> "20" Or IsNumeric(sTmp) = False Or sTmp.Length <> 8 Then
                Err.Raise(10064, , LRS(10064)) ' Something wrong with the licensecode
                bOK = False
            End If

            ' Check that license is used within 5 days after release
            'If Int(DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, DateSerial(CInt(Left(sTmp, 2)), CInt(Mid(sTmp, 5, 2)), CInt(Right(sTmp, 2))))) > 5 Then
            If Int(DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, StringToDate(sTmp))) < 0 Then
                Err.Raise(10055, , LRS(10055)) ' Licensupgradefile is to old
                bOK = False
            End If
            ' set new licensedate, from pos 14-21 in code YYYYMMDD
            sTmp = LicDecrypt(Mid(sCode, 14, 8))
            ' Check the new licensedate
            If Left(sTmp, 2) <> "20" Or IsNumeric(sTmp) = False Or sTmp.Length <> 8 Then
                Err.Raise(10064, , LRS(10064)) ' Something wrong with the licensecode
                bOK = False
            End If

            sLicLimitDate = sTmp
            bRetValue = True

            'ElseIf UCase(Left(sCode, 2)) = "1-" Then
            '    ' License extended to 10.03 + 1 year - licensecode starts with 1-
            '    ' Check licensenumber, from pos 3 to 5 in code
            '    sTmp = LicDecrypt(Mid(sCode, 3, 5))

            '    If sTmp <> sLicNumber Then
            '        Err.Raise(10055, , LRS(10063)) ' 10063: Lisencenumber in the licensecodeis different from the licensecode in the licensefile.
            '        bOK = False
            '    End If

            '    ' Check licensecode release limitdate, from pos 8 to 15 in code YYYYMMDD
            '    sTmp = LicDecrypt(Mid(sCode, 8, 8))

            '    ' Check the release limit date
            '    If Left(sTmp, 2) <> "20" Or IsNumeric(sTmp) = False Or sTmp.Length <> 6 Then
            '        Err.Raise(10064, , LRS(10064)) ' Something wrong with the licensecode
            '        bOK = False
            '    End If

            '    ' Check that license is used within 5 days after release
            '    'If Int(DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, DateSerial(CInt(Left(sTmp, 2)), CInt(Mid(sTmp, 5, 2)), CInt(Right(sTmp, 2))))) > 5 Then
            '    If Int(DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, StringToDate(sTmp))) < 0 Then
            '        Err.Raise(10055, , LRS(10055)) ' Licensupgradefile is to old
            '        bOK = False
            '    End If
            '    ' set new licensedate, from pos 16-23 in code YYYYMMDD
            '    sTmp = LicDecrypt(Mid(sCode, 16, 8))
            '    ' Check the new licensedate
            '    If Left(sTmp, 2) <> "20" Or IsNumeric(sTmp) = False Or sTmp.Length <> 6 Then
            '        Err.Raise(10064, , LRS(10064)) ' Something wrong with the licensecode
            '        bOK = False
            '    End If

            '    sLicLimitDate = sTmp
            '    bRetValue = True

            'ElseIf UCase(Left(sCode, 2)) = "5-" Then
            '    ' License extended with 5 days - licensecode starts with 5-
            '    ' Check licensenumber, from pos 3 to 5 in code
            '    sTmp = LicDecrypt(Mid(sCode, 3, 5))

            '    If sTmp <> sLicNumber Then
            '        Err.Raise(10055, , LRS(10063)) ' 10063: Lisencenumber in the licensecodeis different from the licensecode in the licensefile.
            '        bOK = False
            '    End If

            '    ' Check licensecode release limitdate, from pos 8 to 15 in code YYYYMMDD
            '    sTmp = LicDecrypt(Mid(sCode, 8, 8))

            '    ' Check the release limit date
            '    If Left(sTmp, 2) <> "20" Or IsNumeric(sTmp) = False Or sTmp.Length <> 6 Then
            '        Err.Raise(10064, , LRS(10064)) ' Something wrong with the licensecode
            '        bOK = False
            '    End If

            '    ' Check that license is used within 5 days after release
            '    'If Int(DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, DateSerial(CInt(Left(sTmp, 2)), CInt(Mid(sTmp, 5, 2)), CInt(Right(sTmp, 2))))) > 5 Then
            '    If Int(DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, StringToDate(sTmp))) < 0 Then
            '        Err.Raise(10055, , LRS(10055)) ' Licensupgradefile is to old
            '        bOK = False
            '    End If
            '    ' set new licensedate, from pos 16-23 in code YYYYMMDD
            '    sTmp = LicDecrypt(Mid(sCode, 16, 8))
            '    ' Check the new licensedate
            '    If Left(sTmp, 2) <> "20" Or IsNumeric(sTmp) = False Or sTmp.Length <> 6 Then
            '        Err.Raise(10064, , LRS(10064)) ' Something wrong with the licensecode
            '        bOK = False
            '    End If

            '    sLicLimitDate = sTmp
            '    bRetValue = True

        ElseIf sCode.Length = 30 Then
            ' ordinary code to set values in licensefile;

            ' Limit no of records, pos 1 - 3 and 6 - 14, crypted
            nLicLimitRecords = Val(LicDecrypt(Left(sCode, 3) & Mid(sCode, 6, 9)))

            ' Limit No of formats, pos 4
            nLicLimitFormats = Int(CDbl(Mid(sCode, 4, 1)))

            'Available services from 27 an out
            sLicServicesAvailable = Mid(sCode, 27)

            ' Limitdate from 15 to 26
            sLicLimitDate = LicDecrypt(Mid(sCode, 15, 12))

            ' Support or not, pos 5. 0 = no support 1 = Support
            If Mid(sCode, 5, 1) = "0" Then
                sLicSupport = "None"
            Else
                sLicSupport = "Support"
            End If

            '------------------------------------------------
            ' Test if we have run into a NEW YEAR;
            '------------------------------------------------
            If Year(Now) > Int(CDbl(Left(sLicDateLastUsed, 4))) Then
                ' new Year, reset counter;
                nLicPreviousRecords = nLicUsedRecords
                nLicUsedRecords = 0
                ' Write to licensefile;
                LicWrite()
            End If
            bRetValue = True
        Else
            bRetValue = False
            Err.Raise(10064, , LRS(10064) & " (Wrong length)") ' Something wrong with the licensecode
            bOK = False
        End If

        If bRetValue Then
            ' Write to licensefile;
            LicWrite()
        End If

        LicCode = bRetValue
    End Function
    'Public Function LicUpgrade() As Boolean
    '    ' 09.10.2017 - denne funksjonen har aldri v�rt ibruk. Fjernes!
    '    ' -------------------------------------------------------------
    '    ' read a licensupgradefile from disk
    '    ' Possible info i upgradefile;
    '    ' - nLicLimitRecords
    '    ' - sLicLimitDate
    '    ' - sLicName
    '    ' - sLicNumber
    '    ' - sLicServicesAvailable
    '    ' - sLicSupport
    '    ' - sLicLimitFormats
    '    ' - sLicUpgradeFileProduced (cannot upgrade from this file more than 30 days after production)
    '    ' Filename is babel.upgrade, and must be saved to app.path
    '    ' File is crypted, like babel.lic
    '    Dim oFs As Scripting.FileSystemObject 'Object
    '    Dim oFile As Scripting.TextStream
    '    Dim sLine As String
    '    Dim sType As String
    '    Dim sArgument As String
    '    Dim sFilename As String
    '    Dim bOK As Boolean
    '    Dim sUpgradeLimitDate As String
    '    Dim nUpgradeLimitRecords As Double
    '    Dim sUpgradeServicesAvailable As String
    '    Dim sUpgradeNumber As String
    '    Dim sUpgradeName As String
    '    'Dim sUpgradeDateResets As String
    '    Dim sUpgradeDateLastUsed As String
    '    Dim nUpgradeLimitFormats As Short
    '    Dim sUpgradeSupport As String

    '    sUpgradeServicesAvailable = ""
    '    sUpgradeLimitDate = ""
    '    sUpgradeNumber = ""
    '    sLicNumber = sUpgradeNumber
    '    sUpgradeName = ""
    '    sUpgradeSupport = ""

    '    'No need to set bAnythingChanged = True because its written anyway

    '    sFilename = My.Application.Info.DirectoryPath & "\babel.upgrade"

    '    ' Check if there is an upgradefile present;
    '    oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
    '    If Not oFs.FileExists(sFilename) Then
    '        bOK = False
    '    Else
    '        bOK = True
    '    End If

    '    If bOK Then
    '        ' Open upgradefile:
    '        oFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForReading, False)

    '        Do While Not oFile.AtEndOfStream = True
    '            sLine = oFile.ReadLine()
    '            sType = LicDecrypt(Left(sLine, InStr(sLine, "=") - 2)) ' License property
    '            sArgument = LicDecrypt(Trim(Mid(sLine, InStr(sLine, "=") + 1))) ' Value in property

    '            Select Case sType
    '                Case "LIMITRECORDS" 'No of available paymentrecords in license
    '                    nUpgradeLimitRecords = CDbl(sArgument)
    '                Case "SERVICESAVAILABLE" 'Which of BabelBanks service is user licensed to use ?
    '                    sUpgradeServicesAvailable = sArgument
    '                Case "LIMITDATE" 'Timeoutdate if limited license
    '                    sUpgradeLimitDate = Trim(sArgument)
    '                Case "NUMBER" 'License number
    '                    sUpgradeNumber = sArgument
    '                Case "NAME" 'License registered to
    '                    sUpgradeName = sArgument
    '                Case "DATELASTUSED" 'Timeoutdate of upgrade
    '                    sUpgradeDateLastUsed = Trim(sArgument)
    '                Case "LIMITFORMATS" 'No of available formats in license
    '                    nUpgradeLimitFormats = CShort(sArgument)
    '                Case "SUPPORT" 'Support or not
    '                    sUpgradeSupport = sArgument

    '            End Select

    '        Loop

    '        ' 01.09.2016 - first close !
    '        oFile.Close()
    '        oFile = Nothing

    '        ' Delete upgradefile;
    '        'oFs.DeleteFile(sFilename, True)
    '        My.Computer.FileSystem.DeleteFile(sFilename)

    '        ' Check first if upgradefile is to old;
    '        If Int(DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, DateSerial(CInt(Left(sUpgradeDateLastUsed, 4)), CInt(Mid(sUpgradeDateLastUsed, 5, 2)), CInt(Right(sUpgradeDateLastUsed, 2))))) > 60 Then
    '            ' more than 60 days since upgradefile was produced. Cannt be used!
    '            'MsgBox "Your upgradefile has expired!" + vbCrLf + "Please contact your dealer", vbCritical, "License"
    '            Err.Raise(10055, , LRS(10055)) ' Licensupgradefile is to old
    '            bOK = False
    '        End If

    '        If bOK Then
    '            ' Set upgrade-values like lic-values
    '            If nUpgradeLimitRecords > 0 Then
    '                nLicLimitRecords = nUpgradeLimitRecords
    '            End If
    '            If Len(sUpgradeServicesAvailable) > 0 Then
    '                sLicServicesAvailable = sUpgradeServicesAvailable
    '            End If
    '            If Len(sUpgradeLimitDate) > 0 Then
    '                sLicLimitDate = sUpgradeLimitDate
    '            End If
    '            If Len(sUpgradeNumber) > 0 Then
    '                sLicNumber = sUpgradeNumber
    '            End If
    '            If Len(sUpgradeName) > 0 Then
    '                sLicName = sUpgradeName
    '            End If
    '            If nUpgradeLimitFormats > 0 Then
    '                nLicLimitFormats = nUpgradeLimitFormats
    '            End If
    '            If Len(sUpgradeSupport) > 0 Then
    '                sLicSupport = sUpgradeSupport
    '            End If

    '            nLicDateResets = 0
    '            sLicDateLastUsed = Str(Year(Now)) & Right("0" & Trim(Str(Month(Now))), 2) & Right("0" & Trim(Str(VB.Day(Now))), 2)

    '        End If
    '    End If


    '    If bOK Then
    '        ' Important; write values, otherwise the old ones is read again
    '        LicWrite()
    '    End If

    '    If Not oFile Is Nothing Then
    '        oFile.Close()
    '        oFile = Nothing
    '    End If

    '    If Not oFs Is Nothing Then
    '        oFs = Nothing
    '    End If

    'End Function

    Public Function LicOkToContinue(Optional ByVal bSilent As Boolean = False, Optional ByVal bLog As Boolean = False, Optional ByVal oBabelLog As vbLog.vbLogging = Nothing) As Boolean
        ' 05.10.2018 added bSilent
        ' Flag if we can continue or not
        Dim bContinue As Boolean
        bContinue = True

        If Not bOK Then
            ' f.ex resat date to many times
            bContinue = False
        End If
        ' Test datelimit;
        If LicDaysRemaining(bSilent, bLog, oBabelLog) < 0 Then
            'MsgBox "Your licensedate has expired!" + vbCrLf + "Please contact your dealer", vbCritical, "License"
            Err.Raise(10051, , LRS(10051)) ' Licensefile has expired on date
            bContinue = False
        End If
        ' Test transactionlimit
        If LicFreeRecords(bSilent, bLog, oBabelLog) < 0 Then
            'MsgBox "Your license has run out of available records !" + vbCrLf + "Please contact your dealer", vbCritical, "License"
            ' If silent, then log
            If bLog Then
                oBabelLog.Heading = "BabelBank ERROR: "
                oBabelLog.AddLogEvent(LRS(10052), System.Diagnostics.TraceEventType.Warning)
            End If
            Err.Raise(10052, , LRS(10052)) ' License has run out of available records
            bContinue = False
        End If

        LicOkToContinue = bContinue
    End Function
    Public Sub LicOneTimeUnlock()
        'New 02.08.02
        'Key to unlock license on expirydate for one time necessity use:
        'Algorithm: vbs adress (99) + abs(dd - mm*2) + yy*3
        'Example: august 2 2002: 99 + abs(2 - 8*2) + 2*3 = 119
        ' This key is keyed in in BabelBank.exe

        'Change lisens-expirydate to todays date;
        LicRead()
        ' Create a limitdate like todays date
        sLicLimitDate = Trim(Str(Year(Now)) & Right("0" & Trim(Str(Month(Now))), 2) & Right("0" & Trim(Str(VB.Day(Now))), 2))
        LicWrite()

    End Sub
    Public Function LicDaysRemaining(Optional ByVal bSilent As Boolean = False, Optional ByVal bLog As Boolean = False, Optional ByVal oBabelLog As vbLog.vbLogging = Nothing) As Integer '  08.03.2011 was Double
        ' 09.06.2020 - added use of Silent and Log. If run silent, then program bombs without logging warning
        ' How many days are left if license is limited on date ?
        ' XNET 15.04.2013 added Visma, no check
        If sLicServicesAvailable = "V" Then
            LicDaysRemaining = 9999
        Else
            LicDaysRemaining = Int(DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, DateSerial(CInt(Left(sLicLimitDate, 4)), CInt(Mid(sLicLimitDate, 5, 2)), CInt(Right(sLicLimitDate, 2)))))
            ' Raise a warning when 15 or less days left of license
            If LicDaysRemaining < 0 Then
                'MsgBox "Your license expired " + Trim(Str(DateDiff("d", DateSerial(Left$(sLicLimitDate, 4), Mid$(sLicLimitDate, 5, 2), Right$(sLicLimitDate, 2)), Now()))) + " days ago!", vbInformation, "License"
                ' License expired %1 days ago
                ' 09.06.2020 - added Logging
                If bLog Then
                    oBabelLog.Heading = "BabelBank Warning: "
                    oBabelLog.AddLogEvent(LRS(10053, Trim(Str(DateDiff(Microsoft.VisualBasic.DateInterval.Day, DateSerial(CInt(Left(sLicLimitDate, 4)), CInt(Mid(sLicLimitDate, 5, 2)), CInt(Right(sLicLimitDate, 2))), Now)))), System.Diagnostics.TraceEventType.Warning)
                End If

                Err.Raise(10053, , LRS(10053, Trim(Str(DateDiff(Microsoft.VisualBasic.DateInterval.Day, DateSerial(CInt(Left(sLicLimitDate, 4)), CInt(Mid(sLicLimitDate, 5, 2)), CInt(Right(sLicLimitDate, 2))), Now)))))
            ElseIf LicDaysRemaining < 20 Then
                If Not bSilent Then
                    MsgBox("Only " + Trim(Str(DateDiff("d", Now(), DateSerial(Left$(sLicLimitDate, 4), Mid$(sLicLimitDate, 5, 2), Right$(sLicLimitDate, 2))))) + " days left of licenseperiod!", vbInformation, "License")
                End If
                ' Only %1 days left of licenseperiod
                ' 09.06.2020 - added Logging
                If bLog Then
                    oBabelLog.Heading = "BabelBank Warning: "
                    oBabelLog.AddLogEvent("Only " + Trim(Str(DateDiff("d", Now(), DateSerial(Left$(sLicLimitDate, 4), Mid$(sLicLimitDate, 5, 2), Right$(sLicLimitDate, 2))))) + " days left of licenseperiod!", System.Diagnostics.TraceEventType.Warning)
                End If
            End If
        End If

    End Function
    Public Function LicFreeRecords(Optional ByVal bSilent As Boolean = False, Optional ByVal bLog As Boolean = False, Optional ByVal oBabelLog As vbLog.vbLogging = Nothing) As Double
        ' 05.10.2018 added bSilent
        ' Return number of free records
        ' XNET 15.04.2013 added Visma, no check
        If sLicServicesAvailable = "V" Then
            LicFreeRecords = 999999999
        Else
            If nLicLimitRecords = 0 Then ' 0 when free license
                LicFreeRecords = 999999999
            Else
                LicFreeRecords = (nLicLimitRecords - nLicUsedRecords)
                ' not in use -->LicWarning  ' Raise a warning when few records left
                ' Raise a warning when less than 10 percent of records left
                If (nLicLimitRecords - nLicUsedRecords) / nLicLimitRecords < 0.1 And (nLicLimitRecords - nLicUsedRecords) > -1 Then
                    'MsgBox "Only " + Trim(Str((nLicLimitRecords - nLicUsedRecords))) + " records left in license!", vbInformation, "License"
                    ' Only %1 payements left in license
                    ' Show message only, no err.raise, because this will halt program
                    'Err.Raise 10060, , LRS(10060, Trim(Str((nLicLimitRecords - nLicUsedRecords))))
                    ' 05.10.2018 added If Not bSilent
                    If Not bSilent Then
                        MsgBox(LRS(10060, Trim(Str(nLicLimitRecords - nLicUsedRecords))), MsgBoxStyle.Critical, "BabelBank")
                    Else
                        ' If silent, then log
                        If bLog Then
                            oBabelLog.Heading = "BabelBank warning: "
                            oBabelLog.AddLogEvent(LRS(10060, Trim(Str(nLicLimitRecords - nLicUsedRecords))), System.Diagnostics.TraceEventType.Warning)
                        End If
                    End If
                End If

            End If
        End If
    End Function
    Public Sub LicUpdate(ByRef nNewRecords As Double)
        ' Update number of used records
        nLicPreviousRecords = nLicUsedRecords
        If nNewRecords > 0 Then
            nLicUsedRecords = nLicUsedRecords + nNewRecords
            bAnythingChanged = True
        End If
    End Sub

    Friend Sub LicWarning()
        ' Raise a warning when less than 10 percent of records left
        If (nLicLimitRecords - nLicUsedRecords) / nLicLimitRecords < 0.1 Then
            'MsgBox "Only " + Trim(Str((nLicLimitRecords - nLicUsedRecords))) + " records left in license!", vbInformation, "License"
            ' Only %1 payements left in license
            ' Show message only, no err.raise, because this will halt program
            'Err.Raise 10060, , LRS(10060, Trim(Str((nLicLimitRecords - nLicUsedRecords))))
            MsgBox(LRS(10060, Trim(Str(nLicLimitRecords - nLicUsedRecords))), MsgBoxStyle.Critical, "BabelBank")

        End If
        ' Raise a warning when 25 or less days left of license
        If DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, DateSerial(Val(Left(sLicLimitDate, 4)), Val(Mid(sLicLimitDate, 5, 2)), Val(Right(sLicLimitDate, 2)))) < 0 Then
            'MsgBox "Your license expired " + Trim(Str(DateDiff("d", DateSerial(Left$(sLicLimitDate, 4), Mid$(sLicLimitDate, 5, 2), Right$(sLicLimitDate, 2)), Now()))) + " days ago!", vbInformation, "License"
            Err.Raise(10053, , LRS(10053, Trim(Str(DateDiff(Microsoft.VisualBasic.DateInterval.Day, DateSerial(CInt(Left(sLicLimitDate, 4)), CInt(Mid(sLicLimitDate, 5, 2)), CInt(Right(sLicLimitDate, 2))), Now)))))
        ElseIf DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, DateSerial(CInt(Left(sLicLimitDate, 4)), CInt(Mid(sLicLimitDate, 5, 2)), CInt(Right(sLicLimitDate, 2)))) < 40 Then
            'MsgBox "Only " + Trim(Str(DateDiff("d", Now(), DateSerial(Left$(sLicLimitDate, 4), Mid$(sLicLimitDate, 5, 2), Right$(sLicLimitDate, 2))))) + " days left of licenseperiod!", vbInformation, "License"
            MsgBox(LRS(10054, Trim(Str(DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, DateSerial(CInt(Left(sLicLimitDate, 4)), CInt(Mid(sLicLimitDate, 5, 2)), CInt(Right(sLicLimitDate, 2))))))), MsgBoxStyle.Exclamation)

        End If
    End Sub
    Public Function LicRead() As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String
        Dim sType As String
        Dim sArgument As String
        Dim nLinesInterpreted As Short ' No of lines read and Interpreted correctly
        Dim sMySQL As String
        Dim sMyFile As String
        'Dim vDate As Date
        Dim oLiccFile As Scripting.File
        Dim sErrorText As String = ""

        ' New 26.09.2018
        Dim oStreamReader As System.IO.StreamReader


        Try
            ' Check for licenseupgrade:
            ' 09.10.2017 - Not in use.
            'LicUpgrade()

            oFs = New Scripting.FileSystemObject
            bOK = True
            ' ----> bruk system.io.file.exists ???

            If Not oFs.FileExists(sLicFileName) Then
                ' If licensefile not present,copy from db_ins
                ' Licensefile not present in given directory, check if present as
                ' sLicFileName & "_vb"
                ' This as a backup-solution if licensfile is deleted
                ' Wait, to see if other changes has done the job
                'If oFs.FileExists(App.path + "\db_ins\babel.lic") Then
                '    ' copy _vb-file
                '    oFs.CopyFile sLicFileName & "_vb", sLicFileName
                'End If

                'If Not oFs.FileExists(sLicFileName) Then
                'MsgBox "Can't find licensefile", vbCritical, "License"

                '01.09.2016 - try for a while to find it!
                If Not oFs.FileExists(sLicFileName) Then
                    Pause(1)
                End If
                If Not oFs.FileExists(sLicFileName) Then
                    Pause(1)
                End If
                If Not oFs.FileExists(sLicFileName) Then
                    Err.Raise(10056, , LRS(10056) & " " & sLicFileName)
                    bOK = False
                Else
                    bOK = True
                End If
                'End If
            End If

            If bOK Then 'Else
                oLiccFile = oFs.GetFile(sLicFileName)

                If oLiccFile.Attributes And Scripting.__MIDL___MIDL_itf_scrrun_0000_0000_0001.ReadOnly Then
                    '10061: The license file, %1, is writeprotected. BabelBank terminates
                    oLiccFile = Nothing
                    Err.Raise(10061, "LicRead", LRS(10061, sLicFileName) & vbCrLf & vbCrLf & LRS(10017))
                    bOK = False
                Else
                    oLiccFile = Nothing
                    bOK = True
                End If
            End If

            ' XOKNET 15.04.2013 added Visma, no check
            If bOK Then

                ' 26.09.2018 - TEST using System.IO.

                'If Today.ToString = "08.10.2018 00:00:00" Then
                If Not RunTime() Then  'NOTRUNTIME
                    '=============================================
                    ' ONLY FOR INTERNAL USE FIRST!!!
                    '=============================================
                    ' Open licensefile:
                    oStreamReader = New System.IO.StreamReader(sLicFileName)
                    nLinesInterpreted = 0

                    ' Due to the Visma project, we have to first find out if this is a Visma-license or not,
                    ' before we proceed with further action.
                    ' Because of that, we have to open the licensefile twice !
                    Do While Not oStreamReader.EndOfStream
                        sLine = oStreamReader.ReadLine()
                        sType = LicDecrypt(Left$(sLine, InStr(sLine, "=") - 2))          ' License property
                        sArgument = Trim(LicDecrypt(Mid$(sLine, InStr(sLine, "=") + 1))) ' Value in property
                        If sType = "SERVICESAVAILABLE" Then 'Which of BabelBanks service is user licensed to use ?
                            sLicServicesAvailable = sArgument
                        End If
                    Loop
                    oStreamReader.Close()
                    oStreamReader = Nothing
                    '=============================================

                Else
                    ' Open licensefile:
                    oFile = oFs.OpenTextFile(sLicFileName, Scripting.IOMode.ForReading, False)
                    nLinesInterpreted = 0

                    ' XNET 05.07.2013 -
                    ' Due to the Visma project, we have to first find out if this is a Visma-license or not,
                    ' before we proceed with further action.
                    ' Because of that, we have to open the licensefile twice !
                    Do While Not oFile.AtEndOfStream = True
                        sLine = oFile.ReadLine()
                        sType = LicDecrypt(Left$(sLine, InStr(sLine, "=") - 2))          ' License property
                        sArgument = Trim(LicDecrypt(Mid$(sLine, InStr(sLine, "=") + 1))) ' Value in property
                        If sType = "SERVICESAVAILABLE" Then 'Which of BabelBanks service is user licensed to use ?
                            sLicServicesAvailable = sArgument
                        End If
                    Loop
                    oFile.Close()
                    oFile = Nothing
                End If

                If Not sLicServicesAvailable = "V" Then

                    ' Open for second time, to find rest of the values in licensefile;
                    ' ----------------------------------------------------------------
                    ' If Today.ToString =
                    If Not RunTime() Then  'NOTRUNTIME
                        '=============================================
                        ' ONLY FOR INTERNAL USE FIRST!!!
                        '=============================================

                        oStreamReader = New System.IO.StreamReader(sLicFileName)
                        nLinesInterpreted = 0

                        Do While Not oStreamReader.EndOfStream = True
                            sLine = oStreamReader.ReadLine()
                            If Len(sLine) > 0 Then
                                sType = LicDecrypt(Left(sLine, InStr(sLine, "=") - 2)) ' License property
                                sArgument = Trim(LicDecrypt(Mid(sLine, InStr(sLine, "=") + 1))) ' Value in property

                                Select Case sType
                                    Case "USEDRECORDS" 'No of used paymentrecords
                                        nLicUsedRecords = CDbl(sArgument)
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If
                                        sMyFile = BB_DatabasePath(False, False)   ' XNET 08.07.2013 added Two parameters

                                        If Not EmptyString(sMyFile) Then '30.06.2014 - No database used
                                            'If Dir(sMyFile) <> "" Then
                                            If System.IO.File.Exists(sMyFile) Then
                                                ' From version 1.00.25 Number of records used is stored in database
                                                ' (in Company, Code), scrambled as in babel.lic
                                                ' From version 1.00.33 Number of Records is stored in database,
                                                ' in table Profile, field Code
                                                oMyDal = New vbBabel.DAL
                                                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                                                sErrorText = "Connectionstring: " & oMyDal.Connectionstring
                                                If Not oMyDal.ConnectToDB() Then
                                                    Throw New System.Exception(oMyDal.ErrorMessage)
                                                End If

                                                sMySQL = "Select * from Profile"
                                                oMyDal.SQL = sMySQL
                                                sErrorText = "F�r Reader_Execute: " & sMySQL
                                                If oMyDal.Reader_Execute() Then
                                                    Do While oMyDal.Reader_ReadRecord
                                                        If oMyDal.Reader_GetString("Code") <> "" Then
                                                            If oMyDal.Reader_GetString("Code") = "zzxinit" Then
                                                                ' Initial value, keep the one from Babel.Lic
                                                            Else
                                                                sArgument = LicDecrypt(oMyDal.Reader_GetString("Code"))
                                                                If Not IsNumeric(sArgument) Then
                                                                    ' Changed 25.02.03 by JanP
                                                                    ' Caused problems  when updating from previous versions!
                                                                    ' take number from licensefile
                                                                    sArgument = CStr(nLicUsedRecords)
                                                                    'Err.Raise 10059, , LRS(10059) 'Something wrong with licensefile!
                                                                End If
                                                                nLicUsedRecords = CDbl(sArgument)
                                                            End If
                                                        Else
                                                            ' user have deleted Code, error
                                                            Err.Raise(10059, , LRS(10059)) 'Something wrong with licensefile!
                                                        End If
                                                    Loop
                                                Else
                                                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                                                End If
                                            End If
                                        Else
                                            nLicUsedRecords = 0
                                        End If
                                        sErrorText = "Etter Reader_Execute: " & sMySQL

                                    Case "PREVIOUSRECORDS" 'No of used paymentrecords
                                        nLicPreviousRecords = CDbl(sArgument)
                                    Case "LIMITRECORDS" 'No of available paymentrecords in license
                                        nLicLimitRecords = CDbl(sArgument)
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If
                                    Case "LIMITFORMATS" 'No of available formats in license
                                        nLicLimitFormats = sArgument
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If

                                    Case "NOOFUSERS"
                                        nLicNoOfUsers = CShort(sArgument)
                                    Case "NOOFCOMPUTERS"
                                        nLicNoOfComputers = CShort(sArgument)
                                    Case "SERVICESAVAILABLE" 'Which of BabelBanks service is user licensed to use ?
                                        sLicServicesAvailable = sArgument
                                    Case "DATELASTUSED" 'Date last time records were imported
                                        sLicDateLastUsed = sArgument
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If
                                    Case "LIMITDATE" 'Timeoutdate if limited license
                                        sLicLimitDate = Trim(sArgument)
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If
                                    Case "NUMBER" 'License number
                                        sLicNumber = Trim(sArgument) '???
                                    Case "NAME" 'License registered to
                                        sLicName = sArgument
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If
                                    Case "SUPPORT" 'Supportype (or no support)
                                        sLicSupport = sArgument
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If

                                    Case "DATERESETS" ' No of times date is reset (for testlicenses)
                                        nLicDateResets = CShort(sArgument)

                                End Select
                            End If
                        Loop

                        oStreamReader.Close()
                        oStreamReader = Nothing

                    Else
                        oFile = oFs.OpenTextFile(sLicFileName, Scripting.IOMode.ForReading, False)
                        nLinesInterpreted = 0

                        Do While Not oFile.AtEndOfStream = True

                            sLine = oFile.ReadLine()
                            If Len(sLine) > 0 Then
                                sType = LicDecrypt(Left(sLine, InStr(sLine, "=") - 2)) ' License property
                                sArgument = Trim(LicDecrypt(Mid(sLine, InStr(sLine, "=") + 1))) ' Value in property

                                Select Case sType
                                    Case "USEDRECORDS" 'No of used paymentrecords
                                        nLicUsedRecords = CDbl(sArgument)
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If
                                        sMyFile = BB_DatabasePath(False, False)   ' XNET 08.07.2013 added Two parameters

                                        If Not EmptyString(sMyFile) Then '30.06.2014 - No database used
                                            If Dir(sMyFile) <> "" Then
                                                ' From version 1.00.25 Number of records used is stored in database
                                                ' (in Company, Code), scrambled as in babel.lic
                                                ' From version 1.00.33 Number of Records is stored in database,
                                                ' in table Profile, field Code
                                                oMyDal = New vbBabel.DAL
                                                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                                                sErrorText = "Connectionstring: " & oMyDal.Connectionstring
                                                If Not oMyDal.ConnectToDB() Then
                                                    Throw New System.Exception(oMyDal.ErrorMessage)
                                                End If

                                                sMySQL = "Select * from Profile"
                                                oMyDal.SQL = sMySQL
                                                sErrorText = "F�r Reader_Execute: " & sMySQL
                                                If oMyDal.Reader_Execute() Then
                                                    Do While oMyDal.Reader_ReadRecord
                                                        If oMyDal.Reader_GetString("Code") <> "" Then
                                                            If oMyDal.Reader_GetString("Code") = "zzxinit" Then
                                                                ' Initial value, keep the one from Babel.Lic
                                                            Else
                                                                sArgument = LicDecrypt(oMyDal.Reader_GetString("Code"))
                                                                If Not IsNumeric(sArgument) Then
                                                                    ' Changed 25.02.03 by JanP
                                                                    ' Caused problems  when updating from previous versions!
                                                                    ' take number from licensefile
                                                                    sArgument = CStr(nLicUsedRecords)
                                                                    'Err.Raise 10059, , LRS(10059) 'Something wrong with licensefile!
                                                                End If
                                                                nLicUsedRecords = CDbl(sArgument)
                                                            End If
                                                        Else
                                                            ' user have deleted Code, error
                                                            Err.Raise(10059, , LRS(10059)) 'Something wrong with licensefile!
                                                        End If
                                                    Loop
                                                Else
                                                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                                                End If
                                            End If
                                        Else
                                            nLicUsedRecords = 0
                                        End If
                                        sErrorText = "Etter Reader_Execute: " & sMySQL

                                    Case "PREVIOUSRECORDS" 'No of used paymentrecords
                                        nLicPreviousRecords = CDbl(sArgument)
                                    Case "LIMITRECORDS" 'No of available paymentrecords in license
                                        nLicLimitRecords = CDbl(sArgument)
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If
                                    Case "LIMITFORMATS" 'No of available formats in license
                                        nLicLimitFormats = sArgument
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If

                                    Case "NOOFUSERS"
                                        nLicNoOfUsers = CShort(sArgument)
                                    Case "NOOFCOMPUTERS"
                                        nLicNoOfComputers = CShort(sArgument)
                                    Case "SERVICESAVAILABLE" 'Which of BabelBanks service is user licensed to use ?
                                        sLicServicesAvailable = sArgument
                                    Case "DATELASTUSED" 'Date last time records were imported
                                        sLicDateLastUsed = sArgument
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If
                                    Case "LIMITDATE" 'Timeoutdate if limited license
                                        sLicLimitDate = Trim(sArgument)
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If
                                    Case "NUMBER" 'License number
                                        sLicNumber = Trim(sArgument) '???
                                    Case "NAME" 'License registered to
                                        sLicName = sArgument
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If
                                    Case "SUPPORT" 'Supportype (or no support)
                                        sLicSupport = sArgument
                                        If Len(Trim(Mid(sLine, InStr(sLine, "=") + 1))) > 0 Then
                                            nLinesInterpreted = nLinesInterpreted + 1
                                        End If

                                    Case "DATERESETS" ' No of times date is reset (for testlicenses)
                                        nLicDateResets = CShort(sArgument)

                                End Select
                            End If
                        Loop

                        oFile.Close()
                        oFile = Nothing

                    End If ' IF Not Runtime Then
                    '----------------------------------------------
                    ' Test for INITIAL, temporarary, startuplicense
                    '----------------------------------------------
                    'If sLicNumber = "0" And sLicName = "MIDLERTIDIG LISENS" And nLicLimitRecords = 99999 And nLicUsedRecords = 1 Then

                    If sLicNumber = "0" And sLicName = "MIDLERTIDIG LISENS" And nLicLimitRecords = 99999 Then

                        ' Temp.license, OK
                        ' Do a test, to check if user has installed another temporary license, upon an old one,
                        ' to cheat !!!!
                        ' When temp-license is used the first time, we are writing the file msvbbb.dll to (REMOVED that part)
                        ' \windows\system or \winnt\system32, that we can test for;
                        ' 26.09.2018 - Neste linje er kuttet ut - vi bruker jo ikke oFS etter dette !!!!!
                        ' ---------> oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
                        'If oFs.FileExists(GetSystemDir() & "\msvbbb.dll") Then
                        ' 10.11.2015 REMOVED the msvbbb.dll-check, it creates problems because we may not have permissions
                        'If oFs.FileExists(Environment.GetEnvironmentVariable("windir", EnvironmentVariableTarget.Machine) & "\msvbbb.dll") Then
                        '    ' Check date for msvbbb.dll - date is set when this file is copied first time ..
                        '    ' New in 1.01.33 - check remaining days by useing msvbbb.dll's datestamp
                        '    ' Check datestamp of file, older than 40 days?
                        '    'vDate = FileDateTime(GetSystemDir() & "\msvbbb.dll")
                        '    vDate = FileDateTime(Environment.GetEnvironmentVariable("windir", EnvironmentVariableTarget.Machine) & "\msvbbb.dll")
                        '    If System.DateTime.FromOADate(vDate.ToOADate + 40) < Now Then
                        '        ' Oh no you sodd, you cant trick us !!!
                        '        bOK = False
                        '        sLicLimitDate = Trim(Str(Year(System.DateTime.FromOADate(Now.ToOADate - 1))) & Right("0" & Trim(Str(Month(System.DateTime.FromOADate(Now.ToOADate - 1)))), 2) & Right("0" & Trim(Str(VB.Day(System.DateTime.FromOADate(Now.ToOADate - 1)))), 2))
                        '        nLicLimitRecords = 0
                        '        LicWrite()
                        '        ' Force a licenseerror;
                        '        Err.Raise(10051, , LRS(10051)) ' Licensefile has expired on date
                        '    Else

                        '        sLicLimitDate = Trim(Str(Year(System.DateTime.FromOADate(vDate.ToOADate + 40))) & Right("0" & Trim(Str(Month(System.DateTime.FromOADate(vDate.ToOADate + 40)))), 2) & Right("0" & Trim(Str(VB.Day(System.DateTime.FromOADate(vDate.ToOADate + 40)))), 2))
                        '    End If
                        'Else
                        bOK = True
                        '
                        ' Create a limitdate, for testuse, 40 days from now
                        ' 10.11.2015 Changed to 50
                        sLicLimitDate = Trim(Str(Year(System.DateTime.FromOADate(Now.ToOADate + 50))) & Right("0" & Trim(Str(Month(System.DateTime.FromOADate(Now.ToOADate + 50)))), 2) & Right("0" & Trim(Str(VB.Day(System.DateTime.FromOADate(Now.ToOADate + 50)))), 2))
                        nLicUsedRecords = 0
                        nLinesInterpreted = nLinesInterpreted + 1
                        ' To mark that we have been using a temp-license, put a file into win/system
                        ' Simply, copy and rename setup414.dll to msvbbb.dll
                        'oFs.CopyFile App.path + "\setup414.dll", GetSystemDir() + "\msvbbb.dll", True
                        ' Create a file instead! Will give todays date on file
                        'oFs.CreateTextFile(GetSystemDir() & "\msvbbb.dll")

                        '10.22.2015 - Next line can cause errors whne copying to Windows, or wherever. 
                        ' KUTTER UT hele dritten med msvbbb.dll - det skaper mer problem enn nytte
                        'oFs.CreateTextFile(Environment.GetEnvironmentVariable("windir", EnvironmentVariableTarget.Machine) & "\msvbbb.dll")
                        'End If
                    End If


                    ' Controls:
                    '==========
                    'Test if crucial lines have been deleted:
                    If nLinesInterpreted < 4 Then
                        ' Lines are deleted from babel.lic !
                        Err.Raise(10059, , LRS(10059)) 'Something wrong with licensefile!
                    End If

                    ' Test if computerdate has been reset:
                    If Year(Now) < Int(CDbl(Left(sLicDateLastUsed, 4))) Then
                        ' If date have been reset over a new year, then issue a warning;
                        Err.Raise(10058, , LRS(10058))
                        bOK = False
                    End If
                    If Int(DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, DateSerial(CInt(Left(sLicDateLastUsed, 4)), CInt(Mid(sLicDateLastUsed, 5, 2)), CInt(Right(sLicDateLastUsed, 2))))) > 0 Then
                        ' date have been reset!
                        ' Update "resetcounter", how may times have date been reset back
                        nLicDateResets = nLicDateResets + 1
                        ' Issue a warning if user has a datelimit, and have reset more than 2 times
                        If nLicDateResets > 2 And Int(DateDiff(Microsoft.VisualBasic.DateInterval.Day, Now, DateSerial(CInt(Left(sLicLimitDate, 4)), CInt(Mid(sLicLimitDate, 5, 2)), CInt(Right(sLicLimitDate, 2))))) < 100 Then
                            'Date have bben changed to many times
                            Err.Raise(10057, , LRS(10057))
                            bOK = False
                        Else
                            bOK = True
                        End If
                    Else
                        'Lic file parsed Ok !
                        bOK = True
                    End If

                    '------------------------------------------------
                    ' Test if we have run into a NEW YEAR;
                    '------------------------------------------------
                    If Year(Now) > Int(CDbl(Left(sLicDateLastUsed, 4))) Then
                        ' new Year, reset counter;
                        nLicPreviousRecords = nLicUsedRecords
                        nLicUsedRecords = 0
                        ' To be sure, write to licensefile;
                        LicWrite()

                    End If
                End If   'If Not sLicServicesAvailable = "V" Then
            End If

            If Today.ToString <> "08.10.2018 00:00:00" Then  ' added 27.09.2018 - remove when System.io has been tested !!!!!
                If Not oFile Is Nothing Then
                    oFile.Close()
                    oFile = Nothing
                End If

                If Not oFs Is Nothing Then
                    oFs = Nothing
                End If
            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            'NBNBNB!!! Essential that the new exceptions message starts with FunctionX: LicRead
            If Not EmptyString(ex.StackTrace) Then
                Throw New Exception("FunctionX: LicRead" & vbCrLf & sErrorText & ex.Message & vbCrLf & "Stacktrace: " & vbCrLf & ex.StackTrace)
            Else
                Throw New Exception("FunctionX: LicRead" & vbCrLf & sErrorText & ex.Message)
            End If

        Finally

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        LicRead = bOK

    End Function
    Public Function LicWrite() As Boolean
        'Dim oFs As Object  'Scripting Changed 25.11.04
        Dim oFs As Scripting.FileSystemObject = Nothing
        Dim oFile As Scripting.TextStream = Nothing
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim bReturnValue As Boolean = True
        Dim sUsed As String
        Dim sLicFileNameCopy As String = ""

        'Try

        ' ************* NEW TEST With StreamWrite *******************
        'If Today.ToString = "08.10.2018 00:00:00" Then
        If Not RunTime() Then  'NOTRUNTIME
            ' Can't touch Database when Visma !
            If Not sLicServicesAvailable = "V" Then
                If System.IO.File.Exists(sLicFileName) Then
                    '  Make a copy of the current licensefile;
                    sLicFileNameCopy = sLicFileName & "_Copy"
                End If

                ' if we have a copy, delete it
                If System.IO.File.Exists(sLicFileNameCopy) Then
                    System.IO.File.Delete(sLicFileNameCopy)
                End If

                ' Open a COPY to write new licensefiledata:
                Dim oStreamWriter As New System.IO.StreamWriter(sLicFileNameCopy, True)

                oStreamWriter.WriteLine(LicCrypt("USEDRECORDS") & " = " & LicCrypt(Str(nLicUsedRecords), True))
                oStreamWriter.WriteLine((LicCrypt("PREVIOUSRECORDS") & " = " & LicCrypt(Str(nLicPreviousRecords), True)))
                oStreamWriter.WriteLine((LicCrypt("LIMITRECORDS") & " = " & LicCrypt(Str(nLicLimitRecords), True)))
                'UPGRADE_WARNING: Couldn't resolve default property of object nLicLimitFormats. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oStreamWriter.WriteLine((LicCrypt("LIMITFORMATS") & " = " & LicCrypt(Str(nLicLimitFormats), True)))
                oStreamWriter.WriteLine((LicCrypt("NOOFUSERS") & " = " & LicCrypt(Str(nLicNoOfUsers), True)))
                oStreamWriter.WriteLine((LicCrypt("NOOFCOMPUTERS") & " = " & LicCrypt(Str(nLicNoOfComputers), True)))
                oStreamWriter.WriteLine((LicCrypt("SERVICESAVAILABLE") & " = " & LicCrypt(sLicServicesAvailable)))
                ' Set datelastused to today
                sLicDateLastUsed = Str(Year(Now)) & Right("0" & Trim(Str(Month(Now))), 2) & Right("0" & Trim(Str(VB.Day(Now))), 2)
                oStreamWriter.WriteLine((LicCrypt("DATELASTUSED") & " = " & LicCrypt(sLicDateLastUsed, True)))
                oStreamWriter.WriteLine((LicCrypt("LIMITDATE") & " = " & LicCrypt(sLicLimitDate, True)))
                oStreamWriter.WriteLine((LicCrypt("NUMBER") & " = " & LicCrypt(sLicNumber, True)))
                oStreamWriter.WriteLine((LicCrypt("NAME") & " = " & LicCrypt(sLicName)))
                oStreamWriter.WriteLine((LicCrypt("SUPPORT") & " = " & LicCrypt(sLicSupport)))
                oStreamWriter.WriteLine((LicCrypt("DATERESETS") & " = " & LicCrypt(Str(nLicDateResets), True)))


                oStreamWriter.Close()
                oStreamWriter = Nothing

                ' at this stage we have written to a copy of the licensefile
                ' then, delete the original one if we know we have a copy;
                If System.IO.File.Exists(sLicFileNameCopy) Then
                    If System.IO.File.Exists(sLicFileName) Then
                        System.IO.File.Delete(sLicFileName)
                    End If
                    ' rename copy to original filename for licensefile
                    My.Computer.FileSystem.RenameFile(sLicFileNameCopy, IO.Path.GetFileName(sLicFileName))
                End If

                ' From version 1.00.25 Number of records used is stored in database
                ' (in Company, Code), scrambled as in babel.lic

                oMyDal = New vbBabel.DAL
                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDal.ConnectToDB() Then
                    Throw New System.Exception(oMyDal.ErrorMessage)
                End If

                sUsed = LicCrypt(nLicUsedRecords.ToString, True)

                oMyDal.SQL = "UPDATE Profile SET Code ='" & sUsed & "'"

                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        bReturnValue = True
                    Else
                        ' added 12.09.2018 all lines before Throw to clean up.
                        bAnythingChanged = False
                        If Not oMyDal Is Nothing Then
                            oMyDal.Close()
                            oMyDal = Nothing
                        End If

                        Throw New Exception("LicWrite: None or several records affected")   '& vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                bAnythingChanged = False
            End If

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

        Else
            ' *** like before 28.09.2018

            ' XNET 05.07.2013 - Can't touch Database when Visma !
            If Not sLicServicesAvailable = "V" Then
                ' 01.09.2016 pause to be sure license is free from other processes
                'Pause(0.2)
                oFs = New Scripting.FileSystemObject

                ' ---------------------------------------------------------------
                ' 19.04.2011- Major change -
                ' do not delete the file, before we know we have written a copy !
                ' ---------------------------------------------------------------
                ' Erase the licensefile, we're creating a new one:
                'If Dir(sLicFileName) <> "" Then
                '    ''Kill(sLicFileName)
                '    ''Pause((0.1)) ' to make sure file has been removed

                '    For lTimeCounter = 1 To 10
                '        ' 25.11.04 - Not useing FileSystemObject, due to problems
                '        'If oFs.FileExists(sLicFileName) Then  ' To be certain that file is not present
                '        If Dir(sLicFileName) <> "" Then
                '            'Still alive
                '            Pause((0.1)) ' to make sure file has been removed
                '        Else
                '            Exit For
                '        End If
                '    Next lTimeCounter
                'End If

                ' Changed 25.11.04
                'If Not oFs.FileExists(sLicFileName) Then  ' To be certain that file is not present
                If Dir(sLicFileName) <> "" Then
                    ' NEW - Make a copy of the current licensefile;
                    sLicFileNameCopy = sLicFileName & "_Copy"
                End If

                ' if we have a copy, delete it
                If Dir(sLicFileNameCopy) <> "" Then
                    My.Computer.FileSystem.DeleteFile(sLicFileNameCopy)
                    ' 01.09.2016 - Creates problems? Pause((0.1)) ' to make sure file has been removed
                End If

                ' Open a COPY to write new licensefiledata:
                oFile = oFs.OpenTextFile(sLicFileNameCopy, Scripting.IOMode.ForAppending, True)
                oFile.WriteLine((LicCrypt("USEDRECORDS") & " = " & LicCrypt(Str(nLicUsedRecords), True)))
                oFile.WriteLine((LicCrypt("PREVIOUSRECORDS") & " = " & LicCrypt(Str(nLicPreviousRecords), True)))
                oFile.WriteLine((LicCrypt("LIMITRECORDS") & " = " & LicCrypt(Str(nLicLimitRecords), True)))
                'UPGRADE_WARNING: Couldn't resolve default property of object nLicLimitFormats. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFile.WriteLine((LicCrypt("LIMITFORMATS") & " = " & LicCrypt(Str(nLicLimitFormats), True)))
                oFile.WriteLine((LicCrypt("NOOFUSERS") & " = " & LicCrypt(Str(nLicNoOfUsers), True)))
                oFile.WriteLine((LicCrypt("NOOFCOMPUTERS") & " = " & LicCrypt(Str(nLicNoOfComputers), True)))
                oFile.WriteLine((LicCrypt("SERVICESAVAILABLE") & " = " & LicCrypt(sLicServicesAvailable)))
                ' Set datelastused to today
                sLicDateLastUsed = Str(Year(Now)) & Right("0" & Trim(Str(Month(Now))), 2) & Right("0" & Trim(Str(VB.Day(Now))), 2)
                oFile.WriteLine((LicCrypt("DATELASTUSED") & " = " & LicCrypt(sLicDateLastUsed, True)))
                oFile.WriteLine((LicCrypt("LIMITDATE") & " = " & LicCrypt(sLicLimitDate, True)))
                oFile.WriteLine((LicCrypt("NUMBER") & " = " & LicCrypt(sLicNumber, True)))
                oFile.WriteLine((LicCrypt("NAME") & " = " & LicCrypt(sLicName)))
                oFile.WriteLine((LicCrypt("SUPPORT") & " = " & LicCrypt(sLicSupport)))
                oFile.WriteLine((LicCrypt("DATERESETS") & " = " & LicCrypt(Str(nLicDateResets), True)))

                '27.12.2011 KI:Added next line to free sLicFileNameCopy
                oFile.Close()
                oFile = Nothing

                ' at this stage we have written to a copy of the licensefile
                ' then, delete the original one if we know we have a copy;
                If Dir(sLicFileNameCopy) <> "" Then
                    ' 01.09.2016 Changed If
                    If My.Computer.FileSystem.FileExists(sLicFileName) Then
                        'if Dir(sLicFileName) <> "" Then
                        My.Computer.FileSystem.DeleteFile(sLicFileName)
                    End If
                    ' rename copy to original filename for licensefile
                    My.Computer.FileSystem.RenameFile(sLicFileNameCopy, IO.Path.GetFileName(sLicFileName))
                End If

                ' From version 1.00.25 Number of records used is stored in database
                ' (in Company, Code), scrambled as in babel.lic

                oMyDal = New vbBabel.DAL
                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDal.ConnectToDB() Then
                    Throw New System.Exception(oMyDal.ErrorMessage)
                End If

                sUsed = LicCrypt(nLicUsedRecords.ToString, True)

                oMyDal.SQL = "UPDATE Profile SET Code ='" & sUsed & "'"

                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        bReturnValue = True
                    Else
                        ' added 12.09.2018 all lines before Throw to clean up.
                        bAnythingChanged = False
                        If Not oMyDal Is Nothing Then
                            oMyDal.Close()
                            oMyDal = Nothing
                        End If

                        If Not oFile Is Nothing Then
                            oFile.Close()
                            oFile = Nothing
                        End If

                        If Not oFs Is Nothing Then
                            oFs = Nothing
                        End If

                        Throw New Exception("LicWrite: None or several records affected")   '& vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If
                ' 01.09.2016 pause to be sure license is free from other processes
                Pause(0.2)

                ' 12.09.2018 <-------------------
                ' Re Kjell's problems at Lindorff where oLicense is not released before Pause(2) at the end of ImportExport
                ' Added next line
                bAnythingChanged = False
            End If

            ' 11.09.2018 Cut the Catch/End Try - let the calling function handle the error.

            'Catch ex As Exception

            '    If Not oMyDal Is Nothing Then
            '        oMyDal.Close()
            '        oMyDal = Nothing
            '    End If

            '    If Not oFile Is Nothing Then
            '        oFile.Close()
            '        oFile = Nothing
            '    End If

            '    If Not oFs Is Nothing Then
            '        oFs = Nothing
            '    End If

            '    bReturnValue = False

            '    ' 11.09.2018 - commented Throw New Exception("Function: LicWrite" & vbCrLf & ex.Message)

            'End Try

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If
            '*********************************** OLD ********************
        End If

        Return bReturnValue

    End Function
    Friend Function LicDecrypt(ByRef sString As String) As String
        ' Decrypt a string from licenstable
        ' Numerics run through a cdv-control as well
        Dim sResult As String
        Dim i As Short
        Dim bNumeric As Boolean
        Dim nSquarePart As Short
        Dim sFirstPart As String
        Dim sBaseNumber As String
        Dim sChar As String
        Dim nResult As Double
        sResult = ""

        If Right(sString, 1) = "-" Then
            ' Numeric, special crypting
            bNumeric = True
            sString = Left(sString, Len(sString) - 1)
        End If

        'If IsNull(sString) Then
        ' 07.12.2009 Changed to
        If EmptyString(sString) Then
            sResult = ""
        Else

            For i = 1 To Len(sString)
                sChar = Mid(sString, i, 1) ' One character at a time
                Select Case sChar
                    Case "i"
                        sResult = sResult & "A"
                    Case "A"
                        sResult = sResult & "B"
                    Case "Q"
                        sResult = sResult & "C"
                    Case "Z"
                        sResult = sResult & "D"
                    Case "y"
                        sResult = sResult & "E"
                    Case "r"
                        sResult = sResult & "F"
                    Case "H"
                        sResult = sResult & "G"
                    Case "E"
                        sResult = sResult & "H"
                    Case "M"
                        sResult = sResult & "I"
                    Case "G"
                        sResult = sResult & "J"
                    Case "N"
                        sResult = sResult & "K"
                    Case "c"
                        sResult = sResult & "L"
                    Case "9"
                        sResult = sResult & "M"
                    Case "F"
                        sResult = sResult & "N"
                    Case "2"
                        sResult = sResult & "O"
                    Case "b"
                        sResult = sResult & "P"
                    Case "5"
                        sResult = sResult & "Q"
                    Case "o"
                        sResult = sResult & "R"
                    Case "L"
                        sResult = sResult & "S"
                    Case "7"
                        sResult = sResult & "T"
                    Case "4"
                        sResult = sResult & "U"
                    Case "S"
                        sResult = sResult & "V"
                    Case "T"
                        sResult = sResult & "W"
                    Case "8"
                        sResult = sResult & "X"
                    Case "6"
                        sResult = sResult & "Y"
                    Case "u"
                        sResult = sResult & "Z"
                    Case "-"
                        sResult = sResult & " "
                    Case "X"
                        sResult = sResult & "0"
                    Case "D"
                        sResult = sResult & "1"
                    Case "j"
                        sResult = sResult & "2"
                    Case "P"
                        sResult = sResult & "3"
                    Case "3"
                        sResult = sResult & "4"
                    Case "1"
                        sResult = sResult & "5"
                    Case "0"
                        sResult = sResult & "6"
                    Case "W"
                        sResult = sResult & "7"
                    Case "V"
                        sResult = sResult & "8"
                    Case "K"
                        sResult = sResult & "9"
                End Select
            Next i
        End If

        If bNumeric Then
            ' Have to CONTROL numerics, to ensure that they are not changed

            ' split in two, three last digits is square-part of crypting
            ' that is, the first three digits after square of result
            nSquarePart = Val(Right(sResult, 3))
            sFirstPart = Left(sResult, 8) ' always 8 digits

            ' Check if anything have been changed:
            If Val(Right(Str(Int(System.Math.Sqrt(Val(sFirstPart)) * 1000)), 3)) <> nSquarePart Then
                Err.Raise(10059, , LRS(10059)) 'Something wrong with licensefile!
            End If

            ' then subtract 8 from first digit, 7 from second, etc:
            sBaseNumber = ""
            For i = 1 To 8
                sBaseNumber = sBaseNumber & Right(Str(Val(Mid(sFirstPart, i, 1)) + i + 1), 1)
            Next i
            nResult = Val(sBaseNumber)
            sResult = Str(Int(nResult))

        End If
        LicDecrypt = sResult

    End Function
    Friend Function LicCrypt(ByVal sString As String, Optional ByRef bNumeric As Boolean = False) As String
        ' crypt a string to licenstable
        ' Numerics run through a cdv-control as well
        Dim i As Short
        Dim sChar As String
        Dim sResult As String
        Dim sTemp As String
        Dim nSquare As Double

        sResult = ""
        sString = Trim(sString)

        If bNumeric Then
            ' extra control for numerics

            ' Always 8 digits
            sResult = Right("00000000" & Trim(sString), 8)
            ' scramble, to make it tougher to read
            ' 1. digit is added 8, 2. digit is added 7, etc, last is added 1
            sTemp = sResult
            sResult = ""
            For i = 1 To 8
                sResult = sResult & Right(Str(Val(CStr(CDbl(Mid(sTemp, i, 1)) + (9 - i)))), 1) ' last digit
            Next i
            ' Square root of scrambled 8-digit string
            nSquare = System.Math.Sqrt(Val(sResult))
            ' take first three decimaldigits og square root:
            nSquare = Int(nSquare * 1000)
            ' place these three digits as last three in numeric-result
            sResult = sResult & Right(Str(nSquare), 3)
            sString = sResult
        End If

        sResult = ""

        For i = 1 To Len(sString)
            sChar = UCase(Mid(sString, i, 1)) ' One character at a time
            Select Case sChar
                Case "A"
                    sResult = sResult & "i"
                Case "B"
                    sResult = sResult & "A"
                Case "C"
                    sResult = sResult & "Q"
                Case "D"
                    sResult = sResult & "Z"
                Case "E"
                    sResult = sResult & "y"
                Case "F"
                    sResult = sResult & "r"
                Case "G"
                    sResult = sResult & "H"
                Case "H"
                    sResult = sResult & "E"
                Case "I"
                    sResult = sResult & "M"
                Case "J"
                    sResult = sResult & "G"
                Case "K"
                    sResult = sResult & "N"
                Case "L"
                    sResult = sResult & "c"
                Case "M"
                    sResult = sResult & "9"
                Case "N"
                    sResult = sResult & "F"
                Case "O"
                    sResult = sResult & "2"
                Case "P"
                    sResult = sResult & "b"
                Case "Q"
                    sResult = sResult & "5"
                Case "R"
                    sResult = sResult & "o"
                Case "S"
                    sResult = sResult & "L"
                Case "T"
                    sResult = sResult & "7"
                Case "U"
                    sResult = sResult & "4"
                Case "V"
                    sResult = sResult & "S"
                Case "W"
                    sResult = sResult & "T"
                Case "X"
                    sResult = sResult & "8"
                Case "Y"
                    sResult = sResult & "6"
                Case "Z"
                    sResult = sResult & "u"
                Case " "
                    sResult = sResult & "-"
                Case "0"
                    sResult = sResult & "X"
                Case "1"
                    sResult = sResult & "D"
                Case "2"
                    sResult = sResult & "j"
                Case "3"
                    sResult = sResult & "P"
                Case "4"
                    sResult = sResult & "3"
                Case "5"
                    sResult = sResult & "1"
                Case "6"
                    sResult = sResult & "0"
                Case "7"
                    sResult = sResult & "W"
                Case "8"
                    sResult = sResult & "V"
                Case "9"
                    sResult = sResult & "K"
                Case Else
                    MsgBox("Error in LicCrypt - Illegal character " & sChar)
            End Select
        Next i

        ' Put a sign on numerics, to tell the decrypt function that we have a numeric
        If bNumeric Then
            sResult = sResult & "-"
        End If
        LicCrypt = sResult


    End Function
    Private Sub Class_Initialize_Renamed()
        bOK = False 'Not ready yet
        sLicFileName = BB_LicensePath() 'App.path + "\babel.lic"
        LicRead() ' Import from License.dat
        bAnythingChanged = False

    End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
        'sLicFileName = BB_LicensePath() 'App.path + "\babel.lic"
        'LicRead() ' Import from License.dat
        'bAnythingChanged = False
    End Sub
    Private Sub Class_Terminate_Renamed()
        ' Write to ini-file when licenseclass is released
        If bAnythingChanged Then
            ' added 22.02.2011, probably not necessary but ...
            bAnythingChanged = False
            LicWrite()
        End If

    End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
End Class
