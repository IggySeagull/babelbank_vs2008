Option Strict Off
Option Explicit On
Module WriteFile
	
	'Public Function WriteCREMULInternFile(oBabelFiles As BabelFiles, bMultiFiles As Boolean, sFilenameOut As String, iFormat_ID As Integer, sI_Account As String, sOwnRef As String, sAdditionalID As String) As Boolean
	Public Function WriteCREMULInternFile(ByRef oBabelFiles As BabelFiles, ByRef sFilenameOut As String) As Boolean
		Dim oBabelFile As BabelFile
		Dim oBatch As Batch
		Dim oPayment As Payment
		Dim oInvoice As Invoice
		Dim oFreeText As Freetext
        Dim oFs As Scripting.FileSystemObject
		Dim oFile As Scripting.TextStream
		Dim sLine, sTransType As String

        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            oFile.WriteLine(("Filename: " & sFilenameOut))
            oFile.WriteBlankLines((2))

            For Each oBabelFile In oBabelFiles
                'Nothing to report

                For Each oBatch In oBabelFile.Batches
                    sLine = "Post #" & oBatch.Index & " on the bank-statement"
                    oFile.WriteLine((sLine))
                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sLine = "Payees accountnumber: " & oBatch.Payments(1).I_Account
                    oFile.WriteLine()
                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().PayCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sTransType = bbGetPayCode(oBatch.Payments(1).PayCode, "CREMUL")
                    sLine = "Transactiontype: "
                    Select Case sTransType
                        Case "230"
                            sLine = sLine & "Total amount valid KID"
                        Case "231"
                            sLine = sLine & "Total amount invalid KID"
                        Case "232"
                            sLine = sLine & "Total amount AutoGiro"
                        Case "233"
                            sLine = sLine & "Total amount electronic payments"
                        Case "234"
                            sLine = sLine & "Total amount Giro notes"
                        Case "BKT"
                            sLine = sLine & "Single bank internal transactions"
                        Case Else
                    End Select
                    oFile.WriteLine((sLine))
                    sLine = "Date: " & CStr(StringToDate((oBatch.DATE_Production)))
                    oFile.WriteLine((sLine))
                    sLine = "Amount: " & Str(oBatch.MON_TransferredAmount / 100)
                    oFile.WriteLine((sLine))
                    oFile.WriteBlankLines((2))

                    For Each oPayment In oBatch.Payments
                        sLine = "Payment #" & oPayment.Index
                        oFile.WriteLine((sLine))
                        sLine = "Payers name: " & oPayment.E_Name
                        oFile.WriteLine((sLine))
                        sLine = "Payers accountnumber: " & oPayment.E_Account & "       Amount: " & Str(oPayment.MON_TransferredAmount / 100)
                        oFile.WriteLine((sLine))
                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.Freetexts.Count > 0 Then
                                oFile.WriteLine(("Freetext:"))
                                For Each oFreeText In oInvoice.Freetexts
                                    oFile.WriteLine((oFreeText.Text))
                                Next oFreeText
                            End If
                        Next oInvoice

                    Next oPayment

                Next oBatch

            Next oBabelFile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteCREMULInternFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteCREMULInternFile = True

    End Function
	
    Function WriteNavision_for_SI_Data(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef bPostAgainstObservationAccount As Boolean, ByVal sSpecial As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim iFreetextCounter As Short
        Dim sFreetextFixed, sFreeTextVariable As String
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oOldClient As vbBabel.Client
        Dim oaccount As Account
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sLineToWrite As String
        Dim bFirstBatch As Boolean
        Dim nSequenceNoTotal As Double
        Dim bBatchRecordWritten As Boolean
        Dim sDateToUse, sAmount As String
        Dim sVoucherNo As String
        Dim nSeqNoType As Short
        Dim sAlphaPart As String
        Dim nDigitsLen As Short
        Dim sGLAccount As String
        Dim sUnmatchedInfo As String
        Dim bUseTransferredAmount As Boolean
        Dim bRecordsExported As Boolean
        Dim sOldAccountNo As String = String.Empty
        Dim bAccountFound As Boolean
        Dim sOldClientNo As String
        Dim sTmp As String = ""

        ' added 03.05.2016 - to be able to export files where we have matched on Original Amount;
        Dim bUseOriginalAmount As Boolean = False
        Dim bDummy As Boolean = False
        Dim nTotalInvoices As Double = 0
        Dim nTemp As Double = 0
        Dim ntempTotalAmount As Double = 0

        Dim bUseOriginalAmountOnThisPayment As Boolean = True

        Try

            ' Find settings for Matching against origianlly paid amount/currency
            RestoreManualSetup(bDummy, bDummy, bDummy, bDummy, bDummy, bUseOriginalAmount) 'bAllowDifferenceInCurrency

            ' lag en outputfil
            bAppendFile = False
            bBatchRecordWritten = False
            bFirstBatch = True
            bRecordsExported = False

            oFs = New Scripting.FileSystemObject


            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'New 06.09.2006 by kjell
                        If Not oPayment.Exported Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                '17.02.2015 - Why test on account and not only client (this created problems for Hesnes when there where incoming payments on two accounts for the same client
                                'Commented next IF
                                'If oPayment.I_Account = sI_Account Then
                                If Len(oPayment.VB_ClientNo) > 0 Then
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        bExportoBabel = True
                                    End If
                                Else
                                    bExportoBabel = True
                                End If
                                'End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'New 06.09.2006 by kjell
                            If Not oPayment.Exported Then
                                'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    '17.02.2015 - Why test on account and not only client (this created problems for Hesnes when there where incoming payments on two accounts for the same client
                                    'Commented next IF
                                    'If oPayment.I_Account = sI_Account Then
                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBatch = True
                                        End If
                                    Else
                                        'bExportoBatch = True
                                    End If
                                    'End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                bBatchRecordWritten = False
                                If bFirstBatch = True Then
                                    'This is the first batch and we want to retrieve the
                                    ' document number for Navision.
                                    'We select this counter from the sequencenumber in oProfile
                                    bFirstBatch = False
                                    'First check if we use a profile
                                    If oBatch.VB_ProfileInUse = True Then
                                        '                        'Then if it is a return-file
                                        '                        If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        nSeqNoType = oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                        Select Case nSeqNoType
                                            'Always use the seqno at filesetup level
                                            Case 1
                                                'Only for the first batch if we have one sequenceseries!
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                nSequenceNoTotal = oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal
                                                If nSequenceNoTotal > 99999999 Then
                                                    nSequenceNoTotal = 0
                                                End If
                                            Case 2
                                                ' VoucherNo at Clientlevel:
                                                ' Find Client

                                            Case Else
                                                Err.Raise(12002, "WriteNavision_for_SI_Data", LRS(12002, "Navision", sFilenameOut))

                                        End Select
                                    End If
                                End If

                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            j = 0
                            For Each oPayment In oBatch.Payments
                                bUseTransferredAmount = False
                                'Currency
                                If Len(oPayment.MON_InvoiceCurrency) <> 3 Then
                                    sLineToWrite = sLineToWrite & "NOK"
                                Else
                                    'Temporary change, because valuta transactions are not yet implemented
                                    'Changed by Kjell 29.06.2005
                                    'Also changed for all other transactiontypes.
                                    If CDbl(Trim(CStr(Len(oPayment.MON_TransferCurrency)))) = 3 Then
                                        If Trim(oPayment.MON_InvoiceCurrency) <> Trim(oPayment.MON_TransferCurrency) Then
                                            bUseTransferredAmount = True
                                        End If
                                    End If
                                End If
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile

                                'New 06.09.2006 by kjell
                                If Not oPayment.Exported Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        '17.02.2015 - Why test on account and not only client (this created problems for Hesnes when there where incoming payments on two accounts for the same client
                                        'Commented next IF
                                        'If oPayment.I_Account = sI_Account Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoPayment = True
                                            End If
                                            'Else
                                            'bExportoPayment = True
                                        End If
                                        'End If
                                    End If
                                End If
                                If bExportoPayment Then

                                    '08.01.2015 - Added next loop to find client-/accountstuff
                                    If oPayment.I_Account <> sOldAccountNo Then
                                        If oPayment.VB_ProfileInUse Then
                                            sI_Account = oPayment.I_Account
                                            bAccountFound = False
                                            oOldClient = oClient
                                            For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                For Each oClient In oFilesetup.Clients
                                                    For Each oaccount In oClient.Accounts
                                                        If sI_Account = oaccount.Account Then
                                                            sGLAccount = oaccount.GLAccount.Trim
                                                            sOldAccountNo = oPayment.I_Account
                                                            bAccountFound = True
                                                            Exit For
                                                        End If
                                                    Next oaccount
                                                    If bAccountFound Then
                                                        '08.01.2015 - All code inside this IF is added
                                                        If nSeqNoType = 2 Then

                                                            'Store the VoucherNo from last used client
                                                            If sOldClientNo <> oClient.ClientNo Then
                                                                If IsNothing(sOldClientNo) Then
                                                                    sOldClientNo = oClient.ClientNo
                                                                Else
                                                                    sOldClientNo = oClient.ClientNo
                                                                    sVoucherNo = sAlphaPart & PadLeft(Trim(Str(nSequenceNoTotal)), nDigitsLen, "0")
                                                                    oOldClient.VoucherNo = sVoucherNo
                                                                    oOldClient.Status = Profile.CollectionStatus.Changed
                                                                    oBatch.VB_Profile.Status = Profile.CollectionStatus.ChangeUnder
                                                                    oBatch.VB_Profile.FileSetups(iFormat_ID).Status = Profile.CollectionStatus.ChangeUnder
                                                                End If
                                                            End If

                                                            'Retrieve the VoucherNo from new client
                                                            sVoucherNo = Trim(oClient.VoucherNo)
                                                            sGLAccount = Trim(oaccount.GLAccount)

                                                            ' Keep numberpart of VocherNo
                                                            ' How many digits in "last" part of voucher
                                                            nDigitsLen = 0
                                                            sAlphaPart = ""
                                                            For i = Len(sVoucherNo) To 1 Step -1
                                                                If IsNumeric(Mid(sVoucherNo, i, 1)) Then
                                                                    nDigitsLen = nDigitsLen + 1
                                                                Else
                                                                    Exit For
                                                                End If
                                                            Next i
                                                            sAlphaPart = Left(sVoucherNo, Len(sVoucherNo) - nDigitsLen)
                                                            nSequenceNoTotal = Val(Right(sVoucherNo, nDigitsLen))
                                                            If sVoucherNo = "" Then
                                                                Err.Raise(12002, "WriteNavision_for_SI_Data", LRS(12002, "Navision", sFilenameOut))
                                                            End If
                                                        End If
                                                        Exit For
                                                    End If
                                                Next oClient
                                                If bAccountFound Then Exit For
                                            Next oFilesetup
                                        End If
                                    End If

                                    If Not bAccountFound Then
                                        Err.Raise(12003, "WriteNavision_for_SI_Data", LRS(12003))
                                        '12003: Could not find account %1.Please enter the account in setup."
                                    End If


                                    bRecordsExported = True
                                    '19/8-2004 - New code by Kjell
                                    'Added code for writing records after the new way of manual matching
                                    If bPostAgainstObservationAccount Then
                                        'Second time export - (new way - posting against the 'unidentified account)
                                        sLineToWrite = ""
                                        sLineToWrite = "K" & Space(4)
                                        If sSpecial = "KEYSTONE" Then
                                            ' no bilagsnummer for Keystone
                                            sLineToWrite = sLineToWrite & Space(10)
                                        Else
                                            If nSeqNoType = 1 Then
                                                nSequenceNoTotal = nSequenceNoTotal + 1
                                                If nSequenceNoTotal > 99999999 Then
                                                    nSequenceNoTotal = 0
                                                End If
                                                'Document no.
                                                'sLineToWrite = sLineToWrite & "A" & PadLeft(Trim(Str(nSequenceNoTotal)), 10, "0")
                                                sLineToWrite = sLineToWrite & "A" & PadLeft(Trim(Str(nSequenceNoTotal)), 9, "0") '07.01.2015 - Changed from 10 to 9 in PadLeft to get the total length to 10
                                            Else 'SeqnoType =2, ClientLevel
                                                nSequenceNoTotal = nSequenceNoTotal + 1
                                                'Document no.
                                                'sLineToWrite = sLineToWrite & sAlphaPart & PadRight(PadLeft(Trim(Str(nSequenceNoTotal)), nDigitsLen, "0"), 10, " ")
                                                sLineToWrite = sLineToWrite & PadRight(sAlphaPart & PadLeft(Trim(Str(nSequenceNoTotal)), nDigitsLen, "0"), 10, " ")
                                            End If
                                        End If
                                        'Date
                                        If Not oPayment.DATE_Value = "19900101" Then
                                            sDateToUse = oPayment.DATE_Value
                                        Else 'Use payment_date
                                            If Not oPayment.DATE_Payment = "19900101" Then
                                                sDateToUse = oPayment.DATE_Payment
                                            Else
                                                sDateToUse = DateToString(Now)
                                            End If
                                        End If
                                        ' 02.10.2019
                                        ' Teknotherm AS �nsker � bruke bokf�ringsdato
                                        If oBabel.VB_Profile.CompanyName = "Teknotherm AS" Then
                                            If Not oPayment.DATE_Payment = "19900101" Then
                                                sDateToUse = oPayment.DATE_Payment
                                            Else
                                                sDateToUse = DateToString(Now)
                                            End If
                                        End If
                                        sLineToWrite = sLineToWrite & Space(6) & sDateToUse & " "

                                        'Accountnumber
                                        If EmptyString((oPayment.VB_ObservationAccount)) Then
                                            Err.Raise(1, "WriteNavision_for_SI_Data", "Det er ikke angitt noen feilreskontro som motpost p� betalingen.")
                                        Else
                                            sLineToWrite = sLineToWrite & PadRight(oPayment.VB_ObservationAccount, 13, " ")
                                        End If
                                        'Amount/Currency
                                        If bUseTransferredAmount Then
                                            sAmount = ConvertFromAmountToString((oPayment.MON_TransferredAmount), , ".")
                                            sLineToWrite = sLineToWrite & PadLeft(sAmount, 20, " ")
                                            sLineToWrite = sLineToWrite & Trim(oPayment.MON_TransferCurrency)
                                        Else
                                            sAmount = ConvertFromAmountToString((oPayment.MON_InvoiceAmount), , ".")
                                            sLineToWrite = sLineToWrite & PadLeft(sAmount, 20, " ")
                                            If Len(oPayment.MON_InvoiceCurrency) <> 3 Then
                                                sLineToWrite = sLineToWrite & "NOK"
                                            Else
                                                sLineToWrite = sLineToWrite & Trim(oPayment.MON_InvoiceCurrency)
                                            End If
                                        End If
                                        'Amount
                                        sAmount = Replace(sAmount, ".", "")
                                        sLineToWrite = sLineToWrite & Space(5) & PadLeft(sAmount, 13, " ")
                                        'Credit if positive (should always be positive
                                        ' opposite of first time posting
                                        If oPayment.MON_InvoiceAmount < 0 Then
                                            sLineToWrite = sLineToWrite & " " & "K" & " "
                                        Else
                                            sLineToWrite = sLineToWrite & " " & "D" & " "
                                        End If
                                        sLineToWrite = sLineToWrite & Space(36)
                                        'Bank text - Reference number
                                        sLineToWrite = sLineToWrite & PadRight(Trim(oPayment.E_Name), 13, " ") & Space(17)
                                        sLineToWrite = sLineToWrite & PadRight(Trim(oPayment.E_Name), 50, " ")
                                        sLineToWrite = sLineToWrite & Space(16)
                                        If EmptyString((oPayment.MATCH_MatchingIDAgainstObservationAccount)) Then
                                            Err.Raise(1, "WriteNavision_for_SI_Data", "Det er ikke angitt noen unik ID p� posteringen mot observasjonskontoen." & vbCrLf & "Betalers navn: " & oPayment.E_Name & vbCrLf & "Bel�p:      " & Str(oPayment.MON_InvoiceAmount) & vbCrLf & "BabelBank avsluttes")
                                        Else
                                            sLineToWrite = sLineToWrite & PadRight(oPayment.MATCH_MatchingIDAgainstObservationAccount, 25, " ") 'New Matching-ID (L�penummer)
                                        End If
                                        sLineToWrite = sLineToWrite & Space(25) 'Unique ERP_ID
                                        oFile.WriteLine((sLineToWrite))
                                        'End new code
                                    Else
                                        'First time export - (old way including posting on the GL bank)
                                        'Write GL-line if not previously written
                                        If Not bBatchRecordWritten Then
                                            sLineToWrite = ""
                                            ' 01.06.2016
                                            ' Some NAV customers do not book to Bank-account, but to Financeaccount instead (F)
                                            ' We have no setup for this, so use Special
                                            ' 04.02.2019- Keystone will use "B", not "F" anymore
                                            If sSpecial = "KEYSTONE" Then
                                                'sLineToWrite = "F" & Space(4)
                                                sLineToWrite = "B" & Space(4)
                                                ' no Documentno for Keystone
                                                sLineToWrite = sLineToWrite & Space(10)
                                            Else
                                                ' Normally, use "B" for Bankposting
                                                sLineToWrite = "B" & Space(4)
                                                If nSeqNoType = 1 Then
                                                    nSequenceNoTotal = nSequenceNoTotal + 1
                                                    If nSequenceNoTotal > 99999999 Then
                                                        nSequenceNoTotal = 0
                                                    End If
                                                    'Document no.
                                                    sLineToWrite = sLineToWrite & "A" & PadLeft(Trim(Str(nSequenceNoTotal)), 9, "0") '07.01.2015 - Changed from 10 to 9 in PadLeft to get the total length to 10
                                                Else 'SeqnoType =2, ClientLevel
                                                    nSequenceNoTotal = nSequenceNoTotal + 1
                                                    'Document no.
                                                    sLineToWrite = sLineToWrite & PadRight(sAlphaPart & PadLeft(Trim(Str(nSequenceNoTotal)), nDigitsLen, "0"), 10, " ")
                                                End If
                                            End If
                                            'Date
                                            If Not oPayment.DATE_Value = "19900101" Then
                                                sDateToUse = oPayment.DATE_Value
                                            Else 'Use payment_date
                                                If Not oPayment.DATE_Payment = "19900101" Then
                                                    sDateToUse = oPayment.DATE_Payment
                                                Else
                                                    sDateToUse = DateToString(Now)
                                                End If
                                            End If
                                            ' 28.09.2016 - use bookdate for Keystone (why not for all???)
                                            If sSpecial = "KEYSTONE" Then
                                                sDateToUse = oPayment.DATE_Payment
                                            End If
                                            ' 02.10.2019
                                            ' Teknotherm AS �nsker � bruke bokf�ringsdato
                                            If oBabel.VB_Profile.CompanyName = "Teknotherm AS" Then
                                                If Not oPayment.DATE_Payment = "19900101" Then
                                                    sDateToUse = oPayment.DATE_Payment
                                                Else
                                                    sDateToUse = DateToString(Now)
                                                End If
                                            End If
                                            sLineToWrite = sLineToWrite & Space(6) & sDateToUse & " "

                                            'Accountnumber
                                            'sLineToWrite = sLineToWrite & "NHOVED       "
                                            sLineToWrite = sLineToWrite & PadRight(sGLAccount, 13, " ")
                                            'Amount/Currency
                                            If bUseTransferredAmount Then
                                                sAmount = ConvertFromAmountToString((oBatch.MON_TransferredAmount), , ".")
                                                sLineToWrite = sLineToWrite & PadLeft(sAmount, 20, " ")
                                                sLineToWrite = sLineToWrite & Trim(oPayment.MON_TransferCurrency)
                                            Else
                                                sAmount = ConvertFromAmountToString((oBatch.MON_InvoiceAmount), , ".")
                                                sLineToWrite = sLineToWrite & PadLeft(sAmount, 20, " ")
                                                If Len(oPayment.MON_InvoiceCurrency) <> 3 Then
                                                    sLineToWrite = sLineToWrite & "NOK"
                                                Else
                                                    sLineToWrite = sLineToWrite & Trim(oPayment.MON_InvoiceCurrency)
                                                End If
                                            End If
                                            'Amount
                                            sAmount = Replace(sAmount, ".", "")
                                            sLineToWrite = sLineToWrite & Space(5) & PadLeft(sAmount, 13, " ")
                                            If oBatch.MON_InvoiceAmount < 0 Then
                                                sLineToWrite = sLineToWrite & " K"
                                            Else
                                                sLineToWrite = sLineToWrite & " D"
                                            End If
                                            'Sign + AppliesToDocNo - InvoiceNo + Something else - not in use on GL
                                            sLineToWrite = sLineToWrite & Space(37)
                                            'Bank text - Reference number
                                            sLineToWrite = sLineToWrite & PadRight(Trim(oBatch.REF_Bank), 13, " ") & Space(17)

                                            ' 28.09.2016
                                            ' Special text for Keystone, for the "Bank"-posting (Financial posting)
                                            ' They have one payment = one bankbooking
                                            If sSpecial = "KEYSTONE" Then
                                                sTmp = ""
                                                'For Each oInvoice In oPayment.Invoices
                                                '    For Each oFreeText In oInvoice.Freetexts
                                                '        sTmp = sTmp & Trim(oFreeText.Text)
                                                '    Next
                                                'Next
                                                ' Only payers name required for Keystone
                                                sTmp = Trim(oPayment.E_Name) & " " & sTmp
                                                sLineToWrite = sLineToWrite & PadRight(Trim(sTmp), 50, " ")
                                            Else
                                                sLineToWrite = sLineToWrite & PadRight(Trim(oBatch.REF_Bank), 50, " ")
                                            End If

                                            sLineToWrite = sLineToWrite & Space(16)
                                            sLineToWrite = sLineToWrite & Space(50) 'New Matching-ID (L�penummer) 25 + Unique ERP_ID 25

                                            ' 27.08.2020 - The extra 35 causes problems for Keystone
                                            If sSpecial <> "KEYSTONE" And sSpecial <> "TEKNOTHERM" Then
                                                ' 05.03.2020 - added e_account for update of NAV, on customerlines. add spaces here
                                                sLineToWrite = sLineToWrite & Space(35)
                                            End If
                                            oFile.WriteLine((sLineToWrite))

                                            bBatchRecordWritten = True
                                        End If 'If Not bBatchRecordWritten Then
                                    End If 'If bPostAgainstObservationAccount Then

                                    'Write Payment-lines
                                    'First we have to find the freetext
                                    iFreetextCounter = 0
                                    sFreetextFixed = ""
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Original = True Then
                                            For Each oFreeText In oInvoice.Freetexts
                                                iFreetextCounter = iFreetextCounter + 1
                                                If iFreetextCounter = 1 Then
                                                    sFreetextFixed = RTrim(oFreeText.Text)
                                                ElseIf iFreetextCounter = 2 Then
                                                    sFreetextFixed = sFreetextFixed & " " & RTrim(oFreeText.Text)
                                                Else
                                                    Exit For
                                                End If
                                            Next oFreeText
                                        End If
                                    Next oInvoice
                                    If sSpecial = "KEYSTONE" Then
                                        sFreetextFixed = Trim(oPayment.E_Name)  ' name only for Keystone
                                    Else
                                        sFreetextFixed = Trim(oPayment.E_Name) & " " & sFreetextFixed
                                    End If
                                    If oPayment.MATCH_Matched = BabelFiles.MatchStatus.NotMatched And oPayment.VB_WritePaymentAmountIfOpen Then
                                        sLineToWrite = ""
                                        'Always open
                                        sLineToWrite = "K" & Space(2) & "0" & Space(1)

                                        If nSeqNoType = 1 Then
                                            'Document no.
                                            'sLineToWrite = sLineToWrite & "A" & PadLeft(Trim(Str(nSequenceNoTotal)), 10, "0")
                                            sLineToWrite = sLineToWrite & "A" & PadLeft(Trim(Str(nSequenceNoTotal)), 9, "0") '07.01.2015 - Changed from 10 to 9 in PadLeft to get the total length to 10
                                        Else
                                            'sLineToWrite = sLineToWrite & sAlphaPart & PadRight(PadLeft(Trim(Str(nSequenceNoTotal)), nDigitsLen, "0"), 10, " ")
                                            sLineToWrite = sLineToWrite & PadRight(sAlphaPart & PadLeft(Trim(Str(nSequenceNoTotal)), nDigitsLen, "0"), 10, " ")
                                        End If
                                        'Date
                                        If Not oPayment.DATE_Value = "19900101" Then
                                            sDateToUse = oPayment.DATE_Value
                                        Else 'Use payment_date
                                            If Not oPayment.DATE_Payment = "19900101" Then
                                                sDateToUse = oPayment.DATE_Payment
                                            Else
                                                sDateToUse = DateToString(Now)
                                            End If
                                        End If
                                        ' 28.09.2016 - use bookdate for Keystone (why not for all???)
                                        If sSpecial = "KEYSTONE" Then
                                            sDateToUse = oPayment.DATE_Payment
                                        End If
                                        ' 02.10.2019
                                        ' Teknotherm AS �nsker � bruke bokf�ringsdato
                                        If oBabel.VB_Profile.CompanyName = "Teknotherm AS" Then
                                            If Not oPayment.DATE_Payment = "19900101" Then
                                                sDateToUse = oPayment.DATE_Payment
                                            Else
                                                sDateToUse = DateToString(Now)
                                            End If
                                        End If
                                        sLineToWrite = sLineToWrite & Space(6) & sDateToUse & " "
                                        'Accountnumber
                                        'The payment is always unmatched, hence all invoices are unmatched
                                        ' and the invoices match_id is equal to the info necessary to post on the observation account
                                        sUnmatchedInfo = ""
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Final = True Then
                                                sLineToWrite = sLineToWrite & PadRight(oInvoice.MATCH_ID, 13, " ")
                                                sUnmatchedInfo = oInvoice.MATCH_ID
                                                Exit For
                                            End If
                                        Next oInvoice

                                        If EmptyString(sUnmatchedInfo) Then
                                            Err.Raise(12002, "WriteNavision_for_SI_Data", LRS(12002, "Navision", sFilenameOut))
                                        End If
                                        'Amount/Currency
                                        'New 17.03.2004
                                        'Multiply amount with -1 for all payments
                                        If bUseTransferredAmount Then
                                            sAmount = ConvertFromAmountToString(oPayment.MON_TransferredAmount * -1, , ".")
                                            sLineToWrite = sLineToWrite & PadLeft(sAmount, 20, " ")
                                            sLineToWrite = sLineToWrite & Trim(oPayment.MON_TransferCurrency)
                                        Else
                                            sAmount = ConvertFromAmountToString(oPayment.MON_InvoiceAmount * -1, , ".")
                                            sLineToWrite = sLineToWrite & PadLeft(sAmount, 20, " ")
                                            If Len(oPayment.MON_InvoiceCurrency) <> 3 Then
                                                sLineToWrite = sLineToWrite & "NOK"
                                            Else
                                                sLineToWrite = sLineToWrite & Trim(oPayment.MON_InvoiceCurrency)
                                            End If
                                        End If
                                        'Amount
                                        sAmount = Replace(sAmount, ".", "")
                                        sLineToWrite = sLineToWrite & Space(5) & PadLeft(sAmount, 13, " ")
                                        'Sign
                                        'New 17.03.2004
                                        'Changed for GL-postings
                                        If oPayment.MON_InvoiceAmount < 0 Then
                                            sLineToWrite = sLineToWrite & " " & "D" & " "
                                        Else
                                            sLineToWrite = sLineToWrite & " " & "K" & " "
                                        End If
                                        'AppliesToDocNo - InvoiceNo
                                        sLineToWrite = sLineToWrite & Space(18) 'Always open, No invoiceNo
                                        sLineToWrite = sLineToWrite & PadRight(sUnmatchedInfo, 18, " ") & Space(30)
                                        'Customer text
                                        sFreeTextVariable = sFreetextFixed
                                        For Each oFreeText In oInvoice.Freetexts
                                            If oFreeText.Qualifier > 2 Then
                                                sFreeTextVariable = RTrim(oFreeText.Text) & " " & sFreeTextVariable
                                            End If
                                        Next oFreeText
                                        sLineToWrite = sLineToWrite & PadRight(sFreeTextVariable, 50, " ")
                                        'Document type from Myfield
                                        'New 17.03.2004
                                        sLineToWrite = sLineToWrite & Space(16) 'Always open, No document type
                                        sLineToWrite = sLineToWrite & Space(25) 'New Matching-ID (L�penummer)
                                        sLineToWrite = sLineToWrite & PadRight(oPayment.Unique_ERPID, 25, " ") ' Unique ERP_ID 25
                                        oFile.WriteLine((sLineToWrite))


                                    Else
                                        ' added 20.05.2016
                                        If bUseOriginalAmount Then
                                            ' First, find amount in currency total for all invoices;
                                            ' Will be used to book the "NOK" amount for each invoice
                                            nTotalInvoices = 0
                                            For Each oInvoice In oPayment.Invoices
                                                If oInvoice.MATCH_Final = True Then
                                                    nTotalInvoices = nTotalInvoices + oInvoice.MON_InvoiceAmount
                                                End If
                                            Next
                                        End If
                                        ntempTotalAmount = 0

                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_Final = True Then
                                                sLineToWrite = ""
                                                'MatchTypes
                                                'OpenInvoice = -1
                                                'MatchedOnInvoice = 0
                                                'MatchedOnCustomer = 1
                                                'MatchedOnGL = 2
                                                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.OpenInvoice Then
                                                    sLineToWrite = "K" & Space(2) & "0" & Space(1)
                                                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL Then
                                                    sLineToWrite = "F" & Space(4)
                                                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnSupplier Then
                                                    sLineToWrite = "L" & Space(2) & "1" & Space(1)
                                                Else
                                                    sLineToWrite = "K" & Space(2) & "1" & Space(1)
                                                End If

                                                If sSpecial = "KEYSTONE" Then
                                                    ' no bilagsnummer for keystone
                                                    sLineToWrite = sLineToWrite & Space(10)
                                                Else
                                                    If nSeqNoType = 1 Then
                                                        'Document no.
                                                        'sLineToWrite = sLineToWrite & "A" & PadLeft(Trim(Str(nSequenceNoTotal)), 10, "0")
                                                        sLineToWrite = sLineToWrite & "A" & PadLeft(Trim(Str(nSequenceNoTotal)), 9, "0") '07.01.2015 - Changed from 10 to 9 in PadLeft to get the total length to 10
                                                    Else
                                                        'sLineToWrite = sLineToWrite & sAlphaPart & PadRight(PadLeft(Trim(Str(nSequenceNoTotal)), nDigitsLen, "0"), 10, " ")
                                                        sLineToWrite = sLineToWrite & PadRight(sAlphaPart & PadLeft(Trim(Str(nSequenceNoTotal)), nDigitsLen, "0"), 10, " ")
                                                    End If
                                                End If
                                                'Date
                                                If Not oPayment.DATE_Value = "19900101" Then
                                                    sDateToUse = oPayment.DATE_Value
                                                Else 'Use payment_date
                                                    If Not oPayment.DATE_Payment = "19900101" Then
                                                        sDateToUse = oPayment.DATE_Payment
                                                    Else
                                                        sDateToUse = DateToString(Now)
                                                    End If
                                                End If
                                                ' 28.09.2016 - use bookdate for Keystone (why not for all???)
                                                If sSpecial = "KEYSTONE" Then
                                                    sDateToUse = oPayment.DATE_Payment
                                                End If
                                                ' 02.10.2019
                                                ' Teknotherm AS �nsker � bruke bokf�ringsdato
                                                If oBabel.VB_Profile.CompanyName = "Teknotherm AS" Then
                                                    If Not oPayment.DATE_Payment = "19900101" Then
                                                        sDateToUse = oPayment.DATE_Payment
                                                    Else
                                                        sDateToUse = DateToString(Now)
                                                    End If
                                                End If
                                                sLineToWrite = sLineToWrite & Space(6) & sDateToUse & " "
                                                'Accountnumber

                                                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.OpenInvoice Then
                                                    sLineToWrite = sLineToWrite & PadRight(oInvoice.MATCH_ID, 13, " ")
                                                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL Then
                                                    sLineToWrite = sLineToWrite & PadRight(oInvoice.MATCH_ID, 13, " ")
                                                Else
                                                    sLineToWrite = sLineToWrite & PadRight(oInvoice.CustomerNo, 13, " ")
                                                End If
                                                'Amount/Currency
                                                'New 17.03.2004
                                                'Multiply amount with -1 for all payments

                                                ' 03.05.2016 Added option to match on Original paid amount
                                                ' changed 20.05.2016

                                                ' 12.06.2019 for Keystone, added next lines, and changed If 7 lines below
                                                If sSpecial = "KEYSTONE" Then
                                                    bUseOriginalAmountOnThisPayment = True
                                                    ' If same currency for account and originally paid amount, then do not use originalAmount
                                                    If oPayment.MON_AccountCurrency = oPayment.MON_OriginallyPaidCurrency Then
                                                        'bUseOriginalAmountOnThisPayment = False
                                                    End If
                                                End If
                                                If bUseOriginalAmount And bUseOriginalAmountOnThisPayment Then
                                                    ' When we have different currencies on invoice and bank, we need to put the Bank-amount (normally NOK)
                                                    ' in the first amountscolumn.
                                                    ' If more than one invoice, we have to calculate the NOK amount on each invoice to NOK first;

                                                    If oPayment.Invoices.Count = 1 Then
                                                        sAmount = ConvertFromAmountToString(oPayment.MON_InvoiceAmount * -1, , ".")
                                                        sLineToWrite = sLineToWrite & PadLeft(sAmount, 20, " ")
                                                        sLineToWrite = sLineToWrite & Trim(oPayment.MON_OriginallyPaidCurrency)
                                                        ' for next amountcolumn, use amount in currency - found at Invoicelevel
                                                        sAmount = ConvertFromAmountToString(oPayment.MON_OriginallyPaidAmount * -1, , ".")
                                                    Else
                                                        ' Need to calculate "bankamount" (NOK) to each invoice;
                                                        ' Book the NOK amount for this invoice, by taking the payment NOK amount, 
                                                        ' and multiply with this invoices total part of the total payment
                                                        nTemp = Math.Round((oPayment.MON_InvoiceAmount * oInvoice.MON_InvoiceAmount / nTotalInvoices), 0) * -1
                                                        ' 17.06.2016 -Total up amount, in case rounding gives us a diff (of 0,01)
                                                        ' First time used for Keystone
                                                        ntempTotalAmount = ntempTotalAmount + nTemp
                                                        If oInvoice.Index = oPayment.Invoices(oPayment.Invoices.Count).Index Then
                                                            If oPayment.MON_InvoiceAmount + ntempTotalAmount <> 0 Then
                                                                ' adjust last invoice
                                                                nTemp = nTemp - (oPayment.MON_InvoiceAmount + ntempTotalAmount)
                                                            End If
                                                        End If

                                                        sAmount = ConvertFromAmountToString(nTemp, , ".")
                                                        sLineToWrite = sLineToWrite & PadLeft(sAmount, 20, " ")
                                                        sLineToWrite = sLineToWrite & Trim(oPayment.MON_OriginallyPaidCurrency)
                                                        ' for next amountcolumn, use amount in currency - found at Invoicelevel
                                                        sAmount = ConvertFromAmountToString(oInvoice.MON_InvoiceAmount * -1, , ".")
                                                    End If

                                                    ' from here like code Pre 03.05.2016
                                                ElseIf bUseTransferredAmount Then
                                                    sAmount = ConvertFromAmountToString(oInvoice.MON_TransferredAmount * -1, , ".")
                                                    sLineToWrite = sLineToWrite & PadLeft(sAmount, 20, " ")
                                                    sLineToWrite = sLineToWrite & Trim(oPayment.MON_TransferCurrency)
                                                Else
                                                    sAmount = ConvertFromAmountToString(oInvoice.MON_InvoiceAmount * -1, , ".")
                                                    sLineToWrite = sLineToWrite & PadLeft(sAmount, 20, " ")
                                                    If Len(oPayment.MON_InvoiceCurrency) <> 3 Then
                                                        sLineToWrite = sLineToWrite & "NOK"
                                                    Else
                                                        sLineToWrite = sLineToWrite & Trim(oPayment.MON_InvoiceCurrency)
                                                    End If
                                                End If

                                                sAmount = Replace(sAmount, ".", "")
                                                sLineToWrite = sLineToWrite & Space(5) & PadLeft(sAmount, 13, " ")
                                                'Sign
                                                'New 17.03.2004
                                                'Changed for GL-postings
                                                'New 13.09.2004 by Kjell. There are no longer any differences regarding debit/credit posting of the different transactions
                                                If oInvoice.MON_InvoiceAmount < 0 Then
                                                    sLineToWrite = sLineToWrite & " D " 'ex. creditnote, charges
                                                Else
                                                    sLineToWrite = sLineToWrite & " K " 'ex. Invoice
                                                End If

                                                'AppliesToDocNo - InvoiceNo
                                                sLineToWrite = sLineToWrite & PadRight(oInvoice.InvoiceNo, 18, " ")
                                                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.OpenInvoice Then
                                                    sLineToWrite = sLineToWrite & PadRight(oInvoice.MATCH_ID, 18, " ") & Space(30)
                                                Else
                                                    sLineToWrite = sLineToWrite & Space(48)
                                                End If
                                                'Customer text
                                                sFreeTextVariable = sFreetextFixed
                                                For Each oFreeText In oInvoice.Freetexts
                                                    If oFreeText.Qualifier > 2 Then
                                                        sFreeTextVariable = RTrim(oFreeText.Text) & " " & sFreeTextVariable
                                                    End If
                                                Next oFreeText
                                                sLineToWrite = sLineToWrite & PadRight(sFreeTextVariable, 50, " ")
                                                'Document type from Myfield
                                                'New 17.03.2004
                                                sLineToWrite = sLineToWrite & " " & PadRight(oInvoice.MyField, 15, " ")
                                                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.OpenInvoice Or oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL Or oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer Then
                                                    sLineToWrite = sLineToWrite & Space(25)
                                                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnSupplier Then
                                                    If Not EmptyString((oInvoice.InvoiceNo)) Then
                                                        'Matched against an invoice/creditnote, state a Lopenr. from Navision
                                                        If EmptyString((oInvoice.MATCH_ID)) Then
                                                            'Just to be sure, check if a match_ID is stated.
                                                            sLineToWrite = sLineToWrite & Space(25)
                                                        Else
                                                            sLineToWrite = sLineToWrite & PadRight(oInvoice.MATCH_ID, 25, " ")
                                                        End If
                                                    Else
                                                        'Posted directly against a supplier
                                                        sLineToWrite = sLineToWrite & Space(25)
                                                    End If
                                                Else
                                                    If EmptyString((oInvoice.MATCH_ID)) Then
                                                        sLineToWrite = sLineToWrite & Space(25)
                                                    Else
                                                        sLineToWrite = sLineToWrite & PadRight(oInvoice.MATCH_ID, 25, " ")
                                                    End If
                                                End If
                                                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.OpenInvoice Then
                                                    If EmptyString((oPayment.Unique_ERPID)) Then
                                                        sLineToWrite = sLineToWrite & Space(25)
                                                    Else
                                                        sLineToWrite = sLineToWrite & PadRight(oPayment.Unique_ERPID, 25, " ")
                                                    End If
                                                Else
                                                    sLineToWrite = sLineToWrite & Space(25)
                                                End If

                                                ' 05.03.2020
                                                ' add a field for payers accountno. Used by NAV to update customer bank account
                                                ' 14.09.2020 - this field gives Keystone problems
                                                If sSpecial <> "KEYSTONE" And sSpecial <> "TEKNOTHERM" Then
                                                    If oPayment.E_Account = "119999" Then
                                                        ' 09.03.2020 - do not update Diversekonto
                                                        sLineToWrite = sLineToWrite & Space(35)
                                                    Else
                                                        sLineToWrite = sLineToWrite & PadRight(oPayment.E_Account, 35, " ")
                                                    End If
                                                End If


                                                oFile.WriteLine((sLineToWrite))
                                            End If 'oInvoice.MATCH_Final = True
                                        Next oInvoice

                                    End If 'oPayment.MATCH_Matched = MatchStatus.NotMatched And oPayment.VB_WritePaymentAmountIfOpen Then
                                    oPayment.Exported = True
                                End If 'bExportoPayment
                    Next oPayment ' payment


                        End If


                    Next oBatch 'batch

                    'We have to save the last used sequenceno. in some cases
                    'First check if we use a profile
                    If oBatch.VB_ProfileInUse = True Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                            'Always use the seqno at filesetup level
                            Case 1
                                'Only for the first batch if we have one sequenceseries!
                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal
                                oBatch.VB_Profile.Status = 2
                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 1
                            Case 2
                                'oBatch.VB_Profile.Status = 2
                                'oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 2
                                'oBatch.VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).Status = 1
                                sVoucherNo = sAlphaPart & PadLeft(Trim(Str(nSequenceNoTotal)), nDigitsLen, "0")
                                oClient.VoucherNo = sVoucherNo
                                oClient.Status = Profile.CollectionStatus.Changed
                                oBatch.VB_Profile.Status = Profile.CollectionStatus.ChangeUnder
                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oBatch.VB_Profile.FileSetups(iFormat_ID).Status = Profile.CollectionStatus.ChangeUnder

                            Case Else
                                Err.Raise(12002, "WriteNavision_for_SI_Data", LRS(12002, "Navision", sFilenameOut))

                        End Select

                    End If

                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteNavision_for_SI_Data" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteNavision_for_SI_Data = True

    End Function
    Function WriteExcelCSV_SDV(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sClientNo As String, ByVal sSpecial As String) As Boolean
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim i As Double, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment, oInvoice As Invoice
        Dim oFreeText As FreeText
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim sLineToWrite As String
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sConvertedAccount As String
        Dim bRetrieveClientInfo As Boolean
        Dim iCounter As Integer = 0
        Dim sTmp As String = ""
        Dim bIncludeOCR As Boolean = False

        'Format for Odin
        'EGENREFERANSE;BELASTET_BELOP;BELASTET_DATO;STATUS;FEILKODE;FEILMELDING

        Try

            bRetrieveClientInfo = True

            If sSpecial = "ODIN_UTBETALING" Or sSpecial = "AEROGULF_RETURN" Then
                bRetrieveClientInfo = False
            End If

            ' lag en outputfil
            sOldAccountNo = vbNullString

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            If sSpecial = "ACTIVEBRANDS_REVISOR" Then
                ' Dato;Bel�p;Valuta;Fakturanr;Fritekst
                sLineToWrite = "Dato;Bel�p;Valuta;Fakturanr;Fritekst"
                oFile.WriteLine(sLineToWrite)
            End If

            If sSpecial = "SEBOCR" Then
                ' Engangsoppdrag for en SEB kunde
                ' Oversett fra OCR fil til Excel
                ' Ref Dag Jonhaugen, SEB
                ' "Nr.	";Ant ;Tk. Kd;Re. Kd;Dist. Kd;Nets-dato ;"Id-nr	";"Debet kto	";"Kredit kto	";"Bel�p	";KID REF
                sLineToWrite = "Nr.	;Ant ;Tk. Kd;Re. Kd;Dist. Kd;Nets-dato ;Id-nr	;Debet kto	;Kredit kto	;Bel�p	;KID REF"
                oFile.WriteLine(sLineToWrite)
            End If

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If Not bMultiFiles Then
                            bExportoBabel = True
                        Else
                            'If oPayment.I_Account = sI_Account Then
                            ' 01.10.2018 � changed above If to:
                            If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                If Len(oPayment.VB_ClientNo) > 0 Then
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        bExportoBabel = True
                                    End If
                                Else
                                    bExportoBabel = True
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            If bIncludeOCR Then
                                'OK
                            Else
                                If IsOCR(oPayment.PayCode) Then
                                    bExportoBabel = False
                                Else
                                    'OK
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    If sSpecial = "ODIN_UTBETALING" Then
                        If oBabel.StatusCode <> "01" And oBabel.StatusCode <> "02" Then
                            Err.Raise(5287, "WriteExcelCSV_SDV", "Feil p� filniv�, kan ikke lage en gyldig fil til Genus." & vbCrLf & "Feilkode: " & oBabel.StatusCode)
                        End If
                    End If

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False

                        If sSpecial = "SEBOCR" Then
                            sLineToWrite = oBatch.Index.ToString & ";"
                            If oBatch.Payments.Count > 10 Then
                                sLineToWrite = sLineToWrite & "[+10];"
                            Else
                                sLineToWrite = sLineToWrite & "[" & oBatch.Payments.Count.ToString & "];"
                            End If
                            sLineToWrite = sLineToWrite & "182;"
                            sLineToWrite = sLineToWrite & "0;"
                            sLineToWrite = sLineToWrite & " ;"
                            sTmp = oBatch.DATE_Production
                            sLineToWrite = sLineToWrite & Right(sTmp, 2) & "." & Mid(sTmp, 5, 2) & "." & Left(sTmp, 4) & ";"
                            sLineToWrite = sLineToWrite & oBatch.Payments(1).REF_Own & ";" ' ??? SentralID
                            sLineToWrite = sLineToWrite & " ;"
                            sLineToWrite = sLineToWrite & oBatch.Payments(1).I_Account & ";"
                            sLineToWrite = sLineToWrite & oBatch.MON_InvoiceAmount / 100 & ";"
                            sLineToWrite = sLineToWrite & ";"
                            oFile.WriteLine(sLineToWrite)

                        End If

                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                            If Not bMultiFiles Then
                                bExportoBatch = True
                            Else
                                'If oPayment.I_Account = sI_Account Then
                                ' 01.10.2018 � changed above If to:
                                If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBatch = True
                                        End If
                                    Else
                                        bExportoBatch = True
                                    End If
                                End If
                            End If
                            If bExportoBatch Then
                                If bIncludeOCR Then
                                    'OK
                                Else
                                    If IsOCR(oPayment.PayCode) Then
                                        bExportoBatch = False
                                    Else
                                        'OK
                                    End If
                                End If
                            End If
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            If sSpecial = "ODIN_UTBETALING" Then
                                If oBatch.StatusCode <> "01" And oBatch.StatusCode <> "02" Then
                                    Err.Raise(5287, "WriteExcelCSV_SDV", "Feil p� batchniv�, kan ikke lage en gyldig fil til Genus." & vbCrLf & "Feilkode: " & oBatch.StatusCode)
                                End If
                            End If

                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If Not bMultiFiles Then
                                    bExportoPayment = True
                                Else
                                    'If oPayment.I_Account = sI_Account Then
                                    ' 01.10.2018 � changed above If to:
                                    If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoPayment = True
                                            End If
                                        Else
                                            bExportoPayment = True
                                        End If
                                        If bExportoPayment Then
                                            If bIncludeOCR Then
                                                'OK
                                            Else
                                                If IsOCR(oPayment.PayCode) Then
                                                    bExportoPayment = False
                                                Else
                                                    'OK
                                                End If
                                            End If
                                        End If

                                    End If
                                End If

                                If bExportoPayment Then

                                    'Find the client if needed
                                    If bRetrieveClientInfo Then
                                        If oPayment.I_Account <> sOldAccountNo Then
                                            If oPayment.VB_ProfileInUse Then
                                                sI_Account = oPayment.I_Account
                                                bAccountFound = False
                                                For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                    For Each oClient In oFilesetup.Clients
                                                        For Each oaccount In oClient.Accounts
                                                            If sI_Account = oaccount.Account Then
                                                                sConvertedAccount = Trim$(oaccount.ConvertedAccount)
                                                                sOldAccountNo = oPayment.I_Account
                                                                bAccountFound = True
                                                                Exit For
                                                            End If
                                                        Next oaccount
                                                        If bAccountFound Then Exit For
                                                    Next oClient
                                                    If bAccountFound Then Exit For
                                                Next oFilesetup
                                            End If
                                        End If

                                        If Not bAccountFound Then
                                            If sSpecial <> "HAFSLUND_REVISOR" And sSpecial <> "SEBOCR" Then
                                                Err.Raise(12003, "WriteExcelCSV_SDV", LRS(12003))
                                                '12003: Could not find account %1.Please enter the account in setup."
                                            End If
                                        End If
                                    End If 'If bRetrieveClientInfo Then

                                    For Each oInvoice In oPayment.Invoices
                                        Select Case sSpecial

                                            Case "ODIN_UTBETALING"
                                                'EGENREFERANSE;BELASTET_BELOP;BELASTET_DATO;STATUS;FEILKODE;FEILMELDING
                                                sLineToWrite = ""
                                                'XNET - 31.10.2011 - Changed next line
                                                sLineToWrite = xDelim(Trim(oInvoice.REF_Own), "-", 1) & ";"
                                                sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_TransferredAmount, "", ",") & ";"
                                                If oPayment.DATE_Value = "19900101" Then
                                                    sLineToWrite = sLineToWrite & ";"
                                                Else
                                                    sLineToWrite = sLineToWrite & oPayment.DATE_Value & ";"
                                                End If
                                                If oBabel.StatusCode = "01" Then
                                                    sLineToWrite = sLineToWrite & "M" & ";"
                                                    If oPayment.StatusCode = "01" And oInvoice.StatusCode = "01" Then
                                                        sLineToWrite = sLineToWrite & ";"
                                                    Else
                                                        If oInvoice.StatusCode <> "01" Then
                                                            sLineToWrite = sLineToWrite & oInvoice.StatusCode & ";'" & TelepayStatusTxts(oInvoice.StatusCode) & "'"
                                                        Else
                                                            sLineToWrite = sLineToWrite & oPayment.StatusCode & ";'" & TelepayStatusTxts(oPayment.StatusCode) & "'"
                                                        End If
                                                    End If
                                                ElseIf oBabel.StatusCode = "02" Then
                                                    sLineToWrite = sLineToWrite & "A" & ";"
                                                    If oPayment.StatusCode = "02" And oInvoice.StatusCode = "02" Then
                                                        sLineToWrite = sLineToWrite & ";"
                                                    Else
                                                        If oInvoice.StatusCode <> "02" Then
                                                            sLineToWrite = sLineToWrite & oInvoice.StatusCode & ";'" & TelepayStatusTxts(oInvoice.StatusCode) & "'"
                                                        Else
                                                            sLineToWrite = sLineToWrite & oPayment.StatusCode & ";'" & TelepayStatusTxts(oPayment.StatusCode) & "'"
                                                        End If
                                                    End If
                                                End If
                                                oFile.WriteLine(sLineToWrite)

                                            Case "AEROGULF_RETURN"
                                                'TELLER;EGENREFERANSE;LEVERAND�RNAVN;BELASTET_BELOP;VALUTA;BELASTET_DATO;STATUS;FEILKODE;FEILMELDING
                                                iCounter = iCounter + 1
                                                sLineToWrite = ""
                                                sLineToWrite = Trim(oInvoice.REF_Own) & ";"
                                                sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_TransferredAmount, "", ",") & ";"
                                                If oPayment.DATE_Value = "19900101" Then
                                                    sLineToWrite = sLineToWrite & oPayment.DATE_Payment & ";"
                                                Else
                                                    sLineToWrite = sLineToWrite & oPayment.DATE_Value & ";"
                                                End If
                                                If oBabel.StatusCode = "01" Then
                                                    'sLineToWrite = sLineToWrite & "Received by bank" & ";"
                                                    If oPayment.StatusCode = "01" And oInvoice.StatusCode = "01" Then
                                                        sLineToWrite = sLineToWrite & "Received by bank" & ";;"
                                                    Else
                                                        If oInvoice.StatusCode <> "01" Then
                                                            sLineToWrite = sLineToWrite & "Cancelled" & ";" & oInvoice.StatusCode & ";" & TelepayStatusTxts(oInvoice.StatusCode)
                                                        Else
                                                            sLineToWrite = sLineToWrite & "Cancelled" & ";" & oPayment.StatusCode & ";" & TelepayStatusTxts(oPayment.StatusCode)
                                                        End If
                                                    End If
                                                ElseIf oBabel.StatusCode = "02" Then
                                                    'sLineToWrite = sLineToWrite & "Processed" & ";"
                                                    If oPayment.StatusCode = "02" And oInvoice.StatusCode = "02" Then
                                                        sLineToWrite = sLineToWrite & "Processed" & ";;"
                                                    Else
                                                        If oInvoice.StatusCode <> "02" Then
                                                            sLineToWrite = sLineToWrite & "Cancelled" & ";" & oInvoice.StatusCode & ";" & TelepayStatusTxts(oInvoice.StatusCode)
                                                        Else
                                                            sLineToWrite = sLineToWrite & "Cancelled" & ";" & oPayment.StatusCode & ";" & TelepayStatusTxts(oPayment.StatusCode)
                                                        End If
                                                    End If
                                                End If

                                                oFile.WriteLine(sLineToWrite)


                                            Case "DNBCREMULTEST"
                                                iCounter = iCounter + 1
                                                sLineToWrite = ""
                                                sLineToWrite = iCounter.ToString & ";"
                                                sLineToWrite = sLineToWrite & Trim(oInvoice.REF_Own) & ";"
                                                sLineToWrite = sLineToWrite & Trim(oPayment.E_Name) & ";"
                                                sLineToWrite = sLineToWrite & Trim(oPayment.REF_Bank1) & ";"
                                                sLineToWrite = sLineToWrite & Trim(oPayment.REF_Bank2) & ";"
                                                sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_TransferredAmount, "", ",") & ";"
                                                sLineToWrite = sLineToWrite & Trim(oPayment.MON_InvoiceCurrency) & ";"
                                                If oPayment.DATE_Value = "19900101" Then
                                                    sLineToWrite = sLineToWrite & oPayment.DATE_Payment & ";"
                                                Else
                                                    sLineToWrite = sLineToWrite & oPayment.DATE_Value & ";"
                                                End If

                                                For Each oFreeText In oInvoice.Freetexts
                                                    sLineToWrite = sLineToWrite & oFreeText.Text & ";"
                                                Next
                                                oFile.WriteLine(sLineToWrite)

                                            Case "ACTIVEBRANDS_REVISOR"
                                                ' Dato;Bel�p;Valuta;Fakturanr;Fritekst
                                                iCounter = iCounter + 1
                                                sLineToWrite = ""
                                                If oPayment.DATE_Value = "19900101" Then
                                                    sLineToWrite = sLineToWrite & oPayment.DATE_Payment & ";"
                                                Else
                                                    sLineToWrite = sLineToWrite & oPayment.DATE_Value & ";"
                                                End If
                                                sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_TransferredAmount, "", ",") & ";"
                                                sLineToWrite = sLineToWrite & Trim(oPayment.MON_InvoiceCurrency) & ";"
                                                If Not EmptyString(oInvoice.Unique_Id) Then
                                                    sLineToWrite = sLineToWrite & oInvoice.Unique_Id & ";"
                                                ElseIf Not EmptyString(oInvoice.InvoiceNo) Then
                                                    sLineToWrite = sLineToWrite & oInvoice.InvoiceNo & ";"
                                                Else
                                                    sLineToWrite = sLineToWrite & ";"
                                                End If
                                                For Each oFreeText In oInvoice.Freetexts
                                                    sLineToWrite = sLineToWrite & oFreeText.Text
                                                Next
                                                oFile.WriteLine(sLineToWrite)

                                            Case "HAFSLUND_REVISOR"
                                                ' Betalingsdato; bel�p; KID 
                                                iCounter = iCounter + 1
                                                sLineToWrite = ""
                                                If oPayment.DATE_Value = "19900101" Then
                                                    sLineToWrite = sLineToWrite & oPayment.DATE_Payment & ";"
                                                Else
                                                    sLineToWrite = sLineToWrite & oPayment.DATE_Value & ";"
                                                End If
                                                sLineToWrite = sLineToWrite & ConvertFromAmountToString(oInvoice.MON_TransferredAmount, "", ",") & ";"
                                                sLineToWrite = sLineToWrite & Trim(oPayment.MON_InvoiceCurrency) & ";"
                                                If Not EmptyString(oInvoice.Unique_Id) Then
                                                    sLineToWrite = sLineToWrite & oInvoice.Unique_Id & ";"
                                                ElseIf Not EmptyString(oInvoice.InvoiceNo) Then
                                                    sLineToWrite = sLineToWrite & oInvoice.InvoiceNo & ";"
                                                Else
                                                    sLineToWrite = sLineToWrite & ";"
                                                End If
                                                oFile.WriteLine(sLineToWrite)

                                            Case "SEBOCR"
                                                sLineToWrite = oBatch.Index.ToString & ";"
                                                sLineToWrite = sLineToWrite & ">>;"
                                                sLineToWrite = sLineToWrite & "371;"
                                                sLineToWrite = sLineToWrite & "0;"
                                                sLineToWrite = sLineToWrite & " ;"
                                                sTmp = oPayment.DATE_Payment
                                                sLineToWrite = sLineToWrite & Right(sTmp, 2) & "." & Mid(sTmp, 5, 2) & "." & Left(sTmp, 4) & ";" ' FORMAT ???
                                                If EmptyString(Replace(oPayment.REF_Bank2, "0", "")) Then
                                                    sLineToWrite = sLineToWrite & oPayment.REF_Bank1 & ";"  ' ??? BlankettNr ???
                                                Else
                                                    sLineToWrite = sLineToWrite & oPayment.REF_Bank2 & ";"  ' ??? BlankettNr ???
                                                End If
                                                sLineToWrite = sLineToWrite & oPayment.E_Account & ";"
                                                sLineToWrite = sLineToWrite & oPayment.I_Account & ";"
                                                sLineToWrite = sLineToWrite & (oPayment.MON_InvoiceAmount / 100).ToString & ";" ' format
                                                sLineToWrite = sLineToWrite & oPayment.Invoices(1).Unique_Id
                                                oFile.WriteLine(sLineToWrite)
                                        End Select

                                    Next oInvoice

                                    oPayment.Exported = True

                                End If
                            Next ' payment
                        End If ' bExportoBatch Then
                    Next 'batch

                End If

            Next

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteExcelCSV_SDV" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteExcelCSV_SDV = True

    End Function

End Module
