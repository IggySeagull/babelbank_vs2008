Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmBackupView
	Inherits System.Windows.Forms.Form
	Public ifrmReturn As Short
	Public iRow As Short
    Dim aFiles(,) As Object
    Private sMode As String = ""  ' DATABASE or nothing
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		ifrmReturn = 3
		Me.Hide()
	End Sub
	Public Sub SendFilesArray(ByRef aF As Object)
        aFiles = aF
		iRow = 1
	End Sub
	Private Sub cmdCopy_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCopy.Click
        ifrmReturn = 1
        Me.Hide()
	End Sub
    Public Sub SetMode(ByVal sM As String)
        sMode = sM  ' DATABASE or nothing
    End Sub
	
	Private Sub cmdView_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdView.Click
        'TODO - RAPPORT

        Dim oBabelReport As vbBabel.BabelReport
        Dim oBabelFiles As vbBabel.BabelFiles
        Dim oBabel As BabelFile
		Dim iImport As Short
		Dim oFs As Scripting.FileSystemObject
		Dim oFile As Scripting.TextStream
		Dim iLines As Short
		Dim sText As String
		Dim bNothingToDo As Boolean
        Dim iElementNo As Short
        Dim frmTextView As frmTextView
        Dim oView As vbBabel.bbViewer

        'iElementNo = iRow '- 1
        ' Find elementNo in col 7 (will work also if we sort!)
        iElementNo = Val(Me.gridFiles.Rows(iRow).Cells(7).Value)
		
		
		' If unknown fileformat, open in "textview"
        If aFiles(5, iElementNo) < 1 Then
            oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
            oFile = oFs.OpenTextFile(aFiles(7, iElementNo))
            iLines = 0
            Do While oFile.AtEndOfStream <> True
                iLines = iLines + 1
                sText = sText & oFile.ReadLine & vbCrLf
                ' Read max 1000 lines!
                If iLines > 1000 Then
                    Exit Do
                End If
            Loop

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If
            ' Show in form;

            'TODO - RAPPORT
            frmTextView = New frmTextView
            If Len(aFiles(7, iElementNo)) > 50 Then
                frmTextView.Text = "..." & VB.Right(aFiles(7, iElementNo), 50)
            Else
                frmTextView.Text = aFiles(7, iElementNo)
            End If
            frmTextView.txtFile.Text = sText

            frmTextView.ShowDialog()


        Else
            oBabelFiles = New vbBabel.BabelFiles
            oBabel = oBabelFiles.Add()
            oBabel.ImportFormat = aFiles(5, iElementNo)
            oBabel.FilenameIn = aFiles(7, iElementNo)
            oBabel.REPORT_Print = BabelFiles.PrintType.NoPrint
            oBabel.REPORT_Screen = BabelFiles.PrintType.PrintBatches
            '        oBabel.VB_FilenameInNo = aImportInformation(5, iArrayCounter)
            '        oBabel.Version = aImportInformation(7, iArrayCounter)
            '        oBabel.Bank = aImportInformation(9, iArrayCounter)
            'No use of profile
            iImport = oBabel.Import()

            ' Use new bbViewer class

            oView = New vbBabel.bbViewer
            oView.BabelFiles = oBabelFiles
            oView.Show()
        End If

    End Sub
	
	Private Sub frmBackupView_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		FormvbStyle(Me, "dollar.jpg")
		FormLRSCaptions(Me)
	End Sub
	
	Private Sub frmBackupView_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
		
        If ifrmReturn < 1 Then
            'User has closed the form without clicking on a button
            ifrmReturn = 3
        End If
		
	End Sub
    Private Sub gridFiles_RowEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridFiles.RowEnter
        ' Enter this row, 

        Try

            With gridFiles
                iRow = e.RowIndex
            End With

        Catch
            Debug.WriteLine("Error in RowEnter" & Err.Description)
            If MsgBox("Error in RowEnter - " & Err.Description, MsgBoxStyle.YesNo) = vbYes Then
                ' Cancel
            End If
        End Try

    End Sub

    Private Sub cmdMail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMail.Click
        ifrmReturn = 4
        Me.Hide()

    End Sub
End Class
