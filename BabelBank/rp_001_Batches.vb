Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document
'Imports Microsoft.Win32

Public Class rp_001_Batches
    Dim sOldI_Account As String
    Dim sI_Account As String
    Dim sClientInfo As String
    Dim iBabelFile_ID As Integer
    Dim iBatch_ID As Integer
    Dim sBreakField As String

    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean
    Dim bShowFilename As Boolean

    Dim oBabelFiles As vbBabel.BabelFiles
    Dim oBabelFile As vbBabel.BabelFile
    Dim oBatch As vbBabel.Batch
    Dim oPayment As vbBabel.Payment
    Dim oInvoice As vbBabel.Invoice
    Dim bFirstTime As Boolean

    Dim iDetailLevel As Integer
    Dim sReportName As String
    Dim sSpecial As String
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim sClientNumber As String
    Dim bReportOnSelectedItems As Boolean
    Dim bIncludeOCR As Boolean
    Dim bSQLServer As Boolean
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean


    Private Sub rp_001_Batches_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        '        Fields.Add("CategoryID")
        '        Fields.Add("CategoryName")
        Fields.Add("BreakField")
        If Not bReportFromDatabase Then
            Fields.Add("Grouping")
            Fields.Add("Company_ID")
            Fields.Add("FilenameIn")
            Fields.Add("I_Branch")
            Fields.Add("SequenceNo")
            Fields.Add("Babelfile_ID")
            Fields.Add("Batch_ID")
            Fields.Add("DATE_Production")
            Fields.Add("MON_TransferredAmount")
            Fields.Add("MON_TransferCurrency")
            Fields.Add("REF_Bank")
            Fields.Add("I_Account")
            Fields.Add("FormatType")
            'Fields.Add("DATE_Value")
            Fields.Add("DATE_Payment")
            Fields.Add("NoOfPayments")
            Fields.Add("Client")
        End If

    End Sub

    Private Sub rp_001_Batches_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        If Not bReportFromDatabase Then
            Me.Cancel()
        End If
    End Sub

    Private Sub rp_001_Batches_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        iBabelFile_ID = -1
        iBatch_ID = -1
        bShowBatchfooterTotals = True
        sBreakField = ""
        bFirstTime = True

        Select Case iBreakLevel
            Case 0 'NOBREAK
                bShowBatchfooterTotals = False
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = False

            Case 1 'BREAK_ON_ACCOUNT (and per day)
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 2 'BREAK_ON_BATCH
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 3 'BREAK_ON_PAYMENT
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 4 'BREAK_ON_ACCOUNTONLY
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 5 'BREAK_ON_CLIENT 'New
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = False
                bShowFilename = False

            Case 6 'BREAK_ON_FILE 'Added 09.09.2014
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = True

            Case 999
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = True

            Case Else 'NOBREAK
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False

        End Select

        'Long or ShortDate
        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtDATE_Production.OutputFormat = "D"
            Me.txtDetailDATE_Production.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtDATE_Production.OutputFormat = "d"
            Me.txtDetailDATE_Production.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = LRS(60118) '"Rapport delsummer" 
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4.2
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'End If

        'grBatchHeader
        'Me.lblFilename.Text =  LRS(40001) '"Filnavn:"
        Me.lblDate_Production.Text = LRS(40027) '"Prod.dato:" 
        Me.lblClient.Text = LRS(40030) '"Klient:" 
        Me.lblI_Account.Text = LRS(40138) '"Own account:" 'LRS(40020) '"Mottakerkonto:" 

        'grBatchFooter
        Me.lblBatchfooterAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblBatchfooterNoofPayments.Text = LRS(40105) '"Deltotal - Antall:" 

        'grBabelFileFooter
        Me.lblTotalAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblTotalNoOfPayments.Text = LRS(40106) '"Total - Antall:" 

        ' Show details or not
        If Not bShowBatchfooterTotals Then
            Me.grBatchFooter.Visible = False
        End If
        If Not bShowDATE_Production Then
            Me.txtDATE_Production.Visible = False
        End If
        If Not bShowClientName Then
            Me.txtClientName.Visible = False
        End If
        If Not bShowI_Account Then
            Me.txtI_Account.Visible = False
        End If
        If Not bShowFilename Then
            Me.txtFilename.Visible = False
        End If

        If iDetailLevel = 0 Then
            ' no details
            Me.txtI_Branch.Visible = False
            Me.txtFilenameIn2.Visible = False
            Me.txtSequenceNo.Visible = False

            Me.txtNoOfPayments.Top = 0
            Me.txtDetailDATE_Production.Top = 0
            Me.txtMON_TransferredAmount.Top = 0
            Me.txtREF_Bank.Top = 0
            Me.txtMON_TransferCurrency.Top = 0

            ' Resize batch-section;
            Me.grBatchHeader.Height = 0.2
        End If

    End Sub

    Private Sub rp_001_Batches_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        Dim sDATE_Production As String
        Dim sDATE_Payment As String
        Dim bContinue As Boolean
        Dim bFoundClient As Boolean

        ' Counters for items in the different Babel collections
        Static iBabelFiles As Integer
        Static iLastUsedBabelFilesItem As Integer
        Static iBatches As Integer
        ' Counters for last item in use in the different Babel collections
        ' This way we do not need to set the different objects each time,
        ' just when an item-value has changed
        Static xCreditAccount As String
        Static xDate As String

        If Not bReportFromDatabase Then

            If bFirstTime Then
                bFirstTime = False
                iBabelFiles = 1
                iBatches = 1
            End If

            ' Spin through collections to find next suitbable record before any other
            ' proecessing is done
            ' Position to correct items in collections
            Do Until iBabelFiles > oBabelFiles.Count
                ' Try to set as few times as possible
                If oBabelFile Is Nothing Then
                    oBabelFile = oBabelFiles(iBabelFiles)
                    iLastUsedBabelFilesItem = iBabelFiles
                Else
                    'If iBabelFiles <> oBabelFile.Index Then
                    If iLastUsedBabelFilesItem <> iBabelFiles Then
                        iBatches = 1
                        ' Added 19.05.2004
                        xCreditAccount = "xxxx"
                        xDate = "x"

                        oBabelFile = oBabelFiles(iBabelFiles)
                        iLastUsedBabelFilesItem = iBabelFiles  'oBabelFile.Index
                    End If
                End If

                Do Until iBatches > oBabelFile.Batches.Count
                    oBatch = oBabelFile.Batches(iBatches)

                    bContinue = False
                    If Not EmptyString(sClientNumber) Then
                        If oBatch.Payments.Count > 0 Then
                            If oBatch.Payments.Item(1).VB_ClientNo = sClientNumber Then
                                bContinue = True
                            End If
                        Else
                            bContinue = False
                        End If
                    Else
                        bContinue = True
                    End If

                    iBatches = iBatches + 1

                    If bContinue Then
                        Exit Do
                    End If

                Loop ' iBatches
                If bContinue Then
                    Exit Do
                End If
                iBabelFiles = iBabelFiles + 1
                iBatches = 1
            Loop ' iBabelFiles

            If iBabelFiles > oBabelFiles.Count Then
                eArgs.EOF = True
                iBabelFiles = 0 ' to reset statics next time !

                Exit Sub
            End If

            If oBabelFile.VB_ProfileInUse Then
                Me.Fields("Grouping").Value = Trim(Str(oBabelFile.VB_Profile.Company_ID)) & "-" & Trim(Str(oBabelFile.Index)) & "-" & Trim(Str(oBatch.Index))
                Me.Fields("Company_ID").Value = oBabelFile.VB_Profile.Company_ID
            Else
                Me.Fields("Grouping").Value = "1-" & Trim(Str(oBabelFile.Index)) & "-" & Trim(Str(oBatch.Index))
                Me.Fields("Company_ID").Value = "1"
            End If
            Me.Fields("FilenameIn").Value = oBabelFile.FilenameIn

            '23.01.2020 - Added next IF
            If Not EmptyString(sClientNumber) Then
                If oBatch.Payments.Count > 0 Then
                    Me.Fields("Client").Value = sClientNumber
                End If
            End If

            Me.Fields("I_Branch").Value = oBatch.I_Branch
            Me.Fields("SequenceNo").Value = Trim(Str(oBatch.SequenceNoStart)) & "-" & Trim(Str(oBatch.SequenceNoEnd))
            Me.Fields("Babelfile_ID").Value = oBabelFile.Index
            Me.Fields("Batch_ID").Value = oBatch.Index
            If sSpecial = "GULFMARK" Then
                ' 21.11.2016 - Gulfmark will always have date as DD/MM/YYYY
                ' make date DD/MM/YYYY
                Me.Fields("DATE_Production").Value = Format(oBatch.DATE_Production, "dd/MM/yyyy")
            Else
                Me.Fields("DATE_Production").Value = oBatch.DATE_Production
            End If
            If oBatch.MON_TransferredAmount = 0 Then
                If oBatch.Payments.Count = 0 Then  ' added If 31.08.2016
                    Me.Fields("MON_TransferredAmount").Value = 0
                    Me.Fields("MON_TransferCurrency").Value = ""
                Else
                    If oBatch.MON_InvoiceAmount = 0 Then
                        Me.Fields("MON_TransferredAmount").Value = 0
                        Me.Fields("MON_TransferCurrency").Value = oBatch.Payments.Item(1).MON_InvoiceCurrency
                    Else
                        Me.Fields("MON_TransferredAmount").Value = oBatch.MON_InvoiceAmount / 100
                        Me.Fields("MON_TransferCurrency").Value = oBatch.Payments.Item(1).MON_InvoiceCurrency
                    End If
                End If

            Else
                Me.Fields("MON_TransferredAmount").Value = oBatch.MON_TransferredAmount / 100
                Me.Fields("MON_TransferCurrency").Value = oBatch.Payments.Item(1).MON_TransferCurrency
            End If
            Me.Fields("REF_Bank").Value = oBatch.REF_Bank
            If oBatch.Payments.Count = 0 Then  ' added If 31.08.2016
                Me.Fields("I_Account").Value = ""
            Else
                Me.Fields("I_Account").Value = oBatch.Payments.Item(1).I_Account
            End If
            Me.Fields("FormatType").Value = oBatch.FormatType
            'Me.Fields("DATE_Value").Value = oBatch.Payments.Item(1).DATE_Value
            If oBatch.Payments.Count = 0 Then  ' added If 31.08.2016
                Me.Fields("DATE_Payment").Value = ""
            Else
                Me.Fields("DATE_Payment").Value = oBatch.Payments.Item(1).DATE_Payment
            End If
            If sSpecial = "NHST_LONN" Then  ' added 03.05.2022
                Me.Fields("NoOfPayments").Value = oBatch.MON_BalanceIN
            Else
                Me.Fields("NoOfPayments").Value = oBatch.Payments.Count
            End If
            eArgs.EOF = False

        End If

        If Not eArgs.EOF Then

            Select Case iBreakLevel
                Case 0 'NOBREAK
                    Me.Fields("BreakField").Value = ""
                    'Me.lblFilename.Visible = False
                    'Me.txtFilename.Visible = False

                Case 1 'BREAK_ON_ACCOUNT (and per day)
                    'Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Production").Value
                    ' 03.02.2016 changed to DATE_Payment
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Payment").Value

                Case 2 'BREAK_ON_BATCH
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value))

                Case 3 'BREAK_ON_PAYMENT
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value))
                    'Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value)) & "-" & Trim$(Str(Me.Fields("Payment_ID").Value))

                Case 4 'BREAK_ON_ACCOUNTONLY
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value

                Case 5 'BREAK_ON_CLIENT 'New
                    Me.Fields("BreakField").Value = Me.Fields("Client").Value

                Case 6 'BREAK_ON_FILE 'Added 09.09.2014
                    Me.Fields("BreakField").Value = Me.Fields("BabelFile_ID").Value.ToString

                Case 999 'Added 06.01.2015 - Use the breaklevel from the special SQL
                    'The break is set in the SQL

                Case Else
                    Me.Fields("BreakField").Value = ""

            End Select

            'START WORKING HERE
            If Not IsDBNull(Me.Fields("I_Account").Value) Then
                sI_Account = Me.Fields("I_Account").Value
            Else
                sI_Account = ""
            End If
            If sI_Account <> sOldI_Account And Not EmptyString(sI_Account) Then
                sClientInfo = FindClientName(sI_Account)
                sOldI_Account = sI_Account
            End If

            Me.txtClientName.Value = sClientInfo

            sDATE_Production = Me.Fields("DATE_Production").Value
            Me.txtDATE_Production.Value = DateSerial(CInt(Left$(sDATE_Production, 4)), CInt(Mid$(sDATE_Production, 5, 2)), CInt(Right$(sDATE_Production, 2)))

            sDATE_Payment = Me.Fields("DATE_Payment").Value
            If Not EmptyString(sDATE_Payment) Then  ' added If 31.08.2016
                Me.txtDetailDATE_Production.Value = DateSerial(CInt(Left$(sDATE_Payment, 4)), CInt(Mid$(sDATE_Payment, 5, 2)), CInt(Right$(sDATE_Payment, 2)))
            Else
                Me.txtDetailDATE_Production.Value = ""
            End If
        End If

    End Sub
    Private Sub Detail_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail.Format

        Me.txtNoOfPayments.Text = LRS(40014) & Me.txtNoOfPayments.Text '"Antall betalinger: "

        If Me.txtSequenceNo.Visible = True Then
            If Not Me.txtHlpFormatType.Text = "Telepay" Then
                Me.txtHlpFormatType.Text = False
            End If
        End If

    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oBabelFiles = Value
        End Set
    End Property
    Public WriteOnly Property ClientNumber() As String
        Set(ByVal Value As String)
            sClientNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportOnSelectedItems() As Boolean
        Set(ByVal Value As Boolean)
            bReportOnSelectedItems = Value
        End Set
    End Property
    Public WriteOnly Property IncludeOCR() As Boolean
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub grBatchHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grBatchHeader.Format
        Dim pLocation As System.Drawing.PointF
        Dim pSize As System.Drawing.SizeF

        If Me.txtFilename.Visible Then
            pLocation.X = 1.5
            pLocation.Y = 0.3
            Me.txtFilename.Location = pLocation
            pSize.Height = 0.19
            pSize.Width = 4.5
            Me.txtFilename.Size = pSize
            pLocation.X = 0.125
            pLocation.Y = 0.3
            Me.lblI_Account.Location = pLocation
            Me.lblI_Account.Value = LRS(20007)
            Me.txtI_Account.Visible = False
        End If


    End Sub
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class
