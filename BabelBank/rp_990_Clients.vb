Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_990_Clients
    Dim sOldI_Account As String
    Dim sI_Account As String
    Dim sClientInfo As String
    Dim iBabelFile_ID As Integer
    Dim iBatch_ID As Integer
    Dim sBreakField As String

    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean

    Dim oBabelFiles As vbBabel.BabelFiles
    Dim oBabelFile As vbBabel.BabelFile
    Dim oBatch As vbBabel.Batch
    Dim oPayment As vbBabel.Payment
    Dim oInvoice As vbBabel.Invoice
    Dim bFirstTime As Boolean

    Dim iDetailLevel As Integer
    Dim sReportName As String
    Dim sSpecial As String
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim sClientNumber As String
    Dim bReportOnSelectedItems As Boolean
    Dim bIncludeOCR As Boolean
    Dim bSQLServer As Boolean = False
    Dim aReportArray(,) As String
    Dim iReportEntries As Integer
    Dim iReportCounter As Integer
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean


    Private Sub rp_001_Batches_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        Dim aNumberArray(,) As Double
        Dim iCounter As Integer
        Dim bAdd As Boolean
        Dim sClientNo As String
        Dim bClientFound As Boolean
        Dim bClientNameFound As Boolean
        Dim bFirstEntry As Boolean
        Dim oFileSetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client

        bFirstEntry = True

        Fields.Add("BreakField")
        ' Can't use "Report on saved data" from setup
        If Not bReportFromDatabase Then
            Fields.Add("ClientNo")
            Fields.Add("ClientName")
            Fields.Add("NoOfRecords")
            Fields.Add("TotalAmount")

            'Create an array consisting of the necessary values
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        bAdd = False
                        If bIncludeOCR Then
                            bAdd = True
                        Else
                            If IsOCR(oPayment.PayCode) Then
                                bAdd = False
                            Else
                                bAdd = True
                            End If
                        End If
                        If bAdd Then
                            'Check if client is already stated
                            sClientNo = oPayment.VB_ClientNo
                            bClientFound = False
                            If bFirstEntry Then
                                bClientFound = False
                            Else
                                For iCounter = 0 To UBound(aReportArray, 2)
                                    'If aReportArray(iCounter, 0) = sClientNo Then
                                    If aReportArray(0, iCounter) = sClientNo Then
                                        bClientFound = True
                                        Exit For
                                    End If
                                Next iCounter
                            End If
                            If Not bClientFound Then
                                If bFirstEntry Then
                                    bFirstEntry = False
                                    iCounter = 0
                                Else
                                    iCounter = UBound(aReportArray, 2) + 1
                                End If
                                ReDim Preserve aReportArray(3, iCounter)
                                aReportArray(0, iCounter) = oPayment.VB_ClientNo
                                bClientNameFound = False
                                If oPayment.VB_ProfileInUse Then
                                    For Each oFileSetup In oPayment.VB_Profile.FileSetups
                                        'The best way is to check the FileSetup_ID but we do not have the value so if we find the right client number, use it!
                                        For Each oClient In oFileSetup.Clients
                                            If oClient.ClientNo = sClientNo Then
                                                aReportArray(1, iCounter) = oClient.Name
                                                bClientNameFound = True
                                                Exit For
                                            End If
                                        Next oClient
                                        If bClientNameFound Then
                                            Exit For
                                        End If
                                    Next oFileSetup
                                End If
                                ReDim Preserve aNumberArray(1, iCounter)
                            End If
                            aNumberArray(0, iCounter) = aNumberArray(0, iCounter) + 1
                            aNumberArray(1, iCounter) = aNumberArray(1, iCounter) + oPayment.MON_InvoiceAmount
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile
            If Not aReportArray Is Nothing Then
                For iCounter = 0 To UBound(aReportArray, 2)
                    aReportArray(2, iCounter) = RemoveLeadingCharacters(ConvertFromAmountToString(aNumberArray(0, iCounter), "", ""), "0")
                    aReportArray(3, iCounter) = ConvertFromAmountToString(aNumberArray(1, iCounter), "", "")
                Next
                iReportEntries = UBound(aReportArray, 2)
            Else
                Me.Cancel()
            End If
        End If


    End Sub

    Private Sub rp_001_Batches_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        If Not bReportFromDatabase Then
            Me.Cancel()
        End If
    End Sub

    Private Sub rp_001_Batches_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        bFirstTime = True
        iReportEntries = 0

        'Long or ShortDate
        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = LRS(60201) 'Clients 
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4.2
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'End If

        'grHeader
        Me.lblClientNo.Text = Replace(LRS(60202), ":", "") 'Klientnr
        Me.lblClientName.Text = LRS(60107) 'Klient 
        Me.lblNoOfRecords.Text = LRS(60059) 'Antall 
        Me.lblTotalAmount.Text = LRS(60060) 'Bel�p 

        'grFooter

    End Sub

    Private Sub rp_001_Batches_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        If Not bReportFromDatabase Then

            If bFirstTime Then
                bFirstTime = False
                iReportCounter = 0
            Else
                iReportCounter = iReportCounter + 1
                If iReportCounter > iReportEntries Then
                    eArgs.EOF = True

                    Exit Sub
                End If
            End If

            Me.Fields("ClientNo").Value = aReportArray(0, iReportCounter)
            Me.Fields("ClientName").Value = aReportArray(1, iReportCounter)
            Me.Fields("NoOfRecords").Value = aReportArray(2, iReportCounter)
            Me.Fields("TotalAmount").Value = Val(aReportArray(3, iReportCounter)) / 100

            eArgs.EOF = False

        End If

        If Not eArgs.EOF Then

            Select Case iBreakLevel
                Case 0 'NOBREAK
                    Me.Fields("BreakField").Value = ""

            End Select

            'START WORKING HERE

        End If

    End Sub
    Private Sub Detail_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail.Format

    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oBabelFiles = Value
        End Set
    End Property
    Public WriteOnly Property ClientNumber() As String
        Set(ByVal Value As String)
            sClientNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportOnSelectedItems() As Boolean
        Set(ByVal Value As Boolean)
            bReportOnSelectedItems = Value
        End Set
    End Property
    Public WriteOnly Property IncludeOCR() As Boolean
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class
