﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class arBBView
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ARView = New DataDynamics.ActiveReports.Viewer.Viewer
        Me.PdfExport = New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog
        Me.dlgFileNameSave = New System.Windows.Forms.SaveFileDialog
        Me.SuspendLayout()
        '
        'ARView
        '
        Me.ARView.BackColor = System.Drawing.SystemColors.Control
        Me.ARView.Document = New DataDynamics.ActiveReports.Document.Document("ARNet Document")
        Me.ARView.Location = New System.Drawing.Point(-1, 0)
        Me.ARView.Name = "ARView"
        Me.ARView.ReportViewer.CurrentPage = 0
        Me.ARView.ReportViewer.DisplayUnits = DataDynamics.ActiveReports.Viewer.DisplayUnits.Metric
        Me.ARView.ReportViewer.MultiplePageCols = 3
        Me.ARView.ReportViewer.MultiplePageRows = 2
        Me.ARView.ReportViewer.ViewType = DataDynamics.ActiveReports.Viewer.ViewType.Normal
        Me.ARView.Size = New System.Drawing.Size(992, 752)
        Me.ARView.TabIndex = 0
        Me.ARView.TableOfContents.Text = "Table Of Contents"
        Me.ARView.TableOfContents.Width = 200
        Me.ARView.TabTitleLength = 35
        Me.ARView.Toolbar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'OpenFileDialog
        '
        Me.OpenFileDialog.FileName = "OpenFileDialog"
        '
        'arBBView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(994, 746)
        Me.Controls.Add(Me.ARView)
        Me.Name = "arBBView"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ARView As DataDynamics.ActiveReports.Viewer.Viewer
    Friend WithEvents PdfExport As DataDynamics.ActiveReports.Export.Pdf.PdfExport
    Friend WithEvents OpenFileDialog As System.Windows.Forms.OpenFileDialog
    Public WithEvents dlgFileNameSave As System.Windows.Forms.SaveFileDialog
End Class
