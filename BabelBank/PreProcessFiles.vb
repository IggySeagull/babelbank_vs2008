﻿Imports System.IO
Public Class PreProcessFiles
    'Benyttes for å kontrollere filer for gyldig format.
    'Benyttes spesielt av bankenes supportavdeling, gjerne i sammenheng med Class ViewPayments.
    Private sFileName As String
    Dim fFormatExpected As BabelFiles.FileType
    Dim frmPre As frmPreProcess

    Public Property FormatExpected() As vbBabel.BabelFiles.FileType
        Get
            FormatExpected = fFormatExpected
        End Get
        Set(ByVal Value As vbBabel.BabelFiles.FileType)
            fFormatExpected = Value
        End Set
    End Property
    Public Property FileName() As String
        Get
            FileName = sFileName
        End Get
        Set(ByVal Value As String)
            sFileName = Value
        End Set
    End Property

    Public Sub Analyze()
        frmPre = New frmPreProcess

        ' Show dialog
        frmPre.FileName = sFileName
        frmPre.ShowDialog()

    End Sub
End Class
