﻿Imports System.Windows.Forms
Imports DGVPrinterHelper
Public Class frmViewBatch
    Private oBabelFiles As vbBabel.BabelFiles
    Private frmViewPayment As frmViewPayment
    Dim frmViewSearch As frmViewFilter
    Private aColumns(,) As Object
    Private Sub frmViewBatch_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg")
        FormLRSCaptions(Me)
        Me.gridBatch.Focus()
    End Sub
    Private Sub frmViewBatch_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        ' check if searchwindow is open
        If Not frmViewSearch Is Nothing Then
            frmViewSearch.Close()
            frmViewSearch = Nothing
        End If
    End Sub

    Private Sub gridBatch_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles gridBatch.CellMouseClick
        ' Browse all payments for actual batch
        Dim nBabelFileIndex As Double
        Dim nBatchIndex As Double

        If e.RowIndex > -1 Then
            If (Control.ModifierKeys And Keys.Control) <> Keys.Control Then  ' If Ctrl+Click, do nothing, leave it to default handling (select)

                nBabelFileIndex = Me.gridBatch.Rows(e.RowIndex).Cells(0).Value
                nBatchIndex = Me.gridBatch.Rows(e.RowIndex).Cells(1).Value

                If e.ColumnIndex = -1 Then
                    ' clicked in row header - present detailinfo
                    Dim frmViewPaymentDetail As New frmViewPaymentDetail
                    frmViewPaymentDetail.SetBabelFilesInViewPayment(oBabelFiles)
                    frmViewPaymentDetail.PassBatch(oBabelFiles(nBabelFileIndex).Batches(nBatchIndex))
                    frmViewPaymentDetail.Fill_gridBatchDetail()
                    frmViewPaymentDetail.ShowDialog()
                    frmViewPaymentDetail = Nothing
                Else
                    ' fill grid, with this, or all selected rows
                    FillGridPayments()
                End If
            End If
        End If
    End Sub
    Public Sub SetBabelFilesInViewBatch(ByRef NewVal As vbBabel.BabelFiles)
        oBabelFiles = NewVal
    End Sub
    Private Sub FillGridPaymentHeaders(ByVal nBabelFileIndex As Double, ByVal nBatchIndex As Double)
        ' Browse all payments for actual batch
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim lCounter As Long
        Dim iImportFmt As vbBabel.BabelFiles.FileType

        Try

            ' First, construct grid
            With frmViewPayment.gridPayment
                .EnableHeadersVisualStyles = False
                .Columns.Clear()
                .ScrollBars = ScrollBars.Both
                .AllowUserToAddRows = False
                .RowHeadersVisible = True
                .RowHeadersWidth = 70
                .RowHeadersDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowHeadersDefaultCellStyle.SelectionForeColor = Color.Black
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowsDefaultCellStyle.SelectionForeColor = Color.Black
                .RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .BorderStyle = BorderStyle.None
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .AutoSize = False
                .ReadOnly = True
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                .TopLeftHeaderCell.Style.BackColor = Color.LightBlue

                ' then add columns
                ' always a hidden col with babelfileindex as col 0
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                ' and always a hidden col with batchindex as col 1
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                ' and always a hidden col with paymentindex as col 2
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                If nBatchIndex > 0 Then
                    iImportFmt = oBabelFiles.Item(nBabelFileIndex).Batches.Item(nBatchIndex).ImportFormat
                Else
                    If oBabelFiles.Count > 0 Then
                        ' assume same format for all imported files, otherwise setup will be for the first format
                        iImportFmt = oBabelFiles.Item(nBabelFileIndex).ImportFormat
                    End If
                End If

                aColumns = FindViewColumnsPayment(iImportFmt, oBabelFiles.Item(1).StatusCode)

                ' Headers, widths, etc
                For lCounter = 0 To aColumns.GetUpperBound(1)
                    txtColumn = New DataGridViewTextBoxColumn
                    txtColumn.HeaderText = aColumns(1, lCounter)
                    txtColumn.Width = aColumns(2, lCounter)
                    If aColumns(0, lCounter) = "MON_InvoiceAmount" Or _
                       aColumns(0, lCounter) = "MON_TransferredAmount" Then
                        txtColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        txtColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                        txtColumn.DefaultCellStyle.Format = "##,##0.00"
                        txtColumn.ValueType = GetType(System.Double)  ' to sort correctly
                    ElseIf aColumns(0, lCounter) = "Date_Payment" Or _
                        aColumns(0, lCounter) = "Date_Value" Then
                        txtColumn.ValueType = GetType(System.DateTime)
                    End If ' to sort correctly
                    .Columns.Add(txtColumn)
                Next

            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

    End Sub
    Private Sub FillGridPayments()
        ' fill the grid with content from payments in batch/batches
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment

        Dim nTotalAmount As Double
        Dim nTotalNoOfPayments As Double
        Dim iImportFmt As vbBabel.BabelFiles.FileType
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim lCounter As Long
        Dim iCellCounter As Integer
        Dim x As Long
        Dim nBabelFileIndex As Long
        Dim nBatchIndex As Long
        Dim bGridPaymentConstructed As Boolean

        nTotalAmount = 0
        nTotalNoOfPayments = 0
        bGridPaymentConstructed = False

        Try
            frmViewPayment = New frmViewPayment
            frmViewPayment.SetBabelFilesInViewPayment(oBabelFiles)

            Do While x < Me.gridBatch.Rows.Count
                If Me.gridBatch.Rows(x).Selected Then
                    nBabelFileIndex = Me.gridBatch.Rows(x).Cells(0).Value
                    nBatchIndex = Me.gridBatch.Rows(x).Cells(1).Value

                    If Not bGridPaymentConstructed Then
                        ' gridconstruct
                        FillGridPaymentHeaders(nBabelFileIndex, nBatchIndex)
                        bGridPaymentConstructed = True
                    End If

                    oBatch = oBabelFiles.Item(nBabelFileIndex).Batches.Item(nBatchIndex)

                    iImportFmt = oBatch.ImportFormat
                    nTotalNoOfPayments = nTotalNoOfPayments + oBatch.Payments.Count
                    If oBatch.MON_TransferredAmount <> 0 Then
                        nTotalAmount = nTotalAmount + oBatch.MON_TransferredAmount
                    Else
                        nTotalAmount = nTotalAmount + oBatch.MON_InvoiceAmount
                    End If

                    For Each oPayment In oBatch.Payments
                        ' fill grid
                        ' the setup for the grid can either have been save by the user, for each format, or
                        ' not saved, but is dependent upon formatdefaults

                        With frmViewPayment.gridPayment

                            .Rows.Add()
                            .Rows(.RowCount - 1).HeaderCell.Value = .RowCount.ToString

                            .Rows(.RowCount - 1).Cells(0).Value = nBabelFileIndex
                            .Rows(.RowCount - 1).Cells(1).Value = nBatchIndex
                            .Rows(.RowCount - 1).Cells(2).Value = oPayment.Index
                            iCellCounter = 2  ' Start in col 3, because we have a hidden one in col 0 and 1, and checkbox in 2 !

                            For lCounter = 0 To aColumns.GetUpperBound(1)
                                iCellCounter = iCellCounter + 1

                                If aColumns(0, lCounter) = "MON_InvoiceAmount" Then
                                    '.Rows(.RowCount - 1).Cells(iCellCounter).Value = Format(oPayment.MON_InvoiceAmount / 100, "##,##0.00")
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.MON_InvoiceAmount / 100
                                End If
                                If aColumns(0, lCounter) = "MON_TransferredAmount" Then
                                    '.Rows(.RowCount - 1).Cells(iCellCounter).Value = Format(oPayment.MON_TransferredAmount / 100, "##,##0.00")
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.MON_TransferredAmount / 100
                                End If
                                If aColumns(0, lCounter) = "MON_LocalAmount" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.MON_LocalAmount / 100
                                End If
                                If aColumns(0, lCounter) = "MON_AccountAmount" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.MON_AccountAmount / 100
                                End If
                                If aColumns(0, lCounter) = "MON_InvoiceCurrency" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.MON_InvoiceCurrency
                                End If
                                If aColumns(0, lCounter) = "MON_TransferCurrency" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.MON_TransferCurrency
                                End If
                                If aColumns(0, lCounter) = "E_Name" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.E_Name
                                End If
                                If aColumns(0, lCounter) = "E_Account" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.E_Account
                                End If
                                If aColumns(0, lCounter) = "E_Adr1" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.E_Adr1
                                End If
                                If aColumns(0, lCounter) = "E_Adr2" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.E_Adr2
                                End If
                                If aColumns(0, lCounter) = "E_Adr3" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.E_Adr3
                                End If
                                If aColumns(0, lCounter) = "E_City" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.E_City
                                End If
                                If aColumns(0, lCounter) = "E_Zip" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.E_Zip
                                End If
                                If aColumns(0, lCounter) = "E_CountryCode" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.E_CountryCode
                                End If
                                If aColumns(0, lCounter) = "E_OrgNo" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.e_OrgNo
                                End If

                                If aColumns(0, lCounter) = "I_Client" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.I_Client
                                End If
                                If aColumns(0, lCounter) = "I_Name" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.I_Name
                                End If
                                If aColumns(0, lCounter) = "I_Account" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.I_Account
                                End If
                                If aColumns(0, lCounter) = "Date_Payment" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = StringToDate(oPayment.DATE_Payment)
                                End If
                                If aColumns(0, lCounter) = "Date_Value" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = Format(oPayment.DATE_Value, "dddddd")  ' as longdate
                                End If
                                If aColumns(0, lCounter) = "Ref_Bank1" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.REF_Bank1
                                End If
                                If aColumns(0, lCounter) = "Ref_Bank2" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.REF_Bank2
                                End If
                                If aColumns(0, lCounter) = "Ref_Own" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.REF_Own
                                End If
                                If aColumns(0, lCounter) = "Text_E_Statement" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.Text_E_Statement
                                End If
                                If aColumns(0, lCounter) = "Text_I_Statement" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = oPayment.Text_I_Statement
                                End If
                                If aColumns(0, lCounter) = "Priority" Then
                                    .Rows(.RowCount - 1).Cells(iCellCounter).Value = IIf(oPayment.Priority, "X", "")
                                End If

                            Next lCounter
                        End With
                    Next 'oBatch
                End If
                    x = x + 1
            Loop
            'frmViewPayment.Text = LRS(40017) & " " & Trim$(Format(oBatch.MON_InvoiceAmount / 100, "##,##0.00")) & " / " & LRS(40014) & " " & Format(oBatch.Payments.Count, "##,##0")
            frmViewPayment.Text = LRS(40017) & " " & Trim$(Format(nTotalAmount / 100, "##,##0.00")) & " / " & LRS(40014) & " " & Format(nTotalNoOfPayments, "##,##0")

            'frmViewPayment.Show(Me)
            frmViewPayment.ShowDialog(Me)  ' changed 24.01.2022
            'frmViewPayment.Close()
            frmViewPayment = Nothing

        Catch ex As Exception
            frmViewPayment.Close()
            frmViewPayment = Nothing

            MessageBox.Show(ex.Message)

        End Try

        ' hvilke kolonner

        'MON_InvoiceAmount
        'MON_TransferredAmount
        'MON_InvoiceCurrency
        'MON_TransferCurrency
        'E_Name
        'E_Account
        'E_Adr1
        'E_Adr2
        'E_Adr3
        'E_City
        'E_Zip
        'E_CountryCode
        'E_OrgNo
        'I_Client
        'I_Name
        'I_Account
        'Date_Payment
        'Date_Value
        'Ref_Bank1
        'Ref_Bank2
        'Ref_Own
        'bPriority
        'sText_E_Statement
        'sText_I_Statement

        'sPayCode
        'eCheque
        'bToOwnAccount
        'sPayType
        'sDnBNORTBIPayType
        'sVB_ClientNo
        'sMON_ChargesCurrency
        'bMON_ChargeMeDomestic
        'bMON_ChargeMeAbroad
        'nERA_ExchRateAgreed
        'sERA_DealMadeWith
        'dERA_Date
        'nFRW_ForwardContractRate
        'sFRW_ForwardContractNo
        'sNOTI_NotificationMessageToBank
        'sNOTI_NotificationType
        'iNOTI_NotificationParty
        'sNOTI_NotificationAttention
        'sNOTI_NotificationIdent
        'sBANK_SWIFTCode
        'sBANK_BranchNo
        'eBANK_BranchType
        'sBANK_Adr2
        'sBANK_Name
        'sBANK_Adr1
        'sBANK_Adr3
        'sBANK_CountryCode
        'sBANK_SWIFTCodeCorrBank
        'eBANK_BranchTypeCorrBank
        'sBANK_BranchNoCorrBank
        'sBANK_AccountCountryCode
        'sBANK_I_SWIFTCode
        'eBANK_I_BranchType
        'sBANK_NameAddressCorrBank1
        'sBANK_NameAddressCorrBank2
        'sBANK_NameAddressCorrBank3
        'sBANK_NameAddressCorrBank4
        'iImportFormat
        'sREF_EndToEnd


    End Sub
    Private Function FindViewColumnsPayment(ByVal iImportFmt As vbBabel.BabelFiles.FileType, ByVal sStatusCode As String) As Object(,)
        Dim aReturn(,) As Object
        Dim bSavedSetup As Boolean = False

        ' aReturn holds
        ' (0) CollectionName (field)
        ' (1) Headertext
        ' (2) ColumnWidth
        ' (3) Sorting, A, D or nothing

        ' if not any saved setup, then use default, based in imported format
        If Not bSavedSetup Then
            ' Always own account, date payment, amount, currency, external name, external account
            ReDim aReturn(3, 5)
            aReturn(0, 0) = "I_Account"
            aReturn(1, 0) = LRS(60076)   ' Account
            aReturn(2, 0) = "100"
            aReturn(3, 0) = "A"

            aReturn(0, 1) = "Date_Payment"
            aReturn(1, 1) = LRS(60065)  ' date
            aReturn(2, 1) = "70"
            aReturn(3, 1) = ""

            If sStatusCode = "02" Then
                ' use transferredAmount for returnfiles
                aReturn(0, 2) = "MON_TransferredAmount"
            Else
                aReturn(0, 2) = "MON_InvoiceAmount"
            End If
            aReturn(1, 2) = LRS(40021)  ' Amount
            aReturn(2, 2) = "90"
            aReturn(3, 2) = ""

            If sStatusCode = "02" Then
                ' use transferredCurrency for returnfiles
                aReturn(0, 3) = "MON_TransferCurrency"
            Else
                aReturn(0, 3) = "MON_InvoiceCurrency"
            End If
            aReturn(1, 3) = LRS(60066)  ' Currency
            aReturn(2, 3) = "40"
            aReturn(3, 3) = ""

            aReturn(0, 4) = "E_Name"
            aReturn(1, 4) = LRS(60086)  ' Name
            aReturn(2, 4) = "180"
            aReturn(3, 4) = ""

            aReturn(0, 5) = "E_Account"
            aReturn(1, 5) = LRS(60076)  ' Account
            aReturn(2, 5) = "200"
            aReturn(3, 5) = ""


            Select Case iImportFmt
                Case vbBabel.BabelFiles.FileType.Telepay, vbBabel.BabelFiles.FileType.Telepay2, vbBabel.BabelFiles.FileType.TelepayPlus, vbBabel.BabelFiles.FileType.TelepayTBIO


                    ' For OCR, DirRem, Autogiro add 
                    ' For Cremul, Debmul, Paymul add
                    ' For LB add

            End Select


        End If

        FindViewColumnsPayment = aReturn
    End Function
    Private Sub cmdReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        ' call frmViewerReportDialog for setup of report
        Dim frmViewerReportDialog As New frmViewerReportDialog
        Dim sTmp As String

        With frmViewerReportDialog
            .chkPreview.Checked = True
            .lstSorting.Items.Clear()
            ' TODO for real
            ' er disse lrs-ene OK i setup?
            ' skal vi lage dem i lrscommon ?
            '.lstSorting.Items.Add(LRS(60563))   '"LRS-Ingen sortering/Ingen brudd") 'NOBREAK
            '.lstSorting.Items.Add(LRS(60564))   '"LRS-Kontonummer, dato") 'BREAK_ON_ACCOUNT (and per day)
            '.lstSorting.Items.Add(LRS(60565))   '"LRS-Kontosummering")BREAK_ON_BATCH
            '.lstSorting.Items.Add(LRS(60566))   '"LRS-Pr. betaling")BREAK_ON_PAYMENT
            '.lstSorting.Items.Add(LRS(60567))   '"LRS-Kontonummer")BREAK_ON_ACCOUNTONLY
            '.lstSorting.Items.Add(LRS(60568))  '"LRS-Klientnummer")BREAK_ON_CLIENT
            .lstSorting.Items.Add("LRS-Ingen sortering/Ingen brudd") 'NOBREAK
            .lstSorting.Items.Add("LRS-Kontonummer, dato") 'BREAK_ON_ACCOUNT (and per day)
            .lstSorting.Items.Add("LRS-Kontosummering")  'BREAK_ON_BATCH
            .lstSorting.Items.Add("LRS-Pr. betaling") 'BREAK_ON_PAYMENT
            .lstSorting.Items.Add("LRS-Kontonummer") 'BREAK_ON_ACCOUNTONLY
            .lstSorting.Items.Add("LRS-Klientnummer") 'BREAK_ON_CLIENT
            .lstSorting.SelectedIndex = 0

            .ShowDialog()
        End With

        ' TODO - how to send only marked batches into reportclass ?
        If frmViewerReportDialog.DialogResult = 1 Then
            Dim oBabelReport As New vbBabel.BabelReport
            With frmViewerReportDialog
                oBabelReport.ReportNumber = "001"
                oBabelReport.Heading = .txtHeading.Text
                oBabelReport.BreakLevel = .lstSorting.SelectedIndex + 1 ' TODO check !!!
                oBabelReport.Preview = .chkPreview.Checked
                oBabelReport.ToPrint = .chkPrint.Checked
                oBabelReport.ExportFilename = .txtFilename.Text
                If .optHTML.Checked Then
                    oBabelReport.ExportType = "HTML"
                ElseIf .optPDF.Checked Then
                    oBabelReport.ExportType = "PDF"
                ElseIf .optRTF.Checked Then
                    oBabelReport.ExportType = "RTF"
                ElseIf .optTIFF.Checked Then
                    oBabelReport.ExportType = "TIFF"
                ElseIf .optTXT.Checked Then
                    oBabelReport.ExportType = "TXT"
                ElseIf .optXLS.Checked Then
                    oBabelReport.ExportType = "XLS"
                Else
                    oBabelReport.ExportFilename = ""
                    oBabelReport.ExportType = ""
                End If
                If .chkeMail.Checked Then
                    oBabelReport.ToEmail = True
                    ' add one or more e-mail addresses into reportobject
                    sTmp = .txteMail.Text
                    If InStr(sTmp, ";") > 0 Then
                        oBabelReport.AddEmailAddress(Strings.Left(sTmp, InStr(sTmp, ";") - 1))
                        sTmp = Strings.Mid(sTmp, InStr(sTmp, ";") + 1)
                    Else
                        oBabelReport.AddEmailAddress(sTmp)
                    End If
                End If
                oBabelReport.BabelFiles = oBabelFiles
                oBabelReport.RunReport()
                oBabelReport = Nothing

            End With
        End If

    End Sub

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        ' Bring up searchwindow
        frmViewSearch = New frmViewFilter
        ' gridPayment or gridInvoice ?
        frmViewSearch.passGrid(Me.gridBatch)
        frmViewSearch.SetBabelFilesInViewSearch(oBabelFiles)
        frmViewSearch.chkFromStartOfCell.Checked = True
        frmViewSearch.chkFilter.Checked = False
        frmViewSearch.txtSearchFor.Focus()

        frmViewSearch.Show() ' modeless

    End Sub

    Private Sub cmdShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdShow.Click
        ' Show content of more than one batch, by presenting frmViewPayment 
        FillGridPayments()
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click

    End Sub
End Class
