﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewFilter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewFilter))
        Me.cmdAll = New System.Windows.Forms.Button
        Me.cmdNext = New System.Windows.Forms.Button
        Me.cmdClose = New System.Windows.Forms.Button
        Me.txtSearchFor = New System.Windows.Forms.TextBox
        Me.lblSearchFor = New System.Windows.Forms.Label
        Me.chkCurrenctColumn = New System.Windows.Forms.CheckBox
        Me.chkFromStartOfCell = New System.Windows.Forms.CheckBox
        Me.chkFilter = New System.Windows.Forms.CheckBox
        Me.lblFilter = New System.Windows.Forms.Label
        Me.lblPassedEnd = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdAll
        '
        Me.cmdAll.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdAll.Location = New System.Drawing.Point(111, 148)
        Me.cmdAll.Name = "cmdAll"
        Me.cmdAll.Size = New System.Drawing.Size(90, 23)
        Me.cmdAll.TabIndex = 4
        Me.cmdAll.Text = "60222-Søk alle"
        '
        'cmdNext
        '
        Me.cmdNext.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdNext.Location = New System.Drawing.Point(202, 148)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(90, 23)
        Me.cmdNext.TabIndex = 5
        Me.cmdNext.Text = "60223-Søk neste"
        '
        'cmdClose
        '
        Me.cmdClose.Location = New System.Drawing.Point(292, 148)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(67, 23)
        Me.cmdClose.TabIndex = 6
        Me.cmdClose.Text = "55032-Lukk"
        '
        'txtSearchFor
        '
        Me.txtSearchFor.Location = New System.Drawing.Point(89, 34)
        Me.txtSearchFor.Name = "txtSearchFor"
        Me.txtSearchFor.Size = New System.Drawing.Size(265, 20)
        Me.txtSearchFor.TabIndex = 0
        '
        'lblSearchFor
        '
        Me.lblSearchFor.AutoSize = True
        Me.lblSearchFor.Location = New System.Drawing.Point(7, 37)
        Me.lblSearchFor.Name = "lblSearchFor"
        Me.lblSearchFor.Size = New System.Drawing.Size(83, 13)
        Me.lblSearchFor.TabIndex = 7
        Me.lblSearchFor.Text = "60218-Søk etter"
        '
        'chkCurrenctColumn
        '
        Me.chkCurrenctColumn.AutoSize = True
        Me.chkCurrenctColumn.Location = New System.Drawing.Point(89, 60)
        Me.chkCurrenctColumn.Name = "chkCurrenctColumn"
        Me.chkCurrenctColumn.Size = New System.Drawing.Size(168, 17)
        Me.chkCurrenctColumn.TabIndex = 1
        Me.chkCurrenctColumn.Text = "60219-Kun gjeldende kolonne"
        Me.chkCurrenctColumn.UseVisualStyleBackColor = True
        '
        'chkFromStartOfCell
        '
        Me.chkFromStartOfCell.AutoSize = True
        Me.chkFromStartOfCell.Location = New System.Drawing.Point(89, 85)
        Me.chkFromStartOfCell.Name = "chkFromStartOfCell"
        Me.chkFromStartOfCell.Size = New System.Drawing.Size(141, 17)
        Me.chkFromStartOfCell.TabIndex = 2
        Me.chkFromStartOfCell.Text = "60221-Fra starten av felt"
        Me.chkFromStartOfCell.UseVisualStyleBackColor = True
        '
        'chkFilter
        '
        Me.chkFilter.AutoSize = True
        Me.chkFilter.Location = New System.Drawing.Point(258, 60)
        Me.chkFilter.Name = "chkFilter"
        Me.chkFilter.Size = New System.Drawing.Size(100, 17)
        Me.chkFilter.TabIndex = 8
        Me.chkFilter.Text = "60220-Sett filter"
        Me.chkFilter.UseVisualStyleBackColor = True
        '
        'lblFilter
        '
        Me.lblFilter.AutoSize = True
        Me.lblFilter.Location = New System.Drawing.Point(12, 9)
        Me.lblFilter.Name = "lblFilter"
        Me.lblFilter.Size = New System.Drawing.Size(0, 13)
        Me.lblFilter.TabIndex = 12
        '
        'lblPassedEnd
        '
        Me.lblPassedEnd.AutoSize = True
        Me.lblPassedEnd.Location = New System.Drawing.Point(93, 112)
        Me.lblPassedEnd.Name = "lblPassedEnd"
        Me.lblPassedEnd.Size = New System.Drawing.Size(0, 13)
        Me.lblPassedEnd.TabIndex = 13
        '
        'frmViewFilter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(369, 179)
        Me.Controls.Add(Me.lblPassedEnd)
        Me.Controls.Add(Me.lblFilter)
        Me.Controls.Add(Me.chkFilter)
        Me.Controls.Add(Me.chkFromStartOfCell)
        Me.Controls.Add(Me.chkCurrenctColumn)
        Me.Controls.Add(Me.lblSearchFor)
        Me.Controls.Add(Me.txtSearchFor)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.cmdAll)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmViewFilter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "60218-Søk etter"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdAll As System.Windows.Forms.Button
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents txtSearchFor As System.Windows.Forms.TextBox
    Friend WithEvents lblSearchFor As System.Windows.Forms.Label
    Friend WithEvents chkCurrenctColumn As System.Windows.Forms.CheckBox
    Friend WithEvents chkFromStartOfCell As System.Windows.Forms.CheckBox
    Friend WithEvents chkFilter As System.Windows.Forms.CheckBox
    Friend WithEvents lblFilter As System.Windows.Forms.Label
    Friend WithEvents lblPassedEnd As System.Windows.Forms.Label
End Class
