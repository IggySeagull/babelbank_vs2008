Option Strict Off
Option Explicit On
'test
Module WriteTelepay2
	
    Dim oFs As Scripting.FileSystemObject ' 28.12.2010 removed New
	Dim oFile As Scripting.TextStream
	Dim sLine As String ' en output-linje
	
	Private sI_EnterpriseNo As String
	Private sTransDate As String
	Private bDomestic As Boolean
	Private bCurrentIsTBIO As Boolean
	Private bLastWasTBIO As Boolean
	'This variable show if the importfile we are working with is created from the
	' accounting system, or is a returnfile. The variable is set during reading the
	' BabelFile-object.
	Private bFileFromBank As Boolean
	'29.07.2008 - Changed the variable ClientNumber from Integer to string
	Private sClientNumber, sImportFilename As String
	'Old Code
	'Private nClientNumber As Integer, sImportFilename As String
	Private iStructuredPayment As Short 'added 02.05.06
	Private iNoOfBETFOR22_23 As Integer ' 03.02.2010 changed from Integer 'added 03.05.06
	Private iNoOfTextLinesInBETFOR23 As Short 'added 03.05.06
	Private lNoOfRecordsInFile As Integer ' added 05.02.2010, to count "size" for large files (SEB can take max 10 mb)
    ' XNET 28.05.2013
    Private bVismaIntegrator As Boolean
    Function WriteTelepay2File(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef iFilenameInNo As Short, ByRef sCompanyNo As String, ByRef sBranch As String, ByRef sVersion As String, ByRef SurroundWith0099 As Boolean, ByRef bUnix As Boolean, ByRef nSequenceNumberStart As Double, ByRef nSequenceNumberEnd As Double, ByRef sClientNo As String, ByRef sSpecial As String, ByRef eBank As BabelFiles.Bank, Optional ByVal oExternalUpdateConnection As vbBabel.DAL = Nothing, Optional ByVal oExternalReadConnection As vbBabel.DAL = Nothing) As Boolean
        Dim j, i, k As Double
        '-----------------------------------------------------------------------------
        ' 02.-03.05.2006
        ' Done some adjustments.
        'x Rejects mix of structured and unstructured
        '- Warns if more then 25 * 40 chars for unstructured (?)
        '- Rejects more than 999 BETFOR23 / 9999 BETFOR22
        '- Added some rejectcodes
        '- Added conversion for NorgesBank codes
        '------------------------------------------------------------------------------
        ' if bUnix is set to true, then only LineFeed is added at the end of each line,
        ' instead of the ordinary CRLF
        Dim bFirstLine As Boolean
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oClient As Client
        Dim oaccount As Account
        Dim oInvoice As Invoice
        Dim sNewOwnref As String
        Dim bWriteBETFOR00 As Boolean 'Used with the sequencenumbering
        Dim bWriteBETFOR99Later As Boolean 'Used with the sequencenumbering
        Dim bLastBatchWasDomestic As Boolean 'Used to check if we change to/from domestic/intenational
        Dim bFirstBatch As Boolean 'True until we write the first BETFOR00
        Dim bFirstMassTransaction, bWriteOK As Boolean
        Dim iCount As Integer ' 03.02.2010 changed from Integer
        Dim nNoOfBETFOR, nSequenceNoTotal As Double
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim nClientNo As Double
        Dim bFoundClient As Boolean
        Dim xAccount As String
        Dim bExportMassRecords As Boolean ' New 25.10.01 by janp - default: No masstrans for returnfiles
        Dim bLastPaymentWasMass As Boolean
        Dim sDummy As String
        Dim nInvoiceCounter As Double
        Dim iFilenameCounter As Short
        Dim sNBCode As String
        Dim sNBText As String
        Dim bAtLeastOneRecordIsExported As Boolean = False

        'To use with summarizing masspayment
        Dim oMassPayment As Payment
        Dim oMassInvoice As Invoice
        Dim bSumMassPayment As Boolean
        Dim nMassTransferredAmount As Double
        Dim sPayment_ID As String
        Dim lMassCounter As Integer
        Dim bContinueSummarize As Boolean

        ' lag en outputfil
        'Deleted 4/12 - Kjell
        'On Error GoTo errCreateOutputFile

        Try

            'XNET 28.05.2013
            '***************VISMA*****************************
            ' Testing if this is Visma Integrator
            bVismaIntegrator = IsThisVismaIntegrator()
            '************************************************

            ' added next line 28.12.2010 - TODO gave problems running more than once. CHECK ALL SIMILAR EXPORTERS!!!
            oFs = New Scripting.FileSystemObject
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            nInvoiceCounter = 0
            iFilenameCounter = 2 ' Next file is f.ex. Tiltb.2

            bExportoBabel = False
            bWriteBETFOR00 = True
            bWriteBETFOR99Later = False
            bFirstBatch = True
            nSequenceNoTotal = 1 'new by JanP 25.10.01

            For Each oBabel In oBabelFiles
                'If oBabel.Special = "�KONOMISERVICE" Then
                If sSpecial = "�KONOMISERVICE" Then
                    ' For �konomiservice we must create a new BETFOR00/99 for each client!!!
                    SurroundWith0099 = True
                End If

                If oBabel.VB_FilenameInNo = iFilenameInNo Then

                    bExportoBabel = False
                    'Have to go through each batch-object to see if we have objects that shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            'XNET - 29.02.2012 - do not export payments with Exported = True
                            If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Or (sSpecial = "BAMA_RETUR" And oPayment.I_Account = sI_Account) Then ' 12.10.2009 added BAMA_RETUR ...
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If

                            ' added 28.02.2019
                            ' NHST_LONN for Singapore will be exported to an OCBC bank file
                            If sSpecial = "NHST_LONN" And oPayment.VoucherType = "OCB" And oPayment.I_CountryCode = "SG" Then
                                bExportoBabel = False
                            End If

                            If bExportoBabel Then
                                Exit For
                            End If
                        Next oPayment
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oBatch


                    If bExportoBabel Then
                        'Set if the file was created int the accounting system or not.
                        bFileFromBank = oBabel.FileFromBank
                        sImportFilename = oBabel.FilenameIn
                        'i = 0 'FIXED Moved by Janp under case 2


                        'Loop through all Batch objs. in BabelFile obj.
                        For Each oBatch In oBabel.Batches
                            bExportoBatch = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oPayment In oBatch.Payments
                                'XNET - 29.02.2012 - do not export payments with Exported = True
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Or (sSpecial = "BAMA_RETUR" And oPayment.I_Account = sI_Account) Then ' 12.10.2009 added BAMA_RETUR ...
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If
                                ' added 28.02.2019
                                ' NHST_LONN for Singapore will be exported to an OCBC bank file
                                If sSpecial = "NHST_LONN" And oPayment.VoucherType = "OCB" And oPayment.I_CountryCode = "SG" Then
                                    bExportoBatch = False
                                End If

                                'If true exit the loop, and we have data to export
                                If bExportoBatch Then
                                    Exit For
                                End If
                            Next oPayment


                            If bExportoBatch Then

                                ' New by Janp 25.10.01
                                ' Optional if Mass-trans will be exported for returnfiles.
                                ' Default = False
                                ' In filesetup, test ReturnMass
                                If oBatch.VB_ProfileInUse = True Then
                                    If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().ReturnMass. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        bExportMassRecords = oBatch.VB_Profile.FileSetups(iFormat_ID).ReturnMass
                                    Else
                                        bExportMassRecords = True
                                    End If
                                Else
                                    bExportMassRecords = True
                                End If


                                ' New by JanP 25.10.01
                                If Not (bExportMassRecords = False And (oPayment.PayType = "M" Or oPayment.PayType = "S")) Then
                                    ' Don't put mass-payments into returnfiles (unless user has asked for it)
                                    If oBatch.VB_ProfileInUse Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If bFirstBatch Or oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch = True Then
                                            ' First batch in file, then write betfor00
                                            bWriteBETFOR00 = True
                                        Else
                                            ' Not first batch, and masstrans, don't write betfor00
                                            bWriteBETFOR00 = False
                                        End If
                                    Else
                                        ' XNET 15.05.2013 - always KeepBatch when bProfileInUse = False
                                        'If bFirstBatch Then
                                        bWriteBETFOR00 = True
                                        'Else
                                        '    bWriteBETFOR00 = False
                                        'End If
                                    End If

                                Else
                                    ' No export of mass-trans, but check if first betfor00:
                                    If bFirstBatch Then
                                        ' First batch in file, then write betfor00
                                        bWriteBETFOR00 = True
                                    Else
                                        ' Not first batch, and masstrans, don't write betfor00
                                        bWriteBETFOR00 = False
                                    End If
                                End If

                                'Check if we shall use the original sequenceno. or not
                                'First check if we use a profile
                                If oBatch.VB_ProfileInUse = True Then
                                    'Then if it is a return-file
                                    If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True And sSpecial <> "CONECTO" Then
                                        bFirstLine = True
                                        'If oBabel.Special = "CRENO_OCRTP" Then
                                        ' Changed 23.1.06
                                        If sSpecial = "CRENO_OCRTP" Then
                                            'Always use the seqno at filesetup level
                                            If bWriteBETFOR00 Then
                                                If bFirstBatch Then
                                                    'Only for the first batch if we have one sequenceseries!
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    nSequenceNoTotal = oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal + 1
                                                    If nSequenceNoTotal > 9999 Then
                                                        nSequenceNoTotal = 0
                                                    End If
                                                End If
                                            End If
                                        End If
                                    Else
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                            'Use the sequenceno from oBabel
                                            Case 0
                                                If oBatch.SequenceNoStart = -1 Then
                                                    'FIXERROR: Babelerror No sequenceno imported
                                                Else
                                                    nSequenceNoTotal = oBatch.SequenceNoStart
                                                    ' New by JanP 25.10.01
                                                    ' If we have a file with several betfor00/99, then we must
                                                    ' check if any betfor99 is pending, and deduct 1 from sequence, for the
                                                    ' pending betfor99 (iSeqenceNoTotal is added 1 when the pending
                                                    ' BETFOR99 is written;
                                                    If bWriteBETFOR99Later Then
                                                        nSequenceNoTotal = nSequenceNoTotal - 1
                                                    End If
                                                End If
                                                'Use the seqno at filesetup level
                                            Case 1
                                                'If it isn't the first client, just keep on counting.
                                                If bWriteBETFOR00 Then
                                                    ' New by JanP 24.10.01
                                                    If bFirstBatch Then
                                                        'Only for the first batch if we have one sequenceseries!
                                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                        nSequenceNoTotal = oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal + 1
                                                        ' For Scandza - one total sequence series for all clients. Save in Filesetup2 !!!!
                                                        If sSpecial = "SCANDZA" Then
                                                            nSequenceNoTotal = oBatch.VB_Profile.FileSetups(2).SeqNoTotal + 1
                                                        End If
                                                        If nSequenceNoTotal > 9999 Then
                                                            nSequenceNoTotal = 0
                                                        End If
                                                    End If
                                                End If
                                                'Use seqno per client per filesetup
                                            Case 2
                                                bFoundClient = False
                                                i = 0 ' New by Janp 25.10.01 Sets how day sequence will be counted
                                                'If oBabel.Special = "BERGESEN" Then
                                                If sSpecial = "BERGESEN" Then
                                                    'Remember to add this code in the writing of Telepay2
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                                        If Trim(oClient.CompNo) = Trim(oBatch.I_EnterpriseNo) Then
                                                            nSequenceNoTotal = oClient.SeqNoTotal + 1
                                                            If nSequenceNoTotal > 9999 Then
                                                                nSequenceNoTotal = 0
                                                            End If
                                                            nClientNo = oClient.Index
                                                            bFoundClient = True
                                                            Exit For
                                                        End If
                                                    Next oClient
                                                Else
                                                    'Normal situation
                                                    For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                                        ' Added special sequencetreatment for Ergogroup
                                                        If sSpecial = "ERGOREMITTERING" Then
                                                            If oClient.ClientNo = oBatch.Payments(1).VB_ClientNo Then
                                                                nSequenceNoTotal = oClient.SeqNoTotal + 1
                                                                If nSequenceNoTotal > 9999 Then
                                                                    nSequenceNoTotal = 0
                                                                End If
                                                                nClientNo = oClient.Index
                                                                '29.07.2008 - Changed the variable ClientNumber from Integer to string
                                                                sClientNumber = oClient.ClientNo
                                                                bFoundClient = True
                                                                Exit For
                                                            End If
                                                        Else
                                                            ' Normal - for all other than ERGOREMITTERING
                                                            For Each oaccount In oClient.Accounts
                                                                'If oaccount.Account = oBatch.Payments(1).I_Account Then
                                                                ' 06.01.2015 Added functionality for ConvertedAccount
                                                                If oaccount.Account = oBatch.Payments(1).I_Account Or Trim(oaccount.ConvertedAccount) = oBatch.Payments(1).I_Account Then
                                                                    nSequenceNoTotal = oClient.SeqNoTotal + 1
                                                                    If nSequenceNoTotal > 9999 Then
                                                                        nSequenceNoTotal = 0
                                                                    End If
                                                                    nClientNo = oClient.Index
                                                                    '29.07.2008 - Changed the variable ClientNumber from Integer to string
                                                                    sClientNumber = oClient.ClientNo
                                                                    bFoundClient = True
                                                                    Exit For
                                                                End If
                                                            Next oaccount
                                                        End If

                                                        If bFoundClient Then
                                                            Exit For
                                                        End If
                                                    Next oClient
                                                End If
                                        End Select
                                    End If
                                Else
                                    'New 07.06.2006
                                    nSequenceNoTotal = nSequenceNumberStart + 1
                                    If nSequenceNoTotal > 9999 Then
                                        nSequenceNoTotal = 0
                                    End If
                                    bFirstLine = True 'new by JanP 25.10.01
                                End If

                                ' XNET 28.05.2013
                                ' For Visma, find sequenceno from database for current Foretaksnr + Divisjon
                                If bVismaIntegrator Or sSpecial = "RETURNPAYMENTS" Then
                                    If bVismaIntegrator Then
                                        nSequenceNumberStart = Visma_RetrieveSequenceNumber(oExternalReadConnection, oBatch.I_EnterpriseNo, oBatch.I_Branch, oBabel.Visma_ProfileID, "BabelBank_" & oBabel.Visma_SystemDatabase) ' ProfileNo is in iFilenameInNo
                                        nSequenceNoTotal = nSequenceNumberStart + 1
                                        If nSequenceNoTotal > 9999 Then
                                            nSequenceNoTotal = 0
                                        End If
                                    End If
                                    bFirstLine = True  'new by JanP 25.10.01
                                End If

                                'Used in every applicationheader, TBII = Domestic, TBIU = International.
                                If oBatch.Payments.Count = 0 Then
                                    bDomestic = True
                                    'If bFirstBatch Then
                                    '    bLastBatchWasDomestic = True
                                    'End If
                                ElseIf Not oBatch.Payments(1).BANK_I_Domestic Then
                                    ' XNET 23.08.2012 - This can't be true for Norwegian debits !!??
                                    ' Added next If, and changed inside If
                                    'If oPayment.BANK_AccountCountryCode <> "NO" Then
                                    ' XNET 19.07.2013 - added AND
                                    If oPayment.BANK_AccountCountryCode <> "NO" And oPayment.BANK_AccountCountryCode <> "" Then

                                        If oPayment.BANK_AccountCountryCode = oPayment.BANK_CountryCode Then
                                            bDomestic = True
                                        Else
                                            bDomestic = False
                                        End If
                                        bCurrentIsTBIO = True
                                    Else
                                        bCurrentIsTBIO = False
                                        ' XNET 23.08.2012 - added next If
                                        If oBatch.Payments(1).PayType = "I" Then
                                            bDomestic = False
                                        Else
                                            bDomestic = True
                                        End If
                                    End If
                                ElseIf oBatch.Payments(1).PayType = "I" Then
                                    bDomestic = False
                                    bCurrentIsTBIO = False
                                    'If bFirstBatch Then
                                    '    bLastBatchWasDomestic = False
                                    'End If
                                Else
                                    bDomestic = True
                                    bCurrentIsTBIO = False
                                    'If bFirstBatch Then
                                    '    bLastBatchWasDomestic = True
                                    'End If
                                End If

                                ' XNET 19.07.2013
                                ' JanP: Jeg mener vi har en seri�s error her for hva som testes p� ved
                                ' Bank_I_Domestic = True. Dette b�r v�re TBIO-transer, ihvertfall hvis landkodene er like
                                ' Men jeg t�r ikke � gj�re noen generell endring p� dette, og gj�r en test kun for Visma
                                If bVismaIntegrator Then
                                    If oBatch.Payments(1).BANK_I_Domestic Then
                                        bDomestic = True
                                        bCurrentIsTBIO = True
                                    Else
                                        bCurrentIsTBIO = False
                                        If oBatch.Payments(1).PayType = "I" Then
                                            bDomestic = False
                                        Else
                                            bDomestic = True
                                        End If
                                    End If
                                End If

                                ' XokNET 08.02.2012
                                If sSpecial = "NHST_LONN" Then
                                    bCurrentIsTBIO = False
                                    ' XNET 27.03.2012
                                    ' 22.06.2022 FOr Telepay, always Int for NHST_LONN
                                    'If oPayment.PayType = "S" Then
                                    bDomestic = False
                                    'End If
                                End If


                                'Check if it is a change between domestic and international payment, and if
                                ' the bWriteBETFOR99Later flag is true (indicates that KeepBatch is set to false in
                                ' the database
                                If bLastBatchWasDomestic <> bDomestic Then
                                    If bWriteBETFOR99Later Then
                                        'TUFTE - N�r KeepBatch = false s� g�r vi inn i WriteBETFOR99, der legges en til sekvensteller
                                        'Deretter tester vi KeepBatch, og da trekker vi 1n fra sekvensteller
                                        'Her skriver vi allikevel BETFOR99'en som ble laget, men da legger vi ikke til 1 til sekvensteller
                                        'Kan l�ses ved � sende med en spesiell parameter (statuscode = +1) til funksjonen ApplicationHeader
                                        'Slik har vi gjort det for � trekke fra 1
                                        '27.08.2009
                                        ApplicationHeader(False, "+1")
                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                        nNoOfBETFOR = 0
                                    End If
                                    ' New by JanP 25.10.01
                                    If Not (bExportMassRecords = False And (oPayment.PayType = "M" Or oPayment.PayType = "S")) Then
                                        bWriteBETFOR00 = True
                                        bWriteBETFOR99Later = False
                                    Else
                                        'bWriteBETFOR00 = False  'Keep as is
                                        bWriteBETFOR99Later = False
                                    End If
                                End If

                                ' New by JanP 29.10.01
                                If oBatch.Payments.Count = 0 Then
                                    bLastBatchWasDomestic = True
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).PayType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                ElseIf oBatch.Payments(1).PayType = "I" Then
                                    bLastBatchWasDomestic = False
                                Else
                                    bLastBatchWasDomestic = True
                                End If


                                'Used in every applicationheader
                                If Not oBatch.DATE_Production = "19900101" Then
                                    sTransDate = Right(oBatch.DATE_Production, 4) '(oBatch.DATE_Production, "MMDD")
                                Else
                                    'If not imported set like computerdate
                                    sTransDate = VB6.Format(Now, "MMDD")
                                End If
                                'bFirstLine is used to set the sequenceno in the aplicationheader. If true seqno=1
                                'bFirstLine = False
                                'bFirstMassTransaction is used as a flag to treat the first mass transaction specially
                                'We have to create a BETFOR21-transaction before the first mass transaction (BETFOR22).
                                bFirstMassTransaction = True

                                'Reset count of BETFOR-records
                                If bWriteBETFOR00 Then
                                    nNoOfBETFOR = 0
                                    If oBatch.VB_ProfileInUse Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType = 1 Or oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem Then
                                            ' JanP 25.10.01 added ....FromAccountingSystem=True
                                            If bFirstBatch Then
                                                bFirstLine = True
                                            Else
                                                ' Keep on with daysequence until file end
                                                bFirstLine = False
                                            End If
                                        Else
                                            ' Reset day sequence for each betfor00
                                            bFirstLine = True
                                        End If
                                    Else
                                        If bFirstBatch Then
                                            bFirstLine = True
                                        Else
                                            ' Keep on with daysequence until file end
                                            bFirstLine = False
                                        End If
                                    End If
                                End If
                                i = i + 1

                                'If i = 1 Then
                                '    bFirstLine = True
                                'End If

                                'Get the companyno either from a property passed from BabelExport, or
                                ' from the imported file
                                If sCompanyNo <> "" Then
                                    sI_EnterpriseNo = PadLeft(sCompanyNo, 11, "0")
                                Else
                                    sI_EnterpriseNo = PadLeft(oBatch.I_EnterpriseNo, 11, "0")
                                End If
                                ' XokNET 08.02.2012 - pick EnterpriseNo for TELEPAY from inputfile for NHST_LONN
                                If sSpecial = "NHST_LONN" Then
                                    sI_EnterpriseNo = PadLeft(oBatch.I_EnterpriseNo, 11, "0")
                                    sCompanyNo = ""
                                End If

                                'BETFOR00

                                If bWriteBETFOR00 Then
                                    nNoOfBETFOR = 0
                                    sLine = WriteBETFOR00(oBatch, bFirstLine, nSequenceNoTotal, sBranch, sCompanyNo, sSpecial)
                                    bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                    bFirstBatch = False

                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    xAccount = oBatch.Payments(1).I_Account ' Spec. for 0099-format

                                End If
                                j = 0
                                k = 0
                                lMassCounter = 1

                                For Each oPayment In oBatch.Payments

                                    '---------------------------------------
                                    ' Special for betfor0099-format;
                                    ' Surround each account with BETFOR00/99
                                    ' Ref NorgesGruppen
                                    '---------------------------------------
                                    If SurroundWith0099 Then
                                        If oPayment.I_Account <> xAccount Then
                                            ' 04.07.05: Changed from  > 0, to awoid Records with only BETFOR00/BETFOR99
                                            'If nNoOfBETFOR > 0 Then
                                            If nNoOfBETFOR > 1 Then
                                                sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion, sSpecial)
                                                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                nNoOfBETFOR = 0

                                                sLine = WriteBETFOR00(oBatch, bFirstLine, nSequenceNoTotal, sBranch, sCompanyNo, sSpecial, oPayment.VB_ClientNo)
                                                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            End If
                                            xAccount = oPayment.I_Account ' Spec. for 0099-format
                                        End If
                                    End If

                                    '-------------------------------------------------------------
                                    ' Test if it is a TBIO-payment or not, and
                                    ' whether there is a change between TBIO and ordinary Telepay
                                    If oPayment.BANK_I_Domestic Then
                                        bCurrentIsTBIO = False
                                    Else
                                        ' XNET 23.08.2012 - This can't be true for Norwegian debits !!??
                                        ' Added next If
                                        'If oPayment.BANK_AccountCountryCode <> "NO" Then
                                        ' XNET 19.07.2013 - added AND
                                        If oPayment.BANK_AccountCountryCode <> "NO" And oPayment.BANK_AccountCountryCode <> "" Then
                                            bCurrentIsTBIO = True
                                        Else
                                            bCurrentIsTBIO = False
                                        End If
                                    End If
                                    ' XNET 08.02.2012
                                    If sSpecial = "NHST_LONN" Then
                                        bCurrentIsTBIO = False
                                    End If

                                    ' XNET 19.07.2013
                                    ' JanP: Jeg mener vi har en seri�s error her for hva som testes p� ved
                                    ' Bank_I_Domestic = True. Dette b�r v�re TBIO-transer, ihvertfall hvis landkodene er like
                                    ' Men jeg t�r ikke � gj�re noen generell endring p� dette, og gj�r en test kun for Visma
                                    If bVismaIntegrator Then
                                        If oPayment.BANK_I_Domestic Then
                                            bDomestic = True
                                            bCurrentIsTBIO = True
                                        Else
                                            If oPayment.PayType = "I" Then
                                                bCurrentIsTBIO = False
                                                bDomestic = False
                                            Else
                                                bDomestic = True
                                            End If
                                        End If
                                    End If

                                    ' Check if there is a change between TBIO and Telepay
                                    'If bCurrentIsTBIO <> bLastWasTBIO Then
                                    ' XNET 29.02.2012 -
                                    ' Kryper til korset, og tester ogs� p� mulige blandingsbatcher
                                    If _
                                    (bCurrentIsTBIO <> bLastWasTBIO) Or _
                                    (bLastBatchWasDomestic And oPayment.PayType = "I") Or _
                                    (bLastBatchWasDomestic = False And oPayment.PayType <> "I") _
                                    Then
                                        If Not oPayment.Exported Then
                                            ' Change, must write BETFOR99/00
                                            'If nNoOfBETFOR > 0 Then
                                            ' 04.07.05: Changed from  > 0, to awoid Records with only BETFOR00/BETFOR99
                                            If nNoOfBETFOR > 1 Then
                                                sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion, sSpecial)
                                                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                nNoOfBETFOR = 0

                                                'XNET 29.02.2012----------------
                                                ' XNET 19.07.2013
                                                ' JanP: Det under kan ogs� v�re tvilsomt i TBIO-case
                                                ' Men disse g�r vel idag som Telepay (1)
                                                ' Lagt til test for Visma
                                                If Not bVismaIntegrator Then
                                                    If oPayment.PayType = "I" Then
                                                        bDomestic = False
                                                    Else
                                                        bDomestic = True
                                                    End If
                                                End If
                                                bLastBatchWasDomestic = bDomestic
                                                '--------------------------------

                                                ' XNET 07.12.2012
                                                If sSpecial = "NHST_LONN" Then
                                                    ' can't be domestic for NHST_LONN when paytype="I"
                                                    If oBatch.Payments(1).PayType = "I" Then
                                                        bDomestic = False
                                                    End If
                                                End If

                                                sLine = WriteBETFOR00(oBatch, bFirstLine, nSequenceNoTotal, sBranch, sCompanyNo, sSpecial)
                                                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            End If
                                        End If
                                    End If
                                    bLastWasTBIO = bCurrentIsTBIO
                                    '---------------------------------------------------------------

                                    ' 18.12.2007
                                    ' For S|E|B there is a limitation of 999 invoicerecords pr Telepayfile.
                                    ' Due to that, we have to split large files in several files
                                    'If oBabel.Bank = Bank.SEB_NO Then
                                    If eBank = BabelFiles.Bank.SEB_NO Then
                                        ' 04.02.2010 - must also consider Masspayments.
                                        '              If Mass, set the limit to 25 000, which will give a file < 10mb
                                        'If nInvoiceCounter > 899 Then
                                        If (bLastPaymentWasMass = True And lNoOfRecordsInFile > 25000) Or (bLastPaymentWasMass = False And nInvoiceCounter > 899) Then
                                            ' End this file, and produce a new filer
                                            sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion)
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            nNoOfBETFOR = 0
                                            ' Close file
                                            oFile.Close()

                                            ' Open another file, with filenamecounter added 1
                                            oFile = oFs.OpenTextFile(sFilenameOut & "_" & Trim(Str(iFilenameCounter)), Scripting.IOMode.ForAppending, True, 0)
                                            iFilenameCounter = iFilenameCounter + 1

                                            ' start new file with BETFOR00
                                            sLine = WriteBETFOR00(oBatch, bFirstLine, nSequenceNoTotal, sBranch, sCompanyNo, sSpecial)
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)

                                            nInvoiceCounter = 0
                                            k = 0
                                            lNoOfRecordsInFile = 0
                                            bFirstMassTransaction = True
                                        End If
                                    End If

                                    bExportoPayment = False
                                    bLastPaymentWasMass = False

                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    ' added 16.12.2008 - do not export payments with Exported = True
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Or (sSpecial = "BAMA_RETUR" And oPayment.I_Account = sI_Account) Then ' 12.10.2009 added BAMA_RETUR ...
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                    ' 21.03.2014, added next If/EndIf
                                                    If sSpecial = "RETURNPAYMENTS" Then
                                                        ' Export for one I_Account pr file
                                                        If oPayment.I_Account = sI_Account Then
                                                            bExportoPayment = True
                                                        Else
                                                            bExportoPayment = False
                                                        End If
                                                    End If

                                                End If
                                            End If
                                        End If

                                        ' added 28.02.2019
                                        ' NHST_LONN for Singapore will be exported to an OCBC bank file
                                        If sSpecial = "NHST_LONN" And oPayment.VoucherType = "OCB" And oPayment.I_CountryCode = "SG" Then
                                            bExportoPayment = False
                                        End If
                                        ' added 22.06.2022
                                        ' NHST_LONN for US will be exported in Nacha file
                                        If sSpecial = "NHST_LONN" And oPayment.I_CountryCode = "US" Then
                                            bExportoPayment = False
                                        End If


                                    End If


                                    If bExportoPayment Then
                                        bAtLeastOneRecordIsExported = True
                                        j = j + 1
                                        'Test what sort of transaction
                                        ' XNET 19.07.2013 Added or bcurrentIsTBIO, removed Or oPayment.BANK_I_Domestic = False
                                        If oPayment.PayType = "I" Or bCurrentIsTBIO Then
                                            'International payment, or TBIO-payment
                                            sLine = WriteBETFOR01(oPayment, nSequenceNoTotal, sCompanyNo, iFormat_ID, sSpecial)
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            sLine = WriteBETFOR02(oPayment, nSequenceNoTotal, sCompanyNo, sSpecial)
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            sLine = WriteBETFOR03(oPayment, nSequenceNoTotal, sCompanyNo, sSpecial)
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            'Remove later: ExportCheck
                                            oPayment.Exported = True
                                            'Have to reset masstrans flag
                                            bFirstMassTransaction = True
                                            k = 0 'XNET - 11.04.2013 - This counter must be reset per payment,it is used as linenumber in Telepay
                                        ElseIf oPayment.PayType = "M" Or oPayment.PayType = "S" Then
                                            If oPayment.Payment_ID <> sPayment_ID Then
                                                bFirstMassTransaction = True
                                            End If
                                            '04.02.2009 changed next line due to max 9999 betfor22
                                            'If bFirstMassTransaction Then
                                            If bFirstMassTransaction Or k > 9998 Then
                                                'New by JanP 25.10.01 - Option to turn off export of masstranser for returnfiles
                                                If bExportMassRecords Then
                                                    'New code 10.07.2002
                                                    'Have to sumarize alle invoices/payments, to put the
                                                    'amount in Betfor21.
                                                    nMassTransferredAmount = 0
                                                    'Assumes that all BETFOR22 records in the imported file within
                                                    ' a BETFOR21 has the same PAYMENT_ID (also Invoice_ID).
                                                    sPayment_ID = oPayment.Payment_ID

                                                    '21.05.2013 - Added next IF. This code is not necessary when creating a file to the bank
                                                    If oBabel.StatusCode = "00" Then
                                                        bContinueSummarize = False
                                                    Else
                                                        bContinueSummarize = True
                                                    End If

                                                    'Old code
                                                    'bContinueSummarize = True


                                                    'Reset counter for BETFOR22-records
                                                    k = 0
                                                    Do While bContinueSummarize
                                                        If lMassCounter <= oBatch.Payments.Count Then
                                                            oMassPayment = oBatch.Payments.Item(lMassCounter)
                                                            'Have to go through the payment-object to see if the next payment also is
                                                            ' a masspayment from same account, paymentdate, filenameout and ownref
                                                            'If so add them together
                                                            If oMassPayment.VB_FilenameOut_ID = iFormat_ID And (oMassPayment.PayType = "S" Or oMassPayment.PayType = "M") Then
                                                                If oMassPayment.I_Account = sI_Account Then
                                                                    If oMassPayment.Payment_ID = sPayment_ID Then
                                                                        If oMassPayment.REF_Own.Contains("&?") Then
                                                                            'If InStr(oMassPayment.REF_Own, "&?") Then
                                                                            'Set in the part of the OwnRef that BabelBank is using
                                                                            sNewOwnref = Right(oMassPayment.REF_Own, Len(oMassPayment.REF_Own) - InStr(oMassPayment.REF_Own, "&?") - 1)
                                                                        Else
                                                                            sNewOwnref = ""
                                                                        End If
                                                                        If sNewOwnref = sOwnRef Then
                                                                            bSumMassPayment = True
                                                                        End If
                                                                    Else
                                                                        Exit Do
                                                                    End If
                                                                Else
                                                                    Exit Do
                                                                End If
                                                            Else
                                                                Exit Do
                                                            End If
                                                            If bSumMassPayment Then
                                                                For Each oMassInvoice In oMassPayment.Invoices
                                                                    nMassTransferredAmount = nMassTransferredAmount + oMassInvoice.MON_TransferredAmount
                                                                    lMassCounter = lMassCounter + 1
                                                                    '04.02.2009 due to max 9999 betfor22
                                                                    If lMassCounter / 500 = Math.Round(lMassCounter / 500, 0) Then
                                                                        Application.DoEvents()
                                                                    End If

                                                                    If lMassCounter > 9998 Then
                                                                        Exit For
                                                                    End If
                                                                Next oMassInvoice
                                                            End If
                                                        Else
                                                            Exit Do
                                                        End If
                                                    Loop  'oMassPayment

                                                    'End new code

                                                    'BETFOR21 for Masstrans
                                                    sLine = WriteBETFOR21Mass(oPayment, nSequenceNoTotal, sCompanyNo, nMassTransferredAmount, iFormat_ID)
                                                    ' XNET 22.02.2012 - see comments below, but must be careful so we do not have two BETFOR21's in a row
                                                    xAccount = oPayment.I_Account
                                                    'First mass-transaction treated
                                                    bFirstMassTransaction = False
                                                    lNoOfRecordsInFile = lNoOfRecordsInFile + 1
                                                    bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                    'Remove later: ExportCheck
                                                    oPayment.Exported = True
                                                End If
                                            End If
                                        Else
                                            'BETFOR21 for invoicetrans
                                            sLine = WriteBETFOR21Invoice(oPayment, nSequenceNoTotal, sCompanyNo, iFormat_ID, sSpecial)
                                            'Have to reset masstrans flag
                                            bFirstMassTransaction = True
                                            lNoOfRecordsInFile = lNoOfRecordsInFile + 1
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            'Remove later: ExportCheck
                                            oPayment.Exported = True
                                            'XNET 27.01.2011 - must reset Invoicecounter (iSerialNo, L�penummer) pr Betfor21
                                            k = 0
                                        End If
                                        'k = 0  Only one invoice pr payment for masspayments!
                                        'If its a payment to an own account we shall just create a BETFOR21, not 22 or 23
                                        ' Changed 19,10.05 BUT this goes for Domestic only. For Int., create BETFOR04!!!!
                                        If oPayment.ToOwnAccount = False Or oPayment.PayType = "I" Then
                                            For Each oInvoice In oPayment.Invoices
                                                ' XNET 14.06.2013
                                                '**************************************************
                                                '* CRITICAL CHANGE                                *
                                                '* We will now test if oInvoice.Exported = True   *
                                                '* If True, then do not export this invoice       *
                                                '**************************************************
                                                If Not oInvoice.Exported = True Then
                                                    'k will be used as a sequential number.
                                                    k = k + 1
                                                    lNoOfRecordsInFile = lNoOfRecordsInFile + 1

                                                    ' Changed 22.02.2010 -
                                                    ' Caused errors for TBIO, domestics, like Sweden to Sweden
                                                    'If oPayment.PayType = "I" Then
                                                    ' XNET 19.07.2013 Added or bcurrentIsTBIO, removed Or oPayment.BANK_I_Domestic = False
                                                    If oPayment.PayType = "I" Or bCurrentIsTBIO Then
                                                        '29.01.2010 - Added the possibility to add a standard regulatory reporting text and code
                                                        sNBCode = ""
                                                        sNBText = ""
                                                        If oPayment.VB_ProfileInUse Then

                                                            If Not EmptyString(oPayment.VB_Profile.FileSetups.Item(iFormat_ID).ExplanationNB) Then
                                                                If Not oPayment.VB_Profile.FileSetups.Item(iFormat_ID).CodeNB = -1 Then
                                                                    ' XNET 06.11.2012 - in some cases (based on setup) we only need to fill in NBCode for foreign currencies
                                                                    ' this may be because we have no exact amount to test against the linit of NOK 100 000
                                                                    ' Then both codeNB and ExplanationNB must be filled
                                                                    If oPayment.MON_InvoiceCurrency <> "NOK" And oPayment.VB_Profile.FileSetups.Item(iFormat_ID).CodeNBCurrencyOnly Then
                                                                        ' add statebankcode for currencypayments only
                                                                        sNBCode = oPayment.VB_Profile.FileSetups.Item(iFormat_ID).CodeNB
                                                                        sNBText = oPayment.VB_Profile.FileSetups.Item(iFormat_ID).ExplanationNB
                                                                    Else
                                                                        'We've found information about the regulatoryreporting in the setup
                                                                        sNBCode = oPayment.VB_Profile.FileSetups.Item(iFormat_ID).CodeNB
                                                                        sNBText = oPayment.VB_Profile.FileSetups.Item(iFormat_ID).ExplanationNB
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                        'Will create a BETFOR04 transaction
                                                        sLine = WriteBETFOR04(oPayment, oInvoice, k, nSequenceNoTotal, sCompanyNo, sNBCode, sNBText, sSpecial)
                                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                        nInvoiceCounter = nInvoiceCounter + 1
                                                    ElseIf oPayment.PayType = "M" Or oPayment.PayType = "S" Then
                                                        'New by JanP 25.10.01 - Option to turn off export of masstranser for returnfiles
                                                        If bExportMassRecords Then
                                                            ' XNET 22.02.2012 IMPORTANT CHANGE - MAJOR ERROR HERE, THROUGH SEVEREAL YEARS !!
                                                            If oPayment.I_Account <> xAccount Then
                                                                ' Must create new BETFOR21 if change in debitaccount !!!
                                                                'BETFOR21 for Masstrans
                                                                sLine = WriteBETFOR21Mass(oPayment, nSequenceNoTotal, sCompanyNo, nMassTransferredAmount, iFormat_ID)
                                                                'First mass-transaction treated
                                                                bFirstMassTransaction = False
                                                                lNoOfRecordsInFile = lNoOfRecordsInFile + 1
                                                                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                            End If
                                                            xAccount = oPayment.I_Account
                                                            'Will create a BETFOR22 transaction
                                                            sLine = WriteBETFOR22(oPayment, oInvoice, k, nSequenceNoTotal, sCompanyNo, iFormat_ID)
                                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                            oPayment.Exported = True
                                                        End If
                                                        bLastPaymentWasMass = True
                                                    Else
                                                        'Will create a BETFOR23 transaction
                                                        iCount = 0
                                                        Do While iCount - 1 < oInvoice.Freetexts.Count
                                                            If iCount = 0 Then
                                                                iCount = 1
                                                                'nNoOfBETFOR = nNoOfBETFOR - 1
                                                                'nSequenceNoTotal = nSequenceNoTotal - 1
                                                            End If
                                                            If Val(oPayment.StatusCode) > 99 Then
                                                                If Val(CStr(oBabel.BankByCode)) = BabelFiles.Bank.DnB Then 'And oPayment.Invoices.Count = 0 Then
                                                                    ' Changed 14.11.2003
                                                                    ' Sometimes DnB has a BETFOR23, sometimes not for deleted transactions
                                                                    If oInvoice.Freetexts.Count > 0 Or oInvoice.Unique_Id <> "" Then
                                                                        If bFileFromBank Then
                                                                            oPayment.StatusCode = "02"
                                                                        Else
                                                                            oPayment.StatusCode = "00"
                                                                        End If
                                                                        ' 19.11.09 added parameter sSpecial
                                                                        sLine = WriteBETFOR23(oPayment, oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo, iFormat_ID, sSpecial)
                                                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                                    Else
                                                                        Exit For ' No KID, no Freetext, write no betfor23
                                                                    End If
                                                                Else
                                                                    If bFileFromBank Then
                                                                        oPayment.StatusCode = "02"
                                                                    Else
                                                                        oPayment.StatusCode = "00"
                                                                    End If
                                                                    ' 19.11.09 added parameter sSpecial
                                                                    sLine = WriteBETFOR23(oPayment, oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo, iFormat_ID, sSpecial)
                                                                    bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                                    nInvoiceCounter = nInvoiceCounter + 1
                                                                End If
                                                            Else
                                                                ' 19.11.09 added parameter sSpecial
                                                                sLine = WriteBETFOR23(oPayment, oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo, iFormat_ID, sSpecial)
                                                                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                                nInvoiceCounter = nInvoiceCounter + 1
                                                            End If
                                                            ' added 04.12.06 to jump out when structured invoices
                                                            If iStructuredPayment Then
                                                                Exit Do
                                                            End If

                                                        Loop
                                                    End If
                                                End If  'If Not oInvoice.Exported = True Then
                                            Next oInvoice
                                        End If

                                    End If 'bExportoPayment
                                Next oPayment ' payment


                                ' Added by janP 25.10.01
                                'If bExportMassRecords Or bWriteBETFOR99Later Then
                                If Not (bExportMassRecords = False And bLastPaymentWasMass) Then
                                    sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion, sSpecial)
                                    If oBatch.VB_ProfileInUse = True Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch = True Then
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                        Else
                                            bWriteBETFOR99Later = True
                                            ' added 06.07.2009
                                            ' we must subtract 1 from the daysequence, because we have been into WriteBETFOR99, without writing the line!

                                            'TUFTE
                                            ApplicationHeader(False, "-1")
                                        End If
                                    Else
                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                    End If
                                Else
                                    '    bWriteBETFOR99Later = True
                                    ' Masspayments and bExportMassRecords=False,
                                    ' Test if we had any non-mass payments before.
                                    ' 04.07.05: Changed from  > 0, to awoid Records with only BETFOR00/BETFOR99
                                    'If nNoOfBETFOR > 0 Then
                                    If nNoOfBETFOR > 1 Then
                                        ' We had invoice-records before the mass-records.
                                        ' Must write Betfor99
                                        sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion)
                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                    End If
                                End If
                                'We have to save the last used sequenceno. in some cases
                                'First check if we use a profile
                                If oBatch.VB_ProfileInUse = True Then
                                    'Then if it is a return-file.
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True And sSpecial <> "CONECTO" Then
                                        'No action
                                        'If oBabel.Special = "CRENO_OCRTP" Then
                                        If sSpecial = "CRENO_OCRTP" Then
                                            'Always use sequences at the filesetup-level
                                            If bWriteBETFOR99Later Then
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal
                                                sDummy = ApplicationHeader(False, "-1")
                                            Else
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal - 1
                                            End If
                                            oBatch.VB_Profile.Status = 2
                                            'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 1
                                            'We have to omit BETFOR00 in the future (if we don't change between domestic/international)
                                            bWriteBETFOR00 = False
                                        End If
                                    Else
                                        Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                            'Use the sequenceno from oBabel
                                            Case 0
                                                'No action
                                                'Use the seqno at filesetup level
                                            Case 1
                                                If bWriteBETFOR99Later Then
                                                    oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal
                                                    sDummy = ApplicationHeader(False, "-1")
                                                Else
                                                    oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal - 1
                                                    ' For Scandza - one total sequence series for all clients. Save in Filesetup2 !!!!
                                                    If sSpecial = "SCANDZA" Then
                                                        oBatch.VB_Profile.FileSetups(2).SeqNoTotal = nSequenceNoTotal - 1
                                                    End If

                                                End If
                                                oBatch.VB_Profile.Status = 2
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 1
                                                'We have to omit BETFOR00 in the future (if we don't change between domestic/international)
                                                bWriteBETFOR00 = False
                                                ' New by JanP 26.11.01
                                                ' Must also reduce day-sequence by one, since it has been added
                                                ' one in WriteBetfor99, which will never be used!
                                                ' ??????? WRONG ????? Moved to bWriteBETFOR99Later, up!sDummy = ApplicationHeader(False, "-1")
                                                'Use seqno per client per filesetup
                                            Case 2
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).SeqNoTotal = nSequenceNoTotal - 1
                                                oBatch.VB_Profile.Status = 2
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 2
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).Status = 1
                                        End Select
                                    End If
                                End If

                            End If
                    'bLastBatchWasDomestic = False 'Changed by JanP 29.10
                    'bFirstBatch = False  ' Changed by Janp 25.10

                    ' XNET 28.05.2013
                    '         --- save last sequenceno
                    ' For Visma save Sequenceno for EnterpriseNo/Batch;
                    If bVismaIntegrator Then
                        ' at this stage we have added one extra to nSequenceNoTotal
                        nSequenceNoTotal = nSequenceNoTotal - 1
                        Visma_SaveSequenceNumber(oExternalUpdateConnection, oExternalReadConnection, oBatch.I_EnterpriseNo, oBatch.I_Branch, nSequenceNoTotal, oBabel.Visma_ProfileID, "BabelBank_" & oBabel.Visma_SystemDatabase, oBabel.Visma_User)
                    End If

                        Next oBatch 'batch

                    End If
                End If

            Next oBabel 'Babelfile

            'Have to write BETFOR99 if keepbatch is true
            If bWriteBETFOR99Later Then
                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)

                'Added 09.08.2006
                'If seqno at clientlevel and keepbatches is set to false then we we save
                ' nSequenceNoTotal - 1, but when BETFOR99 is written we must save nSequenceNoTotal
                'First, check if we have at least 1 Babelfile
                If oBabelFiles.Count > 0 Then
                    'Then check if we use a profile
                    If oBabelFiles.Item(1).VB_ProfileInUse = True Then
                        'Then if it is a send-file.
                        'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = False Then

                            'Then if it is seqno at the client-level
                            If oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).SeqnoType = 2 Then
                                oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).SeqNoTotal = nSequenceNoTotal - 1
                                oBabelFiles.Item(1).VB_Profile.Status = 2
                                oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).Status = 2
                                oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).Status = 1
                            End If
                        End If
                    End If
                End If
            End If

            'For those who use vbBabel
            nSequenceNumberEnd = nSequenceNoTotal

            'Old code
            ''Have to write BETFOR99 if keepbatch is true
            'If bWriteBETFOR99Later Then
            '    bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
            'End If

            'oFile.Close()
            'oFs.Close

            ' 06.04.2017 added And Not IsThisDNBIntegrator()
            If Not bVismaIntegrator And Not IsThisDNBIntegrator() Then 'Added 27.03.2017 do delete empty files (for SISMO and and split to TBRI/TBMI and DO/IN)
                ' 12.12.2017 - added 
                If Not Left(sSpecial, 5) = "NHST_" Then
                    If Not bAtLeastOneRecordIsExported Then
                        '19.11.2019 - Added this IF. OFile kept the file, so it was writeprotected.
                        If Not oFile Is Nothing Then
                            oFile.Close()
                            oFile = Nothing
                        End If
                        oFs.DeleteFile(sFilenameOut)
                    End If
                End If
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteTelepay2File" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteTelepay2File = True

    End Function
    Private Function WriteBETFOR00(ByRef oBatch As Batch, ByRef bFirstLine As Boolean, ByRef nSequenceNoTotal As Double, ByRef sBranch As String, ByRef sCompanyNo As String, ByRef sSpecial As String, Optional ByRef sClientNo As String = "") As String

        Dim sLine As String

        sLine = ""
        'Build AH, send Returncode
        'bFirstLine is used to set the sequenceno in the aplicationheader.
        ' Added 06.07.06 due to problems with Nordea-files

        '29.04.2015 - Kjell and Jan-Petter agrees that the statuscode should ALWAYS be retrieved from Batch for BETFOR00/99
        sLine = ApplicationHeader(bFirstLine, oBatch.StatusCode)
        '        If oBatch.Payments.Count > 0 Then
        'If sSpecial = "GJENSIDIGE_OW_FROMBANK" Then
        '    sLine = ApplicationHeader(bFirstLine, oBatch.StatusCode)
        'Else
        '    sLine = ApplicationHeader(bFirstLine, oBatch.Payments(1).StatusCode)
        'End If
        'Else
        'sLine = ApplicationHeader(bFirstLine, (oBatch.StatusCode))
        'End If
        sLine = sLine & "BETFOR00"

        ' added next If  28.10.2016
        If IsISO20022ReturnFile(oBatch.ImportFormat) Then
            sCompanyNo = oBatch.I_EnterpriseNo
            sBranch = oBatch.I_Branch
        End If
        ' 02.05.2022 - no enerpriseno for NHST_LONN:
        If sSpecial = "NHST_LONN" Then
            sCompanyNo = oBatch.VB_Profile.CompanyNo
        End If

        ' 17.10.2016 For Skuld, there are three clients. Use the enterpriseno picked up from the stored payments in BabelBanks database
        ' 24.10.2016 kuttet ut bruken av idiotiske IndexOf
        'If sSpecial.IndexOf("SKULD") > 0 Then
        If InStr(sSpecial, "SKULD") > 0 Then
            sCompanyNo = oBatch.I_EnterpriseNo
        End If
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If

        ' XokNET 08.02.2012
        ' For NHST_LONN, find Divisjon in inputfile. If not specified, use LONN
        If sSpecial = "NHST_LONN" Or sSpecial = "CONECTO" Then
            If oBatch.I_Branch <> "00000000000" And Not EmptyString(oBatch.I_Branch) Then
                sBranch = oBatch.I_Branch
            Else
                sBranch = "LONN"
            End If
        End If

        If sBranch <> "" Then
            'Uses Branch from the database
            ' New 04.11.05 to be able to redo from branch to blank branch
            If UCase(sBranch) = "INGEN" Or UCase(sBranch) = "BLANK" Then
                sBranch = Space(11)
            End If
            sLine = sLine & PadRight(sBranch, 11, " ")
        Else
            sLine = sLine & PadRight(oBatch.I_Branch, 11, " ")
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & Space(6)
        If Len(oBatch.DATE_Production) = 8 And oBatch.DATE_Production <> "19900101" Then
            sLine = sLine & Right(oBatch.DATE_Production, 4) 'oBatch.DATE_Production
        Else
            sLine = sLine & VB6.Format(Now, "MMDD")
        End If
        If sSpecial = "HAFSLUND" Or sSpecial = "HAFSLUNDTELEPAY" Or sSpecial = "FJERNVARMETELEPAY" Or sSpecial = "NORGESENERGITELEPAY" Then
            ' 27.03.2009 added special for Hafslund, needs password in file to KLink
            sLine = sLine & PadRight(oBatch.VB_Profile.Custom3Value, 10, " ") ' Password, 85-94
        Else
            sLine = sLine & Space(10) ' Password, 85-94
        End If
        sLine = sLine & "VERSJON002"
        sLine = sLine & Space(10)
        sLine = sLine & PadRight(oBatch.OperatorID, 11, " ") '115-125  ' 01.04.2009, changed from padleft to padright
        'No use of SIGILL
        sLine = sLine & Space(1)
        sLine = sLine & New String("0", 26)
        sLine = sLine & Space(1)
        sLine = sLine & Space(143)

        If Len(oBatch.REF_Own) > 0 Then
            sLine = sLine & PadRight(oBatch.REF_Own, 15, " ") ' 13.09.06 changed from padleft
        Else
            'If oBatch.Cargo.Special = "�KONOMISERVICE" Then  ' Added 17.11.06
            If sSpecial = "�KONOMISERVICE" Then ' Changed 23.11.06
                ' For �konomiservice we must create a new BETFOR00/99 for each client!!!
                If EmptyString(sClientNo) Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().VB_ClientNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sLine = sLine & PadRight(oBatch.Payments(1).VB_ClientNo, 15, " ") 'On first batch we have not yet reachead a payment, and sClientNo is not passed as paramater
                Else
                    sLine = sLine & PadRight(sClientNo, 15, " ")
                End If
            Else
                sLine = sLine & Space(15) ' Egenreferanse - Added 27.05.04
            End If
        End If
        sLine = sLine & Space(9)


        WriteBETFOR00 = sLine

    End Function
    Private Function WriteBETFOR01(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef iFormat_ID As Short, ByRef sSpecial As String) As String
        Dim sErrorMessage As String
        Dim sLine As String
        'Dim sPayCode As String, iCount As Integer
        'Dim oInvoice As Invoice

        sLine = ""
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR01"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If

        If bCurrentIsTBIO Then
            ' TBIO-format has I_Account in BETFOR02, pos 245-279
            sLine = sLine & "00000000000"
        Else
            ' Telepay "classic"
            If sSpecial = "GASSCO_RETURN" Then
                sLine = sLine & PadRight(oPayment.I_Account, 11, " ")
            Else
                sLine = sLine & PadLeft(oPayment.I_Account, 11, "0")   '09.09.2016 endret fra padright(
            End If

        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        ' 06.12.2018 - Sparebank1 has extended their Ref_Bank1 (NtryRef) from 6 to 11 in Camt.054
        ' All over, the best will be to consider the 6 lastmost digits as the most relevant, and unique ones.
        ' Thus, has changed the line below from
        'sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")
        '   to
        sLine = sLine & PadRight(Right(oPayment.Payment_ID, 6), 6, " ")  '(use last 6 digits if more than 6)
        '13.01.2009 - added the IF below due to Francis
        If oPayment.VB_ProfileInUse Then
            ' 04.05.06 Added new test about date
            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateAll. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateAll Then
                ' Set all dates to todays date
                ' 04.01.2008
                ' Changed to give us next bankday, instead of today.
                oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "NO", 0)) 'DateToString(Date)
            End If
        End If

        ' 04.05.06 Added new test about date
        If StringToDate((oPayment.DATE_Payment)) < Date.Today Then
            '13.01.2009 - added the IF below due to Francis
            If oPayment.VB_ProfileInUse Then
                'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateDue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateDue Then
                    ' Set date to todays date
                    ' 04.01.2008
                    ' Changed to give us next bankday, instead of today.
                    oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "NO", 0)) 'DateToString(Date)
                End If
            End If
        End If

        sLine = sLine & Right(oPayment.DATE_Payment, 6)
        sLine = sLine & Left(oPayment.REF_Own & Space(30), 30)
        ' XNET 23.05.2013 - no need for Betalingsvalutasort if same as Fakturavaluta
        'If Len(Trim$(oPayment.MON_TransferCurrency)) <> 3 Then
        If Len(Trim$(oPayment.MON_TransferCurrency)) <> 3 Or (oPayment.MON_TransferCurrency = oPayment.MON_InvoiceCurrency) Then
            sLine = sLine & "   " 'Transf.cur will be equal to inv.cur.
        Else
            sLine = sLine & Trim(oPayment.MON_TransferCurrency)
        End If
        If Len(Trim(oPayment.MON_InvoiceCurrency)) <> 3 Then
            sErrorMessage = LRS(35009, (oPayment.MON_InvoiceCurrency), "Telepay2")
            sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & sImportFilename
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oPayment.MON_InvoiceAmount
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
            Err.Raise(35009, , sErrorMessage)
        Else
            sLine = sLine & Trim(oPayment.MON_InvoiceCurrency)
        End If
        If oPayment.MON_ChargeMeAbroad = True Then
            sLine = sLine & "OUR"
        Else
            sLine = sLine & "BEN"
        End If
        If oPayment.MON_ChargeMeDomestic = True Then
            sLine = sLine & "OUR"
        Else
            sLine = sLine & "BEN"
        End If
        ' This is notification by receiving bank
        ' 05.12.06 added Or oPayment.ToOwnAccount
        If oPayment.NOTI_NotificationParty = 2 Or oPayment.ToOwnAccount Then
            ' new 19.01.05 for TEEKAY
            If oPayment.ToOwnAccount Then
                If oPayment.NOTI_NotificationMessageToBank = "/REC/KONCERN" Then ' Added 07.10.05 ref DnB NIBS
                    sLine = sLine & PadRight("/REC/KONCERN/", 30, " ")
                Else
                    ' New 22.11.05, added PGS 28.06.06
                    'If oPayment.Cargo.Special = "TEEKAY" Or _
                    ''    oPayment.Cargo.Special = "DNBANOKK" Or _
                    ''    oPayment.Cargo.Special = "PGSUK" _
                    '' Changed 23.11.06
                    If sSpecial = "TEEKAY" Or sSpecial = "DNBANOKK" Or sSpecial = "PGSUK" Or sSpecial = "PGSTREASURY" Then
                        sLine = sLine & PadRight("/REC/KONCERN/", 30, " ")
                    Else
                        sLine = sLine & PadRight("/REC/INTC/", 30, " ")
                    End If
                End If
            Else
                'Else
                '    ' Changed 07.10.05
                '   'sLine = sLine & Space(30)
                '   sLine = sLine & PadRight(oPayment.NOTI_NotificationMessageToBank, 30, " ")
                'End If
                ''''''Else
                ' Also notification by RECEIVING BANK
                Select Case oPayment.NOTI_NotificationType
                    Case "PHONE", "TELEFON"
                        sLine = sLine & "PHONE " & PadRight(oPayment.NOTI_NotificationIdent, 24, " ")
                    Case "TELEX"
                        sLine = sLine & "TELEX " & PadRight(oPayment.NOTI_NotificationIdent, 24, " ")
                    Case "FAX"
                        sLine = sLine & "FAX " & PadRight(oPayment.NOTI_NotificationIdent, 26, " ")
                    Case "OTHER"
                        sLine = sLine & "OTHER " & PadRight(oPayment.NOTI_NotificationIdent, 26, " ")
                    Case Else
                        sLine = sLine & PadRight(oPayment.NOTI_NotificationMessageToBank, 30, " ")
                End Select
            End If
        Else
            sLine = sLine & Space(30)
        End If
        'sLine = sLine & PadRight(oPayment.NOTI_NotificationMessageToBank, 30, " ")
        If oPayment.Priority = True Then
            sLine = sLine & "J"
        Else
            sLine = sLine & " "
        End If
        sLine = sLine & PadLeft(CStr(Int(oPayment.ERA_ExchRateAgreed)), 4, "0") '160
        ' Changed 21.10.05 - gave wrong results
        'sLine = sLine & PadRight(oPayment.ERA_ExchRateAgreed - Int(oPayment.ERA_ExchRateAgreed), 4, "0")
        sLine = sLine & PadLeft(CStr(oPayment.ERA_ExchRateAgreed * 10000 - (Int(oPayment.ERA_ExchRateAgreed) * 10000)), 4, "0")

        sLine = sLine & PadRight(oPayment.FRW_ForwardContractNo, 6, " ") '168
        ' changed 20.09.05 - Gave wrong results!
        sLine = sLine & PadLeft(CStr(Int(oPayment.FRW_ForwardContractRate)), 4, "0") '174
        'sLine = sLine & PadRight(oPayment.FRW_ForwardContractRate - Int(oPayment.FRW_ForwardContractRate), 4, "0")
        sLine = sLine & PadLeft(CStr(oPayment.FRW_ForwardContractRate * 10000 - (Int(oPayment.FRW_ForwardContractRate) * 10000)), 4, "0")
        If oPayment.Cheque = BabelFiles.ChequeType.noCheque Then
            sLine = sLine & " " '182
        ElseIf oPayment.Cheque = BabelFiles.ChequeType.SendToPayer Then
            sLine = sLine & "2" ' 27.05.04 Changed from 0
        ElseIf oPayment.Cheque = BabelFiles.ChequeType.SendToReceiver Then
            sLine = sLine & "1"
        End If
        '    If oPayment.DATE_Value <> "19900101" Then
        '        sLine = sLine & Right$(oPayment.DATE_Value, 6)
        '    Else
        'Not logical in Norway, hardcode spaces
        sLine = sLine & Space(6) ' 183 - Valutering mottakende bank
        '    End If
        sLine = sLine & Space(2) '189
        If oPayment.StatusCode <> "02" Then
            sLine = sLine & "000000000000" '191 reell kurs
        Else
            If oPayment.MON_TransferCurrency = oPayment.MON_AccountCurrency Then
                If oPayment.MON_TransferCurrency <> "NOK" Then
                    'If so use the local exchangerate.
                    sLine = sLine & PadLeft(CStr(Int(oPayment.MON_LocalExchRate)), 4, "0")
                    sLine = sLine & PadRight(Mid(Replace(CStr(System.Math.Round(oPayment.MON_LocalExchRate - Int(oPayment.MON_LocalExchRate), 8)), ",", ""), 2), 8, "0")
                Else

                    sLine = sLine & "000000000000"
                End If
            ElseIf oPayment.MON_AccountExchRate <> oPayment.MON_LocalExchRate Or sSpecial = "FRANKMOHN_RETURN" Then
                sLine = sLine & PadLeft(CStr(Int(oPayment.MON_AccountExchRate)), 4, "0")
                sLine = sLine & PadRight(Mid(Replace(CStr(System.Math.Round(oPayment.MON_AccountExchRate - Int(oPayment.MON_AccountExchRate), 8)), ",", ""), 2), 8, "0")
            Else
                sLine = sLine & "000000000000"
            End If
        End If
        sLine = sLine & PadLeft(oPayment.REF_Bank2, 12, " ") '203 Effektueringsref2
        If oPayment.MON_AccountAmount = 0 Then 'Belastest bel�p 215-230
            sLine = sLine & New String("0", 16)
        Else
            sLine = sLine & PadLeft(Str(oPayment.MON_AccountAmount), 16, "0")
        End If
        If oPayment.MON_TransferredAmount = 0 Or oPayment.StatusCode <> "02" Then 'XNET 15.06.2013 - Added test for statuscode
            sLine = sLine & New String("0", 16) '231 Overf�rt bel�p
        Else
            sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 16, "0")
        End If
        '29.07.2008 - Changed the variable ClientNumber from Integer to string
        ' XNET 02.07.2013 changed If-test below
        'If Len(sClientNumber) > 0 Then
        If Len(oPayment.I_Client) > 0 Then
            '18.12.2014 - Changed from PadLeft to PadRight
            sLine = sLine & PadRight(oPayment.I_Client, 5, " ") '247 Klient
        Else
            sLine = sLine & Space(5) '247 Klient
        End If
        sLine = sLine & PadLeft(oPayment.REF_Bank1, 6, "0") '252 Effektueringsref.1
        sLine = sLine & PadRight(oPayment.ERA_DealMadeWith, 6, " ") '258 Avtalt med
        If oPayment.Cancel = True Then
            sLine = sLine & "S" '264 Slettekode
        Else
            sLine = sLine & Space(1)
        End If
        sLine = sLine & Space(1) '265 Clearingcode, not in use
        If oPayment.DATE_Value = "19900101" Or oPayment.StatusCode <> "02" Then 'XNET 15.06.2013 - Added test for statuscode
            'Removed next IF-statement, because nothing is written if statuscode <> "02"
            'If oPayment.StatusCode = "02" Then
            sLine = sLine & New String("0", 6) '266 Valuteringsdato
            'End If
        Else
            sLine = sLine & Right(oPayment.DATE_Value, 6) 'Format(oPayment.DATE_Value, "YYMMDD")
        End If
        sLine = sLine & PadLeft(Format(oPayment.MON_ChargesAmount, "###0"), 9, "0")  '272 Provisjon, 2 desimaler
        If oPayment.StatusCode <> "02" Then
            sLine = sLine & "000000000000" '281 Kurs mot NOK
        Else
            ' ??????????????
            'If (oPayment.MON_LocalExchRate = 100 And oPayment.MON_TransferCurrency <> "NOK") Or oPayment.MON_LocalExchRate = 100 And oPayment.MON_TransferCurrency = "NOK" Then
            '    sLine = sLine & "000000000000"
            'Else
            sLine = sLine & PadLeft(CStr(Int(oPayment.MON_LocalExchRate)), 4, "0")
            sLine = sLine & PadRight(Mid(Replace(CStr(System.Math.Round(oPayment.MON_LocalExchRate - Int(oPayment.MON_LocalExchRate), 8)), ",", ""), 2), 8, "0")
            'End If
        End If
        sLine = sLine & Space(1) ' 293 Slette�rsak
        sLine = sLine & Space(16) '294 Bestillt overf�rt bel�p
        sLine = sLine & Space(1) '310 Info vedr�rende prising
        sLine = sLine & Space(10)

        WriteBETFOR01 = UCase(sLine)


    End Function
    Private Function WriteBETFOR02(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef sSpecial As String) As String
        Dim sAccount As String
        Dim sLine As String

        sLine = ""
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR02"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        If bCurrentIsTBIO Then
            ' TBIO-format has I_Account in BETFOR02, pos 245-279
            sLine = sLine & "00000000000"
        Else
            ' Telepay "classic"
            If sSpecial = "GASSCO_RETURN" Then
                sLine = sLine & PadRight(oPayment.I_Account, 11, " ")
            Else
                sLine = sLine & PadLeft(oPayment.I_Account, 11, "0")     '09.09.2016 endret fra padright(
            End If
        End If

        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        ' 06.12.2018 - Sparebank1 has extended their Ref_Bank1 (NtryRef) from 6 to 11 in Camt.054
        ' All over, the best will be to consider the 6 lastmost digits as the most relevant, and unique ones.
        ' Thus, has changed the line below from
        'sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ") '75
        '   to
        sLine = sLine & PadRight(Right(oPayment.Payment_ID, 6), 6, " ")  ' 75(use last 6 digits if more than 6)
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCode, 11, " ") '81
        sLine = sLine & PadRight(oPayment.BANK_Name, 35, " ") '92
        sLine = sLine & PadRight(oPayment.BANK_Adr1, 35, " ") '127
        sLine = sLine & PadRight(oPayment.BANK_Adr2, 35, " ") '162
        sLine = sLine & PadRight(oPayment.BANK_Adr3, 35, " ") '197
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCodeCorrBank, 11, " ") '232
        If Len(oPayment.BANK_CountryCode) <> 2 Then
            'FIXERROR Babelerror, warning not compulsory?
            sLine = sLine & "  " '243
        Else
            sLine = sLine & oPayment.BANK_CountryCode
        End If

        ' Reserved space for branchNo, 245,15
        ' In Telepay format, must add SC, FW, etc
        Select Case oPayment.BANK_BranchType
            Case BabelFiles.BankBranchType.Fedwire
                sLine = sLine & PadRight("FW" & oPayment.BANK_BranchNo, 15, " ")
            Case BabelFiles.BankBranchType.SortCode
                sLine = sLine & PadRight("SC" & oPayment.BANK_BranchNo, 15, " ")
            Case BabelFiles.BankBranchType.Bankleitzahl
                sLine = sLine & PadRight("BL" & oPayment.BANK_BranchNo, 15, " ")
            Case BabelFiles.BankBranchType.Chips
                sLine = sLine & PadRight("CH" & oPayment.BANK_BranchNo, 15, " ")
            Case Else
                sLine = sLine & PadRight(oPayment.BANK_BranchNo, 15, " ")
        End Select

        If bCurrentIsTBIO Then
            sAccount = Trim(oPayment.I_Account)
            sLine = sLine & PadRight(sAccount, 35, " ") '260
        Else
            sLine = sLine & Space(35)
        End If

        sLine = sLine & Space(26) '295

        WriteBETFOR02 = UCase(sLine)


    End Function
    Private Function WriteBETFOR03(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef sSpecial As String) As String

        Dim sLine As String
        'Dim sPayCode As String, iCount As Integer
        'Dim oInvoice As Invoice


        sLine = ""
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR03"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo '49
        End If
        If bCurrentIsTBIO Then
            ' TBIO-format has I_Account in BETFOR02, pos 260-294
            sLine = sLine & "00000000000" '60
        Else
            ' Telepay "classic"
            If sSpecial = "GASSCO_RETURN" Then
                sLine = sLine & PadRight(oPayment.I_Account, 11, " ")
            Else
                sLine = sLine & PadLeft(oPayment.I_Account, 11, "0")     '09.09.2016 endret fra padright(
            End If
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        ' 06.12.2018 - Sparebank1 has extended their Ref_Bank1 (NtryRef) from 6 to 11 in Camt.054
        ' All over, the best will be to consider the 6 lastmost digits as the most relevant, and unique ones.
        ' Thus, has changed the line below from
        'sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ") '75
        '   to
        sLine = sLine & PadRight(Right(oPayment.Payment_ID, 6), 6, " ")  ' 75(use last 6 digits if more than 6)

        sLine = sLine & PadRight(oPayment.E_Account, 35, " ") '81
        sLine = sLine & PadRight(oPayment.E_Name, 35, " ") '116
        sLine = sLine & PadRight(oPayment.E_Adr1, 35, " ") '151
        sLine = sLine & PadRight(oPayment.E_Adr2, 35, " ") '186
        sLine = sLine & PadRight(oPayment.E_Adr3, 35, " ") '221
        If Len(oPayment.E_CountryCode) <> 2 Then
            If Len(oPayment.BANK_CountryCode) = 2 Then
                sLine = sLine & oPayment.BANK_CountryCode '256
            Else
                'Maybe we should raise an error!
                sLine = sLine & "  "
            End If
        Else
            sLine = sLine & oPayment.E_CountryCode
        End If

        ' This is notification by payers bank
        If oPayment.NOTI_NotificationParty = 1 Or oPayment.NOTI_NotificationParty = 3 Then
            'Receiver shall be notified by payors bank
            If oPayment.NOTI_NotificationType = "TELEX" Then
                If oPayment.NOTI_NotificationIdent = "" Or oPayment.NOTI_NotificationAttention = "" Then
                    sLine = sLine & Space(41)
                    'FIX: Something is wrong. Some fields are compulsory. Issue a warning.
                Else
                    sLine = sLine & "T" '258
                    'Both Telex countrycode and number are stored in Ident.
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 20, " ") '259
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationAttention, 20, " ") '279
                End If
            ElseIf oPayment.NOTI_NotificationType = "FAX" Then
                If oPayment.NOTI_NotificationIdent = "" Or oPayment.NOTI_NotificationAttention = "" Then
                    sLine = sLine & Space(41)
                    'FIX: Something is wrong. Some fields are compulsory. Issue a warning.
                Else
                    sLine = sLine & "F" & "  "
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 18, " ")
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationAttention, 20, " ")
                End If
            Else
                sLine = sLine & Space(41)
            End If
        Else
            'No notification by payors bank
            sLine = sLine & Space(41)
        End If
        sLine = sLine & Space(22) '299

        WriteBETFOR03 = UCase(sLine)


    End Function
    Private Function WriteBETFOR04(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef iSerialNo As Double, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, Optional ByRef sNBCode As String = "", Optional ByRef sNBText As String = "", Optional ByRef sSpecial As String = "") As String

        Dim sLine As String
        Dim iCount As Short
        Dim oFreeText As Freetext

        iCount = 0
        sLine = ""
        sLine = ApplicationHeader(False, (oInvoice.StatusCode))
        sLine = sLine & "BETFOR04"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo '49
        End If
        If bCurrentIsTBIO Then
            ' TBIO-format has I_Account in BETFOR02, pos 260-294
            sLine = sLine & "00000000000" '60
        Else
            ' Telepay "classic"
            If sSpecial = "GASSCO_RETURN" Then
                sLine = sLine & PadRight(oPayment.I_Account, 11, " ")
            Else
                sLine = sLine & PadLeft(oPayment.I_Account, 11, "0")     '09.09.2016 endret fra padright(
            End If
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        ' 06.12.2018 - Sparebank1 has extended their Ref_Bank1 (NtryRef) from 6 to 11 in Camt.054
        ' All over, the best will be to consider the 6 lastmost digits as the most relevant, and unique ones.
        ' Thus, has changed the line below from
        'sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ") '75
        '   to
        sLine = sLine & PadRight(Right(oPayment.Payment_ID, 6), 6, " ")  ' 75(use last 6 digits if more than 6)
        For Each oFreeText In oInvoice.Freetexts
            iCount = iCount + 1
            If iCount > 1 Then
                'FIXERROR: Can't be more than 1 textlines in Telepay International
            Else
                ' 18.01.2010 added KID Utland
                If Not EmptyString((oInvoice.Unique_Id)) Then
                    sLine = sLine & Left(oInvoice.Unique_Id & Space(35), 35) '81
                Else
                    sLine = sLine & Left(oFreeText.Text & Space(35), 35) '81
                End If
            End If
        Next oFreeText
        ' added 18.01.2010
        ' There may be KID only, no freetext!
        If oInvoice.Freetexts.Count = 0 And Not EmptyString((oInvoice.Unique_Id)) Then
            iCount = iCount + 1
            sLine = sLine & Left(oInvoice.Unique_Id & Space(35), 35) '81
        End If
        ' New 19.04.05
        ' Problem if no freetexts - will then miss 35 characters in output!
        If iCount = 0 Then
            sLine = sLine & Space(35)
        End If

        sLine = sLine & Left(oInvoice.REF_Own & Space(35), 35) '116
        sLine = sLine & PadLeft(Str(System.Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0") '151
        If oInvoice.MON_InvoiceAmount < 0 Then
            sLine = sLine & "K" '166
        Else
            sLine = sLine & "D"
        End If
        'If Not (EmptyString(sNBText) And sNBCode <> -1) Then
        ' 09.02.2010
        ' KJELL PELL - se linjen over - dette gir feil, og ser helt KOKO ut.
        ' Har forel�pig endret til <> "-1"
        ' XNET 06.11.2012 - changed this. Hvis det er en NBcode og tekst med i filen inn, bruk denne !
        '    If Not (EmptyString(sNBText) And sNBCode <> "-1") Then
        '        sLine = sLine & PadRight(sNBCode, 6, " ")  '167
        '        sLine = sLine & PadRight(sNBText, 60, " ") '173
        '    Else
        '        sLine = sLine & PadRight(oInvoice.STATEBANK_Code, 6, " ")  '167
        '        sLine = sLine & PadRight(oInvoice.STATEBANK_Text, 60, " ") '173
        '    End If
        If Not EmptyString(oInvoice.STATEBANK_Code) Then
            sLine = sLine & PadRight(oInvoice.STATEBANK_Code, 6, " ")  '167
        ElseIf sNBCode <> "-1" Then
            sLine = sLine & PadRight(sNBCode, 6, " ")  '167
        Else
            sLine = sLine & Space(6)  '167
        End If
        If Not EmptyString(oInvoice.STATEBANK_Text) Then
            sLine = sLine & PadRight(oInvoice.STATEBANK_Text, 60, " ") '173
        ElseIf Not EmptyString(sNBText) Then
            sLine = sLine & PadRight(sNBText, 60, " ") '173
        Else
            sLine = sLine & Space(60)  '173
        End If

        'FIX: To own account is not imported from Telepay
        ' Endret 19.01.05, ref TEEKAY
        If oPayment.ToOwnAccount Then
            sLine = sLine & "J" '233 Til egen konto
        Else
            sLine = sLine & Space(1) '233 Til egen konto
        End If

        sLine = sLine & Space(1) '234
        'Changed 26.08.2010 - Feltet er alfanummerisk og skal paddes med blanke
        sLine = sLine & Space(6) '235
        'Old code
        'sLine = sLine & String(6, "0")  '235
        sLine = sLine & Space(1) '241
        'Changed 26.08.2010 - Feltet er alfanummerisk og skal paddes med blanke
        sLine = sLine & Space(6) '242
        'Old code
        'sLine = sLine & String(6, "0")  '242
        sLine = sLine & Space(45) '248
        'sLine = sLine & Space(1)       '293 KID Utland - not implemented yet
        ' 18.01.2010 added KID Utland
        If Not EmptyString((oInvoice.Unique_Id)) Then
            sLine = sLine & "K" '293 KID Utland
        Else
            ' as pre 18.01.2010
            sLine = sLine & Space(1) '293 KID Utland
        End If
        '18.12.2014 - Changed the IF below
        'If oInvoice.StatusCode = "00" Then
        If oInvoice.StatusCode <> "02" Then
            sLine = sLine & "000"
        Else
            sLine = sLine & PadLeft(Str(iSerialNo), 3, "0") '294
        End If
        sLine = sLine & Space(24)

        WriteBETFOR04 = UCase(sLine)


    End Function

    Private Function WriteBETFOR21Invoice(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef iFormat_ID As Short, ByRef sSpecial As String) As String

        Dim sLine As String
        Dim sPayCode As String
        Dim iCount As Short
        Dim oInvoice As Invoice
        Dim sErrorMessage As String

        ' Reset some controlvalues for each payment:
        '------------------------------------
        iStructuredPayment = -1
        iNoOfBETFOR22_23 = 0
        iNoOfTextLinesInBETFOR23 = 0


        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "TELEPAY")
        ' "New" PayCode 603 when no account specified
        If oPayment.E_Account = "00000000000" Or Len(oPayment.E_Account) = 0 Then
            sPayCode = "603"
        End If

        sLine = ""
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR21" '41
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0") '49
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & PadLeft(oPayment.I_Account, 11, "0") '60      '09.09.2016 endret fra padright(
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71

        ' 06.12.2018 - Sparebank1 has extended their Ref_Bank1 (NtryRef) from 6 to 11 in Camt.054
        ' All over, the best will be to consider the 6 lastmost digits as the most relevant, and unique ones.
        ' Thus, has changed the line below from
        'sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ") '75
        '   to
        sLine = sLine & PadRight(Right(oPayment.Payment_ID, 6), 6, " ")  ' 75(use last 6 digits if more than 6)


        If UCase(sSpecial) <> "NORGESKORFORBUND" Then ' Added this if 02.10.2007
            ' 04.05.06 Added new test about date
            If oPayment.VB_ProfileInUse Then
                'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateAll. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateAll Then
                    ' Set all dates to todays date
                    ' 04.01.2008
                    ' Changed to give us next bankday, instead of today.
                    oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "NO", 0)) 'DateToString(Date)
                End If
            End If

            If oPayment.VB_ProfileInUse Then
                ' 04.05.06 Added new test about date
                If StringToDate((oPayment.DATE_Payment)) <= Date.Today Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateDue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateDue Then
                        ' Set date to todays date
                        ' 04.01.2008
                        ' Changed to give us next bankday, instead of today.
                        oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "NO", 0)) 'DateToString(Date)
                    End If
                End If
            End If
        End If

        sLine = sLine & Right(oPayment.DATE_Payment, 6) '81
        sLine = sLine & Left(oPayment.REF_Own & Space(30), 30) '87
        sLine = sLine & Space(1) '117
        'FIXERROR: Babelerror if E_Accoun longer than 11 or alfanumeric
        If oPayment.E_Account = "00000000000" Or Len(oPayment.E_Account) = 0 Or oPayment.PayCode = "160" Then
            sLine = sLine & "00000000019"
            sPayCode = "603"
        Else
            sLine = sLine & PadRight(oPayment.E_Account, 11, " ") '118
        End If

        If UCase(sSpecial) <> "NORGESKORFORBUND" Then ' Added this if 02.10.2007
            ' added 04.05.06 - Check accountno
            If oPayment.VB_ProfileInUse Then
                'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.VB_Profile.FileSetups(iFormat_ID).CtrlAccount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If oPayment.VB_Profile.FileSetups(iFormat_ID).CtrlAccount Then
                    If Not CheckAccountNoNOR((oPayment.E_Account)) Then
                        sErrorMessage = LRS(35047, (oPayment.E_Account), "Telepay2")
                        sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & sImportFilename
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oPayment.MON_InvoiceAmount / 100
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                        Err.Raise(35047, , sErrorMessage)
                    End If
                End If
            End If
        End If
        sLine = sLine & PadRight(oPayment.E_Name, 30, " ") '129
        sLine = sLine & PadRight(oPayment.E_Adr1, 30, " ") '159
        sLine = sLine & PadRight(oPayment.E_Adr2, 30, " ") '189
        'FIXERROR: Babelerror if E_Zip longer than 4 or alfanumeric
        'If Len(Trim$(oPayment.E_Zip)) = 0 Then
        '    sLine = sLine & "0585"  ' In Telepay Zip is mandatory, but not used!
        'Else
        ' 04.05.06 Changed code for zip with error
        If Len(Trim(oPayment.E_Zip)) <> 4 Or oPayment.E_Zip = "0000" Then
            If UCase(sSpecial) <> "NORGESKORFORBUND" Then ' Added this if 02.10.2007
                If oPayment.VB_ProfileInUse Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.VB_Profile.FileSetups(iFormat_ID).CtrlZip. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If oPayment.VB_Profile.FileSetups(iFormat_ID).CtrlZip Then
                        sErrorMessage = LRS(35046, (oPayment.E_Zip), "Telepay2")
                        sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & sImportFilename
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oPayment.MON_InvoiceAmount / 100
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                        Err.Raise(35046, , sErrorMessage)
                    Else
                        ' fake zip
                        sLine = sLine & "9999"
                    End If
                Else
                    ' fake zip
                    sLine = sLine & "9999"
                End If
            End If
        Else
            sLine = sLine & PadRight(oPayment.E_Zip, 4, " ") '219
        End If
        If Len(Trim(oPayment.E_City)) = 0 Then
            sLine = sLine & PadRight("OSLO", 26, " ") ' In Telepay City is mandatory
        Else
            sLine = sLine & PadRight(oPayment.E_City, 26, " ") '223
        End If
        'The amount if its a payment to own account
        If oPayment.ToOwnAccount = True Then
            iCount = 0
            For Each oInvoice In oPayment.Invoices
                iCount = iCount + 1
                If iCount > 1 Then
                    'FIXERROR: Can't be more than 1 invoice when it is a payment to own account
                Else
                    sLine = sLine & PadLeft(Str(oInvoice.MON_InvoiceAmount), 15, "0")
                End If
            Next oInvoice
        Else
            sLine = sLine & New String("0", 15) '249
        End If
        sLine = sLine & sPayCode '264

        'FIX: Changed by Jan-Petter 28.06 due to no value from CodaImport
        ' Should be added in the importformat.
        ' Rechanged 23.06.05 beacuse "E" was not set for CodaImport
        'If oPayment.PayType = "" Then
        '    sLine = sLine & "F"
        'Else
        If oPayment.ToOwnAccount = True Then
            sLine = sLine & "E"
        Else
            sLine = sLine & "F" '267
        End If
        'End If
        If oPayment.Cancel = True Then
            sLine = sLine & "S"
        Else
            sLine = sLine & Space(1) '268
        End If
        'If this file shall be sent to the bank the amount shall be set to 0.
        If oPayment.StatusCode = "00" Then 'Amount = 0 Then
            sLine = sLine & New String("0", 15) '269
        ElseIf oPayment.StatusCode = "" Then
            'The imported file was not Telepay
            If Not bFileFromBank Then
                sLine = sLine & New String("0", 15)
            Else
                sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 15, "0")
            End If
        Else
            sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 15, "0")
        End If

        If Len(oPayment.I_Client) > 0 Then
            sLine = sLine & PadRight(oPayment.I_Client, 5, " ") '284
        Else
            sLine = sLine & New String("0", 5) '284 Klientreferanse
        End If
        If oPayment.DATE_Value = "19900101" Or oPayment.StatusCode = "00" Then
            sLine = sLine & New String("0", 6) '289
        Else
            sLine = sLine & Right(oPayment.DATE_Value, 6) 'Format(oPayment.DATE_Value, "YYMMDD")
        End If
        'Valutering mottakende bank - sett denne p� innsendingsfiler
        'hvis Date_Value er satt:
        If oPayment.StatusCode = "00" Then '
            sLine = sLine & New String("0", 6) '295
            'sLine = sLine & Right$(oPayment.DATE_Value, 6) '295
        Else
            sLine = sLine & New String("0", 6) '295
        End If

        ' added next if 19.11.2009
        If sSpecial = "FIRSTSECURITIESRETURUTBET" Or sSpecial = "FIRSTSECURITIESRETURINNBET" Then
            If Not EmptyString((oPayment.StatusText)) Then
                sLine = sLine & "D" '301 Slette�rsak
            Else
                sLine = sLine & Space(1) '301 Slette�rsak
            End If
        Else
            sLine = sLine & Space(1) '301 Slette�rsak
        End If
        sLine = sLine & Space(9) '302
        ' XNET 15.05.2013 changed to zeros in next line, as this is a numeric field
        sLine = sLine & New String("0", 10) '311 Blankettnummer - not in use in BabelBank p.t.

        WriteBETFOR21Invoice = UCase(sLine)


    End Function
    Private Function WriteBETFOR21Mass(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef nTransferredAmount As Double, ByRef iFormat_ID As Short) As String

        Dim sLine As String
        Dim sPayCode As String ', iCount As Integer
        Dim sErrorMessage As String
        'Dim oInvoice As Invoice

        ' Reset some controlvalues for each payment:
        '------------------------------------
        iStructuredPayment = -1
        iNoOfBETFOR22_23 = 0
        iNoOfTextLinesInBETFOR23 = 0

        sLine = ""
        'FIX: Have to add some parameters, i.e. Returncode
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR21"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo '49
        End If

        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "TELEPAY", "M")
        If sPayCode = "621" Then
            ' Mass payments, use Ownref as text on receivers statement
            ' Need to put Ownref into Babels Text_E_Statement
            If Len(oPayment.Text_E_Statement) > 0 Then
                oPayment.REF_Own = oPayment.Text_E_Statement
            End If
        End If

        sLine = sLine & PadLeft(oPayment.I_Account, 11, "0") '60    '09.09.2016 endret fra padright(
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        ' 06.12.2018 - Sparebank1 has extended their Ref_Bank1 (NtryRef) from 6 to 11 in Camt.054
        ' All over, the best will be to consider the 6 lastmost digits as the most relevant, and unique ones.
        ' Thus, has changed the line below from
        'sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ") '75
        '   to
        sLine = sLine & PadRight(Right(oPayment.Payment_ID, 6), 6, " ")  ' 75(use last 6 digits if more than 6)


        ' XNET 12.07.2013, added test
        If oPayment.VB_ProfileInUse Then
            ' 04.05.06 Added new test about date
            If oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateAll Then
                ' Set all dates to todays date
                ' 04.01.2008
                ' Changed to give us next bankday, instead of today.
                oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "NO", 0)) 'DateToString(Date)
            End If
        End If

        ' XNET 12.07.2013, added test
        If oPayment.VB_ProfileInUse Then
            ' 04.05.06 Added new test about date
            If StringToDate(oPayment.DATE_Payment) <= Date.Today Then
                If oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateDue Then
                    ' Set date to todays date
                    ' 04.01.2008
                    ' Changed to give us next bankday, instead of today.
                    oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "NO", 0)) 'DateToString(Date)
                End If
            End If
        End If

        sLine = sLine & Right(oPayment.DATE_Payment, 6) '81
        sLine = sLine & Left(oPayment.REF_Own & Space(30), 30) '87
        sLine = sLine & Space(1) '117
        'FIXERROR: Babelerror if E_Accoun longer than 11 or alfanumeric
        If Len(Trim(oPayment.E_Account)) = 11 Then
            sLine = sLine & Space(11) 'PadRight(oPayment.E_Account, 11, " ")
        Else
            sLine = sLine & "00000000000" '118
        End If
        ' XNET 12.07.2013, added test
        If oPayment.VB_ProfileInUse Then
            ' added 04.05.06 - Check accountno
            If oPayment.VB_Profile.FileSetups(iFormat_ID).CtrlAccount Then
                If Not CheckAccountNoNOR((oPayment.E_Account)) Then
                    sErrorMessage = LRS(35047, (oPayment.E_Account), "Telepay2")
                    sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & sImportFilename
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oPayment.MON_InvoiceAmount / 100
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                    Err.Raise(35047, , sErrorMessage)
                End If
            End If
        End If

        sLine = sLine & Space(30) '129
        sLine = sLine & Space(30) '159
        sLine = sLine & Space(30) '189
        sLine = sLine & "0000" '219
        sLine = sLine & Space(26) '223
        'The amount if its a payment to own account
        sLine = sLine & New String("0", 15) '249

        sLine = sLine & sPayCode '264
        If oPayment.PayType = "S" Then 'Salary
            sLine = sLine & "L" '267
        Else
            sLine = sLine & "M"
        End If
        If oPayment.Cancel = True Then
            sLine = sLine & "S" '268
        Else
            sLine = sLine & Space(1)
        End If
        If oPayment.StatusCode <> "02" Then 'No amount in betfor21 in sendfiles
            sLine = sLine & New String("0", 15) ' 269
        Else
            sLine = sLine & PadLeft(Str(nTransferredAmount), 15, "0")
            'sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 15, "0")
        End If
        If Len(oPayment.I_Client) > 0 Then
            sLine = sLine & PadRight(oPayment.I_Client, 5, " ") '284
        Else
            sLine = sLine & New String("0", 5) '284 Klientreferanse
        End If

        If oPayment.DATE_Value = "19900101" Or oPayment.StatusCode = "00" Then
            sLine = sLine & New String("0", 6) '289
        Else
            sLine = sLine & Right(oPayment.DATE_Value, 6) 'Format(oPayment.DATE_Value, "YYMMDD")
        End If

        'Valutering mottakende bank - sett denne p� innsendingsfiler
        'hvis Date_Value er satt:
        If oPayment.StatusCode = "00" Then '
            'sLine = sLine & Right$(oPayment.DATE_Value, 6) '295
            sLine = sLine & New String("0", 6) '295
        Else
            sLine = sLine & New String("0", 6) '295
        End If

        sLine = sLine & Space(1) '301 Slette�rsak
        sLine = sLine & Space(9) '302
        sLine = sLine & Space(10) '311 Blankettnummer - not in use in BabelBank p.t.


        WriteBETFOR21Mass = UCase(sLine)


    End Function

    Private Function WriteBETFOR23(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef iSerialNo As Double, ByRef iCount As Integer, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef iFormat_ID As Short, ByRef sSpecial As String) As String
        ' 19.11.09 added parameter sSpecial

        Dim sLine As String
        'Dim oFreeText As FreeText
        Dim iCountLocal As Short
        Dim bLocalStructured As Boolean
        Dim sErrorMessage As String

        ' Max 999 BETFOR23 (added 03.05.06)
        iNoOfBETFOR22_23 = iNoOfBETFOR22_23 + 1
        If iNoOfBETFOR22_23 > 999 Then
            ' XNET 12.07.2013, to take care of Visma and vbBabel.dll
            'If oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong Then  ' 22.05.06 added if test
            sErrorMessage = LRS(35043, (oPayment.MON_InvoiceCurrency), "Telepay2")
            sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & sImportFilename
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oPayment.MON_InvoiceAmount / 100
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
            ' XNET 12.07.2013, moved test
            If oPayment.VB_ProfileInUse Then
                If oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong Then  ' 22.05.06 added if test
                    Err.Raise(35043, , sErrorMessage)
                End If
            Else
                Err.Raise(35043, , sErrorMessage)
            End If
            'End If
        End If

        sLine = ""
        sLine = ApplicationHeader(False, (oInvoice.StatusCode))
        sLine = sLine & "BETFOR23" '41
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo '49
        End If
        sLine = sLine & PadLeft(oPayment.I_Account, 11, "0") '60      '09.09.2016 endret fra padright(
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        If oInvoice.StatusCode = "00" Then
            sLine = sLine & Space(6) '75
        Else
            ' 06.12.2018 - Sparebank1 has extended their Ref_Bank1 (NtryRef) from 6 to 11 in Camt.054
            ' All over, the best will be to consider the 6 lastmost digits as the most relevant, and unique ones.
            ' Thus, has changed the line below from
            'sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ") '75
            '   to
            sLine = sLine & PadRight(Right(oPayment.Payment_ID, 6), 6, " ")  ' 75(use last 6 digits if more than 6)
        End If

        'Can't have both Freetext and KID
        iCountLocal = 0

        If Not EmptyString((oInvoice.Unique_Id)) Then
            'KID stated
            sLine = sLine & Space(120) '81
            sLine = sLine & PadRight(oInvoice.Unique_Id, 27, " ") '201
            'To get out of the loop in WriteTelepay2File
            iCount = oInvoice.Freetexts.Count + 2
            bLocalStructured = True

        ElseIf Not EmptyString((oInvoice.InvoiceNo)) Then
            'Invoiceno stated, and no KID
            sLine = sLine & Space(147) '81
            ' To get out of the loop in WriteTelepay2File
            ' Creates error if more than two freetexts in use !!! iCount = oInvoice.Freetexts.Count + 2
            bLocalStructured = True

        Else
            'NO KID or invoiceno stated
            If oInvoice.Freetexts.Count >= iCount Then
                For iCountLocal = 1 To 3
                    If oInvoice.Freetexts.Count >= iCount Then
                        ' 20.09.05 -  Changed, in case freetext.text is not 40 pos
                        'sLine = sLine & oInvoice.Freetexts(iCount).Text
                        'UPGRADE_WARNING: Couldn't resolve default property of object oInvoice.Freetexts().Text. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sLine = sLine & PadRight(Left(oInvoice.Freetexts(iCount).Text, 40), 40, " ")
                        iCount = iCount + 1
                        iNoOfTextLinesInBETFOR23 = iNoOfTextLinesInBETFOR23 + 1
                    End If
                Next iCountLocal
                sLine = PadLine(sLine, 227, " ") '81 - 227
                ' Added new test of 25 * 40 chars text in BETFOR23
                ' 12.10.2016 - la dette gjelde kun innsendingsfiler - added test p� status
                If iNoOfTextLinesInBETFOR23 > 25 And oInvoice.StatusCode = "00" Then
                    ' XNET 12.07.2013, to take care of Visma and vbBabel.dll
                    'If oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong Then  ' 22.05.06 added if test
                    ' Reject!
                    sErrorMessage = LRS(35045, (oPayment.MON_InvoiceCurrency), "Telepay2")
                    sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & sImportFilename
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oPayment.MON_InvoiceAmount / 100
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                    ' XNET 12.07.2013, to take care of Visma and vbBabel.dll
                    If oPayment.VB_ProfileInUse Then
                        If oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong Then  ' 22.05.06 added if test
                            Err.Raise(35045, , sErrorMessage)
                        Else
                            Err.Raise(35045, , sErrorMessage)
                        End If
                    End If
                    'End If
                End If
            Else
                'If no freetext is given, then add a "."
                sLine = sLine & "." & Space(146) '81
            End If
            bLocalStructured = False

        End If

        ' 02.05.06 Added test for mix of structured/unstructured
        If iStructuredPayment > -1 Then ' iStructured=-1 for each new payment
            If iStructuredPayment = 0 And bLocalStructured = True Or iStructuredPayment = 1 And bLocalStructured = False Then
                'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ' XNET 12.07.2013 - redone test below, to take care of Visma with no profiles (and vbBabel.dll)
                'If oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong Then  ' 22.05.06 added if test
                ' error - mix of structured/unstructured
                sErrorMessage = LRS(35042, (oPayment.MON_InvoiceCurrency), "Telepay2")
                sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & sImportFilename
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oPayment.MON_InvoiceAmount / 100
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                If oPayment.VB_ProfileInUse Then
                    If oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong Then  ' 22.05.06 added if test
                        Err.Raise(35042, , sErrorMessage)
                    End If
                Else
                    Err.Raise(35042, , sErrorMessage)
                End If
                'End If
            End If
        End If
        '---------- end test mix -------------------------------
        If bLocalStructured = True Then
            iStructuredPayment = 1
        Else
            iStructuredPayment = 0
        End If

        sLine = sLine & Left(oInvoice.REF_Own & Space(30), 30) '228 Egenref
        ' XNET 13.09.2012 - If inputfile has both KID and freetext, then we can miss here, and end up with 0-amount !
        ' added last part of If
        'If iCount > 4 Then
        If iCount > 4 And Len(Trim$(oInvoice.Unique_Id)) = 0 Then
            sLine = sLine & New String("0", 15)
        Else
            sLine = sLine & PadLeft(Str(System.Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0") '258
        End If
        ' XNET 13.09.2012 - If inputfile has both KID and freetext, then we can miss here, and end up with 0-amount !
        ' added last part of If
        'If iCount > 4 Then

        ' added 25.03.2015
        ' If invoice is deleted, mark with "-" in pos 273
        If oInvoice.Cancel = True And oInvoice.ToSpecialReport = True Then
            sLine = sLine & "-" '273
        ElseIf iCount > 4 And Len(Trim$(oInvoice.Unique_Id)) = 0 Then
            sLine = sLine & "D" '273
        ElseIf oInvoice.MON_InvoiceAmount < 0 Then
            sLine = sLine & "K"
        Else
            sLine = sLine & "D"
        End If
        If Len(Trim(oInvoice.InvoiceNo)) > 0 And Len(Trim(oInvoice.Unique_Id)) = 0 Then
            ' Use Fakturanr only when KID is not in use
            sLine = sLine & PadRight(oInvoice.InvoiceNo, 20, " ") '274
        Else
            sLine = sLine & Space(20) '274
        End If

        '18.12.2014 - Changed the IF below
        'If oInvoice.StatusCode = "00" Then
        'If oInvoice.StatusCode <> "02" Then
        ' 03.07.2018 - Iflg dokumentasjon skal det �re l�penr for 01 og 02 filer
        If oInvoice.StatusCode <> "02" And oInvoice.StatusCode <> "01" Then
            sLine = sLine & "000" '274
        ElseIf oInvoice.StatusCode = "" Then
            'The imported file was not Telepay
            If Not bFileFromBank = True Then
                sLine = sLine & "000"
            Else
                sLine = sLine & PadLeft(Str(iSerialNo), 3, "0")
            End If
        Else
            sLine = sLine & PadLeft(Str(iSerialNo), 3, "0")
        End If

        ' added next if 19.11.2009
        If sSpecial = "FIRSTSECURITIESRETURUTBET" Or sSpecial = "FIRSTSECURITIESRETURINNBET" Then
            If Not EmptyString((oPayment.StatusText)) Then
                sLine = sLine & "D" '297 Slette�rsak
            Else
                sLine = sLine & Space(1) '297 Slette�rsak
            End If
        Else
            sLine = sLine & Space(1) ' 297 Slette�rsak, not implemented
        End If

        If Len(Trim(oInvoice.CustomerNo)) > 0 And Len(Trim(oInvoice.Unique_Id)) = 0 Then
            ' Use Kundenr only when KID is not in use
            sLine = sLine & PadRight(oInvoice.CustomerNo, 15, " ") '298
        Else
            sLine = sLine & Space(15) '298
        End If

        ' XokNET 30.01.2014 added next If, InvoiceDate only for structured, no KID. Important at Nordea !
        If Len(Trim$(oInvoice.InvoiceNo)) > 0 And Len(Trim$(oInvoice.Unique_Id)) = 0 Then
            sLine = sLine & PadLeft(oInvoice.InvoiceDate, 8, " ")
        Else
            sLine = sLine & Space(8)  '313 Fakturadato
        End If

        WriteBETFOR23 = UCase(sLine)


    End Function
    Private Function WriteBETFOR22(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef iSerialNo As Double, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef iFormat_ID As Short) As String

        Dim sLine As String
        Dim sErrorMessage As String
        'Dim iCount As Integer
        'Dim oFreeText As FreeText

        ' Max 9999 BETFOR22
        iNoOfBETFOR22_23 = iNoOfBETFOR22_23 + 1
        If iNoOfBETFOR22_23 > 9999 Then
            ' XNET 12.07.2013, to take care of Visma and vbBabel.dll
            'If oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong Then  ' 22.05.06 added if test
            sErrorMessage = LRS(35044, (oPayment.MON_InvoiceCurrency), "Telepay2")
            sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & sImportFilename
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oPayment.MON_InvoiceAmount / 100
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
            ' XNET 12.07.2013, to take care of Visma and vbBabel.dll
            If oPayment.VB_ProfileInUse Then
                If oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong Then  ' 22.05.06 added if test
                    Err.Raise(35044, , sErrorMessage)
                End If
            Else
                Err.Raise(35044, , sErrorMessage)
            End If
            'End If
        End If

        sLine = ""
        sLine = ApplicationHeader(False, (oInvoice.StatusCode))
        sLine = sLine & "BETFOR22" '41
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo '49
        End If
        sLine = sLine & PadLeft(oPayment.I_Account, 11, "0") '60      '09.09.2016 endret fra padright(
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        If oInvoice.StatusCode = "00" Then
            sLine = sLine & Space(6) '75
        Else
            sLine = sLine & PadLeft(oInvoice.Invoice_ID, 6, " ") '75
        End If
        sLine = sLine & PadRight(oPayment.E_Account, 11, " ") '81
        sLine = sLine & PadRight(oPayment.E_Name, 30, " ") '92
        sLine = sLine & PadLeft(Str(System.Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0") '122
        If oPayment.Cancel = True Then
            sLine = sLine & "S" '137
        Else
            sLine = sLine & Space(1)
        End If
        sLine = sLine & Left(oInvoice.REF_Own & Space(35), 35) '138
        sLine = sLine & Space(110) '173
        sLine = sLine & PadRight(oInvoice.Extra1, 10, " ") '283 Egenreferanse2
        If oInvoice.StatusCode = "00" Or Len(Trim(oInvoice.StatusCode)) = 0 Then
            sLine = sLine & "0000" '293
        Else
            sLine = sLine & PadLeft(Str(iSerialNo), 4, "0")
        End If
        sLine = sLine & Space(1) ' 297 Slette�rsak, not implemented
        sLine = sLine & Space(23) '

        WriteBETFOR22 = UCase(sLine)


    End Function
    Private Function WriteBETFOR99(ByRef oBatch As Batch, ByRef nBETFORnumber As Double, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef sVersion As String, Optional ByVal sSpecial As String = "") As String

        Dim sLine As String
        sLine = ""
        'Build AH, send Returncode
        If bLastWasTBIO Then
            ' Changed06.07.06 due to problems with Nordea-returnfiles
            'sLine = ApplicationHeader(False, oBatch.StatusCode, True, True) 'TBIO in applicationheader

            '29.04.2015 - Kjell and Jan-Petter agrees that the statuscode should ALWAYS be retrieved from Batch for BETFOR00/99
            sLine = ApplicationHeader(False, oBatch.StatusCode, True, True) 'TBIO in applicationheader
            'sLine = ApplicationHeader(False, IIf(oBatch.Payments.Count > 0, oBatch.Payments(1).StatusCode, oBatch.StatusCode), True, True) 'TBIO in applicationheader
        Else
            ' Changed06.07.06 due to problems with Nordea-returnfiles
            '29.04.2015 - Kjell and Jan-Petter agrees that the statuscode should ALWAYS be retrieved from Batch for BETFOR00/99
            sLine = ApplicationHeader(False, oBatch.StatusCode, True, False)
            'If sSpecial = "GJENSIDIGE_OW_FROMBANK" Then
            '    sLine = ApplicationHeader(False, oBatch.StatusCode, True, False)
            'Else
            '    sLine = ApplicationHeader(False, IIf(oBatch.Payments.Count > 0, oBatch.Payments(1).StatusCode, oBatch.StatusCode), True, False)
            'End If
        End If
        sLine = sLine & "BETFOR99" '41
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo '49
        End If
        sLine = sLine & Space(11) '60
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        sLine = sLine & Space(6) '75
        If oBatch.DATE_Production <> "19900101" Then
            sLine = sLine & Right(oBatch.DATE_Production, 4) 'Format(oBatch.DATE_Production, "MMDD")
        Else
            sLine = sLine & VB6.Format(Now, "MMDD") '81
        End If
        sLine = sLine & Space(4) '85
        sLine = sLine & Space(15) '89
        sLine = sLine & PadLeft(Str(nBETFORnumber + 1), 5, "0") '104
        'FIX: No use of AEGIS or SIGILL
        sLine = sLine & Space(163) '109
        sLine = sLine & Space(4) '272
        sLine = sLine & Space(1) '276
        sLine = sLine & Space(1) '277
        sLine = sLine & Space(1) '278
        sLine = sLine & Space(18) '279


        ' Version software
        ' XNET 19.07.2013
        If bVismaIntegrator Then
            sLine = sLine + PadRight("VISMABABELB" & Replace(sVersion, ".", ""), 16, " ")
        Else
            sLine = sLine + PadRight("BABELBANKP" + Replace(sVersion, ".", ""), 16, " ")
        End If
        sLine = PadRight(sLine, 320, " ") ' Fill until 320-record is ok


        WriteBETFOR99 = (sLine)

    End Function
    Function ApplicationHeader(ByRef bFirstLine As Boolean, ByRef sStatusCode As String, Optional ByRef bISBetfor99 As Boolean = False, Optional ByRef TBIOfor99 As Boolean = False) As String

        Dim sAH As String
        Static nSequenceNo As Double

        If sStatusCode = "-1" Then
            ' reduce nsequenceno by 1 because of unwritten records
            '(new by janP 26.11.01
            nSequenceNo = nSequenceNo - 1
            sAH = ""
        ElseIf sStatusCode = "+1" Then
            ' add 1 to nsequenceno because the record was written after all
            '(new by Kjell 27.08.09
            'Dangerous way to use the StatusCode, but when the boss has done, I don't dare to oppose him.
            nSequenceNo = nSequenceNo + 1
            sAH = ""
        Else

            If bFirstLine = True Then
                nSequenceNo = 1
            Else
                nSequenceNo = nSequenceNo + 1
            End If
            If sStatusCode = "" Then
                'The imported file was not Telepay
                If Not bFileFromBank Then
                    sStatusCode = "00"
                Else
                    sStatusCode = "02"
                End If
            End If

            sAH = ""
            sAH = "AH2"
            sAH = sAH & sStatusCode
            If sStatusCode = "00" Then
                If bISBetfor99 Then
                    If TBIOfor99 Then
                        sAH = sAH & "TBIO"
                    Else
                        If bDomestic Then
                            sAH = sAH & "TBII"
                        Else
                            sAH = sAH & "TBIU"
                        End If
                    End If
                Else
                    If bCurrentIsTBIO Then
                        sAH = sAH & "TBIO"
                    Else
                        If bDomestic Then
                            sAH = sAH & "TBII"
                        Else
                            sAH = sAH & "TBIU"
                        End If
                    End If
                End If
                ' XokNET 31.12.2014
                ' We need an own section for rejected payments
            ElseIf Val(sStatusCode) > 2 Then
                If bISBetfor99 Then
                    If TBIOfor99 Then
                        sAH = sAH & "TBMO"
                    Else
                        If bDomestic Then
                            sAH = sAH & "TBMI"
                        Else
                            sAH = sAH & "TBMU"
                        End If
                    End If
                Else
                    If bCurrentIsTBIO Then
                        sAH = sAH & "TBMO"
                    Else
                        If bDomestic Then
                            sAH = sAH & "TBMI"
                        Else
                            sAH = sAH & "TBMU"
                        End If
                    End If
                End If

            Else
                If bISBetfor99 Then
                    If TBIOfor99 Then
                        sAH = sAH & "TBRO"
                    Else
                        If bDomestic Then
                            sAH = sAH & "TBRI"
                        Else
                            sAH = sAH & "TBRU"
                        End If
                    End If
                Else
                    If bCurrentIsTBIO Then
                        sAH = sAH & "TBRO"
                    Else
                        If bDomestic Then
                            sAH = sAH & "TBRI"
                        Else
                            sAH = sAH & "TBRU"
                        End If
                    End If
                End If
            End If

            If Trim(sTransDate) = "" Then
                sAH = sAH & VB6.Format(Now, "MMDD")
            Else
                sAH = sAH & sTransDate
            End If
            'FIX: Enter Sequenceno. per day
            sAH = sAH & PadLeft(Str(nSequenceNo), 6, "0")
            sAH = sAH & Space(8)
            'FIX: Enter User_ID
            sAH = sAH & Space(11)
            sAH = sAH & "04"
        End If
        ApplicationHeader = sAH

    End Function
	
	'UPGRADE_NOTE: WriteLine was upgraded to WriteLine_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Function WriteLine_Renamed(ByRef sLine As String, ByRef nNoOfBETFOR As Double, ByRef nSequenceNoTotal As Double, ByRef bUnix As Boolean) As Boolean
		' 12.08.02 JaNp
		' Added bUnix
		' if bUnix is set to true, then only LineFeed is added at the end of each line,
		' instead of the ordinary CRLF
		If bUnix Then
			oFile.Write(Mid(sLine, 1, 80) & vbLf)
			oFile.Write(Mid(sLine, 81, 80) & vbLf)
			oFile.Write(Mid(sLine, 161, 80) & vbLf)
			oFile.Write(Mid(sLine, 241, 80) & vbLf)
		Else
			oFile.WriteLine(Mid(sLine, 1, 80))
			oFile.WriteLine(Mid(sLine, 81, 80))
			oFile.WriteLine(Mid(sLine, 161, 80))
			oFile.WriteLine(Mid(sLine, 241, 80))
		End If
		
		nNoOfBETFOR = nNoOfBETFOR + 1
		nSequenceNoTotal = nSequenceNoTotal + 1
		If nSequenceNoTotal > 9999 Then
			nSequenceNoTotal = 0
		End If
		
		
	End Function
    'XOKNET - 30.05.2013 - Added new function
    Private Function Visma_RetrieveSequenceNumber(ByVal oExternalReadConnection As vbBabel.DAL, ByVal sI_EnterpriseNo As String, ByVal sI_Branch As String, ByVal iFilenameInNo As Integer, ByVal sBBDatabase As String) As Integer
        'ProfileNo is in iFilenameInNo
        ' find sequencenumber in Vismadatabase for this Enterpriseno and division
        ' Stored in database as SEQ#00882735532#DIVISION or SEQ#00882735532 (if no division)

        Dim sMySQL As String
        Dim sErrorString As String
        Dim iSequenceNo As Integer

        'On Error GoTo ERR_ReadSEQ

        sErrorString = "Find sequence from database for " & sI_EnterpriseNo & "/" & sI_Branch

        ' Have to read also uncomitted transactions;
        oExternalReadConnection.SQL = "set transaction isolation level read uncommitted"
        If oExternalReadConnection.ExecuteNonQuery Then
            If oExternalReadConnection.RecordsAffected <> -1 Then
                Throw New Exception("Visma_RetrieveSequenceNumber" & vbCrLf & LRS(35307, "'uncomitted'"))
            End If
        Else
            Throw New Exception(LRSCommon(45002) & vbCrLf & oExternalReadConnection.ErrorMessage)
        End If

        sMySQL = "SELECT BB_ID,Value FROM " & sBBDatabase & ".dbo.bbParameter WHERE SubString(BB_ID,1,4) = 'SEQ#' AND ProfileID =" & Str(iFilenameInNo)
        sErrorString = "Select sequencenumber"
        oExternalReadConnection.SQL = sMySQL

        If oExternalReadConnection.Reader_Execute() Then
            If oExternalReadConnection.Reader_HasRows = True Then

                'If rsSEQ.BOF And rsSEQ.eof Then
                '    ' found no sequencesettings
                '    iSequenceNo = -1
                'Else
                iSequenceNo = -1
                ' find seq for current Enterpriseno and division
                'rsSEQ.MoveFirst()
                'Do While Not rsSEQ.eof
                Do While oExternalReadConnection.Reader_ReadRecord
                    'If xDelim(rsSEQ!BB_Id, "#", 2) = sI_EnterpriseNo And xDelim(rsSEQ!BB_Id, "#", 3) = sI_Branch Then
                    If xDelim(oExternalReadConnection.Reader_GetString("BB_Id"), "#", 2) = sI_EnterpriseNo And xDelim(oExternalReadConnection.Reader_GetString("BB_Id"), "#", 3) = sI_Branch Then

                        ' found sequence for this enterpriseno/division
                        iSequenceNo = oExternalReadConnection.Reader_GetString("Value")
                        Exit Do
                    End If
                Loop
            Else
                ' found no sequencesettings
                iSequenceNo = -1
            End If
        Else
            ' found no sequencesettings
            iSequenceNo = -1
        End If

        oExternalReadConnection.Reader_Close()

        ' back to read comitted transactions;
        oExternalReadConnection.SQL = "set transaction isolation level read committed"
        If oExternalReadConnection.ExecuteNonQuery Then
            If oExternalReadConnection.RecordsAffected <> -1 Then
                Throw New Exception("Visma_RetrieveSequenceNumber" & vbCrLf & LRS(35307, "'uncomitted'"))
            End If
        Else
            Throw New Exception(LRSCommon(45002) & vbCrLf & oExternalReadConnection.ErrorMessage)
        End If

        'oExternalReadConnection.Close()
        'oExternalReadConnection = Nothing

        Visma_RetrieveSequenceNumber = iSequenceNo
        '        Exit Function

        'ERR_ReadSEQ:
        '        oExternalReadConnection.Close()
        '        oExternalReadConnection = Nothing

        '        Err.Raise(Err.Number, "Visma_RetrieveSequenceNumber", sErrorString & vbCrLf & Err.Description)

    End Function
    Private Sub Visma_SaveSequenceNumber(ByVal oExternalUpdateConnection As vbBabel.DAL, ByVal oExternalReadConnection As vbBabel.DAL, ByVal sI_EnterpriseNo As String, ByVal sI_Branch As String, ByVal iSequenceNo As Double, ByVal iFilenameInNo As Integer, ByVal sBBDatabase As String, ByVal sUser As String) 'ProfileNo is in iFilenameInNo
        ' Save sequencenumber in Vismadatabase for this Enterpriseno and division
        ' Stored in database as SEQ#00882735532#DIVISION or SEQ#00882735532 (if no division)
        Dim sMySQL As String
        Dim sErrorString As String
        Dim lRecordsAffected As Long
        Dim lMaxParameterID As Long

        'On Error GoTo ERR_WriteSEQ


        ' Have to read also uncomitted transactions;
        oExternalReadConnection.SQL = "set transaction isolation level read uncommitted"
        If oExternalReadConnection.ExecuteNonQuery Then
            If oExternalReadConnection.RecordsAffected <> -1 Then
                Throw New Exception("Visma_SaveSequenceNumber" & vbCrLf & LRS(35307, "'uncomitted'"))
            End If
        Else
            Throw New Exception(LRSCommon(45002) & vbCrLf & oExternalReadConnection.ErrorMessage)
        End If

        ' Find highest ParameterID
        sMySQL = "SELECT MAX(ParameterID) AS MaxID FROM " & sBBDatabase & ".dbo.bbParameter"
        sErrorString = "Select MaxID"
        oExternalReadConnection.SQL = sMySQL

        If oExternalReadConnection.Reader_Execute() Then
            If oExternalReadConnection.Reader_HasRows = True Then
                ' Always returns a record;
                lMaxParameterID = Val(oExternalReadConnection.Reader_GetString("MaxID")) + 1
            Else
                lMaxParameterID = 1
            End If
        Else
            lMaxParameterID = 1
        End If

		' back to read comitted transactions;
		' ---------->>>>>>>>>>>>>>> 20.10.2021 - MAJOR CHANGE; N�r vi bruker sqlclient, s� tryner det p� denne under, ihvertfall noen steder
		'                                         Denne er da kommentert vekk her
		' 08.11.2021 - when using SQLOledb, uncommented
		oExternalReadConnection.SQL = "set transaction isolation level read committed"
		If oExternalReadConnection.ExecuteNonQuery Then
			If oExternalReadConnection.RecordsAffected <> -1 Then
				Throw New Exception("Visma_SaveSequenceNumber" & vbCrLf & LRS(35307, "'uncomitted'"))
			End If
		Else
			Throw New Exception(LRSCommon(45002) & vbCrLf & oExternalReadConnection.ErrorMessage)
		End If


		' NO - do not BeginTrans here, must do in Visma_ImportExport -
		''''oExternalConnection.BeginTrans

		'sMySQL = "UPDATE " & sBBDatabase & ".dbo.bbParameter SET Value = '" & LTrim(Str(iSequenceNo)) & "', ChDt = CONVERT(VARCHAR(8), SYSDATETIME(), 112), ChTm = REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', ''), ChUsr = '" & sUser & "' WHERE BB_ID = 'SEQ#" & Trim$(sI_EnterpriseNo) & "#" & Trim$(sI_Branch) & "#'"
		' Changed 06.06.2014 - now involves ProfileID when saving sequenceNo (added  AND ProfileID =" & Str(iFilenameInNo))
		sMySQL = "UPDATE " & sBBDatabase & ".dbo.bbParameter SET Value = '" & LTrim(Str(iSequenceNo)) & "', ChDt = CONVERT(VARCHAR(8), SYSDATETIME(), 112), ChTm = REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', ''), ChUsr = '" & sUser & "'"
        sMySQL = sMySQL & " WHERE BB_ID = 'SEQ#" & Trim$(sI_EnterpriseNo) & "#" & Trim$(sI_Branch) & "#' AND ProfileID =" & Str(iFilenameInNo)

        sErrorString = "UPDATE " & sBBDatabase & ".dbo.bbParameter " & vbCrLf & sMySQL

        oExternalUpdateConnection.SQL = sMySQL
        oExternalUpdateConnection.ExecuteNonQuery()
        If oExternalUpdateConnection.RecordsAffected = 0 Then
            ' not found, insert instead
            ' XokNET 09.10.2013 added more fields, ChDt, ChTm, ChUsr
            sErrorString = "INSERT INTO " & sBBDatabase & ".dbo.bbParameter "
            sMySQL = "INSERT INTO " & sBBDatabase & ".dbo.bbParameter (ParameterID, BB_ID, LRS_ID, Value, ProfileID, AllowedValues, ParameterType, DataType, ChDt, ChTm, ChUsr) "
            sMySQL = sMySQL & "VALUES (" & lMaxParameterID & ", 'SEQ#" & Trim$(sI_EnterpriseNo) & "#" & Trim$(sI_Branch) & "#', 36363,'" & LTrim(Str(iSequenceNo)) & "'," & Str(iFilenameInNo) & ", '', 3, 1"
            sMySQL = sMySQL & ", CONVERT(VARCHAR(8), SYSDATETIME(), 112), REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', ''), '" & sUser & "')"

            oExternalUpdateConnection.SQL = sMySQL
            oExternalUpdateConnection.ExecuteNonQuery()

            If oExternalUpdateConnection.RecordsAffected <> 1 Then
                Err.Raise(Err.Number, "Visma_SaveSequenceNumber" & vbCrLf & "SQL : " & sMySQL, sErrorString & vbCrLf & Err.Description)
            End If
        End If

        '        Exit Sub

        'ERR_WriteSEQ:
        '        'oExternalConnection.RollbackTrans   ' TODO VISMA - kj�r rollback her eller i Visma_ImportExport ?
        '        Err.Raise(Err.Number, "Visma_SaveSequenceNumber", sErrorString & vbCrLf & Err.Description)

    End Sub
End Module
