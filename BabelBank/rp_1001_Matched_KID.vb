Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document 

Public Class rp_1001_Matched_KID

    Dim iCompany_ID As Integer
    Dim iBabelfile_ID As Integer
    Dim iBatch_ID As Integer
    Dim iPayment_ID As Integer
    Dim iInvoice_ID As Integer
    Dim sOldI_Account As String
    Dim sI_Account As String
    Dim sClientInfo As String
    Dim sClientName As String

    'HUSHOVD
    'Dim nCalculatedMacthedAmount As Double
    'Dim nCalculatedUnMacthedAmount As Double
    Dim nInvoiceCounter As Double

    Dim nTotalAmount As Double
    Dim sOrderBy As String
    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean
    Dim bValid As Boolean

    Dim oBabelFiles As vbBabel.BabelFiles
    Dim oBabelFile As vbBabel.BabelFile
    Dim oBatch As vbBabel.Batch
    Dim oPayment As vbBabel.Payment
    Dim oInvoice As vbBabel.Invoice
    Dim oFreeText As vbBabel.Freetext
    Dim bFirstTime As Boolean
    Dim bFirstVoucherFreetext As Boolean = True

    Dim sReportName As String
    Dim sSpecial As String
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim sClientNumber As String
    Dim bReportOnSelectedItems As Boolean
    Dim bEmptyReport As Boolean
    Dim bSQLServer As Boolean = False
    Dim bIncludeOCR As Boolean
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean

    Private sBBRET_CustomerNoLabel As String
    Private sBBRET_InvoiceNoLabel As String
    Private sBBRET_FilterOnLabel As String
    Private sBBRET_FilterOffLabel As String
    Private sBBRET_ClientNoLabel As String
    Private sBBRET_ExchangeRateLabel As String
    Private sBBRET_VendroNoLabel As String
    Private sBBRET_CurrencyLabel As String
    Private sBBRET_GeneralLedgerLabel As String
    Private sBBRET_VoucherNoLabel As String
    Private sBBRET_NameLabel As String
    Private sBBRET_FreetextLabel As String
    Private sBBRET_CurrencyAmountLabel As String
    Private sBBRET_Currency2Label As String
    Private sBBRET_DiscountLabel As String
    Private sBBRET_BackPaymentLabel As String
    Private sBBRET_MyFieldLabel As String
    Private sBBRET_MyField2Label As String
    Private sBBRET_MyField3Label As String
    Private sBBRET_Dim1Label As String
    Private sBBRET_Dim2Label As String
    Private sBBRET_Dim3Label As String
    Private sBBRET_Dim4Label As String
    Private sBBRET_Dim5Label As String
    Private sBBRET_Dim6Label As String
    Private sBBRET_Dim7Label As String
    Private sBBRET_Dim8Label As String
    Private sBBRET_Dim9Label As String
    Private sBBRET_Dim10Label As String
    Private Sub rp_1001_Matched_KID_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        'Set the different breaklevels

        Try

            Fields.Add("BreakField")

            If Not bReportFromDatabase Then
                Fields.Add("Grouping")
                Fields.Add("Company_ID")
                Fields.Add("Babelfile_ID")
                Fields.Add("Batch_ID")
                Fields.Add("Payment_ID")
                Fields.Add("Invoice_ID")
                Fields.Add("Filename")
                Fields.Add("Client")
                Fields.Add("DATE_Production")
                Fields.Add("I_Account")
                'Fields.Add("Paycode")
                Fields.Add("MON_InvoiceAmount")
                Fields.Add("MON_InvoiceCurrency")
                Fields.Add("MON_TransferredAmount")
                Fields.Add("MON_TransferCurrency")
                Fields.Add("StatusCode")
                Fields.Add("StatusText")
                Fields.Add("E_Name")
                Fields.Add("E_Adr1")
                Fields.Add("E_Adr2")
                Fields.Add("E_Adr3")
                Fields.Add("E_Zip")
                Fields.Add("E_City")
                Fields.Add("E_Account")
                Fields.Add("E_Name")
                Fields.Add("REF_Bank1")
                'Fields.Add("Unique_ID")
                Fields.Add("REF_Bank1")
                Fields.Add("REF_Bank2")
                Fields.Add("REF_Own")
                Fields.Add("DATE_Value")
                Fields.Add("DATE_Payment")
                Fields.Add("PayCode")
                Fields.Add("PayType")
                Fields.Add("BANK_SWIFTCode")
                Fields.Add("BANK_BranchNo")
                Fields.Add("BANK_BranchType")
                Fields.Add("VoucherNo")
                Fields.Add("BabelBank_ID")
                Fields.Add("ExtraD")
                Fields.Add("Extra1")
                Fields.Add("MATCH_UseOriginalAmountInMatching")
                Fields.Add("ImportFormat")
                Fields.Add("MyField")
                Fields.Add("Unique_ID")
                Fields.Add("InvoiceStatusCode")
                Fields.Add("InvoiceNumber")
                Fields.Add("CustomerNumber")
                Fields.Add("InvoiceAmount")
                Fields.Add("Bank")
                Fields.Add("MATCH_Type")
                Fields.Add("Match_ID")
                Fields.Add("Matched")
                Fields.Add("Final")
                Fields.Add("InvoiceLabel")
                Fields.Add("CustomerLabel")
                Fields.Add("PaymentConcerns")
                Fields.Add("HowMatched")
            End If

        Catch

        End Try

    End Sub

    Private Sub rp_1001_Matched_KID_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData

        Try

            bEmptyReport = True
            If Not bReportFromDatabase Then
                Me.Cancel()
            End If

        Catch

        End Try

    End Sub

    Private Sub rp_1001_Matched_KID_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportStart

        Try

            bShowBatchfooterTotals = True
            bFirstTime = True
            bValid = False

            ' Find user-specific labels
            RetrieveLabels(Nothing, "1", sBBRET_CustomerNoLabel, sBBRET_InvoiceNoLabel, sBBRET_ClientNoLabel, _
                                  sBBRET_VendroNoLabel, sBBRET_CurrencyLabel, sBBRET_ExchangeRateLabel, sBBRET_GeneralLedgerLabel, sBBRET_VoucherNoLabel, sBBRET_NameLabel, _
                                  sBBRET_FreetextLabel, sBBRET_CurrencyAmountLabel, sBBRET_Currency2Label, sBBRET_DiscountLabel, sBBRET_BackPaymentLabel, sBBRET_MyFieldLabel, _
                                  sBBRET_MyField2Label, sBBRET_MyField3Label, sBBRET_Dim1Label, sBBRET_Dim2Label, sBBRET_Dim3Label, sBBRET_Dim4Label, _
                                  sBBRET_Dim5Label, sBBRET_Dim6Label, sBBRET_Dim7Label, sBBRET_Dim8Label, sBBRET_Dim9Label, sBBRET_Dim10Label)


            Select Case iBreakLevel
                Case 0 'NOBREAK
                    bShowBatchfooterTotals = False
                    bShowDATE_Production = True
                    bShowClientName = False
                    bShowI_Account = False

                Case 1 'BREAK_ON_ACCOUNT (and per day)
                    bShowDATE_Production = True
                    bShowClientName = True
                    bShowI_Account = True

                Case 2 'BREAK_ON_BATCH
                    bShowDATE_Production = True
                    bShowClientName = True
                    bShowI_Account = True

                Case 3 'BREAK_ON_PAYMENT
                    bShowDATE_Production = True
                    bShowClientName = True
                    bShowI_Account = True

                Case 4 'BREAK_ON_ACCOUNTONLY
                    bShowDATE_Production = True
                    bShowClientName = True
                    bShowI_Account = True

                Case 5 'BREAK_ON_CLIENT 'New
                    bShowDATE_Production = True
                    bShowClientName = True
                    bShowI_Account = False

                Case 999
                    bShowDATE_Production = True
                    bShowClientName = True
                    bShowI_Account = True

                Case Else 'NOBREAK
                    bShowDATE_Production = True
                    bShowClientName = False
                    bShowI_Account = False

            End Select

            If bUseLongDate Then
                Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
                Me.txtDATE_Production.OutputFormat = "D"
                Me.txtDATE_Value.OutputFormat = "D"
                Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
            Else
                Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
                Me.txtDATE_Production.OutputFormat = "d"
                Me.txtDATE_Value.OutputFormat = "d"
                Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
            End If

            ' Setup of pagesize, etc
            Me.PageSettings.DefaultPaperSize = True ' 'A4
            Me.PageSettings.DefaultPaperSource = True
            Me.PageSettings.Margins.Bottom = 1.0F
            Me.PageSettings.Margins.Left = 1.0F
            Me.PageSettings.Margins.Right = 0.75F '1.0F
            Me.PageSettings.Margins.Top = 1.0F
            'Me.PrintWidth = 6.5
            ''Me.PageSettings.PaperWidth = 8.5F
            Me.PageSettings.Orientation = PageOrientation.Portrait

            If EmptyString(sReportName) Then
                Me.lblrptHeader.Text = LRS(40145) '"Posterte KID innbetalinger" 
            Else
                Me.lblrptHeader.Text = sReportName
            End If
            'If Language = Norwegian Then
            Me.rptInfoPageCounterGB.Visible = False
            Me.rptInfoPageCounterNO.Visible = True
            Me.rptInfoPageCounterNO.Left = 4
            'Else
            'Me.rptInfoPageCounterGB.Visible = True
            'Me.rptInfoPageCounterNO.Visible = False
            'Me.rptInfoPageCounterGB.Left = 4
            'End If

            'Set the caption of the labels

            'grBatchHeader
            'Me.lblFilename.Text = LRS(40001) '"Filnavn:" 
            Me.lblDate_Production.Text = LRS(40027) '"Prod.dato:" 
            Me.lblClient.Text = LRS(40030) '"Klient:" 
            Me.lblI_Account.Text = LRS(40020) '"Mottakerkonto:" 

            'grPaymentReceiverHeader
            Me.lblPayor.Text = LRS(60067) & ":" '"Betaler:" 

            'grPaymentHeader
            Me.lblE_Account.Text = LRS(40013) '"Kontonr.:" 
            Me.lblREF_Bank2.Text = LRS(40011) '"Arkivref.:" 
            Me.lblREF_Bank1.Text = LRS(40012) '"Blankettref.:" 
            Me.lblDATE_Value.Text = LRS(40003) '"Val.dato:" 
            Me.lblVoucherNo.Text = LRS(40068) '"Bilagsnummer:" 
            ' 14.09.2015 Userspecific labels
            If Not EmptyString(sBBRET_VoucherNoLabel) Then
                Me.lblVoucherNo.Text = sBBRET_VoucherNoLabel
            End If

            ' 16.09.2020 added next
            Me.lblHowMatched.Text = LRS(40074) '"Hvordan avstemt:" 

            'Detail
            Me.lblConcerns.Text = LRS(40010) '"Bel�pet gjelder:" 

            'grPaymentFooter
            Me.lblTotalPayment.Text = LRS(60089) '"Innbetalt" 
            Me.lblMatchedAmount.Text = LRS(60088) ' Posted
            Me.lblUnmatchedAmount.Text = LRS(40029) 'Unposted

            'grBatchFooter
            Me.lblBatchfooterAmount.Text = LRS(40104) '"Bel�p:"
            Me.lblBatchfooterNoofPayments.Text = LRS(40105) '"Deltotal - Antall:" 

            'grBabelFileFooter
            Me.lblTotalAmount.Text = LRS(40104) '"Bel�p:" 
            Me.lblTotalNoOfPayments.Text = LRS(40106) '"Total - Antall:" 

            ' Special cases;
            If sSpecial = "LINDORFF" Then
                Me.lblExtraD.Text = "Kildekode"
                Me.lblExtra1.Text = "Innbet.kode"
                Me.lblMyField.Text = "Rolle"
            Else
                Me.lblExtraD.Visible = False
                Me.txtExtraD.Visible = False
                Me.lblExtra1.Visible = False
                Me.txtExtra1.Visible = False
                Me.lblMyField.Visible = False
                Me.txtMyField.Visible = False
            End If
            '*******VB6 Code

            ''Me.lblFileSum = LRS(40028)

            sOldI_Account = ""
            sClientInfo = LRS(40107) '"Ingen klientinfo angitt" 

            If Not bShowBatchfooterTotals Then
                Me.grBatchFooter.Visible = False
            End If
            If Not bShowDATE_Production Then
                Me.txtDATE_Production.Visible = False
            End If
            If Not bShowClientName Then
                Me.txtClientName.Visible = False
            End If
            If Not bShowI_Account Then
                Me.txtI_Account.Visible = False
            End If

            'Boasson Hagen
            Me.txtMatchedAmount.Value = 0
            Me.txtUnmatchedAmount.Value = 0
            Me.txtMatchedBatchAmount.Value = 0
            Me.txtMatchedBatchCount.Value = 0
            Me.txtMatchedTotalAmount.Value = 0
            Me.txtMatchedTotalCount.Value = 0

        Catch

        End Try


    End Sub
    Private Sub grPaymentHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grPaymentHeader.Format
        Dim pLocation As System.Drawing.PointF

        'HUSHOVD
        'nCalculatedMacthedAmount = 0
        'ninvoicecounter = 0

        Try

            If EmptyString(Me.txtE_City.Text) Then
                pLocation.X = 0
                pLocation.Y = 0.45
                Me.txtE_Adr3.Location = pLocation
                Me.txtE_Adr3.Visible = True
                Me.txtE_Zip.Visible = False
                Me.txtE_City.Visible = False
            Else
                Me.txtE_Adr3.Visible = False
                Me.txtE_Zip.Visible = True
                Me.txtE_City.Visible = True
            End If

        Catch ex As Exception

        End Try

    End Sub
    Private Sub rp_1001_Matched_KID_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        'The Fields collection should never be accessed outside the DataInitialize and FetchData events
        Dim sDATE_Production As String
        Dim sDATE_Value As String
        Dim bContinue As Boolean
        Dim bExportPayment As Boolean
        Dim sUserText As String

        ' Counters for items in the different Babel collections
        Static lBabelFiles As Long
        Static lLastUsedBabelFilesItem As Long
        Static lBatches As Long
        Static lPayments As Long
        Static lInvoices As Long
        Static lFreetexts As Long
        ' Counters for last item in use in the different Babel collections
        ' This way we do not need to set the different objects each time,
        ' just when an item-value has changed
        Static xCreditAccount As String
        Static xDate As String

        Dim iProgress As Integer

        Try

            If Not bReportFromDatabase Then

                If bFirstTime Then
                    bFirstTime = False
                    lBabelFiles = 1
                    lBatches = 1
                    lPayments = 1
                    bFirstVoucherFreetext = True
                    lInvoices = 1
                    lFreetexts = 0
                    xCreditAccount = "xxxx"
                    xDate = "x"
                End If

                lFreetexts = lFreetexts + 1

                ' Spin through collections to find next suitbable record before any other
                ' proecessing is done
                ' Position to correct items in collections
                Do Until lBabelFiles > oBabelFiles.Count
                    ' Try to set as few times as possible
                    If oBabelFile Is Nothing Then
                        oBabelFile = oBabelFiles(lBabelFiles)
                        lLastUsedBabelFilesItem = lBabelFiles
                    Else
                        'If lBabelFiles <> oBabelFile.Index Then
                        If lLastUsedBabelFilesItem <> lBabelFiles Then
                            lBatches = 1
                            lPayments = 1
                            bFirstVoucherFreetext = True
                            lInvoices = 1
                            ' Added 19.05.2004
                            xCreditAccount = "xxxx"
                            xDate = "x"

                            iProgress = 1

                            oBabelFile = oBabelFiles(lBabelFiles)
                            lLastUsedBabelFilesItem = lBabelFiles  'oBabelFile.Index
                        End If
                    End If

                    Do Until lBatches > oBabelFile.Batches.Count
                        oBatch = oBabelFile.Batches(lBatches)

                        ' Find the next payment in this batch which is original
                        Do Until lPayments > oBatch.Payments.Count

                            oPayment = oBatch.Payments.Item(lPayments)

                            If IsOCR(oPayment.PayCode) Or IsAutogiro(oPayment.PayCode) Then

                                bExportPayment = False

                                iProgress = 2
                                'Sometimes there are restrictions on which payments to export
                                '17.10.2019 - Added next Special
                                If sSpecial = "SGF_EQ" Then
                                    If oPayment.MATCH_Matched > vbBabel.BabelFiles.MatchStatus.PartlyMatched Then '0 Then
                                        If bIncludeOCR Then
                                            bExportPayment = True
                                        Else
                                            If IsOCR(oPayment.PayCode) Then
                                                bExportPayment = False
                                            ElseIf IsAutogiro(oPayment.PayCode) Then
                                                bExportPayment = False
                                            Else
                                                bExportPayment = True
                                            End If
                                        End If
                                    Else
                                        bExportPayment = False
                                    End If
                                Else
                                    If oPayment.MATCH_Matched > vbBabel.BabelFiles.MatchStatus.ProposedMatched Then '0 Then
                                        If bIncludeOCR Then
                                            bExportPayment = True
                                        Else
                                            If IsOCR(oPayment.PayCode) Then
                                                bExportPayment = False
                                            ElseIf IsAutogiro(oPayment.PayCode) Then
                                                bExportPayment = False
                                            Else
                                                bExportPayment = True
                                            End If
                                        End If
                                    Else
                                        bExportPayment = False
                                    End If
                                End If

                                If bReportOnSelectedItems And bExportPayment Then
                                    If oPayment.ToSpecialReport = False Then
                                        bExportPayment = False
                                    End If
                                End If

                                If bExportPayment Then
                                    If Not EmptyString(sClientNumber) Then
                                        If oPayment.VB_ClientNo = sClientNumber Then
                                            bExportPayment = True
                                        End If
                                    Else
                                        bExportPayment = True
                                    End If
                                End If

                                '06.11.2014 - Added code below
                                If bExportPayment Then
                                    If sReportNumber = "508" Then
                                        bExportPayment = False
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                                                If oInvoice.MATCH_Matched Then
                                                    If oInvoice.MATCH_Final Then
                                                        bExportPayment = True
                                                    End If
                                                End If
                                            End If
                                        Next oInvoice
                                    End If
                                End If

                                iProgress = 3
                                ' Added 12.11.07
                                ' Check if it is a clientseparated report, and if so, correct client
                                '21.04.2015 - Added next IF
                                If Not EmptyString(sClientNumber) Then
                                    If Not oPayment.VB_ClientNo.Trim = sClientNumber Then
                                        bExportPayment = False
                                    End If
                                End If

                                If bExportPayment Then

                                    ' Postiton to a, final invoice
                                    ' Find the next invoice in this payment which is original;
                                    Do Until lInvoices > oPayment.Invoices.Count
                                        bContinue = False

                                        oInvoice = oPayment.Invoices.Item(lInvoices)

                                        bContinue = True

                                        'If (Not oInvoice.MATCH_Matched And oInvoice.MATCH_Final) Or oInvoice.MATCH_Original Then
                                        'bContinue = True
                                        'Else
                                        'bContinue = False
                                        'End If

                                        'If oInvoice.MATCH_Original Then
                                        '    If lFreetexts <= oInvoice.Freetexts.Count Then
                                        '        bContinue = True
                                        '        oFreeText = oInvoice.Freetexts.Item(lFreetexts)
                                        '        'lFreetexts = lFreetexts + 1
                                        '        Exit Do
                                        '    End If
                                        '    If oInvoice.Freetexts.Count = 0 And lFreetexts = 1 Then
                                        '        bContinue = True
                                        '        Exit Do
                                        '    End If

                                        'End If

                                        If bContinue Then
                                            lInvoices = lInvoices + 1
                                            Exit Do
                                        End If
                                        'lFreetexts = 1
                                    Loop 'lInvoices
                                Else
                                    bContinue = False
                                    'lPayments = lPayments + 1
                                    'lInvoices = 1
                                End If 'If frmViewer.CorrectReportClient(oPayment) Then

                                If bContinue Then
                                    Exit Do
                                End If

                            End If

                            lPayments = lPayments + 1
                            lInvoices = 1
                            bFirstVoucherFreetext = True

                        Loop 'lPayments

                        If bContinue Then
                            Exit Do
                        End If
                        lBatches = lBatches + 1
                        lPayments = 1
                    Loop ' lBatches
                    If bContinue Then
                        Exit Do
                    End If
                    lBabelFiles = lBabelFiles + 1
                    lBatches = 1
                Loop ' lBabelFiles

                If lBabelFiles > oBabelFiles.Count Then
                    eArgs.EOF = True
                    lBabelFiles = 0 ' to reset statics next time !

                    Exit Sub
                End If

                iProgress = 4
                If oBabelFile.VB_ProfileInUse Then
                    Me.Fields("Grouping").Value = Trim(Str(oPayment.VB_Profile.Company_ID)) & "-" & Trim(Str(oBabelFile.Index)) & "-" & Trim(Str(oBatch.Index)) & "-" & Trim(Str(oPayment.Index))
                    Me.Fields("Company_ID").Value = oPayment.VB_Profile.Company_ID
                Else
                    Me.Fields("Grouping").Value = "1-" & Trim(Str(oBabelFile.Index)) & "-" & Trim(Str(oBatch.Index)) & "-" & Trim(Str(oPayment.Index))
                    Me.Fields("Company_ID").Value = "1"
                End If
                Me.Fields("Babelfile_ID").Value = oBabelFile.Index
                Me.Fields("Batch_ID").Value = oBatch.Index
                Me.Fields("Payment_ID").Value = oPayment.Index
                Me.Fields("Invoice_ID").Value = oInvoice.Index
                Me.Fields("Filename").Value = Right(oBabelFile.FilenameIn, 20)
                Me.Fields("Client").Value = oPayment.VB_Client
                Me.Fields("DATE_Production").Value = oBabelFile.DATE_Production
                Me.Fields("I_Account").Value = oPayment.I_Account
                'Me.Fields("Paycode").Value = oPayment.PayCode
                iProgress = 43
                Me.Fields("MON_InvoiceAmount").Value = oPayment.MON_InvoiceAmount / 100
                iProgress = 44
                Me.Fields("MON_InvoiceCurrency").Value = oPayment.MON_InvoiceCurrency
                If oPayment.MON_TransferredAmount = 0 Then
                    Me.Fields("MON_TransferredAmount").Value = oPayment.MON_InvoiceAmount / 100
                    Me.Fields("MON_TransferCurrency").Value = oPayment.MON_InvoiceCurrency
                Else
                    Me.Fields("MON_TransferredAmount").Value = oPayment.MON_TransferredAmount / 100
                    Me.Fields("MON_TransferCurrency").Value = oPayment.MON_TransferCurrency
                End If
                'me.txtBatchfooterAmount
                iProgress = 5
                Me.Fields("StatusCode").Value = oPayment.StatusCode
                Me.Fields("StatusText").Value = oPayment.StatusText
                Me.Fields("E_Name").Value = oPayment.E_Name
                Me.Fields("E_Adr1").Value = oPayment.E_Adr1
                Me.Fields("E_Adr2").Value = oPayment.E_Adr2
                Me.Fields("E_Adr3").Value = oPayment.E_Adr3
                Me.Fields("E_Zip").Value = oPayment.E_Zip
                Me.Fields("E_City").Value = oPayment.E_City
                Me.Fields("E_Account").Value = oPayment.E_Account
                Me.Fields("E_Name").Value = oPayment.E_Name
                Me.Fields("REF_Bank1").Value = oPayment.REF_Bank1
                Me.Fields("Unique_ID").Value = oInvoice.Unique_Id
                Me.Fields("REF_Bank1").Value = oPayment.REF_Bank1
                Me.Fields("REF_Bank2").Value = oPayment.REF_Bank2
                Me.Fields("REF_Own").Value = oPayment.REF_Own
                Me.Fields("DATE_Value").Value = oPayment.DATE_Value
                Me.Fields("DATE_Payment").Value = oPayment.DATE_Payment
                Me.Fields("PayCode").Value = oPayment.PayCode
                Me.Fields("PayType").Value = oPayment.PayType

                iProgress = 6
                Me.Fields("BANK_SWIFTCode").Value = oPayment.BANK_SWIFTCode
                Me.Fields("BANK_BranchNo").Value = oPayment.BANK_BranchNo
                Me.Fields("BANK_BranchType").Value = oPayment.BANK_BranchType
                Me.Fields("VoucherNo").Value = oPayment.VoucherNo
                Me.Fields("BabelBank_ID").Value = oPayment.Unique_PaymentID
                Me.Fields("ExtraD").Value = oPayment.ExtraD1
                Me.Fields("Extra1").Value = oInvoice.Extra1
                Me.Fields("MATCH_UseOriginalAmountInMatching").Value = oPayment.MATCH_UseoriginalAmountInMatching
                Me.Fields("ImportFormat").Value = oPayment.ImportFormat
                Me.Fields("MyField").Value = oInvoice.MyField
                Me.Fields("Unique_ID").Value = oInvoice.Unique_Id
                If oInvoice.MATCH_Final Then
                    Me.Fields("InvoiceStatusCode").Value = oInvoice.StatusCode
                    Me.Fields("InvoiceNumber").Value = oInvoice.InvoiceNo
                    Me.Fields("CustomerNumber").Value = oInvoice.CustomerNo
                    Me.Fields("InvoiceAmount").Value = oInvoice.MON_InvoiceAmount / 100
                Else
                    Me.Fields("InvoiceStatusCode").Value = ""
                    Me.Fields("InvoiceNumber").Value = ""
                    Me.Fields("CustomerNumber").Value = ""
                    Me.Fields("InvoiceAmount").Value = ""
                End If

                Me.Fields("Bank").Value = oBabelFile.BankByCode

                iProgress = 7

                '03.12.2012 - New fields
                If oInvoice.MATCH_Final Then
                    Me.Fields("InvoiceLabel").Value = LRS(40051) '"Fakturanr."
                    If Not EmptyString(sBBRET_InvoiceNoLabel) Then
                        Me.Fields("InvoiceLabel").Value = sBBRET_InvoiceNoLabel
                    End If
                    Me.Fields("CustomerLabel").Value = LRS(40073) '"Kundenr."
                    If Not EmptyString(sBBRET_CustomerNoLabel) Then
                        Me.Fields("CustomerLabel").Value = sBBRET_CustomerNoLabel
                    End If
                Else
                    Me.Fields("InvoiceLabel").Value = ""
                    Me.Fields("CustomerLabel").Value = ""
                End If
                Me.Fields("Matched").Value = oInvoice.MATCH_Matched
                Me.Fields("Final").Value = oInvoice.MATCH_Final
                Me.Fields("MATCH_Type").Value = oInvoice.MATCH_MatchType
                Me.Fields("MATCH_ID").Value = oInvoice.MATCH_ID

                ' added 16.09.2020
                Me.Fields("HowMatched").Value = oPayment.MATCH_HowMatched

                eArgs.EOF = False

            End If

            iProgress = 8

            If Not eArgs.EOF Then

                Select Case iBreakLevel
                    Case 0 'NOBREAK
                        Me.Fields("BreakField").Value = ""
                        'Me.lblFilename.Visible = False
                        'Me.txtFilename.Visible = False

                    Case 1 'BREAK_ON_ACCOUNT (and per day)
                        'Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Production").Value
                        ' 03.02.2016 changed to DATE_Payment
                        Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Payment").Value

                    Case 2 'BREAK_ON_BATCH
                        Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value))

                    Case 3 'BREAK_ON_PAYMENT
                        Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value)) & "-" & Trim$(Str(Me.Fields("Payment_ID").Value))

                    Case 4 'BREAK_ON_ACCOUNTONLY
                        Me.Fields("BreakField").Value = Me.Fields("I_Account").Value

                    Case 5 'BREAK_ON_CLIENT 'New
                        Me.Fields("BreakField").Value = Me.Fields("Client").Value

                    Case 999 'Added 06.01.2015 - Use the breaklevel from the special SQL
                        'The break is set in the SQL

                    Case Else
                        Me.Fields("BreakField").Value = ""

                End Select

                iProgress = 9
                'Store the keys for usage in the subreport(s)
                iCompany_ID = Me.Fields("Company_ID").Value
                iBabelfile_ID = Me.Fields("Babelfile_ID").Value
                iBatch_ID = Me.Fields("Batch_ID").Value
                iPayment_ID = Me.Fields("Payment_ID").Value
                iInvoice_ID = Me.Fields("Invoice_ID").Value
                'START WORKING HERE
                If Not IsDBNull(Me.Fields("I_Account").Value) Then
                    sI_Account = Me.Fields("I_Account").Value
                Else
                    sI_Account = ""
                End If
                If sI_Account <> sOldI_Account And Not EmptyString(sI_Account) Then
                    sClientInfo = FindClientName(sI_Account)
                    sOldI_Account = sI_Account
                End If

                Me.txtClientName.Value = sClientInfo

                If Me.Fields("Paytype").Value = "I" Then
                    Me.grPaymentInternationalHeader.Visible = True
                Else
                    Me.grPaymentInternationalHeader.Visible = False
                End If

                sDATE_Production = Me.Fields("DATE_Production").Value
                Me.txtDATE_Production.Value = DateSerial(CInt(Left$(sDATE_Production, 4)), CInt(Mid$(sDATE_Production, 5, 2)), CInt(Right$(sDATE_Production, 2)))
                iProgress = 10
                sDATE_Value = Me.Fields("DATE_Value").Value
                Me.txtDATE_Value.Value = DateSerial(CInt(Left$(sDATE_Value, 4)), CInt(Mid$(sDATE_Value, 5, 2)), CInt(Right$(sDATE_Value, 2)))

                If Me.Fields("Paycode").Value = "602" Then
                    Me.txtE_Name.Value = "GIROMAIL"
                Else
                    Me.txtE_Name.Value = Me.Fields("E_Name").Value
                End If

                If sSpecial = "SG FINANS" Then '28.04.2020 - Added this IF
                    If IsOCR(Me.Fields("Paycode").Value) Then
                        Me.Fields("BabelBank_ID").Value = "KID" & Me.Fields("BabelBank_ID").Value
                    Else
                        Me.Fields("BabelBank_ID").Value = "OSL" & Me.Fields("BabelBank_ID").Value
                    End If
                End If

                iProgress = 11
                If Me.Fields("MATCH_Type").Value = 0 Then 'vbBabel.MatchType.MatchedOnInvoice Then

                    If Me.Fields("InvoiceLabel").Value = "BB_InvoiceLabel" Then
                        Me.txtInvoice.Value = LRS(40051) & " " & Me.Fields("InvoiceNumber").Value
                        'Me.txtInvoice.Value = "Fakturanr." & " " & Me.Fields("InvoiceNumber").Value
                        If Not EmptyString(sBBRET_InvoiceNoLabel) Then
                            Me.txtInvoice.Value = sBBRET_InvoiceNoLabel & " " & Me.Fields("InvoiceNumber").Value
                        End If

                    Else
                        Me.txtInvoice.Value = Me.Fields("InvoiceLabel").Value & " " & Me.Fields("InvoiceNumber").Value
                    End If
                    If sSpecial = "LINDORFF" Then
                        Me.txtCustomer.Value = Me.Fields("CustomerNumber").Value
                        ' added next elseif for Sismo 24.04.2007
                    ElseIf sSpecial = "SISMO" Then
                        Me.txtInvoice.Value = "KID/Krav: " & Me.Fields("Match_ID").Value
                        Me.txtCustomer.Value = "F�dselsnr.: " & Me.Fields("CustomerNumber").Value
                    Else
                        If Me.Fields("CustomerLabel").Value = "BB_CustomerLabel" Then
                            Me.txtCustomer.Value = LRS(40073) & " " & Me.Fields("CustomerNumber").Value
                            If Not EmptyString(sBBRET_CustomerNoLabel) Then
                                Me.txtCustomer.Value = sBBRET_CustomerNoLabel & " " & Me.Fields("CustomerNumber").Value
                            End If
                            'Me.txtCustomer.Value = "Kundenr." & " " & Me.Fields("CustomerNumber").Value
                        Else
                            Me.txtCustomer.Value = Me.Fields("CustomerLabel").Value & " " & Me.Fields("CustomerNumber").Value
                        End If
                    End If
                ElseIf Me.Fields("MATCH_Type").Value = 1 Then 'vbBabel.MatchType.MatchedOnCustomer Then
                    Me.txtInvoice.Value = ""
                    If Me.Fields("CustomerLabel").Value = "BB_CustomerLabel" Then
                        Me.txtCustomer.Value = LRS(40073) & " " & Me.Fields("CustomerNumber").Value
                        If Not EmptyString(sBBRET_CustomerNoLabel) Then
                            Me.txtCustomer.Value = sBBRET_CustomerNoLabel & " " & Me.Fields("CustomerNumber").Value
                        End If
                        'Me.txtCustomer.Value = "Kundenr." & " " & Me.Fields("CustomerNumber").Value
                    Else
                        Me.txtCustomer.Value = Me.Fields("CustomerLabel").Value & " " & Me.Fields("CustomerNumber").Value
                    End If
                ElseIf Me.Fields("MATCH_Type").Value = 2 Then 'vbBabel.MatchType.MatchedOnGL Then
                    If Me.Fields("InvoiceLabel").Value = "BB_GLLabel" Then
                        Me.txtInvoice.Value = LRS(40013) & " " & Me.Fields("Match_ID").Value
                        'Me.txtInvoice.Value = "Kontonr.:" & " " & Me.Fields("Match_ID").Value
                    Else
                        'Me.txtInvoice.Value = Me.Fields("InvoiceLabel").Value & " " & Me.Fields("InvoiceNumber").Value
                        Me.txtInvoice.Value = LRS(40077) & " " & Me.Fields("Match_ID").Value
                    End If
                    Me.txtCustomer.Text = ""
                ElseIf Me.Fields("MATCH_Type").Value = 3 Then 'vbBabel.MatchType.MatchedOnSupplier Then
                    If Me.txtInvoice.Text <> "" Then
                        Me.txtInvoice.Value = LRS(40051) & " " & Me.Fields("InvoiceNumber").Value
                        'Me.txtInvoice.Value = "Fakturanr." & " " & Me.Fields("InvoiceNumber").Value
                    Else
                        Me.txtInvoice.Value = ""
                    End If
                    Me.txtCustomer.Value = LRS(40078) & " " & Me.Fields("CustomerNumber").Value
                    'Me.txtCustomer.Value = "Leverand�rnr." & " " & Me.Fields("CustomerNumber").Value
                Else
                    Me.txtInvoice.Value = ""
                    Me.txtCustomer.Value = ""
                End If

                iProgress = 12

                If Me.Fields("Final").Value = "True" Then
                    Me.txtInvoiceCurrency.Value = Me.Fields("MON_InvoiceCurrency").Value
                Else
                    Me.txtInvoiceCurrency.Value = ""
                End If

                Me.lblOriginalAMountUsed.Visible = False

                If sSpecial = "LINDORFF" Then
                    Me.txtInvoice.Value = Me.Fields("Match_ID").Value
                End If

                sUserText = ""
                If oPayment.Invoices.Count < 3 Then
                    For Each oInvoice In oPayment.Invoices
                        If oInvoice.Freetexts.Count > 0 Then ' And oInvoice.MATCH_Final = True Then
                            For Each oFreeText In oInvoice.Freetexts
                                'If oFreeText.Qualifier = 1 Or oFreeText.Qualifier = 4 Or oFreeText.Qualifier = 5 Then
                                '30.11.2015 - Added this ElseIf
                                sUserText = sUserText & oFreeText.Text
                                sUserText = sUserText & vbCrLf 'Add carriagereturn and line feed to the end
                                'End If
                            Next oFreeText
                        End If
                    Next oInvoice
                End If


                iProgress = 13
                '30.11.2015 - Added next IF
                If Not EmptyString(sUserText) Then
                    Me.txtFreetext.Visible = True
                    If sUserText.Length > 1 Then
                        iProgress = 14
                        sUserText = Left(sUserText, Len(sUserText) - 2)
                        Me.Fields("PaymentConcerns").Value = sUserText
                    End If
                    iProgress = 15
                    If Me.Fields("Matched").Value = True Then
                        bValid = True
                    Else
                        bValid = False
                    End If
                    iProgress = 16
                Else
                    '04.03.2021 - Added this Else
                    Me.Fields("PaymentConcerns").Value = vbNullString
                    Me.lblConcerns.Visible = False
                    Me.txtFreetext.Visible = False
                End If

                'OLD WAY - marked Hushovd in the report - New way markes Boasson Hagen
                'Calculation of totals
                'nCalculatedMacthedAmount = Me.txtInvoiceAmount.Value + nCalculatedMacthedAmount

                'If Me.txtTotalPayment.Text <> "txtTotalPayment" Then
                '    nCalculatedUnMacthedAmount = Me.txtTotalPayment.Text - nCalculatedMacthedAmount
                'End If
                iProgress = 17
                Me.txtInvoice.ForeColor = System.Drawing.Color.Black
                Me.txtCustomer.ForeColor = System.Drawing.Color.Black
                Me.txtUniqueID.ForeColor = System.Drawing.Color.Black
                iProgress = 18
                Me.txtInvoiceCurrency.ForeColor = System.Drawing.Color.Black
                Me.txtInvoiceAmount.ForeColor = System.Drawing.Color.Black
                Me.lblExtra1.ForeColor = System.Drawing.Color.Black
                iProgress = 19
                Me.txtExtra1.ForeColor = System.Drawing.Color.Black
                Me.lblMyField.ForeColor = System.Drawing.Color.Black
                Me.txtMyField.ForeColor = System.Drawing.Color.Black
                'Else
                '    'Set text info in the detailsection to grey
                '    Me.txtInvoice.ForeColor = System.Drawing.Color.Gray
                '    Me.txtCustomer.ForeColor = System.Drawing.Color.Gray
                '    Me.txtUniqueID.ForeColor = System.Drawing.Color.Gray
                '    Me.txtInvoiceCurrency.ForeColor = System.Drawing.Color.Gray
                '    Me.txtInvoiceAmount.ForeColor = System.Drawing.Color.Gray
                '    Me.lblExtra1.ForeColor = System.Drawing.Color.Gray
                '    Me.txtExtra1.ForeColor = System.Drawing.Color.Gray
                '    Me.lblMyField.ForeColor = System.Drawing.Color.Gray
                '    Me.txtMyField.ForeColor = System.Drawing.Color.Gray
                'End If

                iProgress = 20
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub Detail_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail.Format
        Dim rptVoucherFreetext As New rp_Sub_Freetext()
        Dim childFreetextDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        Dim sLabel As String
        Dim sText As String
        Dim pLocation As System.Drawing.PointF
        Dim sUserText As String = String.Empty
        Dim iProgress As Integer

        Try
            iProgress = 1
            If sSpecial = "LINDORFF" Then
                nInvoiceCounter = nInvoiceCounter + 1
                Me.txtInvoice.Value = VB6.Format(nInvoiceCounter, "###") & " Saksnr: " & Me.txtInvoice.Value
            End If

            iProgress = 2
            If EmptyString(Me.txtUniqueID.Value) Then
                Me.txtUniqueID.Visible = False
            Else
                Me.txtUniqueID.Value = "KID: " & Me.txtUniqueID.Value
                Me.txtCustomer.Width = 1.75
                Me.txtUniqueID.Visible = True
                '14.09.2015 - Added next 3 lines
                If Not EmptyString(Me.txtInvoice.Value) Then
                    pLocation.X = 0
                    pLocation.Y = 0.24
                Else
                    pLocation.X = 0
                    pLocation.Y = 0
                End If

                ' 16.09.2020 - removed relocation of Unique ID, as it fucked up the detail section when we had both KID and InvoiceNo
                'Me.txtUniqueID.Location = pLocation
            End If

            sLabel = LRS(40114) '"Bilagstekst:" 
            sText = String.Empty

            iProgress = 3

            'NEW WAY - marked Boasson Hagen in the report - Old way markes Hushovd
            If Me.Fields("Matched").Value = True And Me.Fields("Final").Value = True Then
                Me.txtMatchedAmount.Value = Me.txtMatchedAmount.Value + Me.txtInvoiceAmount.Value
                Me.txtUnmatchedAmount.Value = Me.txtUnmatchedAmount.Value + 0
                Me.txtTotalPayment.Value = Me.txtTotalPayment.Value + Me.txtInvoiceAmount.Value
            Else
                If Me.Fields("Final").Value = True Then
                    Me.txtMatchedAmount.Value = Me.txtMatchedAmount.Value + 0
                    Me.txtUnmatchedAmount.Value = Me.txtUnmatchedAmount.Value + Me.txtInvoiceAmount.Value
                    Me.txtTotalPayment.Value = Me.txtTotalPayment.Value + Me.txtInvoiceAmount.Value
                End If
            End If

            iProgress = 4
            '04.03.2021 - Removed all code rearding Me.Fields("PaymentConcerns")
            'The code is run in FetchData
            'If bValid Then
            '    '17.12.2014 - Added next IF 
            '    'If Me.txtInvoiceAmount.Value = "" Then
            '    '    Me.txtInvoiceAmount.Value = "0"
            '    'End If
            '    iProgress = 39
            '    If EmptyString(Me.txtMatchedBatchAmount.Value) Then
            '    End If
            '    iProgress = 40
            '    If EmptyString(Me.txtMatchedBatchAmount.Value) Then
            '    End If
            '    iProgress = 27
            '    If EmptyString(Me.txtMatchedTotalAmount.Value) Then
            '    End If

            '    iProgress = 41
            '    Me.txtMatchedBatchAmount.Value = Me.txtMatchedBatchAmount.Value + Me.txtInvoiceAmount.Value
            '    iProgress = 42
            '    Me.txtMatchedBatchCount.Value = Me.txtMatchedBatchCount.Value + 1
            '    iProgress = 43
            '    Me.txtMatchedTotalAmount.Value = Me.txtMatchedTotalAmount.Value + Me.txtInvoiceAmount.Value
            '    iProgress = 44
            '    Me.txtMatchedTotalCount.Value = Me.txtMatchedTotalCount.Value + 1
            '    iProgress = 45
            'End If

            'sUserText = ""
            'iProgress = 5
            'If oPayment.Invoices.Count < 3 Then
            '    For Each oInvoice In oPayment.Invoices
            '        If oInvoice.Freetexts.Count > 0 Then ' And oInvoice.MATCH_Final = True Then
            '            For Each oFreeText In oInvoice.Freetexts
            '                'If oFreeText.Qualifier = 1 Or oFreeText.Qualifier = 4 Or oFreeText.Qualifier = 5 Then
            '                '30.11.2015 - Added this ElseIf
            '                sUserText = sUserText & oFreeText.Text
            '                sUserText = sUserText & vbCrLf 'Add carriagereturn and line feed to the end
            '                'End If
            '            Next oFreeText
            '        End If
            '    Next oInvoice
            'End If
            'iProgress = 6
            ''04.03.2021 . Emptying PaymentConcerns (keeps the value from the last oayment)
            'Me.Fields("PaymentConcerns").Value = vbNullString
            '30.11.2015 - Added next IF
            'If Not EmptyString(sUserText) Then
            '    If sUserText.Length > 1 Then

            '        sUserText = Left(sUserText, Len(sUserText) - 2)
            '        Me.Fields("PaymentConcerns").Value = sUserText


            '        ''If bFirstVoucherFreetext Then
            '        'rptVoucherFreetext.Label = LRS(40140) '"Kommentar: "
            '        ''bFirstVoucherFreetext = False
            '        ''Else
            '        ''rptVoucherFreetext.Label = String.Empty 'Hides the label as well
            '        ''End If
            '        'rptVoucherFreetext.Text = sUserText
            '        'rptVoucherFreetext.ReportFromDatabase = False

            '        'childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
            '        'childFreetextDataSource.SQL = "SELECT * FROM Company"
            '        'rptVoucherFreetext.DataSource = childFreetextDataSource
            '        ''Me.SubRptFreetext.Visible = True
            '        ''Me.SubRptFreetext.Report = rptVoucherFreetext
            '        'Me.SubRptVoucherFreetext.Visible = True
            '        'Me.SubRptVoucherFreetext.Report = rptVoucherFreetext
            '        'Me.SubRptVoucherFreetext.Visible = False
            '    Else
            '        'Me.SubRptVoucherFreetext.Visible = False
            '    End If
            'End If

            iProgress = 7
        Catch ex As Exception

        End Try

    End Sub

    Private Sub grFreetextHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grFreetextHeader.Format
        Dim rptFreetext As New rp_Sub_Freetext()
        Dim childFreetextDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        Dim rptFreetext2 As New rp_Sub_Freetext()
        Dim childFreetextDataSource2 As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        Dim rptVoucherFreetext As New rp_Sub_Freetext()
        Dim lTextCounter As Long
        Dim sLabel As String
        Dim sMySQL As String
        Dim sText As String = String.Empty
        Dim oLocalInvoice As vbBabel.Invoice

        'If bReportFromDatabase Then
        '    'Show text on statement
        '    'sLabel = LRS(40054)  ' Text on statement
        '    'childFreetextDataSource2.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
        '    'sMySQL = "SELECT XText AS [Freetext], '" & sLabel & "' AS Label FROM [Freetext] WHERE Company_ID = " & Trim$(Str(iCompany_ID))
        '    'sMySQL = sMySQL & " AND BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
        '    'sMySQL = sMySQL & " AND Batch_ID = " & Trim$(Str(iBatch_ID))
        '    'sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
        '    'If bSQLServer Then
        '    '    sMySQL = sMySQL & " AND Qualifier = 2 AND Len(LTrim(XText)) <> 0 ORDER BY Invoice_ID ASC, Freetext_ID ASC"
        '    'Else
        '    '    sMySQL = sMySQL & " AND Qualifier = 2 AND Trim(XText) <> '' ORDER BY Invoice_ID ASC, Freetext_ID ASC"
        '    'End If

        '    'childFreetextDataSource2.SQL = sMySQL
        '    'rptFreetext2.DataSource = childFreetextDataSource2
        '    'Me.SubRptStatementText.Report = rptFreetext2
        '    'rptFreetext2 = Nothing

        '    childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
        '    sMySQL = "SELECT XText AS [Freetext], '' AS Label FROM [Freetext] WHERE Company_ID = " & Trim$(Str(iCompany_ID))
        '    sMySQL = sMySQL & " AND BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
        '    sMySQL = sMySQL & " AND Batch_ID = " & Trim$(Str(iBatch_ID))
        '    sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
        '    sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
        '    sMySQL = sMySQL & " AND Qualifier = 1 ORDER BY Invoice_ID ASC, Freetext_ID ASC"

        '    childFreetextDataSource.SQL = sMySQL
        '    rptFreetext.DataSource = childFreetextDataSource
        '    If Not bValid Then
        '        rptFreetext.SetColorToGrey(True)
        '    End If
        '    Me.SubRptFreetext.Report = rptFreetext
        '    rptFreetext = Nothing

        'Else
        Me.lblConcerns.Visible = False
        sText = ""
        For Each oLocalInvoice In oPayment.Invoices
            If oLocalInvoice.Freetexts.Count > 0 And oLocalInvoice.MATCH_Original = True Then
                For Each oFreeText In oLocalInvoice.Freetexts
                    If oFreeText.Qualifier = 1 Then
                        sText = sText & oFreeText.Text
                        sText = sText & vbCrLf 'Add carriagereturn and line feed to the end
                        lTextCounter = lTextCounter + 1
                    End If
                Next oFreeText
            End If
        Next oLocalInvoice
        'Remove last vbCrLf
        If sText.Length > 1 Then
            sText = Left(sText, Len(sText) - 2)

            If bFirstVoucherFreetext Then
                rptVoucherFreetext.Label = LRS(40010) '"Bel�pet gjelder:" 
                bFirstVoucherFreetext = False
            Else
                rptVoucherFreetext.Label = String.Empty 'Hides the label as well
            End If
            rptVoucherFreetext.Text = sText
            rptVoucherFreetext.ReportFromDatabase = False

            childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
            childFreetextDataSource.SQL = "SELECT * FROM Company"
            rptVoucherFreetext.DataSource = childFreetextDataSource
            'Me.SubRptFreetext.Visible = True
            'Me.SubRptFreetext.Report = rptVoucherFreetext
            'Me.SubRptVoucherFreetext.Visible = True
            'Me.SubRptVoucherFreetext.Report = rptVoucherFreetext
        Else
            'rptVoucherFreetext.ReportFromDatabase = False
            'rptVoucherFreetext.Text = ""
            'Me.SubRptFreetext.Report = rptVoucherFreetext
            'Me.SubRptFreetext.Visible = False
        End If

        'If Not EmptyString(oPayment.Text_I_Statement) Then
        '    sText = oPayment.Text_I_Statement.Trim

        '    rptFreetext.Label = LRS(40054) 'Text on statement 
        '    rptFreetext.Text = sText
        '    rptFreetext.ReportFromDatabase = False
        '    'Me.SubRptFreetext.Report = rptVoucherFreetext

        '    childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
        '    childFreetextDataSource.SQL = "SELECT * FROM Company"
        '    rptFreetext.DataSource = childFreetextDataSource
        '    Me.SubRptStatementText.Visible = True
        '    Me.SubRptStatementText.Report = rptFreetext

        'Else
        '    rptFreetext.ReportFromDatabase = False
        '    rptFreetext.Text = String.Empty
        '    rptFreetext.Label = String.Empty
        '    Me.SubRptStatementText.Visible = False
        '    Me.SubRptStatementText.Report = rptFreetext
        'End If

        'End If

    End Sub

    Private Sub grPaymentFooter_AfterPrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles grPaymentFooter.AfterPrint
        'Boasson Hagen
        Me.txtMatchedAmount.Value = 0
        Me.txtUnmatchedAmount.Value = 0
        If Not bReportFromDatabase Then
            Me.txtTotalPayment.Value = 0 '03.12.2012
        End If
    End Sub

    Private Sub grPaymentFooter_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles grPaymentFooter.BeforePrint
        'HUSHOVD
        'Me.txtUnmatchedAmount.Value = nCalculatedUnMacthedAmount
    End Sub

    Private Sub grBatchFooter_AfterPrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles grBatchFooter.AfterPrint
        Me.txtMatchedBatchAmount.Value = 0
        Me.txtMatchedBatchCount.Value = 0
    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oBabelFiles = Value
        End Set
    End Property
    Public WriteOnly Property ClientNumber() As String
        Set(ByVal Value As String)
            sClientNumber = Value
        End Set
    End Property
    Public WriteOnly Property IncludeOCR() As Boolean
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub

End Class
