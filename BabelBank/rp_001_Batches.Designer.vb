<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_001_Batches
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OleDBDataSource1 As DataDynamics.ActiveReports.DataSources.OleDBDataSource = New DataDynamics.ActiveReports.DataSources.OleDBDataSource
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rp_001_Batches))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtFilenameIn2 = New DataDynamics.ActiveReports.TextBox
        Me.txtI_Branch = New DataDynamics.ActiveReports.TextBox
        Me.txtSequenceNo = New DataDynamics.ActiveReports.TextBox
        Me.txtDetailDATE_Production = New DataDynamics.ActiveReports.TextBox
        Me.txtNoOfPayments = New DataDynamics.ActiveReports.TextBox
        Me.txtREF_Bank = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_TransferCurrency = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_TransferredAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtHlpFormatType = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.Line3 = New DataDynamics.ActiveReports.Line
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grBabelFileHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.grBabelFileFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.lblTotalNoOfPayments = New DataDynamics.ActiveReports.Label
        Me.LineBabelFileFooter1 = New DataDynamics.ActiveReports.Line
        Me.lblTotalAmount = New DataDynamics.ActiveReports.Label
        Me.LineBabelFileFooter2 = New DataDynamics.ActiveReports.Line
        Me.txtTotalNoOfPayments = New DataDynamics.ActiveReports.TextBox
        Me.txtTotalAmount = New DataDynamics.ActiveReports.TextBox
        Me.TotalSum = New DataDynamics.ActiveReports.Field
        Me.grBatchHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapeBatchHeader = New DataDynamics.ActiveReports.Shape
        Me.lblDate_Production = New DataDynamics.ActiveReports.Label
        Me.txtDATE_Production = New DataDynamics.ActiveReports.TextBox
        Me.lblClient = New DataDynamics.ActiveReports.Label
        Me.txtClientName = New DataDynamics.ActiveReports.TextBox
        Me.txtI_Account = New DataDynamics.ActiveReports.TextBox
        Me.lblI_Account = New DataDynamics.ActiveReports.Label
        Me.txtFilename = New DataDynamics.ActiveReports.TextBox
        Me.grBatchFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.LineBatchFooter1 = New DataDynamics.ActiveReports.Line
        Me.lblBatchfooterAmount = New DataDynamics.ActiveReports.Label
        Me.lblBatchfooterNoofPayments = New DataDynamics.ActiveReports.Label
        Me.txtBatchfooterNoofPayments = New DataDynamics.ActiveReports.TextBox
        Me.txtBatchfooterTransAmount = New DataDynamics.ActiveReports.TextBox
        CType(Me.txtFilenameIn2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Branch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSequenceNo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDetailDATE_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREF_Bank, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_TransferCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_TransferredAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHlpFormatType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchfooterTransAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtFilenameIn2, Me.txtI_Branch, Me.txtSequenceNo, Me.txtDetailDATE_Production, Me.txtNoOfPayments, Me.txtREF_Bank, Me.txtMON_TransferCurrency, Me.txtMON_TransferredAmount, Me.txtHlpFormatType})
        Me.Detail.Height = 0.9479167!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'txtFilenameIn2
        '
        Me.txtFilenameIn2.DataField = "FilenameIn"
        Me.txtFilenameIn2.Height = 0.1875!
        Me.txtFilenameIn2.Left = 0.0!
        Me.txtFilenameIn2.Name = "txtFilenameIn2"
        Me.txtFilenameIn2.Text = "txtFilenameIn2"
        Me.txtFilenameIn2.Top = 0.0!
        Me.txtFilenameIn2.Width = 3.875!
        '
        'txtI_Branch
        '
        Me.txtI_Branch.DataField = "I_Branch"
        Me.txtI_Branch.Height = 0.2!
        Me.txtI_Branch.Left = 4.0!
        Me.txtI_Branch.Name = "txtI_Branch"
        Me.txtI_Branch.Text = "txtI_Branch"
        Me.txtI_Branch.Top = 0.0!
        Me.txtI_Branch.Width = 1.0!
        '
        'txtSequenceNo
        '
        Me.txtSequenceNo.DataField = "SequenceNo"
        Me.txtSequenceNo.Height = 0.2!
        Me.txtSequenceNo.Left = 5.1875!
        Me.txtSequenceNo.Name = "txtSequenceNo"
        Me.txtSequenceNo.Text = "txtSequenceNo"
        Me.txtSequenceNo.Top = 0.0!
        Me.txtSequenceNo.Width = 1.0!
        '
        'txtDetailDATE_Production
        '
        Me.txtDetailDATE_Production.CanShrink = True
        Me.txtDetailDATE_Production.Height = 0.2!
        Me.txtDetailDATE_Production.Left = 0.0!
        Me.txtDetailDATE_Production.Name = "txtDetailDATE_Production"
        Me.txtDetailDATE_Production.Text = "txtDATE_Production"
        Me.txtDetailDATE_Production.Top = 0.25!
        Me.txtDetailDATE_Production.Width = 1.0!
        '
        'txtNoOfPayments
        '
        Me.txtNoOfPayments.DataField = "NoOfPayments"
        Me.txtNoOfPayments.Height = 0.1875!
        Me.txtNoOfPayments.Left = 1.125!
        Me.txtNoOfPayments.Name = "txtNoOfPayments"
        Me.txtNoOfPayments.Text = "txtNoOfPayments"
        Me.txtNoOfPayments.Top = 0.25!
        Me.txtNoOfPayments.Width = 1.5625!
        '
        'txtREF_Bank
        '
        Me.txtREF_Bank.DataField = "REF_Bank"
        Me.txtREF_Bank.Height = 0.2!
        Me.txtREF_Bank.Left = 2.75!
        Me.txtREF_Bank.Name = "txtREF_Bank"
        Me.txtREF_Bank.Text = "txtREF_Bank"
        Me.txtREF_Bank.Top = 0.25!
        Me.txtREF_Bank.Width = 1.0!
        '
        'txtMON_TransferCurrency
        '
        Me.txtMON_TransferCurrency.DataField = "MON_TransferCurrency"
        Me.txtMON_TransferCurrency.Height = 0.2!
        Me.txtMON_TransferCurrency.Left = 4.0!
        Me.txtMON_TransferCurrency.Name = "txtMON_TransferCurrency"
        Me.txtMON_TransferCurrency.Text = "txtMON_TransferCurrency"
        Me.txtMON_TransferCurrency.Top = 0.25!
        Me.txtMON_TransferCurrency.Width = 1.0!
        '
        'txtMON_TransferredAmount
        '
        Me.txtMON_TransferredAmount.CanShrink = True
        Me.txtMON_TransferredAmount.DataField = "MON_TransferredAmount"
        Me.txtMON_TransferredAmount.Height = 0.2!
        Me.txtMON_TransferredAmount.Left = 5.25!
        Me.txtMON_TransferredAmount.Name = "txtMON_TransferredAmount"
        Me.txtMON_TransferredAmount.OutputFormat = resources.GetString("txtMON_TransferredAmount.OutputFormat")
        Me.txtMON_TransferredAmount.Style = "text-align: right"
        Me.txtMON_TransferredAmount.Text = "txtMON_TransferredAmount"
        Me.txtMON_TransferredAmount.Top = 0.25!
        Me.txtMON_TransferredAmount.Width = 1.188!
        '
        'txtHlpFormatType
        '
        Me.txtHlpFormatType.DataField = "FormatType"
        Me.txtHlpFormatType.Height = 0.2!
        Me.txtHlpFormatType.Left = 2.260417!
        Me.txtHlpFormatType.Name = "txtHlpFormatType"
        Me.txtHlpFormatType.Text = "txtFormatType1"
        Me.txtHlpFormatType.Top = 0.6666667!
        Me.txtHlpFormatType.Visible = False
        Me.txtHlpFormatType.Width = 1.0!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.Line3, Me.rptInfoPageHeaderDate})
        Me.PageHeader.Height = 0.4583333!
        Me.PageHeader.Name = "PageHeader"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 1.279861!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "font-size: 18pt; text-align: center"
        Me.lblrptHeader.Text = "Batches"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'Line3
        '
        Me.Line3.Height = 0.0!
        Me.Line3.Left = 0.0!
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.39375!
        Me.Line3.Width = 6.299305!
        Me.Line3.X1 = 0.0!
        Me.Line3.X2 = 6.299305!
        Me.Line3.Y1 = 0.39375!
        Me.Line3.Y2 = 0.39375!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right"
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter.Height = 0.2604167!
        Me.PageFooter.Name = "PageFooter"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right"
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right"
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grBabelFileHeader
        '
        Me.grBabelFileHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grBabelFileHeader.Height = 0.063!
        Me.grBabelFileHeader.Name = "grBabelFileHeader"
        Me.grBabelFileHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'grBabelFileFooter
        '
        Me.grBabelFileFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblTotalNoOfPayments, Me.LineBabelFileFooter1, Me.lblTotalAmount, Me.LineBabelFileFooter2, Me.txtTotalNoOfPayments, Me.txtTotalAmount})
        Me.grBabelFileFooter.Height = 0.3541667!
        Me.grBabelFileFooter.Name = "grBabelFileFooter"
        '
        'lblTotalNoOfPayments
        '
        Me.lblTotalNoOfPayments.Height = 0.2!
        Me.lblTotalNoOfPayments.HyperLink = Nothing
        Me.lblTotalNoOfPayments.Left = 2.9!
        Me.lblTotalNoOfPayments.Name = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Style = "font-size: 10pt; font-weight: bold; ddo-char-set: 1"
        Me.lblTotalNoOfPayments.Text = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Top = 0.1!
        Me.lblTotalNoOfPayments.Width = 1.15!
        '
        'LineBabelFileFooter1
        '
        Me.LineBabelFileFooter1.Height = 0.0!
        Me.LineBabelFileFooter1.Left = 2.9!
        Me.LineBabelFileFooter1.LineWeight = 1.0!
        Me.LineBabelFileFooter1.Name = "LineBabelFileFooter1"
        Me.LineBabelFileFooter1.Top = 0.3!
        Me.LineBabelFileFooter1.Width = 3.6625!
        Me.LineBabelFileFooter1.X1 = 2.9!
        Me.LineBabelFileFooter1.X2 = 6.5625!
        Me.LineBabelFileFooter1.Y1 = 0.3!
        Me.LineBabelFileFooter1.Y2 = 0.3!
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Height = 0.2!
        Me.lblTotalAmount.HyperLink = Nothing
        Me.lblTotalAmount.Left = 4.6!
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Style = "font-size: 10pt; font-weight: bold; ddo-char-set: 1"
        Me.lblTotalAmount.Text = "lblTotalAmount"
        Me.lblTotalAmount.Top = 0.1!
        Me.lblTotalAmount.Width = 0.6!
        '
        'LineBabelFileFooter2
        '
        Me.LineBabelFileFooter2.Height = 0.0!
        Me.LineBabelFileFooter2.Left = 2.9!
        Me.LineBabelFileFooter2.LineWeight = 1.0!
        Me.LineBabelFileFooter2.Name = "LineBabelFileFooter2"
        Me.LineBabelFileFooter2.Top = 0.32!
        Me.LineBabelFileFooter2.Width = 3.66!
        Me.LineBabelFileFooter2.X1 = 2.9!
        Me.LineBabelFileFooter2.X2 = 6.56!
        Me.LineBabelFileFooter2.Y1 = 0.32!
        Me.LineBabelFileFooter2.Y2 = 0.32!
        '
        'txtTotalNoOfPayments
        '
        Me.txtTotalNoOfPayments.CanGrow = False
        Me.txtTotalNoOfPayments.DataField = "NoOfPayments"
        Me.txtTotalNoOfPayments.DistinctField = "NoOfPayments"
        Me.txtTotalNoOfPayments.Height = 0.2!
        Me.txtTotalNoOfPayments.Left = 4.05!
        Me.txtTotalNoOfPayments.Name = "txtTotalNoOfPayments"
        Me.txtTotalNoOfPayments.Style = "font-size: 10pt; font-weight: bold; text-align: right; ddo-char-set: 1"
        Me.txtTotalNoOfPayments.SummaryGroup = "grBatchHeader"
        Me.txtTotalNoOfPayments.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalNoOfPayments.Text = "txtTotalNoOfPayments"
        Me.txtTotalNoOfPayments.Top = 0.1!
        Me.txtTotalNoOfPayments.Width = 0.5!
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.DataField = "MON_TransferredAmount"
        Me.txtTotalAmount.Height = 0.2!
        Me.txtTotalAmount.Left = 5.25!
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat")
        Me.txtTotalAmount.Style = "font-size: 10pt; font-weight: bold; text-align: right; ddo-char-set: 1"
        Me.txtTotalAmount.SummaryGroup = "grBatchHeader"
        Me.txtTotalAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalAmount.Text = "txtTotalAmount"
        Me.txtTotalAmount.Top = 0.1!
        Me.txtTotalAmount.Width = 1.188!
        '
        'TotalSum
        '
        Me.TotalSum.DefaultValue = "100"
        Me.TotalSum.FieldType = DataDynamics.ActiveReports.FieldTypeEnum.None
        Me.TotalSum.Formula = "3+3"
        Me.TotalSum.Name = "TotalSum"
        Me.TotalSum.Tag = Nothing
        '
        'grBatchHeader
        '
        Me.grBatchHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapeBatchHeader, Me.lblDate_Production, Me.txtDATE_Production, Me.lblClient, Me.txtClientName, Me.txtI_Account, Me.lblI_Account, Me.txtFilename})
        Me.grBatchHeader.DataField = "Breakfield"
        Me.grBatchHeader.Height = 0.58!
        Me.grBatchHeader.Name = "grBatchHeader"
        Me.grBatchHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'shapeBatchHeader
        '
        Me.shapeBatchHeader.Height = 0.5!
        Me.shapeBatchHeader.Left = 0.0!
        Me.shapeBatchHeader.Name = "shapeBatchHeader"
        Me.shapeBatchHeader.RoundingRadius = 9.999999!
        Me.shapeBatchHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapeBatchHeader.Top = 0.0!
        Me.shapeBatchHeader.Width = 6.45!
        '
        'lblDate_Production
        '
        Me.lblDate_Production.Height = 0.19!
        Me.lblDate_Production.HyperLink = Nothing
        Me.lblDate_Production.Left = 0.1!
        Me.lblDate_Production.Name = "lblDate_Production"
        Me.lblDate_Production.Style = "font-size: 10pt"
        Me.lblDate_Production.Text = "lblDate_Production"
        Me.lblDate_Production.Top = 0.06!
        Me.lblDate_Production.Width = 1.313!
        '
        'txtDATE_Production
        '
        Me.txtDATE_Production.CanShrink = True
        Me.txtDATE_Production.Height = 0.19!
        Me.txtDATE_Production.Left = 1.5!
        Me.txtDATE_Production.Name = "txtDATE_Production"
        Me.txtDATE_Production.OutputFormat = resources.GetString("txtDATE_Production.OutputFormat")
        Me.txtDATE_Production.Style = "font-size: 10pt"
        Me.txtDATE_Production.Text = "txtDATE_Production"
        Me.txtDATE_Production.Top = 0.0625!
        Me.txtDATE_Production.Width = 1.563!
        '
        'lblClient
        '
        Me.lblClient.Height = 0.19!
        Me.lblClient.HyperLink = Nothing
        Me.lblClient.Left = 3.5!
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Style = "font-size: 10pt"
        Me.lblClient.Text = "lblClient"
        Me.lblClient.Top = 0.0604167!
        Me.lblClient.Width = 1.0!
        '
        'txtClientName
        '
        Me.txtClientName.CanGrow = False
        Me.txtClientName.Height = 0.19!
        Me.txtClientName.Left = 4.5!
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Style = "font-size: 10pt"
        Me.txtClientName.Text = "txtClientName"
        Me.txtClientName.Top = 0.0604167!
        Me.txtClientName.Width = 1.9!
        '
        'txtI_Account
        '
        Me.txtI_Account.DataField = "I_Account"
        Me.txtI_Account.Height = 0.19!
        Me.txtI_Account.Left = 4.5!
        Me.txtI_Account.Name = "txtI_Account"
        Me.txtI_Account.Style = "font-size: 10pt; font-weight: normal"
        Me.txtI_Account.Text = "txtI_Account"
        Me.txtI_Account.Top = 0.2504167!
        Me.txtI_Account.Width = 1.9!
        '
        'lblI_Account
        '
        Me.lblI_Account.Height = 0.19!
        Me.lblI_Account.HyperLink = Nothing
        Me.lblI_Account.Left = 3.5!
        Me.lblI_Account.Name = "lblI_Account"
        Me.lblI_Account.Style = "font-size: 10pt"
        Me.lblI_Account.Text = "lblI_Account"
        Me.lblI_Account.Top = 0.2504167!
        Me.lblI_Account.Width = 1.0!
        '
        'txtFilename
        '
        Me.txtFilename.DataField = "Filename"
        Me.txtFilename.Height = 0.19!
        Me.txtFilename.Left = 1.5!
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.Style = "font-size: 10pt; font-weight: normal"
        Me.txtFilename.Text = "txtFilename_ToMove"
        Me.txtFilename.Top = 0.25!
        Me.txtFilename.Width = 1.9!
        '
        'grBatchFooter
        '
        Me.grBatchFooter.CanShrink = True
        Me.grBatchFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.LineBatchFooter1, Me.lblBatchfooterAmount, Me.lblBatchfooterNoofPayments, Me.txtBatchfooterNoofPayments, Me.txtBatchfooterTransAmount})
        Me.grBatchFooter.Height = 0.25!
        Me.grBatchFooter.Name = "grBatchFooter"
        '
        'LineBatchFooter1
        '
        Me.LineBatchFooter1.Height = 0.0!
        Me.LineBatchFooter1.Left = 2.9!
        Me.LineBatchFooter1.LineWeight = 1.0!
        Me.LineBatchFooter1.Name = "LineBatchFooter1"
        Me.LineBatchFooter1.Top = 0.1875!
        Me.LineBatchFooter1.Width = 3.6625!
        Me.LineBatchFooter1.X1 = 2.9!
        Me.LineBatchFooter1.X2 = 6.5625!
        Me.LineBatchFooter1.Y1 = 0.1875!
        Me.LineBatchFooter1.Y2 = 0.1875!
        '
        'lblBatchfooterAmount
        '
        Me.lblBatchfooterAmount.Height = 0.2!
        Me.lblBatchfooterAmount.HyperLink = Nothing
        Me.lblBatchfooterAmount.Left = 4.6!
        Me.lblBatchfooterAmount.Name = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Style = "font-size: 10pt; ddo-char-set: 1"
        Me.lblBatchfooterAmount.Text = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Top = 0.0!
        Me.lblBatchfooterAmount.Width = 0.6!
        '
        'lblBatchfooterNoofPayments
        '
        Me.lblBatchfooterNoofPayments.Height = 0.2!
        Me.lblBatchfooterNoofPayments.HyperLink = Nothing
        Me.lblBatchfooterNoofPayments.Left = 2.9!
        Me.lblBatchfooterNoofPayments.Name = "lblBatchfooterNoofPayments"
        Me.lblBatchfooterNoofPayments.Style = "font-size: 10pt; ddo-char-set: 1"
        Me.lblBatchfooterNoofPayments.Text = "lblBatchfooterNoofPayments"
        Me.lblBatchfooterNoofPayments.Top = 0.0!
        Me.lblBatchfooterNoofPayments.Width = 1.15!
        '
        'txtBatchfooterNoofPayments
        '
        Me.txtBatchfooterNoofPayments.DataField = "NoOfPayments"
        Me.txtBatchfooterNoofPayments.DistinctField = "NoOfPayments"
        Me.txtBatchfooterNoofPayments.Height = 0.2!
        Me.txtBatchfooterNoofPayments.Left = 4.05!
        Me.txtBatchfooterNoofPayments.Name = "txtBatchfooterNoofPayments"
        Me.txtBatchfooterNoofPayments.Style = "font-size: 10pt; text-align: right; ddo-char-set: 1"
        Me.txtBatchfooterNoofPayments.SummaryGroup = "grBatchHeader"
        Me.txtBatchfooterNoofPayments.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtBatchfooterNoofPayments.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtBatchfooterNoofPayments.Text = "txtBatchfooterNoofPayments"
        Me.txtBatchfooterNoofPayments.Top = 0.0!
        Me.txtBatchfooterNoofPayments.Width = 0.5!
        '
        'txtBatchfooterTransAmount
        '
        Me.txtBatchfooterTransAmount.DataField = "MON_TransferredAmount"
        Me.txtBatchfooterTransAmount.Height = 0.2!
        Me.txtBatchfooterTransAmount.Left = 5.25!
        Me.txtBatchfooterTransAmount.Name = "txtBatchfooterTransAmount"
        Me.txtBatchfooterTransAmount.OutputFormat = resources.GetString("txtBatchfooterTransAmount.OutputFormat")
        Me.txtBatchfooterTransAmount.Style = "font-size: 10pt; text-align: right; ddo-char-set: 1"
        Me.txtBatchfooterTransAmount.SummaryGroup = "grBatchHeader"
        Me.txtBatchfooterTransAmount.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtBatchfooterTransAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtBatchfooterTransAmount.Text = "txtBatchfooterTransAmount"
        Me.txtBatchfooterTransAmount.Top = 0.0!
        Me.txtBatchfooterTransAmount.Width = 1.188!
        '
        'rp_001_Batches
        '
        Me.MasterReport = False
        Me.CalculatedFields.Add(Me.TotalSum)
        OleDBDataSource1.ConnectionString = ""
        OleDBDataSource1.SQL = "Select * from"
        Me.DataSource = OleDBDataSource1
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 6.489583!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.grBabelFileHeader)
        Me.Sections.Add(Me.grBatchHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grBatchFooter)
        Me.Sections.Add(Me.grBabelFileFooter)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'Times New Roman'; font-style: italic; font-variant: inherit; font-w" & _
                    "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("", "Heading4", "Normal"))
        CType(Me.txtFilenameIn2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Branch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSequenceNo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDetailDATE_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREF_Bank, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_TransferCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_TransferredAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHlpFormatType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchfooterTransAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader
    Friend WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Friend WithEvents Line3 As DataDynamics.ActiveReports.Line
    Friend WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter
    Friend WithEvents grBabelFileHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents grBabelFileFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents TotalSum As DataDynamics.ActiveReports.Field
    Friend WithEvents grBatchHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents grBatchFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents txtFilenameIn2 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtI_Branch As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtSequenceNo As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtDetailDATE_Production As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtNoOfPayments As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtREF_Bank As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_TransferCurrency As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_TransferredAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtHlpFormatType As DataDynamics.ActiveReports.TextBox
    Friend WithEvents shapeBatchHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents lblDate_Production As DataDynamics.ActiveReports.Label
    Friend WithEvents txtDATE_Production As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblClient As DataDynamics.ActiveReports.Label
    Friend WithEvents txtClientName As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtI_Account As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblI_Account As DataDynamics.ActiveReports.Label
    Friend WithEvents LineBatchFooter1 As DataDynamics.ActiveReports.Line
    Friend WithEvents lblBatchfooterAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents lblBatchfooterNoofPayments As DataDynamics.ActiveReports.Label
    Friend WithEvents lblTotalNoOfPayments As DataDynamics.ActiveReports.Label
    Friend WithEvents LineBabelFileFooter1 As DataDynamics.ActiveReports.Line
    Friend WithEvents lblTotalAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents LineBabelFileFooter2 As DataDynamics.ActiveReports.Line
    Friend WithEvents txtBatchfooterNoofPayments As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtBatchfooterTransAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTotalNoOfPayments As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTotalAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtFilename As DataDynamics.ActiveReports.TextBox
End Class
