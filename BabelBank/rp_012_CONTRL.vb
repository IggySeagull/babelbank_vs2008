Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_012_CONTRL
    Dim sOldI_Account As String
    Dim sI_Account As String
    Dim sClientInfo As String
    Dim iBabelFile_ID As Integer
    Dim iBatch_ID As Integer
    Dim sBreakField As String

    Dim iDetailLevel As Integer
    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean

    Dim sReportName As String
    Dim sSpecial As String
    Dim bSQLServer As Boolean = False
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean

    Private Sub rp_012_CONTRL_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        Fields.Add("BreakField")
        Fields.Add("DATE_Production")
    End Sub

    Private Sub rp_012_CONTRL_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        Me.Cancel()
    End Sub

    Private Sub rp_012_CONTRL_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        iBabelFile_ID = -1
        iBatch_ID = -1
        bShowBatchfooterTotals = True
        sBreakField = ""

        Select Case iBreakLevel
            Case 0 'NOBREAK
                bShowBatchfooterTotals = False
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False

            Case 1 'BREAK_ON_ACCOUNT (and per day)
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case 2 'BREAK_ON_BATCH
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case 3 'BREAK_ON_PAYMENT
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case 4 'BREAK_ON_ACCOUNTONLY
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case 5 'BREAK_ON_CLIENT 'New
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = False

            Case Else 'NOBREAK
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False

        End Select

        'Long or ShortDate
        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtDATE_Production.OutputFormat = "D"
            Me.txtStatus.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtDATE_Production.OutputFormat = "d"
            Me.txtStatus.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = "CONTRL"
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4.2
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'End If

        'grBatchHeader
        'Me.lblFilename.Text = LRS(40001) '"Filnavn:" 
        Me.lblDate_Production.Text = LRS(40027) '"Prod.dato:" 
        Me.lblClient.Text = LRS(40030) '"Klient:" 
        Me.lblI_Account.Text = LRS(40020) '"Mottakerkonto:" 

        'Detail
        Me.lblSender.Text = LRS(40100) & ":" '"Avsender" & ":" 
        Me.lblReceiver.Text = LRS(48011) '"Mottaker:" 
        Me.lblFileID.Text = LRS(40099) & ":" '"Fil ID" & ":" 

        'grBabelFileFooter
        Me.lblNoOfErrors.Text = LRS(40111) '"Antall feil:" 

        ' Show details or not
        If Not bShowBatchfooterTotals Then
            Me.grBatchFooter.Visible = False
        End If
        If Not bShowDATE_Production Then
            Me.txtDATE_Production.Visible = False
        End If
        If Not bShowClientName Then
            Me.txtClientName.Visible = False
        End If
        If Not bShowI_Account Then
            Me.txtI_Account.Visible = False
        End If

        Me.txtNoOfErrors.Text = "0"

    End Sub

    Private Sub rp_012_CONTRL_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        Dim sDATE_Production As String

        If Not eArgs.EOF Then

            Select Case iBreakLevel
                Case 0 'NOBREAK
                    Me.Fields("BreakField").Value = ""
                    'Me.lblFilename.Visible = False
                    'Me.txtFilename.Visible = False

                Case 1 'BREAK_ON_ACCOUNT (and per day)
                    'Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Production").Value
                    ' 03.02.2016 changed to DATE_Payment
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Payment").Value

                Case 2 'BREAK_ON_BATCH
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value))

                Case 3 'BREAK_ON_PAYMENT
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value)) & "-" & Trim$(Str(Me.Fields("Payment_ID").Value))

                Case 4 'BREAK_ON_ACCOUNTONLY
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value

                Case 5 'BREAK_ON_CLIENT 'New
                    Me.Fields("BreakField").Value = Me.Fields("Client").Value

                Case Else
                    Me.Fields("BreakField").Value = ""

            End Select

            'START WORKING HERE

            sDATE_Production = Me.Fields("DATE_Production").Value
            '24.10.2018 - Added next If
            If Not EmptyString(sDATE_Production) Then
                Me.txtDATE_Production.Value = DateSerial(CInt(Left$(sDATE_Production, 4)), CInt(Mid$(sDATE_Production, 5, 2)), CInt(Right$(sDATE_Production, 2)))
            End If

            '13.10.2009 - Changed from oBabelFile.StatusCode = "4" to oBabelFile.StatusCode <> "7"
            If Me.Fields("BabelStatusCode").Value <> "7" And Not (Me.Fields("BabelStatusCode").Value = "01" Or Me.Fields("BabelStatusCode").Value = "02") Then
                If IsDBNull(Me.Fields("XText").Value) Then
                    Me.txtStatus.Text = "CONTRL FEIL, Statuskode " & ": " & Me.Fields("BabelStatusCode").Value
                Else
                    If Not EmptyString(Me.Fields("XText").Value) Then
                        Me.txtStatus.Text = "CONTRL FEIL, Statuskode " & ": " & Me.Fields("BabelStatusCode").Value & " - " & Me.Fields("XText").Value
                    ElseIf Not EmptyString(Me.Fields("StatusText").Value) Then
                        Me.txtStatus.Text = "CONTRL FEIL, Statuskode " & ": " & Me.Fields("BabelStatusCode").Value & " - " & Me.Fields("StatusText").Value
                    Else
                        Me.txtStatus.Text = "CONTRL FEIL, Statuskode " & ": " & Me.Fields("BabelStatusCode").Value
                    End If
                End If
                'LRS(40097) & " " & LRS(40101) & ": " & oBabelFile.StatusCode     '"CONTRL FEIL, Statuskode " & oBabelFile.StatusCode
                Me.txtNoOfErrors.Text = Val(Me.txtNoOfErrors.Text) + 1 'Used to count errors
            ElseIf Me.Fields("BatchStatusCode").Value <> "7" And Not (Me.Fields("BatchStatusCode").Value = "01" Or Me.Fields("BatchStatusCode").Value = "02") Then
                If Not EmptyString(Me.Fields("XText").Value) Then
                    Me.txtStatus.Text = "CONTRL FEIL, Statuskode " & ": " & Me.Fields("BatchStatusCode").Value & " - " & Me.Fields("XText").Value
                ElseIf Not EmptyString(Me.Fields("StatusText").Value) Then
                    Me.txtStatus.Text = "CONTRL FEIL, Statuskode " & ": " & Me.Fields("BatchStatusCode").Value & " - " & Me.Fields("StatusText").Value
                Else
                    Me.txtStatus.Text = "CONTRL FEIL, Statuskode " & ": " & Me.Fields("BatchStatusCode").Value
                End If
                'LRS(40097) & " " & LRS(40101) & ": " & oBatch.StatusCode '  "CONTRL FEIL, Statuskode " & oBatch.StatusCode
                Me.txtNoOfErrors.Text = Val(Me.txtNoOfErrors.Text) + 1 'Used to count errors
            Else
                Me.txtStatus.Text = "CONTRL OK" 'LRS(40098)   '"CONTRL OK"
                Me.txtErrorMark.Text = "0" 'Used to count errors
            End If

        End If

    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class
