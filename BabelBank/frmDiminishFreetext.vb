Option Strict Off
Option Explicit On
Friend Class frmDiminishFreetext
    Inherits System.Windows.Forms.Form
    Public bCancelled As Boolean
    Private Sub thisForm_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        e.Graphics.DrawLine(Pens.Orange, 9, 424, 592, 424)
    End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click

        Me.Hide()
        bCancelled = True

    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click

        Me.Hide()
        bCancelled = False

    End Sub

    Private Sub frmDiminishFreetext_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        bCancelled = False

    End Sub
End Class
