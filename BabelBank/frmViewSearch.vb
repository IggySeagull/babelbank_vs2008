﻿Public Class frmViewFilter
    Dim grid As DataGridView
    Dim oBabelFiles As BabelFiles
    Dim bSearchButtonPressed As Boolean = False
    Const ShowAll = 1
    Const ShowNextOnly = 0
    Private Sub frmViewSearch_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim working_area As Rectangle = SystemInformation.WorkingArea
        Dim x As Integer = working_area.Left + working_area.Width - Me.Width
        Dim y As Integer = working_area.Top + working_area.Height - Me.Height - 50

        Me.Location = New Point(x, y)  'Position to lower right corner
        FormLRSCaptions(Me)
        Me.lblPassedEnd.Text = ""
        Me.grid.Select()
        Me.txtSearchFor.Focus()
    End Sub
    Public Sub passGrid(ByVal g As DataGridView)
        grid = g
    End Sub
    Public Sub SetBabelFilesInViewSearch(ByVal obj As BabelFiles)
        ' pass BabelFilesbject to frmViewPaymentDetail
        oBabelFiles = obj
    End Sub

    Private Sub DoTheSearch(ByVal iMode As Integer)
        Dim sSearchFor As String
        Dim x As Integer
        Dim y As Integer = grid.CurrentCell.ColumnIndex + 1
        Dim iCurrentCol As Integer = grid.CurrentCell.ColumnIndex
        Dim iCurrentRow As Integer = grid.CurrentRow.Index
        Static iLastX As Integer = 0
        Static iLastY As Integer = 0
        Static nNoOfFoundRows As Long
        Dim c As DataGridViewCell
        Dim bFound As Boolean = False
        Dim sCellValue As String = ""
        Dim lStartedSearchAtRow As Long

        lStartedSearchAtRow = grid.CurrentRow.Index
        sSearchFor = UCase(Trim(Me.txtSearchFor.Text))
        Me.lblPassedEnd.Text = ""

        ' --------------------------------
        ' Remove markers from last search:
        ' --------------------------------
        If nNoOfFoundRows > 0 Then
            x = 0
            While x < grid.Rows.Count
                y = 0
                While y < grid.Rows(x).Cells.Count
                    grid.Rows(x).Cells(y).Style.Font = (New Font(Me.Font, FontStyle.Regular))
                    y = y + 1
                End While
                x = x + 1
            End While
        Else
            If iLastX > 0 Then
                ' reset font from last found
                grid.Rows(iLastX).Cells(iLastY).Style.Font = (New Font(Me.Font, FontStyle.Regular))
                iLastX = 0
                iLastY = 0
            End If
        End If

        If iMode = ShowAll Then
            x = 0
            y = 0
        Else
            x = iLastX
            y = iLastY
            ' advance to next cell
            If x >= grid.Rows.Count Then
                x = 0
                y = 0
            End If
            If y >= grid.Rows(x).Cells.Count Then
                x = Math.Min(grid.Rows.Count, grid.CurrentRow.Index + 1)
            Else
                y = y + 1
            End If
        End If

        nNoOfFoundRows = 0
        While x < grid.Rows.Count
            While y < grid.Rows(x).Cells.Count
                bFound = False
                c = grid.Rows(x).Cells(y)
                If c.Visible Then
                    If Not c.Value Is DBNull.Value Or Nothing Then
                        sCellValue = UCase(CType(c.Value, String))
                        If Me.chkFromStartOfCell.Checked Then
                            ' search from start of columncontent
                            If Strings.Left(sCellValue, Len(sSearchFor)) = sSearchFor Then
                                bFound = True
                                nNoOfFoundRows = nNoOfFoundRows + 1
                            End If
                        Else
                            ' search inside field
                            If InStr(sCellValue, sSearchFor) > 0 Then
                                bFound = True
                                nNoOfFoundRows = nNoOfFoundRows + 1
                            End If
                        End If
                        If bFound And (Me.chkCurrenctColumn.Checked = False Or (y = iCurrentCol)) Then
                            ' positon to cell
                            grid.CurrentCell = grid(y, x)
                            grid.Rows(x).Cells(y).Selected = True
                            iLastX = x
                            iLastY = y
                            ' change to bold
                            grid.Rows(x).Cells(y).Style.Font = (New Font(Me.Font, FontStyle.Bold))
                            grid.Focus()
                            If iMode = ShowNextOnly Then
                                Exit While
                            End If
                        Else
                            bFound = False
                        End If
                    End If
                End If
                System.Math.Min(System.Threading.Interlocked.Increment(y), y - 1)
            End While
            If bFound Then
                'If Not Me.chkFilter.Checked Then  ' do not jump out if filter or show all
                If iMode = ShowNextOnly Then
                    Exit While
                End If

            Else
                'If Me.chkFilter.Checked Then
                'grid.Rows(x).Visible = False
                ' to report, set a marker that this one will NOT be reported
                'oBabelFiles(Me.grid.Rows(x).Cells(0).Value).Batches(Me.grid.Rows(x).Cells(1).Value).Payments(Me.grid.Rows(x).Cells(2).Value).ToSpecialReport = False
                'End If
            End If
            System.Math.Min(System.Threading.Interlocked.Increment(x), x - 1)
            y = 0
            If x = grid.Rows.Count And lStartedSearchAtRow > 0 And iMode = 0 Then
                ' wrap, and say that we have reached the end
                Me.lblPassedEnd.Text = "00000-Vi har passert siste linje."
                ' fortsett søket;
                x = 0
                lStartedSearchAtRow = 0 ' for ikke å gå i evig loop
            End If
        End While
        ' position back if not found
        If nNoOfFoundRows = 0 Then
            iLastX = 0
            iLastY = 0
            'If Not Me.chkFilter.Checked Then  ' do not jump out if filterThen
            grid.CurrentCell = grid(iCurrentCol, iCurrentRow)
            grid.Rows(iCurrentRow).Cells(iCurrentCol).Selected = True
            'Else
            'grid.CurrentCell = grid(y, x) ' last found
            'grid.Rows(x).Cells(y).Selected = True
            'End If
        End If
        ' TODO position grid to show selected line on middle
        grid.Focus()
    End Sub
    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        bSearchButtonPressed = True
        DoTheSearch(0)  ' next only
    End Sub

    Private Sub cmdAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAll.Click
        ' if filter, show the filter in label
        'If Me.chkFilter.Checked Then
        'Me.lblFilter.Text = "Filter: " & Replace(Me.lblFilter.Text, "Filter:", "") & " " & Trim(Me.txtSearchFor.Text)
        'End If
        bSearchButtonPressed = True
        DoTheSearch(1)  ' all
    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Close()
        Me.Dispose()
    End Sub
    Private Sub txtSearch_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles txtSearchFor.KeyUp
        Try
            bSearchButtonPressed = False
            If e.KeyCode = Keys.Enter Then
                cmdNext_Click(sender, e)
            End If
            If e.KeyCode = Keys.Escape Then
                Me.Close()
                Me.Dispose()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub chkFilter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFilter.CheckedChanged
        If chkFilter.Checked Then
            If Not bSearchButtonPressed Then
                ' run a search first, to mark rows
                cmdAll_Click(sender, e)
            End If
            If Len(Me.txtSearchFor.Text) > 0 Then
                ' set filter to rows which hit the search
                SetFilter()
            End If
        Else
            ' take away filter
            RemoveFilter()
        End If
    End Sub
    Private Sub RemoveFilter()
        ' run through all lines, and set visible = true
        Dim x, y As Integer
        Dim iCurrentCol As Integer
        Dim iCurrentRow As Integer

        Me.lblFilter.Text = ""
        If grid.CurrentCell.ColumnIndex < 0 Then
            iCurrentCol = 0
        Else
            iCurrentCol = grid.CurrentCell.ColumnIndex
        End If
        If grid.CurrentRow.Index < 0 Then
            iCurrentRow = 0
        Else
            iCurrentRow = grid.CurrentRow.Index
        End If

        While x < grid.Rows.Count
            grid.Rows(x).Visible = True
            'y = 0
            'While y < grid.Rows(x).Cells.Count
            '    grid.Rows(x).Cells(y).Style.Font = (New Font(Me.Font, FontStyle.Regular))
            '    System.Math.Min(System.Threading.Interlocked.Increment(y), y - 1)
            'End While
            System.Math.Min(System.Threading.Interlocked.Increment(x), x - 1)
        End While
        ' position back if not found
        grid.CurrentCell = grid(iCurrentCol, iCurrentRow)
        grid.Rows(iCurrentRow).Cells(iCurrentCol).Selected = True
        grid.Focus()

    End Sub
    Private Sub SetFilter()
        ' run through all lines, and set visible = false in rows that are "bolded"
        Dim x, y As Integer
        Dim iCurrentCol As Integer = grid.CurrentCell.ColumnIndex
        Dim iCurrentRow As Integer = grid.CurrentRow.Index
        Dim bFilterOut As Boolean = False

        If grid.CurrentCell.ColumnIndex < 0 Then
            iCurrentCol = 0
        Else
            iCurrentCol = grid.CurrentCell.ColumnIndex
        End If
        If grid.CurrentRow.Index < 0 Then
            iCurrentRow = 0
        Else
            iCurrentRow = grid.CurrentRow.Index
        End If

        x = 0
        While x < grid.Rows.Count
            y = 0
            bFilterOut = False
            While y < grid.Rows(x).Cells.Count
                If grid.Rows(x).Cells(y).Style.Font Is Nothing Then
                    bFilterOut = False
                ElseIf grid.Rows(x).Cells(y).Style.Font.Style = FontStyle.Regular Then
                    bFilterOut = False
                ElseIf grid.Rows(x).Cells(y).Style.Font.Style = FontStyle.Bold Then
                    bFilterOut = True
                    Exit While
                End If
                System.Math.Min(System.Threading.Interlocked.Increment(y), y - 1)
            End While
            If bFilterOut Then
                grid.Rows(x).Visible = True
            Else
                grid.Rows(x).Visible = False
            End If
            System.Math.Min(System.Threading.Interlocked.Increment(x), x - 1)
        End While
        ' position back 
        If grid(iCurrentCol, iCurrentRow).Visible = True Then
            grid.CurrentCell = grid(iCurrentCol, iCurrentRow)
            grid.Rows(iCurrentRow).Cells(iCurrentCol).Selected = True
        End If
        grid.Focus()

    End Sub

    Private Sub txtSearchFor_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchFor.TextChanged
        bSearchButtonPressed = False
    End Sub
End Class