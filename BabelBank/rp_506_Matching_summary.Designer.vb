<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_506_Matching_summary
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rp_506_Matching_summary))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtHeading = New DataDynamics.ActiveReports.TextBox
        Me.txtOCRCount = New DataDynamics.ActiveReports.TextBox
        Me.txtOCRAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtUnmatchedCount = New DataDynamics.ActiveReports.TextBox
        Me.txtUnmatchedAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtMatchedCount = New DataDynamics.ActiveReports.TextBox
        Me.txtMatchedAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtAllCount = New DataDynamics.ActiveReports.TextBox
        Me.txtAllAmount = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.Line3 = New DataDynamics.ActiveReports.Line
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grBabelFileHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.grBabelFileFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.lblTotalNoOfPayments = New DataDynamics.ActiveReports.Label
        Me.LineBabelFileFooter1 = New DataDynamics.ActiveReports.Line
        Me.lblTotalAmount = New DataDynamics.ActiveReports.Label
        Me.LineBabelFileFooter2 = New DataDynamics.ActiveReports.Line
        Me.txtTotalNoOfPayments = New DataDynamics.ActiveReports.TextBox
        Me.txtTotalAmount = New DataDynamics.ActiveReports.TextBox
        Me.grBatchHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapeBatchHeader = New DataDynamics.ActiveReports.Shape
        Me.lblDate_Production = New DataDynamics.ActiveReports.Label
        Me.txtDATE_Production = New DataDynamics.ActiveReports.TextBox
        Me.lblOCR = New DataDynamics.ActiveReports.Label
        Me.lblUnmatched = New DataDynamics.ActiveReports.Label
        Me.lblMatched = New DataDynamics.ActiveReports.Label
        Me.lblTotal = New DataDynamics.ActiveReports.Label
        Me.grBatchFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.TotalSum = New DataDynamics.ActiveReports.Field
        Me.grTypeHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.txtType = New DataDynamics.ActiveReports.TextBox
        Me.grTypeFooter = New DataDynamics.ActiveReports.GroupFooter
        CType(Me.txtHeading, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOCRCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOCRAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnmatchedCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnmatchedAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMatchedCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMatchedAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAllCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAllAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblOCR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblUnmatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtHeading, Me.txtOCRCount, Me.txtOCRAmount, Me.txtUnmatchedCount, Me.txtUnmatchedAmount, Me.txtMatchedCount, Me.txtMatchedAmount, Me.txtAllCount, Me.txtAllAmount})
        Me.Detail.Height = 0.2604167!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'txtHeading
        '
        Me.txtHeading.Border.BottomColor = System.Drawing.Color.Black
        Me.txtHeading.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtHeading.Border.LeftColor = System.Drawing.Color.Black
        Me.txtHeading.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtHeading.Border.RightColor = System.Drawing.Color.Black
        Me.txtHeading.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtHeading.Border.TopColor = System.Drawing.Color.Black
        Me.txtHeading.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtHeading.Height = 0.15!
        Me.txtHeading.Left = 0.0!
        Me.txtHeading.Name = "txtHeading"
        Me.txtHeading.Style = "font-size: 8.25pt; "
        Me.txtHeading.Text = "txtHeading"
        Me.txtHeading.Top = 0.0!
        Me.txtHeading.Width = 1.4!
        '
        'txtOCRCount
        '
        Me.txtOCRCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtOCRCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtOCRCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtOCRCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtOCRCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRCount.DataField = "NoOfOCR"
        Me.txtOCRCount.Height = 0.15!
        Me.txtOCRCount.Left = 1.45!
        Me.txtOCRCount.Name = "txtOCRCount"
        Me.txtOCRCount.OutputFormat = resources.GetString("txtOCRCount.OutputFormat")
        Me.txtOCRCount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtOCRCount.Text = "txtOCRCount"
        Me.txtOCRCount.Top = 0.0!
        Me.txtOCRCount.Width = 0.3!
        '
        'txtOCRAmount
        '
        Me.txtOCRAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtOCRAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtOCRAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtOCRAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtOCRAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRAmount.Height = 0.15!
        Me.txtOCRAmount.Left = 1.8!
        Me.txtOCRAmount.Name = "txtOCRAmount"
        Me.txtOCRAmount.OutputFormat = resources.GetString("txtOCRAmount.OutputFormat")
        Me.txtOCRAmount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtOCRAmount.Text = "txtOCRAmount"
        Me.txtOCRAmount.Top = 0.0!
        Me.txtOCRAmount.Width = 0.825!
        '
        'txtUnmatchedCount
        '
        Me.txtUnmatchedCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtUnmatchedCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnmatchedCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtUnmatchedCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnmatchedCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtUnmatchedCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnmatchedCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtUnmatchedCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnmatchedCount.DataField = "NoOfUnmatched"
        Me.txtUnmatchedCount.Height = 0.15!
        Me.txtUnmatchedCount.Left = 2.705!
        Me.txtUnmatchedCount.Name = "txtUnmatchedCount"
        Me.txtUnmatchedCount.OutputFormat = resources.GetString("txtUnmatchedCount.OutputFormat")
        Me.txtUnmatchedCount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtUnmatchedCount.Text = "txtUnmatchedCount"
        Me.txtUnmatchedCount.Top = 0.0!
        Me.txtUnmatchedCount.Width = 0.3!
        '
        'txtUnmatchedAmount
        '
        Me.txtUnmatchedAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtUnmatchedAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnmatchedAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtUnmatchedAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnmatchedAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtUnmatchedAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnmatchedAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtUnmatchedAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnmatchedAmount.Height = 0.15!
        Me.txtUnmatchedAmount.Left = 3.055!
        Me.txtUnmatchedAmount.Name = "txtUnmatchedAmount"
        Me.txtUnmatchedAmount.OutputFormat = resources.GetString("txtUnmatchedAmount.OutputFormat")
        Me.txtUnmatchedAmount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtUnmatchedAmount.Text = "txtUnmatchedAmount"
        Me.txtUnmatchedAmount.Top = 0.0!
        Me.txtUnmatchedAmount.Width = 0.825!
        '
        'txtMatchedCount
        '
        Me.txtMatchedCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtMatchedCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMatchedCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtMatchedCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMatchedCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtMatchedCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMatchedCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtMatchedCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMatchedCount.DataField = "NoOfMatched"
        Me.txtMatchedCount.Height = 0.15!
        Me.txtMatchedCount.Left = 3.97!
        Me.txtMatchedCount.Name = "txtMatchedCount"
        Me.txtMatchedCount.OutputFormat = resources.GetString("txtMatchedCount.OutputFormat")
        Me.txtMatchedCount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtMatchedCount.Text = "txtMatchedCount"
        Me.txtMatchedCount.Top = 0.0!
        Me.txtMatchedCount.Width = 0.3!
        '
        'txtMatchedAmount
        '
        Me.txtMatchedAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtMatchedAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMatchedAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtMatchedAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMatchedAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtMatchedAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMatchedAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtMatchedAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMatchedAmount.Height = 0.15!
        Me.txtMatchedAmount.Left = 4.32!
        Me.txtMatchedAmount.Name = "txtMatchedAmount"
        Me.txtMatchedAmount.OutputFormat = resources.GetString("txtMatchedAmount.OutputFormat")
        Me.txtMatchedAmount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtMatchedAmount.Text = "txtMatchedAmount"
        Me.txtMatchedAmount.Top = 0.0!
        Me.txtMatchedAmount.Width = 0.825!
        '
        'txtAllCount
        '
        Me.txtAllCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAllCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAllCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAllCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAllCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtAllCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAllCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtAllCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAllCount.DataField = "NoOfRecords"
        Me.txtAllCount.Height = 0.15!
        Me.txtAllCount.Left = 5.225!
        Me.txtAllCount.Name = "txtAllCount"
        Me.txtAllCount.OutputFormat = resources.GetString("txtAllCount.OutputFormat")
        Me.txtAllCount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtAllCount.Text = "txtAllCount"
        Me.txtAllCount.Top = 0.0!
        Me.txtAllCount.Width = 0.3!
        '
        'txtAllAmount
        '
        Me.txtAllAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAllAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAllAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAllAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAllAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtAllAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAllAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtAllAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAllAmount.DataField = "TotalAmount"
        Me.txtAllAmount.Height = 0.15!
        Me.txtAllAmount.Left = 5.575!
        Me.txtAllAmount.Name = "txtAllAmount"
        Me.txtAllAmount.OutputFormat = resources.GetString("txtAllAmount.OutputFormat")
        Me.txtAllAmount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtAllAmount.Text = "txtAllAmount"
        Me.txtAllAmount.Top = 0.0!
        Me.txtAllAmount.Width = 0.825!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.Line3, Me.rptInfoPageHeaderDate})
        Me.PageHeader.Height = 0.4576389!
        Me.PageHeader.Name = "PageHeader"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Border.BottomColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Border.LeftColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Border.RightColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Border.TopColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 1.279861!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "text-align: center; font-size: 18pt; "
        Me.lblrptHeader.Text = "Matching summary"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'Line3
        '
        Me.Line3.Border.BottomColor = System.Drawing.Color.Black
        Me.Line3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.LeftColor = System.Drawing.Color.Black
        Me.Line3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.RightColor = System.Drawing.Color.Black
        Me.Line3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.TopColor = System.Drawing.Color.Black
        Me.Line3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Height = 0.0!
        Me.Line3.Left = 0.0!
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.39375!
        Me.Line3.Width = 6.45!
        Me.Line3.X1 = 0.0!
        Me.Line3.X2 = 6.45!
        Me.Line3.Y1 = 0.39375!
        Me.Line3.Y2 = 0.39375!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right; "
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter.Height = 0.2604167!
        Me.PageFooter.Name = "PageFooter"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right; "
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right; "
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grBabelFileHeader
        '
        Me.grBabelFileHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grBabelFileHeader.Height = 0.0625!
        Me.grBabelFileHeader.Name = "grBabelFileHeader"
        Me.grBabelFileHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'grBabelFileFooter
        '
        Me.grBabelFileFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblTotalNoOfPayments, Me.LineBabelFileFooter1, Me.lblTotalAmount, Me.LineBabelFileFooter2, Me.txtTotalNoOfPayments, Me.txtTotalAmount})
        Me.grBabelFileFooter.Height = 0.3541667!
        Me.grBabelFileFooter.Name = "grBabelFileFooter"
        '
        'lblTotalNoOfPayments
        '
        Me.lblTotalNoOfPayments.Border.BottomColor = System.Drawing.Color.Black
        Me.lblTotalNoOfPayments.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotalNoOfPayments.Border.LeftColor = System.Drawing.Color.Black
        Me.lblTotalNoOfPayments.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotalNoOfPayments.Border.RightColor = System.Drawing.Color.Black
        Me.lblTotalNoOfPayments.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotalNoOfPayments.Border.TopColor = System.Drawing.Color.Black
        Me.lblTotalNoOfPayments.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotalNoOfPayments.Height = 0.2!
        Me.lblTotalNoOfPayments.HyperLink = Nothing
        Me.lblTotalNoOfPayments.Left = 3.0!
        Me.lblTotalNoOfPayments.Name = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; "
        Me.lblTotalNoOfPayments.Text = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Top = 0.1!
        Me.lblTotalNoOfPayments.Width = 1.1!
        '
        'LineBabelFileFooter1
        '
        Me.LineBabelFileFooter1.Border.BottomColor = System.Drawing.Color.Black
        Me.LineBabelFileFooter1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LineBabelFileFooter1.Border.LeftColor = System.Drawing.Color.Black
        Me.LineBabelFileFooter1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LineBabelFileFooter1.Border.RightColor = System.Drawing.Color.Black
        Me.LineBabelFileFooter1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LineBabelFileFooter1.Border.TopColor = System.Drawing.Color.Black
        Me.LineBabelFileFooter1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LineBabelFileFooter1.Height = 0.0!
        Me.LineBabelFileFooter1.Left = 3.0!
        Me.LineBabelFileFooter1.LineWeight = 1.0!
        Me.LineBabelFileFooter1.Name = "LineBabelFileFooter1"
        Me.LineBabelFileFooter1.Top = 0.3!
        Me.LineBabelFileFooter1.Width = 3.5625!
        Me.LineBabelFileFooter1.X1 = 3.0!
        Me.LineBabelFileFooter1.X2 = 6.5625!
        Me.LineBabelFileFooter1.Y1 = 0.3!
        Me.LineBabelFileFooter1.Y2 = 0.3!
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.lblTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotalAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.lblTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotalAmount.Border.RightColor = System.Drawing.Color.Black
        Me.lblTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotalAmount.Border.TopColor = System.Drawing.Color.Black
        Me.lblTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotalAmount.Height = 0.2!
        Me.lblTotalAmount.HyperLink = Nothing
        Me.lblTotalAmount.Left = 4.6!
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Style = "ddo-char-set: 1; font-weight: bold; font-size: 10pt; "
        Me.lblTotalAmount.Text = "lblTotalAmount"
        Me.lblTotalAmount.Top = 0.1!
        Me.lblTotalAmount.Width = 0.6000001!
        '
        'LineBabelFileFooter2
        '
        Me.LineBabelFileFooter2.Border.BottomColor = System.Drawing.Color.Black
        Me.LineBabelFileFooter2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LineBabelFileFooter2.Border.LeftColor = System.Drawing.Color.Black
        Me.LineBabelFileFooter2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LineBabelFileFooter2.Border.RightColor = System.Drawing.Color.Black
        Me.LineBabelFileFooter2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LineBabelFileFooter2.Border.TopColor = System.Drawing.Color.Black
        Me.LineBabelFileFooter2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LineBabelFileFooter2.Height = 0.0!
        Me.LineBabelFileFooter2.Left = 3.0!
        Me.LineBabelFileFooter2.LineWeight = 1.0!
        Me.LineBabelFileFooter2.Name = "LineBabelFileFooter2"
        Me.LineBabelFileFooter2.Top = 0.32!
        Me.LineBabelFileFooter2.Width = 3.56!
        Me.LineBabelFileFooter2.X1 = 3.0!
        Me.LineBabelFileFooter2.X2 = 6.56!
        Me.LineBabelFileFooter2.Y1 = 0.32!
        Me.LineBabelFileFooter2.Y2 = 0.32!
        '
        'txtTotalNoOfPayments
        '
        Me.txtTotalNoOfPayments.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalNoOfPayments.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalNoOfPayments.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalNoOfPayments.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalNoOfPayments.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalNoOfPayments.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalNoOfPayments.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalNoOfPayments.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalNoOfPayments.CanGrow = False
        Me.txtTotalNoOfPayments.DataField = "NoOfPayments"
        Me.txtTotalNoOfPayments.DistinctField = "NoOfPayments"
        Me.txtTotalNoOfPayments.Height = 0.2!
        Me.txtTotalNoOfPayments.Left = 4.1!
        Me.txtTotalNoOfPayments.Name = "txtTotalNoOfPayments"
        Me.txtTotalNoOfPayments.Style = "ddo-char-set: 1; text-align: right; font-weight: bold; font-size: 10pt; "
        Me.txtTotalNoOfPayments.Text = "txtTotalNoOfPayments"
        Me.txtTotalNoOfPayments.Top = 0.1!
        Me.txtTotalNoOfPayments.Width = 0.45!
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalAmount.Height = 0.2!
        Me.txtTotalAmount.Left = 5.25!
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat")
        Me.txtTotalAmount.Style = "ddo-char-set: 1; text-align: right; font-weight: bold; font-size: 10pt; "
        Me.txtTotalAmount.Text = "txtTotalAmount"
        Me.txtTotalAmount.Top = 0.1!
        Me.txtTotalAmount.Width = 1.188!
        '
        'grBatchHeader
        '
        Me.grBatchHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapeBatchHeader, Me.lblDate_Production, Me.txtDATE_Production, Me.lblOCR, Me.lblUnmatched, Me.lblMatched, Me.lblTotal})
        Me.grBatchHeader.DataField = "BreakField"
        Me.grBatchHeader.Height = 0.875!
        Me.grBatchHeader.Name = "grBatchHeader"
        Me.grBatchHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'shapeBatchHeader
        '
        Me.shapeBatchHeader.Border.BottomColor = System.Drawing.Color.Black
        Me.shapeBatchHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapeBatchHeader.Border.LeftColor = System.Drawing.Color.Black
        Me.shapeBatchHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapeBatchHeader.Border.RightColor = System.Drawing.Color.Black
        Me.shapeBatchHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapeBatchHeader.Border.TopColor = System.Drawing.Color.Black
        Me.shapeBatchHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapeBatchHeader.Height = 0.3!
        Me.shapeBatchHeader.Left = 0.0!
        Me.shapeBatchHeader.Name = "shapeBatchHeader"
        Me.shapeBatchHeader.RoundingRadius = 9.999999!
        Me.shapeBatchHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapeBatchHeader.Top = 0.0!
        Me.shapeBatchHeader.Width = 6.45!
        '
        'lblDate_Production
        '
        Me.lblDate_Production.Border.BottomColor = System.Drawing.Color.Black
        Me.lblDate_Production.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDate_Production.Border.LeftColor = System.Drawing.Color.Black
        Me.lblDate_Production.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDate_Production.Border.RightColor = System.Drawing.Color.Black
        Me.lblDate_Production.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDate_Production.Border.TopColor = System.Drawing.Color.Black
        Me.lblDate_Production.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDate_Production.Height = 0.19!
        Me.lblDate_Production.HyperLink = Nothing
        Me.lblDate_Production.Left = 0.1!
        Me.lblDate_Production.Name = "lblDate_Production"
        Me.lblDate_Production.Style = "font-size: 10pt; "
        Me.lblDate_Production.Text = "lblDate_Production"
        Me.lblDate_Production.Top = 0.06!
        Me.lblDate_Production.Width = 1.313!
        '
        'txtDATE_Production
        '
        Me.txtDATE_Production.Border.BottomColor = System.Drawing.Color.Black
        Me.txtDATE_Production.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDATE_Production.Border.LeftColor = System.Drawing.Color.Black
        Me.txtDATE_Production.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDATE_Production.Border.RightColor = System.Drawing.Color.Black
        Me.txtDATE_Production.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDATE_Production.Border.TopColor = System.Drawing.Color.Black
        Me.txtDATE_Production.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDATE_Production.CanShrink = True
        Me.txtDATE_Production.Height = 0.19!
        Me.txtDATE_Production.Left = 1.5!
        Me.txtDATE_Production.Name = "txtDATE_Production"
        Me.txtDATE_Production.OutputFormat = resources.GetString("txtDATE_Production.OutputFormat")
        Me.txtDATE_Production.Style = "font-size: 10pt; "
        Me.txtDATE_Production.Text = "txtDATE_Production"
        Me.txtDATE_Production.Top = 0.0625!
        Me.txtDATE_Production.Width = 1.563!
        '
        'lblOCR
        '
        Me.lblOCR.Border.BottomColor = System.Drawing.Color.Black
        Me.lblOCR.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblOCR.Border.LeftColor = System.Drawing.Color.Black
        Me.lblOCR.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblOCR.Border.RightColor = System.Drawing.Color.Black
        Me.lblOCR.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblOCR.Border.TopColor = System.Drawing.Color.Black
        Me.lblOCR.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblOCR.Height = 0.19!
        Me.lblOCR.HyperLink = Nothing
        Me.lblOCR.Left = 1.5!
        Me.lblOCR.Name = "lblOCR"
        Me.lblOCR.Style = "text-align: center; font-size: 10pt; "
        Me.lblOCR.Text = "lblOCR"
        Me.lblOCR.Top = 0.625!
        Me.lblOCR.Width = 1.1!
        '
        'lblUnmatched
        '
        Me.lblUnmatched.Border.BottomColor = System.Drawing.Color.Black
        Me.lblUnmatched.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblUnmatched.Border.LeftColor = System.Drawing.Color.Black
        Me.lblUnmatched.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblUnmatched.Border.RightColor = System.Drawing.Color.Black
        Me.lblUnmatched.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblUnmatched.Border.TopColor = System.Drawing.Color.Black
        Me.lblUnmatched.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblUnmatched.Height = 0.19!
        Me.lblUnmatched.HyperLink = Nothing
        Me.lblUnmatched.Left = 2.68!
        Me.lblUnmatched.Name = "lblUnmatched"
        Me.lblUnmatched.Style = "text-align: center; font-size: 10pt; "
        Me.lblUnmatched.Text = "lblUnmatched"
        Me.lblUnmatched.Top = 0.625!
        Me.lblUnmatched.Width = 1.1!
        '
        'lblMatched
        '
        Me.lblMatched.Border.BottomColor = System.Drawing.Color.Black
        Me.lblMatched.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblMatched.Border.LeftColor = System.Drawing.Color.Black
        Me.lblMatched.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblMatched.Border.RightColor = System.Drawing.Color.Black
        Me.lblMatched.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblMatched.Border.TopColor = System.Drawing.Color.Black
        Me.lblMatched.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblMatched.Height = 0.19!
        Me.lblMatched.HyperLink = Nothing
        Me.lblMatched.Left = 3.87!
        Me.lblMatched.Name = "lblMatched"
        Me.lblMatched.Style = "text-align: center; font-size: 10pt; "
        Me.lblMatched.Text = "lblMatched"
        Me.lblMatched.Top = 0.625!
        Me.lblMatched.Width = 1.1!
        '
        'lblTotal
        '
        Me.lblTotal.Border.BottomColor = System.Drawing.Color.Black
        Me.lblTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotal.Border.LeftColor = System.Drawing.Color.Black
        Me.lblTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotal.Border.RightColor = System.Drawing.Color.Black
        Me.lblTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotal.Border.TopColor = System.Drawing.Color.Black
        Me.lblTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotal.Height = 0.19!
        Me.lblTotal.HyperLink = Nothing
        Me.lblTotal.Left = 5.05!
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Style = "text-align: center; font-size: 10pt; "
        Me.lblTotal.Text = "lblTotal"
        Me.lblTotal.Top = 0.625!
        Me.lblTotal.Width = 1.1!
        '
        'grBatchFooter
        '
        Me.grBatchFooter.CanShrink = True
        Me.grBatchFooter.Height = 0.04166667!
        Me.grBatchFooter.Name = "grBatchFooter"
        '
        'TotalSum
        '
        Me.TotalSum.DefaultValue = "100"
        Me.TotalSum.FieldType = DataDynamics.ActiveReports.FieldTypeEnum.None
        Me.TotalSum.Formula = "3+3"
        Me.TotalSum.Name = "TotalSum"
        Me.TotalSum.Tag = Nothing
        '
        'grTypeHeader
        '
        Me.grTypeHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtType})
        Me.grTypeHeader.DataField = "Type"
        Me.grTypeHeader.Height = 0.3333333!
        Me.grTypeHeader.Name = "grTypeHeader"
        '
        'txtType
        '
        Me.txtType.Border.BottomColor = System.Drawing.Color.Black
        Me.txtType.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtType.Border.LeftColor = System.Drawing.Color.Black
        Me.txtType.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtType.Border.RightColor = System.Drawing.Color.Black
        Me.txtType.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtType.Border.TopColor = System.Drawing.Color.Black
        Me.txtType.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtType.Height = 0.19!
        Me.txtType.Left = 0.0!
        Me.txtType.Name = "txtType"
        Me.txtType.Style = "font-size: 10pt; "
        Me.txtType.Text = "txtType"
        Me.txtType.Top = 0.1!
        Me.txtType.Width = 1.9!
        '
        'grTypeFooter
        '
        Me.grTypeFooter.Height = 0.05208333!
        Me.grTypeFooter.Name = "grTypeFooter"
        '
        'rp_506_Matching_summary
        '
        Me.MasterReport = False
        Me.CalculatedFields.Add(Me.TotalSum)
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 6.489583!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.grBabelFileHeader)
        Me.Sections.Add(Me.grBatchHeader)
        Me.Sections.Add(Me.grTypeHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grTypeFooter)
        Me.Sections.Add(Me.grBatchFooter)
        Me.Sections.Add(Me.grBabelFileFooter)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'Times New Roman'; font-style: italic; font-variant: inherit; font-w" & _
                    "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; " & _
                    "", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("", "Heading4", "Normal"))
        CType(Me.txtHeading, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOCRCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOCRAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnmatchedCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnmatchedAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMatchedCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMatchedAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAllCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAllAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblOCR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblUnmatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents txtHeading As DataDynamics.ActiveReports.TextBox
    Friend WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader
    Friend WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Friend WithEvents Line3 As DataDynamics.ActiveReports.Line
    Friend WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter
    Friend WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents grBabelFileHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents grBabelFileFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents lblTotalNoOfPayments As DataDynamics.ActiveReports.Label
    Friend WithEvents LineBabelFileFooter1 As DataDynamics.ActiveReports.Line
    Friend WithEvents lblTotalAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents LineBabelFileFooter2 As DataDynamics.ActiveReports.Line
    Friend WithEvents txtTotalNoOfPayments As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTotalAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents grBatchHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents shapeBatchHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents lblDate_Production As DataDynamics.ActiveReports.Label
    Friend WithEvents txtDATE_Production As DataDynamics.ActiveReports.TextBox
    Friend WithEvents grBatchFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents TotalSum As DataDynamics.ActiveReports.Field
    Friend WithEvents lblOCR As DataDynamics.ActiveReports.Label
    Friend WithEvents lblUnmatched As DataDynamics.ActiveReports.Label
    Friend WithEvents lblMatched As DataDynamics.ActiveReports.Label
    Friend WithEvents lblTotal As DataDynamics.ActiveReports.Label
    Friend WithEvents grTypeHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents txtType As DataDynamics.ActiveReports.TextBox
    Friend WithEvents grTypeFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents txtOCRCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtOCRAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtUnmatchedCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtUnmatchedAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMatchedCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMatchedAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtAllCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtAllAmount As DataDynamics.ActiveReports.TextBox
End Class
