Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

'Things to do when implementing this report in BB
'Search for 'LRS(, and replace the text with the LRS
'Search for vbBabel.MatchType. and replace the number stated with the correct Enum

Public Class rp_505_Cremul_Control

    'NBNBNBNBNBNB
    'If this report is to be used, we have to alter the calculation of the totals like we do on rp_504
    'Don't use the suumarization in AR, but calculate the totals in the code
    'NBNBNBNBNBNB
    Dim iCompany_ID As Integer
    Dim iBabelfile_ID As Integer
    Dim iBatch_ID As Integer
    Dim iPayment_ID As Integer
    Dim iInvoice_ID As Integer
    Dim sOldI_Account As String
    Dim sI_Account As String
    Dim sClientInfo As String
    Dim sClientName As String

    Dim nCalculatedMacthedAmount As Double
    Dim nCalculatedUnMacthedAmount As Double
    Dim nACalculatedMacthedAmount As Double
    Dim nACalculatedUnMacthedAmount As Double
    Dim nInvoiceCounter As Double

    Dim sBreakField As String
    Dim nTotalAmount As Double
    Dim bBreak As Boolean
    Dim sOrderBy As String
    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean

    Dim sReportName As String
    Dim sSpecial As String
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim bIncludeOCR As Boolean
    Dim bSQLServer As Boolean = False
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean

    Private Sub rp_505_Cremul_Unmatched_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        'Set the different breaklevels

        Fields.Add("BreakField")

    End Sub

    Private Sub rp_505_Cremul_Unmatched_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        Me.Cancel()
    End Sub

    Private Sub rp_505_Cremul_Unmatched_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportStart

        bShowBatchfooterTotals = True

        Select Case iBreakLevel
            Case 0 'NOBREAK
                bShowBatchfooterTotals = False
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False

            Case 1 'BREAK_ON_ACCOUNT (and per day)
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case 2 'BREAK_ON_BATCH
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case 3 'BREAK_ON_PAYMENT
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case 4 'BREAK_ON_ACCOUNTONLY
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case 5 'BREAK_ON_CLIENT 'New
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = False

            Case 999
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case Else 'NOBREAK
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False

        End Select

        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtDATE_Production.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtDATE_Production.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = LRS(40033) '  Control Report
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'Me.rptInfoPageCounterGB.Left = 4
        'End If

        'Set the caption of the labels

        'grBatchHeader
        'Me.lblFilename.Text = LRS(40001) '"Filnavn:" 
        Me.lblDate_Production.Text = LRS(40027) '"Prod.dato:" 
        Me.lblClient.Text = LRS(40030) '"Klient:" 
        Me.lblI_Account.Text = LRS(40020) '"Mottakerkonto:" 

        Me.lblName.Text = LRS(60067) '"Betaler" 
        Me.lblRef.Text = LRS(40022) 'Reference
        Me.lblMatched.Text = LRS(60088) ' Matched
        Me.lblUnMatched.Text = LRS(40029) 'Unmatched
        Me.lblAccount.Text = LRS(60089) 'Innbetalt
        Me.lblDiff.Text = LRS(40032) 'Difference

        'grBatchFooter
        Me.lblBatchfooterAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblBatchfooterNoofPayments.Text = LRS(40105) '"Deltotal - Antall:" 

        'grBabelFileFooter
        Me.lblTotalAmount.Text = LRS(40104) '"Bel�p:" 

        sOldI_Account = ""
        sClientInfo = LRS(40107) '"Ingen klientinfo angitt" 
        sBreakField = ""
        bBreak = True
        Me.txtTotalAmount.Value = 0

        If Not bShowBatchfooterTotals Then
            Me.grBatchFooter.Visible = False
        End If
        If Not bShowDATE_Production Then
            Me.txtDATE_Production.Visible = False
        End If
        If Not bShowClientName Then
            Me.txtClientName.Visible = False
        End If
        If Not bShowI_Account Then
            Me.txtI_Account.Visible = False
        End If

    End Sub

    Private Sub rp_505_Cremul_Unmatched_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        'The Fields collection should never be accessed outside the DataInitialize and FetchData events
        Dim sDATE_Production As String

        If Not eArgs.EOF Then

            Select Case iBreakLevel
                Case 0 'NOBREAK
                    Me.Fields("BreakField").Value = ""
                    'Me.lblFilename.Visible = False
                    'Me.txtFilename.Visible = False

                Case 1 'BREAK_ON_ACCOUNT (and per day)
                    'Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Production").Value
                    ' 03.02.2016 changed to DATE_Payment
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Payment").Value

                Case 2 'BREAK_ON_BATCH
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value))

                Case 3 'BREAK_ON_PAYMENT
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value)) & "-" & Trim$(Str(Me.Fields("Payment_ID").Value))

                Case 4 'BREAK_ON_ACCOUNTONLY
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value

                Case 5 'BREAK_ON_CLIENT 'New
                    Me.Fields("BreakField").Value = Me.Fields("Client").Value

                Case 999 'Added 06.01.2015 - Use the breaklevel from the special SQL
                    'The break is set in the SQL

                Case Else
                    Me.Fields("BreakField").Value = ""

            End Select

            ''Store the keys for usage in the subreport(s)
            'iCompany_ID = Me.Fields("Company_ID").Value
            'iBabelfile_ID = Me.Fields("Babelfile_ID").Value
            'iBatch_ID = Me.Fields("Batch_ID").Value
            'iPayment_ID = Me.Fields("Payment_ID").Value
            'iInvoice_ID = Me.Fields("Invoice_ID").Value
            'START WORKING HERE
            If Not IsDBNull(Me.Fields("I_Account").Value) Then
                sI_Account = Me.Fields("I_Account").Value
            Else
                sI_Account = ""
            End If
            If sI_Account <> sOldI_Account And Not EmptyString(sI_Account) Then
                sClientInfo = FindClientName(sI_Account)
                sOldI_Account = sI_Account
            End If

            Me.txtClientName.Value = sClientInfo

            sDATE_Production = Me.Fields("DATE_Production").Value
            Me.txtDATE_Production.Value = DateSerial(CInt(Left$(sDATE_Production, 4)), CInt(Mid$(sDATE_Production, 5, 2)), CInt(Right$(sDATE_Production, 2)))

            'If IsDBNull(Me.txtMatched.Value) Then
            '    Me.txtMatched.Value = 0
            'End If
            'If IsDBNull(Me.txtUnMatched.Value) Then
            '    Me.txtUnMatched.Value = 0
            'End If
            'Me.txtDiff.Value = Me.txtAccount.Value - Me.txtMatched.Value - Me.txtUnMatched.Value

            'If Not IsEqualAmount(Me.txtDiff.Value, 0) Then
            '    'Me.txtName.Font.Bold = True
            '    'Me.txtName.Font = New DataDynamics.ActiveReports.Chart.FontInfo(Color.White, New System.Drawing.Font("Arial", 10.0F, FontStyle.Bold))
            '    Me.txtName.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Bold)

            'End If

        End If

        'Me.txtTotalAmount.Value = Me.txtTotalAmount.Value + Me.txtTotalAmount.Value

    End Sub

    Private Sub Detail_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail.Format

        If IsDBNull(Me.txtMatched.Value) Then
            Me.txtMatched.Value = 0
        End If
        If IsDBNull(Me.txtUnMatched.Value) Then
            Me.txtUnMatched.Value = 0
        End If

        Me.txtDiff.Value = Me.txtAccount.Value - Me.txtMatched.Value - Me.txtUnMatched.Value

        If Not IsEqualAmount(Me.txtDiff.Value, 0) Then
            Me.txtTotalAmount.Value = Me.txtTotalAmount.Value + Me.txtDiff.Value
            Me.txtName.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Bold)
            Me.txtRef.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Bold)
            Me.txtMatched.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Bold)
            Me.txtUnMatched.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Bold)
            Me.txtAccount.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Bold)
            Me.txtDiff.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Bold)
        Else
            Me.txtName.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Regular)
            Me.txtRef.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Regular)
            Me.txtMatched.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Regular)
            Me.txtUnMatched.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Regular)
            Me.txtAccount.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Regular)
            Me.txtDiff.Font = New System.Drawing.Font("Arial", 8.0F, FontStyle.Regular)
        End If

    End Sub

    Private Sub grBabelFileFooter_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles grBabelFileFooter.BeforePrint

        If Me.txtTotalAmount.Value Is Nothing Then
            bEmptyReport = True
        Else
            If IsEqualAmount(Me.txtTotalAmount.Value, 0) Then
                bEmptyReport = True
            Else
                bEmptyReport = False
            End If
        End If

    End Sub

    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property IncludeOCR() As Boolean
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class
