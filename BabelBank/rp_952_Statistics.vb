Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_952_Statistics

    Dim sOldI_Account As String

    Dim iDetailLevel As Integer
    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean
    Dim sStartDate As String
    Dim sEndDate As String

    Dim sReportName As String
    Dim sSpecial As String
    Dim bSQLServer As Boolean = False
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean

    Private Sub rp_952_Statistics_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize

    End Sub

    Private Sub rp_952_Statistics_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        Me.Cancel()
    End Sub

    Private Sub rp_952_Statistics_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        'Long or ShortDate
        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Landscape

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = LRS(40128)   ' BabelBank statistics
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4.2
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'End If

        'grMainHeader
        Me.lblDateInterval.Text = "Datointervall: " '& sFromDate & " - " & sToDate
        Me.lblPayments.Text = LRS(60002) '"Betalinger" 
        Me.lblPercent.Text = "%"
        Me.lblAmount.Text = LRS(40021) '"Bel�p" 
        Me.lblInvoices.Text = LRS(40129) '"Fakturaer" 
        Me.lblPercentInv.Text = "%"

        'grAccumulatedHeader
        Me.lblTotal.Text = LRS(40130) '"Totalt importert" 
        Me.lblOCR.Text = "OCR"
        Me.lblAutogiro.Text = LRS(64038) '"Autogiro" 
        Me.lblOther.Text = LRS(40131) '"Andre" 
        Me.lblAuto.Text = LRS(40132) '"Automatpostert (inkl. forslagsavstemte)" 
        Me.lblProposed.Text = LRS(40133) '"Forslag" 
        Me.lblFinal.Text = LRS(40134) '"Endelig" 

        'Detail
        'Me.lblType.Text = LRS(40134) '"Endelig" - Retrieved from DB

        'grAccumulatedFooter
        Me.lblUnPostedAuto.Text = LRS(40135) '"Upostert etter automatisk" 
        Me.lblManual.Text = LRS(40136) '"Manuelt postert" 
        Me.lblUnMatched.Text = LRS(40137) '"Upostert etter manuell avstemming" 

    End Sub

    Private Sub rp_952_Statistics_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData

        If Not eArgs.EOF Then

            'START WORKING HERE
            Me.txtTotalPercent.Value = 100

        End If

    End Sub
    Private Sub Detail_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail.Format
        Dim rptAutomatch As New rp_Sub_952_AutoMatch
        Dim childFreetextDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        Dim sMySQL As String

        'Show text on statement
        childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
        sMySQL = "SELECT Client_ID AS ClientNo, Type, SUM(NoOfPayments) AS TypeCount, SUM(NoOfInvoices) AS InvoiceSum, "
        sMySQL = sMySQL & "Sum(Amount)/100 AS TypeAmount, Sum(Proposed) AS TypeProposed, Sum(Finally) AS TypeFinal "
        sMySQL = sMySQL & "FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND InfoType = 'Auto' AND Type <> 'OPEN ITEM' "
        sMySQL = sMySQL & "GROUP BY Type, Client_ID, InfoType ORDER BY Client_ID"

        childFreetextDataSource.SQL = sMySQL
        rptAutomatch.DataSource = childFreetextDataSource

        If IsDBNull(Me.txtAutoCount.Value) Then
            rptAutomatch.SetAutoCount(0)
        Else
            rptAutomatch.SetAutoCount(CLng(Me.txtAutoCount.Value))
        End If
        If IsDBNull(Me.txtAutoFinal.Value) Then
            If IsDBNull(Me.txtAutoProposed.Value) Then
                rptAutomatch.SetAutoCountInv(0)
            Else
                rptAutomatch.SetAutoCountInv(CLng(Me.txtAutoProposed.Value))
            End If
        Else
            If IsDBNull(Me.txtAutoProposed.Value) Then
                rptAutomatch.SetAutoCountInv(CLng(Me.txtAutoFinal.Value))
            Else
                rptAutomatch.SetAutoCountInv(CLng(Me.txtAutoFinal.Value + Me.txtAutoProposed.Value))
            End If
        End If
        Me.subAutoMatched.Report = rptAutomatch
        rptAutomatch = Nothing

    End Sub
    Private Sub grAccumulatedFooter_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grAccumulatedFooter.Format
        Dim rptManualMatch As New rp_Sub_952_AutoMatch
        Dim childManualDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        Dim sMySQL As String

        'Show text on statement
        childManualDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
        sMySQL = "SELECT Client_ID AS ClientNo, Type, SUM(NoOfPayments) AS TypeCount, SUM(NoOfInvoices) AS InvoiceSum, "
        sMySQL = sMySQL & "Sum(Amount)/100 AS TypeAmount, Sum(Proposed) AS TypeProposed, Sum(Finally) AS TypeFinal "
        sMySQL = sMySQL & "FROM [Statistics] WHERE Date_Run >= '" & sStartDate & "' AND Date_Run <= '" & sEndDate & "' AND InfoType = 'Manual' AND Type <> 'OPEN ITEM' "
        sMySQL = sMySQL & "GROUP BY Type, Client_ID, InfoType ORDER BY Client_ID"

        childManualDataSource.SQL = sMySQL
        rptManualMatch.DataSource = childManualDataSource
        Me.subManualMatched.Report = rptManualMatch
        rptManualMatch = Nothing

    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property StartDate() As String
        Set(ByVal Value As String)
            sStartDate = Value
        End Set
    End Property
    Public WriteOnly Property EndDate() As String
        Set(ByVal Value As String)
            sEndDate = Value
        End Set
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class
