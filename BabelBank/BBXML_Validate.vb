﻿Option Strict Off
Option Explicit On
Imports System.Xml
Imports System.Xml.Schema

Public Class BBXML_Validate

    Private aValidationError As New System.Collections.ArrayList
    Private bValidationWarningExists As Boolean
    Private bValidationErrorExists As Boolean
    Private sSchemaFile As String
    Private sTargetNameSpace As String
    Private sXMLFileToValidate As String

    Public Property XMLFileToValidate() As String
        Get
            XMLFileToValidate = sXMLFileToValidate
        End Get
        Set(ByVal Value As String)
            sXMLFileToValidate = Value
        End Set
    End Property
    Public Property SchemaFile() As String
        Get
            SchemaFile = sSchemaFile
        End Get
        Set(ByVal Value As String)
            sSchemaFile = Value
        End Set
    End Property
    Public Property TargetNameSpace() As String
        Get
            TargetNameSpace = sTargetNameSpace
        End Get
        Set(ByVal Value As String)
            sTargetNameSpace = Value
        End Set
    End Property
    Public ReadOnly Property ValidationWarningExists() As Boolean
        Get
            ValidationWarningExists = bValidationWarningExists
        End Get
    End Property
    Public ReadOnly Property ValidationErrorExists() As Boolean
        Get
            ValidationErrorExists = bValidationErrorExists
        End Get
    End Property
    Public ReadOnly Property ValidationErrorArray() As System.Collections.ArrayList
        Get
            ValidationErrorArray = aValidationError
        End Get
    End Property
    Public Sub Validate()

        Dim BB_XMLReaderSettings As XmlReaderSettings = New XmlReaderSettings()
        BB_XMLReaderSettings.Schemas.Add(sTargetNameSpace, sSchemaFile)
        BB_XMLReaderSettings.ValidationType = ValidationType.Schema
        AddHandler BB_XMLReaderSettings.ValidationEventHandler, New ValidationEventHandler(AddressOf BB_XMLReaderSettingsValidationEventHandler)

        Dim BBXMLDocument As XmlReader = XmlReader.Create(sXMLFileToValidate, BB_XMLReaderSettings)

        aValidationError.Clear()

        While BBXMLDocument.Read()

        End While

        BBXMLDocument.Close()
        BBXMLDocument = Nothing

    End Sub

    Private Sub BB_XMLReaderSettingsValidationEventHandler(ByVal sender As Object, ByVal e As ValidationEventArgs)

        If e.Severity = XmlSeverityType.Warning Then
            aValidationError.Add("Warning: " & vbCrLf & "LineNo: " & e.Exception.LineNumber.ToString & "      LinePos: " & e.Exception.LinePosition & vbCrLf & e.Message & vbCrLf)
            bValidationWarningExists = True

        ElseIf e.Severity = XmlSeverityType.Error Then
            aValidationError.Add("Error: " & vbCrLf & "LineNo: " & e.Exception.LineNumber.ToString & "      LinePos: " & e.Exception.LinePosition & vbCrLf & e.Message & vbCrLf)
            bValidationErrorExists = True
        End If

    End Sub
















    Public Function XMLValidate(ByVal Schema As String, ByVal sNameSpace As String, ByVal XMLDoc As String) As ArrayList

        Dim objWorkingXML As New System.Xml.XmlDocument
        Dim objValidateXML As System.Xml.XmlValidatingReader
        Dim objSchemasColl As New System.Xml.Schema.XmlSchemaCollection
        aValidationError.Clear()

        objSchemasColl.Add(sNameSpace, Schema)

        'This loads XML string 
        objValidateXML = New System.Xml.XmlValidatingReader(New System.Xml.XmlTextReader(XMLDoc))

        objValidateXML.Schemas.Add(objSchemasColl)

        'This is how you CATCH the errors (with a handler function)
        AddHandler objValidateXML.ValidationEventHandler, AddressOf ValidationCallBack

        'This is WHERE the validation occurs.. WHEN the XML Document READS throughthe validating reader
        Try
            objWorkingXML.Load(objValidateXML)

        Catch ex As Exception
            aValidationError.Add(ex.Message)
            If Not objValidateXML Is Nothing Then
                objValidateXML.Close()
                objValidateXML = Nothing
            End If
            If Not objWorkingXML Is Nothing Then
                objWorkingXML = Nothing
            End If
            If Not objSchemasColl Is Nothing Then
                objSchemasColl = Nothing
            End If
        Finally
            objValidateXML.Close()
            objValidateXML = Nothing
            objWorkingXML = Nothing
            objSchemasColl = Nothing

        End Try

        Return aValidationError

    End Function
    Private Sub ValidationCallBack(ByVal sender As Object, ByVal e As System.Xml.Schema.ValidationEventArgs)
        Select Case e.Severity
            Case System.Xml.Schema.XmlSeverityType.Error
                bValidationErrorExists = True
                aValidationError.Add("Error: " & vbCrLf & "LineNo: " & e.Exception.LineNumber.ToString & "      LinePos: " & e.Exception.LinePosition & vbCrLf & e.Message & vbCrLf)
            Case System.Xml.Schema.XmlSeverityType.Warning
                bValidationWarningExists = False
                aValidationError.Add("Warning: " & vbCrLf & "LineNo: " & e.Exception.LineNumber.ToString & "      LinePos: " & e.Exception.LinePosition & vbCrLf & e.Message & vbCrLf)
        End Select
    End Sub

    Public Sub New()
        bValidationWarningExists = False
        bValidationErrorExists = False
    End Sub
End Class
