﻿Option Strict Off
Option Explicit On
Public Class ErrorObjects
    Implements System.Collections.IEnumerable

    'local variable to hold collection
    Private mCol As Collection
    Public Function Add(Optional ByRef sKey As String = "") As vbBabel.ErrorObject
        'create a new object
        Dim objNewMember As vbBabel.ErrorObject
        objNewMember = New vbBabel.ErrorObject
        sKey = Str(mCol.Count() + 1)

        'set the properties passed into the method
        ''objNewMember.Key = Key
        ''Set objNewMember.Filenames = Filenames
        ''objNewMember.Key = Key
        ''Set objNewMember.Clients = Clients

        If Len(sKey) = 0 Then
            mCol.Add(objNewMember)
        Else
            mCol.Add(objNewMember, sKey)
            objNewMember.Index = CObj(sKey)
        End If


        'return the object created
        Add = objNewMember
        objNewMember = Nothing

    End Function
    Public Function VB_AddWithObject(ByRef oExistingErrorObject As vbBabel.ErrorObject) As vbBabel.ErrorObject
        Dim sKey As String
        'create a new object
        sKey = Str(mCol.Count() + 1)

        mCol.Add(oExistingErrorObject, sKey)
        oExistingErrorObject.Index = CObj(sKey)

        'return the object created
        VB_AddWithObject = oExistingErrorObject

    End Function
    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As vbBabel.ErrorObject
        Get
            'used when referencing an element in the collection
            'vntIndexKey contains either the Index or Key to the collection,
            'this is why it is declared as a Variant
            'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Long
        Get
            'used when retrieving the number of elements in the
            'collection. Syntax: Debug.Print x.Count
            Count = mCol.Count()
        End Get
    End Property
    Public Sub Remove(ByRef vntIndexKey As Object)
        'used when removing an element from the collection
        'vntIndexKey contains either the Index or Key, which is why
        'it is declared as a Variant
        'Syntax: x.Remove(xyz)

        mCol.Remove(vntIndexKey)
    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
    Private Sub Class_Initialize_Renamed()
        'creates the collection when this class is created
        mCol = New Collection
    End Sub
    Private Sub Class_Terminate_Renamed()
        'destroys collection when this class is terminated
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing
    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()
    End Sub
    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        GetEnumerator = mCol.GetEnumerator
    End Function

End Class
