Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("CEdi2Xml_NET.CEdi2Xml")> Public Class CEdi2Xml
	
	Private mbInitOk As Boolean
	Private rAppAdmImport As New CAppAdmImport
	Private mrXmlDoc As MSXML2.DOMDocument40
	Private sImportFilename As String
	Private iCharacterSet As FILEFORMAT
	'Private sImportFormat As String
    Private sMappingFile As String, sMappingFilevbBabel As String
	Private sTagSettingsFile As String
	Private sOutputPath As String
	Private sCodeValues As String
	'Private sExportFormat As String
	Private sCONTRLStatusText As String
	Private oImportObject As vbBabel.BabelFiles
	Private iFilesetupOut As Short 'In BabelExport its called iVB_Format_ID
	Private iFilenameInNo As Short
	Private sCountry As String
	Private bCreateCONTRLMessage As Boolean
	Private sFilenameCONTRL As String
    Private eBank As vbBabel.BabelFiles.Bank
    Private sVB_Version As String, sEDIVersion As String, sFormat As String
    Private bTransmittedFromFDByRJE As Boolean
    Private sSpecial As String
    Private sI_Account As String
    Private sClientNo As String
    Private lBabelFile_ID As Integer

    'Export
    Private rAppAdmExport As New CAppAdmExport
    Private sCompanyNo As String

    Public Enum FILEFORMAT
        FMT_ASCII = 0
        FMT_UNICODE = -1
        FMT_DEFAULT = -2
    End Enum

    '********* START PROPERTY SETTINGS ***********************
    Public ReadOnly Property CONTRLStatusText() As String
        Get
            CONTRLStatusText = sCONTRLStatusText
        End Get
    End Property
    Public Property ImportFilename() As String
        Get
            ImportFilename = sImportFilename
        End Get
        Set(ByVal Value As String)
            sImportFilename = Value
        End Set
    End Property
    Public Property CharacterSet() As FILEFORMAT
        Get
            CharacterSet = iCharacterSet
        End Get
        Set(ByVal Value As FILEFORMAT)
            iCharacterSet = Value
        End Set
    End Property
    Public Property MappingFile() As String
        Get
            MappingFile = sMappingFile
        End Get
        Set(ByVal Value As String)
            sMappingFile = Value
        End Set
    End Property
    Public Property TagSettingsFile() As String
        Get
            TagSettingsFile = sTagSettingsFile
        End Get
        Set(ByVal Value As String)
            sTagSettingsFile = Value
        End Set
    End Property
    Public Property OutputPath() As String
        Get
            OutputPath = sOutputPath
        End Get
        Set(ByVal Value As String)
            sOutputPath = Value
        End Set
    End Property
    Public Property CodeValues() As String
        Get
            CodeValues = sCodeValues
        End Get
        Set(ByVal Value As String)
            sCodeValues = Value
        End Set
    End Property
    Public WriteOnly Property FilesetupOut() As Short
        Set(ByVal Value As Short)
            iFilesetupOut = Value
        End Set
    End Property
    Public WriteOnly Property FilenameInNo() As Short
        Set(ByVal Value As Short)
            iFilenameInNo = Value
        End Set
    End Property
    '06.03.2008 - New, used to create a unique message-ID for the CONTRL
    Public WriteOnly Property BabelFile_ID() As Integer
        Set(ByVal Value As Integer)
            lBabelFile_ID = Value
        End Set
    End Property
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oImportObject = Value
        End Set
    End Property
    Public ReadOnly Property XMLDoc() As MSXML2.DOMDocument40
        Get
            XMLDoc = mrXmlDoc
        End Get
    End Property
    Public ReadOnly Property AppAdmImport() As CAppAdmImport
        Get
            AppAdmImport = rAppAdmImport
        End Get
    End Property
    Public WriteOnly Property CompanyNo() As String
        Set(ByVal Value As String)
            sCompanyNo = Value
        End Set
    End Property
    Public WriteOnly Property Country() As String
        Set(ByVal Value As String)
            sCountry = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public Property I_Account() As String
        Get
            I_Account = sI_Account
        End Get
        Set(ByVal Value As String)
            sI_Account = Value
        End Set
    End Property
    Public Property ClientNo() As String
        Get
            ClientNo = sClientNo
        End Get
        Set(ByVal Value As String)
            sClientNo = Value
        End Set
    End Property
    Public WriteOnly Property CreateCONTRLMessage() As Boolean
        Set(ByVal Value As Boolean)
            bCreateCONTRLMessage = Value
        End Set
    End Property
    Public WriteOnly Property FilenameCONTRL() As String
        Set(ByVal Value As String)
            sFilenameCONTRL = Value
        End Set
    End Property
    Public ReadOnly Property MappingFilevbBabel() As String
        Get
            MappingFilevbBabel = sMappingFilevbBabel
        End Get
    End Property
    Public Property BankByCode() As vbBabel.BabelFiles.Bank
        Get
            BankByCode = eBank
        End Get
        Set(ByVal Value As vbBabel.BabelFiles.Bank)
            eBank = Value
        End Set
    End Property
    Public WriteOnly Property EDIVersion() As String
        Set(ByVal Value As String)
            sEDIVersion = Value
        End Set
    End Property
    Public WriteOnly Property VB_Version() As String
        Set(ByVal Value As String)
            sVB_Version = Value
        End Set
    End Property
    'UPGRADE_NOTE: Format was upgraded to Format_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Public WriteOnly Property Format_Renamed() As String
        Set(ByVal Value As String)
            sFormat = Value
        End Set
    End Property
    '********* END PROPERTY SETTINGS ***********************

    'Public Function Convert(sEDIFile As String, iCharacterSet As FILEFORMAT, sMapping As String, sTagSettingsFile As String, sOutputPath As String, sValues As String) As Boolean
    Public Function Convert() As Boolean
        Dim bx As Boolean
        Dim lErrorNumber As Integer
        Dim sErrorSource As String
        Dim sErrorDescription As String

        Try

            'FIX Commented the following 2 lines.
            ' Have to do something with the parameters
            ' Check that they are all set
            'sMappingFile = FindMappingFileName()
            'InitImport 'sEDIFile, iCharacterSet, sMapping, sTagSettingsFile, sOutputPath, sValues
            If Not mbInitOk Then
                Err.Raise(vbObjectError + 15004, , LRS(15004))
            End If

            'if not rAppAdmImport.CreateProcessingInstruction "xml", " version='1.0' encoding='ISO-8859-1' " then
            If Not rAppAdmImport.CreateProcessingInstruction("xml", " version='1.0' encoding='ISO-8859-1' ") Then
                Err.Raise(vbObjectError + 15005, , LRS(15005))
                'Error during the initialization  of the temporary XML-object
            End If

            rAppAdmImport.TransmittedFromFDByRJE = bTransmittedFromFDByRJE
            rAppAdmImport.FormatIn = sFormat
            rAppAdmImport.BankByCode = eBank
            rAppAdmImport.ParseFile()
            If bCreateCONTRLMessage Then
                bx = CreateCONTRL(True)
            End If
            Convert = True

            mrXmlDoc = rAppAdmImport.XMLDoc
            rAppAdmImport = Nothing

            Exit Function

        Catch ex As Exception

            lErrorNumber = Err.Number
            sErrorSource = Err.Source
            sErrorDescription = Err.Description

            If bCreateCONTRLMessage Then
                bx = CreateCONTRL(False)
            End If
            Convert = False

            'If Today.ToString = "21.08.2018 00:00:00" Then

            If Not EmptyString(sImportFilename) Then
                If Not EmptyString(rAppAdmImport.LastSegment) Then
                    Throw New vbBabel.Payment.PaymentException(ex.Message, ex, Nothing, rAppAdmImport.LastSegment, sImportFilename) ' - Testing Try ... Catch
                Else
                    Throw New vbBabel.Payment.PaymentException(ex.Message, ex, Nothing, "", sImportFilename) ' - Testing Try ... Catch
                End If
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

            'Else

            'Throw
            'End If

        End Try


    End Function
    Public Function Export() As Boolean
        Dim bReturnValue As Boolean
        Dim i As Short

        On Error GoTo errorHandler

        Select Case UCase(sFormat)

            ' XNET 12.06.2012 - added DNB TBI Payments
            Case "DNB TBI PAYMENTS", "DNBNOR TBI PAYMENTS", "TBI_GENERAL", "TBI_FI", "TBI_ACH", "TBI_SALARY", "TBI_REFERENCE", "TBI_LOCAL", "TELEPAYPLUS", "PAIN.001 (ISO 20022)"
                bReturnValue = WriteTBIXMLFile(oImportObject, sOutputPath, sVB_Version, UCase(sFormat))

            Case Else
                'All kinds of EDI-formats

                i = 1
                InitExport()

                If Not mbInitOk Then
                    err.Raise(CInt("-1"), "EDI2XML_Export", "Initiation failed")
                End If

                bReturnValue = rAppAdmExport.CreateEDIFile(sCompanyNo, eBank, sFormat)


                'bReturnValue = rAppAdmExport.CreateEDIFile(sCompanyNo)

        End Select

        Export = bReturnValue
        Exit Function

errorHandler:
        '    #If CDEBUG Then
        '        MsgBox err.Description
        '    #End If
        err.Raise(Err.Number, , Err.Description)
        Export = False

    End Function

    'Friend Function Init(sEDIFile As String, iCharacterSet As FILEFORMAT, sMapping As String, sTagSettingsFile As String, sOutputPath As String, Optional sValues As String) As Boolean
    'FIXX Changed from Friend to public
    Public Function InitImport() As Boolean

        On Error GoTo errorHandler

        sMappingFile = FindMappingFileName(True)
        If sMappingFile = "EMPTY FILE" Then
            mbInitOk = True
            InitImport = False
            Exit Function
        Else

            If Not rAppAdmImport.Init(sImportFilename, iCharacterSet, sMappingFile, sTagSettingsFile, sFormat, sOutputPath, sCodeValues, sSpecial) Then
            End If
        End If


EndOK:

            mbInitOk = True

            InitImport = True
            Exit Function

errorHandler:
            Err.Raise(Err.Number, Err.Source, Err.Description)
            mbInitOk = False
            InitImport = False
    End Function
    Public Function InitExport() As Boolean
        On Error GoTo errorHandler

        sMappingFile = FindMappingFileName(False)
        If Not rAppAdmExport.Init(oImportObject, iFilesetupOut, iFilenameInNo, sOutputPath, sMappingFile, sTagSettingsFile, sFormat, sSpecial, sI_Account, sClientNo) Then
            GoTo errorHandler
        End If

EndOK:

        mbInitOk = True

        InitExport = True
        Exit Function

errorHandler:
        mbInitOk = False
        InitExport = False
    End Function
    Public Function CreateCONTRL(ByRef bTranslationOK As Boolean) As Boolean
        Dim oCONTRL As New CONTRL

        sCONTRLStatusText = oCONTRL.CreateCONTRLMessage(rAppAdmImport, sFilenameCONTRL, bTranslationOK, lBabelFile_ID)

        If Left(sCONTRLStatusText, 5) = "ERROR" Then
            CreateCONTRL = False
        Else
            CreateCONTRL = True
        End If

    End Function
    Private Function FindMappingFileName(ByRef bImport As Boolean) As String
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sFilename As String
        Dim l As Integer
        Dim s As String
        Dim sStartFileInfo As String
        Dim lStartPos, lNextSegmentPos As Integer
        'Dim sVersion As String
        Dim sGroupSep As String
        Dim sElementSep As String
        Dim bExit As Boolean
        Dim iNoOfTagGroups As Short
        Dim sChar As String
        Dim sSenderID, sMappingName As String
        Dim eBankInterpreted As vbBabel.BabelFiles.Bank

        oFs = New Scripting.FileSystemObject

        If bImport Then
            If Not oFs.FileExists(sImportFilename) Then
                Err.Raise(vbObjectError + 15001, , LRS(15001, sImportFilename))
                '"File" & sImportFilename & " doesn't exist"
            End If
            oFile = oFs.OpenTextFile(sImportFilename, Scripting.IOMode.ForReading, False)

            sStartFileInfo = ""
            l = 0
            Do While l < 300 '19.11.2018 - changed from 200 to 300, to detect foreign transactions from DnB fro System-Kredit. 
                s = oFile.Read(1)
                sStartFileInfo = sStartFileInfo & s
                l = l + 1
                If oFile.AtEndOfStream Then
                    Exit Do
                End If
            Loop

            'eBankInterpreted = FindBankSenderInEDIFile(sStartFileInfo, sImportFilename, bImport, sFormat, sEDIVersion, bTransmittedFromFDByRJE, sCountry)

            sStartFileInfo = Replace(sStartFileInfo, vbCr, "")
            sStartFileInfo = Replace(sStartFileInfo, vbLf, "")
            'XokNET - 08.07.2011 - Added next IF
            If InStr(1, sStartFileInfo, "INGEN RETUR FOR ANGITTE PARAMETERE", vbTextCompare) > 0 Then
                FindMappingFileName = "EMPTY FILE"
                Exit Function
            End If

            'Find the version
            lStartPos = InStr(1, sStartFileInfo, "UNH")
            If lStartPos = 0 Then '21.07.2014 - Added check if it is a valid EDI-format
                'The file %1 is not a valid EDI-file & vbcrlf & Start of file: & vbcrlf & %2
                Err.Raise(15191, "FindMappingFilename", LRS(15191, sImportFilename, Left(sStartFileInfo, 40)))
            Else
                'The file %1 is not a valid EDI-file & vbcrlf & Start of file: & vbcrlf & %2
                lStartPos = InStr(lStartPos, sStartFileInfo, sFormat)
            End If
            If lStartPos = 0 Then '21.07.2014 - Added check if it is a valid EDI-format
                Err.Raise(15191, "FindMappingFilename", LRS(15191, sImportFilename, Left(sStartFileInfo, 40)))
            Else
                sEDIVersion = Mid(sStartFileInfo, lStartPos + Len(sFormat) + 1, 1)
            End If
            sEDIVersion = Trim(sEDIVersion) & Trim(Mid(sStartFileInfo, lStartPos + Len(sFormat) + 3, 3))

            'New 28.04.2008 to cover for f.x. CONTRL DnBNORINPS where the version is 3, not 96A as is usual
            'UNH+7999+CONTRL:D:3:UN'
            If Mid(sEDIVersion, Len(sEDIVersion) - 1, 1) = ":" Then
                sEDIVersion = Left(sEDIVersion, Len(sEDIVersion) - 2)
            ElseIf Right(sEDIVersion, 1) = ":" Then
                sEDIVersion = Left(sEDIVersion, Len(sEDIVersion) - 1)
            End If
            If Left(sEDIVersion, 1) <> "D" Then
                sEDIVersion = ""
            End If
            lNextSegmentPos = lStartPos

            'Find the senders ID
            If Left(sStartFileInfo, 3) = "UNA" Then
                sElementSep = Mid(sStartFileInfo, 4, 1)
                sGroupSep = Mid(sStartFileInfo, 5, 1)
            Else
                sElementSep = ":"
                sGroupSep = "+"
            End If
            lStartPos = InStr(1, sStartFileInfo, "UNB")

            l = 1
            iNoOfTagGroups = 0
            bExit = False
            Do While Not bExit
                If Mid(sStartFileInfo, lStartPos + l, 1) = sGroupSep Then
                    iNoOfTagGroups = iNoOfTagGroups + 1
                    If iNoOfTagGroups = 2 Then
                        bExit = True
                    Else
                        l = l + 1
                    End If
                Else
                    l = l + 1
                End If
            Loop

            lStartPos = lStartPos + l

            l = 1
            bExit = False
            Do While Not bExit
                sChar = Mid(sStartFileInfo, lStartPos + l, 1)
                If sChar <> sElementSep And sChar <> sGroupSep Then
                    sSenderID = sSenderID & sChar
                    l = l + 1
                Else
                    bExit = True
                End If
            Loop

            Select Case sSenderID
                Case "00810506482"
                    lNextSegmentPos = InStr(lNextSegmentPos, sStartFileInfo, "BGM")
                    If Mid(sStartFileInfo, lNextSegmentPos, 3) = "BGM" Then
                        Select Case Mid(sStartFileInfo, lNextSegmentPos + 4, 3)
                            Case "435" 'Preadvice of a credit
                                sMappingName = "BBS"
                                eBankInterpreted = vbBabel.BabelFiles.Bank.BBS
                            Case "455" ' Extended credit advice
                                lNextSegmentPos = InStr(1, sStartFileInfo, "UNOC")
                                ' 22.03.06 Removed next if (KI+JanP)
                                ''''If Mid$(sStartFileInfo, lNextSegmentPos + 5, 1) = "1" Then
                                '    sMappingName = "DNB"
                                '    sCountry = "SE"
                                '    eBankInterpreted = DnBNOR_SE
                                'Else
                                ' Added 04.12.2007 because LHL wants other bankreferences.
                                ' We have changed the mappingfile, so they get RFF+ACD instead of RFF+AEK in oPayment.REF_Bank2,
                                ' which is copied to oBatch.REF_Bank when RFF+AII in LIN is empty
                                If sSpecial = "LHL" Then
                                    sMappingName = "DNB_LHL"
                                Else
                                    ' as pre 04.12.2007, for all other than LHL
                                    sMappingName = "DNB"
                                End If
                                eBankInterpreted = vbBabel.BabelFiles.Bank.DnB
                                'End If
                            Case Else
                                eBankInterpreted = vbBabel.BabelFiles.Bank.No_Bank_Specified
                                Err.Raise(vbObjectError + 15002, , LRS(15002, sSenderID))
                                '"Unknown sender of file"
                        End Select
                    Else
                        eBankInterpreted = vbBabel.BabelFiles.Bank.No_Bank_Specified
                        Err.Raise(vbObjectError + 15002, , LRS(15002, sSenderID))
                        '"Unknown sender of file"
                    End If

                Case "012"
                    sMappingName = "FD"
                    eBankInterpreted = vbBabel.BabelFiles.Bank.Fellesdata
                    If Len(sStartFileInfo) > 80 Then
                        'If InStr(12, sStartFileInfo, "'") = Len(Trim$(Left$(sStartFileInfo, 80))) Then
                        If InStr(12, sStartFileInfo, "'") = Len(Trim(Left(sStartFileInfo, 80))) Then
                            'OK, The file is trasmitted by RJE
                            bTransmittedFromFDByRJE = True
                        End If
                    End If
                Case "00008080"
                    If sStartFileInfo.Contains("'BUS++IN") Then
                        '19.11.2018 Added this if to be able to distinguish between files sent from DnB to Nets and files created by Nets
                        '  The assumption is that the file from DnB should contain an international payment, if it is a DnB internal payment we will still use the Nets mapping
                        '  System-kreditt got a CUX-error when importing an international payment.
                        sMappingName = "DNB"
                        eBankInterpreted = vbBabel.BabelFiles.Bank.DnB
                    Else
                        sMappingName = "BBS"
                        eBankInterpreted = vbBabel.BabelFiles.Bank.BBS
                    End If
                Case "NDEANOKKXXX"
                    sMappingName = "Nord"
                    eBankInterpreted = vbBabel.BabelFiles.Bank.Nordea_NO
                Case "5790000243440", "5790000260454"
                    '03.05.2021 - Added 5790000260454 for Wilhelmsen CREMUL
                    'XNET - 06.07.2011 - Added next IF
                    If InStr(sStartFileInfo, "BGM+X1") > 0 Then
                        sMappingName = "DBDK" 'Danske Bank - dansk format
                        eBankInterpreted = vbBabel.BabelFiles.Bank.Danske_Bank_dk
                    Else
                        sMappingName = "DBNO" 'Danske Bank - norske format
                        eBankInterpreted = vbBabel.BabelFiles.Bank.Danske_Bank_NO
                    End If
                    'XNET - 27.12.2011 - Added next case
                Case "5020077862"
                    sMappingName = "SHB"
                    eBankInterpreted = vbBabel.BabelFiles.Bank.Svenska_Handelsbanken
                    sCountry = "SE"
                Case "XIANNOKKXXX"
                    sMappingName = "BBS"
                    eBankInterpreted = vbBabel.BabelFiles.Bank.BBS
                Case "5020329081"
                    sMappingName = "SEB"
                    eBankInterpreted = vbBabel.BabelFiles.Bank.SEB_NO
                Case "NORDEAPROD" 'Used by Nordea Finland (and maybe other countries)
                    sMappingName = "Nord"
                    sCountry = "SF"
                    eBankInterpreted = vbBabel.BabelFiles.Bank.Nordea_eGateway
                    'Customer-specific ID's
                Case "1438086569"
                    sEDIVersion = "D96A"
                    sMappingName = "VISMAFOKUS"
                    sCountry = "NO"
                    eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified
                Case "107388520701"
                    sFormat = "PAYMUL"
                    sEDIVersion = "FPOAAB2912"
                    sCountry = "DE"
                    sMappingName = "DNV"
                    eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified
                Case "Ignis Photonyx A/S"
                    sFormat = "PAYMUL"
                    sEDIVersion = "D96A"
                    sMappingName = "IGNIS"
                    sCountry = "DK"
                    eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified
                Case "4005922028009"
                    sFormat = "PAYMUL"
                    sEDIVersion = "D96A"
                    sMappingName = "Actebis"
                    sCountry = "DK"
                    eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified
                Case "5790000706815", "KILROY TRAVELS INTERNATIONAL"
                    sFormat = "PAYMUL"
                    sEDIVersion = "D96A"
                    sMappingName = "Berlingske"
                    sCountry = "DK"
                    eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified
                Case "KRAFT FOODS"
                    sFormat = "PAYMUL"
                    sEDIVersion = "D96A"
                    sMappingName = "KRAFT"
                    sCountry = "DK"
                    eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified

                Case "DNBANOKK" ' Added 04.01.07, possible DnBNOR INPS
                    'Changed 25.04.2008 from
                    'If InStr(sStartFileInfo, "CREMUL:D:96A:UN:INPS05") > 0 Then
                    'To
                    If InStr(sStartFileInfo, ":D:96A:UN:INPS05") > 0 Then
                        sMappingName = "DNBNORINPS"
                        eBankInterpreted = vbBabel.BabelFiles.Bank.DnBNOR_INPS
                    ElseIf sFormat = "CONTRL" Then
                        sMappingName = "DNBNORINPS"
                        eBankInterpreted = vbBabel.BabelFiles.Bank.DnBNOR_INPS
                    End If

                Case "FDCTEST"
                    sMappingName = "NordeaGateway"
                    sCountry = "SE"
                    eBankInterpreted = vbBabel.BabelFiles.Bank.Nordea_eGateway

                Case "5164070384", "DKFDCGSS", "GFTEST" 'Gjensidige Sweden, PAYMUL from F2100
                    sFormat = "PAYMUL"
                    sEDIVersion = "D96A"
                    sMappingName = "F2100_Gjensidige"
                    sCountry = "SE"
                    eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified

                Case "8480000019905"
                    sMappingName = "Mercadona"
                    sCountry = "I"
                    eBank = vbBabel.BabelFiles.Bank.No_Bank_Specified

                Case Else

                    eBankInterpreted = vbBabel.BabelFiles.Bank.No_Bank_Specified
                    sMappingName = sSenderID
                    'err.Raise vbObjectError + 15002, , Replace(LRS(15002), "%1", sSenderID)
                    '"Unknown sender of file"
            End Select

            'Added 05.01.2006 by Kjell to be able to used the bank from the profile
            If eBank > vbBabel.BabelFiles.Bank.No_Bank_Specified Then
                If eBank <> eBankInterpreted Then
                    ' 22.03.06: Changes made
                    If (eBank = vbBabel.BabelFiles.Bank.DnB And eBankInterpreted = vbBabel.BabelFiles.Bank.BBS) Then
                        'Special situation because we aren't able to distinguish between DnBNOR Internal/BBS
                        '  Keep the eBank from the profile
                        eBank = vbBabel.BabelFiles.Bank.BBS

                        ' 20.01.2015 Added new test for those with mix of DNB NO Cremul and DNB INPS Cremul
                    ElseIf (eBank = vbBabel.BabelFiles.Bank.DnB And eBankInterpreted = vbBabel.BabelFiles.Bank.DnBNOR_INPS) Then
                        'Special situation because we aren't able to distinguish between DnBNOR Internal/BBS
                        '  Keep the eBank from the profile
                        eBank = vbBabel.BabelFiles.Bank.DnBNOR_INPS

                        'BGMAX - Remove next ElseIF - 6 lines - comment is wrong
                    ElseIf (eBank = vbBabel.BabelFiles.Bank.DnBNOR_SE And eBankInterpreted = vbBabel.BabelFiles.Bank.DnB) Then
                        'Special situation because we aren't able to distinguish between DnBNOR Internal/BBS
                        '  Keep the eBank from the profile
                        sMappingName = "DNB"
                        sCountry = "NO"
                        eBankInterpreted = vbBabel.BabelFiles.Bank.DnBNOR_SE
                        'BGMAX - Uncomment next ElseIF - 6 lines
                        '            ElseIf (eBank = DnB And eBankInterpreted = DnBNOR_SE) Then
                        '                'Special situation if the user has choosen DnBNOR as bank when the
                        '                ' file origins from Sweden
                        '                sMappingName = "DNB"
                        '                sCountry = "SE"
                        '                eBankInterpreted = DnBNOR_SE
                    Else
                        'Special case, does not seperate between DnBNOR Internal and DnBNOR BBS
                        Err.Raise(vbObjectError + 15091, "FindMappingFileName", LRS(15091, GetEnumBank(eBank), GetEnumBank(eBankInterpreted), sImportFilename))
                    End If


                    ' 22.03.06 - old code
                    'Special case, does not seperate between DnBNOR Internal and DnBNOR BBS
                    'If Not (eBank = DnB And eBankInterpreted = BBS) Then
                    '    err.Raise vbObjectError + 15091, "FindMappingFileName", Replace(LRS(15091, GetEnumBank(eBank), GetEnumBank(eBankInterpreted)), "%3", sImportFilename)
                    'Else
                    '    'Special situation because we aren't able to distinguish between DnBNOR Internal/BBS
                    '    '  Keep the eBank from the profile
                    '    eBank = BBS
                    'End If
                Else
                    'OK
                End If
            Else
                eBank = eBankInterpreted
            End If
        Else
            Select Case eBank
                Case vbBabel.BabelFiles.Bank.No_Bank_Specified
                    sMappingName = "BBS"
                Case vbBabel.BabelFiles.Bank.A_Norwegian_bank
                    sMappingName = "BBS"
                Case vbBabel.BabelFiles.Bank.DnB
                    sMappingName = "DNB"
                Case vbBabel.BabelFiles.Bank.DnBNOR_SE
                    sMappingName = "DNB"
                    sCountry = "SE"
                Case vbBabel.BabelFiles.Bank.DnBNOR_INPS
                    sMappingName = "DNBNORINPS"
                    sCountry = "NO"
                Case vbBabel.BabelFiles.Bank.Nordea_NO
                    sMappingName = "CBK"
                Case vbBabel.BabelFiles.Bank.Gjensidige_NOR
                    sMappingName = "NOR" 'Danske Bank - norske format
                Case vbBabel.BabelFiles.Bank.BBS
                    sMappingName = "BBS"
                Case vbBabel.BabelFiles.Bank.Danske_Bank_NO
                    sMappingName = "DBNO"
                Case vbBabel.BabelFiles.Bank.Nordea_eGateway
                    sMappingName = "NordeaGateway"
                    'XNET - 22.11.2011 - Added next two cases
                Case vbBabel.BabelFiles.Bank.HSBC_UK
                    sMappingName = "HSBC"
                Case vbBabel.BabelFiles.Bank.SEB_NO
                    sMappingName = "SEBNO"
                Case vbBabel.BabelFiles.Bank.SEB_SE
                    sMappingName = "SEBSE"
                Case Else
                    Err.Raise(vbObjectError + 15002, , LRS(15002, sSenderID))
                    '"Unknown sender of file"
            End Select

        End If

        'New 26.03.2007/14.05.2007
        '29.06.2012 - Added CREMUL for Gjensidige Sweden
        If Not ((sFormat = "PAYMUL" Or sFormat = "BANSTA" Or sFormat = "CONTRL" Or sFormat = "DEBMUL" Or sFormat = "CREMUL" Or sFormat = "COACSU") And bImport = False) Then 'No need for mappingfiles when we write a EDI-file
            If RunTime() Then  'DISKUTERNOTRUNTIME Felles mappe?
                sFilename = My.Application.Info.DirectoryPath & "\" & sFormat & sEDIVersion & sCountry & sMappingName & ".XML"
            Else
                ' VB debug
                If sSpecial = "NRX_CAMT054" Then 'May be removed after 31.07.2018
                    sFilename = "C:\Slett\NRX\Camt.054\" & sFormat & sEDIVersion & sCountry & sMappingName & ".XML"
                Else
                    sFilename = "c:\projects\BabelBank\Edi2Xml\" & sFormat & sEDIVersion & sCountry & sMappingName & ".XML"
                End If
            End If
            'oExportEDIObject.MappingFile = App.path & "\" & GetEnumFileType(iExportFormat) & sEDIVersion & "NOBBS.XML"
            If Not oFs.FileExists(sFilename) Then
                Err.Raise(vbObjectError + 15003, "FindMappingFileName", LRS(15003, sFilename))
                ' "File" & sImportFilename & " doesn't exist"
            End If


            If RunTime() Then  'DISKUTERNOTRUNTIME Felles mappe?
                sMappingFilevbBabel = My.Application.Info.DirectoryPath & "\" & sFormat & sEDIVersion & sCountry & sMappingName & "map.xml"
            Else
                If sSpecial = "NRX_CAMT054" Then 'May be removed after 31.07.2018
                    sMappingFilevbBabel = "C:\Slett\NRX\Camt.054\" & sFormat & sEDIVersion & sCountry & sMappingName & "map.xml"
                Else
                    sMappingFilevbBabel = "c:\projects\BabelBank\Edi2Xml\" & sFormat & sEDIVersion & sCountry & sMappingName & "map.xml"
                End If
            End If


            If Not oFs.FileExists(sMappingFilevbBabel) Then
                Err.Raise(vbObjectError + 15003, "FindMappingFileName", LRS(15003, sMappingFilevbBabel))
                ' "File" & sImportFilename & " doesn't exist"
            End If
        End If

        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        FindMappingFileName = sFilename

    End Function

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Public Sub Class_Initialize_Renamed()

        mbInitOk = False
        sImportFilename = ""
        iCharacterSet = FILEFORMAT.FMT_DEFAULT
        sMappingFile = ""
        sOutputPath = ""
        If RunTime() Then   ''DISKUTERNOTRUNTIME FElles mappe?
            sTagSettingsFile = My.Application.Info.DirectoryPath & "\tagsettings.xml"
            sCodeValues = My.Application.Info.DirectoryPath & "\values.xml"
        Else
            sTagSettingsFile = "C:\Projects\BabelBank\Edi2XML\tagsettings.xml"
            sCodeValues = "C:\Projects\BabelBank\Edi2XML\values.xml"
        End If
        sFormat = ""
        bCreateCONTRLMessage = False
        sFilenameCONTRL = ""
        bTransmittedFromFDByRJE = False
        ' When edi2xml.dll is called, the code attempts
        ' to find a local satellite DLL that will contain all the
        ' resources.
        sSpecial = ""
        sI_Account = ""
        sClientNo = ""
        iFilenameInNo = -1
        'If Not (LoadLocalizedResources("vbbabel")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL for vbbabel", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
        'End If

    End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
End Class
