Option Strict Off
Option Explicit On
Module WriteDatabase
	
	Function WriteDatabase_Innbetaling(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByRef eTypeofRecords As BabelFiles.TypeofRecords) As Boolean
		Dim i, j As Double
		Dim oBabel As BabelFile
		Dim oBatch As Batch
		Dim oPayment As Payment
		Dim oInvoice As Invoice
		Dim oFreeText As Freetext
		Dim iFreetextCounter As Short
		Dim sFreetextFixed, sFreeTextVariable As String
		Dim oFilesetup As FileSetup
		Dim oClient As Client
		Dim oaccount As Account
		Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
		Dim bFirstBatch As Boolean
		Dim nSequenceNoTotal As Double
		Dim bGLRecordWritten As Boolean
		Dim sVoucherNo As String
		Dim sAlphaPart As String
		Dim nDigitsLen As Short
		Dim sGLAccount As String
		Dim sOldAccountNo As String
		Dim sAccountToUseInUpdayTotals As String
		Dim aAccountsToUseInUpdayTotals() As String
		Dim sOldDate As String
		Dim bAccountFound As Boolean
		Dim lInvoiceCounter As Integer
		Dim lCounter, lArrayCounter As Integer
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim lCompanyNo As Integer
		Dim sProvider As String
		Dim sUID, sPWD As String
		Dim sMySQL As String
        Dim lRecordsAffected As Integer
		Dim bUpdateDone As Boolean
		
        Dim aQueryArray(,,) As String
		''''REMOVE NEXT LINE?
		'''Dim aClientArray() As String
		Dim aAmountArray() As Double

		Dim nTotalDebit, nTotalCredit As Double
		Dim nTotalRecords As Integer
		Dim nTotalBunt As Double
		Dim sInitiateSelect As String
		Dim sInitiateUpdate As String
		Dim sFileSelect As String
		Dim sFileUpdate As String
		Dim sBatchSelect As String
		Dim sBatchUpdate As String
		Dim sPaymentSelect As String
		Dim sPaymentUpdate As String
		Dim sTerminateSelect As String
		Dim sTerminateUpdate As String
		Dim sInitiateSelectOriginal As String
		Dim sInitiateUpdateOriginal As String
		Dim sFileSelectOriginal As String
		Dim sFileUpdateOriginal As String
		Dim sBatchSelectOriginal As String
		Dim sBatchUpdateOriginal As String
		Dim sPaymentSelectOriginal As String
		Dim sPaymentUpdateOriginal As String
		Dim sTerminateSelectOriginal As String
		Dim sTerminateUpdateOriginal As String
		
        Dim sInintiateParameter1 As String = ""
        Dim sInintiateParameter2 As String = ""
        Dim sFileParameter1 As String = ""
        Dim sFileParameter2 As String = ""
        Dim sBatchParameter1 As String = ""
        Dim sBatchParameter2 As String = ""
        Dim sPaymentParameter1 As String = ""
        Dim sPaymentParameter2 As String = ""
        Dim sTerminateParameter1 As String = ""
        Dim sTerminateParameter2 As String = ""

        Try

            lCompanyNo = 1 'TODO: - Hardcoded CompanyNo
            lInvoiceCounter = 0
            'Build an array consisting of the ConnectionStrings and Queries for aftermatcinh agianst the ERP-system.
            aQueryArray = GetERPDBInfo(lCompanyNo, 3)
            'sProvider = aQueryArray(0, 0, 0) ' Holds connectionstring
            'sUID = aQueryArray(0, 1, 0)
            'sPWD = aQueryArray(0, 2, 0)
            '' Change UID/PWD:
            '' Fill in UserID and Password
            'sProvider = Replace(sProvider, "=UID", "=" & sUID)
            'sProvider = Replace(sProvider, "=PWD", "=" & sPWD)
            ' Logon
            oMyDal.TypeOfDB = DAL.DatabaseType.ERPDB
            oMyDal.Company_ID = lCompanyNo
            oMyDal.DBProfile_ID = 1 'TODO - Hardcoded DBProfile_ID
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            nTotalBunt = 0

            bUpdateDone = False
            sInitiateSelect = ""
            sInitiateUpdate = ""
            sFileSelect = ""
            sFileUpdate = ""
            sBatchSelect = ""
            sBatchUpdate = ""
            sPaymentSelect = ""
            sPaymentUpdate = ""
            sTerminateSelect = ""
            sTerminateUpdate = ""

            'Store SQL's
            For lCounter = 0 To UBound(aQueryArray, 3)

                Select Case UCase(aQueryArray(0, 4, lCounter))

                    'Name of the SQL's set up in Matchingrules - After matching
                    Case "SELECT_INITIATE"
                        'Parameters: BBRET_I_PARAMETER? - ? is number
                        'Run before traversing through the collections, and before any update happens
                        sInitiateSelectOriginal = UCase(aQueryArray(0, 0, lCounter))

                    Case "UPDATE_INITIATE"
                        'Run before traversing through the collections
                        sInitiateUpdateOriginal = UCase(aQueryArray(0, 0, lCounter))

                    Case "SELECT_FILE"
                        'Parameters: BBRET_F_PARAMETER? - ? is number
                        'Run after each new BabelFile
                        sFileSelectOriginal = UCase(aQueryArray(0, 0, lCounter))

                    Case "UPDATE_FILE"
                        'Run after each new BabelFile
                        sFileUpdateOriginal = UCase(aQueryArray(0, 0, lCounter))

                    Case "SELECT_BATCH"
                        'Parameters: BBRET_B_PARAMETER? - ? is number
                        'Run after each new Batch
                        sBatchSelectOriginal = UCase(aQueryArray(0, 0, lCounter))

                    Case "UPDATE_BATCH"
                        'Run after each new Batch
                        sBatchUpdateOriginal = UCase(aQueryArray(0, 0, lCounter))

                    Case "SELECT_PAYMENT"
                        'Parameters: BBRET_P_PARAMETER? - ? is number
                        'Run after each new Payment
                        sPaymentSelectOriginal = UCase(aQueryArray(0, 0, lCounter))

                    Case "UPDATE_PAYMENT"
                        'Run after each new Payment
                        sPaymentUpdateOriginal = UCase(aQueryArray(0, 0, lCounter))

                    Case "SELECT_TERMINATE"
                        'Parameters: BBRET_T_PARAMETER? - ? is number
                        'Run at the end
                        sTerminateSelectOriginal = UCase(aQueryArray(0, 0, lCounter))

                    Case "UPDATE_TERMINATE"
                        'Run at the end
                        sTerminateUpdateOriginal = UCase(aQueryArray(0, 0, lCounter))

                End Select

            Next lCounter

            'INITIATE
            If Not EmptyString(sInitiateSelectOriginal) Then
                sInitiateSelect = sInitiateSelectOriginal

                oMyDal.SQL = sInitiateSelect
                If oMyDal.Reader_Execute() Then
                    Do While oMyDal.Reader_ReadRecord
                        If InStr(1, sInitiateSelect, "BBRET_I_PARAMETER1", CompareMethod.Text) > 0 Then
                            sInintiateParameter1 = oMyDal.Reader_GetString("BBRET_I_PARAMETER1")
                        Else
                            sInintiateParameter1 = ""
                        End If
                        If InStr(1, sInitiateSelect, "BBRET_I_PARAMETER2", CompareMethod.Text) > 0 Then
                            sInintiateParameter2 = oMyDal.Reader_GetString("BBRET_I_PARAMETER2")
                        Else
                            sInintiateParameter2 = ""
                        End If
                        Exit Do
                    Loop
                Else
                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                End If

            End If
            If Not EmptyString(sInitiateUpdateOriginal) Then
                sInitiateUpdate = sInitiateUpdateOriginal
                If Not EmptyString(sInitiateSelect) Then
                    If sInintiateParameter1 <> "" Then
                        sInitiateUpdate = Replace(sInitiateUpdate, "BB_I_Parameter1", sInintiateParameter1, , , CompareMethod.Text)
                    End If
                    If sInintiateParameter2 <> "" Then
                        sInitiateUpdate = Replace(sInitiateUpdate, "BB_I_Parameter2", sInintiateParameter2, , , CompareMethod.Text)
                    End If

                End If
                'Now()
                sInitiateUpdate = Replace(sInitiateUpdate, "BB_SysDate", VB6.Format(Now, "YYYYMMDD"), , , CompareMethod.Text)
                sInitiateUpdate = Replace(sInitiateUpdate, "BB_SysTime", VB6.Format(Now, "hhmmss"), , , CompareMethod.Text)

                oMyDal.SQL = sInitiateUpdate

                If oMyDal.ExecuteNonQuery Then
                    If Not oMyDal.RecordsAffected > 0 Then
                        Err.Raise(34222, "WriteDatabase_Innbetaling", "En uventet feil oppstod ved skriving av initialiseringsrecord." & vbCrLf & "SQL: " & sInitiateUpdate & vbCrLf & vbCrLf & "BabelBank avbrytes!")
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

            End If

            bFirstBatch = True
            sOldAccountNo = ""
            sOldDate = "18990000"

            For Each oBabel In oBabelFiles

                bExportoBabel = False

                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If Not bMultiFiles Then
                            bExportoBabel = True
                        Else
                            'If oPayment.I_Account = sI_Account Then
                            ' 01.10.2018 � changed above If to:
                            If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                If Len(oPayment.VB_ClientNo) > 0 Then
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        bExportoBabel = True
                                    End If
                                Else
                                    bExportoBabel = True
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            If eTypeofRecords = BabelFiles.TypeofRecords.OCRonly Then
                                If Not IsOCR((oPayment.PayCode)) Then
                                    bExportoBabel = False
                                End If
                            Else
                                If IsOCR((oPayment.PayCode)) Then
                                    bExportoBabel = False
                                Else
                                    bExportoBabel = False
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                            bExportoBabel = True
                                            Exit For
                                        End If
                                    Next oInvoice
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    If Not EmptyString(sFileSelectOriginal) Then
                        sFileParameter1 = ""
                        sFileParameter2 = ""
                        sFileSelect = sFileSelectOriginal

                        oMyDal.SQL = sFileSelect
                        If oMyDal.Reader_Execute() Then
                            Do While oMyDal.Reader_ReadRecord
                                If InStr(1, sFileSelect, "BBRET_F_PARAMETER1", CompareMethod.Text) > 0 Then
                                    sFileParameter1 = oMyDal.Reader_GetString("BBRET_F_PARAMETER1")
                                Else
                                    sFileParameter1 = ""
                                End If
                                If InStr(1, sFileSelect, "BBRET_F_PARAMETER2", CompareMethod.Text) > 0 Then
                                    sFileParameter2 = oMyDal.Reader_GetString("BBRET_F_PARAMETER2")
                                Else
                                    sFileParameter2 = ""
                                End If
                                Exit Do
                            Loop
                        Else
                            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                        End If

                    End If
                    If Not EmptyString(sFileUpdateOriginal) Then
                        sFileUpdate = sFileUpdateOriginal
                        If Not EmptyString(sInitiateSelect) Then
                            If sInintiateParameter1 <> "" Then
                                sFileUpdate = Replace(sFileUpdate, "BB_I_Parameter1", sInintiateParameter1, , , CompareMethod.Text)
                            End If
                            If sInintiateParameter2 <> "" Then
                                sFileUpdate = Replace(sFileUpdate, "BB_I_Parameter2", sInintiateParameter2, , , CompareMethod.Text)
                            End If
                        End If
                        If Not EmptyString(sFileSelect) Then
                            If sFileParameter1 <> "" Then
                                sFileUpdate = Replace(sFileUpdate, "BB_F_Parameter1", sFileParameter1, , , CompareMethod.Text)
                            End If
                            If sFileParameter2 <> "" Then
                                sFileUpdate = Replace(sFileUpdate, "BB_F_Parameter2", sFileParameter2, , , CompareMethod.Text)
                            End If
                        End If

                        oMyDal.SQL = sFileUpdate

                        If oMyDal.ExecuteNonQuery Then
                            If Not oMyDal.RecordsAffected > 0 Then
                                Err.Raise(34222, "WriteDatabase_Innbetaling", "En uventet feil oppstod ved skriving av filoppdatering." & vbCrLf & "SQL: " & sFileUpdate & vbCrLf & vbCrLf & "BabelBank avbrytes!")
                            End If
                        Else
                            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                        End If

                    End If

                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                            If Not bMultiFiles Then
                                bExportoBatch = True
                            Else
                                'If oPayment.I_Account = sI_Account Then
                                ' 01.10.2018 � changed above If to:
                                If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBatch = True
                                        End If
                                    Else
                                        bExportoBatch = True
                                    End If
                                End If
                            End If

                            If bExportoBatch Then

                                If eTypeofRecords = BabelFiles.TypeofRecords.OCRonly Then
                                    If Not IsOCR((oPayment.PayCode)) Then
                                        bExportoBatch = False
                                    End If
                                Else
                                    If IsOCR((oPayment.PayCode)) Then
                                        bExportoBatch = False
                                    Else
                                        bExportoBatch = False
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                bExportoBatch = True
                                                Exit For
                                            End If
                                        Next oInvoice
                                    End If
                                End If
                            End If

                            If bExportoBatch Then
                                Exit For
                            End If

                        Next oPayment


                        If bExportoBatch Then

                            If Not EmptyString(sBatchSelectOriginal) Then
                                sBatchParameter1 = ""
                                sBatchParameter2 = ""
                                sBatchSelect = sBatchSelectOriginal

                                oMyDal.SQL = sBatchSelect
                                If oMyDal.Reader_Execute() Then
                                    Do While oMyDal.Reader_ReadRecord
                                        If InStr(1, sBatchSelect, "BBRET_B_PARAMETER1", CompareMethod.Text) > 0 Then
                                            sBatchParameter1 = oMyDal.Reader_GetString("BBRET_B_PARAMETER1")
                                        Else
                                            sBatchParameter1 = ""
                                        End If
                                        If InStr(1, sBatchSelect, "BBRET_B_PARAMETER2", CompareMethod.Text) > 0 Then
                                            sBatchParameter2 = oMyDal.Reader_GetString("BBRET_B_PARAMETER2")
                                        Else
                                            sBatchParameter2 = ""
                                        End If
                                        Exit Do
                                    Loop
                                Else
                                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                                End If
                            End If
                            If Not EmptyString(sBatchUpdateOriginal) Then
                                sBatchUpdate = sBatchUpdateOriginal

                                If Not EmptyString(sInitiateSelect) Then
                                    If sInintiateParameter1 <> "" Then
                                        sBatchUpdate = Replace(sBatchUpdate, "BB_I_Parameter1", sInintiateParameter1, , , CompareMethod.Text)
                                    End If
                                    If sInintiateParameter2 <> "" Then
                                        sBatchUpdate = Replace(sBatchUpdate, "BB_I_Parameter2", sInintiateParameter2, , , CompareMethod.Text)
                                    End If
                                End If
                                If Not EmptyString(sFileSelect) Then
                                    If sFileParameter1 <> "" Then
                                        sBatchUpdate = Replace(sBatchUpdate, "BB_F_Parameter1", sFileParameter1, , , CompareMethod.Text)
                                    End If
                                    If sFileParameter2 <> "" Then
                                        sBatchUpdate = Replace(sBatchUpdate, "BB_F_Parameter2", sFileParameter2, , , CompareMethod.Text)
                                    End If
                                End If
                                If Not EmptyString(sBatchSelect) Then
                                    If sBatchParameter1 <> "" Then
                                        sBatchUpdate = Replace(sBatchUpdate, "BB_B_Parameter1", sBatchParameter1, , , CompareMethod.Text)
                                    End If
                                    If sBatchParameter2 <> "" Then
                                        sBatchUpdate = Replace(sBatchUpdate, "BB_B_Parameter2", sBatchParameter2, , , CompareMethod.Text)
                                    End If
                                End If

                                oMyDal.SQL = sBatchUpdate

                                If oMyDal.ExecuteNonQuery Then
                                    If Not oMyDal.RecordsAffected > 0 Then
                                        Err.Raise(34222, "WriteDatabase_Innbetaling", "En uventet feil oppstod ved skriving av bunt." & vbCrLf & "SQL: " & sBatchUpdate & vbCrLf & vbCrLf & "BabelBank avbrytes!")
                                    End If
                                Else
                                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                End If

                            End If

                            bGLRecordWritten = False
                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False

                                'If (eTypeofRecords = TypeofRecords.AllRecords And oPayment.MATCH_Matched = MatchStatus.Matched) Or eTypeofRecords = TypeofRecords.OCRonly Then
                                ' Export both matched an  unmatched!!!
                                'If eTypeofRecords = TypeofRecords.AllRecords Or eTypeofRecords = TypeofRecords.OCRonly Then
                                If oPayment.Exported = False Then
                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        'If oPayment.I_Account = sI_Account Then
                                        ' 01.10.2018 � changed above If to:
                                        If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If

                                End If 'oPayment.MATCH_Matched = MatchStatus.Matched Then

                                If bExportoPayment Then
                                    If eTypeofRecords = BabelFiles.TypeofRecords.OCRonly Then
                                        If Not IsOCR((oPayment.PayCode)) Then
                                            bExportoPayment = False
                                        End If
                                    Else
                                        If IsOCR((oPayment.PayCode)) Then
                                            bExportoPayment = False
                                        Else
                                            bExportoPayment = False
                                            For Each oInvoice In oPayment.Invoices
                                                If oInvoice.MATCH_MatchType <> BabelFiles.MatchType.MatchedOnGL Then
                                                    bExportoPayment = True
                                                    Exit For
                                                End If
                                            Next oInvoice
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then
                                    'Find the client
                                    'If oPayment.I_Account <> sOldAccountNo Then
                                    If oPayment.I_Account <> sOldAccountNo Then

                                        If oPayment.VB_ProfileInUse Then
                                            sI_Account = oPayment.I_Account
                                            bAccountFound = False
                                            For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                For Each oClient In oFilesetup.Clients
                                                    For Each oaccount In oClient.Accounts
                                                        If sI_Account = oaccount.Account Then
                                                            sGLAccount = Trim(oaccount.GLAccount)
                                                            sOldAccountNo = oPayment.I_Account
                                                            bAccountFound = True

                                                            Exit For
                                                        End If
                                                    Next oaccount
                                                    If bAccountFound Then
                                                        Exit For
                                                    End If
                                                Next oClient
                                                If bAccountFound Then Exit For
                                            Next oFilesetup
                                        End If
                                    End If

                                    If Not bAccountFound Then
                                        Err.Raise(12003, "WriteSI_SIRI", LRS(12003))
                                        '12003: Could not find account %1.Please enter the account in setup."
                                    End If

                                    'First we have to find the fixed part of the freetext
                                    iFreetextCounter = 0
                                    sFreetextFixed = ""
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Original = True Then
                                            For Each oFreeText In oInvoice.Freetexts
                                                If oFreeText.Qualifier < 3 Then
                                                    iFreetextCounter = iFreetextCounter + 1
                                                    If iFreetextCounter = 1 Then
                                                        sFreetextFixed = RTrim(oFreeText.Text)
                                                    ElseIf iFreetextCounter = 2 Then
                                                        sFreetextFixed = sFreetextFixed & " " & RTrim(oFreeText.Text)
                                                    Else
                                                        Exit For
                                                    End If
                                                End If
                                            Next oFreeText
                                        End If
                                    Next oInvoice

                                    For Each oInvoice In oPayment.Invoices
                                        If Not EmptyString(sPaymentSelectOriginal) Then
                                            sPaymentParameter1 = ""
                                            sPaymentParameter2 = ""
                                            sPaymentSelect = sPaymentSelectOriginal

                                            'CustomerNo
                                            sPaymentSelect = Replace(sPaymentSelect, "BB_CustomerNo", Trim(oInvoice.CustomerNo), , , CompareMethod.Text)
                                            'InvoiceNo
                                            sPaymentSelect = Replace(sPaymentSelect, "BB_InvoiceNo", Trim(oInvoice.InvoiceNo), , , CompareMethod.Text)
                                            sPaymentSelect = Replace(sPaymentSelect, "BB_InvoiceIdentifier", Trim(oInvoice.InvoiceNo), , , CompareMethod.Text)
                                            'Currency
                                            sPaymentSelect = Replace(sPaymentSelect, "BB_Currency", Trim(oPayment.MON_InvoiceCurrency), , , CompareMethod.Text)
                                            'Amount
                                            sPaymentSelect = Replace(sPaymentSelect, "BB_Amount", ConvertFromAmountToString((oInvoice.MON_InvoiceAmount), , "."), , , CompareMethod.Text)
                                            'Payment date
                                            sPaymentSelect = Replace(sPaymentSelect, "BB_ValueDate", oPayment.DATE_Value, , , CompareMethod.Text)
                                            'Now()
                                            sPaymentSelect = Replace(sPaymentSelect, "BB_SysDate", VB6.Format(Now, "YYYYMMDD"), , , CompareMethod.Text)
                                            sPaymentSelect = Replace(sPaymentSelect, "BB_SysTime", VB6.Format(Now, "hhmmss"), , , CompareMethod.Text)

                                            oMyDal.SQL = sPaymentSelect
                                            If oMyDal.Reader_Execute() Then
                                                Do While oMyDal.Reader_ReadRecord
                                                    If InStr(1, sPaymentSelect, "BBRET_P_PARAMETER1", CompareMethod.Text) > 0 Then
                                                        sPaymentParameter1 = oMyDal.Reader_GetString("BBRET_P_PARAMETER1")
                                                    Else
                                                        sPaymentParameter1 = ""
                                                    End If
                                                    If InStr(1, sPaymentSelect, "BBRET_P_PARAMETER2", CompareMethod.Text) > 0 Then
                                                        sPaymentParameter2 = oMyDal.Reader_GetString("BBRET_P_PARAMETER2")
                                                    Else
                                                        sPaymentParameter2 = ""
                                                    End If
                                                    Exit Do
                                                Loop
                                            Else
                                                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                                            End If

                                        End If
                                        If Not EmptyString(sPaymentUpdateOriginal) Then
                                            sPaymentUpdate = sPaymentUpdateOriginal

                                            If Not EmptyString(sInitiateSelect) Then
                                                If sInintiateParameter1 <> "" Then
                                                    sPaymentUpdate = Replace(sPaymentUpdate, "BB_I_Parameter1", sInintiateParameter1, , , CompareMethod.Text)
                                                End If
                                                If sInintiateParameter2 <> "" Then
                                                    sPaymentUpdate = Replace(sPaymentUpdate, "BB_I_Parameter2", sInintiateParameter2, , , CompareMethod.Text)
                                                End If
                                            End If
                                            If Not EmptyString(sFileSelect) Then
                                                If sFileParameter1 <> "" Then
                                                    sPaymentUpdate = Replace(sPaymentUpdate, "BB_F_Parameter1", sFileParameter1, , , CompareMethod.Text)
                                                End If
                                                If sFileParameter2 <> "" Then
                                                    sPaymentUpdate = Replace(sPaymentUpdate, "BB_F_Parameter2", sFileParameter2, , , CompareMethod.Text)
                                                End If
                                            End If
                                            If Not EmptyString(sBatchSelect) Then
                                                If sBatchParameter1 <> "" Then
                                                    sPaymentUpdate = Replace(sPaymentUpdate, "BB_B_Parameter1", sBatchParameter1, , , CompareMethod.Text)
                                                End If
                                                If sBatchParameter2 <> "" Then
                                                    sPaymentUpdate = Replace(sPaymentUpdate, "BB_B_Parameter2", sBatchParameter2, , , CompareMethod.Text)
                                                End If
                                            End If
                                            If Not EmptyString(sPaymentSelect) Then
                                                sPaymentUpdate = Replace(sPaymentUpdate, "BB_P_Parameter1", sPaymentParameter1, , , CompareMethod.Text)
                                                sPaymentUpdate = Replace(sPaymentUpdate, "BB_P_Parameter2", sPaymentParameter2, , , CompareMethod.Text)
                                            End If

                                            'Replace variables from the collections
                                            'TransactionNumber
                                            lInvoiceCounter = lInvoiceCounter + 1
                                            sPaymentUpdate = Replace(sPaymentUpdate, "BB_InvoiceCounter", Trim(Str(lInvoiceCounter)), , , CompareMethod.Text)
                                            'CustomerNo
                                            sPaymentUpdate = Replace(sPaymentUpdate, "BB_CustomerNo", Trim(oInvoice.CustomerNo), , , CompareMethod.Text)
                                            'InvoiceNo
                                            sPaymentUpdate = Replace(sPaymentUpdate, "BB_InvoiceNo", Trim(oInvoice.InvoiceNo), , , CompareMethod.Text)
                                            'Currency
                                            sPaymentUpdate = Replace(sPaymentUpdate, "BB_Currency", Trim(oPayment.MON_InvoiceCurrency), , , CompareMethod.Text)
                                            'Amount
                                            sPaymentUpdate = Replace(sPaymentUpdate, "BB_Amount", ConvertFromAmountToString((oInvoice.MON_InvoiceAmount), , "."), , , CompareMethod.Text)
                                            'Payment date
                                            sPaymentUpdate = Replace(sPaymentUpdate, "BB_ValueDate", oPayment.DATE_Value, , , CompareMethod.Text)
                                            'Now()
                                            sPaymentUpdate = Replace(sPaymentUpdate, "BB_SysDate", VB6.Format(Now, "YYYYMMDD"), , , CompareMethod.Text)
                                            sPaymentUpdate = Replace(sPaymentUpdate, "BB_SysTime", VB6.Format(Now, "hhmmss"), , , CompareMethod.Text)

                                            oMyDal.SQL = sPaymentUpdate

                                            If oMyDal.ExecuteNonQuery Then
                                                If Not oMyDal.RecordsAffected > 0 Then
                                                    Err.Raise(34222, "WriteDatabase_Innbetaling", "En uventet feil oppstod ved skriving av betaling." & vbCrLf & "SQL: " & sPaymentUpdate & vbCrLf & vbCrLf & "BabelBank avbrytes!")
                                                End If
                                            Else
                                                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                            End If

                                        End If

                                    Next oInvoice

                                    oPayment.Exported = True

                                End If 'bExportoPayment
                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If
            Next oBabel 'BabelFile

            ' Oppdater siste bunt
            If lInvoiceCounter > 0 Then
                If Not EmptyString(sTerminateSelectOriginal) Then
                    sTerminateParameter1 = ""
                    sTerminateParameter2 = ""
                    sTerminateSelect = sTerminateSelectOriginal

                    oMyDal.SQL = sTerminateSelect
                    If oMyDal.Reader_Execute() Then
                        Do While oMyDal.Reader_ReadRecord
                            If InStr(1, sTerminateSelect, "BBRET_T_PARAMETER1", CompareMethod.Text) > 0 Then
                                sTerminateParameter1 = oMyDal.Reader_GetString("BBRET_T_PARAMETER1")
                            Else
                                sTerminateParameter1 = ""
                            End If
                            If InStr(1, sTerminateSelect, "BBRET_T_PARAMETER2", CompareMethod.Text) > 0 Then
                                sTerminateParameter2 = oMyDal.Reader_GetString("BBRET_T_PARAMETER2")
                            Else
                                sTerminateParameter2 = ""
                            End If
                            Exit Do
                        Loop
                    Else
                        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                    End If
                End If
                If Not EmptyString(sTerminateUpdateOriginal) Then
                    sTerminateUpdate = sTerminateUpdateOriginal

                    If Not EmptyString(sInitiateSelect) Then
                        If sInintiateParameter1 <> "" Then
                            sTerminateUpdate = Replace(sTerminateUpdate, "BB_I_Parameter1", sInintiateParameter1, , , CompareMethod.Text)
                        End If
                        If sInintiateParameter2 <> "" Then
                            sTerminateUpdate = Replace(sTerminateUpdate, "BB_I_Parameter2", sInintiateParameter2, , , CompareMethod.Text)
                        End If
                    End If
                    If Not EmptyString(sFileSelect) Then
                        If sFileParameter1 <> "" Then
                            sTerminateUpdate = Replace(sTerminateUpdate, "BB_F_Parameter1", sFileParameter1, , , CompareMethod.Text)
                        End If
                        If sFileParameter2 <> "" Then
                            sTerminateUpdate = Replace(sTerminateUpdate, "BB_F_Parameter2", sFileParameter2, , , CompareMethod.Text)
                        End If
                    End If
                    If Not EmptyString(sTerminateSelect) Then
                        If sTerminateParameter1 <> "" Then
                            sTerminateUpdate = Replace(sTerminateUpdate, "BB_T_Parameter1", sTerminateParameter1, , , CompareMethod.Text)
                        End If
                        If sBatchParameter2 <> "" Then
                            sTerminateUpdate = Replace(sTerminateUpdate, "BB_T_Parameter2", sTerminateParameter2, , , CompareMethod.Text)
                        End If
                    End If

                    'Replace variables from the collections
                    'TransactionNumber
                    sTerminateUpdate = Replace(sTerminateUpdate, "BB_InvoiceCounter", Trim(Str(lInvoiceCounter)), , , CompareMethod.Text)
                    'Now()
                    sTerminateUpdate = Replace(sTerminateUpdate, "BB_SysDate", VB6.Format(Now, "YYYYMMDD"), , , CompareMethod.Text)
                    sTerminateUpdate = Replace(sTerminateUpdate, "BB_SysTime", VB6.Format(Now, "hhmmss"), , , CompareMethod.Text)

                    oMyDal.SQL = sTerminateUpdate 'In the original code this line and the three above all said sPaymentUpdate but that must be wrong (probably ha this code never been used)

                    If oMyDal.ExecuteNonQuery Then
                        If Not oMyDal.RecordsAffected > 0 Then
                            Err.Raise(34222, "WriteDatabase_Innbetaling", "En uventet feil oppstod ved skriving av avslutningsrecord." & vbCrLf & "SQL: " & sTerminateUpdate & vbCrLf & vbCrLf & "BabelBank avbrytes!")
                        End If
                    Else
                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                    End If

                End If
            End If

            If oBabelFiles.Count > 0 Then
                If oBabelFiles.Item(1).Cargo.Special = "ULEFOS_SF" Then
                    'Mark all payments as exported, because so far just the transaction with a reference is exported
                    For Each oBabel In oBabelFiles
                        For Each oBatch In oBabel.Batches
                            For Each oPayment In oBatch.Payments
                                oPayment.Exported = True
                            Next oPayment
                        Next oBatch
                    Next oBabel
                End If
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteDatabase_Innbetaling" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If
            If Not oClient Is Nothing Then
                oClient = Nothing
            End If
            If Not oaccount Is Nothing Then
                oaccount = Nothing
            End If

        End Try

        WriteDatabase_Innbetaling = True

    End Function
End Module
