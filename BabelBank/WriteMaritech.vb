Option Strict Off
Option Explicit On
Module WriteMaritech
	' Write file with incoming payments (matched and unmatched) to
	' Maritech.
	' Based on Telepayformat
	
    Dim oFs As Scripting.FileSystemObject
	Dim oFile As Scripting.TextStream
	Dim sLine As String ' en output-linje
	
	' Vi m� dimme noen variable fdifferentor
	'I_Enterprise is used in all  BETFOR-records.
	Private sI_EnterpriseNo As String
	Private sTransDate As String
	Private bDomestic As Boolean
	'This variable show if the importfile we are working with is created from the
	' accounting system, or is a returnfile. The variable is set during reading the
	' BabelFile-object.
	Private bFileFromBank As Boolean
    'XNET - 30.05.2013 - Added new parameter
    Function WriteMaritechFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef iFilenameInNo As Short, ByRef sCompanyNo As String, ByRef sBranch As String, ByRef sVersion As String, ByRef SurroundWith0099 As Boolean, ByRef sClientNo As String, ByRef sSpecial As String) As Boolean
        Dim j, i, k As Double
        Dim bFirstLine As Boolean
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oClient As Client
        Dim oaccount As Account
        Dim oInvoice As Invoice
        'Dim oProfile As Profile
        'Dim oFileSetup As FileSetup
        'Dim op As Payment
        Dim sNewOwnref As String
        Dim bWriteBETFOR00 As Boolean 'Used with the sequencenumbering
        Dim bWriteBETFOR99Later As Boolean 'Used with the sequencenumbering
        Dim bLastBatchWasDomestic As Boolean 'Used to check if we change to/from domestic/intenational
        Dim bFirstBatch As Boolean 'True until we write the first BETFOR00
        Dim bFirstMassTransaction, bWriteOK As Boolean
        'Dim bCheckFilenameOut As Boolean
        Dim iCount As Short
        Dim nNoOfBETFOR, nSequenceNoTotal As Double
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim nClientNo As Double
        Dim bFoundClient As Boolean
        Dim xAccount As String
        Dim bExportMassRecords As Boolean ' New 25.10.01 by janp - default: No masstrans for returnfiles
        Dim bLastPaymentWasMass As Boolean
        Dim sDummy As String
        Dim sInternalAccount As String

        'To use with summarizing masspayment
        Dim oMassPayment As Payment
        Dim oMassInvoice As Invoice
        Dim bSumMassPayment As Boolean
        Dim nMassTransferredAmount As Double
        Dim sPayment_ID As String
        Dim lMassCounter As Integer
        Dim bContinueSummarize As Boolean

        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            bExportoBabel = False

            bWriteBETFOR00 = True
            bWriteBETFOR99Later = False
            bFirstBatch = True
            nSequenceNoTotal = 1 'new by JanP 25.10.01
            bFoundClient = False

            For Each oBabel In oBabelFiles
                If oBabel.VB_FilenameInNo = iFilenameInNo Then

                    bExportoBabel = False
                    'Have to go through each batch-object to see if we have objects that shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            ' XNET 29.11.2012, added delete of "omitted-payments", from Manual payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.StatusCode <> "-1" Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    ' XNET 29.11.2012, added delete of "omitted-payments", from Manual payments
                                    'If oPayment.I_Account = sI_Account And oPayment.StatusCode <> "-1" Then
                                    ' 01.06.2017 - MUST test on ClientNo, not I_Account
                                    If oPayment.VB_ClientNo = sClientNo And oPayment.StatusCode <> "-1" Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If
                        Next oPayment
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oBatch

                    If bExportoBabel Then
                        'Set if the file was created int the accounting system or not.
                        bFileFromBank = oBabel.FileFromBank
                        'i = 0 'FIXED Moved by Janp under case 2


                        'Loop through all Batch objs. in BabelFile obj.
                        For Each oBatch In oBabel.Batches
                            bExportoBatch = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oPayment In oBatch.Payments
                                ' XNET 29.11.2012, added delete of "omitted-payments", from Manual payments
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.StatusCode <> "-1" Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        ' XOKNET 29.11.2012, added delete of "omitted-payments", from Manual payments
                                        'If oPayment.I_Account = sI_Account And oPayment.StatusCode <> "-1" Then
                                        ' 01.06.2017 - MUST test on ClientNo, not I_Account
                                        If oPayment.VB_ClientNo = sClientNo And oPayment.StatusCode <> "-1" Then

                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If
                                'If true exit the loop, and we have data to export
                                If bExportoBatch Then
                                    Exit For
                                End If
                            Next oPayment


                            If bExportoBatch Then

                                ' New by Janp 25.10.01
                                ' Optional if Mass-trans will be exported for returnfiles.
                                ' Default = False
                                ' In filesetup, test ReturnMass
                                If oBatch.VB_ProfileInUse = True Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().ReturnMass. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        bExportMassRecords = oBatch.VB_Profile.FileSetups(iFormat_ID).ReturnMass
                                    Else
                                        bExportMassRecords = True
                                    End If
                                Else
                                    bExportMassRecords = True
                                End If

                                ' New by JanP 25.10.01
                                If Not (bExportMassRecords = False And (oPayment.PayType = "M" Or oPayment.PayType = "S")) Then
                                    ' Don't put mass-payments into returnfiles (unless user has asked for it)
                                    'If bFirstBatch then
                                    ' Changed by JanP 29.10.01
                                    If oBatch.VB_ProfileInUse Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If bFirstBatch Or oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch = True Then
                                            ' First batch in file, then write betfor00
                                            bWriteBETFOR00 = True
                                        Else
                                            ' Not first batch, and masstrans, don't write betfor00
                                            bWriteBETFOR00 = False
                                        End If
                                    Else
                                        If bFirstBatch Then
                                            bWriteBETFOR00 = True
                                        Else
                                            bWriteBETFOR00 = False
                                        End If
                                    End If
                                Else
                                    ' No export of mass-trans, but check if first betfor00:
                                    If bFirstBatch Then
                                        ' First batch in file, then write betfor00
                                        bWriteBETFOR00 = True
                                    Else
                                        ' Not first batch, and masstrans, don't write betfor00
                                        bWriteBETFOR00 = False
                                    End If
                                End If


                                'Check if we shall use the original sequenceno. or not
                                'First check if we use a profile
                                If oBatch.VB_ProfileInUse = True Then

                                    bFoundClient = False
                                    If Not bFoundClient Then
                                        For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                            For Each oaccount In oClient.Accounts
                                                If oaccount.Account = oBatch.Payments(1).I_Account Then
                                                    If EmptyString((oaccount.GLAccount)) Then
                                                        ' 30.05.2017 - for Int.accounts, more than 11 digits, need to us ConvertedAccount
                                                        If Not EmptyString(oaccount.ConvertedAccount) Then
                                                            sInternalAccount = Trim(oaccount.ConvertedAccount)
                                                        Else
                                                            sInternalAccount = Trim(oaccount.Account)
                                                        End If
                                                    Else
                                                        sInternalAccount = Trim(oaccount.GLAccount)
                                                    End If
                                                    bFoundClient = True
                                                    Exit For
                                                End If
                                            Next oaccount
                                            If bFoundClient Then
                                                Exit For
                                            End If
                                        Next oClient
                                    End If

                                    'Then if it is a return-file
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True Then
                                        bFirstLine = True
                                    Else
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                            'Use the sequenceno from oBabel
                                            Case 0
                                                If oBatch.SequenceNoStart = -1 Then
                                                    'FIXERROR: Babelerror No sequenceno imported
                                                Else
                                                    nSequenceNoTotal = oBatch.SequenceNoStart
                                                    ' New by JanP 25.10.01
                                                    ' If we have a file with several betfor00/99, then we must
                                                    ' check if any betfor99 is pending, and deduct 1 from sequence, for the
                                                    ' pending betfor99 (iSeqenceNoTotal is added 1 when the pending
                                                    ' BETFOR99 is written;
                                                    If bWriteBETFOR99Later Then
                                                        nSequenceNoTotal = nSequenceNoTotal - 1
                                                    End If
                                                End If
                                                'Use the seqno at filesetup level
                                            Case 1
                                                'If it isn't the first client, just keep on counting.
                                                If bWriteBETFOR00 Then
                                                    ' New by JanP 24.10.01
                                                    If bFirstBatch Then
                                                        'Only for the first batch if we have one sequenceseries!
                                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                        nSequenceNoTotal = oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal + 1
                                                        If nSequenceNoTotal > 9999 Then
                                                            nSequenceNoTotal = 0
                                                        End If
                                                    End If
                                                End If
                                                'Use seqno per client per filesetup
                                            Case 2
                                                bFoundClient = False
                                                i = 0 ' New by Janp 25.10.01 Sets how day sequence will be counted
                                                '                                For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                                For Each oaccount In oClient.Accounts
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    If oaccount.Account = oBatch.Payments(1).I_Account Then
                                                        nSequenceNoTotal = oClient.SeqNoTotal + 1
                                                        If nSequenceNoTotal > 9999 Then
                                                            nSequenceNoTotal = 0
                                                        End If
                                                        nClientNo = oClient.Index
                                                        '                                            bFoundClient = True
                                                        Exit For
                                                    End If
                                                Next oaccount
                                                '                                    If bFoundClient Then
                                                '                                        Exit For
                                                '                                    End If
                                                '                                Next
                                        End Select
                                    End If
                                Else
                                    nSequenceNoTotal = 1
                                    bFirstLine = True 'new by JanP 25.10.01
                                End If
                                'Used in every applicationheader, TBII = Domestic, TBIU = International.
                                If oBatch.Payments.Count = 0 Then
                                    bDomestic = True
                                    'If bFirstBatch Then
                                    '    bLastBatchWasDomestic = True
                                    'End If
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).PayType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                ElseIf oBatch.Payments(1).PayType = "I" Or oBatch.Payments(1).PayType = "D" Then
                                    bDomestic = False
                                    'If bFirstBatch Then
                                    '    bLastBatchWasDomestic = False
                                    'End If
                                Else
                                    bDomestic = True
                                    'If bFirstBatch Then
                                    '    bLastBatchWasDomestic = True
                                    'End If
                                End If
                                'Check if it is a change between domestic and international payment, and if
                                ' the bWriteBETFOR99Later flag is true (indicates that KeepBatch is set to false in
                                ' the database
                                If bLastBatchWasDomestic <> bDomestic Then
                                    If bWriteBETFOR99Later Then
                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                        nNoOfBETFOR = 0
                                    End If
                                    ' New by JanP 25.10.01
                                    If Not (bExportMassRecords = False And (oPayment.PayType = "M" Or oPayment.PayType = "S")) Then
                                        bWriteBETFOR00 = True
                                        bWriteBETFOR99Later = False
                                    Else
                                        'bWriteBETFOR00 = False  'Keep as is
                                        bWriteBETFOR99Later = False
                                    End If
                                End If

                                ' New by JanP 29.10.01
                                If oBatch.Payments.Count = 0 Then
                                    bLastBatchWasDomestic = True
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).PayType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                ElseIf oBatch.Payments(1).PayType = "I" Or oBatch.Payments(1).PayType = "D" Then
                                    bLastBatchWasDomestic = False
                                Else
                                    bLastBatchWasDomestic = True
                                End If


                                'Used in every applicationheader
                                If Not oBatch.DATE_Production = "19900101" Then
                                    sTransDate = Right(oBatch.DATE_Production, 4) '(oBatch.DATE_Production, "MMDD")
                                Else
                                    'If not imported set like computerdate
                                    sTransDate = VB6.Format(Now, "MMDD")
                                End If
                                'bFirstLine is used to set the sequenceno in the aplicationheader. If true seqno=1
                                'bFirstLine = False
                                'bFirstMassTransaction is used as a flag to treat the first mass transaction specially
                                'We have to create a BETFOR21-transaction before the first mass transaction (BETFOR22).
                                bFirstMassTransaction = True

                                'Reset count of BETFOR-records
                                If bWriteBETFOR00 Then
                                    nNoOfBETFOR = 0
                                    If oBatch.VB_ProfileInUse Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType = 1 Or oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem Then
                                            ' JanP 25.10.01 added ....FromAccountingSystem=True
                                            If bFirstBatch Then
                                                bFirstLine = True
                                            Else
                                                ' Keep on with daysequence until file end
                                                bFirstLine = False
                                            End If
                                        Else
                                            ' Reset day sequence for each betfor00
                                            bFirstLine = True
                                        End If
                                    Else
                                        If bFirstBatch Then
                                            bFirstLine = True
                                        Else
                                            ' Keep on with daysequence until file end
                                            bFirstLine = False
                                        End If
                                    End If
                                End If
                                i = i + 1

                                'If i = 1 Then
                                '    bFirstLine = True
                                'End If

                                'Get the companyno either from a property passed from BabelExport, or
                                ' from the imported file
                                If sCompanyNo <> "" Then
                                    sI_EnterpriseNo = PadLeft(sCompanyNo, 11, "0")
                                Else
                                    sI_EnterpriseNo = PadLeft(oBatch.I_EnterpriseNo, 11, "0")
                                End If
                                'BETFOR00
                                If bWriteBETFOR00 Then
                                    sLine = WriteBETFOR00(oBatch, bFirstLine, nSequenceNoTotal, sBranch, sCompanyNo)
                                    bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                    bFirstBatch = False

                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    xAccount = oBatch.Payments(1).I_Account ' Spec. for 0099-format

                                End If
                                j = 0
                                k = 0
                                lMassCounter = 1
                                For Each oPayment In oBatch.Payments

                                    '---------------------------------------
                                    ' Special for betfor0099-format;
                                    ' Surround each account with BETFOR00/99
                                    ' Ref NorgesGruppen
                                    '---------------------------------------
                                    If SurroundWith0099 Then
                                        If oPayment.I_Account <> xAccount Then
                                            If nNoOfBETFOR > 0 Then
                                                sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion)
                                                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                                nNoOfBETFOR = 0

                                                sLine = WriteBETFOR00(oBatch, bFirstLine, nSequenceNoTotal, sBranch, sCompanyNo)
                                                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                            End If
                                            xAccount = oPayment.I_Account ' Spec. for 0099-format
                                        End If
                                    End If

                                    bExportoPayment = False
                                    bLastPaymentWasMass = False

                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    ' XNET 29.11.2012, added delete of "omitted-payments", from Manual payments
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.StatusCode <> "-1" Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            ' XNET 29.11.2012, added delete of "omitted-payments", from Manual payments
                                            'If oPayment.I_Account = sI_Account And oPayment.StatusCode <> "-1" Then
                                            ' 01.06.2017 - MUST test on ClientNo, not I_Account
                                            If oPayment.VB_ClientNo = sClientNo And oPayment.StatusCode <> "-1" Then

                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If

                                    If bExportoPayment Then
                                        j = j + 1
                                        'Test what sort of transaction
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).PayType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If oPayment.PayType = "I" Or oBatch.Payments(1).PayType = "D" Then
                                            'International payment
                                            sLine = WriteBETFOR01(oPayment, nSequenceNoTotal, sCompanyNo, sInternalAccount, sSpecial)
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                            sLine = WriteBETFOR02(oPayment, nSequenceNoTotal, sCompanyNo, sInternalAccount)
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                            sLine = WriteBETFOR03(oPayment, nSequenceNoTotal, sCompanyNo, sInternalAccount)
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                            'Remove later: ExportCheck
                                            oPayment.Exported = True
                                            'Have to reset masstrans flag
                                            bFirstMassTransaction = True
                                        ElseIf oPayment.PayType = "M" Or oPayment.PayType = "S" Then
                                            If oPayment.Payment_ID <> sPayment_ID Then
                                                bFirstMassTransaction = True
                                            End If
                                            If bFirstMassTransaction Then
                                                'New by JanP 25.10.01 - Option to turn off export of masstranser for returnfiles
                                                If bExportMassRecords Then
                                                    'New code 10.07.2002
                                                    'Have to sumarize alle invoices/payments, to put the
                                                    'amount in Betfor21.
                                                    nMassTransferredAmount = 0
                                                    'Assumes that all BETFOR22 records in the imported file within
                                                    ' a BETFOR21 has the same PAYMENT_ID (also Invoice_ID).
                                                    sPayment_ID = oPayment.Payment_ID
                                                    bContinueSummarize = True
                                                    'Reset counter for BETFOR22-records
                                                    k = 0
                                                    Do While bContinueSummarize
                                                        If lMassCounter <= oBatch.Payments.Count Then
                                                            oMassPayment = oBatch.Payments.Item(lMassCounter)
                                                            'Have to go through the payment-object to see if the next payment also is
                                                            ' a masspayment from same account, paymentdate, filenameout and ownref
                                                            'If so add them together
                                                            If oMassPayment.VB_FilenameOut_ID = iFormat_ID And (oMassPayment.PayType = "S" Or oMassPayment.PayType = "M") Then
                                                                If oMassPayment.I_Account = sI_Account Then
                                                                    If oMassPayment.Payment_ID = sPayment_ID Then
                                                                        If InStr(oMassPayment.REF_Own, "&?") Then
                                                                            'Set in the part of the OwnRef that BabelBank is using
                                                                            sNewOwnref = Right(oMassPayment.REF_Own, Len(oMassPayment.REF_Own) - InStr(oMassPayment.REF_Own, "&?") - 1)
                                                                        Else
                                                                            sNewOwnref = ""
                                                                        End If
                                                                        If sNewOwnref = sOwnRef Then
                                                                            bSumMassPayment = True
                                                                        End If
                                                                    Else
                                                                        Exit Do
                                                                    End If
                                                                Else
                                                                    Exit Do
                                                                End If
                                                            Else
                                                                Exit Do
                                                            End If
                                                            If bSumMassPayment Then
                                                                For Each oMassInvoice In oMassPayment.Invoices
                                                                    nMassTransferredAmount = nMassTransferredAmount + oMassInvoice.MON_TransferredAmount
                                                                    lMassCounter = lMassCounter + 1
                                                                Next oMassInvoice
                                                            End If
                                                        Else
                                                            Exit Do
                                                        End If
                                                    Loop  'oMassPayment

                                                    'End new code

                                                    'BETFOR21 for Masstrans
                                                    sLine = WriteBETFOR21Mass(oPayment, nSequenceNoTotal, sCompanyNo, nMassTransferredAmount)
                                                    'First mass-transaction treated
                                                    bFirstMassTransaction = False
                                                    bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                                    'Remove later: ExportCheck
                                                    oPayment.Exported = True
                                                End If
                                            End If
                                        Else
                                            'BETFOR21 for invoicetrans
                                            sLine = WriteBETFOR21Invoice(oPayment, nSequenceNoTotal, sCompanyNo)
                                            'Have to reset masstrans flag
                                            bFirstMassTransaction = True
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                            'Remove later: ExportCheck
                                            oPayment.Exported = True
                                        End If
                                        'k = 0  Only one invoice pr payment for masspayments!
                                        'If its a payment to an own account we shall just create a BETFOR21, not 22 or 23
                                        If Not oPayment.ToOwnAccount Then
                                            For Each oInvoice In oPayment.Invoices
                                                'Check if the invoice is final!
                                                If oInvoice.MATCH_Final Then
                                                    'k will be used as a sequential number.
                                                    k = k + 1
                                                    ' 03.09.2019 - Fra Camt.054 kan vi lage PayType=M.
                                                    '           Men vi skal aldri ha BETFOR22 i Maritech sammenheng, s� derfor lagt til PayType = "M"
                                                    If oPayment.PayType = "I" Or oBatch.Payments(1).PayType = "D" Or oBatch.Payments(1).PayType = "M" Then
                                                        'Will create a BETFOR04 transaction
                                                        'XNET - 30.05.2013 - Added new parameter
                                                        sLine = WriteBETFOR04(oPayment.I_Account, oInvoice, k, nSequenceNoTotal, sCompanyNo, oPayment, sInternalAccount, sSpecial)
                                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                                    ElseIf oPayment.PayType = "M" Or oPayment.PayType = "S" Then
                                                        'New by JanP 25.10.01 - Option to turn off export of masstranser for returnfiles
                                                        If bExportMassRecords Then
                                                            'Will create a BETFOR22 transaction
                                                            sLine = WriteBETFOR22(oPayment, oInvoice, k, nSequenceNoTotal, sCompanyNo)
                                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                                            oPayment.Exported = True
                                                        End If
                                                        bLastPaymentWasMass = True
                                                    Else
                                                        'Will create a BETFOR23 transaction
                                                        iCount = 0
                                                        Do While iCount - 1 < oInvoice.Freetexts.Count
                                                            If iCount = 0 Then
                                                                iCount = 1
                                                                'nNoOfBETFOR = nNoOfBETFOR - 1
                                                                'nSequenceNoTotal = nSequenceNoTotal - 1
                                                            End If
                                                            If Val(oPayment.StatusCode) > 99 Then
                                                                If Val(CStr(oBabel.BankByCode)) = BabelFiles.Bank.DnB Then
                                                                    'DnB doesn't create a BETFOR23-record when the transaction
                                                                    ' is manually deleted.
                                                                Else
                                                                    If bFileFromBank Then
                                                                        oPayment.StatusCode = "02"
                                                                    Else
                                                                        oPayment.StatusCode = "00"
                                                                    End If
                                                                    sLine = WriteBETFOR23((oPayment.I_Account), oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo)
                                                                    bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                                                End If
                                                            Else
                                                                sLine = WriteBETFOR23((oPayment.I_Account), oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo)
                                                                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                                            End If
                                                        Loop
                                                    End If
                                                End If 'If oInvoice.MATCH_Final
                                            Next oInvoice
                                            'If its an international payment, check if its a difference between the
                                            '  Account amount (without charges) and the invoice amount
                                            'If so, create an BETFOR04 with the charges
                                            'Changed 11.10.2004 by Kjell
                                            'Excluded Paytype "D" from the next IF-statement
                                            'Changed 4.11.2004 by back to include paytype "D"
                                            'Because for Coast some payments are marked as "D" and have a charge.
                                            'For all domestic payments AccountAmount are set = InvoiceAmount if no
                                            ' AccountAmount are specificly stated.

                                            'Changed 13.12.2005 by Kjell
                                            'BEcause we now are matching on MON_invoiceAmount = Posted amount
                                            ' and not MON_OriginallyPaidAmount = Paid Amount
                                            ' there is no longer necessary to post the charges like we didi before
                                            'Has commented the code

                                            '''''                                If oPayment.PayType = "I" Or oPayment.PayType = "D" Then
                                            '''''                                    'Changed 06.02.04 by Kjell, because Leroy expierienced positive charges
                                            '''''                                    '  also made a change in WriteBETFOR04Charges
                                            '''''                                    If oPayment.MON_AccountAmount - oPayment.MON_InvoiceAmount <> 0 Then
                                            '''''                                    'If oPayment.MON_AccountAmount - oPayment.MON_InvoiceAmount < 0 Then
                                            '''''                                        'Must be same currency' if not its impossible to calculate the charges
                                            '''''                                        If oPayment.MON_TransferCurrency = oPayment.MON_InvoiceCurrency Then
                                            '''''                                            'Create an 'charges-invoice'
                                            '''''                                            k = k + 1
                                            '''''                                            sLine = WriteBETFOR04Charges(oPayment.I_Account, oPayment, k, nSequenceNoTotal, sCompanyNo, oClient.ChargesAccount)
                                            '''''                                            bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                            '''''                                        End If
                                            '''''                                    End If
                                            '''''                                End If 'oPayment.PayType = "I"

                                        End If

                                    End If
                                Next oPayment ' payment


                                ' Added by janP 25.10.01
                                'If bExportMassRecords Or bWriteBETFOR99Later Then
                                If Not (bExportMassRecords = False And bLastPaymentWasMass) Then
                                    sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion)
                                    If oBatch.VB_ProfileInUse = True Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        If oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch = True Then
                                            bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                        Else
                                            bWriteBETFOR99Later = True
                                        End If
                                    Else
                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                    End If
                                Else
                                    '    bWriteBETFOR99Later = True
                                    ' Masspayments and bExportMassRecords=False,
                                    ' Test if we had any non-mass payments before.
                                    If nNoOfBETFOR > 0 Then
                                        ' We had invoice-records before the mass-records.
                                        ' Must write Betfor99
                                        sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion)
                                        bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
                                    End If
                                End If
                                'We have to save the last used sequenceno. in some cases
                                'First check if we use a profile
                                If oBatch.VB_ProfileInUse = True Then
                                    'Then if it is a return-file.
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True Then
                                        'No action
                                    Else
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                            'Use the sequenceno from oBabel
                                            Case 0
                                                'No action
                                                'Use the seqno at filesetup level
                                            Case 1
                                                If bWriteBETFOR99Later Then
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal
                                                    sDummy = ApplicationHeader(False, "-1")
                                                Else
                                                    'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                    oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal - 1
                                                End If
                                                oBatch.VB_Profile.Status = 2
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 1
                                                'We have to omit BETFOR00 in the future (if we don't change between domestic/international)
                                                bWriteBETFOR00 = False
                                                ' New by JanP 26.11.01
                                                ' Must also reduce day-sequence by one, since it has been added
                                                ' one in WriteBetfor99, which will never be used!
                                                ' ??????? WRONG ????? Moved to bWriteBETFOR99Later, up!sDummy = ApplicationHeader(False, "-1")
                                                'Use seqno per client per filesetup
                                            Case 2
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).SeqNoTotal = nSequenceNoTotal - 1
                                                oBatch.VB_Profile.Status = 2
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 2
                                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).Status = 1
                                        End Select
                                    End If
                                End If

                            End If
                            'bLastBatchWasDomestic = False 'Changed by JanP 29.10
                            'bFirstBatch = False  ' Changed by Janp 25.10
                        Next oBatch 'batch

                    End If
                End If

            Next oBabel 'Babelfile

            'Have to write BETFOR99 if keepbatch is true
            If bWriteBETFOR99Later Then
                bWriteOK = WriteLine_Renamed(sLine, nNoOfBETFOR, nSequenceNoTotal)
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteMaritechFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteMaritechFile = True

        'Deleted 4/12 - Kjell
        'Exit Function
        '
        'errCreateOutputFile:
        '    oFile.Close
        '    'oFs.Close
        '    Set oFs = Nothing
        '    WriteTelepayFile = False

    End Function

    Function WriteBETFOR00(ByRef oBatch As Batch, ByRef bFirstLine As Boolean, ByRef nSequenceNoTotal As Double, ByRef sBranch As String, ByRef sCompanyNo As String) As String

        Dim sLine As String

        sLine = ""
        'Build AH, send Returncode
        'bFirstLine is used to set the sequenceno in the aplicationheader.
        sLine = ApplicationHeader(bFirstLine, (oBatch.StatusCode))
        sLine = sLine & "BETFOR00"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        If sBranch <> "" Then
            'Uses Branch from the database
            sLine = sLine & PadRight(sBranch, 11, " ")
        Else
            sLine = sLine & PadRight(oBatch.I_Branch, 11, " ")
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & Space(6)
        If Len(oBatch.DATE_Production) = 8 And oBatch.DATE_Production <> "19900101" Then
            sLine = sLine & Right(oBatch.DATE_Production, 4) 'oBatch.DATE_Production
        Else
            sLine = sLine & VB6.Format(Now, "MMDD")
        End If
        sLine = sLine & Space(10)
        sLine = sLine & "VERSJON002"
        sLine = sLine & Space(10)
        sLine = sLine & PadLeft(oBatch.OperatorID, 11, " ")
        'No use of SIGILL
        sLine = sLine & Space(1)
        sLine = sLine & New String("0", 26)
        sLine = sLine & Space(1)
        sLine = sLine & Space(142)
        sLine = sLine & Space(25)

        WriteBETFOR00 = sLine

    End Function
    Function WriteBETFOR01(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef sInternalAccount As String, ByVal sSpecial As String) As String

        Dim sLine As String
        'Dim sPayCode As String, iCount As Integer
        'Dim oInvoice As Invoice

        sLine = ""
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR01"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If

        sLine = sLine & PadRight(sInternalAccount, 11, "0")
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")
        sLine = sLine & Right(oPayment.DATE_Payment, 6) 'Format(oPayment.DATE_Payment, "YYMMDD")
        'sLine = sLine & Left$(oPayment.REF_Own + Space(30), 30)
        ' Set ownref to blank (so far)
        sLine = sLine & Space(30)

        If sSpecial = "SKAGERAK" Or sSpecial = "TEXT_TO_NAME" Then
            ' 06.09.2018
            ' For Skagerak og Skagerak_Terminal, NOK causes problems.
            ' Because of this we need to change from NOK to NKR
            If oPayment.MON_InvoiceCurrency = "NOK" Then
                oPayment.MON_InvoiceCurrency = "NKR"
                oPayment.MON_TransferCurrency = "NKR"
            End If

        End If
        sLine = sLine & PadRight(oPayment.MON_TransferCurrency, 3, " ")

        If Len(oPayment.MON_InvoiceCurrency) <> 3 Then
            sLine = sLine & PadRight(oPayment.MON_TransferCurrency, 3, " ")
            'FIXERROR: Babelerror
        Else
            If oPayment.MON_InvoiceCurrency <> oPayment.MON_TransferCurrency Then
                sLine = sLine & PadRight(oPayment.MON_TransferCurrency, 3, " ")
            Else
                sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ")
            End If
        End If
        If oPayment.MON_ChargeMeAbroad = True Then
            sLine = sLine & "OUR"
        Else
            sLine = sLine & "BEN"
        End If
        If oPayment.MON_ChargeMeDomestic = True Then
            sLine = sLine & "OUR"
        Else
            sLine = sLine & "BEN"
        End If
        If oPayment.NOTI_NotificationParty < 2 Then
            sLine = sLine & Space(30)
        Else
            Select Case oPayment.NOTI_NotificationType
                Case "PHONE"
                    sLine = sLine & "PHONE" & PadRight(oPayment.NOTI_NotificationIdent, 25, " ")
                Case "TELEX"
                    sLine = sLine & "TELEX" & PadRight(oPayment.NOTI_NotificationIdent, 25, " ")
                Case "OTHER"
                    sLine = sLine & "OTHER" & PadRight(oPayment.NOTI_NotificationIdent, 25, " ")
                Case Else
                    sLine = sLine & Space(30)
            End Select
        End If
        'sLine = sLine & PadRight(oPayment.NOTI_NotificationMessageToBank, 30, " ")
        If oPayment.Priority = True Then
            sLine = sLine & "J"
        Else
            sLine = sLine & " "
        End If
        sLine = sLine & PadLeft(CStr(Int(oPayment.ERA_ExchRateAgreed)), 4, "0")
        sLine = sLine & PadRight(CStr(oPayment.ERA_ExchRateAgreed - Int(oPayment.ERA_ExchRateAgreed)), 4, "0")
        'sLine = sLine & PadLeft(oPayment.ERA_ExchRateAgreed, 8, "0")
        sLine = sLine & PadRight(oPayment.FRW_ForwardContractNo, 6, " ")
        sLine = sLine & PadLeft(CStr(Int(oPayment.FRW_ForwardContractRate)), 4, "0")
        sLine = sLine & PadRight(CStr(oPayment.FRW_ForwardContractRate - Int(oPayment.FRW_ForwardContractRate)), 4, "0")
        'sLine = sLine & PadLeft(oPayment.FRW_ForwardContractRate, 8, "0")
        If oPayment.Cheque = BabelFiles.ChequeType.noCheque Then
            sLine = sLine & " "
        ElseIf oPayment.Cheque = BabelFiles.ChequeType.SendToPayer Then
            sLine = sLine & "0"
        ElseIf oPayment.Cheque = BabelFiles.ChequeType.SendToReceiver Then
            sLine = sLine & "1"
        End If
        sLine = sLine & New String("0", 6) & Space(2)
        If oPayment.StatusCode <> "02" Then
            sLine = sLine & "000000000000"
        Else
            If Len(oPayment.MON_InvoiceCurrency) = 3 And oPayment.MON_LocalExchRate <> 0 Then
                sLine = sLine & PadLeft(CStr(Int(oPayment.MON_LocalExchRate)), 4, "0")
                sLine = sLine & PadRight(Mid(Replace(CStr(System.Math.Round(oPayment.MON_LocalExchRate - Int(oPayment.MON_LocalExchRate), 8)), ",", ""), 2), 8, "0")
            Else
                sLine = sLine & "000000000000"
            End If

            ''        If oPayment.MON_TransferCurrency = oPayment.MON_AccountCurrency Then
            ''            If oPayment.MON_TransferCurrency <> "NOK" Then
            ''                'If so use the local exchangerate.
            ''                sLine = sLine & PadLeft(Int(oPayment.MON_LocalExchRate), 4, "0")
            ''                sLine = sLine & PadRight(Mid$(Replace(Round(oPayment.MON_LocalExchRate - Int(oPayment.MON_LocalExchRate), 8), ",", ""), 2), 8, "0")
            ''            Else
            ''                sLine = sLine & "000000000000"
            ''            End If
            ''        ElseIf oPayment.MON_AccountExchRate <> oPayment.MON_LocalExchRate Then
            ''            sLine = sLine & PadLeft(Int(oPayment.MON_AccountExchRate), 4, "0")
            ''            sLine = sLine & PadRight(Mid$(Replace(Round(oPayment.MON_AccountExchRate - Int(oPayment.MON_AccountExchRate), 8), ",", ""), 2), 8, "0")
            'sLine = sLine & PadLeft(oPayment.MON_AccountExchRate, 12, "0")  'Relle kurs, 191-202
        End If
        sLine = sLine & Space(12) 'FIXED from 16

        ' 12.10.2017
        ' problems at Skagerak; On LIN, we have no MOA:346, and then have no MON_AccountAmount. We need this, so we do as below.
        ' Skagerak will always have same currency for invoice as accounts currency
        If sSpecial = "SKAGERAK" Or sSpecial = "TEXT_TO_NAME" Then
            ' 08.05.2018 - sliter med at AccountAmount er feil - bruk invoiceamount for Skagerak
            ' 25.09.2019 - tatt vekk if-testen under
            'If oPayment.MON_AccountAmount = 0 Then
            oPayment.MON_AccountAmount = oPayment.MON_InvoiceAmount
            'End If
        End If

        If oPayment.MON_AccountAmount = 0 Then 'Belastest bel�p 215-230
            sLine = sLine & New String("0", 16)
            'New code
            sLine = sLine & New String("0", 16)
        Else
            sLine = sLine & PadLeft(Str(oPayment.MON_AccountAmount), 16, "0")
            'New code
            sLine = sLine & PadLeft(Str(oPayment.MON_AccountAmount), 16, "0")
        End If
        'Changed when an BETFOR04 is created for charges.
        'For new code see over
        '    If oPayment.MON_InvoiceAmount = 0 Then
        '        sLine = sLine & String(16, "0")
        '    Else
        '        sLine = sLine & PadLeft(Str(oPayment.MON_InvoiceAmount), 16, "0")
        '    End If
        sLine = sLine & Space(5)
        sLine = sLine & PadLeft(oPayment.REF_Bank1, 6, "0")
        sLine = sLine & PadRight(oPayment.ERA_DealMadeWith, 6, " ")
        If oPayment.Cancel = True Then
            sLine = sLine & "S"
        Else
            sLine = sLine & Space(1)
        End If
        'FIX: This field may still be in use
        sLine = sLine & Space(1)
        If oPayment.DATE_Value = "19900101" Then
            sLine = sLine & New String("0", 6)
        Else
            sLine = sLine & Right(oPayment.DATE_Value, 6) 'Format(oPayment.DATE_Value, "YYMMDD")
        End If
        If oPayment.MON_TransferCurrency = oPayment.MON_InvoiceCurrency Then
            ' 26.03.2018 - never put chargesamount here for Skagerak, always set as 0
            If sSpecial = "SKAGERAK" Or sSpecial = "TEXT_TO_NAME" Or sSpecial = "SEKKINGSTAD" Then
                sLine = sLine & New String("0", 9)
            Else
                sLine = sLine & PadLeft(Str(CDbl(Trim(CStr(oPayment.MON_InvoiceAmount - oPayment.MON_AccountAmount)))), 9, "0")
            End If
            ' 08.04.2018 - Denne linjen m� fjernes og l� med i forrige versjon.
            ' Dette bel en alvorlig feil for Skagerak/Sekkingstad
            ' ------>sLine = sLine & PadLeft(Str(CDbl(Trim(CStr(oPayment.MON_InvoiceAmount - oPayment.MON_AccountAmount)))), 9, "0")
        Else
            sLine = sLine & New String("0", 9)
        End If
        'sLine = sLine & PadLeft(Str(oPayment.MON_ChargesAmount), 9, "0")
        If oPayment.StatusCode <> "02" Then
            sLine = sLine & "000000000000"
        Else
            If Len(oPayment.MON_InvoiceCurrency) = 3 And oPayment.MON_LocalExchRate <> 0 Then
                sLine = sLine & PadLeft(CStr(Int(oPayment.MON_LocalExchRate)), 4, "0")
                sLine = sLine & PadRight(Mid(Replace(CStr(System.Math.Round(oPayment.MON_LocalExchRate - Int(oPayment.MON_LocalExchRate), 8)), ",", ""), 2), 8, "0")
            Else
                sLine = sLine & "000000000000"
            End If
        End If
        'sLine = sLine & PadLeft(oPayment.MON_LocalExchRate, 12, "0")
        sLine = sLine & Space(28)

        WriteBETFOR01 = UCase(sLine)


    End Function
    Function WriteBETFOR02(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef sInternalAccount As String) As String
        Dim sLine As String

        sLine = ""
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR02"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & PadRight(sInternalAccount, 11, "0")

        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCode, 11, " ")
        sLine = sLine & PadRight(oPayment.BANK_Name, 35, " ")
        sLine = sLine & PadRight(oPayment.BANK_Adr1, 35, " ")
        sLine = sLine & PadRight(oPayment.BANK_Adr2, 35, " ")
        sLine = sLine & PadRight(oPayment.BANK_Adr3, 35, " ")
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCodeCorrBank, 11, " ")
        If Len(oPayment.BANK_CountryCode) <> 2 Then
            'FIXERROR Babelerror, warning not compulsory?
            sLine = sLine & "  "
        Else
            sLine = sLine & oPayment.BANK_CountryCode
        End If
        sLine = sLine & PadRight(oPayment.BANK_BranchNo, 15, " ")

        sLine = sLine & Space(20)

        sLine = sLine & Space(41)

        WriteBETFOR02 = UCase(sLine)


    End Function
    Function WriteBETFOR03(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef sInternalAccount As String) As String

        Dim sLine As String
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sEnteredText As String
        'Dim sPayCode As String, iCount As Integer
        'Dim oInvoice As Invoice


        sLine = ""
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR03"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & PadRight(sInternalAccount, 11, "0")
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")
        sLine = sLine & PadRight(oPayment.E_Account, 35, " ")

        'Special code for Maritech. If user has added any text during manual matching,
        '   add this text in front of payers name. Maritech uses this field when posting
        '   aKonto-payments
        sEnteredText = ""
        For Each oInvoice In oPayment.Invoices
            For Each oFreeText In oInvoice.Freetexts
                If oFreeText.Qualifier = 4 Then
                    sEnteredText = oFreeText.Text
                End If
            Next oFreeText
        Next oInvoice
        If Len(sEnteredText) > 0 Then
            sLine = sLine & PadRight(sEnteredText & " - " & oPayment.E_Name, 35, " ")
        Else
            If oPayment.E_Name = "" Then
                sLine = sLine & Space(35)
            Else
                sLine = sLine & PadRight(oPayment.E_Name, 35, " ")
            End If
        End If
        sLine = sLine & PadRight(oPayment.E_Adr1, 35, " ")
        sLine = sLine & PadRight(oPayment.E_Adr2, 35, " ")
        sLine = sLine & PadRight(oPayment.E_Adr3, 35, " ")
        If Len(oPayment.E_CountryCode) <> 2 Then
            'FIXERROR: Babelerror
            sLine = sLine & Space(2)
        Else
            sLine = sLine & oPayment.E_CountryCode
        End If
        If oPayment.NOTI_NotificationParty = (1 Or 3) Then
            'Receiver shall be notified by payors bank
            If oPayment.NOTI_NotificationType = "TELEX" Then
                If oPayment.NOTI_NotificationIdent = "" Or oPayment.NOTI_NotificationAttention = "" Then
                    sLine = sLine & Space(41)
                    'FIX: Something is wrong. Some fields are compulsory. Issue a warning.
                Else
                    sLine = sLine & "T"
                    'Both Telex countrycode and number are stored in Ident.
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 20, " ")
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationAttention, 20, " ")
                End If
            ElseIf oPayment.NOTI_NotificationType = "FAX" Then
                If oPayment.NOTI_NotificationIdent = "" Or oPayment.NOTI_NotificationAttention = "" Then
                    sLine = sLine & Space(41)
                    'FIX: Something is wrong. Some fields are compulsory. Issue a warning.
                Else
                    sLine = sLine & "F" & "  "
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 18, " ")
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationAttention, 20, " ")
                End If
            Else
                'FIXERROR: Babelerror. Not valid code, just warning
                sLine = sLine & Space(41)
            End If
        Else
            'No notification by payors bank
            sLine = sLine & Space(41)
        End If
        'Old code
        'sLine = sLine & PadRight(oPayment.Telex_CountryCode, 2, " ")
        'If oPayment.NOTI_NotificationParty <> "" And oPayment.TelexNo = "" Then
        'FIXERROR: Babelerror
        'Else
        '    sLine = sLine & PadRight(oPayment.TelexNo, 18, " ")
        'End If
        'If oPayment.NOTI_NotificationParty <> "" And oPayment.NOTI_NotificationAttention = "" Then
        'FIXERROR: Babelerror
        'Else
        'sLine = sLine & PadRight(oPayment.NOTI_NotificationAttention, 20, " ")
        'End If
        sLine = sLine & Space(22)

        WriteBETFOR03 = UCase(sLine)


    End Function
    'XNET - 30.05.2013 - Added new parameter
    Function WriteBETFOR04(ByRef sI_Account As String, ByRef oInvoice As Invoice, ByRef iSerialNo As Double, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef oPayment As vbBabel.Payment, ByRef sInternalAccount As String, ByRef sSpecial As String) As String

        Dim sLine As String
        Dim iCount As Short
        Dim oFreeText As Freetext
        Dim sTmp As String
        Dim sMatchType As String
        Dim sResponse As String

        iCount = 0
        sLine = ""
        sLine = ApplicationHeader(False, (oInvoice.StatusCode))
        sLine = sLine & "BETFOR04"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & PadRight(sInternalAccount, 11, "0")

        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oInvoice.Invoice_ID, 6, " ")
        If oFreeText Is Nothing Then
            sLine = sLine & Space(35)
        Else
            For Each oFreeText In oInvoice.Freetexts
                iCount = iCount + 1
                If iCount > 1 Then
                    'FIXERROR: Can't be more than 1 textlines in Telepay International
                Else
                    sLine = sLine & Left(oFreeText.Text & Space(35), 35)
                End If
            Next oFreeText
        End If
        'sLine = sLine & PadRight(oInvoice.REF_Own, 35, " ")
        'New code by Kjell 7/9-2004, next two IFs
        ' Egenreferanse, 116-150
        ' 21.03.06: Added code to be able to check if MatchID starts with letter
        If oPayment.VB_Profile.RunTempCode Then
            If Not (Asc(UCase(Left(oInvoice.MATCH_ID, 1))) > 64 And Asc(UCase(Left(oInvoice.MATCH_ID, 1))) < 90) Then
                ' No Letter. Construct info to user, and present an inputbox
                sTmp = ""
                sTmp = sTmp & oPayment.E_Name & vbCrLf
                sTmp = sTmp & "Totalbel�p " & VB6.Format(oPayment.MON_InvoiceAmount / 100, "##,##0.00") & vbCrLf
                sTmp = sTmp & "Dato " & oPayment.DATE_Payment & vbCrLf
                sTmp = sTmp & "Fakturabel�p " & VB6.Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00") & vbCrLf
                sTmp = sTmp & sMatchType & vbCrLf
                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer Then
                    sMatchType = "Avstemt p� kunde"
                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL Then
                    sMatchType = "Avstemt hovedbok"
                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice Then
                    sMatchType = "Avstemt p� faktura"
                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnSupplier Then
                    sMatchType = "Avstemt p� leverand�r"
                Else
                    sMatchType = "Uavstemt"
                End If
                sTmp = sTmp & sMatchType & vbCrLf & vbCrLf

                sResponse = oInvoice.MATCH_ID

                sResponse = InputBox(sTmp & "Legg inn ny avstemmingsref:", "FEIL I AVSTEMMINGSREF.!", sResponse)
                If Not EmptyString(sResponse) Then
                    oInvoice.MATCH_ID = sResponse
                End If
            End If
        End If

        ' XNET 27.05.2013 - coded especially for SALMAR first
        ' For some reason we end up with only reskontronr for aKonto-f�ringer. Can't find out why.
        ' OK for all other, so we will as a n�dl�sning! set them as aKonto
        If sSpecial = "SALMAR" Then
            If Not (Asc(UCase(Left$(oInvoice.MATCH_ID, 1))) > 64 And _
                Asc(UCase(Left$(oInvoice.MATCH_ID, 1))) < 90) Then
                oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer
                oPayment.MATCH_InvoiceAKontoID = "U-101-" & oInvoice.MATCH_ID
            End If
        End If

        ' 16.08.2018 - added next If, as also Sekkingstad and Skagerak Salmon have problems with aKonto, without U-100-
        ' TEXT_TO_NAME is used for Skagerak Salmon and Skagerak Terminal
        If sSpecial = "SKAGERAK" Or sSpecial = "TEXT_TO_NAME" Or sSpecial = "SEKKINGSTAD" Then
            If Mid(oInvoice.MATCH_ID, 2, 1) <> "-" Then
                ' we have like 33533 only, which probably is a customernumber
                ' add U-100 or U-101
                ' 30.08.2019 - changed the below, as Skagerak has also started using FAC (utkj�psordning for DNB)
                'If oPayment.VB_ClientNo = "FAC" Then
                If oPayment.VB_ClientNo = "FAC" And sSpecial = "SEKKINGSTAD" Then
                    oInvoice.MATCH_ID = "U-100-" & oInvoice.MATCH_ID
                ElseIf oPayment.VB_ClientNo = "FAC" And sSpecial = "TEXT_TO_NAME" Then
                    ' Skagerak
                    oInvoice.MATCH_ID = "U-101-" & oInvoice.MATCH_ID
                Else
                    oInvoice.MATCH_ID = "U-" & oPayment.VB_ClientNo & "-" & oInvoice.MATCH_ID  ' Skagerak Sa
                End If
            End If
        End If
            If Not oInvoice.MATCH_PartlyPaid Then
                ' XNET 07.05.2013 - added If for MatchedOnCustomer, to be able to use BBRET_AkontoID for aKonto-matched invoices (U-101-12345)
                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer And Not EmptyString(oPayment.MATCH_InvoiceAKontoID) Then
                    ' aKonto
                    sLine = sLine & Left$(oPayment.MATCH_InvoiceAKontoID + Space(35), 35)
                    'XNET - 30.05.2013 - Added next line
                    oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnCustomer
                Else
                    sLine = sLine & Left$(oInvoice.MATCH_ID + Space(35), 35)
                End If
            Else
                If Left(oInvoice.MATCH_ID, 1) = "A" Then
                    sLine = sLine & "B" & Left(Mid(oInvoice.MATCH_ID, 2) & Space(35), 34)
                Else
                    sLine = sLine & Left(oInvoice.MATCH_ID & Space(35), 35)
                End If
            End If
            'Old code
            'sLine = sLine & Left$(oInvoice.MATCH_ID + Space(35), 35)
            If oPayment.MON_TransferCurrency = oPayment.MON_InvoiceCurrency Then
                sLine = sLine & PadLeft(Str(System.Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0")
                If oInvoice.MON_InvoiceAmount < 0 Then
                    sLine = sLine & "K"
                Else
                    sLine = sLine & "D"
                End If
            Else
                sLine = sLine & PadLeft(Str(System.Math.Abs(oInvoice.MON_TransferredAmount)), 15, "0")
                If oInvoice.MON_InvoiceAmount < 0 Then
                    sLine = sLine & "K"
                Else
                    sLine = sLine & "D"
                End If
            End If
            sLine = sLine & PadRight(oInvoice.STATEBANK_Code, 6, " ")
            'Changed code 04.04.2005, to add reference from bank
            'sLine = sLine & PadRight(oInvoice.STATEBANK_Text, 60, " ")
            If Not EmptyString(oPayment.REF_Bank1) Then
                sLine = sLine & PadRight(Trim(oPayment.REF_Bank1), 60, " ") 'STATEBANK_Text
            ElseIf Not EmptyString(oPayment.REF_Bank2) Then
                sLine = sLine & PadRight(Trim(oPayment.REF_Bank2), 60, " ") 'STATEBANK_Text
            Else
                sLine = sLine & Space(60)
            End If
            'FIX: To own account is not imported from Telepay
            sLine = sLine & Space(1)
            sLine = sLine & Space(1)
            sLine = sLine & New String("0", 6)
            sLine = sLine & Space(1)
            sLine = sLine & New String("0", 6)
            sLine = sLine & Space(46)
            If oInvoice.StatusCode = "00" Then
                sLine = sLine & "000"
            Else
                sLine = sLine & PadLeft(Str(iSerialNo), 3, "0")
            End If
            sLine = sLine & Space(24)

            WriteBETFOR04 = UCase(sLine)


    End Function
    Function WriteBETFOR04Charges(ByRef sI_Account As String, ByRef oPayment As Payment, ByRef iSerialNo As Double, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef sChargesAccount As String) As String

        Dim sLine As String
        Dim iCount As Short

        iCount = 0
        sLine = ""
        'sLine = ApplicationHeader(False, oInvoice.StatusCode)
        sLine = ApplicationHeader(False, "02")
        sLine = sLine & "BETFOR04"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & PadRight(sI_Account, 11, "0")

        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        'sLine = sLine & PadRight(oInvoice.Invoice_ID, 6, " ")
        sLine = sLine & Space(6)
        '    If oFreeText Is Nothing Then
        '        sLine = sLine & Space(35)
        '    Else
        '        For Each oFreeText In oInvoice.Freetexts
        '            iCount = iCount + 1
        '            If iCount > 1 Then
        '                'FIXERROR: Can't be more than 1 textlines in Telepay International
        '            Else
        '                sLine = sLine & Left(oFreeText.Text + Space(35), 35)
        '            End If
        '        Next
        '    End If
        sLine = sLine & Space(35)
        'sLine = sLine & Left$(oInvoice.MATCH_ID + Space(35), 35)
        sLine = sLine & PadRight(Trim(sChargesAccount), 35, " ")

        'Changed 06.02.04 by Kjell, because Leroy expierienced positive charges
        '  also made a change in WriteBETFOR04Charges
        If oPayment.MON_AccountAmount - oPayment.MON_InvoiceAmount < 0 Then
            sLine = sLine & PadLeft(Trim(Str(System.Math.Abs(oPayment.MON_AccountAmount - oPayment.MON_InvoiceAmount))), 15, "0")
            sLine = sLine & "K"
        Else
            sLine = sLine & PadLeft(Trim(Str(System.Math.Abs(oPayment.MON_InvoiceAmount - oPayment.MON_AccountAmount))), 15, "0")
            sLine = sLine & "D"
            '''        MsgBox "Postert bel�p p� bankkonto er st�rre enn innbetalt bel�p fra kunde." & vbCrLf & _
            ''''            "Det vil bli generert et positivt gebyr. Sjekk oppdatering i Maritech." & vbCrLf & vbCrLf & _
            ''''            "Betalers navn:   " & oPayment.E_Name & vbCrLf & _
            ''''            "Innbetalt bel�p: " & Trim$(Str(oPayment.MON_InvoiceAmount)) & vbCrLf & _
            ''''            "Postert bel�p:   " & Trim$(Str(oPayment.MON_AccountAmount)) & vbCrLf & _
            ''''            "Positivt gebyr:  " & Trim$(Str(Abs(oPayment.MON_InvoiceAmount - oPayment.MON_AccountAmount)))
        End If

        'Old code
        'sLine = sLine & PadLeft(Trim$(Str(Abs(oPayment.MON_AccountAmount - oPayment.MON_InvoiceAmount))), 15, "0")
        'sLine = sLine & "K"


        'sLine = sLine & PadRight(oInvoice.STATEBANK_Code, 6, " ")
        sLine = sLine & Space(6)
        'Changed code 04.04.2005, to add reference from bank
        'sLine = sLine & space(60)
        If Not EmptyString((oPayment.REF_Bank1)) Then
            sLine = sLine & PadRight(Trim(oPayment.REF_Bank1), 60, " ") 'STATEBANK_Text
        ElseIf Not EmptyString((oPayment.REF_Bank2)) Then
            sLine = sLine & PadRight(Trim(oPayment.REF_Bank2), 60, " ") 'STATEBANK_Text
        Else
            sLine = sLine & Space(60)
        End If

        'FIX: To own account is not imported from Telepay
        sLine = sLine & Space(1)
        sLine = sLine & Space(1)
        sLine = sLine & New String("0", 6)
        sLine = sLine & Space(1)
        sLine = sLine & New String("0", 6)
        sLine = sLine & Space(46)
        '    If oInvoice.StatusCode = "00" Then
        '        sLine = sLine & "000"
        '    Else
        '        sLine = sLine & PadLeft(Str(iSerialNo), 3, "0")
        '    End If
        sLine = sLine & PadLeft(Str(iSerialNo), 3, "0")

        sLine = sLine & Space(24)

        WriteBETFOR04Charges = UCase(sLine)


    End Function
    Function WriteBETFOR21Invoice(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String) As String

        Dim sLine As String
        Dim sPayCode As String
        Dim iCount As Short
        Dim oInvoice As Invoice

        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "TELEPAY")
        ' "New" PayCode 603 when no account specified
        If oPayment.E_Account = "00000000000" Or Len(oPayment.E_Account) = 0 Then
            sPayCode = "603"
        End If

        sLine = ""
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR21"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")
        sLine = sLine & Right(oPayment.DATE_Payment, 6) 'Format(oPayment.DATE_Payment, "YYMMDD")
        'sLine = sLine & PadRight(oPayment.REF_Own, 30, " ")
        sLine = sLine & Left(oPayment.REF_Own & Space(30), 30)
        sLine = sLine & Space(1)
        'FIXERROR: Babelerror if E_Accoun longer than 11 or alfanumeric
        If oPayment.E_Account = "00000000000" Or Len(oPayment.E_Account) = 0 Or oPayment.PayCode = "160" Then
            sLine = sLine & "00000000019"
        Else
            sLine = sLine & PadRight(oPayment.E_Account, 11, " ")
        End If
        sLine = sLine & PadRight(oPayment.E_Name, 30, " ")
        sLine = sLine & PadRight(oPayment.E_Adr1, 30, " ")
        sLine = sLine & PadRight(oPayment.E_Adr2, 30, " ")
        'FIXERROR: Babelerror if E_Zip longer than 4 or alfanumeric
        If Len(Trim(oPayment.E_Zip)) = 0 Then
            sLine = sLine & "0579" ' In Telepay Zip is mandatory
        Else
            sLine = sLine & PadRight(oPayment.E_Zip, 4, " ")
        End If
        If Len(Trim(oPayment.E_City)) = 0 Then
            sLine = sLine & PadRight("OSLO", 26, " ") ' In Telepay City is mandatory
        Else
            sLine = sLine & PadRight(oPayment.E_City, 26, " ")
        End If
        'The amount if its a payment to own account
        If oPayment.ToOwnAccount = True Then
            iCount = 0
            For Each oInvoice In oPayment.Invoices
                iCount = iCount + 1
                If iCount > 1 Then
                    'FIXERROR: Can't be more than 1 invoice when it is a payment to own account
                Else
                    sLine = sLine & PadLeft(Str(oInvoice.MON_InvoiceAmount), 15, "0")
                End If
            Next oInvoice
        Else
            sLine = sLine & New String("0", 15)
        End If
        sLine = sLine & sPayCode

        'FIX: Changed by Jan-Petter 28.06 due to no value from CodaImport
        ' Should be added in the importformat.
        If oPayment.PayType = "" Then
            sLine = sLine & "F"
        Else
            If oPayment.ToOwnAccount = True Then
                sLine = sLine & "E"
            Else
                sLine = sLine & "F"
            End If
        End If
        If oPayment.Cancel = True Then
            sLine = sLine & "S"
            If bFileFromBank Then
                sLine = Left(sLine, 3) & "02" & Mid(sLine, 7)
            Else
                sLine = Left(sLine, 3) & "00" & Mid(sLine, 7)
            End If
        Else
            sLine = sLine & Space(1)
        End If
        'If this file shall be sent to the bank the amount shall be set to 0.
        If oPayment.StatusCode = "00" Then 'Amount = 0 Then
            sLine = sLine & New String("0", 15)
        ElseIf oPayment.StatusCode = "" Then
            'The imported file was not Telepay
            If Not bFileFromBank Then
                sLine = sLine & New String("0", 15)
            Else
                sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 15, "0")
            End If
        Else
            sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 15, "0")
        End If
        sLine = sLine & New String("0", 5)
        If oPayment.DATE_Value = "19900101" Then
            sLine = sLine & New String("0", 6)
        Else
            sLine = sLine & Right(oPayment.DATE_Value, 6) 'Format(oPayment.DATE_Value, "YYMMDD")
        End If
        sLine = sLine & Space(26)


        WriteBETFOR21Invoice = UCase(sLine)


    End Function
    Function WriteBETFOR21Mass(ByRef oPayment As Payment, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef nTransferredAmount As Double) As String

        Dim sLine As String
        Dim sPayCode As String ', iCount As Integer
        'Dim oInvoice As Invoice


        sLine = ""
        'FIX: Have to add some parameters, i.e. Returncode
        sLine = ApplicationHeader(False, (oPayment.StatusCode))
        sLine = sLine & "BETFOR21"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If

        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "TELEPAY", "M")
        If sPayCode = "621" Then
            ' Mass payments, use Ownref as text on receivers statement
            ' Need to put Ownref into Babels Text_E_Statement
            oPayment.REF_Own = oPayment.Text_E_Statement
        End If

        sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")
        sLine = sLine & Right(oPayment.DATE_Payment, 6) 'Format(oPayment.DATE_Payment, "YYMMDD")
        'sLine = sLine & PadRight(oPayment.REF_Own, 30, " ")
        sLine = sLine & Left(oPayment.REF_Own & Space(30), 30)
        sLine = sLine & Space(1)
        'FIXERROR: Babelerror if E_Accoun longer than 11 or alfanumeric
        If Len(Trim(oPayment.E_Account)) = 11 Then
            sLine = sLine & Space(11) 'PadRight(oPayment.E_Account, 11, " ")
        Else
            sLine = sLine & "00000000000"
        End If
        sLine = sLine & Space(30) 'PadRight(oPayment.E_Name, 30, " ")
        sLine = sLine & Space(30) 'PadRight(oPayment.E_Adr1, 30, " ")
        sLine = sLine & Space(30) 'PadRight(oPayment.E_Adr2, 30, " ")
        'FIXERROR: Babelerror if E_Zip longer than 4 or alfanumeric
        sLine = sLine & "0000" 'PadRight(oPayment.E_Zip, 4, " ")
        sLine = sLine & Space(26) 'PadRight(oPayment.E_City, 26, " ")
        'The amount if its a payment to own account
        sLine = sLine & New String("0", 15)

        sLine = sLine & sPayCode
        If oPayment.PayType = "S" Then 'Salary
            sLine = sLine & "L"
        Else
            sLine = sLine & "M"
        End If
        If oPayment.Cancel = True Then
            sLine = sLine & "S"
        Else
            sLine = sLine & Space(1)
        End If
        If oPayment.StatusCode <> "02" Then 'No amount in betfor21 in sendfiles
            sLine = sLine & New String("0", 15)
        Else
            sLine = sLine & PadLeft(Str(nTransferredAmount), 15, "0")
            'sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 15, "0")
        End If
        sLine = sLine & Space(5)
        If oPayment.DATE_Value = "19900101" Then
            sLine = sLine & Space(6)
        Else
            sLine = sLine & Right(oPayment.DATE_Value, 6) 'Format(oPayment.DATE_Value, "YYMMDD")
        End If
        sLine = sLine & Space(26)


        WriteBETFOR21Mass = UCase(sLine)


    End Function

    Function WriteBETFOR23(ByRef sI_Account As String, ByRef oInvoice As Invoice, ByRef iSerialNo As Double, ByRef iCount As Short, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String) As String

        Dim sLine As String
        'Dim oFreeText As FreeText
        Dim iCountLocal As Short


        sLine = ""
        sLine = ApplicationHeader(False, (oInvoice.StatusCode))
        sLine = sLine & "BETFOR23"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & PadRight(sI_Account, 11, "0")
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oInvoice.Invoice_ID, 6, " ")
        'Can't have both Freetext and KID
        iCountLocal = 0
        If oInvoice.Freetexts.Count >= iCount Then
            For iCountLocal = 1 To 3
                If oInvoice.Freetexts.Count >= iCount Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object oInvoice.Freetexts().Text. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sLine = sLine & oInvoice.Freetexts(iCount).Text
                    iCount = iCount + 1
                End If
            Next iCountLocal
            sLine = PadLine(sLine, 227, " ")
        ElseIf oInvoice.Unique_Id <> "" Then
            sLine = sLine & Space(120)
            sLine = sLine & PadRight(oInvoice.Unique_Id, 27, " ")
        Else
            'If no freetext is given, the add a "."
            sLine = sLine & "." & Space(146)
        End If
        'sLine = sLine & PadRight(oInvoice.REF_Own, 30, " ")
        sLine = sLine & Left(oInvoice.MATCH_ID & Space(30), 30)
        If iCount > 4 Then
            sLine = sLine & New String("0", 15)
        Else
            sLine = sLine & PadLeft(Str(System.Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0")
        End If
        If iCount > 4 Then
            sLine = sLine & "D"
        ElseIf oInvoice.MON_InvoiceAmount < 0 Then
            sLine = sLine & "K"
        Else
            sLine = sLine & "D"
        End If
        sLine = sLine & Space(20)
        If oInvoice.StatusCode = "00" Then
            sLine = sLine & "000"
        ElseIf oInvoice.StatusCode = "" Then
            'The imported file was not Telepay
            If Not bFileFromBank = True Then
                sLine = sLine & "000"
            Else
                sLine = sLine & PadLeft(Str(iSerialNo), 3, "0")
            End If
        Else
            sLine = sLine & PadLeft(Str(iSerialNo), 3, "0")
        End If
        sLine = sLine & Space(24)

        WriteBETFOR23 = UCase(sLine)


    End Function
    Function WriteBETFOR22(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef iSerialNo As Double, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String) As String

        Dim sLine As String
        'Dim iCount As Integer
        'Dim oFreeText As FreeText

        sLine = ""
        sLine = ApplicationHeader(False, (oInvoice.StatusCode))
        sLine = sLine & "BETFOR22"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadLeft(oInvoice.Invoice_ID, 6, " ")
        sLine = sLine & PadRight(oPayment.E_Account, 11, " ")
        sLine = sLine & PadRight(oPayment.E_Name, 30, " ")
        sLine = sLine & PadLeft(Str(System.Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0")
        If oPayment.Cancel = True Then
            sLine = sLine & "S"
        Else
            sLine = sLine & Space(1)
        End If
        'sLine = sLine & PadRight(oInvoice.REF_Own, 35, " ")
        sLine = sLine & Left(oInvoice.REF_Own & Space(35), 35)
        sLine = sLine & Space(110)
        sLine = sLine & PadRight(oInvoice.Extra1, 10, " ")
        If oInvoice.StatusCode = "00" Or Len(Trim(oInvoice.StatusCode)) = 0 Then
            sLine = sLine & "0000"
        Else
            sLine = sLine & PadLeft(Str(iSerialNo), 4, "0")
        End If
        sLine = sLine & Space(24)

        WriteBETFOR22 = UCase(sLine)


    End Function
    Function WriteBETFOR99(ByRef oBatch As Batch, ByRef nBETFORnumber As Double, ByRef nSequenceNoTotal As Double, ByRef sCompanyNo As String, ByRef sVersion As String) As String

        Dim sLine As String


        sLine = ""
        'Build AH, send Returncode
        sLine = ApplicationHeader(False, (oBatch.StatusCode))

        sLine = sLine & "BETFOR99"
        If sCompanyNo <> "" Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        sLine = sLine & Space(11)
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & Space(6)
        If oBatch.DATE_Production <> "19900101" Then
            sLine = sLine & Right(oBatch.DATE_Production, 4) 'Format(oBatch.DATE_Production, "MMDD")
        Else
            sLine = sLine & VB6.Format(Now, "MMDD")
        End If
        sLine = sLine & Space(19)
        sLine = sLine & PadLeft(Str(nBETFORnumber + 1), 5, "0")
        'FIX: No use of AEGIS or SIGILL
        sLine = sLine & Space(188)
        ' Only four chars and four digits for versioninfo
        sLine = sLine & "BABL" & Left(Replace(sVersion, ".", ""), 1) & Right(Replace(sVersion, ".", ""), 3)
        sLine = PadRight(sLine, 320, " ") ' Fill until 320-record is ok


        WriteBETFOR99 = (sLine)

    End Function
    Function ApplicationHeader(ByRef bFirstLine As Boolean, ByRef sStatusCode As String) As String

        Dim sAH As String
        Static nSequenceNo As Double

        ' XNET 05.02.2013 -
        ' Det vil vel aldri bli aktuelt � ikke skrive de 40 f�rste i en BETFOR for Maritech !!!?
        ' Dette gav problemer noen ganger med I- poster (uavstemte, til observasjonskonto)
        'If sStatusCode = "-1" Then
        '    ' reduce nsequenceno by 1 because of unwritten records
        '    '(new bay janP 26.11.01
        '    nSequenceNo = nSequenceNo - 1
        '    sAH = vbNullString
        'Else

        If bFirstLine = True Then
            nSequenceNo = 1
        Else
            nSequenceNo = nSequenceNo + 1
        End If
        If sStatusCode = "" Then
            'The imported file was not Telepay
            If Not bFileFromBank Then
                sStatusCode = "00"
            Else
                sStatusCode = "02"
            End If
        End If

        sAH = ""
        sAH = "AH1"
        sAH = sAH & sStatusCode
        If sStatusCode = "00" Then
            If bDomestic Then
                sAH = sAH & "TBII"
            Else
                sAH = sAH & "TBIU"
            End If
        Else
            If bDomestic Then
                sAH = sAH & "TBRI"
            Else
                sAH = sAH & "TBRU"
            End If
        End If
        If Trim(sTransDate) = "" Then
            sAH = sAH & VB6.Format(Now, "MMDD")
        Else
            sAH = sAH & sTransDate
        End If
        'FIX: Enter Sequenceno. per day
        sAH = sAH & PadLeft(Str(nSequenceNo), 6, "0")
        sAH = sAH & Space(8)
        'FIX: Enter User_ID
        sAH = sAH & Space(11)
        sAH = sAH & "04"
        'End If
        ApplicationHeader = sAH

    End Function
	
	'UPGRADE_NOTE: WriteLine was upgraded to WriteLine_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Function WriteLine_Renamed(ByRef sLine As String, ByRef nNoOfBETFOR As Object, ByRef nSequenceNoTotal As Object) As Boolean
		oFile.WriteLine(Mid(sLine, 1, 80))
		oFile.WriteLine(Mid(sLine, 81, 80))
		oFile.WriteLine(Mid(sLine, 161, 80))
		oFile.WriteLine(Mid(sLine, 241, 80))
		
		
		'UPGRADE_WARNING: Couldn't resolve default property of object nNoOfBETFOR. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		nNoOfBETFOR = nNoOfBETFOR + 1
		'UPGRADE_WARNING: Couldn't resolve default property of object nSequenceNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		nSequenceNoTotal = nSequenceNoTotal + 1
		'UPGRADE_WARNING: Couldn't resolve default property of object nSequenceNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If nSequenceNoTotal > 9999 Then
			'UPGRADE_WARNING: Couldn't resolve default property of object nSequenceNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			nSequenceNoTotal = 0
		End If
		
		
	End Function

    Function WriteMaritechCSV(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sClientNo As String, ByVal iVB_FilenameInNo As Integer, ByVal sSpecial As String) As Boolean
        ' 02.01.2017
        ' New format for Incoming payments to Maritech.
        ' First used by Sekkingstad/Skagerak
        ' NOT IN USE! 

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim i, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim iFreetextCounter As Short
        Dim sFreetextFixed, sFreeTextVariable As String
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sLineToWrite As String
        'Dim bFirstBatch As Boolean
        Dim nSequenceNoTotal As Double
        Dim bGLBankWritten As Boolean
        Dim sVoucherNo As String
        Dim sAlphaPart As String
        Dim nDigitsLen As Short
        Dim sGLAccount As String
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        'Dim sFileNameTelepay As String = String.Empty
        '03.03.2016 Added next 2 variables
        Dim oBackup As vbBabel.BabelFileHandling
        Dim bRecordWritten As Boolean = False
        Dim sCompanyNo As String = String.Empty

        Try
            ' lag en outputfil
            bAppendFile = False
            bGLBankWritten = False
            sOldAccountNo = ""

            'Doesn't matter if a file exist. If so, append

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If Not bMultiFiles Then
                            bExportoBabel = True
                        Else
                            If oPayment.I_Account = sI_Account Then
                                If Len(oPayment.VB_ClientNo) > 0 Then
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        bExportoBabel = True
                                    End If
                                Else
                                    bExportoBabel = True
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'New code 30.10.2002 - Regarding FalseOCR, and avtalegiro
                            If Not bMultiFiles Then
                                bExportoBatch = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            bExportoBatch = True
                                        End If
                                    Else
                                        bExportoBatch = True
                                    End If
                                End If
                            End If
                        Next oPayment

                        If bExportoBatch Then
                            bGLBankWritten = False
                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If Not bMultiFiles Then
                                    bExportoPayment = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoPayment = True
                                            End If
                                        Else
                                            bExportoPayment = True
                                        End If
                                    End If
                                End If
                                If bExportoPayment Then
                                    'Find the client
                                    If oPayment.I_Account <> sOldAccountNo Then
                                        If oPayment.VB_ProfileInUse Then
                                            sI_Account = oPayment.I_Account
                                            bAccountFound = False
                                            For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                                For Each oClient In oFilesetup.Clients
                                                    For Each oaccount In oClient.Accounts
                                                        If sI_Account = oaccount.Account Then
                                                            sCompanyNo = oClient.CompNo
                                                            sGLAccount = Trim(oaccount.GLAccount)
                                                            sOldAccountNo = oPayment.I_Account
                                                            bAccountFound = True

                                                            Exit For
                                                        End If
                                                    Next oaccount
                                                    If bAccountFound Then Exit For
                                                Next oClient
                                                If bAccountFound Then Exit For
                                            Next oFilesetup
                                        End If
                                    End If

                                    If Not bAccountFound Then
                                        Err.Raise(12003, "WriteBabelBank_InnbetalingsFile", LRS(12003))
                                        '12003: Could not find account %1.Please enter the account in setup."
                                    End If

                                    bRecordWritten = True

                                    'Write GL-line if not previously written
                                    If Not bGLBankWritten Then
                                        'Don't write 0-amounts, creates errors when imported to Visma
                                        If oBatch.MON_InvoiceAmount <> 0 Then
                                            sLineToWrite = WriteMaritechCSV_Record("B", sGLAccount, oBatch, oPayment, oInvoice, oClient, oPayment.E_Name, sSpecial)
                                            oFile.WriteLine(sLineToWrite)
                                        End If
                                        bGLBankWritten = True
                                    End If

                                    'First we have to find the freetext to the payment-lines.
                                    iFreetextCounter = 0
                                    sFreetextFixed = ""
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Original = True Then
                                            For Each oFreeText In oInvoice.Freetexts
                                                iFreetextCounter = iFreetextCounter + 1
                                                If iFreetextCounter = 1 Then
                                                    sFreetextFixed = RTrim(oFreeText.Text)
                                                ElseIf iFreetextCounter = 2 Then
                                                    sFreetextFixed = sFreetextFixed & " " & RTrim(oFreeText.Text)
                                                Else
                                                    Exit For
                                                End If
                                            Next oFreeText
                                        End If
                                    Next oInvoice
                                    sFreetextFixed = sFreetextFixed & " " & Trim(oPayment.E_Name) & " "

                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Final = True And oInvoice.MON_InvoiceAmount <> 0 Then
                                            'Add the variable part of the freetext
                                            sFreeTextVariable = sFreetextFixed
                                            For Each oFreeText In oInvoice.Freetexts
                                                If oFreeText.Qualifier > 2 Then
                                                    sFreeTextVariable = RTrim(oFreeText.Text) & " " & sFreeTextVariable
                                                End If
                                            Next oFreeText


                                            If oInvoice.MATCH_Matched Then
                                                If oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnInvoice Then
                                                    '12.12.2016 - Added next if
                                                    If Not IsEqualAmount(oInvoice.MON_InvoiceAmount, 0) Then
                                                        sLineToWrite = WriteMaritechCSV_Record("I", oInvoice.CustomerNo, oBatch, oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial)
                                                    End If
                                                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.OpenInvoice Then
                                                    sLineToWrite = WriteMaritechCSV_Record("", oInvoice.CustomerNo, oBatch, oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial)
                                                ElseIf oInvoice.MATCH_MatchType = BabelFiles.MatchType.MatchedOnGL Then
                                                    sLineToWrite = WriteMaritechCSV_Record("", oInvoice.MATCH_ID, oBatch, oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial)
                                                Else
                                                    sLineToWrite = WriteMaritechCSV_Record("", oInvoice.CustomerNo, oBatch, oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial)
                                                End If
                                            Else
                                                sLineToWrite = WriteMaritechCSV_Record("", oInvoice.MATCH_ID, oBatch, oPayment, oInvoice, oClient, sFreeTextVariable, sSpecial)
                                            End If

                                            oFile.WriteLine(sLineToWrite)

                                        End If 'oInvoice.MATCH_Final = True
                                    Next oInvoice
                                    oPayment.Exported = True
                                End If 'bExportoPayment
                            Next oPayment ' payment


                        End If

                        'We have to save the last used sequenceno. in some cases
                        'First check if we use a profile
                        If oBatch.VB_ProfileInUse = True Then
                            Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                'Always use the seqno at filesetup level
                                Case 1
                                    'Only for the first batch if we have one sequenceseries!
                                    oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal
                                    oBatch.VB_Profile.Status = 2
                                    oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 1
                                Case 2
                                    sVoucherNo = sAlphaPart & PadLeft(Trim(Str(nSequenceNoTotal)), nDigitsLen, "0")
                                    oClient.VoucherNo = sVoucherNo
                                    oClient.Status = Profile.CollectionStatus.Changed
                                    oBatch.VB_Profile.Status = Profile.CollectionStatus.ChangeUnder
                                    oBatch.VB_Profile.FileSetups(iFormat_ID).Status = Profile.CollectionStatus.ChangeUnder

                                Case Else
                                    Err.Raise(12002, "WriteMaritechCSV", LRS(12002, "Maritech", sFilenameOut))

                            End Select

                        End If

                    Next oBatch 'batch

                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteMaritechCSV" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteMaritechCSV = True

    End Function
    Private Function WriteMaritechCSV_Record(ByRef sType As String, ByRef sAccount As String, ByRef oBatch As vbBabel.Batch, ByRef oPayment As vbBabel.Payment, ByRef oInvoice As vbBabel.Invoice, ByRef oClient As vbBabel.Client, ByRef sFreetext As String, ByRef sSpecial As String) As String
        Dim sLineToWrite As String
        Dim sTemp As String = ""

        '1 Periodedato
        '2 Bilagsdato
        '3 Profilnr
        '4 Tekstkode
        '5 Bilagstekst
        '6 Kontonr(Debet)
        '7 Kontonr(kredit)
        '8 Mvakode
        '9 Valutabel�p
        '10 NOKbel�p
        '11 Valutakode
        '12 Valutakurs
        '13 Forfallsdato
        '14 KID


        sLineToWrite = ""
        ' 1 PeriodeDato
        sLineToWrite = sLineToWrite & Format(StringToDate(oPayment.DATE_Payment), "dd.MM.yyyy") & ";"
        ' 2 Dato
        sLineToWrite = sLineToWrite & Format(StringToDate(oPayment.DATE_Payment), "dd.MM.yyyy") & ";"
        ' 3 Profilnr
        sLineToWrite = sLineToWrite & "22" & ";"
        ' 4 Tekstkode
        sLineToWrite = sLineToWrite & "22" & ";"
        ' 5 Bilagstekst
        sLineToWrite = sLineToWrite & Left(sFreetext, 60) & ";"
        ' 6 Kontonr(Debet) / 7 Kontonr (kredit)
        If sType = "B" Then
            ' Bankrecord
            If oPayment.MON_InvoiceAmount < 0 Then
                sLineToWrite = sLineToWrite & "" & ";"
                sLineToWrite = sLineToWrite & sAccount & ";"
            Else
                sLineToWrite = sLineToWrite & sAccount & ";"
                sLineToWrite = sLineToWrite & "" & ";"
            End If
        Else
            If oInvoice.MON_InvoiceAmount < 0 Then
                sLineToWrite = sLineToWrite & sAccount & ";"
                sLineToWrite = sLineToWrite & "" & ";"
            Else
                sLineToWrite = sLineToWrite & "" & ";"
                sLineToWrite = sLineToWrite & sAccount & ";"
            End If
        End If
        ' 8 Mvakode
        If sType = "B" Then
            ' bank GL, value = 0
            sLineToWrite = sLineToWrite & "0" & ";"
            ' 9 Valutabel�p
            sLineToWrite = sLineToWrite & Trim(Replace(CStr(oPayment.MON_InvoiceAmount / 100), "-", "")) & ";" 'Amount
        Else
            ' otherwise, value = 4
            sLineToWrite = sLineToWrite & "4" & ";"
            ' 9 Valutabel�p
            sLineToWrite = sLineToWrite & Trim(Replace(CStr(oInvoice.MON_InvoiceAmount / 100), "-", "")) & ";" 'Amount
        End If

        ' 10 NOKbel�p
        ' Have no NOK-amount
        sLineToWrite = sLineToWrite & "0" & ";"

        '11 Valutakode
        sLineToWrite = sLineToWrite & oPayment.MON_InvoiceCurrency & ";"

        '12 Valutakurs
        sTemp = Replace(Trim$(Str$(oPayment.MON_LocalExchRate)), ",", ".")
        If InStr(1, sTemp, ".", vbTextCompare) > 0 Then
            sTemp = Left$(sTemp, InStr(1, sTemp, ".", vbTextCompare) - 1) & PadRight(Mid$(sTemp, InStr(1, sTemp, ".", vbTextCompare)), 9, "0")
        Else
            sTemp = sTemp & ".00000000"
        End If
        sLineToWrite = sLineToWrite & sTemp & ";" ' Exch.rate against basevaluta

        '13 Forfallsdato
        If sType = "B" Then
            sLineToWrite = sLineToWrite & "" & ";"
        Else
            sLineToWrite = sLineToWrite & oInvoice.MyField & ";"
        End If

        '14 KID
        If sType = "I" Then
            '  Invoicerecord
            ' KID = clientno + invoiceno + control (?)
            sTemp = oClient.ClientNo & oInvoice.InvoiceNo
            ' add control
            sTemp = sTemp & AddCDV10digit(sTemp)
            sLineToWrite = sLineToWrite & sTemp    '& oInvoice.MATCH_ID
        End If

        WriteMaritechCSV_Record = sLineToWrite

    End Function
End Module
