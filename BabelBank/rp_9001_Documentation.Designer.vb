<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_9001_Documentation
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rp_9001_Documentation))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.Line3 = New DataDynamics.ActiveReports.Line
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grBabelFileHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.grBabelFileFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.grBatchHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapeBatchHeader = New DataDynamics.ActiveReports.Shape
        Me.grBatchFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.TotalSum = New DataDynamics.ActiveReports.Field
        Me.txtLabel = New DataDynamics.ActiveReports.TextBox
        Me.txtBody = New DataDynamics.ActiveReports.TextBox
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLabel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBody, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtLabel, Me.txtBody})
        Me.Detail.Height = 2.84375!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.Line3, Me.rptInfoPageHeaderDate})
        Me.PageHeader.Height = 0.4576389!
        Me.PageHeader.Name = "PageHeader"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Border.BottomColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Border.LeftColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Border.RightColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Border.TopColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 1.279861!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "text-align: center; font-size: 18pt; "
        Me.lblrptHeader.Text = "BabelBank documentation"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'Line3
        '
        Me.Line3.Border.BottomColor = System.Drawing.Color.Black
        Me.Line3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.LeftColor = System.Drawing.Color.Black
        Me.Line3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.RightColor = System.Drawing.Color.Black
        Me.Line3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.TopColor = System.Drawing.Color.Black
        Me.Line3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Height = 0.0!
        Me.Line3.Left = 0.0!
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.39375!
        Me.Line3.Width = 6.299305!
        Me.Line3.X1 = 0.0!
        Me.Line3.X2 = 6.299305!
        Me.Line3.Y1 = 0.39375!
        Me.Line3.Y2 = 0.39375!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right; "
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter.Height = 0.2604167!
        Me.PageFooter.Name = "PageFooter"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right; "
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right; "
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grBabelFileHeader
        '
        Me.grBabelFileHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grBabelFileHeader.Height = 0.0625!
        Me.grBabelFileHeader.Name = "grBabelFileHeader"
        Me.grBabelFileHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'grBabelFileFooter
        '
        Me.grBabelFileFooter.CanShrink = True
        Me.grBabelFileFooter.Height = 0.04166667!
        Me.grBabelFileFooter.Name = "grBabelFileFooter"
        '
        'grBatchHeader
        '
        Me.grBatchHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapeBatchHeader})
        Me.grBatchHeader.DataField = "BreakField"
        Me.grBatchHeader.Height = 0.6340278!
        Me.grBatchHeader.Name = "grBatchHeader"
        Me.grBatchHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'shapeBatchHeader
        '
        Me.shapeBatchHeader.Border.BottomColor = System.Drawing.Color.Black
        Me.shapeBatchHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapeBatchHeader.Border.LeftColor = System.Drawing.Color.Black
        Me.shapeBatchHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapeBatchHeader.Border.RightColor = System.Drawing.Color.Black
        Me.shapeBatchHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapeBatchHeader.Border.TopColor = System.Drawing.Color.Black
        Me.shapeBatchHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapeBatchHeader.Height = 0.5!
        Me.shapeBatchHeader.Left = 0.0!
        Me.shapeBatchHeader.Name = "shapeBatchHeader"
        Me.shapeBatchHeader.RoundingRadius = 9.999999!
        Me.shapeBatchHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapeBatchHeader.Top = 0.0!
        Me.shapeBatchHeader.Width = 6.45!
        '
        'grBatchFooter
        '
        Me.grBatchFooter.CanShrink = True
        Me.grBatchFooter.Height = 0.0625!
        Me.grBatchFooter.Name = "grBatchFooter"
        '
        'TotalSum
        '
        Me.TotalSum.DefaultValue = "100"
        Me.TotalSum.FieldType = DataDynamics.ActiveReports.FieldTypeEnum.None
        Me.TotalSum.Formula = "3+3"
        Me.TotalSum.Name = "TotalSum"
        Me.TotalSum.Tag = Nothing
        '
        'txtLabel
        '
        Me.txtLabel.Border.BottomColor = System.Drawing.Color.Black
        Me.txtLabel.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtLabel.Border.LeftColor = System.Drawing.Color.Black
        Me.txtLabel.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtLabel.Border.RightColor = System.Drawing.Color.Black
        Me.txtLabel.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtLabel.Border.TopColor = System.Drawing.Color.Black
        Me.txtLabel.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtLabel.Height = 0.25!
        Me.txtLabel.Left = 0.6875!
        Me.txtLabel.Name = "txtLabel"
        Me.txtLabel.Style = ""
        Me.txtLabel.Text = "txtLabel"
        Me.txtLabel.Top = 0.3125!
        Me.txtLabel.Width = 1.1875!
        '
        'txtBody
        '
        Me.txtBody.Border.BottomColor = System.Drawing.Color.Black
        Me.txtBody.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtBody.Border.LeftColor = System.Drawing.Color.Black
        Me.txtBody.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtBody.Border.RightColor = System.Drawing.Color.Black
        Me.txtBody.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtBody.Border.TopColor = System.Drawing.Color.Black
        Me.txtBody.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtBody.Height = 0.25!
        Me.txtBody.Left = 2.1875!
        Me.txtBody.Name = "txtBody"
        Me.txtBody.Style = ""
        Me.txtBody.Text = "txtBody"
        Me.txtBody.Top = 0.3125!
        Me.txtBody.Width = 3.75!
        '
        'rp_9001_Documentation
        '
        Me.MasterReport = False
        Me.CalculatedFields.Add(Me.TotalSum)
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 6.489583!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.grBabelFileHeader)
        Me.Sections.Add(Me.grBatchHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grBatchFooter)
        Me.Sections.Add(Me.grBabelFileFooter)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'Times New Roman'; font-style: italic; font-variant: inherit; font-w" & _
                    "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; " & _
                    "", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("", "Heading4", "Normal"))
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLabel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBody, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader
    Friend WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Friend WithEvents Line3 As DataDynamics.ActiveReports.Line
    Friend WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter
    Friend WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents grBabelFileHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents grBabelFileFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents grBatchHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents shapeBatchHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents grBatchFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents TotalSum As DataDynamics.ActiveReports.Field
    Friend WithEvents txtLabel As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtBody As DataDynamics.ActiveReports.TextBox
End Class
