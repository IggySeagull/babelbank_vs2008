Option Strict Off
Option Explicit On
Module WritePostbOCROld
	
	'Dim oFs As Object
	'Dim oFile As Object
	'Dim sLine As String ' en output-linje
	'Dim i As Double
	'Dim oBatch As Batch
	'' Vi m� dimme noen variable for
	'' - batchniv�: sum bel�p, antall records, antall transer, f�rste dato, siste dato
	Dim nSumAmount, nNoOfTransactions As Double
	'Dim dBatchLastDate As Date, dBatchFirstDate As Date
	'' - fileniv�: sum bel�p, antall records, antall transer, siste dato
	'Dim cFileSumAmount As Currency, nFileNoTransactions As Double
	'Dim dFileFirstDate As Date
	
    Function WritePostbOCROldFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        'Dim i As Double, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        'Dim op As Payment
        'Dim nTransactionNo As Double
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim nExistingLines As Double
        Dim sTempFilename As String
        Dim iCount As Short
        Dim bFoundRecord3 As Boolean
        Dim oFileWrite, oFileRead As Scripting.TextStream
        Dim bAppendFile As Boolean
        Dim dDate As Date
        Dim sIAccountNo As String
        '
        '

        Try

            bAppendFile = False

            'Check if we are appending to an existing file.
            ' If so BB has to delete the last 89-record, and save the counters

            nSumAmount = 0
            nNoOfTransactions = 0

            oFs = New Scripting.FileSystemObject

            If oFs.FileExists(sFilenameOut) Then
                bAppendFile = True
                bFoundRecord3 = False
                nExistingLines = 0
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTempFilename = Left(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\BBFile.tmp"
                'First count the number of lines in the existing file
                Do While Not oFile.AtEndOfStream = True
                    nExistingLines = nExistingLines + 1
                    oFile.SkipLine()
                Loop
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'Then check if one of the 2 last lines are a 3-line
                ' Store the number of lines until the 3-line
                Do Until oFile.Line = nExistingLines - 1
                    oFile.SkipLine()
                Loop
                For iCount = 1 To 0 Step -1
                    sLine = oFile.ReadLine()
                    If Len(sLine) > 8 Then
                        If Left(sLine, 1) = "3" Then
                            nExistingLines = nExistingLines - iCount
                            bFoundRecord3 = True
                            Exit For
                        End If
                    End If
                Next iCount
                oFile.Close()
                'UPGRADE_NOTE: Object oFile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                oFile = Nothing
                If Not bFoundRecord3 Then
                    'FIX: Babelerror Noe feil med fila som ligger der fra f�r.
                    MsgBox("The existing file is not an OCR-file")
                    WritePostbOCROldFile = False
                    Exit Function
                    'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFs = Nothing
                Else
                    'Copy existing file to a temporary file and delete existing file
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFs.CopyFile(sFilenameOut, sTempFilename, True)
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFs.DeleteFile(sFilenameOut)
                    'Copy the content of the temporary file to the new file,
                    ' excluding the 89-record
                    oFileRead = oFs.OpenTextFile(sTempFilename, Scripting.IOMode.ForReading, False, 0)
                    'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    oFileWrite = oFs.CreateTextFile(sFilenameOut, True, 0)
                    Do Until oFileRead.Line > nExistingLines - 1
                        sLine = oFileRead.ReadLine
                        oFileWrite.WriteLine((sLine))
                    Loop
                    'Store the neccesary counters and totalamount
                    sLine = oFileRead.ReadLine
                    nNoOfTransactions = Val(Mid(sLine, 45, 7))
                    nSumAmount = Val(Mid(sLine, 34, 11))
                    'Kill'em all
                    oFileRead.Close()
                    'UPGRADE_NOTE: Object oFileRead may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFileRead = Nothing
                    oFileWrite.Close()
                    'UPGRADE_NOTE: Object oFileWrite may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oFileWrite = Nothing
                    oFs.DeleteFile(sTempFilename)
                End If
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object sFilenameOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    dDate = StringToDate((oPayment.DATE_Payment))
                                    sIAccountNo = oPayment.I_Account
                                    sLine = WriteTransaction(oBatch, oPayment)
                                    oFile.WriteLine((sLine))
                                    oPayment.Exported = True
                                End If

                            Next oPayment ' payment

                            '                sLine = Wr_OCRBatchEnd(oBatch)
                            '                oFile.WriteLine (sLine)

                        End If
                    Next oBatch 'batch

                    sLine = WriteFileEnd(dDate, sIAccountNo)
                    oFile.WriteLine((sLine))
                    nSumAmount = 0
                    nNoOfTransactions = 0

                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WritePostbOCROldFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WritePostbOCROldFile = True

    End Function
	Public Function WriteTransaction(ByRef oBatch As Batch, ByRef oPayment As Payment) As String
		
		Dim sLine As String
		
		' Add to Batch-totals:
		nSumAmount = nSumAmount + oPayment.MON_TransferredAmount
		nNoOfTransactions = nNoOfTransactions + 1
		
		sLine = "2"
		If StringToDate((oPayment.DATE_Payment)) > (System.Date.FromOADate(Now.ToOADate - 1000)) Then
			' hvis vi har lagret en dato, bruk denne
            sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Payment)), "ddmmyy")
        Else
            ' vi har ikke lagret dato, sett inn dagens dato
            sLine = sLine & VB6.Format(Now, "ddmmyy")
        End If
        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sLine = sLine & PadRight(Mid(oBatch.Payments(1).I_Account, 1, 11), 11, " ")
        'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If Len(Trim(oPayment.Invoices(1).Unique_Id)) > 16 Then
            'FIX: Babelerror. Maxlength in this format is 16
            'MsgBox "Too long KID"
            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sLine = sLine & PadLeft(oPayment.Invoices(1).Unique_Id, 16, "0") ' KID
        Else
            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sLine = sLine & PadLeft(oPayment.Invoices(1).Unique_Id, 16, "0") ' KID
        End If
        sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 11, "0") ' Amount
        sLine = sLine & "000000"

        WriteTransaction = sLine

    End Function

    Public Function WriteFileEnd(ByRef dDate As Date, ByRef sIAccountNo As String) As String

        Dim sLine As String

        sLine = "3"
        If dDate > (System.Date.FromOADate(Now.ToOADate - 1000)) Then
            ' hvis vi har lagret en dato, bruk denne
            sLine = sLine & VB6.Format(dDate, "ddmmyy")
        Else
            ' vi har ikke lagret dato, sett inn dagens dato
            sLine = sLine & VB6.Format(Now, "ddmmyy")
        End If
        sLine = sLine & PadRight(Mid(sIAccountNo, 1, 11), 11, " ")
        sLine = sLine & Space(15)
        sLine = sLine & PadLeft(CStr(nSumAmount), 12, "0") ' TotalAmount
        sLine = sLine & PadLeft(Str(nNoOfTransactions), 6, "0") ' TotalNoOfTransactions

        WriteFileEnd = sLine

    End Function
End Module
