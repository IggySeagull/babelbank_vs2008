<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmCorrectNameAddress
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtCity As System.Windows.Forms.TextBox
	Public WithEvents txtName As System.Windows.Forms.TextBox
	Public WithEvents txtZIP As System.Windows.Forms.TextBox
	Public WithEvents txtAdr3 As System.Windows.Forms.TextBox
	Public WithEvents txtAdr2 As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents txtAdr1 As System.Windows.Forms.TextBox
	Public WithEvents lblZIP As System.Windows.Forms.Label
	Public WithEvents lblCity As System.Windows.Forms.Label
    Public WithEvents lblAdr3 As System.Windows.Forms.Label
	Public WithEvents lblAdr2 As System.Windows.Forms.Label
	Public WithEvents lblName As System.Windows.Forms.Label
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents lblAdr1 As System.Windows.Forms.Label
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtCity = New System.Windows.Forms.TextBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtZIP = New System.Windows.Forms.TextBox
        Me.txtAdr3 = New System.Windows.Forms.TextBox
        Me.txtAdr2 = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtAdr1 = New System.Windows.Forms.TextBox
        Me.lblZIP = New System.Windows.Forms.Label
        Me.lblCity = New System.Windows.Forms.Label
        Me.lblAdr3 = New System.Windows.Forms.Label
        Me.lblAdr2 = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblAdr1 = New System.Windows.Forms.Label
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtCity
        '
        Me.txtCity.AcceptsReturn = True
        Me.txtCity.BackColor = System.Drawing.SystemColors.Window
        Me.txtCity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCity.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCity.Location = New System.Drawing.Point(146, 193)
        Me.txtCity.MaxLength = 0
        Me.txtCity.Name = "txtCity"
        Me.txtCity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCity.Size = New System.Drawing.Size(202, 19)
        Me.txtCity.TabIndex = 5
        '
        'txtName
        '
        Me.txtName.AcceptsReturn = True
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtName.Location = New System.Drawing.Point(146, 79)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtName.Size = New System.Drawing.Size(202, 19)
        Me.txtName.TabIndex = 0
        '
        'txtZIP
        '
        Me.txtZIP.AcceptsReturn = True
        Me.txtZIP.BackColor = System.Drawing.SystemColors.Window
        Me.txtZIP.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtZIP.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtZIP.Location = New System.Drawing.Point(146, 171)
        Me.txtZIP.MaxLength = 0
        Me.txtZIP.Name = "txtZIP"
        Me.txtZIP.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtZIP.Size = New System.Drawing.Size(54, 19)
        Me.txtZIP.TabIndex = 4
        '
        'txtAdr3
        '
        Me.txtAdr3.AcceptsReturn = True
        Me.txtAdr3.BackColor = System.Drawing.SystemColors.Window
        Me.txtAdr3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAdr3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAdr3.Location = New System.Drawing.Point(146, 148)
        Me.txtAdr3.MaxLength = 0
        Me.txtAdr3.Name = "txtAdr3"
        Me.txtAdr3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAdr3.Size = New System.Drawing.Size(202, 19)
        Me.txtAdr3.TabIndex = 3
        '
        'txtAdr2
        '
        Me.txtAdr2.AcceptsReturn = True
        Me.txtAdr2.BackColor = System.Drawing.SystemColors.Window
        Me.txtAdr2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAdr2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAdr2.Location = New System.Drawing.Point(146, 125)
        Me.txtAdr2.MaxLength = 0
        Me.txtAdr2.Name = "txtAdr2"
        Me.txtAdr2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAdr2.Size = New System.Drawing.Size(202, 19)
        Me.txtAdr2.TabIndex = 2
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(269, 231)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(80, 25)
        Me.cmdOK.TabIndex = 6
        Me.cmdOK.TabStop = False
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'txtAdr1
        '
        Me.txtAdr1.AcceptsReturn = True
        Me.txtAdr1.BackColor = System.Drawing.SystemColors.Window
        Me.txtAdr1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAdr1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAdr1.Location = New System.Drawing.Point(146, 102)
        Me.txtAdr1.MaxLength = 0
        Me.txtAdr1.Name = "txtAdr1"
        Me.txtAdr1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAdr1.Size = New System.Drawing.Size(202, 19)
        Me.txtAdr1.TabIndex = 1
        '
        'lblZIP
        '
        Me.lblZIP.BackColor = System.Drawing.SystemColors.Control
        Me.lblZIP.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblZIP.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblZIP.Location = New System.Drawing.Point(8, 173)
        Me.lblZIP.Name = "lblZIP"
        Me.lblZIP.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblZIP.Size = New System.Drawing.Size(116, 16)
        Me.lblZIP.TabIndex = 12
        Me.lblZIP.Text = "60097 - Postnr"
        '
        'lblCity
        '
        Me.lblCity.BackColor = System.Drawing.SystemColors.Control
        Me.lblCity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCity.Location = New System.Drawing.Point(8, 196)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCity.Size = New System.Drawing.Size(120, 16)
        Me.lblCity.TabIndex = 11
        Me.lblCity.Text = "60098 - Poststed"
        '
        'lblAdr3
        '
        Me.lblAdr3.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdr3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdr3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdr3.Location = New System.Drawing.Point(8, 150)
        Me.lblAdr3.Name = "lblAdr3"
        Me.lblAdr3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdr3.Size = New System.Drawing.Size(120, 16)
        Me.lblAdr3.TabIndex = 10
        Me.lblAdr3.Text = "60146 - Adresse"
        '
        'lblAdr2
        '
        Me.lblAdr2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdr2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdr2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdr2.Location = New System.Drawing.Point(8, 126)
        Me.lblAdr2.Name = "lblAdr2"
        Me.lblAdr2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdr2.Size = New System.Drawing.Size(116, 16)
        Me.lblAdr2.TabIndex = 9
        Me.lblAdr2.Text = "60146 - Adresse"
        '
        'lblName
        '
        Me.lblName.BackColor = System.Drawing.SystemColors.Control
        Me.lblName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblName.Location = New System.Drawing.Point(8, 79)
        Me.lblName.Name = "lblName"
        Me.lblName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblName.Size = New System.Drawing.Size(117, 16)
        Me.lblName.TabIndex = 8
        Me.lblName.Text = "60086 - Navn"
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(0, 3)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(32, 27)
        Me._Image1_0.TabIndex = 13
        Me._Image1_0.TabStop = False
        '
        'lblAdr1
        '
        Me.lblAdr1.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdr1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdr1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdr1.Location = New System.Drawing.Point(8, 103)
        Me.lblAdr1.Name = "lblAdr1"
        Me.lblAdr1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdr1.Size = New System.Drawing.Size(118, 16)
        Me.lblAdr1.TabIndex = 7
        Me.lblAdr1.Text = "60146 - Adresse"
        '
        'frmCorrectNameAddress
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(363, 263)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtCity)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.txtZIP)
        Me.Controls.Add(Me.txtAdr3)
        Me.Controls.Add(Me.txtAdr2)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtAdr1)
        Me.Controls.Add(Me.lblZIP)
        Me.Controls.Add(Me.lblCity)
        Me.Controls.Add(Me.lblAdr3)
        Me.Controls.Add(Me.lblAdr2)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblAdr1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmCorrectNameAddress"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "60086 - Navn"
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class
