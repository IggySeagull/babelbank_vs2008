﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewPayment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gridPayment = New System.Windows.Forms.DataGridView
        Me.OK_Button = New System.Windows.Forms.Button
        Me.cmdPrint = New System.Windows.Forms.Button
        Me.cmdReport = New System.Windows.Forms.Button
        Me.cmdAddColumn = New System.Windows.Forms.Button
        Me.cmdRemoveColumn = New System.Windows.Forms.Button
        Me.cmdSaveSetup = New System.Windows.Forms.Button
        Me.gridInvoice = New System.Windows.Forms.DataGridView
        Me.cmdSearch = New System.Windows.Forms.Button
        CType(Me.gridPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridInvoice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gridPayment
        '
        Me.gridPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridPayment.Location = New System.Drawing.Point(12, 27)
        Me.gridPayment.Name = "gridPayment"
        Me.gridPayment.Size = New System.Drawing.Size(938, 343)
        Me.gridPayment.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(885, 506)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 2
        Me.OK_Button.Text = "55032-Lukk"
        '
        'cmdPrint
        '
        Me.cmdPrint.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdPrint.Location = New System.Drawing.Point(814, 506)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(67, 23)
        Me.cmdPrint.TabIndex = 3
        Me.cmdPrint.Text = "55003-Print"
        '
        'cmdReport
        '
        Me.cmdReport.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdReport.Location = New System.Drawing.Point(741, 506)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.Size = New System.Drawing.Size(67, 23)
        Me.cmdReport.TabIndex = 6
        Me.cmdReport.Text = "40026 Rapport"
        '
        'cmdAddColumn
        '
        Me.cmdAddColumn.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdAddColumn.Location = New System.Drawing.Point(12, 509)
        Me.cmdAddColumn.Name = "cmdAddColumn"
        Me.cmdAddColumn.Size = New System.Drawing.Size(116, 23)
        Me.cmdAddColumn.TabIndex = 7
        Me.cmdAddColumn.Text = "00000 - Legg til kolonne"
        Me.cmdAddColumn.Visible = False
        '
        'cmdRemoveColumn
        '
        Me.cmdRemoveColumn.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdRemoveColumn.Location = New System.Drawing.Point(134, 509)
        Me.cmdRemoveColumn.Name = "cmdRemoveColumn"
        Me.cmdRemoveColumn.Size = New System.Drawing.Size(116, 23)
        Me.cmdRemoveColumn.TabIndex = 8
        Me.cmdRemoveColumn.Text = "00000 - Fjern kolonne"
        Me.cmdRemoveColumn.Visible = False
        '
        'cmdSaveSetup
        '
        Me.cmdSaveSetup.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdSaveSetup.Location = New System.Drawing.Point(256, 509)
        Me.cmdSaveSetup.Name = "cmdSaveSetup"
        Me.cmdSaveSetup.Size = New System.Drawing.Size(116, 23)
        Me.cmdSaveSetup.TabIndex = 9
        Me.cmdSaveSetup.Text = "00000 - Lagre oppsett"
        Me.cmdSaveSetup.Visible = False
        '
        'gridInvoice
        '
        Me.gridInvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridInvoice.Location = New System.Drawing.Point(12, 377)
        Me.gridInvoice.Name = "gridInvoice"
        Me.gridInvoice.Size = New System.Drawing.Size(938, 113)
        Me.gridInvoice.TabIndex = 10
        '
        'cmdSearch
        '
        Me.cmdSearch.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdSearch.Location = New System.Drawing.Point(668, 506)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(67, 23)
        Me.cmdSearch.TabIndex = 11
        Me.cmdSearch.Text = "55031-Søk"
        '
        'frmViewPayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(962, 539)
        Me.Controls.Add(Me.cmdSearch)
        Me.Controls.Add(Me.gridInvoice)
        Me.Controls.Add(Me.cmdSaveSetup)
        Me.Controls.Add(Me.cmdRemoveColumn)
        Me.Controls.Add(Me.cmdAddColumn)
        Me.Controls.Add(Me.cmdReport)
        Me.Controls.Add(Me.cmdPrint)
        Me.Controls.Add(Me.gridPayment)
        Me.Controls.Add(Me.OK_Button)
        Me.Name = "frmViewPayment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Payment"
        CType(Me.gridPayment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridInvoice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gridPayment As System.Windows.Forms.DataGridView
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    Friend WithEvents cmdReport As System.Windows.Forms.Button
    Friend WithEvents cmdAddColumn As System.Windows.Forms.Button
    Friend WithEvents cmdRemoveColumn As System.Windows.Forms.Button
    Friend WithEvents cmdSaveSetup As System.Windows.Forms.Button
    Friend WithEvents gridInvoice As System.Windows.Forms.DataGridView
    Friend WithEvents cmdSearch As System.Windows.Forms.Button
End Class
