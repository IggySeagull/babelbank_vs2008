Option Strict Off
Option Explicit On
Module WriteSEBScreen
	' Common  .bas for several SEBScreen formats
	Dim nFileSumAmount As Double
	Dim nFileNoRecords As Short
	
	'All formats are fullformats
	Dim bInternalTransfersExists As Boolean
	Dim bInternationalPaymentsExists As Boolean
	Dim bAdviceToReceiveExists As Boolean
	Dim bSwedishBGCPaymentsExists As Boolean
	Dim bUeberwissungExists As Boolean
	Dim bUeberwissungSammlerExists As Boolean
	Dim bBACSExists As Boolean
	Dim bCHAPSExists As Boolean
	Dim bChipsAndFedsExists As Boolean
	Dim bPMJFinlandExists As Boolean
	Dim bFrenchVirementsExists As Boolean
	Dim bFrenchPrelevementsExists As Boolean
	Dim bFrenchLCRExists As Boolean
	Dim bDanishDomesticExists As Boolean
	Dim bAccountInOtherBankExists As Boolean
	
	
	Function WriteSEBScreenFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef iFilenameInNo As Short, ByRef sCompanyNo As String, ByRef sBranch As String, ByRef sVersion As String, ByRef nSequenceNumberStart As Double, ByRef nSequenceNumberEnd As Double, Optional ByRef bFILE_OverWrite As Boolean = False, Optional ByRef bFILE_WarningIfExists As Boolean = False) As Boolean
		'This function will not write ay files, but will detect which format should be exported on each format
		'  and will then call the other WriteSEBScreen-functions to actually write the file.
        Dim sLine As String ' en output-linje
		Dim oBabel As vbBabel.BabelFile
		Dim oBatch As vbBabel.Batch
		Dim oPayment As vbBabel.Payment
		Dim oInvoice As vbBabel.Invoice
		Dim oFreeText As vbBabel.FreeText
		Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
		Dim bAppendFile As Boolean
		Dim sDate As String
		Dim i As Short
		Dim sFreetext As String
		Dim iInvoiceCounter As Short
		Dim bFoundClient As Boolean
		Dim sOldAccount As String
		Dim oaccount As Account
		Dim oClient As Client
		Dim sSwift As String
		Dim sModifiedFilenameOut As String
		Dim iStartPosFilename As Short
		Dim iPosForFilenameSuffix As Short
		Dim bx As Boolean
		
		'Set all the differents formats to False
		bInternalTransfersExists = False
		bInternationalPaymentsExists = False
		bAdviceToReceiveExists = False
		bSwedishBGCPaymentsExists = False
		bUeberwissungExists = False
		bUeberwissungSammlerExists = False
		bBACSExists = False
		bCHAPSExists = False
		bChipsAndFedsExists = False
		bPMJFinlandExists = False
		bFrenchVirementsExists = False
		bFrenchPrelevementsExists = False
		bFrenchLCRExists = False
		bDanishDomesticExists = False
		bAccountInOtherBankExists = False
		
        Try

            bAppendFile = False

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    bFoundClient = False
                                    If oPayment.I_Account <> sOldAccount Then
                                        sSwift = ""
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                            For Each oaccount In oClient.Accounts
                                                If oaccount.Account = oPayment.I_Account Or Trim(oaccount.ConvertedAccount) = oPayment.I_Account Then
                                                    sSwift = oaccount.SWIFTAddress
                                                    sOldAccount = oPayment.I_Account
                                                    bFoundClient = True
                                                    Exit For
                                                End If
                                            Next oaccount
                                            If bFoundClient Then
                                                Exit For
                                            End If
                                        Next oClient
                                    End If

                                    If EmptyString(sSwift) Then
                                        Err.Raise(1000, "WriteSEBScreenFile", "No SWIFT addreess stated on the debit account." & vbCrLf & vbCrLf & "Account: " & oPayment.I_Account)
                                    End If

                                    'Add the SWIFTcode to the payment
                                    oPayment.BANK_I_SWIFTCode = sSwift

                                    'Add the PaymentType to the payment
                                    AddSEBScreenPaymentTypeToThePayment(oPayment)


                                End If 'bExporttopayment
                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch

                End If

            Next oBabel


            'After adding the SWIFTCode and the paytype then traverse through the babelfiles collection for
            ' each existing formattype and do the export

            'Create the new FBO filename
            iStartPosFilename = InStrRev(sFilenameOut, "\", , CompareMethod.Text)
            iPosForFilenameSuffix = InStrRev(Mid(sFilenameOut, iStartPosFilename), ".", , CompareMethod.Text)
            If iPosForFilenameSuffix > 1 Then
                sModifiedFilenameOut = Left(sFilenameOut, iStartPosFilename + iPosForFilenameSuffix - 2) & "XXXYYYZZZ" & Mid(sFilenameOut, iStartPosFilename + iPosForFilenameSuffix - 1)
            Else
                sModifiedFilenameOut = Left(sFilenameOut, iStartPosFilename - 1) & "XXXYYYZZZ" & Mid(sFilenameOut, iStartPosFilename)
            End If

            If bInternalTransfersExists Then
                Err.Raise(1001, "WriteSEBScreenFile", "BabelBank is not adapted to SEBScreen internal transfers." & vbCrLf & "Beneficiary's name: " & oPayment.E_Name & vbCrLf & "Beneficiary's account: " & oPayment.E_Account & vbCrLf & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf & "Payer's account: " & oPayment.I_Account & vbCrLf & vbCrLf & "Please contact Your dealer.")
                'bx = DoTheExport(oBabelFiles, bMultiFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_Internal"), iFormat_ID, sI_Account, sOwnRef, "Internal")
            End If
            If bInternationalPaymentsExists Then
                If SEBScreenCheckExportFile(oBabelFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_International"), bFILE_OverWrite, bFILE_WarningIfExists) Then
                    bx = DoTheExport(oBabelFiles, bMultiFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_International"), iFormat_ID, sI_Account, sOwnRef, "International")
                Else
                    WriteSEBScreenFile = False
                    Exit Function
                End If
            End If
            If bAdviceToReceiveExists Then
                Err.Raise(1001, "WriteSEBScreenFile", "BabelBank is not adapted to SEBScreen advice to receive." & vbCrLf & "Beneficiary's name: " & oPayment.E_Name & vbCrLf & "Beneficiary's account: " & oPayment.E_Account & vbCrLf & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf & "Payer's account: " & oPayment.I_Account & vbCrLf & vbCrLf & "Please contact Your dealer.")
                'bx = DoTheExport(oBabelFiles, bMultiFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_Advice"), iFormat_ID, sI_Account, sOwnRef, "Advice")
            End If
            If bSwedishBGCPaymentsExists Then
                If SEBScreenCheckExportFile(oBabelFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_SwedishBGC"), bFILE_OverWrite, bFILE_WarningIfExists) Then
                    bx = DoTheExport(oBabelFiles, bMultiFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_SwedishBGC"), iFormat_ID, sI_Account, sOwnRef, "SwedishBGC")
                Else
                    WriteSEBScreenFile = False
                    Exit Function
                End If
            End If
            If bUeberwissungExists Then
                If SEBScreenCheckExportFile(oBabelFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_Ueberwissung"), bFILE_OverWrite, bFILE_WarningIfExists) Then
                    bx = DoTheExport(oBabelFiles, bMultiFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_Ueberwissung"), iFormat_ID, sI_Account, sOwnRef, "Ueberwissung")
                Else
                    WriteSEBScreenFile = False
                    Exit Function
                End If
            End If
            If bUeberwissungSammlerExists Then
                Err.Raise(1001, "WriteSEBScreenFile", "BabelBank is not adapted to SEBScreen �berwissung sammler." & vbCrLf & "Beneficiary's name: " & oPayment.E_Name & vbCrLf & "Beneficiary's account: " & oPayment.E_Account & vbCrLf & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf & "Payer's account: " & oPayment.I_Account & vbCrLf & vbCrLf & "Please contact Your dealer.")

                'bx = DoTheExport(oBabelFiles, bMultiFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_UeberwissungSammler"), iFormat_ID, sI_Account, sOwnRef, "UeberwissungSammler")
            End If
            If bBACSExists Then
                Err.Raise(1001, "WriteSEBScreenFile", "BabelBank is not adapted to SEBScreen BACS." & vbCrLf & "Beneficiary's name: " & oPayment.E_Name & vbCrLf & "Beneficiary's account: " & oPayment.E_Account & vbCrLf & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf & "Payer's account: " & oPayment.I_Account & vbCrLf & vbCrLf & "Please contact Your dealer.")

            End If
            If bCHAPSExists Then
                Err.Raise(1001, "WriteSEBScreenFile", "BabelBank is not adapted to SEBScreen CHAPS." & vbCrLf & "Beneficiary's name: " & oPayment.E_Name & vbCrLf & "Beneficiary's account: " & oPayment.E_Account & vbCrLf & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf & "Payer's account: " & oPayment.I_Account & vbCrLf & vbCrLf & "Please contact Your dealer.")

            End If
            If bChipsAndFedsExists Then
                If SEBScreenCheckExportFile(oBabelFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_Chips"), bFILE_OverWrite, bFILE_WarningIfExists) Then
                    bx = DoTheExport(oBabelFiles, bMultiFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_Chips"), iFormat_ID, sI_Account, sOwnRef, "Chips")
                Else
                    WriteSEBScreenFile = False
                    Exit Function
                End If
            End If
            If bPMJFinlandExists Then
                Err.Raise(1001, "WriteSEBScreenFile", "BabelBank is not adapted to SEBScreen PMJ Finland." & vbCrLf & "Beneficiary's name: " & oPayment.E_Name & vbCrLf & "Beneficiary's account: " & oPayment.E_Account & vbCrLf & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf & "Payer's account: " & oPayment.I_Account & vbCrLf & vbCrLf & "Please contact Your dealer.")

            End If
            If bFrenchVirementsExists Then
                Err.Raise(1001, "WriteSEBScreenFile", "BabelBank is not adapted to SEBScreen french virements." & vbCrLf & "Beneficiary's name: " & oPayment.E_Name & vbCrLf & "Beneficiary's account: " & oPayment.E_Account & vbCrLf & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf & "Payer's account: " & oPayment.I_Account & vbCrLf & vbCrLf & "Please contact Your dealer.")

            End If
            If bFrenchPrelevementsExists Then
                Err.Raise(1001, "WriteSEBScreenFile", "BabelBank is not adapted to SEBScreen french prelevements." & vbCrLf & "Beneficiary's name: " & oPayment.E_Name & vbCrLf & "Beneficiary's account: " & oPayment.E_Account & vbCrLf & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf & "Payer's account: " & oPayment.I_Account & vbCrLf & vbCrLf & "Please contact Your dealer.")

            End If
            If bFrenchLCRExists Then
                Err.Raise(1001, "WriteSEBScreenFile", "BabelBank is not adapted to SEBScreen french LCR." & vbCrLf & "Beneficiary's name: " & oPayment.E_Name & vbCrLf & "Beneficiary's account: " & oPayment.E_Account & vbCrLf & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf & "Payer's account: " & oPayment.I_Account & vbCrLf & vbCrLf & "Please contact Your dealer.")

            End If
            If bDanishDomesticExists Then
                Err.Raise(1001, "WriteSEBScreenFile", "BabelBank is not adapted to SEBScreen danish domestic." & vbCrLf & "Beneficiary's name: " & oPayment.E_Name & vbCrLf & "Beneficiary's account: " & oPayment.E_Account & vbCrLf & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf & "Payer's account: " & oPayment.I_Account & vbCrLf & vbCrLf & "Please contact Your dealer.")

            End If
            If bAccountInOtherBankExists Then
                If SEBScreenCheckExportFile(oBabelFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_OtherBank"), bFILE_OverWrite, bFILE_WarningIfExists) Then
                    bx = WriteTelepayFile(oBabelFiles, True, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_OtherBank"), iFormat_ID, sI_Account, sOwnRef, iFilenameInNo, sCompanyNo, sBranch, sVersion, False, True, False, nSequenceNumberStart, nSequenceNumberEnd, "9999999", "SEB")
                    ' = WriteTelepay2File(oBabelFiles, bMultiFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_OtherBank"), iFormat_ID, sI_Account, sOwnRef, "OtherBank")
                Else
                    WriteSEBScreenFile = False
                    Exit Function
                End If
            End If

        Catch exx As vbBabel.Payment.PaymentException

            Throw New vbBabel.Payment.PaymentException(exx.Message, exx.InnerException, Nothing, exx.LastLineRead, exx.FilenameImported) ' - Testing Try ... Catch

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteSEBScreenFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

        End Try

        WriteSEBScreenFile = True

    End Function
    Private Function SEBScreenCheckExportFile(ByRef oBabelFiles As BabelFiles, ByRef sFilenameOut As String, ByRef bFILE_OverWrite As Boolean, ByRef bFILE_WarningIfExists As Boolean) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim bProfileInUse As Boolean
        Dim iResponse As Short

        oFs = New Scripting.FileSystemObject

        If Not oBabelFiles Is Nothing Then
            If oBabelFiles.Count > 0 Then
                If oBabelFiles.Item(1).VB_ProfileInUse Then
                    bProfileInUse = True
                Else
                    bProfileInUse = False
                End If
            End If
        End If

        If oFs.FileExists(sFilenameOut) Then
            If bFILE_OverWrite Then
                If bFILE_WarningIfExists Then
                    If bProfileInUse Then
                        'Skal fil <filnavn> overskrives + "Filnavn finnes fra f�r!"
                        iResponse = MsgBox(LRS(12004, sFilenameOut), MsgBoxStyle.YesNo + MsgBoxStyle.Question, Replace(LRS(60003), " %1", "") & "!")
                        If iResponse = MsgBoxResult.No Then
                            'User doesn't want the file to be overwritten. Abandon program
                            SEBScreenCheckExportFile = False
                            Exit Function
                        Else
                            'Delete (overwrite) the file and continue
                            oFs.DeleteFile((sFilenameOut))
                        End If
                    Else
                        SEBScreenCheckExportFile = False
                        Exit Function
                    End If
                Else
                    'No warning just delete the file
                    oFs.DeleteFile((sFilenameOut))
                End If
            Else
                If bProfileInUse Then

                    'Filen  <filnavn> finnes fra f�r+ Programmet avbrytes +
                    MsgBox(LRS(60003, sFilenameOut) & vbCrLf & vbCrLf & LRS(10017), MsgBoxStyle.OKOnly + MsgBoxStyle.Critical, Replace(LRS(60003), " %1", "") & "!")
                End If
                SEBScreenCheckExportFile = False
                Exit Function
            End If
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        SEBScreenCheckExportFile = True

    End Function

    Private Function DoTheExport(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sPayType As String) As Boolean
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As FreeText
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bAppendFile As Boolean, sDate As String
        Dim i As Integer
        Dim sFreetext As String
        Dim iInvoiceCounter As Integer
        Dim bFoundClient As Boolean
        Dim sOldAccount As String
        Dim oaccount As Account
        Dim oClient As Client
        Dim sSwift As String
        Dim bHeaderWritten As Boolean
        Dim bFooterWritten As Boolean, sFooter As String
        Dim bSEGiroPayments As Boolean
        Dim bSEDirectCredit As Boolean
        Dim bSEMoneyOrder As Boolean, bSEAddZipAndCity As Boolean
        Dim bRaiseError As Boolean
        Dim sErrText As String, sErrFixedText As String
        Dim sTemp As String, sZip As String
        Dim bUSWriteBankNameAddress As Boolean, bUSIntermediaryBankIsMandatory As Boolean

        Try

            bHeaderWritten = False
            bFooterWritten = False

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            nFileSumAmount = 0
            nFileNoRecords = 0

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'Check that it is the correct paytype
                        If oPayment.DnBNORTBIPayType = sPayType Then
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            'Check that it is the correct paytype
                            If oPayment.DnBNORTBIPayType = sPayType Then
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If
                                'If true exit the loop, and we have data to export
                                If bExportoBatch Then
                                    Exit For
                                End If
                            End If
                        Next

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else



                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                'Check that it is the correct paytype
                                If oPayment.DnBNORTBIPayType = sPayType Then

                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If oPayment.REF_Own = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    bFoundClient = False
                                    If oPayment.I_Account <> sOldAccount Then
                                        For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                            For Each oaccount In oClient.Accounts
                                                If oaccount.Account = oPayment.I_Account Or Trim$(oaccount.ConvertedAccount) = oPayment.I_Account Then
                                                    sSwift = oaccount.SWIFTAddress
                                                    sOldAccount = oPayment.I_Account
                                                    bFoundClient = True
                                                    Exit For
                                                End If
                                            Next
                                            If bFoundClient Then
                                                Exit For
                                            End If
                                        Next
                                    End If

                                    '-------- fetch content of each payment ---------
                                    ' Total up freetext from all invoices;
                                    sFreetext = vbNullString
                                    For Each oInvoice In oPayment.Invoices
                                        For Each oFreetext In oInvoice.Freetexts
                                            sFreetext = sFreetext & " " & Trim$(oFreetext.Text)
                                        Next
                                    Next

                                    'Create en fixed error text with info from the payment
                                    sErrFixedText = vbNullString
                                    sErrFixedText = "Beneficiary's name: " & oPayment.E_Name & vbCrLf
                                    sErrFixedText = sErrFixedText & "Beneficiary's account: " & oPayment.E_Account & vbCrLf
                                    sErrFixedText = sErrFixedText & "Amount: " & oPayment.MON_InvoiceCurrency & " " & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") & vbCrLf
                                    sErrFixedText = sErrFixedText & "Payer's account: " & oPayment.I_Account

                                    'Write the correct format
                                    Select Case sPayType

                                        Case "Internal"

                                        Case "International"
                                            sLine = ":20:" & Right$(Trim$(oPayment.REF_Own), 12)    ' Transaction reference
                                            oFile.WriteLine(sLine)
                                            sLine = ":30:" & Mid$(oPayment.DATE_Payment, 3)   ' Executin date, YYMMDD
                                            oFile.WriteLine(sLine)
                                            If EmptyString(oPayment.MON_InvoiceCurrency) Then
                                                Err.Raise(1000, "DoTheExport_SEBScreen", "No currency is stated on the payment." & vbCrLf & _
                                                   "Maximum length is 8 digits." & vbCrLf & vbCrLf _
                                                   & "Credit bankgiro: " & oPayment.E_Account & vbCrLf _
                                                   & sErrFixedText)
                                            Else
                                                '02.12.2009
                                                sLine = ":32B:" & oPayment.MON_InvoiceCurrency & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, , ",")  ' Currency/amount
                                                'Old code
                                                'sLine = ":32B:" & oPayment.MON_InvoiceCurrency & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",")  ' Currency/amount
                                            End If
                                            oFile.WriteLine(sLine)
                                            If EmptyString(oPayment.I_Account) Then
                                                Err.Raise(1000, "DoTheExport_SEBScreen", "No debit account is stated on the payment." & vbCrLf & vbCrLf _
                                                   & sErrFixedText)
                                            Else
                                                sLine = ":50:" & oPayment.I_Account   ' Debitaccount. Does not set SWIFT. OK if debitacc in SEB
                                                ' add Debit Swiftcode, already checked that are valid
                                                sLine = sLine & "/" & oPayment.BANK_I_SWIFTCode
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Len(oPayment.BANK_NameAddressCorrBank1) > 0 Then
                                                sLine = ":54A:" & oPayment.BANK_SWIFTCodeCorrBank
                                                oFile.WriteLine(sLine)
                                                sLine = ":54D" & oPayment.BANK_NameAddressCorrBank1
                                                oFile.WriteLine(sLine)
                                                sLine = Left$(oPayment.BANK_NameAddressCorrBank2 & oPayment.BANK_NameAddressCorrBank3 & oPayment.BANK_NameAddressCorrBank4, 35)
                                                oFile.WriteLine(sLine)
                                            End If

                                            'Find which recordtype to use to deliver bankinformation
                                            If EmptyString(oPayment.BANK_SWIFTCode) And EmptyString(oPayment.BANK_BranchNo) Then
                                                Err.Raise(1000, "DoTheExport_SEBScreen", "No SWIFT code stated on the payment." & vbCrLf & vbCrLf _
                                                   & sErrFixedText)
                                            ElseIf EmptyString(oPayment.E_Account) Then
                                                Err.Raise(1000, "DoTheExport_SEBScreen", "No credit account is stated on the payment." & vbCrLf & vbCrLf _
                                                   & sErrFixedText)
                                            Else
                                                If (EmptyString(oPayment.BANK_Name) And Not EmptyString(oPayment.BANK_BranchNo)) Or Not EmptyString(oPayment.BANK_SWIFTCode) Then
                                                    sLine = ":57A:/" & oPayment.E_Account
                                                    oFile.WriteLine(sLine)
                                                    sLine = oPayment.BANK_SWIFTCode
                                                    oFile.WriteLine(sLine)
                                                    If Len(oPayment.BANK_BranchNo) > 0 Then
                                                        sLine = oPayment.BANK_BranchType & oPayment.BANK_BranchNo
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                Else
                                                    If EmptyString(oPayment.BANK_BranchNo) Then
                                                        'No SWIFT or branch
                                                        sLine = ":57B:/" & oPayment.E_Account
                                                        oFile.WriteLine(sLine)
                                                        sLine = oPayment.BANK_Name
                                                        oFile.WriteLine(sLine)
                                                        If Len(oPayment.BANK_Adr1) > 0 Then
                                                            sLine = oPayment.BANK_Adr1
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    Else
                                                        'Only branch
                                                        sLine = ":57D:/" & oPayment.E_Account
                                                        oFile.WriteLine(sLine)
                                                        sLine = oPayment.BANK_BranchType & oPayment.BANK_BranchNo
                                                        oFile.WriteLine(sLine)
                                                        sLine = oPayment.BANK_Name
                                                        oFile.WriteLine(sLine)
                                                        If Len(oPayment.BANK_Adr1) > 0 Then
                                                            sLine = oPayment.BANK_Adr1
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    End If
                                                End If
                                            End If
                                            If EmptyString(oPayment.E_Name) Then
                                                Err.Raise(1000, "DoTheExport_SEBScreen", "Receivers name is not stated on the payment." & vbCrLf & vbCrLf _
                                                   & sErrFixedText)
                                            Else
                                                sLine = ":59:" & oPayment.E_Name
                                                oFile.WriteLine(sLine)
                                                If Len(oPayment.E_Adr1) > 0 Then
                                                    sLine = oPayment.E_Adr1
                                                    oFile.WriteLine(sLine)
                                                End If
                                                If Len(oPayment.E_Adr2) > 0 Then
                                                    sLine = oPayment.E_Adr2
                                                    oFile.WriteLine(sLine)
                                                End If
                                                If Len(oPayment.E_Adr3) > 0 Then
                                                    sLine = oPayment.E_Adr3
                                                    oFile.WriteLine(sLine)
                                                End If
                                            End If
                                            If Len(sFreetext) > 140 Then
                                                If oBabel.VB_ProfileInUse Then
                                                    sFreetext = DiminishFreetext(sFreetext, 140, sErrFixedText)
                                                Else
                                                    sFreetext = Left$(sFreetext, 140)
                                                End If
                                            End If
                                            If Len(sFreetext) > 0 Then
                                                sLine = ":70:" & Left$(sFreetext, 35)
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Len(sFreetext) > 35 Then
                                                sLine = Mid$(sFreetext, 36, 35)
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Len(sFreetext) > 70 Then
                                                sLine = Mid$(sFreetext, 71, 35)
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Len(sFreetext) > 105 Then
                                                sLine = Mid$(sFreetext, 106, 35)
                                                oFile.WriteLine(sLine)
                                            End If

                                            ' Charges - if OUR/OUR = OUR, BEN/BEN=BEN, all other:SHA
                                            ' For USD payments from US(?) - always OUR
                                            If Mid$(oPayment.BANK_I_SWIFTCode, 5, 2) = "US" And oPayment.MON_InvoiceCurrency = "USD" Then
                                                sLine = ":71A:" & "OUR"
                                                oFile.WriteLine(sLine)
                                            Else
                                                If oPayment.MON_ChargeMeDomestic = True And oPayment.MON_ChargeMeAbroad = True Then
                                                    sLine = ":71A:" & "OUR"
                                                    oFile.WriteLine(sLine)
                                                ElseIf oPayment.MON_ChargeMeDomestic = False And oPayment.MON_ChargeMeAbroad = False Then
                                                    sLine = ":71A:" & "BEN"
                                                    oFile.WriteLine(sLine)
                                                Else
                                                    sLine = ":71A:" & "SHA"
                                                    oFile.WriteLine(sLine)
                                                End If
                                            End If

                                            ' Construct Instructions to debit accoun servicing institution
                                            ' Some fixed codes, some freetext
                                            sLine = vbNullString

                                            '25.01.2010 - Always express when payers account is in the US
                                            If Mid$(oPayment.BANK_I_SWIFTCode, 5, 2) = "US" Then
                                                sLine = ":72:PRI=X"
                                            ElseIf oPayment.Priority Then
                                                sLine = ":72:PRI=X"
                                            Else
                                                sLine = ":72:PRI=N"
                                            End If
                                            oFile.WriteLine(sLine)
                                            sLine = vbNullString

                                            ' Reporting (statebank)
                                            ' No reporting required in Denmark or Germany !!
                                            'So far, just create this tag if debitaccount is in Norway
                                            If Mid$(oPayment.BANK_I_SWIFTCode, 5, 2) = "NO" Then
                                                If oPayment.Invoices.Count > 0 Then
                                                    ' Assumes that all invoices has same reporting, use Invoice(1)
                                                    If Len(oPayment.Invoices(1).STATEBANK_Code) > 0 Then
                                                        sLine = sLine & "CBR RK=" & oPayment.Invoices(1).STATEBANK_Code
                                                        If Len(oPayment.E_CountryCode) = 2 Then
                                                            sLine = sLine & " CY=" & oPayment.E_CountryCode
                                                        End If
                                                        sLine = sLine & " CO=NO"
                                                    End If
                                                    If Len(oPayment.Invoices(1).STATEBANK_DATE) > 0 Then
                                                        sLine = sLine & " LN=" & oPayment.Invoices(1).STATEBANK_DATE
                                                    End If
                                                    If Not EmptyString(sLine) Then
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                    If Not EmptyString(oPayment.Invoices(1).STATEBANK_Text) Then
                                                        sLine = Trim$(oPayment.Invoices(1).STATEBANK_Text)
                                                        If Len(sLine) > 35 Then
                                                            oFile.WriteLine(Left$(sLine, 35))
                                                            oFile.WriteLine(Mid$(sLine, 36, 35))
                                                        Else
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    End If
                                                End If
                                            ElseIf Mid$(oPayment.BANK_I_SWIFTCode, 5, 2) = "SE" Then
                                                If oPayment.Invoices.Count > 0 Then
                                                    ' Assumes that all invoices has same reporting, use Invoice(1)
                                                    If Len(oPayment.Invoices(1).STATEBANK_Code) > 0 Then
                                                        sLine = sLine & "CBR RK=" & oPayment.Invoices(1).STATEBANK_Code
                                                        If Len(oPayment.E_CountryCode) = 2 Then
                                                            sLine = sLine & " CY=" & oPayment.E_CountryCode
                                                        End If
                                                        sLine = sLine & " CO=SE"
                                                    End If
                                                    If Len(oPayment.Invoices(1).STATEBANK_DATE) > 0 Then
                                                        sLine = sLine & " LN=" & oPayment.Invoices(1).STATEBANK_DATE
                                                    End If
                                                    If Not EmptyString(sLine) Then
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                    If Not EmptyString(oPayment.Invoices(1).STATEBANK_Text) Then
                                                        sLine = Trim$(oPayment.Invoices(1).STATEBANK_Text)
                                                        If Len(sLine) > 35 Then
                                                            oFile.WriteLine(Left$(sLine, 35))
                                                            oFile.WriteLine(Mid$(sLine, 36, 35))
                                                        Else
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    End If
                                                End If
                                            End If

                                            nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
                                            nFileNoRecords = nFileNoRecords + 1

                                            oPayment.Exported = True

                                            If Not bFooterWritten Then
                                                ' Trailer part
                                                sFooter = ":Z1:BB_NoOfRecords" & vbCrLf
                                                sFooter = sFooter & ":Z2:BB_FileSumAmount"
                                                ' - Trailer end -
                                                bFooterWritten = True
                                            End If

                                        Case "Advice"

                                        Case "SwedishBGC"

                                            'XNET - 19.02.2014 - Added next IF and first part of the IF
                                            If oPayment.PayCode = "601" Then

                                                For Each oInvoice In oPayment.Invoices
                                                    'There are more transactionstypes than mentioned below
                                                    bSEGiroPayments = True
                                                    bSEDirectCredit = False
                                                    bSEMoneyOrder = False
                                                    If Not EmptyString(oInvoice.REF_Own & oPayment.REF_Own) Then
                                                        sLine = ":10:" & Right$(Trim$(oInvoice.REF_Own & oPayment.REF_Own), 12)
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                    sLine = ":11:" & Mid$(oPayment.DATE_Payment, 3)   ' Executin date, YYMMDD
                                                    oFile.WriteLine(sLine)
                                                    If Not EmptyString(oPayment.MON_InvoiceCurrency) Then
                                                        'Only SEK and EUR allowed
                                                        If oPayment.MON_InvoiceCurrency = "SEK" Or oPayment.MON_InvoiceCurrency = "EUR" Then
                                                            'New code 02.12.2008
                                                            sLine = ":13:" & oPayment.MON_InvoiceCurrency & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, , ",") ' Currency/amount
                                                            'Old code
                                                            'sLine = ":13:" & oPayment.MON_InvoiceCurrency & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",")  ' Currency/amount
                                                            oFile.WriteLine(sLine)
                                                        Else
                                                            Err.Raise(1000, "DoTheExport_SEBScreen", "Illegal currency stated on the payment." & vbCrLf & vbCrLf _
                                                               & "Currency: " & oPayment.MON_InvoiceCurrency & vbCrLf _
                                                                & sErrFixedText)
                                                        End If
                                                    Else
                                                        'Assume SEK
                                                        sLine = ":13:SEK" & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, , ",") ' Currency/amount
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                    sLine = ":14:" & oPayment.I_Account   ' Debitaccount.
                                                    oFile.WriteLine(sLine)
                                                    '�   Giro payment.
                                                    '    Tag 15 is required and tags 16, 18, 19, 20 and 21 are not allowed.
                                                    '�   Direct credit.
                                                    '    Tags 16, 17, 20 and 21 are required and tag 15 is not allowed. Tags 18 and 19 are optional.
                                                    '�   Money order.
                                                    '    Tag 17, 20 and 21 are required and tags 15 and 16 are not allowed. Tags 18 and 19 are optional.
                                                    If bSEGiroPayments Then
                                                        oPayment.E_Account = Replace(oPayment.E_Account, "-", vbNullString)
                                                        If Len(Trim$(oPayment.E_Account)) > 8 And IsIBANNumber(oPayment.E_Account, False) = Scripting.Tristate.TristateFalse Then
                                                            Err.Raise(1000, "DoTheExport_SEBScreen", "Illegal credit bankgiro stated on the payment." & vbCrLf & _
                                                               "Maximum length is 8 digits." & vbCrLf & vbCrLf _
                                                               & "Credit bankgiro: " & oPayment.E_Account & vbCrLf _
                                                                & sErrFixedText)
                                                        Else
                                                            sLine = ":15:" & Trim$(oPayment.E_Account)  ' Credit bankgiro
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    End If
                                                    If bSEDirectCredit Then
                                                        If Len(Trim$(oPayment.E_Account)) > 16 And IsIBANNumber(oPayment.E_Account, False) = Scripting.Tristate.TristateFalse Then
                                                            Err.Raise(1000, "DoTheExport_SEBScreen", "Illegal credit account stated on the payment." & vbCrLf & _
                                                               "Maximum length is 16 digits." & vbCrLf & vbCrLf _
                                                               & "Credit account: " & oPayment.E_Account & vbCrLf _
                                                                & sErrFixedText)
                                                        Else
                                                            sLine = ":16:" & Trim$(oPayment.E_Account)  ' Credit account
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    End If
                                                    If bSEDirectCredit Or bSEMoneyOrder Then
                                                        If EmptyString(oPayment.E_Name) Then
                                                            sErrText = "Beneficiary name is required for a XXXXX."
                                                            If bSEDirectCredit Then
                                                                sErrText = Replace(sErrText, "XXXXX", "Direct Credit")
                                                            Else
                                                                sErrText = Replace(sErrText, "XXXXX", "Money Order")
                                                            End If
                                                            Err.Raise(1000, "DoTheExport_SEBScreen", "WriteSEBScreen", sErrText & vbCrLf _
                                                           & sErrFixedText)
                                                        Else
                                                            sLine = ":17:" & Left$(Trim$(oPayment.E_Name), 35) ' Beneficiary name
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    Else
                                                        bSEAddZipAndCity = False
                                                        If Not EmptyString(oPayment.E_Name) Then
                                                            'Changed 21.05.2007 commented a few lines
                                                            'If Not EmptyString(oPayment.E_Zip) Then
                                                            'If Not EmptyString(oPayment.E_City) Then
                                                            sLine = ":17:" & Left$(Trim$(oPayment.E_Name), 35) ' Beneficiary name
                                                            oFile.WriteLine(sLine)
                                                            'End If
                                                            'End If
                                                        End If
                                                    End If
                                                    If bSEDirectCredit Or bSEMoneyOrder Then
                                                        'Check if zip and city is stated inn an address field
                                                        'Sometimes it may be stated like 999 99
                                                        If EmptyString(oPayment.E_Zip) Then
                                                            If vbIsNumeric(Left$(Replace(Left$(oPayment.E_Adr1, 10), " ", vbNullString), 5), "0123456789") Then
                                                                oPayment.E_Zip = Left$(Replace(Left$(oPayment.E_Adr1, 10), " ", vbNullString), 5)
                                                                If EmptyString(oPayment.E_City) Then
                                                                    If InStr(1, Left$(oPayment.E_Adr1, 6), " ") > 0 Then
                                                                        oPayment.E_City = Mid$(oPayment.E_Adr1, 7)
                                                                    Else
                                                                        oPayment.E_City = Mid$(oPayment.E_Adr1, 6)
                                                                    End If
                                                                    oPayment.E_Adr1 = vbNullString
                                                                End If
                                                            ElseIf vbIsNumeric(Left$(Replace(Left$(oPayment.E_Adr2, 10), " ", vbNullString), 5), "0123456789") Then
                                                                oPayment.E_Zip = Left$(Replace(Left$(oPayment.E_Adr2, 10), " ", vbNullString), 5)
                                                                If EmptyString(oPayment.E_City) Then
                                                                    If InStr(1, Left$(oPayment.E_Adr2, 6), " ") > 0 Then
                                                                        oPayment.E_City = Mid$(oPayment.E_Adr2, 7)
                                                                    Else
                                                                        oPayment.E_City = Mid$(oPayment.E_Adr2, 6)
                                                                    End If
                                                                    oPayment.E_Adr2 = vbNullString
                                                                End If
                                                            ElseIf vbIsNumeric(Left$(Replace(Left$(oPayment.E_Adr3, 10), " ", vbNullString), 5), "0123456789") Then
                                                                oPayment.E_Zip = Left$(Replace(Left$(oPayment.E_Adr3, 10), " ", vbNullString), 5)
                                                                If EmptyString(oPayment.E_City) Then
                                                                    If InStr(1, Left$(oPayment.E_Adr3, 6), " ") > 0 Then
                                                                        oPayment.E_City = Mid$(oPayment.E_Adr3, 7)
                                                                    Else
                                                                        oPayment.E_City = Mid$(oPayment.E_Adr3, 6)
                                                                    End If
                                                                    oPayment.E_Adr3 = vbNullString
                                                                End If
                                                            End If
                                                        End If
                                                        If Not EmptyString(oPayment.E_Adr1) Then
                                                            sLine = ":18:" & Left$(Trim$(oPayment.E_Adr1), 35) ' Beneficiary address 1
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    End If
                                                    If bSEDirectCredit Or bSEMoneyOrder Then
                                                        If Not EmptyString(oPayment.E_Adr2) Then
                                                            sLine = ":19:" & Left$(Trim$(oPayment.E_Adr2), 35) ' Beneficiary address 2
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    End If
                                                    If bSEDirectCredit Or bSEMoneyOrder Then
                                                        If EmptyString(oPayment.E_Zip) Then
                                                            sErrText = "Beneficiary postal code is required for a XXXXX."
                                                            If bSEDirectCredit Then
                                                                sErrText = Replace(sErrText, "XXXXX", "Direct Credit")
                                                            Else
                                                                sErrText = Replace(sErrText, "XXXXX", "Money Order")
                                                            End If
                                                            Err.Raise(1000, "DoTheExport_SEBScreen", "WriteSEBScreen", sErrText & vbCrLf _
                                                           & sErrFixedText)
                                                        Else
                                                            sLine = ":20:" & Left$(Trim$(oPayment.E_Zip), 5) '20  Beneficiary postal code
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    Else
                                                        If bSEAddZipAndCity Then
                                                            'Already checked that zip isn't empty
                                                            sLine = ":20:" & Left$(Trim$(oPayment.E_Zip), 5) '20  Beneficiary postal code
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    End If
                                                    If bSEDirectCredit Or bSEMoneyOrder Then
                                                        If EmptyString(oPayment.E_City) Then
                                                            sErrText = "Beneficiary city code is required for a XXXXX."
                                                            If bSEDirectCredit Then
                                                                sErrText = Replace(sErrText, "XXXXX", "Direct Credit")
                                                            Else
                                                                sErrText = Replace(sErrText, "XXXXX", "Money Order")
                                                            End If
                                                            Err.Raise(1000, "DoTheExport_SEBScreen", "WriteSEBScreen", sErrText & vbCrLf _
                                                           & sErrFixedText)
                                                        Else
                                                            sLine = ":21:" & Trim$(oPayment.E_City) '21  Beneficiary City
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    Else
                                                        If bSEAddZipAndCity Then
                                                            'Already checked that city isn't empty
                                                            sLine = ":21:" & Trim$(oPayment.E_City) '21  Beneficiary City
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    End If

                                                    '16.11.2009 New due to C&I Online. KID is now allowed
                                                    If Not EmptyString(oInvoice.Unique_Id) Then
                                                        sLine = ":25:" & Right$(Trim$(oInvoice.Unique_Id), 25) '23 Invoice no.
                                                        oFile.WriteLine(sLine)
                                                    Else
                                                        sFreetext = ""
                                                        For Each oFreetext In oInvoice.Freetexts
                                                            sFreetext = sFreetext & " " & Trim$(oFreetext.Text)
                                                        Next

                                                        If Len(sFreetext) > 140 Then
                                                            If oBabel.VB_ProfileInUse Then
                                                                sFreetext = DiminishFreetext(sFreetext, 140, sErrFixedText)
                                                            Else
                                                                sFreetext = Left$(sFreetext, 140)
                                                            End If
                                                        End If
                                                        If Len(sFreetext) > 0 Then
                                                            sLine = ":26:" & Left$(sFreetext, 35)
                                                            oFile.WriteLine(sLine)
                                                            sFreetext = Mid$(sFreetext, 36)
                                                            If Len(sFreetext) > 0 Then
                                                                sLine = Left$(sFreetext, 35)
                                                                oFile.WriteLine(sLine)
                                                                sFreetext = Mid$(sFreetext, 36)
                                                                If Len(sFreetext) > 0 Then
                                                                    sLine = Left$(sFreetext, 35)
                                                                    oFile.WriteLine(sLine)
                                                                    sFreetext = Mid$(sFreetext, 36)
                                                                    If Len(sFreetext) > 0 Then
                                                                        sLine = Left$(sFreetext, 35)
                                                                        oFile.WriteLine(sLine)
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                    '16.11.2009 - Tag 25 no longer used
                                                    '                            If Len(sFreetext) > 25 Then
                                                    '                                sLine = ":25:" & Mid$(sFreetext, 26, 25)
                                                    '                                oFile.WriteLine (sLine)
                                                    '                            End If

                                                    '16.11.2009 - Added paytype
                                                    'PRI=; BG; PG; DIRECT CREDIT; EXPRESS; MONEY ORDER; MASS TRANSFER, SALARY; MASS TRANSFER, OTHER
                                                    If bSEGiroPayments Then
                                                        ' XOKNET 14.12.2012 - added support for Postgiro
                                                        If oPayment.PayCode = "190" Then
                                                            sLine = ":27:" & "PRI=PG"
                                                        Else
                                                            sLine = ":27:" & "PRI=BG"
                                                        End If
                                                        oFile.WriteLine(sLine)
                                                    ElseIf bSEDirectCredit Then
                                                        sLine = ":27:" & "PRI=DIRECT CREDIT"
                                                        oFile.WriteLine(sLine)
                                                    ElseIf bSEMoneyOrder Then
                                                        sLine = ":27:" & "PRI=MONEY ORDER"
                                                        oFile.WriteLine(sLine)
                                                    End If

                                                    nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
                                                    nFileNoRecords = nFileNoRecords + 1

                                                    oPayment.Exported = True

                                                    If Not bFooterWritten Then
                                                        ' Trailer part
                                                        sFooter = ":Z1:BB_NoOfRecords" & vbCrLf
                                                        sFooter = sFooter & ":Z2:BB_FileSumAmount"
                                                        bFooterWritten = True
                                                        ' - Trailer end -
                                                    End If
                                                Next oInvoice

                                            Else
                                                'Asume DirectCredit if nothing else stated
                                                'There are more transactionstypes than mentioned below
                                                bSEGiroPayments = True
                                                bSEDirectCredit = False
                                                bSEMoneyOrder = False
                                                If Not EmptyString(oPayment.REF_Own) Then
                                                    sLine = ":10:" & Right$(Trim$(oPayment.REF_Own), 12)
                                                    oFile.WriteLine(sLine)
                                                End If
                                                sLine = ":11:" & Mid$(oPayment.DATE_Payment, 3)   ' Executin date, YYMMDD
                                                oFile.WriteLine(sLine)
                                                If Not EmptyString(oPayment.MON_InvoiceCurrency) Then
                                                    'Only SEK and EUR allowed
                                                    If oPayment.MON_InvoiceCurrency = "SEK" Or oPayment.MON_InvoiceCurrency = "EUR" Then
                                                        'New code 02.12.2008
                                                        sLine = ":13:" & oPayment.MON_InvoiceCurrency & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, , ",") ' Currency/amount
                                                        'Old code
                                                        'sLine = ":13:" & oPayment.MON_InvoiceCurrency & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",")  ' Currency/amount
                                                        oFile.WriteLine(sLine)
                                                    Else
                                                        Err.Raise(1000, "DoTheExport_SEBScreen", "Illegal currency stated on the payment." & vbCrLf & vbCrLf _
                                                           & "Currency: " & oPayment.MON_InvoiceCurrency & vbCrLf _
                                                            & sErrFixedText)
                                                    End If
                                                Else
                                                    'Assume SEK
                                                    sLine = ":13:SEK" & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, , ",") ' Currency/amount
                                                    oFile.WriteLine(sLine)
                                                End If
                                                sLine = ":14:" & oPayment.I_Account   ' Debitaccount.
                                                oFile.WriteLine(sLine)
                                                '�   Giro payment.
                                                '    Tag 15 is required and tags 16, 18, 19, 20 and 21 are not allowed.
                                                '�   Direct credit.
                                                '    Tags 16, 17, 20 and 21 are required and tag 15 is not allowed. Tags 18 and 19 are optional.
                                                '�   Money order.
                                                '    Tag 17, 20 and 21 are required and tags 15 and 16 are not allowed. Tags 18 and 19 are optional.
                                                If bSEGiroPayments Then
                                                    oPayment.E_Account = Replace(oPayment.E_Account, "-", vbNullString)
                                                    If Len(Trim$(oPayment.E_Account)) > 8 And IsIBANNumber(oPayment.E_Account, False) = Scripting.Tristate.TristateFalse Then
                                                        Err.Raise(1000, "DoTheExport_SEBScreen", "Illegal credit bankgiro stated on the payment." & vbCrLf & _
                                                           "Maximum length is 8 digits." & vbCrLf & vbCrLf _
                                                           & "Credit bankgiro: " & oPayment.E_Account & vbCrLf _
                                                            & sErrFixedText)
                                                    Else
                                                        sLine = ":15:" & Trim$(oPayment.E_Account)  ' Credit bankgiro
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                End If
                                                If bSEDirectCredit Then
                                                    If Len(Trim$(oPayment.E_Account)) > 16 And IsIBANNumber(oPayment.E_Account, False) = Scripting.Tristate.TristateFalse Then
                                                        Err.Raise(1000, "DoTheExport_SEBScreen", "Illegal credit account stated on the payment." & vbCrLf & _
                                                           "Maximum length is 16 digits." & vbCrLf & vbCrLf _
                                                           & "Credit account: " & oPayment.E_Account & vbCrLf _
                                                            & sErrFixedText)
                                                    Else
                                                        sLine = ":16:" & Trim$(oPayment.E_Account)  ' Credit account
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                End If
                                                If bSEDirectCredit Or bSEMoneyOrder Then
                                                    If EmptyString(oPayment.E_Name) Then
                                                        sErrText = "Beneficiary name is required for a XXXXX."
                                                        If bSEDirectCredit Then
                                                            sErrText = Replace(sErrText, "XXXXX", "Direct Credit")
                                                        Else
                                                            sErrText = Replace(sErrText, "XXXXX", "Money Order")
                                                        End If
                                                        Err.Raise(1000, "DoTheExport_SEBScreen", "WriteSEBScreen", sErrText & vbCrLf _
                                                       & sErrFixedText)
                                                    Else
                                                        sLine = ":17:" & Left$(Trim$(oPayment.E_Name), 35) ' Beneficiary name
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                Else
                                                    bSEAddZipAndCity = False
                                                    If Not EmptyString(oPayment.E_Name) Then
                                                        'Changed 21.05.2007 commented a few lines
                                                        'If Not EmptyString(oPayment.E_Zip) Then
                                                        'If Not EmptyString(oPayment.E_City) Then
                                                        sLine = ":17:" & Left$(Trim$(oPayment.E_Name), 35) ' Beneficiary name
                                                        oFile.WriteLine(sLine)
                                                        'End If
                                                        'End If
                                                    End If
                                                End If
                                                If bSEDirectCredit Or bSEMoneyOrder Then
                                                    'Check if zip and city is stated inn an address field
                                                    'Sometimes it may be stated like 999 99
                                                    If EmptyString(oPayment.E_Zip) Then
                                                        If vbIsNumeric(Left$(Replace(Left$(oPayment.E_Adr1, 10), " ", vbNullString), 5), "0123456789") Then
                                                            oPayment.E_Zip = Left$(Replace(Left$(oPayment.E_Adr1, 10), " ", vbNullString), 5)
                                                            If EmptyString(oPayment.E_City) Then
                                                                If InStr(1, Left$(oPayment.E_Adr1, 6), " ") > 0 Then
                                                                    oPayment.E_City = Mid$(oPayment.E_Adr1, 7)
                                                                Else
                                                                    oPayment.E_City = Mid$(oPayment.E_Adr1, 6)
                                                                End If
                                                                oPayment.E_Adr1 = vbNullString
                                                            End If
                                                        ElseIf vbIsNumeric(Left$(Replace(Left$(oPayment.E_Adr2, 10), " ", vbNullString), 5), "0123456789") Then
                                                            oPayment.E_Zip = Left$(Replace(Left$(oPayment.E_Adr2, 10), " ", vbNullString), 5)
                                                            If EmptyString(oPayment.E_City) Then
                                                                If InStr(1, Left$(oPayment.E_Adr2, 6), " ") > 0 Then
                                                                    oPayment.E_City = Mid$(oPayment.E_Adr2, 7)
                                                                Else
                                                                    oPayment.E_City = Mid$(oPayment.E_Adr2, 6)
                                                                End If
                                                                oPayment.E_Adr2 = vbNullString
                                                            End If
                                                        ElseIf vbIsNumeric(Left$(Replace(Left$(oPayment.E_Adr3, 10), " ", vbNullString), 5), "0123456789") Then
                                                            oPayment.E_Zip = Left$(Replace(Left$(oPayment.E_Adr3, 10), " ", vbNullString), 5)
                                                            If EmptyString(oPayment.E_City) Then
                                                                If InStr(1, Left$(oPayment.E_Adr3, 6), " ") > 0 Then
                                                                    oPayment.E_City = Mid$(oPayment.E_Adr3, 7)
                                                                Else
                                                                    oPayment.E_City = Mid$(oPayment.E_Adr3, 6)
                                                                End If
                                                                oPayment.E_Adr3 = vbNullString
                                                            End If
                                                        End If
                                                    End If
                                                    If Not EmptyString(oPayment.E_Adr1) Then
                                                        sLine = ":18:" & Left$(Trim$(oPayment.E_Adr1), 35) ' Beneficiary address 1
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                End If
                                                If bSEDirectCredit Or bSEMoneyOrder Then
                                                    If Not EmptyString(oPayment.E_Adr2) Then
                                                        sLine = ":19:" & Left$(Trim$(oPayment.E_Adr2), 35) ' Beneficiary address 2
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                End If
                                                If bSEDirectCredit Or bSEMoneyOrder Then
                                                    If EmptyString(oPayment.E_Zip) Then
                                                        sErrText = "Beneficiary postal code is required for a XXXXX."
                                                        If bSEDirectCredit Then
                                                            sErrText = Replace(sErrText, "XXXXX", "Direct Credit")
                                                        Else
                                                            sErrText = Replace(sErrText, "XXXXX", "Money Order")
                                                        End If
                                                        Err.Raise(1000, "DoTheExport_SEBScreen", "WriteSEBScreen", sErrText & vbCrLf _
                                                       & sErrFixedText)
                                                    Else
                                                        sLine = ":20:" & Left$(Trim$(oPayment.E_Zip), 5) '20  Beneficiary postal code
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                Else
                                                    If bSEAddZipAndCity Then
                                                        'Already checked that zip isn't empty
                                                        sLine = ":20:" & Left$(Trim$(oPayment.E_Zip), 5) '20  Beneficiary postal code
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                End If
                                                If bSEDirectCredit Or bSEMoneyOrder Then
                                                    If EmptyString(oPayment.E_City) Then
                                                        sErrText = "Beneficiary city code is required for a XXXXX."
                                                        If bSEDirectCredit Then
                                                            sErrText = Replace(sErrText, "XXXXX", "Direct Credit")
                                                        Else
                                                            sErrText = Replace(sErrText, "XXXXX", "Money Order")
                                                        End If
                                                        Err.Raise(1000, "DoTheExport_SEBScreen", "WriteSEBScreen", sErrText & vbCrLf _
                                                       & sErrFixedText)
                                                    Else
                                                        sLine = ":21:" & Trim$(oPayment.E_City) '21  Beneficiary City
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                Else
                                                    If bSEAddZipAndCity Then
                                                        'Already checked that city isn't empty
                                                        sLine = ":21:" & Trim$(oPayment.E_City) '21  Beneficiary City
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                End If

                                                '16.11.2009 New due to C&I Online. KID is now allowed
                                                If bSEGiroPayments Then
                                                    If oPayment.Invoices.Count = 1 Then
                                                        If Not EmptyString(oPayment.Invoices.Item(1).Unique_Id) Then
                                                            sLine = ":25:" & Right$(Trim$(oPayment.Invoices.Item(1).Unique_Id), 25) '23 Invoice no.
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                    End If
                                                End If
                                                'Possible to structured invoiceno., should do this even if more than one invoice
                                                '16.11.2009 - Structured invoices are not longer supported in the C&I Online
                                                '''''                            If oPayment.Invoices.Count = 1 Then
                                                '''''                                If Not EmptyString(oPayment.Invoices.Item(1).InvoiceNo) Then
                                                '''''                                    sLine = ":23:" & Right$(Trim$(oPayment.Invoices.Item(1).InvoiceNo), 25) '23 Invoice no.
                                                '''''                                    oFile.WriteLine (sLine)
                                                '''''                                End If
                                                '''''                            End If
                                                '16.11.2009 - Max freetext increased from 50 to 140 (4*35)
                                                If Len(sFreetext) > 140 Then
                                                    If oBabel.VB_ProfileInUse Then
                                                        sFreetext = DiminishFreetext(sFreetext, 140, sErrFixedText)
                                                    Else
                                                        sFreetext = Left$(sFreetext, 140)
                                                    End If
                                                End If
                                                If Len(sFreetext) > 0 Then
                                                    sLine = ":26:" & Left$(sFreetext, 35)
                                                    oFile.WriteLine(sLine)
                                                    sFreetext = Mid$(sFreetext, 36)
                                                    If Len(sFreetext) > 0 Then
                                                        sLine = Left$(sFreetext, 35)
                                                        oFile.WriteLine(sLine)
                                                        sFreetext = Mid$(sFreetext, 36)
                                                        If Len(sFreetext) > 0 Then
                                                            sLine = Left$(sFreetext, 35)
                                                            oFile.WriteLine(sLine)
                                                            sFreetext = Mid$(sFreetext, 36)
                                                            If Len(sFreetext) > 0 Then
                                                                sLine = Left$(sFreetext, 35)
                                                                oFile.WriteLine(sLine)
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                                '16.11.2009 - Tag 25 no longer used
                                                '                            If Len(sFreetext) > 25 Then
                                                '                                sLine = ":25:" & Mid$(sFreetext, 26, 25)
                                                '                                oFile.WriteLine (sLine)
                                                '                            End If

                                                '16.11.2009 - Added paytype
                                                'PRI=; BG; PG; DIRECT CREDIT; EXPRESS; MONEY ORDER; MASS TRANSFER, SALARY; MASS TRANSFER, OTHER
                                                If bSEGiroPayments Then
                                                    ' XOKNET 14.12.2012 - added support for Postgiro
                                                    If oPayment.PayCode = "190" Then
                                                        sLine = ":27:" & "PRI=PG"
                                                    Else
                                                        sLine = ":27:" & "PRI=BG"
                                                    End If
                                                    oFile.WriteLine(sLine)
                                                ElseIf bSEDirectCredit Then
                                                    sLine = ":27:" & "PRI=DIRECT CREDIT"
                                                    oFile.WriteLine(sLine)
                                                ElseIf bSEMoneyOrder Then
                                                    sLine = ":27:" & "PRI=MONEY ORDER"
                                                    oFile.WriteLine(sLine)
                                                End If

                                                nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
                                                nFileNoRecords = nFileNoRecords + 1

                                                oPayment.Exported = True

                                                If Not bFooterWritten Then
                                                    ' Trailer part
                                                    sFooter = ":Z1:BB_NoOfRecords" & vbCrLf
                                                    sFooter = sFooter & ":Z2:BB_FileSumAmount"
                                                    bFooterWritten = True
                                                    ' - Trailer end -
                                                End If
                                            End If

                                        Case "Ueberwissung"
                                            If Not bHeaderWritten Then
                                                bFooterWritten = False
                                                ' Write headerpart;
                                                sLine = ":E1:" & "D"        ' Debit/Credit flag
                                                oFile.WriteLine(sLine)
                                                ' Try to find swiftcode from accountstable;
                                                If Len(sSwift) > 0 Then
                                                    sLine = ":E2:" & sSwift  ' Swiftcode of receiving bank
                                                Else
                                                    Err.Raise(1000, "DoTheExport_SEBScreen", "WriteSEBScreen", "No SWIFT-address found for the debit bankaccount." & vbCrLf & "Please enter the SWIFT-address in the Client Setup.")
                                                End If
                                                oFile.WriteLine(sLine)
                                                sLine = ":E3:" & "S"        ' Single
                                                oFile.WriteLine(sLine)
                                                bHeaderWritten = True
                                            End If

                                            bRaiseError = False
                                            sErrText = vbNullString
                                            If IsIBANNumber(oPayment.E_Account, False, False, sErrText) <> Scripting.Tristate.TristateFalse Then
                                                oPayment.BANK_BranchNo = Mid$(oPayment.E_Account, 5, 8)
                                                oPayment.E_Account = Mid$(oPayment.E_Account, 13)
                                                'Changed 24.05.2007 - IBAN is not mandatory, if a BLZ and account is stated
                                                'BLZ and accounts are validated beneath.
                                                'Else
                                                '    Err.Raise 1000, "DoTheExport_SEBScreen", "Illegal or no IBAN-number stated on the payment." & vbCrLf & vbCrLf _
                                                '       & sErrText _
                                                '       & "IBAN: " & oPayment.E_Account & vbCrLf _
                                                '       & sErrFixedText
                                            End If
                                            If Len(oPayment.BANK_BranchNo) = 8 And vbIsNumeric(oPayment.BANK_BranchNo, "0123456789") Then
                                                sLine = ":CA:" & oPayment.BANK_BranchNo             ' BLZ
                                            Else
                                                If Left$(oPayment.BANK_Name, 4) = "//BL" Or Left$(oPayment.BANK_Name, 2) = "BL" Or Left$(oPayment.BANK_Name, 2) = "//" Then
                                                    sTemp = Replace(Replace(oPayment.BANK_Name, "BL", ""), "//", "")
                                                    If Len(sTemp) = 8 And vbIsNumeric(sTemp, "0123456789") Then
                                                        sLine = ":CA:" & sTemp         ' BLZ, try to find in bankname
                                                    Else
                                                        bRaiseError = True
                                                    End If
                                                Else
                                                    bRaiseError = True
                                                End If
                                            End If
                                            If bRaiseError Then
                                                If Len(oPayment.BANK_BranchNo) > 0 Then
                                                    sErrText = oPayment.BANK_BranchNo
                                                Else
                                                    sErrText = sTemp
                                                End If
                                                Err.Raise(1000, "DoTheExport_SEBScreen", "Illegal or no BLZ (bankcode) stated on the payment." & vbCrLf & vbCrLf _
                                                   & "BLZ: " & sErrText & vbCrLf _
                                                   & sErrFixedText)
                                            Else
                                                oFile.WriteLine(sLine)
                                            End If
                                            oPayment.E_Account = Trim$(oPayment.E_Account)
                                            If (Len(oPayment.E_Account) > 5 And Len(oPayment.E_Account) < 11 And vbIsNumeric(oPayment.E_Account, "0123456789")) Then
                                                sLine = ":CB:" & oPayment.E_Account          ' Account receiver
                                                oFile.WriteLine(sLine)
                                            Else
                                                Err.Raise(1000, "DoTheExport_SEBScreen", "Illegal or no credit account number stated on the payment." & vbCrLf & vbCrLf _
                                                   & "Credit account number: " & oPayment.E_Account & vbCrLf _
                                                   & sErrFixedText)
                                            End If
                                            If Not EmptyString(oPayment.MON_InvoiceCurrency) Then
                                                'Only EUR allowed
                                                If oPayment.MON_InvoiceCurrency = "EUR" Then
                                                    '02.12.2009 - New code
                                                    sLine = ":CC:" & oPayment.MON_InvoiceCurrency & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, , ",")  ' Currency/amount
                                                    'Old code
                                                    'sLine = ":CC:" & oPayment.MON_InvoiceCurrency & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ",", ".")  ' Currency/amount
                                                    oFile.WriteLine(sLine)
                                                Else
                                                    Err.Raise(1000, "DoTheExport_SEBScreen", "Illegal currency stated on the payment." & vbCrLf & vbCrLf _
                                                       & "Currency: " & oPayment.MON_InvoiceCurrency & vbCrLf _
                                                        & sErrFixedText)
                                                End If
                                            Else
                                                'Assume EUR
                                                '02.12.2009 - New code
                                                sLine = ":CC:EUR" & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, , ",")  ' Currency/amount
                                                'Old code
                                                'sLine = ":CC:EUR" & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ",", ".")  ' Currency/amount
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Not EmptyString(sSwift) Then
                                                sLine = ":CE:" & sSwift   ' Debitbank SWIFT
                                            Else
                                                sLine = ":CE:" & oPayment.BANK_I_SWIFTCode   ' Debitbank SWIFT
                                            End If
                                            oFile.WriteLine(sLine)
                                            If Len(oPayment.I_Account) < 11 Then
                                                sLine = ":CF:" & oPayment.I_Account   ' Debitaccount.
                                                oFile.WriteLine(sLine)
                                            Else
                                                Err.Raise(1000, "DoTheExport_SEBScreen", "Illegal debit account number stated on the payment." & vbCrLf & vbCrLf _
                                                   & "Debit account number: " & oPayment.I_Account & vbCrLf _
                                                   & sErrFixedText)
                                            End If
                                            If Not EmptyString(oPayment.E_Name) Then
                                                sLine = ":DA:" & Left$(oPayment.E_Name, 27)
                                                oFile.WriteLine(sLine)
                                            Else
                                                Err.Raise(1000, "DoTheExport_SEBScreen", "No beneficiary name stated on the payment. It's mandatory." & vbCrLf & vbCrLf _
                                                   & sErrFixedText)
                                            End If

                                            If Len(sFreetext) > 108 Then
                                                If oBabel.VB_ProfileInUse Then
                                                    sFreetext = DiminishFreetext(sFreetext, 108, sErrFixedText)
                                                Else
                                                    sFreetext = Left$(sFreetext, 108)
                                                End If
                                            End If

                                            '25.01.2010 - Changed length from 27 to 26, and add 2 in front of each line.
                                            If Len(sFreetext) > 0 Then
                                                sLine = ":DB:2" & Left$(sFreetext, 26)
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Len(sFreetext) > 26 Then
                                                sLine = "2" & Mid$(sFreetext, 27, 26)
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Len(sFreetext) > 54 Then
                                                sLine = "2" & Mid$(sFreetext, 53, 26)
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Len(sFreetext) > 81 Then
                                                sLine = "2" & Mid$(sFreetext, 79, 26)
                                                oFile.WriteLine(sLine)
                                            End If
                                            sLine = ":DC:" & "51000"        ' Textcode - ??????
                                            oFile.WriteLine(sLine)

                                            nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
                                            nFileNoRecords = nFileNoRecords + 1

                                            oPayment.Exported = True

                                            If Not bFooterWritten Then
                                                ' Trailer part
                                                sFooter = ":Z1:BB_NoOfRecords" & vbCrLf
                                                sFooter = sFooter & ":Z2:BB_FileSumAmount"
                                                bFooterWritten = True
                                                ' - Trailer end -
                                            End If

                                        Case "UeberwissungSammler"

                                            'Case bBACSExists Then

                                        Case "Chips"

                                            'NBNBNBNBNBNB Must create a trailer and a new header when there is a change in debitaccount!
                                            If Not bHeaderWritten Then
                                                bFooterWritten = False
                                                ' Write headerpart;
                                                sLine = ":E1:" & "D"        ' Debit/Credit flag
                                                oFile.WriteLine(sLine)
                                                ' Try to find swiftcode from accountstable;
                                                If Len(sSwift) > 0 Then
                                                    sLine = ":E2:" & sSwift  ' Swiftcode of receiving bank
                                                Else
                                                    Err.Raise(1000, "DoTheExport_SEBScreen", "WriteSEBScreen", "No SWIFT-address found for the debit bankaccount." & vbCrLf & "Please enter the SWIFT-address in the Client Setup." & vbCrLf _
                                                   & sErrFixedText)
                                                End If
                                                oFile.WriteLine(sLine)
                                                If EmptyString(oPayment.I_Account) Then
                                                    Err.Raise(1000, "DoTheExport_SEBScreen", "No debit account stated on the payments." & vbCrLf & vbCrLf _
                                                   & sErrFixedText)
                                                ElseIf Len(Trim$(oPayment.I_Account)) > 8 Then
                                                    Err.Raise(1000, "DoTheExport_SEBScreen", "Illegal debit account number stated on the payments." & vbCrLf & vbCrLf _
                                                       & "Debit account number: " & Trim$(oPayment.I_Account) & vbCrLf _
                                                        & sErrFixedText)
                                                Else
                                                    sLine = ":E3:" & "S" & Trim$(oPayment.I_Account) ' Single + debit accoutn number
                                                    oFile.WriteLine(sLine)
                                                End If
                                                bHeaderWritten = True
                                            End If

                                            bUSWriteBankNameAddress = True
                                            bUSIntermediaryBankIsMandatory = False
                                            If Len(oPayment.BANK_BranchNo) > 0 And Len(oPayment.BANK_BranchNo) < 12 Then
                                                If oPayment.BANK_BranchType = BabelFiles.BankBranchType.Chips Or oPayment.BANK_BranchType = BabelFiles.BankBranchType.Fedwire Then
                                                    bUSWriteBankNameAddress = False
                                                    bUSIntermediaryBankIsMandatory = False
                                                    sLine = ":CA:" & oPayment.BANK_BranchNo
                                                    oFile.WriteLine(sLine)
                                                ElseIf oPayment.BANK_BranchType = BabelFiles.BankBranchType.SWIFT Then
                                                    bUSWriteBankNameAddress = False
                                                    bUSIntermediaryBankIsMandatory = True
                                                    sLine = ":CA:" & oPayment.BANK_SWIFTCode
                                                    oFile.WriteLine(sLine)
                                                End If
                                            End If

                                            'Credit account
                                            If Len(Trim$(oPayment.E_Account)) > 0 And Len(Trim$(oPayment.E_Account)) < 16 Then
                                                sLine = ":CB:" & Trim$(oPayment.E_Account)
                                                oFile.WriteLine(sLine)
                                            Else
                                                Err.Raise(1000, "DoTheExport_SEBScreen", "Illegal or no credit account number stated on the payment." & vbCrLf & vbCrLf _
                                                   & "Credit account number: " & oPayment.E_Account & vbCrLf _
                                                   & sErrFixedText)
                                            End If

                                            '02.12.2009 - New code
                                            sLine = ":CC:" & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, , ",")
                                            'Old code
                                            'sLine = ":CC:" & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ",", ".")
                                            oFile.WriteLine(sLine)

                                            If bUSWriteBankNameAddress Then
                                                If Not EmptyString(oPayment.BANK_Name) And Not EmptyString(oPayment.BANK_Adr1 & oPayment.BANK_Adr2 & oPayment.BANK_Adr3) Then
                                                    bUSIntermediaryBankIsMandatory = True
                                                    sLine = ":CD:" & oPayment.BANK_Name
                                                    oFile.WriteLine(sLine)
                                                    sLine = ":CE:" & Left$(Trim$(oPayment.BANK_Adr1) & " " & Trim$(oPayment.BANK_Adr2) & " " & Trim$(oPayment.BANK_Adr3), 35)
                                                    oFile.WriteLine(sLine)
                                                Else
                                                    If Not EmptyString(oPayment.BANK_BranchNo) Then
                                                        sErrText = oPayment.BANK_BranchNo
                                                    Else
                                                        sErrText = oPayment.BANK_SWIFTCode
                                                    End If
                                                    Err.Raise(1000, "DoTheExport_SEBScreen", "You must either state a credit account bank code " & vbCrLf & _
                                                      " or bankname and address when doing a CHIPS or FED payment." & vbCrLf & vbCrLf _
                                                        & "Credit account bank code: " & sErrText _
                                                        & "Credit bankname: " & oPayment.BANK_Name _
                                                        & "Credit bankaddress: " & oPayment.BANK_Adr1 & oPayment.BANK_Adr2 & oPayment.BANK_Adr3 & vbCrLf _
                                                        & sErrFixedText)
                                                End If
                                            End If

                                            If Not EmptyString(oPayment.E_Name) Then
                                                sLine = ":GA:" & oPayment.E_Name
                                                oFile.WriteLine(sLine)
                                            Else
                                                Err.Raise(1000, "DoTheExport_SEBScreen", "Benficiary's name are not stated on the payment. Name is mandatory" & vbCrLf & vbCrLf _
                                                   & sErrFixedText)
                                            End If

                                            If Len(sFreetext) > 210 Then
                                                If oBabel.VB_ProfileInUse Then
                                                    sFreetext = DiminishFreetext(sFreetext, 210, sErrFixedText)
                                                Else
                                                    sFreetext = Left$(sFreetext, 210)
                                                End If
                                            End If

                                            If Len(sFreetext) > 0 Then
                                                sLine = ":GB:" & Left$(sFreetext, 35)
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Len(sFreetext) > 35 Then
                                                sLine = Mid$(sFreetext, 36, 35)
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Len(sFreetext) > 70 Then
                                                sLine = Mid$(sFreetext, 71, 35)
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Len(sFreetext) > 105 Then
                                                sLine = Mid$(sFreetext, 106, 35)
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Len(sFreetext) > 140 Then
                                                sLine = ":GB:" & Mid$(sFreetext, 141, 35)
                                                oFile.WriteLine(sLine)
                                            End If
                                            If Len(sFreetext) > 175 Then
                                                sLine = Mid$(sFreetext, 176, 35)
                                                oFile.WriteLine(sLine)
                                            End If

                                            If bUSIntermediaryBankIsMandatory Then
                                                If Not EmptyString(oPayment.BANK_BranchNoCorrBank) Or Not EmptyString(oPayment.BANK_SWIFTCodeCorrBank) Then
                                                    If Not EmptyString(oPayment.BANK_BranchNoCorrBank) Then
                                                        sLine = ":HA:" & Trim$(oPayment.BANK_BranchNoCorrBank)
                                                        oFile.WriteLine(sLine)
                                                    Else
                                                        sLine = ":HA:" & Trim$(oPayment.BANK_SWIFTCodeCorrBank)
                                                        oFile.WriteLine(sLine)
                                                    End If
                                                ElseIf Not EmptyString(oPayment.BANK_NameAddressCorrBank1) And Not EmptyString(oPayment.BANK_NameAddressCorrBank2 & oPayment.BANK_NameAddressCorrBank3) Then
                                                    sLine = ":HD:" & Trim$(oPayment.BANK_NameAddressCorrBank1)
                                                    oFile.WriteLine(sLine)
                                                    sLine = ":HE:" & Left$(Trim$(oPayment.BANK_NameAddressCorrBank2) & " " & Trim$(oPayment.BANK_NameAddressCorrBank3), 35)
                                                    oFile.WriteLine(sLine)
                                                Else
                                                    Err.Raise(1000, "DoTheExport_SEBScreen", "No bank code are stated on this payment," & vbCrLf & _
                                                      "therefore intermediary bankinformation are mandatory." & vbCrLf & _
                                                      "However no intermediary bankinformation are found." & vbCrLf & vbCrLf _
                                                        & sErrFixedText)
                                                End If
                                            End If

                                            nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
                                            nFileNoRecords = nFileNoRecords + 1

                                            oPayment.Exported = True

                                            If Not bFooterWritten Then
                                                ' Trailer part
                                                sFooter = ":Z1:BB_NoOfRecords" & vbCrLf
                                                sFooter = sFooter & ":Z2:BB_FileSumAmount"
                                                bFooterWritten = True
                                                ' - Trailer end -
                                            End If

                                            'Case bPMJFinlandExists Then
                                            'Case bFrenchVirementsExists Then
                                            'Case bFrenchPrelevementsExists Then
                                            'Case bFrenchLCRExists Then
                                            'Case bDanishDomesticExists Then

                                        Case "OtherBank"
                                            'bx = DoTheExport(oBabelFiles, bMultiFiles, Replace(sModifiedFilenameOut, "XXXYYYZZZ", "_OtherBank"), iFormat_ID, sI_Account, sOwnRef)

                                    End Select

                                End If 'bExporttopayment
                            Next ' payment
                        End If
                    Next 'batch

                End If

            Next


            If Not EmptyString(sFooter) Then
                If InStr(1, sFooter, "BB_NoOfRecords", vbTextCompare) > 0 Then
                    sFooter = Replace(sFooter, "BB_NoOfRecords", LTrim(Str(nFileNoRecords)))
                End If
                If InStr(1, sFooter, "BB_FileSumAmount", vbTextCompare) > 0 Then
                    '02.12.2009 - New code
                    sFooter = Replace(sFooter, "BB_FileSumAmount", ConvertFromAmountToString(nFileSumAmount, , ","))
                    'Old code
                    'sFooter = Replace(sFooter, "BB_FileSumAmount", Replace(LTrim(Str(nFileSumAmount / 100)), ".", ","))
                End If
                oFile.WriteLine(sFooter)
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: DoTheExport" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        DoTheExport = True

    End Function


    Function WriteSEBScreen_International(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sDate As String
        Dim i As Short
        Dim sFreetext As String
        Dim iInvoiceCounter As Short
        Dim bFoundClient As Boolean
        Dim sOldAccount As String
        Dim oaccount As Account
        Dim oClient As Client
        Dim sSwift As String

        Try

            bAppendFile = False

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            nFileSumAmount = 0
            nFileNoRecords = 0

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    bFoundClient = False
                                    If oPayment.I_Account <> sOldAccount Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                            For Each oaccount In oClient.Accounts
                                                If oaccount.Account = oPayment.I_Account Then
                                                    sSwift = oaccount.SWIFTAddress
                                                    sOldAccount = oPayment.I_Account
                                                    bFoundClient = True
                                                    Exit For
                                                End If
                                            Next oaccount
                                            If bFoundClient Then
                                                Exit For
                                            End If
                                        Next oClient
                                    End If
                                    oPayment.BANK_I_SWIFTCode = sSwift
                                    '-------- fetch content of each payment ---------
                                    ' Total up freetext from all invoices;
                                    sFreetext = ""
                                    For Each oInvoice In oPayment.Invoices
                                        For Each oFreeText In oInvoice.Freetexts
                                            sFreetext = sFreetext & Trim(oFreeText.Text)
                                        Next oFreeText
                                    Next oInvoice

                                    If oPayment.PayType = "I" Then
                                        sLine = ":20:" & Left(oPayment.REF_Own, 12) ' Transaction reference
                                        oFile.WriteLine((sLine))
                                        sLine = ":30:" & Mid(oPayment.DATE_Payment, 3) ' Executin date, YYMMDD
                                        oFile.WriteLine((sLine))
                                        sLine = ":32B:" & oPayment.MON_InvoiceCurrency & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") ' Currency/amount
                                        oFile.WriteLine((sLine))
                                        sLine = ":50:" & oPayment.I_Account ' Debitaccount. Does not set SWIFT. OK if debitacc in SEB
                                        If Len(oPayment.BANK_I_SWIFTCode) > 0 Then
                                            ' add Debit Swiftcode
                                            sLine = sLine & "/" & oPayment.BANK_I_SWIFTCode
                                        End If
                                        oFile.WriteLine((sLine))
                                        If Len(oPayment.BANK_NameAddressCorrBank1) > 0 Then
                                            sLine = ":54A:" & oPayment.BANK_SWIFTCodeCorrBank
                                            oFile.WriteLine((sLine))
                                            sLine = ":54D" & oPayment.BANK_NameAddressCorrBank1
                                            oFile.WriteLine((sLine))
                                            sLine = Left(oPayment.BANK_NameAddressCorrBank2 & oPayment.BANK_NameAddressCorrBank3 & oPayment.BANK_NameAddressCorrBank4, 35)
                                            oFile.WriteLine((sLine))
                                        End If
                                        sLine = ":57A:/" & oPayment.E_Account
                                        oFile.WriteLine((sLine))
                                        sLine = oPayment.BANK_SWIFTCode
                                        oFile.WriteLine((sLine))
                                        If Len(oPayment.BANK_BranchNo) > 0 Then
                                            sLine = oPayment.BANK_BranchType & oPayment.BANK_BranchNo
                                            oFile.WriteLine((sLine))
                                        End If
                                        sLine = ":59:" & oPayment.E_Name
                                        oFile.WriteLine((sLine))
                                        If Len(oPayment.E_Adr1) > 0 Then
                                            sLine = oPayment.E_Adr1
                                            oFile.WriteLine((sLine))
                                        End If
                                        If Len(oPayment.E_Adr2) > 0 Then
                                            sLine = oPayment.E_Adr2
                                            oFile.WriteLine((sLine))
                                        End If
                                        If Len(oPayment.E_Adr3) > 0 Then
                                            sLine = oPayment.E_Adr3
                                            oFile.WriteLine((sLine))
                                        End If
                                        If Len(sFreetext) > 0 Then
                                            sLine = ":70:" & Left(sFreetext, 35)
                                            oFile.WriteLine((sLine))
                                        End If
                                        If Len(sFreetext) > 35 Then
                                            sLine = Mid(sFreetext, 36, 35)
                                            oFile.WriteLine((sLine))
                                        End If
                                        If Len(sFreetext) > 70 Then
                                            sLine = Mid(sFreetext, 71, 35)
                                            oFile.WriteLine((sLine))
                                        End If
                                        If Len(sFreetext) > 105 Then
                                            sLine = Mid(sFreetext, 106, 35)
                                            oFile.WriteLine((sLine))
                                        End If
                                        ' Charges - if OUR/OUR = OUR, BEN/BEN=BEN, all other:SHA
                                        If oPayment.MON_ChargeMeDomestic = True And oPayment.MON_ChargeMeAbroad = True Then
                                            sLine = ":71A:" & "OUR"
                                            oFile.WriteLine((sLine))
                                        ElseIf oPayment.MON_ChargeMeDomestic = False And oPayment.MON_ChargeMeAbroad = False Then
                                            sLine = ":71A:" & "BEN"
                                            oFile.WriteLine((sLine))
                                        Else
                                            sLine = ":71A:" & "SHA"
                                            oFile.WriteLine((sLine))
                                        End If

                                        ' Construct Instructions to debit accoun servicing institution
                                        ' Some fixed codes, some freetext
                                        sLine = ""
                                        If oPayment.Priority Then
                                            sLine = "PRI=E"
                                        End If

                                        ' Reporting (statebank)
                                        ' No reporting required in Denmark !!
                                        '                            If oPayment.Invoices.Count > 0 Then
                                        '                                ' Assumes that all invoices has same reporting, use Invoice(1)
                                        '                                If Len(oPayment.Invoices(1).STATEBANK_Code) > 0 Then
                                        '                                    sLine = sLine & " CBR PP=" & oPayment.Invoices(1).STATEBANK_Code
                                        '                                    If Len(oPayment.E_CountryCode) > 0 Then
                                        '                                        sLine = sLine & " CY=" & oPayment.E_CountryCode
                                        '                                    End If
                                        '                                End If
                                        '                                If Len(oPayment.Invoices(1).STATEBANK_DATE) > 0 Then
                                        '                                    sLine = sLine & " LN=" & oPayment.Invoices(1).STATEBANK_DATE
                                        '                                End If
                                        '                            End If
                                        If Len(sLine) > 0 Then
                                            sLine = ":72:" & Trim(sLine)
                                            oFile.WriteLine((sLine))
                                        End If


                                        '                            If oPayment.Invoices.Count > 0 Then
                                        '                                If Len(oPayment.Invoices(1).STATEBANK_Text) > 0 Then
                                        '                                    If Left$(sLine, 4) <> ":72:" Then
                                        '                                        ' Not written labelno 72: yet
                                        '                                        sLine = ":72:"
                                        '                                    Else
                                        '                                        sLine = ""
                                        '                                    End If
                                        '                                    sLine = sLine & Left$(oPayment.Invoices(1).STATEBANK_Text, 35)
                                        '                                    oFile.WriteLine (sLine)
                                        '                                End If
                                        '                                If Len(oPayment.Invoices(1).STATEBANK_Text) > 35 Then
                                        '                                    sLine = Mid$(oPayment.Invoices(1).STATEBANK_Text, 36, 35)
                                        '                                    oFile.WriteLine (sLine)
                                        '                                End If
                                        '                            End If

                                        nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
                                        nFileNoRecords = nFileNoRecords + 1

                                        oPayment.Exported = True

                                    Else
                                        'Sorry!
                                    End If

                                End If 'bExporttopayment
                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch

                End If

            Next oBabel
            ' Trailer part
            sLine = ":Z1:" & LTrim(Str(nFileNoRecords))
            oFile.WriteLine((sLine))
            sLine = ":Z2:" & Replace(LTrim(Str(nFileSumAmount / 100)), ".", ",")
            oFile.WriteLine((sLine))
            ' - Trailer end -

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteSEBScreen_International" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteSEBScreen_International = True

    End Function
    Function WriteSEBScreen_DK(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sDate As String
        Dim i As Short
        Dim sFreetext As String
        Dim iInvoiceCounter As Short
        Dim bFoundClient As Boolean
        Dim sOldAccount As String
        Dim oaccount As Account
        Dim oClient As Client
        Dim sSwift As String

        Try

            bAppendFile = False

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            nFileSumAmount = 0
            nFileNoRecords = 0

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    ' Total up freetext from all invoices;
                                    sFreetext = ""
                                    For Each oInvoice In oPayment.Invoices
                                        For Each oFreeText In oInvoice.Freetexts
                                            sFreetext = sFreetext & Trim(oFreeText.Text)
                                        Next oFreeText
                                    Next oInvoice

                                    If oPayment.PayType <> "I" Then
                                        sLine = ":BDT:" ' Recogniser tag
                                        oFile.WriteLine((sLine))
                                        sLine = ":20:" & Trim(oPayment.REF_Own) ' Own reference
                                        oFile.WriteLine((sLine))
                                        sLine = ":30:" & Mid(oPayment.DATE_Payment, 3) ' Execution date, YYMMDD
                                        oFile.WriteLine((sLine))
                                        sLine = ":32B:" & oPayment.MON_InvoiceCurrency & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") ' Currency/amount
                                        oFile.WriteLine((sLine))
                                        If Len(oPayment.E_Account) = 0 Then
                                            sLine = ":33:" & "1" 'Cheque
                                        ElseIf oPayment.PayCode = "301" Then
                                            ' FI/Giro-type:
                                            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            sLine = ":33:" & Left(oPayment.Invoices(1).Unique_Id, 2) 'FI/Giro-card
                                        Else
                                            ' Next lines changed 12.01.06 from
                                            ' sLine = ":33:" & "0"        'With advise
                                            If oPayment.Priority Then
                                                sLine = ":33:" & "0" 'With advise
                                            Else
                                                sLine = ":33:" & "2" 'Without advise, normal
                                            End If
                                        End If
                                        oFile.WriteLine((sLine))
                                        sLine = ":50:" & oPayment.I_Account ' Debitaccount.
                                        oFile.WriteLine((sLine))
                                        If Len(oPayment.E_Account) > 0 Then
                                            sLine = ":57A:" & oPayment.E_Account
                                            oFile.WriteLine((sLine))
                                        End If
                                        sLine = ":59:" & oPayment.E_Name
                                        oFile.WriteLine((sLine))
                                        If Len(oPayment.E_Account) = 0 Then
                                            ' Adr only if cheque
                                            If Len(oPayment.E_Adr1) > 0 Then
                                                sLine = Left(oPayment.E_Adr1, 33)
                                            Else
                                                sLine = "-"
                                            End If
                                            oFile.WriteLine((sLine))
                                            If Len(oPayment.E_Adr2) > 0 Then
                                                sLine = Left(oPayment.E_Adr2, 33)
                                            Else
                                                sLine = "-"
                                            End If
                                            oFile.WriteLine((sLine))
                                            If Len(oPayment.E_Zip) > 0 Then
                                                sLine = Left(oPayment.E_Zip, 4)
                                            Else
                                                sLine = "-"
                                            End If
                                            oFile.WriteLine((sLine))
                                            If Len(oPayment.E_City) > 0 Then
                                                sLine = Left(oPayment.E_City, 33)
                                            Else
                                                sLine = "-"
                                            End If
                                            oFile.WriteLine((sLine))
                                        End If

                                        If Len(sFreetext) > 0 Then
                                            sLine = ":70:" & Left(sFreetext, 35)
                                            oFile.WriteLine((sLine))
                                        End If
                                        If Len(sFreetext) > 35 Then
                                            sLine = Mid(sFreetext, 36, 35)
                                            oFile.WriteLine((sLine))
                                        End If
                                        If Len(sFreetext) > 70 Then
                                            sLine = Mid(sFreetext, 71, 35)
                                            oFile.WriteLine((sLine))
                                        End If
                                        If Len(sFreetext) > 105 Then
                                            sLine = Mid(sFreetext, 106, 35)
                                            oFile.WriteLine((sLine))
                                        End If
                                        If oPayment.PayCode = "301" Then
                                            ''' sjekk!
                                            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            sLine = ":THD:" & Mid(oPayment.Invoices(1).Unique_Id, 3) ' OCR-ref
                                            oFile.WriteLine((sLine))
                                        End If

                                        nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
                                        nFileNoRecords = nFileNoRecords + 1

                                        oPayment.Exported = True

                                    Else
                                        'Sorry!
                                    End If

                                End If 'bExporttopayment
                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch

                End If

            Next oBabel
            ' Trailer part
            sLine = ":Z1:" & LTrim(Str(nFileNoRecords))
            oFile.WriteLine((sLine))
            sLine = ":Z2:" & Replace(LTrim(Str(nFileSumAmount / 100)), ".", ",")
            oFile.WriteLine((sLine))
            ' - Trailer end -

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteSEBScreen_DK" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteSEBScreen_DK = True

    End Function

    Function WriteSEBScreen_Germany(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sDate As String
        Dim i As Short
        Dim sFreetext As String
        Dim iInvoiceCounter As Short
        Dim bFoundClient As Boolean
        Dim sOldAccount As String
        Dim oaccount As Account
        Dim oClient As Client
        Dim sSwift As String

        Try
            bAppendFile = False

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            nFileSumAmount = 0
            nFileNoRecords = 0

            ' Write headerpart;
            sLine = ":E1:" & "D" ' Debit/Credit flag
            oFile.WriteLine((sLine))
            ' Try to find swiftcode from accountstable;
            'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles(1).VB_Profile.FileSetups(iFormat_ID).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For Each oClient In oBabelFiles(1).VB_Profile.FileSetups(iFormat_ID).Clients
                For Each oaccount In oClient.Accounts
                    sSwift = oaccount.SWIFTAddress
                    If Len(sSwift) > 0 And Left(sSwift, 8) = "ESSEDEFF" Then
                        Exit For
                    End If
                Next oaccount
                If Len(sSwift) > 0 Then
                    Exit For
                End If
            Next oClient
            If Len(sSwift) > 0 Then
                sLine = ":E2:" & sSwift ' Swiftcode of receiving bank
            Else
                sLine = ":E2:" & "ESSEDEFFXXX' Find SWIFT from accountstable. If not set, use Frankfurt?"
            End If
            oFile.WriteLine((sLine))
            sLine = ":E3:" & "S" ' Single
            oFile.WriteLine((sLine))
            ' - Header end -

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else



                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    bFoundClient = False
                                    If oPayment.I_Account <> sOldAccount Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.VB_Profile.FileSetups(iFormat_ID).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                            For Each oaccount In oClient.Accounts
                                                If oaccount.Account = oPayment.I_Account Then
                                                    sSwift = oaccount.SWIFTAddress
                                                    sOldAccount = oPayment.I_Account
                                                    bFoundClient = True
                                                    Exit For
                                                End If
                                            Next oaccount
                                            If bFoundClient Then
                                                Exit For
                                            End If
                                        Next oClient
                                    End If
                                    oPayment.BANK_I_SWIFTCode = sSwift
                                    '-------- fetch content of each payment ---------
                                    ' Total up freetext from all invoices;
                                    sFreetext = ""
                                    For Each oInvoice In oPayment.Invoices
                                        For Each oFreeText In oInvoice.Freetexts
                                            sFreetext = sFreetext & Trim(oFreeText.Text)
                                        Next oFreeText
                                    Next oInvoice

                                    'If oPayment.PayType <> "I" Then
                                    If Len(oPayment.BANK_BranchNo) > 0 Then
                                        sLine = ":CA:" & oPayment.BANK_BranchNo ' BLZ
                                    Else
                                        If Left(oPayment.BANK_Name, 4) = "//BL" Or Left(oPayment.BANK_Name, 2) = "BL" Or Left(oPayment.BANK_Name, 2) = "//" Then
                                            sLine = ":CA:" & Replace(Replace(oPayment.BANK_Name, "BL", ""), "//", "") ' BLZ, try to find in bankname
                                        Else
                                            sLine = ":CA:" & "Unknown BLZ"
                                        End If
                                    End If
                                    oFile.WriteLine((sLine))
                                    sLine = ":CB:" & Trim(oPayment.E_Account) ' Account receiver
                                    oFile.WriteLine((sLine))
                                    sLine = ":CC:" & oPayment.MON_InvoiceCurrency & Replace(LTrim(Str(oPayment.MON_InvoiceAmount / 100)), ".", ",") ' Currency/amount
                                    oFile.WriteLine((sLine))
                                    sLine = ":CE:" & oPayment.BANK_I_SWIFTCode ' Debitbank SWIFT
                                    oFile.WriteLine((sLine))
                                    sLine = ":CF:" & oPayment.I_Account ' Debitaccount.
                                    oFile.WriteLine((sLine))
                                    sLine = ":DA:" & oPayment.E_Name
                                    oFile.WriteLine((sLine))

                                    If Len(sFreetext) > 0 Then
                                        sLine = ":DB:2" & Left(sFreetext, 27)
                                        oFile.WriteLine((sLine))
                                    End If
                                    If Len(sFreetext) > 27 Then
                                        sLine = "2" & Mid(sFreetext, 28, 27)
                                        oFile.WriteLine((sLine))
                                    End If
                                    If Len(sFreetext) > 54 Then
                                        sLine = "2" & Mid(sFreetext, 55, 27)
                                        oFile.WriteLine((sLine))
                                    End If
                                    If Len(sFreetext) > 81 Then
                                        sLine = "2" & Mid(sFreetext, 82, 27)
                                        oFile.WriteLine((sLine))
                                    End If
                                    sLine = ":DC:" & "51000" ' Textcode - ??????
                                    oFile.WriteLine((sLine))

                                    nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
                                    nFileNoRecords = nFileNoRecords + 1

                                    oPayment.Exported = True

                                    'Else
                                    'Sorry!
                                    'End If

                                End If 'bExporttopayment
                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch

                End If

            Next oBabel

            ' Trailer part
            sLine = ":Z1:" & LTrim(Str(nFileNoRecords))
            oFile.WriteLine((sLine))
            sLine = ":Z2:" & Replace(LTrim(Str(nFileSumAmount / 100)), ".", ",")
            oFile.WriteLine((sLine))
            ' - Trailer end -

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteSEBScreen_Germany" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteSEBScreen_Germany = True

    End Function


    Private Function AddSEBScreenPaymentTypeToThePayment(ByRef oPayment As vbBabel.Payment) As Boolean

        ' 09.03.2009, added next line for VingCard
        oPayment.PayTypeSetInImport = False

        If Left(oPayment.BANK_I_SWIFTCode, 4) <> "ESSE" Then
            bAccountInOtherBankExists = True
            oPayment.DnBNORTBIPayType = "OtherBank"
            'Add a clientno to use for exporting the payments from an account in another bank.
            ' The export will only be done for clientno = "9999999"
            oPayment.VB_ClientNo = "9999999"
        Else
            If Not IsPaymentDomestic(oPayment, oPayment.BANK_I_SWIFTCode) Then
                bInternationalPaymentsExists = True
                oPayment.DnBNORTBIPayType = "International"
            Else
                Select Case Mid(oPayment.BANK_I_SWIFTCode, 5, 2)

                    Case "SE"
                        bSwedishBGCPaymentsExists = True
                        oPayment.DnBNORTBIPayType = "SwedishBGC"

                    Case "DE"
                        bUeberwissungExists = True
                        oPayment.DnBNORTBIPayType = "Ueberwissung"

                    Case "US"
                        bChipsAndFedsExists = True
                        oPayment.DnBNORTBIPayType = "Chips"

                    Case Else
                        'Not implemented yet
                        '        bInternalTransfersExists = False oPayment.DnBNORTBIPayType = "Internal"
                        '        bAdviceToReceiveExists = False oPayment.DnBNORTBIPayType = "Advice"
                        '        bUeberwissungSammlerExists = False oPayment.DnBNORTBIPayType = "UeberwissungSammler"
                        '        bBACSExists = False oPayment.DnBNORTBIPayType =
                        '        bCHAPSExists = False oPayment.DnBNORTBIPayType =
                        '        bPMJFinlandExists = False oPayment.DnBNORTBIPayType =
                        '        bFrenchVirementsExists = False oPayment.DnBNORTBIPayType =
                        '        bFrenchPrelevementsExists = False oPayment.DnBNORTBIPayType =
                        '        bFrenchLCRExists = False oPayment.DnBNORTBIPayType =
                        '        bDanishDomesticExists = False oPayment.DnBNORTBIPayType =

                End Select
            End If
        End If


    End Function
    Public Function DiminishFreetext(ByRef sFreetext As String, ByRef iAllowedLength As Short, Optional ByRef sPaymentInfoToBeShown As String = "") As String
        Dim frmDiminishFreetext As New frmDiminishFreetext

        FormvbStyle(frmDiminishFreetext, "")

        frmDiminishFreetext.Text = LRSCommon(64045)
        frmDiminishFreetext.lblTextWithinLimit.Text = LRSCommon(64046)
        frmDiminishFreetext.lblTextOutsideLimit.Text = LRSCommon(64047)
        frmDiminishFreetext.lblExplaination.Text = LRSCommon(64049, Trim(Str(iAllowedLength)))
        frmDiminishFreetext.txtTextWithinLimit.Maxlength = iAllowedLength
        frmDiminishFreetext.txtTextWithinLimit.Text = Left(sFreetext, iAllowedLength)
        frmDiminishFreetext.txtTextOutsideLimit.Text = Mid(sFreetext, iAllowedLength + 1)
        frmDiminishFreetext.lblPaymentInfo.Text = LRSCommon(64048) & vbCrLf & sPaymentInfoToBeShown
        frmDiminishFreetext.ShowDialog()

        If Not frmDiminishFreetext.bCancelled Then
            DiminishFreetext = frmDiminishFreetext.txtTextWithinLimit.Text
        Else
            DiminishFreetext = sFreetext
        End If

    End Function
End Module
