Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("ClientHandled_NET.ClientHandled")> Public Class ClientHandled
	
	'local variable(s) to hold property value(s)
	'05.08.2008 - Changed from Variant to String
	Private sFile As String
	Private sClient As String
	Private sClientName As String
	Private sEmail As String
	
	'05.08.2008 - REMOVED next 2 variables  - LSKSIF3-1
	'Private mvarFileType As Variant 'local copy
	'Private mvarFileStatus As Variant 'local copy
	
	Private bClientToScreen As Boolean
	Private bClientToPrint As Boolean
	Private bClientToFile As Boolean
	Private bClientToMail As Boolean
	Private nIndex As Double
	
	'********* START PROPERTY SETTINGS ***********************
	Public Property ClientToMail() As Boolean
		Get
			ClientToMail = bClientToMail
		End Get
		Set(ByVal Value As Boolean)
			bClientToMail = Value
		End Set
	End Property
	Public Property Index() As Double
		Get
			Index = nIndex
		End Get
		Set(ByVal Value As Double)
			nIndex = Val(CStr(Value))
		End Set
	End Property
	Public Property ClientToFile() As Boolean
		Get
			ClientToFile = bClientToFile
		End Get
		Set(ByVal Value As Boolean)
			bClientToFile = Value
		End Set
	End Property
	Public Property ClientToPrint() As Boolean
		Get
			ClientToPrint = bClientToPrint
		End Get
		Set(ByVal Value As Boolean)
			bClientToPrint = Value
		End Set
	End Property
	Public Property ClientToScreen() As Boolean
		Get
			ClientToScreen = bClientToScreen
		End Get
		Set(ByVal Value As Boolean)
			bClientToScreen = Value
		End Set
	End Property
    Public Property EMail() As String
        Get
            EMail = sEmail
        End Get
        Set(ByVal Value As String)
            sEmail = Value
        End Set
    End Property
    Public Property Client() As String
        Get
            Client = sClient
        End Get
        Set(ByVal Value As String)
            sClient = Value
        End Set
    End Property
	Public Property ClientName() As String
		Get
			ClientName = sClientName
		End Get
		Set(ByVal Value As String)
			sClientName = Value
		End Set
	End Property
    Public Property File() As String
        Get
            File = sFile
        End Get
        Set(ByVal Value As String)
            sFile = Value
        End Set
    End Property
	'********* END PROPERTY SETTINGS ***********************
End Class
