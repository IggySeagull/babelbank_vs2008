Option Strict Off
Option Explicit On
Module WriteBankDataDK
	
	'Dim nNoOfTransactions As Double
	Dim nFileSumAmount As Double
	Dim nFileNoRecords As Short
	
	
    Function WriteBankdata_DK(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        ' NB: Version 4.0 of Bankdata

        '"IB000000000000","20051018","                                                                                          ","                                                                                                                                                                                                                                                               ","                                                                                                                                                                                                                                                               ","                                                                                                                                                                                                                                                               "
        '"IB030202000004","0001","20051018","0000000020100+","DKK","2"," 52950010008883","2","0948","8245517115","1","                                   ","Petronella Beata-Lucia Rosenberg","c/o Olga laurentzia Sofi Larsson","Nord�stra solskensgl�ntans v�g 4","1234","106 40 Stora Essingens Stockholm","Den h�r texten ska vara trettiofem ","tecken l�ng per rad. Testen ska ver","ifiera att Sven trunkerar i ok posi","tion och att kunden f�r en ok hint!","Den h�r texten ska vara trettiofem ","tecken l�ng per rad. Testen ska ver","ifiera att Sven trunkerar i ok posi","tion och att kunden f�r en ok hint!","Den h�r texten ska vara trettiofem ","tecken l�ng per rad. Testen e slut!"," ","                                                                                                                                                                                                                       "
        '"IB030202000004","0001","20051018","0000000020200+","EUR","2"," 52950010008883","2","6142","3221685767","1","                                   ","Petronella Rosenberg            ","solskensgl�ntans v�g 5          ","solskensgl�ntans v�g 5          ","1200","106 40 Stockholm                ","                                   ","                                   ","                                   ","                                   ","Den h�r texten b�rjar i DoP 5 och s","lutar i DoP 10. Massor med text m�s","ste skrivas in i f�lten f�r att jag","ska se att texten kommer ut som den","ska. Blablablablablablablablablabla","blablab. Nu slipper jag skriva mer."," ","                                                                                                                                                                                                                       "
        '"IB030202000004","0001","20051018","0000000020200+","DKK","2"," 52950010008883","2","2795","4312334114","1","                                   ","Li Ek                           ","solskensgl�ntan 64              ","solskensgl�ntan 64              ","0000","Stockholm                       ","Nu sn�ar det!                      ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   "," ","                                                                                                                                                                                                                       "
        '"IB030202000004","0001","20051018","0000000020300+","EUR","2"," 52950010008883","2","4277","9319865028","1","                                   ","!"#�%&/()=?`+\@�${[]}<>� Special","                                ","@�${[]}<>�"" v�g 8              ","1234","Specialstaden                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","Specialtext p� sista raden.        "," ","                                                                                                                                                                                                                       "
        '"IB030202000004","0001","20051018","0000000020400+","DKK","2"," 52950010008883","2","2486","5090061382","1","                                   ","      Bosse Blanksteg           ","     c/o Blankberg              ","       Blankstegsgatan 77       ","    ","       Blankstegsberg           ","     Blanka                        ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   "," ","                                                                                                                                                                                                                       "
        '"IB030202000004","0001","20051018","0000000020500+","EUR","2"," 52950010008883","2","4606","6184963785","1","                                   ","NN                              ","c/o N�gon                                ","N�gon gata 100         ","12  ","999 99 N�gonstans               ","N�got                              ","p�                                 ","alla                               ","tio                                ","olika                              ","details                            ","of                                 ","payment                            ","                                   ","                                   "," ","                                                                                                                                                                                                                       "
        '"IB030202000004","0001","20051018","0000000020600+","DKK","2"," 52950010008883","2","1359","8407636156"," ","                                   ","x                               ","                                ","                                ","    ","                                ","�                                  ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","rader                              ","i betalningen.                     "," ","                                                                                                                                                                                                                       "
        '"IB030202000004","0001","20051018","0000000020700+","EUR","2"," 52950010008883","2","9737","2601657549","1","                                   ","Petronella Beata-Lucia Rosenberg","Nord�stra solskensgl�ntans v�g 4","106 40 Stora Essingens Stockholm","4321","106 40 Stora Essingens Stockholm","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   ","                                   "," ","                                                                                                                                                                                                                       "
        '"IB999999999999","20051018","000008","0000000163000+","space_pad_64                                                    ","space_pad_255_1                                                                                                                                                                                                                                                ","space_pad_255_2                                                                                                                                                                                                                                                ","space_pad_255_3                                                                                                                                                                                                                                                "'

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sDate As String
        Dim i As Short
        Dim sFreetext As String
        Dim iInvoiceCounter As Short
        Dim bFileStartWritten As Boolean

        Try

            bAppendFile = False

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            bFileStartWritten = False
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shal
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then
                    If Not bFileStartWritten Then
                        ' write only once!
                        ' When merging Client-files, skip this one except for first client!
                        sLine = WriteBankdata_Start()
                        oFile.WriteLine((sLine))
                        bFileStartWritten = True
                    End If

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If Not bExportoBatch Then
                            bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then


                                    '-------- fetch content of each payment ---------
                                    ' Total up freetext from all invoices;
                                    sFreetext = ""
                                    For Each oInvoice In oPayment.Invoices
                                        For Each oFreeText In oInvoice.Freetexts
                                            sFreetext = sFreetext & " " & oFreeText.Text
                                        Next oFreeText
                                    Next oInvoice
                                    sFreetext = Trim(sFreetext)

                                    If oPayment.ToOwnAccount Then
                                        sLine = WriteBankdata_OwnAccount(oPayment)
                                        oFile.WriteLine((sLine))
                                        oPayment.Exported = True
                                    ElseIf oPayment.PayCode = "301" Then
                                        ' For FI/Giro-cards, must process one invoice at a time:
                                        For Each oInvoice In oPayment.Invoices
                                            sLine = WriteBankdata_Indbetalingskort(oPayment, oInvoice, sFreetext)
                                            oFile.WriteLine((sLine))
                                        Next oInvoice
                                        oPayment.Exported = True
                                    Else
                                        If oPayment.PayType <> "I" Then
                                            sLine = WriteBankdata_Indland(oPayment, sFreetext)
                                            oFile.WriteLine((sLine))
                                            oPayment.Exported = True
                                            ' Do not write recordtype Afsenderoplysninger og referencefelter (Index 2)

                                            ' If more freetext than 9*35, we must use Index 3 and 4
                                            If Len(Trim(sFreetext)) > (10 * 35) Then
                                                ' 22 lines a 35, index "0003"
                                                sLine = WriteBankdata_Adviseringslinier(Mid(sFreetext, 10 * 35 + 1, 22 * 35), "0003")
                                                oFile.WriteLine((sLine))
                                                If Len(sFreetext) > (10 * 35) + (22 * 35) Then
                                                    ' 22 lines a 35, index "0004"
                                                    sLine = WriteBankdata_Adviseringslinier(Mid(sFreetext, 10 * 35 + 22 * 35 + 1, 22 * 35), "0004")
                                                    oFile.WriteLine((sLine))
                                                End If
                                            End If

                                        Else
                                            'WriteBankdata_Udland oPayment, sFreetext
                                        End If
                                    End If

                                End If 'bExporttopayment
                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                    sLine = WriteBankdata_Slut()
                    oFile.WriteLine((sLine))
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteBankdata_DK" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteBankdata_DK = True

    End Function
	Private Function WriteBankdata_Start() As String
		Dim sLine As String
		
		' Reset file-totals:
		nFileSumAmount = 0
		nFileNoRecords = 0
		
		sLine = Chr(34) & "IB000000000000" & Chr(34) & "," ' 1-18
        sLine = sLine & Chr(34) & VB6.Format(Date.Today, "YYYYMMDD") & Chr(34) & ","
        sLine = sLine & Chr(34) & Space(90) & Chr(34) & ","
        sLine = sLine & Chr(34) & Space(255) & Chr(34) & ","
        sLine = sLine & Chr(34) & Space(255) & Chr(34) & ","
        sLine = sLine & Chr(34) & Space(255) & Chr(34)

        WriteBankdata_Start = sLine
    End Function
    Private Function WriteBankdata_OwnAccount(ByRef oPayment As Payment) As String
        Dim sLine As String

        ' Add to file-totals:
        nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
        nFileNoRecords = nFileNoRecords + 1

        sLine = Chr(34) & "IB030201000001" & Chr(34) & "," ' 1-18
        sLine = sLine & Chr(34) & "0001" & Chr(34) & "," ' 19 Index
        sLine = sLine & Chr(34) & oPayment.DATE_Payment & Chr(34) & "," ' 26 Expeditionsdato
        sLine = sLine & Chr(34) & PadLeft(Str(oPayment.MON_InvoiceAmount), 13, "0") & "+" & Chr(34) & "," '37 Trans-bel�p
        If Len(oPayment.MON_InvoiceCurrency) = 0 Then
            oPayment.MON_InvoiceCurrency = "DKK"
        End If
        sLine = sLine & Chr(34) & PadLeft(oPayment.MON_InvoiceCurrency, 3, " ") & Chr(34) & "," '54 M�ntsort
        sLine = sLine & Chr(34) & "2" & Chr(34) & "," ' 60 Fra-type "2" = Bankkonto
        sLine = sLine & Chr(34) & "0" & PadRight(oPayment.I_Account, 14, " ") & Chr(34) & "," ' 64 Kontonr afsender, plus ledende 0
        sLine = sLine & Chr(34) & Left(oPayment.E_Account, 4) & Chr(34) & "," ' 82 Regnr modtager
        sLine = sLine & Chr(34) & Mid(oPayment.E_Account, 5, 10) & Chr(34) & "," ' 89 Kontonr modtager
        ' Not in use for 4.0 sLine = sLine & Chr(34) & PadRight(oPayment.REF_Own, 35, " ") & Chr(34) & ","        ' 102 Eget bilagsnr

        sLine = sLine & Chr(34) & Space(215) & Chr(34) & "," ' 102
        sLine = sLine & Chr(34) & Space(58) & Chr(34) ' 320
        sLine = sLine & Chr(34) & Space(255) & Chr(34) ' 381
        sLine = sLine & Chr(34) & Space(255) & Chr(34) ' 639

        WriteBankdata_OwnAccount = sLine

    End Function
    Private Function WriteBankdata_Indland(ByRef oPayment As Payment, ByVal sFreetext As String) As String
        Dim sLine As String
        Dim i As Short

        ' Add to file-totals:
        nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
        nFileNoRecords = nFileNoRecords + 1

        sLine = Chr(34) & "IB030202000004" & Chr(34) & "," ' 1-18
        sLine = sLine & Chr(34) & "0001" & Chr(34) & "," ' 19 Index
        sLine = sLine & Chr(34) & oPayment.DATE_Payment & Chr(34) & "," ' 26 Expeditionsdato
        sLine = sLine & Chr(34) & PadLeft(Str(oPayment.MON_InvoiceAmount), 13, "0") & "+" & Chr(34) & "," '37 Trans-bel�p
        If Len(oPayment.MON_InvoiceCurrency) = 0 Then
            oPayment.MON_InvoiceCurrency = "DKK"
        End If
        sLine = sLine & Chr(34) & PadLeft(oPayment.MON_InvoiceCurrency, 3, " ") & Chr(34) & "," '54 M�ntsort
        sLine = sLine & Chr(34) & "2" & Chr(34) & "," ' 60 Fra-type "2" = Bankkonto
        sLine = sLine & Chr(34) & "0" & PadRight(oPayment.I_Account, 14, " ") & Chr(34) & "," ' 64 Kontonr afsender, plus ledende 0
        If Len(Trim(oPayment.E_Account)) = 0 Then
            sLine = sLine & Chr(34) & "1" & Chr(34) & "," ' 82 - "1" = Send check
        Else
            sLine = sLine & Chr(34) & "2" & Chr(34) & "," ' 82 - "2" = Overf�r til bankkonto
        End If
        sLine = sLine & Chr(34) & PadRight(Left(oPayment.E_Account, 4), 4, " ") & Chr(34) & "," ' 86 Regnr modtager
        sLine = sLine & Chr(34) & PadRight(Mid(oPayment.E_Account, 5, 10), 10, " ") & Chr(34) & "," ' 93 Kontonr modtager

        If oPayment.Cheque > BabelFiles.ChequeType.SendToReceiver Then
            sLine = sLine & Chr(34) & "2" & Chr(34) & "," ' 106 - "2" = Send check til modtager
        ElseIf oPayment.PayCode = "404" Then  ''' changed 07.2.05 Or Len(Trim$(sFreetext)) > 35 Then
            ' If "Betaling med melding", eller melding s� lang at vi ikke f�r plass p� 35 pos, i tekst kontoutdrag
            sLine = sLine & Chr(34) & "1" & Chr(34) & "," ' 106 - "1" = Hurtigadvis
        Else
            sLine = sLine & Chr(34) & "0" & Chr(34) & "," ' 106 - "0" = Meddelelse p� modtagers kontoudskrift
        End If

        sLine = sLine & Chr(34) & PadRight(oPayment.Text_E_Statement, 35, " ") & Chr(34) & "," ' 110 Posteringstekst

        sLine = sLine & Chr(34) & PadRight(oPayment.E_Name, 32, " ") & Chr(34) & "," ' 148 Navn p� modtager
        sLine = sLine & Chr(34) & PadRight(oPayment.E_Adr1, 32, " ") & Chr(34) & "," ' 183 Adr1 p� modtager
        sLine = sLine & Chr(34) & PadRight(oPayment.E_Adr2, 32, " ") & Chr(34) & "," ' 218 Adr2 p� modtager
        sLine = sLine & Chr(34) & PadRight(oPayment.E_Zip, 4, " ") & Chr(34) & "," ' 253 Postnr
        sLine = sLine & Chr(34) & PadRight(oPayment.E_City, 32, " ") & Chr(34) & "," ' 260 Bynavn

        '' Ikke i 4.0 sLine = sLine & Chr(34) & PadRight(oPayment.REF_Own, 35, " ") & Chr(34) & ","   ' 295 Eget bilagsnr

        ' Upto 10 fields of 35 chars with freetext, Notification to receiver
        ' Pad to 10*35
        sFreetext = PadRight(sFreetext, 10 * 35, " ")
        For i = 1 To 10
            sLine = sLine & Chr(34) & Mid(sFreetext, (i - 1) * 35 + 1, 35) & Chr(34) & "," ' 295 - 637 Adviseringstekst 1-10
        Next i

        sLine = sLine & Chr(34) & Space(1) & Chr(34) & "," ' 675
        sLine = sLine & Chr(34) & Space(215) & Chr(34) ' 679

        ' Check if we are going to write index 2;
        ' 14.03.06 Added oPayment.Text_I_Statement
        If Len(Trim(oPayment.REF_Bank1)) > 0 Or Len(Trim(oPayment.REF_Bank2)) > 0 Or Len(Trim(oPayment.Text_I_Statement)) > 0 Then
            sLine = sLine & vbCrLf ' Must also write line 1 !
            sLine = sLine & Chr(34) & "IB030202000004" & Chr(34) & "," ' 1-18
            sLine = sLine & Chr(34) & "0002" & Chr(34) & "," ' 19 Index

            sLine = sLine & Chr(34) & PadRight(oPayment.I_Name, 35, " ") & Chr(34) & "," ' 3  -26
            sLine = sLine & Chr(34) & PadRight(oPayment.I_Adr1, 35, " ") & Chr(34) & "," ' 4  -64
            sLine = sLine & Chr(34) & PadRight(oPayment.I_Adr2, 35, " ") & Chr(34) & "," ' 5  -102
            sLine = sLine & Chr(34) & PadRight(oPayment.I_Adr3, 35, " ") & Chr(34) & "," ' 6  -140
            sLine = sLine & Chr(34) & Space(35) & Chr(34) & "," ' 7  -178


            sLine = sLine & Chr(34) & PadRight(oPayment.REF_Bank1, 35, " ") & Chr(34) & "," ' 8  -216   Creditor ID
            sLine = sLine & Chr(34) & PadRight(oPayment.REF_Bank2, 35, " ") & Chr(34) & "," ' 9  -254   Primary document
            sLine = sLine & Chr(34) & PadRight(oPayment.Text_I_Statement, 35, " ") & Chr(34) & "," ' 10 -292   DebitorID (text on statement)
            sLine = sLine & Chr(34) & Space(48) & Chr(34) & ","
            sLine = sLine & Chr(34) & Space(255) & Chr(34) & ","
            sLine = sLine & Chr(34) & Space(255) & Chr(34)

        End If
        WriteBankdata_Indland = sLine

    End Function
    Private Function WriteBankdata_Indbetalingskort(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef sFreetext As String) As String
        Dim sLine As String
        Dim i As Short

        'PS Must run once for each invoice in a payment!

        ' Add to file-totals:
        nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
        nFileNoRecords = nFileNoRecords + 1

        sLine = Chr(34) & "IB030207000001" & Chr(34) & "," ' 1-18
        sLine = sLine & Chr(34) & "0001" & Chr(34) & "," ' 19 Index
        sLine = sLine & Chr(34) & oPayment.DATE_Payment & Chr(34) & "," ' 26 Expeditionsdato
        sLine = sLine & Chr(34) & PadLeft(Str(oInvoice.MON_InvoiceAmount), 13, "0") & "+" & Chr(34) & "," '37 Trans-bel�p
        If Len(oPayment.MON_InvoiceCurrency) = 0 Then
            oPayment.MON_InvoiceCurrency = "DKK"
        End If
        sLine = sLine & Chr(34) & PadLeft(oPayment.MON_InvoiceCurrency, 3, " ") & Chr(34) & "," '54 M�ntsort

        sLine = sLine & Chr(34) & "2" & Chr(34) & "," ' 60 Fra-type "2" = Bankkonto
        sLine = sLine & Chr(34) & "0" & PadRight(oPayment.I_Account, 14, " ") & Chr(34) & "," ' 64 Kontonr afsender, plus ledende 0

        sLine = sLine & Chr(34) & Left(oInvoice.Unique_Id, 2) & Chr(34) & "," ' 82 - kortart
        sLine = sLine & Chr(34) & PadRight(Mid(oInvoice.Unique_Id, 3), 19, " ") & Chr(34) & "," ' 87 Betalingsid
        sLine = sLine & Chr(34) & Space(4) & Chr(34) & "," ' 109 Giro reg.nr Ikke i bruk
        sLine = sLine & Chr(34) & Space(10) & Chr(34) & "," ' 116 Giro konto.nr N�r brukes dette ?
        sLine = sLine & Chr(34) & PadLeft(oPayment.E_Account, 8, "0") & Chr(34) & "," ' 129 Modtagers kreditornr

        sLine = sLine & Chr(34) & PadRight(oPayment.E_Name, 32, " ") & Chr(34) & "," ' 140 Navn p� modtager
        sLine = sLine & Chr(34) & Space(32) & Chr(34) & "," ' 175 Navn fra register, ikke i bruk
        'sLine = sLine & Chr(34) & PadRight(oPayment.REF_Own, 35, " ") & Chr(34) & ","   ' 210 Eget bilagsnr

        sLine = sLine & Chr(34) & Space(35) & Chr(34) & "," ' 210
        sLine = sLine & Chr(34) & Space(35) & Chr(34) & "," ' 248
        sLine = sLine & Chr(34) & Space(35) & Chr(34) & "," ' 286
        sLine = sLine & Chr(34) & Space(35) & Chr(34) & "," ' 324
        sLine = sLine & Chr(34) & Space(35) & Chr(34) & "," ' 362

        ' Upto 7 fields of 35 chars with freetext, Notification to receiver
        ' Pad to 7*35
        sFreetext = PadRight(sFreetext, 7 * 35, " ")
        For i = 1 To 7
            sLine = sLine & Chr(34) & Mid(sFreetext, (i - 1) * 35 + 1, 35) & Chr(34) & "," ' 400 - 622 Adviseringstekst 1-7
        Next i

        sLine = sLine & Chr(34) & Space(10) & Chr(34) & "," ' 666
        sLine = sLine & Chr(34) & Space(215) & Chr(34) ' 679

        WriteBankdata_Indbetalingskort = sLine

    End Function

    Private Function WriteBankdata_Adviseringslinier(ByVal sFreetext As String, ByRef sIndex As String) As String
        Dim sLine As String
        Dim i As Short

        ' Pad sfreetext to 22 * 35
        sFreetext = PadRight(sFreetext, 22 * 35, " ")

        sLine = Chr(34) & "IB030202000004" & Chr(34) & "," ' 1-18
        sLine = sLine & Chr(34) & sIndex & Chr(34) & "," ' 19 Index
        For i = 1 To 22
            sLine = sLine & Chr(34) & Mid(sFreetext, (i - 1) * 35 + 1, 35) & Chr(34) & "," ' 26 - 861 Adviseringstekst 1-22
        Next i
        sLine = sLine & Chr(34) & Space(32) & Chr(34) ' 862

        WriteBankdata_Adviseringslinier = sLine

    End Function

    Private Function WriteBankdata_Slut() As String
        Dim sLine As String

        sLine = Chr(34) & "IB999999999999" & Chr(34) & "," ' 1-18
        sLine = sLine & Chr(34) & VB6.Format(Date.Today, "YYYYMMDD") & Chr(34) & "," ' 19
        sLine = sLine & Chr(34) & PadLeft(Str(nFileNoRecords), 6, "0") & Chr(34) & "," ' 30
        sLine = sLine & Chr(34) & PadLeft(Str(nFileSumAmount), 13, "0") & "+" & Chr(34) & "," ' 39
        sLine = sLine & Chr(34) & Space(64) & Chr(34) & "," ' 56
        sLine = sLine & Chr(34) & Space(255) & Chr(34) & "," ' 123
        sLine = sLine & Chr(34) & Space(255) & Chr(34) & "," ' 381
        sLine = sLine & Chr(34) & Space(255) & Chr(34) ' 639

        WriteBankdata_Slut = sLine
    End Function
End Module
