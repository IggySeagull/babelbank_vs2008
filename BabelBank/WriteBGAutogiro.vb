﻿Option Strict Off
Option Explicit On
Module WriteBGAutogiro

    ' =====================================================
    ' Gjelder foreløpig KUN Autogiro trekk
    ' =====================================================
    Dim sLine As String ' en output-linje
    Dim oBatch As Batch
    ' - filenivå: sum beløp, antall records, antall transer, siste dato
    '             disse påvirkes nedover i nivåene
    Dim bFileStartWritten As Boolean
    Dim sSpecial As String = ""

    Function WriteBGAutogiroFile(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sAdditionalID As String, ByVal sSpecial As String) As Boolean

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim i As Double, j As Double, k As Double, l As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim sNewOwnref As String
        Dim sTxt As String
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim sOldAccount As String = ""
        Dim sOldDebitAccount As String = ""
        Dim sOldCurrency As String = ""
        Dim sOldDatePayment As String = ""


        Try

            sOldAccount = "--------------------"

            If Len(Dir(sFilenameOut)) > 0 Then
                bFileStartWritten = True
            Else
                bFileStartWritten = False
            End If

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If oPayment.Cancel = False And Not oPayment.Exported Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = vbNullString
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If 'If oPayment.Cancel = False Then
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then

                    If Not bFileStartWritten Then
                        ' write only once!
                        sLine = WriteBGAutogiroFileStart(oBabel, sAdditionalID)
                        oFile.WriteLine(sLine)
                        bFileStartWritten = True
                    End If

                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False And Not oPayment.Exported Then

                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = vbNullString
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then
                            i = i + 1

                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    If oPayment.Cancel = False And Not oPayment.Exported Then

                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = vbNullString
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If 'If oPayment.Cancel = False Then
                                End If

                                If bExportoPayment Then
                                    j = j + 1


                                    For Each oInvoice In oPayment.Invoices
                                        sTxt = ""

                                        If Not oInvoice.Exported Then
                                            sLine = WriteAutogiroInbetalningspost(oPayment, oInvoice, sSpecial)
                                            oFile.WriteLine(sLine)
                                        End If ' If Not oInvoice.Exported Then

                                        oInvoice.Exported = True
                                    Next oInvoice
                                    oPayment.Exported = True
                                End If
                            Next ' payment
                        End If
                    Next 'batch
                End If
            Next 'Babelfile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteBGAutogiroFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteBGAutogiroFile = True

    End Function
    Function WriteBGAutogiroFileStart(ByVal oBabelFile As vbBabel.BabelFile, ByVal sAdditionalNo As String) As String
        Dim sLine As String

        sLine = "01"        ' 1-2
        sLine = sLine & Format(Date.Today, "yyyyMMdd")  '3-10 Skrivdag
        sLine = sLine & "AUTOGIRO" '11-18
        sLine = sLine & Space(44)  '19-62 blank
        If EmptyString(sAdditionalNo) Then
            sLine = sLine & PadLeft(oBabelFile.Batches(1).I_EnterpriseNo, 6, "0")      '63-68 Kundnummer
        Else
            sLine = sLine & PadLeft(sAdditionalNo, 6, "0")    '63-68 Kundnummer
        End If
        If oBabelFile.Batches.Count > 0 Then
            If oBabelFile.Batches(1).Payments.Count > 0 Then
                sLine = sLine & PadLeft(oBabelFile.Batches(1).Payments(1).I_Account, 10, "0")  '69-78 Mottagers bankgironr
            End If
        End If
        sLine = sLine & Space(2)    '79-80

        WriteBGAutogiroFileStart = sLine

    End Function
    Function WriteAutogiroInbetalningspost(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sSpecial As String) As String
        Dim sLine As String
        Dim sRefCode As String = ""
        Dim sTxt As String = ""


        sLine = "82"        '1-2
        sLine = sLine & oPayment.DATE_Payment '3-10 Betalningsdag
        sLine = sLine & "0" ' 11 Periodkod - Foreløpig 0 = En gang
        sLine = sLine & Space(3) ' 12-14 Antal Självförnyande uppdrag
        sLine = sLine & " " ' 15 Blank
        ' Hvis Betalarnummer i ligger på Unique_ID på første invoice, bruker vi denne.
        ' Hvis ikke, så bruker vi REF_Own på oPayment
        If Not EmptyString(oInvoice.Unique_Id) Then
            If sSpecial = "FLYKTNINGHJELPEN_AUTOGIRO" Then
                ' 07.11.2017 - we have cut KID to 8 digits, 
                sLine = sLine & PadLeft(Left(oInvoice.InvoiceNo, 8), 16, "0")  ' 16-31 Betalarnummer
            Else
                sLine = sLine & PadLeft(oInvoice.Unique_Id, 16, "0")  ' 16-31 Betalarnummer
            End If
        Else
            sLine = sLine & PadLeft(oPayment.REF_Own, 16, "0")  ' 16-31 Betalarnummer
        End If

        sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 12, "0")  '32-43 Belopp, i øre
        sLine = sLine & PadLeft(oPayment.I_Account, 10, "0")  '44-53 Mottagers bankgironr
        ' Hvis Betalarnummer i ligger på Unique_ID på første invoice, bruker vi denne.
        ' Hvis ikke, så bruker vi REF_Own på oPayment
        If Not EmptyString(oInvoice.Unique_Id) Then
            sLine = sLine & PadLeft(oInvoice.Unique_Id, 16, "0")  ' 54-69 Betalingsmottagarens egen referens
        Else
            sLine = sLine & PadLeft(oPayment.REF_Own, 16, "0")  ' 54-69 Betalingsmottagarens egen referens
        End If
        sLine = sLine & Space(11)   '70-80

        WriteAutogiroInbetalningspost = sLine

    End Function

End Module

