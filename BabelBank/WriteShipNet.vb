Option Strict Off
Option Explicit On
Module WriteShipNet
    Function WriteShipNetFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim sDate As String
        Dim sText As String
        'Dim lErrno As Long

        Dim sErrorString As String
        Dim lErrno As Integer

        Try
            bAppendFile = False

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects to
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects that shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If Not bExportoBatch Then
                            '    bExportoBatch = False
                        Else

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then
                                    For Each oInvoice In oPayment.Invoices

                                        ' Start output of paymentrecord
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.VB_Profile.CompanyNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        sLine = oBabelFiles.VB_Profile.CompanyNo ' Foretaksnr
                                        sLine = sLine & ";" & oBatch.I_EnterpriseNo
                                        sLine = sLine & ";" & oBatch.I_Branch
                                        sLine = sLine & ";" & bbGetPayCode((oPayment.PayCode), "ShipNet", oPayment.PayType)
                                        sLine = sLine & ";" & Right(oBatch.StatusCode, 1)
                                        sLine = sLine & ";" & oBatch.REF_Own

                                        sLine = sLine & ";" & oInvoice.REF_Own
                                        sLine = sLine & ";" & oPayment.I_Account
                                        sLine = sLine & ";" & oPayment.E_Account
                                        sLine = sLine & ";" ' Factoringkonto, not implemented (10)
                                        sLine = sLine & ";" & oPayment.I_Name
                                        sLine = sLine & ";" & oPayment.I_Adr1
                                        sLine = sLine & ";" & oPayment.I_Adr2
                                        sLine = sLine & ";" & oPayment.I_Adr3
                                        sLine = sLine & ";" & oPayment.I_Zip
                                        sLine = sLine & ";" & oPayment.I_City
                                        sLine = sLine & ";" & oPayment.I_CountryCode

                                        sLine = sLine & ";" & oPayment.E_Name
                                        sLine = sLine & ";" & oPayment.E_Adr1
                                        sLine = sLine & ";" & oPayment.E_Adr2
                                        sLine = sLine & ";" & oPayment.E_Adr3
                                        sLine = sLine & ";" & oPayment.E_Zip
                                        sLine = sLine & ";" & oPayment.E_City
                                        sLine = sLine & ";" & oPayment.E_CountryCode

                                        sLine = sLine & ";" ' Not in use (25)
                                        sLine = sLine & ";" ' Not implemented (26)

                                        sLine = sLine & ";" & oPayment.NOTI_NotificationIdent
                                        sLine = sLine & ";" & oPayment.NOTI_NotificationAttention

                                        sLine = sLine & ";" & oPayment.BANK_Name
                                        sLine = sLine & ";" & oPayment.BANK_Adr1
                                        sLine = sLine & ";" & oPayment.BANK_Adr2
                                        sLine = sLine & ";" & oPayment.BANK_Adr3
                                        sLine = sLine & ";" ' Bank PostNr - del av Bank_Adr3, how?????
                                        sLine = sLine & ";" ' Not in use (34)
                                        sLine = sLine & ";" & oPayment.BANK_CountryCode

                                        sLine = sLine & ";" & oInvoice.Unique_Id
                                        sLine = sLine & ";" & oPayment.BANK_SWIFTCode ' 37

                                        ' In ShipNet format, must add SC, FW, etc
                                        Select Case oPayment.BANK_BranchType
                                            Case BabelFiles.BankBranchType.Fedwire
                                                oPayment.BANK_BranchNo = "FW" & oPayment.BANK_BranchNo
                                            Case BabelFiles.BankBranchType.SortCode
                                                oPayment.BANK_BranchNo = "SC" & oPayment.BANK_BranchNo
                                            Case BabelFiles.BankBranchType.Bankleitzahl
                                                oPayment.BANK_BranchNo = "BL" & oPayment.BANK_BranchNo
                                            Case BabelFiles.BankBranchType.Chips
                                                oPayment.BANK_BranchNo = "CH" & oPayment.BANK_BranchNo
                                            Case BabelFiles.BankBranchType.MEPS
                                                oPayment.BANK_BranchNo = "ME" & oPayment.BANK_BranchNo

                                        End Select
                                        sLine = sLine & ";" & oPayment.BANK_BranchNo ' 38
                                        sLine = sLine & oPayment.BANK_SWIFTCodeCorrBank & ";"

                                        If oPayment.NOTI_NotificationParty = 1 Then
                                            sLine = sLine & ";" & oPayment.NOTI_NotificationMessageToBank
                                        End If

                                        sLine = sLine & ";" & oInvoice.STATEBANK_Code
                                        sLine = sLine & ";" & oInvoice.STATEBANK_Text

                                        sLine = sLine & ";" ' Not implemented (43)
                                        sDate = oPayment.DATE_Value
                                        If sDate <> "19900101" Then
                                            sLine = sLine & ";" & Mid(sDate, 7, 2) & Mid(sDate, 5, 2) & Left(sDate, 4) 'ddmmyy
                                        Else
                                            sLine = sLine & ";"
                                        End If
                                        sDate = oPayment.DATE_Payment
                                        sLine = sLine & ";" & Mid(sDate, 7, 2) & Mid(sDate, 5, 2) & Left(sDate, 4) 'ddmmyy
                                        sLine = sLine & ";" ' Not implemented (46)

                                        sLine = sLine & ";" & VB6.Format(oInvoice.MON_InvoiceAmount / 100, "###0.00")
                                        sLine = sLine & ";" ' Not implemented (48)
                                        sLine = sLine & ";" & VB6.Format(oInvoice.MON_TransferredAmount / 100, "###0.00")

                                        sLine = sLine & ";" & oPayment.MON_InvoiceCurrency
                                        sLine = sLine & ";" & oPayment.MON_TransferCurrency

                                        If oPayment.ERA_ExchRateAgreed > 0 Then
                                            sLine = sLine & ";" & Trim(Str(oPayment.ERA_ExchRateAgreed))
                                        Else
                                            sLine = sLine & ";"
                                        End If
                                        sLine = sLine & ";" & oPayment.ERA_DealMadeWith
                                        sLine = sLine & ";" & VB6.Format(oPayment.MON_AccountExchRate, "###0.00000000")
                                        sLine = sLine & ";" & VB6.Format(oPayment.MON_LocalExchRate, "###0.00000000")
                                        sLine = sLine & ";" & oPayment.REF_Bank2
                                        sLine = sLine & ";" & oPayment.REF_Bank1
                                        sLine = sLine & ";" & oPayment.FRW_ForwardContractNo
                                        If oPayment.FRW_ForwardContractRate > 0 Then
                                            sLine = sLine & ";" & VB6.Format(oPayment.FRW_ForwardContractRate, "###0.00000000")
                                        Else
                                            sLine = sLine & ";"
                                        End If
                                        sLine = sLine & ";" & VB6.Format(oPayment.MON_ChargesAmount, "###0.00")
                                        If oPayment.MON_ChargeMeDomestic Then
                                            sLine = sLine & ";OUR"
                                        Else
                                            sLine = sLine & ";BEN"
                                        End If
                                        If oPayment.MON_ChargeMeAbroad Then
                                            sLine = sLine & ";OUR"
                                        Else
                                            sLine = sLine & ";BEN"
                                        End If

                                        sLine = sLine & ";" & oInvoice.InvoiceNo
                                        sLine = sLine & ";" ' Not implemented (64)
                                        sLine = sLine & ";" & oInvoice.CustomerNo
                                        sText = ""
                                        For Each oFreeText In oInvoice.Freetexts
                                            If oFreeText.Text <> "" Then
                                                sText = sText & oFreeText.Text
                                            End If
                                        Next oFreeText
                                        sLine = sLine & ";" & Left(sText, 255)

                                        If Len(sLine) > 0 Then
                                            oFile.WriteLine((sLine))
                                        End If
                                        oPayment.Exported = True
                                    Next oInvoice

                                End If
                                oPayment.Exported = True

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteShipNetFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteShipNetFile = True

    End Function
End Module
