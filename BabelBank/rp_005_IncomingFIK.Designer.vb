<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_005_IncomingFIK
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(rp_005_IncomingFIK))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtDATE_Payment = New DataDynamics.ActiveReports.TextBox
        Me.txtFIK = New DataDynamics.ActiveReports.TextBox
        Me.txtKortart = New DataDynamics.ActiveReports.TextBox
        Me.txtDATE_Value = New DataDynamics.ActiveReports.TextBox
        Me.txtCurrency = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_P_TransferredAmount = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.linePageHeader = New DataDynamics.ActiveReports.Line
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grBabelFileHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.grBabelFileFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.LineBabelFileFooter2 = New DataDynamics.ActiveReports.Line
        Me.LineBabelFileFooter1 = New DataDynamics.ActiveReports.Line
        Me.txtTotalNoOfPayments = New DataDynamics.ActiveReports.TextBox
        Me.lblTotalNoOfPayments = New DataDynamics.ActiveReports.Label
        Me.txtTotalAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblTotalAmount = New DataDynamics.ActiveReports.Label
        Me.grBatchHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapeBatchHeader = New DataDynamics.ActiveReports.Shape
        Me.txtI_Account = New DataDynamics.ActiveReports.TextBox
        Me.txtDATE_Production = New DataDynamics.ActiveReports.TextBox
        Me.txtClientName = New DataDynamics.ActiveReports.TextBox
        Me.lblDate_Production = New DataDynamics.ActiveReports.Label
        Me.lblClient = New DataDynamics.ActiveReports.Label
        Me.lblI_Account = New DataDynamics.ActiveReports.Label
        Me.lblFIK = New DataDynamics.ActiveReports.Label
        Me.lblDATE_Payment = New DataDynamics.ActiveReports.Label
        Me.lblKortart = New DataDynamics.ActiveReports.Label
        Me.lblDATE_Value = New DataDynamics.ActiveReports.Label
        Me.lblCurrency = New DataDynamics.ActiveReports.Label
        Me.lblAmount = New DataDynamics.ActiveReports.Label
        Me.txtFilename = New DataDynamics.ActiveReports.TextBox
        Me.grBatchFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.lblBatchfooterAmount = New DataDynamics.ActiveReports.Label
        Me.txtBatchfooterAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblBatchfooterNoofPayments = New DataDynamics.ActiveReports.Label
        Me.txtBatchfooterNoofPayments = New DataDynamics.ActiveReports.TextBox
        Me.LineBatchFooter1 = New DataDynamics.ActiveReports.Line
        CType(Me.txtDATE_Payment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFIK, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtKortart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Value, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_P_TransferredAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblFIK, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDATE_Payment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblKortart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDATE_Value, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtDATE_Payment, Me.txtFIK, Me.txtKortart, Me.txtDATE_Value, Me.txtCurrency, Me.txtMON_P_TransferredAmount})
        Me.Detail.Height = 0.21875!
        Me.Detail.Name = "Detail"
        '
        'txtDATE_Payment
        '
        Me.txtDATE_Payment.Height = 0.15!
        Me.txtDATE_Payment.Left = 0.0!
        Me.txtDATE_Payment.Name = "txtDATE_Payment"
        Me.txtDATE_Payment.OutputFormat = resources.GetString("txtDATE_Payment.OutputFormat")
        Me.txtDATE_Payment.Style = "font-size: 8pt"
        Me.txtDATE_Payment.Text = "txtDATE_Payment"
        Me.txtDATE_Payment.Top = 0.0!
        Me.txtDATE_Payment.Width = 1.0!
        '
        'txtFIK
        '
        Me.txtFIK.DataField = "Unique_ID"
        Me.txtFIK.Height = 0.15!
        Me.txtFIK.Left = 1.0!
        Me.txtFIK.Name = "txtFIK"
        Me.txtFIK.Style = "font-size: 8pt"
        Me.txtFIK.Text = "txtFIK"
        Me.txtFIK.Top = 0.0!
        Me.txtFIK.Width = 2.0!
        '
        'txtKortart
        '
        Me.txtKortart.DataField = "KortArtKode"
        Me.txtKortart.Height = 0.15!
        Me.txtKortart.Left = 3.0!
        Me.txtKortart.Name = "txtKortart"
        Me.txtKortart.OutputFormat = resources.GetString("txtKortart.OutputFormat")
        Me.txtKortart.Style = "font-size: 8pt"
        Me.txtKortart.Text = "txtKortArt"
        Me.txtKortart.Top = 0.0!
        Me.txtKortart.Width = 0.5!
        '
        'txtDATE_Value
        '
        Me.txtDATE_Value.Height = 0.15!
        Me.txtDATE_Value.Left = 3.5!
        Me.txtDATE_Value.Name = "txtDATE_Value"
        Me.txtDATE_Value.OutputFormat = resources.GetString("txtDATE_Value.OutputFormat")
        Me.txtDATE_Value.Style = "font-size: 8pt"
        Me.txtDATE_Value.Text = "txtDATE_Value"
        Me.txtDATE_Value.Top = 0.0!
        Me.txtDATE_Value.Width = 1.5!
        '
        'txtCurrency
        '
        Me.txtCurrency.DataField = "MON_TransferCurrency"
        Me.txtCurrency.Height = 0.15!
        Me.txtCurrency.Left = 5.0!
        Me.txtCurrency.Name = "txtCurrency"
        Me.txtCurrency.Style = "font-size: 8pt"
        Me.txtCurrency.Text = "Cur"
        Me.txtCurrency.Top = 0.0!
        Me.txtCurrency.Width = 0.4!
        '
        'txtMON_P_TransferredAmount
        '
        Me.txtMON_P_TransferredAmount.DataField = "MON_TransferredAmount"
        Me.txtMON_P_TransferredAmount.Height = 0.125!
        Me.txtMON_P_TransferredAmount.Left = 5.4!
        Me.txtMON_P_TransferredAmount.Name = "txtMON_P_TransferredAmount"
        Me.txtMON_P_TransferredAmount.OutputFormat = resources.GetString("txtMON_P_TransferredAmount.OutputFormat")
        Me.txtMON_P_TransferredAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_P_TransferredAmount.Text = "txtMON_P_TransferredAmount"
        Me.txtMON_P_TransferredAmount.Top = 0.0!
        Me.txtMON_P_TransferredAmount.Width = 1.0625!
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.rptInfoPageHeaderDate, Me.linePageHeader})
        Me.PageHeader1.Height = 0.4479167!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 1.3!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "font-size: 18pt; text-align: center"
        Me.lblrptHeader.Text = "Incoming FIK payments"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right"
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'linePageHeader
        '
        Me.linePageHeader.Height = 0.0!
        Me.linePageHeader.Left = 0.0!
        Me.linePageHeader.LineWeight = 1.0!
        Me.linePageHeader.Name = "linePageHeader"
        Me.linePageHeader.Top = 0.375!
        Me.linePageHeader.Width = 6.5!
        Me.linePageHeader.X1 = 0.0!
        Me.linePageHeader.X2 = 6.5!
        Me.linePageHeader.Y1 = 0.375!
        Me.linePageHeader.Y2 = 0.375!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter1.Height = 0.25!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right"
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right"
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grBabelFileHeader
        '
        Me.grBabelFileHeader.CanShrink = True
        Me.grBabelFileHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grBabelFileHeader.Height = 0.0625!
        Me.grBabelFileHeader.Name = "grBabelFileHeader"
        '
        'grBabelFileFooter
        '
        Me.grBabelFileFooter.CanShrink = True
        Me.grBabelFileFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.LineBabelFileFooter2, Me.LineBabelFileFooter1, Me.txtTotalNoOfPayments, Me.lblTotalNoOfPayments, Me.txtTotalAmount, Me.lblTotalAmount})
        Me.grBabelFileFooter.Height = 0.3541667!
        Me.grBabelFileFooter.Name = "grBabelFileFooter"
        '
        'LineBabelFileFooter2
        '
        Me.LineBabelFileFooter2.Height = 0.0!
        Me.LineBabelFileFooter2.Left = 3.7!
        Me.LineBabelFileFooter2.LineWeight = 1.0!
        Me.LineBabelFileFooter2.Name = "LineBabelFileFooter2"
        Me.LineBabelFileFooter2.Top = 0.32!
        Me.LineBabelFileFooter2.Width = 2.8!
        Me.LineBabelFileFooter2.X1 = 3.7!
        Me.LineBabelFileFooter2.X2 = 6.5!
        Me.LineBabelFileFooter2.Y1 = 0.32!
        Me.LineBabelFileFooter2.Y2 = 0.32!
        '
        'LineBabelFileFooter1
        '
        Me.LineBabelFileFooter1.Height = 0.0!
        Me.LineBabelFileFooter1.Left = 3.7!
        Me.LineBabelFileFooter1.LineWeight = 1.0!
        Me.LineBabelFileFooter1.Name = "LineBabelFileFooter1"
        Me.LineBabelFileFooter1.Top = 0.3!
        Me.LineBabelFileFooter1.Width = 2.8!
        Me.LineBabelFileFooter1.X1 = 3.7!
        Me.LineBabelFileFooter1.X2 = 6.5!
        Me.LineBabelFileFooter1.Y1 = 0.3!
        Me.LineBabelFileFooter1.Y2 = 0.3!
        '
        'txtTotalNoOfPayments
        '
        Me.txtTotalNoOfPayments.CanGrow = False
        Me.txtTotalNoOfPayments.DataField = "Grouping"
        Me.txtTotalNoOfPayments.DistinctField = "Grouping"
        Me.txtTotalNoOfPayments.Height = 0.15!
        Me.txtTotalNoOfPayments.Left = 4.6!
        Me.txtTotalNoOfPayments.Name = "txtTotalNoOfPayments"
        Me.txtTotalNoOfPayments.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; ddo-char-set: 0"
        Me.txtTotalNoOfPayments.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DCount
        Me.txtTotalNoOfPayments.SummaryGroup = "grBatchHeader"
        Me.txtTotalNoOfPayments.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalNoOfPayments.Text = "txtTotalNoOfPayments"
        Me.txtTotalNoOfPayments.Top = 0.1!
        Me.txtTotalNoOfPayments.Width = 0.3!
        '
        'lblTotalNoOfPayments
        '
        Me.lblTotalNoOfPayments.Height = 0.15!
        Me.lblTotalNoOfPayments.HyperLink = Nothing
        Me.lblTotalNoOfPayments.Left = 3.7!
        Me.lblTotalNoOfPayments.Name = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.lblTotalNoOfPayments.Text = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Top = 0.1!
        Me.lblTotalNoOfPayments.Width = 0.9!
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.DataField = "MON_TransferredAmount"
        Me.txtTotalAmount.DistinctField = "Grouping"
        Me.txtTotalAmount.Height = 0.15!
        Me.txtTotalAmount.Left = 5.438!
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat")
        Me.txtTotalAmount.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; ddo-char-set: 0"
        Me.txtTotalAmount.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DSum
        Me.txtTotalAmount.SummaryGroup = "grBatchHeader"
        Me.txtTotalAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalAmount.Text = "txtTotalAmount"
        Me.txtTotalAmount.Top = 0.1!
        Me.txtTotalAmount.Width = 1.0!
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Height = 0.15!
        Me.lblTotalAmount.HyperLink = Nothing
        Me.lblTotalAmount.Left = 4.95!
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.lblTotalAmount.Text = "lblTotalAmount"
        Me.lblTotalAmount.Top = 0.1!
        Me.lblTotalAmount.Width = 0.5!
        '
        'grBatchHeader
        '
        Me.grBatchHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapeBatchHeader, Me.txtI_Account, Me.txtDATE_Production, Me.txtClientName, Me.lblDate_Production, Me.lblClient, Me.lblI_Account, Me.lblFIK, Me.lblDATE_Payment, Me.lblKortart, Me.lblDATE_Value, Me.lblCurrency, Me.lblAmount, Me.txtFilename})
        Me.grBatchHeader.DataField = "BreakField"
        Me.grBatchHeader.Height = 0.875!
        Me.grBatchHeader.Name = "grBatchHeader"
        Me.grBatchHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'shapeBatchHeader
        '
        Me.shapeBatchHeader.Height = 0.5!
        Me.shapeBatchHeader.Left = 0.0!
        Me.shapeBatchHeader.Name = "shapeBatchHeader"
        Me.shapeBatchHeader.RoundingRadius = 9.999999!
        Me.shapeBatchHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapeBatchHeader.Top = 0.05!
        Me.shapeBatchHeader.Width = 6.45!
        '
        'txtI_Account
        '
        Me.txtI_Account.DataField = "I_Account"
        Me.txtI_Account.Height = 0.19!
        Me.txtI_Account.Left = 4.5!
        Me.txtI_Account.Name = "txtI_Account"
        Me.txtI_Account.Style = "font-size: 10pt; font-weight: normal"
        Me.txtI_Account.Text = "txtI_Account"
        Me.txtI_Account.Top = 0.3004167!
        Me.txtI_Account.Width = 1.9!
        '
        'txtDATE_Production
        '
        Me.txtDATE_Production.CanShrink = True
        Me.txtDATE_Production.Height = 0.19!
        Me.txtDATE_Production.Left = 1.5!
        Me.txtDATE_Production.Name = "txtDATE_Production"
        Me.txtDATE_Production.OutputFormat = resources.GetString("txtDATE_Production.OutputFormat")
        Me.txtDATE_Production.Style = "font-size: 10pt"
        Me.txtDATE_Production.Text = "txtDATE_Production"
        Me.txtDATE_Production.Top = 0.125!
        Me.txtDATE_Production.Width = 1.563!
        '
        'txtClientName
        '
        Me.txtClientName.CanGrow = False
        Me.txtClientName.Height = 0.19!
        Me.txtClientName.Left = 4.5!
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Style = "font-size: 10pt"
        Me.txtClientName.Text = "txtClientName"
        Me.txtClientName.Top = 0.1104167!
        Me.txtClientName.Width = 1.9!
        '
        'lblDate_Production
        '
        Me.lblDate_Production.Height = 0.19!
        Me.lblDate_Production.HyperLink = Nothing
        Me.lblDate_Production.Left = 0.125!
        Me.lblDate_Production.Name = "lblDate_Production"
        Me.lblDate_Production.Style = "font-size: 10pt"
        Me.lblDate_Production.Text = "lblDate_Production"
        Me.lblDate_Production.Top = 0.125!
        Me.lblDate_Production.Width = 1.313!
        '
        'lblClient
        '
        Me.lblClient.Height = 0.19!
        Me.lblClient.HyperLink = Nothing
        Me.lblClient.Left = 3.5!
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Style = "font-size: 10pt"
        Me.lblClient.Text = "lblClient"
        Me.lblClient.Top = 0.125!
        Me.lblClient.Width = 1.0!
        '
        'lblI_Account
        '
        Me.lblI_Account.Height = 0.19!
        Me.lblI_Account.HyperLink = Nothing
        Me.lblI_Account.Left = 3.5!
        Me.lblI_Account.Name = "lblI_Account"
        Me.lblI_Account.Style = "font-size: 10pt"
        Me.lblI_Account.Text = "lblI_Account"
        Me.lblI_Account.Top = 0.3004167!
        Me.lblI_Account.Width = 1.0!
        '
        'lblFIK
        '
        Me.lblFIK.Height = 0.15!
        Me.lblFIK.HyperLink = Nothing
        Me.lblFIK.Left = 1.0!
        Me.lblFIK.MultiLine = False
        Me.lblFIK.Name = "lblFIK"
        Me.lblFIK.Style = "font-size: 8pt"
        Me.lblFIK.Text = "lblFIK"
        Me.lblFIK.Top = 0.625!
        Me.lblFIK.Width = 2.0!
        '
        'lblDATE_Payment
        '
        Me.lblDATE_Payment.Height = 0.15!
        Me.lblDATE_Payment.HyperLink = Nothing
        Me.lblDATE_Payment.Left = 0.0!
        Me.lblDATE_Payment.MultiLine = False
        Me.lblDATE_Payment.Name = "lblDATE_Payment"
        Me.lblDATE_Payment.Style = "font-size: 8pt"
        Me.lblDATE_Payment.Text = "lblDATE_Payment"
        Me.lblDATE_Payment.Top = 0.625!
        Me.lblDATE_Payment.Width = 1.0!
        '
        'lblKortart
        '
        Me.lblKortart.Height = 0.15!
        Me.lblKortart.HyperLink = Nothing
        Me.lblKortart.Left = 3.0!
        Me.lblKortart.Name = "lblKortart"
        Me.lblKortart.Style = "font-size: 8pt"
        Me.lblKortart.Text = "lblKortart"
        Me.lblKortart.Top = 0.625!
        Me.lblKortart.Width = 0.5!
        '
        'lblDATE_Value
        '
        Me.lblDATE_Value.Height = 0.15!
        Me.lblDATE_Value.HyperLink = Nothing
        Me.lblDATE_Value.Left = 3.5!
        Me.lblDATE_Value.Name = "lblDATE_Value"
        Me.lblDATE_Value.Style = "font-size: 8pt"
        Me.lblDATE_Value.Text = "lblDATE_Value"
        Me.lblDATE_Value.Top = 0.625!
        Me.lblDATE_Value.Width = 1.5!
        '
        'lblCurrency
        '
        Me.lblCurrency.Height = 0.125!
        Me.lblCurrency.HyperLink = Nothing
        Me.lblCurrency.Left = 5.0!
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Style = "font-size: 8pt"
        Me.lblCurrency.Text = "Cur"
        Me.lblCurrency.Top = 0.625!
        Me.lblCurrency.Width = 0.4!
        '
        'lblAmount
        '
        Me.lblAmount.Height = 0.15!
        Me.lblAmount.HyperLink = Nothing
        Me.lblAmount.Left = 5.4!
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Style = "font-size: 8pt"
        Me.lblAmount.Text = "lblAmount"
        Me.lblAmount.Top = 0.625!
        Me.lblAmount.Width = 1.063!
        '
        'txtFilename
        '
        Me.txtFilename.DataField = "Filename"
        Me.txtFilename.Height = 0.19!
        Me.txtFilename.Left = 1.5!
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.Style = "font-size: 10pt; font-weight: normal"
        Me.txtFilename.Text = "txtFilename_ToMove"
        Me.txtFilename.Top = 0.3!
        Me.txtFilename.Width = 1.9!
        '
        'grBatchFooter
        '
        Me.grBatchFooter.CanShrink = True
        Me.grBatchFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblBatchfooterAmount, Me.txtBatchfooterAmount, Me.lblBatchfooterNoofPayments, Me.txtBatchfooterNoofPayments, Me.LineBatchFooter1})
        Me.grBatchFooter.Height = 0.25!
        Me.grBatchFooter.Name = "grBatchFooter"
        '
        'lblBatchfooterAmount
        '
        Me.lblBatchfooterAmount.Height = 0.15!
        Me.lblBatchfooterAmount.HyperLink = Nothing
        Me.lblBatchfooterAmount.Left = 4.95!
        Me.lblBatchfooterAmount.Name = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.lblBatchfooterAmount.Text = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Top = 0.0!
        Me.lblBatchfooterAmount.Width = 0.5!
        '
        'txtBatchfooterAmount
        '
        Me.txtBatchfooterAmount.DataField = "MON_TransferredAmount"
        Me.txtBatchfooterAmount.DistinctField = "Grouping"
        Me.txtBatchfooterAmount.Height = 0.15!
        Me.txtBatchfooterAmount.Left = 5.45!
        Me.txtBatchfooterAmount.Name = "txtBatchfooterAmount"
        Me.txtBatchfooterAmount.OutputFormat = resources.GetString("txtBatchfooterAmount.OutputFormat")
        Me.txtBatchfooterAmount.Style = "font-size: 8.25pt; text-align: right; ddo-char-set: 0"
        Me.txtBatchfooterAmount.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DSum
        Me.txtBatchfooterAmount.SummaryGroup = "grBatchHeader"
        Me.txtBatchfooterAmount.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtBatchfooterAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtBatchfooterAmount.Text = "txtBatchfooterAmount"
        Me.txtBatchfooterAmount.Top = 0.0!
        Me.txtBatchfooterAmount.Width = 1.0!
        '
        'lblBatchfooterNoofPayments
        '
        Me.lblBatchfooterNoofPayments.Height = 0.15!
        Me.lblBatchfooterNoofPayments.HyperLink = Nothing
        Me.lblBatchfooterNoofPayments.Left = 3.7!
        Me.lblBatchfooterNoofPayments.Name = "lblBatchfooterNoofPayments"
        Me.lblBatchfooterNoofPayments.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.lblBatchfooterNoofPayments.Text = "lblBatchfooterNoofPayments"
        Me.lblBatchfooterNoofPayments.Top = 0.0!
        Me.lblBatchfooterNoofPayments.Width = 0.9!
        '
        'txtBatchfooterNoofPayments
        '
        Me.txtBatchfooterNoofPayments.DataField = "Grouping"
        Me.txtBatchfooterNoofPayments.DistinctField = "Grouping"
        Me.txtBatchfooterNoofPayments.Height = 0.15!
        Me.txtBatchfooterNoofPayments.Left = 4.6!
        Me.txtBatchfooterNoofPayments.Name = "txtBatchfooterNoofPayments"
        Me.txtBatchfooterNoofPayments.Style = "font-size: 8.25pt; text-align: right; ddo-char-set: 0"
        Me.txtBatchfooterNoofPayments.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DCount
        Me.txtBatchfooterNoofPayments.SummaryGroup = "grBatchHeader"
        Me.txtBatchfooterNoofPayments.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtBatchfooterNoofPayments.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtBatchfooterNoofPayments.Text = "txtBatchfooterNoofPayments"
        Me.txtBatchfooterNoofPayments.Top = 0.0!
        Me.txtBatchfooterNoofPayments.Width = 0.3!
        '
        'LineBatchFooter1
        '
        Me.LineBatchFooter1.Height = 0.0!
        Me.LineBatchFooter1.Left = 3.7!
        Me.LineBatchFooter1.LineWeight = 1.0!
        Me.LineBatchFooter1.Name = "LineBatchFooter1"
        Me.LineBatchFooter1.Top = 0.21!
        Me.LineBatchFooter1.Width = 2.8!
        Me.LineBatchFooter1.X1 = 3.7!
        Me.LineBatchFooter1.X2 = 6.5!
        Me.LineBatchFooter1.Y1 = 0.21!
        Me.LineBatchFooter1.Y2 = 0.21!
        '
        'rp_005_IncomingFIK
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 6.500501!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.grBabelFileHeader)
        Me.Sections.Add(Me.grBatchHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grBatchFooter)
        Me.Sections.Add(Me.grBabelFileFooter)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.txtDATE_Payment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFIK, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtKortart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Value, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_P_TransferredAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblFIK, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDATE_Payment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblKortart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDATE_Value, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents txtDATE_Payment As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtFIK As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtKortart As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtDATE_Value As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtCurrency As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_P_TransferredAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Friend WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Friend WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents linePageHeader As DataDynamics.ActiveReports.Line
    Friend WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    Friend WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents grBabelFileHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents grBabelFileFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents LineBabelFileFooter2 As DataDynamics.ActiveReports.Line
    Friend WithEvents LineBabelFileFooter1 As DataDynamics.ActiveReports.Line
    Friend WithEvents txtTotalNoOfPayments As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblTotalNoOfPayments As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTotalAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblTotalAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents grBatchHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents shapeBatchHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents txtI_Account As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtDATE_Production As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtClientName As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblDate_Production As DataDynamics.ActiveReports.Label
    Friend WithEvents lblClient As DataDynamics.ActiveReports.Label
    Friend WithEvents lblI_Account As DataDynamics.ActiveReports.Label
    Friend WithEvents lblFIK As DataDynamics.ActiveReports.Label
    Friend WithEvents lblDATE_Payment As DataDynamics.ActiveReports.Label
    Friend WithEvents lblKortart As DataDynamics.ActiveReports.Label
    Friend WithEvents lblDATE_Value As DataDynamics.ActiveReports.Label
    Friend WithEvents lblCurrency As DataDynamics.ActiveReports.Label
    Friend WithEvents lblAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents grBatchFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents lblBatchfooterAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents txtBatchfooterAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblBatchfooterNoofPayments As DataDynamics.ActiveReports.Label
    Friend WithEvents txtBatchfooterNoofPayments As DataDynamics.ActiveReports.TextBox
    Friend WithEvents LineBatchFooter1 As DataDynamics.ActiveReports.Line
    Private WithEvents txtFilename As DataDynamics.ActiveReports.TextBox
End Class
