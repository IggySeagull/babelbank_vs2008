Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmSpecial
    Inherits System.Windows.Forms.Form
    'This form is used for special cases, which need some information.
    'All controls are disable (except lblExplaination), so the form must be created in form_load
    Public sSpecial As String
    Public bAbandon As Boolean
    Private Sub thisForm_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        Select Case sSpecial

            Case "ULEFOS_NV"
                e.Graphics.DrawLine(Pens.Orange, 9, 300, 582, 300)   ' m� justeres
            Case "NORDEA_LIV"
                e.Graphics.DrawLine(Pens.Orange, 9, 320, 582, 320)   ' m� justeres
            Case Else
                e.Graphics.DrawLine(Pens.Orange, 9, 230, 582, 230)
        End Select
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click

        bAbandon = True
        Me.Hide()

    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Dim bHide As Boolean
        Dim sDay As String
        Dim sMonth As String
        Dim sYear As String

        bHide = True

        If sSpecial = "NORDEA_LIV" Then
            If Len(Me.Text1.Text) - Len(Replace(Me.Text1.Text, ".", "")) = 2 Then
                ' OK, two . in datestring:
                ' Test date given;
                sDay = VB.Left(Me.Text1.Text, InStr(Me.Text1.Text, ".") - 1)
                sMonth = Mid(Me.Text1.Text, InStr(Me.Text1.Text, ".") + 1, 2)
                If VB.Right(sMonth, 1) = "." Then
                    ' only one digit for month
                    sMonth = VB.Left(sMonth, 1)
                End If
                sYear = VB.Right(Me.Text1.Text, 4)
                If Val(sYear) < 2000 Or Val(sYear) > 2099 Then
                    MsgBox(LRSCommon(20032) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                    'Erroneus year stated - Please try again
                    bHide = False
                End If
                If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
                    MsgBox(LRSCommon(20033) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                    'Erroneus month stated - Please try again
                    bHide = False
                End If
                If Val(sDay) < 1 Or Val(sDay) > 31 Then
                    MsgBox(LRSCommon(20034) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                    'Erroneus day stated - Please try again
                    bHide = False
                End If
                If Val(sMonth) = 4 Or Val(sMonth) = 6 Or Val(sMonth) = 9 Or Val(sMonth) = 11 Then
                    If Val(sDay) > 30 Then
                        MsgBox(LRSCommon(20034) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                        'Erroneus day stated - Please try again
                        bHide = False
                    End If
                End If
                If Val(sMonth) = 2 Then
                    If Val(sDay) > 29 Then
                        MsgBox(LRSCommon(20034) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                        'Erroneus day stated - Please try again
                        bHide = False
                    End If
                End If
                'Repeat the datevalidation for the second date stated
                If Len(Me.Text2.Text) - Len(Replace(Me.Text2.Text, ".", "")) = 2 Then
                    ' OK, two . in datestring:
                    ' Test date given;
                    sDay = VB.Left(Me.Text2.Text, InStr(Me.Text2.Text, ".") - 1)
                    sMonth = Mid(Me.Text2.Text, InStr(Me.Text2.Text, ".") + 1, 2)
                    If VB.Right(sMonth, 1) = "." Then
                        ' only one digit for month
                        sMonth = VB.Left(sMonth, 1)
                    End If
                    sYear = VB.Right(Me.Text2.Text, 4)
                    If Val(sYear) < 2000 Or Val(sYear) > 2099 Then
                        MsgBox(LRSCommon(20032) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                        'Erroneus year stated - Please try again
                        bHide = False
                    End If
                    If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
                        MsgBox(LRSCommon(20033) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                        'Erroneus month stated - Please try again
                        bHide = False
                    End If
                    If Val(sDay) < 1 Or Val(sDay) > 31 Then
                        MsgBox(LRSCommon(20034) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                        'Erroneus day stated - Please try again
                        bHide = False
                    End If
                    If Val(sMonth) = 4 Or Val(sMonth) = 6 Or Val(sMonth) = 9 Or Val(sMonth) = 11 Then
                        If Val(sDay) > 30 Then
                            MsgBox(LRSCommon(20034) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                            'Erroneus day stated - Please try again
                            bHide = False
                        End If
                    End If
                    If Val(sMonth) = 2 Then
                        If Val(sDay) > 29 Then
                            MsgBox(LRSCommon(20034) & LRSCommon(20031), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                            'Erroneus day stated - Please try again
                            bHide = False
                        End If
                    End If
                Else
                    MsgBox(LRSCommon(20035, "dd.mm.yyyy"), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                    'Erroneus date stated! vbcrlf Use the format dd.mm.yyyy, when You enter the date
                    bHide = False
                End If
            Else
                MsgBox(LRSCommon(20035, "dd.mm.yyyy"), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                'Erroneus date stated! vbcrlf Use the format dd.mm.yyyy, when You enter the date
                bHide = False
            End If

        End If

        If bHide Then
            bAbandon = False
            Me.Hide()
        End If

    End Sub

    Private Sub frmSpecial_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        Select Case sSpecial

            Case "ULEFOS_NV"
                Me.Height = VB6.TwipsToPixelsY(3300)
                Me.Width = VB6.TwipsToPixelsX(7650)
                Me.lblComboBox.Visible = True
                Me.lblComboBox.Enabled = True
                Me.lblComboBox.Text = LRS(64051)
                Me.cmbCombobox.Enabled = True
                Me.cmbCombobox.Visible = True
                Me.lblExplaination.Top = VB6.TwipsToPixelsY(1300)
                Me.lblExplaination.Width = VB6.TwipsToPixelsX(4000)
                Me.lblExplaination.Height = VB6.TwipsToPixelsY(800)
                Me.lblExplaination.Text = LRS(64050) '
                Me.lblComboBox.Top = VB6.TwipsToPixelsY(1700)
                Me.lblComboBox.Left = VB6.TwipsToPixelsX(1920)
                Me.cmbCombobox.Top = VB6.TwipsToPixelsY(1700)
                Me.cmbCombobox.Left = VB6.TwipsToPixelsX(4900)
                'Me.Line1.X1 = 0
                'Me.Line1.X2 = VB6.TwipsToPixelsX(7250)
                'Me.Line1.Y1 = VB6.TwipsToPixelsY(2200)
                'Me.Line1.Y2 = VB6.TwipsToPixelsY(2200)
                Me.cmdOK.Top = VB6.TwipsToPixelsY(2400)
                Me.cmdOK.Left = VB6.TwipsToPixelsX(4800)
                Me.cmdCancel.Top = VB6.TwipsToPixelsY(2400)
                Me.cmdCancel.Left = VB6.TwipsToPixelsX(5960)
            Case "NORDEA_LIV"
                Me.Height = VB6.TwipsToPixelsY(4100)
                Me.Width = VB6.TwipsToPixelsX(7250)
                Me.lbl1.Visible = True
                Me.lbl1.Enabled = True
                Me.lbl1.Text = "Fra og med dato (format DD.MM.YYYY):"
                Me.lbl1.Top = VB6.TwipsToPixelsY(1700)
                Me.lbl1.Left = VB6.TwipsToPixelsX(1920)
                Me.lbl1.Width = VB6.TwipsToPixelsX(3000)
                Me.Text1.Visible = True
                Me.Text1.Enabled = True
                Me.Text1.Top = VB6.TwipsToPixelsY(1700)
                Me.Text1.Left = VB6.TwipsToPixelsX(4900)
                Me.lblExplaination.Width = VB6.TwipsToPixelsX(4000)
                Me.lblExplaination.Height = VB6.TwipsToPixelsY(800)
                Me.lblExplaination.Text = "Angi fradato og tildato for utplukk av poster." & vbCrLf & "BabelBank vil kun hente ut poster som ikke tidligere er eksportert, hvis du ikke endrer dette."
                Me.lbl2.Visible = True
                Me.lbl2.Enabled = True
                Me.lbl2.Text = "Til og med dato (format DD.MM.YYYY):"
                Me.lbl2.Top = VB6.TwipsToPixelsY(2000)
                Me.lbl2.Left = VB6.TwipsToPixelsX(1920)
                Me.lbl2.Width = VB6.TwipsToPixelsX(3000)
                Me.Text2.Visible = True
                Me.Text2.Enabled = True
                Me.Text2.Top = VB6.TwipsToPixelsY(2000)
                Me.Text2.Left = VB6.TwipsToPixelsX(4900)
                Me.Check1.Visible = True
                Me.Check1.Enabled = True
                Me.Check1.CheckState = System.Windows.Forms.CheckState.Checked
                Me.Check1.Text = "Ikke tidligere eksporterte poster"
                Me.Check1.Top = VB6.TwipsToPixelsY(2300)
                Me.Check1.Left = VB6.TwipsToPixelsX(1920)
                Me.Check1.Width = VB6.TwipsToPixelsX(5000)


                'Me.Line1.X1 = 0
                'Me.Line1.X2 = VB6.TwipsToPixelsX(7250)
                'Me.Line1.Y1 = VB6.TwipsToPixelsY(3000)
                'Me.Line1.Y2 = VB6.TwipsToPixelsY(3000)
                Me.cmdOK.Top = VB6.TwipsToPixelsY(3200)
                Me.cmdOK.Left = VB6.TwipsToPixelsX(4800)
                Me.cmdCancel.Top = VB6.TwipsToPixelsY(3200)
                Me.cmdCancel.Left = VB6.TwipsToPixelsX(5960)


        End Select

        bAbandon = True

        FormvbStyle(Me, "")
        FormLRSCaptions(Me)

    End Sub
End Class
