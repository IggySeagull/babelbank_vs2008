Option Strict Off
Option Explicit On
Module BabelError
	'05.09.2008 Old code. Theese variables was declared twice
	'Dim eFormat As FileType, sFilename As String, sRecord As String, sId As String
	
	Dim lId As Integer
	Dim sFormat As String
	
    Sub BabelImportError(ByRef eFormat As Object, ByRef sFilename As String, ByRef sRecord As String, ByRef sId As String, Optional ByRef sVariable1 As String = "", Optional ByRef sVariable2 As String = "", Optional ByRef sVariable3 As String = "", Optional ByRef iLineNo As Short = 0)
        Dim sLRS As String

        'sRecord = Replace(sRecord, Chr(34), "'")

        lId = Val(Left(sId, 5))

        '14.02.2018 - We have a problem if the sErrMsg starts with 5 digits and a '-', because then the function 
        '  FormLRSCaptions in the frmBB_Error.frm will try to run a LRS using the errorcodes 5 digit
        ' Therefore change from - to # in the ID
        If sId.Length > 6 Then
            If vbIsNumeric(sId.Substring(0, 5)) And sId.Substring(5, 1) = "-" Then
                sId = sId.Substring(0, 5) & "#" & sId.Substring(6)
            End If
        End If

        sFormat = GetEnumFileType(eFormat)

        If Len(sVariable1) = 0 Then
            sLRS = LRS(lId)
        ElseIf Len(sVariable2) = 0 Then
            sLRS = LRS(lId, sVariable1)
        ElseIf Len(sVariable3) = 0 Then
            sLRS = LRS(lId, sVariable1, sVariable2)
        Else
            sLRS = Replace(LRS(lId, sVariable1, sVariable2), "%3", sVariable3)
        End If

        'Err.Raise(vbObjectError + lId, , sId & ": " & LRS(64001) & vbTab & sLRS & Chr(13) & LRS(64002) & vbTab & sFormat & Chr(13) & LRS(64003) & vbTab & sFilename & Chr(13) & IIf(iLineNo > 0, LRS(64004) & vbTab & Str(iLineNo) & Chr(13), "") & LRS(64004) & Chr(13) & sRecord)
        Err.Raise(vbObjectError + lId, , sId & ": " & LRS(64001) & vbCrLf & sLRS & Chr(13) & LRS(64002) & vbCrLf & sFormat & Chr(13) & LRS(64003) & vbCrLf & sFilename & Chr(13) & IIf(iLineNo > 0, LRS(64004) & vbTab & Str(iLineNo) & Chr(13), "") & LRS(64004) & Chr(13) & sRecord)

    End Sub
    Public Function New_BabelImportError(ByRef eFormat As Object, ByRef sId As String, Optional ByRef sVariable1 As String = "", Optional ByRef sVariable2 As String = "", Optional ByRef sVariable3 As String = "", Optional ByRef iLineNo As Short = 0)
        Dim sLRS As String

        'sRecord = Replace(sRecord, Chr(34), "'")

        lId = Val(Left(sId, 5))

        '14.02.2018 - We have a problem if the sErrMsg starts with 5 digits and a '-', because then the function 
        '  FormLRSCaptions in the frmBB_Error.frm will try to run a LRS using the errorcodes 5 digit
        ' Therefore change from - to # in the ID
        If sId.Length > 6 Then
            If vbIsNumeric(sId.Substring(0, 5)) And sId.Substring(5, 1) = "-" Then
                sId = sId.Substring(0, 5) & "#" & sId.Substring(6)
            End If
        End If

        sFormat = GetEnumFileType(eFormat)

        If Len(sVariable1) = 0 Then
            sLRS = LRS(lId)
        ElseIf Len(sVariable2) = 0 Then
            sLRS = LRS(lId, sVariable1)
        ElseIf Len(sVariable3) = 0 Then
            sLRS = LRS(lId, sVariable1, sVariable2)
        Else
            sLRS = Replace(LRS(lId, sVariable1, sVariable2), "%3", sVariable3)
        End If

        'Err.Raise(vbObjectError + lId, , sId & ": " & LRS(64001) & vbTab & sLRS & Chr(13) & LRS(64002) & vbTab & sFormat & Chr(13) & LRS(64003) & vbTab & sFilename & Chr(13) & IIf(iLineNo > 0, LRS(64004) & vbTab & Str(iLineNo) & Chr(13), "") & LRS(64004) & Chr(13) & sRecord)
        New_BabelImportError = sId & ": " & LRS(64001) & vbCrLf & sLRS & Chr(13) & LRS(64002) & vbCrLf & sFormat '& Chr(13) & LRS(64003) & vbCrLf & IIf(iLineNo > 0, LRS(64004) & vbTab & Str(iLineNo) & Chr(13), "") & LRS(64004)

    End Function
End Module
