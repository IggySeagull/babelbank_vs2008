Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module WriteCzech_Payments_Citibank
	
    Function WriteCzech_Payments_CitibankFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef iFilenameInNo As Short) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext

        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bFileFromBank As Boolean
        Dim sNewOwnref As String
        Dim iTextCounter As Short
        Dim sFreetext, sDate As String
        Dim iFreetextCounter As Short
        Dim bKSymbolExist As Boolean
        Dim sErrorMessage, sFieldname As String
        Dim sEAccountFirstPart As String
        Dim i, iLines As Short

        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                If oBabel.VB_FilenameInNo = iFilenameInNo Then

                    bExportoBabel = False
                    'Have to go through each batch-object to see if we have objects that shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If
                        Next oPayment
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oBatch

                    If bExportoBabel Then
                        'Set if the file was created int the accounting system or not.
                        bFileFromBank = oBabel.FileFromBank
                        'i = 0 'FIXED Moved by Janp under case 2


                        'Loop through all Batch objs. in BabelFile obj.
                        For Each oBatch In oBabel.Batches
                            bExportoBatch = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oPayment In oBatch.Payments
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If
                                'If true exit the loop, and we have data to export
                                If bExportoBatch Then
                                    Exit For
                                End If
                            Next oPayment


                            If bExportoBatch Then

                                '                    i = i + 1
                                '
                                '                    'Get the companyno either from a property passed from BabelExport, or
                                '                    ' from the imported file
                                '                    If sCompanyNo <> "" Then
                                '                        sI_EnterpriseNo = sCompanyNo
                                '                    Else
                                '                        sI_EnterpriseNo = oBatch.I_EnterpriseNo
                                '                    End If
                                '                    'BETFOR00
                                '                    If bWriteRecord10 Then
                                '                        sLine = WriteRecord10(sI_Account, bEuroAccount, oBatch.Payments(1).ERA_ExchRateAgreed, oBatch.Payments(1).ERA_DealMadeWith, oBatch.Payments(1).MON_TransferCurrency, sFilenameOut)
                                '                        oFile.WriteLine (sLine)
                                '                        bWriteRecord10 = False
                                '                        lNumberofRecords = lNumberofRecords + 1
                                '                        'New record10 for this vriables
                                '                        sLastAgreedRate = oBatch.Payments(1).ERA_ExchRateAgreed
                                '                        sLastAccount = sI_Account
                                '                        sLastCurrencyCode = oBatch.Payments(1).MON_TransferCurrency
                                '                    End If

                                For Each oPayment In oBatch.Payments


                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If

                                    If bExportoPayment Then

                                        For Each oInvoice In oPayment.Invoices

                                            bKSymbolExist = False
                                            'Test first if it is a domestic payment or not
                                            If oPayment.PayType <> "I" Then
                                                'Currency
                                                oFile.WriteLine(("0"))
                                                oFile.WriteLine(("CZK"))

                                                'Amount
                                                oFile.WriteLine(("1"))
                                                'oFile.WriteLine (VB6.Format(oInvoice.MON_InvoiceAmount / 100, "###0.00"))
                                                oFile.WriteLine((Left(Trim(Str(oInvoice.MON_InvoiceAmount)), Len(Trim(Str(oInvoice.MON_InvoiceAmount))) - 2) & "." & Right(Trim(Str(oInvoice.MON_InvoiceAmount)), 2)))

                                                'Value date, Format "DD/MM/YY
                                                oFile.WriteLine(("2"))
                                                sDate = oPayment.DATE_Payment
                                                If sDate <> "19900101" Then
                                                    oFile.WriteLine((Right(sDate, 2) & "/" & Mid(sDate, 5, 2) & "/" & Mid(sDate, 3, 2)))
                                                Else
                                                    'No bookdate exist. It's mandatory.
                                                    sFieldname = "book date"
                                                    Err.Raise(1, , "Unexpected error during export.")
                                                End If

                                                'Document number
                                                If Len(oPayment.REF_Own) > 0 Then
                                                    If Len(oPayment.REF_Own) < 11 Then
                                                        oFile.WriteLine(("3"))
                                                        oFile.WriteLine((oPayment.REF_Own))
                                                    Else
                                                        oFile.WriteLine(("3"))
                                                        oFile.WriteLine(Left(oPayment.REF_Own, 10))
                                                    End If
                                                Else
                                                    'No document number (invoiceno.) exist. It's mandatory.
                                                    sFieldname = "document number (payment_REF_Own)"
                                                    Err.Raise(1, , "Unexpected error during export.")
                                                End If

                                                '                                    If Len(oInvoice.InvoiceNo) > 0 Then
                                                '                                        If Len(oInvoice.InvoiceNo) < 11 Then
                                                '                                            oFile.WriteLine ("3")
                                                '                                            oFile.WriteLine (oInvoice.InvoiceNo)
                                                '                                        Else
                                                '                                            oFile.WriteLine ("3")
                                                '                                            oFile.WriteLine Left((oInvoice.InvoiceNo), 10)
                                                '                                        End If
                                                '                                    Else
                                                '                                        'No document number (invoiceno.) exist. It's mandatory.
                                                '                                        sFieldname = "document number (invoiceno.)"
                                                '                                        Err.Raise 1, , "Unexpected error during export."
                                                '                                    End If

                                                'Debit account number
                                                If Len(oPayment.I_Account) > 0 Then
                                                    'debit accountno must be numeric.
                                                    If IsNumeric(oPayment.I_Account) Then
                                                        'debit accountno must be exactly 10 char.
                                                        If Len(oPayment.I_Account) = 10 Then
                                                            oFile.WriteLine(("4"))
                                                            oFile.WriteLine((oPayment.I_Account))
                                                        Else
                                                            If Len(oPayment.I_Account) > 10 Then
                                                                sErrorMessage = LRS(35035)
                                                                sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                                Err.Raise(35035, , sErrorMessage)
                                                            Else
                                                                sErrorMessage = LRS(35036)
                                                                sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                                Err.Raise(35036, , sErrorMessage)
                                                            End If
                                                        End If
                                                    Else
                                                        sErrorMessage = LRS(35034)
                                                        sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                        Err.Raise(35034, , sErrorMessage)
                                                    End If 'IsNumeric(oPayment.I_Account) Then
                                                Else
                                                    sFieldname = "debit account"
                                                    Err.Raise(1, , "Unexpected error during export.")
                                                End If

                                                'Branch - not used
                                                'oFile.WriteLine ("5")
                                                'sDate =

                                                'S-Symbol debit
                                                'oFile.WriteLine ("6")
                                                'oFile.WriteLine

                                                'V-Symbol debit
                                                'oFile.WriteLine ("7")
                                                'oFile.WriteLine

                                                'Debit A/C Name
                                                'oFile.WriteLine ("8")
                                                'oFile.WriteLine

                                                'One of field 9 and 10 must be filled out
                                                'Bank code chars.
                                                'oFile.WriteLine ("9")
                                                'oFile.WriteLine ("KOMB")

                                                'Bank code numeric
                                                If Len(oPayment.E_AccountPrefix) = 4 And IsNumeric(oPayment.E_AccountPrefix) Then
                                                    oFile.WriteLine(("10"))
                                                    oFile.WriteLine((oPayment.E_AccountPrefix))
                                                Else
                                                    sErrorMessage = LRS(35037, oPayment.E_AccountPrefix)
                                                    sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                    Err.Raise(35037, , sErrorMessage)
                                                End If

                                                sEAccountFirstPart = ""

                                                'Credit A/N Base
                                                If Len(oPayment.E_Account) > 0 Then
                                                    ' Changed by JanP 18.06.03, after speaking til Trodn R�vang, Elkj�p
                                                    ' E_Account may be divided into two parts, divided by -
                                                    ' First part in field 12, second part in field 11
                                                    If InStr(oPayment.E_Account, "-") > 0 Then
                                                        sEAccountFirstPart = Left(oPayment.E_Account, InStr(oPayment.E_Account, "-") - 1)
                                                        oPayment.E_Account = Mid(oPayment.E_Account, InStr(oPayment.E_Account, "-") + 1)
                                                    End If

                                                    If Len(oPayment.E_Account) < 11 And IsNumeric(oPayment.E_Account) Then
                                                        oFile.WriteLine(("11"))
                                                        'Changed 4/10-2004, by Kjell, iAccountSepPos er kun dimmet og dermed alltid 0
                                                        'oFile.WriteLine (Mid$(oPayment.E_Account, iAccountSepPos + 1))
                                                        oFile.WriteLine(oPayment.E_Account)
                                                    Else
                                                        'Changed 17/6-2004 by Kjell. Same change done earlier on international payments
                                                        sErrorMessage = LRS(35041, (oPayment.E_Account), "10")
                                                        'Old code
                                                        'sErrorMessage = lrs(35038, oPayment.E_AccountPrefix)
                                                        sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                        Err.Raise(35041, , sErrorMessage)
                                                    End If
                                                Else
                                                    sFieldname = "credit A/N base"
                                                    Err.Raise(1, , "Unexpected error during export.")
                                                End If

                                                'Credit A/N Optional
                                                ' Changed by JanP 18.06.03, after speaking til Trodn R�vang, Elkj�p
                                                ' E_Account may be divided into two parts, place first part here!
                                                'oFile.WriteLine ("12")
                                                'oFile.WriteLine

                                                If Len(sEAccountFirstPart) > 0 Then
                                                    oFile.WriteLine(("12"))
                                                    oFile.WriteLine(sEAccountFirstPart)
                                                End If

                                                'S-Symbol Credit
                                                If Len(oPayment.E_AccountSuffix) > 0 And IsNumeric(oPayment.E_AccountSuffix) Then
                                                    If Len(oPayment.E_AccountSuffix) < 11 Then
                                                        oFile.WriteLine(("13"))
                                                        oFile.WriteLine((oPayment.E_AccountSuffix))
                                                    Else
                                                        oFile.WriteLine(("13"))
                                                        oFile.WriteLine(Left(oPayment.E_AccountSuffix, 10))
                                                    End If
                                                End If

                                                'V-Symbol Credit
                                                If Len(oInvoice.InvoiceNo) > 0 And IsNumeric(oInvoice.InvoiceNo) Then
                                                    If Len(oInvoice.InvoiceNo) < 11 Then
                                                        oFile.WriteLine(("14"))
                                                        oFile.WriteLine((oInvoice.InvoiceNo))
                                                    Else
                                                        oFile.WriteLine(("14"))
                                                        oFile.WriteLine(Left(oInvoice.InvoiceNo, 10))
                                                    End If
                                                End If

                                                'Credit A/C Optional
                                                If Len(oPayment.E_Name) > 0 Then
                                                    'Should check if other than characters than 0-9 og A-Z and special characters
                                                    oFile.WriteLine(("15"))
                                                    If Len(oPayment.E_Name) < 21 Then
                                                        oFile.WriteLine(oPayment.E_Name)
                                                    Else
                                                        oFile.WriteLine(Left(oPayment.E_Name, 20))
                                                    End If
                                                End If


                                                'Transaction type
                                                oFile.WriteLine(("16"))
                                                oFile.WriteLine(("11"))

                                                'K-symbol
                                                If Len(oInvoice.STATEBANK_Code) > 0 Then
                                                    If IsNumeric(oInvoice.STATEBANK_Code) And Len(oInvoice.STATEBANK_Code) < 11 Then
                                                        oFile.WriteLine(("17"))
                                                        oFile.WriteLine((oInvoice.STATEBANK_Code))
                                                    Else
                                                        sErrorMessage = LRS(35039, oPayment.E_AccountPrefix)
                                                        sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                        Err.Raise(35039, , sErrorMessage)
                                                    End If
                                                Else
                                                    'No K-symbol exist. It's mandatory.
                                                    sFieldname = "K-Symbol"
                                                    Err.Raise(1, , "Unexpected error during export.")
                                                End If

                                                'Debit Identification, use Ownref
                                                If Len(oPayment.REF_Own) > 0 Then
                                                    If Len(oPayment.REF_Own) < 36 Then
                                                        oFile.WriteLine(("18"))
                                                        oFile.WriteLine((oPayment.REF_Own))
                                                        oFile.WriteLine((""))
                                                        oFile.WriteLine((""))
                                                        oFile.WriteLine((""))
                                                    ElseIf Len(oPayment.REF_Own) < 71 Then
                                                        oFile.WriteLine(("18"))
                                                        oFile.WriteLine((Left(oPayment.REF_Own, 35)))
                                                        oFile.WriteLine((Mid(oPayment.REF_Own, 36)))
                                                        oFile.WriteLine((""))
                                                        oFile.WriteLine((""))
                                                    ElseIf Len(oPayment.REF_Own) < 106 Then
                                                        oFile.WriteLine(("18"))
                                                        oFile.WriteLine((Left(oPayment.REF_Own, 35)))
                                                        oFile.WriteLine((Mid(oPayment.REF_Own, 36, 35)))
                                                        oFile.WriteLine((Mid(oPayment.REF_Own, 71)))
                                                        oFile.WriteLine((""))
                                                    ElseIf Len(oPayment.REF_Own) < 141 Then
                                                        oFile.WriteLine(("18"))
                                                        oFile.WriteLine((Left(oPayment.REF_Own, 35)))
                                                        oFile.WriteLine((Mid(oPayment.REF_Own, 36, 35)))
                                                        oFile.WriteLine((Mid(oPayment.REF_Own, 71, 35)))
                                                        oFile.WriteLine((Mid(oPayment.REF_Own, 106)))
                                                    Else
                                                        Err.Raise(1, "vbBabel", "The debit identification is to long. Maximium length is 140 characters.")
                                                    End If
                                                End If

                                                'Credit identification
                                                'oFile.WriteLine ("19")
                                                'oFile.WriteLine

                                                'Beneficiary mess - Freetext
                                                'In Citibank-format you may have 4 freetextline, each max 35 positions
                                                If oInvoice.Freetexts.Count > 0 Then
                                                    oFile.WriteLine(("20"))
                                                    iFreetextCounter = 0
                                                    sFreetext = ""
                                                    For Each oFreeText In oInvoice.Freetexts
                                                        sFreetext = sFreetext & Trim(oFreeText.Text)
                                                        If Len(sFreetext) < 36 Then
                                                            oFile.WriteLine((sFreetext))
                                                            iFreetextCounter = iFreetextCounter + 1
                                                            sFreetext = ""
                                                        Else
                                                            oFile.WriteLine((Left(sFreetext, 35)))
                                                            iFreetextCounter = iFreetextCounter + 1
                                                            sFreetext = Mid(sFreetext, 36)
                                                        End If
                                                        If iFreetextCounter = 4 Then Exit For
                                                    Next oFreeText
                                                    For iTextCounter = iFreetextCounter To 3 'Maximum 4 lines
                                                        If Len(sFreetext) > 0 Then
                                                            If Len(sFreetext) < 36 Then
                                                                oFile.WriteLine((sFreetext))
                                                                sFreetext = ""
                                                            Else
                                                                oFile.WriteLine((Left(sFreetext, 35)))
                                                                sFreetext = Mid(sFreetext, 36)
                                                            End If
                                                        Else
                                                            oFile.WriteLine((""))
                                                        End If
                                                    Next iTextCounter
                                                End If

                                                sFieldname = ""

                                                oFile.WriteLine(("-1"))
                                                '--- end domestic payments ---
                                            Else
                                                'International payment
                                                '---------------------

                                                '0 Destination name
                                                oFile.WriteLine(("0"))
                                                oFile.WriteLine(("CITIBANK PRAGUE"))

                                                ' 1 Transfer method
                                                oFile.WriteLine(("1"))
                                                oFile.WriteLine(("TT"))

                                                ' 2 Predefined code, optional, not used here
                                                ' Skip field 2
                                                'oFile.WriteLine ("2")
                                                'oFile.WriteLine ("")

                                                ' 3 Customer reference
                                                If Len(oInvoice.REF_Own) > 0 Then
                                                    ' Use invoice ref_own if filled
                                                    oFile.WriteLine(("3"))
                                                    oFile.WriteLine(Left(oInvoice.REF_Own, 16))
                                                ElseIf Len(oPayment.REF_Own) > 0 Then
                                                    oFile.WriteLine(("3"))
                                                    oFile.WriteLine(Left(oPayment.REF_Own, 16))
                                                Else
                                                    'No document number (invoiceno.) exist. It's mandatory.
                                                    'Fake an ownref
                                                    oFile.WriteLine(("3"))
                                                    oFile.WriteLine("X")
                                                End If

                                                ' 4 Currency
                                                oFile.WriteLine(("4"))
                                                oFile.WriteLine(oPayment.MON_TransferCurrency)

                                                ' 5 Amount
                                                oFile.WriteLine(("5"))
                                                oFile.WriteLine((Left(Trim(Str(oInvoice.MON_InvoiceAmount)), Len(Trim(Str(oInvoice.MON_InvoiceAmount))) - 2) & "." & Right(Trim(Str(oInvoice.MON_InvoiceAmount)), 2)))

                                                ' 6 Value date, Format "DD/MM/YY
                                                oFile.WriteLine(("6"))
                                                sDate = oPayment.DATE_Payment
                                                If sDate <> "19900101" And StringToDate(sDate) > Now Then
                                                    oFile.WriteLine((Right(sDate, 2) & "/" & Mid(sDate, 5, 2) & "/" & Mid(sDate, 3, 2)))
                                                Else
                                                    'Use todays date
                                                    oFile.WriteLine(Right("0" & Trim(Str(VB.Day(Now))), 2) & "/" & Right("0" & Trim(Str(Month(Now))), 2) & "/" & Right(Str(Year(Now)), 2))
                                                End If

                                                ' 7 Customer Account (debit account)
                                                If Len(oPayment.I_Account) > 0 Then
                                                    'debit accountno must be numeric.
                                                    If IsNumeric(oPayment.I_Account) Then
                                                        'debit accountno must be exactly 10 char.
                                                        If Len(oPayment.I_Account) = 10 Then
                                                            oFile.WriteLine(("7"))
                                                            oFile.WriteLine((oPayment.I_Account))
                                                        Else
                                                            If Len(oPayment.I_Account) > 10 Then
                                                                sErrorMessage = LRS(35035)
                                                                sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                                Err.Raise(35035, , sErrorMessage)
                                                            Else
                                                                sErrorMessage = LRS(35036)
                                                                sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                                sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                                Err.Raise(35036, , sErrorMessage)
                                                            End If
                                                        End If
                                                    Else
                                                        sErrorMessage = LRS(35034)
                                                        sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                        Err.Raise(35034, , sErrorMessage)
                                                    End If 'IsNumeric(oPayment.I_Account) Then
                                                Else
                                                    sFieldname = "Debit account"
                                                    Err.Raise(1, , "Unexpected error during export.")
                                                End If

                                                ' 8 Beneficiary account
                                                If Len(oPayment.E_Account) > 0 Then
                                                    If Len(oPayment.E_Account) < 35 Then
                                                        oFile.WriteLine(("8"))
                                                        oFile.WriteLine(oPayment.E_Account)
                                                    Else
                                                        sErrorMessage = LRS(35041, (oPayment.E_Account), "34")
                                                        sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                        Err.Raise(35041, , sErrorMessage)
                                                    End If
                                                Else
                                                    sFieldname = "Beneficiary account"
                                                    Err.Raise(1, , "Unexpected error during export.")
                                                End If

                                                ' 9 Beneficiary
                                                iLines = 0
                                                oFile.WriteLine(("9"))
                                                If Trim(oPayment.E_Name) <> "" Then
                                                    iLines = iLines + 1
                                                    oFile.WriteLine(oPayment.E_Name)
                                                End If
                                                If Trim(oPayment.E_Adr1) <> "" Then
                                                    iLines = iLines + 1
                                                    oFile.WriteLine(oPayment.E_Adr1)
                                                End If
                                                If Trim(oPayment.E_Adr2) <> "" Then
                                                    iLines = iLines + 1
                                                    oFile.WriteLine(oPayment.E_Adr2)
                                                End If
                                                If Trim(oPayment.E_City) <> "" Or oPayment.E_CountryCode <> "" Then
                                                    iLines = iLines + 1
                                                    oFile.WriteLine(oPayment.E_City & " " & oPayment.E_CountryCode)
                                                End If
                                                For i = iLines + 1 To 4
                                                    ' Must always have 4 lines
                                                    oFile.WriteLine("")
                                                Next i
                                                ' 10 Beneficiary bank
                                                oFile.WriteLine(("10"))
                                                If Len(oPayment.BANK_SWIFTCode) > 0 Then
                                                    oFile.WriteLine(oPayment.BANK_SWIFTCode)
                                                    oFile.WriteLine(oPayment.BANK_Name)
                                                    oFile.WriteLine(oPayment.BANK_Adr1)
                                                    oFile.WriteLine(oPayment.BANK_Adr3)
                                                ElseIf Len(oPayment.BANK_BranchNo) > 0 Then
                                                    oFile.WriteLine(oPayment.BANK_BranchNo)
                                                    oFile.WriteLine(oPayment.BANK_Name)
                                                    oFile.WriteLine(oPayment.BANK_Adr1)
                                                    oFile.WriteLine(oPayment.BANK_Adr3)
                                                Else
                                                    oFile.WriteLine(oPayment.BANK_Name)
                                                    oFile.WriteLine(oPayment.BANK_Adr1)
                                                    oFile.WriteLine(oPayment.BANK_Adr2)
                                                    oFile.WriteLine(oPayment.BANK_Adr3)
                                                End If

                                                ' 11 Beneficiary ident
                                                oFile.WriteLine("11")
                                                oFile.WriteLine("N")

                                                ' 12 Ordering customer
                                                ' Not used here
                                                oFile.WriteLine("12")
                                                oFile.WriteLine("")

                                                ' 13 Charges option
                                                oFile.WriteLine("13")
                                                If oPayment.MON_ChargeMeAbroad Then
                                                    oFile.WriteLine("OUR")
                                                Else
                                                    oFile.WriteLine("BEN")
                                                End If

                                                ' 14 Charges account
                                                ' Not used here
                                                oFile.WriteLine("14")
                                                oFile.WriteLine("")

                                                ' 15 Advice code
                                                oFile.WriteLine("15")

                                                If oPayment.NOTI_NotificationType = "PHONE" Then
                                                    oFile.WriteLine("2")
                                                ElseIf oPayment.NOTI_NotificationType = "TELEX" Then
                                                    oFile.WriteLine("3")
                                                Else
                                                    oFile.WriteLine("0")
                                                End If

                                                '16 - 19 Details No 1 - 4- Freetext
                                                'In Citibank-format you may have 4 freetextline, each max 35 positions
                                                If oInvoice.Freetexts.Count > 0 Then

                                                    ' Put all freetext into one string
                                                    sFreetext = ""
                                                    For Each oFreeText In oInvoice.Freetexts
                                                        sFreetext = sFreetext & Trim(oFreeText.Text)
                                                    Next oFreeText
                                                    oFile.WriteLine(("16"))
                                                    oFile.WriteLine(Left(sFreetext, 35))
                                                    sFreetext = Mid(sFreetext, 36)
                                                    oFile.WriteLine(("17"))
                                                    oFile.WriteLine(Left(sFreetext, 35))
                                                    sFreetext = Mid(sFreetext, 36)
                                                    oFile.WriteLine(("18"))
                                                    oFile.WriteLine(Left(sFreetext, 35))
                                                    sFreetext = Mid(sFreetext, 36)
                                                    oFile.WriteLine(("19"))
                                                    oFile.WriteLine(Left(sFreetext, 35))
                                                Else
                                                    oFile.WriteLine(("16"))
                                                    oFile.WriteLine("")
                                                    oFile.WriteLine(("17"))
                                                    oFile.WriteLine("")
                                                    oFile.WriteLine(("18"))
                                                    oFile.WriteLine("")
                                                    oFile.WriteLine(("19"))
                                                    oFile.WriteLine("")
                                                End If

                                                ' 20 Bank Information
                                                ' Use K-Symbol
                                                If Len(oInvoice.STATEBANK_Code) > 0 Then
                                                    If IsNumeric(oInvoice.STATEBANK_Code) And Len(oInvoice.STATEBANK_Code) < 11 Then
                                                        oFile.WriteLine(("20"))
                                                        oFile.WriteLine((oInvoice.STATEBANK_Code))
                                                    Else
                                                        sErrorMessage = LRS(35039, oPayment.E_AccountPrefix)
                                                        sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                        Err.Raise(35039, , sErrorMessage)
                                                    End If
                                                Else
                                                    'No K-symbol exist. It's mandatory.
                                                    sFieldname = "K-Symbol"
                                                    Err.Raise(1, , "Unexpected error during export. (K-Symbol)")
                                                End If


                                                sFieldname = ""

                                                'symbolic tag to mark end of record
                                                oFile.WriteLine(("-1"))
                                                '--- end International payments ---
                                            End If


                                            oPayment.Exported = True

                                        Next oInvoice

                                    End If 'bExportoPayment


                                Next oPayment

                            End If 'bExportoBatch

                        Next oBatch

                    End If 'bExportoBabel

                End If 'oBabel.VB_FilenameInNo = iFilenameInNo

            Next oBabel

        Catch ex As Exception

            If Len(sFieldname) > 0 Then
                'Something is wrong regarding the content of an exportfield.
                sErrorMessage = LRS(35029, sFieldname)
                sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(10015)
                Throw New vbBabel.Payment.PaymentException("Function: WriteCzech_Payments_CitibankFile" & vbCrLf & ex.Message & vbCrLf & sErrorMessage, ex, oPayment, "", sFilenameOut)
            Else
                Throw New vbBabel.Payment.PaymentException("Function: WriteCzech_Payments_CitibankFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)
            End If

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteCzech_Payments_CitibankFile = True

    End Function
End Module
