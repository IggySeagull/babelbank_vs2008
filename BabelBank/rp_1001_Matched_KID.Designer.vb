<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_1001_Matched_KID
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(rp_1001_Matched_KID))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtInvoice = New DataDynamics.ActiveReports.TextBox
        Me.txtInvoiceAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtCustomer = New DataDynamics.ActiveReports.TextBox
        Me.txtInvoiceCurrency = New DataDynamics.ActiveReports.TextBox
        Me.lblExtra1 = New DataDynamics.ActiveReports.Label
        Me.txtExtra1 = New DataDynamics.ActiveReports.TextBox
        Me.lblMyField = New DataDynamics.ActiveReports.Label
        Me.txtMyField = New DataDynamics.ActiveReports.TextBox
        Me.txtUniqueID = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.linePageHeader = New DataDynamics.ActiveReports.Line
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grBabelFileHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.grBabelFileFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.LineBabelFileFooter2 = New DataDynamics.ActiveReports.Line
        Me.LineBabelFileFooter1 = New DataDynamics.ActiveReports.Line
        Me.lblTotalNoOfPayments = New DataDynamics.ActiveReports.Label
        Me.lblTotalAmount = New DataDynamics.ActiveReports.Label
        Me.txtMatchedTotalAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtMatchedTotalCount = New DataDynamics.ActiveReports.TextBox
        Me.grBatchHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapeBatchHeader = New DataDynamics.ActiveReports.Shape
        Me.txtI_Account = New DataDynamics.ActiveReports.TextBox
        Me.txtDATE_Production = New DataDynamics.ActiveReports.TextBox
        Me.txtClientName = New DataDynamics.ActiveReports.TextBox
        Me.lblDate_Production = New DataDynamics.ActiveReports.Label
        Me.lblClient = New DataDynamics.ActiveReports.Label
        Me.lblI_Account = New DataDynamics.ActiveReports.Label
        Me.grBatchFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.lblBatchfooterAmount = New DataDynamics.ActiveReports.Label
        Me.lblBatchfooterNoofPayments = New DataDynamics.ActiveReports.Label
        Me.LineBatchFooter1 = New DataDynamics.ActiveReports.Line
        Me.txtMatchedBatchAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtMatchedBatchCount = New DataDynamics.ActiveReports.TextBox
        Me.grPaymentReceiverHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.lblPayor = New DataDynamics.ActiveReports.Label
        Me.grPaymentReceiverFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.grPaymentHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.txtE_Name = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_P_InvoiceAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Adr1 = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Adr2 = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Adr3 = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Zip = New DataDynamics.ActiveReports.TextBox
        Me.txtE_City = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Account = New DataDynamics.ActiveReports.TextBox
        Me.txtREF_Bank2 = New DataDynamics.ActiveReports.TextBox
        Me.txtREF_Bank1 = New DataDynamics.ActiveReports.TextBox
        Me.txtDATE_Value = New DataDynamics.ActiveReports.TextBox
        Me.txtVoucherNo = New DataDynamics.ActiveReports.TextBox
        Me.txtExtraD = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_P_InvoiceCurrency = New DataDynamics.ActiveReports.TextBox
        Me.lblE_Account = New DataDynamics.ActiveReports.Label
        Me.lblREF_Bank2 = New DataDynamics.ActiveReports.Label
        Me.lblREF_Bank1 = New DataDynamics.ActiveReports.Label
        Me.lblDATE_Value = New DataDynamics.ActiveReports.Label
        Me.lblVoucherNo = New DataDynamics.ActiveReports.Label
        Me.lblExtraD = New DataDynamics.ActiveReports.Label
        Me.txtBabelBank_ID = New DataDynamics.ActiveReports.TextBox
        Me.lblBB_ID = New DataDynamics.ActiveReports.Label
        Me.txtFreetext = New DataDynamics.ActiveReports.TextBox
        Me.lblConcerns = New DataDynamics.ActiveReports.Label
        Me.lblHowMatched = New DataDynamics.ActiveReports.Label
        Me.txtHowMatched = New DataDynamics.ActiveReports.TextBox
        Me.grPaymentFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.linePaymentFooter = New DataDynamics.ActiveReports.Line
        Me.txtUnmatchedAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblTotalPayment = New DataDynamics.ActiveReports.Label
        Me.lblMatchedAmount = New DataDynamics.ActiveReports.Label
        Me.lblUnmatchedAmount = New DataDynamics.ActiveReports.Label
        Me.txtTotalPayment = New DataDynamics.ActiveReports.TextBox
        Me.txtMatchedAmount = New DataDynamics.ActiveReports.TextBox
        Me.grPaymentInternationalHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.grPaymentInternationalFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.lblOriginalAMountUsed = New DataDynamics.ActiveReports.Label
        Me.grFreetextHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.grFreetextFooter = New DataDynamics.ActiveReports.GroupFooter
        CType(Me.txtInvoice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInvoiceAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCustomer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtInvoiceCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblExtra1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtExtra1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMyField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMyField, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUniqueID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMatchedTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMatchedTotalCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMatchedBatchAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMatchedBatchCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPayor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Name, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_P_InvoiceAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Adr1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Adr2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Adr3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Zip, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_City, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREF_Bank2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREF_Bank1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Value, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVoucherNo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtExtraD, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_P_InvoiceCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblE_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblREF_Bank2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblREF_Bank1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDATE_Value, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblVoucherNo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblExtraD, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBabelBank_ID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBB_ID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFreetext, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblConcerns, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblHowMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtHowMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnmatchedAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMatchedAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblUnmatchedAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMatchedAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblOriginalAMountUsed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtInvoice, Me.txtInvoiceAmount, Me.txtCustomer, Me.txtInvoiceCurrency, Me.lblExtra1, Me.txtExtra1, Me.lblMyField, Me.txtMyField, Me.txtUniqueID})
        Me.Detail.Height = 0.5104167!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'txtInvoice
        '
        Me.txtInvoice.Height = 0.15!
        Me.txtInvoice.Left = 0.0!
        Me.txtInvoice.Name = "txtInvoice"
        Me.txtInvoice.Style = "font-size: 8pt"
        Me.txtInvoice.Text = "txtInvoice"
        Me.txtInvoice.Top = 0.0!
        Me.txtInvoice.Width = 1.75!
        '
        'txtInvoiceAmount
        '
        Me.txtInvoiceAmount.DataField = "InvoiceAmount"
        Me.txtInvoiceAmount.Height = 0.15!
        Me.txtInvoiceAmount.Left = 5.42!
        Me.txtInvoiceAmount.Name = "txtInvoiceAmount"
        Me.txtInvoiceAmount.OutputFormat = resources.GetString("txtInvoiceAmount.OutputFormat")
        Me.txtInvoiceAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtInvoiceAmount.Text = "Invoice Amount"
        Me.txtInvoiceAmount.Top = 0.0!
        Me.txtInvoiceAmount.Width = 1.0!
        '
        'txtCustomer
        '
        Me.txtCustomer.CanGrow = False
        Me.txtCustomer.Height = 0.15!
        Me.txtCustomer.Left = 1.75!
        Me.txtCustomer.Name = "txtCustomer"
        Me.txtCustomer.Style = "font-size: 8pt"
        Me.txtCustomer.Text = "txtCustomer"
        Me.txtCustomer.Top = 0.0!
        Me.txtCustomer.Width = 1.45!
        '
        'txtInvoiceCurrency
        '
        Me.txtInvoiceCurrency.Height = 0.15!
        Me.txtInvoiceCurrency.Left = 5.0!
        Me.txtInvoiceCurrency.Name = "txtInvoiceCurrency"
        Me.txtInvoiceCurrency.Style = "font-size: 8pt"
        Me.txtInvoiceCurrency.Text = "CurI"
        Me.txtInvoiceCurrency.Top = 0.0!
        Me.txtInvoiceCurrency.Width = 0.4!
        '
        'lblExtra1
        '
        Me.lblExtra1.Height = 0.15!
        Me.lblExtra1.HyperLink = Nothing
        Me.lblExtra1.Left = 0.0!
        Me.lblExtra1.Name = "lblExtra1"
        Me.lblExtra1.Style = "font-size: 8pt"
        Me.lblExtra1.Text = "lblExtra1"
        Me.lblExtra1.Top = 0.3!
        Me.lblExtra1.Width = 1.0!
        '
        'txtExtra1
        '
        Me.txtExtra1.DataField = "Extra1"
        Me.txtExtra1.Height = 0.15!
        Me.txtExtra1.Left = 1.0!
        Me.txtExtra1.Name = "txtExtra1"
        Me.txtExtra1.Style = "font-size: 8pt"
        Me.txtExtra1.Text = "txtExtra1"
        Me.txtExtra1.Top = 0.3!
        Me.txtExtra1.Width = 2.0!
        '
        'lblMyField
        '
        Me.lblMyField.Height = 0.15!
        Me.lblMyField.HyperLink = Nothing
        Me.lblMyField.Left = 3.0!
        Me.lblMyField.Name = "lblMyField"
        Me.lblMyField.Style = "font-size: 8pt"
        Me.lblMyField.Text = "lblMyField"
        Me.lblMyField.Top = 0.3!
        Me.lblMyField.Width = 1.0!
        '
        'txtMyField
        '
        Me.txtMyField.DataField = "MyField"
        Me.txtMyField.Height = 0.15!
        Me.txtMyField.Left = 4.0!
        Me.txtMyField.Name = "txtMyField"
        Me.txtMyField.Style = "font-size: 8pt"
        Me.txtMyField.Text = "txtMyField"
        Me.txtMyField.Top = 0.3!
        Me.txtMyField.Width = 2.0!
        '
        'txtUniqueID
        '
        Me.txtUniqueID.DataField = "Unique_ID"
        Me.txtUniqueID.Height = 0.15!
        Me.txtUniqueID.Left = 3.192!
        Me.txtUniqueID.Name = "txtUniqueID"
        Me.txtUniqueID.Style = "font-size: 8pt"
        Me.txtUniqueID.Text = "txtUniqueID"
        Me.txtUniqueID.Top = 0.0!
        Me.txtUniqueID.Width = 1.75!
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.rptInfoPageHeaderDate, Me.linePageHeader})
        Me.PageHeader1.Height = 0.4479167!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 1.3!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "font-size: 18pt; text-align: center"
        Me.lblrptHeader.Text = "Matched KID payments"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right"
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'linePageHeader
        '
        Me.linePageHeader.Height = 0.0!
        Me.linePageHeader.Left = 0.0!
        Me.linePageHeader.LineWeight = 1.0!
        Me.linePageHeader.Name = "linePageHeader"
        Me.linePageHeader.Top = 0.375!
        Me.linePageHeader.Width = 6.5!
        Me.linePageHeader.X1 = 0.0!
        Me.linePageHeader.X2 = 6.5!
        Me.linePageHeader.Y1 = 0.375!
        Me.linePageHeader.Y2 = 0.375!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter1.Height = 0.25!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right"
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right"
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grBabelFileHeader
        '
        Me.grBabelFileHeader.CanShrink = True
        Me.grBabelFileHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grBabelFileHeader.Height = 0.0625!
        Me.grBabelFileHeader.Name = "grBabelFileHeader"
        '
        'grBabelFileFooter
        '
        Me.grBabelFileFooter.CanShrink = True
        Me.grBabelFileFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.LineBabelFileFooter2, Me.LineBabelFileFooter1, Me.lblTotalNoOfPayments, Me.lblTotalAmount, Me.txtMatchedTotalAmount, Me.txtMatchedTotalCount})
        Me.grBabelFileFooter.Height = 0.3541667!
        Me.grBabelFileFooter.KeepTogether = True
        Me.grBabelFileFooter.Name = "grBabelFileFooter"
        '
        'LineBabelFileFooter2
        '
        Me.LineBabelFileFooter2.Height = 0.0!
        Me.LineBabelFileFooter2.Left = 3.7!
        Me.LineBabelFileFooter2.LineWeight = 1.0!
        Me.LineBabelFileFooter2.Name = "LineBabelFileFooter2"
        Me.LineBabelFileFooter2.Top = 0.32!
        Me.LineBabelFileFooter2.Width = 2.8!
        Me.LineBabelFileFooter2.X1 = 3.7!
        Me.LineBabelFileFooter2.X2 = 6.5!
        Me.LineBabelFileFooter2.Y1 = 0.32!
        Me.LineBabelFileFooter2.Y2 = 0.32!
        '
        'LineBabelFileFooter1
        '
        Me.LineBabelFileFooter1.Height = 0.0!
        Me.LineBabelFileFooter1.Left = 3.7!
        Me.LineBabelFileFooter1.LineWeight = 1.0!
        Me.LineBabelFileFooter1.Name = "LineBabelFileFooter1"
        Me.LineBabelFileFooter1.Top = 0.3!
        Me.LineBabelFileFooter1.Width = 2.8!
        Me.LineBabelFileFooter1.X1 = 3.7!
        Me.LineBabelFileFooter1.X2 = 6.5!
        Me.LineBabelFileFooter1.Y1 = 0.3!
        Me.LineBabelFileFooter1.Y2 = 0.3!
        '
        'lblTotalNoOfPayments
        '
        Me.lblTotalNoOfPayments.Height = 0.15!
        Me.lblTotalNoOfPayments.HyperLink = Nothing
        Me.lblTotalNoOfPayments.Left = 3.7!
        Me.lblTotalNoOfPayments.Name = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.lblTotalNoOfPayments.Text = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Top = 0.1!
        Me.lblTotalNoOfPayments.Width = 0.95!
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Height = 0.15!
        Me.lblTotalAmount.HyperLink = Nothing
        Me.lblTotalAmount.Left = 4.95!
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.lblTotalAmount.Text = "lblTotalAmount"
        Me.lblTotalAmount.Top = 0.1!
        Me.lblTotalAmount.Width = 0.5!
        '
        'txtMatchedTotalAmount
        '
        Me.txtMatchedTotalAmount.Height = 0.15!
        Me.txtMatchedTotalAmount.Left = 5.45!
        Me.txtMatchedTotalAmount.Name = "txtMatchedTotalAmount"
        Me.txtMatchedTotalAmount.OutputFormat = resources.GetString("txtMatchedTotalAmount.OutputFormat")
        Me.txtMatchedTotalAmount.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; ddo-char-set: 0"
        Me.txtMatchedTotalAmount.Text = "txtMatchedTotalAmount"
        Me.txtMatchedTotalAmount.Top = 0.1!
        Me.txtMatchedTotalAmount.Width = 1.0!
        '
        'txtMatchedTotalCount
        '
        Me.txtMatchedTotalCount.CanGrow = False
        Me.txtMatchedTotalCount.Height = 0.15!
        Me.txtMatchedTotalCount.Left = 4.65!
        Me.txtMatchedTotalCount.Name = "txtMatchedTotalCount"
        Me.txtMatchedTotalCount.OutputFormat = resources.GetString("txtMatchedTotalCount.OutputFormat")
        Me.txtMatchedTotalCount.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; ddo-char-set: 0"
        Me.txtMatchedTotalCount.Text = "txtMatchedTotalCount"
        Me.txtMatchedTotalCount.Top = 0.1!
        Me.txtMatchedTotalCount.Width = 0.25!
        '
        'grBatchHeader
        '
        Me.grBatchHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapeBatchHeader, Me.txtI_Account, Me.txtDATE_Production, Me.txtClientName, Me.lblDate_Production, Me.lblClient, Me.lblI_Account})
        Me.grBatchHeader.DataField = "BreakField"
        Me.grBatchHeader.Height = 0.6354167!
        Me.grBatchHeader.Name = "grBatchHeader"
        Me.grBatchHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'shapeBatchHeader
        '
        Me.shapeBatchHeader.Height = 0.5!
        Me.shapeBatchHeader.Left = 0.0!
        Me.shapeBatchHeader.Name = "shapeBatchHeader"
        Me.shapeBatchHeader.RoundingRadius = 9.999999!
        Me.shapeBatchHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapeBatchHeader.Top = 0.05!
        Me.shapeBatchHeader.Width = 6.45!
        '
        'txtI_Account
        '
        Me.txtI_Account.DataField = "I_Account"
        Me.txtI_Account.Height = 0.19!
        Me.txtI_Account.Left = 4.5!
        Me.txtI_Account.Name = "txtI_Account"
        Me.txtI_Account.Style = "font-size: 10pt; font-weight: normal"
        Me.txtI_Account.Text = "txtI_Account"
        Me.txtI_Account.Top = 0.3004167!
        Me.txtI_Account.Width = 1.9!
        '
        'txtDATE_Production
        '
        Me.txtDATE_Production.CanShrink = True
        Me.txtDATE_Production.Height = 0.19!
        Me.txtDATE_Production.Left = 1.5!
        Me.txtDATE_Production.Name = "txtDATE_Production"
        Me.txtDATE_Production.OutputFormat = resources.GetString("txtDATE_Production.OutputFormat")
        Me.txtDATE_Production.Style = "font-size: 10pt"
        Me.txtDATE_Production.Text = "txtDATE_Production"
        Me.txtDATE_Production.Top = 0.125!
        Me.txtDATE_Production.Width = 1.563!
        '
        'txtClientName
        '
        Me.txtClientName.CanGrow = False
        Me.txtClientName.Height = 0.19!
        Me.txtClientName.Left = 4.5!
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Style = "font-size: 10pt; text-justify: auto"
        Me.txtClientName.Text = "txtClientName"
        Me.txtClientName.Top = 0.1104167!
        Me.txtClientName.Width = 1.9!
        '
        'lblDate_Production
        '
        Me.lblDate_Production.Height = 0.19!
        Me.lblDate_Production.HyperLink = Nothing
        Me.lblDate_Production.Left = 0.125!
        Me.lblDate_Production.Name = "lblDate_Production"
        Me.lblDate_Production.Style = "font-size: 10pt"
        Me.lblDate_Production.Text = "lblDate_Production"
        Me.lblDate_Production.Top = 0.125!
        Me.lblDate_Production.Width = 1.313!
        '
        'lblClient
        '
        Me.lblClient.Height = 0.19!
        Me.lblClient.HyperLink = Nothing
        Me.lblClient.Left = 3.5!
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Style = "font-size: 10pt"
        Me.lblClient.Text = "lblClient"
        Me.lblClient.Top = 0.125!
        Me.lblClient.Width = 1.0!
        '
        'lblI_Account
        '
        Me.lblI_Account.Height = 0.19!
        Me.lblI_Account.HyperLink = Nothing
        Me.lblI_Account.Left = 3.5!
        Me.lblI_Account.Name = "lblI_Account"
        Me.lblI_Account.Style = "font-size: 10pt"
        Me.lblI_Account.Text = "lblI_Account"
        Me.lblI_Account.Top = 0.3004167!
        Me.lblI_Account.Width = 1.0!
        '
        'grBatchFooter
        '
        Me.grBatchFooter.CanShrink = True
        Me.grBatchFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblBatchfooterAmount, Me.lblBatchfooterNoofPayments, Me.LineBatchFooter1, Me.txtMatchedBatchAmount, Me.txtMatchedBatchCount})
        Me.grBatchFooter.Height = 0.25!
        Me.grBatchFooter.KeepTogether = True
        Me.grBatchFooter.Name = "grBatchFooter"
        '
        'lblBatchfooterAmount
        '
        Me.lblBatchfooterAmount.Height = 0.15!
        Me.lblBatchfooterAmount.HyperLink = Nothing
        Me.lblBatchfooterAmount.Left = 4.95!
        Me.lblBatchfooterAmount.Name = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.lblBatchfooterAmount.Text = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Top = 0.0!
        Me.lblBatchfooterAmount.Width = 0.5!
        '
        'lblBatchfooterNoofPayments
        '
        Me.lblBatchfooterNoofPayments.Height = 0.15!
        Me.lblBatchfooterNoofPayments.HyperLink = Nothing
        Me.lblBatchfooterNoofPayments.Left = 3.7!
        Me.lblBatchfooterNoofPayments.Name = "lblBatchfooterNoofPayments"
        Me.lblBatchfooterNoofPayments.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.lblBatchfooterNoofPayments.Text = "lblBatchfooterNoofPayments"
        Me.lblBatchfooterNoofPayments.Top = 0.0!
        Me.lblBatchfooterNoofPayments.Width = 0.95!
        '
        'LineBatchFooter1
        '
        Me.LineBatchFooter1.Height = 0.0!
        Me.LineBatchFooter1.Left = 3.7!
        Me.LineBatchFooter1.LineWeight = 1.0!
        Me.LineBatchFooter1.Name = "LineBatchFooter1"
        Me.LineBatchFooter1.Top = 0.21!
        Me.LineBatchFooter1.Width = 2.8!
        Me.LineBatchFooter1.X1 = 3.7!
        Me.LineBatchFooter1.X2 = 6.5!
        Me.LineBatchFooter1.Y1 = 0.21!
        Me.LineBatchFooter1.Y2 = 0.21!
        '
        'txtMatchedBatchAmount
        '
        Me.txtMatchedBatchAmount.Height = 0.15!
        Me.txtMatchedBatchAmount.Left = 5.45!
        Me.txtMatchedBatchAmount.Name = "txtMatchedBatchAmount"
        Me.txtMatchedBatchAmount.OutputFormat = resources.GetString("txtMatchedBatchAmount.OutputFormat")
        Me.txtMatchedBatchAmount.Style = "font-size: 8.25pt; font-weight: normal; text-align: right; ddo-char-set: 0"
        Me.txtMatchedBatchAmount.Text = "txtMatchedBatchAmount"
        Me.txtMatchedBatchAmount.Top = 0.0!
        Me.txtMatchedBatchAmount.Width = 1.0!
        '
        'txtMatchedBatchCount
        '
        Me.txtMatchedBatchCount.Height = 0.15!
        Me.txtMatchedBatchCount.Left = 4.65!
        Me.txtMatchedBatchCount.Name = "txtMatchedBatchCount"
        Me.txtMatchedBatchCount.OutputFormat = resources.GetString("txtMatchedBatchCount.OutputFormat")
        Me.txtMatchedBatchCount.Style = "font-size: 8.25pt; font-weight: normal; text-align: right; ddo-char-set: 0"
        Me.txtMatchedBatchCount.Text = "txtMatchedBatchCount"
        Me.txtMatchedBatchCount.Top = 0.0!
        Me.txtMatchedBatchCount.Width = 0.25!
        '
        'grPaymentReceiverHeader
        '
        Me.grPaymentReceiverHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grPaymentReceiverHeader.CanShrink = True
        Me.grPaymentReceiverHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblPayor})
        Me.grPaymentReceiverHeader.DataField = "Grouping"
        Me.grPaymentReceiverHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grPaymentReceiverHeader.Height = 0.197917!
        Me.grPaymentReceiverHeader.Name = "grPaymentReceiverHeader"
        '
        'lblPayor
        '
        Me.lblPayor.Height = 0.15!
        Me.lblPayor.HyperLink = Nothing
        Me.lblPayor.Left = 0.0!
        Me.lblPayor.Name = "lblPayor"
        Me.lblPayor.Style = "font-size: 8pt"
        Me.lblPayor.Text = "lblPayor"
        Me.lblPayor.Top = 0.0!
        Me.lblPayor.Width = 2.0!
        '
        'grPaymentReceiverFooter
        '
        Me.grPaymentReceiverFooter.CanShrink = True
        Me.grPaymentReceiverFooter.Height = 0.1770833!
        Me.grPaymentReceiverFooter.Name = "grPaymentReceiverFooter"
        Me.grPaymentReceiverFooter.Visible = False
        '
        'grPaymentHeader
        '
        Me.grPaymentHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grPaymentHeader.CanShrink = True
        Me.grPaymentHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtE_Name, Me.txtMON_P_InvoiceAmount, Me.txtE_Adr1, Me.txtE_Adr2, Me.txtE_Adr3, Me.txtE_Zip, Me.txtE_City, Me.txtE_Account, Me.txtREF_Bank2, Me.txtREF_Bank1, Me.txtDATE_Value, Me.txtVoucherNo, Me.txtExtraD, Me.txtMON_P_InvoiceCurrency, Me.lblE_Account, Me.lblREF_Bank2, Me.lblREF_Bank1, Me.lblDATE_Value, Me.lblVoucherNo, Me.lblExtraD, Me.txtBabelBank_ID, Me.lblBB_ID, Me.txtFreetext, Me.lblConcerns, Me.lblHowMatched, Me.txtHowMatched})
        Me.grPaymentHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grPaymentHeader.Height = 1.666667!
        Me.grPaymentHeader.Name = "grPaymentHeader"
        '
        'txtE_Name
        '
        Me.txtE_Name.Height = 0.15!
        Me.txtE_Name.Left = 0.0!
        Me.txtE_Name.Name = "txtE_Name"
        Me.txtE_Name.Style = "font-size: 8pt"
        Me.txtE_Name.Text = "txtE_Name"
        Me.txtE_Name.Top = 0.0!
        Me.txtE_Name.Width = 2.0!
        '
        'txtMON_P_InvoiceAmount
        '
        Me.txtMON_P_InvoiceAmount.DataField = "MON_InvoiceAmount"
        Me.txtMON_P_InvoiceAmount.Height = 0.15!
        Me.txtMON_P_InvoiceAmount.Left = 5.45!
        Me.txtMON_P_InvoiceAmount.Name = "txtMON_P_InvoiceAmount"
        Me.txtMON_P_InvoiceAmount.OutputFormat = resources.GetString("txtMON_P_InvoiceAmount.OutputFormat")
        Me.txtMON_P_InvoiceAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_P_InvoiceAmount.Text = "txtMON_P_InvoiceAmount"
        Me.txtMON_P_InvoiceAmount.Top = 0.0!
        Me.txtMON_P_InvoiceAmount.Width = 1.0!
        '
        'txtE_Adr1
        '
        Me.txtE_Adr1.DataField = "E_Adr1"
        Me.txtE_Adr1.Height = 0.15!
        Me.txtE_Adr1.Left = 0.0!
        Me.txtE_Adr1.Name = "txtE_Adr1"
        Me.txtE_Adr1.Style = "font-size: 8pt"
        Me.txtE_Adr1.Text = "txtE_Adr1"
        Me.txtE_Adr1.Top = 0.15!
        Me.txtE_Adr1.Width = 2.0!
        '
        'txtE_Adr2
        '
        Me.txtE_Adr2.DataField = "E_Adr2"
        Me.txtE_Adr2.Height = 0.15!
        Me.txtE_Adr2.Left = 0.0!
        Me.txtE_Adr2.Name = "txtE_Adr2"
        Me.txtE_Adr2.Style = "font-size: 8pt"
        Me.txtE_Adr2.Text = "txtE_Adr2"
        Me.txtE_Adr2.Top = 0.3!
        Me.txtE_Adr2.Width = 2.0!
        '
        'txtE_Adr3
        '
        Me.txtE_Adr3.DataField = "E_Adr3"
        Me.txtE_Adr3.Height = 0.15!
        Me.txtE_Adr3.Left = 0.0!
        Me.txtE_Adr3.Name = "txtE_Adr3"
        Me.txtE_Adr3.Style = "font-size: 8pt"
        Me.txtE_Adr3.Text = "txtE_Adr3"
        Me.txtE_Adr3.Top = 0.6000001!
        Me.txtE_Adr3.Width = 2.0!
        '
        'txtE_Zip
        '
        Me.txtE_Zip.DataField = "E_Zip"
        Me.txtE_Zip.Height = 0.1875!
        Me.txtE_Zip.Left = 0.0!
        Me.txtE_Zip.Name = "txtE_Zip"
        Me.txtE_Zip.Style = "font-size: 8pt"
        Me.txtE_Zip.Text = "txtE_Zip"
        Me.txtE_Zip.Top = 0.45!
        Me.txtE_Zip.Width = 0.375!
        '
        'txtE_City
        '
        Me.txtE_City.DataField = "E_City"
        Me.txtE_City.Height = 0.15!
        Me.txtE_City.Left = 0.5!
        Me.txtE_City.Name = "txtE_City"
        Me.txtE_City.Style = "font-size: 8pt"
        Me.txtE_City.Text = "txtE_City"
        Me.txtE_City.Top = 0.45!
        Me.txtE_City.Width = 1.5!
        '
        'txtE_Account
        '
        Me.txtE_Account.DataField = "E_Account"
        Me.txtE_Account.Height = 0.15!
        Me.txtE_Account.Left = 3.25!
        Me.txtE_Account.Name = "txtE_Account"
        Me.txtE_Account.Style = "font-size: 8pt"
        Me.txtE_Account.Text = "txtE_Account"
        Me.txtE_Account.Top = 0.0!
        Me.txtE_Account.Width = 1.5!
        '
        'txtREF_Bank2
        '
        Me.txtREF_Bank2.DataField = "REF_Bank2"
        Me.txtREF_Bank2.Height = 0.15!
        Me.txtREF_Bank2.Left = 3.25!
        Me.txtREF_Bank2.Name = "txtREF_Bank2"
        Me.txtREF_Bank2.Style = "font-size: 8pt"
        Me.txtREF_Bank2.Text = "txtREF_Bank2"
        Me.txtREF_Bank2.Top = 0.15!
        Me.txtREF_Bank2.Width = 1.5!
        '
        'txtREF_Bank1
        '
        Me.txtREF_Bank1.DataField = "REF_Bank1"
        Me.txtREF_Bank1.Height = 0.15!
        Me.txtREF_Bank1.Left = 3.25!
        Me.txtREF_Bank1.Name = "txtREF_Bank1"
        Me.txtREF_Bank1.Style = "font-size: 8pt"
        Me.txtREF_Bank1.Text = "txtREF_Bank1"
        Me.txtREF_Bank1.Top = 0.3!
        Me.txtREF_Bank1.Width = 1.5!
        '
        'txtDATE_Value
        '
        Me.txtDATE_Value.Height = 0.15!
        Me.txtDATE_Value.Left = 3.25!
        Me.txtDATE_Value.Name = "txtDATE_Value"
        Me.txtDATE_Value.OutputFormat = resources.GetString("txtDATE_Value.OutputFormat")
        Me.txtDATE_Value.Style = "font-size: 8pt"
        Me.txtDATE_Value.Text = "txtDATE_Value"
        Me.txtDATE_Value.Top = 0.45!
        Me.txtDATE_Value.Width = 2.0!
        '
        'txtVoucherNo
        '
        Me.txtVoucherNo.DataField = "VoucherNo"
        Me.txtVoucherNo.Height = 0.15!
        Me.txtVoucherNo.Left = 3.25!
        Me.txtVoucherNo.Name = "txtVoucherNo"
        Me.txtVoucherNo.Style = "font-size: 8pt"
        Me.txtVoucherNo.Text = "txtVoucherNo"
        Me.txtVoucherNo.Top = 0.6!
        Me.txtVoucherNo.Width = 2.0!
        '
        'txtExtraD
        '
        Me.txtExtraD.DataField = "ExtraD"
        Me.txtExtraD.Height = 0.15!
        Me.txtExtraD.Left = 3.25!
        Me.txtExtraD.Name = "txtExtraD"
        Me.txtExtraD.Style = "font-size: 8pt"
        Me.txtExtraD.Text = "txtExtraD"
        Me.txtExtraD.Top = 0.9!
        Me.txtExtraD.Width = 3.0!
        '
        'txtMON_P_InvoiceCurrency
        '
        Me.txtMON_P_InvoiceCurrency.DataField = "MON_InvoiceCurrency"
        Me.txtMON_P_InvoiceCurrency.Height = 0.15!
        Me.txtMON_P_InvoiceCurrency.Left = 5.0!
        Me.txtMON_P_InvoiceCurrency.Name = "txtMON_P_InvoiceCurrency"
        Me.txtMON_P_InvoiceCurrency.Style = "font-size: 8pt"
        Me.txtMON_P_InvoiceCurrency.Text = "PCur"
        Me.txtMON_P_InvoiceCurrency.Top = 0.0!
        Me.txtMON_P_InvoiceCurrency.Width = 0.4!
        '
        'lblE_Account
        '
        Me.lblE_Account.Height = 0.15!
        Me.lblE_Account.HyperLink = Nothing
        Me.lblE_Account.Left = 2.2!
        Me.lblE_Account.MultiLine = False
        Me.lblE_Account.Name = "lblE_Account"
        Me.lblE_Account.Style = "font-size: 8pt"
        Me.lblE_Account.Text = "lblE_Account"
        Me.lblE_Account.Top = 0.0!
        Me.lblE_Account.Width = 1.0!
        '
        'lblREF_Bank2
        '
        Me.lblREF_Bank2.Height = 0.15!
        Me.lblREF_Bank2.HyperLink = Nothing
        Me.lblREF_Bank2.Left = 2.2!
        Me.lblREF_Bank2.Name = "lblREF_Bank2"
        Me.lblREF_Bank2.Style = "font-size: 8pt"
        Me.lblREF_Bank2.Text = "lblREF_Bank2-ExecutionRef"
        Me.lblREF_Bank2.Top = 0.15!
        Me.lblREF_Bank2.Width = 1.0!
        '
        'lblREF_Bank1
        '
        Me.lblREF_Bank1.Height = 0.15!
        Me.lblREF_Bank1.HyperLink = Nothing
        Me.lblREF_Bank1.Left = 2.2!
        Me.lblREF_Bank1.Name = "lblREF_Bank1"
        Me.lblREF_Bank1.Style = "font-size: 8pt"
        Me.lblREF_Bank1.Text = "lblREF_Bank1-Giroref"
        Me.lblREF_Bank1.Top = 0.3!
        Me.lblREF_Bank1.Width = 1.0!
        '
        'lblDATE_Value
        '
        Me.lblDATE_Value.Height = 0.15!
        Me.lblDATE_Value.HyperLink = Nothing
        Me.lblDATE_Value.Left = 2.2!
        Me.lblDATE_Value.Name = "lblDATE_Value"
        Me.lblDATE_Value.Style = "font-size: 8pt"
        Me.lblDATE_Value.Text = "lblDATE_Value"
        Me.lblDATE_Value.Top = 0.45!
        Me.lblDATE_Value.Width = 1.0!
        '
        'lblVoucherNo
        '
        Me.lblVoucherNo.Height = 0.15!
        Me.lblVoucherNo.HyperLink = Nothing
        Me.lblVoucherNo.Left = 2.2!
        Me.lblVoucherNo.Name = "lblVoucherNo"
        Me.lblVoucherNo.Style = "font-size: 8pt"
        Me.lblVoucherNo.Text = "lblVoucherNo"
        Me.lblVoucherNo.Top = 0.6!
        Me.lblVoucherNo.Width = 1.0!
        '
        'lblExtraD
        '
        Me.lblExtraD.Height = 0.15!
        Me.lblExtraD.HyperLink = Nothing
        Me.lblExtraD.Left = 2.2!
        Me.lblExtraD.Name = "lblExtraD"
        Me.lblExtraD.Style = "font-size: 8pt"
        Me.lblExtraD.Text = "lblExtraD"
        Me.lblExtraD.Top = 0.9!
        Me.lblExtraD.Width = 1.0!
        '
        'txtBabelBank_ID
        '
        Me.txtBabelBank_ID.DataField = "BabelBank_ID"
        Me.txtBabelBank_ID.Height = 0.15!
        Me.txtBabelBank_ID.Left = 0.5!
        Me.txtBabelBank_ID.Name = "txtBabelBank_ID"
        Me.txtBabelBank_ID.Style = "font-size: 8pt"
        Me.txtBabelBank_ID.Text = "txtBabelBank_ID"
        Me.txtBabelBank_ID.Top = 0.9!
        Me.txtBabelBank_ID.Width = 1.5!
        '
        'lblBB_ID
        '
        Me.lblBB_ID.Height = 0.15!
        Me.lblBB_ID.HyperLink = Nothing
        Me.lblBB_ID.Left = 0.0!
        Me.lblBB_ID.MultiLine = False
        Me.lblBB_ID.Name = "lblBB_ID"
        Me.lblBB_ID.Style = "font-size: 8pt"
        Me.lblBB_ID.Text = "BB_Id:"
        Me.lblBB_ID.Top = 0.9!
        Me.lblBB_ID.Width = 0.45!
        '
        'txtFreetext
        '
        Me.txtFreetext.DataField = "PaymentConcerns"
        Me.txtFreetext.Height = 0.15!
        Me.txtFreetext.Left = 0.0!
        Me.txtFreetext.Name = "txtFreetext"
        Me.txtFreetext.Style = "font-size: 8pt"
        Me.txtFreetext.Text = "txtFreetext"
        Me.txtFreetext.Top = 1.357!
        Me.txtFreetext.Width = 4.75!
        '
        'lblConcerns
        '
        Me.lblConcerns.Height = 0.15!
        Me.lblConcerns.HyperLink = Nothing
        Me.lblConcerns.Left = 0.0!
        Me.lblConcerns.Name = "lblConcerns"
        Me.lblConcerns.Style = "font-size: 8pt"
        Me.lblConcerns.Text = "lblConcerns"
        Me.lblConcerns.Top = 1.207!
        Me.lblConcerns.Width = 1.0!
        '
        'lblHowMatched
        '
        Me.lblHowMatched.Height = 0.15!
        Me.lblHowMatched.HyperLink = Nothing
        Me.lblHowMatched.Left = 2.2!
        Me.lblHowMatched.Name = "lblHowMatched"
        Me.lblHowMatched.Style = "font-size: 8pt"
        Me.lblHowMatched.Text = "lblHowMatched"
        Me.lblHowMatched.Top = 0.7500001!
        Me.lblHowMatched.Width = 1.0!
        '
        'txtHowMatched
        '
        Me.txtHowMatched.DataField = "HowMatched"
        Me.txtHowMatched.Height = 0.15!
        Me.txtHowMatched.Left = 3.25!
        Me.txtHowMatched.Name = "txtHowMatched"
        Me.txtHowMatched.Style = "font-size: 8pt"
        Me.txtHowMatched.Text = "txtHowMatched"
        Me.txtHowMatched.Top = 0.7500001!
        Me.txtHowMatched.Width = 3.0!
        '
        'grPaymentFooter
        '
        Me.grPaymentFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.linePaymentFooter, Me.txtUnmatchedAmount, Me.lblTotalPayment, Me.lblMatchedAmount, Me.lblUnmatchedAmount, Me.txtTotalPayment, Me.txtMatchedAmount})
        Me.grPaymentFooter.Height = 0.281!
        Me.grPaymentFooter.Name = "grPaymentFooter"
        '
        'linePaymentFooter
        '
        Me.linePaymentFooter.Height = 0.0!
        Me.linePaymentFooter.Left = 0.0!
        Me.linePaymentFooter.LineWeight = 1.0!
        Me.linePaymentFooter.Name = "linePaymentFooter"
        Me.linePaymentFooter.Top = 0.15!
        Me.linePaymentFooter.Width = 6.5!
        Me.linePaymentFooter.X1 = 0.0!
        Me.linePaymentFooter.X2 = 6.5!
        Me.linePaymentFooter.Y1 = 0.15!
        Me.linePaymentFooter.Y2 = 0.15!
        '
        'txtUnmatchedAmount
        '
        Me.txtUnmatchedAmount.Height = 0.15!
        Me.txtUnmatchedAmount.Left = 5.65!
        Me.txtUnmatchedAmount.Name = "txtUnmatchedAmount"
        Me.txtUnmatchedAmount.OutputFormat = resources.GetString("txtUnmatchedAmount.OutputFormat")
        Me.txtUnmatchedAmount.Style = "font-size: 8pt; font-weight: bold; text-align: right"
        Me.txtUnmatchedAmount.Text = "txtUnmatchedAmount"
        Me.txtUnmatchedAmount.Top = 0.0!
        Me.txtUnmatchedAmount.Width = 0.8!
        '
        'lblTotalPayment
        '
        Me.lblTotalPayment.Height = 0.15!
        Me.lblTotalPayment.HyperLink = Nothing
        Me.lblTotalPayment.Left = 1.75!
        Me.lblTotalPayment.Name = "lblTotalPayment"
        Me.lblTotalPayment.Style = "font-size: 8pt; font-weight: bold; text-align: left"
        Me.lblTotalPayment.Text = "lblTotalPayment"
        Me.lblTotalPayment.Top = 0.0!
        Me.lblTotalPayment.Width = 0.75!
        '
        'lblMatchedAmount
        '
        Me.lblMatchedAmount.Height = 0.15!
        Me.lblMatchedAmount.HyperLink = Nothing
        Me.lblMatchedAmount.Left = 3.35!
        Me.lblMatchedAmount.Name = "lblMatchedAmount"
        Me.lblMatchedAmount.Style = "font-size: 8pt; font-weight: bold; text-align: left"
        Me.lblMatchedAmount.Text = "lblMatchedAmount"
        Me.lblMatchedAmount.Top = 0.0!
        Me.lblMatchedAmount.Width = 0.7!
        '
        'lblUnmatchedAmount
        '
        Me.lblUnmatchedAmount.Height = 0.15!
        Me.lblUnmatchedAmount.HyperLink = Nothing
        Me.lblUnmatchedAmount.Left = 4.95!
        Me.lblUnmatchedAmount.Name = "lblUnmatchedAmount"
        Me.lblUnmatchedAmount.Style = "font-size: 8pt; font-weight: bold; text-align: left"
        Me.lblUnmatchedAmount.Text = "lblUnmatchedAmount"
        Me.lblUnmatchedAmount.Top = 0.0!
        Me.lblUnmatchedAmount.Width = 0.7!
        '
        'txtTotalPayment
        '
        Me.txtTotalPayment.DataField = "TotalPayment"
        Me.txtTotalPayment.Height = 0.15!
        Me.txtTotalPayment.Left = 2.5!
        Me.txtTotalPayment.Name = "txtTotalPayment"
        Me.txtTotalPayment.OutputFormat = resources.GetString("txtTotalPayment.OutputFormat")
        Me.txtTotalPayment.Style = "font-size: 8pt; font-weight: bold; text-align: right"
        Me.txtTotalPayment.Text = "txtTotalPayment"
        Me.txtTotalPayment.Top = 0.0!
        Me.txtTotalPayment.Width = 0.8!
        '
        'txtMatchedAmount
        '
        Me.txtMatchedAmount.Height = 0.15!
        Me.txtMatchedAmount.Left = 4.1!
        Me.txtMatchedAmount.Name = "txtMatchedAmount"
        Me.txtMatchedAmount.OutputFormat = resources.GetString("txtMatchedAmount.OutputFormat")
        Me.txtMatchedAmount.Style = "font-size: 8pt; font-weight: bold; text-align: right"
        Me.txtMatchedAmount.Text = "txtMatchedAmount"
        Me.txtMatchedAmount.Top = 0.0!
        Me.txtMatchedAmount.Width = 0.8!
        '
        'grPaymentInternationalHeader
        '
        Me.grPaymentInternationalHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grPaymentInternationalHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grPaymentInternationalHeader.Height = 0.1249996!
        Me.grPaymentInternationalHeader.KeepTogether = True
        Me.grPaymentInternationalHeader.Name = "grPaymentInternationalHeader"
        '
        'grPaymentInternationalFooter
        '
        Me.grPaymentInternationalFooter.CanShrink = True
        Me.grPaymentInternationalFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblOriginalAMountUsed})
        Me.grPaymentInternationalFooter.Height = 0.2708333!
        Me.grPaymentInternationalFooter.Name = "grPaymentInternationalFooter"
        '
        'lblOriginalAMountUsed
        '
        Me.lblOriginalAMountUsed.Height = 0.15!
        Me.lblOriginalAMountUsed.HyperLink = Nothing
        Me.lblOriginalAMountUsed.Left = 0.0!
        Me.lblOriginalAMountUsed.Name = "lblOriginalAMountUsed"
        Me.lblOriginalAMountUsed.Style = "font-size: 8pt"
        Me.lblOriginalAMountUsed.Text = "lblOriginalAMountUsed"
        Me.lblOriginalAMountUsed.Top = 0.05!
        Me.lblOriginalAMountUsed.Width = 3.0!
        '
        'grFreetextHeader
        '
        Me.grFreetextHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grFreetextHeader.CanShrink = True
        Me.grFreetextHeader.Height = 0.06916666!
        Me.grFreetextHeader.Name = "grFreetextHeader"
        '
        'grFreetextFooter
        '
        Me.grFreetextFooter.Height = 0.2291667!
        Me.grFreetextFooter.Name = "grFreetextFooter"
        Me.grFreetextFooter.Visible = False
        '
        'rp_1001_Matched_KID
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 6.500501!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.grBabelFileHeader)
        Me.Sections.Add(Me.grBatchHeader)
        Me.Sections.Add(Me.grPaymentReceiverHeader)
        Me.Sections.Add(Me.grPaymentHeader)
        Me.Sections.Add(Me.grPaymentInternationalHeader)
        Me.Sections.Add(Me.grFreetextHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grFreetextFooter)
        Me.Sections.Add(Me.grPaymentInternationalFooter)
        Me.Sections.Add(Me.grPaymentFooter)
        Me.Sections.Add(Me.grPaymentReceiverFooter)
        Me.Sections.Add(Me.grBatchFooter)
        Me.Sections.Add(Me.grBabelFileFooter)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.txtInvoice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInvoiceAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCustomer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtInvoiceCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblExtra1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtExtra1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMyField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMyField, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUniqueID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMatchedTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMatchedTotalCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMatchedBatchAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMatchedBatchCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPayor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Name, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_P_InvoiceAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Adr1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Adr2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Adr3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Zip, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_City, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREF_Bank2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREF_Bank1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Value, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVoucherNo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtExtraD, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_P_InvoiceCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblE_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblREF_Bank2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblREF_Bank1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDATE_Value, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblVoucherNo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblExtraD, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBabelBank_ID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBB_ID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFreetext, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblConcerns, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblHowMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtHowMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnmatchedAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalPayment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMatchedAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblUnmatchedAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalPayment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMatchedAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblOriginalAMountUsed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail
    Private WithEvents txtInvoice As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtInvoiceAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtCustomer As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtInvoiceCurrency As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblExtra1 As DataDynamics.ActiveReports.Label
    Private WithEvents txtExtra1 As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblMyField As DataDynamics.ActiveReports.Label
    Private WithEvents txtMyField As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtUniqueID As DataDynamics.ActiveReports.TextBox
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Private WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents linePageHeader As DataDynamics.ActiveReports.Line
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    Private WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents grBabelFileHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents grBabelFileFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents LineBabelFileFooter2 As DataDynamics.ActiveReports.Line
    Private WithEvents LineBabelFileFooter1 As DataDynamics.ActiveReports.Line
    Private WithEvents lblTotalNoOfPayments As DataDynamics.ActiveReports.Label
    Private WithEvents lblTotalAmount As DataDynamics.ActiveReports.Label
    Private WithEvents txtMatchedTotalAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMatchedTotalCount As DataDynamics.ActiveReports.TextBox
    Private WithEvents grBatchHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents shapeBatchHeader As DataDynamics.ActiveReports.Shape
    Private WithEvents txtI_Account As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtDATE_Production As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtClientName As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblDate_Production As DataDynamics.ActiveReports.Label
    Private WithEvents lblClient As DataDynamics.ActiveReports.Label
    Private WithEvents lblI_Account As DataDynamics.ActiveReports.Label
    Private WithEvents grBatchFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents lblBatchfooterAmount As DataDynamics.ActiveReports.Label
    Private WithEvents lblBatchfooterNoofPayments As DataDynamics.ActiveReports.Label
    Private WithEvents LineBatchFooter1 As DataDynamics.ActiveReports.Line
    Private WithEvents txtMatchedBatchAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMatchedBatchCount As DataDynamics.ActiveReports.TextBox
    Private WithEvents grPaymentReceiverHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents grPaymentReceiverFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents grPaymentHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents txtE_Name As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMON_P_InvoiceAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_Adr1 As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_Adr2 As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_Adr3 As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_Zip As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_City As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_Account As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtREF_Bank2 As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtDATE_Value As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtVoucherNo As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtExtraD As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMON_P_InvoiceCurrency As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblE_Account As DataDynamics.ActiveReports.Label
    Private WithEvents lblREF_Bank2 As DataDynamics.ActiveReports.Label
    Private WithEvents lblDATE_Value As DataDynamics.ActiveReports.Label
    Private WithEvents lblVoucherNo As DataDynamics.ActiveReports.Label
    Private WithEvents lblExtraD As DataDynamics.ActiveReports.Label
    Private WithEvents txtBabelBank_ID As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblBB_ID As DataDynamics.ActiveReports.Label
    Private WithEvents grPaymentFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents linePaymentFooter As DataDynamics.ActiveReports.Line
    Private WithEvents txtUnmatchedAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblTotalPayment As DataDynamics.ActiveReports.Label
    Private WithEvents lblMatchedAmount As DataDynamics.ActiveReports.Label
    Private WithEvents lblUnmatchedAmount As DataDynamics.ActiveReports.Label
    Private WithEvents txtTotalPayment As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMatchedAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents grPaymentInternationalHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents grPaymentInternationalFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents lblOriginalAMountUsed As DataDynamics.ActiveReports.Label
    Private WithEvents grFreetextHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents grFreetextFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents lblPayor As DataDynamics.ActiveReports.Label
    Private WithEvents txtREF_Bank1 As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblREF_Bank1 As DataDynamics.ActiveReports.Label
    Private WithEvents txtFreetext As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblConcerns As DataDynamics.ActiveReports.Label
    Private WithEvents lblHowMatched As DataDynamics.ActiveReports.Label
    Private WithEvents txtHowMatched As DataDynamics.ActiveReports.TextBox
End Class
