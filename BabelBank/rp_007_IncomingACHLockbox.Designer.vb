<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_007_IncomingACHLockbox
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(rp_007_IncomingACHLockbox))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.linePageHeader = New DataDynamics.ActiveReports.Line
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grBabelFileHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.grBabelFileFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.txtTotalNoOfPayments = New DataDynamics.ActiveReports.TextBox
        Me.lblTotalNoOfPayments = New DataDynamics.ActiveReports.Label
        Me.txtTotalAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblTotalAmount = New DataDynamics.ActiveReports.Label
        Me.LineBabelFileFooter2 = New DataDynamics.ActiveReports.Line
        Me.LineBabelFileFooter1 = New DataDynamics.ActiveReports.Line
        Me.grBatchHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapeBatchHeader = New DataDynamics.ActiveReports.Shape
        Me.txtI_Account = New DataDynamics.ActiveReports.TextBox
        Me.txtDATE_Production = New DataDynamics.ActiveReports.TextBox
        Me.txtClientName = New DataDynamics.ActiveReports.TextBox
        Me.lblDate_Production = New DataDynamics.ActiveReports.Label
        Me.lblClient = New DataDynamics.ActiveReports.Label
        Me.lblI_Account = New DataDynamics.ActiveReports.Label
        Me.txtFilename = New DataDynamics.ActiveReports.TextBox
        Me.grBatchFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.txtTotalDebitAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblTotalCreditAmount = New DataDynamics.ActiveReports.Label
        Me.txtTotalCreditAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblTotalDebitAmount = New DataDynamics.ActiveReports.Label
        Me.LineBatchFooter1 = New DataDynamics.ActiveReports.Line
        Me.grPaymentHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.txtE_Name = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_P_TransferredAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtLockboxnumber = New DataDynamics.ActiveReports.TextBox
        Me.txtExecutionRef = New DataDynamics.ActiveReports.TextBox
        Me.txtDATE_Value = New DataDynamics.ActiveReports.TextBox
        Me.txtReceivingCompanyID = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_P_TransferCurrency = New DataDynamics.ActiveReports.TextBox
        Me.lblLockboxno = New DataDynamics.ActiveReports.Label
        Me.lblCheckDate = New DataDynamics.ActiveReports.Label
        Me.lblREF_Own = New DataDynamics.ActiveReports.Label
        Me.lblReceivingCompany = New DataDynamics.ActiveReports.Label
        Me.txtCheckDate = New DataDynamics.ActiveReports.TextBox
        Me.lblCheckNumber = New DataDynamics.ActiveReports.Label
        Me.txtBIC = New DataDynamics.ActiveReports.TextBox
        Me.lblDATE_Value = New DataDynamics.ActiveReports.Label
        Me.txtBANK_Branch = New DataDynamics.ActiveReports.TextBox
        Me.lblOrderingCompany = New DataDynamics.ActiveReports.Label
        Me.lblArchiveRef = New DataDynamics.ActiveReports.Label
        Me.txtI_Name = New DataDynamics.ActiveReports.TextBox
        Me.lblREF_Bank2 = New DataDynamics.ActiveReports.Label
        Me.lblBatchNumber = New DataDynamics.ActiveReports.Label
        Me.txtREF_Bank1 = New DataDynamics.ActiveReports.TextBox
        Me.txtBatchNumber = New DataDynamics.ActiveReports.TextBox
        Me.txtItemNumber = New DataDynamics.ActiveReports.TextBox
        Me.lblItemNumber = New DataDynamics.ActiveReports.Label
        Me.lblFreetextSpecial = New DataDynamics.ActiveReports.Label
        Me.txtDebitCredit = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_AccountCurrency = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_AccountAmount = New DataDynamics.ActiveReports.TextBox
        Me.SubReportFreetextSpecial = New DataDynamics.ActiveReports.SubReport
        Me.grPaymentFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.grFreetextHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.SubRptFreetext = New DataDynamics.ActiveReports.SubReport
        Me.lblConcerns = New DataDynamics.ActiveReports.Label
        Me.grFreetextFooter = New DataDynamics.ActiveReports.GroupFooter
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalDebitAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalCreditAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalCreditAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalDebitAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Name, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_P_TransferredAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLockboxnumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtExecutionRef, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Value, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtReceivingCompanyID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_P_TransferCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblLockboxno, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCheckDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblREF_Own, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblReceivingCompany, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCheckDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCheckNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBIC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDATE_Value, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBANK_Branch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblOrderingCompany, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblArchiveRef, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Name, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblREF_Bank2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREF_Bank1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtItemNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblItemNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblFreetextSpecial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDebitCredit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_AccountCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_AccountAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblConcerns, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Height = 0.05138889!
        Me.Detail.Name = "Detail"
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.rptInfoPageHeaderDate, Me.linePageHeader})
        Me.PageHeader1.Height = 0.4479167!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 1.3!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "font-size: 18pt; text-align: center"
        Me.lblrptHeader.Text = "Payments with invoicedetails"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right"
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'linePageHeader
        '
        Me.linePageHeader.Height = 0.0!
        Me.linePageHeader.Left = 0.0!
        Me.linePageHeader.LineWeight = 1.0!
        Me.linePageHeader.Name = "linePageHeader"
        Me.linePageHeader.Top = 0.375!
        Me.linePageHeader.Width = 6.5!
        Me.linePageHeader.X1 = 0.0!
        Me.linePageHeader.X2 = 6.5!
        Me.linePageHeader.Y1 = 0.375!
        Me.linePageHeader.Y2 = 0.375!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter1.Height = 0.25!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right"
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right"
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grBabelFileHeader
        '
        Me.grBabelFileHeader.CanShrink = True
        Me.grBabelFileHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grBabelFileHeader.Height = 0.0625!
        Me.grBabelFileHeader.Name = "grBabelFileHeader"
        '
        'grBabelFileFooter
        '
        Me.grBabelFileFooter.CanShrink = True
        Me.grBabelFileFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtTotalNoOfPayments, Me.lblTotalNoOfPayments, Me.txtTotalAmount, Me.lblTotalAmount})
        Me.grBabelFileFooter.Height = 0.3541667!
        Me.grBabelFileFooter.Name = "grBabelFileFooter"
        '
        'txtTotalNoOfPayments
        '
        Me.txtTotalNoOfPayments.CanGrow = False
        Me.txtTotalNoOfPayments.DataField = "Grouping"
        Me.txtTotalNoOfPayments.DistinctField = "Grouping"
        Me.txtTotalNoOfPayments.Height = 0.15!
        Me.txtTotalNoOfPayments.Left = 4.6!
        Me.txtTotalNoOfPayments.Name = "txtTotalNoOfPayments"
        Me.txtTotalNoOfPayments.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; ddo-char-set: 0"
        Me.txtTotalNoOfPayments.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DCount
        Me.txtTotalNoOfPayments.SummaryGroup = "grBatchHeader"
        Me.txtTotalNoOfPayments.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalNoOfPayments.Text = "txtTotalNoOfPayments"
        Me.txtTotalNoOfPayments.Top = 0.1!
        Me.txtTotalNoOfPayments.Width = 0.3!
        '
        'lblTotalNoOfPayments
        '
        Me.lblTotalNoOfPayments.Height = 0.15!
        Me.lblTotalNoOfPayments.HyperLink = Nothing
        Me.lblTotalNoOfPayments.Left = 3.7!
        Me.lblTotalNoOfPayments.Name = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.lblTotalNoOfPayments.Text = "lblTotalNoOfPayments"
        Me.lblTotalNoOfPayments.Top = 0.1!
        Me.lblTotalNoOfPayments.Width = 0.9!
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.DataField = "MON_TransferredAmount"
        Me.txtTotalAmount.DistinctField = "Grouping"
        Me.txtTotalAmount.Height = 0.15!
        Me.txtTotalAmount.Left = 5.438!
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat")
        Me.txtTotalAmount.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; ddo-char-set: 0"
        Me.txtTotalAmount.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DSum
        Me.txtTotalAmount.SummaryGroup = "grBatchHeader"
        Me.txtTotalAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalAmount.Text = "txtTotalAmount"
        Me.txtTotalAmount.Top = 0.1!
        Me.txtTotalAmount.Width = 1.0!
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Height = 0.15!
        Me.lblTotalAmount.HyperLink = Nothing
        Me.lblTotalAmount.Left = 4.95!
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Style = "font-size: 8.25pt; font-weight: bold; ddo-char-set: 0"
        Me.lblTotalAmount.Text = "lblTotalAmount"
        Me.lblTotalAmount.Top = 0.1!
        Me.lblTotalAmount.Width = 0.5!
        '
        'LineBabelFileFooter2
        '
        Me.LineBabelFileFooter2.Height = 0.0!
        Me.LineBabelFileFooter2.Left = 2.6875!
        Me.LineBabelFileFooter2.LineWeight = 1.0!
        Me.LineBabelFileFooter2.Name = "LineBabelFileFooter2"
        Me.LineBabelFileFooter2.Top = 0.375!
        Me.LineBabelFileFooter2.Width = 2.8125!
        Me.LineBabelFileFooter2.X1 = 2.6875!
        Me.LineBabelFileFooter2.X2 = 5.5!
        Me.LineBabelFileFooter2.Y1 = 0.375!
        Me.LineBabelFileFooter2.Y2 = 0.375!
        '
        'LineBabelFileFooter1
        '
        Me.LineBabelFileFooter1.Height = 0.0!
        Me.LineBabelFileFooter1.Left = 2.6875!
        Me.LineBabelFileFooter1.LineWeight = 1.0!
        Me.LineBabelFileFooter1.Name = "LineBabelFileFooter1"
        Me.LineBabelFileFooter1.Top = 0.25!
        Me.LineBabelFileFooter1.Width = 2.8125!
        Me.LineBabelFileFooter1.X1 = 2.6875!
        Me.LineBabelFileFooter1.X2 = 5.5!
        Me.LineBabelFileFooter1.Y1 = 0.25!
        Me.LineBabelFileFooter1.Y2 = 0.25!
        '
        'grBatchHeader
        '
        Me.grBatchHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapeBatchHeader, Me.txtI_Account, Me.txtDATE_Production, Me.txtClientName, Me.lblDate_Production, Me.lblClient, Me.lblI_Account, Me.txtFilename})
        Me.grBatchHeader.DataField = "BreakField"
        Me.grBatchHeader.Height = 0.8025!
        Me.grBatchHeader.Name = "grBatchHeader"
        Me.grBatchHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'shapeBatchHeader
        '
        Me.shapeBatchHeader.Height = 0.5!
        Me.shapeBatchHeader.Left = 0.0!
        Me.shapeBatchHeader.Name = "shapeBatchHeader"
        Me.shapeBatchHeader.RoundingRadius = 9.999999!
        Me.shapeBatchHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapeBatchHeader.Top = 0.05!
        Me.shapeBatchHeader.Width = 6.45!
        '
        'txtI_Account
        '
        Me.txtI_Account.DataField = "I_Account"
        Me.txtI_Account.Height = 0.19!
        Me.txtI_Account.Left = 4.5!
        Me.txtI_Account.Name = "txtI_Account"
        Me.txtI_Account.Style = "font-size: 10pt; font-weight: normal"
        Me.txtI_Account.Text = "txtI_Account"
        Me.txtI_Account.Top = 0.3004167!
        Me.txtI_Account.Width = 1.9!
        '
        'txtDATE_Production
        '
        Me.txtDATE_Production.CanShrink = True
        Me.txtDATE_Production.Height = 0.19!
        Me.txtDATE_Production.Left = 1.5!
        Me.txtDATE_Production.Name = "txtDATE_Production"
        Me.txtDATE_Production.OutputFormat = resources.GetString("txtDATE_Production.OutputFormat")
        Me.txtDATE_Production.Style = "font-size: 10pt"
        Me.txtDATE_Production.Text = "txtDATE_Production"
        Me.txtDATE_Production.Top = 0.125!
        Me.txtDATE_Production.Width = 1.563!
        '
        'txtClientName
        '
        Me.txtClientName.CanGrow = False
        Me.txtClientName.Height = 0.19!
        Me.txtClientName.Left = 4.5!
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Style = "font-size: 10pt"
        Me.txtClientName.Text = "txtClientName"
        Me.txtClientName.Top = 0.1104167!
        Me.txtClientName.Width = 1.9!
        '
        'lblDate_Production
        '
        Me.lblDate_Production.Height = 0.19!
        Me.lblDate_Production.HyperLink = Nothing
        Me.lblDate_Production.Left = 0.125!
        Me.lblDate_Production.Name = "lblDate_Production"
        Me.lblDate_Production.Style = "font-size: 10pt"
        Me.lblDate_Production.Text = "lblDate_Production"
        Me.lblDate_Production.Top = 0.125!
        Me.lblDate_Production.Width = 1.313!
        '
        'lblClient
        '
        Me.lblClient.Height = 0.19!
        Me.lblClient.HyperLink = Nothing
        Me.lblClient.Left = 3.5!
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Style = "font-size: 10pt"
        Me.lblClient.Text = "lblClient"
        Me.lblClient.Top = 0.125!
        Me.lblClient.Width = 1.0!
        '
        'lblI_Account
        '
        Me.lblI_Account.Height = 0.19!
        Me.lblI_Account.HyperLink = Nothing
        Me.lblI_Account.Left = 3.5!
        Me.lblI_Account.Name = "lblI_Account"
        Me.lblI_Account.Style = "font-size: 10pt"
        Me.lblI_Account.Text = "lblI_Account"
        Me.lblI_Account.Top = 0.3004167!
        Me.lblI_Account.Width = 1.0!
        '
        'txtFilename
        '
        Me.txtFilename.DataField = "Filename"
        Me.txtFilename.Height = 0.19!
        Me.txtFilename.Left = 1.5!
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.Style = "font-size: 10pt; font-weight: normal"
        Me.txtFilename.Text = "txtFilename_ToMove"
        Me.txtFilename.Top = 0.3!
        Me.txtFilename.Width = 1.9!
        '
        'grBatchFooter
        '
        Me.grBatchFooter.CanShrink = True
        Me.grBatchFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtTotalDebitAmount})
        Me.grBatchFooter.Height = 0.25!
        Me.grBatchFooter.Name = "grBatchFooter"
        '
        'txtTotalDebitAmount
        '
        Me.txtTotalDebitAmount.DataField = "Grouping"
        Me.txtTotalDebitAmount.DistinctField = "Grouping"
        Me.txtTotalDebitAmount.Height = 0.125!
        Me.txtTotalDebitAmount.Left = 3.5625!
        Me.txtTotalDebitAmount.Name = "txtTotalDebitAmount"
        Me.txtTotalDebitAmount.Style = "font-size: 8.25pt; text-align: right; ddo-char-set: 0"
        Me.txtTotalDebitAmount.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DCount
        Me.txtTotalDebitAmount.SummaryGroup = "grBatchHeader"
        Me.txtTotalDebitAmount.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotalDebitAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotalDebitAmount.Text = "txtBatchfooterNoofPayments"
        Me.txtTotalDebitAmount.Top = 0.0!
        Me.txtTotalDebitAmount.Width = 0.875!
        '
        'lblTotalCreditAmount
        '
        Me.lblTotalCreditAmount.Height = 0.15!
        Me.lblTotalCreditAmount.HyperLink = Nothing
        Me.lblTotalCreditAmount.Left = 4.625!
        Me.lblTotalCreditAmount.Name = "lblTotalCreditAmount"
        Me.lblTotalCreditAmount.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.lblTotalCreditAmount.Text = "lblBatchfooterAmount"
        Me.lblTotalCreditAmount.Top = 0.0625!
        Me.lblTotalCreditAmount.Width = 0.5!
        '
        'txtTotalCreditAmount
        '
        Me.txtTotalCreditAmount.DataField = "MON_TransferredAmount"
        Me.txtTotalCreditAmount.DistinctField = "Grouping"
        Me.txtTotalCreditAmount.Height = 0.15!
        Me.txtTotalCreditAmount.Left = 5.3125!
        Me.txtTotalCreditAmount.Name = "txtTotalCreditAmount"
        Me.txtTotalCreditAmount.OutputFormat = resources.GetString("txtTotalCreditAmount.OutputFormat")
        Me.txtTotalCreditAmount.Style = "font-size: 8.25pt; text-align: right; ddo-char-set: 0"
        Me.txtTotalCreditAmount.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.DSum
        Me.txtTotalCreditAmount.SummaryGroup = "grBatchHeader"
        Me.txtTotalCreditAmount.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotalCreditAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotalCreditAmount.Text = "txtBatchfooterAmount"
        Me.txtTotalCreditAmount.Top = 0.0625!
        Me.txtTotalCreditAmount.Width = 1.0!
        '
        'lblTotalDebitAmount
        '
        Me.lblTotalDebitAmount.Height = 0.15!
        Me.lblTotalDebitAmount.HyperLink = Nothing
        Me.lblTotalDebitAmount.Left = 2.5625!
        Me.lblTotalDebitAmount.Name = "lblTotalDebitAmount"
        Me.lblTotalDebitAmount.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.lblTotalDebitAmount.Text = "lblBatchfooterNoofPayments"
        Me.lblTotalDebitAmount.Top = 0.0625!
        Me.lblTotalDebitAmount.Width = 0.95!
        '
        'LineBatchFooter1
        '
        Me.LineBatchFooter1.Height = 0.0!
        Me.LineBatchFooter1.Left = 2.9375!
        Me.LineBatchFooter1.LineWeight = 1.0!
        Me.LineBatchFooter1.Name = "LineBatchFooter1"
        Me.LineBatchFooter1.Top = 0.0625!
        Me.LineBatchFooter1.Width = 2.75!
        Me.LineBatchFooter1.X1 = 2.9375!
        Me.LineBatchFooter1.X2 = 5.6875!
        Me.LineBatchFooter1.Y1 = 0.0625!
        Me.LineBatchFooter1.Y2 = 0.0625!
        '
        'grPaymentHeader
        '
        Me.grPaymentHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grPaymentHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtE_Name, Me.txtMON_P_TransferredAmount, Me.txtLockboxnumber, Me.txtExecutionRef, Me.txtDATE_Value, Me.txtReceivingCompanyID, Me.txtMON_P_TransferCurrency, Me.lblLockboxno, Me.lblCheckDate, Me.lblREF_Own, Me.lblReceivingCompany, Me.txtCheckDate, Me.lblCheckNumber, Me.txtBIC, Me.lblDATE_Value, Me.txtBANK_Branch, Me.lblOrderingCompany, Me.lblArchiveRef, Me.txtI_Name, Me.lblREF_Bank2, Me.lblBatchNumber, Me.txtREF_Bank1, Me.txtBatchNumber, Me.txtItemNumber, Me.lblItemNumber, Me.lblFreetextSpecial, Me.txtDebitCredit, Me.txtMON_AccountCurrency, Me.txtMON_AccountAmount, Me.SubReportFreetextSpecial})
        Me.grPaymentHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grPaymentHeader.Height = 1.65625!
        Me.grPaymentHeader.Name = "grPaymentHeader"
        '
        'txtE_Name
        '
        Me.txtE_Name.DataField = "E_Name"
        Me.txtE_Name.Height = 0.15!
        Me.txtE_Name.Left = 0.0!
        Me.txtE_Name.Name = "txtE_Name"
        Me.txtE_Name.Style = "font-size: 8pt"
        Me.txtE_Name.Text = "txtE_Name"
        Me.txtE_Name.Top = 0.15!
        Me.txtE_Name.Width = 2.0!
        '
        'txtMON_P_TransferredAmount
        '
        Me.txtMON_P_TransferredAmount.DataField = "MON_TransferredAmount"
        Me.txtMON_P_TransferredAmount.Height = 0.15!
        Me.txtMON_P_TransferredAmount.Left = 5.45!
        Me.txtMON_P_TransferredAmount.Name = "txtMON_P_TransferredAmount"
        Me.txtMON_P_TransferredAmount.OutputFormat = resources.GetString("txtMON_P_TransferredAmount.OutputFormat")
        Me.txtMON_P_TransferredAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_P_TransferredAmount.Text = "txtMON_P_TransferredAmount"
        Me.txtMON_P_TransferredAmount.Top = 0.0!
        Me.txtMON_P_TransferredAmount.Width = 1.0!
        '
        'txtLockboxnumber
        '
        Me.txtLockboxnumber.Height = 0.15!
        Me.txtLockboxnumber.Left = 3.55!
        Me.txtLockboxnumber.Name = "txtLockboxnumber"
        Me.txtLockboxnumber.Style = "font-size: 8pt"
        Me.txtLockboxnumber.Text = "txtLockboxnumber"
        Me.txtLockboxnumber.Top = 0.0!
        Me.txtLockboxnumber.Width = 1.4!
        '
        'txtExecutionRef
        '
        Me.txtExecutionRef.Height = 0.3!
        Me.txtExecutionRef.Left = 1.2!
        Me.txtExecutionRef.Name = "txtExecutionRef"
        Me.txtExecutionRef.Style = "font-size: 8pt"
        Me.txtExecutionRef.Text = "txtExecutionRef"
        Me.txtExecutionRef.Top = 0.3!
        Me.txtExecutionRef.Width = 0.9!
        '
        'txtDATE_Value
        '
        Me.txtDATE_Value.Height = 0.15!
        Me.txtDATE_Value.Left = 3.55!
        Me.txtDATE_Value.Name = "txtDATE_Value"
        Me.txtDATE_Value.OutputFormat = resources.GetString("txtDATE_Value.OutputFormat")
        Me.txtDATE_Value.Style = "font-size: 8pt"
        Me.txtDATE_Value.Text = "txtDATE_Value"
        Me.txtDATE_Value.Top = 0.6!
        Me.txtDATE_Value.Width = 1.4!
        '
        'txtReceivingCompanyID
        '
        Me.txtReceivingCompanyID.Height = 0.15!
        Me.txtReceivingCompanyID.Left = 0.0!
        Me.txtReceivingCompanyID.Name = "txtReceivingCompanyID"
        Me.txtReceivingCompanyID.Style = "font-size: 8pt"
        Me.txtReceivingCompanyID.Text = "txtReceivingCompanyID"
        Me.txtReceivingCompanyID.Top = 0.9!
        Me.txtReceivingCompanyID.Width = 2.0!
        '
        'txtMON_P_TransferCurrency
        '
        Me.txtMON_P_TransferCurrency.DataField = "MON_TransferCurrency"
        Me.txtMON_P_TransferCurrency.Height = 0.15!
        Me.txtMON_P_TransferCurrency.Left = 5.0!
        Me.txtMON_P_TransferCurrency.Name = "txtMON_P_TransferCurrency"
        Me.txtMON_P_TransferCurrency.Style = "font-size: 8pt"
        Me.txtMON_P_TransferCurrency.Text = "PCur"
        Me.txtMON_P_TransferCurrency.Top = 0.0!
        Me.txtMON_P_TransferCurrency.Width = 0.4!
        '
        'lblLockboxno
        '
        Me.lblLockboxno.Height = 0.15!
        Me.lblLockboxno.HyperLink = Nothing
        Me.lblLockboxno.Left = 2.2!
        Me.lblLockboxno.MultiLine = False
        Me.lblLockboxno.Name = "lblLockboxno"
        Me.lblLockboxno.Style = "font-size: 8pt"
        Me.lblLockboxno.Text = "Lockbox number"
        Me.lblLockboxno.Top = 0.0!
        Me.lblLockboxno.Width = 1.3!
        '
        'lblCheckDate
        '
        Me.lblCheckDate.Height = 0.15!
        Me.lblCheckDate.HyperLink = Nothing
        Me.lblCheckDate.Left = 2.2!
        Me.lblCheckDate.Name = "lblCheckDate"
        Me.lblCheckDate.Style = "font-size: 8pt"
        Me.lblCheckDate.Text = "lblCheckDate"
        Me.lblCheckDate.Top = 0.45!
        Me.lblCheckDate.Width = 1.3!
        '
        'lblREF_Own
        '
        Me.lblREF_Own.Height = 0.15!
        Me.lblREF_Own.HyperLink = Nothing
        Me.lblREF_Own.Left = 2.2!
        Me.lblREF_Own.Name = "lblREF_Own"
        Me.lblREF_Own.Style = "font-size: 8pt"
        Me.lblREF_Own.Text = "lblREF_Own"
        Me.lblREF_Own.Top = 0.75!
        Me.lblREF_Own.Width = 1.3!
        '
        'lblReceivingCompany
        '
        Me.lblReceivingCompany.Height = 0.15!
        Me.lblReceivingCompany.HyperLink = Nothing
        Me.lblReceivingCompany.Left = 0.0!
        Me.lblReceivingCompany.Name = "lblReceivingCompany"
        Me.lblReceivingCompany.Style = "font-size: 8pt"
        Me.lblReceivingCompany.Text = "Receiving Company"
        Me.lblReceivingCompany.Top = 0.6!
        Me.lblReceivingCompany.Width = 1.5!
        '
        'txtCheckDate
        '
        Me.txtCheckDate.Height = 0.15!
        Me.txtCheckDate.Left = 3.55!
        Me.txtCheckDate.Name = "txtCheckDate"
        Me.txtCheckDate.Style = "font-size: 8pt"
        Me.txtCheckDate.Text = "txtCheckDate"
        Me.txtCheckDate.Top = 0.45!
        Me.txtCheckDate.Width = 1.4!
        '
        'lblCheckNumber
        '
        Me.lblCheckNumber.Height = 0.15!
        Me.lblCheckNumber.HyperLink = Nothing
        Me.lblCheckNumber.Left = 2.2!
        Me.lblCheckNumber.Name = "lblCheckNumber"
        Me.lblCheckNumber.Style = "font-size: 8pt"
        Me.lblCheckNumber.Text = "lblCheck Number:"
        Me.lblCheckNumber.Top = 0.3!
        Me.lblCheckNumber.Width = 1.3!
        '
        'txtBIC
        '
        Me.txtBIC.Height = 0.15!
        Me.txtBIC.Left = 3.55!
        Me.txtBIC.Name = "txtBIC"
        Me.txtBIC.Style = "font-size: 8pt"
        Me.txtBIC.Text = "txtBIC"
        Me.txtBIC.Top = 0.75!
        Me.txtBIC.Width = 1.4!
        '
        'lblDATE_Value
        '
        Me.lblDATE_Value.Height = 0.15!
        Me.lblDATE_Value.HyperLink = Nothing
        Me.lblDATE_Value.Left = 2.2!
        Me.lblDATE_Value.Name = "lblDATE_Value"
        Me.lblDATE_Value.Style = "font-size: 8pt"
        Me.lblDATE_Value.Text = "lblDATE_Value"
        Me.lblDATE_Value.Top = 0.6!
        Me.lblDATE_Value.Width = 1.3!
        '
        'txtBANK_Branch
        '
        Me.txtBANK_Branch.Height = 0.15!
        Me.txtBANK_Branch.Left = 3.55!
        Me.txtBANK_Branch.Name = "txtBANK_Branch"
        Me.txtBANK_Branch.Style = "font-size: 8pt"
        Me.txtBANK_Branch.Text = "txtBANK_Branch"
        Me.txtBANK_Branch.Top = 0.9!
        Me.txtBANK_Branch.Width = 1.4!
        '
        'lblOrderingCompany
        '
        Me.lblOrderingCompany.Height = 0.15!
        Me.lblOrderingCompany.HyperLink = Nothing
        Me.lblOrderingCompany.Left = 0.0!
        Me.lblOrderingCompany.MultiLine = False
        Me.lblOrderingCompany.Name = "lblOrderingCompany"
        Me.lblOrderingCompany.Style = "font-size: 8pt"
        Me.lblOrderingCompany.Text = "Ordering Company"
        Me.lblOrderingCompany.Top = 0.0!
        Me.lblOrderingCompany.Width = 1.2!
        '
        'lblArchiveRef
        '
        Me.lblArchiveRef.Height = 0.15!
        Me.lblArchiveRef.HyperLink = Nothing
        Me.lblArchiveRef.Left = 0.0!
        Me.lblArchiveRef.MultiLine = False
        Me.lblArchiveRef.Name = "lblArchiveRef"
        Me.lblArchiveRef.Style = "font-size: 8pt"
        Me.lblArchiveRef.Text = "Ordering Company ID"
        Me.lblArchiveRef.Top = 0.3!
        Me.lblArchiveRef.Width = 1.2!
        '
        'txtI_Name
        '
        Me.txtI_Name.DataField = "I_Name"
        Me.txtI_Name.Height = 0.15!
        Me.txtI_Name.Left = 0.0!
        Me.txtI_Name.Name = "txtI_Name"
        Me.txtI_Name.Style = "font-size: 8pt"
        Me.txtI_Name.Text = "txtI_Name"
        Me.txtI_Name.Top = 0.75!
        Me.txtI_Name.Width = 2.0!
        '
        'lblREF_Bank2
        '
        Me.lblREF_Bank2.Height = 0.15!
        Me.lblREF_Bank2.HyperLink = Nothing
        Me.lblREF_Bank2.Left = 2.2!
        Me.lblREF_Bank2.Name = "lblREF_Bank2"
        Me.lblREF_Bank2.Style = "font-size: 8pt"
        Me.lblREF_Bank2.Text = "lblREF_Bank2"
        Me.lblREF_Bank2.Top = 0.9!
        Me.lblREF_Bank2.Width = 1.3!
        '
        'lblBatchNumber
        '
        Me.lblBatchNumber.Height = 0.15!
        Me.lblBatchNumber.HyperLink = Nothing
        Me.lblBatchNumber.Left = 2.2!
        Me.lblBatchNumber.Name = "lblBatchNumber"
        Me.lblBatchNumber.Style = "font-size: 8pt"
        Me.lblBatchNumber.Text = "lblBatchNumber"
        Me.lblBatchNumber.Top = 1.05!
        Me.lblBatchNumber.Width = 1.3!
        '
        'txtREF_Bank1
        '
        Me.txtREF_Bank1.DataField = "REF_Bank1"
        Me.txtREF_Bank1.Height = 0.15!
        Me.txtREF_Bank1.Left = 3.55!
        Me.txtREF_Bank1.Name = "txtREF_Bank1"
        Me.txtREF_Bank1.Style = "font-size: 8pt"
        Me.txtREF_Bank1.Text = "txtREF_Bank1"
        Me.txtREF_Bank1.Top = 0.3!
        Me.txtREF_Bank1.Width = 1.4!
        '
        'txtBatchNumber
        '
        Me.txtBatchNumber.Height = 0.125!
        Me.txtBatchNumber.Left = 3.55!
        Me.txtBatchNumber.Name = "txtBatchNumber"
        Me.txtBatchNumber.Style = "font-size: 8pt"
        Me.txtBatchNumber.Text = "BatchNo"
        Me.txtBatchNumber.Top = 1.05!
        Me.txtBatchNumber.Width = 0.3!
        '
        'txtItemNumber
        '
        Me.txtItemNumber.Height = 0.15!
        Me.txtItemNumber.Left = 4.85!
        Me.txtItemNumber.Name = "txtItemNumber"
        Me.txtItemNumber.Style = "font-size: 8pt"
        Me.txtItemNumber.Text = "txtItemNo"
        Me.txtItemNumber.Top = 1.05!
        Me.txtItemNumber.Width = 0.5!
        '
        'lblItemNumber
        '
        Me.lblItemNumber.Height = 0.15!
        Me.lblItemNumber.HyperLink = Nothing
        Me.lblItemNumber.Left = 3.85!
        Me.lblItemNumber.Name = "lblItemNumber"
        Me.lblItemNumber.Style = "font-size: 8pt"
        Me.lblItemNumber.Text = "lblItemNumber"
        Me.lblItemNumber.Top = 1.05!
        Me.lblItemNumber.Width = 1.0!
        '
        'lblFreetextSpecial
        '
        Me.lblFreetextSpecial.Height = 0.125!
        Me.lblFreetextSpecial.HyperLink = Nothing
        Me.lblFreetextSpecial.Left = 4.0!
        Me.lblFreetextSpecial.MultiLine = False
        Me.lblFreetextSpecial.Name = "lblFreetextSpecial"
        Me.lblFreetextSpecial.Style = "font-size: 8pt"
        Me.lblFreetextSpecial.Text = "lblFreetextSpecial"
        Me.lblFreetextSpecial.Top = 1.375!
        Me.lblFreetextSpecial.Width = 2.0625!
        '
        'txtDebitCredit
        '
        Me.txtDebitCredit.Height = 0.125!
        Me.txtDebitCredit.Left = 5.625!
        Me.txtDebitCredit.Name = "txtDebitCredit"
        Me.txtDebitCredit.Style = "font-size: 8pt"
        Me.txtDebitCredit.Text = "DC"
        Me.txtDebitCredit.Top = 0.9375!
        Me.txtDebitCredit.Width = 0.75!
        '
        'txtMON_AccountCurrency
        '
        Me.txtMON_AccountCurrency.DataField = "MON_AccountCurrency"
        Me.txtMON_AccountCurrency.Height = 0.15!
        Me.txtMON_AccountCurrency.Left = 5.0!
        Me.txtMON_AccountCurrency.Name = "txtMON_AccountCurrency"
        Me.txtMON_AccountCurrency.Style = "font-size: 8pt"
        Me.txtMON_AccountCurrency.Text = "ACur"
        Me.txtMON_AccountCurrency.Top = 0.15!
        Me.txtMON_AccountCurrency.Width = 0.4!
        '
        'txtMON_AccountAmount
        '
        Me.txtMON_AccountAmount.DataField = "MON_AccountAmount"
        Me.txtMON_AccountAmount.Height = 0.15!
        Me.txtMON_AccountAmount.Left = 5.45!
        Me.txtMON_AccountAmount.Name = "txtMON_AccountAmount"
        Me.txtMON_AccountAmount.OutputFormat = resources.GetString("txtMON_AccountAmount.OutputFormat")
        Me.txtMON_AccountAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_AccountAmount.Text = "txtMON_AccountAmount"
        Me.txtMON_AccountAmount.Top = 0.15!
        Me.txtMON_AccountAmount.Width = 1.0!
        '
        'SubReportFreetextSpecial
        '
        Me.SubReportFreetextSpecial.CloseBorder = False
        Me.SubReportFreetextSpecial.Height = 0.15!
        Me.SubReportFreetextSpecial.Left = 0.0!
        Me.SubReportFreetextSpecial.Name = "SubReportFreetextSpecial"
        Me.SubReportFreetextSpecial.Report = Nothing
        Me.SubReportFreetextSpecial.ReportName = "rp_Sub_Freetext"
        Me.SubReportFreetextSpecial.Top = 1.375!
        Me.SubReportFreetextSpecial.Width = 4.75!
        '
        'grPaymentFooter
        '
        Me.grPaymentFooter.CanShrink = True
        Me.grPaymentFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblTotalDebitAmount, Me.lblTotalCreditAmount, Me.txtTotalCreditAmount, Me.LineBatchFooter1, Me.LineBabelFileFooter1, Me.LineBabelFileFooter2})
        Me.grPaymentFooter.Height = 0.5104167!
        Me.grPaymentFooter.Name = "grPaymentFooter"
        '
        'grFreetextHeader
        '
        Me.grFreetextHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(245, Byte), Integer))
        Me.grFreetextHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.SubRptFreetext, Me.lblConcerns})
        Me.grFreetextHeader.Height = 0.6145833!
        Me.grFreetextHeader.Name = "grFreetextHeader"
        '
        'SubRptFreetext
        '
        Me.SubRptFreetext.CloseBorder = False
        Me.SubRptFreetext.Height = 0.15!
        Me.SubRptFreetext.Left = 0.0!
        Me.SubRptFreetext.Name = "SubRptFreetext"
        Me.SubRptFreetext.Report = Nothing
        Me.SubRptFreetext.ReportName = "rp_Sub_Freetext"
        Me.SubRptFreetext.Top = 0.3!
        Me.SubRptFreetext.Width = 4.75!
        '
        'lblConcerns
        '
        Me.lblConcerns.Height = 0.15!
        Me.lblConcerns.HyperLink = Nothing
        Me.lblConcerns.Left = 0.0!
        Me.lblConcerns.Name = "lblConcerns"
        Me.lblConcerns.Style = "font-size: 8pt"
        Me.lblConcerns.Text = "lblConcerns"
        Me.lblConcerns.Top = 0.3!
        Me.lblConcerns.Width = 1.0!
        '
        'grFreetextFooter
        '
        Me.grFreetextFooter.CanShrink = True
        Me.grFreetextFooter.Height = 0.03125!
        Me.grFreetextFooter.Name = "grFreetextFooter"
        Me.grFreetextFooter.Visible = False
        '
        'rp_007_IncomingACHLockbox
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 6.500501!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.grBabelFileHeader)
        Me.Sections.Add(Me.grBatchHeader)
        Me.Sections.Add(Me.grPaymentHeader)
        Me.Sections.Add(Me.grFreetextHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grFreetextFooter)
        Me.Sections.Add(Me.grPaymentFooter)
        Me.Sections.Add(Me.grBatchFooter)
        Me.Sections.Add(Me.grBabelFileFooter)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalDebitAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalCreditAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalCreditAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalDebitAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Name, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_P_TransferredAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLockboxnumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtExecutionRef, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Value, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtReceivingCompanyID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_P_TransferCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblLockboxno, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCheckDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblREF_Own, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblReceivingCompany, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCheckDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCheckNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBIC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDATE_Value, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBANK_Branch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblOrderingCompany, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblArchiveRef, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Name, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblREF_Bank2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREF_Bank1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtItemNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblItemNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblFreetextSpecial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDebitCredit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_AccountCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_AccountAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblConcerns, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Friend WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Friend WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents linePageHeader As DataDynamics.ActiveReports.Line
    Friend WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    Friend WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents grBabelFileHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents grBabelFileFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents LineBabelFileFooter2 As DataDynamics.ActiveReports.Line
    Friend WithEvents LineBabelFileFooter1 As DataDynamics.ActiveReports.Line
    Friend WithEvents txtTotalNoOfPayments As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblTotalNoOfPayments As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTotalAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblTotalAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents grBatchHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents shapeBatchHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents txtI_Account As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtDATE_Production As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtClientName As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblDate_Production As DataDynamics.ActiveReports.Label
    Friend WithEvents lblClient As DataDynamics.ActiveReports.Label
    Friend WithEvents lblI_Account As DataDynamics.ActiveReports.Label
    Friend WithEvents grBatchFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents lblTotalCreditAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTotalCreditAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblTotalDebitAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTotalDebitAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents LineBatchFooter1 As DataDynamics.ActiveReports.Line
    Friend WithEvents grPaymentHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents txtE_Name As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_P_TransferredAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtLockboxnumber As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtExecutionRef As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtDATE_Value As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtReceivingCompanyID As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_P_TransferCurrency As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblLockboxno As DataDynamics.ActiveReports.Label
    Friend WithEvents lblCheckDate As DataDynamics.ActiveReports.Label
    Friend WithEvents lblREF_Own As DataDynamics.ActiveReports.Label
    Friend WithEvents lblReceivingCompany As DataDynamics.ActiveReports.Label
    Friend WithEvents txtCheckDate As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblCheckNumber As DataDynamics.ActiveReports.Label
    Friend WithEvents txtBIC As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblDATE_Value As DataDynamics.ActiveReports.Label
    Friend WithEvents txtBANK_Branch As DataDynamics.ActiveReports.TextBox
    Friend WithEvents grPaymentFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents grFreetextHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents SubRptFreetext As DataDynamics.ActiveReports.SubReport
    Friend WithEvents lblConcerns As DataDynamics.ActiveReports.Label
    Friend WithEvents grFreetextFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents lblOrderingCompany As DataDynamics.ActiveReports.Label
    Friend WithEvents lblArchiveRef As DataDynamics.ActiveReports.Label
    Friend WithEvents txtI_Name As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblREF_Bank2 As DataDynamics.ActiveReports.Label
    Friend WithEvents lblBatchNumber As DataDynamics.ActiveReports.Label
    Friend WithEvents txtREF_Bank1 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtBatchNumber As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtItemNumber As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblItemNumber As DataDynamics.ActiveReports.Label
    Friend WithEvents lblFreetextSpecial As DataDynamics.ActiveReports.Label
    Friend WithEvents txtDebitCredit As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_AccountCurrency As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMON_AccountAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents SubReportFreetextSpecial As DataDynamics.ActiveReports.SubReport
    Private WithEvents txtFilename As DataDynamics.ActiveReports.TextBox
End Class
