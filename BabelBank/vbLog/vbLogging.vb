﻿Option Strict Off
Option Explicit On

Imports System.IO

Public Class vbLogging
    ' 12.09.2018 - added public constants;
    ' TypeError = 1         ' Log only errormessages
    ' TypeWarning = 2       ' Log also warningmessages
    ' TypeInfo = 4          ' Log also infomessages without details
    ' TypeInfoDetail = 8    ' Log all messages

    ' 12.09.2018
    'System.Diagnostics.TraceEventType.error = 2
    'System.Diagnostics.TraceEventType.warning = 4
    'System.Diagnostics.TraceEventType.information = 8
    'System.Diagnostics.TraceEventType.informationDetail = 16 ???

    Public Const vbLogEventTypeError = 1
    Public Const vbLogEventTypeWarning = 2
    Public Const vbLogEventTypeInformation = 4
    Public Const vbLogEventTypeInformationDetail = 8

    Private Const EVENTLOG_SUCCESS = 0
    Private Const EVENTLOG_ERROR_TYPE = 1
    Private Const EVENTLOG_WARNING_TYPE = 2
    Private Const EVENTLOG_INFORMATION_TYPE = 4
    Private Const EVENTLOG_AUDIT_SUCCESS = 8
    Private Const EVENTLOG_AUDIT_FAILURE = 10
    Public Enum eLogModes
        mLogAuto = 0
        mLogOff = 1
        mLogToFile = 2
        mLogToNT = 3
        mLogOverwrite = &H10
        mLogThreadID = &H20
    End Enum
    ' Class variables
    Private sLogPath As String
    Private iLogMode As Integer         ' WinLog, FileLog or Maillog
    Private iLogType As Integer         ' Error, warning, info, infodetail
    Private iLogDetail As Integer '         
    ' TypeError = 1         ' Log only errormessages
    ' TypeWarning = 2       ' Log also warningmessages
    ' TypeInfo = 4          ' Log also infomessages without details
    ' TypeInfoDetail = 8    ' Log all messages

    Private nAutoDeleteLines As Double  ' Number of lines to store before deleting
    Private bLogOverwrite As Boolean    ' Start with new logfile for each run of application
    Private sHeading As String          ' Subject in mail
    Private sApp As String              ' Application
    Public Sub New()
        sLogPath = ""
        iLogMode = eLogModes.mLogToFile
        iLogType = 2
        iLogDetail = 2
        nAutoDeleteLines = 500
        bLogOverwrite = True
        sHeading = ""
        sApp = ""

    End Sub
    'Private Sub class_terminate()
    Public Sub StopLog()
        Dim sErrDescription As String

        On Error GoTo errCleanUp

        If Not bLogOverwrite Then
            If Len(Dir(sLogPath)) > 0 Then
                ClearLog()
            End If
        End If
        MyBase.Finalize()

        Exit Sub

errCleanUp:
        '31.07.2008 - Changed the errorhandling, because of wrong arguments in the Err.Raise
        If Len(Trim$(sErrDescription)) = 0 Then
            sErrDescription = Err.Description
        End If

        Err.Raise(20454, "vbLog.StopLog - Shutdownerror logging to file", sErrDescription)

    End Sub
    Public Sub StartLog()  '(logTarget As String, LogMode As eLogModes)
        Dim sErrDescription As String

        On Error GoTo errCleanUp

        ' if new log each time, then delete file first;
        If bLogOverwrite Then
            'If Len(Dir(sLogPath)) > 0 Then
            '    Kill(sLogPath)
            'End If
            ' 14.09.2018 - changed to use System.IO.
            If File.Exists(sLogPath) Then
                File.Delete(sLogPath)
            End If
        End If
        Exit Sub

errCleanUp:
        If Len(Trim$(sErrDescription)) = 0 Then
            sErrDescription = Err.Description
        End If

        Err.Raise(20455, "vbLog - Startuperror logging to file", sErrDescription)

    End Sub
    Public Function AddLogEvent(ByVal sMessage As String, ByVal EventType As Object) As Boolean

        Dim iNumStrings As Integer
        Dim hEventLog As Long
        Dim hMsgs As Long
        Dim cbStringSize As Long
        Dim iEventID As Integer
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oStreamWriter As StreamWriter

        On Error GoTo errCleanUp
        'If EventType <= iLogType Then
        If EventType <= iLogDetail Then

            ' added 20.02.07, to better mark errormessages
            If EventType = EVENTLOG_ERROR_TYPE Then
                sMessage = ">>>>>>>>>>>>>>>>>>>>" & sMessage & "<<<<<<<<<<<<<<<<<<<<"
            End If

            ' Log events according to Type, as set in vbLog.LogDetail
            If EventType = vbLogEventTypeInformationDetail Then
                ' vbLogEventTypeInformationDetail is no real Visual Basic type
                EventType = EVENTLOG_INFORMATION_TYPE
            End If

            '-----------------------------
            ' Write to a specified logfile
            '-----------------------------
            ' if log file does not exist, create it;
            If Not File.Exists(sLogPath) Then
                File.Create(sLogPath).Dispose()  ' Dispose forces a close, and file is releases!
            End If
            '05.06.2020 - Addded encoding parameter for SG Finans. No parameter gave UTF-8
            oStreamWriter = New StreamWriter(sLogPath, True, System.Text.Encoding.Default)

            If EventType = 0 And InStr(sMessage, "--------------------------------------------") > 0 Then
                oStreamWriter.WriteLine(Trim$(sMessage))
            Else
                oStreamWriter.WriteLine(Format(Now, "yyyy.MM.dd HH:mm:ss") & " " & sApp & Chr(9) & Trim$(sHeading) & vbCrLf & "-" & Space(17) & Trim$(sMessage))
            End If
            oStreamWriter.Close()
            oStreamWriter = Nothing
        End If

        AddLogEvent = True

        Exit Function

errCleanUp:

        AddLogEvent = False
        Err.Raise(20456, "vbLog - Error logging to file", Err.Description)
    End Function

    Public Function AddLogEvent_Remove(ByVal sMessage As String, ByVal EventType As Object) As Boolean
        ' Remove this one when Stream has been tested

        Dim iNumStrings As Integer
        Dim hEventLog As Long
        Dim hMsgs As Long
        Dim cbStringSize As Long
        Dim iEventID As Integer
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream

        On Error GoTo errCleanUp
        'If EventType <= iLogType Then
        If EventType <= iLogDetail Then

            ' added 20.02.07, to better mark errormessages
            If EventType = EVENTLOG_ERROR_TYPE Then
                sMessage = ">>>>>>>>>>>>>>>>>>>>" & sMessage & "<<<<<<<<<<<<<<<<<<<<"
            End If

            ' Log events according to Type, as set in vbLog.LogDetail
            If EventType = vbLogEventTypeInformationDetail Then
                ' vbLogEventTypeInformationDetail is no real Visual Basic type
                EventType = EVENTLOG_INFORMATION_TYPE

            End If

            '-----------------------------
            ' Write to a specified logfile
            '-----------------------------
            oFs = New Scripting.FileSystemObject
            oFile = oFs.OpenTextFile(sLogPath, Scripting.IOMode.ForAppending, True, 0)
            ' added 30.11.2007, to make the report a bit prettier
            If EventType = 0 And InStr(sMessage, "--------------------------------------------") > 0 Then
                oFile.WriteLine(Trim$(sMessage))
            Else
                oFile.WriteLine(Format(Now, "yyyy.MM.dd hh:mm:ss") & " " & sApp & Chr(9) & Trim$(sHeading) & vbCrLf & "-" & Space(17) & Trim$(sMessage))
            End If
            oFile.Close()
            oFile = Nothing
            oFs = Nothing
        End If

        AddLogEvent_Remove = True

        Exit Function

errCleanUp:

        AddLogEvent_Remove = False
        Err.Raise(20456, "vbLog - Error logging to file", Err.Description)
    End Function
    Public Sub ClearLog()
        ' Remove this one when Stream has been tested
        ' Delete lines from log

        Dim iLineCounter As Long
        Dim aLines() As String
        Dim sLine As String
        Dim oStreamReader As StreamReader
        Dim oStreamWriter As StreamWriter

        'Pass the file path and the file name to the StreamReader constructor.
        oStreamReader = New StreamReader(sLogPath)

        ReDim aLines(0)
        ' Read from logfile into aLines
        Do While Not oStreamReader.EndOfStream
            iLineCounter = iLineCounter + 1
            sLine = oStreamReader.ReadLine()
            If iLineCounter > 1 Then
                ReDim Preserve aLines(UBound(aLines) + 1)
            End If
            aLines(UBound(aLines)) = sLine
        Loop
        oStreamReader.Close()
        oStreamReader = Nothing

        If iLineCounter > nAutoDeleteLines Then
            If File.Exists(sLogPath) Then
                File.Delete(sLogPath)
            End If

            '05.06.2020 - Addded encoding parameter for SG Finans. No parameter gave UTF-8
            oStreamWriter = New StreamWriter(sLogPath, True, System.Text.Encoding.Default)
            If UBound(aLines) < nAutoDeleteLines Then
                nAutoDeleteLines = UBound(aLines) + 1
            End If
            For iLineCounter = UBound(aLines) - nAutoDeleteLines + 1 To UBound(aLines)
                oStreamWriter.WriteLine(aLines(iLineCounter))
            Next iLineCounter
            oStreamWriter.Close()
            oStreamWriter = Nothing

        End If

    End Sub
    Public Sub ClearLog_Remove()
        ' Remove this one when Stream has been tested
        ' Delete lines from log

        Dim iLineCounter As Long
        Dim aLines() As String
        Dim sLine As String
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream

        ' this sub has been changed 08.03.07 to use FileSystemObject
        ReDim aLines(0)
        oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
        'Set oFile = oFs.OpenTextFile(sLogPath, ForReading, True, 0)
        oFile = oFs.OpenTextFile(sLogPath, Scripting.IOMode.ForReading, False, 0)  ' 11.08.2008 Satt Create = False, da filen må finnes siden vi kun skal lese i den

        ' Read from logfile into aLines
        Do While Not oFile.AtEndOfStream
            iLineCounter = iLineCounter + 1
            sLine = oFile.ReadLine()
            If iLineCounter > 1 Then
                ReDim Preserve aLines(UBound(aLines) + 1)
            End If
            aLines(UBound(aLines)) = sLine
        Loop
        oFile.Close()
        oFile = Nothing

        If iLineCounter > nAutoDeleteLines Then

            oFs.DeleteFile(sLogPath, False)
            oFile = oFs.OpenTextFile(sLogPath, Scripting.IOMode.ForAppending, True, 0)

            If UBound(aLines) < nAutoDeleteLines Then
                nAutoDeleteLines = UBound(aLines) + 1
            End If
            For iLineCounter = UBound(aLines) - nAutoDeleteLines + 1 To UBound(aLines)
                oFile.WriteLine(aLines(iLineCounter))
            Next iLineCounter
            oFile.Close()
            oFile = Nothing

        End If
        oFs = Nothing

    End Sub
    Public Property LogPath() As String
        Get
            Return sLogPath
        End Get
        Set(ByVal value As String)
            sLogPath = value
        End Set
    End Property
    Public Property LogType() As Integer
        ' Log to
        ' 1 = Windows log NOT IN USE
        ' 2 = Log to file, specified by LogType
        ' 3 = Send log with mail, possibly SMTP-mail NOT IN USE
        Get
            Return iLogType
        End Get
        Set(ByVal value As Integer)
            iLogType = value
        End Set
    End Property
    Public Property LogDetail() As Integer
        ' TypeError = 1         ' Log only errormessages
        ' TypeWarning = 2       ' Log also warningmessages
        ' TypeInfo = 4          ' Log also infomessages without details
        ' TypeInfoDetail = 8    ' Log all messages
        Get
            Return iLogDetail
        End Get
        Set(ByVal value As Integer)
            iLogDetail = value
        End Set
    End Property
    Public Property AutoDeleteLines() As Double
        ' Number of lines to save in log
        Get
            Return nAutoDeleteLines
        End Get
        Set(ByVal value As Double)
            nAutoDeleteLines = value
        End Set

    End Property
    Public Property LogOverWrite() As Boolean
        ' Start with new logg for each application startup
        Get
            Return bLogOverwrite
        End Get
        Set(ByVal value As Boolean)
            bLogOverwrite = value
        End Set

    End Property
    Public Property Heading() As String
        Get
            Return sHeading
        End Get
        Set(ByVal value As String)
            sHeading = value
        End Set
    End Property

End Class
