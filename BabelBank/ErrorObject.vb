﻿Option Strict Off
Option Explicit On
Public Class ErrorObject
    '-------------------------------------------------------------
    ' ErrorObject, a collecion of one or more errors for this payment
    ' - CollectionLevel - Level in collection (BabelFile, Batch, Payment, Invoice, Freetext)
    ' - Propertyname  - Which field in collection has error
    ' - PropertyValue - Value for the errounous property
    ' - GridColumnName - ColumnName used in grid
    ' - Errormessage  - Errortext, languagedependent
    '-------------------------------------------------------------

    Private nIndex As Double
    Private sCollectionLevel As String
    Private sPropertyName As String
    Private sPropertyValue As String
    Private sGridColumnName As String
    Private sErrorMessage As String
    Private sErrorType As String 'Error, Warning
    '********* START PROPERTY SETTINGS ***********************
    Public Property Index() As Double
        Get
            Index = nIndex
        End Get
        Set(ByVal Value As Double)
            nIndex = Value
        End Set
    End Property
    Public Property CollectionLevel() As String
        Get
            CollectionLevel = sCollectionLevel
        End Get
        Set(ByVal Value As String)
            sCollectionLevel = Value
        End Set
    End Property
    Public Property PropertyName() As String
        Get
            PropertyName = sPropertyName
        End Get
        Set(ByVal Value As String)
            sPropertyName = Value
        End Set
    End Property
    Public Property PropertyValue() As String
        Get
            PropertyValue = sPropertyValue
        End Get
        Set(ByVal Value As String)
            sPropertyValue = Value
        End Set
    End Property
    Public Property GridColumnName() As String
        Get
            GridColumnName = sGridColumnName
        End Get
        Set(ByVal Value As String)
            sGridColumnName = Value
        End Set
    End Property
    Public Property ErrorMessage() As String
        Get
            ErrorMessage = sErrorMessage
        End Get
        Set(ByVal Value As String)
            sErrorMessage = Value
        End Set
    End Property
    Public Property ErrorType() As String
        Get
            ErrorType = sErrorType
        End Get
        Set(ByVal Value As String)
            sErrorType = Value
        End Set
    End Property
    Private Sub Class_Initialize_Renamed()
        sCollectionLevel = ""
        sPropertyName = ""
        sPropertyValue = ""
        sGridColumnName = ""
        sErrorMessage = ""

    End Sub

End Class
