Option Strict Off
Option Explicit On
Module WriteHDP_Citibank
	
    Function WriteHDP_CitibankFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef iFilenameInNo As Short) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream

        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice

        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bFileFromBank As Boolean
        Dim sNewOwnref As String
        Dim sFreetext, sDate As String
        Dim sErrorMessage, sFieldname As String

        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                If oBabel.VB_FilenameInNo = iFilenameInNo Then

                    bExportoBabel = False
                    'Have to go through each batch-object to see if we have objects that shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If
                        Next oPayment
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oBatch

                    If bExportoBabel Then
                        'Set if the file was created int the accounting system or not.
                        bFileFromBank = oBabel.FileFromBank
                        'i = 0 'FIXED Moved by Janp under case 2


                        'Loop through all Batch objs. in BabelFile obj.
                        For Each oBatch In oBabel.Batches
                            bExportoBatch = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oPayment In oBatch.Payments
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If
                                'If true exit the loop, and we have data to export
                                If bExportoBatch Then
                                    Exit For
                                End If
                            Next oPayment


                            If bExportoBatch Then

                                '                    i = i + 1
                                '
                                '                    'Get the companyno either from a property passed from BabelExport, or
                                '                    ' from the imported file
                                '                    If sCompanyNo <> "" Then
                                '                        sI_EnterpriseNo = sCompanyNo
                                '                    Else
                                '                        sI_EnterpriseNo = oBatch.I_EnterpriseNo
                                '                    End If
                                '                    'BETFOR00
                                '                    If bWriteRecord10 Then
                                '                        sLine = WriteRecord10(sI_Account, bEuroAccount, oBatch.Payments(1).ERA_ExchRateAgreed, oBatch.Payments(1).ERA_DealMadeWith, oBatch.Payments(1).MON_TransferCurrency, sFilenameOut)
                                '                        oFile.WriteLine (sLine)
                                '                        bWriteRecord10 = False
                                '                        lNumberofRecords = lNumberofRecords + 1
                                '                        'New record10 for this vriables
                                '                        sLastAgreedRate = oBatch.Payments(1).ERA_ExchRateAgreed
                                '                        sLastAccount = sI_Account
                                '                        sLastCurrencyCode = oBatch.Payments(1).MON_TransferCurrency
                                '                    End If

                                For Each oPayment In oBatch.Payments


                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    End If

                                    If bExportoPayment Then

                                        For Each oInvoice In oPayment.Invoices

                                            'Test first if it is a domestic payment or not
                                            If oPayment.PayType <> "I" Then

                                                'Transaction Type
                                                oFile.WriteLine(("0"))
                                                oFile.WriteLine(("11"))

                                                'Reference
                                                If Len(oPayment.REF_Own) > 0 Then
                                                    If Len(oPayment.REF_Own) < 11 Then
                                                        oFile.WriteLine(("1"))
                                                        oFile.WriteLine((oPayment.REF_Own))
                                                    Else
                                                        oFile.WriteLine(("1"))
                                                        oFile.WriteLine(Left(oPayment.REF_Own, 10))
                                                    End If
                                                Else
                                                    'No refererence exist. It's mandatory.
                                                    sFieldname = "Reference"
                                                    Err.Raise(1, , "Unexpected error during export.")
                                                End If

                                                'Debit account
                                                If Len(oPayment.I_Account) > 0 Then
                                                    If Len(Trim(oPayment.I_Account)) < 11 Then
                                                        oFile.WriteLine(("2"))
                                                        oFile.WriteLine((Trim(oPayment.I_Account)))
                                                    Else
                                                        oFile.WriteLine(("2"))
                                                        oFile.WriteLine(Left(Trim(oPayment.I_Account), 10))
                                                    End If
                                                Else
                                                    'No debit account exist. It's mandatory.
                                                    sFieldname = "Debit account"
                                                    Err.Raise(1, , "Unexpected error during export.")
                                                End If

                                                'Not in use - Name
                                                oFile.WriteLine(("3"))
                                                oFile.WriteLine((""))

                                                'Amount
                                                oFile.WriteLine(("4"))
                                                If oInvoice.MON_InvoiceAmount >= 0 Then
                                                    oFile.WriteLine((Left(Trim(Str(oInvoice.MON_InvoiceAmount)), Len(Trim(Str(oInvoice.MON_InvoiceAmount))) - 2) & "." & Right(Trim(Str(oInvoice.MON_InvoiceAmount)), 2)))
                                                    'oFile.WriteLine (VB6.Format(oInvoice.MON_InvoiceAmount / 100, "###0.00"))
                                                Else
                                                    sErrorMessage = LRS(35040)
                                                    sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                    Err.Raise(35040, , sErrorMessage)
                                                End If

                                                'Bookdate
                                                sDate = oPayment.DATE_Payment
                                                If sDate <> "19900101" Then
                                                    oFile.WriteLine(("5"))
                                                    oFile.WriteLine((Right(sDate, 2) & "/" & Mid(sDate, 5, 2) & "/" & Mid(sDate, 3, 2)))
                                                Else
                                                    'No bookdate exist. It's mandatory.
                                                    sFieldname = "Book date"
                                                    Err.Raise(1, , "Unexpected error during export.")
                                                End If
                                                sFieldname = ""

                                                '                            The beneficiarys bankaccountnuber har the following rules:
                                                '                            Option A:
                                                '                            If the bank a/c nr. has 16 digits, then:
                                                '                            Field 6: last 8 digits of Account No ( 8 or 16 digits),
                                                '                            Field 9: the first 3 digits of the bank a/c nr.,
                                                '                            Field 10: the last 5 digits from the first 8 digits.
                                                '
                                                '                            Option B:
                                                '                            If the bank a/c nr. has 24 digits, then:
                                                '                            Field 6: last 16 digits of Account No ( 8 or 16 digits),
                                                '                            Field 9: the first 3 digits of the bank a/c nr.,
                                                '                            Field 10: the last 5 digits from the first 8 digits.
                                                If Not IsNumeric(oPayment.E_Account) Then
                                                    sErrorMessage = LRS(35033)
                                                    sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                    Err.Raise(35033, , sErrorMessage)
                                                Else
                                                    If Len(oPayment.E_Account) = 16 Then
                                                        oFile.WriteLine(("6"))
                                                        oFile.WriteLine((Right(oPayment.E_Account, 8)))
                                                        If Len(oPayment.E_Name) < 33 Then
                                                            oFile.WriteLine(("7"))
                                                            oFile.WriteLine((oPayment.E_Name))
                                                        Else
                                                            oFile.WriteLine(("7"))
                                                            oFile.WriteLine(Left(oPayment.E_Name, 32))
                                                        End If
                                                        oFile.WriteLine(("9"))
                                                        oFile.WriteLine((Left(oPayment.E_Account, 3)))
                                                        oFile.WriteLine(("10"))
                                                        oFile.WriteLine((Right(Left(oPayment.E_Account, 8), 5)))

                                                    ElseIf Len(oPayment.E_Account) = 24 Then
                                                        oFile.WriteLine(("6"))
                                                        oFile.WriteLine((Right(oPayment.E_Account, 16)))
                                                        If Len(oPayment.E_Name) < 33 Then
                                                            oFile.WriteLine(("7"))
                                                            oFile.WriteLine((oPayment.E_Name))
                                                        Else
                                                            oFile.WriteLine(("7"))
                                                            oFile.WriteLine(Left(oPayment.E_Name, 32))
                                                        End If
                                                        oFile.WriteLine(("9"))
                                                        oFile.WriteLine((Left(oPayment.E_Account, 3)))
                                                        oFile.WriteLine(("10"))
                                                        oFile.WriteLine((Right(Left(oPayment.E_Account, 8), 5)))

                                                    Else
                                                        sErrorMessage = Replace(LRS(35024), "%1", oPayment.E_Account)
                                                        sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, "") & oBabel.FilenameIn
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, "") & oInvoice.MON_InvoiceAmount
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                                                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, "") & oPayment.E_Name
                                                        Err.Raise(35024, , sErrorMessage)
                                                    End If
                                                End If

                                                'Beneficiary's bank name
                                                oFile.WriteLine(("12"))
                                                If Len(oPayment.BANK_Name) > 0 Then 'Always "" when domestic
                                                    oFile.WriteLine((oPayment.BANK_Name))
                                                Else
                                                    oFile.WriteLine((""))
                                                End If

                                                'Details of payment
                                                'In HDP you may have 2 freetextline, each max 27 positions
                                                oFile.WriteLine(("13"))
                                                If oInvoice.Freetexts.Count > 0 Then
                                                    ' OK, we have freetext
                                                    sFreetext = Trim(oInvoice.Freetexts.Item(1).Text)
                                                    If Len(sFreetext) < 28 Then
                                                        oFile.WriteLine((sFreetext))
                                                        'Check if we have more freetext-objects
                                                        If oInvoice.Freetexts.Count > 1 Then
                                                            sFreetext = oInvoice.Freetexts.Item(2).Text
                                                            If Len(sFreetext) < 28 Then
                                                                oFile.WriteLine((sFreetext))
                                                            Else
                                                                oFile.WriteLine((Left(sFreetext, 27)))
                                                            End If
                                                        Else
                                                            oFile.WriteLine((""))
                                                        End If
                                                    Else
                                                        oFile.WriteLine((Left(sFreetext, 27)))
                                                        If oInvoice.Freetexts.Count > 1 Then
                                                            'We have more than one line
                                                            sFreetext = sFreetext & " " & Trim(oInvoice.Freetexts.Item(2).Text)
                                                            sFreetext = Mid(sFreetext, Len(sFreetext) - 27)
                                                        Else
                                                            If Len(sFreetext) = 27 Then
                                                                sFreetext = ""
                                                            Else
                                                                sFreetext = Mid(sFreetext, 28)
                                                            End If
                                                        End If
                                                        If Len(sFreetext) > 0 Then
                                                            If Len(sFreetext) < 28 Then
                                                                oFile.WriteLine((sFreetext))
                                                            Else
                                                                oFile.WriteLine((Left(sFreetext, 27)))
                                                            End If
                                                        Else
                                                            oFile.WriteLine((""))
                                                        End If
                                                    End If
                                                Else
                                                    oFile.WriteLine((""))
                                                    oFile.WriteLine((""))
                                                End If

                                                'Service code? - Will be implemented by citibank 1/6-2003
                                                If Len(oInvoice.STATEBANK_Code) > 0 Then
                                                    oFile.WriteLine(("14"))
                                                    oFile.WriteLine((oInvoice.STATEBANK_Code))
                                                Else
                                                    'No servicecode exist. It's mandatory.
                                                    sFieldname = "Servicecode"
                                                    Err.Raise(1, , "Unexpected error during export.")
                                                End If

                                                sFieldname = ""
                                                oFile.WriteLine(("-1"))
                                                oPayment.Exported = True

                                            Else
                                                'International payment


                                            End If 'If oPayment.PayType = "I"

                                        Next oInvoice

                                    End If 'bExportoPayment


                                Next oPayment

                            End If 'bExportoBatch

                        Next oBatch

                    End If 'bExportoBabel

                End If 'oBabel.VB_FilenameInNo = iFilenameInNo

            Next oBabel

        Catch ex As Exception

            If Len(sFieldname) > 0 Then
                'Something is wrong regarding the content of an exportfield.
                Throw New vbBabel.Payment.PaymentException("Function: WriteHDP_CitibankFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)
            Else
                'just use the errormessage that has been created
                Throw New vbBabel.Payment.PaymentException("Function: WriteHDP_CitibankFile" & vbCrLf & Replace(LRS(35029), "%1", sFieldname) & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)
            End If

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteHDP_CitibankFile = True

    End Function
End Module
