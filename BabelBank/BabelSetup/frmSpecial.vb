Option Strict Off
Option Explicit On
Friend Class frmSpecial
	Inherits System.Windows.Forms.Form
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		SaveSpecial()
		Me.Hide()
	End Sub
	
	Private Sub frmSpecial_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		FormvbStyle(Me, "")
    End Sub
    Sub SaveSpecial()
        oFilesetup.TreatSpecial = Trim(Me.txtSpecial.Text)
    End Sub
End Class
