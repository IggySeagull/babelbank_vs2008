<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDnB_TBIW_Send
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtDivision As System.Windows.Forms.TextBox
    Public WithEvents cmdMappingFile As System.Windows.Forms.Button
	Public WithEvents cmdReport As System.Windows.Forms.Button
	Public WithEvents chkPrint As System.Windows.Forms.CheckBox
    Public WithEvents cmbFormat As System.Windows.Forms.ComboBox
	Public WithEvents txtCompany As System.Windows.Forms.TextBox
	Public WithEvents txtBackupPath As System.Windows.Forms.TextBox
	Public WithEvents txtFromInternal As System.Windows.Forms.TextBox
	Public WithEvents txtToBank As System.Windows.Forms.TextBox
	Public WithEvents txtProfilename As System.Windows.Forms.TextBox
	Public WithEvents txtAccountsPath As System.Windows.Forms.TextBox
	Public WithEvents cmdFileOpenAccount As System.Windows.Forms.Button
	Public WithEvents cmdAdvanced As System.Windows.Forms.Button
	Public WithEvents cmdFileOpenOut As System.Windows.Forms.Button
	Public WithEvents chkBackupOut As System.Windows.Forms.CheckBox
	Public WithEvents chkBackupIn As System.Windows.Forms.CheckBox
	Public WithEvents cmdFileOpenIn As System.Windows.Forms.Button
	Public WithEvents cmdEMail As System.Windows.Forms.Button
	Public WithEvents cmdFileOpenBackup As System.Windows.Forms.Button
	Public WithEvents txtCompanyNo As System.Windows.Forms.TextBox
	Public WithEvents txtDelDays As System.Windows.Forms.TextBox
	Public WithEvents chkBackup As System.Windows.Forms.CheckBox
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdOK As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents lblDivision As System.Windows.Forms.Label
	Public WithEvents lblFileAccount As System.Windows.Forms.Label
    Public WithEvents lblProfilename As System.Windows.Forms.Label
	Public WithEvents lblToBank As System.Windows.Forms.Label
    Public WithEvents lblFromInternal As System.Windows.Forms.Label
	Public WithEvents lblInFormats As System.Windows.Forms.Label
    Public WithEvents lblCompanyNo As System.Windows.Forms.Label
	Public WithEvents lblDelDays As System.Windows.Forms.Label
	Public WithEvents lblBackupPath As System.Windows.Forms.Label
	Public WithEvents lblCompany As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDnB_TBIW_Send))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtDivision = New System.Windows.Forms.TextBox
        Me.cmdMappingFile = New System.Windows.Forms.Button
        Me.cmdReport = New System.Windows.Forms.Button
        Me.chkPrint = New System.Windows.Forms.CheckBox
        Me.cmbFormat = New System.Windows.Forms.ComboBox
        Me.txtCompany = New System.Windows.Forms.TextBox
        Me.txtBackupPath = New System.Windows.Forms.TextBox
        Me.txtFromInternal = New System.Windows.Forms.TextBox
        Me.txtToBank = New System.Windows.Forms.TextBox
        Me.txtProfilename = New System.Windows.Forms.TextBox
        Me.txtAccountsPath = New System.Windows.Forms.TextBox
        Me.cmdFileOpenAccount = New System.Windows.Forms.Button
        Me.cmdAdvanced = New System.Windows.Forms.Button
        Me.cmdFileOpenOut = New System.Windows.Forms.Button
        Me.chkBackupOut = New System.Windows.Forms.CheckBox
        Me.chkBackupIn = New System.Windows.Forms.CheckBox
        Me.cmdFileOpenIn = New System.Windows.Forms.Button
        Me.cmdEMail = New System.Windows.Forms.Button
        Me.cmdFileOpenBackup = New System.Windows.Forms.Button
        Me.txtCompanyNo = New System.Windows.Forms.TextBox
        Me.txtDelDays = New System.Windows.Forms.TextBox
        Me.chkBackup = New System.Windows.Forms.CheckBox
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdOK = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.lblDivision = New System.Windows.Forms.Label
        Me.lblFileAccount = New System.Windows.Forms.Label
        Me.lblProfilename = New System.Windows.Forms.Label
        Me.lblToBank = New System.Windows.Forms.Label
        Me.lblFromInternal = New System.Windows.Forms.Label
        Me.lblInFormats = New System.Windows.Forms.Label
        Me.lblCompanyNo = New System.Windows.Forms.Label
        Me.lblDelDays = New System.Windows.Forms.Label
        Me.lblBackupPath = New System.Windows.Forms.Label
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.lblLine2 = New System.Windows.Forms.Label
        Me.lblLine3 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtDivision
        '
        Me.txtDivision.AcceptsReturn = True
        Me.txtDivision.BackColor = System.Drawing.SystemColors.Window
        Me.txtDivision.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDivision.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDivision.Location = New System.Drawing.Point(469, 78)
        Me.txtDivision.MaxLength = 11
        Me.txtDivision.Name = "txtDivision"
        Me.txtDivision.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDivision.Size = New System.Drawing.Size(107, 20)
        Me.txtDivision.TabIndex = 2
        '
        'cmdMappingFile
        '
        Me.cmdMappingFile.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMappingFile.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdMappingFile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdMappingFile.Location = New System.Drawing.Point(219, 383)
        Me.cmdMappingFile.Name = "cmdMappingFile"
        Me.cmdMappingFile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdMappingFile.Size = New System.Drawing.Size(104, 21)
        Me.cmdMappingFile.TabIndex = 36
        Me.cmdMappingFile.TabStop = False
        Me.cmdMappingFile.Text = "55046 &Mappingfile"
        Me.cmdMappingFile.UseVisualStyleBackColor = False
        '
        'cmdReport
        '
        Me.cmdReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdReport.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdReport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdReport.Location = New System.Drawing.Point(9, 383)
        Me.cmdReport.Name = "cmdReport"
        Me.cmdReport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdReport.Size = New System.Drawing.Size(104, 21)
        Me.cmdReport.TabIndex = 35
        Me.cmdReport.TabStop = False
        Me.cmdReport.Text = "60305 - Reports"
        Me.cmdReport.UseVisualStyleBackColor = False
        '
        'chkPrint
        '
        Me.chkPrint.BackColor = System.Drawing.SystemColors.Control
        Me.chkPrint.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkPrint.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkPrint.Location = New System.Drawing.Point(490, 271)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkPrint.Size = New System.Drawing.Size(123, 17)
        Me.chkPrint.TabIndex = 34
        Me.chkPrint.Text = "60271 - Send til skriver"
        Me.chkPrint.UseVisualStyleBackColor = False
        Me.chkPrint.Visible = False
        '
        'cmbFormat
        '
        Me.cmbFormat.BackColor = System.Drawing.SystemColors.Window
        Me.cmbFormat.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFormat.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbFormat.Location = New System.Drawing.Point(310, 157)
        Me.cmbFormat.Name = "cmbFormat"
        Me.cmbFormat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbFormat.Size = New System.Drawing.Size(267, 21)
        Me.cmbFormat.TabIndex = 6
        '
        'txtCompany
        '
        Me.txtCompany.AcceptsReturn = True
        Me.txtCompany.BackColor = System.Drawing.SystemColors.Window
        Me.txtCompany.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCompany.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCompany.Location = New System.Drawing.Point(310, 53)
        Me.txtCompany.MaxLength = 35
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCompany.Size = New System.Drawing.Size(267, 20)
        Me.txtCompany.TabIndex = 0
        '
        'txtBackupPath
        '
        Me.txtBackupPath.AcceptsReturn = True
        Me.txtBackupPath.BackColor = System.Drawing.SystemColors.Window
        Me.txtBackupPath.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBackupPath.Enabled = False
        Me.txtBackupPath.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBackupPath.Location = New System.Drawing.Point(310, 125)
        Me.txtBackupPath.MaxLength = 150
        Me.txtBackupPath.Name = "txtBackupPath"
        Me.txtBackupPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBackupPath.Size = New System.Drawing.Size(267, 20)
        Me.txtBackupPath.TabIndex = 5
        '
        'txtFromInternal
        '
        Me.txtFromInternal.AcceptsReturn = True
        Me.txtFromInternal.BackColor = System.Drawing.SystemColors.Window
        Me.txtFromInternal.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFromInternal.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFromInternal.Location = New System.Drawing.Point(310, 189)
        Me.txtFromInternal.MaxLength = 255
        Me.txtFromInternal.Name = "txtFromInternal"
        Me.txtFromInternal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFromInternal.Size = New System.Drawing.Size(267, 20)
        Me.txtFromInternal.TabIndex = 7
        '
        'txtToBank
        '
        Me.txtToBank.AcceptsReturn = True
        Me.txtToBank.BackColor = System.Drawing.SystemColors.Window
        Me.txtToBank.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtToBank.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtToBank.Location = New System.Drawing.Point(310, 240)
        Me.txtToBank.MaxLength = 255
        Me.txtToBank.Name = "txtToBank"
        Me.txtToBank.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtToBank.Size = New System.Drawing.Size(267, 20)
        Me.txtToBank.TabIndex = 9
        '
        'txtProfilename
        '
        Me.txtProfilename.AcceptsReturn = True
        Me.txtProfilename.BackColor = System.Drawing.SystemColors.Window
        Me.txtProfilename.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtProfilename.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtProfilename.Location = New System.Drawing.Point(310, 340)
        Me.txtProfilename.MaxLength = 49
        Me.txtProfilename.Name = "txtProfilename"
        Me.txtProfilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtProfilename.Size = New System.Drawing.Size(267, 20)
        Me.txtProfilename.TabIndex = 13
        '
        'txtAccountsPath
        '
        Me.txtAccountsPath.AcceptsReturn = True
        Me.txtAccountsPath.BackColor = System.Drawing.SystemColors.Window
        Me.txtAccountsPath.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAccountsPath.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAccountsPath.Location = New System.Drawing.Point(310, 300)
        Me.txtAccountsPath.MaxLength = 255
        Me.txtAccountsPath.Name = "txtAccountsPath"
        Me.txtAccountsPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAccountsPath.Size = New System.Drawing.Size(267, 20)
        Me.txtAccountsPath.TabIndex = 12
        '
        'cmdFileOpenAccount
        '
        Me.cmdFileOpenAccount.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpenAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpenAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpenAccount.Image = CType(resources.GetObject("cmdFileOpenAccount.Image"), System.Drawing.Image)
        Me.cmdFileOpenAccount.Location = New System.Drawing.Point(582, 298)
        Me.cmdFileOpenAccount.Name = "cmdFileOpenAccount"
        Me.cmdFileOpenAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpenAccount.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpenAccount.TabIndex = 32
        Me.cmdFileOpenAccount.TabStop = False
        Me.cmdFileOpenAccount.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpenAccount.UseVisualStyleBackColor = False
        '
        'cmdAdvanced
        '
        Me.cmdAdvanced.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAdvanced.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAdvanced.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAdvanced.Location = New System.Drawing.Point(114, 383)
        Me.cmdAdvanced.Name = "cmdAdvanced"
        Me.cmdAdvanced.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAdvanced.Size = New System.Drawing.Size(104, 21)
        Me.cmdAdvanced.TabIndex = 29
        Me.cmdAdvanced.TabStop = False
        Me.cmdAdvanced.Text = "55008 &Avansert oppsett"
        Me.cmdAdvanced.UseVisualStyleBackColor = False
        '
        'cmdFileOpenOut
        '
        Me.cmdFileOpenOut.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpenOut.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpenOut.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpenOut.Image = CType(resources.GetObject("cmdFileOpenOut.Image"), System.Drawing.Image)
        Me.cmdFileOpenOut.Location = New System.Drawing.Point(584, 237)
        Me.cmdFileOpenOut.Name = "cmdFileOpenOut"
        Me.cmdFileOpenOut.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpenOut.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpenOut.TabIndex = 28
        Me.cmdFileOpenOut.TabStop = False
        Me.cmdFileOpenOut.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpenOut.UseVisualStyleBackColor = False
        '
        'chkBackupOut
        '
        Me.chkBackupOut.BackColor = System.Drawing.SystemColors.Control
        Me.chkBackupOut.Checked = True
        Me.chkBackupOut.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBackupOut.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkBackupOut.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkBackupOut.Location = New System.Drawing.Point(310, 271)
        Me.chkBackupOut.Name = "chkBackupOut"
        Me.chkBackupOut.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkBackupOut.Size = New System.Drawing.Size(176, 17)
        Me.chkBackupOut.TabIndex = 11
        Me.chkBackupOut.Text = "60066 - Legg i backupkatalog"
        Me.chkBackupOut.UseVisualStyleBackColor = False
        '
        'chkBackupIn
        '
        Me.chkBackupIn.BackColor = System.Drawing.SystemColors.Control
        Me.chkBackupIn.Checked = True
        Me.chkBackupIn.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBackupIn.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkBackupIn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkBackupIn.Location = New System.Drawing.Point(310, 212)
        Me.chkBackupIn.Name = "chkBackupIn"
        Me.chkBackupIn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkBackupIn.Size = New System.Drawing.Size(281, 18)
        Me.chkBackupIn.TabIndex = 8
        Me.chkBackupIn.Text = "60060 - Legg i backupkatalogen etter import"
        Me.chkBackupIn.UseVisualStyleBackColor = False
        '
        'cmdFileOpenIn
        '
        Me.cmdFileOpenIn.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpenIn.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpenIn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpenIn.Image = CType(resources.GetObject("cmdFileOpenIn.Image"), System.Drawing.Image)
        Me.cmdFileOpenIn.Location = New System.Drawing.Point(582, 188)
        Me.cmdFileOpenIn.Name = "cmdFileOpenIn"
        Me.cmdFileOpenIn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpenIn.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpenIn.TabIndex = 25
        Me.cmdFileOpenIn.TabStop = False
        Me.cmdFileOpenIn.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpenIn.UseVisualStyleBackColor = False
        '
        'cmdEMail
        '
        Me.cmdEMail.BackColor = System.Drawing.SystemColors.Control
        Me.cmdEMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdEMail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdEMail.Location = New System.Drawing.Point(517, 98)
        Me.cmdEMail.Name = "cmdEMail"
        Me.cmdEMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdEMail.Size = New System.Drawing.Size(88, 21)
        Me.cmdEMail.TabIndex = 23
        Me.cmdEMail.TabStop = False
        Me.cmdEMail.Text = "60032 &EMail"
        Me.cmdEMail.UseVisualStyleBackColor = False
        '
        'cmdFileOpenBackup
        '
        Me.cmdFileOpenBackup.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpenBackup.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpenBackup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpenBackup.Image = CType(resources.GetObject("cmdFileOpenBackup.Image"), System.Drawing.Image)
        Me.cmdFileOpenBackup.Location = New System.Drawing.Point(582, 122)
        Me.cmdFileOpenBackup.Name = "cmdFileOpenBackup"
        Me.cmdFileOpenBackup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpenBackup.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpenBackup.TabIndex = 22
        Me.cmdFileOpenBackup.TabStop = False
        Me.cmdFileOpenBackup.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpenBackup.UseVisualStyleBackColor = False
        '
        'txtCompanyNo
        '
        Me.txtCompanyNo.AcceptsReturn = True
        Me.txtCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtCompanyNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCompanyNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCompanyNo.Location = New System.Drawing.Point(310, 78)
        Me.txtCompanyNo.MaxLength = 50
        Me.txtCompanyNo.Name = "txtCompanyNo"
        Me.txtCompanyNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCompanyNo.Size = New System.Drawing.Size(81, 20)
        Me.txtCompanyNo.TabIndex = 1
        '
        'txtDelDays
        '
        Me.txtDelDays.AcceptsReturn = True
        Me.txtDelDays.BackColor = System.Drawing.SystemColors.Window
        Me.txtDelDays.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDelDays.Enabled = False
        Me.txtDelDays.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDelDays.Location = New System.Drawing.Point(469, 100)
        Me.txtDelDays.MaxLength = 0
        Me.txtDelDays.Name = "txtDelDays"
        Me.txtDelDays.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDelDays.Size = New System.Drawing.Size(31, 20)
        Me.txtDelDays.TabIndex = 4
        '
        'chkBackup
        '
        Me.chkBackup.BackColor = System.Drawing.SystemColors.Control
        Me.chkBackup.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkBackup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkBackup.Location = New System.Drawing.Point(199, 101)
        Me.chkBackup.Name = "chkBackup"
        Me.chkBackup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkBackup.Size = New System.Drawing.Size(97, 17)
        Me.chkBackup.TabIndex = 3
        Me.chkBackup.Text = "60045 - Backup"
        Me.chkBackup.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(458, 383)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 15
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdOK
        '
        Me.CmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.CmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdOK.Location = New System.Drawing.Point(532, 383)
        Me.CmdOK.Name = "CmdOK"
        Me.CmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdOK.Size = New System.Drawing.Size(73, 21)
        Me.CmdOK.TabIndex = 14
        Me.CmdOK.Text = "&OK"
        Me.CmdOK.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(384, 383)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 16
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "55001 - &Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        Me.CmdHelp.Visible = False
        '
        'lblDivision
        '
        Me.lblDivision.BackColor = System.Drawing.SystemColors.Control
        Me.lblDivision.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDivision.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDivision.Location = New System.Drawing.Point(393, 80)
        Me.lblDivision.Name = "lblDivision"
        Me.lblDivision.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDivision.Size = New System.Drawing.Size(72, 20)
        Me.lblDivision.TabIndex = 37
        Me.lblDivision.Text = "60244-Divisjon"
        '
        'lblFileAccount
        '
        Me.lblFileAccount.BackColor = System.Drawing.SystemColors.Control
        Me.lblFileAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFileAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFileAccount.Location = New System.Drawing.Point(199, 304)
        Me.lblFileAccount.Name = "lblFileAccount"
        Me.lblFileAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFileAccount.Size = New System.Drawing.Size(97, 30)
        Me.lblFileAccount.TabIndex = 31
        Me.lblFileAccount.Text = "60296 - Sti for kontofil fra TBIW"
        '
        'lblProfilename
        '
        Me.lblProfilename.BackColor = System.Drawing.SystemColors.Control
        Me.lblProfilename.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProfilename.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProfilename.Location = New System.Drawing.Point(199, 342)
        Me.lblProfilename.Name = "lblProfilename"
        Me.lblProfilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProfilename.Size = New System.Drawing.Size(111, 19)
        Me.lblProfilename.TabIndex = 30
        Me.lblProfilename.Text = "60078 - Profilens navn"
        '
        'lblToBank
        '
        Me.lblToBank.BackColor = System.Drawing.SystemColors.Control
        Me.lblToBank.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblToBank.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblToBank.Location = New System.Drawing.Point(199, 240)
        Me.lblToBank.Name = "lblToBank"
        Me.lblToBank.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblToBank.Size = New System.Drawing.Size(110, 15)
        Me.lblToBank.TabIndex = 27
        Me.lblToBank.Text = "60065 - Fil til bank"
        '
        'lblFromInternal
        '
        Me.lblFromInternal.BackColor = System.Drawing.SystemColors.Control
        Me.lblFromInternal.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFromInternal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFromInternal.Location = New System.Drawing.Point(199, 186)
        Me.lblFromInternal.Name = "lblFromInternal"
        Me.lblFromInternal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFromInternal.Size = New System.Drawing.Size(103, 47)
        Me.lblFromInternal.TabIndex = 26
        Me.lblFromInternal.Text = "60056 - Filer fra internt system"
        '
        'lblInFormats
        '
        Me.lblInFormats.BackColor = System.Drawing.SystemColors.Control
        Me.lblInFormats.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblInFormats.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblInFormats.Location = New System.Drawing.Point(199, 156)
        Me.lblInFormats.Name = "lblInFormats"
        Me.lblInFormats.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblInFormats.Size = New System.Drawing.Size(105, 35)
        Me.lblInFormats.TabIndex = 24
        Me.lblInFormats.Text = "60053 - Velg format for filer fra regnskap"
        '
        'lblCompanyNo
        '
        Me.lblCompanyNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblCompanyNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompanyNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompanyNo.Location = New System.Drawing.Point(199, 78)
        Me.lblCompanyNo.Name = "lblCompanyNo"
        Me.lblCompanyNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompanyNo.Size = New System.Drawing.Size(104, 19)
        Me.lblCompanyNo.TabIndex = 21
        Me.lblCompanyNo.Text = "60043 - Foretaksnr"
        '
        'lblDelDays
        '
        Me.lblDelDays.BackColor = System.Drawing.SystemColors.Control
        Me.lblDelDays.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDelDays.Enabled = False
        Me.lblDelDays.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDelDays.Location = New System.Drawing.Point(310, 101)
        Me.lblDelDays.Name = "lblDelDays"
        Me.lblDelDays.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDelDays.Size = New System.Drawing.Size(145, 17)
        Me.lblDelDays.TabIndex = 20
        Me.lblDelDays.Text = "60046 - Slett etter antall dager:"
        '
        'lblBackupPath
        '
        Me.lblBackupPath.BackColor = System.Drawing.SystemColors.Control
        Me.lblBackupPath.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBackupPath.Enabled = False
        Me.lblBackupPath.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBackupPath.Location = New System.Drawing.Point(199, 125)
        Me.lblBackupPath.Name = "lblBackupPath"
        Me.lblBackupPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBackupPath.Size = New System.Drawing.Size(97, 19)
        Me.lblBackupPath.TabIndex = 19
        Me.lblBackupPath.Text = "60047 - Backupkat."
        '
        'lblCompany
        '
        Me.lblCompany.BackColor = System.Drawing.SystemColors.Control
        Me.lblCompany.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompany.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompany.Location = New System.Drawing.Point(199, 56)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompany.Size = New System.Drawing.Size(104, 17)
        Me.lblCompany.TabIndex = 18
        Me.lblCompany.Text = "60039 - Firma:"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(200, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(393, 17)
        Me.lblHeading.TabIndex = 17
        Me.lblHeading.Text = "60049 - Sendeprofil"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(182, 150)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(425, 1)
        Me.lblLine1.TabIndex = 57
        Me.lblLine1.Text = "Label1"
        '
        'lblLine2
        '
        Me.lblLine2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine2.Location = New System.Drawing.Point(182, 233)
        Me.lblLine2.Name = "lblLine2"
        Me.lblLine2.Size = New System.Drawing.Size(425, 1)
        Me.lblLine2.TabIndex = 58
        Me.lblLine2.Text = "Label1"
        '
        'lblLine3
        '
        Me.lblLine3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine3.Location = New System.Drawing.Point(10, 370)
        Me.lblLine3.Name = "lblLine3"
        Me.lblLine3.Size = New System.Drawing.Size(595, 1)
        Me.lblLine3.TabIndex = 59
        Me.lblLine3.Text = "Label1"
        '
        'frmDnB_TBIW_Send
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(622, 413)
        Me.Controls.Add(Me.lblLine3)
        Me.Controls.Add(Me.lblLine2)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtDivision)
        Me.Controls.Add(Me.cmdMappingFile)
        Me.Controls.Add(Me.cmdReport)
        Me.Controls.Add(Me.chkPrint)
        Me.Controls.Add(Me.cmbFormat)
        Me.Controls.Add(Me.txtCompany)
        Me.Controls.Add(Me.txtBackupPath)
        Me.Controls.Add(Me.txtFromInternal)
        Me.Controls.Add(Me.txtToBank)
        Me.Controls.Add(Me.txtProfilename)
        Me.Controls.Add(Me.txtAccountsPath)
        Me.Controls.Add(Me.cmdFileOpenAccount)
        Me.Controls.Add(Me.cmdAdvanced)
        Me.Controls.Add(Me.cmdFileOpenOut)
        Me.Controls.Add(Me.chkBackupOut)
        Me.Controls.Add(Me.chkBackupIn)
        Me.Controls.Add(Me.cmdFileOpenIn)
        Me.Controls.Add(Me.cmdEMail)
        Me.Controls.Add(Me.cmdFileOpenBackup)
        Me.Controls.Add(Me.txtCompanyNo)
        Me.Controls.Add(Me.txtDelDays)
        Me.Controls.Add(Me.chkBackup)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdOK)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.lblDivision)
        Me.Controls.Add(Me.lblFileAccount)
        Me.Controls.Add(Me.lblProfilename)
        Me.Controls.Add(Me.lblToBank)
        Me.Controls.Add(Me.lblFromInternal)
        Me.Controls.Add(Me.lblInFormats)
        Me.Controls.Add(Me.lblCompanyNo)
        Me.Controls.Add(Me.lblDelDays)
        Me.Controls.Add(Me.lblBackupPath)
        Me.Controls.Add(Me.lblCompany)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDnB_TBIW_Send"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "60049 - Sendeprofil"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents lblLine2 As System.Windows.Forms.Label
    Friend WithEvents lblLine3 As System.Windows.Forms.Label
#End Region
End Class
