<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmAdvancedReturn
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents chkKeepClients As System.Windows.Forms.CheckBox
	Public WithEvents chkExtractFromKID As System.Windows.Forms.CheckBox
	Public WithEvents chkNegativeAmount As System.Windows.Forms.CheckBox
	Public WithEvents cmdFileOpen As System.Windows.Forms.Button
	Public WithEvents txtBackupPath As System.Windows.Forms.TextBox
	Public WithEvents chkSplitFBO As System.Windows.Forms.CheckBox
	Public WithEvents txtDelDays As System.Windows.Forms.TextBox
	Public WithEvents txtAdditionalNo As System.Windows.Forms.TextBox
	Public WithEvents chkCompanyNoOnClient As System.Windows.Forms.CheckBox
	Public WithEvents txtCompanyNo As System.Windows.Forms.TextBox
	Public WithEvents chkRejectWrong As System.Windows.Forms.CheckBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents chkReturnMass As System.Windows.Forms.CheckBox
    Public WithEvents lblBackupPath As System.Windows.Forms.Label
	Public WithEvents lblDelDays As System.Windows.Forms.Label
	Public WithEvents lblAdditionalNo As System.Windows.Forms.Label
	Public WithEvents lblCompanyNo As System.Windows.Forms.Label
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdvancedReturn))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkKeepClients = New System.Windows.Forms.CheckBox
        Me.chkExtractFromKID = New System.Windows.Forms.CheckBox
        Me.chkNegativeAmount = New System.Windows.Forms.CheckBox
        Me.cmdFileOpen = New System.Windows.Forms.Button
        Me.txtBackupPath = New System.Windows.Forms.TextBox
        Me.chkSplitFBO = New System.Windows.Forms.CheckBox
        Me.txtDelDays = New System.Windows.Forms.TextBox
        Me.txtAdditionalNo = New System.Windows.Forms.TextBox
        Me.chkCompanyNoOnClient = New System.Windows.Forms.CheckBox
        Me.txtCompanyNo = New System.Windows.Forms.TextBox
        Me.chkRejectWrong = New System.Windows.Forms.CheckBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.chkReturnMass = New System.Windows.Forms.CheckBox
        Me.lblBackupPath = New System.Windows.Forms.Label
        Me.lblDelDays = New System.Windows.Forms.Label
        Me.lblAdditionalNo = New System.Windows.Forms.Label
        Me.lblCompanyNo = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblLine1 = New System.Windows.Forms.Label
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkKeepClients
        '
        Me.chkKeepClients.BackColor = System.Drawing.SystemColors.Control
        Me.chkKeepClients.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkKeepClients.Enabled = False
        Me.chkKeepClients.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkKeepClients.Location = New System.Drawing.Point(176, 164)
        Me.chkKeepClients.Name = "chkKeepClients"
        Me.chkKeepClients.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkKeepClients.Size = New System.Drawing.Size(425, 17)
        Me.chkKeepClients.TabIndex = 18
        Me.chkKeepClients.Text = "Behold klientinndeling i filen"
        Me.chkKeepClients.UseVisualStyleBackColor = False
        '
        'chkExtractFromKID
        '
        Me.chkExtractFromKID.BackColor = System.Drawing.SystemColors.Control
        Me.chkExtractFromKID.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkExtractFromKID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkExtractFromKID.Location = New System.Drawing.Point(176, 143)
        Me.chkExtractFromKID.Name = "chkExtractFromKID"
        Me.chkExtractFromKID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkExtractFromKID.Size = New System.Drawing.Size(425, 17)
        Me.chkExtractFromKID.TabIndex = 17
        Me.chkExtractFromKID.Text = "Hent ut informasjon fra KID"
        Me.chkExtractFromKID.UseVisualStyleBackColor = False
        '
        'chkNegativeAmount
        '
        Me.chkNegativeAmount.BackColor = System.Drawing.SystemColors.Control
        Me.chkNegativeAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkNegativeAmount.Enabled = False
        Me.chkNegativeAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkNegativeAmount.Location = New System.Drawing.Point(177, 123)
        Me.chkNegativeAmount.Name = "chkNegativeAmount"
        Me.chkNegativeAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkNegativeAmount.Size = New System.Drawing.Size(425, 17)
        Me.chkNegativeAmount.TabIndex = 16
        Me.chkNegativeAmount.Text = "Avvis transaksjoner hvis de har negativt bel�p"
        Me.chkNegativeAmount.UseVisualStyleBackColor = False
        '
        'cmdFileOpen
        '
        Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
        Me.cmdFileOpen.Location = New System.Drawing.Point(547, 278)
        Me.cmdFileOpen.Name = "cmdFileOpen"
        Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen.TabIndex = 14
        Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen.UseVisualStyleBackColor = False
        '
        'txtBackupPath
        '
        Me.txtBackupPath.AcceptsReturn = True
        Me.txtBackupPath.BackColor = System.Drawing.SystemColors.Window
        Me.txtBackupPath.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBackupPath.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBackupPath.Location = New System.Drawing.Point(281, 278)
        Me.txtBackupPath.MaxLength = 150
        Me.txtBackupPath.Name = "txtBackupPath"
        Me.txtBackupPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBackupPath.Size = New System.Drawing.Size(253, 20)
        Me.txtBackupPath.TabIndex = 5
        '
        'chkSplitFBO
        '
        Me.chkSplitFBO.BackColor = System.Drawing.SystemColors.Control
        Me.chkSplitFBO.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkSplitFBO.Enabled = False
        Me.chkSplitFBO.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkSplitFBO.Location = New System.Drawing.Point(152, 54)
        Me.chkSplitFBO.Name = "chkSplitFBO"
        Me.chkSplitFBO.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkSplitFBO.Size = New System.Drawing.Size(483, 17)
        Me.chkSplitFBO.TabIndex = 13
        Me.chkSplitFBO.Text = "Splitt ut avtalegiroposter i egen fil (kun for OCR-formatet). Bytter plass med ma" & _
            "ssetr i kode."
        Me.chkSplitFBO.UseVisualStyleBackColor = False
        Me.chkSplitFBO.Visible = False
        '
        'txtDelDays
        '
        Me.txtDelDays.AcceptsReturn = True
        Me.txtDelDays.BackColor = System.Drawing.SystemColors.Window
        Me.txtDelDays.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDelDays.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDelDays.Location = New System.Drawing.Point(323, 187)
        Me.txtDelDays.MaxLength = 3
        Me.txtDelDays.Name = "txtDelDays"
        Me.txtDelDays.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDelDays.Size = New System.Drawing.Size(31, 20)
        Me.txtDelDays.TabIndex = 2
        '
        'txtAdditionalNo
        '
        Me.txtAdditionalNo.AcceptsReturn = True
        Me.txtAdditionalNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtAdditionalNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAdditionalNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAdditionalNo.Location = New System.Drawing.Point(281, 251)
        Me.txtAdditionalNo.MaxLength = 0
        Me.txtAdditionalNo.Name = "txtAdditionalNo"
        Me.txtAdditionalNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAdditionalNo.Size = New System.Drawing.Size(145, 20)
        Me.txtAdditionalNo.TabIndex = 4
        '
        'chkCompanyNoOnClient
        '
        Me.chkCompanyNoOnClient.BackColor = System.Drawing.SystemColors.Control
        Me.chkCompanyNoOnClient.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkCompanyNoOnClient.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkCompanyNoOnClient.Location = New System.Drawing.Point(446, 229)
        Me.chkCompanyNoOnClient.Name = "chkCompanyNoOnClient"
        Me.chkCompanyNoOnClient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkCompanyNoOnClient.Size = New System.Drawing.Size(175, 17)
        Me.chkCompanyNoOnClient.TabIndex = 10
        Me.chkCompanyNoOnClient.TabStop = False
        Me.chkCompanyNoOnClient.Text = "Foretaksnr. pr klient"
        Me.chkCompanyNoOnClient.UseVisualStyleBackColor = False
        Me.chkCompanyNoOnClient.Visible = False
        '
        'txtCompanyNo
        '
        Me.txtCompanyNo.AcceptsReturn = True
        Me.txtCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtCompanyNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCompanyNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCompanyNo.Location = New System.Drawing.Point(281, 225)
        Me.txtCompanyNo.MaxLength = 0
        Me.txtCompanyNo.Name = "txtCompanyNo"
        Me.txtCompanyNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCompanyNo.Size = New System.Drawing.Size(145, 20)
        Me.txtCompanyNo.TabIndex = 3
        '
        'chkRejectWrong
        '
        Me.chkRejectWrong.BackColor = System.Drawing.SystemColors.Control
        Me.chkRejectWrong.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkRejectWrong.Enabled = False
        Me.chkRejectWrong.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRejectWrong.Location = New System.Drawing.Point(177, 82)
        Me.chkRejectWrong.Name = "chkRejectWrong"
        Me.chkRejectWrong.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkRejectWrong.Size = New System.Drawing.Size(425, 17)
        Me.chkRejectWrong.TabIndex = 0
        Me.chkRejectWrong.Text = "Avvis feilmarkerte transaksjoner?"
        Me.chkRejectWrong.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(431, 326)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 8
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(280, 326)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 6
        Me.CmdCancel.Text = "&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(144, 326)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 7
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "&Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'chkReturnMass
        '
        Me.chkReturnMass.BackColor = System.Drawing.SystemColors.Control
        Me.chkReturnMass.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkReturnMass.Enabled = False
        Me.chkReturnMass.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkReturnMass.Location = New System.Drawing.Point(177, 103)
        Me.chkReturnMass.Name = "chkReturnMass"
        Me.chkReturnMass.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkReturnMass.Size = New System.Drawing.Size(425, 17)
        Me.chkReturnMass.TabIndex = 1
        Me.chkReturnMass.Text = "Ta med massetranser (l�nn, pensjon) i returfiler"
        Me.chkReturnMass.UseVisualStyleBackColor = False
        '
        'lblBackupPath
        '
        Me.lblBackupPath.BackColor = System.Drawing.SystemColors.Control
        Me.lblBackupPath.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBackupPath.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBackupPath.Location = New System.Drawing.Point(177, 279)
        Me.lblBackupPath.Name = "lblBackupPath"
        Me.lblBackupPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBackupPath.Size = New System.Drawing.Size(97, 19)
        Me.lblBackupPath.TabIndex = 15
        Me.lblBackupPath.Text = "Backupkatalog"
        '
        'lblDelDays
        '
        Me.lblDelDays.BackColor = System.Drawing.SystemColors.Control
        Me.lblDelDays.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDelDays.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDelDays.Location = New System.Drawing.Point(177, 189)
        Me.lblDelDays.Name = "lblDelDays"
        Me.lblDelDays.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDelDays.Size = New System.Drawing.Size(145, 17)
        Me.lblDelDays.TabIndex = 12
        Me.lblDelDays.Text = "Slett etter antall dager:"
        '
        'lblAdditionalNo
        '
        Me.lblAdditionalNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdditionalNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdditionalNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdditionalNo.Location = New System.Drawing.Point(177, 253)
        Me.lblAdditionalNo.Name = "lblAdditionalNo"
        Me.lblAdditionalNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdditionalNo.Size = New System.Drawing.Size(93, 19)
        Me.lblAdditionalNo.TabIndex = 11
        Me.lblAdditionalNo.Text = "Kundeenhetsid"
        '
        'lblCompanyNo
        '
        Me.lblCompanyNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblCompanyNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompanyNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompanyNo.Location = New System.Drawing.Point(177, 225)
        Me.lblCompanyNo.Name = "lblCompanyNo"
        Me.lblCompanyNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompanyNo.Size = New System.Drawing.Size(93, 19)
        Me.lblCompanyNo.TabIndex = 9
        Me.lblCompanyNo.Text = "Foretaksnummer"
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(0, 0)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(32, 56)
        Me._Image1_0.TabIndex = 19
        Me._Image1_0.TabStop = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 311)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(600, 1)
        Me.lblLine1.TabIndex = 29
        Me.lblLine1.Text = "Label1"
        '
        'frmAdvancedReturn
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(620, 353)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.chkKeepClients)
        Me.Controls.Add(Me.chkExtractFromKID)
        Me.Controls.Add(Me.chkNegativeAmount)
        Me.Controls.Add(Me.cmdFileOpen)
        Me.Controls.Add(Me.txtBackupPath)
        Me.Controls.Add(Me.chkSplitFBO)
        Me.Controls.Add(Me.txtDelDays)
        Me.Controls.Add(Me.txtAdditionalNo)
        Me.Controls.Add(Me.chkCompanyNoOnClient)
        Me.Controls.Add(Me.txtCompanyNo)
        Me.Controls.Add(Me.chkRejectWrong)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.chkReturnMass)
        Me.Controls.Add(Me.lblBackupPath)
        Me.Controls.Add(Me.lblDelDays)
        Me.Controls.Add(Me.lblAdditionalNo)
        Me.Controls.Add(Me.lblCompanyNo)
        Me.Controls.Add(Me._Image1_0)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmAdvancedReturn"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Avansert oppsett - returfiler"
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
