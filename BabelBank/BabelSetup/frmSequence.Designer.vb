﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSequence
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gridClients = New System.Windows.Forms.DataGridView
        Me.optSequencePrFile = New System.Windows.Forms.RadioButton
        Me.txtSequenceNumber = New System.Windows.Forms.TextBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.optSequencePrClient = New System.Windows.Forms.RadioButton
        Me.optSequenceFromFile = New System.Windows.Forms.RadioButton
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.lstProfiles = New System.Windows.Forms.ListBox
        Me.lblFileSequence = New System.Windows.Forms.Label
        Me.lblSelectProfile = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        CType(Me.gridClients, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gridClients
        '
        Me.gridClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridClients.Location = New System.Drawing.Point(240, 159)
        Me.gridClients.Name = "gridClients"
        Me.gridClients.Size = New System.Drawing.Size(328, 199)
        Me.gridClients.TabIndex = 21
        '
        'optSequencePrFile
        '
        Me.optSequencePrFile.BackColor = System.Drawing.SystemColors.Window
        Me.optSequencePrFile.Cursor = System.Windows.Forms.Cursors.Default
        Me.optSequencePrFile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optSequencePrFile.Location = New System.Drawing.Point(32, 85)
        Me.optSequencePrFile.Name = "optSequencePrFile"
        Me.optSequencePrFile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optSequencePrFile.Size = New System.Drawing.Size(217, 17)
        Me.optSequencePrFile.TabIndex = 15
        Me.optSequencePrFile.TabStop = True
        Me.optSequencePrFile.Text = "Seqencenumbers total pr. file"
        Me.optSequencePrFile.UseVisualStyleBackColor = False
        '
        'txtSequenceNumber
        '
        Me.txtSequenceNumber.AcceptsReturn = True
        Me.txtSequenceNumber.BackColor = System.Drawing.SystemColors.Window
        Me.txtSequenceNumber.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSequenceNumber.Enabled = False
        Me.txtSequenceNumber.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSequenceNumber.Location = New System.Drawing.Point(448, 67)
        Me.txtSequenceNumber.MaxLength = 4
        Me.txtSequenceNumber.Name = "txtSequenceNumber"
        Me.txtSequenceNumber.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSequenceNumber.Size = New System.Drawing.Size(33, 20)
        Me.txtSequenceNumber.TabIndex = 13
        Me.txtSequenceNumber.Text = "0"
        '
        'optSequencePrClient
        '
        Me.optSequencePrClient.BackColor = System.Drawing.SystemColors.Window
        Me.optSequencePrClient.Cursor = System.Windows.Forms.Cursors.Default
        Me.optSequencePrClient.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optSequencePrClient.Location = New System.Drawing.Point(32, 105)
        Me.optSequencePrClient.Name = "optSequencePrClient"
        Me.optSequencePrClient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optSequencePrClient.Size = New System.Drawing.Size(217, 17)
        Me.optSequencePrClient.TabIndex = 16
        Me.optSequencePrClient.TabStop = True
        Me.optSequencePrClient.Text = "Sequencenumbers pr. client"
        Me.optSequencePrClient.UseVisualStyleBackColor = False
        '
        'optSequenceFromFile
        '
        Me.optSequenceFromFile.BackColor = System.Drawing.SystemColors.Window
        Me.optSequenceFromFile.Cursor = System.Windows.Forms.Cursors.Default
        Me.optSequenceFromFile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optSequenceFromFile.Location = New System.Drawing.Point(32, 65)
        Me.optSequenceFromFile.Name = "optSequenceFromFile"
        Me.optSequenceFromFile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optSequenceFromFile.Size = New System.Drawing.Size(217, 17)
        Me.optSequenceFromFile.TabIndex = 14
        Me.optSequenceFromFile.TabStop = True
        Me.optSequenceFromFile.Text = "Use sequencenumbers from imported file"
        Me.optSequenceFromFile.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.Enabled = False
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(372, 383)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(89, 25)
        Me.cmdOK.TabIndex = 17
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(467, 383)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(89, 25)
        Me.cmdCancel.TabIndex = 18
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'lstProfiles
        '
        Me.lstProfiles.BackColor = System.Drawing.SystemColors.Window
        Me.lstProfiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstProfiles.Enabled = False
        Me.lstProfiles.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstProfiles.Location = New System.Drawing.Point(32, 159)
        Me.lstProfiles.Name = "lstProfiles"
        Me.lstProfiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstProfiles.Size = New System.Drawing.Size(185, 199)
        Me.lstProfiles.TabIndex = 12
        '
        'lblFileSequence
        '
        Me.lblFileSequence.BackColor = System.Drawing.SystemColors.Control
        Me.lblFileSequence.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFileSequence.Enabled = False
        Me.lblFileSequence.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFileSequence.Location = New System.Drawing.Point(272, 67)
        Me.lblFileSequence.Name = "lblFileSequence"
        Me.lblFileSequence.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFileSequence.Size = New System.Drawing.Size(173, 17)
        Me.lblFileSequence.TabIndex = 20
        Me.lblFileSequence.Text = "Previous sequencenumber "
        '
        'lblSelectProfile
        '
        Me.lblSelectProfile.BackColor = System.Drawing.SystemColors.Control
        Me.lblSelectProfile.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSelectProfile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSelectProfile.Location = New System.Drawing.Point(32, 135)
        Me.lblSelectProfile.Name = "lblSelectProfile"
        Me.lblSelectProfile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSelectProfile.Size = New System.Drawing.Size(185, 17)
        Me.lblSelectProfile.TabIndex = 19
        Me.lblSelectProfile.Text = "Select profile"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(19, 371)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(545, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmSequence
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(582, 414)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.gridClients)
        Me.Controls.Add(Me.optSequencePrFile)
        Me.Controls.Add(Me.txtSequenceNumber)
        Me.Controls.Add(Me.optSequencePrClient)
        Me.Controls.Add(Me.optSequenceFromFile)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.lstProfiles)
        Me.Controls.Add(Me.lblFileSequence)
        Me.Controls.Add(Me.lblSelectProfile)
        Me.Name = "frmSequence"
        Me.Text = "frmSequenceNew"
        CType(Me.gridClients, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gridClients As System.Windows.Forms.DataGridView
    Public WithEvents optSequencePrFile As System.Windows.Forms.RadioButton
    Public WithEvents txtSequenceNumber As System.Windows.Forms.TextBox
    Public WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents optSequencePrClient As System.Windows.Forms.RadioButton
    Public WithEvents optSequenceFromFile As System.Windows.Forms.RadioButton
    Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents cmdCancel As System.Windows.Forms.Button
    Public WithEvents lstProfiles As System.Windows.Forms.ListBox
    Public WithEvents lblFileSequence As System.Windows.Forms.Label
    Public WithEvents lblSelectProfile As System.Windows.Forms.Label
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
End Class
