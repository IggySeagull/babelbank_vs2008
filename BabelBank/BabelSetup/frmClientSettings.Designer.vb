<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmClientSettings
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdUse As System.Windows.Forms.Button
	Public WithEvents cmdAutoBookings As System.Windows.Forms.Button
	Public WithEvents cmdShowMatchDiff As System.Windows.Forms.Button
	Public WithEvents chkCashDiscountAccount As System.Windows.Forms.CheckBox
	Public WithEvents cmdNotifications As System.Windows.Forms.Button
	Public WithEvents lstNotifications As System.Windows.Forms.ListBox
	Public WithEvents chkUnmatchedAccount As System.Windows.Forms.CheckBox
	Public WithEvents cmdShowPatterns As System.Windows.Forms.Button
	Public WithEvents cmdShowDiffAccounts As System.Windows.Forms.Button
	Public WithEvents cmbdbProfiles As System.Windows.Forms.ComboBox
	Public WithEvents txtPatternGroup As System.Windows.Forms.TextBox
	Public WithEvents cmdRemovePatternGroup As System.Windows.Forms.Button
	Public WithEvents cmdAddEasyPatternGroup As System.Windows.Forms.Button
	Public WithEvents cmdAdddbProfile As System.Windows.Forms.Button
	Public WithEvents txtClientNo As System.Windows.Forms.TextBox
	Public WithEvents cmbDiffType As System.Windows.Forms.ComboBox
	Public WithEvents txtDiffText As System.Windows.Forms.TextBox
	Public WithEvents txtDiffPattern As System.Windows.Forms.TextBox
	Public WithEvents txtDiffAccountName As System.Windows.Forms.TextBox
	Public WithEvents cmdAddEasyDiffAccount As System.Windows.Forms.Button
	Public WithEvents cmdRemoveDiffAccount As System.Windows.Forms.Button
	Public WithEvents lstDiffAccounts As System.Windows.Forms.ListBox
	Public WithEvents txtAccountNo As System.Windows.Forms.TextBox
	Public WithEvents cmdAddEasyAccount As System.Windows.Forms.Button
	Public WithEvents lstAccounts As System.Windows.Forms.ListBox
	Public WithEvents cmdAddEasyClient As System.Windows.Forms.Button
	Public WithEvents cmdRemoveClient As System.Windows.Forms.Button
	Public WithEvents cmdRemoveAccount As System.Windows.Forms.Button
	Public WithEvents cmdShowClient As System.Windows.Forms.Button
	Public WithEvents lstPatternGroup As System.Windows.Forms.ListBox
	Public WithEvents cmdShowAccounts As System.Windows.Forms.Button
	Public WithEvents cmdSelectProfiles As System.Windows.Forms.Button
	Public WithEvents lstProfiles As System.Windows.Forms.ListBox
	Public WithEvents txtClientName As System.Windows.Forms.TextBox
	Public WithEvents lstClients As System.Windows.Forms.ListBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents _lblNotifications_1 As System.Windows.Forms.Label
	Public WithEvents lblProfiles As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lblDiffText As System.Windows.Forms.Label
	Public WithEvents lblDiffType As System.Windows.Forms.Label
	Public WithEvents lblDiffPattern As System.Windows.Forms.Label
	Public WithEvents lblDiffName As System.Windows.Forms.Label
	Public WithEvents lblPatternGroupName As System.Windows.Forms.Label
	Public WithEvents lblAccountNo As System.Windows.Forms.Label
	Public WithEvents lblClientName As System.Windows.Forms.Label
	Public WithEvents lblClientNo As System.Windows.Forms.Label
	Public WithEvents lblPatternGroup As System.Windows.Forms.Label
	Public WithEvents lblDBProfile As System.Windows.Forms.Label
    Public WithEvents lblDiffAccounts As System.Windows.Forms.Label
	Public WithEvents lblAccounts As System.Windows.Forms.Label
    Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	Public WithEvents lblNotifications As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdUse = New System.Windows.Forms.Button
        Me.cmdAutoBookings = New System.Windows.Forms.Button
        Me.cmdShowMatchDiff = New System.Windows.Forms.Button
        Me.chkCashDiscountAccount = New System.Windows.Forms.CheckBox
        Me.cmdNotifications = New System.Windows.Forms.Button
        Me.lstNotifications = New System.Windows.Forms.ListBox
        Me.chkUnmatchedAccount = New System.Windows.Forms.CheckBox
        Me.cmdShowPatterns = New System.Windows.Forms.Button
        Me.cmdShowDiffAccounts = New System.Windows.Forms.Button
        Me.cmbdbProfiles = New System.Windows.Forms.ComboBox
        Me.txtPatternGroup = New System.Windows.Forms.TextBox
        Me.cmdRemovePatternGroup = New System.Windows.Forms.Button
        Me.cmdAddEasyPatternGroup = New System.Windows.Forms.Button
        Me.cmdAdddbProfile = New System.Windows.Forms.Button
        Me.txtClientNo = New System.Windows.Forms.TextBox
        Me.cmbDiffType = New System.Windows.Forms.ComboBox
        Me.txtDiffText = New System.Windows.Forms.TextBox
        Me.txtDiffPattern = New System.Windows.Forms.TextBox
        Me.txtDiffAccountName = New System.Windows.Forms.TextBox
        Me.cmdAddEasyDiffAccount = New System.Windows.Forms.Button
        Me.cmdRemoveDiffAccount = New System.Windows.Forms.Button
        Me.lstDiffAccounts = New System.Windows.Forms.ListBox
        Me.txtAccountNo = New System.Windows.Forms.TextBox
        Me.cmdAddEasyAccount = New System.Windows.Forms.Button
        Me.lstAccounts = New System.Windows.Forms.ListBox
        Me.cmdAddEasyClient = New System.Windows.Forms.Button
        Me.cmdRemoveClient = New System.Windows.Forms.Button
        Me.cmdRemoveAccount = New System.Windows.Forms.Button
        Me.cmdShowClient = New System.Windows.Forms.Button
        Me.lstPatternGroup = New System.Windows.Forms.ListBox
        Me.cmdShowAccounts = New System.Windows.Forms.Button
        Me.cmdSelectProfiles = New System.Windows.Forms.Button
        Me.lstProfiles = New System.Windows.Forms.ListBox
        Me.txtClientName = New System.Windows.Forms.TextBox
        Me.lstClients = New System.Windows.Forms.ListBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me._lblNotifications_1 = New System.Windows.Forms.Label
        Me.lblProfiles = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblDiffText = New System.Windows.Forms.Label
        Me.lblDiffType = New System.Windows.Forms.Label
        Me.lblDiffPattern = New System.Windows.Forms.Label
        Me.lblDiffName = New System.Windows.Forms.Label
        Me.lblPatternGroupName = New System.Windows.Forms.Label
        Me.lblAccountNo = New System.Windows.Forms.Label
        Me.lblClientName = New System.Windows.Forms.Label
        Me.lblClientNo = New System.Windows.Forms.Label
        Me.lblPatternGroup = New System.Windows.Forms.Label
        Me.lblDBProfile = New System.Windows.Forms.Label
        Me.lblDiffAccounts = New System.Windows.Forms.Label
        Me.lblAccounts = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblNotifications = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(Me.components)
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblNotifications, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdUse
        '
        Me.cmdUse.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUse.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdUse.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdUse.Location = New System.Drawing.Point(532, 544)
        Me.cmdUse.Name = "cmdUse"
        Me.cmdUse.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdUse.Size = New System.Drawing.Size(73, 21)
        Me.cmdUse.TabIndex = 52
        Me.cmdUse.Text = "55043-&Bruk/Lagre"
        Me.cmdUse.UseVisualStyleBackColor = False
        '
        'cmdAutoBookings
        '
        Me.cmdAutoBookings.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAutoBookings.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAutoBookings.Enabled = False
        Me.cmdAutoBookings.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAutoBookings.Location = New System.Drawing.Point(24, 379)
        Me.cmdAutoBookings.Name = "cmdAutoBookings"
        Me.cmdAutoBookings.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAutoBookings.Size = New System.Drawing.Size(100, 21)
        Me.cmdAutoBookings.TabIndex = 51
        Me.cmdAutoBookings.TabStop = False
        Me.cmdAutoBookings.Text = "60436 - AutoBooking"
        Me.cmdAutoBookings.UseVisualStyleBackColor = False
        '
        'cmdShowMatchDiff
        '
        Me.cmdShowMatchDiff.BackColor = System.Drawing.SystemColors.Control
        Me.cmdShowMatchDiff.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdShowMatchDiff.Enabled = False
        Me.cmdShowMatchDiff.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdShowMatchDiff.Location = New System.Drawing.Point(610, 282)
        Me.cmdShowMatchDiff.Name = "cmdShowMatchDiff"
        Me.cmdShowMatchDiff.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdShowMatchDiff.Size = New System.Drawing.Size(73, 21)
        Me.cmdShowMatchDiff.TabIndex = 50
        Me.cmdShowMatchDiff.TabStop = False
        Me.cmdShowMatchDiff.Text = "55022-Bel�psavvik"
        Me.cmdShowMatchDiff.UseVisualStyleBackColor = False
        '
        'chkCashDiscountAccount
        '
        Me.chkCashDiscountAccount.BackColor = System.Drawing.SystemColors.Control
        Me.chkCashDiscountAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkCashDiscountAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkCashDiscountAccount.Location = New System.Drawing.Point(609, 372)
        Me.chkCashDiscountAccount.Name = "chkCashDiscountAccount"
        Me.chkCashDiscountAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkCashDiscountAccount.Size = New System.Drawing.Size(95, 35)
        Me.chkCashDiscountAccount.TabIndex = 49
        Me.chkCashDiscountAccount.Text = "60394 - Cashdiscount account"
        Me.chkCashDiscountAccount.UseVisualStyleBackColor = False
        '
        'cmdNotifications
        '
        Me.cmdNotifications.BackColor = System.Drawing.SystemColors.Control
        Me.cmdNotifications.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdNotifications.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNotifications.Location = New System.Drawing.Point(610, 433)
        Me.cmdNotifications.Name = "cmdNotifications"
        Me.cmdNotifications.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdNotifications.Size = New System.Drawing.Size(73, 21)
        Me.cmdNotifications.TabIndex = 47
        Me.cmdNotifications.TabStop = False
        Me.cmdNotifications.Text = "55019-Select"
        Me.cmdNotifications.UseVisualStyleBackColor = False
        '
        'lstNotifications
        '
        Me.lstNotifications.BackColor = System.Drawing.SystemColors.Window
        Me.lstNotifications.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstNotifications.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstNotifications.Location = New System.Drawing.Point(368, 432)
        Me.lstNotifications.Name = "lstNotifications"
        Me.lstNotifications.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstNotifications.Size = New System.Drawing.Size(234, 69)
        Me.lstNotifications.TabIndex = 15
        '
        'chkUnmatchedAccount
        '
        Me.chkUnmatchedAccount.BackColor = System.Drawing.SystemColors.Control
        Me.chkUnmatchedAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUnmatchedAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUnmatchedAccount.Location = New System.Drawing.Point(609, 334)
        Me.chkUnmatchedAccount.Name = "chkUnmatchedAccount"
        Me.chkUnmatchedAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUnmatchedAccount.Size = New System.Drawing.Size(95, 35)
        Me.chkUnmatchedAccount.TabIndex = 12
        Me.chkUnmatchedAccount.Text = "60382-Unmatched account"
        Me.chkUnmatchedAccount.UseVisualStyleBackColor = False
        '
        'cmdShowPatterns
        '
        Me.cmdShowPatterns.BackColor = System.Drawing.SystemColors.Control
        Me.cmdShowPatterns.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdShowPatterns.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdShowPatterns.Location = New System.Drawing.Point(260, 430)
        Me.cmdShowPatterns.Name = "cmdShowPatterns"
        Me.cmdShowPatterns.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdShowPatterns.Size = New System.Drawing.Size(73, 21)
        Me.cmdShowPatterns.TabIndex = 46
        Me.cmdShowPatterns.TabStop = False
        Me.cmdShowPatterns.Text = "55016-Show"
        Me.cmdShowPatterns.UseVisualStyleBackColor = False
        '
        'cmdShowDiffAccounts
        '
        Me.cmdShowDiffAccounts.BackColor = System.Drawing.SystemColors.Control
        Me.cmdShowDiffAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdShowDiffAccounts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdShowDiffAccounts.Location = New System.Drawing.Point(610, 229)
        Me.cmdShowDiffAccounts.Name = "cmdShowDiffAccounts"
        Me.cmdShowDiffAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdShowDiffAccounts.Size = New System.Drawing.Size(73, 21)
        Me.cmdShowDiffAccounts.TabIndex = 45
        Me.cmdShowDiffAccounts.TabStop = False
        Me.cmdShowDiffAccounts.Text = "55016-Show"
        Me.cmdShowDiffAccounts.UseVisualStyleBackColor = False
        '
        'cmbdbProfiles
        '
        Me.cmbdbProfiles.BackColor = System.Drawing.SystemColors.Window
        Me.cmbdbProfiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbdbProfiles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbdbProfiles.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbdbProfiles.Location = New System.Drawing.Point(369, 180)
        Me.cmbdbProfiles.Name = "cmbdbProfiles"
        Me.cmbdbProfiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbdbProfiles.Size = New System.Drawing.Size(233, 21)
        Me.cmbdbProfiles.TabIndex = 4
        '
        'txtPatternGroup
        '
        Me.txtPatternGroup.AcceptsReturn = True
        Me.txtPatternGroup.BackColor = System.Drawing.SystemColors.Window
        Me.txtPatternGroup.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPatternGroup.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPatternGroup.Location = New System.Drawing.Point(136, 507)
        Me.txtPatternGroup.MaxLength = 0
        Me.txtPatternGroup.Name = "txtPatternGroup"
        Me.txtPatternGroup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPatternGroup.Size = New System.Drawing.Size(114, 20)
        Me.txtPatternGroup.TabIndex = 14
        '
        'cmdRemovePatternGroup
        '
        Me.cmdRemovePatternGroup.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemovePatternGroup.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRemovePatternGroup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRemovePatternGroup.Location = New System.Drawing.Point(261, 456)
        Me.cmdRemovePatternGroup.Name = "cmdRemovePatternGroup"
        Me.cmdRemovePatternGroup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRemovePatternGroup.Size = New System.Drawing.Size(73, 21)
        Me.cmdRemovePatternGroup.TabIndex = 33
        Me.cmdRemovePatternGroup.TabStop = False
        Me.cmdRemovePatternGroup.Text = "55015-Remove"
        Me.cmdRemovePatternGroup.UseVisualStyleBackColor = False
        '
        'cmdAddEasyPatternGroup
        '
        Me.cmdAddEasyPatternGroup.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddEasyPatternGroup.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAddEasyPatternGroup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAddEasyPatternGroup.Location = New System.Drawing.Point(261, 505)
        Me.cmdAddEasyPatternGroup.Name = "cmdAddEasyPatternGroup"
        Me.cmdAddEasyPatternGroup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAddEasyPatternGroup.Size = New System.Drawing.Size(73, 21)
        Me.cmdAddEasyPatternGroup.TabIndex = 32
        Me.cmdAddEasyPatternGroup.TabStop = False
        Me.cmdAddEasyPatternGroup.Text = "55018-Add"
        Me.cmdAddEasyPatternGroup.UseVisualStyleBackColor = False
        '
        'cmdAdddbProfile
        '
        Me.cmdAdddbProfile.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAdddbProfile.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAdddbProfile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAdddbProfile.Location = New System.Drawing.Point(610, 179)
        Me.cmdAdddbProfile.Name = "cmdAdddbProfile"
        Me.cmdAdddbProfile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAdddbProfile.Size = New System.Drawing.Size(73, 21)
        Me.cmdAdddbProfile.TabIndex = 31
        Me.cmdAdddbProfile.TabStop = False
        Me.cmdAdddbProfile.Text = "55017-Add (...)"
        Me.cmdAdddbProfile.UseVisualStyleBackColor = False
        '
        'txtClientNo
        '
        Me.txtClientNo.AcceptsReturn = True
        Me.txtClientNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtClientNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtClientNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtClientNo.Location = New System.Drawing.Point(113, 136)
        Me.txtClientNo.MaxLength = 0
        Me.txtClientNo.Name = "txtClientNo"
        Me.txtClientNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtClientNo.Size = New System.Drawing.Size(141, 20)
        Me.txtClientNo.TabIndex = 1
        '
        'cmbDiffType
        '
        Me.cmbDiffType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbDiffType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbDiffType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDiffType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbDiffType.Location = New System.Drawing.Point(473, 359)
        Me.cmbDiffType.Name = "cmbDiffType"
        Me.cmbDiffType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbDiffType.Size = New System.Drawing.Size(129, 21)
        Me.cmbDiffType.TabIndex = 10
        '
        'txtDiffText
        '
        Me.txtDiffText.AcceptsReturn = True
        Me.txtDiffText.BackColor = System.Drawing.SystemColors.Window
        Me.txtDiffText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDiffText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDiffText.Location = New System.Drawing.Point(473, 383)
        Me.txtDiffText.MaxLength = 0
        Me.txtDiffText.Name = "txtDiffText"
        Me.txtDiffText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDiffText.Size = New System.Drawing.Size(129, 20)
        Me.txtDiffText.TabIndex = 11
        '
        'txtDiffPattern
        '
        Me.txtDiffPattern.AcceptsReturn = True
        Me.txtDiffPattern.BackColor = System.Drawing.SystemColors.Window
        Me.txtDiffPattern.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDiffPattern.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDiffPattern.Location = New System.Drawing.Point(473, 335)
        Me.txtDiffPattern.MaxLength = 0
        Me.txtDiffPattern.Name = "txtDiffPattern"
        Me.txtDiffPattern.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDiffPattern.Size = New System.Drawing.Size(129, 20)
        Me.txtDiffPattern.TabIndex = 9
        '
        'txtDiffAccountName
        '
        Me.txtDiffAccountName.AcceptsReturn = True
        Me.txtDiffAccountName.BackColor = System.Drawing.SystemColors.Window
        Me.txtDiffAccountName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDiffAccountName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDiffAccountName.Location = New System.Drawing.Point(473, 311)
        Me.txtDiffAccountName.MaxLength = 0
        Me.txtDiffAccountName.Name = "txtDiffAccountName"
        Me.txtDiffAccountName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDiffAccountName.Size = New System.Drawing.Size(129, 20)
        Me.txtDiffAccountName.TabIndex = 8
        '
        'cmdAddEasyDiffAccount
        '
        Me.cmdAddEasyDiffAccount.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddEasyDiffAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAddEasyDiffAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAddEasyDiffAccount.Location = New System.Drawing.Point(610, 308)
        Me.cmdAddEasyDiffAccount.Name = "cmdAddEasyDiffAccount"
        Me.cmdAddEasyDiffAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAddEasyDiffAccount.Size = New System.Drawing.Size(73, 21)
        Me.cmdAddEasyDiffAccount.TabIndex = 29
        Me.cmdAddEasyDiffAccount.TabStop = False
        Me.cmdAddEasyDiffAccount.Text = "55018-Add"
        Me.cmdAddEasyDiffAccount.UseVisualStyleBackColor = False
        '
        'cmdRemoveDiffAccount
        '
        Me.cmdRemoveDiffAccount.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemoveDiffAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRemoveDiffAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRemoveDiffAccount.Location = New System.Drawing.Point(610, 255)
        Me.cmdRemoveDiffAccount.Name = "cmdRemoveDiffAccount"
        Me.cmdRemoveDiffAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRemoveDiffAccount.Size = New System.Drawing.Size(73, 21)
        Me.cmdRemoveDiffAccount.TabIndex = 28
        Me.cmdRemoveDiffAccount.TabStop = False
        Me.cmdRemoveDiffAccount.Text = "55015-Remove"
        Me.cmdRemoveDiffAccount.UseVisualStyleBackColor = False
        '
        'lstDiffAccounts
        '
        Me.lstDiffAccounts.BackColor = System.Drawing.SystemColors.Window
        Me.lstDiffAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstDiffAccounts.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstDiffAccounts.Location = New System.Drawing.Point(368, 235)
        Me.lstDiffAccounts.Name = "lstDiffAccounts"
        Me.lstDiffAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstDiffAccounts.Size = New System.Drawing.Size(234, 69)
        Me.lstDiffAccounts.TabIndex = 7
        '
        'txtAccountNo
        '
        Me.txtAccountNo.AcceptsReturn = True
        Me.txtAccountNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtAccountNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAccountNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAccountNo.Location = New System.Drawing.Point(114, 311)
        Me.txtAccountNo.MaxLength = 0
        Me.txtAccountNo.Name = "txtAccountNo"
        Me.txtAccountNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAccountNo.Size = New System.Drawing.Size(140, 20)
        Me.txtAccountNo.TabIndex = 6
        '
        'cmdAddEasyAccount
        '
        Me.cmdAddEasyAccount.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddEasyAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAddEasyAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAddEasyAccount.Location = New System.Drawing.Point(261, 310)
        Me.cmdAddEasyAccount.Name = "cmdAddEasyAccount"
        Me.cmdAddEasyAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAddEasyAccount.Size = New System.Drawing.Size(73, 21)
        Me.cmdAddEasyAccount.TabIndex = 27
        Me.cmdAddEasyAccount.TabStop = False
        Me.cmdAddEasyAccount.Text = "55018-Add"
        Me.cmdAddEasyAccount.UseVisualStyleBackColor = False
        '
        'lstAccounts
        '
        Me.lstAccounts.BackColor = System.Drawing.SystemColors.Window
        Me.lstAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstAccounts.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstAccounts.Location = New System.Drawing.Point(24, 232)
        Me.lstAccounts.Name = "lstAccounts"
        Me.lstAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstAccounts.Size = New System.Drawing.Size(229, 69)
        Me.lstAccounts.TabIndex = 5
        '
        'cmdAddEasyClient
        '
        Me.cmdAddEasyClient.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddEasyClient.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAddEasyClient.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAddEasyClient.Location = New System.Drawing.Point(261, 135)
        Me.cmdAddEasyClient.Name = "cmdAddEasyClient"
        Me.cmdAddEasyClient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAddEasyClient.Size = New System.Drawing.Size(73, 21)
        Me.cmdAddEasyClient.TabIndex = 26
        Me.cmdAddEasyClient.TabStop = False
        Me.cmdAddEasyClient.Text = "55018 - Add"
        Me.cmdAddEasyClient.UseVisualStyleBackColor = False
        '
        'cmdRemoveClient
        '
        Me.cmdRemoveClient.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemoveClient.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRemoveClient.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRemoveClient.Location = New System.Drawing.Point(261, 85)
        Me.cmdRemoveClient.Name = "cmdRemoveClient"
        Me.cmdRemoveClient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRemoveClient.Size = New System.Drawing.Size(73, 21)
        Me.cmdRemoveClient.TabIndex = 25
        Me.cmdRemoveClient.TabStop = False
        Me.cmdRemoveClient.Text = "55015-Remove"
        Me.cmdRemoveClient.UseVisualStyleBackColor = False
        '
        'cmdRemoveAccount
        '
        Me.cmdRemoveAccount.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemoveAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRemoveAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRemoveAccount.Location = New System.Drawing.Point(261, 256)
        Me.cmdRemoveAccount.Name = "cmdRemoveAccount"
        Me.cmdRemoveAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRemoveAccount.Size = New System.Drawing.Size(73, 21)
        Me.cmdRemoveAccount.TabIndex = 24
        Me.cmdRemoveAccount.TabStop = False
        Me.cmdRemoveAccount.Text = "55015-Remove"
        Me.cmdRemoveAccount.UseVisualStyleBackColor = False
        '
        'cmdShowClient
        '
        Me.cmdShowClient.BackColor = System.Drawing.SystemColors.Control
        Me.cmdShowClient.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdShowClient.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdShowClient.Location = New System.Drawing.Point(262, 60)
        Me.cmdShowClient.Name = "cmdShowClient"
        Me.cmdShowClient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdShowClient.Size = New System.Drawing.Size(73, 21)
        Me.cmdShowClient.TabIndex = 23
        Me.cmdShowClient.TabStop = False
        Me.cmdShowClient.Text = "55016-Show"
        Me.cmdShowClient.UseVisualStyleBackColor = False
        '
        'lstPatternGroup
        '
        Me.lstPatternGroup.BackColor = System.Drawing.SystemColors.Window
        Me.lstPatternGroup.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstPatternGroup.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstPatternGroup.Location = New System.Drawing.Point(21, 433)
        Me.lstPatternGroup.Name = "lstPatternGroup"
        Me.lstPatternGroup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstPatternGroup.Size = New System.Drawing.Size(229, 69)
        Me.lstPatternGroup.TabIndex = 13
        '
        'cmdShowAccounts
        '
        Me.cmdShowAccounts.BackColor = System.Drawing.SystemColors.Control
        Me.cmdShowAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdShowAccounts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdShowAccounts.Location = New System.Drawing.Point(261, 229)
        Me.cmdShowAccounts.Name = "cmdShowAccounts"
        Me.cmdShowAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdShowAccounts.Size = New System.Drawing.Size(73, 21)
        Me.cmdShowAccounts.TabIndex = 21
        Me.cmdShowAccounts.TabStop = False
        Me.cmdShowAccounts.Text = "55016-Show"
        Me.cmdShowAccounts.UseVisualStyleBackColor = False
        '
        'cmdSelectProfiles
        '
        Me.cmdSelectProfiles.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSelectProfiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSelectProfiles.Enabled = False
        Me.cmdSelectProfiles.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSelectProfiles.Location = New System.Drawing.Point(610, 60)
        Me.cmdSelectProfiles.Name = "cmdSelectProfiles"
        Me.cmdSelectProfiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSelectProfiles.Size = New System.Drawing.Size(73, 21)
        Me.cmdSelectProfiles.TabIndex = 19
        Me.cmdSelectProfiles.TabStop = False
        Me.cmdSelectProfiles.Text = "55019-Select"
        Me.cmdSelectProfiles.UseVisualStyleBackColor = False
        '
        'lstProfiles
        '
        Me.lstProfiles.BackColor = System.Drawing.SystemColors.Window
        Me.lstProfiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstProfiles.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstProfiles.Location = New System.Drawing.Point(366, 61)
        Me.lstProfiles.Name = "lstProfiles"
        Me.lstProfiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstProfiles.Size = New System.Drawing.Size(234, 95)
        Me.lstProfiles.TabIndex = 3
        '
        'txtClientName
        '
        Me.txtClientName.AcceptsReturn = True
        Me.txtClientName.BackColor = System.Drawing.SystemColors.Window
        Me.txtClientName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtClientName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtClientName.Location = New System.Drawing.Point(113, 160)
        Me.txtClientName.MaxLength = 0
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtClientName.Size = New System.Drawing.Size(142, 20)
        Me.txtClientName.TabIndex = 2
        '
        'lstClients
        '
        Me.lstClients.BackColor = System.Drawing.SystemColors.Window
        Me.lstClients.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstClients.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstClients.Location = New System.Drawing.Point(21, 60)
        Me.lstClients.Name = "lstClients"
        Me.lstClients.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstClients.Size = New System.Drawing.Size(234, 69)
        Me.lstClients.TabIndex = 0
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(610, 544)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 18
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(454, 544)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 17
        Me.CmdCancel.TabStop = False
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(376, 544)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 16
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "55001 - &Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        '_lblNotifications_1
        '
        Me._lblNotifications_1.BackColor = System.Drawing.SystemColors.Control
        Me._lblNotifications_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._lblNotifications_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNotifications.SetIndex(Me._lblNotifications_1, CType(1, Short))
        Me._lblNotifications_1.Location = New System.Drawing.Point(368, 414)
        Me._lblNotifications_1.Name = "_lblNotifications_1"
        Me._lblNotifications_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._lblNotifications_1.Size = New System.Drawing.Size(157, 14)
        Me._lblNotifications_1.TabIndex = 48
        Me._lblNotifications_1.Text = "60021 -Notifications"
        '
        'lblProfiles
        '
        Me.lblProfiles.BackColor = System.Drawing.SystemColors.Control
        Me.lblProfiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProfiles.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProfiles.Location = New System.Drawing.Point(366, 44)
        Me.lblProfiles.Name = "lblProfiles"
        Me.lblProfiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProfiles.Size = New System.Drawing.Size(188, 15)
        Me.lblProfiles.TabIndex = 44
        Me.lblProfiles.Text = "60001 - Profilename"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(24, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(172, 12)
        Me.Label1.TabIndex = 43
        Me.Label1.Text = "60016 - Clients"
        '
        'lblDiffText
        '
        Me.lblDiffText.BackColor = System.Drawing.SystemColors.Control
        Me.lblDiffText.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDiffText.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDiffText.Location = New System.Drawing.Point(368, 385)
        Me.lblDiffText.Name = "lblDiffText"
        Me.lblDiffText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDiffText.Size = New System.Drawing.Size(106, 19)
        Me.lblDiffText.TabIndex = 42
        Me.lblDiffText.Text = "60210 - Text"
        '
        'lblDiffType
        '
        Me.lblDiffType.BackColor = System.Drawing.SystemColors.Control
        Me.lblDiffType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDiffType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDiffType.Location = New System.Drawing.Point(368, 360)
        Me.lblDiffType.Name = "lblDiffType"
        Me.lblDiffType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDiffType.Size = New System.Drawing.Size(106, 19)
        Me.lblDiffType.TabIndex = 41
        Me.lblDiffType.Text = "60359-AccountType"
        '
        'lblDiffPattern
        '
        Me.lblDiffPattern.BackColor = System.Drawing.SystemColors.Control
        Me.lblDiffPattern.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDiffPattern.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDiffPattern.Location = New System.Drawing.Point(368, 336)
        Me.lblDiffPattern.Name = "lblDiffPattern"
        Me.lblDiffPattern.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDiffPattern.Size = New System.Drawing.Size(106, 19)
        Me.lblDiffPattern.TabIndex = 40
        Me.lblDiffPattern.Text = "60036-Account"
        '
        'lblDiffName
        '
        Me.lblDiffName.BackColor = System.Drawing.SystemColors.Control
        Me.lblDiffName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDiffName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDiffName.Location = New System.Drawing.Point(368, 312)
        Me.lblDiffName.Name = "lblDiffName"
        Me.lblDiffName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDiffName.Size = New System.Drawing.Size(106, 19)
        Me.lblDiffName.TabIndex = 39
        Me.lblDiffName.Text = "60206-Name"
        '
        'lblPatternGroupName
        '
        Me.lblPatternGroupName.AutoSize = True
        Me.lblPatternGroupName.BackColor = System.Drawing.SystemColors.Control
        Me.lblPatternGroupName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPatternGroupName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPatternGroupName.Location = New System.Drawing.Point(21, 510)
        Me.lblPatternGroupName.Name = "lblPatternGroupName"
        Me.lblPatternGroupName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPatternGroupName.Size = New System.Drawing.Size(125, 13)
        Me.lblPatternGroupName.TabIndex = 38
        Me.lblPatternGroupName.Text = "60361-Patterntype Name"
        '
        'lblAccountNo
        '
        Me.lblAccountNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblAccountNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAccountNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAccountNo.Location = New System.Drawing.Point(24, 313)
        Me.lblAccountNo.Name = "lblAccountNo"
        Me.lblAccountNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAccountNo.Size = New System.Drawing.Size(89, 17)
        Me.lblAccountNo.TabIndex = 37
        Me.lblAccountNo.Text = "60356-AccountNo"
        '
        'lblClientName
        '
        Me.lblClientName.BackColor = System.Drawing.SystemColors.Control
        Me.lblClientName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblClientName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblClientName.Location = New System.Drawing.Point(21, 164)
        Me.lblClientName.Name = "lblClientName"
        Me.lblClientName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblClientName.Size = New System.Drawing.Size(93, 15)
        Me.lblClientName.TabIndex = 36
        Me.lblClientName.Text = "60028-Clientname"
        '
        'lblClientNo
        '
        Me.lblClientNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblClientNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblClientNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblClientNo.Location = New System.Drawing.Point(22, 139)
        Me.lblClientNo.Name = "lblClientNo"
        Me.lblClientNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblClientNo.Size = New System.Drawing.Size(93, 17)
        Me.lblClientNo.TabIndex = 35
        Me.lblClientNo.Text = "60034 - ClientNo"
        '
        'lblPatternGroup
        '
        Me.lblPatternGroup.BackColor = System.Drawing.SystemColors.Control
        Me.lblPatternGroup.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPatternGroup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPatternGroup.Location = New System.Drawing.Point(24, 414)
        Me.lblPatternGroup.Name = "lblPatternGroup"
        Me.lblPatternGroup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPatternGroup.Size = New System.Drawing.Size(227, 15)
        Me.lblPatternGroup.TabIndex = 34
        Me.lblPatternGroup.Text = "60360 - Patterntype"
        '
        'lblDBProfile
        '
        Me.lblDBProfile.BackColor = System.Drawing.SystemColors.Control
        Me.lblDBProfile.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDBProfile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDBProfile.Location = New System.Drawing.Point(369, 163)
        Me.lblDBProfile.Name = "lblDBProfile"
        Me.lblDBProfile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDBProfile.Size = New System.Drawing.Size(196, 12)
        Me.lblDBProfile.TabIndex = 30
        Me.lblDBProfile.Text = "dbProfile"
        '
        'lblDiffAccounts
        '
        Me.lblDiffAccounts.BackColor = System.Drawing.SystemColors.Control
        Me.lblDiffAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDiffAccounts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDiffAccounts.Location = New System.Drawing.Point(368, 215)
        Me.lblDiffAccounts.Name = "lblDiffAccounts"
        Me.lblDiffAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDiffAccounts.Size = New System.Drawing.Size(173, 14)
        Me.lblDiffAccounts.TabIndex = 22
        Me.lblDiffAccounts.Text = "60358 - Deviation Accounts differences"
        '
        'lblAccounts
        '
        Me.lblAccounts.BackColor = System.Drawing.SystemColors.Control
        Me.lblAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAccounts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAccounts.Location = New System.Drawing.Point(23, 214)
        Me.lblAccounts.Name = "lblAccounts"
        Me.lblAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAccounts.Size = New System.Drawing.Size(173, 14)
        Me.lblAccounts.TabIndex = 20
        Me.lblAccounts.Text = "60012 - Accounts"
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(3, 72)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(11, 21)
        Me._Image1_0.TabIndex = 53
        Me._Image1_0.TabStop = False
        '
        'frmClientSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(705, 562)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdUse)
        Me.Controls.Add(Me.cmdAutoBookings)
        Me.Controls.Add(Me.cmdShowMatchDiff)
        Me.Controls.Add(Me.chkCashDiscountAccount)
        Me.Controls.Add(Me.cmdNotifications)
        Me.Controls.Add(Me.lstNotifications)
        Me.Controls.Add(Me.chkUnmatchedAccount)
        Me.Controls.Add(Me.cmdShowPatterns)
        Me.Controls.Add(Me.cmdShowDiffAccounts)
        Me.Controls.Add(Me.cmbdbProfiles)
        Me.Controls.Add(Me.txtPatternGroup)
        Me.Controls.Add(Me.cmdRemovePatternGroup)
        Me.Controls.Add(Me.cmdAddEasyPatternGroup)
        Me.Controls.Add(Me.cmdAdddbProfile)
        Me.Controls.Add(Me.txtClientNo)
        Me.Controls.Add(Me.cmbDiffType)
        Me.Controls.Add(Me.txtDiffText)
        Me.Controls.Add(Me.txtDiffPattern)
        Me.Controls.Add(Me.txtDiffAccountName)
        Me.Controls.Add(Me.cmdAddEasyDiffAccount)
        Me.Controls.Add(Me.cmdRemoveDiffAccount)
        Me.Controls.Add(Me.lstDiffAccounts)
        Me.Controls.Add(Me.txtAccountNo)
        Me.Controls.Add(Me.cmdAddEasyAccount)
        Me.Controls.Add(Me.lstAccounts)
        Me.Controls.Add(Me.cmdAddEasyClient)
        Me.Controls.Add(Me.cmdRemoveClient)
        Me.Controls.Add(Me.cmdRemoveAccount)
        Me.Controls.Add(Me.cmdShowClient)
        Me.Controls.Add(Me.lstPatternGroup)
        Me.Controls.Add(Me.cmdShowAccounts)
        Me.Controls.Add(Me.cmdSelectProfiles)
        Me.Controls.Add(Me.lstProfiles)
        Me.Controls.Add(Me.txtClientName)
        Me.Controls.Add(Me.lstClients)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me._lblNotifications_1)
        Me.Controls.Add(Me.lblProfiles)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblDiffText)
        Me.Controls.Add(Me.lblDiffType)
        Me.Controls.Add(Me.lblDiffPattern)
        Me.Controls.Add(Me.lblDiffName)
        Me.Controls.Add(Me.lblPatternGroupName)
        Me.Controls.Add(Me.lblAccountNo)
        Me.Controls.Add(Me.lblClientName)
        Me.Controls.Add(Me.lblClientNo)
        Me.Controls.Add(Me.lblPatternGroup)
        Me.Controls.Add(Me.lblDBProfile)
        Me.Controls.Add(Me.lblDiffAccounts)
        Me.Controls.Add(Me.lblAccounts)
        Me.Controls.Add(Me._Image1_0)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(372, 30)
        Me.Name = "frmClientSettings"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "60016 - Clients"
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblNotifications, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#End Region
End Class
