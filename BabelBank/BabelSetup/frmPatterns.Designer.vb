<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmPatterns
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdRemovePattern As System.Windows.Forms.Button
	Public WithEvents cmdAddEasyPattern As System.Windows.Forms.Button
	Public WithEvents txtPattern As System.Windows.Forms.TextBox
	Public WithEvents lstPatterns As System.Windows.Forms.CheckedListBox
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdOK As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents lblKIDExplaination As System.Windows.Forms.Label
	Public WithEvents lblDescription As System.Windows.Forms.Label
	Public WithEvents lblPattern As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdRemovePattern = New System.Windows.Forms.Button
        Me.cmdAddEasyPattern = New System.Windows.Forms.Button
        Me.txtPattern = New System.Windows.Forms.TextBox
        Me.lstPatterns = New System.Windows.Forms.CheckedListBox
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdOK = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.lblKIDExplaination = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblPattern = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdRemovePattern
        '
        Me.cmdRemovePattern.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemovePattern.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRemovePattern.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRemovePattern.Location = New System.Drawing.Point(479, 152)
        Me.cmdRemovePattern.Name = "cmdRemovePattern"
        Me.cmdRemovePattern.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRemovePattern.Size = New System.Drawing.Size(76, 21)
        Me.cmdRemovePattern.TabIndex = 2
        Me.cmdRemovePattern.TabStop = False
        Me.cmdRemovePattern.Text = "55015-Remove"
        Me.cmdRemovePattern.UseVisualStyleBackColor = False
        '
        'cmdAddEasyPattern
        '
        Me.cmdAddEasyPattern.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddEasyPattern.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAddEasyPattern.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAddEasyPattern.Location = New System.Drawing.Point(479, 263)
        Me.cmdAddEasyPattern.Name = "cmdAddEasyPattern"
        Me.cmdAddEasyPattern.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAddEasyPattern.Size = New System.Drawing.Size(76, 21)
        Me.cmdAddEasyPattern.TabIndex = 3
        Me.cmdAddEasyPattern.TabStop = False
        Me.cmdAddEasyPattern.Text = "55018-Add"
        Me.cmdAddEasyPattern.UseVisualStyleBackColor = False
        '
        'txtPattern
        '
        Me.txtPattern.AcceptsReturn = True
        Me.txtPattern.BackColor = System.Drawing.SystemColors.Window
        Me.txtPattern.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPattern.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPattern.Location = New System.Drawing.Point(307, 264)
        Me.txtPattern.MaxLength = 0
        Me.txtPattern.Name = "txtPattern"
        Me.txtPattern.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPattern.Size = New System.Drawing.Size(149, 20)
        Me.txtPattern.TabIndex = 0
        '
        'lstPatterns
        '
        Me.lstPatterns.BackColor = System.Drawing.SystemColors.Window
        Me.lstPatterns.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstPatterns.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstPatterns.Location = New System.Drawing.Point(201, 63)
        Me.lstPatterns.Name = "lstPatterns"
        Me.lstPatterns.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstPatterns.Size = New System.Drawing.Size(255, 109)
        Me.lstPatterns.TabIndex = 1
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(403, 343)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 5
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdOK
        '
        Me.CmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.CmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdOK.Location = New System.Drawing.Point(483, 343)
        Me.CmdOK.Name = "CmdOK"
        Me.CmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdOK.Size = New System.Drawing.Size(73, 21)
        Me.CmdOK.TabIndex = 6
        Me.CmdOK.Text = "&OK"
        Me.CmdOK.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(327, 343)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 4
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "55001 - &Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'lblKIDExplaination
        '
        Me.lblKIDExplaination.BackColor = System.Drawing.SystemColors.Control
        Me.lblKIDExplaination.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKIDExplaination.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblKIDExplaination.Location = New System.Drawing.Point(198, 192)
        Me.lblKIDExplaination.Name = "lblKIDExplaination"
        Me.lblKIDExplaination.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKIDExplaination.Size = New System.Drawing.Size(353, 65)
        Me.lblKIDExplaination.TabIndex = 10
        Me.lblKIDExplaination.Text = "60514 - Explain use of KID"
        '
        'lblDescription
        '
        Me.lblDescription.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescription.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescription.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescription.Location = New System.Drawing.Point(199, 291)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescription.Size = New System.Drawing.Size(337, 41)
        Me.lblDescription.TabIndex = 9
        Me.lblDescription.Text = "60151 - Fakturabeskrivelse: #=Numerisk tegn $=Alfanumerisk tegn   _=Blank  @=Ikke" & _
            " numerisk tegn   Andre tegn er ""ekte"""
        '
        'lblPattern
        '
        Me.lblPattern.BackColor = System.Drawing.SystemColors.Control
        Me.lblPattern.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPattern.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPattern.Location = New System.Drawing.Point(199, 267)
        Me.lblPattern.Name = "lblPattern"
        Me.lblPattern.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPattern.Size = New System.Drawing.Size(102, 18)
        Me.lblPattern.TabIndex = 8
        Me.lblPattern.Text = "60363 - Pattern"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(194, 18)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(323, 17)
        Me.lblHeading.TabIndex = 7
        Me.lblHeading.Text = "60362 - Patterns"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 336)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(545, 1)
        Me.lblLine1.TabIndex = 78
        Me.lblLine1.Text = "Label1"
        '
        'frmPatterns
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(567, 370)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdRemovePattern)
        Me.Controls.Add(Me.cmdAddEasyPattern)
        Me.Controls.Add(Me.txtPattern)
        Me.Controls.Add(Me.lstPatterns)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdOK)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.lblKIDExplaination)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.lblPattern)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPatterns"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "60362 - Patterns"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
