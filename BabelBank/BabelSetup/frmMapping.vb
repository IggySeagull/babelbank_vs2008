Option Strict Off
Option Explicit On
Friend Class frmMapping
    Inherits System.Windows.Forms.Form

    Dim bAnythingChangedInThisForm As Boolean
    Dim bStatus As Boolean
    Private Sub cmdMappingFile_Click()
        Me.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(Me.Top) + 3000)
        Me.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(Me.Left) + 500)
        VB6.ShowForm(Me, 1, Me)

    End Sub

    'UPGRADE_WARNING: Event cmbFileType.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub cmbFileType_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbFileType.SelectedIndexChanged
        If Me.cmbFileType.SelectedIndex = 0 Or Me.cmbFileType.SelectedIndex = 2 Then ' Delimited or Excel
            ' hide all length-textboxes
            Me.txtLen1.Visible = False
            Me.txtLen2.Visible = False
            Me.txtLen3.Visible = False
            Me.txtLen4.Visible = False
            Me.txtLen5.Visible = False
            Me.txtLen6.Visible = False
            Me.txtLen7.Visible = False
            Me.txtLen8.Visible = False
            Me.txtLen9.Visible = False
            Me.txtLen10.Visible = False
            Me.txtLen11.Visible = False
            Me.txtLen12.Visible = False
            Me.txtLen13.Visible = False
            Me.txtLen14.Visible = False
            Me.txtLen15.Visible = False
            If Me.cmbFileType.SelectedIndex = 0 Then
                ' Delimited
                Me.lblDelimiter.Visible = True
                Me.lblSurround.Visible = True
                Me.txtSurround.Visible = True
                Me.txtDelimiter.Visible = True
            Else
                ' Excel
                Me.lblDelimiter.Visible = False
                Me.lblSurround.Visible = False
                Me.txtSurround.Visible = False
                Me.txtDelimiter.Visible = False
            End If

        ElseIf Me.cmbFileType.SelectedIndex = 1 Then  ' Fixed
            ' show all length-textboxes
            Me.txtLen1.Visible = True
            Me.txtLen2.Visible = True
            Me.txtLen3.Visible = True
            Me.txtLen4.Visible = True
            Me.txtLen5.Visible = True
            Me.txtLen6.Visible = True
            Me.txtLen7.Visible = True
            Me.txtLen8.Visible = True
            Me.txtLen9.Visible = True
            Me.txtLen10.Visible = True
            Me.txtLen11.Visible = True
            Me.txtLen12.Visible = True
            Me.txtLen13.Visible = True
            Me.txtLen14.Visible = True
            Me.txtLen15.Visible = True

            Me.lblDelimiter.Visible = False
            Me.lblSurround.Visible = False
            Me.txtSurround.Visible = False
            Me.txtDelimiter.Visible = False

        End If


        bAnythingChangedInThisForm = True
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        If bAnythingChangedInThisForm Then
            ' Save to Mappingfile
            SaveToMappingFile()
        End If

        Me.Hide()
        bStatus = True


    End Sub

    Private Sub cmdSave_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSave.Click
        ' Save to Mappingfile
        SaveToMappingFile()

    End Sub

    Sub frmMapping_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg")
        FormLRSCaptions(Me)
        ' Sett DnBNor TBI Sendprofile in forms caption:
        '''FormSelectAllTextboxs Me

        bAnythingChangedInThisForm = False
    End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        If bAnythingChangedInThisForm Then
            If MsgBox(LRS(60539), MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1) Then ' lrs  "Vil du lagre endring til mappingfil"
                SaveToMappingFile()
            End If
        End If

        Me.Hide()
        bStatus = False

    End Sub
    Private Sub cmdFileOpenMapping_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpenMapping.Click
        ' Hent filnavn
        Dim spath As String
        'spath = FixPath((Me.txtMappingFile.Text))
        spath = BrowseForFilesOrFolders(Me.txtMappingFile.Text, Me, LRS(60010), True)

        If Len(spath) > 0 Then
            Me.txtMappingFile.Text = spath
        End If

    End Sub

    Private Sub cmdLoad_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdLoad.Click

        ' Try to find mappingfile
        If txtMappingFile.Text <> "" And Dir(txtMappingFile.Text) <> "" Then

            ' Fill from MappingObject into textcontrols in frmMapping
            FillFrmMapping()

        Else
            MsgBox(LRS(60540), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "Mapping") 'lrs  Mappingfile not found!
        End If


    End Sub
    Public Function FillFrmMapping() As Object
        Dim sTmp As String
        Dim oXMLMappingObject As New MSXML2.DOMDocument40
        Dim NodeAttribute As MSXML2.IXMLDOMNode

        If Dir(txtMappingFile.Text) <> "" Then
            oXMLMappingObject.load(txtMappingFile.Text)

            ' From General
            '-------------
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/General")
            ' Fill from oXMLMappingObject into textcontrols in frmMapping
            Me.txtHeaderLines.Text = NodeAttribute.attributes.getNamedItem("Headerlines").nodeValue
            Me.txtDelimiter.Text = NodeAttribute.attributes.getNamedItem("Delimiter").nodeValue
            Me.txtSurround.Text = NodeAttribute.attributes.getNamedItem("Surround").nodeValue
            Me.txtThousandSep.Text = NodeAttribute.attributes.getNamedItem("AmountThousandSep").nodeValue
            Me.txtDecimalSep.Text = NodeAttribute.attributes.getNamedItem("AmountDecimalSep").nodeValue

            'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sTmp = NodeAttribute.attributes.getNamedItem("FileType").nodeValue
            Select Case sTmp
                Case "Delimited"
                    Me.cmbFileType.SelectedIndex = 0
                Case "Fixed"
                    Me.cmbFileType.SelectedIndex = 1
                Case "Excel"
                    Me.cmbFileType.SelectedIndex = 2
                Case Else
                    Me.cmbFileType.SelectedIndex = -1 ' err
            End Select

            Me.txtDateFormat.Text = NodeAttribute.attributes.getNamedItem("DateFormat").nodeValue

            ' For each variable
            '------------------
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/I_Account")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos1.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen1.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
                Me.txtFormat1.Text = NodeAttribute.attributes.getNamedItem("Fixed").nodeValue
            End If
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/E_Account")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos2.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen2.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
            End If
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/Name")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos3.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen3.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
            End If
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/Adr1")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos4.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen4.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
            End If
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/Adr2")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos5.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen5.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
            End If
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/Zip")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos6.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen6.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
            End If
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/City")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos7.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen7.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
            End If
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/PaymentDate")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos8.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen8.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
            End If
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/Amount")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos9.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen9.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
            End If

            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/Currency")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos10.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen10.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
                Me.txtFormat10.Text = NodeAttribute.attributes.getNamedItem("Fixed").nodeValue
            End If

            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/SWIFT")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos11.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen11.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
            End If
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/BranchNo")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos12.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen12.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
            End If
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/OwnRef")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos13.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen13.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
                Me.txtFormat13.Text = NodeAttribute.attributes.getNamedItem("Fixed").nodeValue
            End If
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/Freetext")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos14.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen14.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
                Me.txtFormat14.Text = NodeAttribute.attributes.getNamedItem("Fixed").nodeValue
            End If
            NodeAttribute = oXMLMappingObject.selectSingleNode("//*/Type")
            If Not NodeAttribute Is Nothing Then
                Me.txtPos15.Text = NodeAttribute.attributes.getNamedItem("Pos").nodeValue
                Me.txtLen15.Text = NodeAttribute.attributes.getNamedItem("Length").nodeValue
                If Not NodeAttribute.attributes.getNamedItem("Format") Is Nothing Then
                    Me.txtFormat15.Text = NodeAttribute.attributes.getNamedItem("Format").nodeValue
                End If
            End If
        End If

        'UPGRADE_NOTE: Object oXMLMappingObject may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oXMLMappingObject = Nothing
        bAnythingChangedInThisForm = False

    End Function
    Private Function SaveToMappingFile() As Object
        ' Save values from frmMapping to Mapping XML-file
        Dim sTmp As String
        Dim docXMLExport As New MSXML2.DOMDocument40
        'Dim pi As MSXML2.IXMLDOMProcessingInstruction
        ''Dim nodRootElement As MSXML2.IXMLDOMElement
        Dim newEle As MSXML2.IXMLDOMElement
        Dim newGeneralEle As MSXML2.IXMLDOMElement
        Dim newTypeEle As MSXML2.IXMLDOMElement
        Dim newLineEle As MSXML2.IXMLDOMElement
        Dim newAtt As MSXML2.IXMLDOMElement

        docXMLExport.preserveWhiteSpace = True
        docXMLExport.async = False
        ' Top element in mappingfile
        newEle = docXMLExport.createNode(MSXML2.tagDOMNodeType.NODE_ELEMENT, "BabelMapping", "")
        docXMLExport.documentElement = newEle
        'UPGRADE_NOTE: Object newEle may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        newEle = Nothing

        ' Write the General-part:
        '------------------------
        newGeneralEle = docXMLExport.createElement("General")
        newGeneralEle.setAttribute("Headerlines", Val(Me.txtHeaderLines.Text))
        newGeneralEle.setAttribute("Delimiter", Me.txtDelimiter.Text)
        newGeneralEle.setAttribute("Surround", Me.txtSurround.Text)
        newGeneralEle.setAttribute("AmountThousandSep", Me.txtThousandSep.Text)
        newGeneralEle.setAttribute("AmountDecimalSep", Me.txtDecimalSep.Text)

        Select Case Me.cmbFileType.SelectedIndex
            Case 0
                newGeneralEle.setAttribute("FileType", "Delimited")
            Case 1
                newGeneralEle.setAttribute("FileType", "Fixed")
            Case 2
                newGeneralEle.setAttribute("FileType", "Excel")
                ' Always ; as delimiter for Excel
                newGeneralEle.setAttribute("Delimiter", ";")
            Case Else
                ' -1, not selected. Use Delimited as "default"
                Me.cmbFileType.SelectedIndex = 0
                newGeneralEle.setAttribute("FileType", "Delimited")
        End Select

        newGeneralEle.setAttribute("DateFormat", Me.txtDateFormat.Text)

        'UPGRADE_WARNING: Couldn't resolve default property of object newGeneralEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        docXMLExport.documentElement.appendChild(newGeneralEle)


        ' Save pos, len, etc for each variable
        '-------------------------------------
        ' First, create a new ChildNode, Type
        newTypeEle = docXMLExport.createElement("Line")

        ' Then create new child to Type,
        ' FromAccount
        '-------------------------------------------
        If Not (EmptyString((Me.txtPos1.Text)) And EmptyString((Me.txtFormat1.Text))) Then
            newLineEle = docXMLExport.createElement("I_Account")
            newLineEle.setAttribute("Pos", Me.txtPos1.Text)
            newLineEle.setAttribute("Length", Me.txtLen1.Text)
            newLineEle.setAttribute("Fixed", Me.txtFormat1.Text)

            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' ToAccount
        '-------------------------------------------
        If Not EmptyString((Me.txtPos2.Text)) Then
            newLineEle = docXMLExport.createElement("E_Account")
            newLineEle.setAttribute("Pos", Me.txtPos2.Text)
            newLineEle.setAttribute("Length", Me.txtLen2.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' Receivers name
        '-------------------------------------------
        If Not EmptyString((Me.txtPos3.Text)) Then
            newLineEle = docXMLExport.createElement("Name")
            newLineEle.setAttribute("Pos", Me.txtPos3.Text)
            newLineEle.setAttribute("Length", Me.txtLen3.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' Receivers adr1
        '-------------------------------------------
        If Not EmptyString((Me.txtPos5.Text)) Then
            newLineEle = docXMLExport.createElement("Adr1")
            newLineEle.setAttribute("Pos", Me.txtPos4.Text)
            newLineEle.setAttribute("Length", Me.txtLen4.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' Receivers adr2
        '-------------------------------------------
        If Not EmptyString((Me.txtPos5.Text)) Then
            newLineEle = docXMLExport.createElement("Adr2")
            newLineEle.setAttribute("Pos", Me.txtPos5.Text)
            newLineEle.setAttribute("Length", Me.txtLen5.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' Receivers Zip
        '-------------------------------------------
        If Not EmptyString((Me.txtPos6.Text)) Then
            newLineEle = docXMLExport.createElement("Zip")
            newLineEle.setAttribute("Pos", Me.txtPos6.Text)
            newLineEle.setAttribute("Length", Me.txtLen6.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' Receivers City
        '-------------------------------------------
        If Not EmptyString((Me.txtPos7.Text)) Then
            newLineEle = docXMLExport.createElement("City")
            newLineEle.setAttribute("Pos", Me.txtPos7.Text)
            newLineEle.setAttribute("Length", Me.txtLen7.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If


        ' Then create new child to Type,
        ' PaymentDate
        '-------------------------------------------
        If Not EmptyString((Me.txtPos8.Text)) Then
            newLineEle = docXMLExport.createElement("PaymentDate")
            newLineEle.setAttribute("Pos", Me.txtPos8.Text)
            newLineEle.setAttribute("Length", Me.txtLen8.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' Amount
        '-------------------------------------------
        If Not EmptyString((Me.txtPos9.Text)) Then
            newLineEle = docXMLExport.createElement("Amount")
            newLineEle.setAttribute("Pos", Me.txtPos9.Text)
            newLineEle.setAttribute("Length", Me.txtLen9.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' Currency
        '-------------------------------------------
        If Not (EmptyString((Me.txtPos10.Text)) And EmptyString((Me.txtFormat10.Text))) Then
            newLineEle = docXMLExport.createElement("Currency")
            newLineEle.setAttribute("Pos", Me.txtPos10.Text)
            newLineEle.setAttribute("Length", Me.txtLen10.Text)
            newLineEle.setAttribute("Fixed", Me.txtFormat10.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' SWIFT
        '-------------------------------------------
        If Not EmptyString((Me.txtPos11.Text)) Then
            newLineEle = docXMLExport.createElement("SWIFT")
            newLineEle.setAttribute("Pos", Me.txtPos11.Text)
            newLineEle.setAttribute("Length", Me.txtLen11.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' Bankcode (bankbranch, FED, BLZ, Sortcode etc)
        '-------------------------------------------
        If Not EmptyString((Me.txtPos12.Text)) Then
            newLineEle = docXMLExport.createElement("BranchNo")
            newLineEle.setAttribute("Pos", Me.txtPos12.Text)
            newLineEle.setAttribute("Length", Me.txtLen12.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' OwnRef
        '-------------------------------------------
        If Not EmptyString((Me.txtPos13.Text)) Then
            newLineEle = docXMLExport.createElement("OwnRef")
            newLineEle.setAttribute("Pos", Me.txtPos13.Text)
            newLineEle.setAttribute("Length", Me.txtLen13.Text)
            newLineEle.setAttribute("Fixed", Me.txtFormat13.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' Freetext, notification to receiver
        '-------------------------------------------
        If Not EmptyString((Me.txtPos14.Text)) Then
            newLineEle = docXMLExport.createElement("Freetext")
            newLineEle.setAttribute("Pos", Me.txtPos14.Text)
            newLineEle.setAttribute("Length", Me.txtLen14.Text)
            newLineEle.setAttribute("Fixed", Me.txtFormat14.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If

        ' Then create new child to Type,
        ' Type
        '-------------------------------------------
        If Not EmptyString((Me.txtPos15.Text)) Then
            newLineEle = docXMLExport.createElement("Type")
            newLineEle.setAttribute("Pos", Me.txtPos15.Text)
            newLineEle.setAttribute("Length", Me.txtLen15.Text)
            newLineEle.setAttribute("Format", Me.txtFormat15.Text)
            ' Then add the PaymentDate child to Type-child
            'UPGRADE_WARNING: Couldn't resolve default property of object newLineEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            newTypeEle.appendChild(newLineEle)
        End If


        'UPGRADE_WARNING: Couldn't resolve default property of object newTypeEle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        docXMLExport.documentElement.appendChild(newTypeEle)


        docXMLExport.save(Me.txtMappingFile.Text)
        'UPGRADE_NOTE: Object docXMLExport may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        docXMLExport = Nothing
        docXMLExport = New MSXML2.DOMDocument40
        docXMLExport.async = False
        bAnythingChangedInThisForm = False


    End Function
    'UPGRADE_WARNING: Event txtHeaderLines.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtHeaderLines_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHeaderLines.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtDateFormat.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtDateFormat_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDateFormat.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtDecimalSep.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtDecimalSep_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDecimalSep.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtThousandSep.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtThousandSep_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtThousandSep.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtDelimiter.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtDelimiter_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDelimiter.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtSurround.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtSurround_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSurround.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtMappingFile.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtMappingFile_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMappingFile.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    Private Sub txtPayType_Change()
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos1.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos1_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos1.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen1.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen1_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen1.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos2.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos2_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos2.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen2.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen2_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen2.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos3.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos3_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos3.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen3.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen3_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen3.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos4.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos4_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos4.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen4.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen4_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen4.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos5.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos5_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos5.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen5.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen5_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen5.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos6.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos6_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos6.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen6.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen6_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen6.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos7.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos7_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos7.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen7.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen7_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen7.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos8.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos8_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos8.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen8.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen8_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen8.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos9.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos9_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos9.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen9.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen9_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen9.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos10.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos10_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos10.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen10.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen10_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen10.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos11.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos11_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos11.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen11.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen11_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen11.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos12.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos12_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos12.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen12.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen12_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen12.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos13.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos13_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos13.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen13.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen13_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen13.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos14.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos14_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos14.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen14.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen14_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen14.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtPos15.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPos15_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPos15.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
    'UPGRADE_WARNING: Event txtLen15.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtLen15_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLen15.TextChanged
        bAnythingChangedInThisForm = True
    End Sub
End Class
