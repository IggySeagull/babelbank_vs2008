<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmClientAutoBook
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtVATCode As System.Windows.Forms.TextBox
	Public WithEvents cmdRemoveCriteria As System.Windows.Forms.Button
	Public WithEvents cmdAddDetail As System.Windows.Forms.Button
	Public WithEvents lstAutoBookDetails As System.Windows.Forms.ListBox
	Public WithEvents chkProposedMatch As System.Windows.Forms.CheckBox
	Public WithEvents chkActive As System.Windows.Forms.CheckBox
	Public WithEvents cmdAddEasyAutoBook As System.Windows.Forms.Button
	Public WithEvents txtAutoValue As System.Windows.Forms.TextBox
	Public WithEvents cmbAutoSelectField As System.Windows.Forms.ComboBox
	Public WithEvents cmdShowAutoBooks As System.Windows.Forms.Button
	Public WithEvents cmbAutoType As System.Windows.Forms.ComboBox
	Public WithEvents txtAutoText As System.Windows.Forms.TextBox
	Public WithEvents txtAutoToAccount As System.Windows.Forms.TextBox
	Public WithEvents txtAutoName As System.Windows.Forms.TextBox
	Public WithEvents cmdUpdateAutoBook As System.Windows.Forms.Button
	Public WithEvents cmdRemoveAutoBook As System.Windows.Forms.Button
	Public WithEvents lstAutoBooks As System.Windows.Forms.ListBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
    Public WithEvents lblVATCode As System.Windows.Forms.Label
	Public WithEvents lblAutoValue As System.Windows.Forms.Label
	Public WithEvents lblAutoSelectField As System.Windows.Forms.Label
    Public WithEvents lblAutoText As System.Windows.Forms.Label
	Public WithEvents lbAutoType As System.Windows.Forms.Label
	Public WithEvents lblAutoToAccount As System.Windows.Forms.Label
	Public WithEvents lblAutoName As System.Windows.Forms.Label
	Public WithEvents lblAuto As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtVATCode = New System.Windows.Forms.TextBox
        Me.cmdRemoveCriteria = New System.Windows.Forms.Button
        Me.cmdAddDetail = New System.Windows.Forms.Button
        Me.lstAutoBookDetails = New System.Windows.Forms.ListBox
        Me.chkProposedMatch = New System.Windows.Forms.CheckBox
        Me.chkActive = New System.Windows.Forms.CheckBox
        Me.cmdAddEasyAutoBook = New System.Windows.Forms.Button
        Me.txtAutoValue = New System.Windows.Forms.TextBox
        Me.cmbAutoSelectField = New System.Windows.Forms.ComboBox
        Me.cmdShowAutoBooks = New System.Windows.Forms.Button
        Me.cmbAutoType = New System.Windows.Forms.ComboBox
        Me.txtAutoText = New System.Windows.Forms.TextBox
        Me.txtAutoToAccount = New System.Windows.Forms.TextBox
        Me.txtAutoName = New System.Windows.Forms.TextBox
        Me.cmdUpdateAutoBook = New System.Windows.Forms.Button
        Me.cmdRemoveAutoBook = New System.Windows.Forms.Button
        Me.lstAutoBooks = New System.Windows.Forms.ListBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.lblVATCode = New System.Windows.Forms.Label
        Me.lblAutoValue = New System.Windows.Forms.Label
        Me.lblAutoSelectField = New System.Windows.Forms.Label
        Me.lblAutoText = New System.Windows.Forms.Label
        Me.lbAutoType = New System.Windows.Forms.Label
        Me.lblAutoToAccount = New System.Windows.Forms.Label
        Me.lblAutoName = New System.Windows.Forms.Label
        Me.lblAuto = New System.Windows.Forms.Label
        Me.cmdDimensions = New System.Windows.Forms.Button
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtVATCode
        '
        Me.txtVATCode.AcceptsReturn = True
        Me.txtVATCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtVATCode.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVATCode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVATCode.Location = New System.Drawing.Point(200, 292)
        Me.txtVATCode.MaxLength = 10
        Me.txtVATCode.Name = "txtVATCode"
        Me.txtVATCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVATCode.Size = New System.Drawing.Size(53, 20)
        Me.txtVATCode.TabIndex = 4
        '
        'cmdRemoveCriteria
        '
        Me.cmdRemoveCriteria.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemoveCriteria.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRemoveCriteria.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRemoveCriteria.Location = New System.Drawing.Point(376, 397)
        Me.cmdRemoveCriteria.Name = "cmdRemoveCriteria"
        Me.cmdRemoveCriteria.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRemoveCriteria.Size = New System.Drawing.Size(73, 27)
        Me.cmdRemoveCriteria.TabIndex = 26
        Me.cmdRemoveCriteria.TabStop = False
        Me.cmdRemoveCriteria.Text = "55044-Remove &criteria"
        Me.cmdRemoveCriteria.UseVisualStyleBackColor = False
        '
        'cmdAddDetail
        '
        Me.cmdAddDetail.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddDetail.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAddDetail.Enabled = False
        Me.cmdAddDetail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAddDetail.Location = New System.Drawing.Point(376, 366)
        Me.cmdAddDetail.Name = "cmdAddDetail"
        Me.cmdAddDetail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAddDetail.Size = New System.Drawing.Size(73, 27)
        Me.cmdAddDetail.TabIndex = 25
        Me.cmdAddDetail.TabStop = False
        Me.cmdAddDetail.Text = "55042-Add criteria"
        Me.cmdAddDetail.UseVisualStyleBackColor = False
        '
        'lstAutoBookDetails
        '
        Me.lstAutoBookDetails.BackColor = System.Drawing.SystemColors.Window
        Me.lstAutoBookDetails.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstAutoBookDetails.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstAutoBookDetails.Location = New System.Drawing.Point(45, 366)
        Me.lstAutoBookDetails.Name = "lstAutoBookDetails"
        Me.lstAutoBookDetails.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstAutoBookDetails.Size = New System.Drawing.Size(314, 95)
        Me.lstAutoBookDetails.TabIndex = 24
        Me.lstAutoBookDetails.TabStop = False
        '
        'chkProposedMatch
        '
        Me.chkProposedMatch.BackColor = System.Drawing.SystemColors.Control
        Me.chkProposedMatch.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkProposedMatch.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkProposedMatch.Location = New System.Drawing.Point(200, 321)
        Me.chkProposedMatch.Name = "chkProposedMatch"
        Me.chkProposedMatch.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkProposedMatch.Size = New System.Drawing.Size(161, 17)
        Me.chkProposedMatch.TabIndex = 6
        Me.chkProposedMatch.Text = "60403 - Proposed match"
        Me.chkProposedMatch.UseVisualStyleBackColor = False
        '
        'chkActive
        '
        Me.chkActive.BackColor = System.Drawing.SystemColors.Control
        Me.chkActive.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkActive.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkActive.Location = New System.Drawing.Point(48, 321)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkActive.Size = New System.Drawing.Size(129, 17)
        Me.chkActive.TabIndex = 5
        Me.chkActive.Text = "60452 - Active"
        Me.chkActive.UseVisualStyleBackColor = False
        '
        'cmdAddEasyAutoBook
        '
        Me.cmdAddEasyAutoBook.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddEasyAutoBook.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAddEasyAutoBook.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAddEasyAutoBook.Location = New System.Drawing.Point(375, 147)
        Me.cmdAddEasyAutoBook.Name = "cmdAddEasyAutoBook"
        Me.cmdAddEasyAutoBook.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAddEasyAutoBook.Size = New System.Drawing.Size(73, 21)
        Me.cmdAddEasyAutoBook.TabIndex = 23
        Me.cmdAddEasyAutoBook.TabStop = False
        Me.cmdAddEasyAutoBook.Text = "55018-Add"
        Me.cmdAddEasyAutoBook.UseVisualStyleBackColor = False
        '
        'txtAutoValue
        '
        Me.txtAutoValue.AcceptsReturn = True
        Me.txtAutoValue.BackColor = System.Drawing.SystemColors.Window
        Me.txtAutoValue.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAutoValue.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAutoValue.Location = New System.Drawing.Point(200, 498)
        Me.txtAutoValue.MaxLength = 50
        Me.txtAutoValue.Name = "txtAutoValue"
        Me.txtAutoValue.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAutoValue.Size = New System.Drawing.Size(160, 20)
        Me.txtAutoValue.TabIndex = 8
        '
        'cmbAutoSelectField
        '
        Me.cmbAutoSelectField.BackColor = System.Drawing.SystemColors.Window
        Me.cmbAutoSelectField.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbAutoSelectField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAutoSelectField.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbAutoSelectField.Location = New System.Drawing.Point(200, 472)
        Me.cmbAutoSelectField.Name = "cmbAutoSelectField"
        Me.cmbAutoSelectField.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbAutoSelectField.Size = New System.Drawing.Size(160, 21)
        Me.cmbAutoSelectField.TabIndex = 7
        '
        'cmdShowAutoBooks
        '
        Me.cmdShowAutoBooks.BackColor = System.Drawing.SystemColors.Control
        Me.cmdShowAutoBooks.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdShowAutoBooks.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdShowAutoBooks.Location = New System.Drawing.Point(375, 70)
        Me.cmdShowAutoBooks.Name = "cmdShowAutoBooks"
        Me.cmdShowAutoBooks.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdShowAutoBooks.Size = New System.Drawing.Size(73, 21)
        Me.cmdShowAutoBooks.TabIndex = 20
        Me.cmdShowAutoBooks.TabStop = False
        Me.cmdShowAutoBooks.Text = "55016-Show"
        Me.cmdShowAutoBooks.UseVisualStyleBackColor = False
        '
        'cmbAutoType
        '
        Me.cmbAutoType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbAutoType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbAutoType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAutoType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbAutoType.Location = New System.Drawing.Point(200, 237)
        Me.cmbAutoType.Name = "cmbAutoType"
        Me.cmbAutoType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbAutoType.Size = New System.Drawing.Size(160, 21)
        Me.cmbAutoType.TabIndex = 2
        '
        'txtAutoText
        '
        Me.txtAutoText.AcceptsReturn = True
        Me.txtAutoText.BackColor = System.Drawing.SystemColors.Window
        Me.txtAutoText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAutoText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAutoText.Location = New System.Drawing.Point(200, 263)
        Me.txtAutoText.MaxLength = 50
        Me.txtAutoText.Name = "txtAutoText"
        Me.txtAutoText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAutoText.Size = New System.Drawing.Size(160, 20)
        Me.txtAutoText.TabIndex = 3
        '
        'txtAutoToAccount
        '
        Me.txtAutoToAccount.AcceptsReturn = True
        Me.txtAutoToAccount.BackColor = System.Drawing.SystemColors.Window
        Me.txtAutoToAccount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAutoToAccount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAutoToAccount.Location = New System.Drawing.Point(200, 212)
        Me.txtAutoToAccount.MaxLength = 30
        Me.txtAutoToAccount.Name = "txtAutoToAccount"
        Me.txtAutoToAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAutoToAccount.Size = New System.Drawing.Size(160, 20)
        Me.txtAutoToAccount.TabIndex = 1
        '
        'txtAutoName
        '
        Me.txtAutoName.AcceptsReturn = True
        Me.txtAutoName.BackColor = System.Drawing.SystemColors.Window
        Me.txtAutoName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAutoName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAutoName.Location = New System.Drawing.Point(200, 188)
        Me.txtAutoName.MaxLength = 30
        Me.txtAutoName.Name = "txtAutoName"
        Me.txtAutoName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAutoName.Size = New System.Drawing.Size(160, 20)
        Me.txtAutoName.TabIndex = 0
        '
        'cmdUpdateAutoBook
        '
        Me.cmdUpdateAutoBook.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUpdateAutoBook.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdUpdateAutoBook.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdUpdateAutoBook.Location = New System.Drawing.Point(375, 122)
        Me.cmdUpdateAutoBook.Name = "cmdUpdateAutoBook"
        Me.cmdUpdateAutoBook.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdUpdateAutoBook.Size = New System.Drawing.Size(73, 21)
        Me.cmdUpdateAutoBook.TabIndex = 15
        Me.cmdUpdateAutoBook.TabStop = False
        Me.cmdUpdateAutoBook.Text = "55023-Update"
        Me.cmdUpdateAutoBook.UseVisualStyleBackColor = False
        '
        'cmdRemoveAutoBook
        '
        Me.cmdRemoveAutoBook.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemoveAutoBook.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRemoveAutoBook.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRemoveAutoBook.Location = New System.Drawing.Point(375, 96)
        Me.cmdRemoveAutoBook.Name = "cmdRemoveAutoBook"
        Me.cmdRemoveAutoBook.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRemoveAutoBook.Size = New System.Drawing.Size(73, 21)
        Me.cmdRemoveAutoBook.TabIndex = 14
        Me.cmdRemoveAutoBook.TabStop = False
        Me.cmdRemoveAutoBook.Text = "55015-Remove"
        Me.cmdRemoveAutoBook.UseVisualStyleBackColor = False
        '
        'lstAutoBooks
        '
        Me.lstAutoBooks.BackColor = System.Drawing.SystemColors.Window
        Me.lstAutoBooks.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstAutoBooks.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstAutoBooks.Location = New System.Drawing.Point(48, 72)
        Me.lstAutoBooks.Name = "lstAutoBooks"
        Me.lstAutoBooks.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstAutoBooks.Size = New System.Drawing.Size(312, 95)
        Me.lstAutoBooks.TabIndex = 10
        Me.lstAutoBooks.TabStop = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(377, 541)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 9
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(299, 541)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 12
        Me.CmdCancel.TabStop = False
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(220, 541)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 11
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "55001 - &Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'lblVATCode
        '
        Me.lblVATCode.BackColor = System.Drawing.SystemColors.Control
        Me.lblVATCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVATCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVATCode.Location = New System.Drawing.Point(48, 292)
        Me.lblVATCode.Name = "lblVATCode"
        Me.lblVATCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVATCode.Size = New System.Drawing.Size(147, 19)
        Me.lblVATCode.TabIndex = 27
        Me.lblVATCode.Text = "60401 -  VATCode"
        '
        'lblAutoValue
        '
        Me.lblAutoValue.BackColor = System.Drawing.SystemColors.Control
        Me.lblAutoValue.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAutoValue.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAutoValue.Location = New System.Drawing.Point(48, 500)
        Me.lblAutoValue.Name = "lblAutoValue"
        Me.lblAutoValue.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAutoValue.Size = New System.Drawing.Size(153, 19)
        Me.lblAutoValue.TabIndex = 22
        Me.lblAutoValue.Text = "60438 - Fieldvalue"
        '
        'lblAutoSelectField
        '
        Me.lblAutoSelectField.BackColor = System.Drawing.SystemColors.Control
        Me.lblAutoSelectField.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAutoSelectField.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAutoSelectField.Location = New System.Drawing.Point(48, 474)
        Me.lblAutoSelectField.Name = "lblAutoSelectField"
        Me.lblAutoSelectField.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAutoSelectField.Size = New System.Drawing.Size(153, 19)
        Me.lblAutoSelectField.TabIndex = 21
        Me.lblAutoSelectField.Text = "60437 - Select from field"
        '
        'lblAutoText
        '
        Me.lblAutoText.BackColor = System.Drawing.SystemColors.Control
        Me.lblAutoText.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAutoText.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAutoText.Location = New System.Drawing.Point(48, 265)
        Me.lblAutoText.Name = "lblAutoText"
        Me.lblAutoText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAutoText.Size = New System.Drawing.Size(153, 19)
        Me.lblAutoText.TabIndex = 19
        Me.lblAutoText.Text = "60210 - Text"
        '
        'lbAutoType
        '
        Me.lbAutoType.BackColor = System.Drawing.SystemColors.Control
        Me.lbAutoType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lbAutoType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbAutoType.Location = New System.Drawing.Point(48, 239)
        Me.lbAutoType.Name = "lbAutoType"
        Me.lbAutoType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbAutoType.Size = New System.Drawing.Size(153, 19)
        Me.lbAutoType.TabIndex = 18
        Me.lbAutoType.Text = "60359-AccountType"
        '
        'lblAutoToAccount
        '
        Me.lblAutoToAccount.BackColor = System.Drawing.SystemColors.Control
        Me.lblAutoToAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAutoToAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAutoToAccount.Location = New System.Drawing.Point(48, 213)
        Me.lblAutoToAccount.Name = "lblAutoToAccount"
        Me.lblAutoToAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAutoToAccount.Size = New System.Drawing.Size(153, 19)
        Me.lblAutoToAccount.TabIndex = 17
        Me.lblAutoToAccount.Text = "60439 - Book to account"
        '
        'lblAutoName
        '
        Me.lblAutoName.BackColor = System.Drawing.SystemColors.Control
        Me.lblAutoName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAutoName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAutoName.Location = New System.Drawing.Point(48, 189)
        Me.lblAutoName.Name = "lblAutoName"
        Me.lblAutoName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAutoName.Size = New System.Drawing.Size(154, 19)
        Me.lblAutoName.TabIndex = 16
        Me.lblAutoName.Text = "60206-Name"
        '
        'lblAuto
        '
        Me.lblAuto.BackColor = System.Drawing.SystemColors.Control
        Me.lblAuto.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAuto.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAuto.Location = New System.Drawing.Point(48, 52)
        Me.lblAuto.Name = "lblAuto"
        Me.lblAuto.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAuto.Size = New System.Drawing.Size(173, 14)
        Me.lblAuto.TabIndex = 13
        Me.lblAuto.Text = "60436 - Autobookings"
        '
        'cmdDimensions
        '
        Me.cmdDimensions.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDimensions.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDimensions.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDimensions.Location = New System.Drawing.Point(375, 212)
        Me.cmdDimensions.Name = "cmdDimensions"
        Me.cmdDimensions.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDimensions.Size = New System.Drawing.Size(73, 21)
        Me.cmdDimensions.TabIndex = 29
        Me.cmdDimensions.TabStop = False
        Me.cmdDimensions.Text = "55049-Dim"
        Me.cmdDimensions.UseVisualStyleBackColor = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 352)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(435, 1)
        Me.lblLine1.TabIndex = 30
        Me.lblLine1.Text = "Label1"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(13, 529)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(435, 1)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Label1"
        '
        'frmClientAutoBook
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(461, 575)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdDimensions)
        Me.Controls.Add(Me.txtVATCode)
        Me.Controls.Add(Me.cmdRemoveCriteria)
        Me.Controls.Add(Me.cmdAddDetail)
        Me.Controls.Add(Me.lstAutoBookDetails)
        Me.Controls.Add(Me.chkProposedMatch)
        Me.Controls.Add(Me.chkActive)
        Me.Controls.Add(Me.cmdAddEasyAutoBook)
        Me.Controls.Add(Me.txtAutoValue)
        Me.Controls.Add(Me.cmbAutoSelectField)
        Me.Controls.Add(Me.cmdShowAutoBooks)
        Me.Controls.Add(Me.cmbAutoType)
        Me.Controls.Add(Me.txtAutoText)
        Me.Controls.Add(Me.txtAutoToAccount)
        Me.Controls.Add(Me.txtAutoName)
        Me.Controls.Add(Me.cmdUpdateAutoBook)
        Me.Controls.Add(Me.cmdRemoveAutoBook)
        Me.Controls.Add(Me.lstAutoBooks)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.lblVATCode)
        Me.Controls.Add(Me.lblAutoValue)
        Me.Controls.Add(Me.lblAutoSelectField)
        Me.Controls.Add(Me.lblAutoText)
        Me.Controls.Add(Me.lbAutoType)
        Me.Controls.Add(Me.lblAutoToAccount)
        Me.Controls.Add(Me.lblAutoName)
        Me.Controls.Add(Me.lblAuto)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(372, 30)
        Me.Name = "frmClientAutoBook"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "60436 - AutoBooking"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents cmdDimensions As System.Windows.Forms.Button
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
#End Region 
End Class
