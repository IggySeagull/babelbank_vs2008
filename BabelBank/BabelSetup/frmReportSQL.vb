﻿Public Class frmReportSQL
    Private sReportNo As String
    Private iSortOrder As Short
    Private bIncludeOCR As Boolean
    Private sSavedSelect As String
    Private sSavedWhere As String
    Private sSavedOrderBy As String
    Private bSavebeforeReport As Boolean 'Added 05.12.2013
    Private bActivateSpecialReport As Boolean
    Private Sub frmReportSQL_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "", 400)
        FormLRSCaptions(Me)
        sSavedSelect = Me.txtSelect.Text
        sSavedWhere = Me.txtWhere.Text
        sSavedOrderBy = Me.txtOrderBy.Text
        bActivateSpecialReport = Me.chkActivateSQL.Checked
    End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        ' If user has temporarily saved to sSavedSQL, save that one !
        If sSavedSelect = "" And sSavedWhere = "" And sSavedOrderBy = "" Then
            Me.DialogResult = 0
        Else
            Me.txtSelect.Text = sSavedSelect
            Me.txtWhere.Text = sSavedWhere
            Me.txtOrderBy.Text = sSavedOrderBy
            Me.chkActivateSQL.Checked = bActivateSpecialReport
            Me.DialogResult = 1
        End If

        Me.Hide()
    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.DialogResult = 1
        Me.Hide()
    End Sub
    Public Sub PassReportNo(ByVal sNo As String)
        sReportNo = sNo
    End Sub
    Public Sub PassSortOrder(ByVal iSort As Short)
        iSortOrder = iSort
    End Sub
    Public Sub PassIncludeOCR(ByVal bInclude As Boolean)
        bIncludeOCR = bInclude
    End Sub
    Public Sub PassSavebeforeReport(ByVal bSavebefore As Boolean)
        bSavebeforeReport = bSavebefore
    End Sub
    Public Sub PassReport(ByVal oRp As vbBabel.Report)
        oReport = oRp
    End Sub
    Private Sub cmdResetStandard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdResetStandard.Click
        ' Set standard for this report
        Dim sSelectPart As String
        Dim sWherePart As String
        Dim sOrderPart As String
        Dim sMySQL As String

        sSelectPart = ""
        sWherePart = ""
        sOrderPart = ""
        sMySQL = Replace(RetrieveSQLForReports(BB_UseSQLServer(), sReportNo, iSortOrder, bIncludeOCR, bActivateSpecialReport, "", sSelectPart, sWherePart, sOrderPart), "@?@", "'")
        Me.txtSelect.Text = sSelectPart
        Me.txtWhere.Text = sWherePart
        Me.txtOrderBy.Text = sOrderPart
        Me.chkActivateSQL.Checked = False
        sSavedSelect = ""
        sSavedWhere = ""
        sSavedOrderBy = ""

    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        ' temporary save to sSaved...
        sSavedSelect = Me.txtSelect.Text
        sSavedWhere = Me.txtWhere.Text
        sSavedOrderBy = Me.txtOrderBy.Text
        bActivateSpecialReport = Me.chkActivateSQL.Checked
    End Sub
End Class