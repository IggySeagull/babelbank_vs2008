Option Strict Off
Option Explicit On
Friend Class frmEmail
	Inherits System.Windows.Forms.Form
	
    Private Sub cmdTest_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdTest.Click
        'send email for test
        Dim mymail As vbBabel.vbMail
        Dim sHost As String
        Dim sMsgNote As String

        Me.lblResult.Text = ""
        mymail = New vbBabel.vbMail

        If Me.optSMTP.Checked Then
            On Error GoTo errMail
            ' Use new class/dll vbSendmail
            '-----------------------------
            mymail.EMail_SMTP = True
            mymail.eMailSMTPHost = Me.txtSMTPHost.Text
        Else
            On Error GoTo MAPIFail
            mymail.EMail_SMTP = False
        End If

        mymail.eMailSender = Me.txtSender.Text
        'mymail.UserName = "Jan-Petter@VisualBanking.net"
        'mymail.Password = "koko"
        'poSendMail.UseAuthentication = True
        mymail.eMailSenderDisplayName = Me.txtDisplayName.Text
        mymail.AddReceivers(Me.txtSupport.Text, Me.txtDisplayName.Text)
        mymail.AddReceivers(Me.txtAutoReceiver.Text, Me.txtAutoDisplayName.Text)
        mymail.eMailReplyTo = Me.txtReplyAdress.Text
        mymail.Subject = "BabelBank Testmail"
        mymail.Body = ""

        '29.09.2017 - Added next IF
        If BB_UseSQLServer() Then
            mymail.Body = "Kunden benytter SQL Server." & vbCrLf
        Else
            ' send Profile.mdb_Copy, due to problems sending .mdb-files, creates errors in MAPI
            If Len(Dir(BB_DatabasePath() & "_copy")) > 0 Then
                Kill(BB_DatabasePath() & "_copy")
            End If
            FileCopy(BB_DatabasePath, BB_DatabasePath() & "_copy")

            mymail.AddAttachment(BB_DatabasePath() & "_copy")
            mymail.AddAttachment(BB_LicensePath())

        End If

        'poSendMail.Attachment = "c:\projects\babelbank\license\babel.lic"
        'poSendMail.Attachment = "c:\projects\babelbank\license\babel.lic"
        If Me.optSMTP.Checked Then
            mymail.Body = mymail.Body & "Testmail from BabelBank with SMTP succesfully sendt!"
        Else
            mymail.Body = mymail.Body & "Testmail from BabelBank with MAPI succesfully sendt!"
        End If

        mymail.Send()

        If Not mymail.Success Then
            Me.lblResult.Text = LRS(60190) ' Error sending testmail
        Else
            If Me.optSMTP.Checked Then
                ' Gather info
                sHost = mymail.eMailSMTPHost
                Me.lblResult.Visible = True
                Me.lblResult.Text = LRS(60189) & " " & sHost
            Else
                Me.lblResult.Text = LRS(60191)
            End If
        End If

        mymail = Nothing
        Exit Sub

errMail:
        Me.lblResult.Visible = True
        Me.lblResult.Text = LRS(60190) & vbCrLf & Err.Description ' Error sending testmail
        mymail = Nothing
        Exit Sub

MAPIFail:
        ' Test errors:
        'Cancel of Mail-session gives Error.!
        If Err.Number <> 32001 Then ' User cancelled process
            Me.lblResult.Text = LRS(60190) & " " & Err.Description ' Error sending testmail
        Else
            Me.lblResult.Text = LRS(60192)
        End If
        mymail = Nothing


    End Sub

    Private Sub frmEmail_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        FormLRSCaptions(Me)
        FormvbStyle(Me, "dollar.jpg", 500)
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next

        Me.lblResult.Visible = False

    End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Me.lblResult.Text = ""
        Me.Hide()
    End Sub
    Private Sub frmEmail_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
            If MsgBox(LRS(60011), MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, LRS(60016)) = MsgBoxResult.Yes Then
                ' End form without saving
                Me.lblResult.Text = ""
                Cancel = False
            Else
                ' Save first! Do not end form
                Cancel = True
            End If
        End If
        eventArgs.Cancel = Cancel
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        SaveEMailSettings(Me)
        Me.lblResult.Text = ""
        Me.Hide()
    End Sub

    'UPGRADE_WARNING: Event optSMTP.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optSMTP_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optSMTP.CheckedChanged
        If eventSender.Checked Then
            If optSMTP.Checked Then
                ' SMTP selected, show controls;
                Me.lblAutoDisplayName.Visible = True
                Me.lblAutoReceiver.Visible = True
                Me.lblAutoSender.Visible = True
                Me.lblDisplayName.Visible = True
                Me.lblReplyAdress.Visible = True
                Me.lblSender.Visible = True
                Me.lblSMTPHost.Visible = True

                Me.txtAutoDisplayName.Visible = True
                Me.txtAutoReceiver.Visible = True
                Me.txtAutoSender.Visible = True
                Me.txtDisplayName.Visible = True
                Me.txtReplyAdress.Visible = True
                Me.txtSender.Visible = True
                Me.txtSMTPHost.Visible = True
            End If
        End If
    End Sub
    'UPGRADE_WARNING: Event optMAPI.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optMAPI_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optMAPI.CheckedChanged
        If eventSender.Checked Then
            If optMAPI.Checked Then
                ' MAPI selected, do not show controls;
                Me.lblAutoDisplayName.Visible = False
                Me.lblAutoReceiver.Visible = False
                Me.lblAutoSender.Visible = False
                Me.lblDisplayName.Visible = False
                Me.lblReplyAdress.Visible = False
                Me.lblSender.Visible = False
                Me.lblSMTPHost.Visible = False

                Me.txtAutoDisplayName.Visible = False
                Me.txtAutoReceiver.Visible = False
                Me.txtAutoSender.Visible = False
                Me.txtDisplayName.Visible = False
                Me.txtReplyAdress.Visible = False
                Me.txtSender.Visible = False
                Me.txtSMTPHost.Visible = False
            End If
        End If
    End Sub
End Class
