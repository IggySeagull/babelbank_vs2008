<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmClientDetail
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtBB_Period As System.Windows.Forms.TextBox
	Public WithEvents txtBB_Year As System.Windows.Forms.TextBox
	Public WithEvents chkAddVoucherCounter As System.Windows.Forms.CheckBox
	Public WithEvents chkAddOnOcr As System.Windows.Forms.CheckBox
	Public WithEvents optGetVNoBB As System.Windows.Forms.RadioButton
	Public WithEvents optGetVNoERP As System.Windows.Forms.RadioButton
	Public WithEvents OptDontUse As System.Windows.Forms.RadioButton
	Public WithEvents txtVoucherType2 As System.Windows.Forms.TextBox
	Public WithEvents txtVoucherNo2 As System.Windows.Forms.TextBox
    Public WithEvents txtVoucherNo As System.Windows.Forms.TextBox
	Public WithEvents txtDivision As System.Windows.Forms.TextBox
	Public WithEvents txtClientName As System.Windows.Forms.TextBox
	Public WithEvents txtClientNo As System.Windows.Forms.TextBox
	Public WithEvents txtCompanyNo As System.Windows.Forms.TextBox
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdOK As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents lblBB_Period As System.Windows.Forms.Label
	Public WithEvents lblBB_Year As System.Windows.Forms.Label
	Public WithEvents lblDescribeVoucherNo As System.Windows.Forms.Label
	Public WithEvents lblVoucherType2 As System.Windows.Forms.Label
	Public WithEvents lblVoucherNo2 As System.Windows.Forms.Label
	Public WithEvents lblVoucherType As System.Windows.Forms.Label
	Public WithEvents lblVoucherNo As System.Windows.Forms.Label
	Public WithEvents lblDivision As System.Windows.Forms.Label
	Public WithEvents lblClientName As System.Windows.Forms.Label
    Public WithEvents lblCompanyNo As System.Windows.Forms.Label
	Public WithEvents lblClientNo As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
		Me.txtBB_Period = New System.Windows.Forms.TextBox()
		Me.txtBB_Year = New System.Windows.Forms.TextBox()
		Me.chkAddVoucherCounter = New System.Windows.Forms.CheckBox()
		Me.chkAddOnOcr = New System.Windows.Forms.CheckBox()
		Me.optGetVNoBB = New System.Windows.Forms.RadioButton()
		Me.optGetVNoERP = New System.Windows.Forms.RadioButton()
		Me.OptDontUse = New System.Windows.Forms.RadioButton()
		Me.txtVoucherType2 = New System.Windows.Forms.TextBox()
		Me.txtVoucherNo2 = New System.Windows.Forms.TextBox()
		Me.txtVoucherNo = New System.Windows.Forms.TextBox()
		Me.txtDivision = New System.Windows.Forms.TextBox()
		Me.txtClientName = New System.Windows.Forms.TextBox()
		Me.txtClientNo = New System.Windows.Forms.TextBox()
		Me.txtCompanyNo = New System.Windows.Forms.TextBox()
		Me.CmdCancel = New System.Windows.Forms.Button()
		Me.CmdOK = New System.Windows.Forms.Button()
		Me.CmdHelp = New System.Windows.Forms.Button()
		Me.lblBB_Period = New System.Windows.Forms.Label()
		Me.lblBB_Year = New System.Windows.Forms.Label()
		Me.lblDescribeVoucherNo = New System.Windows.Forms.Label()
		Me.lblVoucherType2 = New System.Windows.Forms.Label()
		Me.lblVoucherNo2 = New System.Windows.Forms.Label()
		Me.lblVoucherType = New System.Windows.Forms.Label()
		Me.lblVoucherNo = New System.Windows.Forms.Label()
		Me.lblDivision = New System.Windows.Forms.Label()
		Me.lblClientName = New System.Windows.Forms.Label()
		Me.lblCompanyNo = New System.Windows.Forms.Label()
		Me.lblClientNo = New System.Windows.Forms.Label()
		Me.lblHeading = New System.Windows.Forms.Label()
		Me.lblISO20022 = New System.Windows.Forms.Label()
		Me.lblMsgID = New System.Windows.Forms.Label()
		Me.txtMsgId_Variable = New System.Windows.Forms.TextBox()
		Me.chkSEPASingle = New System.Windows.Forms.CheckBox()
		Me.txtHistoryDeleteDays = New System.Windows.Forms.TextBox()
		Me.lblDeleteDays = New System.Windows.Forms.Label()
		Me.chkSavePaymentData = New System.Windows.Forms.CheckBox()
		Me.lblValidationLevel = New System.Windows.Forms.Label()
		Me.chkEndToEndRefFromBabelBank = New System.Windows.Forms.CheckBox()
		Me.lstValidationLevel = New System.Windows.Forms.ComboBox()
		Me.chkUseStateBankText = New System.Windows.Forms.CheckBox()
		Me.lblLine1 = New System.Windows.Forms.Label()
		Me.lblLine2 = New System.Windows.Forms.Label()
		Me.lblLine3 = New System.Windows.Forms.Label()
		Me.cmdAdvanced = New System.Windows.Forms.Button()
		Me.txtVoucherType = New System.Windows.Forms.TextBox()
		Me.lblClientGroup = New System.Windows.Forms.Label()
		Me.cmbClientGroup = New System.Windows.Forms.ComboBox()
		Me.txtSocialSecurityNo = New System.Windows.Forms.TextBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.SuspendLayout()
		'
		'txtBB_Period
		'
		Me.txtBB_Period.AcceptsReturn = True
		Me.txtBB_Period.BackColor = System.Drawing.SystemColors.Window
		Me.txtBB_Period.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtBB_Period.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtBB_Period.Location = New System.Drawing.Point(940, 576)
		Me.txtBB_Period.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtBB_Period.MaxLength = 2
		Me.txtBB_Period.Name = "txtBB_Period"
		Me.txtBB_Period.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtBB_Period.Size = New System.Drawing.Size(32, 22)
		Me.txtBB_Period.TabIndex = 22
		'
		'txtBB_Year
		'
		Me.txtBB_Year.AcceptsReturn = True
		Me.txtBB_Year.BackColor = System.Drawing.SystemColors.Window
		Me.txtBB_Year.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtBB_Year.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtBB_Year.Location = New System.Drawing.Point(623, 576)
		Me.txtBB_Year.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtBB_Year.MaxLength = 4
		Me.txtBB_Year.Name = "txtBB_Year"
		Me.txtBB_Year.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtBB_Year.Size = New System.Drawing.Size(53, 22)
		Me.txtBB_Year.TabIndex = 21
		'
		'chkAddVoucherCounter
		'
		Me.chkAddVoucherCounter.BackColor = System.Drawing.SystemColors.Control
		Me.chkAddVoucherCounter.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkAddVoucherCounter.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkAddVoucherCounter.Location = New System.Drawing.Point(349, 465)
		Me.chkAddVoucherCounter.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.chkAddVoucherCounter.Name = "chkAddVoucherCounter"
		Me.chkAddVoucherCounter.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkAddVoucherCounter.Size = New System.Drawing.Size(507, 22)
		Me.chkAddVoucherCounter.TabIndex = 15
		Me.chkAddVoucherCounter.Text = "60505 - Add a counter to the vouchernumber for each payment"
		Me.chkAddVoucherCounter.UseVisualStyleBackColor = False
		'
		'chkAddOnOcr
		'
		Me.chkAddOnOcr.BackColor = System.Drawing.SystemColors.Control
		Me.chkAddOnOcr.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkAddOnOcr.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkAddOnOcr.Location = New System.Drawing.Point(349, 438)
		Me.chkAddOnOcr.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.chkAddOnOcr.Name = "chkAddOnOcr"
		Me.chkAddOnOcr.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkAddOnOcr.Size = New System.Drawing.Size(417, 22)
		Me.chkAddOnOcr.TabIndex = 14
		Me.chkAddOnOcr.Text = "60502 - Set voucherno also on OCR-payments"
		Me.chkAddOnOcr.UseVisualStyleBackColor = False
		'
		'optGetVNoBB
		'
		Me.optGetVNoBB.BackColor = System.Drawing.SystemColors.Window
		Me.optGetVNoBB.Cursor = System.Windows.Forms.Cursors.Default
		Me.optGetVNoBB.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optGetVNoBB.Location = New System.Drawing.Point(744, 411)
		Me.optGetVNoBB.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.optGetVNoBB.Name = "optGetVNoBB"
		Me.optGetVNoBB.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optGetVNoBB.Size = New System.Drawing.Size(215, 21)
		Me.optGetVNoBB.TabIndex = 13
		Me.optGetVNoBB.TabStop = True
		Me.optGetVNoBB.Text = "60434 - Get from BB"
		Me.optGetVNoBB.UseVisualStyleBackColor = False
		'
		'optGetVNoERP
		'
		Me.optGetVNoERP.BackColor = System.Drawing.SystemColors.Window
		Me.optGetVNoERP.Cursor = System.Windows.Forms.Cursors.Default
		Me.optGetVNoERP.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optGetVNoERP.Location = New System.Drawing.Point(541, 411)
		Me.optGetVNoERP.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.optGetVNoERP.Name = "optGetVNoERP"
		Me.optGetVNoERP.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optGetVNoERP.Size = New System.Drawing.Size(204, 21)
		Me.optGetVNoERP.TabIndex = 12
		Me.optGetVNoERP.TabStop = True
		Me.optGetVNoERP.Text = "60433 - Get from ERP"
		Me.optGetVNoERP.UseVisualStyleBackColor = False
		'
		'OptDontUse
		'
		Me.OptDontUse.BackColor = System.Drawing.SystemColors.Window
		Me.OptDontUse.Cursor = System.Windows.Forms.Cursors.Default
		Me.OptDontUse.ForeColor = System.Drawing.SystemColors.ControlText
		Me.OptDontUse.Location = New System.Drawing.Point(349, 411)
		Me.OptDontUse.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.OptDontUse.Name = "OptDontUse"
		Me.OptDontUse.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.OptDontUse.Size = New System.Drawing.Size(193, 21)
		Me.OptDontUse.TabIndex = 11
		Me.OptDontUse.TabStop = True
		Me.OptDontUse.Text = "60432 - Don't use"
		Me.OptDontUse.UseVisualStyleBackColor = False
		'
		'txtVoucherType2
		'
		Me.txtVoucherType2.AcceptsReturn = True
		Me.txtVoucherType2.BackColor = System.Drawing.SystemColors.Window
		Me.txtVoucherType2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtVoucherType2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtVoucherType2.Location = New System.Drawing.Point(824, 523)
		Me.txtVoucherType2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtVoucherType2.MaxLength = 255
		Me.txtVoucherType2.Name = "txtVoucherType2"
		Me.txtVoucherType2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtVoucherType2.Size = New System.Drawing.Size(148, 22)
		Me.txtVoucherType2.TabIndex = 19
		'
		'txtVoucherNo2
		'
		Me.txtVoucherNo2.AcceptsReturn = True
		Me.txtVoucherNo2.BackColor = System.Drawing.SystemColors.Window
		Me.txtVoucherNo2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtVoucherNo2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtVoucherNo2.Location = New System.Drawing.Point(499, 523)
		Me.txtVoucherNo2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtVoucherNo2.MaxLength = 255
		Me.txtVoucherNo2.Name = "txtVoucherNo2"
		Me.txtVoucherNo2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtVoucherNo2.Size = New System.Drawing.Size(177, 22)
		Me.txtVoucherNo2.TabIndex = 18
		'
		'txtVoucherNo
		'
		Me.txtVoucherNo.AcceptsReturn = True
		Me.txtVoucherNo.BackColor = System.Drawing.SystemColors.Window
		Me.txtVoucherNo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtVoucherNo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtVoucherNo.Location = New System.Drawing.Point(499, 492)
		Me.txtVoucherNo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtVoucherNo.MaxLength = 255
		Me.txtVoucherNo.Name = "txtVoucherNo"
		Me.txtVoucherNo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtVoucherNo.Size = New System.Drawing.Size(177, 22)
		Me.txtVoucherNo.TabIndex = 16
		'
		'txtDivision
		'
		Me.txtDivision.AcceptsReturn = True
		Me.txtDivision.BackColor = System.Drawing.SystemColors.Window
		Me.txtDivision.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDivision.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDivision.Location = New System.Drawing.Point(824, 108)
		Me.txtDivision.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtDivision.MaxLength = 50
		Me.txtDivision.Name = "txtDivision"
		Me.txtDivision.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDivision.Size = New System.Drawing.Size(148, 22)
		Me.txtDivision.TabIndex = 3
		'
		'txtClientName
		'
		Me.txtClientName.AcceptsReturn = True
		Me.txtClientName.BackColor = System.Drawing.SystemColors.Window
		Me.txtClientName.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtClientName.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtClientName.Location = New System.Drawing.Point(703, 78)
		Me.txtClientName.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtClientName.MaxLength = 50
		Me.txtClientName.Name = "txtClientName"
		Me.txtClientName.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtClientName.Size = New System.Drawing.Size(269, 22)
		Me.txtClientName.TabIndex = 1
		'
		'txtClientNo
		'
		Me.txtClientNo.AcceptsReturn = True
		Me.txtClientNo.BackColor = System.Drawing.SystemColors.Window
		Me.txtClientNo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtClientNo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtClientNo.Location = New System.Drawing.Point(499, 78)
		Me.txtClientNo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtClientNo.MaxLength = 35
		Me.txtClientNo.Name = "txtClientNo"
		Me.txtClientNo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtClientNo.Size = New System.Drawing.Size(96, 22)
		Me.txtClientNo.TabIndex = 0
		'
		'txtCompanyNo
		'
		Me.txtCompanyNo.AcceptsReturn = True
		Me.txtCompanyNo.BackColor = System.Drawing.SystemColors.Window
		Me.txtCompanyNo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCompanyNo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCompanyNo.Location = New System.Drawing.Point(499, 108)
		Me.txtCompanyNo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtCompanyNo.MaxLength = 50
		Me.txtCompanyNo.Name = "txtCompanyNo"
		Me.txtCompanyNo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCompanyNo.Size = New System.Drawing.Size(177, 22)
		Me.txtCompanyNo.TabIndex = 2
		'
		'CmdCancel
		'
		Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CmdCancel.Location = New System.Drawing.Point(787, 615)
		Me.CmdCancel.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.CmdCancel.Name = "CmdCancel"
		Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CmdCancel.Size = New System.Drawing.Size(97, 26)
		Me.CmdCancel.TabIndex = 24
		Me.CmdCancel.TabStop = False
		Me.CmdCancel.Text = "55002-&Avbryt"
		Me.CmdCancel.UseVisualStyleBackColor = False
		'
		'CmdOK
		'
		Me.CmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.CmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.CmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CmdOK.Location = New System.Drawing.Point(893, 615)
		Me.CmdOK.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.CmdOK.Name = "CmdOK"
		Me.CmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CmdOK.Size = New System.Drawing.Size(97, 26)
		Me.CmdOK.TabIndex = 25
		Me.CmdOK.Text = "&OK"
		Me.CmdOK.UseVisualStyleBackColor = False
		'
		'CmdHelp
		'
		Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
		Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
		Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CmdHelp.Location = New System.Drawing.Point(680, 615)
		Me.CmdHelp.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.CmdHelp.Name = "CmdHelp"
		Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CmdHelp.Size = New System.Drawing.Size(97, 26)
		Me.CmdHelp.TabIndex = 23
		Me.CmdHelp.TabStop = False
		Me.CmdHelp.Text = "55001 - &Hjelp"
		Me.CmdHelp.UseVisualStyleBackColor = False
		'
		'lblBB_Period
		'
		Me.lblBB_Period.BackColor = System.Drawing.SystemColors.Control
		Me.lblBB_Period.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblBB_Period.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblBB_Period.Location = New System.Drawing.Point(691, 576)
		Me.lblBB_Period.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblBB_Period.Name = "lblBB_Period"
		Me.lblBB_Period.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblBB_Period.Size = New System.Drawing.Size(229, 21)
		Me.lblBB_Period.TabIndex = 29
		Me.lblBB_Period.Text = "60553 - Periode"
		'
		'lblBB_Year
		'
		Me.lblBB_Year.BackColor = System.Drawing.SystemColors.Control
		Me.lblBB_Year.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblBB_Year.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblBB_Year.Location = New System.Drawing.Point(349, 576)
		Me.lblBB_Year.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblBB_Year.Name = "lblBB_Year"
		Me.lblBB_Year.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblBB_Year.Size = New System.Drawing.Size(256, 21)
		Me.lblBB_Year.TabIndex = 28
		Me.lblBB_Year.Text = "60552 - Regnskapsår"
		'
		'lblDescribeVoucherNo
		'
		Me.lblDescribeVoucherNo.BackColor = System.Drawing.SystemColors.Control
		Me.lblDescribeVoucherNo.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblDescribeVoucherNo.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblDescribeVoucherNo.Location = New System.Drawing.Point(349, 315)
		Me.lblDescribeVoucherNo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblDescribeVoucherNo.Name = "lblDescribeVoucherNo"
		Me.lblDescribeVoucherNo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblDescribeVoucherNo.Size = New System.Drawing.Size(620, 91)
		Me.lblDescribeVoucherNo.TabIndex = 27
		Me.lblDescribeVoucherNo.Text = "60435 - Describe the use of VoucherNos"
		'
		'lblVoucherType2
		'
		Me.lblVoucherType2.BackColor = System.Drawing.SystemColors.Control
		Me.lblVoucherType2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblVoucherType2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblVoucherType2.Location = New System.Drawing.Point(691, 526)
		Me.lblVoucherType2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblVoucherType2.Name = "lblVoucherType2"
		Me.lblVoucherType2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblVoucherType2.Size = New System.Drawing.Size(143, 22)
		Me.lblVoucherType2.TabIndex = 26
		Me.lblVoucherType2.Text = "60425-VoucherType"
		'
		'lblVoucherNo2
		'
		Me.lblVoucherNo2.BackColor = System.Drawing.SystemColors.Control
		Me.lblVoucherNo2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblVoucherNo2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblVoucherNo2.Location = New System.Drawing.Point(349, 526)
		Me.lblVoucherNo2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblVoucherNo2.Name = "lblVoucherNo2"
		Me.lblVoucherNo2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblVoucherNo2.Size = New System.Drawing.Size(143, 22)
		Me.lblVoucherNo2.TabIndex = 25
		Me.lblVoucherNo2.Text = "60428-Voucherno2"
		'
		'lblVoucherType
		'
		Me.lblVoucherType.BackColor = System.Drawing.SystemColors.Control
		Me.lblVoucherType.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblVoucherType.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblVoucherType.Location = New System.Drawing.Point(691, 554)
		Me.lblVoucherType.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblVoucherType.Name = "lblVoucherType"
		Me.lblVoucherType.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblVoucherType.Size = New System.Drawing.Size(143, 22)
		Me.lblVoucherType.TabIndex = 24
		Me.lblVoucherType.Text = "60425-Voucher Type"
		Me.lblVoucherType.Visible = False
		'
		'lblVoucherNo
		'
		Me.lblVoucherNo.BackColor = System.Drawing.SystemColors.Control
		Me.lblVoucherNo.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblVoucherNo.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblVoucherNo.Location = New System.Drawing.Point(349, 495)
		Me.lblVoucherNo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblVoucherNo.Name = "lblVoucherNo"
		Me.lblVoucherNo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblVoucherNo.Size = New System.Drawing.Size(143, 22)
		Me.lblVoucherNo.TabIndex = 23
		Me.lblVoucherNo.Text = "60354-Voucherno"
		'
		'lblDivision
		'
		Me.lblDivision.BackColor = System.Drawing.SystemColors.Control
		Me.lblDivision.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblDivision.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblDivision.Location = New System.Drawing.Point(693, 108)
		Me.lblDivision.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblDivision.Name = "lblDivision"
		Me.lblDivision.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblDivision.Size = New System.Drawing.Size(127, 23)
		Me.lblDivision.TabIndex = 22
		Me.lblDivision.Text = "60244 - Division"
		'
		'lblClientName
		'
		Me.lblClientName.BackColor = System.Drawing.SystemColors.Control
		Me.lblClientName.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblClientName.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblClientName.Location = New System.Drawing.Point(608, 81)
		Me.lblClientName.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblClientName.Name = "lblClientName"
		Me.lblClientName.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblClientName.Size = New System.Drawing.Size(84, 21)
		Me.lblClientName.TabIndex = 21
		Me.lblClientName.Text = "60031-Name"
		'
		'lblCompanyNo
		'
		Me.lblCompanyNo.BackColor = System.Drawing.SystemColors.Control
		Me.lblCompanyNo.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCompanyNo.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCompanyNo.Location = New System.Drawing.Point(349, 108)
		Me.lblCompanyNo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblCompanyNo.Name = "lblCompanyNo"
		Me.lblCompanyNo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCompanyNo.Size = New System.Drawing.Size(139, 23)
		Me.lblCompanyNo.TabIndex = 20
		Me.lblCompanyNo.Text = "60043 - Foretaksnr"
		'
		'lblClientNo
		'
		Me.lblClientNo.BackColor = System.Drawing.SystemColors.Control
		Me.lblClientNo.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblClientNo.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblClientNo.Location = New System.Drawing.Point(349, 81)
		Me.lblClientNo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblClientNo.Name = "lblClientNo"
		Me.lblClientNo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblClientNo.Size = New System.Drawing.Size(139, 21)
		Me.lblClientNo.TabIndex = 19
		Me.lblClientNo.Text = "60034 - Client No"
		'
		'lblHeading
		'
		Me.lblHeading.BackColor = System.Drawing.Color.Transparent
		Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblHeading.ImageAlign = System.Drawing.ContentAlignment.TopCenter
		Me.lblHeading.Location = New System.Drawing.Point(508, 33)
		Me.lblHeading.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblHeading.Name = "lblHeading"
		Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblHeading.Size = New System.Drawing.Size(200, 25)
		Me.lblHeading.TabIndex = 18
		Me.lblHeading.Text = "60336 - Client details"
		'
		'lblISO20022
		'
		Me.lblISO20022.AutoSize = True
		Me.lblISO20022.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblISO20022.Location = New System.Drawing.Point(349, 148)
		Me.lblISO20022.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblISO20022.Name = "lblISO20022"
		Me.lblISO20022.Size = New System.Drawing.Size(79, 17)
		Me.lblISO20022.TabIndex = 31
		Me.lblISO20022.Text = "ISO20022"
		'
		'lblMsgID
		'
		Me.lblMsgID.BackColor = System.Drawing.SystemColors.Control
		Me.lblMsgID.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblMsgID.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblMsgID.Location = New System.Drawing.Point(349, 175)
		Me.lblMsgID.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblMsgID.Name = "lblMsgID"
		Me.lblMsgID.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblMsgID.Size = New System.Drawing.Size(256, 23)
		Me.lblMsgID.TabIndex = 47
		Me.lblMsgID.Text = "60629 - Variable part of MessageID"
		'
		'txtMsgId_Variable
		'
		Me.txtMsgId_Variable.BackColor = System.Drawing.SystemColors.Window
		Me.txtMsgId_Variable.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMsgId_Variable.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMsgId_Variable.Location = New System.Drawing.Point(613, 171)
		Me.txtMsgId_Variable.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtMsgId_Variable.MaxLength = 5
		Me.txtMsgId_Variable.Name = "txtMsgId_Variable"
		Me.txtMsgId_Variable.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMsgId_Variable.Size = New System.Drawing.Size(63, 22)
		Me.txtMsgId_Variable.TabIndex = 4
		'
		'chkSEPASingle
		'
		Me.chkSEPASingle.BackColor = System.Drawing.SystemColors.Control
		Me.chkSEPASingle.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkSEPASingle.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkSEPASingle.Location = New System.Drawing.Point(693, 174)
		Me.chkSEPASingle.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.chkSEPASingle.Name = "chkSEPASingle"
		Me.chkSEPASingle.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkSEPASingle.Size = New System.Drawing.Size(268, 22)
		Me.chkSEPASingle.TabIndex = 8
		Me.chkSEPASingle.Text = "60630 - SEPA Singleposting"
		Me.chkSEPASingle.UseVisualStyleBackColor = False
		'
		'txtHistoryDeleteDays
		'
		Me.txtHistoryDeleteDays.AcceptsReturn = True
		Me.txtHistoryDeleteDays.BackColor = System.Drawing.SystemColors.Window
		Me.txtHistoryDeleteDays.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtHistoryDeleteDays.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtHistoryDeleteDays.Location = New System.Drawing.Point(613, 202)
		Me.txtHistoryDeleteDays.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtHistoryDeleteDays.MaxLength = 3
		Me.txtHistoryDeleteDays.Name = "txtHistoryDeleteDays"
		Me.txtHistoryDeleteDays.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtHistoryDeleteDays.Size = New System.Drawing.Size(63, 22)
		Me.txtHistoryDeleteDays.TabIndex = 5
		'
		'lblDeleteDays
		'
		Me.lblDeleteDays.BackColor = System.Drawing.SystemColors.Control
		Me.lblDeleteDays.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblDeleteDays.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblDeleteDays.Location = New System.Drawing.Point(349, 206)
		Me.lblDeleteDays.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblDeleteDays.Name = "lblDeleteDays"
		Me.lblDeleteDays.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblDeleteDays.Size = New System.Drawing.Size(256, 23)
		Me.lblDeleteDays.TabIndex = 50
		Me.lblDeleteDays.Text = "60631 - Delete in database after nn days"
		'
		'chkSavePaymentData
		'
		Me.chkSavePaymentData.BackColor = System.Drawing.SystemColors.Control
		Me.chkSavePaymentData.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkSavePaymentData.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkSavePaymentData.Location = New System.Drawing.Point(693, 204)
		Me.chkSavePaymentData.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.chkSavePaymentData.Name = "chkSavePaymentData"
		Me.chkSavePaymentData.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkSavePaymentData.Size = New System.Drawing.Size(268, 22)
		Me.chkSavePaymentData.TabIndex = 9
		Me.chkSavePaymentData.Text = "60632 - Save paymentdata"
		Me.chkSavePaymentData.UseVisualStyleBackColor = False
		'
		'lblValidationLevel
		'
		Me.lblValidationLevel.BackColor = System.Drawing.SystemColors.Control
		Me.lblValidationLevel.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblValidationLevel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblValidationLevel.Location = New System.Drawing.Point(349, 236)
		Me.lblValidationLevel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblValidationLevel.Name = "lblValidationLevel"
		Me.lblValidationLevel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblValidationLevel.Size = New System.Drawing.Size(256, 23)
		Me.lblValidationLevel.TabIndex = 53
		Me.lblValidationLevel.Text = "60633 - ValidationLevel"
		'
		'chkEndToEndRefFromBabelBank
		'
		Me.chkEndToEndRefFromBabelBank.BackColor = System.Drawing.SystemColors.Control
		Me.chkEndToEndRefFromBabelBank.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkEndToEndRefFromBabelBank.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkEndToEndRefFromBabelBank.Location = New System.Drawing.Point(693, 234)
		Me.chkEndToEndRefFromBabelBank.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.chkEndToEndRefFromBabelBank.Name = "chkEndToEndRefFromBabelBank"
		Me.chkEndToEndRefFromBabelBank.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkEndToEndRefFromBabelBank.Size = New System.Drawing.Size(295, 22)
		Me.chkEndToEndRefFromBabelBank.TabIndex = 10
		Me.chkEndToEndRefFromBabelBank.Text = "60634 - BabelBank creates EndToEndRef"
		Me.chkEndToEndRefFromBabelBank.UseVisualStyleBackColor = False
		'
		'lstValidationLevel
		'
		Me.lstValidationLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.lstValidationLevel.FormattingEnabled = True
		Me.lstValidationLevel.Location = New System.Drawing.Point(569, 234)
		Me.lstValidationLevel.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.lstValidationLevel.Name = "lstValidationLevel"
		Me.lstValidationLevel.Size = New System.Drawing.Size(105, 24)
		Me.lstValidationLevel.TabIndex = 6
		'
		'chkUseStateBankText
		'
		Me.chkUseStateBankText.BackColor = System.Drawing.SystemColors.Control
		Me.chkUseStateBankText.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkUseStateBankText.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkUseStateBankText.Location = New System.Drawing.Point(693, 145)
		Me.chkUseStateBankText.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.chkUseStateBankText.Name = "chkUseStateBankText"
		Me.chkUseStateBankText.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkUseStateBankText.Size = New System.Drawing.Size(268, 22)
		Me.chkUseStateBankText.TabIndex = 7
		Me.chkUseStateBankText.Text = "60644 - Standardtext to  the National Bank"
		Me.chkUseStateBankText.UseVisualStyleBackColor = False
		'
		'lblLine1
		'
		Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.lblLine1.Location = New System.Drawing.Point(347, 139)
		Me.lblLine1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblLine1.Name = "lblLine1"
		Me.lblLine1.Size = New System.Drawing.Size(633, 1)
		Me.lblLine1.TabIndex = 56
		Me.lblLine1.Text = "Label1"
		'
		'lblLine2
		'
		Me.lblLine2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.lblLine2.Location = New System.Drawing.Point(347, 303)
		Me.lblLine2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblLine2.Name = "lblLine2"
		Me.lblLine2.Size = New System.Drawing.Size(633, 1)
		Me.lblLine2.TabIndex = 57
		Me.lblLine2.Text = "Label1"
		'
		'lblLine3
		'
		Me.lblLine3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.lblLine3.Location = New System.Drawing.Point(13, 608)
		Me.lblLine3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblLine3.Name = "lblLine3"
		Me.lblLine3.Size = New System.Drawing.Size(973, 1)
		Me.lblLine3.TabIndex = 58
		Me.lblLine3.Text = "Label1"
		'
		'cmdAdvanced
		'
		Me.cmdAdvanced.BackColor = System.Drawing.SystemColors.Control
		Me.cmdAdvanced.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdAdvanced.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdAdvanced.Location = New System.Drawing.Point(693, 266)
		Me.cmdAdvanced.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.cmdAdvanced.Name = "cmdAdvanced"
		Me.cmdAdvanced.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdAdvanced.Size = New System.Drawing.Size(200, 26)
		Me.cmdAdvanced.TabIndex = 60
		Me.cmdAdvanced.TabStop = False
		Me.cmdAdvanced.Text = "55008 - &Advanced settings"
		Me.cmdAdvanced.UseVisualStyleBackColor = False
		'
		'txtVoucherType
		'
		Me.txtVoucherType.AcceptsReturn = True
		Me.txtVoucherType.BackColor = System.Drawing.SystemColors.Window
		Me.txtVoucherType.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtVoucherType.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtVoucherType.Location = New System.Drawing.Point(824, 551)
		Me.txtVoucherType.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtVoucherType.MaxLength = 255
		Me.txtVoucherType.Name = "txtVoucherType"
		Me.txtVoucherType.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtVoucherType.Size = New System.Drawing.Size(148, 22)
		Me.txtVoucherType.TabIndex = 20
		Me.txtVoucherType.Visible = False
		'
		'lblClientGroup
		'
		Me.lblClientGroup.BackColor = System.Drawing.SystemColors.Control
		Me.lblClientGroup.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblClientGroup.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblClientGroup.Location = New System.Drawing.Point(691, 495)
		Me.lblClientGroup.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.lblClientGroup.Name = "lblClientGroup"
		Me.lblClientGroup.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblClientGroup.Size = New System.Drawing.Size(143, 22)
		Me.lblClientGroup.TabIndex = 62
		Me.lblClientGroup.Text = "60598-ClientGroup"
		'
		'cmbClientGroup
		'
		Me.cmbClientGroup.FormattingEnabled = True
		Me.cmbClientGroup.ItemHeight = 16
		Me.cmbClientGroup.Location = New System.Drawing.Point(824, 492)
		Me.cmbClientGroup.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.cmbClientGroup.Name = "cmbClientGroup"
		Me.cmbClientGroup.Size = New System.Drawing.Size(148, 24)
		Me.cmbClientGroup.TabIndex = 63
		'
		'txtSocialSecurityNo
		'
		Me.txtSocialSecurityNo.BackColor = System.Drawing.SystemColors.Window
		Me.txtSocialSecurityNo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSocialSecurityNo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSocialSecurityNo.Location = New System.Drawing.Point(569, 266)
		Me.txtSocialSecurityNo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.txtSocialSecurityNo.MaxLength = 20
		Me.txtSocialSecurityNo.Name = "txtSocialSecurityNo"
		Me.txtSocialSecurityNo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSocialSecurityNo.Size = New System.Drawing.Size(107, 22)
		Me.txtSocialSecurityNo.TabIndex = 7
		'
		'Label1
		'
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Location = New System.Drawing.Point(349, 270)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.Size = New System.Drawing.Size(209, 23)
		Me.Label1.TabIndex = 65
		Me.Label1.Text = "60691 - Social security no."
		'
		'frmClientDetail
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ClientSize = New System.Drawing.Size(1004, 654)
		Me.Controls.Add(Me.txtSocialSecurityNo)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.cmbClientGroup)
		Me.Controls.Add(Me.lblClientGroup)
		Me.Controls.Add(Me.txtVoucherType)
		Me.Controls.Add(Me.cmdAdvanced)
		Me.Controls.Add(Me.lblLine3)
		Me.Controls.Add(Me.lblLine2)
		Me.Controls.Add(Me.lblLine1)
		Me.Controls.Add(Me.chkUseStateBankText)
		Me.Controls.Add(Me.lstValidationLevel)
		Me.Controls.Add(Me.chkEndToEndRefFromBabelBank)
		Me.Controls.Add(Me.lblValidationLevel)
		Me.Controls.Add(Me.chkSavePaymentData)
		Me.Controls.Add(Me.txtHistoryDeleteDays)
		Me.Controls.Add(Me.lblDeleteDays)
		Me.Controls.Add(Me.chkSEPASingle)
		Me.Controls.Add(Me.txtMsgId_Variable)
		Me.Controls.Add(Me.lblMsgID)
		Me.Controls.Add(Me.lblISO20022)
		Me.Controls.Add(Me.txtBB_Period)
		Me.Controls.Add(Me.txtBB_Year)
		Me.Controls.Add(Me.chkAddVoucherCounter)
		Me.Controls.Add(Me.chkAddOnOcr)
		Me.Controls.Add(Me.optGetVNoBB)
		Me.Controls.Add(Me.optGetVNoERP)
		Me.Controls.Add(Me.OptDontUse)
		Me.Controls.Add(Me.txtVoucherType2)
		Me.Controls.Add(Me.txtVoucherNo2)
		Me.Controls.Add(Me.txtVoucherNo)
		Me.Controls.Add(Me.txtDivision)
		Me.Controls.Add(Me.txtClientName)
		Me.Controls.Add(Me.txtClientNo)
		Me.Controls.Add(Me.txtCompanyNo)
		Me.Controls.Add(Me.CmdCancel)
		Me.Controls.Add(Me.CmdOK)
		Me.Controls.Add(Me.CmdHelp)
		Me.Controls.Add(Me.lblBB_Period)
		Me.Controls.Add(Me.lblBB_Year)
		Me.Controls.Add(Me.lblDescribeVoucherNo)
		Me.Controls.Add(Me.lblVoucherType2)
		Me.Controls.Add(Me.lblVoucherNo2)
		Me.Controls.Add(Me.lblVoucherType)
		Me.Controls.Add(Me.lblVoucherNo)
		Me.Controls.Add(Me.lblDivision)
		Me.Controls.Add(Me.lblClientName)
		Me.Controls.Add(Me.lblCompanyNo)
		Me.Controls.Add(Me.lblClientNo)
		Me.Controls.Add(Me.lblHeading)
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Location = New System.Drawing.Point(3, 22)
		Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmClientDetail"
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "60336 - Client details"
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents lblISO20022 As System.Windows.Forms.Label
    Public WithEvents lblMsgID As System.Windows.Forms.Label
    Public WithEvents txtMsgId_Variable As System.Windows.Forms.TextBox
    Public WithEvents chkSEPASingle As System.Windows.Forms.CheckBox
    Public WithEvents txtHistoryDeleteDays As System.Windows.Forms.TextBox
    Public WithEvents lblDeleteDays As System.Windows.Forms.Label
    Public WithEvents chkSavePaymentData As System.Windows.Forms.CheckBox
    Public WithEvents lblValidationLevel As System.Windows.Forms.Label
    Public WithEvents chkEndToEndRefFromBabelBank As System.Windows.Forms.CheckBox
    Friend WithEvents lstValidationLevel As System.Windows.Forms.ComboBox
    Public WithEvents chkUseStateBankText As System.Windows.Forms.CheckBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents lblLine2 As System.Windows.Forms.Label
    Friend WithEvents lblLine3 As System.Windows.Forms.Label
    Public WithEvents cmdAdvanced As System.Windows.Forms.Button
    Public WithEvents txtVoucherType As System.Windows.Forms.TextBox
    Public WithEvents lblClientGroup As System.Windows.Forms.Label
    Friend WithEvents cmbClientGroup As System.Windows.Forms.ComboBox
    Public WithEvents txtSocialSecurityNo As System.Windows.Forms.TextBox
    Public WithEvents Label1 As System.Windows.Forms.Label
#End Region
End Class
