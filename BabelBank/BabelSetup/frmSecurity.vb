﻿Imports System.Windows.Forms
Imports DidiSoft.Pgp
Public Class frmSecurity
    Dim oFS As vbBabel.FileSetup
    Dim bImportBankPublicKey As Boolean = False
    Dim bIsReturnProfile As Boolean = False
    Private Sub frmSecurity_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "")
        FormLRSCaptions(Me)
    End Sub
    Public Sub PassFilesetupTofrmSecurity(ByRef oPassedFileSetup As vbBabel.FileSetup)
        oFS = oPassedFileSetup
    End Sub
    Public Sub IsReturnProfile(ByRef bReturn As Boolean)
        bIsReturnProfile = bReturn
        If bIsReturnProfile Then
            Me.chkEncrypt.Text = LRS(60674)  ' Decrypt
            Me.chkSign.Text = LRS(60675)  ' Check signature
        End If
    End Sub
    Public Function ImportBankPublicKey() As Boolean
        ImportBankPublicKey = bImportBankPublicKey
    End Function
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        SavefrmSecurity(oFS)
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cmdFileOpenPublickey_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFileOpenPublickey.Click
        ' Hent filnavn
        Dim spath As String
        Dim sFile As String
        ' Keep filename after Browsing for folder;
        ' Must have \ and .
        If InStr(Me.txtfilenamePublicKey.Text, "\") > 0 And InStr(Me.txtfilenamePublicKey.Text, ".") > 0 Then
            sFile = Mid(Me.txtfilenamePublicKey.Text, InStrRev(Me.txtfilenamePublicKey.Text, "\"))
        ElseIf InStr(Me.txtfilenamePublicKey.Text, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & Me.txtfilenamePublicKey.Text
        Else
            sFile = ""
        End If

        spath = BrowseForFilesOrFolders(Me.txtfilenamePublicKey.Text, Me, LRS(60010), True)

        If Len(spath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(spath, ".") = 0 Then
                Me.txtfilenamePublicKey.Text = Replace(spath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                Me.txtfilenamePublicKey.Text = spath
            End If
        End If

    End Sub

    Private Sub cmdCreateKeys_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCreateKeys.Click
        ' Create private and public key pair for signing, and save to BabelBanks database
        Dim pgp As New PGPLib()
        Dim ks As KeyStore = New KeyStore("local.keystore", "VB00998776878787")  ' random
        Dim sUserID As String = ""
        Dim bOkToGenerate As Boolean = True
        Dim sPassword As String = ""
        Dim keySize As Integer = 2048
        'Dim keyAlgorithm As KeyAlgorithm = keyAlgorithm.RSA
        'Dim compressionTypes As CompressionAlgorithm() = GetCompressionAlgorithms()
        'Dim cypherTypes As CypherAlgorithm() = GetCiphers()
        'Dim hashTypes As HashAlgorithm() = GetHashes()
        Dim sPrivateKey As String = ""
        Dim sPublicKey As String = ""

        Try

            If Not EmptyString(oFS.secKeyCreationDate) Then
                ' Ask to be sure - it's important not to destroy the old keypair
                If Not MsgBox(LRS(60666), MsgBoxStyle.YesNo, "BabelBank") = MsgBoxResult.Yes Then
                    bOkToGenerate = False
                End If
                ' confirm with another question
                If Not MsgBox(LRS(60667, StringToDate(oFS.secKeyCreationDate)).ToString, MsgBoxStyle.YesNo, "BabelBank") = MsgBoxResult.Yes Then
                    bOkToGenerate = False
                End If
            End If

            If secCheckUserIDandPassword() Then
                bOkToGenerate = True
                sUserID = Me.txtUserid.Text
                sPassword = Me.txtPassword.Text
            Else
                bOkToGenerate = False
            End If

            If bOkToGenerate Then
                ' Check password against saved password.
                ' If no password - user must confim password
                If EmptyString(BBDecrypt(oFilesetup.secPassword, 8197)) Then
                    If Me.txtPasswordConfirm.Visible = False Then
                        Me.lblPasswordConfirm.Visible = True
                        Me.txtPasswordConfirm.Visible = True
                        'Me.txtPasswordConfirm.Focus()
                        bOkToGenerate = False
                    Else
                        ' have PasswordConfirm visible
                        If Me.txtPassword.Text <> Me.txtPasswordConfirm.Text Then
                            MsgBox(LRS(60671), MsgBoxStyle.Exclamation, "BabelBank")  '        "Passwords doesn't match
                            bOkToGenerate = False
                        End If

                    End If
                Else
                    If Me.txtPassword.Text <> BBDecrypt(oFilesetup.secPassword, 8197) Then
                        MsgBox(LRS(60671), MsgBoxStyle.Exclamation, "BabelBank")  '        "Passwords doesn't match
                        bOkToGenerate = False
                    End If
                End If

                If bOkToGenerate Then
                    ' Generate a key pair with no expiration date - let the bank take care for expirationdate
                    ' Generate RSA OpenPGP key
                    ks.GenerateRsaKeyPair(keySize, sUserID, sPassword)

                    ' save keypair in BabelBanks database
                    sPrivateKey = ks.ExportPrivateKeyAsString(sUserID)
                    oFS.secOurPrivateKey = sPrivateKey
                    sPublicKey = ks.ExportPublicKeyAsString(sUserID)
                    oFS.secOurPublicKey = sPublicKey
                    ' Save creation date
                    oFS.secKeyCreationDate = DateToString(Today.Date)
                    ' save userid and password for keys
                    oFS.secUserID = sUserID
                    oFS.secPassword = BBEncrypt(sPassword, 8197)

                    ' Export our public key?
                    If Not EmptyString(Me.txtfilenamePublicKey.Text) Then
                        ' If MsgBox("Will you like to export the public key, which must be sent to the bank?", MsgBoxStyle.YesNo, "BabelBank") = MsgBoxResult.Yes Then
                        If MsgBox(LRS(60662) & "." & vbCrLf & LRS(60665), MsgBoxStyle.YesNo, "BabelBank") = MsgBoxResult.Yes Then
                            ks.ExportPublicKey(Me.txtfilenamePublicKey.Text, sUserID, True)
                        End If
                    End If
                End If
            End If

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Function GetCiphers() As CypherAlgorithm()
        Dim cypherAlgorithms As New ArrayList()

        ' So far, supports AES_256 only
        cypherAlgorithms.Add(CypherAlgorithm.AES_256)

        Dim cypherTypes As CypherAlgorithm() = New CypherAlgorithm(cypherAlgorithms.Count - 1) {}
        For i As Integer = 0 To cypherAlgorithms.Count - 1
            cypherTypes(i) = DirectCast(cypherAlgorithms(i), CypherAlgorithm)
        Next
    End Function
    Private Function GetCompressionAlgorithms() As CompressionAlgorithm()
        Dim compressionAlgorithms As New ArrayList()
        ' So far, supports only UNcompressed 
        compressionAlgorithms.Add(CompressionAlgorithm.UNCOMPRESSED)

        Dim compressionTypes As CompressionAlgorithm() = New CompressionAlgorithm(compressionAlgorithms.Count - 1) {}
        For i As Integer = 0 To compressionAlgorithms.Count - 1
            compressionTypes(i) = DirectCast(compressionAlgorithms(i), CompressionAlgorithm)
        Next

        Return compressionTypes
    End Function
    Function GetHashes() As HashAlgorithm()
        Dim hashAlgorithms As New ArrayList()
        ' so far, supports SHA256 only
        If Me.cmbHash.SelectedText = Me.cmbHash.FindString("SHA256") Then
            hashAlgorithms.Add(HashAlgorithm.SHA256)
        End If
        Dim hashTypes As HashAlgorithm() = New HashAlgorithm(hashAlgorithms.Count - 1) {}
        For i As Integer = 0 To hashAlgorithms.Count - 1
            hashTypes(i) = DirectCast(hashAlgorithms(i), HashAlgorithm)
        Next

        Return hashTypes
    End Function

    Private Sub cmdfileOpenBankpublicKey_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdfileOpenBankpublicKey.Click
        ' Hent filnavn
        Dim spath As String
        Dim sFile As String
        ' Keep filename after Browsing for folder;
        ' Must have \ and .
        If InStr(Me.txtFilenameBankPublicKey.Text, "\") > 0 And InStr(Me.txtFilenameBankPublicKey.Text, ".") > 0 Then
            sFile = Mid(Me.txtFilenameBankPublicKey.Text, InStrRev(Me.txtFilenameBankPublicKey.Text, "\"))
        ElseIf InStr(Me.txtFilenameBankPublicKey.Text, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & Me.txtFilenameBankPublicKey.Text
        Else
            sFile = ""
        End If

        spath = BrowseForFilesOrFolders(Me.txtFilenameBankPublicKey.Text, Me, LRS(60010), True)

        If Len(spath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(spath, ".") = 0 Then
                Me.txtFilenameBankPublicKey.Text = Replace(spath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                Me.txtFilenameBankPublicKey.Text = spath
            End If
        End If

    End Sub
    Private Sub txtUserid_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtUserid.Validating
        If InStr(txtUserid.Text, "@") = 0 Or InStr(txtUserid.Text, ".") = 0 Or Len(txtUserid.Text) < 5 Then
            ' Cancel the event and select the text to be corrected by the user.
            e.Cancel = True
            txtUserid.Select(0, Len(Me.txtUserid.Text))
            MsgBox(LRS(60669), MsgBoxStyle.Exclamation, "BabelBank")
        End If
    End Sub


    Private Sub cmdImportbankPublicKey_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImportbankPublicKey.Click
        If secCheckUserIDandPassword() Then
            If Not EmptyString(Me.txtFilenameBankPublicKey.Text) Then
                ' Import banks public key to BabelBank?
                If MsgBox(LRS(60673) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1, "BabelBank") Then   ' Import banks public key to BabelBank?
                    bImportBankPublicKey = True
                Else
                    bImportBankPublicKey = False
                End If
            End If
            ' save password to filesetup, later to database
            oFS.secPassword = BBEncrypt(Me.txtPassword.Text, 8197)
        Else
            bImportBankPublicKey = False
        End If
    End Sub

    Private Sub chkEncrypt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEncrypt.Click
        ' 15.05.2019 - don't check password - we will need the option to activate Decrypt for returnfiles without any other settings

        'If Not secCheckUserIDandPassword() Then
        '    ' reset value. should not allow change without valid password
        '    If Me.chkEncrypt.Checked Then
        '        Me.chkEncrypt.Checked = False
        '    Else
        '        Me.chkEncrypt.Checked = True
        '    End If
        'End If
    End Sub
    Private Sub chkSign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSign.Click
        If Not secCheckUserIDandPassword() Then
            ' reset value. should not allow change without valid password
            If Me.chkSign.Checked Then
                Me.chkSign.Checked = False
            Else
                Me.chkSign.Checked = True
            End If
        End If
    End Sub
    Private Function secCheckUserIDandPassword() As Boolean
        Dim bOK As Boolean = True

        ' 25.10.2021 - do not need userId for SecureEnvelope - skip this test
        'If EmptyString(Me.txtUserid.Text) Then
        '    MsgBox(LRS(60661), MsgBoxStyle.Exclamation, "BabelBank")  '        "Please enter the user ID for key generation"
        '    bOK = False
        'End If
        If EmptyString(Me.txtPassword.Text) Then
            MsgBox(LRS(60668), MsgBoxStyle.Exclamation, "BabelBank")  '        "Please enter password for key generation"
            bOK = False
        End If

        If bOK Then
            ' Check password against saved password.
            ' If no password - user must confim password
            If EmptyString(BBDecrypt(oFilesetup.secPassword, 8197)) Then
                If Me.txtPasswordConfirm.Visible = False Then
                    Me.lblPasswordConfirm.Visible = True
                    Me.txtPasswordConfirm.Visible = True
                    bOK = False
                Else
                    ' have PasswordConfirm visible
                    If Me.txtPassword.Text <> Me.txtPasswordConfirm.Text Then
                        MsgBox(LRS(60671), MsgBoxStyle.Exclamation, "BabelBank")  '        "Passwords doesn't match
                        bOK = False
                    End If

                End If
            Else
                If Me.txtPassword.Text <> BBDecrypt(oFilesetup.secPassword, 8197) Then
                    MsgBox(LRS(60671), MsgBoxStyle.Exclamation, "BabelBank")  '        "Passwords doesn't match
                    bOK = False
                End If
            End If
        End If
        secCheckUserIDandPassword = bOK

    End Function

    Private Sub cmdExportPublicKey_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExportPublicKey.Click

        If secCheckUserIDandPassword() Then
            Dim ks As KeyStore = New KeyStore("local.keystore", "VB00998776878787")  ' random
            ks.ImportPublicKey(oFS.secOurPublicKey)

            ' Export our public key?
            If Not EmptyString(Me.txtfilenamePublicKey.Text) Then
                ' If MsgBox("Will you like to export the public key, which must be sent to the bank?", MsgBoxStyle.YesNo, "BabelBank") = MsgBoxResult.Yes Then
                If MsgBox(LRS(60665), MsgBoxStyle.YesNo, "BabelBank") = MsgBoxResult.Yes Then
                    ks.ExportPublicKey(Me.txtfilenamePublicKey.Text, oFS.secUserID, True)
                End If
            End If
        End If

    End Sub

End Class
