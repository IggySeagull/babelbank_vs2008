Option Strict Off
Option Explicit On
'Imports VB = Microsoft.VisualBasic
Friend Class frmClientAutoBook
    Inherits System.Windows.Forms.Form

    Private frmClientSettings As frmClientSettings

    Private bAnythingChanged As Boolean
    Private oMyDal As vbBabel.DAL = Nothing
    Private iAutoBook_ID As Short
    Private iAutoBookDetail_ID As Short
    Private iNextAutoBook_ID As Short
    Private iNextAutoBookDetail_ID As Short
    Private bDontBotherHandlingKeypress As Boolean
    Private iClient_ID As Integer

    ' 06.06.2019 - changed use of SetItemData and GetItemData
    Private listitem As New _MyListBoxItem

    Public Sub SetClient_ID(ByVal iC_Id As Integer)
        iClient_ID = iC_Id
    End Sub
    Private Function SaveAutoBook(Optional ByRef bAsk As Boolean = True) As Boolean
        Dim bYes As Boolean

        bYes = True

        If bAnythingChanged Then
            If bAsk Then
                bYes = MsgBox(LRS(60470) & " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes 'Save Autobook
            Else
                bYes = True
            End If
        End If ' bAnythingChanged?

        If bYes Then
            bAnythingChanged = False
        End If
        SaveAutoBook = bYes

    End Function
    Private Sub cmdAddEasyAutoBook_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAddEasyAutoBook.Click
        Dim sTmp As String
        Dim sText As String
        Dim sMySQL As String = ""

        ' Removed txtAutovalue 23.01.07, is no a part of AutoBook Detail
        If Len(Trim(Me.txtAutoName.Text)) > 0 And Len(Me.txtAutoToAccount.Text) > 0 Then
            ' Add AutoBook and content of textboxes to lstAutoBooks
            With Me

                ' Add one last AutoBook_ID
                iNextAutoBook_ID = iNextAutoBook_ID + 1

                ' Always add as index 0
                '.lstAutoBooks.Items.Add(IIf(.chkActive.CheckState = 1, "->", " ") & .txtAutoName.Text)
                'VB6.SetItemData(.lstAutoBooks, .lstAutoBooks.Items.Count - 1, iNextAutoBook_ID)
                ' 06.06.2019 - changed use of SetItemData and GetItemData
                'fill the listitem
                listitem = New _MyListBoxItem
                listitem.ItemString = IIf(.chkActive.CheckState = 1, "->", " ") & .txtAutoName.Text
                listitem.ItemData = iNextAutoBook_ID
                .lstAutoBooks.Items.Add(listitem)
                '------------ end 06.06.2019 changes -------------------------

                .txtAutoName.Focus()
                .cmdRemoveAutoBook.Enabled = True

                ' Add to recordset rsAutoBook
                '----------------------------
                sMySQL = "INSERT INTO Autobook (Company_ID, Client_ID, AutoBook_ID, Name, ToAccount, BookText, Type, Active, ProposeMatch, VATCode) VALUES("
                sMySQL = sMySQL & "1, " 'TODO: Hardcoded Company_ID
                sMySQL = sMySQL & iClient_ID.ToString & ", "
                sMySQL = sMySQL & iNextAutoBook_ID.ToString & ", '"
                sMySQL = sMySQL & .txtAutoName.Text & "', '"
                sMySQL = sMySQL & .txtAutoToAccount.Text & "', '"
                sMySQL = sMySQL & .txtAutoText.Text & "', "
                sMySQL = sMySQL & (.cmbAutoType.SelectedIndex + 1).ToString & ", "
                If CBool(.chkActive.CheckState) = True Then
                    sMySQL = sMySQL & "True, "
                Else
                    sMySQL = sMySQL & "False, "
                End If
                If CBool(.chkProposedMatch.CheckState) = True Then
                    sMySQL = sMySQL & "True, '"
                Else
                    sMySQL = sMySQL & "False, '"
                End If
                sMySQL = sMySQL & .txtVATCode.Text & "')"

                oMyDal.SQL = sMySQL
                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        'OK
                    Else
                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                .lstAutoBooks.SelectedIndex = .lstAutoBooks.Items.Count - 1 ' Position to last added line

                Me.lstAutoBookDetails.Items.Clear()
                sText = ""

                ' Fill up .lstAutoBookDetails with all criterias for current autobooking
                'KJELL - This code doesn't seem to be run because rsAutoBookDetail is empty when the button is clicked
                'Don't converted in .NET
                '**************** Start Omitted code **************
                'Do While Not rsAutoBookDetail.EOF
                '    Select Case rsAutoBookDetail.Fields("SelectOnField").Value
                '        Case "E_Account"
                '            sText = "Konto" '"E_Account"
                '        Case "E_Name"
                '            sText = "Navn" '"E_Name"
                '        Case "Freetext"
                '            sText = "Fritekst" '
                '        Case "REF_Bank1"
                '            sText = "Bankref." '
                '        Case "I_Account"
                '            sText = "Mottakerkonto" 'I_Account
                '        Case "MON_InvoiceAmount"
                '            sText = "Bel�p"
                '    End Select
                '    sText = sText & " = " & rsAutoBookDetail.Fields("ValueInField").Value
                '    Me.lstAutoBookDetails.Items.Add((sText))
                '    rsAutoBookDetail.MoveNext()
                'Loop
                '**************** End Omitted code **************

                ' set focus to first line
                Me.lstAutoBooks.SelectedIndex = Me.lstAutoBooks.Items.Count - 1


            End With
            bAnythingChanged = True
            Me.cmdAddEasyAutoBook.Enabled = False
        End If
        Me.cmdShowAutoBooks.Enabled = True
        Me.cmdAddDetail.Enabled = True
        Me.cmbAutoSelectField.Enabled = True
        Me.txtAutoValue.Enabled = True

        Me.txtAutoValue.Text = "" ' 17.11.2008, remove text from details when adding a new Autobook
        'Me.cmdAddDetail.Visible = True


    End Sub
    Private Sub cmdAddDetail_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAddDetail.Click
        Dim sText As String
        Dim sMySQL As String = ""
        Dim sCompanyID As String = "1" 'TODO: Hardcoded Company_ID

        If Len(Me.txtAutoValue.Text) > 0 Then
            ' Add AutoBook and content of textboxes to lstAutoBooks
            With Me

                ' Add one last AutoBookDetail_ID
                iNextAutoBookDetail_ID = iNextAutoBookDetail_ID + 1

                sText = "" 'This variabel is used to show the last added AutoBookDetail in the Form.
                ' Add to recordset rsAutoBookDetail
                '----------------------------------
                sMySQL = "INSERT INTO AutoBookDetail (Company_ID, Client_ID, AutoBook_ID, AutoBookDetail_ID, SelectOnField, ValueInField) VALUES("
                sMySQL = sMySQL & sCompanyID & ", "
                sMySQL = sMySQL & iClient_ID.ToString & ", "
                sMySQL = sMySQL & iAutoBook_ID.ToString & ", "
                sMySQL = sMySQL & iNextAutoBookDetail_ID.ToString & ", '"
                Select Case .cmbAutoSelectField.SelectedIndex
                    Case 0
                        sMySQL = sMySQL & "E_Account" & "', '"
                        sText = "Konto"
                    Case 1
                        sMySQL = sMySQL & "E_Name" & "', '"
                        sText = "Navn"
                    Case 2
                        sMySQL = sMySQL & "Freetext" & "', '"
                        sText = "Fritekst"
                    Case 3
                        sMySQL = sMySQL & "REF_Bank1" & "', '"
                        sText = "Bankref."
                    Case 4
                        sMySQL = sMySQL & "I_Account" & "', '"
                        sText = "Mottakerkonto"
                    Case 5
                        sMySQL = sMySQL & "MON_InvoiceAmount" & "', '"
                        sText = "Bel�p"
                    Case 6
                        sMySQL = sMySQL & "MON_InvoiceCurrency" & "', '"
                        sText = "Valuta"
                End Select
                sMySQL = sMySQL & .txtAutoValue.Text & "')"
                sText = sText & " = " & .txtAutoValue.Text

                oMyDal.SQL = sMySQL
                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        'OK
                    Else
                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                'Added 07.12.2011
                iAutoBookDetail_ID = iNextAutoBookDetail_ID 'The ID show in Autobookdetail

                ' Fill up .lstAutoBookDetails with all criterias for current autobooking

                'Me.lstAutoBookDetails.Items.Add((sText))
                'VB6.SetItemData(.lstAutoBookDetails, .lstAutoBookDetails.Items.Count - 1, iNextAutoBookDetail_ID)
                ' 06.06.2019 - changed use of SetItemData and GetItemData
                'fill the listitem
                listitem = New _MyListBoxItem
                listitem.ItemString = sText
                listitem.ItemData = iNextAutoBookDetail_ID
                .lstAutoBookDetails.Items.Add(listitem)

                Me.txtAutoValue.Text = "" ' 17.11.2008, remove text from details when adding a new Autobook

            End With
            bAnythingChanged = True
            Me.cmdAddDetail.Enabled = False
            'Me.cmbAutoSelectField.Enabled = False
            'Me.txtAutoValue.Enabled = False

        End If
    End Sub

    Private Sub cmdRemoveCriteria_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRemoveCriteria.Click
        Dim sMySQL As String
        Dim sCompanyID As String
        sCompanyID = "1"

        If Me.lstAutoBookDetails.Items.Count > 0 Then
            'iAutoBookDetail_ID = VB6.GetItemData(Me.lstAutoBookDetails, Me.lstAutoBookDetails.SelectedIndex)
            ' 06.06.2019 - changed use of SetItemData and GetItemData
            iAutoBookDetail_ID = Me.lstAutoBookDetails.Items(Me.lstAutoBookDetails.SelectedIndex).itemdata

            If lstAutoBookDetails.SelectedIndex > -1 Then
                ' Delete a AutoBookDetail from list
                'If MsgBox(LRS(60381) & " " & Trim(VB.Left(VB6.GetItemString(lstAutoBookDetails, lstAutoBookDetails.SelectedIndex), 100)) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett AutoBook ?
                ' 06.06.2019 - changed use of SetItemData and GetItemData
                If MsgBox(LRS(60381) & " " & Trim(Strings.Left(lstAutoBookDetails.Items(lstAutoBookDetails.SelectedIndex).itemstring, 100)) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett AutoBook ?
                    sMySQL = "DELETE FROM AutoBookDetail Where Company_ID = " & sCompanyID & " AND Client_ID = " & iClient_ID.ToString & " AND AutoBook_ID = " & iAutoBook_ID.ToString & " AND AutoBookDetail_ID = " & iAutoBookDetail_ID.ToString

                    oMyDal.SQL = sMySQL
                    If oMyDal.ExecuteNonQuery Then
                        If oMyDal.RecordsAffected > 0 Then
                            'OK
                        Else
                            Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                        End If
                    Else
                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                    End If

                    lstAutoBookDetails.Items.RemoveAt((lstAutoBookDetails.SelectedIndex))
                    bAnythingChanged = True

                    ' refill lstAutoBookDetails;
                    ' Find in table AutoBookDetail
                    With Me
                        dbGetAutoBookDetail(oMyDal, iClient_ID, iAutoBook_ID)
                        If oMyDal.Reader_HasRows Then
                            Do While oMyDal.Reader_ReadRecord
                                Select Case oMyDal.Reader_GetString("SelectOnField")
                                    Case "E_Account"
                                        .cmbAutoSelectField.SelectedIndex = 0
                                    Case "E_Name"
                                        .cmbAutoSelectField.SelectedIndex = 1
                                    Case "Freetext"
                                        .cmbAutoSelectField.SelectedIndex = 2
                                    Case "REF_Bank1"
                                        .cmbAutoSelectField.SelectedIndex = 3
                                    Case "I_Account"
                                        .cmbAutoSelectField.SelectedIndex = 4
                                    Case "MON_InvoiceAmount"
                                        .cmbAutoSelectField.SelectedIndex = 5
                                    Case "MON_InvoiceCurrency"
                                        .cmbAutoSelectField.SelectedIndex = 6
                                End Select
                                .txtAutoValue.Text = oMyDal.Reader_GetString("ValueInField")
                                Exit Do 'Show the first
                            Loop
                        Else
                            iAutoBookDetail_ID = 0
                        End If

                    End With
                End If

            End If
        End If
    End Sub

    Private Sub cmdUpdateAutoBook_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUpdateAutoBook.Click
        Dim sText As String
        Dim sSelectOnField As String = ""
        Dim sMySQL As String
        Dim sCompanyID As String = "1" 'TODO: Hardcoded CompanyID
        bAnythingChanged = True

        With Me
            Select Case .cmbAutoSelectField.SelectedIndex
                Case 0
                    sSelectOnField = "E_Account"
                    sText = "Konto" '"E_Account"
                Case 1
                    sSelectOnField = "E_Name"
                    sText = "Navn" '"E_Name"
                Case 2
                    sSelectOnField = "Freetext"
                    sText = "Fritekst" '
                Case 3
                    sSelectOnField = "REF_Bank1"
                    sText = "Bankref." '
                Case 4
                    sSelectOnField = "I_Account"
                    sText = "Mottakerkonto" 'I_Account
                Case 5
                    sSelectOnField = "MON_InvoiceAmount"
                    sText = "Bel�p"
                Case 6
                    sSelectOnField = "MON_InvoiceCurrency"
                    sText = "Valuta"
            End Select

            ' (04.09.2015) oppdater details kun hvis det er valgt noe her;
            If .txtAutoValue.Text <> "" And sSelectOnField <> "" Then
                sMySQL = "UPDATE AutoBookDetail SET SelectOnField = '" & sSelectOnField
                sMySQL = sMySQL & "', ValueInField = '" & .txtAutoValue.Text
                sMySQL = sMySQL & "' WHERE Company_ID = " & sCompanyID & " AND Client_ID = " & iClient_ID.ToString & " AND AutoBook_ID = " & iAutoBook_ID.ToString & " AND AutoBookDetail_ID = " & iAutoBookDetail_ID.ToString

                oMyDal.SQL = sMySQL
                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        'OK
                    Else
                        'Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                        ' 06.06.2019 - etter sletting av kriterie kan vi komme hit, uten � ha noe � oppdatere
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If
            End If

            sText = sText & " = " & .txtAutoValue.Text
            If Me.lstAutoBookDetails.SelectedIndex = -1 Then
                Me.lstAutoBookDetails.SelectedIndex = 0
            End If

            'VB6.SetItemString(Me.lstAutoBookDetails, Me.lstAutoBookDetails.SelectedIndex, sText)
            ' 06.06.2019 - changed use of SetItemData and GetItemData
            Me.lstAutoBookDetails.Items(Me.lstAutoBookDetails.SelectedIndex).itemstring = sText

            sMySQL = "UPDATE AutoBook SET Name = '" & .txtAutoName.Text
            sMySQL = sMySQL & "', ToAccount = '" & .txtAutoToAccount.Text
            sMySQL = sMySQL & "', BookText = '" & .txtAutoText.Text
            sMySQL = sMySQL & "', Type = " & .cmbAutoType.SelectedIndex + 1
            If CBool(.chkActive.CheckState) = True Then
                sMySQL = sMySQL & ", Active = True"
            Else
                sMySQL = sMySQL & ", Active = False"
            End If
            If CBool(.chkProposedMatch.CheckState) Then
                sMySQL = sMySQL & ", ProposeMatch = True"
            Else
                sMySQL = sMySQL & ", ProposeMatch = False"
            End If
            sMySQL = sMySQL & ", VATCode = '" & .txtVATCode.Text
            sMySQL = sMySQL & "' WHERE Company_ID = " & sCompanyID & " AND Client_ID = " & iClient_ID.ToString & " AND AutoBook_ID = " & iAutoBook_ID.ToString

            oMyDal.SQL = sMySQL
            If oMyDal.ExecuteNonQuery Then
                If oMyDal.RecordsAffected = 1 Then
                    'OK
                Else
                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If

            'sText = sText & " = " & .txtAutoValue.Text 'Once again?

            'VB6.SetItemString(.lstAutoBooks, .lstAutoBooks.SelectedIndex, IIf(.chkActive.CheckState = 1, "->", " ") & .txtAutoName.Text)
            ' 06.06.2019 replaced VB6.  functions
            .lstAutoBooks.Items(.lstAutoBooks.SelectedIndex).itemstring = IIf(.chkActive.CheckState = 1, "->", " ") & .txtAutoName.Text

        End With
    End Sub
    Private Sub lstAutoBooks_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstAutoBooks.DoubleClick
        cmdShowAutoBooks_Click(cmdShowAutoBooks, New System.EventArgs())
        Me.cmdShowAutoBooks.Enabled = True
    End Sub
    Private Sub lstAutoBooks_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstAutoBooks.SelectedIndexChanged
        cmdShowAutoBooks_Click(cmdShowAutoBooks, New System.EventArgs())
        Me.cmdShowAutoBooks.Enabled = True
    End Sub
    Private Sub lstAutoBookDetails_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstAutoBookDetails.DoubleClick
        cmdShowAutoBookDetails_Click()
        Me.cmdShowAutoBooks.Enabled = True
    End Sub
    Private Sub lstAutoBookDetails_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstAutoBookDetails.SelectedIndexChanged
        cmdShowAutoBookDetails_Click()
        Me.cmdShowAutoBooks.Enabled = True
    End Sub
    Private Sub txtAutoName_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAutoName.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.cmbAutoSelectField.Focus()
        End If
    End Sub
    Private Sub txtAutoName_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAutoName.TextChanged
        ' added 17.11.2008
        ' test if user has touched this textbox
        Me.cmdAddEasyAutoBook.Enabled = True

        'dbGetAutoBook(oMyDal, iClient_ID)
        'If oMyDal.Reader_HasRows Then
        'Me.cmdAddDetail.Enabled = True
        'End If
    End Sub
    Private Sub txtAutoValue_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAutoValue.TextChanged
        ' added 17.11.2008
        ' test if user has touched this textbox
        'dbGetAutoBook(oMyDal, iClient_ID)
        'If oMyDal.Reader_HasRows Then
        Me.cmdAddDetail.Enabled = True
        Me.cmdAddEasyAutoBook.Enabled = True

        'End If

    End Sub

    Private Sub cmbAutoSelectField_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles cmbAutoSelectField.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtAutoValue.Focus()
        End If
    End Sub
    Private Sub txtAutoValue_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAutoValue.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtAutoToAccount.Focus()
        End If
    End Sub
    Private Sub txtAutoToAccount_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAutoToAccount.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.cmbAutoType.Focus()
        End If
    End Sub
    Private Sub cmbAutoType_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles cmbAutoType.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtAutoText.Focus()
        End If
    End Sub
    'Private Sub txtAutoText_KeyUp(KeyCode As Integer, Shift As Integer)
    'If KeyCode = vbKeyReturn Then
    '    ' Return in txtAutoText acts as AddEasy
    '    cmdAddEasyAutoBook_Click
    'End If
    'End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        Dim bOKToEnd As Boolean

        bOKToEnd = True

        If bAnythingChanged Then
            ' Ask if we are going to save first!
            bOKToEnd = SaveAutoBook()
        End If

        ''If bOKToEnd Then
        '' Discard all changes
        'cnAutoBook.RollbackTrans()
        'cnAutoBook.Close()
        ''UPGRADE_NOTE: Object oDatabase may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        'oDatabase = Nothing

        Me.Close()
        'End If
    End Sub
    'Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        ' Save clientsettings to database
        '...
        '...
        If SaveAutoBook(False) Then ' Save previous client?
            'cnAutoBook.CommitTrans()
            'cnAutoBook.Close()
            ''UPGRADE_NOTE: Object oDatabase may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            'oDatabase = Nothing

            Me.Close()
        End If
    End Sub

    Private Sub cmdRemoveAutoBook_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRemoveAutoBook.Click
        Dim sMySQL As String
        Dim sCompanyID As String
        sCompanyID = "1" 'TODO: Hardcoded CompanyID

        'iAutoBook_ID = VB6.GetItemData(Me.lstAutoBooks, Me.lstAutoBooks.SelectedIndex)
        '06.06.2019 - changed use of SetItemData and GetItemData
        iAutoBook_ID = Me.lstAutoBooks.Items(Me.lstAutoBooks.SelectedIndex).itemdata

        If lstAutoBooks.SelectedIndex > -1 Then
            ' Delete a AutoBooks from list
            'If MsgBox(LRS(60381) & " " & Trim(VB.Left(VB6.GetItemString(lstAutoBooks, lstAutoBooks.SelectedIndex), 100)) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett AutoBook ?
            '06.06.2019 erstattet VB6.GetItemString
            If MsgBox(LRS(60381) & " " & Trim(Strings.Left(lstAutoBooks.Items(lstAutoBooks.SelectedIndex).itemstring, 100)) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett AutoBook ?
                sMySQL = "DELETE FROM AutoBook Where Company_ID = " & sCompanyID & " AND AutoBook_ID = " & iAutoBook_ID.ToString
                ' AutoBookDetail is related to AutoBook, then all detail records will be automatically deleted (I hope)

                oMyDal.SQL = sMySQL
                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        'OK
                    Else
                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                lstAutoBooks.Items.RemoveAt((lstAutoBooks.SelectedIndex))
                bAnythingChanged = True

                If lstAutoBooks.Items.Count > 0 Then
                    lstAutoBooks.SelectedIndex = 0
                End If

                ' Blank in form;
                With Me
                    .txtAutoName.Text = ""
                    .txtAutoValue.Text = ""
                    .txtAutoText.Text = ""
                    .txtAutoToAccount.Text = ""
                    .txtVATCode.Text = ""
                    .chkActive.CheckState = bVal(False)
                    .chkProposedMatch.CheckState = bVal(False)

                    .cmdRemoveAutoBook.Enabled = False

                    ' set focus to first line
                    If Me.lstAutoBookDetails.Items.Count > 0 Then
                        Me.lstAutoBookDetails.SelectedIndex = 0
                    End If
                End With
            End If

        End If
    End Sub

    Private Sub cmdShowAutoBooks_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdShowAutoBooks.Click
        Dim i As Short
        Dim sText As String
        Dim iRecordCounter As Integer = 0

        If Me.lstAutoBooks.SelectedIndex > -1 Then
            'iAutoBook_ID = VB6.GetItemData(Me.lstAutoBooks, Me.lstAutoBooks.SelectedIndex)
            '06.06.2019 - changed use of SetItemData and GetItemData
            iAutoBook_ID = Me.lstAutoBooks.Items(Me.lstAutoBooks.SelectedIndex).itemdata

            ' Find in recordset
            dbGetAutoBook(oMyDal, iClient_ID, iAutoBook_ID)

            If oMyDal.Reader_HasRows Then
                ' Fill txtBoxes
                With Me
                    Do While oMyDal.Reader_ReadRecord
                        .txtAutoName.Text = oMyDal.Reader_GetString("Name")
                        .txtAutoText.Text = oMyDal.Reader_GetString("BookText")
                        .txtAutoToAccount.Text = oMyDal.Reader_GetString("ToAccount")
                        .txtVATCode.Text = oMyDal.Reader_GetString("VATCode")
                        .cmbAutoType.SelectedIndex = CInt(oMyDal.Reader_GetString("Type")) - 1
                        .chkActive.CheckState = bVal(CBool(oMyDal.Reader_GetString("Active")))
                        .chkProposedMatch.CheckState = bVal(CBool(oMyDal.Reader_GetString("ProposeMatch")))

                        '.txtAutoName.SetFocus
                        .cmdRemoveAutoBook.Enabled = True

                        Exit Do 'Only one
                    Loop

                    ' Fill up .lstAutoBookDetails with all criterias for current autobooking
                    ' Find in table AutoBookDetail
                    dbGetAutoBookDetail(oMyDal, iClient_ID, iAutoBook_ID)
                    Me.lstAutoBookDetails.Items.Clear()
                    sText = ""
                    iRecordCounter = 0

                    If oMyDal.Reader_HasRows Then
                        Do While oMyDal.Reader_ReadRecord
                            iRecordCounter = iRecordCounter + 1
                            'If iRecordCounter = 1 Then
                            '    ' Find correct field
                            '    Select Case oMyDal.Reader_GetString("SelectOnField")
                            '        Case "E_Account"
                            '            .cmbAutoSelectField.SelectedIndex = 0
                            '        Case "E_Name"
                            '            .cmbAutoSelectField.SelectedIndex = 1
                            '        Case "Freetext"
                            '            .cmbAutoSelectField.SelectedIndex = 2
                            '        Case "REF_Bank1"
                            '            .cmbAutoSelectField.SelectedIndex = 3
                            '        Case "I_Account"
                            '            .cmbAutoSelectField.SelectedIndex = 4
                            '        Case "MON_InvoiceAmount"
                            '            .cmbAutoSelectField.SelectedIndex = 5
                            '    End Select
                            '    .txtAutoValue.Text = oMyDal.Reader_GetString("ValueInField")
                            '    iAutoBookDetail_ID = CShort(oMyDal.Reader_GetString("AutoBookDetail_ID"))
                            'End If
                            Select Case oMyDal.Reader_GetString("SelectOnField")
                                Case "E_Account"
                                    sText = "Konto" '"E_Account"
                                Case "E_Name"
                                    sText = "Navn" '"E_Name"
                                Case "Freetext"
                                    sText = "Fritekst" '
                                Case "REF_Bank1"
                                    sText = "Bankref." '
                                Case "I_Account"
                                    sText = "Mottakerkonto" 'I_Account
                                Case "MON_InvoiceAmount"
                                    sText = "Bel�p"
                                Case "MON_InvoiceCurrency"
                                    sText = "Valuta"
                            End Select
                            sText = sText & " = " & oMyDal.Reader_GetString("ValueInField")
                            'Me.lstAutoBookDetails.Items.Add((sText))
                            'VB6.SetItemData(Me.lstAutoBookDetails, Me.lstAutoBookDetails.Items.Count - 1, CInt(oMyDal.Reader_GetString("AutoBookDetail_ID")))
                            ' 06.06.2019 - changed use of SetItemData and GetItemData
                            listitem = New _MyListBoxItem
                            listitem.ItemString = sText
                            listitem.ItemData = CInt(oMyDal.Reader_GetString("AutoBookDetail_ID"))
                            Me.lstAutoBookDetails.Items.Add(listitem)

                            iNextAutoBookDetail_ID = CInt(oMyDal.Reader_GetString("AutoBookDetail_ID"))
                        Loop
                    Else
                        iAutoBookDetail_ID = 0
                    End If

                    .cmbAutoSelectField.SelectedIndex = -1
                    .txtAutoValue.Text = ""
                    .cmdAddDetail.Enabled = True
                    Me.cmbAutoSelectField.Enabled = True
                    Me.txtAutoValue.Enabled = True
                    If .cmbAutoType.SelectedIndex = 1 Then
                        Me.cmdDimensions.Enabled = True
                    Else
                        Me.cmdDimensions.Enabled = False
                    End If
                    ' Update listbox, in case name has changed
                    'VB6.SetItemString(.lstAutoBooks, .lstAutoBooks.SelectedIndex, IIf(.chkActive.CheckState = 1, "->", " ") & .txtAutoName.Text)
                    ' 06.06.2019 replaced VB6.  functions
                    .lstAutoBooks.Items(.lstAutoBooks.SelectedIndex).itemstring = IIf(.chkActive.CheckState = 1, "->", " ") & .txtAutoName.Text

                End With
            End If
        End If
    End Sub
    Private Sub cmdShowAutoBookDetails_Click()
        Dim i As Short

        If Me.lstAutoBookDetails.SelectedIndex > -1 Then
            ' 06.06.2019 - changed use of SetItemData and GetItemData
            'iAutoBookDetail_ID = VB6.GetItemData(Me.lstAutoBookDetails, Me.lstAutoBookDetails.SelectedIndex)
            iAutoBookDetail_ID = Me.lstAutoBookDetails.Items(Me.lstAutoBookDetails.SelectedIndex).itemdata

            ' Find in recordset
            dbGetAutoBookDetail(oMyDal, iClient_ID, iAutoBook_ID, iAutoBookDetail_ID)

            If oMyDal.Reader_HasRows Then
                ' Fill txtBoxes
                With Me

                    If oMyDal.Reader_ReadRecord Then
                        ' Find correct field
                        Select Case oMyDal.Reader_GetString("SelectOnField")
                            Case "E_Account"
                                .cmbAutoSelectField.SelectedIndex = 0
                            Case "E_Name"
                                .cmbAutoSelectField.SelectedIndex = 1
                            Case "Freetext"
                                .cmbAutoSelectField.SelectedIndex = 2
                            Case "REF_Bank1"
                                .cmbAutoSelectField.SelectedIndex = 3
                            Case "I_Account"
                                .cmbAutoSelectField.SelectedIndex = 4
                            Case "MON_InvoiceAmount"
                                .cmbAutoSelectField.SelectedIndex = 5
                            Case "MON_InvoiceCurrency"
                                .cmbAutoSelectField.SelectedIndex = 6
                        End Select

                        .txtAutoValue.Text = oMyDal.Reader_GetString("ValueInField")
                        '.txtAutoValue.SetFocus

                    End If
                End With
            End If
        End If

    End Sub
    Private Sub frmClientAutoBook_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim sText As String
        Dim iRecordCounter As Integer = 0

        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If

        FormvbStyle(Me, "", 400)
        FormLRSCaptions(Me)
        ' added 17.11.2008
        Me.cmdAddDetail.Enabled = False
        Me.cmbAutoSelectField.Enabled = False
        Me.txtAutoValue.Enabled = False
        Me.cmdDimensions.Enabled = False

        With Me

            ' Fill in fields;
            .cmbAutoSelectField.Items.Clear()
            .cmbAutoSelectField.Items.Add("Konto") '"E_Account"
            .cmbAutoSelectField.Items.Add("Navn") '"E_Name"
            .cmbAutoSelectField.Items.Add("Fritekst") '
            .cmbAutoSelectField.Items.Add("Bankref.") '
            .cmbAutoSelectField.Items.Add("Mottakerkonto") 'I_Account
            .cmbAutoSelectField.Items.Add("Bel�p")
            .cmbAutoSelectField.Items.Add("Valuta")
            .cmbAutoSelectField.SelectedIndex = 0

            ' Fill listbox for Add AutoBooks
            .cmbAutoType.Items.Clear()
            .cmbAutoType.Items.Add(LRS(60345)) '"Kundereskontro 1"
            .cmbAutoType.Items.Add(LRS(60344)) '"Hovedbokskonto 2"
            .cmbAutoType.Items.Add(LRS(60346)) '"KID 3"
            '.cmbAutoType.AddItem LRS(60347) '"Fra ERP 4" Not relevant here !
            .cmbAutoType.SelectedIndex = 0

            ' Fill lstAutoBooks
            iRecordCounter = 0
            iNextAutoBook_ID = 0 ' like before but should it be +1?
            Me.lstAutoBooks.Items.Clear()
            dbGetAutoBook(oMyDal, iClient_ID) ' Find autobookings for current client
            If oMyDal.Reader_HasRows Then
                Do While oMyDal.Reader_ReadRecord
                    iRecordCounter = iRecordCounter + 1
                    'If iRecordCounter = 1 Then
                    '    iAutoBook_ID = CInt(oMyDal.Reader_GetString("AutoBook_ID"))
                    '    .txtAutoName.Text = oMyDal.Reader_GetString("Name")
                    '    .txtAutoText.Text = oMyDal.Reader_GetString("BookText")
                    '    .txtVATCode.Text = oMyDal.Reader_GetString("VATCode")
                    '    .txtAutoToAccount.Text = oMyDal.Reader_GetString("ToAccount")
                    '    .cmbAutoType.SelectedIndex = CInt(oMyDal.Reader_GetString("Type")) - 1
                    'End If

                    '.lstAutoBooks.Items.Add(IIf(oMyDal.Reader_GetString("Active"), "->", " ") & oMyDal.Reader_GetString("Name"))
                    'VB6.SetItemData(.lstAutoBooks, .lstAutoBooks.Items.Count - 1, oMyDal.Reader_GetString("AutoBook_ID"))
                    ' 06.06.2019 - changed use of SetItemData and GetItemData
                    'fill the listitem
                    listitem = New _MyListBoxItem
                    listitem.ItemString = IIf(oMyDal.Reader_GetString("Active"), "->", " ") & oMyDal.Reader_GetString("Name")
                    listitem.ItemData = oMyDal.Reader_GetString("AutoBook_ID")
                    .lstAutoBooks.Items.Add(listitem)
                    'sText = .lstAutoBooks.Items(0).itemstring
                    'iAutoBook_ID = .lstAutoBooks.Items(0).itemdata
                    '------------ end 06.06.2019 changes -------------------------

                    If CInt(oMyDal.Reader_GetString("AutoBook_ID")) > iNextAutoBook_ID Then
                        iNextAutoBook_ID = CInt(oMyDal.Reader_GetString("AutoBook_ID")) 'This will give the same value as in VB6, but shoul it be added with one after the Loop?
                    End If
                Loop
                'Blank all information
                .txtAutoName.Text = ""
                .txtAutoText.Text = ""
                .txtVATCode.Text = ""
                .txtAutoToAccount.Text = ""
                .chkProposedMatch.Checked = False
                .chkActive.Checked = False
                ' Find in table AutoBookDetail
                Me.lstAutoBookDetails.Items.Clear()
                dbGetAutoBookDetail(oMyDal, iClient_ID, iAutoBook_ID)
                iRecordCounter = 0
                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord
                        ' Find correct field
                        iRecordCounter = iRecordCounter + 1
                        iNextAutoBookDetail_ID = CShort(oMyDal.Reader_GetString("AutoBookDetail_ID"))
                    Loop
                End If
            Else
                Me.lstAutoBookDetails.Items.Clear()
                iNextAutoBookDetail_ID = 0
            End If
            Me.cmbAutoSelectField.SelectedIndex = -1
            Me.txtAutoValue.Text = ""

        End With

        bAnythingChanged = False

        Me.cmdAddDetail.Enabled = False
        Me.cmbAutoSelectField.Enabled = False
        Me.txtAutoValue.Enabled = False

    End Sub
    Public Function AnythingChanged() As Boolean
        AnythingChanged = bAnythingChanged
    End Function
    Public Function ResetAnythingChanged()
        bAnythingChanged = False
    End Function
    'Added 18.02.2016
    Private Sub cmdDimensions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDimensions.Click

        Dim frmDimensions As New frmDimensions
        frmDimensions.SetAutoBookAccount_ID(iAutoBook_ID)
        frmDimensions.SetClient_ID(iClient_ID)
        frmDimensions.ShowDialog()
        frmDimensions.Close()
        frmDimensions = Nothing

    End Sub

    Private Sub cmbAutoType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAutoType.SelectedIndexChanged

        If Me.cmbAutoType.SelectedIndex = 1 Then
            Me.cmdDimensions.Enabled = True
        Else
            Me.cmdDimensions.Enabled = False
        End If

    End Sub
End Class
