<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDnB_TBIW_Charges
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents chkChargesFromSetup As System.Windows.Forms.CheckBox
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents chkChargeMeAbroad As System.Windows.Forms.CheckBox
	Public WithEvents chkChargeMeDomestic As System.Windows.Forms.CheckBox
    Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
    Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkChargesFromSetup = New System.Windows.Forms.CheckBox
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.chkChargeMeAbroad = New System.Windows.Forms.CheckBox
        Me.chkChargeMeDomestic = New System.Windows.Forms.CheckBox
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblPurposeCode = New System.Windows.Forms.Label
        Me.txtPurposeCode = New System.Windows.Forms.TextBox
        Me.lblLine1 = New System.Windows.Forms.Label
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkChargesFromSetup
        '
        Me.chkChargesFromSetup.BackColor = System.Drawing.SystemColors.Control
        Me.chkChargesFromSetup.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkChargesFromSetup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkChargesFromSetup.Location = New System.Drawing.Point(200, 72)
        Me.chkChargesFromSetup.Name = "chkChargesFromSetup"
        Me.chkChargesFromSetup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkChargesFromSetup.Size = New System.Drawing.Size(425, 41)
        Me.chkChargesFromSetup.TabIndex = 4
        Me.chkChargesFromSetup.Text = "60549 - This setup for charges will override possible chargesinfo found in import" & _
            "file"
        Me.chkChargesFromSetup.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(480, 232)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 3
        Me.CmdCancel.TabStop = False
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(556, 232)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'chkChargeMeAbroad
        '
        Me.chkChargeMeAbroad.BackColor = System.Drawing.SystemColors.Control
        Me.chkChargeMeAbroad.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkChargeMeAbroad.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkChargeMeAbroad.Location = New System.Drawing.Point(240, 144)
        Me.chkChargeMeAbroad.Name = "chkChargeMeAbroad"
        Me.chkChargeMeAbroad.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkChargeMeAbroad.Size = New System.Drawing.Size(354, 17)
        Me.chkChargeMeAbroad.TabIndex = 1
        Me.chkChargeMeAbroad.Text = "60517 - Charge me abroad"
        Me.chkChargeMeAbroad.UseVisualStyleBackColor = False
        '
        'chkChargeMeDomestic
        '
        Me.chkChargeMeDomestic.BackColor = System.Drawing.SystemColors.Control
        Me.chkChargeMeDomestic.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkChargeMeDomestic.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkChargeMeDomestic.Location = New System.Drawing.Point(240, 117)
        Me.chkChargeMeDomestic.Name = "chkChargeMeDomestic"
        Me.chkChargeMeDomestic.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkChargeMeDomestic.Size = New System.Drawing.Size(354, 17)
        Me.chkChargeMeDomestic.TabIndex = 0
        Me.chkChargeMeDomestic.Text = "60516 - Charge me domestic"
        Me.chkChargeMeDomestic.UseVisualStyleBackColor = False
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(0, 0)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(32, 56)
        Me._Image1_0.TabIndex = 5
        Me._Image1_0.TabStop = False
        '
        'lblPurposeCode
        '
        Me.lblPurposeCode.AutoSize = True
        Me.lblPurposeCode.Location = New System.Drawing.Point(202, 173)
        Me.lblPurposeCode.Name = "lblPurposeCode"
        Me.lblPurposeCode.Size = New System.Drawing.Size(154, 13)
        Me.lblPurposeCode.TabIndex = 7
        Me.lblPurposeCode.Text = "60625 - Standard purposecode"
        '
        'txtPurposeCode
        '
        Me.txtPurposeCode.Location = New System.Drawing.Point(378, 170)
        Me.txtPurposeCode.MaxLength = 4
        Me.txtPurposeCode.Name = "txtPurposeCode"
        Me.txtPurposeCode.Size = New System.Drawing.Size(59, 20)
        Me.txtPurposeCode.TabIndex = 8
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 224)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(625, 1)
        Me.lblLine1.TabIndex = 57
        Me.lblLine1.Text = "Label1"
        '
        'frmDnB_TBIW_Charges
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(644, 265)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtPurposeCode)
        Me.Controls.Add(Me.lblPurposeCode)
        Me.Controls.Add(Me.chkChargesFromSetup)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.chkChargeMeAbroad)
        Me.Controls.Add(Me.chkChargeMeDomestic)
        Me.Controls.Add(Me._Image1_0)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDnB_TBIW_Charges"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "55045 - Charges"
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblPurposeCode As System.Windows.Forms.Label
    Friend WithEvents txtPurposeCode As System.Windows.Forms.TextBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
