<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmWiz11_FilenameFromBank
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdFilter As System.Windows.Forms.Button
	Public WithEvents cmdSpecial As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents CmdFinish As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdNext As System.Windows.Forms.Button
	Public WithEvents CmdBack As System.Windows.Forms.Button
    Public WithEvents cmdFileOpen2 As System.Windows.Forms.Button
	Public WithEvents cmdFileOpen1 As System.Windows.Forms.Button
	Public WithEvents cmbBankname As System.Windows.Forms.ComboBox
	Public WithEvents cmdMatch As System.Windows.Forms.Button
	Public WithEvents txtFilename1 As System.Windows.Forms.TextBox
	Public WithEvents chkBackup As System.Windows.Forms.CheckBox
	Public WithEvents txtFilename2 As System.Windows.Forms.TextBox
    Public WithEvents lblBankname As System.Windows.Forms.Label
	Public WithEvents lblFilename1 As System.Windows.Forms.Label
	Public WithEvents lblFilename2 As System.Windows.Forms.Label
    Public WithEvents fraFromBank As System.Windows.Forms.GroupBox
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
    Public WithEvents lblHeading As System.Windows.Forms.Label
	Public WithEvents lblDescr As System.Windows.Forms.Label
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWiz11_FilenameFromBank))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdFilter = New System.Windows.Forms.Button
        Me.cmdSpecial = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.CmdFinish = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdNext = New System.Windows.Forms.Button
        Me.CmdBack = New System.Windows.Forms.Button
        Me.fraFromBank = New System.Windows.Forms.GroupBox
        Me.cmdFileOpen3 = New System.Windows.Forms.Button
        Me.cmdFileOpen2 = New System.Windows.Forms.Button
        Me.cmdFileOpen1 = New System.Windows.Forms.Button
        Me.cmbBankname = New System.Windows.Forms.ComboBox
        Me.cmdMatch = New System.Windows.Forms.Button
        Me.txtFilename1 = New System.Windows.Forms.TextBox
        Me.chkBackup = New System.Windows.Forms.CheckBox
        Me.txtFilename2 = New System.Windows.Forms.TextBox
        Me.txtFilename3 = New System.Windows.Forms.TextBox
        Me.lblBankname = New System.Windows.Forms.Label
        Me.lblFilename1 = New System.Windows.Forms.Label
        Me.lblFilename2 = New System.Windows.Forms.Label
        Me.lblFilename3 = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblDescr = New System.Windows.Forms.Label
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.cmdSecurity = New System.Windows.Forms.Button
        Me.fraFromBank.SuspendLayout()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdFilter
        '
        Me.cmdFilter.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFilter.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFilter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFilter.Location = New System.Drawing.Point(16, 269)
        Me.cmdFilter.Name = "cmdFilter"
        Me.cmdFilter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFilter.Size = New System.Drawing.Size(110, 21)
        Me.cmdFilter.TabIndex = 34
        Me.cmdFilter.Text = "&Filter"
        Me.cmdFilter.UseVisualStyleBackColor = False
        '
        'cmdSpecial
        '
        Me.cmdSpecial.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSpecial.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSpecial.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSpecial.Location = New System.Drawing.Point(16, 303)
        Me.cmdSpecial.Name = "cmdSpecial"
        Me.cmdSpecial.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSpecial.Size = New System.Drawing.Size(110, 21)
        Me.cmdSpecial.TabIndex = 27
        Me.cmdSpecial.Text = "&Spesial"
        Me.cmdSpecial.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(152, 303)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 3
        Me.CmdHelp.Text = "&Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'CmdFinish
        '
        Me.CmdFinish.BackColor = System.Drawing.SystemColors.Control
        Me.CmdFinish.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdFinish.Enabled = False
        Me.CmdFinish.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdFinish.Location = New System.Drawing.Point(530, 303)
        Me.CmdFinish.Name = "CmdFinish"
        Me.CmdFinish.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdFinish.Size = New System.Drawing.Size(73, 21)
        Me.CmdFinish.TabIndex = 7
        Me.CmdFinish.Text = "F&ullf�r"
        Me.CmdFinish.UseVisualStyleBackColor = False
        Me.CmdFinish.Visible = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(288, 303)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 4
        Me.CmdCancel.Text = "&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdNext
        '
        Me.CmdNext.BackColor = System.Drawing.SystemColors.Control
        Me.CmdNext.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdNext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdNext.Location = New System.Drawing.Point(439, 303)
        Me.CmdNext.Name = "CmdNext"
        Me.CmdNext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdNext.Size = New System.Drawing.Size(73, 21)
        Me.CmdNext.TabIndex = 6
        Me.CmdNext.Text = "&Neste >"
        Me.CmdNext.UseVisualStyleBackColor = False
        '
        'CmdBack
        '
        Me.CmdBack.BackColor = System.Drawing.SystemColors.Control
        Me.CmdBack.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdBack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdBack.Location = New System.Drawing.Point(365, 303)
        Me.CmdBack.Name = "CmdBack"
        Me.CmdBack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdBack.Size = New System.Drawing.Size(73, 21)
        Me.CmdBack.TabIndex = 5
        Me.CmdBack.Text = "< &Forrige"
        Me.CmdBack.UseVisualStyleBackColor = False
        '
        'fraFromBank
        '
        Me.fraFromBank.BackColor = System.Drawing.SystemColors.Window
        Me.fraFromBank.Controls.Add(Me.cmdFileOpen3)
        Me.fraFromBank.Controls.Add(Me.cmdFileOpen2)
        Me.fraFromBank.Controls.Add(Me.cmdFileOpen1)
        Me.fraFromBank.Controls.Add(Me.cmbBankname)
        Me.fraFromBank.Controls.Add(Me.cmdMatch)
        Me.fraFromBank.Controls.Add(Me.txtFilename1)
        Me.fraFromBank.Controls.Add(Me.chkBackup)
        Me.fraFromBank.Controls.Add(Me.txtFilename2)
        Me.fraFromBank.Controls.Add(Me.txtFilename3)
        Me.fraFromBank.Controls.Add(Me.lblBankname)
        Me.fraFromBank.Controls.Add(Me.lblFilename1)
        Me.fraFromBank.Controls.Add(Me.lblFilename2)
        Me.fraFromBank.Controls.Add(Me.lblFilename3)
        Me.fraFromBank.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraFromBank.Location = New System.Drawing.Point(184, 102)
        Me.fraFromBank.Name = "fraFromBank"
        Me.fraFromBank.Padding = New System.Windows.Forms.Padding(0)
        Me.fraFromBank.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraFromBank.Size = New System.Drawing.Size(420, 180)
        Me.fraFromBank.TabIndex = 15
        Me.fraFromBank.TabStop = False
        Me.fraFromBank.Text = "Filer fra bank"
        '
        'cmdFileOpen3
        '
        Me.cmdFileOpen3.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen3.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen3.Image = CType(resources.GetObject("cmdFileOpen3.Image"), System.Drawing.Image)
        Me.cmdFileOpen3.Location = New System.Drawing.Point(383, 88)
        Me.cmdFileOpen3.Name = "cmdFileOpen3"
        Me.cmdFileOpen3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen3.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen3.TabIndex = 33
        Me.cmdFileOpen3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen3.UseVisualStyleBackColor = False
        '
        'cmdFileOpen2
        '
        Me.cmdFileOpen2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen2.Image = CType(resources.GetObject("cmdFileOpen2.Image"), System.Drawing.Image)
        Me.cmdFileOpen2.Location = New System.Drawing.Point(383, 53)
        Me.cmdFileOpen2.Name = "cmdFileOpen2"
        Me.cmdFileOpen2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen2.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen2.TabIndex = 32
        Me.cmdFileOpen2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen2.UseVisualStyleBackColor = False
        '
        'cmdFileOpen1
        '
        Me.cmdFileOpen1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen1.Image = CType(resources.GetObject("cmdFileOpen1.Image"), System.Drawing.Image)
        Me.cmdFileOpen1.Location = New System.Drawing.Point(383, 19)
        Me.cmdFileOpen1.Name = "cmdFileOpen1"
        Me.cmdFileOpen1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen1.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen1.TabIndex = 31
        Me.cmdFileOpen1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen1.UseVisualStyleBackColor = False
        '
        'cmbBankname
        '
        Me.cmbBankname.BackColor = System.Drawing.SystemColors.Window
        Me.cmbBankname.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbBankname.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbBankname.Location = New System.Drawing.Point(104, 149)
        Me.cmbBankname.Name = "cmbBankname"
        Me.cmbBankname.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbBankname.Size = New System.Drawing.Size(273, 21)
        Me.cmbBankname.TabIndex = 29
        Me.cmbBankname.Text = "<bank>"
        '
        'cmdMatch
        '
        Me.cmdMatch.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMatch.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdMatch.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdMatch.Location = New System.Drawing.Point(306, 120)
        Me.cmdMatch.Name = "cmdMatch"
        Me.cmdMatch.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdMatch.Size = New System.Drawing.Size(105, 21)
        Me.cmdMatch.TabIndex = 28
        Me.cmdMatch.Text = "A&vansert oppsett"
        Me.cmdMatch.UseVisualStyleBackColor = False
        '
        'txtFilename1
        '
        Me.txtFilename1.AcceptsReturn = True
        Me.txtFilename1.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilename1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilename1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilename1.Location = New System.Drawing.Point(104, 20)
        Me.txtFilename1.MaxLength = 255
        Me.txtFilename1.Name = "txtFilename1"
        Me.txtFilename1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilename1.Size = New System.Drawing.Size(273, 20)
        Me.txtFilename1.TabIndex = 0
        '
        'chkBackup
        '
        Me.chkBackup.BackColor = System.Drawing.SystemColors.Window
        Me.chkBackup.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkBackup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkBackup.Location = New System.Drawing.Point(104, 124)
        Me.chkBackup.Name = "chkBackup"
        Me.chkBackup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkBackup.Size = New System.Drawing.Size(225, 18)
        Me.chkBackup.TabIndex = 14
        Me.chkBackup.Text = "Legg i backupkatalogen etter import"
        Me.chkBackup.UseVisualStyleBackColor = False
        '
        'txtFilename2
        '
        Me.txtFilename2.AcceptsReturn = True
        Me.txtFilename2.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilename2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilename2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilename2.Location = New System.Drawing.Point(104, 54)
        Me.txtFilename2.MaxLength = 255
        Me.txtFilename2.Name = "txtFilename2"
        Me.txtFilename2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilename2.Size = New System.Drawing.Size(273, 20)
        Me.txtFilename2.TabIndex = 1
        '
        'txtFilename3
        '
        Me.txtFilename3.AcceptsReturn = True
        Me.txtFilename3.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilename3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilename3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilename3.Location = New System.Drawing.Point(104, 90)
        Me.txtFilename3.MaxLength = 255
        Me.txtFilename3.Name = "txtFilename3"
        Me.txtFilename3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilename3.Size = New System.Drawing.Size(273, 20)
        Me.txtFilename3.TabIndex = 2
        '
        'lblBankname
        '
        Me.lblBankname.BackColor = System.Drawing.SystemColors.Window
        Me.lblBankname.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBankname.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBankname.Location = New System.Drawing.Point(10, 149)
        Me.lblBankname.Name = "lblBankname"
        Me.lblBankname.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBankname.Size = New System.Drawing.Size(95, 25)
        Me.lblBankname.TabIndex = 30
        Me.lblBankname.Text = "Banknavn"
        '
        'lblFilename1
        '
        Me.lblFilename1.BackColor = System.Drawing.SystemColors.Window
        Me.lblFilename1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilename1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilename1.Location = New System.Drawing.Point(9, 23)
        Me.lblFilename1.Name = "lblFilename1"
        Me.lblFilename1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilename1.Size = New System.Drawing.Size(87, 17)
        Me.lblFilename1.TabIndex = 24
        Me.lblFilename1.Text = "Filnavn 1"
        '
        'lblFilename2
        '
        Me.lblFilename2.BackColor = System.Drawing.SystemColors.Window
        Me.lblFilename2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilename2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilename2.Location = New System.Drawing.Point(9, 54)
        Me.lblFilename2.Name = "lblFilename2"
        Me.lblFilename2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilename2.Size = New System.Drawing.Size(87, 17)
        Me.lblFilename2.TabIndex = 23
        Me.lblFilename2.Text = "Filnavn 2"
        '
        'lblFilename3
        '
        Me.lblFilename3.BackColor = System.Drawing.SystemColors.Window
        Me.lblFilename3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilename3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilename3.Location = New System.Drawing.Point(9, 90)
        Me.lblFilename3.Name = "lblFilename3"
        Me.lblFilename3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilename3.Size = New System.Drawing.Size(87, 17)
        Me.lblFilename3.TabIndex = 22
        Me.lblFilename3.Text = "Filnavn 3"
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(151, 215)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(10, 16)
        Me._Image1_0.TabIndex = 35
        Me._Image1_0.TabStop = False
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(183, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(417, 17)
        Me.lblHeading.TabIndex = 26
        Me.lblHeading.Text = "Legg inn navn p� filer fra bank"
        '
        'lblDescr
        '
        Me.lblDescr.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescr.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescr.Location = New System.Drawing.Point(183, 51)
        Me.lblDescr.Name = "lblDescr"
        Me.lblDescr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescr.Size = New System.Drawing.Size(417, 48)
        Me.lblDescr.TabIndex = 25
        Me.lblDescr.Text = resources.GetString("lblDescr.Text")
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(13, 295)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(595, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'cmdSecurity
        '
        Me.cmdSecurity.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSecurity.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSecurity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSecurity.Location = New System.Drawing.Point(16, 243)
        Me.cmdSecurity.Name = "cmdSecurity"
        Me.cmdSecurity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSecurity.Size = New System.Drawing.Size(110, 21)
        Me.cmdSecurity.TabIndex = 82
        Me.cmdSecurity.Text = "55050-S&ikkerhet"
        Me.cmdSecurity.UseVisualStyleBackColor = False
        '
        'frmWiz11_FilenameFromBank
        '
        Me.AcceptButton = Me.CmdNext
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(620, 333)
        Me.Controls.Add(Me.cmdSecurity)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdFilter)
        Me.Controls.Add(Me.cmdSpecial)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.CmdFinish)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdNext)
        Me.Controls.Add(Me.CmdBack)
        Me.Controls.Add(Me.fraFromBank)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.lblDescr)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmWiz11_FilenameFromBank"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Filer fra bank"
        Me.fraFromBank.ResumeLayout(False)
        Me.fraFromBank.PerformLayout()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents cmdFileOpen3 As System.Windows.Forms.Button
    Public WithEvents txtFilename3 As System.Windows.Forms.TextBox
    Public WithEvents lblFilename3 As System.Windows.Forms.Label
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents cmdSecurity As System.Windows.Forms.Button
#End Region 
End Class
