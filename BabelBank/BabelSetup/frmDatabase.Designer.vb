<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDatabase
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtNewDatabase As System.Windows.Forms.TextBox
	Public WithEvents cmdUse As System.Windows.Forms.Button
	Public WithEvents cmbDatabase As System.Windows.Forms.ComboBox
	Public WithEvents cmdNew As System.Windows.Forms.Button
	Public WithEvents cmdTest As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents txtPassword As System.Windows.Forms.TextBox
	Public WithEvents txtUserID As System.Windows.Forms.TextBox
	Public WithEvents txtConnectionString As System.Windows.Forms.TextBox
	Public WithEvents lblDatabase As System.Windows.Forms.Label
    Public WithEvents lblTestConnection As System.Windows.Forms.Label
	Public WithEvents lblPassword As System.Windows.Forms.Label
	Public WithEvents lblUserID As System.Windows.Forms.Label
	Public WithEvents lblConnectionString As System.Windows.Forms.Label
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtNewDatabase = New System.Windows.Forms.TextBox
        Me.cmdUse = New System.Windows.Forms.Button
        Me.cmbDatabase = New System.Windows.Forms.ComboBox
        Me.cmdNew = New System.Windows.Forms.Button
        Me.cmdTest = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.txtUserID = New System.Windows.Forms.TextBox
        Me.txtConnectionString = New System.Windows.Forms.TextBox
        Me.lblDatabase = New System.Windows.Forms.Label
        Me.lblTestConnection = New System.Windows.Forms.Label
        Me.lblPassword = New System.Windows.Forms.Label
        Me.lblUserID = New System.Windows.Forms.Label
        Me.lblConnectionString = New System.Windows.Forms.Label
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblProvider = New System.Windows.Forms.Label
        Me.cmbProvider = New System.Windows.Forms.ComboBox
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.cmbDatabaseType = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtNewDatabase
        '
        Me.txtNewDatabase.AcceptsReturn = True
        Me.txtNewDatabase.BackColor = System.Drawing.SystemColors.Window
        Me.txtNewDatabase.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNewDatabase.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNewDatabase.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtNewDatabase.Location = New System.Drawing.Point(296, 32)
        Me.txtNewDatabase.MaxLength = 50
        Me.txtNewDatabase.Name = "txtNewDatabase"
        Me.txtNewDatabase.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNewDatabase.Size = New System.Drawing.Size(249, 20)
        Me.txtNewDatabase.TabIndex = 1
        Me.txtNewDatabase.TabStop = False
        Me.txtNewDatabase.Visible = False
        '
        'cmdUse
        '
        Me.cmdUse.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUse.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdUse.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdUse.Location = New System.Drawing.Point(560, 421)
        Me.cmdUse.Name = "cmdUse"
        Me.cmdUse.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdUse.Size = New System.Drawing.Size(81, 25)
        Me.cmdUse.TabIndex = 12
        Me.cmdUse.Text = "&Use"
        Me.cmdUse.UseVisualStyleBackColor = False
        '
        'cmbDatabase
        '
        Me.cmbDatabase.BackColor = System.Drawing.SystemColors.Window
        Me.cmbDatabase.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbDatabase.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbDatabase.Location = New System.Drawing.Point(296, 60)
        Me.cmbDatabase.Name = "cmbDatabase"
        Me.cmbDatabase.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbDatabase.Size = New System.Drawing.Size(249, 21)
        Me.cmbDatabase.TabIndex = 2
        '
        'cmdNew
        '
        Me.cmdNew.BackColor = System.Drawing.SystemColors.Control
        Me.cmdNew.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdNew.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNew.Location = New System.Drawing.Point(560, 59)
        Me.cmdNew.Name = "cmdNew"
        Me.cmdNew.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdNew.Size = New System.Drawing.Size(81, 25)
        Me.cmdNew.TabIndex = 21
        Me.cmdNew.Text = "55012 - New"
        Me.cmdNew.UseVisualStyleBackColor = False
        '
        'cmdTest
        '
        Me.cmdTest.BackColor = System.Drawing.SystemColors.Control
        Me.cmdTest.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdTest.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdTest.Location = New System.Drawing.Point(560, 220)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdTest.Size = New System.Drawing.Size(81, 25)
        Me.cmdTest.TabIndex = 22
        Me.cmdTest.Text = "60374 - Test"
        Me.cmdTest.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(387, 421)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 10
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(474, 421)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 11
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'txtPassword
        '
        Me.txtPassword.AcceptsReturn = True
        Me.txtPassword.BackColor = System.Drawing.SystemColors.Window
        Me.txtPassword.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPassword.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtPassword.Location = New System.Drawing.Point(296, 238)
        Me.txtPassword.MaxLength = 50
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPassword.Size = New System.Drawing.Size(185, 20)
        Me.txtPassword.TabIndex = 6
        '
        'txtUserID
        '
        Me.txtUserID.AcceptsReturn = True
        Me.txtUserID.BackColor = System.Drawing.SystemColors.Window
        Me.txtUserID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtUserID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtUserID.Location = New System.Drawing.Point(296, 210)
        Me.txtUserID.MaxLength = 50
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtUserID.Size = New System.Drawing.Size(185, 20)
        Me.txtUserID.TabIndex = 5
        '
        'txtConnectionString
        '
        Me.txtConnectionString.AcceptsReturn = True
        Me.txtConnectionString.BackColor = System.Drawing.SystemColors.Window
        Me.txtConnectionString.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtConnectionString.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtConnectionString.Location = New System.Drawing.Point(296, 144)
        Me.txtConnectionString.MaxLength = 255
        Me.txtConnectionString.Multiline = True
        Me.txtConnectionString.Name = "txtConnectionString"
        Me.txtConnectionString.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtConnectionString.Size = New System.Drawing.Size(345, 59)
        Me.txtConnectionString.TabIndex = 4
        '
        'lblDatabase
        '
        Me.lblDatabase.BackColor = System.Drawing.Color.Transparent
        Me.lblDatabase.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDatabase.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDatabase.Location = New System.Drawing.Point(182, 62)
        Me.lblDatabase.Name = "lblDatabase"
        Me.lblDatabase.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDatabase.Size = New System.Drawing.Size(137, 17)
        Me.lblDatabase.TabIndex = 13
        Me.lblDatabase.Text = "60370 - Name of connection:"
        '
        'lblTestConnection
        '
        Me.lblTestConnection.BackColor = System.Drawing.SystemColors.Control
        Me.lblTestConnection.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTestConnection.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblTestConnection.Location = New System.Drawing.Point(160, 275)
        Me.lblTestConnection.Name = "lblTestConnection"
        Me.lblTestConnection.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTestConnection.Size = New System.Drawing.Size(481, 129)
        Me.lblTestConnection.TabIndex = 12
        '
        'lblPassword
        '
        Me.lblPassword.BackColor = System.Drawing.Color.Transparent
        Me.lblPassword.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPassword.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPassword.Location = New System.Drawing.Point(182, 240)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPassword.Size = New System.Drawing.Size(129, 17)
        Me.lblPassword.TabIndex = 11
        Me.lblPassword.Text = "60373 - Password:"
        '
        'lblUserID
        '
        Me.lblUserID.BackColor = System.Drawing.Color.Transparent
        Me.lblUserID.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblUserID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblUserID.Location = New System.Drawing.Point(182, 212)
        Me.lblUserID.Name = "lblUserID"
        Me.lblUserID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblUserID.Size = New System.Drawing.Size(121, 17)
        Me.lblUserID.TabIndex = 10
        Me.lblUserID.Text = "60372 - User ID:"
        '
        'lblConnectionString
        '
        Me.lblConnectionString.BackColor = System.Drawing.Color.Transparent
        Me.lblConnectionString.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblConnectionString.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblConnectionString.Location = New System.Drawing.Point(182, 146)
        Me.lblConnectionString.Name = "lblConnectionString"
        Me.lblConnectionString.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblConnectionString.Size = New System.Drawing.Size(121, 17)
        Me.lblConnectionString.TabIndex = 9
        Me.lblConnectionString.Text = "60371 - Connectionstring:"
        '
        'lblProvider
        '
        Me.lblProvider.BackColor = System.Drawing.Color.Transparent
        Me.lblProvider.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProvider.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProvider.Location = New System.Drawing.Point(182, 90)
        Me.lblProvider.Name = "lblProvider"
        Me.lblProvider.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProvider.Size = New System.Drawing.Size(110, 17)
        Me.lblProvider.TabIndex = 16
        Me.lblProvider.Text = "Provider"
        '
        'cmbProvider
        '
        Me.cmbProvider.BackColor = System.Drawing.SystemColors.Window
        Me.cmbProvider.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbProvider.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbProvider.Location = New System.Drawing.Point(296, 88)
        Me.cmbProvider.Name = "cmbProvider"
        Me.cmbProvider.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbProvider.Size = New System.Drawing.Size(249, 21)
        Me.cmbProvider.TabIndex = 3
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 416)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(740, 1)
        Me.lblLine1.TabIndex = 57
        Me.lblLine1.Text = "Label1"
        '
        'cmbDatabaseType
        '
        Me.cmbDatabaseType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbDatabaseType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbDatabaseType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbDatabaseType.Location = New System.Drawing.Point(296, 116)
        Me.cmbDatabaseType.Name = "cmbDatabaseType"
        Me.cmbDatabaseType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbDatabaseType.Size = New System.Drawing.Size(249, 21)
        Me.cmbDatabaseType.TabIndex = 58
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(182, 118)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(110, 17)
        Me.Label1.TabIndex = 59
        Me.Label1.Text = "60162 - Database"
        '
        'frmDatabase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(757, 456)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbDatabaseType)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmbProvider)
        Me.Controls.Add(Me.lblProvider)
        Me.Controls.Add(Me.txtNewDatabase)
        Me.Controls.Add(Me.cmdUse)
        Me.Controls.Add(Me.cmbDatabase)
        Me.Controls.Add(Me.cmdNew)
        Me.Controls.Add(Me.cmdTest)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtUserID)
        Me.Controls.Add(Me.txtConnectionString)
        Me.Controls.Add(Me.lblDatabase)
        Me.Controls.Add(Me.lblTestConnection)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.lblUserID)
        Me.Controls.Add(Me.lblConnectionString)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmDatabase"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Database"
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents lblProvider As System.Windows.Forms.Label
    Public WithEvents cmbProvider As System.Windows.Forms.ComboBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents cmbDatabaseType As System.Windows.Forms.ComboBox
    Public WithEvents Label1 As System.Windows.Forms.Label
#End Region 
End Class
