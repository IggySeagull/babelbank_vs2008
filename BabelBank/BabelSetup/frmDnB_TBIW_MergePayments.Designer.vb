﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDNB_TBIW_MergePayments
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.optMergeCreditsOnly = New System.Windows.Forms.RadioButton
        Me.optMergeNoInfo = New System.Windows.Forms.RadioButton
        Me.chkSort = New System.Windows.Forms.CheckBox
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.otpMergeOptimal = New System.Windows.Forms.RadioButton
        Me.optMergeKeepInfo = New System.Windows.Forms.RadioButton
        Me.optDoNotMerge = New System.Windows.Forms.RadioButton
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(12, 208)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(535, 1)
        Me.lblLine1.TabIndex = 67
        Me.lblLine1.Text = "Label1"
        '
        'optMergeCreditsOnly
        '
        Me.optMergeCreditsOnly.BackColor = System.Drawing.Color.Transparent
        Me.optMergeCreditsOnly.Cursor = System.Windows.Forms.Cursors.Default
        Me.optMergeCreditsOnly.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optMergeCreditsOnly.Location = New System.Drawing.Point(219, 134)
        Me.optMergeCreditsOnly.Name = "optMergeCreditsOnly"
        Me.optMergeCreditsOnly.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optMergeCreditsOnly.Size = New System.Drawing.Size(324, 17)
        Me.optMergeCreditsOnly.TabIndex = 66
        Me.optMergeCreditsOnly.TabStop = True
        Me.optMergeCreditsOnly.Text = "60580 - Merge payments ONLY when creditnotes are involved"
        Me.optMergeCreditsOnly.UseVisualStyleBackColor = False
        '
        'optMergeNoInfo
        '
        Me.optMergeNoInfo.BackColor = System.Drawing.Color.Transparent
        Me.optMergeNoInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.optMergeNoInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optMergeNoInfo.Location = New System.Drawing.Point(219, 111)
        Me.optMergeNoInfo.Name = "optMergeNoInfo"
        Me.optMergeNoInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optMergeNoInfo.Size = New System.Drawing.Size(324, 17)
        Me.optMergeNoInfo.TabIndex = 64
        Me.optMergeNoInfo.TabStop = True
        Me.optMergeNoInfo.Text = "60551 - Merge payments, leave NO information"
        Me.optMergeNoInfo.UseVisualStyleBackColor = False
        '
        'chkSort
        '
        Me.chkSort.BackColor = System.Drawing.SystemColors.Control
        Me.chkSort.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkSort.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkSort.Location = New System.Drawing.Point(219, 161)
        Me.chkSort.Name = "chkSort"
        Me.chkSort.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkSort.Size = New System.Drawing.Size(273, 17)
        Me.chkSort.TabIndex = 63
        Me.chkSort.Text = "60546 - Sort before merge"
        Me.chkSort.UseVisualStyleBackColor = False
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(302, 178)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(32, 10)
        Me._Image1_0.TabIndex = 65
        Me._Image1_0.TabStop = False
        '
        'otpMergeOptimal
        '
        Me.otpMergeOptimal.BackColor = System.Drawing.Color.Transparent
        Me.otpMergeOptimal.Cursor = System.Windows.Forms.Cursors.Default
        Me.otpMergeOptimal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.otpMergeOptimal.Location = New System.Drawing.Point(219, 91)
        Me.otpMergeOptimal.Name = "otpMergeOptimal"
        Me.otpMergeOptimal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.otpMergeOptimal.Size = New System.Drawing.Size(324, 17)
        Me.otpMergeOptimal.TabIndex = 62
        Me.otpMergeOptimal.TabStop = True
        Me.otpMergeOptimal.Text = "60415 - Merge payments, regardless of informationloss"
        Me.otpMergeOptimal.UseVisualStyleBackColor = False
        '
        'optMergeKeepInfo
        '
        Me.optMergeKeepInfo.BackColor = System.Drawing.Color.Transparent
        Me.optMergeKeepInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.optMergeKeepInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optMergeKeepInfo.Location = New System.Drawing.Point(219, 70)
        Me.optMergeKeepInfo.Name = "optMergeKeepInfo"
        Me.optMergeKeepInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optMergeKeepInfo.Size = New System.Drawing.Size(324, 17)
        Me.optMergeKeepInfo.TabIndex = 61
        Me.optMergeKeepInfo.TabStop = True
        Me.optMergeKeepInfo.Text = "60414 - Merge payments, but keep all info to receiver"
        Me.optMergeKeepInfo.UseVisualStyleBackColor = False
        '
        'optDoNotMerge
        '
        Me.optDoNotMerge.BackColor = System.Drawing.Color.Transparent
        Me.optDoNotMerge.Cursor = System.Windows.Forms.Cursors.Default
        Me.optDoNotMerge.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optDoNotMerge.Location = New System.Drawing.Point(219, 49)
        Me.optDoNotMerge.Name = "optDoNotMerge"
        Me.optDoNotMerge.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optDoNotMerge.Size = New System.Drawing.Size(324, 17)
        Me.optDoNotMerge.TabIndex = 60
        Me.optDoNotMerge.TabStop = True
        Me.optDoNotMerge.Text = "60416 - Do not merge payments"
        Me.optDoNotMerge.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(397, 218)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 59
        Me.CmdCancel.TabStop = False
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(473, 218)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 58
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'frmDNB_TBIW_MergePayments
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(553, 248)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.optMergeCreditsOnly)
        Me.Controls.Add(Me.optMergeNoInfo)
        Me.Controls.Add(Me.chkSort)
        Me.Controls.Add(Me.otpMergeOptimal)
        Me.Controls.Add(Me.optMergeKeepInfo)
        Me.Controls.Add(Me.optDoNotMerge)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me._Image1_0)
        Me.Name = "frmDNB_TBIW_MergePayments"
        Me.Text = "Merge"
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents optMergeCreditsOnly As System.Windows.Forms.RadioButton
    Public WithEvents optMergeNoInfo As System.Windows.Forms.RadioButton
    Public WithEvents chkSort As System.Windows.Forms.CheckBox
    Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
    Public WithEvents otpMergeOptimal As System.Windows.Forms.RadioButton
    Public WithEvents optMergeKeepInfo As System.Windows.Forms.RadioButton
    Public WithEvents optDoNotMerge As System.Windows.Forms.RadioButton
    Public WithEvents CmdCancel As System.Windows.Forms.Button
    Public WithEvents cmdOK As System.Windows.Forms.Button
End Class
