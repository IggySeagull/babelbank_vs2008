Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmDatabase
	Inherits System.Windows.Forms.Form
	
	'Private oLocalProfile As vbbabel.Profile
	Private iButtonPressed As Short
    Private aDBProfileArray(,,) As String
	Private bFormStateVisible As Boolean
    Private iCMBDatabase_SelectedIndex As Integer = -1
    Dim bNewDBProfile As Boolean
    Dim lCompanyID As Integer


	'UPGRADE_WARNING: Event cmbDatabase.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub cmbDatabase_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbDatabase.SelectedIndexChanged
		
		bNewDBProfile = False
		
		If cmbDatabase.SelectedIndex > 0 And bFormStateVisible Then
			lblConnectionString.Visible = True
			lblUserID.Visible = True
			lblPassword.Visible = True
			txtPassword.Visible = True
			txtConnectionString.Visible = True
			txtUserID.Visible = True
            cmbProvider.Visible = True
            cmbDatabaseType.Visible = True
            cmdOK.Enabled = True
			cmdTest.Visible = True
			cmdUse.Enabled = True
            lblTestConnection.Visible = True
            cmbProvider.SelectedIndex = aDBProfileArray(cmbDatabase.SelectedIndex - 1, 9, 0)
            cmbDatabaseType.SelectedIndex = aDBProfileArray(cmbDatabase.SelectedIndex - 1, 10, 0)
            lblTestConnection.Text = ""
            txtConnectionString.Text = aDBProfileArray(cmbDatabase.SelectedIndex - 1, 0, 0)
            txtUserID.Text = aDBProfileArray(cmbDatabase.SelectedIndex - 1, 1, 0)
            txtPassword.SelectionStart = 0
            txtPassword.SelectionLength = Len(txtPassword.Text)
            '23.12.2009 - Commented next line to prevent showing the password
            'txtPassword.Text = aDBProfileArray(cmbDatabase.ListIndex - 1, 2, 0)
        Else
            If bFormStateVisible Then
                MsgBox("You must either choose an existing connection, or create a new connection.", MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation)
            Else
                bFormStateVisible = True
            End If
        End If
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        lblTestConnection.Text = ""
        iButtonPressed = -1
        Me.Hide()
    End Sub

    Private Sub cmdNew_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdNew.Click
        ' 12.04.2017
        cmbProvider.Visible = True
        cmbDatabaseType.Visible = True
        bNewDBProfile = True
        cmbDatabase.Visible = False
        cmbProvider.SelectedIndex = 0
        cmbDatabaseType.SelectedIndex = 0
        txtNewDatabase.Visible = True
        txtNewDatabase.Top = VB6.TwipsToPixelsY(960)
        txtConnectionString.Text = ""
        txtUserID.Text = ""
        txtPassword.Text = ""
        txtConnectionString.Visible = True
        lblConnectionString.Visible = True
        txtUserID.Visible = True
        lblUserID.Visible = True
        txtPassword.Visible = True
        lblPassword.Visible = True
        lblTestConnection.Text = ""
        lblTestConnection.Visible = True
        cmdUse.Enabled = True
        cmdOK.Enabled = True
        txtNewDatabase.Focus()


    End Sub

    Private Sub cmdUse_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUse.Click

        CheckForChangesAndSave(False)

        lblTestConnection.Text = ""

    End Sub


    Private Sub frmDatabase_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
            lblTestConnection.Text = ""
        End If
        eventArgs.Cancel = Cancel
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click

        CheckForChangesAndSave(False)

        lblTestConnection.Text = ""
        iButtonPressed = 1
        Me.Hide()

    End Sub
    Private Sub cmdTest_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdTest.Click
        ' Test connection

        Dim sErrorString As String
        Dim oMyTestDAL As vbBabel.DAL = Nothing
        Dim ConnectClient As New vbBabel.Conecto.BabelBankServiceClient

        Try

            lblTestConnection.Text = ""
            sErrorString = ""

            If Me.cmbProvider.SelectedIndex = 5 Then
                'Must check each specific connection, when it is WCF
                If InStr(Me.txtConnectionString.Text, "Conecto.no", CompareMethod.Text) Then

                    ' Increase binding max sizes
                    'If TypeOf ConnectClient.Endpoint.Binding Is ServiceModel.BasicHttpBinding Then
                    '    Dim binding As ServiceModel.BasicHttpBinding = _
                    '            CType(ConnectClient.Endpoint.Binding, ServiceModel.BasicHttpBinding)
                    '    Dim max As Integer = Integer.MaxValue ' around 5M size allowed  
                    '    binding.MaxReceivedMessageSize = max 'Increase binding max sizes
                    '    binding.MaxBufferPoolSize = max
                    '    binding.MaxBufferSize = max 'Increase binding max sizes
                    '    binding.ReaderQuotas.MaxStringContentLength = max 'Increase binding max sizes
                    '    binding.ReaderQuotas.MaxDepth = max 'Increase binding max sizes
                    '    binding.ReaderQuotas.MaxArrayLength = max 'Increase binding max sizes
                    '    binding.ReaderQuotas.MaxBytesPerRead = max 'Increase binding max sizes
                    '    binding.ReaderQuotas.MaxNameTableCharCount = max 'Increase binding max sizes
                    '    'binding. .MaxNameTableCharCount = max 'Increase binding max sizes
                    '    binding.SendTimeout = New TimeSpan(3, 0, 0, 0, 0) 'Increase the clientside timeout (BB will now wait 3 minutes before raising an error?
                    '    binding.ReceiveTimeout = New TimeSpan(3, 0, 0, 0, 0)
                    'End If

                    Try
                        'ConnectClient.ClientCredentials.UserName.UserName = Me.txtUserID.Text '"Admin"
                        'ConnectClient.ClientCredentials.UserName.Password = Me.txtPassword.Text '"Cranberry987"

                        'For Each operation As System.ServiceModel.Description.OperationDescription In ConnectClient.Endpoint.Contract.Operations
                        '    Dim operationBehavior As System.ServiceModel.Description.DataContractSerializerOperationBehavior = operation.Behaviors.Find(Of System.ServiceModel.Description.DataContractSerializerOperationBehavior)()
                        '    If operationBehavior Is Nothing Then
                        '        operationBehavior = New System.ServiceModel.Description.DataContractSerializerOperationBehavior(operation)
                        '        operation.Behaviors.Add(operationBehavior)
                        '    End If
                        '    'operationBehavior.DataContractResolver = New TypeResolver()
                        '    operationBehavior.MaxItemsInObjectGraph = Integer.MaxValue
                        'Next

                        'ConnectClient.Endpoint.Address = New System.ServiceModel.EndpointAddress(Me.txtConnectionString.Text)

                        'System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                        'If ConnectClient.IsAlive() Then
                        '    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                        '    lblTestConnection.Text = LRS(60619)  'Connectionstring OK
                        'Else
                        '    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                        'End If
                    Catch ex As Exception
                        lblTestConnection.Text = LRS(60620, ex.Message)

                    End Try

                Else
                    lblTestConnection.Text = LRS(60618)
                End If
            Else
                oMyTestDAL = New vbBabel.DAL
                oMyTestDAL.Provider = Me.cmbProvider.SelectedIndex
                oMyTestDAL.Connectionstring = Me.txtConnectionString.Text
                oMyTestDAL.UserID = Me.txtUserID.Text
                oMyTestDAL.Password = Me.txtPassword.Text
                'oMyTestDAL.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                If Not oMyTestDAL.ConnectToDB() Then
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                    lblTestConnection.Text = oMyTestDAL.ErrorMessage
                Else
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                    lblTestConnection.Text = LRS(60159) & " OK!" 'Connectionstring OK
                End If

                If Not oMyTestDAL Is Nothing Then
                    oMyTestDAL.Close()
                    oMyTestDAL = Nothing
                End If
            End If

        Catch ex As Exception
            lblTestConnection.Text = ex.Message

        End Try

    End Sub

    'Public Function SetLocalProfile(oObj As vbbabel.Profile, Optional bSave As Boolean = True) As Boolean
    '
    'On Error GoTo Errorsetlocalprofile
    '
    'bSaveCondata = bSave
    'Set oLocalProfile = oObj
    '
    'SetLocalProfile = True
    'Exit Function
    '
    'Errorsetlocalprofile:
    '
    'SetLocalProfile = False
    '
    '
    'End Function

    Public Function SetDBProfileArray(ByRef aArray(,,) As String) As Boolean

        aDBProfileArray = VB6.CopyArray(aArray)

    End Function

    Friend Function PressedButton() As Short

        PressedButton = iButtonPressed

    End Function

    Friend Function SetCompanyID(ByRef lID As Integer) As Boolean

        lCompanyID = lID

    End Function
    'Friend Function SaveConnection() As Boolean
    '
    'SaveConnection = bSaveData
    '
    'End Function
    Private Sub frmDatabase_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        FormvbStyle(Me, "dollar.jpg")
        FormLRSCaptions(Me)
        iButtonPressed = -1
        bNewDBProfile = False

    End Sub

    Private Sub frmDatabase_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        'Set oLocalProfile = Nothing

    End Sub



    Private Sub txtPassword_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPassword.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000

        If KeyCode = 13 Then
            Call cmdOK_Click(cmdOK, New System.EventArgs())
        End If

    End Sub

    Private Function CheckForChangesAndSave(ByRef bJustCheck As Boolean) As Boolean
        Dim oMyBBDal As vbBabel.DAL = Nothing
        Dim sErrorString As String
        Dim sMySQL As String
        Dim sConnString As String
        Dim sConnString1, sConnString2 As String
        Dim lRecordsAffected As Integer
        Dim bChangesDone As Boolean
        Dim bReturnValue As Boolean
        Dim lCounter As Integer
        Dim sEncryptedSpace As String
        Dim PauseTime, Start As Single

        On Error GoTo ErrorCheckForChangesAndSave

        bReturnValue = False
        sConnString1 = ""
        sConnString2 = ""

        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

        If bNewDBProfile Then
            bChangesDone = True
        Else
            bChangesDone = False
            If cmbDatabase.SelectedIndex = -1 Then 'Probably changed the name of the connection (then selectedindex will be -1)
                bChangesDone = True
            Else
                If txtConnectionString.Text <> aDBProfileArray(cmbDatabase.SelectedIndex - 1, 0, 0) Then
                    bChangesDone = True
                End If
                If cmbProvider.SelectedIndex <> aDBProfileArray(cmbDatabase.SelectedIndex - 1, 9, 0) Then
                    bChangesDone = True
                End If
                If cmbDatabaseType.SelectedIndex <> aDBProfileArray(cmbDatabase.SelectedIndex - 1, 10, 0) Then
                    bChangesDone = True
                End If
                If txtUserID.Text <> aDBProfileArray(cmbDatabase.SelectedIndex - 1, 1, 0) Then
                    bChangesDone = True
                End If
                If txtPassword.Text <> aDBProfileArray(cmbDatabase.SelectedIndex - 1, 2, 0) Then
                    bChangesDone = True
                End If
            End If
        End If

        If Not bJustCheck Then
            If bChangesDone Then
                oMyBBDal = New vbBabel.DAL
                oMyBBDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyBBDal.ConnectToDB() Then
                    Throw New System.Exception(oMyBBDal.ErrorMessage)
                End If

                If Len(txtConnectionString.Text) = 0 Then
                    Err.Raise(999999, "frmDatabase", "You must enter a connectionstring.")
                End If

                sConnString = txtConnectionString.Text

                If Len(sConnString) > 255 Then
                    If Len(sConnString) > 510 Then
                        MsgBox("The Connectionstring is too long.", MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation)
                    Else
                        sConnString1 = VB.Left(sConnString, 255)
                        sConnString2 = Mid(sConnString, 256)
                    End If
                Else
                    sConnString1 = sConnString
                    sConnString2 = ""
                End If

                If bNewDBProfile Then
                    If Len(txtNewDatabase.Text) = 0 Then
                        Err.Raise(999999, "frmDatabase", "You must give the connection a name.")
                    End If
                    sMySQL = "INSERT INTO DB_Profile(Company_ID, DBProfile_ID, Name, Constring1, "
                    sMySQL = sMySQL & "Constring2, UID, PWD, ProviderType, DatabaseType) "
                    If Array_IsEmpty(aDBProfileArray) Then
                        sMySQL = sMySQL & "VALUES(" & lCompanyID.ToString & ", 1"
                    Else
                        sMySQL = sMySQL & "VALUES(" & lCompanyID.ToString & ", " & (UBound(aDBProfileArray, 1) + 2).ToString
                    End If
                    sMySQL = sMySQL & ", '" & txtNewDatabase.Text
                    sMySQL = sMySQL & "', '" & sConnString1 & "', '" & sConnString2
                    sMySQL = sMySQL & "', '" & txtUserID.Text & "', '" & BBEncrypt((txtPassword.Text), 3)
                    sMySQL = sMySQL & "', " & cmbProvider.SelectedIndex.ToString
                    sMySQL = sMySQL & ", " & cmbDatabaseType.SelectedIndex.ToString & ")"
                Else
                    If cmbDatabase.SelectedIndex = -1 Then
                        sMySQL = "UPDATE DB_Profile SET Name = '" & cmbDatabase.Text
                    Else
                        sMySQL = "UPDATE DB_Profile SET Name = '" & VB6.GetItemString(cmbDatabase, cmbDatabase.SelectedIndex)
                    End If
                    sMySQL = sMySQL & "', ConString1 = '" & sConnString1
                    sMySQL = sMySQL & "', ConString2 = '" & sConnString2
                    sMySQL = sMySQL & "', UID = '" & txtUserID.Text
                    sMySQL = sMySQL & "', PWD = '" & BBEncrypt((txtPassword.Text), 3)
                    sMySQL = sMySQL & "', ProviderType = " & cmbProvider.SelectedIndex.ToString
                    sMySQL = sMySQL & ", DatabaseType = " & cmbDatabaseType.SelectedIndex.ToString
                    sMySQL = sMySQL & " WHERE Company_ID = " & lCompanyID.ToString
                    If cmbDatabase.SelectedIndex = -1 Then
                        sMySQL = sMySQL & " AND DBProfile_ID = " & iCMBDatabase_SelectedIndex.ToString
                    Else
                        sMySQL = sMySQL & " AND DBProfile_ID = " & cmbDatabase.SelectedIndex.ToString
                    End If

                End If

                oMyBBDal.SQL = sMySQL
                If oMyBBDal.ExecuteNonQuery Then
                    lRecordsAffected = oMyBBDal.RecordsAffected
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyBBDal.ErrorMessage)
                End If

                System.Windows.Forms.Application.DoEvents()

                PauseTime = 1 ' Set duration.
                Start = VB.Timer() ' Set start time.
                Do While VB.Timer() < Start + PauseTime
                    System.Windows.Forms.Application.DoEvents() ' Yield to other processes.
                Loop
                If lRecordsAffected < 1 Then
                    Err.Raise(999999, "frmDatabase", "No records updated" & vbCrLf & vbCrLf & oMyBBDal.ErrorMessage)
                Else
                    If bNewDBProfile Then
                        'Add 6 empty-SQL's for manual matching.
                        sEncryptedSpace = BBEncrypt(" ", 3)
                        For lCounter = 1 To 6
                            sMySQL = "INSERT INTO ERPQuery(Company_ID, DBProfile_ID, ERPQuery_ID, Name, Type, "
                            sMySQL = sMySQL & "DescGroup, Qorder, MatchType, SpecialMatch, Query1, Query2, Query3, Query4, Query5, Query6, Query7, Query8, Query9, Query10, Query11, Query12, Query13)"
                            If Array_IsEmpty(aDBProfileArray) Then
                                sMySQL = sMySQL & " VALUES(" & lCompanyID.ToString & ", 1"
                            Else
                                sMySQL = sMySQL & " VALUES(" & lCompanyID.ToString & ", " & (UBound(aDBProfileArray, 1) + 2).ToString
                            End If
                            sMySQL = sMySQL & ", " & lCounter.ToString
                            sMySQL = sMySQL & ", '" & "Not in use', 1, 0, " & lCounter.ToString & ", 2"
                            sMySQL = sMySQL & ", '" & sEncryptedSpace & "', '" & sEncryptedSpace & "', '" & sEncryptedSpace & "', '" & sEncryptedSpace & "', '" & sEncryptedSpace & "', '" & sEncryptedSpace & "', '" & sEncryptedSpace & "', '" & sEncryptedSpace & "', '" & sEncryptedSpace & "', '" & sEncryptedSpace & "', '" & sEncryptedSpace & "', '" & sEncryptedSpace & "', '" & sEncryptedSpace & "', '" & sEncryptedSpace & "')"

                            oMyBBDal.SQL = sMySQL
                            If oMyBBDal.ExecuteNonQuery Then
                                If oMyBBDal.RecordsAffected > 0 Then
                                    bReturnValue = True
                                Else
                                    Err.Raise(999999, "frmDatabase", "Unable to add a new record for manual matching." & vbCrLf & vbCrLf & oMyBBDal.ErrorMessage)
                                End If
                            Else
                                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyBBDal.ErrorMessage)
                            End If

                            System.Windows.Forms.Application.DoEvents()
                            PauseTime = 1 ' Set duration.
                            Start = VB.Timer() ' Set start time.
                            Do While VB.Timer() < Start + PauseTime
                                System.Windows.Forms.Application.DoEvents() ' Yield to other processes.
                            Loop

                        Next lCounter
                    End If
                    aDBProfileArray = GetERPDBInfo(lCompanyID, 1)
                    cmbDatabase.Items.Clear()
                    cmbDatabase.Items.Insert(0, "Choose a Connection")
                    For lCounter = 0 To UBound(aDBProfileArray, 1)
                        cmbDatabase.Items.Insert(lCounter + 1, aDBProfileArray(lCounter, 4, 0))
                    Next lCounter
                    If bNewDBProfile Then
                        bNewDBProfile = False
                        txtNewDatabase.Visible = False
                        cmbDatabase.Visible = True
                    End If
                    cmbDatabase.SelectedIndex = lCounter
                    bReturnValue = True
                End If
            Else
                bReturnValue = False
            End If
        Else
            bReturnValue = bChangesDone
        End If

        If Not oMyBBDal Is Nothing Then
            oMyBBDal.Close()
            oMyBBDal = Nothing
        End If

        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

        CheckForChangesAndSave = bReturnValue

        Exit Function

ErrorCheckForChangesAndSave:

        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

        If Not oMyBBDal Is Nothing Then
            oMyBBDal.Close()
            oMyBBDal = Nothing
        End If
        MsgBox(Err.Description, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Can't save the connection.")

        CheckForChangesAndSave = False

    End Function

    Private Sub lblConnectionString_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblConnectionString.Click
        If Not RunTime() Then
            Me.cmbProvider.SelectedIndex = vbBabel.DAL.ProviderType.OleDb
            Me.cmbDatabaseType.SelectedIndex = 0
            Me.txtConnectionString.Text = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\BabelDemo\ERP.mdb"
        End If

    End Sub
    '15.03.2016 - Old code
    'Private Sub cmbDatabase_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbDatabase.GotFocus
    '    iCMBDatabase_SelectedIndex = Me.cmbDatabase.SelectedIndex
    'End Sub
    'New code - Must find the correct index after we have aelected the one we want.
    Private Sub cmbDatabase_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbDatabase.LostFocus
        iCMBDatabase_SelectedIndex = Me.cmbDatabase.SelectedIndex
    End Sub

End Class
