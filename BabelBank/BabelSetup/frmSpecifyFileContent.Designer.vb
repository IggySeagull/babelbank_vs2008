﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSpecifyFileContent
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmbFileType1 = New System.Windows.Forms.ComboBox
        Me.txtServiceID1 = New System.Windows.Forms.TextBox
        Me.lblFileType = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblServiceId = New System.Windows.Forms.Label
        Me.cmbFileType2 = New System.Windows.Forms.ComboBox
        Me.cmbFileType3 = New System.Windows.Forms.ComboBox
        Me.cmbFileType4 = New System.Windows.Forms.ComboBox
        Me.cmbFileType5 = New System.Windows.Forms.ComboBox
        Me.txtServiceID2 = New System.Windows.Forms.TextBox
        Me.txtServiceID3 = New System.Windows.Forms.TextBox
        Me.txtServiceID4 = New System.Windows.Forms.TextBox
        Me.txtServiceID5 = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(12, 318)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(550, 1)
        Me.lblLine1.TabIndex = 59
        Me.lblLine1.Text = "Label1"
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(478, 322)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 61
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(391, 322)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 60
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmbFileType1
        '
        Me.cmbFileType1.BackColor = System.Drawing.SystemColors.Window
        Me.cmbFileType1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbFileType1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbFileType1.Location = New System.Drawing.Point(226, 167)
        Me.cmbFileType1.Name = "cmbFileType1"
        Me.cmbFileType1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbFileType1.Size = New System.Drawing.Size(193, 21)
        Me.cmbFileType1.TabIndex = 62
        Me.cmbFileType1.Text = "cmbFileType1"
        '
        'txtServiceID1
        '
        Me.txtServiceID1.AcceptsReturn = True
        Me.txtServiceID1.BackColor = System.Drawing.SystemColors.Window
        Me.txtServiceID1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtServiceID1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtServiceID1.Location = New System.Drawing.Point(425, 167)
        Me.txtServiceID1.MaxLength = 35
        Me.txtServiceID1.Name = "txtServiceID1"
        Me.txtServiceID1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtServiceID1.Size = New System.Drawing.Size(134, 20)
        Me.txtServiceID1.TabIndex = 63
        '
        'lblFileType
        '
        Me.lblFileType.BackColor = System.Drawing.SystemColors.Control
        Me.lblFileType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFileType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFileType.Location = New System.Drawing.Point(223, 147)
        Me.lblFileType.Name = "lblFileType"
        Me.lblFileType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFileType.Size = New System.Drawing.Size(144, 17)
        Me.lblFileType.TabIndex = 64
        Me.lblFileType.Text = "FileType"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(194, 68)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(380, 66)
        Me.lblHeading.TabIndex = 65
        Me.lblHeading.Text = "60692 - Explaination"
        '
        'lblServiceId
        '
        Me.lblServiceId.BackColor = System.Drawing.SystemColors.Control
        Me.lblServiceId.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblServiceId.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblServiceId.Location = New System.Drawing.Point(381, 147)
        Me.lblServiceId.Name = "lblServiceId"
        Me.lblServiceId.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblServiceId.Size = New System.Drawing.Size(144, 17)
        Me.lblServiceId.TabIndex = 66
        Me.lblServiceId.Text = "ServiceId"
        '
        'cmbFileType2
        '
        Me.cmbFileType2.BackColor = System.Drawing.SystemColors.Window
        Me.cmbFileType2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbFileType2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbFileType2.Location = New System.Drawing.Point(226, 193)
        Me.cmbFileType2.Name = "cmbFileType2"
        Me.cmbFileType2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbFileType2.Size = New System.Drawing.Size(193, 21)
        Me.cmbFileType2.TabIndex = 67
        Me.cmbFileType2.Text = "cmbFileType2"
        '
        'cmbFileType3
        '
        Me.cmbFileType3.BackColor = System.Drawing.SystemColors.Window
        Me.cmbFileType3.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbFileType3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbFileType3.Location = New System.Drawing.Point(226, 220)
        Me.cmbFileType3.Name = "cmbFileType3"
        Me.cmbFileType3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbFileType3.Size = New System.Drawing.Size(193, 21)
        Me.cmbFileType3.TabIndex = 68
        Me.cmbFileType3.Text = "cmbFileType3"
        '
        'cmbFileType4
        '
        Me.cmbFileType4.BackColor = System.Drawing.SystemColors.Window
        Me.cmbFileType4.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbFileType4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbFileType4.Location = New System.Drawing.Point(226, 247)
        Me.cmbFileType4.Name = "cmbFileType4"
        Me.cmbFileType4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbFileType4.Size = New System.Drawing.Size(193, 21)
        Me.cmbFileType4.TabIndex = 69
        Me.cmbFileType4.Text = "cmbFileType4"
        '
        'cmbFileType5
        '
        Me.cmbFileType5.BackColor = System.Drawing.SystemColors.Window
        Me.cmbFileType5.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbFileType5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbFileType5.Location = New System.Drawing.Point(226, 274)
        Me.cmbFileType5.Name = "cmbFileType5"
        Me.cmbFileType5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbFileType5.Size = New System.Drawing.Size(193, 21)
        Me.cmbFileType5.TabIndex = 70
        Me.cmbFileType5.Text = "cmbFileType5"
        '
        'txtServiceID2
        '
        Me.txtServiceID2.AcceptsReturn = True
        Me.txtServiceID2.BackColor = System.Drawing.SystemColors.Window
        Me.txtServiceID2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtServiceID2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtServiceID2.Location = New System.Drawing.Point(425, 193)
        Me.txtServiceID2.MaxLength = 35
        Me.txtServiceID2.Name = "txtServiceID2"
        Me.txtServiceID2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtServiceID2.Size = New System.Drawing.Size(134, 20)
        Me.txtServiceID2.TabIndex = 71
        '
        'txtServiceID3
        '
        Me.txtServiceID3.AcceptsReturn = True
        Me.txtServiceID3.BackColor = System.Drawing.SystemColors.Window
        Me.txtServiceID3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtServiceID3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtServiceID3.Location = New System.Drawing.Point(425, 220)
        Me.txtServiceID3.MaxLength = 35
        Me.txtServiceID3.Name = "txtServiceID3"
        Me.txtServiceID3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtServiceID3.Size = New System.Drawing.Size(134, 20)
        Me.txtServiceID3.TabIndex = 72
        '
        'txtServiceID4
        '
        Me.txtServiceID4.AcceptsReturn = True
        Me.txtServiceID4.BackColor = System.Drawing.SystemColors.Window
        Me.txtServiceID4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtServiceID4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtServiceID4.Location = New System.Drawing.Point(425, 247)
        Me.txtServiceID4.MaxLength = 35
        Me.txtServiceID4.Name = "txtServiceID4"
        Me.txtServiceID4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtServiceID4.Size = New System.Drawing.Size(134, 20)
        Me.txtServiceID4.TabIndex = 73
        '
        'txtServiceID5
        '
        Me.txtServiceID5.AcceptsReturn = True
        Me.txtServiceID5.BackColor = System.Drawing.SystemColors.Window
        Me.txtServiceID5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtServiceID5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtServiceID5.Location = New System.Drawing.Point(425, 274)
        Me.txtServiceID5.MaxLength = 35
        Me.txtServiceID5.Name = "txtServiceID5"
        Me.txtServiceID5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtServiceID5.Size = New System.Drawing.Size(134, 20)
        Me.txtServiceID5.TabIndex = 74
        '
        'frmSpecifyFileContent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(574, 355)
        Me.Controls.Add(Me.txtServiceID5)
        Me.Controls.Add(Me.txtServiceID4)
        Me.Controls.Add(Me.txtServiceID3)
        Me.Controls.Add(Me.txtServiceID2)
        Me.Controls.Add(Me.cmbFileType5)
        Me.Controls.Add(Me.cmbFileType4)
        Me.Controls.Add(Me.cmbFileType3)
        Me.Controls.Add(Me.cmbFileType2)
        Me.Controls.Add(Me.lblServiceId)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.lblFileType)
        Me.Controls.Add(Me.txtServiceID1)
        Me.Controls.Add(Me.cmbFileType1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lblLine1)
        Me.Name = "frmSpecifyFileContent"
        Me.Text = "frmSpecifyFileContent"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents cmdCancel As System.Windows.Forms.Button
    Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents cmbFileType1 As System.Windows.Forms.ComboBox
    Public WithEvents txtServiceID1 As System.Windows.Forms.TextBox
    Public WithEvents lblFileType As System.Windows.Forms.Label
    Public WithEvents lblHeading As System.Windows.Forms.Label
    Public WithEvents lblServiceId As System.Windows.Forms.Label
    Public WithEvents cmbFileType2 As System.Windows.Forms.ComboBox
    Public WithEvents cmbFileType3 As System.Windows.Forms.ComboBox
    Public WithEvents cmbFileType4 As System.Windows.Forms.ComboBox
    Public WithEvents cmbFileType5 As System.Windows.Forms.ComboBox
    Public WithEvents txtServiceID2 As System.Windows.Forms.TextBox
    Public WithEvents txtServiceID3 As System.Windows.Forms.TextBox
    Public WithEvents txtServiceID4 As System.Windows.Forms.TextBox
    Public WithEvents txtServiceID5 As System.Windows.Forms.TextBox
End Class
