Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("vbBabelSetup_NET.vbBabelSetup")> Public Class vbBabelSetup
	
	Private iFileSetup_ID As Short
	Private oProfile As vbbabel.Profile
	
	'********* START PROPERTY SETTINGS ***********************
	'18.08.2008 - Commented next SET property
	'Public Property Set Filesetup_ID(NewVal As Integer)
	'    iFileSetup_ID = NewVal
	'End Property
	Public WriteOnly Property Filesetup_ID() As Short
		Set(ByVal Value As Short)
			iFileSetup_ID = Value
		End Set
	End Property
	Public Property Profile() As Object
		Get
			Profile = oProfile
		End Get
		Set(ByVal Value As Object)
			If IsReference(Value) And Not TypeOf Value Is String Then
				oProfile = Value
			Else
				oProfile = Value
			End If
		End Set
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
	'UPGRADE_NOTE: class_initialize was upgraded to class_initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Sub Class_Initialize_Renamed()
		iFileSetup_ID = -1
		
		' When Setup is called, the code attempts
		' to find a local satellite DLL that will contain all the
		' resources.
        'If Not (LoadLocalizedResources("Setup")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
        'End If
        If RunTime() Then
            'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'App.HelpFile = My.Application.Info.DirectoryPath & "\babelbank_no.hlp"
        Else
            'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'App.HelpFile = "c:\projects\babelbank\babelbank_no.hlp"
        End If
		
		
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	'UPGRADE_NOTE: class_terminate was upgraded to class_terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    'Public Sub Class_Terminate_Renamed()
    '	'UPGRADE_NOTE: Object oProfile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '	oProfile = Nothing
    'End Sub
	Protected Overrides Sub Finalize()
        'Class_Terminate_Renamed()  24.07.2019
        oProfile = Nothing
		MyBase.Finalize()
	End Sub
	Public Function Setup() As Boolean
		'Dim oProfile As Profile
		Dim bStatus As Boolean
		' pass parameteres to setup.bas
		
		' if iFilesetup_id= -1, then start Wizard for building a new profile
		' if iFilesetup_id> 0, then start with wizard with existing profile, for edit
		' if iFilesetup_id = -9, then mark profile, and connected profiles, for delete
		
		' The rest of the setupstuff is handled in bas-modules
        bStatus = BabelSetup.Main_Renamed(oProfile, iFileSetup_ID)

        ' Test setup DnB TBIW
        'bStatus = BabelSetup.DnB_TBIW_SendProfile(oProfile, iFileSetup_ID)

        ' return bStatus
        Setup = bStatus

    End Function
    Public Function Start_DnB_TBIW_SendProfile() As Boolean
        ' Special setup of Sendprofiles for DnB TelebankInternational WEB
        Dim bStatus As Boolean
        ' The rest of the setupstuff is handled in bas-modules
        bStatus = BabelSetup.DnB_TBIW_SendProfile(oProfile, iFileSetup_ID)

        ' return bStatus
        Start_DnB_TBIW_SendProfile = bStatus

    End Function
End Class
