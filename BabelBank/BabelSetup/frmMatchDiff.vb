﻿Option Strict Off
Option Explicit On
Friend Class frmMatchDiff
    Inherits System.Windows.Forms.Form
    'Dim iCurrentRow As Integer
    'Public bCancel As Boolean
    Private bAnythingChanged As Boolean = False

    Private Sub cmdDelete_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdDelete.Click
        Me.gridDiffs.Rows(Me.gridDiffs.CurrentRow.Index).Visible = False
        bAnythingChanged = True
        'spreadshit
        'Me.sprDiffs.row = iCurrentRow
        'Me.sprDiffs.RowHidden = True

    End Sub

    Private Sub frmMatchDiff_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "")
        FormLRSCaptions(Me)
        'iCurrentRow = 1
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        'If MsgBox("Vil du avslutte uten lagring ?", vbYesNo + vbDefaultButton2, "Konti") = vbYes Then
        If MsgBox(LRS(60011), MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, LRS(60012)) = MsgBoxResult.Yes Then
            ' Cancel, Yes
            'Unload Me
            Me.Hide()
            bAnythingChanged = False
        Else
            ' No Cancel, Continue
        End If

    End Sub
    Private Sub frmMatchDiff_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        If UnloadMode = CloseReason.UserClosing Then
            ' user pressed red x
            Cancel = 1
            cmdCancel_Click(CmdCancel, New System.EventArgs())
        End If
        eventArgs.Cancel = Cancel
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.Hide()
    End Sub
    Public Function AnythingChanged() As Boolean
        AnythingChanged = bAnythingChanged
    End Function

    Private Sub gridDiffs_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridDiffs.CellEndEdit
        bAnythingChanged = True
        ' test if on last line;
        If gridDiffs.CurrentRow.Index = (gridDiffs.Rows.Count - 1) Then
            ' add new line
            gridDiffs.Rows.Add()
        End If

    End Sub

End Class
