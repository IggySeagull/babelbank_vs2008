Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("ClientSetup_NET.ClientSetup")> Public Class ClientSetup
	Private oProfile As Object
    Private Shared frmClientSettings As frmClientSettings
	'********* START PROPERTY SETTINGS ***********************
	Public Property Profile() As Object
		Get
			Profile = oProfile
		End Get
		Set(ByVal Value As Object)
			If IsReference(Value) And Not TypeOf Value Is String Then
				oProfile = Value
			Else
				oProfile = Value
			End If
		End Set
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
	'UPGRADE_NOTE: class_initialize was upgraded to class_initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Sub Class_Initialize_Renamed()
		' When Setup is called, the code attempts
		' to find a local satellite DLL that will contain all the
		' resources.
        'If Not (LoadLocalizedResources("Setup")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
        'End If
        If RunTime() Then
            'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'App.HelpFile = My.Application.Info.DirectoryPath & "\babelbank_no.hlp"
        Else
            'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'App.HelpFile = "c:\projects\babelbank\babelbank_no.hlp"
        End If
		
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	'UPGRADE_NOTE: class_terminate was upgraded to class_terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    'Public Sub Class_Terminate_Renamed()
    '	'UPGRADE_NOTE: Object oProfile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '	oProfile = Nothing
    'End Sub
	Protected Overrides Sub Finalize()
        'Class_Terminate_Renamed()
        '24.07.2019
        oProfile = Nothing
		MyBase.Finalize()
	End Sub
	'Public Function Start() As Boolean
	'' removed 14.04.2010 before DotNet
	'Dim bStatus As Boolean
	'
	'' Check if any profiles exists
	'' If we have no profiles, then there is no use in showing clientsetup:
	'If oProfile.FileSetups.Count = 0 Then
	'    MsgBox LRS(60145), vbOKOnly + vbInformation
	'    bStatus = False
	'Else
	'    ' The rest of the client-setupstuff is handled in bas-modules
	'    bStatus = BabelSetup.ShowClients(oProfile)
	'End If
	'' return bStatus
	'Start = bStatus
	'
	'End Function
	Public Function StartNew() As Boolean
		' "New" clientsetup with parameters also for matching
		'------------------------------------------------------
		Dim bStatus As Boolean
		
		' Check if any profiles exists
		' If we have no profiles, then there is no use in showing clientsetup:
		'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If oProfile.FileSetups.Count = 0 Then
			MsgBox(LRS(60145), MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
			bStatus = False
		Else
            ' call form for "new" clientsetup
            frmClientSettings = New frmClientSettings
            frmClientSettings.ShowDialog()
        End If
		
        frmClientSettings = Nothing

		
	End Function
	
	'KOREN
	'Public Function StartInvoiceDesc() As Boolean
	'Dim bStatus As Boolean
	'
	'' Check if any profiles exists
	'' If we have no profiles, then there is no use in showing clientsetup:
	'If oProfile.FileSetups.Count = 0 Then
	'    MsgBox LRS(60145)
	'    bStatus = False
	'Else
	'    ' The rest of the client-setupstuff is handled in bas-modules
	'    bStatus = BabelSetup.ShowClientsInvoiceDesc(oProfile)
	'End If
	'' return bStatus
	'StartInvoiceDesc = bStatus
	'
	'End Function
	'
End Class
