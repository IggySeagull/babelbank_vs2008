Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmDnB_TBIW_NBcodes
    Inherits System.Windows.Forms.Form
    'Private oLocalFileSetup As vbBabel.FileSetup
    Private bRecordExists As Boolean
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        Me.Hide()
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Dim bEnoughInfo As Boolean

        bEnoughInfo = False
        If Me.cmbCode.SelectedIndex > 0 Then
            If Not EmptyString((Me.txtExplanation.Text)) Then
                bEnoughInfo = True
            Else
                MsgBox(LRS(60446), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRS(60447))
                'MsgBox "Please enter a standard text to explain the reason for the payments.", vbOKOnly, "MISSING INFORMATION"
            End If
        Else
            'No code chosen
            bEnoughInfo = True
        End If

        If bEnoughInfo = True Then
            SaveInfo()
            Me.Hide()
        End If

    End Sub
    Private Sub frmDnB_TBIW_NBcodes_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim aCodes() As String
        Dim lCounter As Integer

        bRecordExists = False
        FormvbStyle(Me, "dollar.jpg", 400)
        FormLRSCaptions(Me)
        aCodes = VB6.CopyArray(LoadNBCodes)

        If Not Array_IsEmpty(aCodes) Then
            For lCounter = 0 To UBound(aCodes, 1)
                Me.cmbCode.Items.Add(aCodes(lCounter))
            Next lCounter
        End If

        ConnectAndRetrieve()

    End Sub
    Private Sub ConnectAndRetrieve()
        Dim lCounter As Integer
        Dim sCode As String
        Dim bCodeFound As Boolean

        sCode = Trim(Str(oFilesetup.CodeNB))

        bCodeFound = False
        For lCounter = 0 To Me.cmbCode.Items.Count - 1
            cmbCode.SelectedIndex = lCounter
            If sCode = VB.Left(Me.cmbCode.Text, 2) Then
                Me.txtExplanation.Text = oFilesetup.ExplanationNB
                bCodeFound = True
                Exit For
            End If
        Next lCounter

        ' XNET 06.11.2012 added next line
        If oFilesetup.CodeNBCurrencyOnly Then
            Me.chkForCurrencyOnly.Checked = 1
        Else
            Me.chkForCurrencyOnly.Checked = 0
        End If

        If Not bCodeFound Then
            cmbCode.SelectedIndex = 0
        End If
    End Sub
    Private Sub frmDnB_TBIW_NBcodes_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        eventArgs.Cancel = Cancel
    End Sub

    Private Sub SaveInfo()

        If Me.cmbCode.SelectedIndex > 0 Then
            oFilesetup.CodeNB = CInt(VB.Left(cmbCode.Text, 2))
            oFilesetup.ExplanationNB = Trim(Me.txtExplanation.Text)
            ' XNET 06.11.2012 added next line
            oFilesetup.CodeNBCurrencyOnly = CBool(Me.chkForCurrencyOnly.Checked)
        Else
            oFilesetup.CodeNB = -1
            oFilesetup.ExplanationNB = " "
            ' XNET 06.11.2012 added next line
            oFilesetup.CodeNBCurrencyOnly = CBool(Me.chkForCurrencyOnly.Checked)
        End If
    End Sub
End Class
