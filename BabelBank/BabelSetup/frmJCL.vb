Option Strict Off
Option Explicit On
Friend Class frmJCL
	Inherits System.Windows.Forms.Form
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		SaveJCL()
		Me.Hide()
    End Sub
    Sub SaveJCL()
        oFilesetup.PreJCL = Trim(Me.txtPreJCL.Text)
        oFilesetup.PostJCL = Trim(Me.txtPostJCL.Text)
    End Sub
End Class
