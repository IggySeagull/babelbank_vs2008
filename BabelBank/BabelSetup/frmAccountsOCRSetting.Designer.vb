<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmAccountOCRSettings
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdDelete As System.Windows.Forms.Button
	Public WithEvents txtName As System.Windows.Forms.TextBox
	Public WithEvents chkNegativeAmounts As System.Windows.Forms.CheckBox
	Public WithEvents optKID3 As System.Windows.Forms.RadioButton
	Public WithEvents optKID2 As System.Windows.Forms.RadioButton
	Public WithEvents optKID1 As System.Windows.Forms.RadioButton
	Public WithEvents txtKIDLen As System.Windows.Forms.TextBox
	Public WithEvents txtCDV As System.Windows.Forms.TextBox
	Public WithEvents txtMask As System.Windows.Forms.TextBox
	Public WithEvents txtInvoiceStart As System.Windows.Forms.TextBox
	Public WithEvents txtInvoiceLen As System.Windows.Forms.TextBox
	Public WithEvents txtCustomerStart As System.Windows.Forms.TextBox
	Public WithEvents txtCustomerLen As System.Windows.Forms.TextBox
	Public WithEvents txtClientLen As System.Windows.Forms.TextBox
	Public WithEvents txtClientStart As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents lblDescription As System.Windows.Forms.Label
	Public WithEvents lblName As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lblCDV As System.Windows.Forms.Label
	Public WithEvents lblFixed As System.Windows.Forms.Label
	Public WithEvents lblStartInvoiceNo As System.Windows.Forms.Label
	Public WithEvents lblInvocieLen As System.Windows.Forms.Label
	Public WithEvents lblCustomerStart As System.Windows.Forms.Label
	Public WithEvents lblCustomerLen As System.Windows.Forms.Label
	Public WithEvents lblClientLen1 As System.Windows.Forms.Label
	Public WithEvents lblClientStart1 As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.txtName = New System.Windows.Forms.TextBox
        Me.chkNegativeAmounts = New System.Windows.Forms.CheckBox
        Me.optKID3 = New System.Windows.Forms.RadioButton
        Me.optKID2 = New System.Windows.Forms.RadioButton
        Me.optKID1 = New System.Windows.Forms.RadioButton
        Me.txtKIDLen = New System.Windows.Forms.TextBox
        Me.txtCDV = New System.Windows.Forms.TextBox
        Me.txtMask = New System.Windows.Forms.TextBox
        Me.txtInvoiceStart = New System.Windows.Forms.TextBox
        Me.txtInvoiceLen = New System.Windows.Forms.TextBox
        Me.txtCustomerStart = New System.Windows.Forms.TextBox
        Me.txtCustomerLen = New System.Windows.Forms.TextBox
        Me.txtClientLen = New System.Windows.Forms.TextBox
        Me.txtClientStart = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblCDV = New System.Windows.Forms.Label
        Me.lblFixed = New System.Windows.Forms.Label
        Me.lblStartInvoiceNo = New System.Windows.Forms.Label
        Me.lblInvocieLen = New System.Windows.Forms.Label
        Me.lblCustomerStart = New System.Windows.Forms.Label
        Me.lblCustomerLen = New System.Windows.Forms.Label
        Me.lblClientLen1 = New System.Windows.Forms.Label
        Me.lblClientStart1 = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdDelete
        '
        Me.cmdDelete.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDelete.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDelete.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDelete.Location = New System.Drawing.Point(134, 432)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDelete.Size = New System.Drawing.Size(73, 21)
        Me.cmdDelete.TabIndex = 28
        Me.cmdDelete.TabStop = False
        Me.cmdDelete.Text = "55015 - &Slett"
        Me.cmdDelete.UseVisualStyleBackColor = False
        '
        'txtName
        '
        Me.txtName.AcceptsReturn = True
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtName.Enabled = False
        Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtName.Location = New System.Drawing.Point(216, 88)
        Me.txtName.MaxLength = 30
        Me.txtName.Name = "txtName"
        Me.txtName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtName.Size = New System.Drawing.Size(148, 20)
        Me.txtName.TabIndex = 1
        '
        'chkNegativeAmounts
        '
        Me.chkNegativeAmounts.BackColor = System.Drawing.SystemColors.Control
        Me.chkNegativeAmounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkNegativeAmounts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkNegativeAmounts.Location = New System.Drawing.Point(47, 384)
        Me.chkNegativeAmounts.Name = "chkNegativeAmounts"
        Me.chkNegativeAmounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkNegativeAmounts.Size = New System.Drawing.Size(321, 25)
        Me.chkNegativeAmounts.TabIndex = 11
        Me.chkNegativeAmounts.Text = "60499 - Export negative amounts"
        Me.chkNegativeAmounts.UseVisualStyleBackColor = False
        '
        'optKID3
        '
        Me.optKID3.BackColor = System.Drawing.SystemColors.Window
        Me.optKID3.Cursor = System.Windows.Forms.Cursors.Default
        Me.optKID3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optKID3.Location = New System.Drawing.Point(195, 56)
        Me.optKID3.Name = "optKID3"
        Me.optKID3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optKID3.Size = New System.Drawing.Size(71, 21)
        Me.optKID3.TabIndex = 14
        Me.optKID3.TabStop = True
        Me.optKID3.Text = "KID 3"
        Me.optKID3.UseVisualStyleBackColor = False
        '
        'optKID2
        '
        Me.optKID2.BackColor = System.Drawing.SystemColors.Window
        Me.optKID2.Cursor = System.Windows.Forms.Cursors.Default
        Me.optKID2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optKID2.Location = New System.Drawing.Point(121, 58)
        Me.optKID2.Name = "optKID2"
        Me.optKID2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optKID2.Size = New System.Drawing.Size(71, 21)
        Me.optKID2.TabIndex = 13
        Me.optKID2.TabStop = True
        Me.optKID2.Text = "KID2"
        Me.optKID2.UseVisualStyleBackColor = False
        '
        'optKID1
        '
        Me.optKID1.BackColor = System.Drawing.SystemColors.Window
        Me.optKID1.Cursor = System.Windows.Forms.Cursors.Default
        Me.optKID1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optKID1.Location = New System.Drawing.Point(48, 58)
        Me.optKID1.Name = "optKID1"
        Me.optKID1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optKID1.Size = New System.Drawing.Size(71, 21)
        Me.optKID1.TabIndex = 0
        Me.optKID1.TabStop = True
        Me.optKID1.Text = "KID 1"
        Me.optKID1.UseVisualStyleBackColor = False
        '
        'txtKIDLen
        '
        Me.txtKIDLen.AcceptsReturn = True
        Me.txtKIDLen.BackColor = System.Drawing.SystemColors.Window
        Me.txtKIDLen.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtKIDLen.Enabled = False
        Me.txtKIDLen.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtKIDLen.Location = New System.Drawing.Point(216, 116)
        Me.txtKIDLen.MaxLength = 2
        Me.txtKIDLen.Name = "txtKIDLen"
        Me.txtKIDLen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtKIDLen.Size = New System.Drawing.Size(34, 20)
        Me.txtKIDLen.TabIndex = 2
        '
        'txtCDV
        '
        Me.txtCDV.AcceptsReturn = True
        Me.txtCDV.BackColor = System.Drawing.SystemColors.Window
        Me.txtCDV.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCDV.Enabled = False
        Me.txtCDV.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCDV.Location = New System.Drawing.Point(216, 302)
        Me.txtCDV.MaxLength = 2
        Me.txtCDV.Name = "txtCDV"
        Me.txtCDV.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCDV.Size = New System.Drawing.Size(34, 20)
        Me.txtCDV.TabIndex = 9
        '
        'txtMask
        '
        Me.txtMask.AcceptsReturn = True
        Me.txtMask.BackColor = System.Drawing.SystemColors.Window
        Me.txtMask.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMask.Enabled = False
        Me.txtMask.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMask.Location = New System.Drawing.Point(216, 328)
        Me.txtMask.MaxLength = 28
        Me.txtMask.Multiline = True
        Me.txtMask.Name = "txtMask"
        Me.txtMask.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMask.Size = New System.Drawing.Size(153, 20)
        Me.txtMask.TabIndex = 10
        '
        'txtInvoiceStart
        '
        Me.txtInvoiceStart.AcceptsReturn = True
        Me.txtInvoiceStart.BackColor = System.Drawing.SystemColors.Window
        Me.txtInvoiceStart.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtInvoiceStart.Enabled = False
        Me.txtInvoiceStart.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtInvoiceStart.Location = New System.Drawing.Point(216, 248)
        Me.txtInvoiceStart.MaxLength = 2
        Me.txtInvoiceStart.Name = "txtInvoiceStart"
        Me.txtInvoiceStart.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtInvoiceStart.Size = New System.Drawing.Size(34, 20)
        Me.txtInvoiceStart.TabIndex = 7
        '
        'txtInvoiceLen
        '
        Me.txtInvoiceLen.AcceptsReturn = True
        Me.txtInvoiceLen.BackColor = System.Drawing.SystemColors.Window
        Me.txtInvoiceLen.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtInvoiceLen.Enabled = False
        Me.txtInvoiceLen.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtInvoiceLen.Location = New System.Drawing.Point(216, 272)
        Me.txtInvoiceLen.MaxLength = 2
        Me.txtInvoiceLen.Name = "txtInvoiceLen"
        Me.txtInvoiceLen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtInvoiceLen.Size = New System.Drawing.Size(34, 20)
        Me.txtInvoiceLen.TabIndex = 8
        '
        'txtCustomerStart
        '
        Me.txtCustomerStart.AcceptsReturn = True
        Me.txtCustomerStart.BackColor = System.Drawing.SystemColors.Window
        Me.txtCustomerStart.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCustomerStart.Enabled = False
        Me.txtCustomerStart.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCustomerStart.Location = New System.Drawing.Point(216, 196)
        Me.txtCustomerStart.MaxLength = 2
        Me.txtCustomerStart.Name = "txtCustomerStart"
        Me.txtCustomerStart.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCustomerStart.Size = New System.Drawing.Size(34, 20)
        Me.txtCustomerStart.TabIndex = 5
        '
        'txtCustomerLen
        '
        Me.txtCustomerLen.AcceptsReturn = True
        Me.txtCustomerLen.BackColor = System.Drawing.SystemColors.Window
        Me.txtCustomerLen.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCustomerLen.Enabled = False
        Me.txtCustomerLen.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCustomerLen.Location = New System.Drawing.Point(216, 222)
        Me.txtCustomerLen.MaxLength = 2
        Me.txtCustomerLen.Name = "txtCustomerLen"
        Me.txtCustomerLen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCustomerLen.Size = New System.Drawing.Size(34, 20)
        Me.txtCustomerLen.TabIndex = 6
        '
        'txtClientLen
        '
        Me.txtClientLen.AcceptsReturn = True
        Me.txtClientLen.BackColor = System.Drawing.SystemColors.Window
        Me.txtClientLen.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtClientLen.Enabled = False
        Me.txtClientLen.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtClientLen.Location = New System.Drawing.Point(216, 169)
        Me.txtClientLen.MaxLength = 2
        Me.txtClientLen.Name = "txtClientLen"
        Me.txtClientLen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtClientLen.Size = New System.Drawing.Size(34, 20)
        Me.txtClientLen.TabIndex = 4
        '
        'txtClientStart
        '
        Me.txtClientStart.AcceptsReturn = True
        Me.txtClientStart.BackColor = System.Drawing.SystemColors.Window
        Me.txtClientStart.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtClientStart.Enabled = False
        Me.txtClientStart.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtClientStart.Location = New System.Drawing.Point(216, 143)
        Me.txtClientStart.MaxLength = 2
        Me.txtClientStart.Name = "txtClientStart"
        Me.txtClientStart.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtClientStart.Size = New System.Drawing.Size(34, 20)
        Me.txtClientStart.TabIndex = 3
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(290, 432)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 12
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(212, 432)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 16
        Me.CmdCancel.TabStop = False
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(56, 432)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 15
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "55001 - &Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'lblDescription
        '
        Me.lblDescription.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescription.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescription.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescription.Location = New System.Drawing.Point(43, 354)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescription.Size = New System.Drawing.Size(337, 28)
        Me.lblDescription.TabIndex = 27
        Me.lblDescription.Text = "60498 - KIDbeskrivelse: #=Numerisk tegn  Andre tegn er ""ekte"""
        '
        'lblName
        '
        Me.lblName.BackColor = System.Drawing.SystemColors.Control
        Me.lblName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblName.Location = New System.Drawing.Point(47, 88)
        Me.lblName.Name = "lblName"
        Me.lblName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblName.Size = New System.Drawing.Size(158, 19)
        Me.lblName.TabIndex = 26
        Me.lblName.Text = "60497 - KID name"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(47, 116)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(158, 19)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "60481 - KID length"
        '
        'lblCDV
        '
        Me.lblCDV.BackColor = System.Drawing.SystemColors.Control
        Me.lblCDV.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCDV.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCDV.Location = New System.Drawing.Point(47, 302)
        Me.lblCDV.Name = "lblCDV"
        Me.lblCDV.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCDV.Size = New System.Drawing.Size(158, 19)
        Me.lblCDV.TabIndex = 24
        Me.lblCDV.Text = "60479 -  CDV Control"
        '
        'lblFixed
        '
        Me.lblFixed.BackColor = System.Drawing.SystemColors.Control
        Me.lblFixed.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFixed.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFixed.Location = New System.Drawing.Point(47, 329)
        Me.lblFixed.Name = "lblFixed"
        Me.lblFixed.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFixed.Size = New System.Drawing.Size(158, 19)
        Me.lblFixed.TabIndex = 23
        Me.lblFixed.Text = "60480 - Fixed pattern"
        '
        'lblStartInvoiceNo
        '
        Me.lblStartInvoiceNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblStartInvoiceNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblStartInvoiceNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblStartInvoiceNo.Location = New System.Drawing.Point(47, 249)
        Me.lblStartInvoiceNo.Name = "lblStartInvoiceNo"
        Me.lblStartInvoiceNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblStartInvoiceNo.Size = New System.Drawing.Size(158, 19)
        Me.lblStartInvoiceNo.TabIndex = 22
        Me.lblStartInvoiceNo.Text = "60477 - Startpos invoiceno"
        '
        'lblInvocieLen
        '
        Me.lblInvocieLen.BackColor = System.Drawing.SystemColors.Control
        Me.lblInvocieLen.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblInvocieLen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblInvocieLen.Location = New System.Drawing.Point(47, 276)
        Me.lblInvocieLen.Name = "lblInvocieLen"
        Me.lblInvocieLen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblInvocieLen.Size = New System.Drawing.Size(158, 19)
        Me.lblInvocieLen.TabIndex = 21
        Me.lblInvocieLen.Text = "60478 - Length invoiceno"
        '
        'lblCustomerStart
        '
        Me.lblCustomerStart.BackColor = System.Drawing.SystemColors.Control
        Me.lblCustomerStart.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCustomerStart.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCustomerStart.Location = New System.Drawing.Point(47, 196)
        Me.lblCustomerStart.Name = "lblCustomerStart"
        Me.lblCustomerStart.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCustomerStart.Size = New System.Drawing.Size(158, 19)
        Me.lblCustomerStart.TabIndex = 20
        Me.lblCustomerStart.Text = "60475 - Startpos customerno"
        '
        'lblCustomerLen
        '
        Me.lblCustomerLen.BackColor = System.Drawing.SystemColors.Control
        Me.lblCustomerLen.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCustomerLen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCustomerLen.Location = New System.Drawing.Point(47, 222)
        Me.lblCustomerLen.Name = "lblCustomerLen"
        Me.lblCustomerLen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCustomerLen.Size = New System.Drawing.Size(158, 19)
        Me.lblCustomerLen.TabIndex = 19
        Me.lblCustomerLen.Text = "60476 - Length customerno"
        '
        'lblClientLen1
        '
        Me.lblClientLen1.BackColor = System.Drawing.SystemColors.Control
        Me.lblClientLen1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblClientLen1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblClientLen1.Location = New System.Drawing.Point(47, 169)
        Me.lblClientLen1.Name = "lblClientLen1"
        Me.lblClientLen1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblClientLen1.Size = New System.Drawing.Size(158, 19)
        Me.lblClientLen1.TabIndex = 18
        Me.lblClientLen1.Text = "60474 - Length clientno"
        '
        'lblClientStart1
        '
        Me.lblClientStart1.BackColor = System.Drawing.SystemColors.Control
        Me.lblClientStart1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblClientStart1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblClientStart1.Location = New System.Drawing.Point(47, 143)
        Me.lblClientStart1.Name = "lblClientStart1"
        Me.lblClientStart1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblClientStart1.Size = New System.Drawing.Size(158, 19)
        Me.lblClientStart1.TabIndex = 17
        Me.lblClientStart1.Text = "60473 - Startpos clientno"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 416)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(365, 1)
        Me.lblLine1.TabIndex = 29
        Me.lblLine1.Text = "Label1"
        '
        'frmAccountOCRSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(385, 458)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdDelete)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.chkNegativeAmounts)
        Me.Controls.Add(Me.optKID3)
        Me.Controls.Add(Me.optKID2)
        Me.Controls.Add(Me.optKID1)
        Me.Controls.Add(Me.txtKIDLen)
        Me.Controls.Add(Me.txtCDV)
        Me.Controls.Add(Me.txtMask)
        Me.Controls.Add(Me.txtInvoiceStart)
        Me.Controls.Add(Me.txtInvoiceLen)
        Me.Controls.Add(Me.txtCustomerStart)
        Me.Controls.Add(Me.txtCustomerLen)
        Me.Controls.Add(Me.txtClientLen)
        Me.Controls.Add(Me.txtClientStart)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblCDV)
        Me.Controls.Add(Me.lblFixed)
        Me.Controls.Add(Me.lblStartInvoiceNo)
        Me.Controls.Add(Me.lblInvocieLen)
        Me.Controls.Add(Me.lblCustomerStart)
        Me.Controls.Add(Me.lblCustomerLen)
        Me.Controls.Add(Me.lblClientLen1)
        Me.Controls.Add(Me.lblClientStart1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(372, 30)
        Me.Name = "frmAccountOCRSettings"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "60472 - OCR Settings"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
