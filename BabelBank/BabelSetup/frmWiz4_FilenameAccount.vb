Option Strict Off
Option Explicit On

Friend Class frmWiz4_FilenameAccount
    Inherits System.Windows.Forms.Form
    Private frmSpecial As frmSpecial
    Private frmFilter As frmFilter
	Private iFormatID As Short
    Public Sub SetfrmSpecial(ByVal frm As frmSpecial)
        frmSpecial = frm
    End Sub
    Public Sub SetfrmFilter(ByVal frm As frmFilter)
        frmFilter = frm
    End Sub
    Private Sub frmWiz4_FilenameAccount_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        FormvbStyle(Me, "dollar.jpg", 500)
        ' 28.12.2010 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next
    End Sub
	
	Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
        ' Show Helptopic
		'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(106))
		
	End Sub
	
	Private Sub cmdSpecial_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSpecial.Click
		VB6.ShowForm(frmSpecial, 1, Me)
	End Sub
	Private Sub cmdFilter_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFilter.Click
		VB6.ShowForm(frmFilter, 1, Me)
	End Sub
	
	Private Sub CmdBack_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdBack.Click
		Me.Hide()
		WizardMove((-1))
	End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
		WizardMove((-99))
	End Sub
	
	Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
		' Hent filnavn
		Dim spath As String
		Dim sFile As String
		' Keep filename after Browsing for folder;
		' Must have \ and .
		If InStr(Me.txtFromInternal.Text, "\") > 0 And InStr(Me.txtFromInternal.Text, ".") > 0 Then
			sFile = Mid(Me.txtFromInternal.Text, InStrRev(Me.txtFromInternal.Text, "\"))
		ElseIf InStr(Me.txtFromInternal.Text, ".") > 0 Then 
			' . only, keep all;
			sFile = "\" & Me.txtFromInternal.Text
		Else
			sFile = ""
		End If


		' Use BrowseForFolders, routine from VBNet ( i Utils)
		'spath = FolderPicker(Me, LRS(60010))   'Velg fil")
		' Changed 18.10.05
		' using a new function where we can pass a path
        'spath = FixPath((Me.txtFromInternal.Text))
        '31.12.2010
        spath = BrowseForFilesOrFolders(Me.txtFromInternal.Text, Me, LRS(60054), True)
		
		' spath ends with end of string - remove
		' Removed 18.10.05, do not end with space
		'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)
		
        ' If browseforfilesorfolders returns empty path, then keep the old one;
		If Len(spath) > 0 Then
			' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
			If InStr(spath, ".") = 0 Then
				Me.txtFromInternal.Text = Replace(spath & sFile, "\\", "\")
			Else
				' Possibly file selected
				Me.txtFromInternal.Text = spath
			End If
		End If
		
	End Sub
	
	Private Sub CmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdNext.Click
		Dim bContinue As Boolean
		bContinue = True
		
		' Either :\ or \\ or // in filename/path;
		If Len(Trim(Me.txtFromInternal.Text)) > 0 Then
			If InStr(Me.txtFromInternal.Text, ":\") = 0 And InStr(Me.txtFromInternal.Text, "\\") = 0 And InStr(Me.txtFromInternal.Text, "//") = 0 Then
				'New 20.09.2007 - Don't show this errormessage if the format is Database
                If iFormatID <> vbBabel.BabelFiles.FileType.Database And iFormatID <> vbBabel.BabelFiles.FileType.Connect_WCF_Remit Then
                    MsgBox(LRS(60193) & " " & Me.txtFromInternal.Text, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Invalid filename!
                    bContinue = False
                End If
			End If
		End If
		If bContinue Then
			Me.Hide()
			WizardMove((1))
		Else
			Me.txtFromInternal.Focus()
		End If
		
	End Sub
	
	
	Private Sub frmWiz4_FilenameAccount_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
			cmdCancel_Click(cmdCancel, New System.EventArgs())
		End If
		eventArgs.Cancel = Cancel
	End Sub
	
	Friend Function SetFormatID(ByRef iID As Short) As Boolean
		
		iFormatID = iID
		
	End Function

    Private Sub txtFromInternal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFromInternal.TextChanged

    End Sub
End Class
