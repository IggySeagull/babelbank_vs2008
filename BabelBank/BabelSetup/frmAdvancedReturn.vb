Option Strict Off
Option Explicit On
Friend Class frmAdvancedReturn
    Inherits System.Windows.Forms.Form

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        'DisableCheckboxes
        Me.Hide()

    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        'DisableCheckboxes
        Me.Hide()
        SaveFormatAdvancedReturn()
    End Sub
    'Private Sub EnableCheckboxes()
    '    chkRejectWrong.Enabled = True
    '    chkReturnMass.Enabled = True
    'End Sub
    'Private Sub DisableCheckboxes()
    '    chkRejectWrong.Enabled = False
    '    chkReturnMass.Enabled = False
    'End Sub
    Private Sub frmAdvancedReturn_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg")
    End Sub

    'UPGRADE_WARNING: Event txtDelDays.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtDelDays_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDelDays.TextChanged
        ' Numerics only
        If Not IsNumeric(txtDelDays.Text) Then
            txtDelDays.Text = ""
        End If
    End Sub
    Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
        ' Hent filnavn
        Dim spath As String

        ' using a new function where we can pass a path
        'spath = FixPath((Me.txtBackupPath.Text))
        spath = BrowseForFilesOrFolders(Me.txtBackupPath.Text, Me, LRS(60010), False)

        ' If browseforfilesorfolders returns empty path, then keep the old one;
        If Len(spath) > 0 Then
            Me.txtBackupPath.Text = spath
        End If

    End Sub

End Class
