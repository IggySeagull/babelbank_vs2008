<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmWiz15_NameReturn
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdSignalFile As System.Windows.Forms.Button
	Public WithEvents cmdLog As System.Windows.Forms.Button
	Public WithEvents chkSilent As System.Windows.Forms.CheckBox
	Public WithEvents txtDescription As System.Windows.Forms.TextBox
    Public WithEvents txtProfilename As System.Windows.Forms.TextBox
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents CmdFinish As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdNext As System.Windows.Forms.Button
	Public WithEvents CmdBack As System.Windows.Forms.Button
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents lblDescription As System.Windows.Forms.Label
	Public WithEvents lblProfilename As System.Windows.Forms.Label
    Public WithEvents lblHeading As System.Windows.Forms.Label
	Public WithEvents lblDescr As System.Windows.Forms.Label
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdSignalFile = New System.Windows.Forms.Button
        Me.cmdLog = New System.Windows.Forms.Button
        Me.chkSilent = New System.Windows.Forms.CheckBox
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.txtProfilename = New System.Windows.Forms.TextBox
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.CmdFinish = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdNext = New System.Windows.Forms.Button
        Me.CmdBack = New System.Windows.Forms.Button
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblProfilename = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblDescr = New System.Windows.Forms.Label
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblLine1 = New System.Windows.Forms.Label
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdSignalFile
        '
        Me.cmdSignalFile.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSignalFile.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSignalFile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSignalFile.Location = New System.Drawing.Point(454, 261)
        Me.cmdSignalFile.Name = "cmdSignalFile"
        Me.cmdSignalFile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSignalFile.Size = New System.Drawing.Size(73, 21)
        Me.cmdSignalFile.TabIndex = 14
        Me.cmdSignalFile.Text = "&Signalfil"
        Me.cmdSignalFile.UseVisualStyleBackColor = False
        '
        'cmdLog
        '
        Me.cmdLog.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLog.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdLog.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdLog.Location = New System.Drawing.Point(531, 261)
        Me.cmdLog.Name = "cmdLog"
        Me.cmdLog.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdLog.Size = New System.Drawing.Size(73, 21)
        Me.cmdLog.TabIndex = 13
        Me.cmdLog.Text = "&Logg"
        Me.cmdLog.UseVisualStyleBackColor = False
        '
        'chkSilent
        '
        Me.chkSilent.BackColor = System.Drawing.SystemColors.Control
        Me.chkSilent.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkSilent.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkSilent.Location = New System.Drawing.Point(200, 235)
        Me.chkSilent.Name = "chkSilent"
        Me.chkSilent.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkSilent.Size = New System.Drawing.Size(371, 22)
        Me.chkSilent.TabIndex = 12
        Me.chkSilent.Text = "Kj�r uten � vise meldinger (silent)"
        Me.chkSilent.UseVisualStyleBackColor = False
        '
        'txtDescription
        '
        Me.txtDescription.AcceptsReturn = True
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescription.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDescription.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDescription.Location = New System.Drawing.Point(304, 153)
        Me.txtDescription.MaxLength = 255
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDescription.Size = New System.Drawing.Size(265, 33)
        Me.txtDescription.TabIndex = 1
        '
        'txtProfilename
        '
        Me.txtProfilename.AcceptsReturn = True
        Me.txtProfilename.BackColor = System.Drawing.SystemColors.Window
        Me.txtProfilename.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtProfilename.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtProfilename.Location = New System.Drawing.Point(304, 113)
        Me.txtProfilename.MaxLength = 50
        Me.txtProfilename.Name = "txtProfilename"
        Me.txtProfilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtProfilename.Size = New System.Drawing.Size(265, 20)
        Me.txtProfilename.TabIndex = 0
        Me.txtProfilename.Text = "<profilnavn>"
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(144, 303)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 2
        Me.CmdHelp.Text = "&Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'CmdFinish
        '
        Me.CmdFinish.BackColor = System.Drawing.SystemColors.Control
        Me.CmdFinish.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdFinish.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdFinish.Location = New System.Drawing.Point(530, 303)
        Me.CmdFinish.Name = "CmdFinish"
        Me.CmdFinish.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdFinish.Size = New System.Drawing.Size(73, 21)
        Me.CmdFinish.TabIndex = 6
        Me.CmdFinish.Text = "F&ullf�r"
        Me.CmdFinish.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(288, 303)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 3
        Me.CmdCancel.Text = "&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdNext
        '
        Me.CmdNext.BackColor = System.Drawing.SystemColors.Control
        Me.CmdNext.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdNext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdNext.Location = New System.Drawing.Point(439, 303)
        Me.CmdNext.Name = "CmdNext"
        Me.CmdNext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdNext.Size = New System.Drawing.Size(73, 21)
        Me.CmdNext.TabIndex = 5
        Me.CmdNext.Text = "&Neste >"
        Me.CmdNext.UseVisualStyleBackColor = False
        Me.CmdNext.Visible = False
        '
        'CmdBack
        '
        Me.CmdBack.BackColor = System.Drawing.SystemColors.Control
        Me.CmdBack.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdBack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdBack.Location = New System.Drawing.Point(365, 303)
        Me.CmdBack.Name = "CmdBack"
        Me.CmdBack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdBack.Size = New System.Drawing.Size(73, 21)
        Me.CmdBack.TabIndex = 4
        Me.CmdBack.Text = "< &Forrige"
        Me.CmdBack.UseVisualStyleBackColor = False
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(144, 261)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(23, 21)
        Me._Image1_0.TabIndex = 15
        Me._Image1_0.TabStop = False
        '
        'lblDescription
        '
        Me.lblDescription.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescription.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescription.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescription.Location = New System.Drawing.Point(200, 153)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescription.Size = New System.Drawing.Size(89, 17)
        Me.lblDescription.TabIndex = 11
        Me.lblDescription.Text = "Beskrivelse"
        '
        'lblProfilename
        '
        Me.lblProfilename.BackColor = System.Drawing.SystemColors.Control
        Me.lblProfilename.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProfilename.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProfilename.Location = New System.Drawing.Point(200, 113)
        Me.lblProfilename.Name = "lblProfilename"
        Me.lblProfilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProfilename.Size = New System.Drawing.Size(89, 19)
        Me.lblProfilename.TabIndex = 9
        Me.lblProfilename.Text = "Profilens navn"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(200, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(393, 17)
        Me.lblHeading.TabIndex = 8
        Me.lblHeading.Text = "Gi et navn til returprofilen"
        '
        'lblDescr
        '
        Me.lblDescr.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescr.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescr.Location = New System.Drawing.Point(200, 66)
        Me.lblDescr.Name = "lblDescr"
        Me.lblDescr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescr.Size = New System.Drawing.Size(385, 37)
        Me.lblDescr.TabIndex = 7
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(13, 296)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(595, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmWiz15_NameReturn
        '
        Me.AcceptButton = Me.cmdLog
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(620, 333)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdSignalFile)
        Me.Controls.Add(Me.cmdLog)
        Me.Controls.Add(Me.chkSilent)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.txtProfilename)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.CmdFinish)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdNext)
        Me.Controls.Add(Me.CmdBack)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.lblProfilename)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.lblDescr)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmWiz15_NameReturn"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Navn for returprofil"
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
