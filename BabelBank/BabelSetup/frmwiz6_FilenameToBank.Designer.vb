<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmWiz6_FilenameToBank
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdSequence As System.Windows.Forms.Button
	Public WithEvents cmdAdvanced As System.Windows.Forms.Button
	Public WithEvents txtAccountsPath As System.Windows.Forms.TextBox
	Public WithEvents chkRemove As System.Windows.Forms.CheckBox
	Public WithEvents chkWarning As System.Windows.Forms.CheckBox
	Public WithEvents chkOverwrite As System.Windows.Forms.CheckBox
	Public WithEvents chkBackup As System.Windows.Forms.CheckBox
	Public WithEvents cmbBankname As System.Windows.Forms.ComboBox
	Public WithEvents txtToBank As System.Windows.Forms.TextBox
	Public WithEvents cmdFileOpen As System.Windows.Forms.Button
	Public WithEvents lblAccountspath As System.Windows.Forms.Label
	Public WithEvents lblBankname As System.Windows.Forms.Label
	Public WithEvents lblToBank As System.Windows.Forms.Label
	Public WithEvents fraToBank As System.Windows.Forms.GroupBox
	Public WithEvents CmdBack As System.Windows.Forms.Button
	Public WithEvents CmdNext As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdFinish As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents lblDescr As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWiz6_FilenameToBank))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdSequence = New System.Windows.Forms.Button
        Me.cmdAdvanced = New System.Windows.Forms.Button
        Me.fraToBank = New System.Windows.Forms.GroupBox
        Me.txtAccountsPath = New System.Windows.Forms.TextBox
        Me.chkRemove = New System.Windows.Forms.CheckBox
        Me.chkWarning = New System.Windows.Forms.CheckBox
        Me.chkOverwrite = New System.Windows.Forms.CheckBox
        Me.chkBackup = New System.Windows.Forms.CheckBox
        Me.cmbBankname = New System.Windows.Forms.ComboBox
        Me.txtToBank = New System.Windows.Forms.TextBox
        Me.cmdFileOpen = New System.Windows.Forms.Button
        Me.lblAccountspath = New System.Windows.Forms.Label
        Me.lblBankname = New System.Windows.Forms.Label
        Me.lblToBank = New System.Windows.Forms.Label
        Me.CmdBack = New System.Windows.Forms.Button
        Me.CmdNext = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdFinish = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblDescr = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.cmdSecurity = New System.Windows.Forms.Button
        Me.frmSecureEnvelope = New System.Windows.Forms.Button
        Me.fraToBank.SuspendLayout()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdSequence
        '
        Me.cmdSequence.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSequence.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSequence.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSequence.Location = New System.Drawing.Point(397, 248)
        Me.cmdSequence.Name = "cmdSequence"
        Me.cmdSequence.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSequence.Size = New System.Drawing.Size(105, 25)
        Me.cmdSequence.TabIndex = 7
        Me.cmdSequence.Text = "&Sekvensnumre"
        Me.cmdSequence.UseVisualStyleBackColor = False
        '
        'cmdAdvanced
        '
        Me.cmdAdvanced.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAdvanced.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAdvanced.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAdvanced.Location = New System.Drawing.Point(504, 248)
        Me.cmdAdvanced.Name = "cmdAdvanced"
        Me.cmdAdvanced.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAdvanced.Size = New System.Drawing.Size(105, 25)
        Me.cmdAdvanced.TabIndex = 8
        Me.cmdAdvanced.Text = "&Avansert oppsett"
        Me.cmdAdvanced.UseVisualStyleBackColor = False
        '
        'fraToBank
        '
        Me.fraToBank.BackColor = System.Drawing.SystemColors.Window
        Me.fraToBank.Controls.Add(Me.txtAccountsPath)
        Me.fraToBank.Controls.Add(Me.chkRemove)
        Me.fraToBank.Controls.Add(Me.chkWarning)
        Me.fraToBank.Controls.Add(Me.chkOverwrite)
        Me.fraToBank.Controls.Add(Me.chkBackup)
        Me.fraToBank.Controls.Add(Me.cmbBankname)
        Me.fraToBank.Controls.Add(Me.txtToBank)
        Me.fraToBank.Controls.Add(Me.cmdFileOpen)
        Me.fraToBank.Controls.Add(Me.lblAccountspath)
        Me.fraToBank.Controls.Add(Me.lblBankname)
        Me.fraToBank.Controls.Add(Me.lblToBank)
        Me.fraToBank.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraToBank.Location = New System.Drawing.Point(184, 96)
        Me.fraToBank.Name = "fraToBank"
        Me.fraToBank.Padding = New System.Windows.Forms.Padding(0)
        Me.fraToBank.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraToBank.Size = New System.Drawing.Size(424, 145)
        Me.fraToBank.TabIndex = 15
        Me.fraToBank.TabStop = False
        Me.fraToBank.Text = "Fil til bank"
        '
        'txtAccountsPath
        '
        Me.txtAccountsPath.AcceptsReturn = True
        Me.txtAccountsPath.BackColor = System.Drawing.SystemColors.Window
        Me.txtAccountsPath.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAccountsPath.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAccountsPath.Location = New System.Drawing.Point(108, 114)
        Me.txtAccountsPath.MaxLength = 0
        Me.txtAccountsPath.Name = "txtAccountsPath"
        Me.txtAccountsPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAccountsPath.Size = New System.Drawing.Size(276, 20)
        Me.txtAccountsPath.TabIndex = 6
        Me.txtAccountsPath.Text = "Text1"
        Me.txtAccountsPath.Visible = False
        '
        'chkRemove
        '
        Me.chkRemove.BackColor = System.Drawing.SystemColors.Window
        Me.chkRemove.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkRemove.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRemove.Location = New System.Drawing.Point(254, 80)
        Me.chkRemove.Name = "chkRemove"
        Me.chkRemove.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkRemove.Size = New System.Drawing.Size(165, 17)
        Me.chkRemove.TabIndex = 4
        Me.chkRemove.Text = "Fjern gamle filer f�r eksport"
        Me.chkRemove.UseVisualStyleBackColor = False
        '
        'chkWarning
        '
        Me.chkWarning.BackColor = System.Drawing.SystemColors.Window
        Me.chkWarning.Checked = True
        Me.chkWarning.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkWarning.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkWarning.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkWarning.Location = New System.Drawing.Point(108, 80)
        Me.chkWarning.Name = "chkWarning"
        Me.chkWarning.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkWarning.Size = New System.Drawing.Size(137, 17)
        Me.chkWarning.TabIndex = 3
        Me.chkWarning.Text = "Varsle hvis fil finnes"
        Me.chkWarning.UseVisualStyleBackColor = False
        '
        'chkOverwrite
        '
        Me.chkOverwrite.BackColor = System.Drawing.SystemColors.Window
        Me.chkOverwrite.Checked = True
        Me.chkOverwrite.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkOverwrite.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkOverwrite.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkOverwrite.Location = New System.Drawing.Point(254, 56)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkOverwrite.Size = New System.Drawing.Size(165, 17)
        Me.chkOverwrite.TabIndex = 2
        Me.chkOverwrite.Text = "Skriv over hvis fil finnes"
        Me.chkOverwrite.UseVisualStyleBackColor = False
        '
        'chkBackup
        '
        Me.chkBackup.BackColor = System.Drawing.SystemColors.Window
        Me.chkBackup.Checked = True
        Me.chkBackup.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBackup.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkBackup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkBackup.Location = New System.Drawing.Point(108, 56)
        Me.chkBackup.Name = "chkBackup"
        Me.chkBackup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkBackup.Size = New System.Drawing.Size(145, 17)
        Me.chkBackup.TabIndex = 1
        Me.chkBackup.Text = "Legg i backupkatalog"
        Me.chkBackup.UseVisualStyleBackColor = False
        '
        'cmbBankname
        '
        Me.cmbBankname.BackColor = System.Drawing.SystemColors.Window
        Me.cmbBankname.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbBankname.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbBankname.Location = New System.Drawing.Point(108, 103)
        Me.cmbBankname.Name = "cmbBankname"
        Me.cmbBankname.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbBankname.Size = New System.Drawing.Size(277, 21)
        Me.cmbBankname.TabIndex = 5
        Me.cmbBankname.Text = "<bank>"
        '
        'txtToBank
        '
        Me.txtToBank.AcceptsReturn = True
        Me.txtToBank.BackColor = System.Drawing.SystemColors.Window
        Me.txtToBank.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtToBank.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtToBank.Location = New System.Drawing.Point(108, 24)
        Me.txtToBank.MaxLength = 255
        Me.txtToBank.Name = "txtToBank"
        Me.txtToBank.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtToBank.Size = New System.Drawing.Size(277, 20)
        Me.txtToBank.TabIndex = 0
        '
        'cmdFileOpen
        '
        Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
        Me.cmdFileOpen.Location = New System.Drawing.Point(391, 20)
        Me.cmdFileOpen.Name = "cmdFileOpen"
        Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen.TabIndex = 16
        Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen.UseVisualStyleBackColor = False
        '
        'lblAccountspath
        '
        Me.lblAccountspath.BackColor = System.Drawing.SystemColors.Window
        Me.lblAccountspath.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAccountspath.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAccountspath.Location = New System.Drawing.Point(8, 113)
        Me.lblAccountspath.Name = "lblAccountspath"
        Me.lblAccountspath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAccountspath.Size = New System.Drawing.Size(97, 25)
        Me.lblAccountspath.TabIndex = 20
        Me.lblAccountspath.Text = "Path til kontofil"
        Me.lblAccountspath.Visible = False
        '
        'lblBankname
        '
        Me.lblBankname.BackColor = System.Drawing.SystemColors.Window
        Me.lblBankname.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBankname.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBankname.Location = New System.Drawing.Point(8, 104)
        Me.lblBankname.Name = "lblBankname"
        Me.lblBankname.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBankname.Size = New System.Drawing.Size(97, 17)
        Me.lblBankname.TabIndex = 19
        Me.lblBankname.Text = "Banknavn"
        '
        'lblToBank
        '
        Me.lblToBank.BackColor = System.Drawing.SystemColors.Window
        Me.lblToBank.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblToBank.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblToBank.Location = New System.Drawing.Point(8, 24)
        Me.lblToBank.Name = "lblToBank"
        Me.lblToBank.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblToBank.Size = New System.Drawing.Size(97, 33)
        Me.lblToBank.TabIndex = 17
        Me.lblToBank.Text = "Filnavn"
        '
        'CmdBack
        '
        Me.CmdBack.BackColor = System.Drawing.SystemColors.Control
        Me.CmdBack.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdBack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdBack.Location = New System.Drawing.Point(365, 303)
        Me.CmdBack.Name = "CmdBack"
        Me.CmdBack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdBack.Size = New System.Drawing.Size(73, 21)
        Me.CmdBack.TabIndex = 11
        Me.CmdBack.Text = "< &Forrige"
        Me.CmdBack.UseVisualStyleBackColor = False
        '
        'CmdNext
        '
        Me.CmdNext.BackColor = System.Drawing.SystemColors.Control
        Me.CmdNext.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdNext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdNext.Location = New System.Drawing.Point(439, 303)
        Me.CmdNext.Name = "CmdNext"
        Me.CmdNext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdNext.Size = New System.Drawing.Size(73, 21)
        Me.CmdNext.TabIndex = 0
        Me.CmdNext.Text = "&Neste >"
        Me.CmdNext.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(288, 303)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 10
        Me.CmdCancel.Text = "&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdFinish
        '
        Me.CmdFinish.BackColor = System.Drawing.SystemColors.Control
        Me.CmdFinish.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdFinish.Enabled = False
        Me.CmdFinish.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdFinish.Location = New System.Drawing.Point(530, 303)
        Me.CmdFinish.Name = "CmdFinish"
        Me.CmdFinish.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdFinish.Size = New System.Drawing.Size(73, 21)
        Me.CmdFinish.TabIndex = 13
        Me.CmdFinish.Text = "F&ullf�r"
        Me.CmdFinish.UseVisualStyleBackColor = False
        Me.CmdFinish.Visible = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(144, 303)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 9
        Me.CmdHelp.Text = "&Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(123, 259)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(14, 14)
        Me._Image1_0.TabIndex = 16
        Me._Image1_0.TabStop = False
        '
        'lblDescr
        '
        Me.lblDescr.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescr.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescr.Location = New System.Drawing.Point(185, 52)
        Me.lblDescr.Name = "lblDescr"
        Me.lblDescr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescr.Size = New System.Drawing.Size(425, 35)
        Me.lblDescr.TabIndex = 18
        Me.lblDescr.Text = "Oppgi navn p� den filen som skal sendes til bank."
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(183, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(409, 17)
        Me.lblHeading.TabIndex = 14
        Me.lblHeading.Text = "Legg inn navn p� filer som skal sendes bank"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(14, 296)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(595, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'cmdSecurity
        '
        Me.cmdSecurity.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSecurity.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSecurity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSecurity.Location = New System.Drawing.Point(290, 248)
        Me.cmdSecurity.Name = "cmdSecurity"
        Me.cmdSecurity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSecurity.Size = New System.Drawing.Size(105, 25)
        Me.cmdSecurity.TabIndex = 81
        Me.cmdSecurity.Text = "55050-S&ikkerhet"
        Me.cmdSecurity.UseVisualStyleBackColor = False
        '
        'frmSecureEnvelope
        '
        Me.frmSecureEnvelope.BackColor = System.Drawing.SystemColors.Control
        Me.frmSecureEnvelope.Cursor = System.Windows.Forms.Cursors.Default
        Me.frmSecureEnvelope.ForeColor = System.Drawing.SystemColors.ControlText
        Me.frmSecureEnvelope.Location = New System.Drawing.Point(183, 248)
        Me.frmSecureEnvelope.Name = "frmSecureEnvelope"
        Me.frmSecureEnvelope.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.frmSecureEnvelope.Size = New System.Drawing.Size(105, 25)
        Me.frmSecureEnvelope.TabIndex = 82
        Me.frmSecureEnvelope.Text = "SecureEnvelope"
        Me.frmSecureEnvelope.UseVisualStyleBackColor = False
        '
        'frmWiz6_FilenameToBank
        '
        Me.AcceptButton = Me.CmdNext
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(622, 335)
        Me.Controls.Add(Me.frmSecureEnvelope)
        Me.Controls.Add(Me.cmdSecurity)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdSequence)
        Me.Controls.Add(Me.cmdAdvanced)
        Me.Controls.Add(Me.fraToBank)
        Me.Controls.Add(Me.CmdBack)
        Me.Controls.Add(Me.CmdNext)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdFinish)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblDescr)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmWiz6_FilenameToBank"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Filnavn for fil til bank"
        Me.fraToBank.ResumeLayout(False)
        Me.fraToBank.PerformLayout()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents cmdSecurity As System.Windows.Forms.Button
    Public WithEvents frmSecureEnvelope As System.Windows.Forms.Button
#End Region 
End Class
