<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmReportPrinter
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents lstPrinters As System.Windows.Forms.ListBox
	Public WithEvents chkSelectPrinter As System.Windows.Forms.CheckBox
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents lblPrintername As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lstPrinters = New System.Windows.Forms.ListBox
        Me.chkSelectPrinter = New System.Windows.Forms.CheckBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblPrintername = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lstPrinters
        '
        Me.lstPrinters.BackColor = System.Drawing.SystemColors.Window
        Me.lstPrinters.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstPrinters.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstPrinters.Location = New System.Drawing.Point(152, 95)
        Me.lstPrinters.Name = "lstPrinters"
        Me.lstPrinters.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstPrinters.Size = New System.Drawing.Size(272, 69)
        Me.lstPrinters.TabIndex = 3
        '
        'chkSelectPrinter
        '
        Me.chkSelectPrinter.BackColor = System.Drawing.SystemColors.Control
        Me.chkSelectPrinter.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkSelectPrinter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkSelectPrinter.Location = New System.Drawing.Point(11, 55)
        Me.chkSelectPrinter.Name = "chkSelectPrinter"
        Me.chkSelectPrinter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkSelectPrinter.Size = New System.Drawing.Size(285, 24)
        Me.chkSelectPrinter.TabIndex = 2
        Me.chkSelectPrinter.Text = "60272 - Velg skriver ved utskrift"
        Me.chkSelectPrinter.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(341, 175)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 1
        Me.cmdCancel.Text = "55002 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(253, 175)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblPrintername
        '
        Me.lblPrintername.BackColor = System.Drawing.SystemColors.Control
        Me.lblPrintername.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPrintername.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPrintername.Location = New System.Drawing.Point(12, 95)
        Me.lblPrintername.Name = "lblPrintername"
        Me.lblPrintername.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPrintername.Size = New System.Drawing.Size(138, 21)
        Me.lblPrintername.TabIndex = 4
        Me.lblPrintername.Text = "60288 - Forhåndsvalgt printer"
        '
        'frmReportPrinter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(449, 207)
        Me.Controls.Add(Me.lstPrinters)
        Me.Controls.Add(Me.chkSelectPrinter)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lblPrintername)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmReportPrinter"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "60275 - Skriveroppsett"
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class
