Option Strict Off
Option Explicit On

Friend Class frmWiz2_WhichProfile
	Inherits System.Windows.Forms.Form
	
	
	Private Sub CmdBack_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdBack.Click
		Me.Hide()
		WizardMove((-1))
	End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
		WizardMove((-99))
	End Sub
	
	Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
        ' Show Helptopic
		'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(104))
	End Sub
	
	Private Sub CmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdNext.Click
		Me.Hide()
		WizardMove((1))
	End Sub
    Private Sub frmWiz2_WhichProfile_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control

        FormvbStyle(Me, "dollar.jpg")
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next

        'Fill lstQuickSetups
        lstQuickProfile.Items.Add(LRS(60126)) ' Telepay merge
        lstQuickProfile.Items.Add(LRS(60127)) ' Telepay split
        lstQuickProfile.Items.Add(LRS(60128)) ' OCR split
        lstQuickProfile.Items.Add(LRS(60129)) ' Convert Dir.Rem to Telepay
        lblQuickprofile.Text = LRS(60130)
    End Sub
	
	Private Sub frmWiz2_WhichProfile_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		
		If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
			cmdCancel_Click(cmdCancel, New System.EventArgs())
		End If
		
		eventArgs.Cancel = Cancel
	End Sub
    'Private Sub lstQuickProfile_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstQuickProfile.SelectedIndexChanged
    '	' Call sub to show frmQuickProfiles with correct settings
    '	quickProfiles((lstQuickProfile.SelectedIndex))

    'End Sub

End Class
