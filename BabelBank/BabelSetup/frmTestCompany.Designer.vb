<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmTestCompany
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtDebugCode As System.Windows.Forms.TextBox
	Public WithEvents chkRunTempCode As System.Windows.Forms.CheckBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents lblDebugCode As System.Windows.Forms.Label
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
    Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtDebugCode = New System.Windows.Forms.TextBox
        Me.chkRunTempCode = New System.Windows.Forms.CheckBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.lblDebugCode = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblLine1 = New System.Windows.Forms.Label
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtDebugCode
        '
        Me.txtDebugCode.AcceptsReturn = True
        Me.txtDebugCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtDebugCode.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDebugCode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDebugCode.Location = New System.Drawing.Point(323, 130)
        Me.txtDebugCode.MaxLength = 3
        Me.txtDebugCode.Name = "txtDebugCode"
        Me.txtDebugCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDebugCode.Size = New System.Drawing.Size(31, 20)
        Me.txtDebugCode.TabIndex = 1
        '
        'chkRunTempCode
        '
        Me.chkRunTempCode.BackColor = System.Drawing.SystemColors.Control
        Me.chkRunTempCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkRunTempCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRunTempCode.Location = New System.Drawing.Point(175, 102)
        Me.chkRunTempCode.Name = "chkRunTempCode"
        Me.chkRunTempCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkRunTempCode.Size = New System.Drawing.Size(256, 17)
        Me.chkRunTempCode.TabIndex = 0
        Me.chkRunTempCode.Text = "59036 - Activate testcode"
        Me.chkRunTempCode.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(319, 187)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 4
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(240, 187)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 3
        Me.CmdCancel.Text = "55002-&Cancel"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(160, 187)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 5
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "55001-&Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'lblDebugCode
        '
        Me.lblDebugCode.BackColor = System.Drawing.SystemColors.Control
        Me.lblDebugCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDebugCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDebugCode.Location = New System.Drawing.Point(177, 132)
        Me.lblDebugCode.Name = "lblDebugCode"
        Me.lblDebugCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDebugCode.Size = New System.Drawing.Size(145, 17)
        Me.lblDebugCode.TabIndex = 2
        Me.lblDebugCode.Text = "59037 - Debugcode"
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(391, 132)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(10, 22)
        Me._Image1_0.TabIndex = 6
        Me._Image1_0.TabStop = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 178)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(415, 1)
        Me.lblLine1.TabIndex = 81
        Me.lblLine1.Text = "Label1"
        '
        'frmTestCompany
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(434, 219)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtDebugCode)
        Me.Controls.Add(Me.chkRunTempCode)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.lblDebugCode)
        Me.Controls.Add(Me._Image1_0)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmTestCompany"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "55010 - Test"
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
