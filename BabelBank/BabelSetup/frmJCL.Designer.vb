<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmJCL
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents txtPostJCL As System.Windows.Forms.TextBox
	Public WithEvents txtPreJCL As System.Windows.Forms.TextBox
	Public WithEvents lblCRLF As System.Windows.Forms.Label
	Public WithEvents lblPostJCL As System.Windows.Forms.Label
	Public WithEvents lblPreJCL As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtPostJCL = New System.Windows.Forms.TextBox
        Me.txtPreJCL = New System.Windows.Forms.TextBox
        Me.lblCRLF = New System.Windows.Forms.Label
        Me.lblPostJCL = New System.Windows.Forms.Label
        Me.lblPreJCL = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(196, 280)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 5
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(108, 280)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 4
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'txtPostJCL
        '
        Me.txtPostJCL.AcceptsReturn = True
        Me.txtPostJCL.BackColor = System.Drawing.SystemColors.Window
        Me.txtPostJCL.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPostJCL.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPostJCL.Location = New System.Drawing.Point(16, 160)
        Me.txtPostJCL.MaxLength = 100
        Me.txtPostJCL.Multiline = True
        Me.txtPostJCL.Name = "txtPostJCL"
        Me.txtPostJCL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPostJCL.Size = New System.Drawing.Size(353, 73)
        Me.txtPostJCL.TabIndex = 3
        '
        'txtPreJCL
        '
        Me.txtPreJCL.AcceptsReturn = True
        Me.txtPreJCL.BackColor = System.Drawing.SystemColors.Window
        Me.txtPreJCL.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPreJCL.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPreJCL.Location = New System.Drawing.Point(16, 40)
        Me.txtPreJCL.MaxLength = 200
        Me.txtPreJCL.Multiline = True
        Me.txtPreJCL.Name = "txtPreJCL"
        Me.txtPreJCL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPreJCL.Size = New System.Drawing.Size(353, 73)
        Me.txtPreJCL.TabIndex = 1
        '
        'lblCRLF
        '
        Me.lblCRLF.BackColor = System.Drawing.SystemColors.Control
        Me.lblCRLF.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCRLF.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCRLF.Location = New System.Drawing.Point(16, 248)
        Me.lblCRLF.Name = "lblCRLF"
        Me.lblCRLF.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCRLF.Size = New System.Drawing.Size(353, 17)
        Me.lblCRLF.TabIndex = 6
        Me.lblCRLF.Text = "Use capital letters CRLF to indicate new line."
        '
        'lblPostJCL
        '
        Me.lblPostJCL.BackColor = System.Drawing.SystemColors.Control
        Me.lblPostJCL.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPostJCL.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPostJCL.Location = New System.Drawing.Point(16, 136)
        Me.lblPostJCL.Name = "lblPostJCL"
        Me.lblPostJCL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPostJCL.Size = New System.Drawing.Size(339, 17)
        Me.lblPostJCL.TabIndex = 2
        Me.lblPostJCL.Text = "JCL at end of file"
        '
        'lblPreJCL
        '
        Me.lblPreJCL.BackColor = System.Drawing.SystemColors.Control
        Me.lblPreJCL.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPreJCL.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPreJCL.Location = New System.Drawing.Point(16, 8)
        Me.lblPreJCL.Name = "lblPreJCL"
        Me.lblPreJCL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPreJCL.Size = New System.Drawing.Size(303, 17)
        Me.lblPreJCL.TabIndex = 0
        Me.lblPreJCL.Text = "JCL in top of file"
        '
        'frmJCL
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(384, 315)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtPostJCL)
        Me.Controls.Add(Me.txtPreJCL)
        Me.Controls.Add(Me.lblCRLF)
        Me.Controls.Add(Me.lblPostJCL)
        Me.Controls.Add(Me.lblPreJCL)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmJCL"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "JCL"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#End Region 
End Class
