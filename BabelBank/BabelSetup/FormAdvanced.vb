Option Strict Off
Option Explicit On
Friend Class frmAdvanced
    Inherits System.Windows.Forms.Form

    Private frmJCL As New frmJCL
    Private frmDnB_TBIW_NBcodes As New frmDnB_TBIW_NBcodes
    Private frmStructuredInvoice As New frmStructuredInvoice
    Dim iSeqNoType As Short
    Friend Function SetSeqNoType(ByRef i As Short) As Boolean

        iSeqNoType = i

    End Function

    'UPGRADE_WARNING: Event chkKeepClients.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkKeepClients_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkKeepClients.CheckStateChanged

        If iSeqNoType = 2 Then
            If chkKeepClients.CheckState = 0 Then
                MsgBox(Replace(Replace(Replace(LRS(60461, LRS(60063), LRS(55008)), "%3", LRS(60108)), "%4", LRS(60239)), "&", ""), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, LRS(60462))
                'The combination to set 'Keep clients' to true under Profilesetup - Filename to bank - Advanced, ' and to set the 'sequencenumber type' to 'Use sequencenumber per client under' under Setup - SequencenoSetup       ' is unlogical.        'If You are not sure, please contact Your dealer.
                'UNLOGICAL COMBINATION
            End If
        End If

    End Sub

    Private Sub cmdJCL_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdJCL.Click
        ' Load texts also into frmJCL;
        frmJCL.Text = LRS(60133)
        frmJCL.lblPreJCL.Text = LRS(60134)
        frmJCL.lblPostJCL.Text = LRS(60135)
        frmJCL.lblCRLF.Text = LRS(60136) ' Use captial letters CRLF to indicate new line.
        frmJCL.cmdCancel.Text = LRS(55002)
        frmJCL.txtPreJCL.Text = Trim(oFilesetup.PreJCL)
        frmJCL.txtPostJCL.Text = Trim(oFilesetup.PostJCL)

        VB6.ShowForm(frmJCL, 1, Me)
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        'DisableCheckboxes
        Me.Hide()
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        'DisableCheckboxes
        Me.Hide()
        SaveFormatAdvanced()
    End Sub
    Private Sub cmdNBCodes_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdNBCodes.Click
        'LUCKNOW
        VB6.ShowForm(frmDnB_TBIW_NBcodes, 1, Me)
        'frmDnB_TBIW_NBcodes.Close()
    End Sub

    Private Sub cmdStructuredText_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdStructuredText.Click
        frmStructuredInvoice.txtSearchText.Text = Trim(oFilesetup.StructText)
        frmStructuredInvoice.txtNoOfPos.Text = Trim(Str(oFilesetup.StructInvoicePos))
        frmStructuredInvoice.txtSearchTextNegative.Text = Trim(oFilesetup.StructTextNegative)
        frmStructuredInvoice.txtNoOfPosNegative.Text = Trim(Str(oFilesetup.StructInvoicePosNegative))

        FormSelectAllTextboxs(frmStructuredInvoice)

        VB6.ShowForm(frmStructuredInvoice, 1, Me)
    End Sub

    Private Sub frmAdvanced_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg", 300)
        FormLRSCaptions(Me)
    End Sub

    'UPGRADE_WARNING: Event optErrorsOnly.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optErrorsOnly_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optErrorsOnly.CheckedChanged
        If eventSender.Checked Then
            If optErrorsOnly.Checked = True Then
                optErrorsWarnings.Checked = False
                optNone.Checked = False
            End If
        End If
    End Sub

    'UPGRADE_WARNING: Event optErrorsWarnings.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optErrorsWarnings_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optErrorsWarnings.CheckedChanged
        If eventSender.Checked Then
            If optErrorsWarnings.Checked = True Then
                optErrorsOnly.Checked = False
                optNone.Checked = False
            End If
        End If
    End Sub

    'UPGRADE_WARNING: Event optNone.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optNone_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optNone.CheckedChanged
        If eventSender.Checked Then
            If optNone.Checked = True Then
                optErrorsOnly.Checked = False
                optErrorsWarnings.Checked = False
            End If
        End If
    End Sub
    Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
        ' Hent filnavn
        Dim spath As String

        ' using a new function where we can pass a path
        'spath = FixPath((Me.txtBackupPath.Text))
        '31.12.2010
        spath = BrowseForFilesOrFolders(Me.txtBackupPath.Text, Me, LRS(60010), False)

        ' If browseforfilesorfolders returns empty path, then keep the old one;
        If Len(spath) > 0 Then
            Me.txtBackupPath.Text = spath
        End If

    End Sub
End Class
