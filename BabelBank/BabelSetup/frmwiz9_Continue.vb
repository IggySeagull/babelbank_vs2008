Option Strict Off
Option Explicit On

Friend Class frmWiz9_Continue
	Inherits System.Windows.Forms.Form
	
	Private Sub CmdBack_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdBack.Click
		Me.Hide()
		WizardMove((-1))
	End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
		WizardMove((-99))
	End Sub
	Private Sub frmWiz9_Continue_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control

        FormvbStyle(Me, "dollar.jpg")
        ' 28.12.2010 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next
    End Sub
	
	Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
		Dim bOK As Boolean
		' Show Helptopic (16)
		'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(111))
		
	End Sub
	
	Private Sub CmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdNext.Click
		Me.Hide()
		WizardMove((1))
	End Sub
	Private Sub CmdFinish_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdFinish.Click
		Me.Hide()
		WizardMove((99))
	End Sub
	
	Private Sub frmWiz9_Continue_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
			cmdCancel_Click(cmdCancel, New System.EventArgs())
		End If
		eventArgs.Cancel = Cancel
	End Sub
	
	'UPGRADE_WARNING: Event optContinueReturnProfile.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub optContinueReturnProfile_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optContinueReturnProfile.CheckedChanged
		If eventSender.Checked Then
			If optContinueReturnProfile.Checked = True Then
				CmdNext.Visible = True
				CmdFinish.Visible = False
				CmdFinish.Enabled = False
			End If
			
		End If
	End Sub
	
	'UPGRADE_WARNING: Event optContinueSendProfile.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub optContinueSendProfile_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optContinueSendProfile.CheckedChanged
		If eventSender.Checked Then
			If optContinueSendProfile.Checked = True Then
				CmdNext.Visible = True
				CmdFinish.Visible = False
				CmdFinish.Enabled = False
			End If
			
		End If
	End Sub
	
	'UPGRADE_WARNING: Event optEnd.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub optEnd_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optEnd.CheckedChanged
		If eventSender.Checked Then
			If optEnd.Checked = True Then
				CmdNext.Visible = False
				CmdFinish.Visible = True
				CmdFinish.Enabled = True
			Else
				CmdNext.Visible = True
				CmdFinish.Visible = False
				CmdFinish.Enabled = False
			End If
		End If
	End Sub
End Class
