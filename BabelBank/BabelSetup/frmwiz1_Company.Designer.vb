<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmWiz1_Company
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents chkLockBabelBank As System.Windows.Forms.CheckBox
	Public WithEvents txtBaseCurrency As System.Windows.Forms.TextBox
	Public WithEvents chkDisableAdminMenus As System.Windows.Forms.CheckBox
	Public WithEvents txtMaxSizeDB As System.Windows.Forms.TextBox
	Public WithEvents cmdTest As System.Windows.Forms.Button
	Public WithEvents cmdEMail As System.Windows.Forms.Button
	Public WithEvents cmdCustom As System.Windows.Forms.Button
	Public WithEvents cmdFileOpen As System.Windows.Forms.Button
	Public WithEvents txtCompanyNo As System.Windows.Forms.TextBox
	Public WithEvents txtDelDays As System.Windows.Forms.TextBox
	Public WithEvents txtBackupPath As System.Windows.Forms.TextBox
	Public WithEvents txtCompany As System.Windows.Forms.TextBox
	Public WithEvents txtAdress1 As System.Windows.Forms.TextBox
	Public WithEvents txtAdress2 As System.Windows.Forms.TextBox
	Public WithEvents txtAdress3 As System.Windows.Forms.TextBox
	Public WithEvents chkBackup As System.Windows.Forms.CheckBox
	Public WithEvents txtAdditionalNo As System.Windows.Forms.TextBox
	Public WithEvents txtCity As System.Windows.Forms.TextBox
	Public WithEvents txtZip As System.Windows.Forms.TextBox
	Public WithEvents CmdBack As System.Windows.Forms.Button
	Public WithEvents CmdNext As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdFinish As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents lblBaseCurrency As System.Windows.Forms.Label
	Public WithEvents lblMb As System.Windows.Forms.Label
	Public WithEvents lblMaxSizeDB As System.Windows.Forms.Label
	Public WithEvents lblCompanyNo As System.Windows.Forms.Label
	Public WithEvents lblDelDays As System.Windows.Forms.Label
	Public WithEvents lblBackupPath As System.Windows.Forms.Label
	Public WithEvents lblAdditionalNo As System.Windows.Forms.Label
	Public WithEvents lblCompany As System.Windows.Forms.Label
	Public WithEvents lblAdress1 As System.Windows.Forms.Label
	Public WithEvents lblZip As System.Windows.Forms.Label
	Public WithEvents lblAdress3 As System.Windows.Forms.Label
	Public WithEvents lblAdress2 As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
	Public WithEvents lblCity As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWiz1_Company))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkLockBabelBank = New System.Windows.Forms.CheckBox
        Me.txtBaseCurrency = New System.Windows.Forms.TextBox
        Me.chkDisableAdminMenus = New System.Windows.Forms.CheckBox
        Me.txtMaxSizeDB = New System.Windows.Forms.TextBox
        Me.cmdTest = New System.Windows.Forms.Button
        Me.cmdEMail = New System.Windows.Forms.Button
        Me.cmdCustom = New System.Windows.Forms.Button
        Me.cmdFileOpen = New System.Windows.Forms.Button
        Me.txtCompanyNo = New System.Windows.Forms.TextBox
        Me.txtDelDays = New System.Windows.Forms.TextBox
        Me.txtBackupPath = New System.Windows.Forms.TextBox
        Me.txtCompany = New System.Windows.Forms.TextBox
        Me.txtAdress1 = New System.Windows.Forms.TextBox
        Me.txtAdress2 = New System.Windows.Forms.TextBox
        Me.txtAdress3 = New System.Windows.Forms.TextBox
        Me.chkBackup = New System.Windows.Forms.CheckBox
        Me.txtAdditionalNo = New System.Windows.Forms.TextBox
        Me.txtCity = New System.Windows.Forms.TextBox
        Me.txtZip = New System.Windows.Forms.TextBox
        Me.CmdBack = New System.Windows.Forms.Button
        Me.CmdNext = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdFinish = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.lblBaseCurrency = New System.Windows.Forms.Label
        Me.lblMb = New System.Windows.Forms.Label
        Me.lblMaxSizeDB = New System.Windows.Forms.Label
        Me.lblCompanyNo = New System.Windows.Forms.Label
        Me.lblDelDays = New System.Windows.Forms.Label
        Me.lblBackupPath = New System.Windows.Forms.Label
        Me.lblAdditionalNo = New System.Windows.Forms.Label
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblAdress1 = New System.Windows.Forms.Label
        Me.lblZip = New System.Windows.Forms.Label
        Me.lblAdress3 = New System.Windows.Forms.Label
        Me.lblAdress2 = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblCity = New System.Windows.Forms.Label
        Me.lblDivision = New System.Windows.Forms.Label
        Me.txtDivision = New System.Windows.Forms.TextBox
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.chkAfterExport = New System.Windows.Forms.CheckBox
        Me.chkBeforeExport = New System.Windows.Forms.CheckBox
        Me.chkBeforeImport = New System.Windows.Forms.CheckBox
        Me.cmbNoOfBackups = New System.Windows.Forms.ComboBox
        Me.txtPathForBackup = New System.Windows.Forms.TextBox
        Me.chkMakeCopyBeforeExport = New System.Windows.Forms.CheckBox
        Me.lblNoOfBackups = New System.Windows.Forms.Label
        Me.lblPathForBackup = New System.Windows.Forms.Label
        Me.cmdFileExportBackup = New System.Windows.Forms.Button
        Me.lblLine2 = New System.Windows.Forms.Label
        Me.lblLine3 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'chkLockBabelBank
        '
        Me.chkLockBabelBank.BackColor = System.Drawing.SystemColors.Control
        Me.chkLockBabelBank.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkLockBabelBank.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLockBabelBank.Location = New System.Drawing.Point(401, 454)
        Me.chkLockBabelBank.Name = "chkLockBabelBank"
        Me.chkLockBabelBank.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLockBabelBank.Size = New System.Drawing.Size(209, 23)
        Me.chkLockBabelBank.TabIndex = 21
        Me.chkLockBabelBank.Text = "60547 - Lock BabelBank"
        Me.chkLockBabelBank.UseVisualStyleBackColor = False
        '
        'txtBaseCurrency
        '
        Me.txtBaseCurrency.AcceptsReturn = True
        Me.txtBaseCurrency.BackColor = System.Drawing.SystemColors.Window
        Me.txtBaseCurrency.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBaseCurrency.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBaseCurrency.Location = New System.Drawing.Point(311, 241)
        Me.txtBaseCurrency.MaxLength = 3
        Me.txtBaseCurrency.Name = "txtBaseCurrency"
        Me.txtBaseCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBaseCurrency.Size = New System.Drawing.Size(40, 20)
        Me.txtBaseCurrency.TabIndex = 9
        '
        'chkDisableAdminMenus
        '
        Me.chkDisableAdminMenus.BackColor = System.Drawing.SystemColors.Control
        Me.chkDisableAdminMenus.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkDisableAdminMenus.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkDisableAdminMenus.Location = New System.Drawing.Point(200, 457)
        Me.chkDisableAdminMenus.Name = "chkDisableAdminMenus"
        Me.chkDisableAdminMenus.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkDisableAdminMenus.Size = New System.Drawing.Size(192, 17)
        Me.chkDisableAdminMenus.TabIndex = 20
        Me.chkDisableAdminMenus.Text = "60510 - DisableAdminMenus"
        Me.chkDisableAdminMenus.UseVisualStyleBackColor = False
        '
        'txtMaxSizeDB
        '
        Me.txtMaxSizeDB.AcceptsReturn = True
        Me.txtMaxSizeDB.BackColor = System.Drawing.SystemColors.Window
        Me.txtMaxSizeDB.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMaxSizeDB.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMaxSizeDB.Location = New System.Drawing.Point(489, 434)
        Me.txtMaxSizeDB.MaxLength = 7
        Me.txtMaxSizeDB.Name = "txtMaxSizeDB"
        Me.txtMaxSizeDB.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMaxSizeDB.Size = New System.Drawing.Size(40, 20)
        Me.txtMaxSizeDB.TabIndex = 19
        '
        'cmdTest
        '
        Me.cmdTest.BackColor = System.Drawing.SystemColors.Control
        Me.cmdTest.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdTest.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdTest.Location = New System.Drawing.Point(530, 167)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdTest.Size = New System.Drawing.Size(75, 21)
        Me.cmdTest.TabIndex = 17
        Me.cmdTest.TabStop = False
        Me.cmdTest.Text = "55010 - &Test"
        Me.cmdTest.UseVisualStyleBackColor = False
        '
        'cmdEMail
        '
        Me.cmdEMail.BackColor = System.Drawing.SystemColors.Control
        Me.cmdEMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdEMail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdEMail.Location = New System.Drawing.Point(530, 220)
        Me.cmdEMail.Name = "cmdEMail"
        Me.cmdEMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdEMail.Size = New System.Drawing.Size(75, 21)
        Me.cmdEMail.TabIndex = 19
        Me.cmdEMail.TabStop = False
        Me.cmdEMail.Text = "&EMail"
        Me.cmdEMail.UseVisualStyleBackColor = False
        '
        'cmdCustom
        '
        Me.cmdCustom.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCustom.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCustom.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCustom.Location = New System.Drawing.Point(530, 194)
        Me.cmdCustom.Name = "cmdCustom"
        Me.cmdCustom.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCustom.Size = New System.Drawing.Size(75, 21)
        Me.cmdCustom.TabIndex = 18
        Me.cmdCustom.TabStop = False
        Me.cmdCustom.Text = "60147-Egendefinert"
        Me.cmdCustom.UseVisualStyleBackColor = False
        '
        'cmdFileOpen
        '
        Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
        Me.cmdFileOpen.Location = New System.Drawing.Point(574, 297)
        Me.cmdFileOpen.Name = "cmdFileOpen"
        Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen.TabIndex = 13
        Me.cmdFileOpen.TabStop = False
        Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen.UseVisualStyleBackColor = False
        '
        'txtCompanyNo
        '
        Me.txtCompanyNo.AcceptsReturn = True
        Me.txtCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtCompanyNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCompanyNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCompanyNo.Location = New System.Drawing.Point(311, 166)
        Me.txtCompanyNo.MaxLength = 50
        Me.txtCompanyNo.Name = "txtCompanyNo"
        Me.txtCompanyNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCompanyNo.Size = New System.Drawing.Size(137, 20)
        Me.txtCompanyNo.TabIndex = 6
        '
        'txtDelDays
        '
        Me.txtDelDays.AcceptsReturn = True
        Me.txtDelDays.BackColor = System.Drawing.SystemColors.Window
        Me.txtDelDays.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDelDays.Enabled = False
        Me.txtDelDays.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDelDays.Location = New System.Drawing.Point(461, 273)
        Me.txtDelDays.MaxLength = 4
        Me.txtDelDays.Name = "txtDelDays"
        Me.txtDelDays.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDelDays.Size = New System.Drawing.Size(31, 20)
        Me.txtDelDays.TabIndex = 11
        '
        'txtBackupPath
        '
        Me.txtBackupPath.AcceptsReturn = True
        Me.txtBackupPath.BackColor = System.Drawing.SystemColors.Window
        Me.txtBackupPath.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBackupPath.Enabled = False
        Me.txtBackupPath.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBackupPath.Location = New System.Drawing.Point(312, 297)
        Me.txtBackupPath.MaxLength = 150
        Me.txtBackupPath.Name = "txtBackupPath"
        Me.txtBackupPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBackupPath.Size = New System.Drawing.Size(253, 20)
        Me.txtBackupPath.TabIndex = 12
        '
        'txtCompany
        '
        Me.txtCompany.AcceptsReturn = True
        Me.txtCompany.BackColor = System.Drawing.SystemColors.Window
        Me.txtCompany.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCompany.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCompany.Location = New System.Drawing.Point(311, 53)
        Me.txtCompany.MaxLength = 35
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCompany.Size = New System.Drawing.Size(253, 20)
        Me.txtCompany.TabIndex = 0
        '
        'txtAdress1
        '
        Me.txtAdress1.AcceptsReturn = True
        Me.txtAdress1.BackColor = System.Drawing.SystemColors.Window
        Me.txtAdress1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAdress1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAdress1.Location = New System.Drawing.Point(311, 75)
        Me.txtAdress1.MaxLength = 35
        Me.txtAdress1.Name = "txtAdress1"
        Me.txtAdress1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAdress1.Size = New System.Drawing.Size(253, 20)
        Me.txtAdress1.TabIndex = 1
        '
        'txtAdress2
        '
        Me.txtAdress2.AcceptsReturn = True
        Me.txtAdress2.BackColor = System.Drawing.SystemColors.Window
        Me.txtAdress2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAdress2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAdress2.Location = New System.Drawing.Point(311, 96)
        Me.txtAdress2.MaxLength = 35
        Me.txtAdress2.Name = "txtAdress2"
        Me.txtAdress2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAdress2.Size = New System.Drawing.Size(253, 20)
        Me.txtAdress2.TabIndex = 2
        '
        'txtAdress3
        '
        Me.txtAdress3.AcceptsReturn = True
        Me.txtAdress3.BackColor = System.Drawing.SystemColors.Window
        Me.txtAdress3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAdress3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAdress3.Location = New System.Drawing.Point(311, 117)
        Me.txtAdress3.MaxLength = 35
        Me.txtAdress3.Name = "txtAdress3"
        Me.txtAdress3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAdress3.Size = New System.Drawing.Size(253, 20)
        Me.txtAdress3.TabIndex = 3
        '
        'chkBackup
        '
        Me.chkBackup.BackColor = System.Drawing.SystemColors.Control
        Me.chkBackup.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkBackup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkBackup.Location = New System.Drawing.Point(200, 273)
        Me.chkBackup.Name = "chkBackup"
        Me.chkBackup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkBackup.Size = New System.Drawing.Size(97, 17)
        Me.chkBackup.TabIndex = 10
        Me.chkBackup.Text = "Backup"
        Me.chkBackup.UseVisualStyleBackColor = False
        '
        'txtAdditionalNo
        '
        Me.txtAdditionalNo.AcceptsReturn = True
        Me.txtAdditionalNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtAdditionalNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAdditionalNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAdditionalNo.Location = New System.Drawing.Point(311, 214)
        Me.txtAdditionalNo.MaxLength = 50
        Me.txtAdditionalNo.Name = "txtAdditionalNo"
        Me.txtAdditionalNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAdditionalNo.Size = New System.Drawing.Size(137, 20)
        Me.txtAdditionalNo.TabIndex = 8
        '
        'txtCity
        '
        Me.txtCity.AcceptsReturn = True
        Me.txtCity.BackColor = System.Drawing.SystemColors.Window
        Me.txtCity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCity.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCity.Location = New System.Drawing.Point(445, 139)
        Me.txtCity.MaxLength = 35
        Me.txtCity.Name = "txtCity"
        Me.txtCity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCity.Size = New System.Drawing.Size(120, 20)
        Me.txtCity.TabIndex = 5
        '
        'txtZip
        '
        Me.txtZip.AcceptsReturn = True
        Me.txtZip.BackColor = System.Drawing.SystemColors.Window
        Me.txtZip.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtZip.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtZip.Location = New System.Drawing.Point(311, 139)
        Me.txtZip.MaxLength = 10
        Me.txtZip.Name = "txtZip"
        Me.txtZip.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtZip.Size = New System.Drawing.Size(67, 20)
        Me.txtZip.TabIndex = 4
        '
        'CmdBack
        '
        Me.CmdBack.BackColor = System.Drawing.SystemColors.Control
        Me.CmdBack.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdBack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdBack.Location = New System.Drawing.Point(365, 497)
        Me.CmdBack.Name = "CmdBack"
        Me.CmdBack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdBack.Size = New System.Drawing.Size(73, 21)
        Me.CmdBack.TabIndex = 24
        Me.CmdBack.Text = "< &Forrige"
        Me.CmdBack.UseVisualStyleBackColor = False
        Me.CmdBack.Visible = False
        '
        'CmdNext
        '
        Me.CmdNext.BackColor = System.Drawing.SystemColors.Control
        Me.CmdNext.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdNext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdNext.Location = New System.Drawing.Point(440, 497)
        Me.CmdNext.Name = "CmdNext"
        Me.CmdNext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdNext.Size = New System.Drawing.Size(73, 21)
        Me.CmdNext.TabIndex = 25
        Me.CmdNext.Text = "&Neste >"
        Me.CmdNext.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(289, 497)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 23
        Me.CmdCancel.Text = "&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdFinish
        '
        Me.CmdFinish.BackColor = System.Drawing.SystemColors.Control
        Me.CmdFinish.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdFinish.Enabled = False
        Me.CmdFinish.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdFinish.Location = New System.Drawing.Point(530, 497)
        Me.CmdFinish.Name = "CmdFinish"
        Me.CmdFinish.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdFinish.Size = New System.Drawing.Size(73, 21)
        Me.CmdFinish.TabIndex = 26
        Me.CmdFinish.Text = "F&ullf�r"
        Me.CmdFinish.UseVisualStyleBackColor = False
        Me.CmdFinish.Visible = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(144, 497)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 22
        Me.CmdHelp.Text = "&Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'lblBaseCurrency
        '
        Me.lblBaseCurrency.BackColor = System.Drawing.SystemColors.Control
        Me.lblBaseCurrency.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBaseCurrency.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBaseCurrency.Location = New System.Drawing.Point(200, 241)
        Me.lblBaseCurrency.Name = "lblBaseCurrency"
        Me.lblBaseCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBaseCurrency.Size = New System.Drawing.Size(104, 17)
        Me.lblBaseCurrency.TabIndex = 36
        Me.lblBaseCurrency.Text = "60512 - Basisvaluta"
        '
        'lblMb
        '
        Me.lblMb.BackColor = System.Drawing.SystemColors.Control
        Me.lblMb.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMb.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMb.Location = New System.Drawing.Point(532, 434)
        Me.lblMb.Name = "lblMb"
        Me.lblMb.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMb.Size = New System.Drawing.Size(25, 19)
        Me.lblMb.TabIndex = 35
        Me.lblMb.Text = "Mb"
        '
        'lblMaxSizeDB
        '
        Me.lblMaxSizeDB.BackColor = System.Drawing.SystemColors.Control
        Me.lblMaxSizeDB.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMaxSizeDB.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMaxSizeDB.Location = New System.Drawing.Point(200, 434)
        Me.lblMaxSizeDB.Name = "lblMaxSizeDB"
        Me.lblMaxSizeDB.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMaxSizeDB.Size = New System.Drawing.Size(283, 19)
        Me.lblMaxSizeDB.TabIndex = 34
        Me.lblMaxSizeDB.Text = "Maksimal st�rrelse p� BabelBank sin database:"
        '
        'lblCompanyNo
        '
        Me.lblCompanyNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblCompanyNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompanyNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompanyNo.Location = New System.Drawing.Point(200, 166)
        Me.lblCompanyNo.Name = "lblCompanyNo"
        Me.lblCompanyNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompanyNo.Size = New System.Drawing.Size(104, 19)
        Me.lblCompanyNo.TabIndex = 29
        Me.lblCompanyNo.Text = "Foretaksnummer"
        '
        'lblDelDays
        '
        Me.lblDelDays.BackColor = System.Drawing.SystemColors.Control
        Me.lblDelDays.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDelDays.Enabled = False
        Me.lblDelDays.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDelDays.Location = New System.Drawing.Point(312, 273)
        Me.lblDelDays.Name = "lblDelDays"
        Me.lblDelDays.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDelDays.Size = New System.Drawing.Size(145, 17)
        Me.lblDelDays.TabIndex = 28
        Me.lblDelDays.Text = "Slett etter antall dager:"
        '
        'lblBackupPath
        '
        Me.lblBackupPath.AutoSize = True
        Me.lblBackupPath.BackColor = System.Drawing.SystemColors.Control
        Me.lblBackupPath.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBackupPath.Enabled = False
        Me.lblBackupPath.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBackupPath.Location = New System.Drawing.Point(200, 298)
        Me.lblBackupPath.Name = "lblBackupPath"
        Me.lblBackupPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBackupPath.Size = New System.Drawing.Size(79, 13)
        Me.lblBackupPath.TabIndex = 27
        Me.lblBackupPath.Text = "Backupkatalog"
        '
        'lblAdditionalNo
        '
        Me.lblAdditionalNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdditionalNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdditionalNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdditionalNo.Location = New System.Drawing.Point(200, 214)
        Me.lblAdditionalNo.Name = "lblAdditionalNo"
        Me.lblAdditionalNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdditionalNo.Size = New System.Drawing.Size(104, 17)
        Me.lblAdditionalNo.TabIndex = 26
        Me.lblAdditionalNo.Text = "Kunde ID"
        '
        'lblCompany
        '
        Me.lblCompany.BackColor = System.Drawing.SystemColors.Control
        Me.lblCompany.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompany.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompany.Location = New System.Drawing.Point(200, 56)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompany.Size = New System.Drawing.Size(104, 17)
        Me.lblCompany.TabIndex = 25
        Me.lblCompany.Text = "Firma:"
        '
        'lblAdress1
        '
        Me.lblAdress1.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdress1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdress1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdress1.Location = New System.Drawing.Point(200, 77)
        Me.lblAdress1.Name = "lblAdress1"
        Me.lblAdress1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdress1.Size = New System.Drawing.Size(104, 17)
        Me.lblAdress1.TabIndex = 24
        Me.lblAdress1.Text = "Adresse:"
        '
        'lblZip
        '
        Me.lblZip.BackColor = System.Drawing.SystemColors.Control
        Me.lblZip.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblZip.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblZip.Location = New System.Drawing.Point(200, 141)
        Me.lblZip.Name = "lblZip"
        Me.lblZip.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblZip.Size = New System.Drawing.Size(104, 17)
        Me.lblZip.TabIndex = 23
        Me.lblZip.Text = "Postnummer:"
        '
        'lblAdress3
        '
        Me.lblAdress3.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdress3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdress3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdress3.Location = New System.Drawing.Point(200, 120)
        Me.lblAdress3.Name = "lblAdress3"
        Me.lblAdress3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdress3.Size = New System.Drawing.Size(104, 17)
        Me.lblAdress3.TabIndex = 22
        Me.lblAdress3.Text = "Adresse:"
        '
        'lblAdress2
        '
        Me.lblAdress2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdress2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdress2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdress2.Location = New System.Drawing.Point(200, 99)
        Me.lblAdress2.Name = "lblAdress2"
        Me.lblAdress2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdress2.Size = New System.Drawing.Size(104, 17)
        Me.lblAdress2.TabIndex = 21
        Me.lblAdress2.Text = "Adresse:"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(200, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(393, 17)
        Me.lblHeading.TabIndex = 20
        Me.lblHeading.Text = "60039 - Legg inn firmainformasjon"
        '
        'lblCity
        '
        Me.lblCity.BackColor = System.Drawing.SystemColors.Control
        Me.lblCity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCity.Location = New System.Drawing.Point(392, 141)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCity.Size = New System.Drawing.Size(50, 17)
        Me.lblCity.TabIndex = 19
        Me.lblCity.Text = "Poststed:"
        '
        'lblDivision
        '
        Me.lblDivision.BackColor = System.Drawing.SystemColors.Control
        Me.lblDivision.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDivision.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDivision.Location = New System.Drawing.Point(200, 190)
        Me.lblDivision.Name = "lblDivision"
        Me.lblDivision.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDivision.Size = New System.Drawing.Size(104, 19)
        Me.lblDivision.TabIndex = 39
        Me.lblDivision.Text = "60581 - Divisjon"
        '
        'txtDivision
        '
        Me.txtDivision.AcceptsReturn = True
        Me.txtDivision.BackColor = System.Drawing.SystemColors.Window
        Me.txtDivision.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDivision.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDivision.Location = New System.Drawing.Point(311, 190)
        Me.txtDivision.MaxLength = 50
        Me.txtDivision.Name = "txtDivision"
        Me.txtDivision.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDivision.Size = New System.Drawing.Size(137, 20)
        Me.txtDivision.TabIndex = 7
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 488)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(600, 1)
        Me.lblLine1.TabIndex = 79
        Me.lblLine1.Text = "Label1"
        '
        'chkAfterExport
        '
        Me.chkAfterExport.BackColor = System.Drawing.SystemColors.Control
        Me.chkAfterExport.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkAfterExport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAfterExport.Location = New System.Drawing.Point(497, 401)
        Me.chkAfterExport.Name = "chkAfterExport"
        Me.chkAfterExport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAfterExport.Size = New System.Drawing.Size(134, 16)
        Me.chkAfterExport.TabIndex = 18
        Me.chkAfterExport.Text = "60594 - After export"
        Me.chkAfterExport.UseVisualStyleBackColor = False
        '
        'chkBeforeExport
        '
        Me.chkBeforeExport.BackColor = System.Drawing.SystemColors.Control
        Me.chkBeforeExport.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkBeforeExport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkBeforeExport.Location = New System.Drawing.Point(350, 401)
        Me.chkBeforeExport.Name = "chkBeforeExport"
        Me.chkBeforeExport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkBeforeExport.Size = New System.Drawing.Size(134, 16)
        Me.chkBeforeExport.TabIndex = 17
        Me.chkBeforeExport.Text = "60593 - Before export"
        Me.chkBeforeExport.UseVisualStyleBackColor = False
        '
        'chkBeforeImport
        '
        Me.chkBeforeImport.BackColor = System.Drawing.SystemColors.Control
        Me.chkBeforeImport.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkBeforeImport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkBeforeImport.Location = New System.Drawing.Point(203, 401)
        Me.chkBeforeImport.Name = "chkBeforeImport"
        Me.chkBeforeImport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkBeforeImport.Size = New System.Drawing.Size(134, 16)
        Me.chkBeforeImport.TabIndex = 16
        Me.chkBeforeImport.Text = "60592 - Before import"
        Me.chkBeforeImport.UseVisualStyleBackColor = False
        '
        'cmbNoOfBackups
        '
        Me.cmbNoOfBackups.BackColor = System.Drawing.SystemColors.Window
        Me.cmbNoOfBackups.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbNoOfBackups.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbNoOfBackups.Location = New System.Drawing.Point(433, 378)
        Me.cmbNoOfBackups.Name = "cmbNoOfBackups"
        Me.cmbNoOfBackups.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbNoOfBackups.Size = New System.Drawing.Size(40, 21)
        Me.cmbNoOfBackups.TabIndex = 15
        '
        'txtPathForBackup
        '
        Me.txtPathForBackup.AcceptsReturn = True
        Me.txtPathForBackup.BackColor = System.Drawing.SystemColors.Window
        Me.txtPathForBackup.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPathForBackup.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPathForBackup.Location = New System.Drawing.Point(312, 353)
        Me.txtPathForBackup.MaxLength = 0
        Me.txtPathForBackup.Name = "txtPathForBackup"
        Me.txtPathForBackup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPathForBackup.Size = New System.Drawing.Size(253, 20)
        Me.txtPathForBackup.TabIndex = 14
        '
        'chkMakeCopyBeforeExport
        '
        Me.chkMakeCopyBeforeExport.BackColor = System.Drawing.SystemColors.Control
        Me.chkMakeCopyBeforeExport.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkMakeCopyBeforeExport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkMakeCopyBeforeExport.Location = New System.Drawing.Point(200, 332)
        Me.chkMakeCopyBeforeExport.Name = "chkMakeCopyBeforeExport"
        Me.chkMakeCopyBeforeExport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkMakeCopyBeforeExport.Size = New System.Drawing.Size(419, 16)
        Me.chkMakeCopyBeforeExport.TabIndex = 13
        Me.chkMakeCopyBeforeExport.Text = "60589 -Make a copy of the database"
        Me.chkMakeCopyBeforeExport.UseVisualStyleBackColor = False
        '
        'lblNoOfBackups
        '
        Me.lblNoOfBackups.BackColor = System.Drawing.SystemColors.Control
        Me.lblNoOfBackups.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNoOfBackups.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNoOfBackups.Location = New System.Drawing.Point(200, 379)
        Me.lblNoOfBackups.Name = "lblNoOfBackups"
        Me.lblNoOfBackups.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNoOfBackups.Size = New System.Drawing.Size(210, 19)
        Me.lblNoOfBackups.TabIndex = 87
        Me.lblNoOfBackups.Text = "60591 - Number of backup generations"
        '
        'lblPathForBackup
        '
        Me.lblPathForBackup.BackColor = System.Drawing.SystemColors.Control
        Me.lblPathForBackup.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPathForBackup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPathForBackup.Location = New System.Drawing.Point(200, 353)
        Me.lblPathForBackup.Name = "lblPathForBackup"
        Me.lblPathForBackup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPathForBackup.Size = New System.Drawing.Size(107, 25)
        Me.lblPathForBackup.TabIndex = 83
        Me.lblPathForBackup.Text = "60590 - Backuppath"
        '
        'cmdFileExportBackup
        '
        Me.cmdFileExportBackup.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileExportBackup.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileExportBackup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileExportBackup.Image = CType(resources.GetObject("cmdFileExportBackup.Image"), System.Drawing.Image)
        Me.cmdFileExportBackup.Location = New System.Drawing.Point(574, 349)
        Me.cmdFileExportBackup.Name = "cmdFileExportBackup"
        Me.cmdFileExportBackup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileExportBackup.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileExportBackup.TabIndex = 88
        Me.cmdFileExportBackup.TabStop = False
        Me.cmdFileExportBackup.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileExportBackup.UseVisualStyleBackColor = False
        '
        'lblLine2
        '
        Me.lblLine2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine2.Location = New System.Drawing.Point(200, 327)
        Me.lblLine2.Name = "lblLine2"
        Me.lblLine2.Size = New System.Drawing.Size(400, 1)
        Me.lblLine2.TabIndex = 89
        Me.lblLine2.Text = "Label1"
        '
        'lblLine3
        '
        Me.lblLine3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine3.Location = New System.Drawing.Point(198, 426)
        Me.lblLine3.Name = "lblLine3"
        Me.lblLine3.Size = New System.Drawing.Size(400, 1)
        Me.lblLine3.TabIndex = 90
        Me.lblLine3.Text = "Label1"
        '
        'frmWiz1_Company
        '
        Me.AcceptButton = Me.CmdNext
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(642, 530)
        Me.Controls.Add(Me.lblLine3)
        Me.Controls.Add(Me.lblLine2)
        Me.Controls.Add(Me.cmdFileExportBackup)
        Me.Controls.Add(Me.chkAfterExport)
        Me.Controls.Add(Me.chkBeforeExport)
        Me.Controls.Add(Me.chkBeforeImport)
        Me.Controls.Add(Me.cmbNoOfBackups)
        Me.Controls.Add(Me.txtPathForBackup)
        Me.Controls.Add(Me.chkMakeCopyBeforeExport)
        Me.Controls.Add(Me.lblNoOfBackups)
        Me.Controls.Add(Me.lblPathForBackup)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtDivision)
        Me.Controls.Add(Me.lblDivision)
        Me.Controls.Add(Me.chkLockBabelBank)
        Me.Controls.Add(Me.txtBaseCurrency)
        Me.Controls.Add(Me.chkDisableAdminMenus)
        Me.Controls.Add(Me.txtMaxSizeDB)
        Me.Controls.Add(Me.cmdTest)
        Me.Controls.Add(Me.cmdEMail)
        Me.Controls.Add(Me.cmdCustom)
        Me.Controls.Add(Me.cmdFileOpen)
        Me.Controls.Add(Me.txtCompanyNo)
        Me.Controls.Add(Me.txtDelDays)
        Me.Controls.Add(Me.txtBackupPath)
        Me.Controls.Add(Me.txtCompany)
        Me.Controls.Add(Me.txtAdress1)
        Me.Controls.Add(Me.txtAdress2)
        Me.Controls.Add(Me.txtAdress3)
        Me.Controls.Add(Me.chkBackup)
        Me.Controls.Add(Me.txtAdditionalNo)
        Me.Controls.Add(Me.txtCity)
        Me.Controls.Add(Me.txtZip)
        Me.Controls.Add(Me.CmdBack)
        Me.Controls.Add(Me.CmdNext)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdFinish)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.lblBaseCurrency)
        Me.Controls.Add(Me.lblMb)
        Me.Controls.Add(Me.lblMaxSizeDB)
        Me.Controls.Add(Me.lblCompanyNo)
        Me.Controls.Add(Me.lblDelDays)
        Me.Controls.Add(Me.lblBackupPath)
        Me.Controls.Add(Me.lblAdditionalNo)
        Me.Controls.Add(Me.lblCompany)
        Me.Controls.Add(Me.lblAdress1)
        Me.Controls.Add(Me.lblZip)
        Me.Controls.Add(Me.lblAdress3)
        Me.Controls.Add(Me.lblAdress2)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.lblCity)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmWiz1_Company"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Firmaopplysninger"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents lblDivision As System.Windows.Forms.Label
    Public WithEvents txtDivision As System.Windows.Forms.TextBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents chkAfterExport As System.Windows.Forms.CheckBox
    Public WithEvents chkBeforeExport As System.Windows.Forms.CheckBox
    Public WithEvents chkBeforeImport As System.Windows.Forms.CheckBox
    Public WithEvents cmbNoOfBackups As System.Windows.Forms.ComboBox
    Public WithEvents txtPathForBackup As System.Windows.Forms.TextBox
    Public WithEvents chkMakeCopyBeforeExport As System.Windows.Forms.CheckBox
    Public WithEvents lblNoOfBackups As System.Windows.Forms.Label
    Public WithEvents lblPathForBackup As System.Windows.Forms.Label
    Public WithEvents cmdFileExportBackup As System.Windows.Forms.Button
    Friend WithEvents lblLine2 As System.Windows.Forms.Label
    Friend WithEvents lblLine3 As System.Windows.Forms.Label
#End Region 
End Class
