﻿Option Strict Off
Option Explicit On

Public Class frmDimensions
    Inherits System.Windows.Forms.Form
    Private oMyDal As vbBabel.DAL
    Private bAnythingChanged As Boolean
    Private frmClientDetailISO20022 As frmClientDetailISO20022 = Nothing

    Private sMySQL As String
    Private iAccount_ID As Integer
    Private iDiffAccount_ID As Integer
    Private iAutoBook_ID As Integer
    Private iClient_ID As Integer
    Private sMode As String  ' ACCOUNT, DIFFACCOUNT AND AUTOBOOK (Same form used for all) 10.03.2016 RESULTACCOUNT

    Public Sub SetAccount_ID(ByVal iA_Id As Integer)
        iAccount_ID = iA_Id
        sMode = "ACCOUNT"
    End Sub
    Public Sub SetResultAccount_ID(ByVal iA_Id As Integer)
        iAccount_ID = iA_Id
        sMode = "RESULTACCOUNT"
    End Sub
    Public Sub SetDiffAccount_ID(ByVal iA_Id As Integer)
        iDiffAccount_ID = iA_Id
        sMode = "DIFFACCOUNT"
    End Sub
    Public Sub SetAutoBookAccount_ID(ByVal iA_Id As Integer) 'Added 18.02.2016
        iAutoBook_ID = iA_Id
        sMode = "AUTOBOOK"
    End Sub
    Public Sub SetClient_ID(ByVal iC_Id As Integer)
        iClient_ID = iC_Id
    End Sub
    Private Function SaveCurrentAccountDimensionSettings(Optional ByRef bAsk As Boolean = True) As Boolean
        Dim bYes As Boolean
        Dim sCompanyID As String = "1" 'TODO: Hardcoded Company_ID
        Dim bNew As Boolean = False
        Dim sFieldPrefix As String = ""  ' added 10.03.2016
        bYes = True

        If bAnythingChanged Then
            If bAsk Then
                bYes = MsgBox(LRS(60638) & " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes 'Save OCRSettings
            Else
                bYes = True
            End If
            If bYes Then

                ' Save all DIMSettings
                If sMode = "ACCOUNT" Then
                    sMySQL = "UPDATE Account SET "
                ElseIf sMode = "RESULTACCOUNT" Then   ' added 10.03.2016
                    sMySQL = "UPDATE Account SET "
                    sFieldPrefix = "GLResults"
                ElseIf sMode = "DIFFACCOUNT" Then
                    ' Diffaccounts
                    sMySQL = "UPDATE MATCH_DiffAccounts SET "
                Else 'AUTOBOOK
                    sMySQL = "UPDATE Autobook SET "
                End If

                sMySQL = sMySQL & sFieldPrefix & "Dim1 = '" & Me.txtDim1.Text.Trim & "', "
                sMySQL = sMySQL & sFieldPrefix & "Dim2 = '" & Me.txtDim2.Text.Trim & "', "
                sMySQL = sMySQL & sFieldPrefix & "Dim3 = '" & Me.txtDim3.Text.Trim & "', "
                sMySQL = sMySQL & sFieldPrefix & "Dim4 = '" & Me.txtDim4.Text.Trim & "', "
                sMySQL = sMySQL & sFieldPrefix & "Dim5 = '" & Me.txtDim5.Text.Trim & "', "
                sMySQL = sMySQL & sFieldPrefix & "Dim6 = '" & Me.txtDim6.Text.Trim & "', "
                sMySQL = sMySQL & sFieldPrefix & "Dim7 = '" & Me.txtDim7.Text.Trim & "', "
                sMySQL = sMySQL & sFieldPrefix & "Dim8 = '" & Me.txtDim8.Text.Trim & "', "
                sMySQL = sMySQL & sFieldPrefix & "Dim9 = '" & Me.txtDim9.Text.Trim & "', "
                sMySQL = sMySQL & sFieldPrefix & "Dim10= '" & Me.txtDim10.Text.Trim & "' "
                If sMode = "ACCOUNT" Or sMode = "RESULTACCOUNT" Then  ' 10.03.2016 added  sMode = "RESULTACCOUNT"
                    sMySQL = sMySQL & "WHERE Account_Id = " & CStr(iAccount_ID)
                ElseIf sMode = "DIFFACCOUNT" Then
                    ' Diffaccounts
                    sMySQL = sMySQL & "WHERE DiffAccount_Id = " & CStr(iDiffAccount_ID)
                Else 'AUTOBOOK
                    sMySQL = sMySQL & "WHERE Autobook_Id = " & iAutoBook_ID.ToString
                End If
                sMySQL = sMySQL & " AND Client_Id = " & CStr(iClient_ID)

                oMyDal.SQL = sMySQL
                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        'OK
                    Else
                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

            End If ' YesNo to save client?
        End If ' bAnythingChanged?

        If bYes Then
            bAnythingChanged = False
        End If
        SaveCurrentAccountDimensionSettings = bYes

    End Function
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        Dim bOKToEnd As Boolean

        bOKToEnd = True
        If bAnythingChanged Then
            If SaveCurrentAccountDimensionSettings(True) Then '
                If Not oMyDal Is Nothing Then
                    oMyDal.Close()
                    oMyDal = Nothing
                End If
            End If
        End If


        Me.Close()
    End Sub
    Public Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        ' Save dimensionssettings to database

        If SaveCurrentAccountDimensionSettings(False) Then '

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Me.Close()
        End If

    End Sub
    Private Sub frmDimensions_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        Dim sDummy As String

        ' Open BabelBanks database
        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        'oMyDal.TEST_UseCommitFromOutside = True
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If
        FormvbStyle(Me, "", 600)
        FormLRSCaptions(Me)
        ' draw a line;
        'Dim pen As New Pen(Color.FromArgb(255, 0, 0, 0))
        'e.Graphics.DrawLine(pen, 20, 10, 300, 100)

        ' 11.01.2011 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next

        ' If user has set speciallabels for dimensions, use them as labeltexts in frmDimensions;
        RetrieveLabels(oMyDal, "1", sDummy, sDummy, sDummy, _
                              sDummy, sDummy, sDummy, sDummy, sDummy, sDummy, _
                              sDummy, sDummy, sDummy, sDummy, sDummy, sDummy, _
                              sDummy, sDummy, Me.lblDim1.Text, Me.lblDim2.Text, Me.lblDim3.Text, Me.lblDim4.Text, _
                              Me.lblDim5.Text, Me.lblDim6.Text, Me.lblDim7.Text, Me.lblDim8.Text, Me.lblDim9.Text, Me.lblDim10.Text)


        ' load labels values set by user;
        FillAccountDimensions()
        bAnythingChanged = False
    End Sub
    Private Sub thisForm_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        e.Graphics.DrawLine(Pens.Orange, 56, 382, 411, 382)
    End Sub
  
    Private Sub FillAccountDimensions()
        Dim sFieldPrefix As String = ""  ' added 10.03.2016
        Try
            If sMode = "ACCOUNT" Then
                ' Dimensions for BankAccount
                sMySQL = "SELECT Dim1, Dim2, Dim3, Dim4, Dim5, Dim6, Dim7, Dim8, Dim9, Dim10 FROM Account WHERE Company_ID = 1 AND Account_ID = " & iAccount_ID.ToString & " AND Client_ID = " & iClient_ID.ToString 'TODO: Hardcoded CompanyID
            ElseIf sMode = "RESULTACCOUNT" Then
                ' Dimensions for GLResultAccount (Motkonto)
                sFieldPrefix = "GLResults"  ' added 10.03.2016
                sMySQL = "SELECT GLResultsDim1, GLResultsDim2, GLResultsDim3, GLResultsDim4, GLResultsDim5, GLResultsDim6, GLResultsDim7, GLResultsDim8, GLResultsDim9, GLResultsDim10 FROM Account WHERE Company_ID = 1 AND Account_ID = " & iAccount_ID.ToString & " AND Client_ID = " & iClient_ID.ToString 'TODO: Hardcoded CompanyID
            ElseIf sMode = "DIFFACCOUNT" Then
                ' Diffaccounts
                sMySQL = "SELECT Dim1, Dim2, Dim3, Dim4, Dim5, Dim6, Dim7, Dim8, Dim9, Dim10 FROM Match_DiffAccounts WHERE Company_ID = 1 AND DiffAccount_ID = " & iDiffAccount_ID.ToString & " AND Client_ID = " & iClient_ID.ToString 'TODO: Hardcoded CompanyID
            Else ' Autobook added 18.02.2016
                sMySQL = "SELECT Dim1, Dim2, Dim3, Dim4, Dim5, Dim6, Dim7, Dim8, Dim9, Dim10 FROM AutoBook WHERE Company_ID = 1 AND AutoBook_ID = " & iAutoBook_ID.ToString & " AND Client_ID = " & iClient_ID.ToString 'TODO: Hardcoded CompanyID
            End If

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then
                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord
                        Me.txtDim1.Text = oMyDal.Reader_GetString(sFieldPrefix & "Dim1")
                        Me.txtDim2.Text = oMyDal.Reader_GetString(sFieldPrefix & "Dim2")
                        Me.txtDim3.Text = oMyDal.Reader_GetString(sFieldPrefix & "Dim3")
                        Me.txtDim4.Text = oMyDal.Reader_GetString(sFieldPrefix & "Dim4")
                        Me.txtDim5.Text = oMyDal.Reader_GetString(sFieldPrefix & "Dim5")
                        Me.txtDim6.Text = oMyDal.Reader_GetString(sFieldPrefix & "Dim6")
                        Me.txtDim7.Text = oMyDal.Reader_GetString(sFieldPrefix & "Dim7")
                        Me.txtDim8.Text = oMyDal.Reader_GetString(sFieldPrefix & "Dim8")
                        Me.txtDim9.Text = oMyDal.Reader_GetString(sFieldPrefix & "Dim9")
                        Me.txtDim10.Text = oMyDal.Reader_GetString(sFieldPrefix & "Dim10")
                        bAnythingChanged = False
                        Exit Do
                    Loop
                Else
                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                End If

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New System.Exception("Function: FillAccountDimensions." & vbCrLf & ex.Message & vbCrLf & "SQL: " & sMySQL)

        End Try

    End Sub
    Private Sub txtDim_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDim1.TextChanged, txtDim2.TextChanged, txtDim3.TextChanged, txtDim4.TextChanged, txtDim5.TextChanged, txtDim6.TextChanged, txtDim7.TextChanged, txtDim8.TextChanged, txtDim9.TextChanged, txtDim10.TextChanged
        bAnythingChanged = True
    End Sub

End Class