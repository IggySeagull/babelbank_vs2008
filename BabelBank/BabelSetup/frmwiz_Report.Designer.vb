<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmWiz_Report
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents CmdBack As System.Windows.Forms.Button
	Public WithEvents CmdNext As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdFinish As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents lblDescr As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.CmdBack = New System.Windows.Forms.Button
        Me.CmdNext = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdFinish = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblDescr = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lstReport = New System.Windows.Forms.ListBox
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.cmdChange = New System.Windows.Forms.Button
        Me.cmdRemove = New System.Windows.Forms.Button
        Me.lblLine1 = New System.Windows.Forms.Label
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CmdBack
        '
        Me.CmdBack.BackColor = System.Drawing.SystemColors.Control
        Me.CmdBack.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdBack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdBack.Location = New System.Drawing.Point(309, 303)
        Me.CmdBack.Name = "CmdBack"
        Me.CmdBack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdBack.Size = New System.Drawing.Size(73, 21)
        Me.CmdBack.TabIndex = 10
        Me.CmdBack.Text = "55003-< &Forrige"
        Me.CmdBack.UseVisualStyleBackColor = False
        '
        'CmdNext
        '
        Me.CmdNext.BackColor = System.Drawing.SystemColors.Control
        Me.CmdNext.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdNext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdNext.Location = New System.Drawing.Point(383, 303)
        Me.CmdNext.Name = "CmdNext"
        Me.CmdNext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdNext.Size = New System.Drawing.Size(73, 21)
        Me.CmdNext.TabIndex = 0
        Me.CmdNext.Text = "55004-&Neste >"
        Me.CmdNext.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(232, 303)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 9
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdFinish
        '
        Me.CmdFinish.BackColor = System.Drawing.SystemColors.Control
        Me.CmdFinish.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdFinish.Enabled = False
        Me.CmdFinish.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdFinish.Location = New System.Drawing.Point(487, 303)
        Me.CmdFinish.Name = "CmdFinish"
        Me.CmdFinish.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdFinish.Size = New System.Drawing.Size(73, 21)
        Me.CmdFinish.TabIndex = 11
        Me.CmdFinish.Text = "55005-F&ullf�r"
        Me.CmdFinish.UseVisualStyleBackColor = False
        Me.CmdFinish.Visible = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(109, 303)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 0
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "55001-&Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(113, 255)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(14, 16)
        Me._Image1_0.TabIndex = 22
        Me._Image1_0.TabStop = False
        '
        'lblDescr
        '
        Me.lblDescr.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescr.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescr.Location = New System.Drawing.Point(184, 69)
        Me.lblDescr.Name = "lblDescr"
        Me.lblDescr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescr.Size = New System.Drawing.Size(385, 19)
        Me.lblDescr.TabIndex = 14
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(213, 15)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(341, 17)
        Me.lblHeading.TabIndex = 13
        Me.lblHeading.Text = "60267 - Rapporter for sendeprofil"
        '
        'lstReport
        '
        Me.lstReport.FormattingEnabled = True
        Me.lstReport.Location = New System.Drawing.Point(187, 91)
        Me.lstReport.Name = "lstReport"
        Me.lstReport.Size = New System.Drawing.Size(285, 186)
        Me.lstReport.TabIndex = 24
        '
        'cmdAdd
        '
        Me.cmdAdd.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAdd.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAdd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAdd.Location = New System.Drawing.Point(487, 91)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAdd.Size = New System.Drawing.Size(73, 21)
        Me.cmdAdd.TabIndex = 25
        Me.cmdAdd.Text = "55018-Legg til"
        Me.cmdAdd.UseVisualStyleBackColor = False
        '
        'cmdChange
        '
        Me.cmdChange.BackColor = System.Drawing.SystemColors.Control
        Me.cmdChange.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdChange.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdChange.Location = New System.Drawing.Point(487, 118)
        Me.cmdChange.Name = "cmdChange"
        Me.cmdChange.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdChange.Size = New System.Drawing.Size(73, 21)
        Me.cmdChange.TabIndex = 26
        Me.cmdChange.Text = "60017-Endre"
        Me.cmdChange.UseVisualStyleBackColor = False
        '
        'cmdRemove
        '
        Me.cmdRemove.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemove.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRemove.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRemove.Location = New System.Drawing.Point(487, 145)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRemove.Size = New System.Drawing.Size(73, 21)
        Me.cmdRemove.TabIndex = 27
        Me.cmdRemove.Text = "55061-Fjern"
        Me.cmdRemove.UseVisualStyleBackColor = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(15, 293)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(545, 1)
        Me.lblLine1.TabIndex = 79
        Me.lblLine1.Text = "Label1"
        '
        'frmWiz_Report
        '
        Me.AcceptButton = Me.CmdNext
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(574, 335)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdRemove)
        Me.Controls.Add(Me.cmdChange)
        Me.Controls.Add(Me.cmdAdd)
        Me.Controls.Add(Me.lstReport)
        Me.Controls.Add(Me.CmdBack)
        Me.Controls.Add(Me.CmdNext)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdFinish)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblDescr)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmWiz_Report"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "60266 - Rapporter for sendefiler"
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lstReport As System.Windows.Forms.ListBox
    Public WithEvents cmdAdd As System.Windows.Forms.Button
    Public WithEvents cmdChange As System.Windows.Forms.Button
    Public WithEvents cmdRemove As System.Windows.Forms.Button
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region
End Class
