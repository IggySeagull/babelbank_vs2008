Option Strict Off
Option Explicit On
'Imports VB = Microsoft.VisualBasic

Friend Class frmClientSettings
    Inherits System.Windows.Forms.Form

    Public iClient_ID As Short
    Private iAccount_ID As Short
    Private iProfile_ID As Short
    Public iDBProfile_ID As Short
    Public iPatternGroup_ID As Short

    Private oMyDal As vbBabel.DAL

    Private bAnythingChanged As Boolean
    Public bPatternChanged As Boolean
    Private bShowAccounts As Boolean ' Have we used frmAccounts?
    Private bClientSavedAlready As Boolean  ' Used in SaveClient, to just jump out if client is already saved

    Private aDiffAccountsDeleted() As Short
    Private aAccountsDeleted() As Short
    Private aPatternGroupsDeleted() As Short
    Private sClientNo As String
    Private sClientName As String
    Private bDontBotherHandlingKeypress As Boolean
    Private frmClientDetail As New frmClientDetail
    Private frmClientDetailISO20022 As New frmClientDetailISO20022
    Private frmClientNotifications As New frmClientNotifications
    Private frmClientProfiles As New frmClientProfiles
    Private frmClientAutoBook As New frmClientAutoBook
    Private frmAccountOCRSettings As New frmAccountOCRSettings
    Private frmPatterns As New frmPatterns
    Private frmMatchDiff As New frmMatchDiff
    Private frmDiffAccountDetail As New frmDiffAccountDetail
    Private frmAccountDetail As New frmAccountDetail
    Private listitem As New _MyListBoxItem

    Private Sub chkUnmatchedAccount_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkUnmatchedAccount.CheckStateChanged
        Me.bAnythingChanged = True
        If Me.chkUnmatchedAccount.CheckState = 1 Then
            ' Can't have both chkUnmatchedAccount and chkCashdiscountAccount checked
            Me.chkCashDiscountAccount.CheckState = System.Windows.Forms.CheckState.Unchecked
        End If
    End Sub
    Private Sub chkCashdiscountAccount_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkCashDiscountAccount.CheckStateChanged
        Me.bAnythingChanged = True
        If Me.chkCashDiscountAccount.CheckState = 1 Then
            ' Can't have both chkUnmatchedAccount and chkCashdiscountAccount checked
            Me.chkUnmatchedAccount.CheckState = System.Windows.Forms.CheckState.Unchecked
        End If

    End Sub
    Private Sub cmbdbProfiles_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbdbProfiles.SelectedIndexChanged
        Dim iCounter As Integer

        'iDBProfile_ID = VB6.GetItemData(Me.cmbdbProfiles, Me.cmbdbProfiles.SelectedIndex)
        ' 17.07.2019
        iDBProfile_ID = Me.cmbdbProfiles.Items(Me.cmbdbProfiles.SelectedIndex).itemdata

        ' 13.10.2014 have to check if dbprofile_id changes - if so, for clients with more than one connection, pattern can change.
        ' Fill lstPatternGroup - with patterngroups for current dbProfile
        dbGetPatternGroups(oMyDal, iDBProfile_ID)
        iCounter = 1
        With Me
            If oMyDal.Reader_HasRows Then
                .lstPatternGroup.Items.Clear()
                Do While oMyDal.Reader_ReadRecord
                    iCounter = iCounter + 1
                    '.lstPatternGroup.Items.Add(oMyDal.Reader_GetString("Name"))
                    'VB6.SetItemData(.lstPatternGroup, .lstPatternGroup.Items.Count - 1, oMyDal.Reader_GetString("PatternGroup_ID"))
                    ' 16.07.2019
                    listitem = New _MyListBoxItem
                    listitem.ItemString = oMyDal.Reader_GetString("Name")
                    listitem.ItemData = oMyDal.Reader_GetString("PatternGroup_ID")
                    .lstPatternGroup.Items.Add(listitem)

                Loop
                If iCounter = 1 Then
                    .lstPatternGroup.SelectedIndex = 0
                    ' Fill in current patterngroupname in textbox
                    '.txtPatternGroup.Text = .lstPatternGroup.List(.lstPatternGroup.ListIndex)
                    iPatternGroup_ID = CShort(oMyDal.Reader_GetString("PatternGroup_ID"))
                Else
                    .lstPatternGroup.SelectedIndex = -1
                    .cmdRemovePatternGroup.Enabled = False
                End If
            Else
                .lstPatternGroup.SelectedIndex = -1
                .cmdRemovePatternGroup.Enabled = False
            End If

            If Not .lstPatternGroup.SelectedIndex = -1 Then
                ' Fill lstPatterns - with patterns for current PatternGroup
                ' Find all Patterns for current PatternGroup_ID
                dbGetPatterns(oMyDal, iDBProfile_ID, iPatternGroup_ID, iClient_ID)
                iCounter = 0
                If oMyDal.Reader_HasRows Then
                    With frmPatterns
                        .lstPatterns.Items.Clear()

                        Do While oMyDal.Reader_ReadRecord
                            iCounter = iCounter + 1
                            '.lstPatterns.Items.Add(oMyDal.Reader_GetString("Description"))
                            'VB6.SetItemData(.lstPatterns, .lstPatterns.Items.Count - 1, oMyDal.Reader_GetString("Pattern_ID"))
                            ' 12.07.2019
                            listitem = New _MyListBoxItem
                            listitem.ItemString = oMyDal.Reader_GetString("Description")
                            listitem.ItemData = oMyDal.Reader_GetString("Pattern_ID")
                            .lstPatterns.Items.Add(listitem)

                            If CBool(oMyDal.Reader_GetString("KID")) = True Then
                                .lstPatterns.SetItemChecked(.lstPatterns.Items.Count - 1, True)
                            End If
                        Loop
                        If iCounter = 1 Then
                            .lstPatterns.SelectedIndex = 0
                            ' Fill in current pattern in txtPattern
                            '.txtPattern.Text = VB6.GetItemString(.lstPatterns, .lstPatterns.SelectedIndex)
                            ' 12.07.2019
                            .txtPattern.Text = .lstPatterns.Items(.lstPatterns.SelectedIndex).itemstring
                        Else
                            .lstPatterns.SelectedIndex = -1
                            .cmdRemovePattern.Enabled = False
                        End If
                    End With
                Else
                    frmPatterns.cmdRemovePattern.Enabled = False
                End If
            Else
                frmPatterns.cmdRemovePattern.Enabled = False
            End If

        End With
        bAnythingChanged = True

    End Sub
    Private Sub cmdAutoBookings_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAutoBookings.Click
        frmClientAutoBook.SetClient_ID(iClient_ID)
        VB6.ShowForm(frmClientAutoBook, 1, Me)
    End Sub

    Private Sub cmdNotifications_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdNotifications.Click
        ' Fill listboxes in frmClientNotifications with selected and possible Notifications
        Dim i As Short

        With frmClientNotifications
            ' Fill Notificationslist with selected notifications
            dbGetClientNotifications(oMyDal, iClient_ID, False)

            .lstSelected.Items.Clear()
            If oMyDal.Reader_HasRows Then
                Do While oMyDal.Reader_ReadRecord
                    .lstSelected.Items.Add(PadRight(Strings.Left(oMyDal.Reader_GetString("Name"), 30), 30, " ") & Chr(9) & oMyDal.Reader_GetString("EMail_Adress"))
                    VB6.SetItemData(.lstSelected, .lstSelected.Items.Count - 1, CInt(oMyDal.Reader_GetString("Notification_ID")))
                Loop
            End If

            ' Fill list with possible Notifications;
            dbGetClientNotifications(oMyDal, iClient_ID, True)

            .lstPossible.Items.Clear()
            If oMyDal.Reader_HasRows Then
                Do While oMyDal.Reader_ReadRecord
                    '.lstPossible.Items.Add(PadRight(strings.left(oMyDal.Reader_GetString("Name"), 30), 30, " ") & Chr(9) & oMyDal.Reader_GetString("EMail_Adress"))
                    'VB6.SetItemData(.lstPossible, .lstPossible.Items.Count - 1, CInt(oMyDal.Reader_GetString("Notification_ID")))
                    ' 11.07.2019
                    listitem = New _MyListBoxItem
                    listitem.ItemString = PadRight(Strings.Left(oMyDal.Reader_GetString("Name"), 30), 30, " ") & Chr(9) & oMyDal.Reader_GetString("EMail_Adress")
                    listitem.ItemData = CInt(oMyDal.Reader_GetString("Notification_ID"))
                    .lstPossible.Items.Add(listitem)
                Loop
            End If

            If .lstPossible.Items.Count > 0 Then
                .lstPossible.SelectedIndex = 0
            End If
            If .lstSelected.Items.Count > 0 Then
                .lstSelected.SelectedIndex = 0
            End If

            frmClientNotifications.SetClient_ID(iClient_ID)
            frmClientNotifications.SetDAL(oMyDal)
            VB6.ShowForm(frmClientNotifications, 1, Me)
            'bAnythingChanged = True

            ' Update frmClientSettings.lstNotifications
            Me.lstNotifications.Items.Clear()
            For i = 0 To frmClientNotifications.lstSelected.Items.Count - 1
                'Me.lstNotifications.Items.Add(VB6.GetItemString(frmClientNotifications.lstSelected, i))
                'VB6.SetItemData(Me.lstNotifications, i, VB6.GetItemData(frmClientNotifications.lstSelected, i))
                ' 16.07.2019
                listitem = New _MyListBoxItem
                listitem.ItemString = frmClientNotifications.lstSelected.Items(i).itemstring
                listitem.ItemData = frmClientNotifications.lstSelected.Items(i).itemdata
                Me.lstNotifications.Items.Add(listitem)

            Next i

        End With

    End Sub

    Private Sub cmdSelectProfiles_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSelectProfiles.Click
        ' Fill listboxes in frmClientProfiles with selected and possible profiles
        Dim i As Short
        Dim iLRS As String

        With frmClientProfiles
            ' selected profiles;
            ' Fill Profileslist

            dbGetProfiles(oMyDal, iClient_ID)
            .lstSelected.Items.Clear()

            If oMyDal.Reader_HasRows Then
                Do While oMyDal.Reader_ReadRecord
                    If CBool(oMyDal.Reader_GetString("AutoMatch")) Then
                        ' Avstemmingsprofil
                        '.lstSelected.Items.Add(oMyDal.Reader_GetString("ShortName") & " (" & LRS(60341) & ")") 'Avstemming)"
                        iLRS = 60341
                    ElseIf CBool(oMyDal.Reader_GetString("FromAccountingSys")) = True Then
                        ' Sendprofile
                        '.lstSelected.Items.Add(oMyDal.Reader_GetString("ShortName") & " (" & LRS(60003) & ")") 'Send)"
                        iLRS = 60003
                    Else
                        ' returnprofile
                        '.lstSelected.Items.Add(oMyDal.Reader_GetString("ShortName") & " (" & LRS(60004) & ")") '(Retur)"
                        iLRS = 60004
                    End If
                    'VB6.SetItemData(.lstSelected, .lstSelected.Items.Count - 1, oMyDal.Reader_GetString("Filesetup_ID"))
                    ' 11.07.2019
                    listitem = New _MyListBoxItem
                    listitem.ItemString = oMyDal.Reader_GetString("ShortName") & " (" & LRS(iLRS) & ")"
                    listitem.ItemData = oMyDal.Reader_GetString("Filesetup_ID")
                    .lstSelected.Items.Add(listitem)
                Loop
            End If

            ' Fill list with possible profiles;
            dbGetAllProfiles(oMyDal, iClient_ID) ' Find possible profiles
            .lstPossible.Items.Clear()
            If oMyDal.Reader_HasRows Then
                Do While oMyDal.Reader_ReadRecord
                    If CBool(oMyDal.Reader_GetString("AutoMatch")) = True Then
                        ' Avstemmingsprofil
                        '.lstPossible.Items.Add(oMyDal.Reader_GetString("ShortName") & " (" & LRS(60341) & ")") 'Avstemming)"
                        iLRS = 60341
                    ElseIf CBool(oMyDal.Reader_GetString("FromAccountingSys")) = True Then
                        ' Sendprofile
                        '.lstPossible.Items.Add(oMyDal.Reader_GetString("ShortName") & " (" & LRS(60003) & ")") 'Send)"
                        iLRS = 60003
                    Else
                        ' returnprofile
                        '.lstPossible.Items.Add(oMyDal.Reader_GetString("ShortName") & " (" & LRS(60004) & ")") '(Retur)"
                        iLRS = 60004
                    End If
                    ' 11.07.2019
                    listitem = New _MyListBoxItem
                    listitem.ItemString = oMyDal.Reader_GetString("ShortName") & " (" & LRS(iLRS) & ")"
                    listitem.ItemData = 0
                    .lstPossible.Items.Add(listitem)
                Loop
            End If

            If .lstPossible.Items.Count > 0 Then
                .lstPossible.SelectedIndex = 0
            End If
            If .lstSelected.Items.Count > 0 Then
                .lstSelected.SelectedIndex = 0
            End If

            VB6.ShowForm(frmClientProfiles, 1, Me)
            If frmClientProfiles.AnythingChanged Then
                bAnythingChanged = True
                ' Update frmClientSettings.lstProfiles
                Me.lstProfiles.Items.Clear()
                For i = 0 To frmClientProfiles.lstSelected.Items.Count - 1
                    'Me.lstProfiles.Items.Add(VB6.GetItemString(frmClientProfiles.lstSelected, i))
                    'VB6.SetItemData(Me.lstProfiles, i, VB6.GetItemData(frmClientProfiles.lstSelected, i))
                    ' 11.07.2019
                    listitem = New _MyListBoxItem
                    listitem.ItemString = frmClientProfiles.lstSelected.Items(i).itemstring
                    listitem.ItemData = frmClientProfiles.lstSelected.Items(i).itemdata
                    Me.lstProfiles.Items.Add(listitem)
                Next i
            End If
        End With

    End Sub
    'Public Function SaveProfiles() As Object
    ' Lagre fra frmClienteProfiles.lstSelected til rsProfiles
    'Dim i As Short
    'Dim sString As String
    'Dim bNewProfile As Boolean

    'TODO: DAL- Hva gj�r vi egentlig her? Ingenting!
    'Vi setter kun bAnythingChanged = True, noe som vi ikke vet om er sant eller ikke!
    'Har kommentert alt i funksjonen!
    'Skal vi fjerne hele funksjonen?

    '' kan ikke slette, m� catche hvem som er nye fra rsProfiles!
    '' test om items fra lstSelected finnes i rsProfiles, eller i rsPossible
    '' Run through lstSelected;
    'For i = 0 To frmClientProfiles.lstSelected.Items.Count - 1
    '    sString = VB6.GetItemString(frmClientProfiles.lstSelected, i)
    '    sString = strings.left(sString, InStr(sString, " ") - 1) '  remove (Send), etc
    '    ' Check if profile was previosly present in rsProfiles
    '    bNewProfile = True
    '    If rsProfiles.RecordCount > 0 Then
    '        rsProfiles.MoveFirst()
    '        Do While Not rsProfiles.EOF
    '            If rsProfiles.Fields("ShortName").Value = sString Then
    '                bNewProfile = False
    '                Exit Do
    '            End If
    '            rsProfiles.MoveNext()
    '        Loop
    '    End If
    '    If bNewProfile Then
    '        ' add profile into recordset;
    '        'rsProfiles.AddNew
    '        'rsProfiles!FormatIn = 0 ' Testing when saving later on if any with 0
    '        'rsProfiles!ShortName = sString
    '        ' find in rsPossible
    '        'rsPossible.MoveFirst
    '        'Do While Not rsPossible.EOF
    '        '    If rsPossible!ShortName = sString Then
    '        '        bNewProfile = False
    '        '        Exit Do
    '        '    End If
    '        '    rsPossible.MoveNext
    '        'Loop
    '        'rsProfiles!Filesetup_ID = rsPossible!Filesetup_ID
    '    End If
    'Next i
    'bAnythingChanged = True

    'End Function
    Private Sub cmdAddEasyClient_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAddEasyClient.Click
        If Len(Trim(Me.txtClientNo.Text)) And Len(Trim(Me.txtClientName.Text)) > 0 Then
            If ValidateClient(Trim(Me.txtClientNo.Text)) Then

                If SaveClient() Then ' Save previous client?
                    iClient_ID = 0 ' 0 = New client
                    sClientNo = Me.txtClientNo.Text
                    sClientName = Me.txtClientName.Text

                    'Remove info from other controls (moved up about 10 lines 08.06.06)
                    lstAccounts.Items.Clear()
                    lstProfiles.Items.Clear()
                    lstDiffAccounts.Items.Clear()
                    lstNotifications.Items.Clear()

                    bAnythingChanged = True
                    bClientSavedAlready = False
                    ' Add client only by setting clientno and clientname;
                    ' Use clientno and name for txtClientNo and txtClientName
                    With Me
                        '.lstClients.Items.Insert(0, .txtClientNo.Text & Chr(9) & .txtClientName.Text)
                        ' 17.07.2019
                        .lstClients.Items.Insert(0, New _MyListBoxItem(.txtClientNo.Text & Chr(9) & .txtClientName.Text, 0))

                        .lstClients.SelectedIndex = 0

                        ' Fill frmClientDetail with defaults;
                        FillClientDetail() ' Always fill frmClientDetail
                        FillDBProfiles()
                        .txtClientNo.Focus()
                        ' add into database!

                        .txtClientNo.Text = ""
                        .txtClientName.Text = ""
                        .txtClientNo.SelectionStart = 0

                    End With
                    bAnythingChanged = True 'To force another save of the new client to check if a profile is selected
                End If
            End If
        End If
    End Sub
    Private Function SaveClient(Optional ByRef bAsk As Boolean = False) As Boolean  'TODO bAsk var True !!!
        ' This function is called when we change to a new client, or exits form
        Dim sMySQL As String
        Dim sCompany_ID As String
        Dim iFormatIn As Short
        Dim iFormatOut As Short
        Dim iClientFormat_ID As Short
        'Dim iAccount_ID As Integer
        Dim iDiffAccount_ID As Short
        Dim sString As String
        Dim i As Short
        Dim j As Short
        Dim bValid As Boolean
        Dim bYes As Boolean
        Dim iNewDiffAccount_ID As Short
        Dim iAccount_ID As Short  ' Local to take care of only savings, not current account as the iAccount_ID defined in top of module
        Dim bThisIsNewClient As Boolean = False
        Dim sTmp As String = ""

        If Not bClientSavedAlready Then
            ' added 10.06.2010
            ' check sub-forms if any changes have been done:
            If frmClientDetail.AnythingChanged Then
                bAnythingChanged = True
                ' Pick up clientno and clientname from frmClientDetail
                sClientNo = frmClientDetail.txtClientNo.Text
                sClientName = frmClientDetail.txtClientName.Text
            End If
            If frmClientProfiles.AnythingChanged Then
                bAnythingChanged = True
                ' 17.07.2019 - wait to reset until we have saved
                'frmClientProfiles.ResetAnythingChanged()
            End If

            If frmClientAutoBook.AnythingChanged Then
                bAnythingChanged = True
                frmClientAutoBook.ResetAnythingChanged()
            End If
            If frmClientNotifications.AnythingChanged Then
                bAnythingChanged = True
                frmClientNotifications.ResetAnythingChanged()
            End If
            If frmDiffAccountDetail.AnythingChanged Then
                bAnythingChanged = True
                frmDiffAccountDetail.ResetAnythingChanged()
                ' open up for DiffAmounts
                Me.cmdShowMatchDiff.Enabled = True
            End If
            If frmAccountDetail.AnythingChanged Then
                bAnythingChanged = True
                'frmAccountDetail.ResetAnythingChanged()  26.07.2019 commented, because we had problems saving account data some times
            End If

            ' Added 21.09.06
            ' Warn if profile not selected; But only when at least account, diffaccount or pattern is filled in
            'If bAnythingChanged And (Me.lstAccounts.Items.Count > 0 Or Me.lstDiffAccounts.Items.Count > 0 Or Me.lstNotifications.Items.Count > 0) And Me.lstProfiles.Items.Count = 0 Then
            If bAnythingChanged And (Me.lstProfiles.Items.Count = 0 Or Me.lstAccounts.Items.Count = 0) And iClient_ID <> 0 Then
                If Me.lstProfiles.Items.Count = 0 Then
                    MsgBox(LRS(60496), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Please select a profile for this client
                    SaveClient = False
                Else
                    MsgBox(LRS(60600), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Please set an account for this client
                    SaveClient = False
                End If
            Else

                SaveClient = True
                bValid = True
                sCompany_ID = "1"

                ' for new clients, check if clientno is used before
                If bAnythingChanged And iClient_ID = 0 Then
                    ' Validate Clientno - Cannot have identical clientno's
                    sMySQL = "SELECT ClientNo FROM Client WHERE Company_ID = " & sCompany_ID & "And ClientNo = '" & sClientNo & "'"

                    oMyDal.SQL = sMySQL
                    If oMyDal.Reader_Execute() Then
                        If oMyDal.Reader_HasRows Then
                            Do While oMyDal.Reader_ReadRecord
                                ' Clientno used previously
                                MsgBox(Trim(sClientNo) & " - " & LRS(60375), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Clientno used for other client
                                bValid = False
                                Exit Do
                            Loop
                        End If
                    Else
                        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                    End If

                    bAsk = False
                End If

                If bValid Then
                    If bAnythingChanged Then
                        If bAsk Then
                            bYes = MsgBox(LRS(60343) & " " & sClientNo & " " & sClientName & " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes 'Save client ?
                        Else
                            bYes = True
                        End If
                        If bYes Then
                            ' Save current client;
                            If iClient_ID = 0 Then
                                ' New Client!
                                ' Find next Client_ID
                                ' NB! Is it safe to presume that we MoveLast, and add 1 to Client_ID?
                                sMySQL = "SELECT MAX(Client_ID) AS NewID FROM Client WHERE Company_ID = " & sCompany_ID

                                oMyDal.SQL = sMySQL
                                If oMyDal.Reader_Execute() Then
                                    If oMyDal.Reader_HasRows Then
                                        Do While oMyDal.Reader_ReadRecord
                                            iClient_ID = oMyDal.Reader_GetString("NewID") + 1
                                        Loop
                                    Else
                                        iClient_ID = 1
                                    End If
                                Else
                                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                                End If

                                If EmptyString(sClientName) Then
                                    sClientName = "-"
                                End If
                                ' changed 18.08.2011 - must also add ClientNo and ClientName, in case we haven't used frmClientDetail
                                sMySQL = "INSERT INTO Client(Company_ID, Client_ID, ClientNo, Name, VoucherType) VALUES("
                                sMySQL = sMySQL & sCompany_ID & ", " & iClient_ID.ToString & ", '" & sClientNo & "', '" & sClientName & "', ''"
                                sMySQL = sMySQL & ")"

                                oMyDal.SQL = sMySQL

                                If oMyDal.ExecuteNonQuery Then
                                    If oMyDal.RecordsAffected = 1 Then
                                        'OK
                                    Else
                                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                    End If
                                Else
                                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                End If

                                ' Add client_id into lstClients ItemData
                                ' New client always listindex 0 (topmost in listbox)
                                'VB6.SetItemData(Me.lstClients, 0, iClient_ID)
                                ' Moved to end, because it triggers another run in SaveClient
                                bThisIsNewClient = True

                            End If

                            ' Common for both new and old clients;
                            ' Save info to clients table:
                            '----------------------------
                            With frmClientDetail
                                'New 01.12.2004, moved start of SQL UPDATE .. outside SQL.
                                sMySQL = "UPDATE Client SET "
                                If .AnythingChanged Then
                                    .ResetAnythingChanged()
                                    'sMySQL = "UPDATE Client SET ClientNo = '" & sClientNo
                                    sMySQL = sMySQL & "ClientNo = '" & sClientNo
                                    sMySQL = sMySQL & "', Name= '" & sClientName
                                    'HAGLUND - B�r kode v�re noe s�nt som
                                    'sMySQL = sMySQL & "ClientNo = '" & .txtClientNo.Text
                                    'sClientNo = .txtClientNo.Text 
                                    'sMySQL = sMySQL & "', Name= '" & .txtClientName.Text
                                    'sClientName = .txtClientName.Text


                                    'OLD CODE - HAGLUND - Saved also City and CountryCode, which is not a part of setup
                                    'If rsClient Is Nothing Then
                                    '    sMySQL = sMySQL & "', City = '"
                                    '    sMySQL = sMySQL & "', CountryCode = '"
                                    'Else
                                    '    If Not rsClient.EOF Then
                                    '        ' Most of clientinfo is saved in frmClientDetail
                                    '        sMySQL = sMySQL & "', City = '" & rsClient.Fields("City").Value
                                    '        sMySQL = sMySQL & "', CountryCode = '" & rsClient.Fields("CountryCode").Value
                                    '    Else
                                    '        sMySQL = sMySQL & "', City = '"
                                    '        sMySQL = sMySQL & "', CountryCode = '"
                                    '    End If
                                    'End If
                                    'NEW CODE
                                    sMySQL = sMySQL & "', City = '"
                                    sMySQL = sMySQL & "', CountryCode = '"

                                    sMySQL = sMySQL & "', CompNo = '" & Trim(.txtCompanyNo.Text)
                                    sMySQL = sMySQL & "', Division = '" & Trim(.txtDivision.Text) & "', "

                                    ' added 09.07.2015
                                    sMySQL = sMySQL & "MsgID_Variable = '" & Trim(.txtMsgId_Variable.Text) & "', "
                                    sMySQL = sMySQL & "HistoryDelDays = " & Trim(.txtHistoryDeleteDays.Text) & ", "
                                    If .lstValidationLevel.SelectedIndex = 0 Then
                                        sMySQL = sMySQL & "ValidationLevel = 0, "
                                    ElseIf .lstValidationLevel.SelectedIndex = 1 Then
                                        sMySQL = sMySQL & "ValidationLevel = 2, "
                                    ElseIf .lstValidationLevel.SelectedIndex = 2 Then
                                        sMySQL = sMySQL & "ValidationLevel = 4, "
                                    Else
                                        sMySQL = sMySQL & "ValidationLevel = 0, "
                                    End If
                                    sMySQL = sMySQL & "SocialSecurityNo = '" & Trim(.txtSocialSecurityNo.Text) & "', "
                                    If .chkUseStateBankText.CheckState = 1 Then
                                        sMySQL = sMySQL & "UseStateBankText = True, "
                                    Else
                                        sMySQL = sMySQL & "UseStateBankText = False, "
                                    End If
                                    If .chkSEPASingle.CheckState = 1 Then
                                        sMySQL = sMySQL & "SEPASingle = True, "
                                    Else
                                        sMySQL = sMySQL & "SEPASingle = False, "
                                    End If
                                    If .chkSavePaymentData.CheckState = 1 Then
                                        sMySQL = sMySQL & "SavePaymentData = True, "
                                    Else
                                        sMySQL = sMySQL & "SavePaymentData = False, "
                                    End If
                                    If .chkEndToEndRefFromBabelBank.CheckState = 1 Then
                                        sMySQL = sMySQL & "EndToEndRefFromBabelBank = True, "
                                    Else
                                        sMySQL = sMySQL & "EndToEndRefFromBabelBank = False, "
                                    End If
                                    If frmClientDetailISO20022.chkAdjustToSchema.CheckState = 1 Then
                                        sMySQL = sMySQL & "AdjustForSchemaValidation = True, "
                                    Else
                                        sMySQL = sMySQL & "AdjustForSchemaValidation = False, "
                                    End If
                                    If frmClientDetailISO20022.chkIBANToBBANNorway.CheckState = 1 Then
                                        sMySQL = sMySQL & "AdjustToBBANNorway = True, "
                                    Else
                                        sMySQL = sMySQL & "AdjustToBBANNorway = False, "
                                    End If
                                    If frmClientDetailISO20022.chkIBANTOBBANSweden.CheckState = 1 Then
                                        sMySQL = sMySQL & "AdjustToBBANSweden = True, "
                                    Else
                                        sMySQL = sMySQL & "AdjustToBBANSweden = False, "
                                    End If
                                    If frmClientDetailISO20022.chkIBANToBBANDenmark.CheckState = 1 Then
                                        sMySQL = sMySQL & "AdjustToBBANDenmark = True, "
                                    Else
                                        sMySQL = sMySQL & "AdjustToBBANDenmark = False, "
                                    End If
                                    If frmClientDetailISO20022.chkAllowDifferenceInCurrency.CheckState = 1 Then
                                        sMySQL = sMySQL & "UseDifferentInvoiceAndPaymentCurrency = True, "
                                    Else
                                        sMySQL = sMySQL & "UseDifferentInvoiceAndPaymentCurrency = False, "
                                    End If
                                    If frmClientDetailISO20022.chkISOSplitOCR.CheckState = 1 Then
                                        sMySQL = sMySQL & "ISOSplitOCR = True, "
                                    Else
                                        sMySQL = sMySQL & "ISOSplitOCR = False, "
                                    End If
                                    If frmClientDetailISO20022.chkISOSplitOCRWithCredits.CheckState = 1 Then
                                        sMySQL = sMySQL & "ISOSplitOCRWithCredits = True, "
                                    Else
                                        sMySQL = sMySQL & "ISOSplitOCRWithCredits = False, "
                                    End If
                                    If frmClientDetailISO20022.chkISOSRedoFreetextToStruct.CheckState = 1 Then
                                        sMySQL = sMySQL & "ISOSRedoFreetextToStruct = True, "
                                    Else
                                        sMySQL = sMySQL & "ISOSRedoFreetextToStruct = False, "
                                    End If
                                    If frmClientDetailISO20022.chkISODoNotGroup.CheckState = 1 Then
                                        sMySQL = sMySQL & "ISODoNotGroup = True, "
                                    Else
                                        sMySQL = sMySQL & "ISODoNotGroup = False, "
                                    End If
                                    If frmClientDetailISO20022.chkISORemoveBICInSEPA.CheckState = 1 Then
                                        sMySQL = sMySQL & "ISORemoveBICInSEPA = True, "
                                    Else
                                        sMySQL = sMySQL & "ISORemoveBICInSEPA = False, "
                                    End If
                                    If frmClientDetailISO20022.chkUseTransferCurrency.CheckState = 1 Then
                                        sMySQL = sMySQL & "ISOUseTransferCurrency = True, "
                                    Else
                                        sMySQL = sMySQL & "ISOUseTransferCurrency = False, "
                                    End If


                                    sMySQL = sMySQL & "VoucherNo = '" & Trim(.txtVoucherNo.Text) & "'"
                                    sMySQL = sMySQL & ", VoucherType = '" & Trim(.txtVoucherType.Text) & "'"
                                    sMySQL = sMySQL & ", VoucherNo2 = '" & Trim(.txtVoucherNo2.Text) & "'"
                                    sMySQL = sMySQL & ", VoucherType2 = '" & Trim(.txtVoucherType2.Text) & "', "
                                    If .optGetVNoBB.Checked = True Then
                                        sMySQL = sMySQL & " HowToAddVoucherNo = 2, " 'Add from BB
                                    ElseIf .optGetVNoERP.Checked = True Then
                                        sMySQL = sMySQL & " HowToAddVoucherNo = 1, " 'Add from ERP
                                    Else
                                        sMySQL = sMySQL & " HowToAddVoucherNo = 0, " 'Don't add VoucherNo
                                    End If
                                    If .chkAddOnOcr.CheckState = 1 Then
                                        sMySQL = sMySQL & "AddVoucherNoBeforeOCR = True, "
                                    Else
                                        sMySQL = sMySQL & "AddVoucherNoBeforeOCR = False, "
                                    End If
                                    If .chkAddVoucherCounter.CheckState = 1 Then
                                        sMySQL = sMySQL & "AddCounterToVoucherNo = True, "
                                    Else
                                        sMySQL = sMySQL & "AddCounterToVoucherNo = False, "
                                    End If
                                    If EmptyString(.txtBB_Year.Text) Then
                                        .txtBB_Year.Text = VB6.Format(Now, "YYYY")
                                    End If
                                    sMySQL = sMySQL & " BB_Year = " & .txtBB_Year.Text & ","
                                    If EmptyString(.txtBB_Period.Text) Then
                                        .txtBB_Period.Text = VB6.Format(Now, "MM")
                                    End If
                                    sMySQL = sMySQL & " BB_Period = " & .txtBB_Period.Text & ","
                                End If

                                If cmbdbProfiles.SelectedIndex > -1 Then
                                    ' user has selected a dbProfile, use this one
                                    'New 01.12.2004, removed ", " at the start of the SQL
                                    'sMySQL = sMySQL & ", DBProfile_ID = " & Trim$(Str(cmbdbProfiles.ItemData(cmbdbProfiles.ListIndex)))
                                    'sMySQL = sMySQL & "DBProfile_ID = " & Trim(Str(VB6.GetItemData(cmbdbProfiles, cmbdbProfiles.SelectedIndex))) & ","
                                    ' 17.07.2019
                                    sMySQL = sMySQL & "DBProfile_ID = " & Trim(Str(cmbdbProfiles.Items(cmbdbProfiles.SelectedIndex).itemdata)) & ","
                                Else
                                    'In old ADO we retrieved the DB Profile from rsClients if it was not set.
                                    sMySQL = sMySQL & "DBProfile_ID = " & "0" & ","
                                End If
                                ' 16.07.2018
                                ' added setup for ClientGroups (group of accounts used in Manual Matching)
                                If frmClientDetail.cmbClientGroup.SelectedIndex > -1 Then
                                    sMySQL = sMySQL & "ClientGroup = '" & Trim(frmClientDetail.cmbClientGroup.Text) & "'"
                                ElseIf frmClientDetail.cmbClientGroup.Text.Length > 0 Then
                                    sMySQL = sMySQL & "ClientGroup = '" & Trim(frmClientDetail.cmbClientGroup.Text) & "'"
                                Else
                                    'In old ADO we retrieved the DB Profile from rsClients if it was not set.
                                    sMySQL = sMySQL & "ClientGroup = ''"
                                End If

                                sMySQL = sMySQL & " WHERE Company_ID = " & sCompany_ID & " AND Client_ID ="
                                sMySQL = sMySQL & Str(iClient_ID)
                            End With

                            oMyDal.SQL = sMySQL

                            If oMyDal.ExecuteNonQuery Then
                                If oMyDal.RecordsAffected = 1 Then
                                    'OK
                                Else
                                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                End If
                            Else
                                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                            End If

                            ' Save profilesettings
                            '----------------------

                            ' Run through lstProfiles and update database with these profiles
                            For i = 0 To Me.lstProfiles.Items.Count - 1
                                'If VB6.GetItemData(Me.lstProfiles, i) > 0 Then
                                ' 11.07.2019
                                If Me.lstProfiles.Items(i).itemdata > 0 Then
                                    ' this profile is already present, do nothing!!!
                                    i = i
                                Else
                                    ' New profile for this client;
                                    ' Find profile in rsPossible;
                                    'sString = frmClientProfiles.lstSelected.List(i)
                                    'sString = VB6.GetItemString(Me.lstProfiles, i)
                                    ' 11.07.2019
                                    sString = Me.lstProfiles.Items(i).itemstring
                                    '17.09.2007 - Problems if the profile contains a real "(", Excel(XLS)Send
                                    sString = Strings.Left(sString, InStrRev(sString, "(") - 1)
                                    'Old code
                                    'sString = Left$(sString, InStr(sString, "(") - 1)
                                    sString = Trim(sString)

                                    'Find the FormatIn and the format out from the FileSetup-table using the shortname.
                                    sMySQL = "SELECT Filesetup_ID, FileSetupOut FROM Filesetup WHERE Company_ID = " & sCompany_ID & " AND Shortname = '" & sString & "'"

                                    oMyDal.SQL = sMySQL
                                    If oMyDal.Reader_Execute() Then
                                        If oMyDal.Reader_HasRows Then
                                            Do While oMyDal.Reader_ReadRecord
                                                iFormatIn = CShort(oMyDal.Reader_GetString("Filesetup_ID"))
                                                iFormatOut = CShort(oMyDal.Reader_GetString("FileSetupOut"))
                                            Loop
                                        Else
                                            Throw New Exception(LRS(60603, sString)) 'Can't find the correct profile: %1
                                        End If
                                    Else
                                        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                                    End If

                                    ' Find next possible ClientFormat_ID;
                                    ' NB! Is it safe to presume that we MoveLast, and add 1 to ClientFormat_ID?
                                    sMySQL = "SELECT MAX(ClientFormat_ID) AS MaxID FROM ClientFormat WHERE Company_ID = " & sCompany_ID

                                    oMyDal.SQL = sMySQL
                                    If oMyDal.Reader_Execute() Then
                                        If oMyDal.Reader_HasRows Then
                                            Do While oMyDal.Reader_ReadRecord
                                                iClientFormat_ID = CShort(oMyDal.Reader_GetString("MaxID")) + 1
                                            Loop
                                        Else
                                            iClientFormat_ID = 1
                                        End If
                                    Else
                                        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                                    End If

                                    sMySQL = "INSERT INTO ClientFormat "
                                    sMySQL = sMySQL & "(Company_ID, ClientFormat_ID, Client_ID, "
                                    sMySQL = sMySQL & "FormatIn, FormatOut, KIDStart, KIDLength, "
                                    sMySQL = sMySQL & "KIDValue) VALUES("
                                    sMySQL = sMySQL & sCompany_ID & ", "
                                    sMySQL = sMySQL & iClientFormat_ID.ToString & ", "
                                    sMySQL = sMySQL & iClient_ID.ToString & ", "
                                    sMySQL = sMySQL & iFormatIn.ToString & ", "
                                    sMySQL = sMySQL & iFormatOut.ToString
                                    sMySQL = sMySQL & ", -1, -1, -1)"

                                    oMyDal.SQL = sMySQL

                                    If oMyDal.ExecuteNonQuery Then
                                        If oMyDal.RecordsAffected = 1 Then
                                            'OK
                                        Else
                                            Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                        End If
                                    Else
                                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                    End If

                                End If
                            Next i

                            ' Run through lstPossible to see if we have removed any profiles
                            ' 17.07.2019
                            If frmClientProfiles.AnythingChanged Then
                                With frmClientProfiles.lstPossible
                                    For i = 0 To .Items.Count - 1
                                        'If VB6.GetItemData(frmClientProfiles.lstPossible, i) > 0 Then
                                        ' 11.07.2019
                                        ' if we have not been into frmClientProfiles, then lstPossible is empty

                                        If frmClientProfiles.lstPossible.Items(i).itemdata <> Nothing Then
                                            ' This one was previously selected, now removed
                                            ' Must remove from ClientFormat;

                                            sMySQL = "DELETE FROM ClientFormat WHERE Company_ID = " & sCompany_ID & " AND "
                                            'sMySQL = sMySQL & "FormatIn = " & Str(VB6.GetItemData(frmClientProfiles.lstPossible, i)) & " AND Client_ID = " & iClient_ID.ToString
                                            ' 11.07.2019
                                            sMySQL = sMySQL & "FormatIn = " & Str(frmClientProfiles.lstPossible.Items(i).itemdata) & " AND Client_ID = " & iClient_ID.ToString

                                            oMyDal.SQL = sMySQL

                                            If oMyDal.ExecuteNonQuery Then
                                                If oMyDal.RecordsAffected = 1 Then
                                                    'OK
                                                Else
                                                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                                End If
                                            Else
                                                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                            End If

                                        End If
                                    Next i
                                End With
                            End If
                            ' moved here 17.07.2019
                            frmClientProfiles.ResetAnythingChanged()

                            '--------------
                            ' Save accounts
                            '--------------
                            iAccount_ID = 0
                            ' Find highest Account_ID, for this client in Account-table
                            ' NB! Is it safe to presume that we MoveLast, and add 1 to Account_ID?
                            sMySQL = "SELECT MAX(Account_ID) AS MaxID FROM Account WHERE Company_ID = " & sCompany_ID & " AND Client_ID = " & iClient_ID.ToString

                            oMyDal.SQL = sMySQL
                            If oMyDal.Reader_Execute() Then
                                If oMyDal.Reader_HasRows Then
                                    Do While oMyDal.Reader_ReadRecord
                                        iAccount_ID = CShort(oMyDal.Reader_GetString("MaxID"))
                                    Loop
                                Else
                                    iAccount_ID = 0
                                End If
                            Else
                                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                            End If

                            For j = 0 To Me.lstAccounts.Items.Count - 1
                                'If VB6.GetItemData(Me.lstAccounts, j) = 0 Then
                                If Me.lstAccounts.Items(j).itemdata = 0 Then
                                    ' Added Account
                                    iAccount_ID = iAccount_ID + 1
                                    sMySQL = "INSERT INTO Account "
                                    sMySQL = sMySQL & "(Company_ID, Client_ID, Account_ID, "
                                    sMySQL = sMySQL & "Account, GLAccount, GLResultsAccount, "
                                    sMySQL = sMySQL & "ConvertedAccount, ContractNo, AvtalegiroID, SWIFTAddr, CurrencyCode, DebitAccountCountryCode) VALUES("
                                    sMySQL = sMySQL & sCompany_ID & ", "
                                    sMySQL = sMySQL & iClient_ID.ToString & ", "
                                    sMySQL = sMySQL & iAccount_ID.ToString & ", " ' Account_ID

                                    '----------
                                    ' AccountNo
                                    '----------
                                    'sString = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 1))
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 1))
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '-------------------
                                    ' GLAccount (BankGL)
                                    '-------------------
                                    ' After first chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 2)
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 2))
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '-----------------
                                    ' GLResultsAccount
                                    '-----------------
                                    ' After third chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 3)
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 3))
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '-----------------
                                    ' ConvertedAccount
                                    '-----------------
                                    ' After forth chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 4)
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 4))
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '-----------------
                                    ' ContractNo
                                    '-----------------
                                    ' After fifth chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 5)
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 5))
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '-----------------
                                    ' AvtalegiroID
                                    '-----------------
                                    ' After sixth chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 6)
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 6))
                                    sMySQL = sMySQL & "'" & sString & "',"

                                    '-----------------
                                    ' SWIFTAddr
                                    '-----------------
                                    ' After seventh chr(9)
                                    'sString = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 7))
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 7))
                                    sMySQL = sMySQL & "'" & sString & "',"


                                    '-----------------
                                    ' CurrencyCode
                                    '-----------------
                                    ' After ninth chr(9)
                                    'sString = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 9))
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 9))
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    ' XokNET 18.11.2011
                                    '------------------------
                                    ' DebitAccountCountryCode
                                    '------------------------
                                    ' After tenth chr(9)
                                    'sString = Trim$(xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 10))
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 10))
                                    sMySQL = sMySQL & "'" & sString & "') "

                                    oMyDal.SQL = sMySQL

                                    If oMyDal.ExecuteNonQuery Then
                                        If oMyDal.RecordsAffected = 1 Then
                                            'OK
                                        Else
                                            Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                        End If
                                    Else
                                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                    End If

                                    ' 22.05.2017 - Set itemdata to other than 0 so we do not add account more than once
                                    'VB6.SetItemData(Me.lstAccounts, Me.lstAccounts.SelectedIndex, iAccount_ID)
                                    ' 16.07.2019
                                    Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemdata = iAccount_ID


                                Else
                                    ' Update account, already existing
                                    'iAccount_ID = VB6.GetItemData(Me.lstAccounts, j)
                                    ' 16.07.2019
                                    iAccount_ID = Me.lstAccounts.Items(j).itemdata

                                    sMySQL = "UPDATE Account SET "

                                    '----------
                                    ' AccountNo
                                    '----------
                                    sMySQL = sMySQL & "Account = "
                                    'sString = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 1))
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 1))
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '-------------------
                                    ' GLAccount (BankGL)
                                    '-------------------
                                    ' After first chr(9)
                                    sMySQL = sMySQL & "GLAccount = "
                                    'sString = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 2))
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 2))
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '-----------------
                                    ' GLResultsAccount
                                    '-----------------
                                    ' After third chr(9)
                                    sMySQL = sMySQL & "GLResultsAccount = "
                                    'sString = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 3))
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 3))
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '-----------------
                                    ' ConvertedAccount
                                    '-----------------
                                    ' After forth chr(9)
                                    sMySQL = sMySQL & "ConvertedAccount = "
                                    'sString = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 4))
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 4))
                                    sMySQL = sMySQL & "'" & sString & "', "


                                    '-----------------
                                    ' ContractNo
                                    '-----------------
                                    ' After fifth chr(9)
                                    sMySQL = sMySQL & "ContractNo = "
                                    'sString = xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 5)
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 5))
                                    sMySQL = sMySQL & "'" & sString & "', "


                                    '-----------------
                                    ' AvtalegiroID
                                    '-----------------
                                    ' After sixth chr(9)
                                    sMySQL = sMySQL & "AvtalegiroID = "
                                    'sString = xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 6)
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 6))
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '-----------------
                                    ' SWIFT-address
                                    '-----------------
                                    ' After seventh chr(9)
                                    sMySQL = sMySQL & "SWIFTAddr = "
                                    'sString = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 7))
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 7))
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '-----------------
                                    ' CurrencyCode
                                    '-----------------
                                    ' After nineth chr(9)
                                    sMySQL = sMySQL & "CurrencyCode = "
                                    'sString = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 9))
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 9))
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    ' XokNET 07.09.2011
                                    '------------------------
                                    ' DebitAccountCountryCode
                                    '------------------------
                                    ' After tenth chr(9)
                                    sMySQL = sMySQL & "DebitAccountCountryCode="
                                    'sString = Trim$(xDelim(VB6.GetItemString(Me.lstAccounts, j), Chr(9), 10))
                                    ' 16.07.2019
                                    sString = Trim(xDelim(Me.lstAccounts.Items(j).itemstring, Chr(9), 10))
                                    sMySQL = sMySQL & "'" & sString & "' "

                                    sMySQL = sMySQL & "WHERE Company_ID = " & sCompany_ID & " AND "
                                    sMySQL = sMySQL & "Client_ID = " & iClient_ID.ToString & " AND "
                                    sMySQL = sMySQL & "Account_ID = " & iAccount_ID.ToString

                                    oMyDal.SQL = sMySQL

                                    If oMyDal.ExecuteNonQuery Then
                                        If oMyDal.RecordsAffected = 1 Then
                                            'OK
                                        Else
                                            Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                        End If
                                    Else
                                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                    End If

                                End If
                            Next j

                            If Not Array_IsEmpty(aAccountsDeleted) Then
                                ' Run through array with deleted Accounts
                                For i = 0 To UBound(aAccountsDeleted)
                                    iAccount_ID = aAccountsDeleted(i)
                                    If iAccount_ID > 0 Then
                                        sMySQL = "DELETE FROM Account "
                                        sMySQL = sMySQL & "WHERE Company_ID = " & sCompany_ID & " AND "
                                        sMySQL = sMySQL & "Client_ID = " & iClient_ID.ToString & " AND "
                                        sMySQL = sMySQL & "Account_ID = " & iAccount_ID.ToString

                                        oMyDal.SQL = sMySQL

                                        If oMyDal.ExecuteNonQuery Then
                                            If oMyDal.RecordsAffected = 1 Then
                                                'OK
                                            Else
                                                Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                            End If
                                        Else
                                            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                        End If
                                    End If

                                Next i
                            End If
                            frmAccountDetail.ResetAnythingChanged()  ' 26.07.2019 added, because we had problems saving account data some times

                            '----------------------
                            ' Save DiffAccounts
                            '----------------------
                            ' Run through lstDiffAccounts
                            ' New DiffAccounts have .ItemData = 0
                            iDiffAccount_ID = 0
                            ' Find highest DiffAccount_ID, for this client in MATCH_DiffAccounts-table
                            'For i = 0 To Me.lstDiffAccounts.ListCount - 1
                            '    If Me.lstDiffAccounts.ItemData(i) > iDiffAccount_ID Then
                            '        iDiffAccount_ID = Me.lstDiffAccounts.ItemData(i)
                            '    End If
                            'Next i
                            'iDiffAccount_ID = iDiffAccount_ID + 1
                            sCompany_ID = "1" 'HAGLUND - Hvorfor settes denne her?
                            sMySQL = "SELECT MAX(DiffAccount_ID) AS MaxID FROM MATCH_DiffAccounts WHERE Company_ID = " & sCompany_ID

                            oMyDal.SQL = sMySQL
                            If oMyDal.Reader_Execute() Then
                                If oMyDal.Reader_HasRows Then
                                    Do While oMyDal.Reader_ReadRecord
                                        iNewDiffAccount_ID = CShort(oMyDal.Reader_GetString("MaxID")) + 1
                                    Loop
                                Else
                                    iNewDiffAccount_ID = 1
                                End If
                            Else
                                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                            End If

                            For j = 0 To Me.lstDiffAccounts.Items.Count - 1
                                'If VB6.GetItemData(Me.lstDiffAccounts, j) = 0 Then
                                ' 17.07.2019
                                If Me.lstDiffAccounts.Items(j).itemdata = 0 Then
                                    ' Added DiffAccount with AddEasy
                                    sMySQL = "INSERT INTO MATCH_DiffAccounts "
                                    sMySQL = sMySQL & "(Company_ID, Client_ID, DiffAccount_ID, "
                                    sMySQL = sMySQL & "Name, Pattern, Type, Txt, [Function], VATCode, CurrencyCode) VALUES("
                                    sMySQL = sMySQL & sCompany_ID & ", "
                                    sMySQL = sMySQL & iClient_ID.ToString & ", "
                                    sMySQL = sMySQL & iNewDiffAccount_ID.ToString & ", " ' Account_ID

                                    '----------
                                    ' Name
                                    '----------
                                    'sString = Strings.Left(Trim(xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 1)), 30)
                                    sString = Strings.Left(Trim(xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 1)), 30)
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '----------
                                    ' Pattern
                                    '----------
                                    ' After first chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 2)
                                    ' 17.07.2019
                                    sString = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 2)
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '----------
                                    ' Type
                                    '----------
                                    ' After third chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 4)
                                    ' 17.07.2019
                                    sString = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 4)

                                    sMySQL = sMySQL & "" & sString & ", "

                                    '----------
                                    ' Txt
                                    '----------
                                    ' After second chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 3)
                                    ' 17.07.2019
                                    sString = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 3)

                                    sMySQL = sMySQL & "'" & sString & "',"

                                    '------------------------------------------------
                                    ' Function (NoMatchAccount, Cashdicount or empty)
                                    '------------------------------------------------
                                    ' After forth chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 5)
                                    ' 17.07.2019
                                    sString = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 5)

                                    ' Holds text "NOMATCH", "CASHDISCOUNT" or ""
                                    sMySQL = sMySQL & "'" & Trim(sString) & "', "

                                    '----------
                                    ' VATCode
                                    '----------
                                    ' After fifth chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 6)
                                    ' 17.07.2019
                                    sString = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 6)

                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '----------
                                    ' CurrencyCode
                                    '----------
                                    ' After sixth chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 7)
                                    ' 17.07.2019
                                    sString = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 7)

                                    sMySQL = sMySQL & "'" & Trim(sString) & "')"

                                    oMyDal.SQL = sMySQL

                                    If oMyDal.ExecuteNonQuery Then
                                        If oMyDal.RecordsAffected = 1 Then
                                            'OK
                                        Else
                                            Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                        End If
                                    Else
                                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                    End If

                                    ' New 17.03.06
                                    ' For new diffaccounts, added with AddEasy, there may already have
                                    ' been input records into Match_Diff (amounts for diff)
                                    ' they will then have Match_DiffAccountID = 0
                                    ' Change Match_DiffAccountID from 0 to iNewDiffAccount_ID

                                    sMySQL = "UPDATE MATCH_Diff "
                                    sMySQL = sMySQL & "SET Match_DiffAccountID = " & iNewDiffAccount_ID.ToString
                                    sMySQL = sMySQL & " WHERE Match_DiffAccountID = 0 AND Company_ID = "
                                    sMySQL = sMySQL & sCompany_ID

                                    oMyDal.SQL = sMySQL

                                    If oMyDal.ExecuteNonQuery Then
                                        If oMyDal.RecordsAffected = 1 Then
                                            'OK
                                        Else
                                            'OK, then the DiffAccountID is OK already
                                        End If
                                    Else
                                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                    End If

                                    iNewDiffAccount_ID = iNewDiffAccount_ID + 1

                                Else
                                    ' Update diffaccount
                                    'iDiffAccount_ID = VB6.GetItemData(Me.lstDiffAccounts, j)
                                    iDiffAccount_ID = Me.lstDiffAccounts.Items(j).itemdata

                                    sMySQL = "UPDATE MATCH_DiffAccounts SET "
                                    '----------
                                    ' Name
                                    '----------
                                    sMySQL = sMySQL & "Name = "
                                    'sString = Strings.Left(Trim(xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 1)), 30)
                                    sString = Strings.Left(Trim(xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 1)), 30)
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '----------
                                    ' Pattern
                                    '----------
                                    ' After first chr(9)
                                    sMySQL = sMySQL & "Pattern = "
                                    'sString = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 2)
                                    ' 17.07.2019
                                    sString = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 2)
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '----------
                                    ' Type
                                    '----------
                                    ' After third chr(9)
                                    sMySQL = sMySQL & "Type = "
                                    'sString = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 4)
                                    ' 17.07.2019
                                    sString = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 4)
                                    sMySQL = sMySQL & "" & sString & ", "

                                    '----------
                                    ' Text
                                    '----------
                                    ' After second chr(9)
                                    sMySQL = sMySQL & "Txt = "
                                    'sString = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 3)
                                    ' 17.07.2019
                                    sString = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 3)
                                    sMySQL = sMySQL & "'" & sString & "', "

                                    '-------------------------------------------------
                                    ' Function (NoMatchAccount, CashDiscount or empty)
                                    '-------------------------------------------------
                                    ' After forth chr(9)
                                    sMySQL = sMySQL & "[Function] = "
                                    'sString = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 5)
                                    ' 17.07.2019
                                    sString = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 5)
                                    ' Holds text "NOMATCH", "CASHDISCOUNT" or ""
                                    sMySQL = sMySQL & "'" & Trim(sString) & "', "

                                    '----------
                                    ' VATCode
                                    '----------
                                    ' After fifth chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 6)
                                    ' 17.07.2019
                                    sString = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 6)
                                    sMySQL = sMySQL & "VATCode = '" & sString & "',"

                                    '----------
                                    ' CurrencyCode
                                    '----------
                                    ' After sixth chr(9)
                                    'sString = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 7)
                                    ' 17.07.2019
                                    sString = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 7)
                                    sMySQL = sMySQL & "CurrencyCode = '" & sString & "'"

                                    sMySQL = sMySQL & "WHERE Company_ID = " & sCompany_ID & " AND "
                                    sMySQL = sMySQL & "Client_ID = " & iClient_ID.ToString & " AND "
                                    sMySQL = sMySQL & "DiffAccount_ID = " & iDiffAccount_ID.ToString

                                    oMyDal.SQL = sMySQL

                                    If oMyDal.ExecuteNonQuery Then
                                        If oMyDal.RecordsAffected = 1 Then
                                            'OK
                                        Else
                                            Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                        End If
                                    Else
                                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                    End If
                                End If
                            Next j

                            If Not Array_IsEmpty(aDiffAccountsDeleted) Then
                                ' Run through array with delted DiffAccounts
                                For i = 0 To UBound(aDiffAccountsDeleted)
                                    iDiffAccount_ID = aDiffAccountsDeleted(i)

                                    'First check if there are any entries in Match_Diff connected to this MATCH_DiffAccount
                                    sMySQL = "DELETE FROM MATCH_Diff "
                                    sMySQL = sMySQL & "WHERE Company_ID = " & sCompany_ID & " AND "
                                    sMySQL = sMySQL & "MATCH_DiffAccountID = " & iDiffAccount_ID.ToString

                                    oMyDal.SQL = sMySQL

                                    If oMyDal.ExecuteNonQuery Then
                                        'OK 
                                    Else
                                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                    End If

                                    sMySQL = "DELETE FROM MATCH_DiffAccounts "
                                    sMySQL = sMySQL & "WHERE Company_ID = " & sCompany_ID & " AND "
                                    sMySQL = sMySQL & "Client_ID = " & iClient_ID.ToString & " AND "
                                    sMySQL = sMySQL & "DiffAccount_ID = " & iDiffAccount_ID.ToString

                                    oMyDal.SQL = sMySQL

                                    If oMyDal.ExecuteNonQuery Then
                                        If oMyDal.RecordsAffected = 1 Then
                                            'OK
                                        Else
                                            Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                        End If
                                    Else
                                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                    End If

                                Next i
                            End If

                            '----------------------
                            ' Save PatternGroups
                            '----------------------
                            SaveClient = SavePattern(False)

                        End If ' YesNo to save client?
                    End If ' bAnythingChanged?

                    'bAnythingChanged = False
                    bClientSavedAlready = True
                Else
                    SaveClient = False
                End If ' Not bValid
            End If

        Else

            SaveClient = True

        End If

        bAnythingChanged = False
        ' Add client_id into lstClients ItemData
        ' New client always listindex 0 (topmost in listbox)
        If bThisIsNewClient Then
            ' Moved to end, because it triggers another run in SaveClient
            bThisIsNewClient = False
            'VB6.SetItemData(Me.lstClients, 0, iClient_ID)
            ' 16.07.2019
            Me.lstClients.Items(0).itemdata = iClient_ID
        End If

    End Function
    Private Function ValidateAccount(ByRef sAccountNo As String) As Boolean
        ' Is accountno unique?

        Dim sMySQL As String
        Dim sCompany_ID As String
        Dim i As Short

        sCompany_ID = "1" 'TODO: Hardcoded ComapnyID

        If Len(Trim(sAccountNo)) > 3 Then
            sMySQL = "SELECT Account, ClientNo, Name FROM Account A, Client C WHERE A.Company_ID = "
            sMySQL = sMySQL & sCompany_ID & " AND Account = '" & sAccountNo & "' AND A.Company_ID = C.Company_ID AND A.Client_ID = C.Client_ID"

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then
                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord
                        MsgBox(LRS(60379, oMyDal.Reader_GetString("ClientNo") & " " & oMyDal.Reader_GetString("Name")), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                        bDontBotherHandlingKeypress = True ' otherwise fucks up when use press return in msgbox
                        ValidateAccount = False
                        Me.txtAccountNo.SelectionStart = 0
                        Me.txtAccountNo.SelectionLength = Len(Me.txtAccountNo.Text)
                        Me.txtAccountNo.Focus()
                        Exit Do
                    Loop
                Else
                    ' Must also check against lstAccounts, to see if we have keyed it in before
                    ValidateAccount = True
                    For i = 0 To Me.lstAccounts.Items.Count - 1
                        'If Trim(Strings.Left(VB6.GetItemString(Me.lstAccounts, i), 100)) = sAccountNo Then
                        ' 16.07.2019
                        If Trim(Strings.Left(Me.lstAccounts.Items(i).itemstring, 100)) = sAccountNo Then
                            ValidateAccount = False
                            MsgBox(LRS(60379, sClientNo & " " & sClientName), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                            bDontBotherHandlingKeypress = True ' otherwise fucks up when use press return in msgbox
                            Exit For
                        End If
                    Next i
                End If
            Else
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
            End If

        End If

    End Function
    Private Function ValidateClient(ByRef sClientNo As String) As Boolean
        ' Is clientno unique?

        Dim sMySQL As String
        Dim sCompany_ID As String

        sCompany_ID = "1" 'TODO: Hardcoded Company_ID

        sMySQL = "SELECT ClientNo, Name FROM Client "
        sMySQL = sMySQL & "WHERE Company_ID = " & sCompany_ID & " AND "
        sMySQL = sMySQL & "ClientNo = '" & sClientNo & "'"

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            If oMyDal.Reader_HasRows Then
                Do While oMyDal.Reader_ReadRecord
                    MsgBox(LRS(60380, oMyDal.Reader_GetString("Name")), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                    bDontBotherHandlingKeypress = True ' otherwise fucks up when use press return in msgbox
                    ValidateClient = False
                    Me.txtClientNo.SelectionStart = 0
                    Me.txtClientNo.SelectionLength = Len(Me.txtClientNo.Text)
                    Me.txtClientName.SelectionStart = 0
                    Me.txtClientName.SelectionLength = Len(Me.txtClientName.Text)

                    Me.txtClientNo.Focus()
                    Exit Do
                Loop
            Else
                ValidateClient = True
            End If
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Function
    Private Function SavePattern(Optional ByRef bAsk As Boolean = True) As Boolean
        ' This function is called when we change to a new PatternGroup, or exits form
        Dim sMySQL As String
        Dim sCompany_ID As String
        Dim iPG_ID As Short
        Dim iPattern_ID As Short
        Dim j As Short
        Dim bSave As Boolean
        Dim iResponse As Short

        sCompany_ID = "1" 'TODO: Hardcoded Company_ID
        SavePattern = True

        'If Format(Now(), "yyyyMMdd") = "20181015" Then
        '    MsgBox("Kommer av en eller annen grunn inn i SavePattern")
        'End If

        If bPatternChanged Then
            If iDBProfile_ID < 1 Then

                MsgBox(LRS(60417), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Please select a dbProfile
                SavePattern = False
            Else

                If bAsk Then
                    'New code by Kjell 28.07.2005
                    'iResponse = MsgBox(LRS(60364) & " " & VB6.GetItemString(Me.lstPatternGroup, Me.lstPatternGroup.SelectedIndex) & " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") 'Save Patterngroup ?
                    ' 16.07.2019
                    iResponse = MsgBox(LRS(60364) & " " & Me.lstPatternGroup.Items(Me.lstPatternGroup.SelectedIndex).itemstring & " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") 'Save Patterngroup ?
                    If iResponse = MsgBoxResult.Yes Then
                        bSave = True
                    Else
                        bSave = False
                    End If
                    'Old code - always returned true
                    'bSave = MsgBox(LRS(60364) & " " & Me.lstPatternGroup.List(Me.lstPatternGroup.ListIndex) & " ?", vbYesNo, "BabelBank")   'Save Patterngroup ?
                Else
                    bSave = True
                End If
                If bSave Then
                    With Me
                        ' Save current patterngroup;

                        If iPatternGroup_ID = 0 Then
                            ' New PatternGroup!
                            ' Find next PatternGroup_ID
                            ' NB! Is it safe to presume that we MoveLast, and add 1 to PatternGroup_ID?
                            'Kjell: Yes, if we order by PatternGroup_ID.
                            sMySQL = "SELECT MAX(PatternGroup_ID) AS MaxID FROM PatternGroup "
                            sMySQL = sMySQL & "WHERE Company_ID = " & sCompany_ID & " AND "
                            'New code by Kjell 28.07.2005
                            'sMySQL = sMySQL & "DBProfile_ID=" & iDBProfile_ID
                            sMySQL = sMySQL & "DBProfile_ID = " & iDBProfile_ID.ToString

                            oMyDal.SQL = sMySQL
                            If oMyDal.Reader_Execute() Then
                                If oMyDal.Reader_HasRows Then
                                    Do While oMyDal.Reader_ReadRecord
                                        iPG_ID = CShort(oMyDal.Reader_GetString("MaxID")) + 1
                                    Loop
                                Else
                                    iPG_ID = 1
                                End If
                            Else
                                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                            End If

                            ' Do not insert PatternGroups like <Ny>, <New>
                            'If Not (Left$(.txtPatternGroup.Text, 1) = "<" And Right$(.txtPatternGroup.Text, 1) = ">") Then
                            sMySQL = "INSERT INTO PatternGroup(Company_ID, DBProfile_ID, PatternGroup_ID, Name) VALUES("
                            sMySQL = sMySQL & sCompany_ID & ", " & iDBProfile_ID.ToString & ", "
                            'sMySQL = sMySQL & iPG_ID.ToString & ", '" & Trim(VB6.GetItemString(.lstPatternGroup, .lstPatternGroup.SelectedIndex)) & "' "
                            ' 16.07.2019
                            sMySQL = sMySQL & iPG_ID.ToString & ", '" & Trim(.lstPatternGroup.Items(.lstPatternGroup.SelectedIndex).itemstring) & "' "
                            sMySQL = sMySQL & ")"

                            oMyDal.SQL = sMySQL

                            ' 30.09.2014
                            ' Exception:
                            ' Unable to save pattern when All Connections is selected
                            ' Have to select one single connection to be able to save pattern
                            ' Not ideal, but may be the best way
                            If iDBProfile_ID <> 99 Then
                                If oMyDal.ExecuteNonQuery Then
                                    If oMyDal.RecordsAffected = 1 Then
                                        'OK
                                    Else
                                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                    End If
                                Else
                                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                End If
                            Else
                                MsgBox("To add 'Patterns', Select one dbProfile, not All")
                            End If

                            bPatternChanged = False
                            If .lstPatternGroup.SelectedIndex > -1 Then
                                ' Also, update ItemData for this element in lstPatternGroups
                                'VB6.SetItemData(Me.lstPatternGroup, .lstPatternGroup.SelectedIndex, iPG_ID)
                                ' 16.07.2019
                                Me.lstPatternGroup.Items(Me.lstPatternGroup.SelectedIndex).itemdata = iPG_ID
                            End If

                            'New by Kjell 28.07.2005
                            iPatternGroup_ID = iPG_ID
                            'End If
                        Else
                            iPG_ID = iPatternGroup_ID

                            'sMySQL = "UPDATE PatternGroup SET Name = '" & Trim$(.txtPatternGroup.Text) & "' "
                            'sMySQL = "UPDATE PatternGroup SET Name = '" & Trim(VB6.GetItemString(.lstPatternGroup, .lstPatternGroup.SelectedIndex)) & "' "
                            ' 16.07.2019
                            sMySQL = "UPDATE PatternGroup SET Name = '" & Trim(.lstPatternGroup.Items(.lstPatternGroup.SelectedIndex).itemstring) & "' "
                            sMySQL = sMySQL & "WHERE Company_ID = " & sCompany_ID & " AND "
                            sMySQL = sMySQL & "DBProfile_ID = " & iDBProfile_ID.ToString & " AND "
                            sMySQL = sMySQL & "PatternGroup_ID = " & iPG_ID.ToString

                            oMyDal.SQL = sMySQL

                            If oMyDal.ExecuteNonQuery Then
                                If oMyDal.RecordsAffected = 1 Then
                                    'OK
                                Else
                                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                End If
                            Else
                                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                            End If

                        End If
                    End With


                    '----------------------
                    ' Save Patterns
                    '----------------------

                    'sCompany_ID = "1"
                    'sMySQL = "SELECT MAX(Pattern_ID) AS MaxID FROM Pattern WHERE Company_ID=" & sCompany_ID

                    'oMyDal.SQL = sMySQL
                    'If oMyDal.Reader_Execute() Then
                    '    If oMyDal.Reader_HasRows Then
                    '        Do While oMyDal.Reader_ReadRecord
                    '            iPattern_ID = CShort(oMyDal.Reader_GetString("MaxID")) + 1
                    '        Loop
                    '    Else
                    '        iPattern_ID = 1
                    '    End If
                    'Else
                    '    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                    'End If

                    For j = 0 To frmPatterns.lstPatterns.Items.Count - 1

                        'iPattern_ID = VB6.GetItemData(frmPatterns.lstPatterns, j)
                        ' 12.07.2019
                        'iPattern_ID = frmPatterns.lstPatterns.Items(j).itemdata

                        'If VB6.GetItemData(frmPatterns.lstPatterns, j) = 0 Then
                        ' 12.07.2019
                        If frmPatterns.lstPatterns.Items(j).itemdata = 0 Then
                            ' Added Pattern with AddEasy
                            ' Do not insert Patterns like <Ny>, <New>

                            '===========================================================
                            ' 12.07.2019 
                            ' find highest Pattern_ID
                            ' THIS IS MOVED INSIDE THE FOR/Next/If 12.07.2019
                            sCompany_ID = "1"
                            sMySQL = "SELECT MAX(Pattern_ID) AS MaxID FROM Pattern WHERE Company_ID=" & sCompany_ID

                            oMyDal.SQL = sMySQL
                            If oMyDal.Reader_Execute() Then
                                If oMyDal.Reader_HasRows Then
                                    Do While oMyDal.Reader_ReadRecord
                                        iPattern_ID = CShort(oMyDal.Reader_GetString("MaxID")) + 1
                                    Loop
                                Else
                                    iPattern_ID = 1
                                End If
                            Else
                                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                            End If
                            '===========================================================
                            'If Not (strings.left(VB6.GetItemString(frmPatterns.lstPatterns, j), 1) = "<" And VB.Right(VB6.GetItemString(frmPatterns.lstPatterns, j), 1) = ">") Then
                            ' 12.07.2019
                            If Not (Strings.Left(frmPatterns.lstPatterns.Items(j).itemstring, 1) = "<" And Strings.Right(frmPatterns.lstPatterns.Items(j).itemstring, 1) = ">") Then
                                sMySQL = "INSERT INTO Pattern "
                                sMySQL = sMySQL & "(Company_ID, DBProfile_ID, Client_ID, PatternGroup_ID, Pattern_ID, Description, KID "
                                sMySQL = sMySQL & ") VALUES("
                                sMySQL = sMySQL & sCompany_ID & ", "
                                sMySQL = sMySQL & iDBProfile_ID.ToString & ", "
                                sMySQL = sMySQL & iClient_ID.ToString & ", "
                                sMySQL = sMySQL & iPG_ID.ToString & ", "
                                sMySQL = sMySQL & iPattern_ID.ToString & ", "
                                'sMySQL = sMySQL & "'" & VB6.GetItemString(frmPatterns.lstPatterns, j) & "', "
                                ' 12.07.2019
                                sMySQL = sMySQL & "'" & frmPatterns.lstPatterns.Items(j).itemstring & "', "
                                If frmPatterns.lstPatterns.GetItemChecked(j) = True Then
                                    sMySQL = sMySQL & "True"
                                Else
                                    sMySQL = sMySQL & "False"
                                End If
                                sMySQL = sMySQL & ") "
                                'Set ItemData for this pattern equal to
                                ' the iPattern_ID to prevent it from being updated once more
                                'VB6.SetItemData(frmPatterns.lstPatterns, j, iPattern_ID)
                                ' 12.07.2019
                                frmPatterns.lstPatterns.Items(j).itemdata = iPattern_ID

                                iPattern_ID = iPattern_ID + 1

                                oMyDal.SQL = sMySQL

                                If oMyDal.ExecuteNonQuery Then
                                    If oMyDal.RecordsAffected = 1 Then
                                        'OK
                                    Else
                                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                    End If
                                Else
                                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                End If
                            End If
                        Else
                            ' Update Pattern
                            'iPattern_ID = VB6.GetItemData(frmPatterns.lstPatterns, j)
                            ' 12.07.2019
                            iPattern_ID = frmPatterns.lstPatterns.Items(j).itemdata

                            sMySQL = "UPDATE Pattern SET "
                            sMySQL = sMySQL & "Description = "
                            'sMySQL = sMySQL & "'" & VB6.GetItemString(frmPatterns.lstPatterns, j) & "', "
                            ' 12.07.2019
                            sMySQL = sMySQL & "'" & frmPatterns.lstPatterns.Items(j).itemstring & "', "
                            sMySQL = sMySQL & "KID = "
                            If frmPatterns.lstPatterns.GetItemChecked(j) = True Then
                                sMySQL = sMySQL & "True "
                            Else
                                sMySQL = sMySQL & "False "
                            End If

                            sMySQL = sMySQL & "WHERE Company_ID = " & sCompany_ID & " AND "
                            sMySQL = sMySQL & "DBProfile_ID = " & iDBProfile_ID.ToString & " AND "
                            sMySQL = sMySQL & "Client_ID = " & iClient_ID.ToString & " AND "
                            sMySQL = sMySQL & "PatternGroup_ID = " & iPG_ID.ToString & " AND "
                            sMySQL = sMySQL & "Pattern_ID = " & iPattern_ID.ToString

                            oMyDal.SQL = sMySQL

                            If oMyDal.ExecuteNonQuery Then
                                If oMyDal.RecordsAffected = 1 Then
                                    'OK
                                Else
                                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                End If
                            Else
                                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                            End If
                        End If
                    Next j
                End If ' bSave - YesNo to save client?
                SavePattern = True
            End If

            bPatternChanged = False
            SavePattern = True
        End If ' bAnythingChanged?

        'If Format(Now(), "yyyyMMdd") = "20181015" Then
        '    MsgBox("Da er vi ferdig!")
        'End If


    End Function
    Private Sub cmdShowClient_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdShowClient.Click
        If lstClients.SelectedIndex > -1 Then
            'frmClientDetail.AnythingChangedInThisForm = False
            Dim xClient_ID As String
            Dim i As Integer
            xClient_ID = iClient_ID
            frmClientDetail.PassfrmISO20022(frmClientDetailISO20022)
            VB6.ShowForm(frmClientDetail, 1, Me)
            If frmClientDetail.AnythingChanged Then
                'VB6.SetItemString(Me.lstClients, Me.lstClients.SelectedIndex, frmClientDetail.txtClientNo.Text & Chr(9) & frmClientDetail.txtClientName.Text)
                ' 17.07.2019
                Me.lstClients.Items(Me.lstClients.SelectedIndex).itemstring = frmClientDetail.txtClientNo.Text & Chr(9) & frmClientDetail.txtClientName.Text

                ' 15.12.2016 - this next line fucks up with call to SelectIndexChanged, and empties SetItemData for lstClients - removed!
                'Me.lstClients.Items(Me.lstClients.SelectedIndex) = frmClientDetail.txtClientNo.Text & Chr(9) & frmClientDetail.txtClientName.Text
                iClient_ID = xClient_ID
                'VB6.SetItemData(Me.lstClients, lstClients.SelectedIndex, iClient_ID)
                ' 16.07.2019
                Me.lstClients.Items(Me.lstClients.SelectedIndex).itemdata = iClient_ID
                sClientNo = frmClientDetail.txtClientNo.Text
                sClientName = frmClientDetail.txtClientName.Text 'HAGLUND - her blir det satt i VB6 (f�r lagring), men linjen to over trigger savingen
            End If
            ' SaveClient(False) TODO Trenger vi � kj�re SaveClient her ?????
        End If
    End Sub
    Private Sub FillClientDetail()
        ' Fill frmClientDetail

        dbGetClients(oMyDal, iClient_ID) 'HAGLUND - Er det riktig � bruke Client_ID

        With frmClientDetail
            If iClient_ID > 0 Then
                If oMyDal.Reader_ReadRecord Then
                    .txtClientNo.Text = oMyDal.Reader_GetString("ClientNo")
                    .txtClientName.Text = oMyDal.Reader_GetString("Name")
                    .txtCompanyNo.Text = oMyDal.Reader_GetString("CompNo")
                    .txtDivision.Text = oMyDal.Reader_GetString("Division")

                    ' added 09.07.2015
                    .txtMsgId_Variable.Text = oMyDal.Reader_GetString("MsgID_Variable")
                    .txtHistoryDeleteDays.Text = oMyDal.Reader_GetString("HistoryDelDays")
                    .lstValidationLevel.Items.Clear()
                    'NoControl = 0
                    'NormalControl = 2
                    'TightControl = 4
                    .lstValidationLevel.Items.Insert(0, LRS(60635))  ' 0 No validation
                    .lstValidationLevel.Items.Insert(1, LRS(60636))  ' 2 Normal validation
                    .lstValidationLevel.Items.Insert(2, LRS(60637))  ' 4 Tight validation
                    .lstValidationLevel.SelectedIndex = Val(oMyDal.Reader_GetString("ValidationLevel")) / 2
                    .txtSocialSecurityNo.Text = oMyDal.Reader_GetString("SocialSecurityNo")
                    If CBool(oMyDal.Reader_GetString("UseStateBankText")) = True Then
                        .chkUseStateBankText.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        .chkUseStateBankText.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("SEPASingle")) = True Then
                        .chkSEPASingle.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        .chkSEPASingle.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("SavePaymentData")) = True Then
                        .chkSavePaymentData.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        .chkSavePaymentData.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("EndToEndRefFromBabelBank")) = True Then
                        .chkEndToEndRefFromBabelBank.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        .chkEndToEndRefFromBabelBank.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("AdjustForSchemaValidation")) = True Then
                        frmClientDetailISO20022.chkAdjustToSchema.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        frmClientDetailISO20022.chkAdjustToSchema.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("AdjustForSchemaValidation")) = True Then
                        frmClientDetailISO20022.chkAdjustToSchema.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        frmClientDetailISO20022.chkAdjustToSchema.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("AdjustToBBANNorway")) = True Then
                        frmClientDetailISO20022.chkIBANToBBANNorway.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        frmClientDetailISO20022.chkIBANToBBANNorway.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("AdjustToBBANSweden")) = True Then
                        frmClientDetailISO20022.chkIBANTOBBANSweden.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        frmClientDetailISO20022.chkIBANTOBBANSweden.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("AdjustToBBANDenmark")) = True Then
                        frmClientDetailISO20022.chkIBANToBBANDenmark.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        frmClientDetailISO20022.chkIBANToBBANDenmark.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("UseDifferentInvoiceAndPaymentCurrency")) = True Then
                        frmClientDetailISO20022.chkAllowDifferenceInCurrency.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        frmClientDetailISO20022.chkAllowDifferenceInCurrency.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("ISOSplitOCR")) = True Then
                        frmClientDetailISO20022.chkISOSplitOCR.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        frmClientDetailISO20022.chkISOSplitOCR.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("ISOSplitOCRWithCredits")) = True Then
                        frmClientDetailISO20022.chkISOSplitOCRWithCredits.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        frmClientDetailISO20022.chkISOSplitOCRWithCredits.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("ISOSRedoFreetextToStruct")) = True Then
                        frmClientDetailISO20022.chkISOSRedoFreetextToStruct.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        frmClientDetailISO20022.chkISOSRedoFreetextToStruct.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("ISODoNotGroup")) = True Then
                        frmClientDetailISO20022.chkISODoNotGroup.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        frmClientDetailISO20022.chkISODoNotGroup.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("ISORemoveBICInSEPA")) = True Then
                        frmClientDetailISO20022.chkISORemoveBICInSEPA.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        frmClientDetailISO20022.chkISORemoveBICInSEPA.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("ISOUseTransferCurrency")) = True Then
                        frmClientDetailISO20022.chkUseTransferCurrency.Checked = System.Windows.Forms.CheckState.Checked
                    Else
                        frmClientDetailISO20022.chkUseTransferCurrency.Checked = System.Windows.Forms.CheckState.Unchecked
                    End If


                    ' ----------------
                    .txtVoucherNo.Text = oMyDal.Reader_GetString("VoucherNo")
                    .txtVoucherType.Text = oMyDal.Reader_GetString("VoucherType")
                    .txtVoucherNo2.Text = oMyDal.Reader_GetString("VoucherNo2")
                    .txtVoucherType2.Text = oMyDal.Reader_GetString("VoucherType2")
                    FillClientGroups()
                    .cmbClientGroup.Text = oMyDal.Reader_GetString("ClientGroup")

                    Select Case CInt(oMyDal.Reader_GetString("HowToAddVoucherNo"))
                        Case DontAddVoucherNo
                            .OptDontUse.Checked = True
                        Case AddVoucherNoFromERP
                            .optGetVNoERP.Checked = True
                        Case AddVoucherNoFromBB
                            .optGetVNoBB.Checked = True
                        Case Else
                            .OptDontUse.Checked = True
                    End Select
                    If CBool(oMyDal.Reader_GetString("AddVoucherNoBeforeOCR")) = True Then
                        .chkAddOnOcr.CheckState = System.Windows.Forms.CheckState.Checked
                    Else
                        .chkAddOnOcr.CheckState = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If CBool(oMyDal.Reader_GetString("AddCounterToVoucherNo")) = True Then
                        .chkAddVoucherCounter.CheckState = System.Windows.Forms.CheckState.Checked
                    Else
                        .chkAddVoucherCounter.CheckState = System.Windows.Forms.CheckState.Unchecked
                    End If
                    If Not oMyDal.Reader_GetString("BB_Year") = "" Then
                        .txtBB_Year.Text = oMyDal.Reader_GetString("BB_Year")
                    Else
                        .txtBB_Year.Text = Trim(Str(Year(Date.Today)))
                    End If
                    If Not oMyDal.Reader_GetString("BB_Period") = "" Then
                        .txtBB_Period.Text = oMyDal.Reader_GetString("BB_Period")
                    Else
                        .txtBB_Period.Text = Trim(Str(Month(Date.Today)))
                    End If
                End If

            Else
                ' New client
                .txtClientNo.Text = Me.txtClientNo.Text
                .txtClientName.Text = Me.txtClientName.Text
                .txtCompanyNo.Text = ""
                .txtDivision.Text = ""

                ' added 09.07.2015
                .txtMsgId_Variable.Text = ""
                .txtHistoryDeleteDays.Text = "90"
                .lstValidationLevel.Items.Clear()
                'NoControl = 0
                'NormalControl = 2
                'TightControl = 4
                .lstValidationLevel.Items.Insert(0, LRS(60635))  ' 0 No validation
                .lstValidationLevel.Items.Insert(1, LRS(60636))  ' 2 Normal validation
                .lstValidationLevel.Items.Insert(2, LRS(60637))  ' 4 Tight validation
                .lstValidationLevel.SelectedIndex = 0
                .chkUseStateBankText.Checked = False
                .chkSEPASingle.Checked = False
                .chkSavePaymentData.Checked = True
                .chkEndToEndRefFromBabelBank.Checked = True
                '---

                .txtVoucherNo.Text = ""
                .txtVoucherType.Text = ""
                .txtVoucherNo2.Text = ""
                .txtVoucherType2.Text = ""
                .OptDontUse.Checked = True
                .chkAddOnOcr.CheckState = False
                .chkAddVoucherCounter.CheckState = False
                .txtBB_Year.Text = Trim(CStr(Year(Date.Today)))
                .txtBB_Period.Text = Trim(Str(Month(Date.Today)))
            End If

            .ResetAnythingChanged()   ' Set to true when filling initial values, reset to false
        End With

    End Sub
    Private Sub FillDBProfiles()

        dbGetDBProfiles(oMyDal) ' Find all DBProfiles

        ' Fill DBProfilesList
        Me.cmbdbProfiles.Items.Clear()

        Do While oMyDal.Reader_ReadRecord
            'Me.cmbdbProfiles.Items.Add(oMyDal.Reader_GetString("Name"))
            'VB6.SetItemData(Me.cmbdbProfiles, Me.cmbdbProfiles.Items.Count - 1, oMyDal.Reader_GetString("DBProfile_ID"))
            ' 16.07.2019
            Me.cmbdbProfiles.Items.Add(New _MyListBoxItem(oMyDal.Reader_GetString("Name"), oMyDal.Reader_GetString("DBProfile_ID")))

        Loop

        ' If more than one collection, give the option av selecting all
        If Me.cmbdbProfiles.Items.Count > 1 Then
            'Me.cmbdbProfiles.Items.Add(LRS(60627))
            'VB6.SetItemData(Me.cmbdbProfiles, Me.cmbdbProfiles.Items.Count - 1, 99)
            Me.cmbdbProfiles.Items.Add(New _MyListBoxItem(LRS(60627), 99))
        End If
    End Sub
    Private Sub cmdAddEasyAccount_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAddEasyAccount.Click
        If ClientSelected() And Len(Trim(Me.txtAccountNo.Text)) > 0 Then
            If ValidateAccount(Trim(Me.txtAccountNo.Text)) Then
                ' Add account only by setting Accountno and Accountname;
                With Me
                    '.lstAccounts.Items.Insert(0, PadRight(Replace(Replace(.txtAccountNo.Text, ".", ""), " ", ""), 100, " ") & Chr(9) & " " & Chr(9) & " " & Chr(9) & " " & Chr(9) & " " & Chr(9) & " " & Chr(9) & " " & Chr(9) & " " & Chr(9) & " " & Chr(9) & " ")
                    ' 16.07.2019
                    .lstAccounts.Items.Insert(0, New _MyListBoxItem(PadRight(Replace(Replace(.txtAccountNo.Text, ".", ""), " ", ""), 100, " ") & Chr(9) & " " & Chr(9) & " " & Chr(9) & " " & Chr(9) & " " & Chr(9) & " " & Chr(9) & " " & Chr(9) & " " & Chr(9) & " " & Chr(9) & " ", 0))

                    .txtAccountNo.Text = ""
                    .txtAccountNo.SelectionStart = 0
                    .cmdRemoveAccount.Enabled = True
                    .lstAccounts.SelectedIndex = 0
                    .txtAccountNo.Focus()
                End With
                bAnythingChanged = True
                bClientSavedAlready = False  ' added 15.12.2016
            End If
        End If
    End Sub
    Private Sub cmdAddEasyDiffAccount_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAddEasyDiffAccount.Click
        Dim sTmp As String
        If ClientSelected() Then
            If Len(Trim(Me.txtDiffAccountName.Text)) > 0 And Len(Me.txtDiffPattern.Text) > 0 Then
                ' Add DiffAccount to lstDiffAccounts
                With Me

                    ' Always add as index 0
                    sTmp = ""
                    sTmp = sTmp & PadRight(.txtDiffAccountName.Text, 100, " ") & Chr(9)
                    sTmp = sTmp & Trim(.txtDiffPattern.Text) & Chr(9)
                    sTmp = sTmp & Trim(.txtDiffText.Text) & Chr(9)
                    sTmp = sTmp & Str(.cmbDiffType.SelectedIndex + 1) & Chr(9)
                    If .chkUnmatchedAccount.CheckState = 1 Then
                        sTmp = sTmp & "NOMATCH" & Chr(9)
                    ElseIf .chkCashDiscountAccount.CheckState = 1 Then
                        sTmp = sTmp & "CASHDISCOUNT"
                    Else
                        sTmp = sTmp & ""
                    End If
                    ' Also, add blank for VATCode and CurrencyCode as default
                    sTmp = sTmp & Chr(9) & "" & Chr(9) & ""

                    ' Always add as index 0
                    '.lstDiffAccounts.Items.Add(sTmp)
                    ' 17.07.2019
                    .lstDiffAccounts.Items.Add(New _MyListBoxItem(sTmp, 0))

                    .txtDiffAccountName.Text = ""
                    .txtDiffPattern.Text = ""
                    If .cmbDiffType.Items.Count > 0 Then
                        .cmbDiffType.SelectedIndex = 0
                    End If
                    .txtDiffText.Text = ""
                    .chkUnmatchedAccount.CheckState = System.Windows.Forms.CheckState.Unchecked
                    .chkCashDiscountAccount.CheckState = System.Windows.Forms.CheckState.Unchecked

                    .txtDiffAccountName.Focus()
                    .cmdRemoveDiffAccount.Enabled = True
                    .lstDiffAccounts.SelectedIndex = 0
                    ' Close Bel�psavvik until DiffAccount is saved
                    Me.cmdShowMatchDiff.Enabled = False
                End With
                bAnythingChanged = True
            End If
            Me.cmdShowMatchDiff.Enabled = True
        End If
    End Sub
    Private Sub cmdAddEasyPatternGroup_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAddEasyPatternGroup.Click
        If ClientSelected() Then
            If Len(Trim(Me.txtPatternGroup.Text)) > 0 Then
                bPatternChanged = False

                ' added 08.06.2011 - if Pattern is set, but no Db_Profile, we must warn
                If Me.cmbdbProfiles.Items.Count = 0 Then
                    MsgBox(LRS(60602), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Please select a DB-Profile for this client
                    Me.txtPatternGroup.Text = ""
                Else
                    With Me
                        '.lstPatternGroup.Items.Insert(0, .txtPatternGroup.Text)
                        ' 16.07.2019
                        .lstPatternGroup.Items.Insert(0, New _MyListBoxItem(.txtPatternGroup.Text, 0))
                        .lstPatternGroup.SelectedIndex = 0
                        bPatternChanged = True
                        ' Add PatternGroup only by setting name;
                        'new by Kjell, 27.07.2005, to trigger that it is a new Pattern Group in the function SavePattern
                        iPatternGroup_ID = 0
                        'Moved here 28.07.2005
                        If Not SavePattern(False) Then
                            ' carry on with same pattern
                        End If 'Else
                        .txtPatternGroup.Text = ""
                        .txtPatternGroup.SelectionStart = 0
                        .cmdRemovePatternGroup.Enabled = True
                        frmPatterns.txtPattern.Text = ""
                        frmPatterns.lstPatterns.Items.Clear()
                    End With
                    bAnythingChanged = True
                End If
            End If
        End If
    End Sub

    Private Sub cmdShowMatchDiff_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdShowMatchDiff.Click
        ' Show form frmMatchDiff with spread which displays current MatchDiff for selected client
        Dim i As Short
        Dim nNext_ID As Short
        Dim sCompanyID As String
        Dim iMATCHDiff_ID, iMatch_DiffAccountID As Short
        Dim bAddedNew As Boolean
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim chkColumn As DataGridViewCheckBoxColumn
        Dim lRowNo As Long
        Dim sMySQL As String

        sCompanyID = "1" 'TODO: Hardcoded Company_ID
        bAddedNew = False

        'Added 24.07.2013 - Check if an item is selected
        If Me.lstDiffAccounts.SelectedIndex > -1 Then
            'iMatch_DiffAccountID = VB6.GetItemData(Me.lstDiffAccounts, Me.lstDiffAccounts.SelectedIndex)
            iMatch_DiffAccountID = Me.lstDiffAccounts.Items(Me.lstDiffAccounts.SelectedIndex).itemdata

            ' Get MatchDiffs for currenct MatchDiffAccount_ID
            dbGetMatch_Diff(oMyDal, iMatch_DiffAccountID, nNext_ID)

            With frmMatchDiff.gridDiffs

                .ScrollBars = ScrollBars.Vertical
                '''''''''''.SelectionMode = DataGridViewSelectionMode.CellSelect
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.FixedSingle
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                '.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
                '.AutoSize = True
                '.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                '.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
                .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
                .AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells


                'Spreadshit
                '.MaxCols = 7
                '.ScrollBars = SS_SCROLLBAR_V_ONLY
                '.ScrollBarExtMode = True ' show scrollbars only when needed
                ''.OperationMode = SS_OP_MODE_SINGLE_SELECT
                '.EditEnterAction = SS_CELL_EDITMODE_EXIT_NEXT
                '.EditModeReplace = True
                '.EditModePermanent = True
                '.ProcessTab = True
                '.GrayAreaBackColor = System.Drawing.Color.White
                ''UPGRADE_ISSUE: VBControlExtender method sprDiffs.BorderStyle was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                '.BorderStyle = FPSpreadADO.BorderStyleConstants.BorderStyleNone
                '.RowHeaderDisplay = FPSpreadADO.HeaderDisplayConstants.DispBlank

                frmMatchDiff.lblExplaination.Text = LRS(60507)
                'Enter information about which deviations in amount (per currency) that should be allowed.
                'The difference will be posted according to the setup of this deviationaccount. & vbcrlf
                'The deviation will automatically be posted, if the rule under automatic matching is marked as 'Use adjustments' (Bruk avrunding).
                'However the payment will only be proposed (not fully) matched, and must be approved by a user in manual matching. & vbcrlf & vbcrlf
                'If You want to allow this deviation per invoice, mark this in the column named 'Per invoice'.
                'This is useful for roundings and reminders.

                ' remove all rows/columns from last time;
                .Columns.Clear()
                .Rows.Clear()

                ' Col headings and widths:
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.HeaderText = Replace(LRS(60212), ":", "") '"Currency"
                txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable
                txtColumn.Width = WidthFromSpreadToGrid(8)
                .Columns.Add(txtColumn)


                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.HeaderText = Replace(LRS(60211), ":", "") '"Amount"
                txtColumn.SortMode = DataGridViewColumnSortMode.NotSortable
                txtColumn.Width = WidthFromSpreadToGrid(10)
                .Columns.Add(txtColumn)

                chkColumn = New DataGridViewCheckBoxColumn
                chkColumn.HeaderText = LRS(60412) ' "Exact"
                chkColumn.SortMode = DataGridViewColumnSortMode.NotSortable
                chkColumn.Width = WidthFromSpreadToGrid(6)
                chkColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                chkColumn.FlatStyle = FlatStyle.Standard
                chkColumn.CellTemplate = New DataGridViewCheckBoxCell()
                chkColumn.TrueValue = True
                chkColumn.FalseValue = False

                .Columns.Add(chkColumn)

                ' CompanyID, hidden
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)


                ' MatchDIff_ID, hidden
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                ' MatchDiffAccount_ID, hidden
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                chkColumn = New DataGridViewCheckBoxColumn
                chkColumn.HeaderText = LRS(60506) ' "Per invoice"
                chkColumn.SortMode = DataGridViewColumnSortMode.NotSortable
                chkColumn.Width = WidthFromSpreadToGrid(10)
                chkColumn.FlatStyle = FlatStyle.Standard
                chkColumn.CellTemplate = New DataGridViewCheckBoxCell()
                chkColumn.TrueValue = True
                chkColumn.FalseValue = False
                .Columns.Add(chkColumn)

                ' Fill spread
                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord
                        .Rows.Add()
                        lRowNo = .RowCount - 1

                        ' Col1
                        .Rows(lRowNo).Cells(0).Value = oMyDal.Reader_GetString("Currency")
                        ' Col2
                        .Rows(lRowNo).Cells(1).Value = CDbl(oMyDal.Reader_GetString("Amount") / 100).ToString
                        ' Col 3
                        If CBool(oMyDal.Reader_GetString("Exact")) = True Then
                            .Rows(lRowNo).Cells(2).Value = True
                        Else
                            .Rows(lRowNo).Cells(2).Value = False
                        End If
                        ' Col 4
                        .Rows(lRowNo).Cells(3).Value = oMyDal.Reader_GetString("Company_ID")
                        ' Col 5
                        .Rows(lRowNo).Cells(4).Value = oMyDal.Reader_GetString("MATCHDiff_ID")
                        ' Col 6
                        .Rows(lRowNo).Cells(5).Value = oMyDal.Reader_GetString("MATCH_DiffAccountID")
                        ' Col 7
                        If CBool(oMyDal.Reader_GetString("AdjustPerInvoice")) = True Then
                            .Rows(lRowNo).Cells(6).Value = True
                        Else
                            .Rows(lRowNo).Cells(6).Value = False
                        End If

                    Loop
                    ' one extra line; for new diffamount
                    .Rows.Add()
                Else
                    ' one extra line; for new diffamount
                    .Rows.Add()
                End If

                ' position to top left
                .Rows(0).Cells(0).Selected = True

                frmMatchDiff.ShowDialog()

                '-------------
                ' Save if OK
                '-------------
                If frmMatchDiff.AnythingChanged Then
                    For i = 0 To .Rows.Count - 2   ' We always have a blank line at the end, thus - 2   ' .MaxRows - 1

                        If .Rows(i).Visible = False Then
                            'This item is deleted
                            If .Rows(i).Cells(3).Value <> 0 Then '(Company_ID)
                                'Delete from MATCH_Diff
                                '16.08.2020 - removed * from SQL, raise error in SQL Server
                                sMySQL = "DELETE FROM MATCH_Diff WHERE Company_ID = " & sCompanyID & " AND MatchDiff_ID = " & CShort(.Rows(i).Cells(4).Value).ToString & " AND MATCH_DiffAccountID = " & CShort(.Rows(i).Cells(5).Value).ToString
                            Else
                                'This item was both added and removed in frmMatchDiff
                                sMySQL = ""
                            End If
                        Else
                            If .Rows(i).Cells(3).Value <> 0 Then '(Company_ID)
                                ' Not a new item
                                iMATCHDiff_ID = CShort(.Rows(i).Cells(4).Value)
                                iMatch_DiffAccountID = CShort(.Rows(i).Cells(5).Value)
                            Else
                                ' New item keyed in by user
                                iMATCHDiff_ID = 0
                            End If
                            ' Empty col 5 = Newly added row;
                            If iMATCHDiff_ID = 0 Then
                                sMySQL = "INSERT INTO Match_Diff(Company_ID, MatchDiff_ID, Amount, [Currency], Exact, MATCH_DiffAccountID, AdjustPerInvoice) VALUES("
                                sMySQL = sMySQL & sCompanyID & ", " & nNext_ID.ToString & ", "
                                sMySQL = sMySQL & (ConvertToAmount(.Rows(i).Cells(1).Value, "", ",.")).ToString & ", '"
                                sMySQL = sMySQL & Strings.Left(.Rows(i).Cells(0).Value, 3) & "', "
                                If .Rows(i).Cells(2).Value = True Then
                                    sMySQL = sMySQL & "True, "
                                Else
                                    sMySQL = sMySQL & "False, "
                                End If
                                sMySQL = sMySQL & iMatch_DiffAccountID.ToString & ", "
                                If .Rows(i).Cells(6).Value = True Then
                                    sMySQL = sMySQL & "True)"
                                Else
                                    sMySQL = sMySQL & "False)"
                                End If
                                nNext_ID = nNext_ID + 1
                            Else
                                sMySQL = "UPDATE Match_Diff SET "
                                sMySQL = sMySQL & "Amount = " & (ConvertToAmount(.Rows(i).Cells(1).Value, "", ",."))
                                sMySQL = sMySQL & ", [Currency] = '" & Strings.Left(.Rows(i).Cells(0).Value, 3)
                                If .Rows(i).Cells(2).Value = True Then
                                    sMySQL = sMySQL & "', Exact = True"
                                Else
                                    sMySQL = sMySQL & "', Exact = False"
                                End If
                                If .Rows(i).Cells(6).Value = True Then
                                    sMySQL = sMySQL & ", AdjustPerInvoice = True "
                                Else
                                    sMySQL = sMySQL & ", AdjustPerInvoice = False "
                                End If
                                sMySQL = sMySQL & "WHERE Company_ID = " & sCompanyID & " AND MatchDiff_ID = " & iMATCHDiff_ID.ToString & " AND MATCH_DiffAccountID = " & iMatch_DiffAccountID.ToString
                            End If
                        End If

                        If Not sMySQL = "" Then
                            oMyDal.SQL = sMySQL

                            If oMyDal.ExecuteNonQuery Then
                                If oMyDal.RecordsAffected = 1 Then
                                    'OK
                                Else
                                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                                End If
                            Else
                                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                            End If
                        End If
                    Next i
                    ' save to table?????
                    '' save to file!  rsMatch_Diffs.Save
                End If

            End With
        End If

    End Sub

    Private Sub cmdShowPatterns_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdShowPatterns.Click

        'If Format(Now(), "yyyyMMdd") = "20181010" Then
        '    MsgBox("Kommet inn i cmdShowPatterns_Click")
        'End If

        If Me.lstPatternGroup.SelectedIndex > -1 Then

            'If Format(Now(), "yyyyMMdd") = "20181010" Then
            '    MsgBox("Indexen er > -1")
            'End If

            frmPatterns.ShowDialog()

            If frmPatterns.AnythingChanged Then
                bPatternChanged = True
                '    If frmPatterns.PatternRemoved Then
                '        RemovePattern()
                '    End If
            End If
            If bPatternChanged Then
                SavePattern(False)
            End If

            'frmPatterns.lstPatterns.
            'frmPatterns. = Nothing

        End If

    End Sub

    Private Sub cmdUse_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUse.Click
        SaveClient(False)
    End Sub

    Private Sub lstAccounts_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstAccounts.DoubleClick
        ' show accountdetails
        cmdShowAccounts_Click(cmdShowAccounts, New System.EventArgs())
    End Sub
    Private Sub lstDiffAccounts_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstDiffAccounts.DoubleClick
        'Show diffaccountsdetails
        cmdShowDiffAccounts_Click(cmdShowDiffAccounts, New System.EventArgs())
        Me.cmdShowMatchDiff.Enabled = True
    End Sub
    'UPGRADE_WARNING: Event lstDiffAccounts.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub lstDiffAccounts_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstDiffAccounts.SelectedIndexChanged
        Me.cmdShowMatchDiff.Enabled = True
    End Sub

    Private Sub lstPatternGroup_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstPatternGroup.MouseDoubleClick
        'Show pattern details
        cmdShowPatterns_Click(cmdShowPatterns, New System.EventArgs())
    End Sub

    Private Sub txtClientNo_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtClientNo.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            If Not bDontBotherHandlingKeypress Then
                ' IF Enter in txtClientNo, move to txtClientName
                Me.txtClientName.Focus()
            Else
                bDontBotherHandlingKeypress = False
            End If
        End If
    End Sub
    Private Sub txtClientName_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtClientName.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            If Not bDontBotherHandlingKeypress Then
                ' IF Enter in txtClientName, then assume that user wants to add new client
                cmdAddEasyClient_Click(cmdAddEasyClient, New System.EventArgs())
            Else
                bDontBotherHandlingKeypress = False
            End If
        End If
    End Sub

    Private Sub txtAccountNo_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAccountNo.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            If Not bDontBotherHandlingKeypress Then
                ' IF Enter in txtAccountNo, then assume that user wants to add new account
                cmdAddEasyAccount_Click(cmdAddEasyAccount, New System.EventArgs())
            Else
                bDontBotherHandlingKeypress = False
            End If
        End If
    End Sub
    Private Sub txtDiffText_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDiffText.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return in txtDiffText acts as AddEasy
            cmdAddEasyDiffAccount_Click(cmdAddEasyDiffAccount, New System.EventArgs())
        End If
    End Sub
    Private Sub txtDiffAccountName_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDiffAccountName.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtDiffPattern.Focus()
        End If
    End Sub
    Private Sub txtDiffPattern_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDiffPattern.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.cmbDiffType.Focus()
        End If
    End Sub
    Private Sub cmbDiffType_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles cmbDiffType.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtDiffText.Focus()
        End If
    End Sub

    'UPGRADE_WARNING: Event lstPatternGroup.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub lstPatternGroup_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstPatternGroup.SelectedIndexChanged

        If SavePattern() Then

            If Me.lstPatternGroup.SelectedIndex > -1 Then
                'If Format(Now(), "yyyyMMdd") = "20181015" Then
                '    MsgBox("Skal kj�re FindPatternInfo.")
                'End If
                FindPatternInfo()
                Me.cmdRemovePatternGroup.Enabled = True
            End If
        End If


    End Sub

    Private Sub FindPatternInfo()
        ' Find Patterns for selected PatternGroup
        'Me.txtPatternGroup.Text = Me.lstPatternGroup.List(Me.lstPatternGroup.ListIndex)

        frmPatterns.lstPatterns.Items.Clear()

        'iPatternGroup_ID = VB6.GetItemData(Me.lstPatternGroup, Me.lstPatternGroup.SelectedIndex)
        ' 16.07.2019
        iPatternGroup_ID = Me.lstPatternGroup.Items(Me.lstPatternGroup.SelectedIndex).itemdata

        ' Find all Patterns for current PatternGroup_ID
        dbGetPatterns(oMyDal, iDBProfile_ID, iPatternGroup_ID, iClient_ID)
        ' Fill lstPatterns - with patterns for current PatternGroup
        With frmPatterns
            If oMyDal.Reader_HasRows Then
                .SetDBProfileID(iDBProfile_ID)
                .SetClient_ID(iClient_ID)
                .SetPatternGroup_ID(iPatternGroup_ID)
                Do While oMyDal.Reader_ReadRecord
                    '.lstPatterns.Items.Add(oMyDal.Reader_GetString("Description"))
                    'VB6.SetItemData(frmPatterns.lstPatterns, frmPatterns.lstPatterns.Items.Count - 1, oMyDal.Reader_GetString("Pattern_ID"))
                    ' 12.07.2019
                    listitem = New _MyListBoxItem
                    listitem.ItemString = oMyDal.Reader_GetString("Description")
                    listitem.ItemData = oMyDal.Reader_GetString("Pattern_ID")
                    .lstPatterns.Items.Add(listitem)

                    If CBool(oMyDal.Reader_GetString("KID")) = True Then
                        .lstPatterns.SetItemChecked(.lstPatterns.Items.Count - 1, True)
                    End If
                Loop

                If .lstPatterns.Items.Count > 0 Then
                    .lstPatterns.SelectedIndex = 0
                End If
                .cmdRemovePattern.Enabled = True
            Else
                .cmdRemovePattern.Enabled = False
                .lstPatterns.Items.Clear()
            End If

        End With
        bPatternChanged = False

    End Sub
    Private Sub txtPatternGroup_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPatternGroup.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' IF Enter in txtPatternGroup, then assume that user wants to add new PatternGroup
            cmdAddEasyPatternGroup_Click(cmdAddEasyPatternGroup, New System.EventArgs())
        End If
    End Sub
    'Private Sub Disable()
    '' Disable all txts used in addeasy-buttons
    '' Is called when listboxes, other buttons than cmdAddEasy, etc are pressed
    '' NOT IN USE
    'With Me
    '.txtAccountNo.Text = ""
    '.txtDiffAccountName.Text = ""
    '.txtDiffPattern.Text = ""
    '.txtDiffText.Text = ""
    '.txtClientName.Text = ""
    '.txtClientNo.Text = ""
    '.chkUnmatchedAccount.Value = 0
    '.chkCashDiscountAccount.Value = 0
    '
    'End With
    'End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        Dim bOKToEnd As Boolean

        bOKToEnd = True

        If bOKToEnd Then
            ' Discard all changes
            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If
            frmDiffAccountDetail.Close()
            frmAccountDetail.Close()
            frmClientDetailISO20022.Close()
            frmClientDetail.Close()
            Me.Close()
        End If
    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        ' Save clientsettings to database
        '...
        '...
        If SaveClient(True) Then ' Save previous client?   ' 09.01.2017 added True for bAsk
            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If
            frmDiffAccountDetail.Close()
            frmAccountDetail.Close()
            frmClientDetailISO20022.Close()


            Me.Close()
        End If
    End Sub

    Private Sub cmdRemoveAccount_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRemoveAccount.Click

        If lstAccounts.SelectedIndex > -1 Then
            ' Delete an account from list
            'If MsgBox(LRS(60015) & " " & Trim(Strings.Left(VB6.GetItemString(lstAccounts, lstAccounts.SelectedIndex), 100)), MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett konto ?
            '24.07.2019
            If MsgBox(LRS(60015) & " " & Trim(Strings.Left(lstAccounts.Items(lstAccounts.SelectedIndex).itemstring, 100)), MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett konto ?
                If Array_IsEmpty(aAccountsDeleted) Then
                    ReDim aAccountsDeleted(0)
                Else
                    ReDim Preserve aAccountsDeleted(UBound(aAccountsDeleted) + 1)
                End If
                'aAccountsDeleted(UBound(aAccountsDeleted)) = VB6.GetItemData(lstAccounts, lstAccounts.SelectedIndex)
                ' 24.07.2019
                aAccountsDeleted(UBound(aAccountsDeleted)) = lstAccounts.Items(lstAccounts.SelectedIndex).itemdata

                lstAccounts.Items.RemoveAt((lstAccounts.SelectedIndex))
            End If
            If lstAccounts.Items.Count > 0 Then
                lstAccounts.SelectedIndex = 0
            End If
            bAnythingChanged = True
        End If
    End Sub
    Private Sub cmdRemoveDiffAccount_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRemoveDiffAccount.Click
        If lstDiffAccounts.SelectedIndex > -1 Then
            ' Delete a DiffAccounts from list
            'If MsgBox(LRS(60381) & " " & Trim(Strings.Left(VB6.GetItemString(lstDiffAccounts, lstDiffAccounts.SelectedIndex), 100)) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett DiffAccount ?
            If MsgBox(LRS(60381) & " " & Trim(Strings.Left(lstDiffAccounts.Items(lstDiffAccounts.SelectedIndex).itemstring, 100)) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett DiffAccount ?
                If Array_IsEmpty(aDiffAccountsDeleted) Then
                    ReDim aDiffAccountsDeleted(0)
                Else
                    ReDim Preserve aDiffAccountsDeleted(UBound(aDiffAccountsDeleted) + 1)
                End If
                'aDiffAccountsDeleted(UBound(aDiffAccountsDeleted)) = VB6.GetItemData(lstDiffAccounts, lstDiffAccounts.SelectedIndex)
                ' 17.07.2019
                aDiffAccountsDeleted(UBound(aDiffAccountsDeleted)) = lstDiffAccounts.Items(lstDiffAccounts.SelectedIndex).itemdata
                lstDiffAccounts.Items.RemoveAt((lstDiffAccounts.SelectedIndex))
            End If
            bAnythingChanged = True
        End If
    End Sub
    Private Sub cmdRemovePatternGroup_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRemovePatternGroup.Click
        ' Delete a PatternGroup from list
        Dim sMySQL As String
        Dim sCompany_ID As String
        Dim lPatternGroup__ID As Integer
        sCompany_ID = "1" 'TODO: Hardcoded CompanyID

        If lstPatternGroup.SelectedIndex > -1 Then
            'If MsgBox(LRS(60381) & " " & VB6.GetItemString(lstPatternGroup, lstPatternGroup.SelectedIndex) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett konto ?
            ' 16.07.2019
            If MsgBox(LRS(60381) & " " & lstPatternGroup.Items(lstPatternGroup.SelectedIndex).itemstring & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett konto ?
                'lPatternGroup__ID = VB6.GetItemData(lstPatternGroup, lstPatternGroup.SelectedIndex)
                lPatternGroup__ID = lstPatternGroup.Items(lstPatternGroup.SelectedIndex).itemdata
                sMySQL = "DELETE FROM PatternGroup "
                sMySQL = sMySQL & "WHERE Company_ID = " & sCompany_ID & " AND "
                sMySQL = sMySQL & "DBProfile_ID = " & iDBProfile_ID.ToString & " AND "
                sMySQL = sMySQL & "PatternGroup_ID = " & lPatternGroup__ID.ToString

                oMyDal.SQL = sMySQL

                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        'OK
                    Else
                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                lstPatternGroup.Items.RemoveAt((lstPatternGroup.SelectedIndex))
                If lstPatternGroup.Items.Count > 0 Then
                    lstPatternGroup.SelectedIndex = 0
                End If

            End If
            bAnythingChanged = True
        End If
    End Sub
    Private Sub cmdRemoveClient_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRemoveClient.Click
        Dim iCounter As Integer
        Dim sCompany_ID As String
        Dim sMySQL As String
        Dim aMatchDiffArray As String() = Nothing

        If ClientSelected() Then
            ' Delete an entire client
            'If MsgBox(LRS(60020) & " " & VB6.GetItemString(lstClients, lstClients.SelectedIndex) & " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett klient
            ' 17.07.2019
            If MsgBox(LRS(60020) & " " & lstClients.Items(lstClients.SelectedIndex).itemstring & " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett klient

                ' Delete from database
                sCompany_ID = "1" 'TODO: Hardcoded Company_ID

                sMySQL = "DELETE FROM Client WHERE Company_ID = " & sCompany_ID & " AND "
                sMySQL = sMySQL & "Client_ID = " & iClient_ID.ToString

                oMyDal.SQL = sMySQL

                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        'OK
                    Else
                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                ' Remove from ClientFormat;
                sMySQL = "DELETE FROM ClientFormat WHERE Company_ID = " & sCompany_ID & " AND "
                sMySQL = sMySQL & "Client_ID = " & iClient_ID.ToString

                oMyDal.SQL = sMySQL

                If oMyDal.ExecuteNonQuery Then
                    'OK
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                ' This also removes client from Notification, etc (????)
                ' Manually check ClientNotifications, and possibly remove

                'Remove from Pattern
                sMySQL = "DELETE FROM Pattern WHERE Company_ID = " & sCompany_ID & " AND "
                sMySQL = sMySQL & "Client_ID = " & Str(iClient_ID)

                oMyDal.SQL = sMySQL

                If oMyDal.ExecuteNonQuery Then
                    'OK
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                'Remove from Match_Diff
                'sMySQL = "DELETE FROM MATCH_Diff D WHERE D.Company_ID = " & sCompany_ID & " AND D.MATCH_DiffAccountID IN (SELECT DiffAccount_ID FROM MATCH_DiffAccounts A WHERE A.Client_ID = " & Str(iClient_ID) & " AND A.Company_ID = " & sCompany_ID & ")"
                sMySQL = "DELETE FROM MATCH_Diff WHERE Company_ID = " & sCompany_ID & " AND MATCH_DiffAccountID IN (SELECT DiffAccount_ID FROM MATCH_DiffAccounts A WHERE A.Client_ID = " & Str(iClient_ID) & " AND A.Company_ID = " & sCompany_ID & ")"
                oMyDal.SQL = sMySQL

                If oMyDal.ExecuteNonQuery Then
                    'OK
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                'OLD CODE TO DELETE FROM MATCH_Diff - It didn't work and is more complicated
                ''To delete from Match_Diff we have to go through Match_DiffAccounts
                ''  to see which items in Match_Diff to delete
                'sMySQL = "SELECT DiffAccount_ID FROM Match_DiffAccounts WHERE Company_ID = " & sCompany_ID & " AND Client_ID = " & iClient_ID.ToString

                'iCounter = -1
                'oMyDal.SQL = sMySQL
                'If oMyDal.Reader_Execute() Then
                '    Do While oMyDal.Reader_ReadRecord
                '        iCounter = iCounter + 1
                '        ReDim Preserve aMatchDiffArray(iCounter)
                '        aMatchDiffArray(iCounter) = CShort(oMyDal.Reader_GetString("DiffAccount_ID"))
                '    Loop
                'Else
                '    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                'End If

                'If Not aMatchDiffArray Is Nothing Then
                '    For iCounter = 0 To aMatchDiffArray.GetUpperBound(0)
                '        If Val(aMatchDiffArray(iCounter)) > 0 Then
                '            'Remove from MatchDiff
                '            sMySQL = "DELETE FROM Match_Diff WHERE Company_ID = " & sCompany_ID & " AND "
                '            sMySQL = sMySQL & "MATCH_DiffAccountID = " & aMatchDiffArray(iCounter)

                '            If oMyDal.ExecuteNonQuery Then
                '                If oMyDal.RecordsAffected > 0 Then
                '                    'OK
                '                Else
                '                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                '                End If
                '            Else
                '                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                '            End If
                '        End If
                '    Next iCounter
                'End If

                'Remove from Match_DiffAccounts
                sMySQL = "DELETE FROM Match_DiffAccounts WHERE Company_ID = " & sCompany_ID & " AND "
                sMySQL = sMySQL & "Client_ID = " & iClient_ID.ToString

                If oMyDal.ExecuteNonQuery Then
                    'OK
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                Me.txtClientNo.Text = ""
                Me.txtClientName.Text = ""
                bAnythingChanged = False
                lstClients.Items.RemoveAt((lstClients.SelectedIndex))

                If Me.lstClients.SelectedIndex < Me.lstClients.Items.Count - 1 Then
                    Me.lstClients.SelectedIndex = Me.lstClients.SelectedIndex + 1
                Else
                    If Me.lstClients.SelectedIndex > 1 Then
                        Me.lstClients.SelectedIndex = Me.lstClients.SelectedIndex - 1
                    End If
                End If
            End If
            bAnythingChanged = False
            Me.lstClients.Focus()
        End If

    End Sub
    Private Sub cmdShowAccounts_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdShowAccounts.Click
        Dim sTmp As String = ""
        If Me.lstAccounts.SelectedIndex > -1 Then

            ' Fill frmAccountDetail for this account;
            With frmAccountDetail
                '.txtAccountNo.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 1))
                '.txtBankGL.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 2))
                '.txtResultGL.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 3))
                '.txtConverted.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 4))
                '.txtContractNo.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 5))
                '.txtAvtalegiro.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 6))
                '.txtSWIFTAdr.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 7))
                '.txtAccountCountryCode.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 8))
                '.txtCurrencyCode.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 9))
                '' XokNET 07.09.2011
                '.txtDebitAccountCountryCode.Text = Trim$(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 10))

                ' 16.07.2019
                .txtAccountNo.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 1))
                .txtBankGL.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 2))
                .txtResultGL.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 3))
                .txtConverted.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 4))
                .txtContractNo.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 5))
                .txtAvtalegiro.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 6))
                .txtSWIFTAdr.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 7))
                .txtAccountCountryCode.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 8))
                .txtCurrencyCode.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 9))
                .txtDebitAccountCountryCode.Text = Trim$(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 10))


                '.SetAccount_ID(iAccount_ID)
                '.SetAccount_ID(VB6.GetItemData(Me.lstAccounts, Me.lstAccounts.SelectedIndex))
                ' 16.07.2019
                .SetAccount_ID(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemdata)
                .SetClient_ID(iClient_ID)

            End With

            ' Show frmAccountDetail
            frmAccountDetail.ShowDialog()

            ' Update listbox
            If frmAccountDetail.AnythingChanged Then
                With frmAccountDetail
                    'Me.lstAccounts.List(Me.lstAccounts.ListIndex) = PadRight(.txtAccountNo.Text, 100, " ") & Chr(9) & .txtBankGL.Text & Chr(9) & .txtResultGL.Text & Chr(9) & .txtConverted.Text & Chr(9) & .txtContractNo.Text & Chr(9) & .txtAvtalegiro.Text & Chr(9) & Trim$(.txtSWIFTAdr.Text)
                    'Me.lstAccounts.Items.Add(PadRight(.txtAccountNo.Text, 100, " ") & Chr(9) & .txtBankGL.Text & Chr(9) & .txtResultGL.Text & Chr(9) & .txtConverted.Text & Chr(9) & .txtContractNo.Text & Chr(9) & .txtAvtalegiro.Text & Chr(9) & Trim(.txtSWIFTAdr.Text) & Chr(9) & Trim(.txtAccountCountryCode.Text) & Chr(9) & Trim(.txtCurrencyCode.Text) & Chr(9) & Trim(.txtDebitAccountCountryCode.Text) & Chr(9) & iClient_ID.ToString)
                    ' replace first part of list, to show correctly in listbox
                    'Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex) = PadRight(.txtAccountNo.Text, 100, " ") & Mid(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex), 101)
                    ' Must save Itemdata, because it is reset when we change content of list
                    'sTmp = VB6.GetItemData(Me.lstAccounts, Me.lstAccounts.SelectedIndex)
                    sTmp = Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemdata
                    'Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex) = PadRight(Replace(Replace(.txtAccountNo.Text, ".", ""), " ", ""), 100, " ") & Chr(9) & .txtBankGL.Text & Chr(9) & .txtResultGL.Text & Chr(9) & .txtConverted.Text & Chr(9) & .txtContractNo.Text & Chr(9) & .txtAvtalegiro.Text & Chr(9) & Trim(.txtSWIFTAdr.Text) & Chr(9) & Trim(.txtAccountCountryCode.Text) & Chr(9) & Trim(.txtCurrencyCode.Text) & Chr(9) & Trim(.txtDebitAccountCountryCode.Text) & Chr(9) & iClient_ID.ToString
                    'VB6.SetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex, PadRight(Replace(Replace(.txtAccountNo.Text, ".", ""), " ", ""), 100, " ") & Chr(9) & .txtBankGL.Text & Chr(9) & .txtResultGL.Text & Chr(9) & .txtConverted.Text & Chr(9) & .txtContractNo.Text & Chr(9) & .txtAvtalegiro.Text & Chr(9) & Trim(.txtSWIFTAdr.Text) & Chr(9) & Trim(.txtAccountCountryCode.Text) & Chr(9) & Trim(.txtCurrencyCode.Text) & Chr(9) & Trim(.txtDebitAccountCountryCode.Text) & Chr(9) & Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 10))) 'Last, 11th item is client_ID
                    'VB6.SetItemData(Me.lstAccounts, Me.lstAccounts.SelectedIndex, sTmp)
                    Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring = PadRight(Replace(Replace(.txtAccountNo.Text, ".", ""), " ", ""), 100, " ") & Chr(9) & .txtBankGL.Text & Chr(9) & .txtResultGL.Text & Chr(9) & .txtConverted.Text & Chr(9) & .txtContractNo.Text & Chr(9) & .txtAvtalegiro.Text & Chr(9) & Trim(.txtSWIFTAdr.Text) & Chr(9) & Trim(.txtAccountCountryCode.Text) & Chr(9) & Trim(.txtCurrencyCode.Text) & Chr(9) & Trim(.txtDebitAccountCountryCode.Text) & Chr(9) & Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 10)) 'Last, 11th item is client_ID
                    Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemdata = sTmp
                End With
            End If
        End If
    End Sub
    Private Sub cmdShowDiffAccounts_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdShowDiffAccounts.Click
        Dim s As String
        Dim j As Short
        If Me.lstDiffAccounts.SelectedIndex > -1 Then
            j = Me.lstDiffAccounts.SelectedIndex
            ' Fill frmDiffAccountDetail for this account;
            With frmDiffAccountDetail
                '.txtDiffAccountName.Text = Trim(xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 1))
                ' 17.07.2019
                .txtDiffAccountName.Text = Trim(xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 1))
                .txtDiffPattern.Text = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 2)
                'If Val(CStr(CDbl(xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 4)) - 1)) > -1 Then
                If Val(CStr(CDbl(xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 4)) - 1)) > -1 Then
                    '.cmbDiffType.SelectedIndex = Val(xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 4)) - 1
                    ' 17.07.2019
                    .cmbDiffType.SelectedIndex = Val(xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 4)) - 1
                End If
                ' NOMATCH account or not
                '.chkUnmatchedAccount.CheckState = IIf(xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 5) = "NOMATCH", 1, 0)
                '' CASHDISCOUNT account or not
                '.chkCashDiscountAccount.CheckState = IIf(xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 5) = "CASHDISCOUNT", 1, 0)

                's = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 3)
                '.txtDiffText.Text = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 3)
                '.txtVATCode.Text = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 6)
                '.txtCurrencyCode.Text = xDelim(VB6.GetItemString(Me.lstDiffAccounts, j), Chr(9), 7)
                '.SetDiffAccount_ID(VB6.GetItemData(Me.lstDiffAccounts, Me.lstDiffAccounts.SelectedIndex))
                ' 17.07.2019
                .chkUnmatchedAccount.CheckState = IIf(xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 5) = "NOMATCH", 1, 0)
                ' CASHDISCOUNT account or not
                .chkCashDiscountAccount.CheckState = IIf(xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 5) = "CASHDISCOUNT", 1, 0)

                s = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 3)
                .txtDiffText.Text = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 3)
                .txtVATCode.Text = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 6)
                .txtCurrencyCode.Text = xDelim(Me.lstDiffAccounts.Items(j).itemstring, Chr(9), 7)
                .SetDiffAccount_ID(Me.lstDiffAccounts.Items(Me.lstDiffAccounts.SelectedIndex).itemdata)

                .SetClient_ID(iClient_ID)

            End With

            ' Show frmDiffAccountDetail
            frmDiffAccountDetail.ShowDialog()

            ' Update listbox
            If frmDiffAccountDetail.AnythingChanged Then
                With frmDiffAccountDetail
                    j = Me.lstDiffAccounts.SelectedIndex
                    'VB6.SetItemString(Me.lstDiffAccounts, j, PadRight(.txtDiffAccountName.Text, 100, " ") & Chr(9) & Trim(.txtDiffPattern.Text) & Chr(9) & Trim(.txtDiffText.Text) & Chr(9) & Str(.cmbDiffType.SelectedIndex + 1) & Chr(9))
                    ' 17.07.2019
                    Me.lstDiffAccounts.Items(j).itemstring = PadRight(.txtDiffAccountName.Text, 100, " ") & Chr(9) & Trim(.txtDiffPattern.Text) & Chr(9) & Trim(.txtDiffText.Text) & Chr(9) & Str(.cmbDiffType.SelectedIndex + 1) & Chr(9)
                    'If .chkUnmatchedAccount.CheckState = 1 Then
                    '    VB6.SetItemString(Me.lstDiffAccounts, j, VB6.GetItemString(Me.lstDiffAccounts, j) & "NOMATCH")
                    'ElseIf .chkCashDiscountAccount.CheckState = 1 Then
                    '    VB6.SetItemString(Me.lstDiffAccounts, j, VB6.GetItemString(Me.lstDiffAccounts, j) & "CASHDISCOUNT")
                    'Else
                    '    VB6.SetItemString(Me.lstDiffAccounts, j, VB6.GetItemString(Me.lstDiffAccounts, j) & "")
                    'End If
                    'VB6.SetItemString(Me.lstDiffAccounts, j, VB6.GetItemString(Me.lstDiffAccounts, j) & Chr(9) & Trim(.txtVATCode.Text))
                    'VB6.SetItemString(Me.lstDiffAccounts, j, VB6.GetItemString(Me.lstDiffAccounts, j) & Chr(9) & Trim(.txtCurrencyCode.Text))

                    ' 17.07.2019
                    If .chkUnmatchedAccount.CheckState = 1 Then
                        Me.lstDiffAccounts.Items(j).itemstring = Me.lstDiffAccounts.Items(j).itemstring & "NOMATCH"
                    ElseIf .chkCashDiscountAccount.CheckState = 1 Then
                        Me.lstDiffAccounts.Items(j).itemstring = Me.lstDiffAccounts.Items(j).itemstring & "CASHDISCOUNT"
                    Else
                        Me.lstDiffAccounts.Items(j).itemstring = Me.lstDiffAccounts.Items(j).itemstring & ""
                    End If
                    Me.lstDiffAccounts.Items(j).itemstring = Me.lstDiffAccounts.Items(j).itemstring & Chr(9) & Trim(.txtVATCode.Text)
                    Me.lstDiffAccounts.Items(j).itemstring = Me.lstDiffAccounts.Items(j).itemstring & Chr(9) & Trim(.txtCurrencyCode.Text)

                End With
            End If
        End If
    End Sub

    Private Sub frmClientSettings_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim iCounter As Short

        'HAGLUND
        ' Open BabelBanks database
        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If

        FormvbStyle(Me, "", 400)
        FormLRSCaptions(Me)

        ' Fill listbox for Add DiffAccounts
        'lrs(

        'Changed 27.04.2005 - see old commented code below
        Me.cmbDiffType.Items.Clear()
        Me.cmbDiffType.Items.Add(LRS(60345)) '"Kundereskontro 1"
        Me.cmbDiffType.Items.Add(LRS(60344)) '"Hovedbokskonto 2"
        Me.cmbDiffType.Items.Add(LRS(60346)) '"KID 3"
        Me.cmbDiffType.Items.Add(LRS(60347)) '"Fra ERP 4"
        Me.cmbDiffType.Items.Add(LRS(60450)) '"Don't adjust invoice" 5
        Me.cmbDiffType.Items.Add(LRS(60583)) '"Leverand�rreskontro 6  added 03.02.2017
        frmDiffAccountDetail.cmbDiffType.Items.Clear()
        frmDiffAccountDetail.cmbDiffType.Items.Add(LRS(60345)) '"Kundereskontro 1"
        frmDiffAccountDetail.cmbDiffType.Items.Add(LRS(60344)) '"Hovedbokskonto 2"
        frmDiffAccountDetail.cmbDiffType.Items.Add(LRS(60346)) '"KID 3"
        frmDiffAccountDetail.cmbDiffType.Items.Add(LRS(60347)) '"Fra ERP 4"
        frmDiffAccountDetail.cmbDiffType.Items.Add(LRS(60450)) '"Don't adjust invoice" 5
        frmDiffAccountDetail.cmbDiffType.Items.Add(LRS(60583)) '"Leverand�rreskontro 6

        ' Import clients from database
        dbGetClients(oMyDal) ' Find all clients, and all fields from clientstable

        With Me
            If oMyDal.Reader_HasRows Then
                iCounter = -1
                Do While oMyDal.Reader_ReadRecord
                    iCounter = iCounter + 1
                    If iCounter = 0 Then
                        ' Present ClientNo and ClientName in listbox:
                        .iClient_ID = oMyDal.Reader_GetString("Client_ID")

                        ' Fill listbox with clients
                        .lstClients.Items.Clear()
                    End If
                    ' Fill lstClients
                    '.lstClients.Items.Add(oMyDal.Reader_GetString("ClientNo") & Chr(9) & oMyDal.Reader_GetString("Name"))
                    'VB6.SetItemData(.lstClients, iCounter, oMyDal.Reader_GetString("Client_ID"))
                    ' 16.07.2019
                    listitem = New _MyListBoxItem
                    listitem.ItemString = oMyDal.Reader_GetString("ClientNo") & Chr(9) & oMyDal.Reader_GetString("Name")
                    listitem.ItemData = oMyDal.Reader_GetString("Client_ID")
                    .lstClients.Items.Add(listitem)

                Loop
            End If

            If .lstClients.Items.Count = 1 Then
                ' only one client, select this one
                Me.lstClients.SelectedIndex = 0
            ElseIf .lstClients.Items.Count > 0 Then
                .lstClients.SelectedIndex = -1
            End If


        End With
        bAnythingChanged = False
        bPatternChanged = False
    End Sub
    Private Sub frmDimensions_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        e.Graphics.DrawLine(Pens.Orange, 12, 208, 684, 208)
        e.Graphics.DrawLine(Pens.Orange, 12, 408, 684, 408)
        e.Graphics.DrawLine(Pens.Orange, 12, 535, 684, 535)
    End Sub
    Private Sub FindClientInfo()
        ' Get a recordset with info for current client
        Dim iDBProfileIndex As Short
        Dim iClientDBProfileID As Short = 0
        Dim sClientClientNo As String = ""
        Dim iCounter As Integer

        ' Reset arrays marking deleted elements;
        Erase aDiffAccountsDeleted
        Erase aAccountsDeleted
        Erase aPatternGroupsDeleted


        ' FIX - remove:
        ' removed 13.10.2014
        'iDBProfile_ID = 1 'TODO: Hardcoded DBProfile_ID
        '----------------

        ' Show info in form
        iProfile_ID = 0
        bShowAccounts = False
        bDontBotherHandlingKeypress = False
        Me.cmdShowMatchDiff.Enabled = False

        ' Remove info in form from any previous client;
        Me.txtAccountNo.Text = ""
        frmPatterns.txtPattern.Text = ""
        frmPatterns.lstPatterns.Items.Clear()
        Me.lstPatternGroup.Items.Clear()
        Me.txtPatternGroup.Text = ""
        Me.cmdSelectProfiles.Enabled = True
        Me.cmdAutoBookings.Enabled = True
        '----------------
        ' Fill recordsets
        '----------------

        ' Find current client
        dbGetClients(oMyDal, iClient_ID) ' Find info for current client
        If oMyDal.Reader_HasRows Then
            Do While oMyDal.Reader_ReadRecord
                iClientDBProfileID = CShort(oMyDal.Reader_GetString("DBProfile_ID"))
                sClientClientNo = oMyDal.Reader_GetString("ClientNo")
            Loop
        End If

        '----------------------------------------------
        ' Fill listboxes etc with info from recordsets
        '----------------------------------------------
        With Me
            .lstProfiles.Items.Clear()

            ' Fill Profileslist
            dbGetProfiles(oMyDal, iClient_ID) ' Find profiles for current client

            iCounter = 0
            Do While oMyDal.Reader_ReadRecord
                iCounter = iCounter + 1
                If iCounter = 1 Then
                    iProfile_ID = CShort(oMyDal.Reader_GetString("Filesetup_ID"))
                End If
                If CBool(oMyDal.Reader_GetString("AutoMatch")) = True Then
                    ' Avstemmingsprofil
                    '.lstProfiles.Items.Add(oMyDal.Reader_GetString("ShortName") & " (" & LRS(60341) & ")") 'Avstemming)"
                    ' 17.07.2019
                    .lstProfiles.Items.Add(New _MyListBoxItem(oMyDal.Reader_GetString("ShortName") & " (" & LRS(60341) & ")", 0)) 'Avstemming)"
                ElseIf CBool(oMyDal.Reader_GetString("FromAccountingSys")) = True Then
                    ' Sendprofile
                    '.lstProfiles.Items.Add(oMyDal.Reader_GetString("ShortName") & " (" & LRS(60003) & ")") 'Send)"
                    ' 17.07.2019
                    .lstProfiles.Items.Add(New _MyListBoxItem(oMyDal.Reader_GetString("ShortName") & " (" & LRS(60003) & ")", 0)) 'Send
                Else
                    ' returnprofile
                    '.lstProfiles.Items.Add(oMyDal.Reader_GetString("ShortName") & " (" & LRS(60004) & ")") '(Retur)"
                    ' 17.07.2019
                    .lstProfiles.Items.Add(New _MyListBoxItem(oMyDal.Reader_GetString("ShortName") & " (" & LRS(60004) & ")", 0)) 'Retur

                End If

                'VB6.SetItemData(.lstProfiles, .lstProfiles.Items.Count - 1, oMyDal.Reader_GetString("Filesetup_ID"))
                ' 16.07.2019
                .lstProfiles.Items(.lstProfiles.Items.Count - 1).itemdata = oMyDal.Reader_GetString("Filesetup_ID")

            Loop
            If iCounter = 1 Then
                .lstProfiles.SelectedIndex = 0
            Else
                .lstProfiles.SelectedIndex = -1
            End If

            ' Fill accountslist
            .lstAccounts.Items.Clear()
            dbGetAccounts(oMyDal, iClient_ID) ' Find accounts for current client

            iCounter = 0
            If oMyDal.Reader_HasRows Then
                Do While oMyDal.Reader_ReadRecord
                    iCounter = iCounter + 1
                    ' Changed 16.08.2007
                    '.lstAccounts.Items.Add(PadRight(oMyDal.Reader_GetString("Account"), 100, " ") & Chr(9) & oMyDal.Reader_GetString("GLAccount") & Chr(9) & oMyDal.Reader_GetString("GLResultsAccount") & Chr(9) & oMyDal.Reader_GetString("ConvertedAccount") & Chr(9) & oMyDal.Reader_GetString("ContractNo") & Chr(9) & oMyDal.Reader_GetString("AvtaleGiroID") & Chr(9) & oMyDal.Reader_GetString("SWIFTAddr") & Chr(9) & oMyDal.Reader_GetString("AccountCountryCode") & Chr(9) & oMyDal.Reader_GetString("CurrencyCode") & Chr(9) & oMyDal.Reader_GetString("DebitAccountCountryCode") & Chr(9) & oMyDal.Reader_GetString("Client_ID"))
                    'VB6.SetItemData(.lstAccounts, .lstAccounts.Items.Count - 1, oMyDal.Reader_GetString("Account_ID"))
                    ' 16.07.2019
                    .lstAccounts.Items.Add(New _MyListBoxItem(PadRight(oMyDal.Reader_GetString("Account"), 100, " ") & Chr(9) & oMyDal.Reader_GetString("GLAccount") & Chr(9) & oMyDal.Reader_GetString("GLResultsAccount") & Chr(9) & oMyDal.Reader_GetString("ConvertedAccount") & Chr(9) & oMyDal.Reader_GetString("ContractNo") & Chr(9) & oMyDal.Reader_GetString("AvtaleGiroID") & Chr(9) & oMyDal.Reader_GetString("SWIFTAddr") & Chr(9) & oMyDal.Reader_GetString("AccountCountryCode") & Chr(9) & oMyDal.Reader_GetString("CurrencyCode") & Chr(9) & oMyDal.Reader_GetString("DebitAccountCountryCode") & Chr(9) & oMyDal.Reader_GetString("Client_ID"), oMyDal.Reader_GetString("Account_ID")))
                Loop
                If iCounter = 1 Then
                    .lstAccounts.SelectedIndex = 0
                Else
                    .lstAccounts.SelectedIndex = -1
                End If
                .cmdRemoveAccount.Enabled = True
            Else
                .cmdRemoveAccount.Enabled = False
                .txtAccountNo.Text = ""
            End If

            .txtDiffAccountName.Text = ""
            .txtDiffPattern.Text = ""
            .txtDiffText.Text = ""
            .cmbDiffType.SelectedIndex = 1 ' Hovedbok
            .chkUnmatchedAccount.CheckState = System.Windows.Forms.CheckState.Unchecked

            ' Fill DiffAccountlist
            .lstDiffAccounts.Items.Clear()

            If iClient_ID > 0 Then
                dbGetDiffAccounts(oMyDal, iClient_ID) ' Find DiffAccounts for current Profile_ID
            Else
                'rsDiffAccounts = Nothing
            End If

            If Not oMyDal.Reader_HasRows Then
                ' new client
                .cmdRemoveDiffAccount.Enabled = False
            Else
                Do While oMyDal.Reader_ReadRecord
                    '.lstDiffAccounts.Items.Add(PadRight(oMyDal.Reader_GetString("Name"), 100, " ") & Chr(9) & oMyDal.Reader_GetString("Pattern") & Chr(9) & oMyDal.Reader_GetString("Txt") & Chr(9) & oMyDal.Reader_GetString("Type") & Chr(9) & oMyDal.Reader_GetString("Function") & Chr(9) & oMyDal.Reader_GetString("VATCode") & Chr(9) & oMyDal.Reader_GetString("CurrencyCode"))
                    'VB6.SetItemData(.lstDiffAccounts, .lstDiffAccounts.Items.Count - 1, oMyDal.Reader_GetString("DiffAccount_ID"))
                    ' 16.07.2019
                    .lstDiffAccounts.Items.Add(New _MyListBoxItem(PadRight(oMyDal.Reader_GetString("Name"), 100, " ") & Chr(9) & oMyDal.Reader_GetString("Pattern") & Chr(9) & oMyDal.Reader_GetString("Txt") & Chr(9) & oMyDal.Reader_GetString("Type") & Chr(9) & oMyDal.Reader_GetString("Function") & Chr(9) & oMyDal.Reader_GetString("VATCode") & Chr(9) & oMyDal.Reader_GetString("CurrencyCode"), oMyDal.Reader_GetString("DiffAccount_ID")))
                Loop
            End If

            ' Fill DBProfilesList
            .cmbdbProfiles.Items.Clear()
            dbGetDBProfiles(oMyDal) ' Find all DBProfiles

            If oMyDal.Reader_HasRows Then
                iDBProfileIndex = -1
                Do While oMyDal.Reader_ReadRecord
                    '.cmbdbProfiles.Items.Add(oMyDal.Reader_GetString("Name"))
                    'VB6.SetItemData(.cmbdbProfiles, .cmbdbProfiles.Items.Count - 1, oMyDal.Reader_GetString("DBProfile_ID"))
                    ' 16.07.2019
                    listitem = New _MyListBoxItem
                    listitem.ItemString = oMyDal.Reader_GetString("Name")
                    listitem.ItemData = oMyDal.Reader_GetString("DBProfile_ID")
                    .cmbdbProfiles.Items.Add(listitem)

                    If iClient_ID > 0 Then
                        If CShort(oMyDal.Reader_GetString("DBProfile_ID")) = iClientDBProfileID Then
                            iDBProfileIndex = cmbdbProfiles.Items.Count - 1
                            iDBProfile_ID = oMyDal.Reader_GetString("DBProfile_ID")
                        End If
                    End If
                Loop
                If .cmbdbProfiles.Items.Count > 1 Then
                    ' If more than one collection, give the option av selecting all
                    'Me.cmbdbProfiles.Items.Add(LRS(60627))
                    'VB6.SetItemData(Me.cmbdbProfiles, Me.cmbdbProfiles.Items.Count - 1, 99)
                    ' 16.07.2019
                    Me.cmbdbProfiles.Items.Add(New _MyListBoxItem(LRS(60627), 99))
                End If
                If iClient_ID > 0 Then
                    If iDBProfileIndex > -1 Then
                        ' Select correct dbProfile for this client
                        .cmbdbProfiles.SelectedIndex = iDBProfileIndex
                        bAnythingChanged = False
                    Else
                        .cmbdbProfiles.SelectedIndex = .cmbdbProfiles.Items.Count - 1
                        iDBProfile_ID = 99
                        bAnythingChanged = False
                    End If
                End If
            End If

            ' Fill lstPatternGroup - with patterngroups for current dbProfile
            dbGetPatternGroups(oMyDal, iDBProfile_ID)
            iCounter = 1
            If oMyDal.Reader_HasRows Then
                .lstPatternGroup.Items.Clear()
                Do While oMyDal.Reader_ReadRecord
                    iCounter = iCounter + 1
                    '.lstPatternGroup.Items.Add(oMyDal.Reader_GetString("Name"))
                    'VB6.SetItemData(.lstPatternGroup, .lstPatternGroup.Items.Count - 1, oMyDal.Reader_GetString("PatternGroup_ID"))
                    ' 16.07.2019
                    listitem = New _MyListBoxItem
                    listitem.ItemString = oMyDal.Reader_GetString("Name")
                    listitem.ItemData = oMyDal.Reader_GetString("PatternGroup_ID")
                    .lstPatternGroup.Items.Add(listitem)
                Loop
                If iCounter = 1 Then
                    .lstPatternGroup.SelectedIndex = 0
                    ' Fill in current patterngroupname in textbox
                    '.txtPatternGroup.Text = .lstPatternGroup.List(.lstPatternGroup.ListIndex)
                    iPatternGroup_ID = CShort(oMyDal.Reader_GetString("PatternGroup_ID"))
                Else
                    .lstPatternGroup.SelectedIndex = -1
                    .cmdRemovePatternGroup.Enabled = False
                End If
            Else
                .lstPatternGroup.SelectedIndex = -1
                .cmdRemovePatternGroup.Enabled = False
            End If

            If Not .lstPatternGroup.SelectedIndex = -1 Then
                ' Fill lstPatterns - with patterns for current PatternGroup
                ' Find all Patterns for current PatternGroup_ID
                dbGetPatterns(oMyDal, iDBProfile_ID, iPatternGroup_ID, iClient_ID)
                iCounter = 0
                If oMyDal.Reader_HasRows Then
                    With frmPatterns
                        .lstPatterns.Items.Clear()

                        Do While oMyDal.Reader_ReadRecord
                            iCounter = iCounter + 1
                            '.lstPatterns.Items.Add(oMyDal.Reader_GetString("Description"))
                            'VB6.SetItemData(.lstPatterns, .lstPatterns.Items.Count - 1, oMyDal.Reader_GetString("Pattern_ID"))
                            ' 12.07.2019
                            listitem = New _MyListBoxItem
                            listitem.ItemString = oMyDal.Reader_GetString("Description")
                            listitem.ItemData = oMyDal.Reader_GetString("Pattern_ID")
                            .lstPatterns.Items.Add(listitem)

                            If CBool(oMyDal.Reader_GetString("KID")) = True Then
                                .lstPatterns.SetItemChecked(.lstPatterns.Items.Count - 1, True)
                            End If
                        Loop
                        If iCounter = 1 Then
                            .lstPatterns.SelectedIndex = 0
                            ' Fill in current pattern in txtPattern
                            '.txtPattern.Text = VB6.GetItemString(.lstPatterns, .lstPatterns.SelectedIndex)
                            ' 12.07.2019
                            .txtPattern.Text = .lstPatterns.Items(.lstPatterns.SelectedIndex).itemstring
                        Else
                            .lstPatterns.SelectedIndex = -1
                            .cmdRemovePattern.Enabled = False
                        End If
                    End With
                Else
                    frmPatterns.cmdRemovePattern.Enabled = False
                End If
            Else
                frmPatterns.cmdRemovePattern.Enabled = False
            End If

            .lstNotifications.Items.Clear()
            If iClient_ID > 0 Then
                ' Fill Notificationslist
                dbGetClientNotifications(oMyDal, iClient_ID)
                iCounter = 0
                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord
                        iCounter = iCounter + 1
                        '.lstNotifications.Items.Add(PadRight(Strings.Left(oMyDal.Reader_GetString("Name"), 25), 25, " ") & Chr(9) & oMyDal.Reader_GetString("EMail_Adress"))
                        'VB6.SetItemData(.lstNotifications, .lstNotifications.Items.Count - 1, oMyDal.Reader_GetString("Notification_ID"))
                        ' 16.07.2019
                        listitem = New _MyListBoxItem
                        listitem.ItemString = PadRight(Strings.Left(oMyDal.Reader_GetString("Name"), 25), 25, " ") & Chr(9) & oMyDal.Reader_GetString("EMail_Adress")
                        listitem.ItemData = oMyDal.Reader_GetString("Notification_ID")
                        .lstNotifications.Items.Add(listitem)
                    Loop
                    If iCounter = 1 Then
                        .lstNotifications.SelectedIndex = 0
                    Else
                        .lstNotifications.SelectedIndex = -1
                    End If
                End If
            End If

            If iClient_ID > 0 And Not sClientClientNo = "" Then
                FillClientDetail() ' Always fill frmClientDetail
            End If

        End With

    End Sub
    Private Sub lstClients_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstClients.SelectedIndexChanged
        If SaveClient(True) Then ' Save previous client?
            If Me.lstClients.SelectedIndex > -1 Then
                'iClient_ID = VB6.GetItemData(Me.lstClients, Me.lstClients.SelectedIndex)
                ' 17.07.2019
                iClient_ID = Me.lstClients.Items(Me.lstClients.SelectedIndex).itemdata
                FindClientInfo() ' Present info for current client in list
                bAnythingChanged = False
                bPatternChanged = False
                'sClientNo = Trim(xDelim(VB6.GetItemString(Me.lstClients, Me.lstClients.SelectedIndex), Chr(9), 1))
                'sClientName = xDelim(VB6.GetItemString(Me.lstClients, Me.lstClients.SelectedIndex), Chr(9), 2)
                ' 17.07.2019
                sClientNo = Trim(xDelim(Me.lstClients.Items(Me.lstClients.SelectedIndex).itemstring, Chr(9), 1))
                sClientName = xDelim(Me.lstClients.Items(Me.lstClients.SelectedIndex).itemstring, Chr(9), 2)

                bDontBotherHandlingKeypress = False
            End If
        End If
        bClientSavedAlready = False  ' New client selected
    End Sub
    Private Sub lstClients_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstClients.DoubleClick
        ' show clientinfo
        cmdShowClient_Click(cmdShowClient, New System.EventArgs())
    End Sub
    Private Function ClientSelected() As Boolean
        If lstClients.SelectedIndex < 0 Then
            ' no client selected
            MsgBox(LRS(60378), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation)
            ClientSelected = False
        Else
            ClientSelected = True
        End If
    End Function
    Public Function Account_ID() As Short
        ' Find Account_ID for current account. Used from frmAccountOCRsettings
        MsgBox("not in use?")
        'Account_ID = VB6.GetItemData(Me.lstAccounts, Me.lstAccounts.SelectedIndex)
        ' 16.07.2019
        Account_ID = Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemdata
        ' added 18.04.2008, in case no AccountID was set in lstAccounts;
        If Account_ID = 0 Then
            Account_ID = iAccount_ID
        End If
    End Function
    Public Function AnythingChanged() As Boolean
        AnythingChanged = bAnythingChanged
    End Function
    Private Sub FillClientGroups()
        ' 16.07.2018
        ' Fill cmdGroupBox dbClientGroup in frmClientDetails

        Dim aClientGroupArray() As String
        Dim iCounter As Integer = 0

        aClientGroupArray = FindClientGroupName(False)

        ' Fill DBProfilesList
        frmClientDetail.cmbClientGroup.Items.Clear()
        If Not Array_IsEmpty(aClientGroupArray) Then
            For iCounter = 0 To aClientGroupArray.Length - 1
                frmClientDetail.cmbClientGroup.Items.Add(aClientGroupArray(iCounter))
            Next
        End If

    End Sub

End Class
