Option Strict Off
Option Explicit On
Friend Class frmAccountDetail
    Inherits System.Windows.Forms.Form

    Private frmAccountOCRSettings As frmAccountOCRSettings
    Private bAnythingChanged As Boolean
    Private iAccount_ID As Integer
    Private iClient_ID As Integer
    Public Sub SetAccount_ID(ByVal iA_Id As Integer)
        iAccount_ID = iA_Id
    End Sub
    Public Sub SetClient_ID(ByVal iC_Id As Integer)
        iClient_ID = iC_Id
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click

        If bAnythingChanged Then
            If MsgBox(LRS(60011) & " " & Trim(Me.txtAccountNo.Text), MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then
                ' Hide without saving
                bAnythingChanged = False
                Me.Hide()
            End If
        Else
            bAnythingChanged = False
            Me.Hide()
        End If
    End Sub

    Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
        'Dim bOK As Boolean
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(120))
    End Sub


    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdOK.Click
        Me.Hide()
    End Sub

    Private Sub cmdOCRSettings_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOCRSettings.Click
        ' Load form for setup of OCR-parameters for this account
        frmAccountOCRSettings = New frmAccountOCRSettings
        frmAccountOCRSettings.SetAccount_ID(iAccount_ID)
        frmAccountOCRSettings.SetClient_ID(iClient_ID)
        VB6.ShowForm(frmAccountOCRSettings, 1, Me)
        frmAccountOCRSettings.Close()
        frmAccountOCRSettings = Nothing
    End Sub

    Sub frmAccountDetail_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        FormvbStyle(Me, "dollar.jpg", 500)
        FormLRSCaptions(Me)

        ' 11.01.2011 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next

        FormSelectAllTextboxs(Me)
        bAnythingChanged = False

    End Sub

    Private Sub frmAccountDetail_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        If UnloadMode = CloseReason.UserClosing Then
            ' user pressed red x
            Cancel = 1
            cmdCancel_Click(CmdCancel, New System.EventArgs())
        End If
        eventArgs.Cancel = Cancel
    End Sub

    Private Sub txtAccountCountryCode_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAccountCountryCode.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtAccountNo_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAccountNo.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtAvtalegiro_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAvtalegiro.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtBankGL_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBankGL.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtContractNo_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtContractNo.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtConverted_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtConverted.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtResultGL_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResultGL.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtSWIFTAdr_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSWIFTAdr.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtCurrencyCode_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCurrencyCode.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtDebitAccountCountryCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDebitAccountCountryCode.TextChanged
        bAnythingChanged = True
    End Sub
    ' XokNET 07.09.2011
    Private Sub txtDebitAccountCountryCode_Change()
        ' XNET 07.09.2011 - Legg inn to kontroller nederst i formen !!!
        ' Og samme i frmTBIW_Send ett eller annet
        bAnythingChanged = True
    End Sub

    Public Function AnythingChanged() As Boolean
        AnythingChanged = bAnythingChanged
    End Function
    Public Function ResetAnythingChanged()
        bAnythingChanged = False
    End Function

    Private Sub cmdDimensionsBank_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDimensionsBank.Click
        Dim frmDimensions As New frmDimensions
        frmDimensions.SetAccount_ID(iAccount_ID)
        frmDimensions.SetClient_ID(iClient_ID)
        frmDimensions.ShowDialog()
        frmDimensions.Close()
        frmDimensions = Nothing

    End Sub

    Private Sub cmdDimensionsResult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDimensionsResult.Click
        Dim frmDimensions As New frmDimensions
        frmDimensions.SetResultAccount_ID(iAccount_ID)
        frmDimensions.SetClient_ID(iClient_ID)
        frmDimensions.ShowDialog()
        frmDimensions.Close()
        frmDimensions = Nothing

    End Sub
End Class
