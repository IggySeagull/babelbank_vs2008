﻿Option Explicit On
Friend Class frmDNB_TBIW_CurrencyField
    Inherits System.Windows.Forms.Form
    ' XokNET 03.07.2012
    ' Added this form to project
    ' LRS is already Xnetted
    Private Sub cmdCancel_Click()
        Me.Hide()
    End Sub

    Private Sub cmdOK_Click()

        SaveInfo()
        Me.Hide()

    End Sub
    Private Sub Form_Load()

        FormvbStyle(Me, "dollar.jpg", 400)
        FormLRSCaptions(Me)

        Me.cmbCode.Items.Add("<No selection>")
        Me.cmbCode.Items.Add("Invoicecurrency")
        Me.cmbCode.Items.Add("Transfercurrency")
        If oFilesetup.CurrencyToUse = "INVOICECURRENCY" Then
            Me.cmbCode.SelectedIndex = 1
        ElseIf oFilesetup.CurrencyToUse = "TRANSFERCURRENCY" Then
            Me.cmbCode.SelectedIndex = 2
        Else
            Me.cmbCode.SelectedIndex = 0
        End If

    End Sub
    Private Sub SaveInfo()
    
        If Me.cmbCode.SelectedIndex = 1 Then
            oFilesetup.CurrencyToUse = "INVOICECURRENCY"
        ElseIf Me.cmbCode.SelectedIndex = 2 Then
            oFilesetup.CurrencyToUse = "TRANSFERCURRENCY"
        Else
            oFilesetup.CurrencyToUse = ""
        End If

    End Sub


End Class