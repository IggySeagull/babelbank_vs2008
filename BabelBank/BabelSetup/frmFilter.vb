Option Strict Off
Option Explicit On
'Imports VB = Microsoft.VisualBasic
Friend Class frmFilter
    Inherits System.Windows.Forms.Form

    Private bAnythingChanged As Boolean
    Public iFileSetup_ID As Short
    Private oMyDal As vbBabel.DAL = Nothing

    Private iFilter_ID As Short
    Private iNextFilter_ID As Short
    Private bDontBotherHandlingKeypress As Boolean
    Private listitem As New _MyListBoxItem
    Private Function SaveFilter(Optional ByRef bAsk As Boolean = True) As Boolean
        Dim bYes As String
        bYes = CStr(True)

        If bAnythingChanged Then
            If bAsk Then
                bYes = CStr(MsgBox(LRS(60455) & " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes) 'Save Filter
            Else
                bYes = CStr(True)
            End If
            If CBool(bYes) Then
                ' Save all Filters
                ' They are already present in rsFilter, so we need do nothing, only commit
                'rsFilter.Close()

                'cnFilter.CommitTrans do it in cmbOK
            End If ' YesNo to save client?
        End If ' bAnythingChanged?

        If CBool(bYes) Then
            bAnythingChanged = False
        End If
        SaveFilter = CBool(bYes)

    End Function
    Private Sub cmdAddEasyFilter_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAddEasyFilter.Click
        Dim sTmp As String = ""
        Dim sMySQL As String = ""

        If Me.cmbFilterSelectField.SelectedIndex > -1 And Len(Me.txtFilterValue.Text) > 0 Then
            ' Add Filter and content of textboxes to lstfilter
            With Me

                ' Add one last Filter_ID
                iNextFilter_ID = iNextFilter_ID + 1

                ' Always add as index 0
                '.lstFilter.Items.Add(.txtFilterValue.Text)
                'VB6.SetItemData(.lstFilter, .lstFilter.Items.Count - 1, iNextFilter_ID)
                ' 11.07.2019
                listitem = New _MyListBoxItem
                listitem.ItemString = .txtFilterValue.Text
                listitem.ItemData = iNextFilter_ID
                .lstFilter.Items.Add(listitem)

                .txtFilterValue.Focus()
                .cmdRemoveFilter.Enabled = True
                .lstFilter.SelectedIndex = 0

                ' Add to database
                sMySQL = "INSERT INTO Filter(Company_ID, Filter_ID, FilterType, FilterValue, Filesetup_ID, IncludeOCR) VALUES("
                sMySQL = sMySQL & "1, " & iNextFilter_ID.ToString
                sTmp = GetFilterType(.cmbFilterSelectField.SelectedIndex)
                sMySQL = sMySQL & ", '" & sTmp & "', '" & .txtFilterValue.Text & "', " & iFileSetup_ID.ToString
                If .chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & ", True)"
                Else
                    sMySQL = sMySQL & ", False)"
                End If

                oMyDal.SQL = sMySQL
                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        'OK
                    Else
                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                .txtFilterValue.Text = ""
                .lstFilter.SelectedIndex = .lstFilter.Items.Count - 1 ' Position to last added line
            End With
            bAnythingChanged = True
        End If
        Me.cmdShowFilter.Enabled = True
    End Sub

    Private Sub cmdUpdateFilter_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUpdateFilter.Click
        Dim sMySQL As String = ""
        Dim sTemp As String = ""

        bAnythingChanged = True

        'iFilter_ID = VB6.GetItemData(Me.lstFilter, Me.lstFilter.SelectedIndex) '.NET converting: Is this the correct way to find the Filter_ID?
        ' 11.07.2019
        iFilter_ID = Me.lstFilter.Items(Me.lstFilter.SelectedIndex).itemdata

        With Me
            ' Update current info to selected list
            sMySQL = "UPDATE Filter SET "
            sTemp = GetFilterType(.cmbFilterSelectField.SelectedIndex)
            sMySQL = sMySQL & "FilterType = '" & sTemp
            If Len(Trim(.txtFilterValue.Text)) > 0 Then
                sMySQL = sMySQL & "', FilterValue = '" & .txtFilterValue.Text
                'VB6.SetItemString(.lstFilter, .lstFilter.SelectedIndex, .txtFilterValue.Text)
                ' 11.07.2019
                .lstFilter.Items(.lstFilter.SelectedIndex).itemstring = .txtFilterValue.Text

                If .chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & "', IncludeOCR = True"
                Else
                    sMySQL = sMySQL & "', IncludeOCR = False"
                End If
            End If
            sMySQL = sMySQL & " WHERE Company_ID = 1 AND FileSetup_ID = " & iFileSetup_ID.ToString & " AND Filter_ID = " & iFilter_ID.ToString

            oMyDal.SQL = sMySQL

            If oMyDal.ExecuteNonQuery Then
                If oMyDal.RecordsAffected = 1 Then
                    'OK
                Else
                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If

            .lstFilter.Items(.lstFilter.SelectedIndex) = .txtFilterValue.Text
            VB6.SetItemData(.lstFilter, .lstFilter.SelectedIndex, iFilter_ID)
            ' 11.07.2019
            .lstFilter.Items(.lstFilter.SelectedIndex).itemstring = .txtFilterValue.Text
            .lstFilter.Items(.lstFilter.SelectedIndex).itemdata = iFilter_ID
            .txtFilterValue.Text = ""

        End With

    End Sub

    Private Sub lstfilter_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstFilter.DoubleClick
        cmdShowFilter_Click(cmdShowFilter, New System.EventArgs())
        Me.cmdShowFilter.Enabled = True
    End Sub
    Private Sub lstfilter_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstFilter.SelectedIndexChanged
        Me.cmdShowFilter.Enabled = True
    End Sub
    Private Sub cmbFilterSelectField_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles cmbFilterSelectField.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtFilterValue.Focus()
        End If
    End Sub
    Private Sub txtFilterValue_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtFilterValue.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to Add-button
            Me.cmdAddEasyFilter.Focus()
        End If
    End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        Dim bOKToEnd As Boolean

        bOKToEnd = True

        If bAnythingChanged Then
            ' Ask if we are going to save first!
            bOKToEnd = SaveFilter()
        End If

        Me.Close()
    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        ' Save clientsettings to database
        '...
        '...
        If SaveFilter(False) Then
            Me.Close()
        End If
    End Sub
    Private Sub cmdRemoveFilter_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRemoveFilter.Click
        Dim sMySQL As String
        Dim sCompanyID As String
        sCompanyID = "1" 'TODO: Hardcoded CompanyID

        'Added test that a line is selected
        If Me.lstFilter.SelectedIndex > -1 Then
            iFilter_ID = VB6.GetItemData(Me.lstFilter, Me.lstFilter.SelectedIndex)
            ' 11.07.2019
            iFilter_ID = Me.lstFilter.Items(Me.lstFilter.SelectedIndex).itemdata

            If lstFilter.SelectedIndex > -1 Then
                ' Delete a Filters from list
                'If MsgBox(LRS(60381) & " " & Trim(VB.Left(VB6.GetItemString(lstFilter, lstFilter.SelectedIndex), 100)) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett Filter ?
                ' 11.07.2019
                If MsgBox(LRS(60381) & " " & Trim(Strings.Left(lstFilter.Items(lstFilter.SelectedIndex).itemstring, 100)) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett Filter ?
                    sMySQL = "DELETE FROM Filter Where Company_ID = " & sCompanyID & " AND Filter_ID = " & iFilter_ID.ToString

                    oMyDal.SQL = sMySQL
                    If oMyDal.ExecuteNonQuery Then
                        If oMyDal.RecordsAffected = 1 Then
                            'OK
                        Else
                            Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                        End If
                    Else
                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                    End If

                    lstFilter.Items.RemoveAt((lstFilter.SelectedIndex))
                    bAnythingChanged = True
                    If lstFilter.Items.Count > 0 Then
                        lstFilter.SelectedIndex = 0
                    End If
                End If

            End If
        End If
    End Sub
    Private Sub cmdShowFilter_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdShowFilter.Click
        Dim i As Short

        If Me.lstFilter.SelectedIndex > -1 Then
            'iFilter_ID = VB6.GetItemData(Me.lstFilter, Me.lstFilter.SelectedIndex)
            ' 11.07.2019
            iFilter_ID = Me.lstFilter.Items(Me.lstFilter.SelectedIndex).itemdata

            ' Find in recordset
            dbGetFilter(oMyDal, iFileSetup_ID, iFilter_ID)

            Do While oMyDal.Reader_ReadRecord
                ' Fill txtBoxes
                With Me
                    ' Find correct field
                    Select Case oMyDal.Reader_GetString("FilterType")
                        Case "REF_BANK"
                            .cmbFilterSelectField.SelectedIndex = 0
                        Case "E_ACCOUNT"
                            .cmbFilterSelectField.SelectedIndex = 1
                        Case "I_ACCOUNT"
                            .cmbFilterSelectField.SelectedIndex = 2
                        Case "I_NAME"
                            .cmbFilterSelectField.SelectedIndex = 3
                        Case "PAYCODE"
                            .cmbFilterSelectField.SelectedIndex = 4
                        Case "REF_BANK1"
                            .cmbFilterSelectField.SelectedIndex = 5
                        Case "REF_BANK2"
                            .cmbFilterSelectField.SelectedIndex = 6
                        Case "FREETEXT"
                            .cmbFilterSelectField.SelectedIndex = 7
                        Case "EXPORTED"
                            .cmbFilterSelectField.SelectedIndex = 8
                        Case "VALUEDATE_MONTH"
                            .cmbFilterSelectField.SelectedIndex = 9
                        Case "ALL_ZERO_AMOUNTS"
                            .cmbFilterSelectField.SelectedIndex = 10
                        Case "ZERO_AMOUNTS_NOTSTRUCTURED"
                            .cmbFilterSelectField.SelectedIndex = 11
                        Case "NEGATIVE AMOUNTS"
                            .cmbFilterSelectField.SelectedIndex = 12
                        Case "E_NAME"
                            .cmbFilterSelectField.SelectedIndex = 13
                        Case "IBAN_ACCOUNT"
                            .cmbFilterSelectField.SelectedIndex = 14
                    End Select

                    .txtFilterValue.Text = oMyDal.Reader_GetString("FilterValue")
                    .cmdRemoveFilter.Enabled = True
                    If CBool(oMyDal.Reader_GetString("IncludeOCR")) = True Then
                        .chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked
                    Else
                        .chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
                    End If

                    ' Update listbox, in case name has changed
                    'VB6.SetItemString(.lstFilter, .lstFilter.SelectedIndex, .txtFilterValue.Text)
                    ' 11.07.2019
                    .lstFilter.Items(.lstFilter.SelectedIndex).itemstring = .txtFilterValue.Text
                End With
            Loop

        End If
    End Sub
    Private Sub frmFilter_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim sMyFile As String
        Dim iCounter As Short

        ' Open BabelBanks database
        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If

        ' Find highest used Filter_ID;
        oMyDal.SQL = "SELECT MAX(Filter_ID) AS MaxID FROM Filter WHERE Company_ID = 1 AND Filesetup_ID = " & iFileSetup_ID.ToString '& " ORDER BY Filter_ID" 'TODO: Hardcoded CompanyID
        If oMyDal.Reader_Execute() Then
            If oMyDal.Reader_HasRows Then
                Do While oMyDal.Reader_ReadRecord
                    iNextFilter_ID = CShort(oMyDal.Reader_GetString("MaxID"))
                    Exit Do
                Loop
            Else
                iNextFilter_ID = 0
            End If
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

        FormvbStyle(Me, "", 400)
        FormLRSCaptions(Me)

        dbGetFilter(oMyDal, iFileSetup_ID) ' Find Filterings for current Filesetup

        With Me

            ' Fill in fields;
            .cmbFilterSelectField.Items.Clear()
            .cmbFilterSelectField.Items.Add(LRS(60485)) '"Bankref. (Kontoutdrag)"  ' Batch.REF_BANK
            .cmbFilterSelectField.Items.Add(LRS(60486)) '"Fra konto"               ' E_ACCOUNT
            .cmbFilterSelectField.Items.Add(LRS(60487)) '"Til konto"               ' I_ACCOUNT
            .cmbFilterSelectField.Items.Add(LRS(60488)) '"Eget Navn"                    ' I_NAME
            .cmbFilterSelectField.Items.Add(LRS(60489)) '"Transaksjonstype"        ' PAYCODE
            .cmbFilterSelectField.Items.Add(LRS(60490)) '"Bankref. 1 (Betaling)"   ' REF_BANK1
            .cmbFilterSelectField.Items.Add(LRS(60491)) '"Bankref. 2 (Betaling)"   ' REF_BANK2
            .cmbFilterSelectField.Items.Add(LRS(60492)) '"Fritekst"                ' FREETEXT
            .cmbFilterSelectField.Items.Add(LRS(60493)) '"Eksportert"              ' EXPORTED
            .cmbFilterSelectField.Items.Add(LRS(60494)) '"M�ned i valutadato       ' VALUEDATE_MONTH - Month in valuedate"
            'XokNET - 11.06.2012 - Added next 2 items
            .cmbFilterSelectField.Items.Add(LRS(60608)) '"Alle nullinnbetalinger"              ' ALL_ZERO_AMOUNTS
            .cmbFilterSelectField.Items.Add(LRS(60609)) '"Nullbetalinger, ikke strukturert       ' ZERO_AMOUNTS_NOTSTRUCTURED
            '05.04.2017 - Changed LRS from 60617 to 60624
            .cmbFilterSelectField.Items.Add(LRS(60624)) '"Negative innbetalinger"              ' ALL_ZERO_AMOUNTS
            .cmbFilterSelectField.Items.Add(LRS(60678)) '"Eksternt Navn"                    ' E_NAME
            .cmbFilterSelectField.Items.Add(LRS(60682)) '"IBAN konto kreditt"                    ' IBAN_ACCOUNT
            .cmbFilterSelectField.SelectedIndex = 0

            ' Fill lstfilter
            Me.lstFilter.Items.Clear()

            dbGetFilter(oMyDal, iFileSetup_ID)

            Do While oMyDal.Reader_ReadRecord
                '.lstFilter.Items.Add(oMyDal.Reader_GetString("FilterValue"))
                'VB6.SetItemData(.lstFilter, .lstFilter.Items.Count - 1, oMyDal.Reader_GetString("Filter_ID"))
                ' 11.07.2019
                listitem = New _MyListBoxItem
                listitem.ItemString = oMyDal.Reader_GetString("FilterValue")
                listitem.ItemData = oMyDal.Reader_GetString("Filter_ID")
                .lstFilter.Items.Add(listitem)

            Loop

        End With

        bAnythingChanged = False
    End Sub
    Private Function GetFilterType(ByVal iID As Integer) As String
        'This function returns the filtertype as a string
        Dim sReturnValue As String

        Select Case iID
            Case 0
                sReturnValue = "REF_BANK"
            Case 1
                sReturnValue = "E_ACCOUNT"
            Case 2
                sReturnValue = "I_ACCOUNT"
            Case 3
                sReturnValue = "I_NAME"
            Case 4
                sReturnValue = "PAYCODE"
            Case 5
                sReturnValue = "REF_BANK1"
            Case 6
                sReturnValue = "REF_BANK2"
            Case 7
                sReturnValue = "FREETEXT"
            Case 8
                sReturnValue = "EXPORTED"
            Case 9
                sReturnValue = "VALUEDATE_MONTH"
                'XokNET - 11.06.2012 - Added next 2 Cases
            Case 10
                sReturnValue = "ALL_ZERO_AMOUNTS"
            Case 11
                sReturnValue = "ZERO_AMOUNTS_NOTSTRUCTURED"
            Case 12
                sReturnValue = "NEGATIVE AMOUNTS"
            Case 13
                sReturnValue = "E_NAME"
            Case 14
                sReturnValue = "IBAN_ACCOUNT"

        End Select

        Return sReturnValue

    End Function
End Class
