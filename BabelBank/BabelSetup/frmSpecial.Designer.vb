<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSpecial
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents txtSpecial As System.Windows.Forms.TextBox
	Public WithEvents lblSpecial As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.txtSpecial = New System.Windows.Forms.TextBox
        Me.lblSpecial = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(72, 92)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(160, 92)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "55002 &Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'txtSpecial
        '
        Me.txtSpecial.AcceptsReturn = True
        Me.txtSpecial.BackColor = System.Drawing.SystemColors.Window
        Me.txtSpecial.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSpecial.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSpecial.Location = New System.Drawing.Point(123, 60)
        Me.txtSpecial.MaxLength = 30
        Me.txtSpecial.Name = "txtSpecial"
        Me.txtSpecial.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSpecial.Size = New System.Drawing.Size(177, 20)
        Me.txtSpecial.TabIndex = 1
        '
        'lblSpecial
        '
        Me.lblSpecial.BackColor = System.Drawing.SystemColors.Control
        Me.lblSpecial.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSpecial.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSpecial.Location = New System.Drawing.Point(11, 60)
        Me.lblSpecial.Name = "lblSpecial"
        Me.lblSpecial.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSpecial.Size = New System.Drawing.Size(97, 20)
        Me.lblSpecial.TabIndex = 0
        Me.lblSpecial.Text = "Spesial"
        '
        'frmSpecial
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(312, 125)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.txtSpecial)
        Me.Controls.Add(Me.lblSpecial)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmSpecial"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Spesial"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#End Region 
End Class
