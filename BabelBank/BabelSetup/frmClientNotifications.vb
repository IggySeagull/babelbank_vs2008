Option Strict Off
Option Explicit On
'Imports VB = Microsoft.VisualBasic
Friend Class frmClientNotifications
    Inherits System.Windows.Forms.Form

    Private frmClientSettings As frmClientSettings
    Private aNotificationsDeleted() As Short
    Private bAnythingChanged As Boolean
    Private oMyDal As vbBabel.DAL = Nothing
    Private iClient_ID As Integer
    Private listitem As New _MyListBoxItem
    Public Sub SetDAL(ByVal DAL As vbBabel.DAL)
        oMyDal = DAL
    End Sub
    Public Sub SetClient_ID(ByVal iC_Id As Integer)
        iClient_ID = iC_Id
    End Sub
    Private Sub cmdAddEasy_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAddEasy.Click
        If Len(Trim(Me.txtName.Text)) > 0 And Len(Me.txtMailAddress.Text) > 0 Then
            With Me

                ' Always add as index 0
                '.lstPossible.Items.Insert(0, PadRight(.txtName.Text, 30, " ") & Chr(9) & Trim(.txtMailAddress.Text))
                ' 11.07.2019
                listitem = New _MyListBoxItem
                listitem.ItemString = PadRight(.txtName.Text, 30, " ") & Chr(9) & Trim(.txtMailAddress.Text)
                'listitem.ItemData = Me.lstSelected.Items(iIndex).itemdata
                Me.lstPossible.Items.Insert(0, listitem)

                .txtName.Text = ""
                .txtMailAddress.Text = ""
                .lstPossible.SelectedIndex = 0

                .txtName.Focus()
            End With
        End If

    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        ' Still sp�rsm�l om avbryt uten lagring
        Me.Hide()
        bAnythingChanged = False
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Dim i As Short
        Dim iNotification_ID As Short
        Dim sMySQL As String
        Dim sCompany_ID As String
        Dim sEMail As String
        Dim sName As String

        sCompany_ID = "1" 'TODO: Hardcoded Company_ID

        Me.Hide()
        bAnythingChanged = True

        ' Add any new Notifications to database;
        ' Find next possible Notification_ID;
        sMySQL = "SELECT MAX(Notification_ID) AS MaxID FROM Notification WHERE Company_ID = " & sCompany_ID

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            If oMyDal.Reader_HasRows Then
                Do While oMyDal.Reader_ReadRecord
                    iNotification_ID = CShort(oMyDal.Reader_GetString("MaxID")) + 1
                Loop
            Else
                iNotification_ID = 1
            End If
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

        ' First, any new in lstPossible?
        For i = 0 To Me.lstPossible.Items.Count - 1
            'If VB6.GetItemData(Me.lstPossible, i) = 0 Then
            ' 11.07.2019
            If Me.lstPossible.Items(i).itemdata = 0 Then
                'sName = Trim(xDelim(VB6.GetItemString(Me.lstPossible, i), Chr(9), 1))
                'sEMail = Trim(xDelim(VB6.GetItemString(Me.lstPossible, i), Chr(9), 2))
                ' 11.07.2019
                sName = Trim(xDelim(Me.lstPossible.Items(i).itemstring, Chr(9), 1))
                sEMail = Trim(xDelim(Me.lstPossible.Items(i).itemstring, Chr(9), 2))

                sMySQL = "INSERT INTO Notification "
                sMySQL = sMySQL & "(Company_ID, Notification_ID, "
                sMySQL = sMySQL & "Name, EMail_Adress) VALUES("
                sMySQL = sMySQL & sCompany_ID & ", "
                sMySQL = sMySQL & iNotification_ID.ToString & ",' "
                sMySQL = sMySQL & sName & "',' "
                sMySQL = sMySQL & sEMail & "')"

                oMyDal.SQL = sMySQL
                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        'OK
                    Else
                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                'VB6.SetItemData(Me.lstPossible, i, iNotification_ID)
                ' 11.07.2019 changed to remove VB6. library
                Me.lstPossible.Items(i).itemdata = iNotification_ID

                iNotification_ID = iNotification_ID + 1
            End If
        Next i

        ' Then, any new in lstSelected ?
        For i = 0 To Me.lstSelected.Items.Count - 1
            'If VB6.GetItemData(Me.lstSelected, i) = 0 Then
            ' 11.07.2019
            If Me.lstSelected.Items(i).itemdata = 0 Then
                'sName = Trim(xDelim(VB6.GetItemString(Me.lstSelected, i), Chr(9), 1))
                'sEMail = Trim(xDelim(VB6.GetItemString(Me.lstSelected, i), Chr(9), 2))
                ' 11.07.2019
                sName = Trim(xDelim(Me.lstSelected.Items(i).itemstring, Chr(9), 1))
                sEMail = Trim(xDelim(Me.lstSelected.Items(i).itemstring, Chr(9), 2))

                sMySQL = "INSERT INTO Notification "
                sMySQL = sMySQL & "(Company_ID, Notification_ID, "
                sMySQL = sMySQL & "Name, EMail_Adress) VALUES("
                sMySQL = sMySQL & sCompany_ID & ", "
                sMySQL = sMySQL & iNotification_ID.ToString & ",' "
                sMySQL = sMySQL & sName & "',' "
                sMySQL = sMySQL & sEMail & "')"

                oMyDal.SQL = sMySQL
                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        'OK
                    Else
                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                'VB6.SetItemData(Me.lstSelected, i, iNotification_ID)
                ' 11.07.2019
                Me.lstSelected.Items(i).itemdata = iNotification_ID

                iNotification_ID = iNotification_ID + 1
            End If
        Next i

        sMySQL = "DELETE FROM ClientNotifications WHERE Company_ID = " & sCompany_ID & " AND "
        sMySQL = sMySQL & "Client_ID = "
        sMySQL = sMySQL & iClient_ID.ToString

        oMyDal.SQL = sMySQL
        If oMyDal.ExecuteNonQuery Then
            If oMyDal.RecordsAffected = 1 Then
                'OK
                'Else
                ' 17.07.2018 - Ved oppstart, ingen emails inne, s� vil vi jo havne i Elsen. Har derfor kommentert ut denne.
                'Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
            End If
        Else
            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
        End If

        For i = 0 To Me.lstSelected.Items.Count - 1

            ' Then add to ClientNotification, to indicate that this client
            ' will use this notification
            sMySQL = "INSERT INTO ClientNotifications "
            sMySQL = sMySQL & "(Company_ID, Notification_ID, Client_ID) VALUES("
            sMySQL = sMySQL & sCompany_ID & ", "
            'sMySQL = sMySQL & Str(VB6.GetItemData(Me.lstSelected, i)) & ", "
            ' 11.07.2019
            sMySQL = sMySQL & Str(Me.lstSelected.Items(i).itemdata) & ", "
            sMySQL = sMySQL & iClient_ID.ToString & ")"

            oMyDal.SQL = sMySQL
            If oMyDal.ExecuteNonQuery Then
                If oMyDal.RecordsAffected = 1 Then
                    'OK
                Else
                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If
        Next i

        ' Check if any Notifiations have been deleted?
        If Not Array_IsEmpty(aNotificationsDeleted) Then
            ' Run through array with deleted Notifications
            For i = 0 To UBound(aNotificationsDeleted)
                iNotification_ID = aNotificationsDeleted(i)
                sMySQL = "DELETE FROM Notification "
                sMySQL = sMySQL & "WHERE Company_ID = " & sCompany_ID & " AND "
                sMySQL = sMySQL & "Notification_ID = " & iNotification_ID.ToString

                oMyDal.SQL = sMySQL
                If oMyDal.ExecuteNonQuery Then
                    'If oMyDal.RecordsAffected = 1 Then
                    '    'OK
                    'Else
                    '    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                    'End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

                ' Must also delete from ClientNotification (if present)
                sMySQL = "DELETE FROM ClientNotifications WHERE Company_ID = " & sCompany_ID & " AND "
                sMySQL = sMySQL & "Notification_ID = " & iNotification_ID.ToString

                oMyDal.SQL = sMySQL
                If oMyDal.ExecuteNonQuery Then
                    'If oMyDal.RecordsAffected = 1 Then
                    '    'OK
                    'Else
                    '    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                    'End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If
            Next i
        End If


    End Sub

    Private Sub cmdRemoveDiffAccount_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRemoveDiffAccount.Click
        If Me.lstPossible.SelectedIndex > -1 Then
            ' If MsgBox(LRS(60381) & " " & Trim(Strings.Left(VB6.GetItemString(lstPossible, lstPossible.SelectedIndex), 30)) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett DiffAccount ?
            ' 11.07.2019
            If MsgBox(LRS(60381) & " " & Trim(Strings.Left(lstPossible.Items(lstPossible.SelectedIndex).itemstring, 30)) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett DiffAccount ?
                If Array_IsEmpty(aNotificationsDeleted) Then
                    ReDim aNotificationsDeleted(0)
                Else
                    ReDim Preserve aNotificationsDeleted(UBound(aNotificationsDeleted) + 1)
                End If
                'aNotificationsDeleted(UBound(aNotificationsDeleted)) = VB6.GetItemData(lstPossible, lstPossible.SelectedIndex)
                ' 11.07.2019
                aNotificationsDeleted(UBound(aNotificationsDeleted)) = lstPossible.Items(lstPossible.SelectedIndex).itemdata

                ' Remove from lstPossible
                Me.lstPossible.Items.RemoveAt((Me.lstPossible.SelectedIndex))
                If Me.lstPossible.Items.Count > 0 Then
                    Me.lstPossible.SelectedIndex = 0
                End If
            End If
        End If
    End Sub

    Private Sub cmdSelectAll_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSelectAll.Click
        Dim i As Short
        For i = Me.lstPossible.Items.Count - 1 To 0 Step -1
            Notificationselect(i)
        Next i
        If Me.lstPossible.Items.Count > 0 Then
            Me.lstSelected.SelectedIndex = 0
        End If
    End Sub

    Private Sub cmdSelectOne_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSelectOne.Click
        Dim iTmp As Integer
        iTmp = Me.lstPossible.SelectedIndex
        If Me.lstPossible.Items.Count > 0 Then
            Notificationselect(iTmp)
            If Me.lstPossible.Items.Count > 0 Then
                Me.lstPossible.SelectedIndex = 0
            End If
        End If
    End Sub
    Private Sub Notificationselect(ByRef iIndex As Short)
        Dim sString As String

        If iIndex > -1 Then
            ' Move item from lstPossible to lstSelected
            'sString = VB6.GetItemString(Me.lstPossible, iIndex)
            ' 11.07.2019
            sString = Me.lstPossible.Items(iIndex).itemstring

            ' Insert into lstSelected
            'Me.lstSelected.Items.Add((sString))
            'VB6.SetItemData(Me.lstSelected, Me.lstSelected.Items.Count - 1, VB6.GetItemData(Me.lstPossible, iIndex))
            ' 11.07.2019
            listitem = New _MyListBoxItem
            listitem.ItemString = sString
            listitem.ItemData = Me.lstPossible.Items(iIndex).itemdata
            Me.lstSelected.Items.Add(listitem)

            Me.lstPossible.Items.RemoveAt(iIndex)
        End If
    End Sub
    Private Sub cmdUnSelectAll_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUnselectAll.Click
        Dim i As Short
        For i = Me.lstSelected.Items.Count - 1 To 0 Step -1
            NotificationsUnSelect(i)
        Next i
        If Me.lstPossible.Items.Count > 0 Then
            Me.lstPossible.SelectedIndex = 0
        End If
    End Sub

    Private Sub cmdUnselectOne_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUnselectOne.Click
        Dim i As Integer
        If Me.lstSelected.SelectedIndex > -1 Then
            i = Me.lstSelected.SelectedIndex
            NotificationsUnSelect(i)
            If Me.lstSelected.Items.Count > 0 Then
                Me.lstSelected.SelectedIndex = 0
            End If
        End If

    End Sub
    Private Sub NotificationsUnSelect(ByRef iIndex As Integer)
        Dim sString As String
        Dim iItemData As Short

        If iIndex > -1 Then
            ' Move item from lstSelected to lstPossible
            'sString = VB6.GetItemString(Me.lstSelected, iIndex)
            'iItemData = VB6.GetItemData(Me.lstSelected, iIndex)
            ' 11.07.2019
            sString = Me.lstSelected.Items(iIndex).itemstring
            iItemData = Me.lstSelected.Items(iIndex).itemdata

            ' Insert into lstPossible
            'Me.lstPossible.Items.Add((sString))
            'VB6.SetItemData(Me.lstPossible, Me.lstPossible.Items.Count - 1, VB6.GetItemData(Me.lstSelected, iIndex))
            ' 11.07.2019
            listitem = New _MyListBoxItem
            listitem.ItemString = sString
            listitem.ItemData = Me.lstSelected.Items(iIndex).itemdata
            Me.lstPossible.Items.Add(listitem)


            Me.lstPossible.SelectedIndex = Me.lstPossible.Items.Count - 1

            Me.lstSelected.Items.RemoveAt(iIndex)
            Me.lstSelected.SelectedIndex = Me.lstSelected.Items.Count - 1

        End If

    End Sub

    Private Sub frmClientNotifications_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormLRSCaptions(Me)
    End Sub
    Private Sub txtName_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtName.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtMailAddress.Focus()
        End If
    End Sub
    Private Sub txtMailAddress_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtMailAddress.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return  acts as AddEasy
            cmdAddEasy_Click(cmdAddEasy, New System.EventArgs())
        End If
    End Sub
    Public Function AnythingChanged() As Boolean
        AnythingChanged = bAnythingChanged
    End Function
    Public Function ResetAnythingChanged()
        bAnythingChanged = False
    End Function
End Class
