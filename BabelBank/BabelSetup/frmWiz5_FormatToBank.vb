Option Strict Off
Option Explicit On

Friend Class frmWiz5_FormatToBank
    Inherits System.Windows.Forms.Form
    Private listitem As _MyListBoxItem
	
	Private Sub cmdFlere_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFlere.Click
		Dim sTmp As String
		txtFlere.Visible = True
		If InStr(lstFormats.Text, "*") > 0 Then
			sTmp = Mid(lstFormats.Text, InStr(lstFormats.Text, "*"), 2)
			lstFormats.Text = sTmp
		Else
			sTmp = lstFormats.Text & " *" & Trim(txtFlere.Text)
			lstFormats.Text = sTmp
			lstFormats.Refresh()
		End If
	End Sub
    Private Sub frmWiz5_FormatToBank_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control

        FormvbStyle(Me, "dollar.jpg")
        ' 28.12.2010 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next
    End Sub
	
	Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
        ' Show Helptopic
		'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(107))
		
	End Sub
	
	Private Sub frmWiz5_FormatToBank_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
			cmdCancel_Click(cmdCancel, New System.EventArgs())
		End If
		eventArgs.Cancel = Cancel
	End Sub
	
	'UPGRADE_WARNING: Event txtFlere.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtFlere_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFlere.TextChanged
		lstFormats.Text = lstFormats.Text & " *" & Trim(txtFlere.Text)
	End Sub
	Private Sub CmdBack_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdBack.Click
		Me.Hide()
		WizardMove((-1))
	End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
		WizardMove((-99))
	End Sub
	Private Sub CmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdNext.Click
		Me.Hide()
		WizardMove((1))
	End Sub
	Private Sub cmdSpecialFormats_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSpecialFormats.Click
		' Changes between listbox for standard and special formats
		If cmdSpecialFormats.Text = LRS(60124) Then ' Do not show specialformats
			Me.FillListbox((False))
		Else
			Me.FillListbox((True))
		End If
		Me.lstFormats.Focus()
	End Sub
	Public Sub FillListbox(ByRef bSpecialFormats As Boolean)
        Dim aFormatsAndProfiles(,) As Object ' empty array, in case we skip back and forth
		Dim i As Short
		Dim iNext As Short
		
		' Fill array with formats, from format table
		aFormatsAndProfiles = LoadFormatsAndProfiles(False, bSpecialFormats) 'from accounting,
		
		' Empty listbox, in case we skip back and forth ..
		Me.lstFormats.Items.Clear()
		' fill listbox:
		For i = 0 To UBound(aFormatsAndProfiles, 2)
            'Me.lstFormats.Items.Add((aFormatsAndProfiles(0, i) + Space(100) + Str(aFormatsAndProfiles(1, i))))
            ' 15.07.2019
            listitem = New _MyListBoxItem
            listitem.ItemString = (aFormatsAndProfiles(0, i) + Space(100) + Str(aFormatsAndProfiles(1, i)))
            Me.lstFormats.Items.Add(listitem)
			' if called with existing sendprofile, show informat in listbox:
			' Check all current sendprofiles;
			oFilesetup = Setup.oProfile.FileSetups(Setup.oProfile.FileSetups.Setup_GotoInitReturn())
			Do 
                If aFormatsAndProfiles(cEnumID, i) = oFilesetup.Enum_ID Then
                    Me.lstFormats.SetSelected(Me.lstFormats.Items.Count - 1, True)
                    'VB6.SetItemData(Me.lstFormats, i, oFilesetup.FileSetup_ID)
                    ' 15.07.2019
                    Me.lstFormats.Items(i).itemdata = oFilesetup.FileSetup_ID
                End If
				' Check if more sendprofiles;
                iNext = Setup.oProfile.FileSetups.Setup_GoToNextReturn()
				If iNext = -1 Then
					Exit Do
				Else
					oFilesetup = Setup.oProfile.FileSetups(iNext)
				End If
			Loop 
			oFilesetup = Setup.oProfile.FileSetups(Setup.oProfile.FileSetups.Setup_GotoInitReturn())
			
		Next i
		If bSpecialFormats Then
			' Show specialformat
			Me.cmdSpecialFormats.Text = LRS(60124) 'Do not show Standardformats
		Else
			' Current filesetup does not showspecialformat
			Me.cmdSpecialFormats.Text = LRS(60123) 'Show Specialformats
		End If
		
	End Sub
End Class
