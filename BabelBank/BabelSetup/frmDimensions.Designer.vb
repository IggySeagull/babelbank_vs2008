﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDimensions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.txtDim3 = New System.Windows.Forms.TextBox
        Me.txtDim2 = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.lblDim1 = New System.Windows.Forms.Label
        Me.lblDim8 = New System.Windows.Forms.Label
        Me.lblDim9 = New System.Windows.Forms.Label
        Me.lblDim6 = New System.Windows.Forms.Label
        Me.lblDim7 = New System.Windows.Forms.Label
        Me.lblDim4 = New System.Windows.Forms.Label
        Me.lblDim5 = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtDim1 = New System.Windows.Forms.TextBox
        Me.txtDim8 = New System.Windows.Forms.TextBox
        Me.txtDim9 = New System.Windows.Forms.TextBox
        Me.txtDim6 = New System.Windows.Forms.TextBox
        Me.txtDim7 = New System.Windows.Forms.TextBox
        Me.txtDim4 = New System.Windows.Forms.TextBox
        Me.txtDim5 = New System.Windows.Forms.TextBox
        Me.lblDim3 = New System.Windows.Forms.Label
        Me.lblDim2 = New System.Windows.Forms.Label
        Me.lblDim10 = New System.Windows.Forms.Label
        Me.txtDim10 = New System.Windows.Forms.TextBox
        Me.lblHeading = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtDim3
        '
        Me.txtDim3.AcceptsReturn = True
        Me.txtDim3.BackColor = System.Drawing.SystemColors.Window
        Me.txtDim3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDim3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDim3.Location = New System.Drawing.Point(238, 128)
        Me.txtDim3.MaxLength = 30
        Me.txtDim3.Name = "txtDim3"
        Me.txtDim3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDim3.Size = New System.Drawing.Size(150, 20)
        Me.txtDim3.TabIndex = 2
        '
        'txtDim2
        '
        Me.txtDim2.AcceptsReturn = True
        Me.txtDim2.BackColor = System.Drawing.SystemColors.Window
        Me.txtDim2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDim2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDim2.Location = New System.Drawing.Point(238, 102)
        Me.txtDim2.MaxLength = 30
        Me.txtDim2.Name = "txtDim2"
        Me.txtDim2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDim2.Size = New System.Drawing.Size(150, 20)
        Me.txtDim2.TabIndex = 1
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(312, 392)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 10
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(234, 392)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 45
        Me.CmdCancel.TabStop = False
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'lblDim1
        '
        Me.lblDim1.BackColor = System.Drawing.SystemColors.Control
        Me.lblDim1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDim1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDim1.Location = New System.Drawing.Point(69, 79)
        Me.lblDim1.Name = "lblDim1"
        Me.lblDim1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDim1.Size = New System.Drawing.Size(150, 20)
        Me.lblDim1.TabIndex = 54
        Me.lblDim1.Text = "Dim1"
        '
        'lblDim8
        '
        Me.lblDim8.BackColor = System.Drawing.SystemColors.Control
        Me.lblDim8.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDim8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDim8.Location = New System.Drawing.Point(69, 261)
        Me.lblDim8.Name = "lblDim8"
        Me.lblDim8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDim8.Size = New System.Drawing.Size(150, 20)
        Me.lblDim8.TabIndex = 53
        Me.lblDim8.Text = "Dim8"
        '
        'lblDim9
        '
        Me.lblDim9.BackColor = System.Drawing.SystemColors.Control
        Me.lblDim9.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDim9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDim9.Location = New System.Drawing.Point(69, 287)
        Me.lblDim9.Name = "lblDim9"
        Me.lblDim9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDim9.Size = New System.Drawing.Size(150, 20)
        Me.lblDim9.TabIndex = 52
        Me.lblDim9.Text = "Dim9"
        '
        'lblDim6
        '
        Me.lblDim6.BackColor = System.Drawing.SystemColors.Control
        Me.lblDim6.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDim6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDim6.Location = New System.Drawing.Point(69, 209)
        Me.lblDim6.Name = "lblDim6"
        Me.lblDim6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDim6.Size = New System.Drawing.Size(150, 20)
        Me.lblDim6.TabIndex = 51
        Me.lblDim6.Text = "Dim6"
        '
        'lblDim7
        '
        Me.lblDim7.BackColor = System.Drawing.SystemColors.Control
        Me.lblDim7.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDim7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDim7.Location = New System.Drawing.Point(69, 235)
        Me.lblDim7.Name = "lblDim7"
        Me.lblDim7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDim7.Size = New System.Drawing.Size(150, 20)
        Me.lblDim7.TabIndex = 50
        Me.lblDim7.Text = "Dim7"
        '
        'lblDim4
        '
        Me.lblDim4.BackColor = System.Drawing.SystemColors.Control
        Me.lblDim4.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDim4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDim4.Location = New System.Drawing.Point(69, 157)
        Me.lblDim4.Name = "lblDim4"
        Me.lblDim4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDim4.Size = New System.Drawing.Size(150, 20)
        Me.lblDim4.TabIndex = 49
        Me.lblDim4.Text = "Dim4"
        '
        'lblDim5
        '
        Me.lblDim5.BackColor = System.Drawing.SystemColors.Control
        Me.lblDim5.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDim5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDim5.Location = New System.Drawing.Point(69, 183)
        Me.lblDim5.Name = "lblDim5"
        Me.lblDim5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDim5.Size = New System.Drawing.Size(150, 20)
        Me.lblDim5.TabIndex = 48
        Me.lblDim5.Text = "Dim5"
        '
        'txtDim1
        '
        Me.txtDim1.AcceptsReturn = True
        Me.txtDim1.BackColor = System.Drawing.SystemColors.Window
        Me.txtDim1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDim1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDim1.Location = New System.Drawing.Point(238, 76)
        Me.txtDim1.MaxLength = 30
        Me.txtDim1.Name = "txtDim1"
        Me.txtDim1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDim1.Size = New System.Drawing.Size(150, 20)
        Me.txtDim1.TabIndex = 0
        '
        'txtDim8
        '
        Me.txtDim8.AcceptsReturn = True
        Me.txtDim8.BackColor = System.Drawing.SystemColors.Window
        Me.txtDim8.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDim8.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDim8.Location = New System.Drawing.Point(238, 258)
        Me.txtDim8.MaxLength = 30
        Me.txtDim8.Name = "txtDim8"
        Me.txtDim8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDim8.Size = New System.Drawing.Size(150, 20)
        Me.txtDim8.TabIndex = 7
        '
        'txtDim9
        '
        Me.txtDim9.AcceptsReturn = True
        Me.txtDim9.BackColor = System.Drawing.SystemColors.Window
        Me.txtDim9.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDim9.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDim9.Location = New System.Drawing.Point(238, 284)
        Me.txtDim9.MaxLength = 30
        Me.txtDim9.Multiline = True
        Me.txtDim9.Name = "txtDim9"
        Me.txtDim9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDim9.Size = New System.Drawing.Size(150, 20)
        Me.txtDim9.TabIndex = 8
        '
        'txtDim6
        '
        Me.txtDim6.AcceptsReturn = True
        Me.txtDim6.BackColor = System.Drawing.SystemColors.Window
        Me.txtDim6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDim6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDim6.Location = New System.Drawing.Point(238, 206)
        Me.txtDim6.MaxLength = 30
        Me.txtDim6.Name = "txtDim6"
        Me.txtDim6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDim6.Size = New System.Drawing.Size(150, 20)
        Me.txtDim6.TabIndex = 5
        '
        'txtDim7
        '
        Me.txtDim7.AcceptsReturn = True
        Me.txtDim7.BackColor = System.Drawing.SystemColors.Window
        Me.txtDim7.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDim7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDim7.Location = New System.Drawing.Point(238, 232)
        Me.txtDim7.MaxLength = 30
        Me.txtDim7.Name = "txtDim7"
        Me.txtDim7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDim7.Size = New System.Drawing.Size(150, 20)
        Me.txtDim7.TabIndex = 6
        '
        'txtDim4
        '
        Me.txtDim4.AcceptsReturn = True
        Me.txtDim4.BackColor = System.Drawing.SystemColors.Window
        Me.txtDim4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDim4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDim4.Location = New System.Drawing.Point(238, 154)
        Me.txtDim4.MaxLength = 30
        Me.txtDim4.Name = "txtDim4"
        Me.txtDim4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDim4.Size = New System.Drawing.Size(150, 20)
        Me.txtDim4.TabIndex = 3
        '
        'txtDim5
        '
        Me.txtDim5.AcceptsReturn = True
        Me.txtDim5.BackColor = System.Drawing.SystemColors.Window
        Me.txtDim5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDim5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDim5.Location = New System.Drawing.Point(238, 180)
        Me.txtDim5.MaxLength = 30
        Me.txtDim5.Name = "txtDim5"
        Me.txtDim5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDim5.Size = New System.Drawing.Size(150, 20)
        Me.txtDim5.TabIndex = 4
        '
        'lblDim3
        '
        Me.lblDim3.BackColor = System.Drawing.SystemColors.Control
        Me.lblDim3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDim3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDim3.Location = New System.Drawing.Point(69, 131)
        Me.lblDim3.Name = "lblDim3"
        Me.lblDim3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDim3.Size = New System.Drawing.Size(150, 20)
        Me.lblDim3.TabIndex = 47
        Me.lblDim3.Text = "Dim3"
        '
        'lblDim2
        '
        Me.lblDim2.BackColor = System.Drawing.SystemColors.Control
        Me.lblDim2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDim2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDim2.Location = New System.Drawing.Point(69, 105)
        Me.lblDim2.Name = "lblDim2"
        Me.lblDim2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDim2.Size = New System.Drawing.Size(150, 20)
        Me.lblDim2.TabIndex = 46
        Me.lblDim2.Text = "Dim2"
        '
        'lblDim10
        '
        Me.lblDim10.BackColor = System.Drawing.SystemColors.Control
        Me.lblDim10.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDim10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDim10.Location = New System.Drawing.Point(69, 313)
        Me.lblDim10.Name = "lblDim10"
        Me.lblDim10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDim10.Size = New System.Drawing.Size(150, 20)
        Me.lblDim10.TabIndex = 59
        Me.lblDim10.Text = "Dim10"
        '
        'txtDim10
        '
        Me.txtDim10.AcceptsReturn = True
        Me.txtDim10.BackColor = System.Drawing.SystemColors.Window
        Me.txtDim10.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDim10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDim10.Location = New System.Drawing.Point(238, 310)
        Me.txtDim10.MaxLength = 30
        Me.txtDim10.Multiline = True
        Me.txtDim10.Name = "txtDim10"
        Me.txtDim10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDim10.Size = New System.Drawing.Size(150, 20)
        Me.txtDim10.TabIndex = 9
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(131, 9)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(191, 18)
        Me.lblHeading.TabIndex = 60
        Me.lblHeading.Text = "55048 - Dimensions"
        '
        'frmDimensions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(420, 429)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.txtDim10)
        Me.Controls.Add(Me.lblDim10)
        Me.Controls.Add(Me.txtDim3)
        Me.Controls.Add(Me.txtDim2)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.lblDim1)
        Me.Controls.Add(Me.lblDim8)
        Me.Controls.Add(Me.lblDim9)
        Me.Controls.Add(Me.lblDim6)
        Me.Controls.Add(Me.lblDim7)
        Me.Controls.Add(Me.lblDim4)
        Me.Controls.Add(Me.lblDim5)
        Me.Controls.Add(Me.txtDim1)
        Me.Controls.Add(Me.txtDim8)
        Me.Controls.Add(Me.txtDim9)
        Me.Controls.Add(Me.txtDim6)
        Me.Controls.Add(Me.txtDim7)
        Me.Controls.Add(Me.txtDim4)
        Me.Controls.Add(Me.txtDim5)
        Me.Controls.Add(Me.lblDim3)
        Me.Controls.Add(Me.lblDim2)
        Me.Name = "frmDimensions"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmDimensions"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents txtDim3 As System.Windows.Forms.TextBox
    Public WithEvents txtDim2 As System.Windows.Forms.TextBox
    Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents CmdCancel As System.Windows.Forms.Button
    Public WithEvents lblDim1 As System.Windows.Forms.Label
    Public WithEvents lblDim8 As System.Windows.Forms.Label
    Public WithEvents lblDim9 As System.Windows.Forms.Label
    Public WithEvents lblDim6 As System.Windows.Forms.Label
    Public WithEvents lblDim7 As System.Windows.Forms.Label
    Public WithEvents lblDim4 As System.Windows.Forms.Label
    Public WithEvents lblDim5 As System.Windows.Forms.Label
    Public WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents txtDim1 As System.Windows.Forms.TextBox
    Public WithEvents txtDim8 As System.Windows.Forms.TextBox
    Public WithEvents txtDim9 As System.Windows.Forms.TextBox
    Public WithEvents txtDim6 As System.Windows.Forms.TextBox
    Public WithEvents txtDim7 As System.Windows.Forms.TextBox
    Public WithEvents txtDim4 As System.Windows.Forms.TextBox
    Public WithEvents txtDim5 As System.Windows.Forms.TextBox
    Public WithEvents lblDim3 As System.Windows.Forms.Label
    Public WithEvents lblDim2 As System.Windows.Forms.Label
    Public WithEvents lblDim10 As System.Windows.Forms.Label
    Public WithEvents txtDim10 As System.Windows.Forms.TextBox
    Public WithEvents lblHeading As System.Windows.Forms.Label
End Class
