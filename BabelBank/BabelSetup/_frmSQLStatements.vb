Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmSQLStatements
    Inherits System.Windows.Forms.Form
    Public mintCurFrame As Short ' Current Frame visible
	Private oQuery As Query
    Private oQueryProfile As vbbabel.Profile
    Private oMyBBDal As vbBabel.DAL = Nothing
    Dim bConnected As Boolean
	Dim sPWD As String
    Private aQueryArray(,,) As String
	Private iDBProfileID As Short
	Private bNewRule, bRuleDeleted As Boolean
	Dim iOldOptionClicked, iOldListIndex As Short
	Dim sOldTxtRuleType As String
    Dim aPatternArray(,) As String
	Dim iCMBPatternIndex As Short 'Holding the index in the combbox,
	'              used to check if it is changed, and for storing (same as PatternGroup_ID in the BBDB.
	Dim blstMatchingRules_ClickCalledFromWithin As Boolean
	Dim bSequenceChanged As Boolean
    Dim sColumnsToEnterData As String

    Private Sub cmbPatterns_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPatterns.SelectedIndexChanged

        iCMBPatternIndex = cmbPatterns.SelectedIndex

    End Sub
	
	Private Sub cmdDeleteRule_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDeleteRule.Click
        Dim sMySQL As String
        Dim lRecordsAffected As Integer
        Dim iResponse As Short

        iResponse = MsgBox(LRS(60426), MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, LRS(60427))
		If iResponse = MsgBoxResult.No Then ' User chose No
			'Nothing to do, just forget the whole thing
		Else ' User chose Yes
			'Delete the rule

			sMySQL = "DELETE * FROM ERPQuery "
            sMySQL = sMySQL & " WHERE Company_ID = " & oQueryProfile.Company_ID.ToString
            sMySQL = sMySQL & " AND DBProfile_ID = " & iDBProfileID.ToString
			sMySQL = sMySQL & " AND ERPQuery_ID = " & Me.txtRuleERPIndex.Text
			
            oMyBBDal.SQL = sMySQL
            If oMyBBDal.ExecuteNonQuery Then

            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyBBDal.ErrorMessage)
            End If

            System.Windows.Forms.Application.DoEvents()
            If oMyBBDal.RecordsAffected < 1 Then
                MsgBox(LRS(60411) & vbCrLf & oMyBBDal.ErrorMessage, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60406))
                'msgbox "An unexcpected error occured.Can 't save the rule
            Else
                bRuleDeleted = True
                If optAutomatic.Checked = True Then
                    aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 1)
                    optAutomatic_CheckedChanged(optAutomatic, New System.EventArgs())
                ElseIf Me.optManual.Checked = True Then
                    aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 2)
                    optManual_CheckedChanged(optManual, New System.EventArgs())
                ElseIf Me.optAfter.Checked = True Then
                    aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 3)
                    optAfter_CheckedChanged(optAfter, New System.EventArgs())
                Else 'optValidation
                    aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 4)
                    optValidation_CheckedChanged(optValidation, New System.EventArgs())
                End If
                ' redraw lstMatchingRules

                bRuleDeleted = False
            End If
			
		End If
		
	End Sub
	
	Private Sub cmdIcon_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdIcon.Click
		' Connect a icon bitmap to selected manual rule
		Dim sGraphicsPath As String
		Dim sFindGraphicsIn As String
		Dim sFile As String
		Dim iIndex As Short
        Dim oFs As Scripting.FileSystemObject
        Dim fs As System.IO.FileStream

		On Error GoTo IconError
		
        oFs = New Scripting.FileSystemObject
		
		iIndex = Me.lstMatchingRules.SelectedIndex + 1
		If RunTime Then
			sFindGraphicsIn = My.Application.Info.DirectoryPath & "\Graphics"
			sGraphicsPath = My.Application.Info.DirectoryPath
		Else
			sFindGraphicsIn = "c:\projects\babelbank\graphics"
			sGraphicsPath = "c:\projects\babelbank\graphics"
        End If
        '31.12.2010
        sFile = BrowseForFilesOrFolders(sFindGraphicsIn, Me, LRS(60010), True)
		' if Cancel, then empty file
		If Not EmptyString(sFile) Then
			'Copy the selected file
            oFs.CopyFile(sFile, sGraphicsPath & "\find_" & Trim(Str(iIndex)) & ".bmp")
            oFs = Nothing

            'Me.imgIcon.Image = System.Drawing.Image.FromFile(sGraphicsPath & "\find_" & Trim(Str(iIndex)) & ".bmp")
            Me.imgIcon.Image = Image.FromStream(New System.IO.MemoryStream(System.IO.File.ReadAllBytes(sGraphicsPath & "\find_" & Trim(Str(iIndex)) & ".bmp")))
            Me.imgIcon.Visible = True
		End If
		Exit Sub
		
IconError: 
		If Err.Number = 481 Then
			' not a picturefile
		End If
		
	End Sub
	
	'Private Sub cmdIntConnection_Click()
	'Dim bx As Boolean
	'
	''bx = oQuery.ConnectERP(False)
	'
	'End Sub
	
	Private Sub cmdIntSave_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdIntSave.Click
		
		Dim bSaveOK As Boolean
		
		bSaveOK = False
		
		If oQuery.SaveMatchingRule(bNewRule) Then
			bSaveOK = True
		End If
		
		'Have to set the listindex in the lstmatchingrules, when we have a new rule.
		If bSaveOK Then
			If bNewRule Then
				If optAutomatic.Checked = True Then
					Call optAutomatic_CheckedChanged(optAutomatic, New System.EventArgs())
				ElseIf Me.optManual.Checked = True Then 
					Call optManual_CheckedChanged(optManual, New System.EventArgs())
				ElseIf Me.optAfter.Checked = True Then 
					Call optAfter_CheckedChanged(optAfter, New System.EventArgs())
				Else
					Call optValidation_CheckedChanged(optValidation, New System.EventArgs())
				End If
			Else
				If optAutomatic.Checked = True Then
					aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 1)
				ElseIf Me.optManual.Checked = True Then 
					aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 2)
				ElseIf Me.optAfter.Checked = True Then 
					aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 3)
				Else
					aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 4)
				End If
			End If
			bNewRule = False
		End If

		
		
	End Sub
	
	Private Sub cmdIntTest_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdIntTest.Click
		Dim bx As Boolean
		
        bx = oQuery.RunTheQuery()
		
	End Sub
	
	Private Sub cmdNewRule_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdNewRule.Click

		'bx = CheckIfRuleIsChanged(lstMatchingRules.ListIndex)
		
		'When You set the listindex, the function CheckIfRuleIsChanged will be run
		lstMatchingRules.SelectedIndex = -1
		
        txtRuleName.Text = ""
        txtRuleOrder.Text = ""
        txtRuleSpecial.Text = ""
        txtRuleType.Text = ""
        sOldTxtRuleType = ""
        txtRuleERPIndex.Text = ""
        lblWriSQLName.Text = ""
        txtWriSQL.Text = ""
        lblIntSQLname.Text = ""
        txtIntSQL.Text = ""
        If cmbPatterns.Items.Count > 0 Then
            cmbPatterns.SelectedIndex = 0
        End If

        lblSummarize.Visible = False
        txtSummarize.Visible = False
        chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
        chkProposeMatch.CheckState = System.Windows.Forms.CheckState.Unchecked
        chkUseAdjustments.CheckState = System.Windows.Forms.CheckState.Unchecked

        EnableAllTextControls()

        txtRuleName.Focus()

        bNewRule = True
    End Sub


    Private Sub cmdWriDescription_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWriDescription.Click
        ' Make description of matching-rule
        ' Find previous text for this matching rule:
        Dim sMySQL As String
        Dim lRecordsAffected As Integer
        Dim frmSQLDescription As New frmSQLDescription
        Dim iRecordCounter As Integer = 0
        Dim sTmp As String = ""

        ' 28.07.2010 to avoid errors when adding Description for a new rule, ask to save first !
        If EmptyString(Me.txtRuleERPIndex.Text) Then
            MsgBox("Vennligst lagre regelen f�r du lager beskrivelse", MsgBoxStyle.Information, "Beskrivelse")
        Else

            sMySQL = "SELECT SQLDescription FROM ERPQuery "
            sMySQL = sMySQL & "WHERE Company_ID = " & oQueryProfile.Company_ID.ToString
            sMySQL = sMySQL & " AND DBProfile_ID = " & iDBProfileID.ToString
            sMySQL = sMySQL & " AND ERPQuery_ID = " & Me.txtRuleERPIndex.Text

            oMyBBDal.SQL = sMySQL
            If oMyBBDal.Reader_Execute() Then
                Do While oMyBBDal.Reader_ReadRecord
                    sTmp = RetrieveIllegalCharactersInString(oMyBBDal.Reader_GetString("SQLDescription"))
                    iRecordCounter = iRecordCounter + 1
                Loop
            Else
                Throw New Exception(LRSCommon(45002) & oMyBBDal.ErrorMessage)
            End If

            If iRecordCounter = 1 Then
                frmSQLDescription.txtDescription.Text = sTmp
            End If

            VB6.ShowForm(frmSQLDescription, 1, Me)

            If frmSQLDescription.AnythingChanged Then
                sMySQL = "UPDATE ERPQuery "
                sMySQL = sMySQL & "SET SQLDescription = '" & ReplaceIllegalCharactersInString((frmSQLDescription.txtDescription.Text))
                sMySQL = sMySQL & "' WHERE Company_ID = " & oQueryProfile.Company_ID.ToString
                sMySQL = sMySQL & " AND DBProfile_ID = " & iDBProfileID.ToString
                sMySQL = sMySQL & " AND ERPQuery_ID = " & Me.txtRuleERPIndex.Text

                oMyBBDal.SQL = sMySQL
                If oMyBBDal.ExecuteNonQuery Then
                    If oMyBBDal.RecordsAffected = 1 Then
                        'OK
                    Else
                        Throw New Exception("No records affected" & vbCrLf & oMyBBDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyBBDal.ErrorMessage)
                End If

            End If
        End If

    End Sub

    'Private Sub cmdWriConnection_Click()
    'Dim bx As Boolean
    '
    ''bx = oQuery.ConnectERP(False)
    '
    'End Sub

    Private Sub cmdWriSave_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWriSave.Click
        Dim bSaveOK As Boolean

        bSaveOK = False

        If oQuery.SaveMatchingRule(bNewRule) Then
            bSaveOK = True
        End If

        'Have to set the listindex in the lstmatchingrules, when we have a new rule.
        If bSaveOK Then
            If bNewRule Then
                'Changed 05.08.2005 by Kjell, to avoid the question: Do You want to save .....
                '  and then save the rule a second time if the user answer Yes.
                bNewRule = False
                If Me.optAutomatic.Checked = True Then
                    Call optAutomatic_CheckedChanged(optAutomatic, New System.EventArgs())
                ElseIf Me.optManual.Checked = True Then
                    Call optManual_CheckedChanged(optManual, New System.EventArgs())
                ElseIf Me.optAfter.Checked = True Then
                    Call optAfter_CheckedChanged(optAfter, New System.EventArgs())
                ElseIf Me.optValidation.Checked = True Then
                    Call optValidation_CheckedChanged(optValidation, New System.EventArgs())
                Else
                    Call optOther_CheckedChanged(optOther, New System.EventArgs())
                End If
            Else
                If optAutomatic.Checked = True Then
                    aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 1)
                ElseIf Me.optManual.Checked = True Then
                    aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 2)
                ElseIf Me.optAfter.Checked = True Then
                    aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 3)
                ElseIf Me.optValidation.Checked = True Then
                    aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 4)
                Else
                    aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 5)
                End If
            End If
            bNewRule = False
        End If
    End Sub

    Private Sub cmdWriTest_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWriTest.Click
        Dim bx As Boolean

        bx = oQuery.RunTheQuery()

    End Sub

    Private Sub frmSQLStatements_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        Dim bx As Boolean

        If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
            bx = CheckIfRuleIsChanged(lstMatchingRules.SelectedIndex)
        End If

        eventArgs.Cancel = Cancel
    End Sub

    Private Sub frmSQLStatements_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        sPWD = ""
        bConnected = False

        oQuery = Nothing

        oQueryProfile = Nothing

        If Not oMyBBDal Is Nothing Then
            oMyBBDal.Close()
            oMyBBDal = Nothing
        End If
        ' added 16.07.2010, close connection
        'ERP_CloseConnection()

    End Sub
    Private Sub imgArrowDown_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles imgArrowDown.Click
        Dim iNewListIndex As Short
        Dim iOldListIndex As Short

        If lstMatchingRules.SelectedIndex < lstMatchingRules.Items.Count - 1 Then
            iNewListIndex = lstMatchingRules.SelectedIndex + 1
            iOldListIndex = iNewListIndex + 1
            oQuery.UpdateQorder(False, iNewListIndex + 1) 'The order used in the DB is not 0-based
        Else
            ' move below last item, goes to top
            iOldListIndex = lstMatchingRules.Items.Count - 1
        End If

        If optAutomatic.Checked = True Then
            'To avoid BB to think it is a new rule
            iOldOptionClicked = 2
            optAutomatic_CheckedChanged(optAutomatic, New System.EventArgs())
        ElseIf optManual.Checked = True Then
            'To avoid BB to think it is a new rule
            iOldOptionClicked = 1
            ' added 03.04.2007 some lines to keep track of icons for each rule
            ' Swap filenames for iOldIndex and iNewListIndex
            IconRename(iNewListIndex, iOldListIndex)

            optManual_CheckedChanged(optManual, New System.EventArgs())
        ElseIf optAfter.Checked = True Then
            'To avoid BB to think it is a new rule
            iOldOptionClicked = 1
            optAfter_CheckedChanged(optAfter, New System.EventArgs())
        ElseIf Me.optValidation.Checked = True Then
            'To avoid BB to think it is a new rule
            iOldOptionClicked = 1
            optValidation_CheckedChanged(optValidation, New System.EventArgs())
        Else
            'To avoid BB to think it is a new rule
            iOldOptionClicked = 1
            optOther_CheckedChanged(optOther, New System.EventArgs())
        End If

        lstMatchingRules.SelectedIndex = iNewListIndex

    End Sub
    Sub IconRename(ByRef iOldIndex As Short, ByRef iNewIndex As Short)
        ' when shifting position for manual rules, must then rename their respective iconnames
        Dim sGraphicsPath As String
        Dim oFs As Scripting.FileSystemObject

        If RunTime() Then
            sGraphicsPath = My.Application.Info.DirectoryPath
        Else
            sGraphicsPath = "c:\projects\babelbank\graphics"
        End If
        Try
            If Len(Dir(sGraphicsPath & "\find_" & Trim(Str(iNewIndex)) & ".bmp")) > 0 Then
                If Len(Dir(sGraphicsPath & "\find_" & Trim(Str(iNewIndex)) & ".bbt")) > 0 Then
                    ' delete old file, so we do not get errors
                    Kill(sGraphicsPath & "\find_" & Trim(Str(iNewIndex)) & ".bbt")
                End If
                ' rename new file to a tempfilename
                'Rename(sGraphicsPath & "\find_" & Trim(Str(iNewIndex)) & ".bmp", sGraphicsPath & "\find_" & Trim(Str(iNewIndex)) & ".bbt")
                oFs = New Scripting.FileSystemObject
                oFs.MoveFile(sGraphicsPath & "\find_" & Trim(Str(iNewIndex)) & ".bmp", sGraphicsPath & "\find_" & Trim(Str(iNewIndex)) & ".bbt")
                oFs = Nothing
            End If
            If Len(Dir(sGraphicsPath & "\find_" & Trim(Str(iOldIndex)) & ".bmp")) > 0 Then
                ' rename old file to new file
                'Rename(sGraphicsPath & "\find_" & Trim(Str(iOldIndex)) & ".bmp", sGraphicsPath & "\find_" & Trim(Str(iNewIndex)) & ".bmp")
                oFs = New Scripting.FileSystemObject
                oFs.MoveFile(sGraphicsPath & "\find_" & Trim(Str(iOldIndex)) & ".bmp", sGraphicsPath & "\find_" & Trim(Str(iNewIndex)) & ".bmp")
                oFs = Nothing
            End If
            If Len(Dir(sGraphicsPath & "\find_" & Trim(Str(iNewIndex)) & ".bbt")) > 0 Then
                ' rename tmp file to a oldfilename
                'Rename(sGraphicsPath & "\find_" & Trim(Str(iNewIndex)) & ".bbt", sGraphicsPath & "\find_" & Trim(Str(iOldIndex)) & ".bmp")
                oFs = New Scripting.FileSystemObject
                oFs.MoveFile(sGraphicsPath & "\find_" & Trim(Str(iNewIndex)) & ".bbt", sGraphicsPath & "\find_" & Trim(Str(iOldIndex)) & ".bmp")
                oFs = Nothing
            End If

        Catch ex As Exception
            ' continue, no renaming
        End Try

    End Sub
    Private Sub imgArrowUp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles imgArrowUp.Click
        Dim iNewListIndex As Short

        If lstMatchingRules.SelectedIndex > 0 Then
            iNewListIndex = lstMatchingRules.SelectedIndex - 1

            oQuery.UpdateQorder(True, iNewListIndex + 1) 'The order used in the DB is not 0-based
        End If

        If optAutomatic.Checked = True Then
            'To avoid BB to think it is a new rule
            iOldOptionClicked = 2
            optAutomatic_CheckedChanged(optAutomatic, New System.EventArgs())
        ElseIf optManual.Checked = True Then
            'To avoid BB to think it is a new rule
            iOldOptionClicked = 1
            ' Swap filenames for iOldIndex and iNewListIndex
            IconRename(iNewListIndex + 2, iNewListIndex + 1)

            optManual_CheckedChanged(optManual, New System.EventArgs())
        ElseIf optAfter.Checked = True Then
            'To avoid BB to think it is a new rule
            iOldOptionClicked = 1
            optAfter_CheckedChanged(optAfter, New System.EventArgs())
        ElseIf optValidation.Checked = True Then
            'To avoid BB to think it is a new rule
            iOldOptionClicked = 1
            optValidation_CheckedChanged(optValidation, New System.EventArgs())
        Else
            'To avoid BB to think it is a new rule
            iOldOptionClicked = 1
            optOther_CheckedChanged(optOther, New System.EventArgs())
        End If

        lstMatchingRules.SelectedIndex = iNewListIndex

    End Sub

    'UPGRADE_WARNING: Event lstMatchingRules.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub lstMatchingRules_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstMatchingRules.SelectedIndexChanged
        Dim bx As Boolean
        Dim lCounter As Integer
        Dim bLocalNewRule As Boolean
        Dim iLocalListIndex As Short

        bLocalNewRule = False
        If lstMatchingRules.SelectedIndex <> iOldListIndex Then
            If blstMatchingRules_ClickCalledFromWithin Then
                'This click-event is called from within this function
                ' Happens when bNewLocalRule is true (The user has created a new rule, but clicked
                ' on another rule before saving the new one
                'Do not do the checks in the else statement, just fill the controls for
                ' the selected rule.
                blstMatchingRules_ClickCalledFromWithin = False
            Else
                iLocalListIndex = -1
                'check if we have changed type of matching-rule
                If bNewRule Then
                    bLocalNewRule = True
                End If
                If optAutomatic.Checked = True Then
                    If iOldOptionClicked = 1 Then
                        bx = CheckIfRuleIsChanged(iOldListIndex)
                    End If
                    If bLocalNewRule Then
                        blstMatchingRules_ClickCalledFromWithin = True
                        iLocalListIndex = lstMatchingRules.SelectedIndex
                        lstMatchingRules.Items.Clear()
                        For lCounter = 1 To UBound(aQueryArray, 3)
                            lstMatchingRules.Items.Add(aQueryArray(iDBProfileID - 1, 4, lCounter))
                        Next lCounter
                        lstMatchingRules.SelectedIndex = iLocalListIndex
                    End If
                ElseIf optManual.Checked = True Then
                    If iOldOptionClicked = 2 Then
                        bx = CheckIfRuleIsChanged(iOldListIndex)
                    End If
                    If bLocalNewRule Then
                        blstMatchingRules_ClickCalledFromWithin = True
                        iLocalListIndex = lstMatchingRules.SelectedIndex
                        lstMatchingRules.Items.Clear()
                        For lCounter = 1 To UBound(aQueryArray, 3)
                            lstMatchingRules.Items.Add(aQueryArray(iDBProfileID - 1, 4, lCounter))
                        Next lCounter
                        lstMatchingRules.SelectedIndex = iLocalListIndex
                    End If
                    ' added 03.04.2007
                    ' Fill in icon for selected rule;
                    ShowRulesIcon(lstMatchingRules.SelectedIndex + 1)

                ElseIf optAfter.Checked = True Then
                    If bLocalNewRule Then
                        blstMatchingRules_ClickCalledFromWithin = True
                        iLocalListIndex = lstMatchingRules.SelectedIndex
                        lstMatchingRules.Items.Clear()
                        For lCounter = 1 To UBound(aQueryArray, 3)
                            lstMatchingRules.Items.Add(aQueryArray(iDBProfileID - 1, 4, lCounter))
                        Next lCounter
                        lstMatchingRules.SelectedIndex = iLocalListIndex
                    End If
                    If iOldOptionClicked = 3 Then
                        bx = CheckIfRuleIsChanged(iOldListIndex)
                    End If
                Else 'optValidation
                    If iOldOptionClicked = 4 Then
                        bx = CheckIfRuleIsChanged(iOldListIndex)
                    End If
                    If bLocalNewRule Then
                        blstMatchingRules_ClickCalledFromWithin = True
                        iLocalListIndex = lstMatchingRules.SelectedIndex
                        lstMatchingRules.Items.Clear()
                        For lCounter = 1 To UBound(aQueryArray, 3)
                            lstMatchingRules.Items.Add(aQueryArray(iDBProfileID - 1, 4, lCounter))
                        Next lCounter
                        lstMatchingRules.SelectedIndex = iLocalListIndex
                    End If
                End If
            End If
        End If

        If lstMatchingRules.SelectedIndex > -1 Then
            txtRuleName.Text = aQueryArray(iDBProfileID - 1, 4, lstMatchingRules.SelectedIndex + 1)
            txtRuleOrder.Text = CStr(lstMatchingRules.SelectedIndex + 1)
            txtRuleSpecial.Text = aQueryArray(iDBProfileID - 1, 2, lstMatchingRules.SelectedIndex + 1)
            txtRuleType.Text = aQueryArray(iDBProfileID - 1, 1, lstMatchingRules.SelectedIndex + 1)
            sOldTxtRuleType = txtRuleType.Text
            txtRuleERPIndex.Text = aQueryArray(iDBProfileID - 1, 5, lstMatchingRules.SelectedIndex + 1)
            lblWriSQLName.Text = aQueryArray(iDBProfileID - 1, 4, lstMatchingRules.SelectedIndex + 1)
            txtWriSQL.Text = aQueryArray(iDBProfileID - 1, 0, lstMatchingRules.SelectedIndex + 1)
            lblIntSQLname.Text = aQueryArray(iDBProfileID - 1, 4, lstMatchingRules.SelectedIndex + 1)
            txtIntSQL.Text = aQueryArray(iDBProfileID - 1, 0, lstMatchingRules.SelectedIndex + 1)

            If optAutomatic.Checked = True Then
                'Show pattern, adjustments and proposematch
                chkUseAdjustments.Visible = True
                chkProposeMatch.Visible = True
                chkIncludeOCR.Visible = True
                cmbPatterns.Visible = True
                lblPattern.Visible = True
                If (txtRuleType.Text = "1" Or txtRuleType.Text = "3") And Not Array_IsEmpty(aPatternArray) Then
                    lblPattern.Visible = True
                    cmbPatterns.Visible = True
                    lblSummarize.Visible = False
                    txtSummarize.Visible = False
                    For lCounter = 0 To UBound(aPatternArray, 2)
                        'cmbPatterns.AddItem (aPatternArray(1, lCounter))
                        ' Mark previously selected
                        If aPatternArray(0, lCounter) = aQueryArray(iDBProfileID - 1, 3, lstMatchingRules.SelectedIndex + 1) Then
                            Me.cmbPatterns.SelectedIndex = lCounter + 1
                            iCMBPatternIndex = lCounter + 1
                            Exit For
                        End If
                    Next lCounter
                    If Me.cmbPatterns.SelectedIndex < 0 Then
                        Me.cmbPatterns.SelectedIndex = 0
                    End If
                ElseIf txtRuleType.Text = "2" Then
                    lblPattern.Visible = False
                    cmbPatterns.Visible = False
                    lblSummarize.Visible = True
                    txtSummarize.Visible = True
                    txtSummarize.Text = aQueryArray(iDBProfileID - 1, 3, lstMatchingRules.SelectedIndex + 1)
                Else
                    Me.cmbPatterns.SelectedIndex = 0
                    iCMBPatternIndex = 0
                End If
            Else
                'Hide pattern, adjustments and proposematch and summarize
                chkUseAdjustments.Visible = False
                chkProposeMatch.Visible = False
                If optValidation.Checked = True Then
                    cmbPatterns.Visible = True
                    lblPattern.Visible = True
                    If lstMatchingRules.SelectedIndex <> iOldListIndex Then
                        PopulatePatternsWithPossibleFieldsToValidate()
                        cmbPatterns.SelectedIndex = aQueryArray(iDBProfileID - 1, 3, lstMatchingRules.SelectedIndex + 1)
                    End If
                Else
                    If Me.cmbPatterns.Items.Count > 0 Then
                        Me.cmbPatterns.SelectedIndex = 0
                    End If
                    cmbPatterns.Visible = False
                    lblPattern.Visible = False
                End If
                If optAfter.Checked = True Then
                    'Show Type and IncludeOCR
                    txtRuleType.Visible = True
                    lblRuleType.Visible = True
                    chkIncludeOCR.Visible = True
                Else
                    'Hide Type asn Include OCR
                    txtRuleType.Visible = False
                    lblRuleType.Visible = False
                    chkIncludeOCR.Visible = False
                End If
            End If

            If aQueryArray(iDBProfileID - 1, 10, lstMatchingRules.SelectedIndex + 1) = "True" Then
                chkUseAdjustments.CheckState = System.Windows.Forms.CheckState.Checked
            Else
                chkUseAdjustments.CheckState = System.Windows.Forms.CheckState.Unchecked
            End If
            If aQueryArray(iDBProfileID - 1, 11, lstMatchingRules.SelectedIndex + 1) = "True" Then
                chkProposeMatch.CheckState = System.Windows.Forms.CheckState.Checked
            Else
                chkProposeMatch.CheckState = System.Windows.Forms.CheckState.Unchecked
            End If
            If aQueryArray(iDBProfileID - 1, 12, lstMatchingRules.SelectedIndex + 1) = "True" Then
                chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked
            Else
                chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
            End If
            bNewRule = False
            bx = oQuery.CheckSQL()
        End If

        iOldListIndex = lstMatchingRules.SelectedIndex

    End Sub

    'UPGRADE_WARNING: Event optAfter.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optAfter_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optAfter.CheckedChanged
        If eventSender.Checked Then
            Dim lCounter As Integer
            Dim bx As Boolean
            Dim iThirdDimension As Short

            If iOldOptionClicked = 3 And Not bRuleDeleted Then
                'The sub is called from the save_click event when we have a new rule
                '  Set the list index to the last item in the lstMatchingRules
                iThirdDimension = lstMatchingRules.Items.Count + 1
            Else
                'When a new option button is clicked or when the delete_click event is triggered
                bx = CheckIfRuleIsChanged(lstMatchingRules.SelectedIndex)
                iThirdDimension = 1
            End If

            iCMBPatternIndex = 0

            aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 3)

            lstMatchingRules.Items.Clear()
            ' new 03.04.2007, hide Icon-selection used for Manual
            Me.imgIcon.Visible = False
            Me.cmdIcon.Visible = False

            If UBound(aQueryArray, 3) = 0 Then
                txtRuleName.Text = ""
                txtRuleOrder.Text = ""
                txtRuleSpecial.Text = ""
                txtRuleType.Text = ""
                sOldTxtRuleType = ""
                lblWriSQLName.Text = ""
                txtWriSQL.Text = ""
                lblIntSQLname.Text = ""
                txtIntSQL.Text = ""
                'Hide pattern, adjustments, Summarize and proposematch
                chkUseAdjustments.Visible = False
                chkProposeMatch.Visible = False
                cmbPatterns.Visible = False
                lblPattern.Visible = False
                lblSummarize.Visible = False
                txtSummarize.Visible = False
                'Show Type and IncludeOCR
                txtRuleType.Visible = True
                lblRuleType.Visible = True
                chkIncludeOCR.Visible = True
                DisableAllTextControls()
            Else
                EnableAllTextControls()
                For lCounter = 1 To UBound(aQueryArray, 3)
                    lstMatchingRules.Items.Add(aQueryArray(iDBProfileID - 1, 4, lCounter))
                Next lCounter
                txtRuleName.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtRuleOrder.Text = CStr(iThirdDimension)
                txtRuleSpecial.Text = aQueryArray(iDBProfileID - 1, 2, iThirdDimension)
                txtRuleType.Text = aQueryArray(iDBProfileID - 1, 1, iThirdDimension)
                sOldTxtRuleType = txtRuleType.Text
                txtRuleERPIndex.Text = aQueryArray(iDBProfileID - 1, 5, iThirdDimension)
                lblWriSQLName.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtWriSQL.Text = aQueryArray(iDBProfileID - 1, 0, iThirdDimension)
                lblIntSQLname.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtIntSQL.Text = aQueryArray(iDBProfileID - 1, 0, iThirdDimension)
                lstMatchingRules.SelectedIndex = iThirdDimension - 1
                'Hide pattern, adjustments, Summarize and proposematch
                chkUseAdjustments.Visible = False
                chkProposeMatch.Visible = False
                cmbPatterns.Visible = False
                lblPattern.Visible = False
                lblSummarize.Visible = False
                txtSummarize.Visible = False
                'Show Type and IncludeOCR
                txtRuleType.Visible = True
                lblRuleType.Visible = True
                chkIncludeOCR.Visible = True
                If aQueryArray(iDBProfileID - 1, 12, iThirdDimension) = "True" Then
                    chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked
                Else
                    chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
                End If
                bx = oQuery.CheckSQL()
            End If

            iOldOptionClicked = 3
            bNewRule = False

        End If
    End Sub

    'UPGRADE_WARNING: Event optAutomatic.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optAutomatic_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optAutomatic.CheckedChanged
        If eventSender.Checked Then
            Dim lCounter As Integer
            Dim iThirdDimension As Short
            Dim bx As Boolean

            iCMBPatternIndex = 0

            'If the form is opened for the first time the array is empty, and
            '   we don't want to check if the rule is changed.
            If Not Array_IsEmpty(aQueryArray) Then
                If iOldOptionClicked = 1 And Not bRuleDeleted Then
                    'The sub is called from the save_click event when we have a new rule
                    '  Set the list index to the last item in the lstMatchingRules
                    iThirdDimension = lstMatchingRules.Items.Count + 1
                Else
                    'When a new option button is clicked or when the delete_click event is triggered
                    bx = CheckIfRuleIsChanged(lstMatchingRules.SelectedIndex)
                    iThirdDimension = 1
                End If
            Else
                iThirdDimension = 1
            End If

            aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 1)
            lstMatchingRules.Items.Clear()

            ' new 03.04.2007, hide Icon-selection used for Manual
            Me.imgIcon.Visible = False
            Me.cmdIcon.Visible = False

            If UBound(aQueryArray, 3) = 0 Then
                txtRuleName.Text = ""
                txtRuleOrder.Text = ""
                txtRuleSpecial.Text = ""
                txtRuleType.Text = ""
                sOldTxtRuleType = ""
                lblWriSQLName.Text = ""
                txtWriSQL.Text = ""
                lblIntSQLname.Text = ""
                txtIntSQL.Text = ""
                'Show Type, includeOCR, ProposeMatch and Use Adjustments
                txtRuleType.Visible = True
                lblRuleType.Visible = True
                chkIncludeOCR.Visible = True
                chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
                chkUseAdjustments.Visible = True
                chkUseAdjustments.CheckState = System.Windows.Forms.CheckState.Unchecked
                chkProposeMatch.Visible = True
                chkProposeMatch.CheckState = System.Windows.Forms.CheckState.Unchecked
                DisableAllTextControls()
            Else
                EnableAllTextControls()
                For lCounter = 1 To UBound(aQueryArray, 3)
                    lstMatchingRules.Items.Add(aQueryArray(iDBProfileID - 1, 4, lCounter))
                Next lCounter
                txtRuleName.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtRuleOrder.Text = CStr(iThirdDimension)
                txtRuleSpecial.Text = aQueryArray(iDBProfileID - 1, 2, iThirdDimension)
                txtRuleType.Text = aQueryArray(iDBProfileID - 1, 1, iThirdDimension)
                sOldTxtRuleType = txtRuleType.Text
                txtRuleERPIndex.Text = aQueryArray(iDBProfileID - 1, 5, iThirdDimension)
                lblWriSQLName.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtWriSQL.Text = aQueryArray(iDBProfileID - 1, 0, iThirdDimension)
                lblIntSQLname.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtIntSQL.Text = aQueryArray(iDBProfileID - 1, 0, iThirdDimension)
                'Show pattern, adjustments and proposematch
                chkUseAdjustments.Visible = True
                chkProposeMatch.Visible = True
                If Val(aQueryArray(iDBProfileID - 1, 1, iThirdDimension)) = 2 Then
                    'Hide the use of patterns, and show the possibility of summarize among open items
                    cmbPatterns.Visible = False
                    lblPattern.Visible = False
                    lblSummarize.Visible = True
                    txtSummarize.Visible = True
                    txtSummarize.Text = aQueryArray(iDBProfileID - 1, 3, iThirdDimension)
                Else
                    'Hide the use of possibility of summarize among open items and show the patterns
                    cmbPatterns.Visible = True
                    lblPattern.Visible = True
                    'Populate Patterns
                    'The field is also used in optValidate, thus
                    lblPattern.Text = LRS(60319)
                    'cmbPatterns.Items.Clear()
                    'cmbPatterns.Items.Add("")
                    If Not Array_IsEmpty(aPatternArray) Then
                        For lCounter = 0 To UBound(aPatternArray, 2)
                            'cmbPatterns.Items.Add((aPatternArray(1, lCounter)))
                            ' Mark previously selected
                            If aPatternArray(0, lCounter) = aQueryArray(iDBProfileID - 1, 3, iThirdDimension) Then
                                Me.cmbPatterns.SelectedIndex = lCounter + 1

                                iCMBPatternIndex = lCounter + 1
                            End If
                        Next lCounter
                    Else
                        Me.cmbPatterns.SelectedIndex = 0
                    End If
                End If
                'Show Type and includeOCR
                txtRuleType.Visible = True
                lblRuleType.Visible = True
                chkIncludeOCR.Visible = True
                If aQueryArray(iDBProfileID - 1, 10, iThirdDimension) = "True" Then
                    chkUseAdjustments.CheckState = System.Windows.Forms.CheckState.Checked
                Else
                    chkUseAdjustments.CheckState = System.Windows.Forms.CheckState.Unchecked
                End If
                If aQueryArray(iDBProfileID - 1, 11, iThirdDimension) = "True" Then
                    chkProposeMatch.CheckState = System.Windows.Forms.CheckState.Checked
                Else
                    chkProposeMatch.CheckState = System.Windows.Forms.CheckState.Unchecked
                End If
                If aQueryArray(iDBProfileID - 1, 12, iThirdDimension) = "True" Then
                    chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked
                Else
                    chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
                End If
                lstMatchingRules.SelectedIndex = iThirdDimension - 1
                bx = oQuery.CheckSQL()
            End If

            iOldOptionClicked = 1
            bNewRule = False

        End If
    End Sub


    'UPGRADE_WARNING: Event optManual.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optManual_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optManual.CheckedChanged
        If eventSender.Checked Then
            Dim lCounter As Integer
            Dim iThirdDimension As Short
            Dim bx As Boolean

            If iOldOptionClicked = 2 And Not bRuleDeleted Then
                'The sub is called from the save_click event when we have a new rule
                '  Set the list index to the last item in the lstMatchingRules
                iThirdDimension = lstMatchingRules.Items.Count + 1
                ' Fill in icon for selected rule;
                ShowRulesIcon(lstMatchingRules.Items.Count + 1)

            Else
                'When a new option button is clicked or when the delete_click event is triggered
                bx = CheckIfRuleIsChanged(lstMatchingRules.SelectedIndex)
                iThirdDimension = 1
                ' Fill in icon for selected rule;
                'ShowRulesIcon(1)
                ShowRulesIcon(lstMatchingRules.SelectedIndex)  ' changed 20.07.2011
            End If

            iCMBPatternIndex = 0

            ' TODO 20.07.2011 JPS - CheckChanged i frmSQLStatements.vb
            ' Ved Endre rekkef�lge,  imgArrowDown_Click, ser det ut som om basen ikke rekker
            ' � skrive ferdig, f�r vi her henter opp igjen regelene.
            ' F�rer til at vi i democaset "mister" en regel, alts� f�r igjen
            ' kun 5 av 6 regler i aQueryArray.
            ' Kan det ha noe med at UpdateQorder ikke er "dallet" ?
            aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 2)

            lstMatchingRules.Items.Clear()
            ' new 03.04.2007, hide Icon-selection used for Manual
            Me.imgIcon.Visible = True
            Me.cmdIcon.Visible = True

            If UBound(aQueryArray, 3) = 0 Then
                txtRuleName.Text = ""
                txtRuleOrder.Text = ""
                txtRuleSpecial.Text = ""
                txtRuleType.Text = ""
                sOldTxtRuleType = ""
                lblWriSQLName.Text = ""
                txtWriSQL.Text = ""
                lblIntSQLname.Text = ""
                txtIntSQL.Text = ""
                DisableAllTextControls()
            Else
                EnableAllTextControls()
                For lCounter = 1 To UBound(aQueryArray, 3)
                    lstMatchingRules.Items.Add(aQueryArray(iDBProfileID - 1, 4, lCounter))
                Next lCounter
                lstMatchingRules.Refresh()
                txtRuleName.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtRuleOrder.Text = CStr(lCounter)
                txtRuleSpecial.Text = aQueryArray(iDBProfileID - 1, 2, iThirdDimension)
                txtRuleType.Text = aQueryArray(iDBProfileID - 1, 1, iThirdDimension)
                sOldTxtRuleType = txtRuleType.Text
                txtRuleERPIndex.Text = aQueryArray(iDBProfileID - 1, 5, iThirdDimension)
                lblWriSQLName.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtWriSQL.Text = aQueryArray(iDBProfileID - 1, 0, iThirdDimension)
                bx = oQuery.CheckSQL()
                lblIntSQLname.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtIntSQL.Text = aQueryArray(iDBProfileID - 1, 0, iThirdDimension)
                'Hide pattern, adjustments, summarize and proposematch
                chkUseAdjustments.Visible = False
                chkProposeMatch.Visible = False
                cmbPatterns.Visible = False
                lblPattern.Visible = False
                lblSummarize.Visible = False
                txtSummarize.Visible = False
                'Hide Type and IncludeOCR
                txtRuleType.Visible = False
                lblRuleType.Visible = False
                chkIncludeOCR.Visible = False

                lstMatchingRules.SelectedIndex = iThirdDimension - 1
            End If

            iOldOptionClicked = 2
            bNewRule = False

        End If
        Me.lstMatchingRules.Focus()
    End Sub
    Private Sub ShowRulesIcon(ByRef iIndex As Short)
        Dim sGraphicsPath As String

        If RunTime() Then
            sGraphicsPath = My.Application.Info.DirectoryPath
        Else
            sGraphicsPath = "c:\projects\babelbank\graphics"
        End If
        ' The icon bitmaps must have names find_1.bmp, find_2,bmp, etc
        If Len(Dir(sGraphicsPath & "\find_" & Trim(Str(iIndex)) & ".bmp")) > 0 Then
            ' 19.07.2011 - for some reason, VB locks the file when using it as image.
            ' can be solved by using Memorystream as below
            Me.imgIcon.Image = Image.FromStream(New System.IO.MemoryStream(System.IO.File.ReadAllBytes(sGraphicsPath & "\find_" & Trim(Str(iIndex)) & ".bmp")))
        Else
            Me.imgIcon.Visible = False
        End If

    End Sub

    'UPGRADE_WARNING: Event optOther.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optOther_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optOther.CheckedChanged
        If eventSender.Checked Then
            Dim lCounter As Integer
            Dim i As Short
            Dim iThirdDimension As Short
            Dim bx As Boolean

            On Error GoTo ERR_optOther_Click

            If iOldOptionClicked = 5 And Not bRuleDeleted Then
                'The sub is called from the save_click event when we have a new rule
                '  Set the list index to the last item in the lstMatchingRules
                iThirdDimension = lstMatchingRules.Items.Count + 1
            Else
                'When a new option button is clicked or when the delete_click event is triggered
                bx = CheckIfRuleIsChanged(lstMatchingRules.SelectedIndex)
                iThirdDimension = 1
            End If

            'iCMBPatternIndex = 0

            aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 5)

            lstMatchingRules.Items.Clear()
            ' new 03.04.2007, hide Icon-selection used for Manual
            Me.imgIcon.Visible = False
            Me.cmdIcon.Visible = False

            If UBound(aQueryArray, 3) = 0 Then
                txtRuleName.Text = ""
                txtRuleOrder.Text = ""
                txtRuleSpecial.Text = ""
                txtRuleType.Text = ""
                sOldTxtRuleType = ""
                lblWriSQLName.Text = ""
                txtWriSQL.Text = ""
                lblIntSQLname.Text = ""
                txtIntSQL.Text = ""
                'Hide pattern, adjustments, summarize and proposematch
                chkUseAdjustments.Visible = False
                chkProposeMatch.Visible = False
                cmbPatterns.Visible = False
                lblPattern.Visible = False
                lblSummarize.Visible = False
                txtSummarize.Visible = False
                'Hide Type and IncludeOCR
                txtRuleType.Visible = False
                lblRuleType.Visible = False
                chkIncludeOCR.Visible = False
                DisableAllTextControls()
            Else
                EnableAllTextControls()
                For lCounter = 1 To UBound(aQueryArray, 3)
                    lstMatchingRules.Items.Add(aQueryArray(iDBProfileID - 1, 4, lCounter))
                Next lCounter
                txtRuleName.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtRuleOrder.Text = CStr(lCounter)
                txtRuleSpecial.Text = aQueryArray(iDBProfileID - 1, 2, iThirdDimension)
                'txtRuleType.Text = aQueryArray(iDBProfileID - 1, 1, iThirdDimension)
                'sOldTxtRuleType = txtRuleType.Text
                txtRuleERPIndex.Text = aQueryArray(iDBProfileID - 1, 5, iThirdDimension)
                lblWriSQLName.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtWriSQL.Text = aQueryArray(iDBProfileID - 1, 0, iThirdDimension)
                bx = oQuery.CheckSQL()
                lblIntSQLname.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtIntSQL.Text = aQueryArray(iDBProfileID - 1, 0, iThirdDimension)
                'Hide pattern, adjustments, summarize and proposematch
                chkUseAdjustments.Visible = False
                chkProposeMatch.Visible = False
                cmbPatterns.Visible = False
                lblPattern.Visible = False

                lblSummarize.Visible = False
                txtSummarize.Visible = False
                'Hide Type and IncludeOCR
                txtRuleType.Visible = False
                lblRuleType.Visible = False
                chkIncludeOCR.Visible = False
                lstMatchingRules.SelectedIndex = iThirdDimension - 1
            End If

            iOldOptionClicked = 5
            bNewRule = False

            On Error GoTo 0
            Exit Sub

ERR_optOther_Click:

            Err.Raise(Err.Number, "optOther_Click", Err.Description)

        End If
    End Sub

    'UPGRADE_WARNING: Event optValidation.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optValidation_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optValidation.CheckedChanged
        If eventSender.Checked Then
            Dim lCounter As Integer
            Dim i As Short
            Dim iThirdDimension As Short
            Dim bx As Boolean

            On Error GoTo ERR_optValidation_Click

            If iOldOptionClicked = 4 And Not bRuleDeleted Then
                'The sub is called from the save_click event when we have a new rule
                '  Set the list index to the last item in the lstMatchingRules
                iThirdDimension = lstMatchingRules.Items.Count + 1
            Else
                'When a new option button is clicked or when the delete_click event is triggered
                bx = CheckIfRuleIsChanged(lstMatchingRules.SelectedIndex)
                iThirdDimension = 1
            End If

            'iCMBPatternIndex = 0

            aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 4)

            lstMatchingRules.Items.Clear()
            ' new 03.04.2007, hide Icon-selection used for Manual
            Me.imgIcon.Visible = False
            Me.cmdIcon.Visible = False

            If UBound(aQueryArray, 3) = 0 Then
                txtRuleName.Text = ""
                txtRuleOrder.Text = ""
                txtRuleSpecial.Text = ""
                txtRuleType.Text = ""
                sOldTxtRuleType = ""
                lblWriSQLName.Text = ""
                txtWriSQL.Text = ""
                lblIntSQLname.Text = ""
                txtIntSQL.Text = ""
                'Hide pattern, adjustments, summarize and proposematch
                chkUseAdjustments.Visible = False
                chkProposeMatch.Visible = False
                cmbPatterns.Visible = True
                lblPattern.Visible = True
                PopulatePatternsWithPossibleFieldsToValidate()
                lblSummarize.Visible = False
                txtSummarize.Visible = False
                'Hide Type and IncludeOCR
                txtRuleType.Visible = False
                lblRuleType.Visible = False
                chkIncludeOCR.Visible = False
                DisableAllTextControls()
            Else
                EnableAllTextControls()
                For lCounter = 1 To UBound(aQueryArray, 3)
                    lstMatchingRules.Items.Add(aQueryArray(iDBProfileID - 1, 4, lCounter))
                Next lCounter
                txtRuleName.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtRuleOrder.Text = CStr(lCounter)
                txtRuleSpecial.Text = aQueryArray(iDBProfileID - 1, 2, iThirdDimension)
                'txtRuleType.Text = aQueryArray(iDBProfileID - 1, 1, iThirdDimension)
                'sOldTxtRuleType = txtRuleType.Text
                txtRuleERPIndex.Text = aQueryArray(iDBProfileID - 1, 5, iThirdDimension)
                lblWriSQLName.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtWriSQL.Text = aQueryArray(iDBProfileID - 1, 0, iThirdDimension)
                bx = oQuery.CheckSQL()
                lblIntSQLname.Text = aQueryArray(iDBProfileID - 1, 4, iThirdDimension)
                txtIntSQL.Text = aQueryArray(iDBProfileID - 1, 0, iThirdDimension)
                'Hide pattern, adjustments, summarize and proposematch
                chkUseAdjustments.Visible = False
                chkProposeMatch.Visible = False
                cmbPatterns.Visible = True
                lblPattern.Visible = True
                'Populate Patterns
                'The field is also used in optValidate, thus
                lblPattern.Text = LRS(60465)

                PopulatePatternsWithPossibleFieldsToValidate()

                cmbPatterns.SelectedIndex = aQueryArray(iDBProfileID - 1, 3, iThirdDimension)

                lblSummarize.Visible = False
                txtSummarize.Visible = False
                'Hide Type and IncludeOCR
                txtRuleType.Visible = False
                lblRuleType.Visible = False
                chkIncludeOCR.Visible = False
                lstMatchingRules.SelectedIndex = iThirdDimension - 1
            End If

            iOldOptionClicked = 4
            bNewRule = False

            On Error GoTo 0
            Exit Sub

ERR_optValidation_Click:

            Err.Raise(Err.Number, "optValidation_Click", Err.Description)

        End If
    End Sub
    Private Sub Tabstrip1_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles TabStrip1.ClickEvent
        Dim bx As Boolean

        If TabStrip1.SelectedItem.Index = mintCurFrame + 1 Then
            Exit Sub ' No need to change frame.
        Else
            If TabStrip1.SelectedItem.Index = 2 Then
                bx = oQuery.UpdateSQLInterpreted()
            End If
            ' Otherwise, hide old frame, show new.
            frame1(TabStrip1.SelectedItem.Index - 1).Visible = True
            frame1(mintCurFrame).Visible = False
            ' Set mintCurFrame to new value.
            mintCurFrame = TabStrip1.SelectedItem.Index - 1
        End If

    End Sub

    Private Sub frmSQLStatements_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim sErrorString As String
        Dim lHeight, lTop, lLeft, lWidth As Integer

        lTop = 0
        lLeft = 0
        lHeight = VB6.PixelsToTwipsY(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height) - 500
        lWidth = VB6.PixelsToTwipsX(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width)

        ' Size to fit screen
        Me.Top = VB6.TwipsToPixelsY(lTop)
        Me.Left = VB6.TwipsToPixelsX(lLeft)
        Me.Height = VB6.TwipsToPixelsY(lHeight)
        Me.Width = VB6.TwipsToPixelsX(lWidth)

        sPWD = ""
        FormLRSCaptions(Me)
        sErrorString = ""
        FormvbStyle(Me, "dollar.jpg", 500)


        mintCurFrame = 0

        'cmbPatterns.Items.Clear()
        'cmbPatterns.Items.Add("")
        'cmbPatterns.SelectedIndex = 0

        bNewRule = False
        bRuleDeleted = False
        blstMatchingRules_ClickCalledFromWithin = False

        lblSummarize.Left = VB6.TwipsToPixelsX(1490)
        lblSummarize.Top = VB6.TwipsToPixelsY(2160)
        txtSummarize.Left = VB6.TwipsToPixelsX(3170)
        txtSummarize.Top = VB6.TwipsToPixelsY(2120)

        oMyBBDal = New vbBabel.DAL
        oMyBBDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyBBDal.ConnectToDB() Then
            Throw New System.Exception(oMyBBDal.ErrorMessage)
        End If

        'If Len(conERPConnection.ConnectionString) = 0 Then
        '    Me.Hide()
        'End If

        Me.optAfter.BringToFront() 'Bring to front
        Me.optOther.BringToFront()

    End Sub

    Friend Function SetQuery(ByRef oObj As Query) As Boolean

        On Error GoTo ErrorSetQuery

        oQuery = oObj
        SetQuery = True
        Exit Function

ErrorSetQuery:
        Err.Raise(1, , "Can't set the Query-object in the SQLStatement-form. Errormessage: " & vbCrLf & Err.Description)
        SetQuery = False

    End Function
    Friend Function SetPatternArray(ByRef aPatterns(,) As String) As Boolean
        Dim lCounter As Long = 0

        aPatternArray = aPatterns

        cmbPatterns.Items.Clear()
        cmbPatterns.Items.Add("")
        If Not Array_IsEmpty(aPatternArray) Then
            For lCounter = 0 To UBound(aPatternArray, 2)
                cmbPatterns.Items.Add((aPatternArray(1, lCounter)))
            Next lCounter
            'Me.cmbPatterns.SelectedIndex = 0
        End If

    End Function

    Friend Function SetColumnsToEnterData(ByRef s As String) As Boolean

        sColumnsToEnterData = s

    End Function

    Friend Function SetProfile(ByRef oObj As vbbabel.Profile) As Boolean

        On Error GoTo ErrorSetProfile

        oQueryProfile = oObj
        Exit Function

ErrorSetProfile:
        Err.Raise(1, , "Can't set the Profile-object in the SQLStatement-form. Errormessage: " & vbCrLf & Err.Description)
        SetProfile = False

    End Function

    Friend Function GetPatternIndex() As Short

        GetPatternIndex = iCMBPatternIndex

    End Function

    Friend Function IsConnected() As Boolean

        IsConnected = bConnected

    End Function




    Private Sub txtParaAccount_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParaAccount.Enter
        txtParaAccount.SelectionStart = 0
        txtParaAccount.SelectionLength = Len(txtParaAccount.Text)
    End Sub

    Private Sub txtParaAccount_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParaAccount.Leave
        oQuery.UpdateSQLInterpreted()
    End Sub

    Private Sub txtParaAmount_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParaAmount.Enter
        txtParaAmount.SelectionStart = 0
        txtParaAmount.SelectionLength = Len(txtParaAmount.Text)
    End Sub

    Private Sub txtParaAmount_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParaAmount.Leave
        oQuery.UpdateSQLInterpreted()
    End Sub

    Private Sub txtParaClient_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParaClient.Enter
        txtParaClient.SelectionStart = 0
        txtParaClient.SelectionLength = Len(txtParaClient.Text)
    End Sub

    Private Sub txtParaClient_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParaClient.Leave
        oQuery.UpdateSQLInterpreted()
    End Sub

    Private Sub txtParaCustomer_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParaCustomer.Enter
        txtParaCustomer.SelectionStart = 0
        txtParaCustomer.SelectionLength = Len(txtParaCustomer.Text)
    End Sub

    Private Sub txtParaCustomer_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParaCustomer.Leave
        oQuery.UpdateSQLInterpreted()
    End Sub

    Private Sub txtParaName_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParaName.Enter
        txtParaName.SelectionStart = 0
        txtParaName.SelectionLength = Len(txtParaName.Text)
    End Sub

    Private Sub txtParaName_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParaName.Leave
        oQuery.UpdateSQLInterpreted()
    End Sub

    Private Sub txtParaInvoice_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParaInvoice.Enter
        txtParaInvoice.SelectionStart = 0
        txtParaInvoice.SelectionLength = Len(txtParaInvoice.Text)
    End Sub

    Private Sub txtParaInvoice_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtParaInvoice.Leave
        oQuery.UpdateSQLInterpreted()
    End Sub

    'UPGRADE_WARNING: Event txtRuleType.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtRuleType_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtRuleType.TextChanged

        If optAutomatic.Checked = True Then

            If sOldTxtRuleType <> txtRuleType.Text Then
                sOldTxtRuleType = txtRuleType.Text
                Select Case Trim(txtRuleType.Text)

                    Case "2"
                        lblSummarize.Visible = True
                        txtSummarize.Visible = True
                        lblPattern.Visible = False
                        cmbPatterns.Visible = False

                    Case Else
                        lblSummarize.Visible = False
                        txtSummarize.Visible = False
                        lblPattern.Visible = True
                        cmbPatterns.Visible = True
                        If cmbPatterns.Items.Count > 0 Then
                            cmbPatterns.SelectedIndex = 0
                        End If
                        iCMBPatternIndex = 0

                End Select
            End If

        Else

            lblSummarize.Visible = False
            txtSummarize.Visible = False
            lblPattern.Visible = False
            If optValidation.Checked = True Then
                cmbPatterns.Visible = True
            Else
                cmbPatterns.Visible = False
            End If

        End If

    End Sub

    Private Sub txtWriSQL_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtWriSQL.Enter
        txtParaAmount.SelectionStart = 0
        txtParaAmount.SelectionLength = Len(txtWriSQL.Text)
    End Sub

    Private Sub txtWriSQL_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtWriSQL.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        Dim bx, bRunCheck As Boolean

        bRunCheck = True
        If Shift = 1 Then
            If KeyCode > 36 And KeyCode < 41 Then
                bRunCheck = False
            ElseIf KeyCode = System.Windows.Forms.Keys.F2 Then
                bx = oQuery.AddRestrictedNames(txtWriSQL, "BB_Amount")

            End If
        ElseIf Shift = 2 Then
            If KeyCode = 86 Then

            Else
                bRunCheck = False
            End If
        Else
            If KeyCode = 16 Then
                bRunCheck = False
            End If
        End If

        If bRunCheck Then
            bx = oQuery.CheckSQL()
        End If

    End Sub
    Private Sub txtIntSQL_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtIntSQL.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        Dim bx, bRunCheck As Boolean

        bRunCheck = True
        If Shift = 1 Then
            If KeyCode > 36 And KeyCode < 41 Then
                bRunCheck = False
            End If
        ElseIf Shift = 2 Then
            If KeyCode = 86 Then

            Else
                bRunCheck = False
            End If
        Else
            If KeyCode = 16 Then
                bRunCheck = False
            End If
        End If

        If bRunCheck Then
            bx = oQuery.CheckSQL()
        End If

    End Sub
    Friend Function SetQueryArray(ByRef aArray() As String) As Boolean

        aQueryArray = VB6.CopyArray(aArray)

    End Function

    Friend Function SetiDBProfileID(ByRef i As Short) As Boolean

        iDBProfileID = i

    End Function

    Friend Function GetMatchType() As Short

        'This variable keeps the MatchType (automatic, manual etc...)
        GetMatchType = iOldOptionClicked

    End Function

    Private Function CheckIfRuleIsChanged(ByRef iListIndex As Short) As Boolean
        Dim bChangesDone, bx As Boolean
        Dim iResponse As Short

        On Error GoTo ErrorCheckIfRuleIsChanged

        bChangesDone = False

        If bNewRule Then
            bChangesDone = True
        ElseIf iListIndex = -1 Then
            bChangesDone = False
        Else
            'You enter here when You select a new existing matching rule, or when the button <New> is clicked.
            If txtRuleName.Text <> aQueryArray(iDBProfileID - 1, 4, iListIndex + 1) Then
                bChangesDone = True
            End If
            If CDbl(txtRuleOrder.Text) <> iListIndex + 1 Then
                bChangesDone = True
            End If
            If txtRuleSpecial.Text <> aQueryArray(iDBProfileID - 1, 2, iListIndex + 1) Then
                bChangesDone = True
            End If
            If txtRuleType.Text <> aQueryArray(iDBProfileID - 1, 1, iListIndex + 1) Then
                bChangesDone = True
            End If
            'If txtRuleDescription <> aQueryArray(iDBProfileID - 1, 3, iListIndex + 1) Then
            '    bChangesDone = True
            'End If
            If cmbPatterns.Visible = True Then
                If cmbPatterns.SelectedIndex <> CDbl(aQueryArray(iDBProfileID - 1, 3, iListIndex + 1)) Then
                    'If iCMBPatternIndex <> cmbPatterns.ListIndex Then
                    bChangesDone = True
                End If
            End If
            If txtSummarize.Visible = True Then
                If txtSummarize.Text <> aQueryArray(iDBProfileID - 1, 3, iListIndex + 1) Then
                    bChangesDone = True
                End If
            End If
            If chkUseAdjustments.CheckState = System.Windows.Forms.CheckState.Unchecked Then
                If aQueryArray(iDBProfileID - 1, 10, iListIndex + 1) = "True" Then
                    bChangesDone = True
                End If
            Else
                If aQueryArray(iDBProfileID - 1, 10, iListIndex + 1) = "False" Then
                    bChangesDone = True
                End If
            End If
            If chkProposeMatch.CheckState = System.Windows.Forms.CheckState.Unchecked Then
                If aQueryArray(iDBProfileID - 1, 11, iListIndex + 1) = "True" Then
                    bChangesDone = True
                End If
            Else
                If aQueryArray(iDBProfileID - 1, 11, iListIndex + 1) = "False" Then
                    bChangesDone = True
                End If
            End If
            If chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Unchecked Then
                If aQueryArray(iDBProfileID - 1, 12, iListIndex + 1) = "True" Then
                    bChangesDone = True
                End If
            Else
                If aQueryArray(iDBProfileID - 1, 12, iListIndex + 1) = "False" Then
                    bChangesDone = True
                End If
            End If

            If txtWriSQL.Text <> aQueryArray(iDBProfileID - 1, 0, iListIndex + 1) Then
                bChangesDone = True
            End If
            '    If txtRuleName.Text <> aQueryArray(iDBProfileID - 1, 4, lstMatchingRules.ListIndex + 1) Then
            '        bChangesDone = True
            '    End If
            '    If txtRuleOrder.Text <> lstMatchingRules.ListIndex + 1 Then
            '        bChangesDone = True
            '    End If
            '    If txtRuleSpecial.Text <> aQueryArray(iDBProfileID - 1, 2, lstMatchingRules.ListIndex + 1) Then
            '        bChangesDone = True
            '    End If
            '    If txtRuleType.Text <> aQueryArray(iDBProfileID - 1, 1, lstMatchingRules.ListIndex + 1) Then
            '        bChangesDone = True
            '    End If
            '    If txtWriSQL.Text <> aQueryArray(iDBProfileID - 1, 0, lstMatchingRules.ListIndex + 1) Then
            '        bChangesDone = True
            '    End If
        End If

        If bChangesDone Then
            ' Next If added 28.07.2010, so we do not get a question after we have delete a rule
            If bRuleDeleted Then
                iResponse = MsgBoxResult.Yes
            Else
                iResponse = MsgBox("Vil du lagre endringene i avstemmingsregel: " & Me.lblWriSQLName.Text, MsgBoxStyle.YesNoCancel + MsgBoxStyle.Question, "BabelBank")
            End If
            Select Case iResponse
                Case MsgBoxResult.Yes
                    bx = oQuery.SaveMatchingRule(bNewRule)
                    If optAutomatic.Checked = True Then
                        aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 1)
                    ElseIf Me.optManual.Checked = True Then
                        aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 2)
                    ElseIf Me.optAfter.Checked = True Then
                        aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 3)
                    Else
                        aQueryArray = GetERPDBInfo((oQueryProfile.Company_ID), 4)
                    End If
                    bChangesDone = bx
                Case MsgBoxResult.No
                    bChangesDone = False
                Case MsgBoxResult.Cancel
                    bChangesDone = False
            End Select
        End If

        CheckIfRuleIsChanged = bChangesDone

        Exit Function

ErrorCheckIfRuleIsChanged:
        CheckIfRuleIsChanged = False


    End Function

    Sub PopulatePatternsWithPossibleFieldsToValidate()
        Dim i As Short

        cmbPatterns.Items.Clear()
        cmbPatterns.Items.Add("")
        ''' cmbPatterns.SelectedIndex = 1

        'NBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBN!!!!!!!!!!!!!!!!!
        'IF YOU DO CHANGES HERE, CHECK ALSO
        ' BABELBANKEXE - FRMMATCH_MANUALSETUP - FORM LOAD
        'NBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBN!!!!!!!!!!!!!!!!!

        'Populate cmbPatterns only with columns that may be edited by user!
        For i = 1 To Len(sColumnsToEnterData)
            If Mid(sColumnsToEnterData, i, 1) = "1" Then

                ' Set which columns that are eligible
                Select Case i

                    Case 1
                        cmbPatterns.Items.Add((LRSCommon(41011))) 'ClientNo  1
                    Case 2
                        cmbPatterns.Items.Add((LRSCommon(41001))) 'CustomerNo  2
                    Case 3
                        cmbPatterns.Items.Add((LRSCommon(41002))) 'InvoiceNo   3
                    Case 4
                        cmbPatterns.Items.Add((LRSCommon(41003))) 'Debit       4
                    Case 5
                        cmbPatterns.Items.Add((LRSCommon(41004))) 'Credit      5
                    Case 6
                        cmbPatterns.Items.Add((LRSCommon(41005))) 'Currency    6
                    Case 7
                        cmbPatterns.Items.Add((LRSCommon(41006))) 'ExchangeRate  7
                    Case 8
                        cmbPatterns.Items.Add((LRSCommon(41007))) 'Ledger Account 8
                    Case 9
                        cmbPatterns.Items.Add((LRSCommon(41008))) 'SupplierNo  9
                    Case 10
                        cmbPatterns.Items.Add((LRSCommon(41009))) 'VoucherNo   10
                    Case 11
                        cmbPatterns.Items.Add((LRSCommon(41012))) 'Name   11
                    Case 12
                        cmbPatterns.Items.Add((LRSCommon(41010))) 'Freetext    12
                    Case 15
                        cmbPatterns.Items.Add("MyField") 'MyField    15
                    Case Else
                        'cmbPatterns.AddItem "Innbet.kode"  '    11
                        'cmbPatterns.AddItem "Rolle"        '    12
                        'cmbPatterns.AddItem "Not in use"  'MatchID    13
                        'cmbPatterns.AddItem "Not in use"  'AkontoID    14
                End Select

            Else
                cmbPatterns.Items.Add(LRS(60467)) 'Not valid
            End If
        Next i


    End Sub
    Sub DisableAllTextControls()

        txtRuleName.Enabled = False
        txtRuleOrder.Enabled = False
        txtRuleSpecial.Enabled = False
        txtRuleType.Enabled = False
        txtWriSQL.Enabled = False
        txtIntSQL.Enabled = False
        txtRuleType.Enabled = False
        cmbPatterns.Enabled = False
        chkIncludeOCR.Enabled = False
        chkUseAdjustments.Enabled = False
        chkProposeMatch.Enabled = False

    End Sub
    Sub EnableAllTextControls()

        txtRuleName.Enabled = True
        txtRuleOrder.Enabled = True
        txtRuleSpecial.Enabled = True
        txtRuleType.Enabled = True
        txtWriSQL.Enabled = True
        txtIntSQL.Enabled = True
        txtRuleType.Enabled = True
        cmbPatterns.Enabled = True
        chkIncludeOCR.Enabled = True
        chkUseAdjustments.Enabled = True
        chkProposeMatch.Enabled = True


    End Sub

    Private Sub cmdEnd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEnd.Click
        Me.Close()
    End Sub


End Class
