<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmCustomized
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtInvoiceDescription As System.Windows.Forms.TextBox
	Public WithEvents txtGeneralNote As System.Windows.Forms.TextBox
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents txtCustom4 As System.Windows.Forms.TextBox
	Public WithEvents txtCustom1 As System.Windows.Forms.TextBox
	Public WithEvents txtCustom2 As System.Windows.Forms.TextBox
	Public WithEvents txtCustom3 As System.Windows.Forms.TextBox
	Public WithEvents lblCustom4 As System.Windows.Forms.Label
	Public WithEvents lblCustom1 As System.Windows.Forms.Label
	Public WithEvents lblCustom2 As System.Windows.Forms.Label
	Public WithEvents lblCustom3 As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Frame2 = New System.Windows.Forms.GroupBox
        Me.txtInvoiceDescription = New System.Windows.Forms.TextBox
        Me.txtGeneralNote = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Frame1 = New System.Windows.Forms.GroupBox
        Me.txtCustom4 = New System.Windows.Forms.TextBox
        Me.txtCustom1 = New System.Windows.Forms.TextBox
        Me.txtCustom2 = New System.Windows.Forms.TextBox
        Me.txtCustom3 = New System.Windows.Forms.TextBox
        Me.lblCustom4 = New System.Windows.Forms.Label
        Me.lblCustom1 = New System.Windows.Forms.Label
        Me.lblCustom2 = New System.Windows.Forms.Label
        Me.lblCustom3 = New System.Windows.Forms.Label
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.Frame2.SuspendLayout()
        Me.Frame1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Frame2
        '
        Me.Frame2.BackColor = System.Drawing.SystemColors.Window
        Me.Frame2.Controls.Add(Me.txtInvoiceDescription)
        Me.Frame2.Controls.Add(Me.txtGeneralNote)
        Me.Frame2.Controls.Add(Me.Label2)
        Me.Frame2.Controls.Add(Me.Label1)
        Me.Frame2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Frame2.Location = New System.Drawing.Point(13, 76)
        Me.Frame2.Name = "Frame2"
        Me.Frame2.Padding = New System.Windows.Forms.Padding(0)
        Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame2.Size = New System.Drawing.Size(445, 82)
        Me.Frame2.TabIndex = 12
        Me.Frame2.TabStop = False
        Me.Frame2.Text = "60254 - Text for Matching Setup"
        '
        'txtInvoiceDescription
        '
        Me.txtInvoiceDescription.AcceptsReturn = True
        Me.txtInvoiceDescription.BackColor = System.Drawing.SystemColors.Window
        Me.txtInvoiceDescription.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtInvoiceDescription.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtInvoiceDescription.Location = New System.Drawing.Point(242, 20)
        Me.txtInvoiceDescription.MaxLength = 50
        Me.txtInvoiceDescription.Name = "txtInvoiceDescription"
        Me.txtInvoiceDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtInvoiceDescription.Size = New System.Drawing.Size(194, 19)
        Me.txtInvoiceDescription.TabIndex = 0
        Me.txtInvoiceDescription.Text = "60152 - Invoicedescription"
        '
        'txtGeneralNote
        '
        Me.txtGeneralNote.AcceptsReturn = True
        Me.txtGeneralNote.BackColor = System.Drawing.SystemColors.Window
        Me.txtGeneralNote.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtGeneralNote.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtGeneralNote.Location = New System.Drawing.Point(242, 46)
        Me.txtGeneralNote.MaxLength = 50
        Me.txtGeneralNote.Name = "txtGeneralNote"
        Me.txtGeneralNote.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtGeneralNote.Size = New System.Drawing.Size(194, 19)
        Me.txtGeneralNote.TabIndex = 1
        Me.txtGeneralNote.Text = "60253 - Generalnote"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Window
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(12, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(224, 17)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "60252 - Text for column 4, ... pattern"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Window
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(11, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(224, 17)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "60251 - Text for column 3, ... pattern"
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.SystemColors.Window
        Me.Frame1.Controls.Add(Me.txtCustom4)
        Me.Frame1.Controls.Add(Me.txtCustom1)
        Me.Frame1.Controls.Add(Me.txtCustom2)
        Me.Frame1.Controls.Add(Me.txtCustom3)
        Me.Frame1.Controls.Add(Me.lblCustom4)
        Me.Frame1.Controls.Add(Me.lblCustom1)
        Me.Frame1.Controls.Add(Me.lblCustom2)
        Me.Frame1.Controls.Add(Me.lblCustom3)
        Me.Frame1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Frame1.Location = New System.Drawing.Point(11, 164)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.Padding = New System.Windows.Forms.Padding(0)
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(447, 143)
        Me.Frame1.TabIndex = 7
        Me.Frame1.TabStop = False
        Me.Frame1.Text = "60250 - Text for Setup, Userdefined menuitems"
        '
        'txtCustom4
        '
        Me.txtCustom4.AcceptsReturn = True
        Me.txtCustom4.BackColor = System.Drawing.SystemColors.Window
        Me.txtCustom4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCustom4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCustom4.Location = New System.Drawing.Point(242, 100)
        Me.txtCustom4.MaxLength = 50
        Me.txtCustom4.Name = "txtCustom4"
        Me.txtCustom4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCustom4.Size = New System.Drawing.Size(194, 19)
        Me.txtCustom4.TabIndex = 5
        '
        'txtCustom1
        '
        Me.txtCustom1.AcceptsReturn = True
        Me.txtCustom1.BackColor = System.Drawing.SystemColors.Window
        Me.txtCustom1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCustom1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCustom1.Location = New System.Drawing.Point(242, 19)
        Me.txtCustom1.MaxLength = 50
        Me.txtCustom1.Name = "txtCustom1"
        Me.txtCustom1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCustom1.Size = New System.Drawing.Size(194, 19)
        Me.txtCustom1.TabIndex = 2
        '
        'txtCustom2
        '
        Me.txtCustom2.AcceptsReturn = True
        Me.txtCustom2.BackColor = System.Drawing.SystemColors.Window
        Me.txtCustom2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCustom2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCustom2.Location = New System.Drawing.Point(242, 46)
        Me.txtCustom2.MaxLength = 50
        Me.txtCustom2.Name = "txtCustom2"
        Me.txtCustom2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCustom2.Size = New System.Drawing.Size(194, 19)
        Me.txtCustom2.TabIndex = 3
        '
        'txtCustom3
        '
        Me.txtCustom3.AcceptsReturn = True
        Me.txtCustom3.BackColor = System.Drawing.SystemColors.Window
        Me.txtCustom3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCustom3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCustom3.Location = New System.Drawing.Point(242, 73)
        Me.txtCustom3.MaxLength = 50
        Me.txtCustom3.Name = "txtCustom3"
        Me.txtCustom3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCustom3.Size = New System.Drawing.Size(194, 19)
        Me.txtCustom3.TabIndex = 4
        '
        'lblCustom4
        '
        Me.lblCustom4.BackColor = System.Drawing.SystemColors.Window
        Me.lblCustom4.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCustom4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCustom4.Location = New System.Drawing.Point(13, 104)
        Me.lblCustom4.Name = "lblCustom4"
        Me.lblCustom4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCustom4.Size = New System.Drawing.Size(224, 17)
        Me.lblCustom4.TabIndex = 15
        Me.lblCustom4.Text = "60456 - Text for custommenyline 4"
        '
        'lblCustom1
        '
        Me.lblCustom1.BackColor = System.Drawing.SystemColors.Window
        Me.lblCustom1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCustom1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCustom1.Location = New System.Drawing.Point(14, 25)
        Me.lblCustom1.Name = "lblCustom1"
        Me.lblCustom1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCustom1.Size = New System.Drawing.Size(224, 17)
        Me.lblCustom1.TabIndex = 11
        Me.lblCustom1.Text = "60247 - Text for custommenyline 1"
        '
        'lblCustom2
        '
        Me.lblCustom2.BackColor = System.Drawing.SystemColors.Window
        Me.lblCustom2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCustom2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCustom2.Location = New System.Drawing.Point(13, 51)
        Me.lblCustom2.Name = "lblCustom2"
        Me.lblCustom2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCustom2.Size = New System.Drawing.Size(224, 17)
        Me.lblCustom2.TabIndex = 10
        Me.lblCustom2.Text = "60248 - Text for custommenyline 2"
        '
        'lblCustom3
        '
        Me.lblCustom3.BackColor = System.Drawing.SystemColors.Window
        Me.lblCustom3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCustom3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCustom3.Location = New System.Drawing.Point(13, 77)
        Me.lblCustom3.Name = "lblCustom3"
        Me.lblCustom3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCustom3.Size = New System.Drawing.Size(224, 17)
        Me.lblCustom3.TabIndex = 9
        Me.lblCustom3.Text = "60249 - Text for custommenyline 3"
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(291, 316)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 6
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(379, 316)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 8
        Me.cmdCancel.Text = "55002 &Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'frmCustomized
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(475, 347)
        Me.Controls.Add(Me.Frame2)
        Me.Controls.Add(Me.Frame1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmCustomized"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Customized"
        Me.Frame2.ResumeLayout(False)
        Me.Frame1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class
