Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("Company_NET.Company")> Public Class Company
    Public oProfile As vbbabel.Profile
    Public bStatus As Boolean
    Private Shared frmWiz1_Company As frmWiz1_Company
    Private frmCustomized As frmCustomized
    Private frmDatabase As frmDatabase
    Private frmEmail As frmEmail
    '********* START PROPERTY SETTINGS ***********************
    Public WriteOnly Property Profile() As vbBabel.Profile
        Set(ByVal Value As vbBabel.Profile)
            oProfile = Value
        End Set
    End Property
    
	'********* END PROPERTY SETTINGS ***********************
	
	'UPGRADE_NOTE: class_initialize was upgraded to class_initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Sub Class_Initialize_Renamed()
		
		' When Setup is called, the code attempts
		' to find a local satellite DLL that will contain all the
		' resources.
        'If Not (LoadLocalizedResources("Setup")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
        'End If
        If RunTime() Then
            'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'App.HelpFile = My.Application.Info.DirectoryPath & "\babelbank_no.hlp"
        Else
            'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'App.HelpFile = "c:\projects\babelbank\babelbank_no.hlp"
        End If
		
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
    Public Function Start() As Boolean
        Dim i As Integer

        ' show wiz1, company info
        On Error GoTo testErr
        bStatus = True
        Setup.iWizardStepNo = 1
        Setup.oProfile = oProfile

        frmWiz1_Company = New frmWiz1_Company
        frmCustomized = New frmCustomized
        frmDatabase = New frmDatabase
        frmEmail = New frmEmail

        frmWiz1_Company.Text = LRS(60038) 'Companyinformation
        ' Fill up with companyinfo
        frmWiz1_Company.lblHeading.Text = LRS(60038) 'Companyinformation
        frmWiz1_Company.lblCompany.Text = LRS(60039) 'Companyname
        frmWiz1_Company.lblAdress1.Text = LRS(60040) ' Adress:
        frmWiz1_Company.lblAdress2.Text = LRS(60040) ' Adress:
        frmWiz1_Company.lblAdress3.Text = LRS(60040) ' Adress:
        frmWiz1_Company.lblZip.Text = LRS(60041) ' Zipcode:
        frmWiz1_Company.lblCity.Text = LRS(60042) ' City:
        frmWiz1_Company.lblCompanyNo.Text = LRS(60043) 'CompanyNo:
        frmWiz1_Company.lblAdditionalNo.Text = LRS(60044) 'CustomerID:
        frmWiz1_Company.chkBackup.Text = LRS(60045) 'Backup:
        frmWiz1_Company.lblDelDays.Text = LRS(60046) 'Delete after no of days:
        frmWiz1_Company.lblBackupPath.Text = LRS(60047) 'Backuppath:
        frmWiz1_Company.lblMaxSizeDB.Text = LRSCommon(40001) 'Maximum size for BabelBank's database:

        ShowButtonCaptions(frmWiz1_Company)

        frmWiz1_Company.txtCompany.Text = oProfile.CompanyName
        frmWiz1_Company.txtAdress1.Text = oProfile.CompanyAdr1
        frmWiz1_Company.txtAdress2.Text = oProfile.CompanyAdr2
        frmWiz1_Company.txtAdress3.Text = oProfile.CompanyAdr3
        frmWiz1_Company.txtZip.Text = oProfile.CompanyZip
        frmWiz1_Company.txtCity.Text = oProfile.CompanyCity
        frmWiz1_Company.txtCompanyNo.Text = oProfile.CompanyNo
        frmWiz1_Company.txtDivision.Text = oProfile.Division
        frmWiz1_Company.txtAdditionalNo.Text = oProfile.AdditionalNo
        frmWiz1_Company.txtBaseCurrency.Text = oProfile.BaseCurrency
        frmWiz1_Company.chkBackup.CheckState = bVal(oProfile.Backup)
        frmWiz1_Company.txtDelDays.Text = oProfile.DelDays
        frmWiz1_Company.txtBackupPath.Text = oProfile.BackupPath
        frmWiz1_Company.txtMaxSizeDB.Text = Trim(Str(oProfile.MaxSizeDB))
        frmWiz1_Company.chkDisableAdminMenus.CheckState = bVal(oProfile.DisableAdminMenus)
        frmWiz1_Company.chkLockBabelBank.CheckState = bVal(oProfile.LockBabelBank)

        frmWiz1_Company.chkMakeCopyBeforeExport.CheckState = bVal(oProfile.CreateBackup)
        frmWiz1_Company.chkBeforeImport.CheckState = bVal(oProfile.BckBeforeImport)
        frmWiz1_Company.chkBeforeExport.CheckState = bVal(oProfile.BckBeforeExport)
        frmWiz1_Company.chkAfterExport.CheckState = bVal(oProfile.BckAfterExport)
        frmWiz1_Company.txtPathForBackup.Text = oProfile.ExportBackuppath
        For i = 1 To 10
            frmWiz1_Company.cmbNoOfBackups.Items.Add(Trim(Str(i)))
        Next i
        frmWiz1_Company.cmbNoOfBackups.SelectedIndex = oProfile.ExportBackupGenerations - 1


        ' New 25.10.02 JanP
        frmDatabase.txtConnectionString.Text = Trim(oProfile.ConString)
        frmDatabase.txtConnectionString.SelectionStart = 0
        frmDatabase.txtConnectionString.SelectionLength = Len(Trim(oProfile.ConString))
        frmDatabase.txtUserID.Text = Trim(oProfile.ConUID)
        frmDatabase.txtUserID.SelectionStart = 0
        frmDatabase.txtUserID.SelectionLength = Len(Trim(oProfile.ConUID))
        frmDatabase.txtPassword.Text = Trim(oProfile.ConPWD)
        frmDatabase.txtPassword.SelectionStart = 0
        frmDatabase.txtPassword.SelectionLength = Len(Trim(oProfile.ConPWD))

        frmWiz1_Company.CmdBack.Enabled = False
        frmWiz1_Company.CmdNext.Text = "&OK"
        VB6.SetDefault(frmWiz1_Company.CmdNext, False)

        frmWiz1_Company.ShowDialog()

        bStatus = frmWiz1_Company.bStatus

        frmWiz1_Company = Nothing
        frmCustomized = Nothing
        frmDatabase = Nothing
        frmEmail = Nothing

        Start = bStatus
        Exit Function

testErr:
        MsgBox("Err")


    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
