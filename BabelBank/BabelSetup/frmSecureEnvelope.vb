﻿Imports System.Windows.Forms
Imports System
Imports System.Security.Cryptography
Imports System.Security.Cryptography.X509Certificates
Imports System.IO

Public Class frmSecureEnvelope
    Dim oFS As vbBabel.FileSetup
    Private Sub frmSecurity_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "")
        FormLRSCaptions(Me)
        FindNordeaCertificate()

    End Sub
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        If Not EmptyString(Me.txtSenderID.Text) Then
            SavefrmSecureEnvelope(oFS)
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Close()
        Else
            MsgBox("Fyll inn verdi for SignerID!", , "BabelBank")
        End If
    End Sub
    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Public Sub PassFilesetupTofrmSecurity(ByRef oPassedFileSetup As vbBabel.FileSetup)
        oFS = oPassedFileSetup
    End Sub
    Private Sub FindNordeaCertificate()
        'Create a X509Store object.

        Dim x509Store As New X509Store(StoreName.My, StoreLocation.CurrentUser)
        Dim cert As X509Certificate2
        Dim bFoundSecureEnvelopeSertificate As Boolean = False

            Try
            ' create and open store for read-only access
            x509Store.Open(OpenFlags.ReadOnly)
            ' search store
            'Dim col As New X509Certificate2Collection
            'col = x509Store.Certificates.Find(X509FindType.FindByIssuerName, "Nordea", True)
            For Each cert In x509Store.Certificates
                If cert.IssuerName.Name.IndexOf("Nordea Corporate") > -1 Then
                    bFoundSecureEnvelopeSertificate = True
                    Me.lblDescription.Text = "Usteder: " & cert.Issuer & Environment.NewLine & "Utløpsdato: " & cert.NotBefore & Environment.NewLine & "Utstedt til: " & cert.GetName
                End If
            Next
            x509Store.Close()

            ' then try to find certificate in LocalMachine if not found in CurrentUser
            If bFoundSecureEnvelopeSertificate = False Then
                x509Store = New X509Store(StoreName.My, StoreLocation.LocalMachine)
                x509Store.Open(OpenFlags.ReadOnly)
                For Each cert In x509Store.Certificates
                    If cert.IssuerName.Name.IndexOf("Nordea Corporate") > -1 Then
                        bFoundSecureEnvelopeSertificate = True
                        Me.lblDescription.Text = "Usteder: " & cert.Issuer & Environment.NewLine & "Utløpsdato: " & cert.NotAfter & Environment.NewLine & "Utstedt til: " & cert.GetName
                        
                    End If
                Next
                x509Store.Close()
            End If

            If bFoundSecureEnvelopeSertificate = False Then
                Me.lblDescription.Text = "Finner ikke noe sertifikat fra Nordea." & Environment.NewLine & "Husk at Secure Envelope sertifikatet må være lagret i Windows Certificate Store." & vbCrLf & ".P12-filen skal importeres med 'Importveiviser for sertifikat."
            End If

        Catch ex As Exception

            Me.lblDescription.Text = "Finner ikke noe sertifikat fra Nordea." & Environment.NewLine & "Husk at Secure Envelope sertifikatet må være lagret i Windows Certificate Store." & vbCrLf & ".P12-filen skal importeres med 'Importveiviser for sertifikat."

        Finally

            x509Store.Close()

        End Try

        End Sub

    Private Sub cmdFileOpenPublickey_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFileOpenPublickey.Click
        ' Hent filnavn
        Dim spath As String
        Dim sFile As String
        ' Keep filename after Browsing for folder;
        ' Must have \ and .
        If InStr(Me.txtfilenamePublicKey.Text, "\") > 0 And InStr(Me.txtfilenamePublicKey.Text, ".") > 0 Then
            sFile = Mid(Me.txtfilenamePublicKey.Text, InStrRev(Me.txtfilenamePublicKey.Text, "\"))
        ElseIf InStr(Me.txtfilenamePublicKey.Text, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & Me.txtfilenamePublicKey.Text
        Else
            sFile = ""
        End If

        spath = BrowseForFilesOrFolders(Me.txtfilenamePublicKey.Text, Me, LRS(60010), True)

        If Len(spath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(spath, ".") = 0 Then
                Me.txtfilenamePublicKey.Text = Replace(spath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                Me.txtfilenamePublicKey.Text = spath
            End If
        End If


    End Sub
End Class
