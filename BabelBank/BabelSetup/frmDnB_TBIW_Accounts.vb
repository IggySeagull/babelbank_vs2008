Option Strict Off
Option Explicit On
Friend Class frmDnB_TBIW_Accounts
    Inherits System.Windows.Forms.Form
    Private frmAccountDetail As New frmAccountDetail
    Private listitem As New _MyListBoxItem
    Private Sub cmdAdd_Click()
        ' XNET 28.06.2011 - added this Add-function.
        ' XNET 22.07.2011 - many changes, take whole function which is obvious since we have not xOKnetted !

        ' TODO REMEMBER to add the button in frmDnB_TBIW_Accounts
        ' TODO REMEMBER to add LRS (60579) - "Please save this profile first, before adding accounts !"
        Dim sMySQL As String = ""
        Dim sMySQL2 As String = ""
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim oMyDal2 As vbBabel.DAL = Nothing

        ' XNET 22.07.2011
        Dim sCompany_ID As String
        Dim iClientFormat_ID As Integer
        Dim sFilesetup_ID As String = ""
        Dim sFilesetupOut As String = ""

        sCompany_ID = "1"

        ' XNET 22.07.2011
        ' Test if profile is saved - must do so first
        sMySQL = "SELECT * From Filesetup"
        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then

            'If rsClients.EOF Then
            If Not oMyDal.Reader_HasRows Then
                ' No profiles saved !
                MsgBox(LRS(60579), vbOKOnly, LRS(60298))
                'MsgBox "Please save this profile first, before adding accounts !", vbOKOnly


            Else

                ' XNET 22.07.2011 - changed below
                'sMySQL = "SELECT * From Client WHERE Client_ID = 1" '
                sMySQL = "SELECT * From Client"
                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then

                    'If rsClients.EOF Then
                    If Not oMyDal.Reader_HasRows Then
                        ' No clients registered !
                        'MsgBox LRS(60578), vbOKOnly, LRS(60298)
                        'MsgBox "You must have at least one client before adding accounts. This can be done from the commandbutton ""Clients"" in the main form.", "Accounts", vbOKOnly

                        ' XNET 22.07.2011 -
                        ' If no Clients, add a new Client
                        ' ClientNo TBI1
                        ' ClientName TBI1

                        sMySQL = "INSERT INTO Client(Company_ID, Client_ID, ClientNo, Name) VALUES("
                        sMySQL = sMySQL & sCompany_ID & ", 1, 'TBI1', 'TBI1')"

                        If oMyDal.ExecuteNonQuery Then
                            If oMyDal.RecordsAffected = 1 Then
                                'OK
                            Else
                                Throw New Exception("Unable to add client!" & vbCrLf & oMyDal.ErrorMessage)
                            End If
                        Else
                            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                        End If
                    End If

                    ' have clients - make sure all profiles are connected to the clients
                    ' Must add this client and profile to ClientFormat
                    ' do so for all profiles


                    ' Find next ClientFormat_ID
                    sMySQL = "SELECT MAX(ClientFormat_ID) AS MAX_ID From ClientFormat"
                    oMyDal.SQL = sMySQL
                    If oMyDal.Reader_Execute() Then

                        'If rsClients.EOF Then
                        If Not oMyDal.Reader_HasRows Then
                            Do While oMyDal.Reader_ReadRecord

                                'If IsNull(rsClientFormat!MAX_ID) Then
                                If oMyDal.Reader_GetString("MAX_ID") = Nothing Then
                                    iClientFormat_ID = 1
                                Else
                                    iClientFormat_ID = Val(oMyDal.Reader_GetString("MAX_ID")) + 1
                                End If
                                Exit Do
                            Loop
                        End If
                    End If

                    sMySQL = "SELECT * From Filesetup"
                    oMyDal.SQL = sMySQL
                    If oMyDal.Reader_Execute() Then

                        Do While oMyDal.Reader_ReadRecord
                            sFilesetup_ID = oMyDal.Reader_GetString("FileSetup_ID")
                            sFilesetupOut = oMyDal.Reader_GetString("FileSetupOut")

                            ' Check if this ClientFormat is already set;
                            sMySQL2 = "SELECT * From ClientFormat WHERE Client_ID = 1 AND "
                            sMySQL2 = sMySQL2 & "FormatIn =" & sFilesetup_ID & " AND "
                            sMySQL2 = sMySQL2 & "FormatOut = " & sFilesetupOut
                            oMyDal2.SQL = sMySQL2
                            If oMyDal2.Reader_Execute() Then

                                If Not oMyDal2.Reader_HasRows Then
                                    ' Check also the "other way", if set as FormatIn = 1 and FormatOut = 2 in first
                                    ' test, then this time check if FormatIn = 2 and FormatOut = 1 in this test
                                    ' Check if this ClientFormat is already set;
                                    sMySQL2 = "SELECT * From ClientFormat WHERE Client_ID = 1 AND "
                                    sMySQL2 = sMySQL2 & "FormatOut =" & sFilesetup_ID & " AND "
                                    sMySQL2 = sMySQL2 & "FormatIn = " & sFilesetupOut
                                    oMyDal2.SQL = sMySQL2
                                    If oMyDal2.Reader_Execute() Then

                                        If Not oMyDal2.Reader_HasRows Then
                                            sMySQL = "INSERT INTO ClientFormat "
                                            sMySQL = sMySQL & "(Company_ID, ClientFormat_ID, Client_ID, "
                                            sMySQL = sMySQL & "FormatIn, FormatOut, KIDStart, KIDLength, "
                                            sMySQL = sMySQL & "KIDValue) VALUES("
                                            sMySQL = sMySQL & sCompany_ID & ","
                                            sMySQL = sMySQL & Str(iClientFormat_ID) & ", "
                                            sMySQL = sMySQL & "1, "
                                            sMySQL = sMySQL & sFilesetup_ID & ", "
                                            sMySQL = sMySQL & sFilesetupOut & ", "
                                            sMySQL = sMySQL & "-1, -1, -1)"

                                            oMyDal2.SQL = sMySQL
                                            If oMyDal2.ExecuteNonQuery Then
                                                If oMyDal2.RecordsAffected = 1 Then
                                                    'OK
                                                Else
                                                    Throw New Exception("Error when Inserting into ClientFormat!" & vbCrLf & oMyDal.ErrorMessage)
                                                End If
                                            Else
                                                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                                            End If
                                        End If
                                    End If
                                End If
                            End If

                            iClientFormat_ID = iClientFormat_ID + 1
                            ' no need to put for both Filesetup out and in, thus skip next record
                            oMyDal.Reader_ReadRecord()

                        Loop

                        With frmAccountDetail
                            'Hide txtBoxes and labels that are irrelevant here
                            .lblAvtalegiro.Visible = False
                            .txtAvtalegiro.Visible = False
                            .lblBankGL.Visible = False
                            .txtBankGL.Visible = False
                            .lblResultGL.Visible = False
                            .txtResultGL.Visible = False
                            .lblContractNo.Visible = False
                            .txtContractNo.Visible = False
                            .cmdOCRSettings.Visible = False

                            .lblAccountCountryCode.Left = 2865
                            .lblAccountCountryCode.Top = 3135
                            .txtAccountCountryCode.Left = 5265  ' XNET 30.06.2011
                            .txtAccountCountryCode.Top = 3090
                            .lblAccountCountryCode.Visible = True
                            .txtAccountCountryCode.Visible = True

                            ' clear all info i form
                            .txtAccountCountryCode.Text = ""
                            .txtAccountNo.Text = ""
                            .txtConverted.Text = ""
                            .txtCurrencyCode.Text = ""
                            ' XNET 07.09.2011
                            .txtDebitAccountCountryCode.Text = ""
                            .txtSWIFTAdr.Text = ""

                        End With

                        ' Show frmAccountDetail
                        frmAccountDetail.Show()

                        ' Update database and listbox
                        If Not frmAccountDetail.AnythingChanged Then
                            SaveAccountInfo()  ' save to Accounts table
                        End If
                    End If
                End If
            End If
        Else
            Throw New Exception("Unable to add client! - Error in oMyDal.Reader_Execute()" & vbCrLf & oMyDal.ErrorMessage)
        End If

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If
        If Not oMyDal2 Is Nothing Then
            oMyDal2.Close()
            oMyDal2 = Nothing
        End If

    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.Hide()
    End Sub

    Private Sub lstAccounts_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstAccounts.DoubleClick
        ' show accountdetails
        cmdShowAccounts_Click(cmdShowAccounts, New System.EventArgs())
    End Sub
    Private Sub frmDnB_TBIW_Accounts_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg", 400, 60)
        FormLRSCaptions(Me)
    End Sub
    Private Sub cmdShowAccounts_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdShowAccounts.Click

        If Me.lstAccounts.SelectedIndex > -1 Then

            With frmAccountDetail
                'Hide AvtaleGiroID and show AccountCountryCode
                .lblBankGL.Visible = False
                .txtBankGL.Visible = False
                .lblResultGL.Visible = False
                .txtResultGL.Visible = False
                .lblContractNo.Visible = False
                .txtContractNo.Visible = False
                .cmdOCRSettings.Visible = False

                .lblAccountCountryCode.Left = VB6.TwipsToPixelsX(2865)
                .lblAccountCountryCode.Top = VB6.TwipsToPixelsY(3135)
                .txtAccountCountryCode.Left = VB6.TwipsToPixelsX(5295)
                .txtAccountCountryCode.Top = VB6.TwipsToPixelsY(3090)
                .lblAccountCountryCode.Visible = True
                .txtAccountCountryCode.Visible = True

                ' Fill frmAccountDetail for this account;
                '.txtAccountNo.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 1))
                '.txtBankGL.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 2))
                '.txtResultGL.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 3))
                '.txtConverted.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 4))
                '.txtContractNo.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 5))
                '.txtAvtalegiro.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 6))
                '.txtSWIFTAdr.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 7))
                '.txtAccountCountryCode.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 8))
                '.txtCurrencyCode.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 9))
                '.txtDebitAccountCountryCode.Text = Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 10))

                ' 15.07.2019
                .txtAccountNo.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 1))
                .txtBankGL.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 2))
                .txtResultGL.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 3))
                .txtConverted.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 4))
                .txtContractNo.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 5))
                .txtAvtalegiro.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 6))
                .txtSWIFTAdr.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 7))
                .txtAccountCountryCode.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 8))
                .txtCurrencyCode.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 9))
                .txtDebitAccountCountryCode.Text = Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 10))

            End With

            ' Show frmAccountDetail
            FormSelectAllTextboxs(frmAccountDetail)
            frmAccountDetail.ShowDialog()

            ' Update listbox
            If Not frmAccountDetail.AnythingChanged Then
                If frmAccountDetail.AnythingChanged Then
                    'Save data locally in this form
                    With frmAccountDetail
                        'VB6.SetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex, PadRight(.txtAccountNo.Text, 100, " ") & Chr(9) & .txtBankGL.Text & Chr(9) & .txtResultGL.Text & Chr(9) & .txtConverted.Text & Chr(9) & .txtContractNo.Text & Chr(9) & .txtAvtalegiro.Text & Chr(9) & Trim(.txtSWIFTAdr.Text) & Chr(9) & Trim(.txtAccountCountryCode.Text) & Chr(9) & Trim(.txtCurrencyCode.Text) & Chr(9) & Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 10))) 'Last, 10th item is client_ID
                        'VB6.SetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex, PadRight(.txtAccountNo.Text, 100, " ") & Chr(9) & .txtBankGL.Text & Chr(9) & .txtResultGL.Text & Chr(9) & .txtConverted.Text & Chr(9) & .txtContractNo.Text & Chr(9) & .txtAvtalegiro.Text & Chr(9) & Trim(.txtSWIFTAdr.Text) & Chr(9) & Trim(.txtAccountCountryCode.Text) & Chr(9) & Trim(.txtCurrencyCode.Text) & Chr(9) & Trim(.txtDebitAccountCountryCode.Text) & Chr(9) & Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 10))) 'Last, 10th item is client_ID
                        ' 11.07.2019
                        Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring = PadRight(.txtAccountNo.Text, 100, " ") & Chr(9) & .txtBankGL.Text & Chr(9) & .txtResultGL.Text & Chr(9) & .txtConverted.Text & Chr(9) & .txtContractNo.Text & Chr(9) & .txtAvtalegiro.Text & Chr(9) & Trim(.txtSWIFTAdr.Text) & Chr(9) & Trim(.txtAccountCountryCode.Text) & Chr(9) & Trim(.txtCurrencyCode.Text) & Chr(9) & Trim(.txtDebitAccountCountryCode.Text) & Chr(9) & Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 10)) 'Last, 10th item is client_ID
                        'Save data to the BBDB.
                    End With
                    SaveAccountInfo()
                End If
            End If
        End If

    End Sub
    Private Sub SaveAccountInfo(Optional ByVal bNewAccount As Boolean = False)
        Dim sMySQL As String
        Dim sString As String
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sAccount_ID As Integer
        Dim sMaxSQL As String

        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If

        With frmAccountDetail
            If Not bNewAccount Then
                sMySQL = "UPDATE Account SET "

                '----------
                ' AccountNo
                '----------
                sMySQL = sMySQL & "Account = "
                sString = Trim(.txtAccountNo.Text)
                sMySQL = sMySQL & "'" & sString & "', "

                '-------------------
                ' GLAccount (BankGL)
                '-------------------
                sMySQL = sMySQL & "GLAccount = "
                sString = Trim(.txtBankGL.Text)
                sMySQL = sMySQL & "'" & sString & "', "

                '-----------------
                ' GLResultsAccount
                '-----------------
                sMySQL = sMySQL & "GLResultsAccount = "
                sString = Trim(.txtResultGL.Text)
                sMySQL = sMySQL & "'" & sString & "', "

                '-----------------
                ' ConvertedAccount
                '-----------------
                sMySQL = sMySQL & "ConvertedAccount = "
                sString = Trim(.txtConverted.Text)
                sMySQL = sMySQL & "'" & sString & "', "

                '-----------------
                ' ContractNo
                '-----------------
                sMySQL = sMySQL & "ContractNo = "
                sString = Trim(.txtContractNo.Text)
                sMySQL = sMySQL & "'" & sString & "', "

                '-----------------
                ' AvtalegiroID
                '-----------------
                'Not saved because it can't be changed in frmAccountDetail when called from this form

                '-----------------
                ' SWIFT-address
                '-----------------
                sMySQL = sMySQL & "SWIFTAddr = "
                sString = Trim(.txtSWIFTAdr.Text)
                sMySQL = sMySQL & "'" & sString & "', "

                '-----------------
                ' Default countrycode for payees account
                '-----------------
                sMySQL = sMySQL & "AccountCountryCode = "
                sString = Trim(.txtAccountCountryCode.Text)
                sMySQL = sMySQL & "'" & sString & "', "

                '----------------------------------------
                ' Accounts currencycode, added 16.08.2007
                '----------------------------------------
                sMySQL = sMySQL & "CurrencyCode = "
                sString = Trim(.txtCurrencyCode.Text)
                sMySQL = sMySQL & "'" & sString & "' "

                '----------------------------------------
                ' XokNET DebitAccounts countrycode, added 07.09.2011
                '----------------------------------------
                sMySQL = sMySQL & "DebitAccountCountryCode = "
                sString = Trim$(.txtDebitAccountCountryCode.Text)
                sMySQL = sMySQL & "'" & sString & "' "

                sMySQL = sMySQL & "WHERE Company_ID = 1 AND " 'TODO: Hardcoded Company_ID
                'sMySQL = sMySQL & "Client_ID = " & Trim(xDelim(VB6.GetItemString(Me.lstAccounts, Me.lstAccounts.SelectedIndex), Chr(9), 11)) & " AND "
                ' 11.07.2019
                sMySQL = sMySQL & "Client_ID = " & Trim(xDelim(Me.lstAccounts.Items(Me.lstAccounts.SelectedIndex).itemstring, Chr(9), 11)) & " AND "
                sMySQL = sMySQL & "Account_ID = " & Trim(Str(VB6.GetItemData(Me.lstAccounts, Me.lstAccounts.SelectedIndex)))
                ' Update account, already existing
                'iAccount_ID = Me.lstAccounts.ItemData(j)

            Else
                ' user has Added new account;
                ' XokNET 28.06.2011
                ' For new accounts, we ASSUME the following scenario -
                ' because this is a simplified accountsinput for DnB for one-client setups;
                ' ClientNo = 1, attach to this client only
                ' Find last Account ID;

                sMaxSQL = "SELECT MAX(Account_ID) AS MAXAccount_ID FROM Account WHERE Client_ID = 1"
                oMyDal.SQL = sMaxSQL

                If oMyDal.Reader_Execute() Then
                    oMyDal.Reader_ReadRecord()
                    If oMyDal.Reader_GetString("MAXAccount_ID") = Nothing Then
                        'If IsNull(rsAccounts!MaxAccount_ID) Then
                        sAccount_ID = 1
                    Else
                        sAccount_ID = Str(Val(oMyDal.Reader_GetString("MAXAccount_ID")) + 1)
                    End If

                End If

                sMySQL = "INSERT INTO ACCOUNT(Company_ID, Client_ID, Account_ID, Account, ConvertedAccount, SwiftAddr, AccountCountryCode, CurrencyCode, DebitAccountCountryCode) VALUES ("
                sMySQL = sMySQL & "1, 1, " & sAccount_ID & ", '" & Trim$(.txtAccountNo.Text) & "', '" & Trim$(.txtConverted.Text) & "', '"
                sMySQL = sMySQL & Trim$(.txtSWIFTAdr.Text) & "', '" & Trim$(.txtAccountCountryCode.Text) & "', '" & Trim$(.txtCurrencyCode.Text) & "', '" & Trim$(.txtDebitAccountCountryCode.Text) & "')"

            End If


            oMyDal.SQL = sMySQL
            If oMyDal.ExecuteNonQuery Then
                If oMyDal.RecordsAffected = 1 Then
                    'OK
                Else
                    Throw New Exception("Error inserting into account table" & vbCrLf & oMyDal.ErrorMessage)
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If

            ' XokNET 30.06.2011 For newly added account, fill listbox as we have done for existing accounts;
            If bNewAccount Then
                ' find info from database
                sMaxSQL = "SELECT * FROM Account WHERE Account_ID = " & sAccount_ID
                oMyDal.SQL = sMaxSQL
                If oMyDal.Reader_Execute() Then
                    oMyDal.Reader_ReadRecord()
                    If oMyDal.Reader_HasRows Then
                        Do While oMyDal.Reader_ReadRecord
                            'Me.lstAccounts.Items.Add(PadRight(oMyDal.Reader_GetString("Account"), 100, " ") & Chr(9) & oMyDal.Reader_GetString("GLAccount") & Chr(9) & oMyDal.Reader_GetString("GLResultsAccount") & Chr(9) & oMyDal.Reader_GetString("ConvertedAccount") & Chr(9) & oMyDal.Reader_GetString("ContractNo") & Chr(9) & oMyDal.Reader_GetString("AvtaleGiroID") & Chr(9) & oMyDal.Reader_GetString("SWIFTAddr") & Chr(9) & oMyDal.Reader_GetString("AccountCountryCode") & Chr(9) & oMyDal.Reader_GetString("CurrencyCode") & Chr(9) & oMyDal.Reader_GetString("DebitAccountCountryCode") & Chr(9) & oMyDal.Reader_GetString("Client_ID"))
                            'VB6.SetItemData(Me.lstAccounts, (Me.lstAccounts.Items.Count - 1), oMyDal.Reader_GetString("Account_ID"))
                            ' 11.07.2019
                            listitem = New _MyListBoxItem
                            listitem.ItemString = PadRight(oMyDal.Reader_GetString("Account"), 100, " ") & Chr(9) & oMyDal.Reader_GetString("GLAccount") & Chr(9) & oMyDal.Reader_GetString("GLResultsAccount") & Chr(9) & oMyDal.Reader_GetString("ConvertedAccount") & Chr(9) & oMyDal.Reader_GetString("ContractNo") & Chr(9) & oMyDal.Reader_GetString("AvtaleGiroID") & Chr(9) & oMyDal.Reader_GetString("SWIFTAddr") & Chr(9) & oMyDal.Reader_GetString("AccountCountryCode") & Chr(9) & oMyDal.Reader_GetString("CurrencyCode") & Chr(9) & oMyDal.Reader_GetString("DebitAccountCountryCode") & Chr(9) & oMyDal.Reader_GetString("Client_ID")
                            listitem.ItemData = oMyDal.Reader_GetString("Account_ID")
                            Me.lstAccounts.Items.Add(listitem)
                        Loop
                    End If
                End If


            End If


            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

        End With

    End Sub
End Class
