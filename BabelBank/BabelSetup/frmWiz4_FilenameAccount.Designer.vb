<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmWiz4_FilenameAccount
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdFilter As System.Windows.Forms.Button
	Public WithEvents cmdSpecial As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents CmdFinish As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdNext As System.Windows.Forms.Button
	Public WithEvents CmdBack As System.Windows.Forms.Button
	Public WithEvents chkManual As System.Windows.Forms.CheckBox
	Public WithEvents cmdFileOpen As System.Windows.Forms.Button
    Public WithEvents chkBackup As System.Windows.Forms.CheckBox
	Public WithEvents txtFromInternal As System.Windows.Forms.TextBox
    Public WithEvents lblFromInternal As System.Windows.Forms.Label
    Public WithEvents fraFromInternal As System.Windows.Forms.GroupBox
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
    Public WithEvents lblHeading As System.Windows.Forms.Label
	Public WithEvents lblDescr As System.Windows.Forms.Label
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWiz4_FilenameAccount))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdFilter = New System.Windows.Forms.Button
        Me.cmdSpecial = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.CmdFinish = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdNext = New System.Windows.Forms.Button
        Me.CmdBack = New System.Windows.Forms.Button
        Me.fraFromInternal = New System.Windows.Forms.GroupBox
        Me.chkManual = New System.Windows.Forms.CheckBox
        Me.cmdFileOpen = New System.Windows.Forms.Button
        Me.chkBackup = New System.Windows.Forms.CheckBox
        Me.txtFromInternal = New System.Windows.Forms.TextBox
        Me.lblFromInternal = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblDescr = New System.Windows.Forms.Label
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.fraFromInternal.SuspendLayout()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdFilter
        '
        Me.cmdFilter.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFilter.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFilter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFilter.Location = New System.Drawing.Point(304, 260)
        Me.cmdFilter.Name = "cmdFilter"
        Me.cmdFilter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFilter.Size = New System.Drawing.Size(147, 21)
        Me.cmdFilter.TabIndex = 18
        Me.cmdFilter.Text = "&Filter"
        Me.cmdFilter.UseVisualStyleBackColor = False
        '
        'cmdSpecial
        '
        Me.cmdSpecial.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSpecial.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSpecial.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSpecial.Location = New System.Drawing.Point(456, 260)
        Me.cmdSpecial.Name = "cmdSpecial"
        Me.cmdSpecial.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSpecial.Size = New System.Drawing.Size(147, 21)
        Me.cmdSpecial.TabIndex = 15
        Me.cmdSpecial.Text = "&Spesial"
        Me.cmdSpecial.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(144, 303)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 4
        Me.CmdHelp.Text = "&Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'CmdFinish
        '
        Me.CmdFinish.BackColor = System.Drawing.SystemColors.Control
        Me.CmdFinish.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdFinish.Enabled = False
        Me.CmdFinish.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdFinish.Location = New System.Drawing.Point(530, 303)
        Me.CmdFinish.Name = "CmdFinish"
        Me.CmdFinish.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdFinish.Size = New System.Drawing.Size(73, 21)
        Me.CmdFinish.TabIndex = 8
        Me.CmdFinish.Text = "F&ullf�r"
        Me.CmdFinish.UseVisualStyleBackColor = False
        Me.CmdFinish.Visible = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(288, 303)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 5
        Me.CmdCancel.Text = "&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdNext
        '
        Me.CmdNext.BackColor = System.Drawing.SystemColors.Control
        Me.CmdNext.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdNext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdNext.Location = New System.Drawing.Point(439, 303)
        Me.CmdNext.Name = "CmdNext"
        Me.CmdNext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdNext.Size = New System.Drawing.Size(73, 21)
        Me.CmdNext.TabIndex = 0
        Me.CmdNext.Text = "&Neste >"
        Me.CmdNext.UseVisualStyleBackColor = False
        '
        'CmdBack
        '
        Me.CmdBack.BackColor = System.Drawing.SystemColors.Control
        Me.CmdBack.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdBack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdBack.Location = New System.Drawing.Point(365, 303)
        Me.CmdBack.Name = "CmdBack"
        Me.CmdBack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdBack.Size = New System.Drawing.Size(73, 21)
        Me.CmdBack.TabIndex = 6
        Me.CmdBack.Text = "< &Forrige"
        Me.CmdBack.UseVisualStyleBackColor = False
        '
        'fraFromInternal
        '
        Me.fraFromInternal.BackColor = System.Drawing.SystemColors.Window
        Me.fraFromInternal.Controls.Add(Me.chkManual)
        Me.fraFromInternal.Controls.Add(Me.cmdFileOpen)
        Me.fraFromInternal.Controls.Add(Me.chkBackup)
        Me.fraFromInternal.Controls.Add(Me.txtFromInternal)
        Me.fraFromInternal.Controls.Add(Me.lblFromInternal)
        Me.fraFromInternal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraFromInternal.Location = New System.Drawing.Point(185, 133)
        Me.fraFromInternal.Name = "fraFromInternal"
        Me.fraFromInternal.Padding = New System.Windows.Forms.Padding(0)
        Me.fraFromInternal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraFromInternal.Size = New System.Drawing.Size(423, 101)
        Me.fraFromInternal.TabIndex = 9
        Me.fraFromInternal.TabStop = False
        Me.fraFromInternal.Text = "Filer fra internt system"
        '
        'chkManual
        '
        Me.chkManual.BackColor = System.Drawing.SystemColors.Window
        Me.chkManual.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkManual.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkManual.Location = New System.Drawing.Point(108, 46)
        Me.chkManual.Name = "chkManual"
        Me.chkManual.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkManual.Size = New System.Drawing.Size(241, 17)
        Me.chkManual.TabIndex = 17
        Me.chkManual.Text = "Manuelt valg av filer"
        Me.chkManual.UseVisualStyleBackColor = False
        '
        'cmdFileOpen
        '
        Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
        Me.cmdFileOpen.Location = New System.Drawing.Point(390, 17)
        Me.cmdFileOpen.Name = "cmdFileOpen"
        Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen.TabIndex = 16
        Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen.UseVisualStyleBackColor = False
        '
        'chkBackup
        '
        Me.chkBackup.BackColor = System.Drawing.SystemColors.Window
        Me.chkBackup.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkBackup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkBackup.Location = New System.Drawing.Point(108, 70)
        Me.chkBackup.Name = "chkBackup"
        Me.chkBackup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkBackup.Size = New System.Drawing.Size(281, 17)
        Me.chkBackup.TabIndex = 3
        Me.chkBackup.Text = "Legg i backupkatalogen etter import"
        Me.chkBackup.UseVisualStyleBackColor = False
        '
        'txtFromInternal
        '
        Me.txtFromInternal.AcceptsReturn = True
        Me.txtFromInternal.BackColor = System.Drawing.SystemColors.Window
        Me.txtFromInternal.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFromInternal.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFromInternal.Location = New System.Drawing.Point(108, 21)
        Me.txtFromInternal.MaxLength = 255
        Me.txtFromInternal.Name = "txtFromInternal"
        Me.txtFromInternal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFromInternal.Size = New System.Drawing.Size(277, 20)
        Me.txtFromInternal.TabIndex = 0
        '
        'lblFromInternal
        '
        Me.lblFromInternal.BackColor = System.Drawing.SystemColors.Window
        Me.lblFromInternal.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFromInternal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFromInternal.Location = New System.Drawing.Point(8, 24)
        Me.lblFromInternal.Name = "lblFromInternal"
        Me.lblFromInternal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFromInternal.Size = New System.Drawing.Size(100, 33)
        Me.lblFromInternal.TabIndex = 12
        Me.lblFromInternal.Text = "Filnavn"
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(130, 260)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(17, 21)
        Me._Image1_0.TabIndex = 19
        Me._Image1_0.TabStop = False
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(183, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(417, 17)
        Me.lblHeading.TabIndex = 14
        Me.lblHeading.Text = "Legg inn navn p� filer fra internt system (regnskap)"
        '
        'lblDescr
        '
        Me.lblDescr.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescr.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescr.Location = New System.Drawing.Point(186, 52)
        Me.lblDescr.Name = "lblDescr"
        Me.lblDescr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescr.Size = New System.Drawing.Size(417, 63)
        Me.lblDescr.TabIndex = 13
        Me.lblDescr.Text = resources.GetString("lblDescr.Text")
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(13, 296)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(595, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmWiz4_FilenameAccount
        '
        Me.AcceptButton = Me.CmdNext
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(620, 333)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdFilter)
        Me.Controls.Add(Me.cmdSpecial)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.CmdFinish)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdNext)
        Me.Controls.Add(Me.CmdBack)
        Me.Controls.Add(Me.fraFromInternal)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.lblDescr)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmWiz4_FilenameAccount"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Filnavn fra intern system"
        Me.fraFromInternal.ResumeLayout(False)
        Me.fraFromInternal.PerformLayout()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
