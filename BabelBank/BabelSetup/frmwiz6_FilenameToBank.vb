Option Strict Off
Option Explicit On

Friend Class frmWiz6_FilenameToBank
	Inherits System.Windows.Forms.Form
	
	Private Sub cmdAdvanced_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAdvanced.Click
		LoadFormatAdvanced()
	End Sub
	Private Sub frmWiz6_FilenameToBank_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        FormvbStyle(Me, "dollar.jpg", 550)
        FormLRSCaptions(Me)
        ' 28.12.2010 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next
    End Sub
	
	Private Sub CmdBack_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdBack.Click
		Me.Hide()
		WizardMove((-1))
	End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
		WizardMove((-99))
	End Sub

	Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
		' Hent filnavn
		
		Dim spath As String
		Dim sFile As String
		' Keep filename after Browsing for folder;
		' Must have \ and .
		If InStr(Me.txtToBank.Text, "\") > 0 And InStr(Me.txtToBank.Text, ".") > 0 Then
			sFile = Mid(Me.txtToBank.Text, InStrRev(Me.txtToBank.Text, "\"))
		ElseIf InStr(Me.txtToBank.Text, ".") > 0 Then 
			' . only, keep all;
			sFile = "\" & Me.txtToBank.Text
		Else
			sFile = ""
		End If
		
        spath = BrowseForFilesOrFolders(Me.txtToBank.Text, Me, LRS(60010), True)
		
		If Len(spath) > 0 Then
			' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
			If InStr(spath, ".") = 0 Then
				Me.txtToBank.Text = Replace(spath & sFile, "\\", "\")
			Else
				' Possibly file selected
				Me.txtToBank.Text = spath
			End If
		End If
		
	End Sub
	
	Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
		Dim bOK As Boolean
		' Show Helptopic
		'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(108))
		
	End Sub
	Private Sub CmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdNext.Click
		Dim bContinue As Boolean
		bContinue = True
		
		' Validate filenames:
		' No * in out-filenames!
		' No combination of $ and �
		If InStr(Me.txtToBank.Text, "*") > 0 Then
			MsgBox(LRS(60139), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation)
			bContinue = False
		End If
        If (InStr(Me.txtToBank.Text, "�") > 0 And InStr(Me.txtToBank.Text, "�") > 0) Then
            MsgBox(LRS(60140), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation)
            bContinue = False
        End If
		'If InStr(Me.txtToBank.Text, "�") > 1 Then
		If Len(Me.txtToBank.Text) - Len(Replace(Me.txtToBank.Text, "�", "")) > 1 Then
			MsgBox(LRS(60141), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation) ' more than one �
			bContinue = False
		End If
		' Either :\ or \\ or // in filename/path;
		If Len(Trim(Me.txtToBank.Text)) > 0 Then
			If InStr(Me.txtToBank.Text, ":\") = 0 And InStr(Me.txtToBank.Text, "\\") = 0 And InStr(Me.txtToBank.Text, "//") = 0 Then
				' Added 11.10.06, Can have LogonString for database formats;
				If InStr(Me.txtToBank.Text, "Provider") = 0 And InStr(Me.txtToBank.Text, "DSN=") = 0 Then
					MsgBox(LRS(60193) & " " & Me.txtToBank.Text, MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation) ' Invalid filename!
					bContinue = False
				End If
			End If
		End If
		If bContinue Then
			
			Me.Hide()
			WizardMove((0)) ' spesielt for Wizard6, Jump h�ndteres i WizardMove for Wiz6
		Else
			Me.txtToBank.Focus()
		End If
	End Sub
	
	Private Sub cmdSequence_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSequence.Click
		SetupSequence()
	End Sub
	
	Private Sub frmWiz6_FilenameToBank_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
			cmdCancel_Click(cmdCancel, New System.EventArgs())
		End If
		eventArgs.Cancel = Cancel
	End Sub

    Private Sub cmdSecurity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSecurity.Click
        LoadfrmSecurity()
    End Sub

    Private Sub frmSecureEnvelope_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles frmSecureEnvelope.Click
        LoadfrmSecureEnvelope()
    End Sub
End Class
