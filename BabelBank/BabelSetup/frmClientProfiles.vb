Option Strict Off
Option Explicit On
Friend Class frmClientProfiles
    Inherits System.Windows.Forms.Form

    Private frmClientSettings As frmClientSettings
    Private bAnythingChanged As Boolean
    Private listitem As New _MyListBoxItem
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        ' Still sp�rsm�l om avbryt uten lagring
        Me.Hide()
        bAnythingChanged = False
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.Hide()
        bAnythingChanged = True
    End Sub

    Private Sub cmdSelectAll_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSelectAll.Click
        Dim i As Short
        For i = Me.lstPossible.Items.Count - 1 To 0 Step -1
            ProfileSelect(i)
        Next i
        If Me.lstSelected.Items.Count > 0 Then
            Me.lstSelected.SelectedIndex = 0
        End If
    End Sub

    Private Sub cmdSelectOne_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSelectOne.Click
        Dim iIndex As Integer
        iIndex = Me.lstPossible.SelectedIndex
        ProfileSelect(iIndex)
        If Me.lstPossible.Items.Count > 0 Then
            Me.lstPossible.SelectedIndex = 0
        End If
    End Sub
    Private Sub ProfileSelect(ByRef iIndex As Short)
        Dim sString As String
        If iIndex > -1 Then
            ' Move item from lstPossible to lstSelected
            'sString = VB6.GetItemString(Me.lstPossible, iIndex)
            ' 11.07.2019
            sString = Me.lstPossible.Items(iIndex).itemstring
            ' Remove (Send), (Return) or (Match)
            'sString = Left$(sString, InStr(sString, " ") - 1)
            Me.lstPossible.Items.RemoveAt(iIndex)

            ' Insert into lstSelected
            'Me.lstSelected.Items.Add((sString))
            ' 11.07.2019
            listitem = New _MyListBoxItem
            listitem.ItemString = sString
            'listitem.ItemData = Me.lstSelected.Items(Me.lstSelected.SelectedIndex).itemdata
            Me.lstSelected.Items.Add(listitem)

        End If
    End Sub
    Private Sub cmdUnSelectAll_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUnselectAll.Click
        Dim i As Short
        For i = Me.lstSelected.Items.Count - 1 To 0 Step -1
            ProfileUnSelect(i)
        Next i
        Me.lstPossible.SelectedIndex = 0
    End Sub

    Private Sub cmdUnselectOne_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUnselectOne.Click
        ProfileUnSelect(Me.lstSelected.SelectedIndex)
        If Me.lstSelected.Items.Count > 0 Then
            Me.lstSelected.SelectedIndex = 0
        End If

    End Sub
    Private Sub ProfileUnSelect(ByVal iIndex As Short)
        Dim sString As String
        If iIndex > -1 Then
            ' Move item from lstSelected to lstPossible
            'sString = VB6.GetItemString(Me.lstSelected, iIndex)
            ' 11.07.2019
            sString = Me.lstSelected.Items(iIndex).itemstring
            ' Insert into lstPossible
            'Me.lstPossible.Items.Add((sString))
            'VB6.SetItemData(Me.lstPossible, Me.lstPossible.Items.Count - 1, VB6.GetItemData(Me.lstSelected, Me.lstSelected.SelectedIndex))
            ' 11.07.2019
            listitem = New _MyListBoxItem
            listitem.ItemString = sString
            listitem.ItemData = Me.lstSelected.Items(iIndex).itemdata
            Me.lstPossible.Items.Add(listitem)

            Me.lstPossible.SelectedIndex = Me.lstPossible.Items.Count - 1
            Me.lstSelected.Items.RemoveAt(iIndex)
        End If

    End Sub

    Private Sub frmClientProfiles_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "")
        FormLRSCaptions(Me)
    End Sub
    Public Function AnythingChanged() As Boolean
        AnythingChanged = bAnythingChanged
    End Function
    Public Function ResetAnythingChanged() As Boolean
        bAnythingChanged = False
    End Function
End Class
