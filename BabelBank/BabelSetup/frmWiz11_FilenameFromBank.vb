Option Strict Off
Option Explicit On

Friend Class frmWiz11_FilenameFromBank
    Inherits System.Windows.Forms.Form
    Private frmFilter As New frmFilter
    Private frmSpecial As New frmSpecial
    Private frmMatch As New frmMatch
	
	Private Sub cmdFilter_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFilter.Click
		VB6.ShowForm(frmFilter, 1, Me)
	End Sub
	
	Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
        ' Show Helptopic
		'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(113))
		
	End Sub
	Private Sub frmWiz11_FilenameFromBank_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        FormvbStyle(Me, "dollar.jpg", 550)
        FormLRSCaptions(Me)

        ' 28.12.2010 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next
    End Sub
    Public Sub SetfrmFilter_11(ByVal frm As frmFilter)
        frmFilter = frm
    End Sub
    Private Sub cmdSpecial_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSpecial.Click
        ' Load text into frmSpecial
        frmSpecial.Text = Mid(LRS(60137), 2)
        frmSpecial.lblSpecial.Text = Mid(LRS(60137), 2)
        frmSpecial.cmdCancel.Text = LRS(55002)
        frmSpecial.txtSpecial.Text = Trim(oFilesetup.TreatSpecial & Space(100))

        VB6.ShowForm(frmSpecial, 1, Me)
    End Sub
    Private Sub cmdMatch_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdMatch.Click
        ' Load text into frmMatch
        frmMatch.Text = LRS(60157)
        frmMatch.chkAutoMatch.Text = LRS(60158) ' Automatic matching
        frmMatch.chkMatchOCR.Text = LRS(60163) ' Match OCR
        frmMatch.cmdCancel.Text = LRS(55002)
        frmMatch.chkAutoMatch.CheckState = bVal(oFilesetup.AutoMatch)
        frmMatch.chkMatchOCR.CheckState = bVal(oFilesetup.MatchOCR)
        frmMatch.chkValidate.CheckState = bVal(oFilesetup.ValidateFormatAtImport)
        '05.01.2022 - Added
        frmMatch.Company_ID(1) 'Hardcoded Company_ID
        frmMatch.FileSetup_ID(oFilesetup.FileSetup_ID)

        VB6.ShowForm(frmMatch, 1, Me)
    End Sub
	
	Private Sub CmdBack_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdBack.Click
		Me.Hide()
		WizardMove((-1))
	End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
		WizardMove((-99))
	End Sub
	Private Sub cmdFileOpen1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen1.Click
		' Hent filnavn
		Dim spath As String
		Dim sFile As String
		' Keep filename after Browsing for folder;
		' Must have \ and .
		If InStr(Me.txtFilename1.Text, "\") > 0 And InStr(Me.txtFilename1.Text, ".") > 0 Then
			sFile = Mid(Me.txtFilename1.Text, InStrRev(Me.txtFilename1.Text, "\"))
		ElseIf InStr(Me.txtFilename1.Text, ".") > 0 Then 
			' . only, keep all;
			sFile = "\" & Me.txtFilename1.Text
		Else
			sFile = ""
		End If
		
		' Use BrowseForFolders, routine from VBNet ( i Utils)
		'spath = FolderPicker(Me, LRS(60010))   'Velg fil")
		' spath ends with end of string - remove
		'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)
		
		' Changed 18.10.05
		' using a new function where we can pass a path
        'spath = FixPath((Me.txtFilename1.Text))
        '31.12.2010
        spath = BrowseForFilesOrFolders(Me.txtFilename1.Text, Me, LRS(60010), True)
		
		If Len(spath) > 0 Then
			' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
			If InStr(spath, ".") = 0 Then
				Me.txtFilename1.Text = Replace(spath & sFile, "\\", "\")
			Else
				' Possibly file selected
				Me.txtFilename1.Text = spath
			End If
		End If
		
	End Sub
	Private Sub cmdFileOpen2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen2.Click
		' Hent filnavn
		Dim spath As String
		Dim sFile As String
		' Keep filename after Browsing for folder;
		' Must have \ and .
		If InStr(Me.txtFilename2.Text, "\") > 0 And InStr(Me.txtFilename2.Text, ".") > 0 Then
			sFile = Mid(Me.txtFilename2.Text, InStrRev(Me.txtFilename2.Text, "\"))
		ElseIf InStr(Me.txtFilename2.Text, ".") > 0 Then 
			' . only, keep all;
			sFile = "\" & Me.txtFilename2.Text
		Else
			sFile = ""
		End If
		
		' Use BrowseForFolders, routine from VBNet ( i Utils)
		'spath = FolderPicker(Me, LRS(60010))   'Velg fil")
		' spath ends with end of string - remove
		'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)
		
		' Changed 18.10.05
		' using a new function where we can pass a path
        'spath = FixPath((Me.txtFilename2.Text))
        '31.12.2010
        spath = BrowseForFilesOrFolders(Me.txtFilename2.Text, Me, LRS(60010), True)
		
		If Len(spath) > 0 Then
			' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
			If InStr(spath, ".") = 0 Then
				Me.txtFilename2.Text = Replace(spath & sFile, "\\", "\")
			Else
				' Possibly file selected
				Me.txtFilename2.Text = spath
			End If
		End If
		
	End Sub
	Private Sub cmdFileOpen3_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen3.Click
		' Hent filnavn
		Dim spath As String
		Dim sFile As String
		' Keep filename after Browsing for folder;
		' Must have \ and .
		If InStr(Me.txtFilename3.Text, "\") > 0 And InStr(Me.txtFilename3.Text, ".") > 0 Then
			sFile = Mid(Me.txtFilename3.Text, InStrRev(Me.txtFilename3.Text, "\"))
		ElseIf InStr(Me.txtFilename3.Text, ".") > 0 Then 
			' . only, keep all;
			sFile = "\" & Me.txtFilename3.Text
		Else
			sFile = ""
		End If
		
		' Use BrowseForFolders, routine from VBNet ( i Utils)
		'spath = FolderPicker(Me, LRS(60010))   'Velg fil")
		' spath ends with end of string - remove
		'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)
		
		' Changed 18.10.05
		' using a new function where we can pass a path
        'spath = FixPath((Me.txtFilename3.Text))
        '31.12.2010
        spath = BrowseForFilesOrFolders(Me.txtFilename3.Text, Me, LRS(60010), True)
		
		If Len(spath) > 0 Then
			' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
			If InStr(spath, ".") = 0 Then
				Me.txtFilename3.Text = Replace(spath & sFile, "\\", "\")
			Else
				' Possibly file selected
				Me.txtFilename3.Text = spath
			End If
		End If
		
	End Sub
	Private Sub CmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdNext.Click
		Dim bContinue As Boolean
		bContinue = True
		
		' Either :\ or \\ or // in filename/path;
		If Len(Trim(Me.txtFilename1.Text)) > 0 Then
			If InStr(Me.txtFilename1.Text, ":\") = 0 And InStr(Me.txtFilename1.Text, "\\") = 0 And InStr(Me.txtFilename1.Text, "//") = 0 Then
				MsgBox(LRS(60193) & " " & Me.txtFilename1.Text, MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation) ' Invalid filename!
				bContinue = False
				Me.txtFilename1.Focus()
			End If
		End If
		
		' Either :\ or \\ or // in filename/path;
		If bContinue And Len(Trim(Me.txtFilename2.Text)) > 0 Then
			If InStr(Me.txtFilename2.Text, ":\") = 0 And InStr(Me.txtFilename2.Text, "\\") = 0 And InStr(Me.txtFilename2.Text, "//") = 0 Then
				MsgBox(LRS(60193) & " " & Me.txtFilename2.Text, MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation) ' Invalid filename!
				bContinue = False
				Me.txtFilename2.Focus()
			End If
		End If
		
		' Either :\ or \\ or // in filename/path;
		If bContinue And Len(Trim(Me.txtFilename3.Text)) > 0 Then
			If InStr(Me.txtFilename3.Text, ":\") = 0 And InStr(Me.txtFilename3.Text, "\\") = 0 And InStr(Me.txtFilename3.Text, "//") = 0 Then
				MsgBox(LRS(60193) & " " & Me.txtFilename2.Text, MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation) ' Invalid filename!
				bContinue = False
				Me.txtFilename3.Focus()
			End If
		End If
		If bContinue Then
			Me.Hide()
			WizardMove((0))
		End If
	End Sub
	
	Private Sub frmWiz11_FilenameFromBank_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
			cmdCancel_Click(cmdCancel, New System.EventArgs())
		End If
		eventArgs.Cancel = Cancel
	End Sub

    Private Sub cmdSecurity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSecurity.Click
        LoadfrmSecurityReturn()
    End Sub
End Class
