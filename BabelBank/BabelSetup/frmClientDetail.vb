Option Strict Off
Option Explicit On

Friend Class frmClientDetail
    Inherits System.Windows.Forms.Form

    Private bAnythingChanged As Boolean
    Private frmClientDetailISO20022 As frmClientDetailISO20022
    ' must save initial settings in case frmClientDetailISO20022 is Cancelled
    Dim bAdjustForSchemaValidation As Boolean
    Dim bAdjustToBBANNorway As Boolean = False
    Dim bAdjustToBBANSweden As Boolean = False
    Dim bAdjustToBBANDenmark As Boolean = False
    Dim bUseDifferentInvoiceAndPaymentCurrency As Boolean = False
    Dim bISOSplitOCR As Boolean = False
    Dim bISOSplitOCRWithCredits As Boolean = False
    Dim bISOSRedoFreetextToStruct As Boolean = False
    Dim bISODoNotGroup As Boolean = False
    Dim bISORemoveBICInSEPA As Boolean = False
    Dim bISOUseTransferCurrency As Boolean = False
    Public Sub PassfrmISO20022(ByVal frm As frmClientDetailISO20022)
        frmClientDetailISO20022 = frm
    End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        If bAnythingChanged Then
            If MsgBox(LRS(60011) & " " & Trim(Me.txtClientNo.Text) & " " & Me.txtClientName.Text, MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Save client ?
                ' Hide without saving
                bAnythingChanged = False
                Me.Hide()
            Else
                bAnythingChanged = True
            End If
        Else
            bAnythingChanged = False
            Me.Hide()
        End If
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdOK.Click
        Me.Hide()

    End Sub

    Sub frmClientDetail_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        FormvbStyle(Me, "dollar.jpg")
        FormLRSCaptions(Me)

        ' 11.01.2011 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(300, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next
        FormSelectAllTextboxs(Me)
        bAnythingChanged = False

    End Sub

    Private Sub frmClientDetail_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason

        If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
            Cancel = 1
            cmdCancel_Click(CmdCancel, New System.EventArgs())
        End If
        eventArgs.Cancel = Cancel
    End Sub
    Private Sub chkAddOnOCR_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkAddOnOcr.CheckStateChanged
        bAnythingChanged = True
    End Sub
    Private Sub chkAddVoucherCounter_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkAddVoucherCounter.CheckStateChanged
        bAnythingChanged = True
    End Sub
    Private Sub OptDontUse_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles OptDontUse.CheckedChanged
        If eventSender.Checked Then
            bAnythingChanged = True
        End If
    End Sub
    Private Sub optGetVNoBB_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optGetVNoBB.CheckedChanged
        If eventSender.Checked Then
            bAnythingChanged = True
        End If
    End Sub
    Private Sub optGetVNoERP_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optGetVNoERP.CheckedChanged
        If eventSender.Checked Then
            bAnythingChanged = True
        End If
    End Sub
    Private Sub txtClientNo_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtClientNo.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtClientName_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtClientName.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtCompanyNo_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCompanyNo.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtDivision_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDivision.TextChanged
        bAnythingChanged = True
    End Sub
    'UPGRADE_WARNING: Event txtVoucherNo.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtVoucherNo_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtVoucherNo.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtVoucherType_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        bAnythingChanged = True
    End Sub
    Private Sub txtVoucherNo2_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtVoucherNo2.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtVoucherType2_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtVoucherType2.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtBB_Year_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBB_Year.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtBB_Period_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBB_Period.TextChanged
        bAnythingChanged = True
    End Sub
    Public Function AnythingChanged() As Boolean
        AnythingChanged = bAnythingChanged
    End Function
    Public Sub ResetAnythingChanged()
        bAnythingChanged = False
    End Sub
    Private Sub txtMsgId_Variable_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMsgId_Variable.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtMsgId_Variable_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMsgId_Variable.KeyPress
        Dim allowedChars As String = "."
        If Char.IsDigit(e.KeyChar) = False And Char.IsControl(e.KeyChar) = False And allowedChars.IndexOf(e.KeyChar) = -1 Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtHistoryDeleteDays_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtHistoryDeleteDays.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtSocialSecurityNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSocialSecurityNo.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub chkStateBankText_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUseStateBankText.CheckedChanged
        bAnythingChanged = True
    End Sub
    Private Sub chkSEPASingle_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSEPASingle.CheckedChanged
        bAnythingChanged = True
    End Sub

    Private Sub chkSavePaymentData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSavePaymentData.CheckedChanged
        bAnythingChanged = True
    End Sub
    Private Sub chkEndToEndRefFromBabelBank_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEndToEndRefFromBabelBank.CheckedChanged
        bAnythingChanged = True
    End Sub
    Private Sub lstValidationLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstValidationLevel.SelectedIndexChanged
        bAnythingChanged = True
    End Sub
    Private Sub cmbClientGroup_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbClientGroup.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub cmbClientGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbClientGroup.SelectedIndexChanged
        bAnythingChanged = True
    End Sub


    Private Sub cmdAdvanced_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdvanced.Click
        ' must save initial settings in case frmClientDetailISO20022 is Cancelled
        bAdjustForSchemaValidation = frmClientDetailISO20022.chkAdjustToSchema.Checked
        bAdjustToBBANNorway = frmClientDetailISO20022.chkIBANToBBANNorway.Checked
        bAdjustToBBANSweden = frmClientDetailISO20022.chkIBANTOBBANSweden.Checked
        bAdjustToBBANDenmark = frmClientDetailISO20022.chkIBANToBBANDenmark.Checked
        bUseDifferentInvoiceAndPaymentCurrency = frmClientDetailISO20022.chkAllowDifferenceInCurrency.Checked
        bISOSplitOCR = frmClientDetailISO20022.chkISOSplitOCR.Checked
        bISOSplitOCRWithCredits = frmClientDetailISO20022.chkISOSplitOCRWithCredits.Checked
        bISOSRedoFreetextToStruct = frmClientDetailISO20022.chkISOSRedoFreetextToStruct.Checked
        bISODoNotGroup = frmClientDetailISO20022.chkISODoNotGroup.Checked
        bISODoNotGroup = frmClientDetailISO20022.chkISORemoveBICInSEPA.Checked
        bISOUseTransferCurrency = frmClientDetailISO20022.chkUseTransferCurrency.Checked

        frmClientDetailISO20022.ShowDialog()
        ' if not OK, then reset to initialvalues
        If frmClientDetailISO20022.DialogResult = False Then
            ' must restore initial settings because frmClientDetailISO20022 is Cancelled
            frmClientDetailISO20022.chkAdjustToSchema.Checked = bAdjustForSchemaValidation
            frmClientDetailISO20022.chkIBANToBBANNorway.Checked = bAdjustToBBANNorway
            frmClientDetailISO20022.chkIBANTOBBANSweden.Checked = bAdjustToBBANSweden
            frmClientDetailISO20022.chkIBANToBBANDenmark.Checked = bAdjustToBBANDenmark
            frmClientDetailISO20022.chkAllowDifferenceInCurrency.Checked = bUseDifferentInvoiceAndPaymentCurrency
            frmClientDetailISO20022.chkISOSplitOCR.Checked = bISOSplitOCR
            frmClientDetailISO20022.chkISOSplitOCRWithCredits.Checked = bISOSplitOCRWithCredits
            frmClientDetailISO20022.chkISOSRedoFreetextToStruct.Checked = bISOSRedoFreetextToStruct
            frmClientDetailISO20022.chkISODoNotGroup.Checked = bISODoNotGroup
            frmClientDetailISO20022.chkISORemoveBICInSEPA.Checked = bISORemoveBICInSEPA
            frmClientDetailISO20022.chkUseTransferCurrency.Checked = bISOUseTransferCurrency
        End If
        bAnythingChanged = frmClientDetailISO20022.AnythingChanged
    End Sub

	Private Sub lblCompanyNo_Click(sender As Object, e As EventArgs) Handles lblCompanyNo.Click
		ToolTip1.Show("Text to display", Me, Me.Top + 50, 100)
	End Sub

	'Private Sub lblCompanyNo_MouseHover(sender As Object, e As EventArgs) Handles lblCompanyNo.MouseHover
	'	ToolTip1.Show("Text to display", Me, Me.Top + 50, 100)
	'End Sub

	'Private Sub lblCompanyNo_Leave(sender As Object, e As EventArgs) Handles lblCompanyNo.Leave
	'	ToolTip1.Show("", Me, Me.Top + 50, 100)
	'End Sub
End Class
