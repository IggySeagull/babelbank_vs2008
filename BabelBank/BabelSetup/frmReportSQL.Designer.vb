﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportSQL
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.txtSelect = New System.Windows.Forms.TextBox
        Me.lblSelect = New System.Windows.Forms.Label
        Me.lblWhere = New System.Windows.Forms.Label
        Me.txtWhere = New System.Windows.Forms.TextBox
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.cmdSave = New System.Windows.Forms.Button
        Me.cmdResetStandard = New System.Windows.Forms.Button
        Me.chkActivateSQL = New System.Windows.Forms.CheckBox
        Me.lblLine1 = New System.Windows.Forms.Label
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image1_0.Location = New System.Drawing.Point(45, 393)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(14, 16)
        Me._Image1_0.TabIndex = 32
        Me._Image1_0.TabStop = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(401, 393)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 31
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(579, 393)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 30
        Me.cmdCancel.TabStop = False
        Me.cmdCancel.Text = "55002 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'txtSelect
        '
        Me.txtSelect.Location = New System.Drawing.Point(161, 71)
        Me.txtSelect.Multiline = True
        Me.txtSelect.Name = "txtSelect"
        Me.txtSelect.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSelect.Size = New System.Drawing.Size(500, 175)
        Me.txtSelect.TabIndex = 34
        Me.txtSelect.Text = "SELECT .... FROM"
        '
        'lblSelect
        '
        Me.lblSelect.AutoSize = True
        Me.lblSelect.Location = New System.Drawing.Point(161, 52)
        Me.lblSelect.Name = "lblSelect"
        Me.lblSelect.Size = New System.Drawing.Size(89, 13)
        Me.lblSelect.TabIndex = 35
        Me.lblSelect.Text = "60604-SelectPart"
        '
        'lblWhere
        '
        Me.lblWhere.AutoSize = True
        Me.lblWhere.Location = New System.Drawing.Point(158, 250)
        Me.lblWhere.Name = "lblWhere"
        Me.lblWhere.Size = New System.Drawing.Size(91, 13)
        Me.lblWhere.TabIndex = 37
        Me.lblWhere.Text = "60605-WherePart"
        '
        'txtWhere
        '
        Me.txtWhere.Location = New System.Drawing.Point(158, 269)
        Me.txtWhere.Multiline = True
        Me.txtWhere.Name = "txtWhere"
        Me.txtWhere.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtWhere.Size = New System.Drawing.Size(500, 41)
        Me.txtWhere.TabIndex = 36
        Me.txtWhere.Text = "WHERE"
        '
        'lblOrderBy
        '
        Me.lblOrderBy.AutoSize = True
        Me.lblOrderBy.Location = New System.Drawing.Point(158, 313)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(85, 13)
        Me.lblOrderBy.TabIndex = 39
        Me.lblOrderBy.Text = "60606-OrderPart"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.Location = New System.Drawing.Point(158, 330)
        Me.txtOrderBy.Multiline = True
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOrderBy.Size = New System.Drawing.Size(500, 45)
        Me.txtOrderBy.TabIndex = 38
        Me.txtOrderBy.Text = "ORDER BY"
        '
        'cmdSave
        '
        Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Location = New System.Drawing.Point(488, 393)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSave.Size = New System.Drawing.Size(81, 25)
        Me.cmdSave.TabIndex = 40
        Me.cmdSave.Text = "55043 - Bruk/Lagre"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'cmdResetStandard
        '
        Me.cmdResetStandard.BackColor = System.Drawing.SystemColors.Control
        Me.cmdResetStandard.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdResetStandard.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdResetStandard.Location = New System.Drawing.Point(12, 353)
        Me.cmdResetStandard.Name = "cmdResetStandard"
        Me.cmdResetStandard.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdResetStandard.Size = New System.Drawing.Size(140, 21)
        Me.cmdResetStandard.TabIndex = 41
        Me.cmdResetStandard.TabStop = False
        Me.cmdResetStandard.Text = "60607 - Reset standard"
        Me.cmdResetStandard.UseVisualStyleBackColor = False
        '
        'chkActivateSQL
        '
        Me.chkActivateSQL.AutoSize = True
        Me.chkActivateSQL.Location = New System.Drawing.Point(12, 330)
        Me.chkActivateSQL.Name = "chkActivateSQL"
        Me.chkActivateSQL.Size = New System.Drawing.Size(158, 17)
        Me.chkActivateSQL.TabIndex = 42
        Me.chkActivateSQL.Text = "60617-Activate special SQL"
        Me.chkActivateSQL.UseVisualStyleBackColor = True
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 386)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(680, 1)
        Me.lblLine1.TabIndex = 79
        Me.lblLine1.Text = "Label1"
        '
        'frmReportSQL
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 425)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.chkActivateSQL)
        Me.Controls.Add(Me.cmdResetStandard)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.lblOrderBy)
        Me.Controls.Add(Me.txtOrderBy)
        Me.Controls.Add(Me.lblWhere)
        Me.Controls.Add(Me.txtWhere)
        Me.Controls.Add(Me.lblSelect)
        Me.Controls.Add(Me.txtSelect)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Name = "frmReportSQL"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmReportSQL"
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
    Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents txtSelect As System.Windows.Forms.TextBox
    Friend WithEvents lblSelect As System.Windows.Forms.Label
    Friend WithEvents lblWhere As System.Windows.Forms.Label
    Friend WithEvents txtWhere As System.Windows.Forms.TextBox
    Friend WithEvents lblOrderBy As System.Windows.Forms.Label
    Friend WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Public WithEvents cmdSave As System.Windows.Forms.Button
    Public WithEvents cmdResetStandard As System.Windows.Forms.Button
    Friend WithEvents chkActivateSQL As System.Windows.Forms.CheckBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
End Class
