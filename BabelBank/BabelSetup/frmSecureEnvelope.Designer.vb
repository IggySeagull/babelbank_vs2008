﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSecureEnvelope
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSecureEnvelope))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.OK_Button = New System.Windows.Forms.Button
        Me.Cancel_Button = New System.Windows.Forms.Button
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblSenderID = New System.Windows.Forms.Label
        Me.txtSenderID = New System.Windows.Forms.TextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.chkSecureEnvelope = New System.Windows.Forms.CheckBox
        Me.lblPublicKeyFile = New System.Windows.Forms.Label
        Me.txtfilenamePublicKey = New System.Windows.Forms.TextBox
        Me.cmdFileOpenPublickey = New System.Windows.Forms.Button
        Me.lblPassword = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.chkGZIP = New System.Windows.Forms.CheckBox
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(578, 306)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'lblHeading
        '
        Me.lblHeading.AutoSize = True
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.Location = New System.Drawing.Point(113, 70)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.Size = New System.Drawing.Size(191, 25)
        Me.lblHeading.TabIndex = 1
        Me.lblHeading.Text = "Secure Envelope"
        '
        'lblSenderID
        '
        Me.lblSenderID.AutoSize = True
        Me.lblSenderID.Location = New System.Drawing.Point(140, 139)
        Me.lblSenderID.Name = "lblSenderID"
        Me.lblSenderID.Size = New System.Drawing.Size(55, 13)
        Me.lblSenderID.TabIndex = 106
        Me.lblSenderID.Text = "Sender ID"
        '
        'txtSenderID
        '
        Me.txtSenderID.AcceptsReturn = True
        Me.txtSenderID.BackColor = System.Drawing.SystemColors.Window
        Me.txtSenderID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSenderID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSenderID.Location = New System.Drawing.Point(356, 135)
        Me.txtSenderID.MaxLength = 50
        Me.txtSenderID.Name = "txtSenderID"
        Me.txtSenderID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSenderID.Size = New System.Drawing.Size(277, 20)
        Me.txtSenderID.TabIndex = 3
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Location = New System.Drawing.Point(140, 167)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(70, 13)
        Me.lblDescription.TabIndex = 107
        Me.lblDescription.Text = "lblDescription"
        '
        'chkSecureEnvelope
        '
        Me.chkSecureEnvelope.AutoSize = True
        Me.chkSecureEnvelope.Location = New System.Drawing.Point(356, 109)
        Me.chkSecureEnvelope.Name = "chkSecureEnvelope"
        Me.chkSecureEnvelope.Size = New System.Drawing.Size(105, 17)
        Me.chkSecureEnvelope.TabIndex = 1
        Me.chkSecureEnvelope.Text = "SecureEnvelope"
        Me.chkSecureEnvelope.UseVisualStyleBackColor = True
        '
        'lblPublicKeyFile
        '
        Me.lblPublicKeyFile.AutoSize = True
        Me.lblPublicKeyFile.Location = New System.Drawing.Point(140, 235)
        Me.lblPublicKeyFile.Name = "lblPublicKeyFile"
        Me.lblPublicKeyFile.Size = New System.Drawing.Size(210, 13)
        Me.lblPublicKeyFile.TabIndex = 111
        Me.lblPublicKeyFile.Text = "60681-If certicate from file, specify filename"
        '
        'txtfilenamePublicKey
        '
        Me.txtfilenamePublicKey.AcceptsReturn = True
        Me.txtfilenamePublicKey.BackColor = System.Drawing.SystemColors.Window
        Me.txtfilenamePublicKey.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtfilenamePublicKey.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtfilenamePublicKey.Location = New System.Drawing.Point(356, 235)
        Me.txtfilenamePublicKey.MaxLength = 250
        Me.txtfilenamePublicKey.Name = "txtfilenamePublicKey"
        Me.txtfilenamePublicKey.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtfilenamePublicKey.Size = New System.Drawing.Size(277, 20)
        Me.txtfilenamePublicKey.TabIndex = 4
        '
        'cmdFileOpenPublickey
        '
        Me.cmdFileOpenPublickey.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpenPublickey.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpenPublickey.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpenPublickey.Image = CType(resources.GetObject("cmdFileOpenPublickey.Image"), System.Drawing.Image)
        Me.cmdFileOpenPublickey.Location = New System.Drawing.Point(639, 231)
        Me.cmdFileOpenPublickey.Name = "cmdFileOpenPublickey"
        Me.cmdFileOpenPublickey.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpenPublickey.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpenPublickey.TabIndex = 110
        Me.cmdFileOpenPublickey.TabStop = False
        Me.cmdFileOpenPublickey.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpenPublickey.UseVisualStyleBackColor = False
        '
        'lblPassword
        '
        Me.lblPassword.AutoSize = True
        Me.lblPassword.Location = New System.Drawing.Point(140, 269)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(141, 13)
        Me.lblPassword.TabIndex = 113
        Me.lblPassword.Text = "60664-Private key password"
        '
        'txtPassword
        '
        Me.txtPassword.AcceptsReturn = True
        Me.txtPassword.BackColor = System.Drawing.SystemColors.Window
        Me.txtPassword.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPassword.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPassword.Location = New System.Drawing.Point(356, 267)
        Me.txtPassword.MaxLength = 50
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPassword.Size = New System.Drawing.Size(277, 20)
        Me.txtPassword.TabIndex = 5
        '
        'chkGZIP
        '
        Me.chkGZIP.AutoSize = True
        Me.chkGZIP.Location = New System.Drawing.Point(489, 109)
        Me.chkGZIP.Name = "chkGZIP"
        Me.chkGZIP.Size = New System.Drawing.Size(51, 17)
        Me.chkGZIP.TabIndex = 2
        Me.chkGZIP.Text = "GZIP"
        Me.chkGZIP.UseVisualStyleBackColor = True
        '
        'frmSecureEnvelope
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(736, 347)
        Me.Controls.Add(Me.chkGZIP)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.lblPublicKeyFile)
        Me.Controls.Add(Me.txtfilenamePublicKey)
        Me.Controls.Add(Me.cmdFileOpenPublickey)
        Me.Controls.Add(Me.chkSecureEnvelope)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.lblSenderID)
        Me.Controls.Add(Me.txtSenderID)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSecureEnvelope"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmSecureEnvelope"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents lblHeading As System.Windows.Forms.Label
    Friend WithEvents lblSenderID As System.Windows.Forms.Label
    Public WithEvents txtSenderID As System.Windows.Forms.TextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents chkSecureEnvelope As System.Windows.Forms.CheckBox
    Friend WithEvents lblPublicKeyFile As System.Windows.Forms.Label
    Public WithEvents txtfilenamePublicKey As System.Windows.Forms.TextBox
    Public WithEvents cmdFileOpenPublickey As System.Windows.Forms.Button
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Public WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents chkGZIP As System.Windows.Forms.CheckBox

End Class
