Option Strict Off
Option Explicit On
Friend Class frmCustomized
	Inherits System.Windows.Forms.Form
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        oProfile.Custom1Text = Me.txtCustom1.Text
        oProfile.Custom2Text = Me.txtCustom2.Text
        oProfile.Custom3Text = Me.txtCustom3.Text
        oProfile.Custom4Text = Me.txtCustom4.Text
        oProfile.InvoiceDescText = Me.txtInvoiceDescription.Text
        oProfile.GeneralNoteText = Me.txtGeneralNote.Text
        Me.Hide()
    End Sub
    Private Sub frmCustomized_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "")
        FormLRSCaptions(Me)
    End Sub
End Class
