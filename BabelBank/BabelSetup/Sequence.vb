Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("Sequence_NET.Sequence")> Public Class Sequence
    Private oProfile As vbbabel.Profile
    Private iFileSetup_ID As Short ' Used If called from wizard, otherwise 0
    Private bCalledFromWizard As Boolean
    Private Shared frmsequence As frmSequence
    '********* START PROPERTY SETTINGS ***********************
    Public WriteOnly Property Filesetup_ID() As Short
        Set(ByVal Value As Short)
            iFileSetup_ID = Value
        End Set
    End Property
    Public Property Profile() As vbbabel.Profile
        Get
            Profile = oProfile
        End Get
        Set(ByVal Value As vbbabel.Profile)
            oProfile = Value
        End Set
    End Property
    '********* END PROPERTY SETTINGS ***********************

    'UPGRADE_NOTE: class_initialize was upgraded to class_initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Public Sub Class_Initialize_Renamed()
        ' When Setup is called, the code attempts
        ' to find a local satellite DLL that will contain all the
        ' resources.
        'If Not (LoadLocalizedResources("Setup")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
        'End If
        If RunTime() Then
            'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'App.HelpFile = My.Application.Info.DirectoryPath & "\babelbank_no.hlp"
        Else
            'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'App.HelpFile = "c:\projects\babelbank\babelbank_no.hlp"
        End If

        iFileSetup_ID = 0
    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
    'UPGRADE_NOTE: class_terminate was upgraded to class_terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    'Public Sub Class_Terminate_Renamed()
    '    'UPGRADE_NOTE: Object oProfile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '    oProfile = Nothing
    'End Sub
    Protected Overrides Sub Finalize()
        'Class_Terminate_Renamed()
        '24.07.2019
        oProfile = Nothing
        MyBase.Finalize()
    End Sub
    Public Function Start() As Boolean
        Dim bStatus As Boolean
        ' The rest of the sequence-stuff is handled in bas-modules - moved 15.05.2010
        'UPGRADE_WARNING: Couldn't resolve default property of object SequenceSetup.SequenceChange(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        bStatus = SequenceChange(oProfile, iFileSetup_ID)

        ' return bStatus
        Start = bStatus

    End Function
    Public Function SequenceChange(ByRef oP As Object, ByRef iFSetup_ID As Short) As Boolean
        Dim oFilesetup As vbbabel.FileSetup
        Dim aSendProfiles(,) As String
        'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
        'DOTNETT REDIM Dim aSendProfiles() As String
        Dim iCounter As Short
        Dim i As Short
        Dim bFormatWithSequenceNoFound As Boolean
        Dim bStatus As Boolean

        'Dim iSeq As Integer
        'frmSequence = New frmSequence

        If iFSetup_ID <> 0 Then
            ' called from wizard
            iFileSetup_ID = iFSetup_ID
            bCalledFromWizard = True
        Else
            bCalledFromWizard = False
        End If

        oProfile = oP
        iCounter = -1
        ReDim aSendProfiles(1, 0)

        ' Language strings for controls:
        frmsequence = New frmSequence
        frmsequence.CalledFromWizard = bCalledFromWizard
        frmsequence.FileSetup_ID = iFileSetup_ID
        frmsequence.Profile = oProfile
        frmsequence.Text = LRS(60105) '"Sequencenumbers"
        frmSequence.optSequenceFromFile.Text = LRS(60106) '"Use sequencenumbers from imported file"
        frmSequence.optSequencePrFile.Text = LRS(60107) '"Seqencenumbers total pr. file"
        frmSequence.optSequencePrClient.Text = LRS(60108) '"Sequencenumbers pr. client"
        frmSequence.lblSelectProfile.Text = LRS(60109) '"Select profile"
        frmSequence.lblFileSequence.Text = LRS(60110) '"Previous sequencenumber "
        frmSequence.cmdCancel.Text = LRS(55002) '"&Cancel"

        frmSequence.gridClients.Visible = False

        If Not bCalledFromWizard Then
            ' called from BabelBankEXE, select profile

            ' Check if we have any Sendprofiles with Telepay as outformat;
            For Each oFilesetup In oProfile.FileSetups
                bFormatWithSequenceNoFound = False
                If (oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.TelepayPlus Or oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.Telepay Or oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.Telepay2 Or oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.TelepayTBIO Or oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.PAYMUL Or oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.PAYMULAutack) And oFilesetup.FileNameOut1 <> "" And oFilesetup.FromAccountingSystem = False Then
                    bFormatWithSequenceNoFound = True
                ElseIf oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.PINK_LAAN And oFilesetup.FileNameOut1 <> "" Then
                    bFormatWithSequenceNoFound = True
                    frmsequence.txtSequenceNumber.MaxLength = 8
                    frmsequence.txtSequenceNumber.Width = VB6.TwipsToPixelsX(1000)
                ElseIf oFilesetup.TreatSpecial = "UTLEIEMEGLEREN" Then
                    bFormatWithSequenceNoFound = True
                End If
                If bFormatWithSequenceNoFound Then
                    iCounter = iCounter + 1
                    ReDim Preserve aSendProfiles(1, iCounter)
                    aSendProfiles(0, iCounter) = oFilesetup.ShortName ' swap later on.., this is returnprofilename
                    aSendProfiles(1, iCounter) = oFilesetup.FileSetupOut
                    'If iCounter = 0 Then
                    '   iFileSetup_ID = oFilesetup.FileSetup_ID
                    'End If
                End If
            Next oFilesetup

            If iCounter > -1 Then
                ' Swap from returnprofilename to sendprofilename
                For i = 0 To UBound(aSendProfiles, 2)
                    oFilesetup = oProfile.FileSetups(Val(aSendProfiles(1, i)))
                    aSendProfiles(0, i) = oFilesetup.ShortName
                Next i
            End If

            If iCounter < 0 Then
                MsgBox(LRS(60111), MsgBoxStyle.OkOnly + MsgBoxStyle.Information) '"No profiles with sequencenumbers found!"
            Else
                PrepareSequenceSpread()
                frmSequence.lstProfiles.Items.Clear()
                For i = 0 To iCounter
                    'UPGRADE_WARNING: Couldn't resolve default property of object aSendProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    frmSequence.lstProfiles.Items.Add(aSendProfiles(0, i))
                Next i
                If iCounter > 0 Then
                    ' User must choose sendprofile from listbox;
                    frmSequence.lstProfiles.Enabled = True
                    frmSequence.bFormLoaded = True

                Else
                    ' Only one Telepayformat involved
                    frmSequence.lstProfiles.Enabled = False
                    'frmSequence.optSequenceFromFile.Enabled = True
                    'frmSequence.optSequencePrFile.Enabled = True
                    'frmSequence.optSequencePrClient.Enabled = True
                    frmSequence.lblFileSequence.Enabled = True
                    frmSequence.txtSequenceNumber.Enabled = True
                    frmSequence.cmdOK.Enabled = True
                    'iFileSetup_ID = oFilesetup.Filesetup_ID
                    'frmsequence.FillSpreadSequence()

                End If
            End If
        Else
            PrepareSequenceSpread()
            frmSequence.lstProfiles.Visible = False
            frmSequence.lblSelectProfile.Visible = False
            'frmSequence.optSequenceFromFile.Enabled = True
            'frmSequence.optSequencePrFile.Enabled = True
            'frmSequence.optSequencePrClient.Enabled = True
            'frmSequence.lblFileSequence.Enabled = True
            'frmSequence.txtSequenceNumber.Enabled = True
            frmSequence.cmdOK.Enabled = True
            frmsequence.FillSpreadSequence()

            'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oFilesetup = oProfile.FileSetups(iFileSetup_ID)
        End If

        If iCounter > -1 Or bCalledFromWizard Then
            If Not bCalledFromWizard Then
                If frmsequence.lstProfiles.Items.Count > 0 Then
                    frmsequence.lstProfiles.SelectedIndex = 0
                End If
            End If
            'frmSequence.bFormLoaded = False

            frmsequence.ShowDialog()

            ' save if OK is pressed;
            'If bStatus Or bCalledFromWizard Then
            'If bStatus Then ' Added by JanP 07.02.03
            'frmsequence.saveprofileseq()
            'End If
            '  check status of frmSequence. OK = True
            bStatus = frmsequence.Status
            frmsequence.Close()

            'End If
        Else
            bStatus = False
        End If

        'UPGRADE_WARNING: Couldn't resolve default property of object SequenceChange. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        SequenceChange = bStatus

    End Function
    
    Private Function PrepareSequenceSpread() As Boolean
        Dim txtColumn As DataGridViewTextBoxColumn

        With frmsequence.gridClients
            .ScrollBars = ScrollBars.Vertical
            '.SelectionMode = DataGridViewSelectionMode.CellSelect
            ' not possible to add rows manually!
            .AllowUserToAddRows = False
            .RowHeadersVisible = False
            '.MultiSelect = False
            .BorderStyle = BorderStyle.None
            .BackgroundColor = Color.White

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60028) '"Clientname"
            txtColumn.Width = WidthFromSpreadToGrid(14)
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60105) '"SequenceNumber"
            txtColumn.Width = WidthFromSpreadToGrid(8)
            .Columns.Add(txtColumn)

        End With

    End Function

    
    
End Class