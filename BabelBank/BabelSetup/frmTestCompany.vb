Option Strict Off
Option Explicit On
Friend Class frmTestCompany
    Inherits System.Windows.Forms.Form

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        Me.Hide()
    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.Hide()
        ' Update from frmTestCompany:
        oProfile.RunTempCode = CBool(Me.chkRunTempCode.CheckState)
        oProfile.DebugCode = Val(Me.txtDebugCode.Text)

    End Sub
    Private Sub frmTestCompany_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg", , 60)
        FormLRSCaptions(Me)
        FormSelectAllTextboxs(Me)

    End Sub

    'UPGRADE_WARNING: Event txtDebugCode.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtDebugCode_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDebugCode.TextChanged
        ' Numerics only
        If Not IsNumeric(txtDebugCode.Text) Then
            txtDebugCode.Text = ""
        End If
    End Sub
End Class
