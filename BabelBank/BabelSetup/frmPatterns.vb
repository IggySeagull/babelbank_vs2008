Option Strict Off
Option Explicit On
Friend Class frmPatterns
    Inherits System.Windows.Forms.Form
    Private bAnythingChanged As Boolean = False
    Private bRemoved As Boolean = False
    Private iDBProfile_ID As Integer
    Private iClient_ID As Integer
    Private iPatternGroup_ID As Integer
    Private lastMousePosition As Point
    Private listitem As New _MyListBoxItem
    Private Sub ChkKID_Click()

        bAnythingChanged = True

    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click

        If bAnythingChanged Then
            If MsgBox(LRS(60011) & " " & Trim(Me.txtPattern.Text), MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then
                ' Hide without saving
                bAnythingChanged = False
                bRemoved = False
                Me.Hide()
            End If
        Else
            bAnythingChanged = False
            bRemoved = False
            Me.Hide()
        End If
    End Sub

    Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
        'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(120))
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdOK.Click
        Me.Hide()
    End Sub

    Private Sub cmdRemovePattern_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRemovePattern.Click
        RemovePattern()
        bRemoved = True
        bAnythingChanged = True
    End Sub
    Public Function PatternRemoved() As Boolean
        PatternRemoved = bRemoved
    End Function
    Sub frmPatterns_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        FormvbStyle(Me, "dollar.jpg")
        FormLRSCaptions(Me)

        For Each ctl In Me.Controls
            If ctl.Name = "Background" Then
                Me.lblHeading.BackColor = Color.Transparent
                Me.lblHeading.Parent = ctl
            End If
        Next
        ' 11.01.2011 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next

        If Format(Now(), "yyyyMMdd") = "20181010" Then
            MsgBox("Ferdig med styling")
        End If

        FormSelectAllTextboxs(Me)
        bAnythingChanged = False

        If Format(Now(), "yyyyMMdd") = "20181010" Then
            MsgBox("Ferdig med loading.")
        End If


    End Sub

    Private Sub frmPatterns_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        If UnloadMode = CloseReason.UserClosing Then
            ' user pressed red x
            Cancel = 1
            cmdCancel_Click(CmdCancel, New System.EventArgs())
        End If
        eventArgs.Cancel = Cancel
    End Sub

    'UPGRADE_WARNING: Event lstPatterns.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub lstPatterns_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstPatterns.SelectedIndexChanged

        Me.cmdRemovePattern.Enabled = True

    End Sub

    'UPGRADE_WARNING: ListBox event lstPatterns.ItemCheck has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
    Private Sub lstPatterns_ItemCheck(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.ItemCheckEventArgs) Handles lstPatterns.ItemCheck

        bAnythingChanged = True

    End Sub

    'UPGRADE_WARNING: Event txtPattern.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtPattern_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPattern.TextChanged
        Me.cmdRemovePattern.Enabled = False

        bAnythingChanged = True
    End Sub
    Private Sub cmdAddEasyPattern_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAddEasyPattern.Click
        ' Add Pattern only by setting Pattern description;

        With Me
            If Len(Trim(.txtPattern.Text)) > 0 Then
                '.lstPatterns.Items.Insert(0, .txtPattern.Text)
                ' 12.07.2019
                listitem = New _MyListBoxItem
                listitem.ItemString = txtPattern.Text
                .lstPatterns.Items.Add(listitem)
                .lstPatterns.SelectedIndex = 0
                .txtPattern.Text = ""
                .txtPattern.SelectionStart = 0
                .cmdRemovePattern.Enabled = True
                .txtPattern.Focus()

                bAnythingChanged = True
            End If
        End With

    End Sub

    Private Sub txtPattern_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtPattern.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' IF Enter in txtPattern, then assume that user wants to add new Pattern
            cmdAddEasyPattern_Click(cmdAddEasyPattern, New System.EventArgs())
        End If
    End Sub
    Public Function AnythingChanged() As Boolean
        AnythingChanged = bAnythingChanged
    End Function
    Public Sub SetDBProfileID(ByVal i As Integer)
        iDBProfile_ID = i
    End Sub
    Public Sub SetClient_ID(ByVal i As Integer)
        iClient_ID = i
    End Sub
    Public Sub SetPatternGroup_ID(ByVal i As Integer)
        iPatternGroup_ID = i
    End Sub
    Private Sub RemovePattern()
        ' Delete a Pattern from list
        Dim sMySQL As String
        Dim sCompany_ID As String
        Dim iPattern_ID As Integer
        Dim oMyDAL As vbBabel.DAL = Nothing
        sCompany_ID = "1" 'TODO: Hardcoded Company_ID

        With Me
            If .lstPatterns.SelectedIndex > -1 Then
                If oMyDAL Is Nothing Then
                    oMyDAL = New vbBabel.DAL
                    oMyDAL.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                    If Not oMyDAL.ConnectToDB() Then
                        Throw New System.Exception(oMyDAL.ErrorMessage)
                    End If
                End If
                'If MsgBox(LRS(60381) & " " & VB6.GetItemString(.lstPatterns, .lstPatterns.SelectedIndex) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett konto ?
                ' 12.07.2019
                If MsgBox(LRS(60381) & " " & .lstPatterns.Items(.lstPatterns.SelectedIndex).itemstring & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Slett konto ?
                    iPattern_ID = .lstPatterns.Items(.lstPatterns.SelectedIndex).itemdata

                    ' For newly added Patterns, we have no records in database, do not run DELETE
                    If iPattern_ID > 0 Then
                        sMySQL = "DELETE FROM Pattern "
                        sMySQL = sMySQL & "WHERE Company_ID = " & sCompany_ID & " AND "
                        sMySQL = sMySQL & "DBProfile_ID = " & iDBProfile_ID.ToString & " AND "
                        sMySQL = sMySQL & "Client_ID = " & iClient_ID.ToString & " AND "
                        sMySQL = sMySQL & "PatternGroup_ID = " & iPatternGroup_ID.ToString & " AND "
                        sMySQL = sMySQL & "Pattern_ID = " & iPattern_ID.ToString

                        oMyDAL.SQL = sMySQL

                        If oMyDAL.ExecuteNonQuery Then
                            If oMyDAL.RecordsAffected = 1 Then
                                'OK
                            Else
                                Throw New Exception("No records affected" & vbCrLf & oMyDAL.ErrorMessage)
                            End If
                        Else
                            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDAL.ErrorMessage)
                        End If
                    End If
                    .lstPatterns.Items.RemoveAt((.lstPatterns.SelectedIndex))
                    If .lstPatterns.Items.Count > 0 Then
                        .lstPatterns.SelectedIndex = 0
                    End If

                End If
                bAnythingChanged = True
            End If
        End With

        If Not oMyDAL Is Nothing Then
            oMyDAL.Close()
            oMyDAL = Nothing
        End If

    End Sub

    'Private Sub lstPatterns_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstPatterns.SelectedValueChanged
    '    ' show tooltip
    '    ToolTip1.SetToolTip(lstPatterns, lstPatterns.Text)
    'End Sub
    Private Sub lstPatterns_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lstPatterns.MouseMove

        Dim MousePositionInClientCoords As Point = Me.lstPatterns.PointToClient(Me.MousePosition)
        'Check if the mouse has moved a reasonable distance  
        If MousePositionInClientCoords.Y > lastMousePosition.Y - 5 And MousePositionInClientCoords.Y < lastMousePosition.Y + 5 And _
            MousePositionInClientCoords.X > lastMousePosition.X - 10 And MousePositionInClientCoords.X < lastMousePosition.X + 10 Then
            'do nothing  

        Else
            'Save the current mouse position as the last mouse position  
            lastMousePosition = MousePositionInClientCoords

            Dim indexUnderTheMouse As Integer = Me.lstPatterns.IndexFromPoint(MousePositionInClientCoords)
            If indexUnderTheMouse > -1 Then
                Dim s As String = Me.lstPatterns.Items(indexUnderTheMouse).ToString
                ' Count no of # 
                s = s & " = " & (Len(s) - Len(s.Replace("#", ""))).ToString & " #"
                Dim g As Graphics = Me.lstPatterns.CreateGraphics
                'If g.MeasureString(s, Me.lstPatterns.Font).Width > Me.lstPatterns.ClientRectangle.Width Then
                'Me.ToolTip1.SetToolTip(Me.lstPatterns, s)
                'Else
                Me.ToolTip1.SetToolTip(Me.lstPatterns, s)
                'End If
                g.Dispose()
            End If
        End If
    End Sub
End Class
