﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDNB_TBIW_MailPrint
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDNB_TBIW_MailPrint))
        Me.lblLine2 = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.lstDnBNOR = New System.Windows.Forms.CheckedListBox
        Me.cmdFileOpenTemplate = New System.Windows.Forms.Button
        Me.txtWordTemplate = New System.Windows.Forms.TextBox
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.txtMailAddresses = New System.Windows.Forms.TextBox
        Me.optAll = New System.Windows.Forms.RadioButton
        Me.opt140 = New System.Windows.Forms.RadioButton
        Me.cmdEMail = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.chkPrint = New System.Windows.Forms.CheckBox
        Me.chkMail = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblWordTemplate = New System.Windows.Forms.Label
        Me.lblPayType = New System.Windows.Forms.Label
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblLine2
        '
        Me.lblLine2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine2.Location = New System.Drawing.Point(15, 269)
        Me.lblLine2.Name = "lblLine2"
        Me.lblLine2.Size = New System.Drawing.Size(625, 1)
        Me.lblLine2.TabIndex = 75
        Me.lblLine2.Text = "Label1"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(163, 108)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(475, 1)
        Me.lblLine1.TabIndex = 74
        Me.lblLine1.Text = "Label1"
        '
        'lstDnBNOR
        '
        Me.lstDnBNOR.BackColor = System.Drawing.SystemColors.Window
        Me.lstDnBNOR.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstDnBNOR.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstDnBNOR.Items.AddRange(New Object() {"DnBNOR Denmark", "DnBNOR Great Britain", "DnBNOR Singapore", "DnBNOR Sweden", "DnBNOR USA"})
        Me.lstDnBNOR.Location = New System.Drawing.Point(504, 135)
        Me.lstDnBNOR.Name = "lstDnBNOR"
        Me.lstDnBNOR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstDnBNOR.Size = New System.Drawing.Size(130, 79)
        Me.lstDnBNOR.TabIndex = 71
        '
        'cmdFileOpenTemplate
        '
        Me.cmdFileOpenTemplate.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpenTemplate.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpenTemplate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpenTemplate.Image = CType(resources.GetObject("cmdFileOpenTemplate.Image"), System.Drawing.Image)
        Me.cmdFileOpenTemplate.Location = New System.Drawing.Point(492, 238)
        Me.cmdFileOpenTemplate.Name = "cmdFileOpenTemplate"
        Me.cmdFileOpenTemplate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpenTemplate.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpenTemplate.TabIndex = 70
        Me.cmdFileOpenTemplate.TabStop = False
        Me.cmdFileOpenTemplate.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpenTemplate.UseVisualStyleBackColor = False
        '
        'txtWordTemplate
        '
        Me.txtWordTemplate.AcceptsReturn = True
        Me.txtWordTemplate.BackColor = System.Drawing.SystemColors.Window
        Me.txtWordTemplate.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWordTemplate.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtWordTemplate.Location = New System.Drawing.Point(180, 240)
        Me.txtWordTemplate.MaxLength = 255
        Me.txtWordTemplate.Name = "txtWordTemplate"
        Me.txtWordTemplate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWordTemplate.Size = New System.Drawing.Size(300, 20)
        Me.txtWordTemplate.TabIndex = 69
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(5, 2)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(32, 56)
        Me._Image1_0.TabIndex = 73
        Me._Image1_0.TabStop = False
        '
        'txtMailAddresses
        '
        Me.txtMailAddresses.AcceptsReturn = True
        Me.txtMailAddresses.BackColor = System.Drawing.SystemColors.Window
        Me.txtMailAddresses.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMailAddresses.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMailAddresses.Location = New System.Drawing.Point(180, 135)
        Me.txtMailAddresses.MaxLength = 255
        Me.txtMailAddresses.Multiline = True
        Me.txtMailAddresses.Name = "txtMailAddresses"
        Me.txtMailAddresses.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMailAddresses.Size = New System.Drawing.Size(300, 79)
        Me.txtMailAddresses.TabIndex = 67
        '
        'optAll
        '
        Me.optAll.BackColor = System.Drawing.SystemColors.Control
        Me.optAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.optAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optAll.Location = New System.Drawing.Point(384, 80)
        Me.optAll.Name = "optAll"
        Me.optAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optAll.Size = New System.Drawing.Size(148, 17)
        Me.optAll.TabIndex = 66
        Me.optAll.TabStop = True
        Me.optAll.Text = "60398 - For all payments"
        Me.optAll.UseVisualStyleBackColor = False
        '
        'opt140
        '
        Me.opt140.BackColor = System.Drawing.SystemColors.Control
        Me.opt140.Cursor = System.Windows.Forms.Cursors.Default
        Me.opt140.ForeColor = System.Drawing.SystemColors.ControlText
        Me.opt140.Location = New System.Drawing.Point(181, 80)
        Me.opt140.Name = "opt140"
        Me.opt140.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.opt140.Size = New System.Drawing.Size(194, 17)
        Me.opt140.TabIndex = 65
        Me.opt140.TabStop = True
        Me.opt140.Text = "60397 - For paymentinfo > 140 char."
        Me.opt140.UseVisualStyleBackColor = False
        '
        'cmdEMail
        '
        Me.cmdEMail.BackColor = System.Drawing.SystemColors.Control
        Me.cmdEMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdEMail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdEMail.Location = New System.Drawing.Point(541, 77)
        Me.cmdEMail.Name = "cmdEMail"
        Me.cmdEMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdEMail.Size = New System.Drawing.Size(94, 21)
        Me.cmdEMail.TabIndex = 64
        Me.cmdEMail.TabStop = False
        Me.cmdEMail.Text = "60032 - EMail"
        Me.cmdEMail.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(485, 279)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 62
        Me.CmdCancel.TabStop = False
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(561, 279)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 61
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'chkPrint
        '
        Me.chkPrint.BackColor = System.Drawing.SystemColors.Control
        Me.chkPrint.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkPrint.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkPrint.Location = New System.Drawing.Point(181, 59)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkPrint.Size = New System.Drawing.Size(354, 17)
        Me.chkPrint.TabIndex = 60
        Me.chkPrint.Text = "60396 - Print paymentinfo"
        Me.chkPrint.UseVisualStyleBackColor = False
        '
        'chkMail
        '
        Me.chkMail.BackColor = System.Drawing.SystemColors.Control
        Me.chkMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkMail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkMail.Location = New System.Drawing.Point(181, 39)
        Me.chkMail.Name = "chkMail"
        Me.chkMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkMail.Size = New System.Drawing.Size(354, 17)
        Me.chkMail.TabIndex = 59
        Me.chkMail.Text = "60395 - Mail paymentinfo"
        Me.chkMail.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(501, 118)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(145, 17)
        Me.Label1.TabIndex = 72
        Me.Label1.Text = "60413 -  Mail to DnBNOR Branches"
        '
        'lblWordTemplate
        '
        Me.lblWordTemplate.BackColor = System.Drawing.SystemColors.Control
        Me.lblWordTemplate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWordTemplate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWordTemplate.Location = New System.Drawing.Point(181, 223)
        Me.lblWordTemplate.Name = "lblWordTemplate"
        Me.lblWordTemplate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWordTemplate.Size = New System.Drawing.Size(341, 17)
        Me.lblWordTemplate.TabIndex = 68
        Me.lblWordTemplate.Text = "60400 - Word-template for printfile"
        '
        'lblPayType
        '
        Me.lblPayType.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayType.Location = New System.Drawing.Point(181, 118)
        Me.lblPayType.Name = "lblPayType"
        Me.lblPayType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayType.Size = New System.Drawing.Size(301, 17)
        Me.lblPayType.TabIndex = 63
        Me.lblPayType.Text = "60399 - Mailaddress(es) separated by ;"
        '
        'frmDNB_TBIW_MailPrint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(648, 303)
        Me.Controls.Add(Me.lblLine2)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.lstDnBNOR)
        Me.Controls.Add(Me.cmdFileOpenTemplate)
        Me.Controls.Add(Me.txtWordTemplate)
        Me.Controls.Add(Me.txtMailAddresses)
        Me.Controls.Add(Me.optAll)
        Me.Controls.Add(Me.opt140)
        Me.Controls.Add(Me.cmdEMail)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.chkPrint)
        Me.Controls.Add(Me.chkMail)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblWordTemplate)
        Me.Controls.Add(Me.lblPayType)
        Me.Controls.Add(Me._Image1_0)
        Me.Name = "frmDNB_TBIW_MailPrint"
        Me.Text = "frmDNB_TBIW_MailPrint_New"
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine2 As System.Windows.Forms.Label
    Public WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents lstDnBNOR As System.Windows.Forms.CheckedListBox
    Public WithEvents cmdFileOpenTemplate As System.Windows.Forms.Button
    Public WithEvents txtWordTemplate As System.Windows.Forms.TextBox
    Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
    Public WithEvents txtMailAddresses As System.Windows.Forms.TextBox
    Public WithEvents optAll As System.Windows.Forms.RadioButton
    Public WithEvents opt140 As System.Windows.Forms.RadioButton
    Public WithEvents cmdEMail As System.Windows.Forms.Button
    Public WithEvents CmdCancel As System.Windows.Forms.Button
    Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents chkPrint As System.Windows.Forms.CheckBox
    Public WithEvents chkMail As System.Windows.Forms.CheckBox
    Public WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents lblWordTemplate As System.Windows.Forms.Label
    Public WithEvents lblPayType As System.Windows.Forms.Label
End Class
