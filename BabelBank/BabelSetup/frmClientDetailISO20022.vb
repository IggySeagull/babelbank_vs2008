﻿Imports System.Windows.Forms
' Johannes# 
Public Class frmClientDetailISO20022
    Dim bAnythingChanged As Boolean
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Hide()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        If bAnythingChanged Then
            If MsgBox(LRS(60011), MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then 'Save client ?
                ' Hide without saving
                bAnythingChanged = False
                Me.Hide()
            Else
                bAnythingChanged = True
            End If
        Else
            bAnythingChanged = False
            Me.Hide()
        End If
    End Sub
    Sub frmClientDetailISO20022_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control

        FormvbStyle(Me, "dollar.jpg")
        FormLRSCaptions(Me)

        ' 11.01.2011 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(160, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next
        FormSelectAllTextboxs(Me)
        bAnythingChanged = False

    End Sub

    Private Sub chkAdjustToSchema_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAdjustToSchema.CheckedChanged
        bAnythingChanged = True
    End Sub
    Private Sub chkIBANToBBANNorway_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIBANToBBANNorway.CheckedChanged
        bAnythingChanged = True
    End Sub
    Private Sub chkIBANToBBANNSweden_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIBANTOBBANSweden.CheckedChanged
        bAnythingChanged = True
    End Sub
    Private Sub chkIBANToBBANDenmark_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIBANToBBANDenmark.CheckedChanged
        bAnythingChanged = True
    End Sub
    Private Sub chkAllowDifferenceInCurrency_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAllowDifferenceInCurrency.CheckedChanged
        bAnythingChanged = True
    End Sub
    Public Function AnythingChanged() As Boolean
        AnythingChanged = bAnythingChanged
    End Function
    Private Sub chkISOSplitOCR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkISOSplitOCR.CheckedChanged
        bAnythingChanged = True
    End Sub

    Private Sub chkISOSplitOCRWithCredits_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkISOSplitOCRWithCredits.CheckedChanged
        bAnythingChanged = True
    End Sub

    Private Sub chkISOSRedoFreetextToStruct_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkISOSRedoFreetextToStruct.CheckedChanged
        bAnythingChanged = True
    End Sub

    Private Sub chkISODoNotGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkISODoNotGroup.CheckedChanged
        bAnythingChanged = True
    End Sub
    Private Sub chkRemoveBICInSEPA_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkISORemoveBICInSEPA.CheckedChanged
        bAnythingChanged = True
    End Sub
    Private Sub chkUseTransferCurrency_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUseTransferCurrency.CheckedChanged
        bAnythingChanged = True
    End Sub
End Class
