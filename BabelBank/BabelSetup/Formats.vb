Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("Formats_NET.Formats")> Public Class Formats
	' Return an array with all possible formats
    Public Function ReturnFormats(ByRef bPaymentOut As Boolean) As String(,)
        Dim aFormats(,) As String

        'UPGRADE_WARNING: Couldn't resolve default property of object bb_utils.ArrayFormat. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        aFormats = ArrayFormat(False, IIf(bPaymentOut, "out", "in"), True, False)
        ReturnFormats = VB6.CopyArray(aFormats)

    End Function
End Class
