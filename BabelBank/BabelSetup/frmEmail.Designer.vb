<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmEmail
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtSupport As System.Windows.Forms.TextBox
	Public WithEvents txtCCSupport As System.Windows.Forms.TextBox
    Public WithEvents txtSMTPHost As System.Windows.Forms.TextBox
	Public WithEvents txtReplyAdress As System.Windows.Forms.TextBox
	Public WithEvents txtAutoReceiver As System.Windows.Forms.TextBox
	Public WithEvents txtAutoDisplayName As System.Windows.Forms.TextBox
	Public WithEvents txtDisplayName As System.Windows.Forms.TextBox
	Public WithEvents txtAutoSender As System.Windows.Forms.TextBox
	Public WithEvents txtSender As System.Windows.Forms.TextBox
	Public WithEvents optMAPI As System.Windows.Forms.RadioButton
	Public WithEvents optSMTP As System.Windows.Forms.RadioButton
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdTest As System.Windows.Forms.Button
	Public WithEvents lblSupport As System.Windows.Forms.Label
	Public WithEvents lblCCSupport As System.Windows.Forms.Label
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents lblResult As System.Windows.Forms.Label
	Public WithEvents lblSMTPHost As System.Windows.Forms.Label
	Public WithEvents lblReplyAdress As System.Windows.Forms.Label
	Public WithEvents lblAutoReceiver As System.Windows.Forms.Label
	Public WithEvents lblAutoDisplayName As System.Windows.Forms.Label
	Public WithEvents lblDisplayName As System.Windows.Forms.Label
	Public WithEvents lblAutoSender As System.Windows.Forms.Label
	Public WithEvents lblSender As System.Windows.Forms.Label
	Public WithEvents lblDescr As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmail))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtSupport = New System.Windows.Forms.TextBox
        Me.txtCCSupport = New System.Windows.Forms.TextBox
        Me.txtSMTPHost = New System.Windows.Forms.TextBox
        Me.txtReplyAdress = New System.Windows.Forms.TextBox
        Me.txtAutoReceiver = New System.Windows.Forms.TextBox
        Me.txtAutoDisplayName = New System.Windows.Forms.TextBox
        Me.txtDisplayName = New System.Windows.Forms.TextBox
        Me.txtAutoSender = New System.Windows.Forms.TextBox
        Me.txtSender = New System.Windows.Forms.TextBox
        Me.optMAPI = New System.Windows.Forms.RadioButton
        Me.optSMTP = New System.Windows.Forms.RadioButton
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdTest = New System.Windows.Forms.Button
        Me.lblSupport = New System.Windows.Forms.Label
        Me.lblCCSupport = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblResult = New System.Windows.Forms.Label
        Me.lblSMTPHost = New System.Windows.Forms.Label
        Me.lblReplyAdress = New System.Windows.Forms.Label
        Me.lblAutoReceiver = New System.Windows.Forms.Label
        Me.lblAutoDisplayName = New System.Windows.Forms.Label
        Me.lblDisplayName = New System.Windows.Forms.Label
        Me.lblAutoSender = New System.Windows.Forms.Label
        Me.lblSender = New System.Windows.Forms.Label
        Me.lblDescr = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtSupport
        '
        Me.txtSupport.AcceptsReturn = True
        Me.txtSupport.BackColor = System.Drawing.SystemColors.Window
        Me.txtSupport.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSupport.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSupport.Location = New System.Drawing.Point(412, 222)
        Me.txtSupport.MaxLength = 0
        Me.txtSupport.Name = "txtSupport"
        Me.txtSupport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSupport.Size = New System.Drawing.Size(233, 20)
        Me.txtSupport.TabIndex = 0
        Me.txtSupport.Text = "@"
        '
        'txtCCSupport
        '
        Me.txtCCSupport.AcceptsReturn = True
        Me.txtCCSupport.BackColor = System.Drawing.SystemColors.Window
        Me.txtCCSupport.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCCSupport.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCCSupport.Location = New System.Drawing.Point(412, 254)
        Me.txtCCSupport.MaxLength = 0
        Me.txtCCSupport.Name = "txtCCSupport"
        Me.txtCCSupport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCCSupport.Size = New System.Drawing.Size(233, 20)
        Me.txtCCSupport.TabIndex = 1
        Me.txtCCSupport.Text = "@"
        '
        'txtSMTPHost
        '
        Me.txtSMTPHost.AcceptsReturn = True
        Me.txtSMTPHost.BackColor = System.Drawing.SystemColors.Window
        Me.txtSMTPHost.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSMTPHost.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSMTPHost.Location = New System.Drawing.Point(412, 472)
        Me.txtSMTPHost.MaxLength = 0
        Me.txtSMTPHost.Name = "txtSMTPHost"
        Me.txtSMTPHost.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSMTPHost.Size = New System.Drawing.Size(233, 20)
        Me.txtSMTPHost.TabIndex = 8
        '
        'txtReplyAdress
        '
        Me.txtReplyAdress.AcceptsReturn = True
        Me.txtReplyAdress.BackColor = System.Drawing.SystemColors.Window
        Me.txtReplyAdress.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtReplyAdress.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtReplyAdress.Location = New System.Drawing.Point(412, 441)
        Me.txtReplyAdress.MaxLength = 0
        Me.txtReplyAdress.Name = "txtReplyAdress"
        Me.txtReplyAdress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtReplyAdress.Size = New System.Drawing.Size(233, 20)
        Me.txtReplyAdress.TabIndex = 7
        Me.txtReplyAdress.Text = "60187 - Vennligst@ikke_svar"
        '
        'txtAutoReceiver
        '
        Me.txtAutoReceiver.AcceptsReturn = True
        Me.txtAutoReceiver.BackColor = System.Drawing.SystemColors.Window
        Me.txtAutoReceiver.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAutoReceiver.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAutoReceiver.Location = New System.Drawing.Point(412, 410)
        Me.txtAutoReceiver.MaxLength = 0
        Me.txtAutoReceiver.Name = "txtAutoReceiver"
        Me.txtAutoReceiver.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAutoReceiver.Size = New System.Drawing.Size(233, 20)
        Me.txtAutoReceiver.TabIndex = 6
        Me.txtAutoReceiver.Text = "@"
        '
        'txtAutoDisplayName
        '
        Me.txtAutoDisplayName.AcceptsReturn = True
        Me.txtAutoDisplayName.BackColor = System.Drawing.SystemColors.Window
        Me.txtAutoDisplayName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAutoDisplayName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAutoDisplayName.Location = New System.Drawing.Point(413, 378)
        Me.txtAutoDisplayName.MaxLength = 0
        Me.txtAutoDisplayName.Name = "txtAutoDisplayName"
        Me.txtAutoDisplayName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAutoDisplayName.Size = New System.Drawing.Size(233, 20)
        Me.txtAutoDisplayName.TabIndex = 5
        Me.txtAutoDisplayName.Text = "BabelBank"
        '
        'txtDisplayName
        '
        Me.txtDisplayName.AcceptsReturn = True
        Me.txtDisplayName.BackColor = System.Drawing.SystemColors.Window
        Me.txtDisplayName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDisplayName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDisplayName.Location = New System.Drawing.Point(412, 316)
        Me.txtDisplayName.MaxLength = 0
        Me.txtDisplayName.Name = "txtDisplayName"
        Me.txtDisplayName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDisplayName.Size = New System.Drawing.Size(233, 20)
        Me.txtDisplayName.TabIndex = 3
        '
        'txtAutoSender
        '
        Me.txtAutoSender.AcceptsReturn = True
        Me.txtAutoSender.BackColor = System.Drawing.SystemColors.Window
        Me.txtAutoSender.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAutoSender.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAutoSender.Location = New System.Drawing.Point(412, 347)
        Me.txtAutoSender.MaxLength = 0
        Me.txtAutoSender.Name = "txtAutoSender"
        Me.txtAutoSender.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAutoSender.Size = New System.Drawing.Size(233, 20)
        Me.txtAutoSender.TabIndex = 4
        Me.txtAutoSender.Text = "Automail@BabelBank.com"
        '
        'txtSender
        '
        Me.txtSender.AcceptsReturn = True
        Me.txtSender.BackColor = System.Drawing.SystemColors.Window
        Me.txtSender.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSender.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSender.Location = New System.Drawing.Point(412, 285)
        Me.txtSender.MaxLength = 0
        Me.txtSender.Name = "txtSender"
        Me.txtSender.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSender.Size = New System.Drawing.Size(233, 20)
        Me.txtSender.TabIndex = 2
        Me.txtSender.Text = "@"
        '
        'optMAPI
        '
        Me.optMAPI.BackColor = System.Drawing.Color.Transparent
        Me.optMAPI.Cursor = System.Windows.Forms.Cursors.Default
        Me.optMAPI.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optMAPI.Location = New System.Drawing.Point(187, 167)
        Me.optMAPI.Name = "optMAPI"
        Me.optMAPI.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optMAPI.Size = New System.Drawing.Size(457, 55)
        Me.optMAPI.TabIndex = 15
        Me.optMAPI.TabStop = True
        Me.optMAPI.Text = resources.GetString("optMAPI.Text")
        Me.optMAPI.UseVisualStyleBackColor = False
        '
        'optSMTP
        '
        Me.optSMTP.BackColor = System.Drawing.Color.Transparent
        Me.optSMTP.Checked = True
        Me.optSMTP.Cursor = System.Windows.Forms.Cursors.Default
        Me.optSMTP.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optSMTP.Location = New System.Drawing.Point(187, 122)
        Me.optSMTP.Name = "optSMTP"
        Me.optSMTP.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optSMTP.Size = New System.Drawing.Size(457, 41)
        Me.optSMTP.TabIndex = 14
        Me.optSMTP.TabStop = True
        Me.optSMTP.Text = "60179 - SMTP - Hvis mail skal kunne sendes automatisk, m� du velge SMTP. BabelBan" & _
            "k vil da opptre som en egen ""mailklient"", uavhengig av om du har installert mail" & _
            "programmer som Outlook, Notes, etc."
        Me.optSMTP.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(564, 547)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 11
        Me.cmdCancel.Text = "55002 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(392, 547)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 9
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdTest
        '
        Me.cmdTest.BackColor = System.Drawing.SystemColors.Control
        Me.cmdTest.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdTest.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdTest.Location = New System.Drawing.Point(479, 547)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdTest.Size = New System.Drawing.Size(81, 25)
        Me.cmdTest.TabIndex = 10
        Me.cmdTest.Text = "&Test"
        Me.cmdTest.UseVisualStyleBackColor = False
        '
        'lblSupport
        '
        Me.lblSupport.BackColor = System.Drawing.SystemColors.Control
        Me.lblSupport.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSupport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSupport.Location = New System.Drawing.Point(186, 224)
        Me.lblSupport.Name = "lblSupport"
        Me.lblSupport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSupport.Size = New System.Drawing.Size(217, 17)
        Me.lblSupport.TabIndex = 25
        Me.lblSupport.Text = "60294 - Mailadresse support BabelBank"
        '
        'lblCCSupport
        '
        Me.lblCCSupport.BackColor = System.Drawing.SystemColors.Control
        Me.lblCCSupport.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCCSupport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCCSupport.Location = New System.Drawing.Point(185, 255)
        Me.lblCCSupport.Name = "lblCCSupport"
        Me.lblCCSupport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCCSupport.Size = New System.Drawing.Size(217, 17)
        Me.lblCCSupport.TabIndex = 24
        Me.lblCCSupport.Text = "60295 - CC Mailadresse (kopi) support"
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(0, 0)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(32, 56)
        Me._Image1_0.TabIndex = 26
        Me._Image1_0.TabStop = False
        '
        'lblResult
        '
        Me.lblResult.BackColor = System.Drawing.SystemColors.Control
        Me.lblResult.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblResult.ForeColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(21, Byte), Integer))
        Me.lblResult.Location = New System.Drawing.Point(185, 521)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblResult.Size = New System.Drawing.Size(460, 21)
        Me.lblResult.TabIndex = 23
        Me.lblResult.Text = "lblResult"
        Me.lblResult.Visible = False
        '
        'lblSMTPHost
        '
        Me.lblSMTPHost.BackColor = System.Drawing.SystemColors.Control
        Me.lblSMTPHost.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSMTPHost.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSMTPHost.Location = New System.Drawing.Point(185, 472)
        Me.lblSMTPHost.Name = "lblSMTPHost"
        Me.lblSMTPHost.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSMTPHost.Size = New System.Drawing.Size(217, 41)
        Me.lblSMTPHost.TabIndex = 22
        Me.lblSMTPHost.Text = "60188 - SMTP Hostadresse (Hvis blank, fors�ker BabelBank � finne denne automatisk" & _
            ")"
        '
        'lblReplyAdress
        '
        Me.lblReplyAdress.BackColor = System.Drawing.SystemColors.Control
        Me.lblReplyAdress.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReplyAdress.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblReplyAdress.Location = New System.Drawing.Point(185, 441)
        Me.lblReplyAdress.Name = "lblReplyAdress"
        Me.lblReplyAdress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReplyAdress.Size = New System.Drawing.Size(217, 17)
        Me.lblReplyAdress.TabIndex = 21
        Me.lblReplyAdress.Text = "60186 - Mottaker mailadresse"
        '
        'lblAutoReceiver
        '
        Me.lblAutoReceiver.BackColor = System.Drawing.SystemColors.Control
        Me.lblAutoReceiver.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAutoReceiver.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAutoReceiver.Location = New System.Drawing.Point(185, 410)
        Me.lblAutoReceiver.Name = "lblAutoReceiver"
        Me.lblAutoReceiver.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAutoReceiver.Size = New System.Drawing.Size(217, 17)
        Me.lblAutoReceiver.TabIndex = 20
        Me.lblAutoReceiver.Text = "60185 - Mottaker av driftsmeldinger ( @ )"
        '
        'lblAutoDisplayName
        '
        Me.lblAutoDisplayName.BackColor = System.Drawing.SystemColors.Control
        Me.lblAutoDisplayName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAutoDisplayName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAutoDisplayName.Location = New System.Drawing.Point(185, 379)
        Me.lblAutoDisplayName.Name = "lblAutoDisplayName"
        Me.lblAutoDisplayName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAutoDisplayName.Size = New System.Drawing.Size(217, 17)
        Me.lblAutoDisplayName.TabIndex = 19
        Me.lblAutoDisplayName.Text = "60184 - Visningsnavn BabelBankavsender"
        '
        'lblDisplayName
        '
        Me.lblDisplayName.BackColor = System.Drawing.SystemColors.Control
        Me.lblDisplayName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDisplayName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDisplayName.Location = New System.Drawing.Point(185, 317)
        Me.lblDisplayName.Name = "lblDisplayName"
        Me.lblDisplayName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDisplayName.Size = New System.Drawing.Size(217, 17)
        Me.lblDisplayName.TabIndex = 18
        Me.lblDisplayName.Text = "60182 - Visningsnavn avsender"
        '
        'lblAutoSender
        '
        Me.lblAutoSender.BackColor = System.Drawing.SystemColors.Control
        Me.lblAutoSender.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAutoSender.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAutoSender.Location = New System.Drawing.Point(185, 348)
        Me.lblAutoSender.Name = "lblAutoSender"
        Me.lblAutoSender.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAutoSender.Size = New System.Drawing.Size(217, 17)
        Me.lblAutoSender.TabIndex = 17
        Me.lblAutoSender.Text = "60183 - Avsender BabelBank ( @ )"
        '
        'lblSender
        '
        Me.lblSender.BackColor = System.Drawing.SystemColors.Control
        Me.lblSender.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSender.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSender.Location = New System.Drawing.Point(185, 286)
        Me.lblSender.Name = "lblSender"
        Me.lblSender.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSender.Size = New System.Drawing.Size(217, 17)
        Me.lblSender.TabIndex = 16
        Me.lblSender.Text = "60181 - Mailavsender ( @ )"
        '
        'lblDescr
        '
        Me.lblDescr.BackColor = System.Drawing.Color.Transparent
        Me.lblDescr.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescr.Location = New System.Drawing.Point(188, 71)
        Me.lblDescr.Name = "lblDescr"
        Me.lblDescr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescr.Size = New System.Drawing.Size(449, 52)
        Me.lblDescr.TabIndex = 13
        Me.lblDescr.Text = resources.GetString("lblDescr.Text")
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(220, 28)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(393, 17)
        Me.lblHeading.TabIndex = 12
        Me.lblHeading.Text = "60177 E-mail setup"
        '
        'frmEmail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(658, 580)
        Me.Controls.Add(Me.txtSupport)
        Me.Controls.Add(Me.txtCCSupport)
        Me.Controls.Add(Me.txtSMTPHost)
        Me.Controls.Add(Me.txtReplyAdress)
        Me.Controls.Add(Me.txtAutoReceiver)
        Me.Controls.Add(Me.txtAutoDisplayName)
        Me.Controls.Add(Me.txtDisplayName)
        Me.Controls.Add(Me.txtAutoSender)
        Me.Controls.Add(Me.txtSender)
        Me.Controls.Add(Me.optMAPI)
        Me.Controls.Add(Me.optSMTP)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdTest)
        Me.Controls.Add(Me.lblSupport)
        Me.Controls.Add(Me.lblCCSupport)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblResult)
        Me.Controls.Add(Me.lblSMTPHost)
        Me.Controls.Add(Me.lblReplyAdress)
        Me.Controls.Add(Me.lblAutoReceiver)
        Me.Controls.Add(Me.lblAutoDisplayName)
        Me.Controls.Add(Me.lblDisplayName)
        Me.Controls.Add(Me.lblAutoSender)
        Me.Controls.Add(Me.lblSender)
        Me.Controls.Add(Me.lblDescr)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmEmail"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "EMail Setup"
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#End Region 
End Class
