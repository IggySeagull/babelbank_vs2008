Option Strict Off
Option Explicit On

Friend Class frmWiz3_FormatFromAccount
	Inherits System.Windows.Forms.Form
	
	Private Sub frmWiz3_FormatFromAccount_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        FormvbStyle(Me, "dollar.jpg")
        ' 28.12.2010 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next
    End Sub
	
	Private Sub CmdBack_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdBack.Click
		Me.Hide()
		WizardMove((-1))
	End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
		WizardMove((-99))
	End Sub
	
	Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
		Dim bOK As Boolean
		' Show Helptopic
		'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(105))
		
	End Sub
	
	Private Sub CmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdNext.Click
		Me.Hide()
		WizardMove((1))
	End Sub
	Private Sub cmdSpecialFormats_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSpecialFormats.Click
		If cmdSpecialFormats.Text = LRS(60124) Then ' Do not show specialformats
			Me.FillListbox((False))
		Else
			Me.FillListbox((True))
		End If
		Me.lstFormats.Focus()
	End Sub
	Public Sub FillListbox(ByRef bSpecialFormats As Boolean)
        Dim aFormatsAndProfiles(,) As Object ' empty array, in case we skip back and forth
		Dim i As Short
		
		' Fill array with formats, from format table
		aFormatsAndProfiles = LoadFormatsAndProfiles(True, bSpecialFormats) 'from accounting
		
		' Empty listbox, in case we skip back and forth ..
		Me.lstFormats.Items.Clear()
		' fill listbox:
		For i = 0 To UBound(aFormatsAndProfiles, 2)
			'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Me.lstFormats.Items.Add((aFormatsAndProfiles(0, i) + Space(100) + Str(aFormatsAndProfiles(1, i))))
			' if called with existing sendprofile, show informat in listbox:
			'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If aFormatsAndProfiles(cEnumID, i) = oFilesetup.Enum_ID Then
				Me.lstFormats.SelectedIndex = Me.lstFormats.Items.Count - 1
			End If
		Next i
		If bSpecialFormats Then
			' Show specialformat
			Me.cmdSpecialFormats.Text = LRS(60124) 'Do not show Standardformats
		Else
			' Current filesetup does not showspecialformat
			Me.cmdSpecialFormats.Text = LRS(60123) 'Show Specialformats
		End If
		
	End Sub
	
	Private Sub frmWiz3_FormatFromAccount_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
			cmdCancel_Click(cmdCancel, New System.EventArgs())
		End If
		eventArgs.Cancel = Cancel
	End Sub
End Class
