<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmWiz2_WhichProfile
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents optMatch As System.Windows.Forms.RadioButton
	Public WithEvents lstQuickProfile As System.Windows.Forms.ListBox
	Public WithEvents optReturn As System.Windows.Forms.RadioButton
	Public WithEvents optSend As System.Windows.Forms.RadioButton
	Public WithEvents CmdBack As System.Windows.Forms.Button
	Public WithEvents CmdNext As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdFinish As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents lblExplainMatch As System.Windows.Forms.Label
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents lblQuickprofile As System.Windows.Forms.Label
	Public WithEvents lblExplainReturn As System.Windows.Forms.Label
	Public WithEvents lblExplainSend As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    Public WithEvents Background As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.optMatch = New System.Windows.Forms.RadioButton
        Me.lstQuickProfile = New System.Windows.Forms.ListBox
        Me.optReturn = New System.Windows.Forms.RadioButton
        Me.optSend = New System.Windows.Forms.RadioButton
        Me.CmdBack = New System.Windows.Forms.Button
        Me.CmdNext = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdFinish = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.lblExplainMatch = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblQuickprofile = New System.Windows.Forms.Label
        Me.lblExplainReturn = New System.Windows.Forms.Label
        Me.lblExplainSend = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.Background = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblLine1 = New System.Windows.Forms.Label
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Background, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'optMatch
        '
        Me.optMatch.BackColor = System.Drawing.SystemColors.Window
        Me.optMatch.Cursor = System.Windows.Forms.Cursors.Default
        Me.optMatch.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optMatch.Location = New System.Drawing.Point(192, 120)
        Me.optMatch.Name = "optMatch"
        Me.optMatch.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optMatch.Size = New System.Drawing.Size(228, 17)
        Me.optMatch.TabIndex = 2
        Me.optMatch.TabStop = True
        Me.optMatch.Text = "Avstemmingsprofil"
        Me.optMatch.UseVisualStyleBackColor = False
        Me.optMatch.Visible = False
        '
        'lstQuickProfile
        '
        Me.lstQuickProfile.BackColor = System.Drawing.SystemColors.Window
        Me.lstQuickProfile.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstQuickProfile.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstQuickProfile.Location = New System.Drawing.Point(424, 72)
        Me.lstQuickProfile.Name = "lstQuickProfile"
        Me.lstQuickProfile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstQuickProfile.Size = New System.Drawing.Size(161, 56)
        Me.lstQuickProfile.TabIndex = 11
        Me.lstQuickProfile.Visible = False
        '
        'optReturn
        '
        Me.optReturn.BackColor = System.Drawing.SystemColors.Window
        Me.optReturn.Cursor = System.Windows.Forms.Cursors.Default
        Me.optReturn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optReturn.Location = New System.Drawing.Point(192, 96)
        Me.optReturn.Name = "optReturn"
        Me.optReturn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optReturn.Size = New System.Drawing.Size(241, 17)
        Me.optReturn.TabIndex = 1
        Me.optReturn.TabStop = True
        Me.optReturn.Text = "Returprofil"
        Me.optReturn.UseVisualStyleBackColor = False
        '
        'optSend
        '
        Me.optSend.BackColor = System.Drawing.SystemColors.Window
        Me.optSend.Checked = True
        Me.optSend.Cursor = System.Windows.Forms.Cursors.Default
        Me.optSend.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optSend.Location = New System.Drawing.Point(192, 72)
        Me.optSend.Name = "optSend"
        Me.optSend.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optSend.Size = New System.Drawing.Size(241, 17)
        Me.optSend.TabIndex = 0
        Me.optSend.TabStop = True
        Me.optSend.Text = "Sendeprofil"
        Me.optSend.UseVisualStyleBackColor = False
        '
        'CmdBack
        '
        Me.CmdBack.BackColor = System.Drawing.SystemColors.Control
        Me.CmdBack.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdBack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdBack.Location = New System.Drawing.Point(365, 303)
        Me.CmdBack.Name = "CmdBack"
        Me.CmdBack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdBack.Size = New System.Drawing.Size(73, 21)
        Me.CmdBack.TabIndex = 5
        Me.CmdBack.Text = "< &Forrige"
        Me.CmdBack.UseVisualStyleBackColor = False
        '
        'CmdNext
        '
        Me.CmdNext.BackColor = System.Drawing.SystemColors.Control
        Me.CmdNext.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdNext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdNext.Location = New System.Drawing.Point(440, 303)
        Me.CmdNext.Name = "CmdNext"
        Me.CmdNext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdNext.Size = New System.Drawing.Size(73, 21)
        Me.CmdNext.TabIndex = 6
        Me.CmdNext.Text = "&Neste >"
        Me.CmdNext.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(288, 303)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 4
        Me.CmdCancel.Text = "&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdFinish
        '
        Me.CmdFinish.BackColor = System.Drawing.SystemColors.Control
        Me.CmdFinish.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdFinish.Enabled = False
        Me.CmdFinish.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdFinish.Location = New System.Drawing.Point(530, 303)
        Me.CmdFinish.Name = "CmdFinish"
        Me.CmdFinish.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdFinish.Size = New System.Drawing.Size(73, 21)
        Me.CmdFinish.TabIndex = 7
        Me.CmdFinish.Text = "F&ullf�r"
        Me.CmdFinish.UseVisualStyleBackColor = False
        Me.CmdFinish.Visible = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(144, 303)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 3
        Me.CmdHelp.Text = "&Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'lblExplainMatch
        '
        Me.lblExplainMatch.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplainMatch.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplainMatch.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplainMatch.Location = New System.Drawing.Point(192, 234)
        Me.lblExplainMatch.Name = "lblExplainMatch"
        Me.lblExplainMatch.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplainMatch.Size = New System.Drawing.Size(401, 33)
        Me.lblExplainMatch.TabIndex = 13
        Me.lblExplainMatch.Text = "En avstemmingsprofil brukes for � oppdatere innbetalinger mot poster i kunderesko" & _
            "ntro."
        Me.lblExplainMatch.Visible = False
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Background.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(102, 246)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(19, 21)
        Me._Image1_0.TabIndex = 14
        Me._Image1_0.TabStop = False
        '
        'lblQuickprofile
        '
        Me.lblQuickprofile.BackColor = System.Drawing.SystemColors.Control
        Me.lblQuickprofile.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblQuickprofile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblQuickprofile.Location = New System.Drawing.Point(427, 50)
        Me.lblQuickprofile.Name = "lblQuickprofile"
        Me.lblQuickprofile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblQuickprofile.Size = New System.Drawing.Size(161, 17)
        Me.lblQuickprofile.TabIndex = 12
        Me.lblQuickprofile.Text = "Label1"
        Me.lblQuickprofile.Visible = False
        '
        'lblExplainReturn
        '
        Me.lblExplainReturn.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplainReturn.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplainReturn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplainReturn.Location = New System.Drawing.Point(192, 199)
        Me.lblExplainReturn.Name = "lblExplainReturn"
        Me.lblExplainReturn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplainReturn.Size = New System.Drawing.Size(401, 33)
        Me.lblExplainReturn.TabIndex = 10
        Me.lblExplainReturn.Text = "En returprofil benyttes for � behandle returfiler fra banken, f.eks. for � splitt" & _
            "e en returfil i flere klientfiler."
        '
        'lblExplainSend
        '
        Me.lblExplainSend.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplainSend.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplainSend.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplainSend.Location = New System.Drawing.Point(192, 152)
        Me.lblExplainSend.Name = "lblExplainSend"
        Me.lblExplainSend.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplainSend.Size = New System.Drawing.Size(401, 33)
        Me.lblExplainSend.TabIndex = 9
        Me.lblExplainSend.Text = "En sendeprofil benyttes ved innsending av filer til banken, f.eks. for � sl� samm" & _
            "en klientfiler til en sendefil."
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.Color.Transparent
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(200, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(400, 17)
        Me.lblHeading.TabIndex = 8
        Me.lblHeading.Text = "Skal du lage sendeprofil eller returprofil ?"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(14, 295)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(595, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmWiz2_WhichProfile
        '
        Me.AcceptButton = Me.CmdNext
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(622, 335)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.optMatch)
        Me.Controls.Add(Me.lstQuickProfile)
        Me.Controls.Add(Me.optReturn)
        Me.Controls.Add(Me.optSend)
        Me.Controls.Add(Me.CmdBack)
        Me.Controls.Add(Me.CmdNext)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdFinish)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.lblExplainMatch)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblQuickprofile)
        Me.Controls.Add(Me.lblExplainReturn)
        Me.Controls.Add(Me.lblExplainSend)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmWiz2_WhichProfile"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Profiltype"
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Background, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
