Option Strict Off
Option Explicit On
Friend Class frmAccountOCRSettings
    Inherits System.Windows.Forms.Form

    Private oMyDal As vbBabel.DAL
    Private bAnythingChanged As Boolean

    Private iOCRSettings_ID As Short
    Private bDontBotherHandlingKeypress As Boolean
    Private sMySQL As String
    Private iAccount_ID As Integer
    Private iClient_ID As Integer
    Public Sub SetAccount_ID(ByVal iA_Id As Integer)
        iAccount_ID = iA_Id
    End Sub
    Public Sub SetClient_ID(ByVal iC_Id As Integer)
        iClient_ID = iC_Id
    End Sub
    Private Function SaveCurrentOCRSettings(Optional ByRef bAsk As Boolean = True) As Boolean
        Dim bYes As Boolean
        Dim sCompanyID As String = "1" 'TODO: Hardcoded Company_ID
        Dim bNew As Boolean = False
        bYes = True

        If bAnythingChanged Then
            If bAsk Then
                bYes = MsgBox(LRS(60471) & " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes 'Save OCRSettings
            Else
                bYes = True
            End If
            If bYes Then

                'ha med navn p� KID, ikke KID1, KID2, KID3

                sMySQL = "SELECT Count(*) AS Occurance FROM KIDDescription WHERE Company_ID = " & sCompanyID & " AND KIDDescription_ID = " & iOCRSettings_ID.ToString & " AND Account_ID = " & iAccount_ID.ToString & " And Client_ID = " & iClient_ID.ToString
                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then
                    If oMyDal.Reader_HasRows Then
                        If oMyDal.Reader_ReadRecord Then
                            If oMyDal.Reader_GetString("Occurance") = 0 Then
                                bNew = True
                            Else
                                bNew = False
                            End If
                        Else
                            bNew = True
                        End If
                    Else
                        bNew = True
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                End If

                ' added 20.08.2015, in case we have not filled in all fields;
                If Me.txtName.Text = "" Then
                    Me.txtName.Text = "KidSetup"
                End If
                If Me.txtClientStart.Text = "" Then
                    Me.txtClientStart.Text = "-1"
                End If
                If Me.txtClientLen.Text = "" Then
                    Me.txtClientLen.Text = "-1"
                End If
                If Me.txtCustomerStart.Text = "" Then
                    Me.txtCustomerStart.Text = "-1"
                End If
                If Me.txtCustomerLen.Text = "" Then
                    Me.txtCustomerLen.Text = "-1"
                End If
                If Me.txtInvoiceStart.Text = "" Then
                    Me.txtInvoiceStart.Text = "-1"
                End If
                If Me.txtInvoiceLen.Text = "" Then
                    Me.txtInvoiceLen.Text = "-1"
                End If
                If Me.txtCDV.Text = "" Then
                    Me.txtCDV.Text = "10"
                End If
                If Me.txtKIDLen.Text = "" Then
                    Me.txtKIDLen.Text = "0"
                End If

                ' Save all OCRSettings
                If bNew Then
                    sMySQL = "INSERT INTO KIDDescription(Company_ID, Client_ID, Account_ID, KIDDescription_ID, KIDName, "
                    sMySQL = sMySQL & "CDVControl, StartPosClient, LengthClient, StartPosCustomer, LengthCustomer, StartPosInvoice, "
                    sMySQL = sMySQL & "LengthInvoice, KIDLength, KIDMask, AllowNegative) "
                    sMySQL = sMySQL & "VALUES ( " & sCompanyID & ", " & iClient_ID.ToString & ", " & iAccount_ID.ToString
                    sMySQL = sMySQL & ", " & iOCRSettings_ID.ToString & ", '" & Me.txtName.Text & "', '" & Me.txtCDV.Text
                    sMySQL = sMySQL & "', " & Trim(Me.txtClientStart.Text) & ", " & Trim(Me.txtClientLen.Text) & ", " & Trim(Me.txtCustomerStart.Text)
                    sMySQL = sMySQL & ", " & Trim(Me.txtCustomerLen.Text) & ", " & Trim(Me.txtInvoiceStart.Text) & ", " & Trim(Me.txtInvoiceLen.Text)
                    sMySQL = sMySQL & ", " & Trim(Me.txtKIDLen.Text) & ", '" & Me.txtMask.Text & "', "
                    If Me.chkNegativeAmounts.CheckState = 1 Then
                        sMySQL = sMySQL & "True"
                    Else
                        sMySQL = sMySQL & "False"
                    End If
                    sMySQL = sMySQL & ")"
                Else
                    sMySQL = "UPDATE KIDDescription SET KIDName = '" & Me.txtName.Text
                    sMySQL = sMySQL & "', CDVControl = '" & Me.txtCDV.Text & "', StartPosClient = " & Me.txtClientStart.Text
                    sMySQL = sMySQL & ", LengthClient = " & Me.txtClientLen.Text & ", StartPosCustomer = " & Me.txtCustomerStart.Text
                    sMySQL = sMySQL & ", LengthCustomer = " & Me.txtCustomerLen.Text & ", StartPosInvoice = " & Me.txtInvoiceStart.Text
                    sMySQL = sMySQL & ", LengthInvoice = " & Me.txtInvoiceLen.Text & ", KIDLength = " & Me.txtKIDLen.Text
                    sMySQL = sMySQL & ", KIDMask = '" & Me.txtMask.Text
                    If Me.chkNegativeAmounts.CheckState = 1 Then
                        sMySQL = sMySQL & "', AllowNegative = True "
                    Else
                        sMySQL = sMySQL & "', AllowNegative = False "
                    End If
                    sMySQL = sMySQL & "WHERE Company_ID = " & sCompanyID & " And KIDDescription_ID = " & iOCRSettings_ID.ToString & " And Account_ID = " & iAccount_ID.ToString & " And Client_ID = " & iClient_ID.ToString
                End If

                oMyDal.SQL = sMySQL
                If oMyDal.ExecuteNonQuery Then
                    If oMyDal.RecordsAffected = 1 Then
                        'OK
                    Else
                        Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

            End If ' YesNo to save client?
        End If ' bAnythingChanged?

        If bYes Then
            bAnythingChanged = False
        End If
        SaveCurrentOCRSettings = bYes

    End Function
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        Dim bOKToEnd As Boolean

        bOKToEnd = True
        If bAnythingChanged Then
            ' Discard all changes
            'oMyDal.TEST_Rollback() 'Kjell: Are we sure that we have a BeginTrans here (f.ex. when delete is run)
        End If

        Me.Close()
    End Sub

    Private Sub cmdDelete_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDelete.Click

        If MsgBox(LRS(60511) & Str(iOCRSettings_ID) & "?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, LRS(60472)) = MsgBoxResult.Yes Then
            'Delete - TODO: Hardcoded Company_ID
            sMySQL = "DELETE FROM KIDDescription WHERE Company_ID = 1 AND KIDDescription_ID = " & iOCRSettings_ID.ToString & " AND Account_ID = " & iAccount_ID.ToString & " And Client_ID = " & iClient_ID.ToString

            oMyDal.SQL = sMySQL
            If oMyDal.ExecuteNonQuery Then
                If oMyDal.RecordsAffected = 1 Then
                    'OK
                Else
                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If

            ' Blank all values in screen
            Me.txtName.Text = ""
            Me.txtCDV.Text = ""
            Me.txtClientStart.Text = ""
            Me.txtClientLen.Text = ""
            Me.txtCustomerStart.Text = ""
            Me.txtCustomerLen.Text = ""
            Me.txtInvoiceStart.Text = ""
            Me.txtInvoiceLen.Text = ""
            Me.txtMask.Text = ""
            'oMyDal.TEST_CommitTrans()

            Me.Close()

            bAnythingChanged = False
        End If
    End Sub

    Public Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        ' Save clientsettings to database
        '...
        '...

        If SaveCurrentOCRSettings(False) Then '
            'oMyDal.TEST_CommitTrans()

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Me.Close()
        End If

    End Sub
    Private Sub frmAccountOCRSettings_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        ' Open BabelBanks database
        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        'oMyDal.TEST_UseCommitFromOutside = True
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If
        'oMyDal.TEST_BeginTrans()

        FormvbStyle(Me, "", 400)
        FormLRSCaptions(Me)


        bAnythingChanged = False
    End Sub

    Private Sub optKID1_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optKID1.CheckedChanged
        If eventSender.Checked Then
            'SaveCurrentOCRSettings()
            EnableTextBoxes()

            iOCRSettings_ID = 1
            ' Find data for OCRSetting 1, if present;
            sMySQL = "SELECT * FROM KIDdescription WHERE Company_ID = 1 AND KIDDescription_ID = 1 AND Account_ID = " & iAccount_ID.ToString & " AND Client_ID = " & iClient_ID.ToString 'TODO: Hardcoded CompanyID

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then
                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord
                        Me.txtCDV.Text = oMyDal.Reader_GetString("CDVControl")
                        Me.txtClientStart.Text = oMyDal.Reader_GetString("StartPosClient")
                        Me.txtClientLen.Text = oMyDal.Reader_GetString("LengthClient")
                        Me.txtCustomerStart.Text = oMyDal.Reader_GetString("StartPosCustomer")
                        Me.txtCustomerLen.Text = oMyDal.Reader_GetString("LengthCustomer")
                        Me.txtInvoiceStart.Text = oMyDal.Reader_GetString("StartPosInvoice")
                        Me.txtInvoiceLen.Text = oMyDal.Reader_GetString("LengthInvoice")
                        Me.txtKIDLen.Text = oMyDal.Reader_GetString("KIDLength")
                        Me.txtName.Text = oMyDal.Reader_GetString("KIDName")
                        Me.txtMask.Text = oMyDal.Reader_GetString("KIDMask")

                        If CBool(oMyDal.Reader_GetString("AllowNegative")) = True Then
                            Me.chkNegativeAmounts.CheckState = CheckState.Checked
                        Else
                            Me.chkNegativeAmounts.CheckState = CheckState.Unchecked
                        End If

                        bAnythingChanged = False
                        Exit Do
                    Loop
                Else
                    ' blank data
                    Me.txtCDV.Text = ""
                    Me.txtClientStart.Text = ""
                    Me.txtClientLen.Text = ""
                    Me.txtCustomerStart.Text = ""
                    Me.txtCustomerLen.Text = ""
                    Me.txtInvoiceStart.Text = ""
                    Me.txtInvoiceLen.Text = ""
                    Me.txtKIDLen.Text = ""
                    Me.txtName.Text = ""
                    Me.txtMask.Text = ""
                    Me.chkNegativeAmounts.CheckState = System.Windows.Forms.CheckState.Unchecked

                    ' Added 03.10.2007
                    ' For SG Finans, we can "Pre-fill" settings here for standard 16 KID
                    ' We must first find out if this is SG Finans, and ReturfilerTilKunde, Special = "SGCLIENTREPORTING":
                    sMySQL = "SELECT TreatSpecial FROM Filesetup"
                    oMyDal.SQL = sMySQL
                    If oMyDal.Reader_Execute() Then
                        Do While oMyDal.Reader_ReadRecord
                            If oMyDal.Reader_GetString("TreatSpecial") = "SGCLIENTREPORTING" Then
                                ' Fill into form
                                Me.txtCDV.Text = "10"
                                Me.txtClientStart.Text = "2"
                                Me.txtClientLen.Text = "4"
                                Me.txtCustomerStart.Text = "0"
                                Me.txtCustomerLen.Text = "0"
                                Me.txtInvoiceStart.Text = "6"
                                Me.txtInvoiceLen.Text = "8"
                                Me.txtKIDLen.Text = "16"
                                Me.txtName.Text = "Standard KID 16"
                                Me.txtMask.Text = "9############00#"

                                Me.chkNegativeAmounts.CheckState = System.Windows.Forms.CheckState.Unchecked
                                bAnythingChanged = True
                                Exit Do

                            End If
                        Loop
                    Else
                        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                    End If

                End If
            Else
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
            End If

        End If
    End Sub
    Private Sub optKID2_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optKID2.CheckedChanged
        If eventSender.Checked Then

            SaveCurrentOCRSettings()
            EnableTextBoxes()
            iOCRSettings_ID = 2

            ' Find data for OCRSetting 1, if present;
            sMySQL = "SELECT * FROM KIDdescription WHERE Company_ID = 1 AND KIDDescription_ID = 2 AND Account_ID = " & iAccount_ID.ToString & " AND Client_ID = " & iClient_ID.ToString 'TODO: Hardcoded CompanyID

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then
                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord
                        Me.txtCDV.Text = oMyDal.Reader_GetString("CDVControl")
                        Me.txtClientStart.Text = oMyDal.Reader_GetString("StartPosClient")
                        Me.txtClientLen.Text = oMyDal.Reader_GetString("LengthClient")
                        Me.txtCustomerStart.Text = oMyDal.Reader_GetString("StartPosCustomer")
                        Me.txtCustomerLen.Text = oMyDal.Reader_GetString("LengthCustomer")
                        Me.txtInvoiceStart.Text = oMyDal.Reader_GetString("StartPosInvoice")
                        Me.txtInvoiceLen.Text = oMyDal.Reader_GetString("LengthInvoice")
                        Me.txtKIDLen.Text = oMyDal.Reader_GetString("KIDLength")
                        Me.txtName.Text = oMyDal.Reader_GetString("KIDName")
                        Me.txtMask.Text = oMyDal.Reader_GetString("KIDMask")

                        'Me.chkNegativeAmounts.CheckState = CBool(oMyDal.Reader_GetString("AllowNegative"))
                        If CBool(oMyDal.Reader_GetString("AllowNegative")) = True Then
                            Me.chkNegativeAmounts.CheckState = CheckState.Checked
                        Else
                            Me.chkNegativeAmounts.CheckState = CheckState.Unchecked
                        End If

                        bAnythingChanged = False
                        Exit Do
                    Loop
                Else
                    ' blank data
                    Me.txtCDV.Text = ""
                    Me.txtClientStart.Text = ""
                    Me.txtClientLen.Text = ""
                    Me.txtCustomerStart.Text = ""
                    Me.txtCustomerLen.Text = ""
                    Me.txtInvoiceStart.Text = ""
                    Me.txtInvoiceLen.Text = ""
                    Me.txtKIDLen.Text = ""
                    Me.txtName.Text = ""
                    Me.txtMask.Text = ""
                    Me.chkNegativeAmounts.CheckState = System.Windows.Forms.CheckState.Unchecked

                End If
            Else
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
            End If
            bAnythingChanged = False
        End If
    End Sub
    Private Sub optKID3_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optKID3.CheckedChanged
        If eventSender.Checked Then
            SaveCurrentOCRSettings()
            EnableTextBoxes()

            iOCRSettings_ID = 3
            ' Find data for OCRSetting 1, if present;
            sMySQL = "SELECT * FROM KIDdescription WHERE Company_ID = 1 AND KIDDescription_ID = 3 AND Account_ID = " & iAccount_ID.ToString & " AND Client_ID = " & iClient_ID.ToString 'TODO: Hardcoded CompanyID

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then
                If oMyDal.Reader_HasRows Then
                    Do While oMyDal.Reader_ReadRecord
                        Me.txtCDV.Text = oMyDal.Reader_GetString("CDVControl")
                        Me.txtClientStart.Text = oMyDal.Reader_GetString("StartPosClient")
                        Me.txtClientLen.Text = oMyDal.Reader_GetString("LengthClient")
                        Me.txtCustomerStart.Text = oMyDal.Reader_GetString("StartPosCustomer")
                        Me.txtCustomerLen.Text = oMyDal.Reader_GetString("LengthCustomer")
                        Me.txtInvoiceStart.Text = oMyDal.Reader_GetString("StartPosInvoice")
                        Me.txtInvoiceLen.Text = oMyDal.Reader_GetString("LengthInvoice")
                        Me.txtKIDLen.Text = oMyDal.Reader_GetString("KIDLength")
                        Me.txtName.Text = oMyDal.Reader_GetString("KIDName")
                        Me.txtMask.Text = oMyDal.Reader_GetString("KIDMask")

                        'Me.chkNegativeAmounts.CheckState = CBool(oMyDal.Reader_GetString("AllowNegative"))
                        If CBool(oMyDal.Reader_GetString("AllowNegative")) = True Then
                            Me.chkNegativeAmounts.CheckState = CheckState.Checked
                        Else
                            Me.chkNegativeAmounts.CheckState = CheckState.Unchecked
                        End If

                        bAnythingChanged = False
                        Exit Do
                    Loop
                Else
                    ' blank data
                    Me.txtCDV.Text = ""
                    Me.txtClientStart.Text = ""
                    Me.txtClientLen.Text = ""
                    Me.txtCustomerStart.Text = ""
                    Me.txtCustomerLen.Text = ""
                    Me.txtInvoiceStart.Text = ""
                    Me.txtInvoiceLen.Text = ""
                    Me.txtKIDLen.Text = ""
                    Me.txtName.Text = ""
                    Me.txtMask.Text = ""
                    Me.chkNegativeAmounts.CheckState = System.Windows.Forms.CheckState.Unchecked

                End If
            Else
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
            End If
            bAnythingChanged = False
        End If
    End Sub
    Private Sub EnableTextBoxes()
        Me.txtCDV.Enabled = True
        Me.txtClientStart.Enabled = True
        Me.txtClientLen.Enabled = True
        Me.txtCustomerStart.Enabled = True
        Me.txtCustomerLen.Enabled = True
        Me.txtInvoiceStart.Enabled = True
        Me.txtInvoiceLen.Enabled = True
        Me.txtKIDLen.Enabled = True
        Me.txtMask.Enabled = True
        Me.txtName.Enabled = True
        Me.txtMask.Enabled = True
        'Me.txtName.SetFocus

    End Sub
    Private Sub chkNegativeAmounts_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNegativeAmounts.CheckStateChanged
        bAnythingChanged = True
    End Sub

    Private Sub txtCDV_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCDV.TextChanged
        bAnythingChanged = True
    End Sub

    Private Sub txtClientLen_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtClientLen.TextChanged
        bAnythingChanged = True
    End Sub

    Private Sub txtClientStart_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtClientStart.TextChanged
        bAnythingChanged = True
    End Sub

    Private Sub txtCustomerLen_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCustomerLen.TextChanged
        bAnythingChanged = True
    End Sub

    Private Sub txtCustomerStart_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCustomerStart.TextChanged
        bAnythingChanged = True
    End Sub

    Private Sub txtMask_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMask.TextChanged
        bAnythingChanged = True
    End Sub

    Private Sub txtInvoiceLen_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtInvoiceLen.TextChanged
        bAnythingChanged = True
    End Sub

    Private Sub txtInvoiceStart_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtInvoiceStart.TextChanged
        bAnythingChanged = True
    End Sub

    Private Sub txtKIDLen_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtKIDLen.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtName_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtName.TextChanged
        bAnythingChanged = True
    End Sub

    Private Sub txtName_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtName.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtKIDLen.SelectionStart = 0
            Me.txtKIDLen.SelectionLength = Len(Me.txtClientStart.Text)
            Me.txtKIDLen.Focus()
        End If
    End Sub

    Private Sub txtKIDLen_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtKIDLen.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtClientStart.SelectionStart = 0
            Me.txtClientStart.SelectionLength = Len(Me.txtClientStart.Text)
            Me.txtClientStart.Focus()
        End If
    End Sub
    Private Sub txtClientStart_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtClientStart.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtClientLen.SelectionStart = 0
            Me.txtClientLen.SelectionLength = Len(Me.txtClientLen.Text)
            Me.txtClientLen.Focus()
        End If
    End Sub
    Private Sub txtClientLen_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtClientLen.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtCustomerStart.SelectionStart = 0
            Me.txtCustomerStart.SelectionLength = Len(Me.txtCustomerStart.Text)
            Me.txtCustomerStart.Focus()
        End If
    End Sub
    Private Sub txtCustomerStart_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtCustomerStart.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtCustomerLen.SelectionStart = 0
            Me.txtCustomerLen.SelectionLength = Len(Me.txtCustomerStart.Text)
            Me.txtCustomerLen.Focus()
        End If
    End Sub
    Private Sub txtCustomerLen_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtCustomerLen.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtInvoiceStart.SelectionStart = 0
            Me.txtInvoiceStart.SelectionLength = Len(Me.txtInvoiceStart.Text)
            Me.txtInvoiceStart.Focus()
        End If
    End Sub
    Private Sub txtInvoiceStart_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtInvoiceStart.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtInvoiceLen.SelectionStart = 0
            Me.txtInvoiceLen.SelectionLength = Len(Me.txtInvoiceStart.Text)
            Me.txtInvoiceLen.Focus()
        End If
    End Sub
    Private Sub txtInvoiceLen_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtInvoiceLen.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtCDV.SelectionStart = 0
            Me.txtCDV.SelectionLength = Len(Me.txtCDV.Text)
            Me.txtCDV.Focus()
        End If
    End Sub
    Private Sub txtcdv_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtCDV.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Then
            ' Return sets focus to next field
            Me.txtMask.SelectionStart = 0
            Me.txtMask.SelectionLength = Len(Me.txtMask.Text)
            Me.txtMask.Focus()
        End If
    End Sub
End Class
