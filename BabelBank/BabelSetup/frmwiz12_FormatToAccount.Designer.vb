<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmWiz12_FormatToAccount
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdSpecialFormats As System.Windows.Forms.Button
	Public WithEvents cmdChooseProfile As System.Windows.Forms.Button
	Public WithEvents txtFlere As System.Windows.Forms.TextBox
	Public WithEvents cmdFlere As System.Windows.Forms.Button
	Public WithEvents lstFormats As System.Windows.Forms.ListBox
	Public WithEvents CmdBack As System.Windows.Forms.Button
	Public WithEvents CmdNext As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdFinish As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents lblFlere As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdSpecialFormats = New System.Windows.Forms.Button
        Me.cmdChooseProfile = New System.Windows.Forms.Button
        Me.txtFlere = New System.Windows.Forms.TextBox
        Me.cmdFlere = New System.Windows.Forms.Button
        Me.lstFormats = New System.Windows.Forms.ListBox
        Me.CmdBack = New System.Windows.Forms.Button
        Me.CmdNext = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdFinish = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblFlere = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblLine1 = New System.Windows.Forms.Label
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdSpecialFormats
        '
        Me.cmdSpecialFormats.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSpecialFormats.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSpecialFormats.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSpecialFormats.Location = New System.Drawing.Point(208, 256)
        Me.cmdSpecialFormats.Name = "cmdSpecialFormats"
        Me.cmdSpecialFormats.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSpecialFormats.Size = New System.Drawing.Size(289, 25)
        Me.cmdSpecialFormats.TabIndex = 11
        Me.cmdSpecialFormats.Text = "Spesialformater"
        Me.cmdSpecialFormats.UseVisualStyleBackColor = False
        '
        'cmdChooseProfile
        '
        Me.cmdChooseProfile.BackColor = System.Drawing.SystemColors.Control
        Me.cmdChooseProfile.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdChooseProfile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdChooseProfile.Location = New System.Drawing.Point(512, 152)
        Me.cmdChooseProfile.Name = "cmdChooseProfile"
        Me.cmdChooseProfile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdChooseProfile.Size = New System.Drawing.Size(97, 41)
        Me.cmdChooseProfile.TabIndex = 10
        Me.cmdChooseProfile.Text = "Velg et tidligere benyttet utformat"
        Me.cmdChooseProfile.UseVisualStyleBackColor = False
        Me.cmdChooseProfile.Visible = False
        '
        'txtFlere
        '
        Me.txtFlere.AcceptsReturn = True
        Me.txtFlere.BackColor = System.Drawing.SystemColors.Window
        Me.txtFlere.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFlere.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFlere.Location = New System.Drawing.Point(576, 111)
        Me.txtFlere.MaxLength = 0
        Me.txtFlere.Name = "txtFlere"
        Me.txtFlere.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFlere.Size = New System.Drawing.Size(25, 20)
        Me.txtFlere.TabIndex = 2
        Me.txtFlere.Text = "2"
        Me.txtFlere.Visible = False
        '
        'cmdFlere
        '
        Me.cmdFlere.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFlere.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFlere.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFlere.Location = New System.Drawing.Point(512, 64)
        Me.cmdFlere.Name = "cmdFlere"
        Me.cmdFlere.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFlere.Size = New System.Drawing.Size(97, 33)
        Me.cmdFlere.TabIndex = 1
        Me.cmdFlere.Text = "&Samme format flere ganger"
        Me.cmdFlere.UseVisualStyleBackColor = False
        Me.cmdFlere.Visible = False
        '
        'lstFormats
        '
        Me.lstFormats.BackColor = System.Drawing.SystemColors.Window
        Me.lstFormats.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstFormats.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstFormats.Location = New System.Drawing.Point(208, 64)
        Me.lstFormats.Name = "lstFormats"
        Me.lstFormats.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstFormats.Size = New System.Drawing.Size(289, 173)
        Me.lstFormats.TabIndex = 0
        '
        'CmdBack
        '
        Me.CmdBack.BackColor = System.Drawing.SystemColors.Control
        Me.CmdBack.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdBack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdBack.Location = New System.Drawing.Point(365, 303)
        Me.CmdBack.Name = "CmdBack"
        Me.CmdBack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdBack.Size = New System.Drawing.Size(73, 21)
        Me.CmdBack.TabIndex = 5
        Me.CmdBack.Text = "< &Forrige"
        Me.CmdBack.UseVisualStyleBackColor = False
        '
        'CmdNext
        '
        Me.CmdNext.BackColor = System.Drawing.SystemColors.Control
        Me.CmdNext.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdNext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdNext.Location = New System.Drawing.Point(440, 303)
        Me.CmdNext.Name = "CmdNext"
        Me.CmdNext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdNext.Size = New System.Drawing.Size(73, 21)
        Me.CmdNext.TabIndex = 6
        Me.CmdNext.Text = "&Neste >"
        Me.CmdNext.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(288, 303)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 4
        Me.CmdCancel.Text = "&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdFinish
        '
        Me.CmdFinish.BackColor = System.Drawing.SystemColors.Control
        Me.CmdFinish.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdFinish.Enabled = False
        Me.CmdFinish.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdFinish.Location = New System.Drawing.Point(530, 303)
        Me.CmdFinish.Name = "CmdFinish"
        Me.CmdFinish.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdFinish.Size = New System.Drawing.Size(73, 21)
        Me.CmdFinish.TabIndex = 7
        Me.CmdFinish.Text = "F&ullf�r"
        Me.CmdFinish.UseVisualStyleBackColor = False
        Me.CmdFinish.Visible = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(144, 303)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 3
        Me.CmdHelp.Text = "&Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(174, 256)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(15, 25)
        Me._Image1_0.TabIndex = 12
        Me._Image1_0.TabStop = False
        '
        'lblFlere
        '
        Me.lblFlere.BackColor = System.Drawing.SystemColors.Control
        Me.lblFlere.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFlere.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFlere.Location = New System.Drawing.Point(517, 112)
        Me.lblFlere.Name = "lblFlere"
        Me.lblFlere.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFlere.Size = New System.Drawing.Size(49, 19)
        Me.lblFlere.TabIndex = 9
        Me.lblFlere.Text = "Antall"
        Me.lblFlere.Visible = False
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(200, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(400, 17)
        Me.lblHeading.TabIndex = 8
        Me.lblHeading.Text = "Velg format for returfil til regnskap"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(14, 295)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(595, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmWiz12_FormatToAccount
        '
        Me.AcceptButton = Me.CmdNext
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(622, 335)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdSpecialFormats)
        Me.Controls.Add(Me.cmdChooseProfile)
        Me.Controls.Add(Me.txtFlere)
        Me.Controls.Add(Me.cmdFlere)
        Me.Controls.Add(Me.lstFormats)
        Me.Controls.Add(Me.CmdBack)
        Me.Controls.Add(Me.CmdNext)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdFinish)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblFlere)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmWiz12_FormatToAccount"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Format for returfil til regnskap"
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
