Option Strict Off
Option Explicit On
Friend Class frmSignalFile
    Inherits System.Windows.Forms.Form

    'UPGRADE_WARNING: Event chkActivate.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkActivate_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkActivate.CheckStateChanged
        If chkActivate.CheckState Then
            Me.lblFilename.Enabled = True
            Me.txtFilename.Enabled = True
            Me.cmdFileOpen.Enabled = True
            Me.lblFilenameError.Enabled = True
            Me.txtFilenameError.Enabled = True
            Me.cmdFileOpenError.Enabled = True
        Else
            Me.lblFilename.Enabled = False
            Me.txtFilename.Enabled = False
            Me.cmdFileOpen.Enabled = False
            Me.lblFilenameError.Enabled = False
            Me.txtFilenameError.Enabled = False
            Me.cmdFileOpenError.Enabled = False
        End If
    End Sub

    Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
        ' Find filename
        Dim spath As String
        Dim sFile As String
        ' Keep filename after Browsing for folder;
        ' Must have \ and .
        If InStr(Me.txtFilename.Text, "\") > 0 And InStr(Me.txtFilename.Text, ".") > 0 Then
            sFile = Mid(Me.txtFilename.Text, InStrRev(Me.txtFilename.Text, "\"))
        ElseIf InStr(Me.txtFilename.Text, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & Me.txtFilename.Text
        Else
            sFile = ""
        End If

        ' Use BrowseForFolders, routine from VBNet ( i Utils)
        'spath = FolderPicker(Me, LRS(60010))   'Velg fil")
        ' spath ends with end of string - remove
        'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)

        ' Changed 18.10.05
        ' using a new function where we can pass a path
        'spath = FixPath((Me.txtFilename.Text))
        '31.12.2010
        spath = BrowseForFilesOrFolders(Me.txtFilename.Text, Me, LRS(60010), True)

        If Len(spath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(spath, ".") = 0 Then
                Me.txtFilename.Text = Replace(spath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                Me.txtFilename.Text = spath
            End If
        End If

    End Sub

    Private Sub frmSignalFile_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "")
        FormLRSCaptions(Me)
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Me.Hide()
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.Hide()
        SaveSignalFile()
    End Sub
    Sub SaveSignalFile()

        oFilesetup.UseSignalFile = CBool(Me.chkActivate.CheckState)
        oFilesetup.FilenameSignalFile = Me.txtFilename.Text
        oFilesetup.FilenameSignalFileError = Me.txtFilenameError.Text

    End Sub

    Private Sub cmdFileOpenError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFileOpenError.Click
        ' Find filename
        Dim spath As String
        Dim sFile As String
        ' Keep filename after Browsing for folder;
        ' Must have \ and .
        If InStr(Me.txtFilenameError.Text, "\") > 0 And InStr(Me.txtFilenameError.Text, ".") > 0 Then
            sFile = Mid(Me.txtFilenameError.Text, InStrRev(Me.txtFilenameError.Text, "\"))
        ElseIf InStr(Me.txtFilenameError.Text, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & Me.txtFilenameError.Text
        Else
            sFile = ""
        End If

        ' Use BrowseForFolders, routine from VBNet ( i Utils)
        'spath = FolderPicker(Me, LRS(60010))   'Velg fil")
        ' spath ends with end of string - remove
        'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)

        ' Changed 18.10.05
        ' using a new function where we can pass a path
        'spath = FixPath((Me.txtFilename.Text))
        '31.12.2010
        spath = BrowseForFilesOrFolders(Me.txtFilenameError.Text, Me, LRS(60010), True)

        If Len(spath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(spath, ".") = 0 Then
                Me.txtFilenameError.Text = Replace(spath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                Me.txtFilenameError.Text = spath
            End If
        End If

    End Sub
End Class
