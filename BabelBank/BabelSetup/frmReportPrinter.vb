Option Strict Off
Option Explicit On
Friend Class frmReportPrinter
	Inherits System.Windows.Forms.Form
	
	Private Sub frmReportPrinter_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "")
		FormLRSCaptions(Me)
	End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Me.DialogResult = 0
        Me.Hide()
	End Sub
	
	Private Sub CmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdOK.Click
        Me.DialogResult = 1
        Me.Hide()
	End Sub
End Class
