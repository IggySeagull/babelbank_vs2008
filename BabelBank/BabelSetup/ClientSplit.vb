Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("ClientSplit_NET.ClientSplit")> Public Class ClientSplit
	Private oProfile As Object
	
	'********* START PROPERTY SETTINGS ***********************
	Public Property Profile() As Object
		Get
			Profile = oProfile
		End Get
		Set(ByVal Value As Object)
			If IsReference(Value) And Not TypeOf Value Is String Then
				oProfile = Value
			Else
				oProfile = Value
			End If
		End Set
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
	'UPGRADE_NOTE: class_initialize was upgraded to class_initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Sub Class_Initialize_Renamed()
		' When Setup is called, the code attempts
		' to find a local satellite DLL that will contain all the
		' resources.
        'If Not (LoadLocalizedResources("Setup")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
        'End If
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	'UPGRADE_NOTE: class_terminate was upgraded to class_terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    'Public Sub Class_Terminate_Renamed()
    '	'UPGRADE_NOTE: Object oProfile may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '	oProfile = Nothing
    'End Sub
	Protected Overrides Sub Finalize()
        'Class_Terminate_Renamed()
        '24.07.2019
        oProfile = Nothing
		MyBase.Finalize()
	End Sub
	Public Function Start() As Boolean
		Dim bStatus As Boolean
		
		' The rest of the client-setupstuff is handled in bas-modules
        'bStatus = BabelSetup.ShowClientsOCRSplit(oProfile)
		
		' return bStatus
		Start = bStatus
		
	End Function
End Class
