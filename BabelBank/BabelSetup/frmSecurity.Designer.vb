﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSecurity
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSecurity))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.OK_Button = New System.Windows.Forms.Button
        Me.Cancel_Button = New System.Windows.Forms.Button
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.lblHash = New System.Windows.Forms.Label
        Me.lblCypher = New System.Windows.Forms.Label
        Me.txtfilenamePublicKey = New System.Windows.Forms.TextBox
        Me.cmdFileOpenPublickey = New System.Windows.Forms.Button
        Me.lblPublicKeyFile = New System.Windows.Forms.Label
        Me.chkEncrypt = New System.Windows.Forms.CheckBox
        Me.chkSign = New System.Windows.Forms.CheckBox
        Me.cmbCypher = New System.Windows.Forms.ComboBox
        Me.cmbHash = New System.Windows.Forms.ComboBox
        Me.lblBankPublicKeyFile = New System.Windows.Forms.Label
        Me.txtFilenameBankPublicKey = New System.Windows.Forms.TextBox
        Me.cmdfileOpenBankpublicKey = New System.Windows.Forms.Button
        Me.cmbCompression = New System.Windows.Forms.ComboBox
        Me.lblCompression = New System.Windows.Forms.Label
        Me.cmdCreateKeys = New System.Windows.Forms.Button
        Me.lblUserID = New System.Windows.Forms.Label
        Me.txtUserid = New System.Windows.Forms.TextBox
        Me.lblKeyCreationDate = New System.Windows.Forms.Label
        Me.cmdExportPublicKey = New System.Windows.Forms.Button
        Me.lblPassword = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.lblPasswordConfirm = New System.Windows.Forms.Label
        Me.txtPasswordConfirm = New System.Windows.Forms.TextBox
        Me.cmdImportbankPublicKey = New System.Windows.Forms.Button
        Me.chkConfidential = New System.Windows.Forms.CheckBox
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(413, 415)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 99
        Me.Cancel_Button.Text = "55002 &Cancel"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(12, 408)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(550, 1)
        Me.lblLine1.TabIndex = 81
        Me.lblLine1.Text = "Label1"
        '
        'lblHash
        '
        Me.lblHash.AutoSize = True
        Me.lblHash.Location = New System.Drawing.Point(372, 188)
        Me.lblHash.Name = "lblHash"
        Me.lblHash.Size = New System.Drawing.Size(32, 13)
        Me.lblHash.TabIndex = 85
        Me.lblHash.Text = "Hash"
        Me.lblHash.Visible = False
        '
        'lblCypher
        '
        Me.lblCypher.AutoSize = True
        Me.lblCypher.Location = New System.Drawing.Point(113, 186)
        Me.lblCypher.Name = "lblCypher"
        Me.lblCypher.Size = New System.Drawing.Size(73, 13)
        Me.lblCypher.TabIndex = 84
        Me.lblCypher.Text = "60655-Cypher"
        '
        'txtfilenamePublicKey
        '
        Me.txtfilenamePublicKey.AcceptsReturn = True
        Me.txtfilenamePublicKey.BackColor = System.Drawing.SystemColors.Window
        Me.txtfilenamePublicKey.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtfilenamePublicKey.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtfilenamePublicKey.Location = New System.Drawing.Point(241, 242)
        Me.txtfilenamePublicKey.MaxLength = 250
        Me.txtfilenamePublicKey.Name = "txtfilenamePublicKey"
        Me.txtfilenamePublicKey.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtfilenamePublicKey.Size = New System.Drawing.Size(277, 20)
        Me.txtfilenamePublicKey.TabIndex = 4
        '
        'cmdFileOpenPublickey
        '
        Me.cmdFileOpenPublickey.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpenPublickey.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpenPublickey.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpenPublickey.Image = CType(resources.GetObject("cmdFileOpenPublickey.Image"), System.Drawing.Image)
        Me.cmdFileOpenPublickey.Location = New System.Drawing.Point(524, 238)
        Me.cmdFileOpenPublickey.Name = "cmdFileOpenPublickey"
        Me.cmdFileOpenPublickey.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpenPublickey.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpenPublickey.TabIndex = 87
        Me.cmdFileOpenPublickey.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpenPublickey.UseVisualStyleBackColor = False
        Me.cmdFileOpenPublickey.Visible = False
        '
        'lblPublicKeyFile
        '
        Me.lblPublicKeyFile.AutoSize = True
        Me.lblPublicKeyFile.Location = New System.Drawing.Point(113, 244)
        Me.lblPublicKeyFile.Name = "lblPublicKeyFile"
        Me.lblPublicKeyFile.Size = New System.Drawing.Size(131, 13)
        Me.lblPublicKeyFile.TabIndex = 88
        Me.lblPublicKeyFile.Text = "60651-OUR public key file"
        '
        'chkEncrypt
        '
        Me.chkEncrypt.AutoSize = True
        Me.chkEncrypt.Location = New System.Drawing.Point(116, 83)
        Me.chkEncrypt.Name = "chkEncrypt"
        Me.chkEncrypt.Size = New System.Drawing.Size(95, 17)
        Me.chkEncrypt.TabIndex = 8
        Me.chkEncrypt.Text = "60653-Encrypt"
        Me.chkEncrypt.UseVisualStyleBackColor = True
        '
        'chkSign
        '
        Me.chkSign.AutoSize = True
        Me.chkSign.Location = New System.Drawing.Point(116, 114)
        Me.chkSign.Name = "chkSign"
        Me.chkSign.Size = New System.Drawing.Size(80, 17)
        Me.chkSign.TabIndex = 9
        Me.chkSign.Text = "60654-Sign"
        Me.chkSign.UseVisualStyleBackColor = True
        '
        'cmbCypher
        '
        Me.cmbCypher.FormattingEnabled = True
        Me.cmbCypher.Location = New System.Drawing.Point(241, 184)
        Me.cmbCypher.Name = "cmbCypher"
        Me.cmbCypher.Size = New System.Drawing.Size(120, 21)
        Me.cmbCypher.TabIndex = 1
        '
        'cmbHash
        '
        Me.cmbHash.FormattingEnabled = True
        Me.cmbHash.Location = New System.Drawing.Point(413, 184)
        Me.cmbHash.Name = "cmbHash"
        Me.cmbHash.Size = New System.Drawing.Size(135, 21)
        Me.cmbHash.TabIndex = 2
        '
        'lblBankPublicKeyFile
        '
        Me.lblBankPublicKeyFile.AutoSize = True
        Me.lblBankPublicKeyFile.Location = New System.Drawing.Point(113, 273)
        Me.lblBankPublicKeyFile.Name = "lblBankPublicKeyFile"
        Me.lblBankPublicKeyFile.Size = New System.Drawing.Size(136, 13)
        Me.lblBankPublicKeyFile.TabIndex = 99
        Me.lblBankPublicKeyFile.Text = "60656-BANK public key file"
        '
        'txtFilenameBankPublicKey
        '
        Me.txtFilenameBankPublicKey.AcceptsReturn = True
        Me.txtFilenameBankPublicKey.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilenameBankPublicKey.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilenameBankPublicKey.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilenameBankPublicKey.Location = New System.Drawing.Point(241, 271)
        Me.txtFilenameBankPublicKey.MaxLength = 250
        Me.txtFilenameBankPublicKey.Name = "txtFilenameBankPublicKey"
        Me.txtFilenameBankPublicKey.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilenameBankPublicKey.Size = New System.Drawing.Size(277, 20)
        Me.txtFilenameBankPublicKey.TabIndex = 5
        '
        'cmdfileOpenBankpublicKey
        '
        Me.cmdfileOpenBankpublicKey.BackColor = System.Drawing.SystemColors.Control
        Me.cmdfileOpenBankpublicKey.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdfileOpenBankpublicKey.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdfileOpenBankpublicKey.Image = CType(resources.GetObject("cmdfileOpenBankpublicKey.Image"), System.Drawing.Image)
        Me.cmdfileOpenBankpublicKey.Location = New System.Drawing.Point(524, 267)
        Me.cmdfileOpenBankpublicKey.Name = "cmdfileOpenBankpublicKey"
        Me.cmdfileOpenBankpublicKey.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdfileOpenBankpublicKey.Size = New System.Drawing.Size(24, 24)
        Me.cmdfileOpenBankpublicKey.TabIndex = 98
        Me.cmdfileOpenBankpublicKey.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdfileOpenBankpublicKey.UseVisualStyleBackColor = False
        Me.cmdfileOpenBankpublicKey.Visible = False
        '
        'cmbCompression
        '
        Me.cmbCompression.FormattingEnabled = True
        Me.cmbCompression.Location = New System.Drawing.Point(241, 213)
        Me.cmbCompression.Name = "cmbCompression"
        Me.cmbCompression.Size = New System.Drawing.Size(120, 21)
        Me.cmbCompression.TabIndex = 3
        '
        'lblCompression
        '
        Me.lblCompression.AutoSize = True
        Me.lblCompression.Location = New System.Drawing.Point(113, 215)
        Me.lblCompression.Name = "lblCompression"
        Me.lblCompression.Size = New System.Drawing.Size(67, 13)
        Me.lblCompression.TabIndex = 100
        Me.lblCompression.Text = "Compression"
        '
        'cmdCreateKeys
        '
        Me.cmdCreateKeys.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCreateKeys.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCreateKeys.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCreateKeys.Location = New System.Drawing.Point(241, 78)
        Me.cmdCreateKeys.Name = "cmdCreateKeys"
        Me.cmdCreateKeys.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCreateKeys.Size = New System.Drawing.Size(289, 25)
        Me.cmdCreateKeys.TabIndex = 102
        Me.cmdCreateKeys.Text = "60657-Create keypair"
        Me.cmdCreateKeys.UseVisualStyleBackColor = False
        '
        'lblUserID
        '
        Me.lblUserID.AutoSize = True
        Me.lblUserID.Location = New System.Drawing.Point(113, 302)
        Me.lblUserID.Name = "lblUserID"
        Me.lblUserID.Size = New System.Drawing.Size(76, 13)
        Me.lblUserID.TabIndex = 104
        Me.lblUserID.Text = "60660-User ID"
        '
        'txtUserid
        '
        Me.txtUserid.AcceptsReturn = True
        Me.txtUserid.BackColor = System.Drawing.SystemColors.Window
        Me.txtUserid.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtUserid.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtUserid.Location = New System.Drawing.Point(241, 300)
        Me.txtUserid.MaxLength = 50
        Me.txtUserid.Name = "txtUserid"
        Me.txtUserid.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtUserid.Size = New System.Drawing.Size(277, 20)
        Me.txtUserid.TabIndex = 6
        '
        'lblKeyCreationDate
        '
        Me.lblKeyCreationDate.AutoSize = True
        Me.lblKeyCreationDate.Location = New System.Drawing.Point(238, 388)
        Me.lblKeyCreationDate.Name = "lblKeyCreationDate"
        Me.lblKeyCreationDate.Size = New System.Drawing.Size(123, 13)
        Me.lblKeyCreationDate.TabIndex = 105
        Me.lblKeyCreationDate.Text = "60662 - Keypair created:"
        '
        'cmdExportPublicKey
        '
        Me.cmdExportPublicKey.BackColor = System.Drawing.SystemColors.Control
        Me.cmdExportPublicKey.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdExportPublicKey.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExportPublicKey.Location = New System.Drawing.Point(241, 109)
        Me.cmdExportPublicKey.Name = "cmdExportPublicKey"
        Me.cmdExportPublicKey.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdExportPublicKey.Size = New System.Drawing.Size(289, 25)
        Me.cmdExportPublicKey.TabIndex = 106
        Me.cmdExportPublicKey.Text = "60663-Export public key to file"
        Me.cmdExportPublicKey.UseVisualStyleBackColor = False
        '
        'lblPassword
        '
        Me.lblPassword.AutoSize = True
        Me.lblPassword.Location = New System.Drawing.Point(113, 330)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(141, 13)
        Me.lblPassword.TabIndex = 108
        Me.lblPassword.Text = "60664-Private key password"
        '
        'txtPassword
        '
        Me.txtPassword.AcceptsReturn = True
        Me.txtPassword.BackColor = System.Drawing.SystemColors.Window
        Me.txtPassword.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPassword.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPassword.Location = New System.Drawing.Point(241, 328)
        Me.txtPassword.MaxLength = 50
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPassword.Size = New System.Drawing.Size(277, 20)
        Me.txtPassword.TabIndex = 7
        '
        'lblPasswordConfirm
        '
        Me.lblPasswordConfirm.AutoSize = True
        Me.lblPasswordConfirm.Location = New System.Drawing.Point(113, 360)
        Me.lblPasswordConfirm.Name = "lblPasswordConfirm"
        Me.lblPasswordConfirm.Size = New System.Drawing.Size(123, 13)
        Me.lblPasswordConfirm.TabIndex = 110
        Me.lblPasswordConfirm.Text = "60670-Confirm password"
        '
        'txtPasswordConfirm
        '
        Me.txtPasswordConfirm.AcceptsReturn = True
        Me.txtPasswordConfirm.BackColor = System.Drawing.SystemColors.Window
        Me.txtPasswordConfirm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPasswordConfirm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPasswordConfirm.Location = New System.Drawing.Point(241, 358)
        Me.txtPasswordConfirm.MaxLength = 50
        Me.txtPasswordConfirm.Name = "txtPasswordConfirm"
        Me.txtPasswordConfirm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPasswordConfirm.Size = New System.Drawing.Size(277, 20)
        Me.txtPasswordConfirm.TabIndex = 8
        '
        'cmdImportbankPublicKey
        '
        Me.cmdImportbankPublicKey.BackColor = System.Drawing.SystemColors.Control
        Me.cmdImportbankPublicKey.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdImportbankPublicKey.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdImportbankPublicKey.Location = New System.Drawing.Point(241, 139)
        Me.cmdImportbankPublicKey.Name = "cmdImportbankPublicKey"
        Me.cmdImportbankPublicKey.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdImportbankPublicKey.Size = New System.Drawing.Size(289, 25)
        Me.cmdImportbankPublicKey.TabIndex = 111
        Me.cmdImportbankPublicKey.Text = "60673-Import bank's public key file"
        Me.cmdImportbankPublicKey.UseVisualStyleBackColor = False
        '
        'chkConfidential
        '
        Me.chkConfidential.AutoSize = True
        Me.chkConfidential.Location = New System.Drawing.Point(116, 144)
        Me.chkConfidential.Name = "chkConfidential"
        Me.chkConfidential.Size = New System.Drawing.Size(114, 17)
        Me.chkConfidential.TabIndex = 113
        Me.chkConfidential.Text = "60689-Confidential"
        Me.chkConfidential.UseVisualStyleBackColor = True
        '
        'frmSecurity
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(571, 456)
        Me.Controls.Add(Me.chkConfidential)
        Me.Controls.Add(Me.cmdImportbankPublicKey)
        Me.Controls.Add(Me.lblPasswordConfirm)
        Me.Controls.Add(Me.txtPasswordConfirm)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.cmdExportPublicKey)
        Me.Controls.Add(Me.lblKeyCreationDate)
        Me.Controls.Add(Me.lblUserID)
        Me.Controls.Add(Me.txtUserid)
        Me.Controls.Add(Me.cmdCreateKeys)
        Me.Controls.Add(Me.cmbCompression)
        Me.Controls.Add(Me.lblCompression)
        Me.Controls.Add(Me.lblBankPublicKeyFile)
        Me.Controls.Add(Me.txtFilenameBankPublicKey)
        Me.Controls.Add(Me.cmdfileOpenBankpublicKey)
        Me.Controls.Add(Me.cmbHash)
        Me.Controls.Add(Me.cmbCypher)
        Me.Controls.Add(Me.chkSign)
        Me.Controls.Add(Me.chkEncrypt)
        Me.Controls.Add(Me.lblPublicKeyFile)
        Me.Controls.Add(Me.txtfilenamePublicKey)
        Me.Controls.Add(Me.cmdFileOpenPublickey)
        Me.Controls.Add(Me.lblHash)
        Me.Controls.Add(Me.lblCypher)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSecurity"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "55050 - Security"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Private WithEvents lblHash As System.Windows.Forms.Label
    Private WithEvents lblCypher As System.Windows.Forms.Label
    Public WithEvents txtfilenamePublicKey As System.Windows.Forms.TextBox
    Public WithEvents cmdFileOpenPublickey As System.Windows.Forms.Button
    Friend WithEvents lblPublicKeyFile As System.Windows.Forms.Label
    Friend WithEvents chkEncrypt As System.Windows.Forms.CheckBox
    Friend WithEvents chkSign As System.Windows.Forms.CheckBox
    Friend WithEvents cmbCypher As System.Windows.Forms.ComboBox
    Friend WithEvents cmbHash As System.Windows.Forms.ComboBox
    Friend WithEvents lblBankPublicKeyFile As System.Windows.Forms.Label
    Public WithEvents txtFilenameBankPublicKey As System.Windows.Forms.TextBox
    Public WithEvents cmdfileOpenBankpublicKey As System.Windows.Forms.Button
    Friend WithEvents cmbCompression As System.Windows.Forms.ComboBox
    Private WithEvents lblCompression As System.Windows.Forms.Label
    Public WithEvents cmdCreateKeys As System.Windows.Forms.Button
    Friend WithEvents lblUserID As System.Windows.Forms.Label
    Public WithEvents txtUserid As System.Windows.Forms.TextBox
    Friend WithEvents lblKeyCreationDate As System.Windows.Forms.Label
    Public WithEvents cmdExportPublicKey As System.Windows.Forms.Button
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Public WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents lblPasswordConfirm As System.Windows.Forms.Label
    Public WithEvents txtPasswordConfirm As System.Windows.Forms.TextBox
    Public WithEvents cmdImportbankPublicKey As System.Windows.Forms.Button
    Friend WithEvents chkConfidential As System.Windows.Forms.CheckBox

End Class
