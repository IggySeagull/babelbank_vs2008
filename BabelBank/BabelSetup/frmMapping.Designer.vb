<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMapping
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtPos10 As System.Windows.Forms.TextBox
	Public WithEvents txtLen10 As System.Windows.Forms.TextBox
	Public WithEvents txtFormat10 As System.Windows.Forms.TextBox
	Public WithEvents txtFormat13 As System.Windows.Forms.TextBox
	Public WithEvents txtFormat14 As System.Windows.Forms.TextBox
	Public WithEvents txtFormat1 As System.Windows.Forms.TextBox
	Public WithEvents cmdLoad As System.Windows.Forms.Button
	Public WithEvents txtHeaderLines As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdSave As System.Windows.Forms.Button
	Public WithEvents txtDecimalSep As System.Windows.Forms.TextBox
	Public WithEvents txtThousandSep As System.Windows.Forms.TextBox
	Public WithEvents txtFormat15 As System.Windows.Forms.TextBox
	Public WithEvents txtDateFormat As System.Windows.Forms.TextBox
	Public WithEvents txtLen15 As System.Windows.Forms.TextBox
	Public WithEvents txtPos15 As System.Windows.Forms.TextBox
	Public WithEvents txtLen14 As System.Windows.Forms.TextBox
	Public WithEvents txtPos14 As System.Windows.Forms.TextBox
	Public WithEvents txtLen13 As System.Windows.Forms.TextBox
	Public WithEvents txtPos13 As System.Windows.Forms.TextBox
	Public WithEvents txtLen12 As System.Windows.Forms.TextBox
	Public WithEvents txtPos12 As System.Windows.Forms.TextBox
	Public WithEvents txtLen11 As System.Windows.Forms.TextBox
	Public WithEvents txtPos11 As System.Windows.Forms.TextBox
	Public WithEvents txtLen9 As System.Windows.Forms.TextBox
	Public WithEvents txtPos9 As System.Windows.Forms.TextBox
	Public WithEvents txtLen8 As System.Windows.Forms.TextBox
	Public WithEvents txtPos8 As System.Windows.Forms.TextBox
	Public WithEvents txtLen7 As System.Windows.Forms.TextBox
	Public WithEvents txtPos7 As System.Windows.Forms.TextBox
	Public WithEvents txtLen6 As System.Windows.Forms.TextBox
	Public WithEvents txtPos6 As System.Windows.Forms.TextBox
	Public WithEvents txtLen5 As System.Windows.Forms.TextBox
	Public WithEvents txtPos5 As System.Windows.Forms.TextBox
	Public WithEvents txtLen4 As System.Windows.Forms.TextBox
	Public WithEvents txtPos4 As System.Windows.Forms.TextBox
	Public WithEvents txtLen3 As System.Windows.Forms.TextBox
	Public WithEvents txtPos3 As System.Windows.Forms.TextBox
	Public WithEvents txtLen2 As System.Windows.Forms.TextBox
	Public WithEvents txtPos2 As System.Windows.Forms.TextBox
	Public WithEvents txtLen1 As System.Windows.Forms.TextBox
	Public WithEvents txtPos1 As System.Windows.Forms.TextBox
	Public WithEvents txtSurround As System.Windows.Forms.TextBox
	Public WithEvents txtDelimiter As System.Windows.Forms.TextBox
	Public WithEvents cmbFileType As System.Windows.Forms.ComboBox
	Public WithEvents txtMappingFile As System.Windows.Forms.TextBox
	Public WithEvents cmdFileOpenMapping As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents lblCurrency As System.Windows.Forms.Label
	Public WithEvents lblHeaderLines As System.Windows.Forms.Label
	Public WithEvents lblDecSep As System.Windows.Forms.Label
	Public WithEvents lblThousandSep As System.Windows.Forms.Label
	Public WithEvents lblPaytype As System.Windows.Forms.Label
	Public WithEvents lblNotification As System.Windows.Forms.Label
	Public WithEvents lblPayersRef As System.Windows.Forms.Label
	Public WithEvents lblBankcode As System.Windows.Forms.Label
	Public WithEvents lblSWIFT As System.Windows.Forms.Label
	Public WithEvents lblAmount As System.Windows.Forms.Label
	Public WithEvents lblDate As System.Windows.Forms.Label
	Public WithEvents lblCity As System.Windows.Forms.Label
	Public WithEvents lblZip As System.Windows.Forms.Label
	Public WithEvents lblAdr2 As System.Windows.Forms.Label
	Public WithEvents lblAdr1 As System.Windows.Forms.Label
	Public WithEvents lblName As System.Windows.Forms.Label
	Public WithEvents lblToAccount As System.Windows.Forms.Label
	Public WithEvents lblFormat As System.Windows.Forms.Label
	Public WithEvents lblLen As System.Windows.Forms.Label
	Public WithEvents lblPos As System.Windows.Forms.Label
	Public WithEvents lblFieldname As System.Windows.Forms.Label
	Public WithEvents lblFromAccount As System.Windows.Forms.Label
	Public WithEvents lblSurround As System.Windows.Forms.Label
	Public WithEvents lblDelimiter As System.Windows.Forms.Label
	Public WithEvents lblInFormats As System.Windows.Forms.Label
    Public WithEvents lblMappingfile As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMapping))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtPos10 = New System.Windows.Forms.TextBox
        Me.txtLen10 = New System.Windows.Forms.TextBox
        Me.txtFormat10 = New System.Windows.Forms.TextBox
        Me.txtFormat13 = New System.Windows.Forms.TextBox
        Me.txtFormat14 = New System.Windows.Forms.TextBox
        Me.txtFormat1 = New System.Windows.Forms.TextBox
        Me.cmdLoad = New System.Windows.Forms.Button
        Me.txtHeaderLines = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdSave = New System.Windows.Forms.Button
        Me.txtDecimalSep = New System.Windows.Forms.TextBox
        Me.txtThousandSep = New System.Windows.Forms.TextBox
        Me.txtFormat15 = New System.Windows.Forms.TextBox
        Me.txtDateFormat = New System.Windows.Forms.TextBox
        Me.txtLen15 = New System.Windows.Forms.TextBox
        Me.txtPos15 = New System.Windows.Forms.TextBox
        Me.txtLen14 = New System.Windows.Forms.TextBox
        Me.txtPos14 = New System.Windows.Forms.TextBox
        Me.txtLen13 = New System.Windows.Forms.TextBox
        Me.txtPos13 = New System.Windows.Forms.TextBox
        Me.txtLen12 = New System.Windows.Forms.TextBox
        Me.txtPos12 = New System.Windows.Forms.TextBox
        Me.txtLen11 = New System.Windows.Forms.TextBox
        Me.txtPos11 = New System.Windows.Forms.TextBox
        Me.txtLen9 = New System.Windows.Forms.TextBox
        Me.txtPos9 = New System.Windows.Forms.TextBox
        Me.txtLen8 = New System.Windows.Forms.TextBox
        Me.txtPos8 = New System.Windows.Forms.TextBox
        Me.txtLen7 = New System.Windows.Forms.TextBox
        Me.txtPos7 = New System.Windows.Forms.TextBox
        Me.txtLen6 = New System.Windows.Forms.TextBox
        Me.txtPos6 = New System.Windows.Forms.TextBox
        Me.txtLen5 = New System.Windows.Forms.TextBox
        Me.txtPos5 = New System.Windows.Forms.TextBox
        Me.txtLen4 = New System.Windows.Forms.TextBox
        Me.txtPos4 = New System.Windows.Forms.TextBox
        Me.txtLen3 = New System.Windows.Forms.TextBox
        Me.txtPos3 = New System.Windows.Forms.TextBox
        Me.txtLen2 = New System.Windows.Forms.TextBox
        Me.txtPos2 = New System.Windows.Forms.TextBox
        Me.txtLen1 = New System.Windows.Forms.TextBox
        Me.txtPos1 = New System.Windows.Forms.TextBox
        Me.txtSurround = New System.Windows.Forms.TextBox
        Me.txtDelimiter = New System.Windows.Forms.TextBox
        Me.cmbFileType = New System.Windows.Forms.ComboBox
        Me.txtMappingFile = New System.Windows.Forms.TextBox
        Me.cmdFileOpenMapping = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.lblHeaderLines = New System.Windows.Forms.Label
        Me.lblDecSep = New System.Windows.Forms.Label
        Me.lblThousandSep = New System.Windows.Forms.Label
        Me.lblPaytype = New System.Windows.Forms.Label
        Me.lblNotification = New System.Windows.Forms.Label
        Me.lblPayersRef = New System.Windows.Forms.Label
        Me.lblBankcode = New System.Windows.Forms.Label
        Me.lblSWIFT = New System.Windows.Forms.Label
        Me.lblAmount = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblCity = New System.Windows.Forms.Label
        Me.lblZip = New System.Windows.Forms.Label
        Me.lblAdr2 = New System.Windows.Forms.Label
        Me.lblAdr1 = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.lblToAccount = New System.Windows.Forms.Label
        Me.lblFormat = New System.Windows.Forms.Label
        Me.lblLen = New System.Windows.Forms.Label
        Me.lblPos = New System.Windows.Forms.Label
        Me.lblFieldname = New System.Windows.Forms.Label
        Me.lblFromAccount = New System.Windows.Forms.Label
        Me.lblSurround = New System.Windows.Forms.Label
        Me.lblDelimiter = New System.Windows.Forms.Label
        Me.lblInFormats = New System.Windows.Forms.Label
        Me.lblMappingfile = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.lblLine2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtPos10
        '
        Me.txtPos10.AcceptsReturn = True
        Me.txtPos10.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos10.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos10.Location = New System.Drawing.Point(399, 405)
        Me.txtPos10.MaxLength = 4
        Me.txtPos10.Name = "txtPos10"
        Me.txtPos10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos10.Size = New System.Drawing.Size(34, 19)
        Me.txtPos10.TabIndex = 73
        '
        'txtLen10
        '
        Me.txtLen10.AcceptsReturn = True
        Me.txtLen10.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen10.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen10.Location = New System.Drawing.Point(456, 405)
        Me.txtLen10.MaxLength = 4
        Me.txtLen10.Name = "txtLen10"
        Me.txtLen10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen10.Size = New System.Drawing.Size(34, 19)
        Me.txtLen10.TabIndex = 72
        '
        'txtFormat10
        '
        Me.txtFormat10.AcceptsReturn = True
        Me.txtFormat10.BackColor = System.Drawing.SystemColors.Window
        Me.txtFormat10.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFormat10.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFormat10.Location = New System.Drawing.Point(504, 405)
        Me.txtFormat10.MaxLength = 30
        Me.txtFormat10.Name = "txtFormat10"
        Me.txtFormat10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFormat10.Size = New System.Drawing.Size(164, 19)
        Me.txtFormat10.TabIndex = 71
        '
        'txtFormat13
        '
        Me.txtFormat13.AcceptsReturn = True
        Me.txtFormat13.BackColor = System.Drawing.SystemColors.Window
        Me.txtFormat13.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFormat13.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFormat13.Location = New System.Drawing.Point(504, 479)
        Me.txtFormat13.MaxLength = 30
        Me.txtFormat13.Name = "txtFormat13"
        Me.txtFormat13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFormat13.Size = New System.Drawing.Size(164, 19)
        Me.txtFormat13.TabIndex = 33
        '
        'txtFormat14
        '
        Me.txtFormat14.AcceptsReturn = True
        Me.txtFormat14.BackColor = System.Drawing.SystemColors.Window
        Me.txtFormat14.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFormat14.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFormat14.Location = New System.Drawing.Point(504, 504)
        Me.txtFormat14.MaxLength = 40
        Me.txtFormat14.Name = "txtFormat14"
        Me.txtFormat14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFormat14.Size = New System.Drawing.Size(164, 19)
        Me.txtFormat14.TabIndex = 36
        '
        'txtFormat1
        '
        Me.txtFormat1.AcceptsReturn = True
        Me.txtFormat1.BackColor = System.Drawing.SystemColors.Window
        Me.txtFormat1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFormat1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFormat1.Location = New System.Drawing.Point(504, 184)
        Me.txtFormat1.MaxLength = 30
        Me.txtFormat1.Name = "txtFormat1"
        Me.txtFormat1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFormat1.Size = New System.Drawing.Size(164, 19)
        Me.txtFormat1.TabIndex = 7
        '
        'cmdLoad
        '
        Me.cmdLoad.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoad.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdLoad.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdLoad.Location = New System.Drawing.Point(507, 93)
        Me.cmdLoad.Name = "cmdLoad"
        Me.cmdLoad.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdLoad.Size = New System.Drawing.Size(153, 21)
        Me.cmdLoad.TabIndex = 70
        Me.cmdLoad.TabStop = False
        Me.cmdLoad.Text = "60521-Load from mappingfile"
        Me.cmdLoad.UseVisualStyleBackColor = False
        '
        'txtHeaderLines
        '
        Me.txtHeaderLines.AcceptsReturn = True
        Me.txtHeaderLines.BackColor = System.Drawing.SystemColors.Window
        Me.txtHeaderLines.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHeaderLines.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtHeaderLines.Location = New System.Drawing.Point(613, 120)
        Me.txtHeaderLines.MaxLength = 2
        Me.txtHeaderLines.Name = "txtHeaderLines"
        Me.txtHeaderLines.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHeaderLines.Size = New System.Drawing.Size(19, 19)
        Me.txtHeaderLines.TabIndex = 4
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(600, 584)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 68
        Me.cmdOK.TabStop = False
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdSave
        '
        Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Location = New System.Drawing.Point(519, 584)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSave.Size = New System.Drawing.Size(73, 21)
        Me.cmdSave.TabIndex = 67
        Me.cmdSave.TabStop = False
        Me.cmdSave.Text = "55043-Bruk/Lagre"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'txtDecimalSep
        '
        Me.txtDecimalSep.AcceptsReturn = True
        Me.txtDecimalSep.BackColor = System.Drawing.SystemColors.Window
        Me.txtDecimalSep.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDecimalSep.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDecimalSep.Location = New System.Drawing.Point(648, 380)
        Me.txtDecimalSep.MaxLength = 1
        Me.txtDecimalSep.Name = "txtDecimalSep"
        Me.txtDecimalSep.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDecimalSep.Size = New System.Drawing.Size(19, 19)
        Me.txtDecimalSep.TabIndex = 26
        '
        'txtThousandSep
        '
        Me.txtThousandSep.AcceptsReturn = True
        Me.txtThousandSep.BackColor = System.Drawing.SystemColors.Window
        Me.txtThousandSep.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtThousandSep.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtThousandSep.Location = New System.Drawing.Point(562, 380)
        Me.txtThousandSep.MaxLength = 1
        Me.txtThousandSep.Name = "txtThousandSep"
        Me.txtThousandSep.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtThousandSep.Size = New System.Drawing.Size(19, 19)
        Me.txtThousandSep.TabIndex = 25
        '
        'txtFormat15
        '
        Me.txtFormat15.AcceptsReturn = True
        Me.txtFormat15.BackColor = System.Drawing.SystemColors.Window
        Me.txtFormat15.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFormat15.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFormat15.Location = New System.Drawing.Point(504, 528)
        Me.txtFormat15.MaxLength = 50
        Me.txtFormat15.Name = "txtFormat15"
        Me.txtFormat15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFormat15.Size = New System.Drawing.Size(164, 19)
        Me.txtFormat15.TabIndex = 39
        '
        'txtDateFormat
        '
        Me.txtDateFormat.AcceptsReturn = True
        Me.txtDateFormat.BackColor = System.Drawing.SystemColors.Window
        Me.txtDateFormat.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDateFormat.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDateFormat.Location = New System.Drawing.Point(504, 356)
        Me.txtDateFormat.MaxLength = 10
        Me.txtDateFormat.Name = "txtDateFormat"
        Me.txtDateFormat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDateFormat.Size = New System.Drawing.Size(92, 19)
        Me.txtDateFormat.TabIndex = 22
        '
        'txtLen15
        '
        Me.txtLen15.AcceptsReturn = True
        Me.txtLen15.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen15.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen15.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen15.Location = New System.Drawing.Point(456, 528)
        Me.txtLen15.MaxLength = 4
        Me.txtLen15.Name = "txtLen15"
        Me.txtLen15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen15.Size = New System.Drawing.Size(34, 19)
        Me.txtLen15.TabIndex = 38
        '
        'txtPos15
        '
        Me.txtPos15.AcceptsReturn = True
        Me.txtPos15.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos15.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos15.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos15.Location = New System.Drawing.Point(399, 528)
        Me.txtPos15.MaxLength = 4
        Me.txtPos15.Name = "txtPos15"
        Me.txtPos15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos15.Size = New System.Drawing.Size(34, 19)
        Me.txtPos15.TabIndex = 37
        '
        'txtLen14
        '
        Me.txtLen14.AcceptsReturn = True
        Me.txtLen14.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen14.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen14.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen14.Location = New System.Drawing.Point(456, 504)
        Me.txtLen14.MaxLength = 4
        Me.txtLen14.Name = "txtLen14"
        Me.txtLen14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen14.Size = New System.Drawing.Size(34, 19)
        Me.txtLen14.TabIndex = 35
        '
        'txtPos14
        '
        Me.txtPos14.AcceptsReturn = True
        Me.txtPos14.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos14.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos14.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos14.Location = New System.Drawing.Point(399, 504)
        Me.txtPos14.MaxLength = 4
        Me.txtPos14.Name = "txtPos14"
        Me.txtPos14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos14.Size = New System.Drawing.Size(34, 19)
        Me.txtPos14.TabIndex = 34
        '
        'txtLen13
        '
        Me.txtLen13.AcceptsReturn = True
        Me.txtLen13.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen13.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen13.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen13.Location = New System.Drawing.Point(456, 479)
        Me.txtLen13.MaxLength = 4
        Me.txtLen13.Name = "txtLen13"
        Me.txtLen13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen13.Size = New System.Drawing.Size(34, 19)
        Me.txtLen13.TabIndex = 32
        '
        'txtPos13
        '
        Me.txtPos13.AcceptsReturn = True
        Me.txtPos13.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos13.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos13.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos13.Location = New System.Drawing.Point(399, 479)
        Me.txtPos13.MaxLength = 4
        Me.txtPos13.Name = "txtPos13"
        Me.txtPos13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos13.Size = New System.Drawing.Size(34, 19)
        Me.txtPos13.TabIndex = 31
        '
        'txtLen12
        '
        Me.txtLen12.AcceptsReturn = True
        Me.txtLen12.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen12.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen12.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen12.Location = New System.Drawing.Point(456, 454)
        Me.txtLen12.MaxLength = 4
        Me.txtLen12.Name = "txtLen12"
        Me.txtLen12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen12.Size = New System.Drawing.Size(34, 19)
        Me.txtLen12.TabIndex = 30
        '
        'txtPos12
        '
        Me.txtPos12.AcceptsReturn = True
        Me.txtPos12.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos12.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos12.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos12.Location = New System.Drawing.Point(399, 454)
        Me.txtPos12.MaxLength = 4
        Me.txtPos12.Name = "txtPos12"
        Me.txtPos12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos12.Size = New System.Drawing.Size(34, 19)
        Me.txtPos12.TabIndex = 29
        '
        'txtLen11
        '
        Me.txtLen11.AcceptsReturn = True
        Me.txtLen11.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen11.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen11.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen11.Location = New System.Drawing.Point(456, 430)
        Me.txtLen11.MaxLength = 4
        Me.txtLen11.Name = "txtLen11"
        Me.txtLen11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen11.Size = New System.Drawing.Size(34, 19)
        Me.txtLen11.TabIndex = 28
        '
        'txtPos11
        '
        Me.txtPos11.AcceptsReturn = True
        Me.txtPos11.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos11.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos11.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos11.Location = New System.Drawing.Point(399, 430)
        Me.txtPos11.MaxLength = 4
        Me.txtPos11.Name = "txtPos11"
        Me.txtPos11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos11.Size = New System.Drawing.Size(34, 19)
        Me.txtPos11.TabIndex = 27
        '
        'txtLen9
        '
        Me.txtLen9.AcceptsReturn = True
        Me.txtLen9.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen9.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen9.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen9.Location = New System.Drawing.Point(456, 380)
        Me.txtLen9.MaxLength = 4
        Me.txtLen9.Name = "txtLen9"
        Me.txtLen9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen9.Size = New System.Drawing.Size(34, 19)
        Me.txtLen9.TabIndex = 24
        '
        'txtPos9
        '
        Me.txtPos9.AcceptsReturn = True
        Me.txtPos9.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos9.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos9.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos9.Location = New System.Drawing.Point(399, 380)
        Me.txtPos9.MaxLength = 4
        Me.txtPos9.Name = "txtPos9"
        Me.txtPos9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos9.Size = New System.Drawing.Size(34, 19)
        Me.txtPos9.TabIndex = 23
        '
        'txtLen8
        '
        Me.txtLen8.AcceptsReturn = True
        Me.txtLen8.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen8.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen8.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen8.Location = New System.Drawing.Point(456, 356)
        Me.txtLen8.MaxLength = 4
        Me.txtLen8.Name = "txtLen8"
        Me.txtLen8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen8.Size = New System.Drawing.Size(34, 19)
        Me.txtLen8.TabIndex = 21
        '
        'txtPos8
        '
        Me.txtPos8.AcceptsReturn = True
        Me.txtPos8.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos8.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos8.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos8.Location = New System.Drawing.Point(399, 356)
        Me.txtPos8.MaxLength = 4
        Me.txtPos8.Name = "txtPos8"
        Me.txtPos8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos8.Size = New System.Drawing.Size(34, 19)
        Me.txtPos8.TabIndex = 20
        '
        'txtLen7
        '
        Me.txtLen7.AcceptsReturn = True
        Me.txtLen7.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen7.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen7.Location = New System.Drawing.Point(456, 331)
        Me.txtLen7.MaxLength = 4
        Me.txtLen7.Name = "txtLen7"
        Me.txtLen7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen7.Size = New System.Drawing.Size(34, 19)
        Me.txtLen7.TabIndex = 19
        '
        'txtPos7
        '
        Me.txtPos7.AcceptsReturn = True
        Me.txtPos7.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos7.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos7.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos7.Location = New System.Drawing.Point(399, 331)
        Me.txtPos7.MaxLength = 4
        Me.txtPos7.Name = "txtPos7"
        Me.txtPos7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos7.Size = New System.Drawing.Size(34, 19)
        Me.txtPos7.TabIndex = 18
        '
        'txtLen6
        '
        Me.txtLen6.AcceptsReturn = True
        Me.txtLen6.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen6.Location = New System.Drawing.Point(456, 306)
        Me.txtLen6.MaxLength = 4
        Me.txtLen6.Name = "txtLen6"
        Me.txtLen6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen6.Size = New System.Drawing.Size(34, 19)
        Me.txtLen6.TabIndex = 17
        '
        'txtPos6
        '
        Me.txtPos6.AcceptsReturn = True
        Me.txtPos6.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos6.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos6.Location = New System.Drawing.Point(399, 306)
        Me.txtPos6.MaxLength = 4
        Me.txtPos6.Name = "txtPos6"
        Me.txtPos6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos6.Size = New System.Drawing.Size(34, 19)
        Me.txtPos6.TabIndex = 16
        '
        'txtLen5
        '
        Me.txtLen5.AcceptsReturn = True
        Me.txtLen5.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen5.Location = New System.Drawing.Point(456, 282)
        Me.txtLen5.MaxLength = 4
        Me.txtLen5.Name = "txtLen5"
        Me.txtLen5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen5.Size = New System.Drawing.Size(34, 19)
        Me.txtLen5.TabIndex = 15
        '
        'txtPos5
        '
        Me.txtPos5.AcceptsReturn = True
        Me.txtPos5.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos5.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos5.Location = New System.Drawing.Point(400, 282)
        Me.txtPos5.MaxLength = 4
        Me.txtPos5.Name = "txtPos5"
        Me.txtPos5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos5.Size = New System.Drawing.Size(34, 19)
        Me.txtPos5.TabIndex = 14
        '
        'txtLen4
        '
        Me.txtLen4.AcceptsReturn = True
        Me.txtLen4.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen4.Location = New System.Drawing.Point(456, 257)
        Me.txtLen4.MaxLength = 4
        Me.txtLen4.Name = "txtLen4"
        Me.txtLen4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen4.Size = New System.Drawing.Size(34, 19)
        Me.txtLen4.TabIndex = 13
        '
        'txtPos4
        '
        Me.txtPos4.AcceptsReturn = True
        Me.txtPos4.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos4.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos4.Location = New System.Drawing.Point(399, 257)
        Me.txtPos4.MaxLength = 4
        Me.txtPos4.Name = "txtPos4"
        Me.txtPos4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos4.Size = New System.Drawing.Size(34, 19)
        Me.txtPos4.TabIndex = 12
        '
        'txtLen3
        '
        Me.txtLen3.AcceptsReturn = True
        Me.txtLen3.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen3.Location = New System.Drawing.Point(456, 232)
        Me.txtLen3.MaxLength = 4
        Me.txtLen3.Name = "txtLen3"
        Me.txtLen3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen3.Size = New System.Drawing.Size(34, 19)
        Me.txtLen3.TabIndex = 11
        '
        'txtPos3
        '
        Me.txtPos3.AcceptsReturn = True
        Me.txtPos3.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos3.Location = New System.Drawing.Point(399, 232)
        Me.txtPos3.MaxLength = 4
        Me.txtPos3.Name = "txtPos3"
        Me.txtPos3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos3.Size = New System.Drawing.Size(34, 19)
        Me.txtPos3.TabIndex = 10
        '
        'txtLen2
        '
        Me.txtLen2.AcceptsReturn = True
        Me.txtLen2.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen2.Location = New System.Drawing.Point(456, 208)
        Me.txtLen2.MaxLength = 4
        Me.txtLen2.Name = "txtLen2"
        Me.txtLen2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen2.Size = New System.Drawing.Size(34, 19)
        Me.txtLen2.TabIndex = 9
        '
        'txtPos2
        '
        Me.txtPos2.AcceptsReturn = True
        Me.txtPos2.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos2.Location = New System.Drawing.Point(399, 208)
        Me.txtPos2.MaxLength = 4
        Me.txtPos2.Name = "txtPos2"
        Me.txtPos2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos2.Size = New System.Drawing.Size(34, 19)
        Me.txtPos2.TabIndex = 8
        '
        'txtLen1
        '
        Me.txtLen1.AcceptsReturn = True
        Me.txtLen1.BackColor = System.Drawing.SystemColors.Window
        Me.txtLen1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLen1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLen1.Location = New System.Drawing.Point(456, 183)
        Me.txtLen1.MaxLength = 4
        Me.txtLen1.Name = "txtLen1"
        Me.txtLen1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLen1.Size = New System.Drawing.Size(34, 19)
        Me.txtLen1.TabIndex = 6
        '
        'txtPos1
        '
        Me.txtPos1.AcceptsReturn = True
        Me.txtPos1.BackColor = System.Drawing.SystemColors.Window
        Me.txtPos1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPos1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPos1.Location = New System.Drawing.Point(399, 183)
        Me.txtPos1.MaxLength = 4
        Me.txtPos1.Name = "txtPos1"
        Me.txtPos1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPos1.Size = New System.Drawing.Size(34, 19)
        Me.txtPos1.TabIndex = 5
        '
        'txtSurround
        '
        Me.txtSurround.AcceptsReturn = True
        Me.txtSurround.BackColor = System.Drawing.SystemColors.Window
        Me.txtSurround.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSurround.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSurround.Location = New System.Drawing.Point(445, 120)
        Me.txtSurround.MaxLength = 1
        Me.txtSurround.Name = "txtSurround"
        Me.txtSurround.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSurround.Size = New System.Drawing.Size(27, 19)
        Me.txtSurround.TabIndex = 3
        '
        'txtDelimiter
        '
        Me.txtDelimiter.AcceptsReturn = True
        Me.txtDelimiter.BackColor = System.Drawing.SystemColors.Window
        Me.txtDelimiter.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDelimiter.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDelimiter.Location = New System.Drawing.Point(310, 120)
        Me.txtDelimiter.MaxLength = 3
        Me.txtDelimiter.Name = "txtDelimiter"
        Me.txtDelimiter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDelimiter.Size = New System.Drawing.Size(27, 19)
        Me.txtDelimiter.TabIndex = 2
        '
        'cmbFileType
        '
        Me.cmbFileType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbFileType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbFileType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFileType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbFileType.Location = New System.Drawing.Point(310, 93)
        Me.cmbFileType.Name = "cmbFileType"
        Me.cmbFileType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbFileType.Size = New System.Drawing.Size(155, 21)
        Me.cmbFileType.TabIndex = 1
        '
        'txtMappingFile
        '
        Me.txtMappingFile.AcceptsReturn = True
        Me.txtMappingFile.BackColor = System.Drawing.SystemColors.Window
        Me.txtMappingFile.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMappingFile.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMappingFile.Location = New System.Drawing.Point(310, 61)
        Me.txtMappingFile.MaxLength = 150
        Me.txtMappingFile.Name = "txtMappingFile"
        Me.txtMappingFile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMappingFile.Size = New System.Drawing.Size(323, 19)
        Me.txtMappingFile.TabIndex = 0
        '
        'cmdFileOpenMapping
        '
        Me.cmdFileOpenMapping.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpenMapping.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpenMapping.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpenMapping.Image = CType(resources.GetObject("cmdFileOpenMapping.Image"), System.Drawing.Image)
        Me.cmdFileOpenMapping.Location = New System.Drawing.Point(638, 58)
        Me.cmdFileOpenMapping.Name = "cmdFileOpenMapping"
        Me.cmdFileOpenMapping.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpenMapping.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpenMapping.TabIndex = 43
        Me.cmdFileOpenMapping.TabStop = False
        Me.cmdFileOpenMapping.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpenMapping.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(437, 584)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 40
        Me.CmdCancel.TabStop = False
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'lblCurrency
        '
        Me.lblCurrency.BackColor = System.Drawing.SystemColors.Control
        Me.lblCurrency.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCurrency.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCurrency.Location = New System.Drawing.Point(200, 407)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCurrency.Size = New System.Drawing.Size(167, 15)
        Me.lblCurrency.TabIndex = 74
        Me.lblCurrency.Text = "60352 - Valuta"
        '
        'lblHeaderLines
        '
        Me.lblHeaderLines.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeaderLines.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeaderLines.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeaderLines.Location = New System.Drawing.Point(488, 123)
        Me.lblHeaderLines.Name = "lblHeaderLines"
        Me.lblHeaderLines.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeaderLines.Size = New System.Drawing.Size(119, 15)
        Me.lblHeaderLines.TabIndex = 69
        Me.lblHeaderLines.Text = "60524-No of headerlines"
        '
        'lblDecSep
        '
        Me.lblDecSep.BackColor = System.Drawing.SystemColors.Control
        Me.lblDecSep.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDecSep.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDecSep.Location = New System.Drawing.Point(584, 379)
        Me.lblDecSep.Name = "lblDecSep"
        Me.lblDecSep.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDecSep.Size = New System.Drawing.Size(47, 15)
        Me.lblDecSep.TabIndex = 66
        Me.lblDecSep.Text = "60534Dec.sep."
        '
        'lblThousandSep
        '
        Me.lblThousandSep.BackColor = System.Drawing.SystemColors.Control
        Me.lblThousandSep.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblThousandSep.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblThousandSep.Location = New System.Drawing.Point(504, 379)
        Me.lblThousandSep.Name = "lblThousandSep"
        Me.lblThousandSep.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblThousandSep.Size = New System.Drawing.Size(68, 15)
        Me.lblThousandSep.TabIndex = 65
        Me.lblThousandSep.Text = "60533-Thousandsep"
        '
        'lblPaytype
        '
        Me.lblPaytype.BackColor = System.Drawing.SystemColors.Control
        Me.lblPaytype.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPaytype.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPaytype.Location = New System.Drawing.Point(200, 530)
        Me.lblPaytype.Name = "lblPaytype"
        Me.lblPaytype.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPaytype.Size = New System.Drawing.Size(167, 15)
        Me.lblPaytype.TabIndex = 64
        Me.lblPaytype.Text = "60489-Transactiontype"
        '
        'lblNotification
        '
        Me.lblNotification.BackColor = System.Drawing.SystemColors.Control
        Me.lblNotification.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNotification.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNotification.Location = New System.Drawing.Point(200, 505)
        Me.lblNotification.Name = "lblNotification"
        Me.lblNotification.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNotification.Size = New System.Drawing.Size(167, 15)
        Me.lblNotification.TabIndex = 63
        Me.lblNotification.Text = "60537-Notification (to receiver)"
        '
        'lblPayersRef
        '
        Me.lblPayersRef.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayersRef.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayersRef.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayersRef.Location = New System.Drawing.Point(200, 481)
        Me.lblPayersRef.Name = "lblPayersRef"
        Me.lblPayersRef.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayersRef.Size = New System.Drawing.Size(167, 15)
        Me.lblPayersRef.TabIndex = 62
        Me.lblPayersRef.Text = "60536-Own ref (payers reference)"
        '
        'lblBankcode
        '
        Me.lblBankcode.BackColor = System.Drawing.SystemColors.Control
        Me.lblBankcode.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBankcode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBankcode.Location = New System.Drawing.Point(200, 456)
        Me.lblBankcode.Name = "lblBankcode"
        Me.lblBankcode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBankcode.Size = New System.Drawing.Size(167, 15)
        Me.lblBankcode.TabIndex = 61
        Me.lblBankcode.Text = "60535- Bankcode"
        '
        'lblSWIFT
        '
        Me.lblSWIFT.BackColor = System.Drawing.SystemColors.Control
        Me.lblSWIFT.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSWIFT.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSWIFT.Location = New System.Drawing.Point(200, 432)
        Me.lblSWIFT.Name = "lblSWIFT"
        Me.lblSWIFT.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSWIFT.Size = New System.Drawing.Size(167, 15)
        Me.lblSWIFT.TabIndex = 60
        Me.lblSWIFT.Text = "SWIFT"
        '
        'lblAmount
        '
        Me.lblAmount.BackColor = System.Drawing.SystemColors.Control
        Me.lblAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAmount.Location = New System.Drawing.Point(200, 383)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAmount.Size = New System.Drawing.Size(167, 15)
        Me.lblAmount.TabIndex = 59
        Me.lblAmount.Text = "60532 - Amount"
        '
        'lblDate
        '
        Me.lblDate.BackColor = System.Drawing.SystemColors.Control
        Me.lblDate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDate.Location = New System.Drawing.Point(200, 358)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDate.Size = New System.Drawing.Size(167, 15)
        Me.lblDate.TabIndex = 58
        Me.lblDate.Text = "60531 - Paymentdate"
        '
        'lblCity
        '
        Me.lblCity.BackColor = System.Drawing.SystemColors.Control
        Me.lblCity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCity.Location = New System.Drawing.Point(200, 334)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCity.Size = New System.Drawing.Size(167, 15)
        Me.lblCity.TabIndex = 57
        Me.lblCity.Text = "60530 - City"
        '
        'lblZip
        '
        Me.lblZip.BackColor = System.Drawing.SystemColors.Control
        Me.lblZip.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblZip.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblZip.Location = New System.Drawing.Point(200, 309)
        Me.lblZip.Name = "lblZip"
        Me.lblZip.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblZip.Size = New System.Drawing.Size(167, 15)
        Me.lblZip.TabIndex = 56
        Me.lblZip.Text = "60529 - Zipcode"
        '
        'lblAdr2
        '
        Me.lblAdr2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdr2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdr2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdr2.Location = New System.Drawing.Point(200, 285)
        Me.lblAdr2.Name = "lblAdr2"
        Me.lblAdr2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdr2.Size = New System.Drawing.Size(167, 15)
        Me.lblAdr2.TabIndex = 55
        Me.lblAdr2.Text = "60528 - Adress2"
        '
        'lblAdr1
        '
        Me.lblAdr1.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdr1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdr1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdr1.Location = New System.Drawing.Point(200, 260)
        Me.lblAdr1.Name = "lblAdr1"
        Me.lblAdr1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdr1.Size = New System.Drawing.Size(167, 15)
        Me.lblAdr1.TabIndex = 54
        Me.lblAdr1.Text = "60527 - Adress1"
        '
        'lblName
        '
        Me.lblName.BackColor = System.Drawing.SystemColors.Control
        Me.lblName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblName.Location = New System.Drawing.Point(200, 236)
        Me.lblName.Name = "lblName"
        Me.lblName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblName.Size = New System.Drawing.Size(167, 15)
        Me.lblName.TabIndex = 53
        Me.lblName.Text = "60488 - Name"
        '
        'lblToAccount
        '
        Me.lblToAccount.BackColor = System.Drawing.SystemColors.Control
        Me.lblToAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblToAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblToAccount.Location = New System.Drawing.Point(200, 211)
        Me.lblToAccount.Name = "lblToAccount"
        Me.lblToAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblToAccount.Size = New System.Drawing.Size(167, 15)
        Me.lblToAccount.TabIndex = 52
        Me.lblToAccount.Text = "60487To account"
        '
        'lblFormat
        '
        Me.lblFormat.BackColor = System.Drawing.SystemColors.Control
        Me.lblFormat.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFormat.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFormat.Location = New System.Drawing.Point(520, 160)
        Me.lblFormat.Name = "lblFormat"
        Me.lblFormat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFormat.Size = New System.Drawing.Size(137, 17)
        Me.lblFormat.TabIndex = 51
        Me.lblFormat.Text = "60526-Format/Fixed"
        '
        'lblLen
        '
        Me.lblLen.BackColor = System.Drawing.SystemColors.Control
        Me.lblLen.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLen.Location = New System.Drawing.Point(458, 160)
        Me.lblLen.Name = "lblLen"
        Me.lblLen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLen.Size = New System.Drawing.Size(33, 17)
        Me.lblLen.TabIndex = 50
        Me.lblLen.Text = "Len"
        '
        'lblPos
        '
        Me.lblPos.BackColor = System.Drawing.SystemColors.Control
        Me.lblPos.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPos.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPos.Location = New System.Drawing.Point(400, 160)
        Me.lblPos.Name = "lblPos"
        Me.lblPos.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPos.Size = New System.Drawing.Size(33, 17)
        Me.lblPos.TabIndex = 49
        Me.lblPos.Text = "Pos"
        '
        'lblFieldname
        '
        Me.lblFieldname.BackColor = System.Drawing.SystemColors.Control
        Me.lblFieldname.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFieldname.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFieldname.Location = New System.Drawing.Point(200, 160)
        Me.lblFieldname.Name = "lblFieldname"
        Me.lblFieldname.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFieldname.Size = New System.Drawing.Size(153, 17)
        Me.lblFieldname.TabIndex = 48
        Me.lblFieldname.Text = "60525-Fieldname"
        '
        'lblFromAccount
        '
        Me.lblFromAccount.BackColor = System.Drawing.SystemColors.Control
        Me.lblFromAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFromAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFromAccount.Location = New System.Drawing.Point(200, 186)
        Me.lblFromAccount.Name = "lblFromAccount"
        Me.lblFromAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFromAccount.Size = New System.Drawing.Size(167, 15)
        Me.lblFromAccount.TabIndex = 47
        Me.lblFromAccount.Text = "60486-From account"
        '
        'lblSurround
        '
        Me.lblSurround.BackColor = System.Drawing.SystemColors.Control
        Me.lblSurround.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSurround.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSurround.Location = New System.Drawing.Point(352, 123)
        Me.lblSurround.Name = "lblSurround"
        Me.lblSurround.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSurround.Size = New System.Drawing.Size(87, 15)
        Me.lblSurround.TabIndex = 46
        Me.lblSurround.Text = "60523-Surround"
        '
        'lblDelimiter
        '
        Me.lblDelimiter.BackColor = System.Drawing.SystemColors.Control
        Me.lblDelimiter.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDelimiter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDelimiter.Location = New System.Drawing.Point(200, 123)
        Me.lblDelimiter.Name = "lblDelimiter"
        Me.lblDelimiter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDelimiter.Size = New System.Drawing.Size(103, 15)
        Me.lblDelimiter.TabIndex = 45
        Me.lblDelimiter.Text = "60522-Delimiter"
        '
        'lblInFormats
        '
        Me.lblInFormats.BackColor = System.Drawing.SystemColors.Control
        Me.lblInFormats.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblInFormats.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblInFormats.Location = New System.Drawing.Point(199, 92)
        Me.lblInFormats.Name = "lblInFormats"
        Me.lblInFormats.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblInFormats.Size = New System.Drawing.Size(105, 15)
        Me.lblInFormats.TabIndex = 44
        Me.lblInFormats.Text = "60520-Filetype"
        '
        'lblMappingfile
        '
        Me.lblMappingfile.BackColor = System.Drawing.SystemColors.Control
        Me.lblMappingfile.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMappingfile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMappingfile.Location = New System.Drawing.Point(199, 61)
        Me.lblMappingfile.Name = "lblMappingfile"
        Me.lblMappingfile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMappingfile.Size = New System.Drawing.Size(97, 15)
        Me.lblMappingfile.TabIndex = 42
        Me.lblMappingfile.Text = "60519-Mappingfile"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(200, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(393, 17)
        Me.lblHeading.TabIndex = 41
        Me.lblHeading.Text = "60538 -Mapping of paymentfile"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(191, 151)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(475, 1)
        Me.lblLine1.TabIndex = 76
        Me.lblLine1.Text = "Label1"
        '
        'lblLine2
        '
        Me.lblLine2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine2.Location = New System.Drawing.Point(10, 574)
        Me.lblLine2.Name = "lblLine2"
        Me.lblLine2.Size = New System.Drawing.Size(670, 1)
        Me.lblLine2.TabIndex = 77
        Me.lblLine2.Text = "Label1"
        '
        'frmMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(688, 614)
        Me.Controls.Add(Me.lblLine2)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtPos10)
        Me.Controls.Add(Me.txtLen10)
        Me.Controls.Add(Me.txtFormat10)
        Me.Controls.Add(Me.txtFormat13)
        Me.Controls.Add(Me.txtFormat14)
        Me.Controls.Add(Me.txtFormat1)
        Me.Controls.Add(Me.cmdLoad)
        Me.Controls.Add(Me.txtHeaderLines)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.txtDecimalSep)
        Me.Controls.Add(Me.txtThousandSep)
        Me.Controls.Add(Me.txtFormat15)
        Me.Controls.Add(Me.txtDateFormat)
        Me.Controls.Add(Me.txtLen15)
        Me.Controls.Add(Me.txtPos15)
        Me.Controls.Add(Me.txtLen14)
        Me.Controls.Add(Me.txtPos14)
        Me.Controls.Add(Me.txtLen13)
        Me.Controls.Add(Me.txtPos13)
        Me.Controls.Add(Me.txtLen12)
        Me.Controls.Add(Me.txtPos12)
        Me.Controls.Add(Me.txtLen11)
        Me.Controls.Add(Me.txtPos11)
        Me.Controls.Add(Me.txtLen9)
        Me.Controls.Add(Me.txtPos9)
        Me.Controls.Add(Me.txtLen8)
        Me.Controls.Add(Me.txtPos8)
        Me.Controls.Add(Me.txtLen7)
        Me.Controls.Add(Me.txtPos7)
        Me.Controls.Add(Me.txtLen6)
        Me.Controls.Add(Me.txtPos6)
        Me.Controls.Add(Me.txtLen5)
        Me.Controls.Add(Me.txtPos5)
        Me.Controls.Add(Me.txtLen4)
        Me.Controls.Add(Me.txtPos4)
        Me.Controls.Add(Me.txtLen3)
        Me.Controls.Add(Me.txtPos3)
        Me.Controls.Add(Me.txtLen2)
        Me.Controls.Add(Me.txtPos2)
        Me.Controls.Add(Me.txtLen1)
        Me.Controls.Add(Me.txtPos1)
        Me.Controls.Add(Me.txtSurround)
        Me.Controls.Add(Me.txtDelimiter)
        Me.Controls.Add(Me.cmbFileType)
        Me.Controls.Add(Me.txtMappingFile)
        Me.Controls.Add(Me.cmdFileOpenMapping)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.lblCurrency)
        Me.Controls.Add(Me.lblHeaderLines)
        Me.Controls.Add(Me.lblDecSep)
        Me.Controls.Add(Me.lblThousandSep)
        Me.Controls.Add(Me.lblPaytype)
        Me.Controls.Add(Me.lblNotification)
        Me.Controls.Add(Me.lblPayersRef)
        Me.Controls.Add(Me.lblBankcode)
        Me.Controls.Add(Me.lblSWIFT)
        Me.Controls.Add(Me.lblAmount)
        Me.Controls.Add(Me.lblDate)
        Me.Controls.Add(Me.lblCity)
        Me.Controls.Add(Me.lblZip)
        Me.Controls.Add(Me.lblAdr2)
        Me.Controls.Add(Me.lblAdr1)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.lblToAccount)
        Me.Controls.Add(Me.lblFormat)
        Me.Controls.Add(Me.lblLen)
        Me.Controls.Add(Me.lblPos)
        Me.Controls.Add(Me.lblFieldname)
        Me.Controls.Add(Me.lblFromAccount)
        Me.Controls.Add(Me.lblSurround)
        Me.Controls.Add(Me.lblDelimiter)
        Me.Controls.Add(Me.lblInFormats)
        Me.Controls.Add(Me.lblMappingfile)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMapping"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "60538 - Mapping of paymentfile"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents lblLine2 As System.Windows.Forms.Label
#End Region 
End Class
