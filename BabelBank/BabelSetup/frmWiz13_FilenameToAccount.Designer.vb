<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmWiz13_FilenameToAccount
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents CmdFinish As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdNext As System.Windows.Forms.Button
	Public WithEvents CmdBack As System.Windows.Forms.Button
	Public WithEvents cmdFileOpen3 As System.Windows.Forms.Button
	Public WithEvents cmdFileOpen2 As System.Windows.Forms.Button
	Public WithEvents cmdFileOpen1 As System.Windows.Forms.Button
	Public WithEvents chkRemove As System.Windows.Forms.CheckBox
	Public WithEvents chkWarning As System.Windows.Forms.CheckBox
	Public WithEvents chkBackup As System.Windows.Forms.CheckBox
	Public WithEvents cmdAdvanced As System.Windows.Forms.Button
	Public WithEvents txtFilename1 As System.Windows.Forms.TextBox
	Public WithEvents txtFilename2 As System.Windows.Forms.TextBox
	Public WithEvents txtFilename3 As System.Windows.Forms.TextBox
	Public WithEvents chkOverwrite As System.Windows.Forms.CheckBox
	Public WithEvents lblFilename3 As System.Windows.Forms.Label
	Public WithEvents lblFilename2 As System.Windows.Forms.Label
	Public WithEvents lblFilename1 As System.Windows.Forms.Label
	Public WithEvents fraToAccount As System.Windows.Forms.GroupBox
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
    Public WithEvents lblHeading As System.Windows.Forms.Label
	Public WithEvents lblDescr As System.Windows.Forms.Label
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWiz13_FilenameToAccount))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.CmdFinish = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdNext = New System.Windows.Forms.Button
        Me.CmdBack = New System.Windows.Forms.Button
        Me.fraToAccount = New System.Windows.Forms.GroupBox
        Me.cmbBankname = New System.Windows.Forms.ComboBox
        Me.lblBankname = New System.Windows.Forms.Label
        Me.cmdFileOpen3 = New System.Windows.Forms.Button
        Me.cmdFileOpen2 = New System.Windows.Forms.Button
        Me.cmdFileOpen1 = New System.Windows.Forms.Button
        Me.chkRemove = New System.Windows.Forms.CheckBox
        Me.chkWarning = New System.Windows.Forms.CheckBox
        Me.chkBackup = New System.Windows.Forms.CheckBox
        Me.cmdAdvanced = New System.Windows.Forms.Button
        Me.txtFilename1 = New System.Windows.Forms.TextBox
        Me.txtFilename2 = New System.Windows.Forms.TextBox
        Me.txtFilename3 = New System.Windows.Forms.TextBox
        Me.chkOverwrite = New System.Windows.Forms.CheckBox
        Me.lblFilename3 = New System.Windows.Forms.Label
        Me.lblFilename2 = New System.Windows.Forms.Label
        Me.lblFilename1 = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblDescr = New System.Windows.Forms.Label
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.fraToAccount.SuspendLayout()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(144, 303)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 4
        Me.CmdHelp.Text = "&Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'CmdFinish
        '
        Me.CmdFinish.BackColor = System.Drawing.SystemColors.Control
        Me.CmdFinish.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdFinish.Enabled = False
        Me.CmdFinish.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdFinish.Location = New System.Drawing.Point(530, 303)
        Me.CmdFinish.Name = "CmdFinish"
        Me.CmdFinish.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdFinish.Size = New System.Drawing.Size(73, 21)
        Me.CmdFinish.TabIndex = 8
        Me.CmdFinish.Text = "F&ullf�r"
        Me.CmdFinish.UseVisualStyleBackColor = False
        Me.CmdFinish.Visible = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(288, 303)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 5
        Me.CmdCancel.Text = "&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdNext
        '
        Me.CmdNext.BackColor = System.Drawing.SystemColors.Control
        Me.CmdNext.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdNext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdNext.Location = New System.Drawing.Point(439, 303)
        Me.CmdNext.Name = "CmdNext"
        Me.CmdNext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdNext.Size = New System.Drawing.Size(73, 21)
        Me.CmdNext.TabIndex = 7
        Me.CmdNext.Text = "&Neste >"
        Me.CmdNext.UseVisualStyleBackColor = False
        '
        'CmdBack
        '
        Me.CmdBack.BackColor = System.Drawing.SystemColors.Control
        Me.CmdBack.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdBack.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdBack.Location = New System.Drawing.Point(365, 303)
        Me.CmdBack.Name = "CmdBack"
        Me.CmdBack.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdBack.Size = New System.Drawing.Size(73, 21)
        Me.CmdBack.TabIndex = 6
        Me.CmdBack.Text = "< &Forrige"
        Me.CmdBack.UseVisualStyleBackColor = False
        '
        'fraToAccount
        '
        Me.fraToAccount.BackColor = System.Drawing.SystemColors.Window
        Me.fraToAccount.Controls.Add(Me.cmbBankname)
        Me.fraToAccount.Controls.Add(Me.lblBankname)
        Me.fraToAccount.Controls.Add(Me.cmdFileOpen3)
        Me.fraToAccount.Controls.Add(Me.cmdFileOpen2)
        Me.fraToAccount.Controls.Add(Me.cmdFileOpen1)
        Me.fraToAccount.Controls.Add(Me.chkRemove)
        Me.fraToAccount.Controls.Add(Me.chkWarning)
        Me.fraToAccount.Controls.Add(Me.chkBackup)
        Me.fraToAccount.Controls.Add(Me.cmdAdvanced)
        Me.fraToAccount.Controls.Add(Me.txtFilename1)
        Me.fraToAccount.Controls.Add(Me.txtFilename2)
        Me.fraToAccount.Controls.Add(Me.txtFilename3)
        Me.fraToAccount.Controls.Add(Me.chkOverwrite)
        Me.fraToAccount.Controls.Add(Me.lblFilename3)
        Me.fraToAccount.Controls.Add(Me.lblFilename2)
        Me.fraToAccount.Controls.Add(Me.lblFilename1)
        Me.fraToAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraToAccount.Location = New System.Drawing.Point(184, 102)
        Me.fraToAccount.Name = "fraToAccount"
        Me.fraToAccount.Padding = New System.Windows.Forms.Padding(0)
        Me.fraToAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraToAccount.Size = New System.Drawing.Size(428, 187)
        Me.fraToAccount.TabIndex = 9
        Me.fraToAccount.TabStop = False
        Me.fraToAccount.Text = "Returfiler til regnskap"
        '
        'cmbBankname
        '
        Me.cmbBankname.FormattingEnabled = True
        Me.cmbBankname.Location = New System.Drawing.Point(104, 109)
        Me.cmbBankname.Name = "cmbBankname"
        Me.cmbBankname.Size = New System.Drawing.Size(273, 21)
        Me.cmbBankname.TabIndex = 23
        Me.cmbBankname.Text = "<bank>"
        '
        'lblBankname
        '
        Me.lblBankname.BackColor = System.Drawing.SystemColors.Window
        Me.lblBankname.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBankname.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBankname.Location = New System.Drawing.Point(8, 109)
        Me.lblBankname.Name = "lblBankname"
        Me.lblBankname.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBankname.Size = New System.Drawing.Size(88, 28)
        Me.lblBankname.TabIndex = 22
        Me.lblBankname.Text = "Banknavn"
        '
        'cmdFileOpen3
        '
        Me.cmdFileOpen3.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen3.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen3.Image = CType(resources.GetObject("cmdFileOpen3.Image"), System.Drawing.Image)
        Me.cmdFileOpen3.Location = New System.Drawing.Point(387, 79)
        Me.cmdFileOpen3.Name = "cmdFileOpen3"
        Me.cmdFileOpen3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen3.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen3.TabIndex = 21
        Me.cmdFileOpen3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen3.UseVisualStyleBackColor = False
        '
        'cmdFileOpen2
        '
        Me.cmdFileOpen2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen2.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen2.Image = CType(resources.GetObject("cmdFileOpen2.Image"), System.Drawing.Image)
        Me.cmdFileOpen2.Location = New System.Drawing.Point(387, 50)
        Me.cmdFileOpen2.Name = "cmdFileOpen2"
        Me.cmdFileOpen2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen2.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen2.TabIndex = 20
        Me.cmdFileOpen2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen2.UseVisualStyleBackColor = False
        '
        'cmdFileOpen1
        '
        Me.cmdFileOpen1.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen1.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen1.Image = CType(resources.GetObject("cmdFileOpen1.Image"), System.Drawing.Image)
        Me.cmdFileOpen1.Location = New System.Drawing.Point(387, 16)
        Me.cmdFileOpen1.Name = "cmdFileOpen1"
        Me.cmdFileOpen1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen1.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen1.TabIndex = 19
        Me.cmdFileOpen1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen1.UseVisualStyleBackColor = False
        '
        'chkRemove
        '
        Me.chkRemove.BackColor = System.Drawing.SystemColors.Window
        Me.chkRemove.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkRemove.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRemove.Location = New System.Drawing.Point(152, 163)
        Me.chkRemove.Name = "chkRemove"
        Me.chkRemove.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkRemove.Size = New System.Drawing.Size(163, 17)
        Me.chkRemove.TabIndex = 18
        Me.chkRemove.Text = "Fjern gamle filer f�r eksport"
        Me.chkRemove.UseVisualStyleBackColor = False
        '
        'chkWarning
        '
        Me.chkWarning.BackColor = System.Drawing.SystemColors.Window
        Me.chkWarning.Checked = True
        Me.chkWarning.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkWarning.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkWarning.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkWarning.Location = New System.Drawing.Point(8, 163)
        Me.chkWarning.Name = "chkWarning"
        Me.chkWarning.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkWarning.Size = New System.Drawing.Size(137, 17)
        Me.chkWarning.TabIndex = 14
        Me.chkWarning.Text = "Varsle hvis fil finnes"
        Me.chkWarning.UseVisualStyleBackColor = False
        '
        'chkBackup
        '
        Me.chkBackup.BackColor = System.Drawing.SystemColors.Window
        Me.chkBackup.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkBackup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkBackup.Location = New System.Drawing.Point(8, 141)
        Me.chkBackup.Name = "chkBackup"
        Me.chkBackup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkBackup.Size = New System.Drawing.Size(138, 17)
        Me.chkBackup.TabIndex = 13
        Me.chkBackup.Text = "Legg i backupkatalog"
        Me.chkBackup.UseVisualStyleBackColor = False
        '
        'cmdAdvanced
        '
        Me.cmdAdvanced.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAdvanced.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAdvanced.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAdvanced.Location = New System.Drawing.Point(315, 156)
        Me.cmdAdvanced.Name = "cmdAdvanced"
        Me.cmdAdvanced.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAdvanced.Size = New System.Drawing.Size(105, 32)
        Me.cmdAdvanced.TabIndex = 12
        Me.cmdAdvanced.Text = "&Avansert oppsett"
        Me.cmdAdvanced.UseVisualStyleBackColor = False
        '
        'txtFilename1
        '
        Me.txtFilename1.AcceptsReturn = True
        Me.txtFilename1.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilename1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilename1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilename1.Location = New System.Drawing.Point(104, 18)
        Me.txtFilename1.MaxLength = 255
        Me.txtFilename1.Name = "txtFilename1"
        Me.txtFilename1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilename1.Size = New System.Drawing.Size(273, 20)
        Me.txtFilename1.TabIndex = 0
        '
        'txtFilename2
        '
        Me.txtFilename2.AcceptsReturn = True
        Me.txtFilename2.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilename2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilename2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilename2.Location = New System.Drawing.Point(104, 51)
        Me.txtFilename2.MaxLength = 255
        Me.txtFilename2.Name = "txtFilename2"
        Me.txtFilename2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilename2.Size = New System.Drawing.Size(273, 20)
        Me.txtFilename2.TabIndex = 1
        '
        'txtFilename3
        '
        Me.txtFilename3.AcceptsReturn = True
        Me.txtFilename3.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilename3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilename3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilename3.Location = New System.Drawing.Point(104, 80)
        Me.txtFilename3.MaxLength = 255
        Me.txtFilename3.Name = "txtFilename3"
        Me.txtFilename3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilename3.Size = New System.Drawing.Size(273, 20)
        Me.txtFilename3.TabIndex = 2
        '
        'chkOverwrite
        '
        Me.chkOverwrite.BackColor = System.Drawing.SystemColors.Window
        Me.chkOverwrite.Checked = True
        Me.chkOverwrite.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkOverwrite.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkOverwrite.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkOverwrite.Location = New System.Drawing.Point(152, 141)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkOverwrite.Size = New System.Drawing.Size(176, 17)
        Me.chkOverwrite.TabIndex = 3
        Me.chkOverwrite.Text = "Skriv over hvis fil finnes"
        Me.chkOverwrite.UseVisualStyleBackColor = False
        '
        'lblFilename3
        '
        Me.lblFilename3.BackColor = System.Drawing.SystemColors.Window
        Me.lblFilename3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilename3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilename3.Location = New System.Drawing.Point(8, 80)
        Me.lblFilename3.Name = "lblFilename3"
        Me.lblFilename3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilename3.Size = New System.Drawing.Size(92, 28)
        Me.lblFilename3.TabIndex = 17
        Me.lblFilename3.Text = "Filnavn 3"
        '
        'lblFilename2
        '
        Me.lblFilename2.BackColor = System.Drawing.SystemColors.Window
        Me.lblFilename2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilename2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilename2.Location = New System.Drawing.Point(8, 51)
        Me.lblFilename2.Name = "lblFilename2"
        Me.lblFilename2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilename2.Size = New System.Drawing.Size(92, 28)
        Me.lblFilename2.TabIndex = 16
        Me.lblFilename2.Text = "Filnavn 2"
        '
        'lblFilename1
        '
        Me.lblFilename1.BackColor = System.Drawing.SystemColors.Window
        Me.lblFilename1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilename1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilename1.Location = New System.Drawing.Point(8, 21)
        Me.lblFilename1.Name = "lblFilename1"
        Me.lblFilename1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilename1.Size = New System.Drawing.Size(92, 28)
        Me.lblFilename1.TabIndex = 15
        Me.lblFilename1.Text = "Filnavn 1"
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(144, 252)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(17, 25)
        Me._Image1_0.TabIndex = 10
        Me._Image1_0.TabStop = False
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(183, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(417, 17)
        Me.lblHeading.TabIndex = 11
        Me.lblHeading.Text = "Legg inn navn p� returfiler til regnskap"
        '
        'lblDescr
        '
        Me.lblDescr.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescr.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescr.Location = New System.Drawing.Point(183, 51)
        Me.lblDescr.Name = "lblDescr"
        Me.lblDescr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescr.Size = New System.Drawing.Size(417, 48)
        Me.lblDescr.TabIndex = 10
        Me.lblDescr.Text = resources.GetString("lblDescr.Text")
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(13, 297)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(595, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmWiz13_FilenameToAccount
        '
        Me.AcceptButton = Me.CmdNext
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(620, 333)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.CmdFinish)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdNext)
        Me.Controls.Add(Me.CmdBack)
        Me.Controls.Add(Me.fraToAccount)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.lblDescr)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmWiz13_FilenameToAccount"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Returfiler til regnskap"
        Me.fraToAccount.ResumeLayout(False)
        Me.fraToAccount.PerformLayout()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmbBankname As System.Windows.Forms.ComboBox
    Public WithEvents lblBankname As System.Windows.Forms.Label
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
