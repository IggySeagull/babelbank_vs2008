<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDiffAccountDetail
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtCurrencyCode As System.Windows.Forms.TextBox
	Public WithEvents txtVATCode As System.Windows.Forms.TextBox
	Public WithEvents chkCashDiscountAccount As System.Windows.Forms.CheckBox
	Public WithEvents chkUnmatchedAccount As System.Windows.Forms.CheckBox
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents CmdOK As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents cmbDiffType As System.Windows.Forms.ComboBox
	Public WithEvents txtDiffPattern As System.Windows.Forms.TextBox
	Public WithEvents txtDiffText As System.Windows.Forms.TextBox
	Public WithEvents txtDiffAccountName As System.Windows.Forms.TextBox
	Public WithEvents lblCurrencyCode As System.Windows.Forms.Label
	Public WithEvents lblVATCode As System.Windows.Forms.Label
    Public WithEvents lbDiffType As System.Windows.Forms.Label
	Public WithEvents lblDiffPattern As System.Windows.Forms.Label
	Public WithEvents lblDiffType As System.Windows.Forms.Label
	Public WithEvents lblDiffAccountName As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtCurrencyCode = New System.Windows.Forms.TextBox
        Me.txtVATCode = New System.Windows.Forms.TextBox
        Me.chkCashDiscountAccount = New System.Windows.Forms.CheckBox
        Me.chkUnmatchedAccount = New System.Windows.Forms.CheckBox
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.CmdOK = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.cmbDiffType = New System.Windows.Forms.ComboBox
        Me.txtDiffPattern = New System.Windows.Forms.TextBox
        Me.txtDiffText = New System.Windows.Forms.TextBox
        Me.txtDiffAccountName = New System.Windows.Forms.TextBox
        Me.lblCurrencyCode = New System.Windows.Forms.Label
        Me.lblVATCode = New System.Windows.Forms.Label
        Me.lbDiffType = New System.Windows.Forms.Label
        Me.lblDiffPattern = New System.Windows.Forms.Label
        Me.lblDiffType = New System.Windows.Forms.Label
        Me.lblDiffAccountName = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.cmdDimensions = New System.Windows.Forms.Button
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtCurrencyCode
        '
        Me.txtCurrencyCode.AcceptsReturn = True
        Me.txtCurrencyCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtCurrencyCode.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCurrencyCode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCurrencyCode.Location = New System.Drawing.Point(352, 212)
        Me.txtCurrencyCode.MaxLength = 10
        Me.txtCurrencyCode.Name = "txtCurrencyCode"
        Me.txtCurrencyCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCurrencyCode.Size = New System.Drawing.Size(30, 20)
        Me.txtCurrencyCode.TabIndex = 5
        '
        'txtVATCode
        '
        Me.txtVATCode.AcceptsReturn = True
        Me.txtVATCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtVATCode.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVATCode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtVATCode.Location = New System.Drawing.Point(352, 184)
        Me.txtVATCode.MaxLength = 10
        Me.txtVATCode.Name = "txtVATCode"
        Me.txtVATCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVATCode.Size = New System.Drawing.Size(134, 20)
        Me.txtVATCode.TabIndex = 4
        '
        'chkCashDiscountAccount
        '
        Me.chkCashDiscountAccount.BackColor = System.Drawing.SystemColors.Control
        Me.chkCashDiscountAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkCashDiscountAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkCashDiscountAccount.Location = New System.Drawing.Point(195, 241)
        Me.chkCashDiscountAccount.Name = "chkCashDiscountAccount"
        Me.chkCashDiscountAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkCashDiscountAccount.Size = New System.Drawing.Size(136, 35)
        Me.chkCashDiscountAccount.TabIndex = 6
        Me.chkCashDiscountAccount.Text = "60394 - Cashdiscount account"
        Me.chkCashDiscountAccount.UseVisualStyleBackColor = False
        '
        'chkUnmatchedAccount
        '
        Me.chkUnmatchedAccount.BackColor = System.Drawing.SystemColors.Control
        Me.chkUnmatchedAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUnmatchedAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUnmatchedAccount.Location = New System.Drawing.Point(352, 241)
        Me.chkUnmatchedAccount.Name = "chkUnmatchedAccount"
        Me.chkUnmatchedAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUnmatchedAccount.Size = New System.Drawing.Size(136, 35)
        Me.chkUnmatchedAccount.TabIndex = 7
        Me.chkUnmatchedAccount.Text = "60382-Unmatched account"
        Me.chkUnmatchedAccount.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(260, 296)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 7
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "55001 - &Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'CmdOK
        '
        Me.CmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.CmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdOK.Location = New System.Drawing.Point(415, 296)
        Me.CmdOK.Name = "CmdOK"
        Me.CmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdOK.Size = New System.Drawing.Size(73, 21)
        Me.CmdOK.TabIndex = 9
        Me.CmdOK.Text = "&OK"
        Me.CmdOK.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(337, 296)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 8
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'cmbDiffType
        '
        Me.cmbDiffType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbDiffType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbDiffType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbDiffType.Location = New System.Drawing.Point(352, 125)
        Me.cmbDiffType.Name = "cmbDiffType"
        Me.cmbDiffType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbDiffType.Size = New System.Drawing.Size(134, 21)
        Me.cmbDiffType.TabIndex = 2
        Me.cmbDiffType.Text = "cmbDiffType"
        '
        'txtDiffPattern
        '
        Me.txtDiffPattern.AcceptsReturn = True
        Me.txtDiffPattern.BackColor = System.Drawing.SystemColors.Window
        Me.txtDiffPattern.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDiffPattern.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDiffPattern.Location = New System.Drawing.Point(352, 96)
        Me.txtDiffPattern.MaxLength = 50
        Me.txtDiffPattern.Name = "txtDiffPattern"
        Me.txtDiffPattern.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDiffPattern.Size = New System.Drawing.Size(134, 20)
        Me.txtDiffPattern.TabIndex = 1
        '
        'txtDiffText
        '
        Me.txtDiffText.AcceptsReturn = True
        Me.txtDiffText.BackColor = System.Drawing.SystemColors.Window
        Me.txtDiffText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDiffText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDiffText.Location = New System.Drawing.Point(352, 156)
        Me.txtDiffText.MaxLength = 50
        Me.txtDiffText.Name = "txtDiffText"
        Me.txtDiffText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDiffText.Size = New System.Drawing.Size(134, 20)
        Me.txtDiffText.TabIndex = 3
        '
        'txtDiffAccountName
        '
        Me.txtDiffAccountName.AcceptsReturn = True
        Me.txtDiffAccountName.BackColor = System.Drawing.SystemColors.Window
        Me.txtDiffAccountName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDiffAccountName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDiffAccountName.Location = New System.Drawing.Point(352, 67)
        Me.txtDiffAccountName.MaxLength = 35
        Me.txtDiffAccountName.Name = "txtDiffAccountName"
        Me.txtDiffAccountName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDiffAccountName.Size = New System.Drawing.Size(134, 20)
        Me.txtDiffAccountName.TabIndex = 0
        '
        'lblCurrencyCode
        '
        Me.lblCurrencyCode.BackColor = System.Drawing.SystemColors.Control
        Me.lblCurrencyCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCurrencyCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCurrencyCode.Location = New System.Drawing.Point(192, 212)
        Me.lblCurrencyCode.Name = "lblCurrencyCode"
        Me.lblCurrencyCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCurrencyCode.Size = New System.Drawing.Size(147, 19)
        Me.lblCurrencyCode.TabIndex = 16
        Me.lblCurrencyCode.Text = "60212 - CurencyCode"
        '
        'lblVATCode
        '
        Me.lblVATCode.BackColor = System.Drawing.SystemColors.Control
        Me.lblVATCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVATCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVATCode.Location = New System.Drawing.Point(195, 184)
        Me.lblVATCode.Name = "lblVATCode"
        Me.lblVATCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVATCode.Size = New System.Drawing.Size(147, 19)
        Me.lblVATCode.TabIndex = 15
        Me.lblVATCode.Text = "60401 -  VATCode"
        '
        'lbDiffType
        '
        Me.lbDiffType.BackColor = System.Drawing.SystemColors.Control
        Me.lbDiffType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lbDiffType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbDiffType.Location = New System.Drawing.Point(195, 125)
        Me.lbDiffType.Name = "lbDiffType"
        Me.lbDiffType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbDiffType.Size = New System.Drawing.Size(154, 22)
        Me.lbDiffType.TabIndex = 14
        Me.lbDiffType.Text = "60359-AccountType"
        '
        'lblDiffPattern
        '
        Me.lblDiffPattern.BackColor = System.Drawing.SystemColors.Control
        Me.lblDiffPattern.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDiffPattern.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDiffPattern.Location = New System.Drawing.Point(195, 97)
        Me.lblDiffPattern.Name = "lblDiffPattern"
        Me.lblDiffPattern.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDiffPattern.Size = New System.Drawing.Size(147, 19)
        Me.lblDiffPattern.TabIndex = 13
        Me.lblDiffPattern.Text = "60036-Account"
        '
        'lblDiffType
        '
        Me.lblDiffType.BackColor = System.Drawing.SystemColors.Control
        Me.lblDiffType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDiffType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDiffType.Location = New System.Drawing.Point(195, 156)
        Me.lblDiffType.Name = "lblDiffType"
        Me.lblDiffType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDiffType.Size = New System.Drawing.Size(147, 19)
        Me.lblDiffType.TabIndex = 12
        Me.lblDiffType.Text = "60210 - Text"
        '
        'lblDiffAccountName
        '
        Me.lblDiffAccountName.BackColor = System.Drawing.SystemColors.Control
        Me.lblDiffAccountName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDiffAccountName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDiffAccountName.Location = New System.Drawing.Point(195, 70)
        Me.lblDiffAccountName.Name = "lblDiffAccountName"
        Me.lblDiffAccountName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDiffAccountName.Size = New System.Drawing.Size(144, 17)
        Me.lblDiffAccountName.TabIndex = 11
        Me.lblDiffAccountName.Text = "60206 - Name"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(194, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(304, 17)
        Me.lblHeading.TabIndex = 10
        Me.lblHeading.Text = "60358 - Deviation Accounts"
        '
        'cmdDimensions
        '
        Me.cmdDimensions.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDimensions.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDimensions.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDimensions.Location = New System.Drawing.Point(388, 210)
        Me.cmdDimensions.Name = "cmdDimensions"
        Me.cmdDimensions.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDimensions.Size = New System.Drawing.Size(100, 21)
        Me.cmdDimensions.TabIndex = 27
        Me.cmdDimensions.Text = "55048-Dimensjoner"
        Me.cmdDimensions.UseVisualStyleBackColor = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(13, 285)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(475, 1)
        Me.lblLine1.TabIndex = 57
        Me.lblLine1.Text = "Label1"
        '
        'frmDiffAccountDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(501, 322)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdDimensions)
        Me.Controls.Add(Me.txtCurrencyCode)
        Me.Controls.Add(Me.txtVATCode)
        Me.Controls.Add(Me.chkCashDiscountAccount)
        Me.Controls.Add(Me.chkUnmatchedAccount)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.CmdOK)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.cmbDiffType)
        Me.Controls.Add(Me.txtDiffPattern)
        Me.Controls.Add(Me.txtDiffText)
        Me.Controls.Add(Me.txtDiffAccountName)
        Me.Controls.Add(Me.lblCurrencyCode)
        Me.Controls.Add(Me.lblVATCode)
        Me.Controls.Add(Me.lbDiffType)
        Me.Controls.Add(Me.lblDiffPattern)
        Me.Controls.Add(Me.lblDiffType)
        Me.Controls.Add(Me.lblDiffAccountName)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDiffAccountDetail"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "60358 - Deviation Accounts"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents cmdDimensions As System.Windows.Forms.Button
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region
End Class
