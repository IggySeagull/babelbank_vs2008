<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmReportAdvanced
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents chkIncludeKID As System.Windows.Forms.CheckBox
    Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents cmdCancel As System.Windows.Forms.Button
    Public WithEvents txtHeading As System.Windows.Forms.TextBox
    Public WithEvents lblHeading As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportAdvanced))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkIncludeKID = New System.Windows.Forms.CheckBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.txtHeading = New System.Windows.Forms.TextBox
        Me.lblHeading = New System.Windows.Forms.Label
        Me.optRTF = New System.Windows.Forms.RadioButton
        Me.optTXT = New System.Windows.Forms.RadioButton
        Me.optPDF = New System.Windows.Forms.RadioButton
        Me.txtFilename = New System.Windows.Forms.TextBox
        Me.cmdFileOpen = New System.Windows.Forms.Button
        Me.chkLongDate = New System.Windows.Forms.CheckBox
        Me.cmdPrinter = New System.Windows.Forms.Button
        Me.lblFilename = New System.Windows.Forms.Label
        Me.fraExport = New System.Windows.Forms.GroupBox
        Me.lbleMail = New System.Windows.Forms.Label
        Me.txteMail = New System.Windows.Forms.TextBox
        Me.lblExplain = New System.Windows.Forms.Label
        Me.chkeMail = New System.Windows.Forms.CheckBox
        Me.chkExport = New System.Windows.Forms.CheckBox
        Me.chkPrint = New System.Windows.Forms.CheckBox
        Me.chkPreview = New System.Windows.Forms.CheckBox
        Me.optTIFF = New System.Windows.Forms.RadioButton
        Me.optXLS = New System.Windows.Forms.RadioButton
        Me.optHTML = New System.Windows.Forms.RadioButton
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lstReport = New System.Windows.Forms.ComboBox
        Me.lblSorting = New System.Windows.Forms.Label
        Me.lstSorting = New System.Windows.Forms.ComboBox
        Me.chkReportOnDatabase = New System.Windows.Forms.CheckBox
        Me.chkSaveBeforeReport = New System.Windows.Forms.CheckBox
        Me.lstReportWhen = New System.Windows.Forms.ComboBox
        Me.lblWhenToReport = New System.Windows.Forms.Label
        Me.cmdSQL = New System.Windows.Forms.Button
        Me.txtReportName = New System.Windows.Forms.TextBox
        Me.lblReportName = New System.Windows.Forms.Label
        Me.lblReportType = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.fraExport.SuspendLayout()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkIncludeKID
        '
        Me.chkIncludeKID.BackColor = System.Drawing.Color.Transparent
        Me.chkIncludeKID.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkIncludeKID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkIncludeKID.Location = New System.Drawing.Point(546, 81)
        Me.chkIncludeKID.Name = "chkIncludeKID"
        Me.chkIncludeKID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkIncludeKID.Size = New System.Drawing.Size(170, 30)
        Me.chkIncludeKID.TabIndex = 9
        Me.chkIncludeKID.Text = "60560 - Inkluder KID"
        Me.chkIncludeKID.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(526, 395)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 21
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(614, 395)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 4
        Me.cmdCancel.TabStop = False
        Me.cmdCancel.Text = "55002 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'txtHeading
        '
        Me.txtHeading.AcceptsReturn = True
        Me.txtHeading.BackColor = System.Drawing.SystemColors.Window
        Me.txtHeading.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHeading.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtHeading.Location = New System.Drawing.Point(332, 105)
        Me.txtHeading.MaxLength = 0
        Me.txtHeading.Name = "txtHeading"
        Me.txtHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHeading.Size = New System.Drawing.Size(200, 20)
        Me.txtHeading.TabIndex = 2
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(183, 105)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(145, 21)
        Me.lblHeading.TabIndex = 0
        Me.lblHeading.Text = "60277 - Rapportoverskrift"
        '
        'optRTF
        '
        Me.optRTF.BackColor = System.Drawing.SystemColors.Window
        Me.optRTF.Cursor = System.Windows.Forms.Cursors.Default
        Me.optRTF.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optRTF.Location = New System.Drawing.Point(86, 45)
        Me.optRTF.Name = "optRTF"
        Me.optRTF.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optRTF.Size = New System.Drawing.Size(70, 18)
        Me.optRTF.TabIndex = 14
        Me.optRTF.TabStop = True
        Me.optRTF.Text = "RTF"
        Me.optRTF.UseVisualStyleBackColor = False
        '
        'optTXT
        '
        Me.optTXT.BackColor = System.Drawing.SystemColors.Window
        Me.optTXT.Cursor = System.Windows.Forms.Cursors.Default
        Me.optTXT.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optTXT.Location = New System.Drawing.Point(161, 45)
        Me.optTXT.Name = "optTXT"
        Me.optTXT.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optTXT.Size = New System.Drawing.Size(70, 18)
        Me.optTXT.TabIndex = 15
        Me.optTXT.TabStop = True
        Me.optTXT.Text = "TXT"
        Me.optTXT.UseVisualStyleBackColor = False
        '
        'optPDF
        '
        Me.optPDF.BackColor = System.Drawing.SystemColors.Window
        Me.optPDF.Cursor = System.Windows.Forms.Cursors.Default
        Me.optPDF.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optPDF.Location = New System.Drawing.Point(14, 45)
        Me.optPDF.Name = "optPDF"
        Me.optPDF.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optPDF.Size = New System.Drawing.Size(70, 18)
        Me.optPDF.TabIndex = 13
        Me.optPDF.TabStop = True
        Me.optPDF.Text = "PDF"
        Me.optPDF.UseVisualStyleBackColor = False
        '
        'txtFilename
        '
        Me.txtFilename.AcceptsReturn = True
        Me.txtFilename.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilename.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilename.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilename.Location = New System.Drawing.Point(156, 69)
        Me.txtFilename.MaxLength = 180
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilename.Size = New System.Drawing.Size(310, 20)
        Me.txtFilename.TabIndex = 20
        '
        'cmdFileOpen
        '
        Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
        Me.cmdFileOpen.Location = New System.Drawing.Point(474, 67)
        Me.cmdFileOpen.Name = "cmdFileOpen"
        Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen.TabIndex = 17
        Me.cmdFileOpen.TabStop = False
        Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen.UseVisualStyleBackColor = False
        '
        'chkLongDate
        '
        Me.chkLongDate.BackColor = System.Drawing.Color.Transparent
        Me.chkLongDate.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkLongDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLongDate.Location = New System.Drawing.Point(546, 53)
        Me.chkLongDate.Name = "chkLongDate"
        Me.chkLongDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLongDate.Size = New System.Drawing.Size(170, 20)
        Me.chkLongDate.TabIndex = 8
        Me.chkLongDate.Text = "60515 - Bruk langt datoformat"
        Me.chkLongDate.UseVisualStyleBackColor = False
        '
        'cmdPrinter
        '
        Me.cmdPrinter.BackColor = System.Drawing.SystemColors.Control
        Me.cmdPrinter.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdPrinter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPrinter.Location = New System.Drawing.Point(546, 131)
        Me.cmdPrinter.Name = "cmdPrinter"
        Me.cmdPrinter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdPrinter.Size = New System.Drawing.Size(107, 21)
        Me.cmdPrinter.TabIndex = 26
        Me.cmdPrinter.TabStop = False
        Me.cmdPrinter.Text = "60275 - Skriveroppsett"
        Me.cmdPrinter.UseVisualStyleBackColor = False
        '
        'lblFilename
        '
        Me.lblFilename.BackColor = System.Drawing.SystemColors.Window
        Me.lblFilename.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilename.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilename.Location = New System.Drawing.Point(11, 71)
        Me.lblFilename.Name = "lblFilename"
        Me.lblFilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilename.Size = New System.Drawing.Size(133, 23)
        Me.lblFilename.TabIndex = 18
        Me.lblFilename.Text = "60274 - Filnavn for eksport"
        '
        'fraExport
        '
        Me.fraExport.BackColor = System.Drawing.SystemColors.Window
        Me.fraExport.Controls.Add(Me.lbleMail)
        Me.fraExport.Controls.Add(Me.txteMail)
        Me.fraExport.Controls.Add(Me.lblExplain)
        Me.fraExport.Controls.Add(Me.chkeMail)
        Me.fraExport.Controls.Add(Me.chkExport)
        Me.fraExport.Controls.Add(Me.chkPrint)
        Me.fraExport.Controls.Add(Me.chkPreview)
        Me.fraExport.Controls.Add(Me.optTIFF)
        Me.fraExport.Controls.Add(Me.optXLS)
        Me.fraExport.Controls.Add(Me.optHTML)
        Me.fraExport.Controls.Add(Me.optRTF)
        Me.fraExport.Controls.Add(Me.optTXT)
        Me.fraExport.Controls.Add(Me.optPDF)
        Me.fraExport.Controls.Add(Me.txtFilename)
        Me.fraExport.Controls.Add(Me.cmdFileOpen)
        Me.fraExport.Controls.Add(Me.lblFilename)
        Me.fraExport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraExport.Location = New System.Drawing.Point(172, 218)
        Me.fraExport.Name = "fraExport"
        Me.fraExport.Padding = New System.Windows.Forms.Padding(0)
        Me.fraExport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraExport.Size = New System.Drawing.Size(518, 165)
        Me.fraExport.TabIndex = 24
        Me.fraExport.TabStop = False
        '
        'lbleMail
        '
        Me.lbleMail.BackColor = System.Drawing.SystemColors.Window
        Me.lbleMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.lbleMail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbleMail.Location = New System.Drawing.Point(120, 98)
        Me.lbleMail.Name = "lbleMail"
        Me.lbleMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbleMail.Size = New System.Drawing.Size(33, 20)
        Me.lbleMail.TabIndex = 28
        Me.lbleMail.Text = "@"
        Me.lbleMail.Visible = False
        '
        'txteMail
        '
        Me.txteMail.AcceptsReturn = True
        Me.txteMail.BackColor = System.Drawing.SystemColors.Window
        Me.txteMail.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txteMail.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txteMail.Location = New System.Drawing.Point(156, 95)
        Me.txteMail.MaxLength = 255
        Me.txteMail.Name = "txteMail"
        Me.txteMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txteMail.Size = New System.Drawing.Size(310, 20)
        Me.txteMail.TabIndex = 27
        Me.txteMail.Visible = False
        '
        'lblExplain
        '
        Me.lblExplain.BackColor = System.Drawing.SystemColors.Window
        Me.lblExplain.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplain.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplain.Location = New System.Drawing.Point(11, 118)
        Me.lblExplain.Name = "lblExplain"
        Me.lblExplain.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplain.Size = New System.Drawing.Size(487, 40)
        Me.lblExplain.TabIndex = 26
        Me.lblExplain.Text = "Description"
        Me.lblExplain.Visible = False
        '
        'chkeMail
        '
        Me.chkeMail.BackColor = System.Drawing.Color.Transparent
        Me.chkeMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkeMail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkeMail.Location = New System.Drawing.Point(14, 94)
        Me.chkeMail.Name = "chkeMail"
        Me.chkeMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkeMail.Size = New System.Drawing.Size(85, 20)
        Me.chkeMail.TabIndex = 24
        Me.chkeMail.Text = "60032-eMail"
        Me.chkeMail.UseVisualStyleBackColor = False
        '
        'chkExport
        '
        Me.chkExport.BackColor = System.Drawing.Color.Transparent
        Me.chkExport.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkExport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkExport.Location = New System.Drawing.Point(354, 13)
        Me.chkExport.Name = "chkExport"
        Me.chkExport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkExport.Size = New System.Drawing.Size(150, 20)
        Me.chkExport.TabIndex = 23
        Me.chkExport.Text = "60273 - Eksport"
        Me.chkExport.UseVisualStyleBackColor = False
        '
        'chkPrint
        '
        Me.chkPrint.BackColor = System.Drawing.Color.Transparent
        Me.chkPrint.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkPrint.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkPrint.Location = New System.Drawing.Point(184, 13)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkPrint.Size = New System.Drawing.Size(150, 20)
        Me.chkPrint.TabIndex = 22
        Me.chkPrint.Text = "60271 - Send til skriver"
        Me.chkPrint.UseVisualStyleBackColor = False
        '
        'chkPreview
        '
        Me.chkPreview.BackColor = System.Drawing.Color.Transparent
        Me.chkPreview.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkPreview.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkPreview.Location = New System.Drawing.Point(14, 13)
        Me.chkPreview.Name = "chkPreview"
        Me.chkPreview.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkPreview.Size = New System.Drawing.Size(150, 20)
        Me.chkPreview.TabIndex = 21
        Me.chkPreview.Text = "60270 - Vis p� skjerm"
        Me.chkPreview.UseVisualStyleBackColor = False
        '
        'optTIFF
        '
        Me.optTIFF.BackColor = System.Drawing.SystemColors.Window
        Me.optTIFF.Cursor = System.Windows.Forms.Cursors.Default
        Me.optTIFF.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optTIFF.Location = New System.Drawing.Point(386, 45)
        Me.optTIFF.Name = "optTIFF"
        Me.optTIFF.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optTIFF.Size = New System.Drawing.Size(70, 18)
        Me.optTIFF.TabIndex = 18
        Me.optTIFF.TabStop = True
        Me.optTIFF.Text = "TIFF"
        Me.optTIFF.UseVisualStyleBackColor = False
        '
        'optXLS
        '
        Me.optXLS.BackColor = System.Drawing.SystemColors.Window
        Me.optXLS.Cursor = System.Windows.Forms.Cursors.Default
        Me.optXLS.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optXLS.Location = New System.Drawing.Point(311, 45)
        Me.optXLS.Name = "optXLS"
        Me.optXLS.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optXLS.Size = New System.Drawing.Size(70, 18)
        Me.optXLS.TabIndex = 17
        Me.optXLS.TabStop = True
        Me.optXLS.Text = "XLS"
        Me.optXLS.UseVisualStyleBackColor = False
        '
        'optHTML
        '
        Me.optHTML.BackColor = System.Drawing.SystemColors.Window
        Me.optHTML.Cursor = System.Windows.Forms.Cursors.Default
        Me.optHTML.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optHTML.Location = New System.Drawing.Point(236, 45)
        Me.optHTML.Name = "optHTML"
        Me.optHTML.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optHTML.Size = New System.Drawing.Size(70, 18)
        Me.optHTML.TabIndex = 16
        Me.optHTML.TabStop = True
        Me.optHTML.Text = "HTML"
        Me.optHTML.UseVisualStyleBackColor = False
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image1_0.Location = New System.Drawing.Point(139, 379)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(14, 16)
        Me._Image1_0.TabIndex = 29
        Me._Image1_0.TabStop = False
        '
        'lstReport
        '
        Me.lstReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.lstReport.FormattingEnabled = True
        Me.lstReport.Location = New System.Drawing.Point(332, 49)
        Me.lstReport.Name = "lstReport"
        Me.lstReport.Size = New System.Drawing.Size(200, 21)
        Me.lstReport.TabIndex = 0
        '
        'lblSorting
        '
        Me.lblSorting.BackColor = System.Drawing.SystemColors.Control
        Me.lblSorting.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSorting.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSorting.Location = New System.Drawing.Point(183, 131)
        Me.lblSorting.Name = "lblSorting"
        Me.lblSorting.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSorting.Size = New System.Drawing.Size(145, 21)
        Me.lblSorting.TabIndex = 32
        Me.lblSorting.Text = "60558 Sortering"
        '
        'lstSorting
        '
        Me.lstSorting.FormattingEnabled = True
        Me.lstSorting.Location = New System.Drawing.Point(332, 131)
        Me.lstSorting.Name = "lstSorting"
        Me.lstSorting.Size = New System.Drawing.Size(200, 21)
        Me.lstSorting.TabIndex = 3
        '
        'chkReportOnDatabase
        '
        Me.chkReportOnDatabase.BackColor = System.Drawing.Color.Transparent
        Me.chkReportOnDatabase.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkReportOnDatabase.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkReportOnDatabase.Location = New System.Drawing.Point(186, 187)
        Me.chkReportOnDatabase.Name = "chkReportOnDatabase"
        Me.chkReportOnDatabase.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkReportOnDatabase.Size = New System.Drawing.Size(233, 20)
        Me.chkReportOnDatabase.TabIndex = 10
        Me.chkReportOnDatabase.Text = "60561-Rapporter fra lagrede data"
        Me.chkReportOnDatabase.UseVisualStyleBackColor = False
        '
        'chkSaveBeforeReport
        '
        Me.chkSaveBeforeReport.BackColor = System.Drawing.Color.Transparent
        Me.chkSaveBeforeReport.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkSaveBeforeReport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkSaveBeforeReport.Location = New System.Drawing.Point(415, 187)
        Me.chkSaveBeforeReport.Name = "chkSaveBeforeReport"
        Me.chkSaveBeforeReport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkSaveBeforeReport.Size = New System.Drawing.Size(275, 20)
        Me.chkSaveBeforeReport.TabIndex = 11
        Me.chkSaveBeforeReport.Text = "60562-Lagre data f�r rapporten kj�res"
        Me.chkSaveBeforeReport.UseVisualStyleBackColor = False
        '
        'lstReportWhen
        '
        Me.lstReportWhen.FormattingEnabled = True
        Me.lstReportWhen.Location = New System.Drawing.Point(332, 159)
        Me.lstReportWhen.Name = "lstReportWhen"
        Me.lstReportWhen.Size = New System.Drawing.Size(200, 21)
        Me.lstReportWhen.TabIndex = 4
        '
        'lblWhenToReport
        '
        Me.lblWhenToReport.BackColor = System.Drawing.SystemColors.Control
        Me.lblWhenToReport.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWhenToReport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWhenToReport.Location = New System.Drawing.Point(183, 160)
        Me.lblWhenToReport.Name = "lblWhenToReport"
        Me.lblWhenToReport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWhenToReport.Size = New System.Drawing.Size(145, 21)
        Me.lblWhenToReport.TabIndex = 38
        Me.lblWhenToReport.Text = "60559 N�r skal rapporten kj�res"
        '
        'cmdSQL
        '
        Me.cmdSQL.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSQL.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSQL.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSQL.Location = New System.Drawing.Point(546, 158)
        Me.cmdSQL.Name = "cmdSQL"
        Me.cmdSQL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSQL.Size = New System.Drawing.Size(107, 21)
        Me.cmdSQL.TabIndex = 40
        Me.cmdSQL.TabStop = False
        Me.cmdSQL.Text = "SQL"
        Me.cmdSQL.UseVisualStyleBackColor = False
        '
        'txtReportName
        '
        Me.txtReportName.AcceptsReturn = True
        Me.txtReportName.BackColor = System.Drawing.SystemColors.Window
        Me.txtReportName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtReportName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtReportName.Location = New System.Drawing.Point(332, 77)
        Me.txtReportName.MaxLength = 0
        Me.txtReportName.Name = "txtReportName"
        Me.txtReportName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtReportName.Size = New System.Drawing.Size(200, 20)
        Me.txtReportName.TabIndex = 1
        '
        'lblReportName
        '
        Me.lblReportName.BackColor = System.Drawing.SystemColors.Control
        Me.lblReportName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReportName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblReportName.Location = New System.Drawing.Point(183, 77)
        Me.lblReportName.Name = "lblReportName"
        Me.lblReportName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReportName.Size = New System.Drawing.Size(145, 21)
        Me.lblReportName.TabIndex = 41
        Me.lblReportName.Text = "60557 - Rapportnavn"
        '
        'lblReportType
        '
        Me.lblReportType.BackColor = System.Drawing.SystemColors.Control
        Me.lblReportType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReportType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblReportType.Location = New System.Drawing.Point(183, 53)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReportType.Size = New System.Drawing.Size(145, 21)
        Me.lblReportType.TabIndex = 43
        Me.lblReportType.Text = "60556 - Rapporttype"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(159, 389)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(545, 1)
        Me.lblLine1.TabIndex = 79
        Me.lblLine1.Text = "Label1"
        '
        'frmReportAdvanced
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(714, 423)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.lblReportType)
        Me.Controls.Add(Me.txtReportName)
        Me.Controls.Add(Me.lblReportName)
        Me.Controls.Add(Me.cmdSQL)
        Me.Controls.Add(Me.lstReportWhen)
        Me.Controls.Add(Me.lblWhenToReport)
        Me.Controls.Add(Me.chkSaveBeforeReport)
        Me.Controls.Add(Me.chkReportOnDatabase)
        Me.Controls.Add(Me.lstSorting)
        Me.Controls.Add(Me.lblSorting)
        Me.Controls.Add(Me.lstReport)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.chkLongDate)
        Me.Controls.Add(Me.cmdPrinter)
        Me.Controls.Add(Me.fraExport)
        Me.Controls.Add(Me.chkIncludeKID)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.txtHeading)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmReportAdvanced"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "60287 - Avansert rapportoppsett"
        Me.fraExport.ResumeLayout(False)
        Me.fraExport.PerformLayout()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents optRTF As System.Windows.Forms.RadioButton
    Public WithEvents optTXT As System.Windows.Forms.RadioButton
    Public WithEvents optPDF As System.Windows.Forms.RadioButton
    Public WithEvents txtFilename As System.Windows.Forms.TextBox
    Public WithEvents cmdFileOpen As System.Windows.Forms.Button
    Public WithEvents chkLongDate As System.Windows.Forms.CheckBox
    Public WithEvents cmdPrinter As System.Windows.Forms.Button
    Public WithEvents lblFilename As System.Windows.Forms.Label
    Public WithEvents fraExport As System.Windows.Forms.GroupBox
    Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
    Friend WithEvents lstReport As System.Windows.Forms.ComboBox
    Public WithEvents optXLS As System.Windows.Forms.RadioButton
    Public WithEvents optHTML As System.Windows.Forms.RadioButton
    Public WithEvents optTIFF As System.Windows.Forms.RadioButton
    Public WithEvents lblSorting As System.Windows.Forms.Label
    Friend WithEvents lstSorting As System.Windows.Forms.ComboBox
    Public WithEvents chkReportOnDatabase As System.Windows.Forms.CheckBox
    Public WithEvents chkSaveBeforeReport As System.Windows.Forms.CheckBox
    Friend WithEvents lstReportWhen As System.Windows.Forms.ComboBox
    Public WithEvents lblWhenToReport As System.Windows.Forms.Label
    Public WithEvents cmdSQL As System.Windows.Forms.Button
    Public WithEvents txtReportName As System.Windows.Forms.TextBox
    Public WithEvents lblReportName As System.Windows.Forms.Label
    Public WithEvents lblReportType As System.Windows.Forms.Label
    Public WithEvents chkExport As System.Windows.Forms.CheckBox
    Public WithEvents chkPrint As System.Windows.Forms.CheckBox
    Public WithEvents chkPreview As System.Windows.Forms.CheckBox
    Public WithEvents lblExplain As System.Windows.Forms.Label
    Public WithEvents chkeMail As System.Windows.Forms.CheckBox
    Public WithEvents lbleMail As System.Windows.Forms.Label
    Public WithEvents txteMail As System.Windows.Forms.TextBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region
End Class
