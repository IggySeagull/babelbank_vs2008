<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmFilter
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents chkIncludeOCR As System.Windows.Forms.CheckBox
	Public WithEvents cmdAddEasyFilter As System.Windows.Forms.Button
	Public WithEvents txtFilterValue As System.Windows.Forms.TextBox
	Public WithEvents cmbFilterSelectField As System.Windows.Forms.ComboBox
	Public WithEvents cmdShowFilter As System.Windows.Forms.Button
	Public WithEvents cmdUpdateFilter As System.Windows.Forms.Button
	Public WithEvents cmdRemoveFilter As System.Windows.Forms.Button
	Public WithEvents lstFilter As System.Windows.Forms.ListBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents lblFilterValue As System.Windows.Forms.Label
	Public WithEvents lblFilterSelectField As System.Windows.Forms.Label
    Public WithEvents lblFilter As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkIncludeOCR = New System.Windows.Forms.CheckBox
        Me.cmdAddEasyFilter = New System.Windows.Forms.Button
        Me.txtFilterValue = New System.Windows.Forms.TextBox
        Me.cmbFilterSelectField = New System.Windows.Forms.ComboBox
        Me.cmdShowFilter = New System.Windows.Forms.Button
        Me.cmdUpdateFilter = New System.Windows.Forms.Button
        Me.cmdRemoveFilter = New System.Windows.Forms.Button
        Me.lstFilter = New System.Windows.Forms.ListBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.lblFilterValue = New System.Windows.Forms.Label
        Me.lblFilterSelectField = New System.Windows.Forms.Label
        Me.lblFilter = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'chkIncludeOCR
        '
        Me.chkIncludeOCR.BackColor = System.Drawing.SystemColors.Control
        Me.chkIncludeOCR.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkIncludeOCR.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkIncludeOCR.Location = New System.Drawing.Point(48, 187)
        Me.chkIncludeOCR.Name = "chkIncludeOCR"
        Me.chkIncludeOCR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkIncludeOCR.Size = New System.Drawing.Size(310, 19)
        Me.chkIncludeOCR.TabIndex = 13
        Me.chkIncludeOCR.Text = "60501 - Include OCR-payments"
        Me.chkIncludeOCR.UseVisualStyleBackColor = False
        '
        'cmdAddEasyFilter
        '
        Me.cmdAddEasyFilter.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddEasyFilter.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAddEasyFilter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAddEasyFilter.Location = New System.Drawing.Point(288, 148)
        Me.cmdAddEasyFilter.Name = "cmdAddEasyFilter"
        Me.cmdAddEasyFilter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAddEasyFilter.Size = New System.Drawing.Size(73, 21)
        Me.cmdAddEasyFilter.TabIndex = 12
        Me.cmdAddEasyFilter.TabStop = False
        Me.cmdAddEasyFilter.Text = "55018-Add"
        Me.cmdAddEasyFilter.UseVisualStyleBackColor = False
        '
        'txtFilterValue
        '
        Me.txtFilterValue.AcceptsReturn = True
        Me.txtFilterValue.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilterValue.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilterValue.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilterValue.Location = New System.Drawing.Point(200, 240)
        Me.txtFilterValue.MaxLength = 0
        Me.txtFilterValue.Name = "txtFilterValue"
        Me.txtFilterValue.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilterValue.Size = New System.Drawing.Size(160, 20)
        Me.txtFilterValue.TabIndex = 1
        '
        'cmbFilterSelectField
        '
        Me.cmbFilterSelectField.BackColor = System.Drawing.SystemColors.Window
        Me.cmbFilterSelectField.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbFilterSelectField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFilterSelectField.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbFilterSelectField.Location = New System.Drawing.Point(200, 214)
        Me.cmbFilterSelectField.Name = "cmbFilterSelectField"
        Me.cmbFilterSelectField.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbFilterSelectField.Size = New System.Drawing.Size(160, 21)
        Me.cmbFilterSelectField.TabIndex = 0
        '
        'cmdShowFilter
        '
        Me.cmdShowFilter.BackColor = System.Drawing.SystemColors.Control
        Me.cmdShowFilter.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdShowFilter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdShowFilter.Location = New System.Drawing.Point(288, 70)
        Me.cmdShowFilter.Name = "cmdShowFilter"
        Me.cmdShowFilter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdShowFilter.Size = New System.Drawing.Size(73, 21)
        Me.cmdShowFilter.TabIndex = 9
        Me.cmdShowFilter.TabStop = False
        Me.cmdShowFilter.Text = "55016-Show"
        Me.cmdShowFilter.UseVisualStyleBackColor = False
        '
        'cmdUpdateFilter
        '
        Me.cmdUpdateFilter.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUpdateFilter.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdUpdateFilter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdUpdateFilter.Location = New System.Drawing.Point(288, 122)
        Me.cmdUpdateFilter.Name = "cmdUpdateFilter"
        Me.cmdUpdateFilter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdUpdateFilter.Size = New System.Drawing.Size(73, 21)
        Me.cmdUpdateFilter.TabIndex = 8
        Me.cmdUpdateFilter.TabStop = False
        Me.cmdUpdateFilter.Text = "55023-Update"
        Me.cmdUpdateFilter.UseVisualStyleBackColor = False
        '
        'cmdRemoveFilter
        '
        Me.cmdRemoveFilter.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemoveFilter.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRemoveFilter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRemoveFilter.Location = New System.Drawing.Point(288, 96)
        Me.cmdRemoveFilter.Name = "cmdRemoveFilter"
        Me.cmdRemoveFilter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRemoveFilter.Size = New System.Drawing.Size(73, 21)
        Me.cmdRemoveFilter.TabIndex = 7
        Me.cmdRemoveFilter.TabStop = False
        Me.cmdRemoveFilter.Text = "55015-Remove"
        Me.cmdRemoveFilter.UseVisualStyleBackColor = False
        '
        'lstFilter
        '
        Me.lstFilter.BackColor = System.Drawing.SystemColors.Window
        Me.lstFilter.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstFilter.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstFilter.Location = New System.Drawing.Point(48, 72)
        Me.lstFilter.Name = "lstFilter"
        Me.lstFilter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstFilter.Size = New System.Drawing.Size(234, 95)
        Me.lstFilter.TabIndex = 2
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(290, 285)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 3
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(212, 285)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 5
        Me.CmdCancel.TabStop = False
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(133, 285)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 4
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "55001 - &Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'lblFilterValue
        '
        Me.lblFilterValue.BackColor = System.Drawing.SystemColors.Control
        Me.lblFilterValue.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilterValue.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilterValue.Location = New System.Drawing.Point(48, 241)
        Me.lblFilterValue.Name = "lblFilterValue"
        Me.lblFilterValue.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilterValue.Size = New System.Drawing.Size(153, 19)
        Me.lblFilterValue.TabIndex = 11
        Me.lblFilterValue.Text = "60438 - Fieldvalue"
        '
        'lblFilterSelectField
        '
        Me.lblFilterSelectField.BackColor = System.Drawing.SystemColors.Control
        Me.lblFilterSelectField.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilterSelectField.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilterSelectField.Location = New System.Drawing.Point(48, 215)
        Me.lblFilterSelectField.Name = "lblFilterSelectField"
        Me.lblFilterSelectField.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilterSelectField.Size = New System.Drawing.Size(154, 19)
        Me.lblFilterSelectField.TabIndex = 10
        Me.lblFilterSelectField.Text = "60437 - Select from field"
        '
        'lblFilter
        '
        Me.lblFilter.BackColor = System.Drawing.SystemColors.Control
        Me.lblFilter.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilter.Location = New System.Drawing.Point(48, 52)
        Me.lblFilter.Name = "lblFilter"
        Me.lblFilter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilter.Size = New System.Drawing.Size(173, 14)
        Me.lblFilter.TabIndex = 6
        Me.lblFilter.Text = "60454 - Filter"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 276)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(360, 1)
        Me.lblLine1.TabIndex = 58
        Me.lblLine1.Text = "Label1"
        '
        'frmFilter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(385, 321)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.chkIncludeOCR)
        Me.Controls.Add(Me.cmdAddEasyFilter)
        Me.Controls.Add(Me.txtFilterValue)
        Me.Controls.Add(Me.cmbFilterSelectField)
        Me.Controls.Add(Me.cmdShowFilter)
        Me.Controls.Add(Me.cmdUpdateFilter)
        Me.Controls.Add(Me.cmdRemoveFilter)
        Me.Controls.Add(Me.lstFilter)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.lblFilterValue)
        Me.Controls.Add(Me.lblFilterSelectField)
        Me.Controls.Add(Me.lblFilter)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(372, 30)
        Me.Name = "frmFilter"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "60454 - Filter"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
