﻿Imports System.Threading
Module lrs_Setup
    ' Johannes# 
    Public Function LRS(ByVal StringIndex As Long, Optional ByVal sString1 As String = vbNullString, Optional ByVal sString2 As String = vbNullString, Optional ByVal sString3 As String = vbNullString) As String
        Dim sReturnString As String
        Dim sLanguage As String
        Dim sNorwegian As String = ""
        Dim sEnglish As String = ""
        Dim sSwedish As String = ""
        Dim sDanish As String = ""
        Dim bUseNorwegian As Boolean = False
        Dim bUseDanish As Boolean = False
        Dim bUseSwedish As Boolean = False
        Dim sTmp As String
        Dim oFS As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Static sLanguageFromFile As String = ""

        sReturnString = ""
        'sLanguage = Thread.CurrentThread.CurrentCulture.ToString

        sTmp = System.Configuration.ConfigurationManager.AppSettings("Language")

        If sTmp = "" Then
            sLanguage = Thread.CurrentThread.CurrentCulture.ToString
        Else
            sLanguage = sTmp
        End If
        ' Check if we have "Language.txt" file with Language code set in file.
        If sLanguageFromFile = "" Then
            ' check languagefile
            oFS = New Scripting.FileSystemObject
            If oFS.FileExists(My.Application.Info.DirectoryPath & "\language.txt") Then
                ' file found, import
                oFile = oFS.OpenTextFile(My.Application.Info.DirectoryPath & "\language.txt", Scripting.IOMode.ForReading)
                sLanguageFromFile = oFile.ReadAll
                oFile.Close()
                oFile = Nothing
                oFS = Nothing
            Else
                sLanguageFromFile = "no file"
            End If
        End If
        If sLanguageFromFile <> "no file" Then
            If sLanguageFromFile = "406" Then
                sLanguage = "da"
            ElseIf sLanguageFromFile = "41d" Then
                sLanguage = "se"
            ElseIf sLanguageFromFile = "414" Then
                sLanguage = "nn-NO"
            ElseIf sLanguageFromFile = "809" Then
                sLanguage = "en"
            End If
        End If


        If sLanguage = "nb-NO" Or sLanguage = "nn-NO" Then
            bUseNorwegian = True
        ElseIf sLanguage = "da" Then
            bUseDanish = True
        ElseIf sLanguage = "se" Then
            bUseSwedish = True
        Else
            bUseNorwegian = False
        End If

        Select Case StringIndex
            Case 10061
                sNorwegian = "10061: Du har ikke lisens til å legge inn nye formater"
                sEnglish = "10061: Licensed number of formats is overrun!"
                sSwedish = "10061: Du har ingen licens för att ange nya format"
                sDanish = "10061: Du har ikke licens til at tilføje nye formater"
            Case 19504
                sNorwegian = "19504: Ugyldig %1 angitt."
                sEnglish = "19504: Invalid %1 specified."
                sSwedish = "19504: Ogiltigt %1 har angetts."
                sDanish = "19504: Ugyldig %1 angivet."
            Case 25000
                sNorwegian = "------ RESERVED FOR UTILS-STRINGS ----"
                sEnglish = "------ RESERVED FOR UTILS-STRINGS ----"
                sSwedish = "------ RESERVED FOR UTILS-STRINGS ----"
                sDanish = "------ RESERVED FOR UTILS-STRINGS ----"
            Case 25001
                sNorwegian = "25001: Kunne ikke koble til ekstern database."
                sEnglish = "25001: Couldn't connect to external database."
                sSwedish = "25001: Kunde inte ansluta till extern databas."
                sDanish = "25001: Kunne ikke koble til ekstern database."
            Case 25002
                sNorwegian = "25002: Påloggingen til databasen rapporterte følgende feil:"
                sEnglish = "25002: The connection reported the following error:"
                sSwedish = "25002: Inloggningen på databasen genererade följande fel:"
                sDanish = "25002: Pålogningen på databasen rapporterede følgende fejl:"
            Case 25003
                sNorwegian = "25003: Feil under oppbygning av påloggingsstreng."
                sEnglish = "25003: Error during the building of the connectionstring."
                sSwedish = "25003: Fel under konstruktion av inloggningssträngen."
                sDanish = "25003: Fejl under opbygning af pålogningsstreng."
            Case 25004
                sNorwegian = "Påloggingsstreng:"
                sEnglish = "Connectionstring:"
                sSwedish = "Inloggningssträng:"
                sDanish = "Pålogningsstreng:"
            Case 25005
                sNorwegian = "For å teste/endre påloggingsstreng, gå inn på Firmaopplysninger under Oppsett i BabelBank."
                sEnglish = "To test/change the connectionstring, start BabelBank and select Company under Setup in the menu."
                sSwedish = "Testa/ändra inloggningssträngen i företagsinformationen under Vyer i BabelBank."
                sDanish = "For at teste/ændre pålogningsstreng, gå ind på Firmaoplysninger under Opsætning i BabelBank."
            Case 55000
                sNorwegian = "--- Buttons ----"
                sEnglish = "--- Buttons ---"
                sSwedish = "--- Buttons ----"
                sDanish = "--- Buttons ----"
            Case 55001
                sNorwegian = "&Hjelp"
                sEnglish = "&Help"
                sSwedish = "&Hjälp"
                sDanish = "&Hjælp"
            Case 55002
                sNorwegian = "&Avbryt"
                sEnglish = "&Cancel"
                sSwedish = "&Avbryt"
                sDanish = "&Afbryd"
            Case 55003
                sNorwegian = "&Forrige"
                sEnglish = "&Previous"
                sSwedish = "&Föregående"
                sDanish = "&Forrige"
            Case 55004
                sNorwegian = "&Neste"
                sEnglish = "&Next"
                sSwedish = "&Nästa"
                sDanish = "&Næste"
            Case 55005
                sNorwegian = "F&ullfør"
                sEnglish = "&Finish"
                sSwedish = "Slu&tför"
                sDanish = "U&dfør"
            Case 55006
                sNorwegian = "&Samme format flere ganger"
                sEnglish = "&Same format more than once"
                sSwedish = "&Samma format flera gånger"
                sDanish = "&Samme format flere gange"
            Case 55007
                sNorwegian = "&Velg format fra annen profil"
                sEnglish = "S&elect format from another profile"
                sSwedish = "&Välj format från annan profil"
                sDanish = "&Vælg format fra anden profil"
            Case 55008
                sNorwegian = "&Avansert oppsett"
                sEnglish = "&Advanced options"
                sSwedish = "&Avanc. inställningar"
                sDanish = "&Avanceret opsætning"
            Case 55009
                sNorwegian = "&Sekvensnumre"
                sEnglish = "&Sequenceno's"
                sSwedish = "&Sekvensnummer"
                sDanish = "&Sekvensnumre"
            Case 55010
                sNorwegian = "&Test"
                sEnglish = "&Test"
                sSwedish = "&Test"
                sDanish = "&Test"
            Case 55011
                sNorwegian = "&Lagre"
                sEnglish = "&Save"
                sSwedish = "&Spara"
                sDanish = "&Gemme"
            Case 55012
                sNorwegian = "&Ny"
                sEnglish = "&New"
                sSwedish = "&Nytt"
                sDanish = "&Ny"
            Case 55013
                sNorwegian = "Legg til fil"
                sEnglish = "Add file"
                sSwedish = "Lägg till fil"
                sDanish = "Tilføj fil"
            Case 55014
                sNorwegian = "Fjern fil"
                sEnglish = "Remove file"
                sSwedish = "Ta bort fil"
                sDanish = "Fjern fil"
            Case 55015
                sNorwegian = "&Slett"
                sEnglish = "&Delete"
                sSwedish = "&Ta bort"
                sDanish = "&Slet"
            Case 55016
                sNorwegian = "Vis"
                sEnglish = "Show"
                sSwedish = "Visa"
                sDanish = "Vis"
            Case 55017
                sNorwegian = "Legg til (...)"
                sEnglish = "Add (...)"
                sSwedish = "Lägg till (...)"
                sDanish = "Tilføj (...)"
            Case 55018
                sNorwegian = "Legg til"
                sEnglish = "Add"
                sSwedish = "Lägg till"
                sDanish = "Tilføj"
            Case 55019
                sNorwegian = "Velg"
                sEnglish = "Select"
                sSwedish = "Välj"
                sDanish = "Vælg"
            Case 55020
                sNorwegian = "&Mail/Print"
                sEnglish = "&Mail/Print"
                sSwedish = "&E-posta/skriv ut"
                sDanish = "&Mail/Udskriv"
            Case 55021
                sNorwegian = "&Slå sammen"
                sEnglish = "M&erge"
                sSwedish = "&Slå ihop"
                sDanish = "&Slå sammen"
            Case 55022
                sNorwegian = "Beløpsavvik"
                sEnglish = "Amountdev."
                sSwedish = "Beloppsavvikelse"
                sDanish = "Beløbsafvigelse"
            Case 55023
                sNorwegian = "&Oppdater"
                sEnglish = "&Update"
                sSwedish = "&Uppdatera"
                sDanish = "&Opdater"
            Case 55024
                sNorwegian = "&Koder Norge"
                sEnglish = "&Codes"
                sSwedish = "&Koder Norge"
                sDanish = "&Koder Norge"
            Case 55025
                sNorwegian = "Bruk"
                sEnglish = "Use"
                sSwedish = "Använd"
                sDanish = "Brug"
            Case 55040
                sNorwegian = "KID-oppsett"
                sEnglish = "KID Setup"
                sSwedish = "OCR-inställning"
                sDanish = "KID-opsætning"
            Case 55041
                sNorwegian = "Beskrivelse"
                sEnglish = "Description"
                sSwedish = "Beskrivning"
                sDanish = "Beskrivelse"
            Case 55042
                sNorwegian = "&Legg til kriterie"
                sEnglish = "Add &criteria"
                sSwedish = "&Lägg till kriterier"
                sDanish = "&Tilføj kriterie"
            Case 55043
                sNorwegian = "&Bruk/Lagre"
                sEnglish = "&Use/Save"
                sSwedish = "&Använd/Spara"
                sDanish = "&Brug/Gem"
            Case 55044
                sNorwegian = "Slett &kriteria"
                sEnglish = "Remove &criteria"
                sSwedish = "Ta bort &kriterier"
                sDanish = "Slet &kriterier"
            Case 55045
                sNorwegian = "&Omkostninger"
                sEnglish = "&Charges"
                sSwedish = "och &kostnader"
                sDanish = "&Omkostninger"
            Case 55046
                sNorwegian = "&Mappingfil"
                sEnglish = "&Mappingfile"
                sSwedish = "&Mappningsfil"
                sDanish = "&Mappingfil"
            Case 55047
                sNorwegian = "&Valutafelt"
                sEnglish = "&Currencyfield"
                sSwedish = "&Valutafält"
                sDanish = "&Valutafelt"
            Case 55048
                sNorwegian = "&Dimensjoner"
                sEnglish = "&Dimensions"
                sSwedish = "&Dimensioner"
                sDanish = "&Dimensioner"
            Case 55049
                sNorwegian = "&Dimensjon"
                sEnglish = "&Dimensions"
                sSwedish = "&Dimension"
                sDanish = "&Dimension"
            Case 55050
                sNorwegian = "Sikkerhet"
                sEnglish = "Security"
                sSwedish = "Säkerhet"
                sDanish = "Sikkerhed"

                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 55060
            Case 55060
                sNorwegian = "&Avslutt"
                sEnglish = "&End"
                sSwedish = "&Avslut"
                sDanish = "&Afslut"
            Case 55061
                sNorwegian = "&Fjern"
                sEnglish = "&Remove"
                sSwedish = "&Ta bort"
                sDanish = "&Fjern"


            Case 59000
                sNorwegian = "--- Format-labels ---"
                sEnglish = "--- Format labels ---"
                sSwedish = "--- Format-labels ---"
                sDanish = "--- Format-labels ---"
            Case 59001
                sNorwegian = "OCR-fil"
                sEnglish = "OCR-file"
                sSwedish = "OCR-fil"
                sDanish = "OCR-fil"
            Case 59002
                sNorwegian = "Autogiro inn"
                sEnglish = "Autogiro in"
                sSwedish = "Autogiro in"
                sDanish = "Autogiro ind"
            Case 59003
                sNorwegian = "Autogiro retur"
                sEnglish = "Autogiro return"
                sSwedish = "Autogiro retur"
                sDanish = "Autogiro retur"
            Case 59004
                sNorwegian = "Dir.rem inn"
                sEnglish = "Dir.rem in"
                sSwedish = "Dir.rem in"
                sDanish = "Dir.bet ind"
            Case 59005
                sNorwegian = "Dir.rem retur"
                sEnglish = "Dir.rem return"
                sSwedish = "Dir.rem retur"
                sDanish = "Dir.bet retur"
            Case 59006
                sNorwegian = "Telepay innsending"
                sEnglish = "Telepay in"
                sSwedish = "Telepay-insändning"
                sDanish = "Telepay indsendelse"
            Case 59007
                sNorwegian = "Mottaksretur"
                sEnglish = "Receipt reply"
                sSwedish = "Mottagningskvitto"
                sDanish = "Modtagelsesretur"
            Case 59008
                sNorwegian = "Avregningsretur"
                sEnglish = "Processing reply"
                sSwedish = "Avräkningsretur"
                sDanish = "Afregningsretur"
            Case 59009
                sNorwegian = "Avvisningsretur"
                sEnglish = "Rejection reply"
                sSwedish = "Avvisningsretur"
                sDanish = "Afvisningsretur"
            Case 59010
                sNorwegian = "Coda-fil"
                sEnglish = "Coda file"
                sSwedish = "Coda-fil"
                sDanish = "Coda-fil"
            Case 59011
                sNorwegian = "Autogirofil fra Capsy"
                sEnglish = "Autogirofile from Capsy"
                sSwedish = "Autogirofil från Capsy"
                sDanish = "Autogirofil fra Capsy"
            Case 59012
                sNorwegian = "Autogirofil til Capsy"
                sEnglish = "Autogirofile to Capsy"
                sSwedish = "Autogirofil till Capsy"
                sDanish = "Autogirofil til Capsy"
            Case 59013
                sNorwegian = "Utbetalingsfil fra Excel"
                sEnglish = "Paymentfile from Excel"
                sSwedish = "Utbetalningsfil från Excel"
                sDanish = "Udbetalingsfil fra Excel"
            Case 59014
                sNorwegian = "Utbetalingsfil til Excel"
                sEnglish = "Paymentfile to Excel"
                sSwedish = "Utbetalningsfil till Excel"
                sDanish = "Udbetalingsfil til Excel"
            Case 59015
                sNorwegian = "CREMUL-fil"
                sEnglish = "CREMUL-file"
                sSwedish = "CREMUL-fil"
                sDanish = "CREMUL-fil"
            Case 59016
                sNorwegian = "PAYMUL-fil"
                sEnglish = "PAYMUL-file"
                sSwedish = "PAYMUL-fil"
                sDanish = "PAYMUL-fil"
            Case 59017
                sNorwegian = "BANSTA-fil"
                sEnglish = "BANSTA-file"
                sSwedish = "BANSTA-fil"
                sDanish = "BANSTA-fil"
            Case 59018
                sNorwegian = "DEBMUL-fil"
                sEnglish = "DEBMUL-file"
                sSwedish = "DEBMUL-fil"
                sDanish = "DEBMUL-fil"
            Case 59019
                sNorwegian = "CSV-fil"
                sEnglish = "CSV-file"
                sSwedish = "CSV-fil"
                sDanish = "CSV-fil"
            Case 59020
                sNorwegian = "Fil til bank"
                sEnglish = "File to bank"
                sSwedish = "Fill till bank"
                sDanish = "Fil til bank"
            Case 59021
                sNorwegian = "Fil fra bank"
                sEnglish = "File from bank"
                sSwedish = "Fil från bank"
                sDanish = "Fil fra bank"
            Case 59022
                sNorwegian = "Filnavn/sti for databasen"
                sEnglish = "Filename/Path for database"
                sSwedish = "Filnamn/sökväg till databasen"
                sDanish = "Filnavn/sti for databasen"
            Case 59023
                sNorwegian = "Fil fra regnskap"
                sEnglish = "File from accounting system"
                sSwedish = "Fil från redovisning"
                sDanish = "Fil fra regnskab"
            Case 59024
                sNorwegian = "Bet. med KID"
                sEnglish = "Payments w/KID"
                sSwedish = "Bet. med OCR"
                sDanish = "Bet. med KID"
            Case 59025
                sNorwegian = "Bet. uten KID"
                sEnglish = "Payments without KID"
                sSwedish = "Bet. utan OCR"
                sDanish = "Bet. uden KID"
            Case 59026
                sNorwegian = "HDP-fil"
                sEnglish = "HDP-File"
                sSwedish = "HDP-fil"
                sDanish = "HDP-fil"
            Case 59027
                sNorwegian = "Fil fra Citibank"
                sEnglish = "File from Citibank"
                sSwedish = "Fil från Citibank"
                sDanish = "Fil fra Citibank"
            Case 59028
                sNorwegian = "Fil til Citibank"
                sEnglish = "File to Citibank"
                sSwedish = "Fil till Citibank"
                sDanish = "Fil til Citibank"
            Case 59029
                sNorwegian = "ShipNet fil"
                sEnglish = "ShipNet file"
                sSwedish = "ShipNet-fil"
                sDanish = "ShipNet fil"
            Case 59030
                sNorwegian = "Innbetalingsfil"
                sEnglish = "Incoming payments"
                sSwedish = "Inbetalningsfil"
                sDanish = "Indbetalingsfil"
            Case 59031
                sNorwegian = "Nattsafefil"
                sEnglish = "DropboxFile"
                sSwedish = "Nattsafe-fil"
                sDanish = "Natsafefil"
            Case 59032
                sNorwegian = "Kontoinfo.fil"
                sEnglish = "Statementfile"
                sSwedish = "Kontoinfo.fil"
                sDanish = "Kontoinfo.fil"
            Case 59033
                sNorwegian = "Hovedboksfil"
                sEnglish = "GL file"
                sSwedish = "Huvudboksfil"
                sDanish = "Hovedboksfil"
            Case 59034
                sNorwegian = "Fil til regnskap"
                sEnglish = "File to ERP system"
                sSwedish = "Fil till redovisning"
                sDanish = "Fil til regnskab"
            Case 59035
                sNorwegian = "&Filter"
                sEnglish = "&Filter"
                sSwedish = "&Filter"
                sDanish = "&Filter"
            Case 59036
                sNorwegian = "Aktiver testkode"
                sEnglish = "Activate testcode"
                sSwedish = "Aktivera testkod"
                sDanish = "Aktiver testkode"
            Case 59037
                sNorwegian = "Debugkode"
                sEnglish = "Debugcode"
                sSwedish = "Felsökningskod"
                sDanish = "Debugkode"
            Case 59038
                sNorwegian = "Strukturert fakturanummer"
                sEnglish = "Structrured invoicenumber"
                sSwedish = "Strukturerat fakturanummer"
                sDanish = "Struktureret fakturanummer"
            Case 59039
                sNorwegian = "Aquariusfil"
                sEnglish = "Aquariusfile"
                sSwedish = "Aquariusfil"
                sDanish = "Aquariusfil"
            Case 59040
                sNorwegian = "Pålogging SIRI"
                sEnglish = "Logonstring SIRI"
                sSwedish = "Inloggning SIRI"
                sDanish = "Pålogning SIRI"
            Case 59041
                sNorwegian = "Pålogging Oracle"
                sEnglish = "Logonstring Oracle"
                sSwedish = "Inloggning Oracle"
                sDanish = "Pålogning Oracle"
            Case 59042
                sNorwegian = "Fakturafil"
                sEnglish = "Invoicefile"
                sSwedish = "Fakturafil"
                sDanish = "Fakturafil"
            Case 59043
                sNorwegian = "Kundefil"
                sEnglish = "Customerfile"
                sSwedish = "Kundfil"
                sDanish = "Kundefil"
            Case 59044
                sNorwegian = "Lockboxfil"
                sEnglish = "Lockboxfile"
                sSwedish = "Betalningsfil"
                sDanish = "Lockboxfil"
            Case 59045
                sNorwegian = "Fastformatfil"
                sEnglish = "Fixedlengt format"
                sSwedish = "Fastformatfil"
                sDanish = "Fastformatfil"
            Case 59046
                sNorwegian = "CONTRL-fil"
                sEnglish = "CONTRL-file"
                sSwedish = "CONTRL-fil"
                sDanish = "CONTRL-fil"
            Case 59047
                sNorwegian = "TAB-separert fil"
                sEnglish = "TAB-separated file"
                sSwedish = "TABB-avgränsad fil"
                sDanish = "TAB-separeret fil"
            Case 59048
                sNorwegian = "Fastformat fil"
                sEnglish = "Fixed format file"
                sSwedish = "Fastformatfil"
                sDanish = "Fastformat fil"
            Case 59049
                sNorwegian = "Tilkobling/Spørring"
                sEnglish = "Connection/Queryname"
                sSwedish = "Anslutning/fråga"
                sDanish = "Tilkobling/forespørgsel"
            Case 59050
                sNorwegian = "Lockbox betalinger"
                sEnglish = "Lockbox payments"
                sSwedish = "Betalningsfilbetalningar"
                sDanish = "Lockbox betalinger"
            Case 59051
                sNorwegian = "ACH betalinger"
                sEnglish = "ACH payments"
                sSwedish = "ACH-betalningar"
                sDanish = "ACH betalinger"
            Case 59052
                sNorwegian = "MT940 betalinger"
                sEnglish = "MT940 payments"
                sSwedish = "MT940-betalningar"
                sDanish = "MT940 betalinger"
            Case 59053
                sNorwegian = "Pain.002-fil"
                sEnglish = "Pain.002-file"
                sSwedish = "Pain.002-fil"
                sDanish = "Pain.002-fil"
            Case 59054
                sNorwegian = "COACSU-fil"
                sEnglish = "COACSU-file"
                sSwedish = "COACSU-fil"
                sDanish = "COACSU-fil"
            Case 59055
                sNorwegian = "Fil innland"
                sEnglish = "File domestic"
                sSwedish = "Fil inland."
                sDanish = "Fil indland"
            Case 59056
                sNorwegian = "Fil utland"
                sEnglish = "File international"
                sSwedish = "Fil utland."
                sDanish = "Fil udland"
                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 59060

            Case 60000
                sNorwegian = "--- Screen ----"
                sEnglish = "--- Screen ---"
                sSwedish = "--- Screen ----"
                sDanish = "--- Screen ----"
            Case 60001
                sNorwegian = "Profilnavn"
                sEnglish = "Profilename"
                sSwedish = "Profilnamn"
                sDanish = "Profilnavn"
            Case 60002
                sNorwegian = "Velg filnavn for"
                sEnglish = "Select filename for"
                sSwedish = "Välj filnamn för"
                sDanish = "Vælg filnavn for"
            Case 60003
                sNorwegian = "Send"
                sEnglish = "Send"
                sSwedish = "Skicka"
                sDanish = "Send"
            Case 60004
                sNorwegian = "Retur"
                sEnglish = "Return"
                sSwedish = "Retur"
                sDanish = "Retur"
            Case 60005
                sNorwegian = "Avslutt wizard ?"
                sEnglish = "Cancel wizard ?"
                sSwedish = "Avsluta guiden ?"
                sDanish = "Afslut wizard ?"
            Case 60006
                sNorwegian = "Du må velge et format"
                sEnglish = "Please select a format"
                sSwedish = "Du måste välja ett format"
                sDanish = "Du skal vælge et format"
            Case 60007
                sNorwegian = "Du må velge et filnavn"
                sEnglish = "Please select a filename"
                sSwedish = "Du måste välja ett filnamn"
                sDanish = "Du skal vælge et filnavn"
            Case 60008
                sNorwegian = "Ingen bank"
                sEnglish = "No bank"
                sSwedish = "Ingen bank"
                sDanish = "Ingen bank"
            Case 60009
                sNorwegian = "Velg katalog"
                sEnglish = "Select path"
                sSwedish = "Välj katalog"
                sDanish = "Vælg katalog"
            Case 60010
                sNorwegian = "Velg fil"
                sEnglish = "Select file"
                sSwedish = "Välj fil"
                sDanish = "Vælg fil"
            Case 60011
                sNorwegian = "Vil du avslutte uten lagring ?"
                sEnglish = "Cancel without saving"
                sSwedish = "Vill du avsluta utan att spara ?"
                sDanish = "Vil du afslutte uden at gemme?"
            Case 60012
                sNorwegian = "Konti"
                sEnglish = "Accounts"
                sSwedish = "konton"
                sDanish = "Konti"
            Case 60013
                sNorwegian = "Klikk først på den konto du vil slette"
                sEnglish = "Select account to delete"
                sSwedish = "Klicka först på kontot som du vill ta bort"
                sDanish = "Klik først på den konto du vil slette"
            Case 60014
                sNorwegian = "Vil du slette konto"
                sEnglish = "Do You want to delete account"
                sSwedish = "Vill du ta bort kontot"
                sDanish = "Vil du slette kontoen"
            Case 60015
                sNorwegian = "Slette konto"
                sEnglish = "Delete account"
                sSwedish = "Ta bort konto"
                sDanish = "Slette konto"
            Case 60016
                sNorwegian = "Klienter"
                sEnglish = "Clients"
                sSwedish = "Klienter"
                sDanish = "Klienter"
            Case 60017
                sNorwegian = "Endre"
                sEnglish = "Change"
                sSwedish = "Ändra"
                sDanish = "Ændre"
            Case 60018
                sNorwegian = "Klikk først på den klienten du vil slette"
                sEnglish = "Select client to delete"
                sSwedish = "Klicka först på klienten som du vill ta bort"
                sDanish = "Klik først på den klient, du vil slette"
            Case 60019
                sNorwegian = "Vil du slette klient"
                sEnglish = "Do You want to delete client"
                sSwedish = "Vill du ta bort klienten"
                sDanish = "Vil du slette klienten"
            Case 60020
                sNorwegian = "Slette klient"
                sEnglish = "Delete client"
                sSwedish = "Ta bort klient"
                sDanish = "Slette klient"
            Case 60021
                sNorwegian = "Varsling"
                sEnglish = "Notification"
                sSwedish = "Avisering"
                sDanish = "Varsling"
            Case 60022
                sNorwegian = "Klikk først på den varsling du vil slette"
                sEnglish = "Select warning to delete"
                sSwedish = "Klicka först på aviseringen som du vill ta bort"
                sDanish = "Klik først på den varsling du vil slette"
            Case 60023
                sNorwegian = "Vil du slette varsling"
                sEnglish = "Do You want to delete warning"
                sSwedish = "Vill du ta bort aviseringen"
                sDanish = "Vil du slette varslingen"
            Case 60024
                sNorwegian = "Slette varsling"
                sEnglish = "Delete warning"
                sSwedish = "Ta bort aviseringen"
                sDanish = "Slette varsling"
            Case 60025
                sNorwegian = "Profiler"
                sEnglish = "Profiles"
                sSwedish = "Profiler"
                sDanish = "Profiler"
            Case 60026
                sNorwegian = "Konti for klient"
                sEnglish = "Accounts for client"
                sSwedish = "Konton för kunden"
                sDanish = "Konti for klient"
            Case 60027
                sNorwegian = "Klient-ID"
                sEnglish = "Client-ID"
                sSwedish = "Klient-ID"
                sDanish = "Klient-ID"
            Case 60028
                sNorwegian = "Klientnavn"
                sEnglish = "Clientname"
                sSwedish = "klientnamn"
                sDanish = "Klientnavn"
            Case 60029
                sNorwegian = "Sendeprofil"
                sEnglish = "Sendprofile"
                sSwedish = "Sändningsprofil"
                sDanish = "Sendeprofil"
            Case 60030
                sNorwegian = "Returprofil"
                sEnglish = "Returnprofile"
                sSwedish = "Returprofil"
                sDanish = "Returprofil"
            Case 60031
                sNorwegian = "Navn"
                sEnglish = "Name"
                sSwedish = "Namn"
                sDanish = "Navn"
            Case 60032
                sNorwegian = "E-Mail"
                sEnglish = "E-Mail"
                sSwedish = "E-post"
                sDanish = "E-mail"
            Case 60033
                sNorwegian = "Fyller opp med max. 3 varslinger"
                sEnglish = "Fills with max. 3 warnings"
                sSwedish = "Fyller upp med max 3 aviseringar"
                sDanish = "Fylder op med maks. 3 varslinger"
            Case 60034
                sNorwegian = "Klientnr."
                sEnglish = "ClientNo"
                sSwedish = "Klientnr."
                sDanish = "Klientnr."
            Case 60035
                sNorwegian = "Mailadresse"
                sEnglish = "Mailadress"
                sSwedish = "E-postadress"
                sDanish = "Mailadresse"
            Case 60036
                sNorwegian = "Kontonummer"
                sEnglish = "AccountNumber"
                sSwedish = "Kontonummer"
                sDanish = "Kontonummer"
            Case 60037
                sNorwegian = "Klientoversikt"
                sEnglish = "Clientslist"
                sSwedish = "Klientöversikt"
                sDanish = "Klientoversigt"
            Case 60038
                sNorwegian = "Legg inn firmainformasjon"
                sEnglish = "Companyinformation"
                sSwedish = "Lägg till företagsinformation"
                sDanish = "Indtast firmainformation"
            Case 60039
                sNorwegian = "Firma:"
                sEnglish = "Company"
                sSwedish = "Företag:"
                sDanish = "Firma:"
            Case 60040
                sNorwegian = "Adresse:"
                sEnglish = "Address"
                sSwedish = "Adress:"
                sDanish = "Adresse:"
            Case 60041
                sNorwegian = "Postnummer:"
                sEnglish = "Zipcode"
                sSwedish = "Postnummer:"
                sDanish = "Postnummer:"
            Case 60042
                sNorwegian = "Poststed:"
                sEnglish = "City"
                sSwedish = "Ort:"
                sDanish = "By:"
            Case 60043
                sNorwegian = "Foretaksnummer"
                sEnglish = "CompanyNo"
                sSwedish = "Företagsnummer"
                sDanish = "Virksomhedsnummer"
            Case 60044
                sNorwegian = "Kunde-ID:"
                sEnglish = "Customer-ID"
                sSwedish = "Kund-ID:"
                sDanish = "Kunde-ID:"
            Case 60045
                sNorwegian = "Backup:"
                sEnglish = "Backup"
                sSwedish = "Säkerhetskopia:"
                sDanish = "Backup:"
            Case 60046
                sNorwegian = "Slett etter antall dager:"
                sEnglish = "Delete after no of days"
                sSwedish = "Ta bort efter antal dagar:"
                sDanish = "Slet efter antal dage:"
            Case 60047
                sNorwegian = "Backupkatalog:"
                sEnglish = "Backuppath"
                sSwedish = "Säkerhetskatalog:"
                sDanish = "Backupkatalog:"
            Case 60048
                sNorwegian = "Lag/Endre send- eller returprofil?"
                sEnglish = "Create/change send- or returnprofile ?"
                sSwedish = "Skapa/Ändra sändnings- eller returprofil?"
                sDanish = "Opret/Ændr sende- eller returprofil?"
            Case 60049
                sNorwegian = "Sendeprofil"
                sEnglish = "Sendprofile"
                sSwedish = "Sändningsprofil"
                sDanish = "Sendeprofil"
            Case 60050
                sNorwegian = "Returprofil"
                sEnglish = "Returnprofile"
                sSwedish = "Returprofil"
                sDanish = "Returprofil"
            Case 60051
                sNorwegian = "En sendeprofil benyttes ved innsending av filer til banken, f.eks. for å slå sammen klientfiler til en sendefil."
                sEnglish = "A sendprofile is used for transferring files to the bank, ex. to merge several clientfiles into one single file."
                sSwedish = "En sändningsprofil som används för att skicka in filer till banken, till exempel för att slå ihop klientfiler till en sändningsfil."
                sDanish = "En sendeprofil benyttes ved indsendelse af filer til banken, f.eks. for at slå klientfiler sammen til en sendefil."
            Case 60052
                sNorwegian = "En returprofil benyttes for å behandle returfiler fra banken, f.eks. for å splitte en returfil i flere klientfiler."
                sEnglish = "A returnprofile is used to handle returnfiles from the bank, ex. to split one returnfile into several clientfiles."
                sSwedish = "En returprofil används för att behandla returfiler från banken, eg. till exempel för att dela upp en returfil i flera klientfiler."
                sDanish = "En returprofil benyttes til at behandle returfiler fra banken, f.eks. til at opdele en returfil i flere klientfiler."
            Case 60053
                sNorwegian = "Velg format for filer fra regnskap"
                sEnglish = "Select a format for accountingfiles"
                sSwedish = "Välj format från redovisning"
                sDanish = "Vælg format for filer fra regnskab"
            Case 60054
                sNorwegian = "Legg inn navn på filer fra internt system (regnskap)"
                sEnglish = "Specify names for files from internal system"
                sSwedish = "Ange namn på filer från internt system (redovisning)"
                sDanish = "Indtast navne på filer fra internt system (regnskab)"
            Case 60055
                sNorwegian = "Angi de filnavn som skal brukes i profilen. Klientfiler angis ved at klientNUMMER oppgis som §§§. Antall § bestemmes av antall sifre i klientnumer. Ex. vil TILBANK.§§ symbolisere filer med navn TILBANK.01, TILBANK.09, TILBANK.83. Alternativt benytt ett £ tegn for å angi klientNAVN som del av filnavn/katalog."
                sEnglish = "Key in filenames to be used in this profile. Clientfiles are named with §§§ showing clientNUMBER. Number of § indicates number of symbols in clientID. Ex. TOBANK.§§ for filenames TOBANK.01, TOBANK.02, where 01 and 02 are clientID's. Alternatively, use a £ sign to mimic clientNAME as part of filename/path."
                sSwedish = "Ange filnamnet som ska användas i profilen. Klientfiler anges genom att kundnummer anges som §§§. Antalet § styrs av antalet siffror i klientnumret. Till exempel TILLBANK. §§ symboliserar filer som heter TILLBANK.01, TILLBANK.09, TILLBANK.83. Alternativt kan du använda ett £-tecken för att ange klientNAMN som en del av filnamnet/katalogen."
                sDanish = "Angiv de filnavne, der skal bruges i profilen. Klientfiler angives ved at klientNUMMER angives som §§§. Antal § bestemmes af antal cifre i klientnummer. F.eks. vil TILBANK.§§ symbolisere filer med navn TILBANK.01, TILBANK.09, TILBANK.83. Alternativt bruges et £ tegn til at angive klientNAVN som del af filnavn/katalog."
            Case 60056
                sNorwegian = "Filer fra internt system"
                sEnglish = "Files from internal system."
                sSwedish = "Filer från internt system"
                sDanish = "Filer fra internt system"
            Case 60057
                sNorwegian = "Filnavn"
                sEnglish = "Filename"
                sSwedish = "Filnamn"
                sDanish = "Filnavn"
            Case 60058
                sNorwegian = "Rapport til printer"
                sEnglish = "Report to printer"
                sSwedish = "Rapport till skrivare"
                sDanish = "Rapport til printer"
            Case 60059
                sNorwegian = "Rapport til skjerm"
                sEnglish = "Report to screen"
                sSwedish = "Rapport för visning"
                sDanish = "Rapport til skærm"
            Case 60060
                sNorwegian = "Legg i backupkatalogen etter import"
                sEnglish = "Copy to backupdirectory after import"
                sSwedish = "Lägg i säkerhetskopieringskatalog efter import"
                sDanish = "Læg i backupkatalog efter import"
            Case 60061
                sNorwegian = "Velg format for fil til bank"
                sEnglish = "Select format for file to bank"
                sSwedish = "Välj format för fil till bank"
                sDanish = "Vælg format for fil til bank"
            Case 60062
                sNorwegian = "Antall"
                sEnglish = "No of times"
                sSwedish = "Antal"
                sDanish = "Antal"
            Case 60063
                sNorwegian = "Legg inn navn på filer som skal sendes bank"
                sEnglish = "Filenames for files to bank"
                sSwedish = "Ange namn på filer som ska skickas till bank"
                sDanish = "Indtast navn på filer, som skal sendes til bank"
            Case 60064
                sNorwegian = "Oppgi navn på den filen som skal sendes til bank."
                sEnglish = "Specify filenames for files that will be transferred to the bank."
                sSwedish = "Ange namn på den fil som ska skickas till banken."
                sDanish = "Angiv navn på den fil, som skal sendes til bank."
            Case 60065
                sNorwegian = "Fil til bank"
                sEnglish = "File to bank"
                sSwedish = "Fill till bank"
                sDanish = "Fil til bank"
            Case 60066
                sNorwegian = "Legg i backupkatalog"
                sEnglish = "Copy to backupdir."
                sSwedish = "Lägg i säkerhetskopiering"
                sDanish = "Læg i backupkatalog"
            Case 60067
                sNorwegian = "Skriv over hvis fil finnes"
                sEnglish = "Overwrite if exists"
                sSwedish = "Skriv över om filen finns"
                sDanish = "Overskriv hvis fil findes"
            Case 60068
                sNorwegian = "Varsle hvis fil finnes"
                sEnglish = "Warning if exists"
                sSwedish = "Avisera om filen finns"
                sDanish = "Advar hvis fil findes"
            Case 60069
                sNorwegian = "Banknavn"
                sEnglish = "Bankname"
                sSwedish = "Banknamn"
                sDanish = "Banknavn"
            Case 60070
                sNorwegian = "Klientrapporter for sendefiler"
                sEnglish = "List clients in file to bank"
                sSwedish = "Klientrapporter för utskicksfiler"
                sDanish = "Klientrapporter for sendefiler"
            Case 60071
                sNorwegian = "Ønsker du å skrive ut eller maile oversikt over de klienter du har laget sendefiler for ?"
                sEnglish = "List merged clients"
                sSwedish = "Vill du skriva ut eller e-posta en översikt över kunder som du har skapat utskicksfiler åt ?"
                sDanish = "Ønsker du at udskrive eller maile en oversigt over de klienter du har oprettet sendefiler for?"
            Case 60072
                sNorwegian = "Klientoversikt ved innsending"
                sEnglish = "List clients"
                sSwedish = "Klientöversikt vid utskick"
                sDanish = "Klientoversigt ved indsendelse"
            Case 60073
                sNorwegian = "Klientoversikt til skjerm"
                sEnglish = "Clients to screen"
                sSwedish = "Klientöversikt till skärm"
                sDanish = "Klientoversigt til skærm"
            Case 60074
                sNorwegian = "Klientoversikt til skriver"
                sEnglish = "Clients to printer"
                sSwedish = "Klientöversikt till skrivare"
                sDanish = "Klientoversigt til printer"
            Case 60075
                sNorwegian = "Klientoversikt til fil"
                sEnglish = "Clients to file"
                sSwedish = "Klientöversikt till fil"
                sDanish = "Klientoversigt til fil"
            Case 60076
                sNorwegian = "Klientoversikt til mail"
                sEnglish = "Clients to mail"
                sSwedish = "Klientöversikt till e-post"
                sDanish = "Klientoversigt til mail"
            Case 60077
                sNorwegian = "Gi et navn til sendeprofilen"
                sEnglish = "Give name to sendprofile"
                sSwedish = "Ange ett namn på utskicksprofilen"
                sDanish = "Giv sendeprofilen et navn"
            Case 60078
                sNorwegian = "Profilens navn"
                sEnglish = "Profilename"
                sSwedish = "Profilens namn"
                sDanish = "Profilens navn"
            Case 60079
                sNorwegian = "Skal vi fortsette til returprofil ?"
                sEnglish = "Continue to Returnprofile ?"
                sSwedish = "Ska vi fortsätta att till returprofilen ?"
                sDanish = "Skal vi fortsætte til returprofil ?"
            Case 60080
                sNorwegian = "Det vil nå være naturlig å fortsette med å lage en returprofil, for å angi verdier for returfiler fra bank. Du kan imidlertid også fortsette med å lage en ny sendeprofil, eller du kan avslutte."
                sEnglish = "The next step will normally be to create a returnprofile, to specify parameters for files from bank. You may also choose to make a NEW sendprofile, or to leave setup if You have no returnfiles to handle."
                sSwedish = "Det skulle nu vara lämpligt att fortsätta med en returprofil, för att ange värden för returfiler från banken. Men du kan också fortsätta med att skapa en ny sändningsprofil, eller avsluta."
                sDanish = "Det vil nu være naturligt at fortsætte med at lave en returprofil, til at angive værdier for returfiler fra bank. Du kan imidlertid også fortsætte med at lave en ny sendeprofil, eller du kan afslutte."
            Case 60081
                sNorwegian = "Fortsett til returprofil"
                sEnglish = "Continue with returnprofile"
                sSwedish = "Fortsätt till returprofil"
                sDanish = "Fortsæt til returprofil"
            Case 60082
                sNorwegian = "Gå videre til NY sendeprofil"
                sEnglish = "Go to NEW sendprofile"
                sSwedish = "Gå vidare till NY sändningsprofil"
                sDanish = "Gå videre til NY sendeprofil"
            Case 60083
                sNorwegian = "Avslutt setup"
                sEnglish = "Leave setup"
                sSwedish = "Avsluta inställningen"
                sDanish = "Afslut setup"
            Case 60084
                sNorwegian = "Fortsett til returprofil ?"
                sEnglish = "Continue to returnprofile ?"
                sSwedish = "Fortsätta till returprofil ?"
                sDanish = "Fortsæt til returprofil ?"
            Case 60085
                sNorwegian = "Velg format for fil fra bank"
                sEnglish = "Select format for file from bank"
                sSwedish = "Välj format för fil från bank"
                sDanish = "Vælg format for fil fra bank"
            Case 60086
                sNorwegian = "Legg inn navn på filer fra bank"
                sEnglish = "Filenames from bank"
                sSwedish = "Ange namn på filer från bank"
                sDanish = "Indtast navn på filer fra bank"
            Case 60087
                sNorwegian = "Angi de filnavn som skal brukes i profilen. Klientfiler angis ved at klientbegrepet oppgis som §§§. Antall § bestemmes av antall sifre i klientnumer. Ex. vil TILBANK.§§ symbolisere filer med navn FRABANK.01, FRABANK.09, FRABANK.83"
                sEnglish = "Key in filenames to be used in this profile. Clientfiles are named with §§§ showing clientID. Number of § indicates number of symbols in clientid. Ex. FROMBANK.§§ is for filenames FROMBANK.01, FROMBANK.02, where 01 and 02 are clientID's."
                sSwedish = "Ange filnamnet som ska användas i profilen. Klientfiler anges genom att klientbegreppet skrivs som §§§.  Antalet § styrs av antalet siffror i klientnumret. Till exempel TILLBANK. §§ symboliseras filer som heter FRÅNBANK.01, FRÅNBANK.09, FRÅNBANK.83"
                sDanish = "Angiv de filnavne, der skal bruges i profilen. Klientfiler angives ved at klientbegrebet angives som §§§. Antal § bestemmes af antal cifre i klientnummer. F.eks. vil TILBANK.§§ symbolisere filer med navn FRABANK.01, FRABANK.09, FRABANK.83"
            Case 60088
                sNorwegian = "Filer fra bank"
                sEnglish = "Files from bank"
                sSwedish = "Filer från bank"
                sDanish = "Filer fra bank"
            Case 60089
                sNorwegian = "Rapport til printer"
                sEnglish = "Report to printer"
                sSwedish = "Rapport till skrivare"
                sDanish = "Rapport til printer"
            Case 60090
                sNorwegian = "Rapport til skjerm"
                sEnglish = "Report to screen"
                sSwedish = "Rapport för visning"
                sDanish = "Rapport til skærm"
            Case 60091
                sNorwegian = "Flytt til backupkatalogen etter import"
                sEnglish = "Move to backupdirectory after import"
                sSwedish = "Flytta till säkerhetskopi etter import"
                sDanish = "Flyt til backupkatalog efter import"
            Case 60092
                sNorwegian = "Velg format for returfil til regnskap"
                sEnglish = "Select format(s) for returnfile to accountingsystem"
                sSwedish = "Välj format för returfil till redovisning"
                sDanish = "Vælg format for returfil til regnskab"
            Case 60093
                sNorwegian = "Legg inn navn på returfiler til regnskap"
                sEnglish = "Filenames for returnfiles to accountingsystem"
                sSwedish = "Ange namn på returfiler till redovisning"
                sDanish = "Indtast navn på returfiler til regnskab"
            Case 60094
                sNorwegian = "Angi navn på returfiler som skal leses tilbake til regnskapsprogrammet. Benytt flere § tegn for å angi posisjoner i filnavn/katalog for klientNUMMER. Benytt ett £ tegn for å angi klientNAVN som del av filnavn/katalog."
                sEnglish = "Specify filenames for returnfiles to accountingsystem. Use one or several § signs as a substitute for clientNUMBER in filename/path. Use a £ sign to mimic clientNAME as part of filename/path."
                sSwedish = "Ange namn på returfiler som ska läsas tillbaka till bokföringsprogrammet. Använd flera §-tecken för att beteckna positioner i filnamnet/katalogen för klientNUMMER. Använd ett £-tecken för att ange klientNAMN som en del av filnamnet/katalogen."
                sDanish = "Angiv navn på returfiler som skal læses tilbage til regnskabsprogrammet. Benyt flere § tegn til at angive positioner i filnavn/katalog for klientNUMMER. Benyt et £ tegn for at angive klientNAVN som del af filnavn/katalog."
            Case 60095
                sNorwegian = "Returfiler til regnskap"
                sEnglish = "Returnfiles to account"
                sSwedish = "Returfiler till redovisning"
                sDanish = "Returfiler til regnskab"
            Case 60096
                sNorwegian = "Klientrapport for returfiler"
                sEnglish = "Clientreport for returnfiles"
                sSwedish = "Klientrapport för returfiler"
                sDanish = "Klientrapport for returfiler"
            Case 60097
                sNorwegian = "Ønsker du å skrive ut elle rmaile oversikt over de klienter du har mottatt returfiler for ?"
                sEnglish = "Do You want to report or mail a clientslisting for returnfiles ?"
                sSwedish = "Vill du skriva ut eller e-posta en översikt över de klienter som du har fått returfiler för ?"
                sDanish = "Vil du udskrive eller maile oversigt over de klienter, du har modtaget returfiler for?"
            Case 60098
                sNorwegian = "Klientoversikt ved returfiler"
                sEnglish = "Clientslist for returnfiles"
                sSwedish = "Klientöversikt vid returfiler"
                sDanish = "Klientoversigt ved returfiler"
            Case 60099
                sNorwegian = "Gi et navn til returprofilen"
                sEnglish = "Name for returnprofile"
                sSwedish = "Namnge returprofilen"
                sDanish = "Giv returfilen et navn"
            Case 60100
                sNorwegian = "Profilnavn må være unikt. Dette er ibruk fra før!"
                sEnglish = "Profilename must be unique. This one is used before!"
                sSwedish = "Profilnamn måste vara unika. Detta används redan!"
                sDanish = "Profilnavnet skal være unikt. Dette er i brug fra før!"
            Case 60101
                sNorwegian = "Klient %1 må kobles til en profil!"
                sEnglish = "Client %1 must be connected to a profile!"
                sSwedish = "Klient %1 måste vara ansluten till en profil!"
                sDanish = "Klient %1 skal kobles til en profil!"
            Case 60102
                sNorwegian = "Ulovlige tegn i profilnavn! (blank, /, \)"
                sEnglish = "Illegal characters in profilename! (space, /, \)"
                sSwedish = "Otillåtna tecken i profilnamn! (blank, /, \)"
                sDanish = "Ulovlige tegn i profilnavn! (mellemrum, /, \)"
            Case 60103
                sNorwegian = "Profilnavn kan ikke starte med RETUR, SEND eller ALL"
                sEnglish = "Profilename can not start with RETUR, SEND, ALL"
                sSwedish = "Profilnamn kan inte börja med RETUR, SEND eller ALL"
                sDanish = "Profilnavn kan ikke starte med RETUR, SEND eller ALL"
            Case 60104
                sNorwegian = "Profil kun for utskrift?"
                sEnglish = "Profile for reporting only ?"
                sSwedish = "Profil endast för utskrift?"
                sDanish = "Profil kun for udskrift?"
            Case 60105
                sNorwegian = "Sekvensnumre"
                sEnglish = "Sequenceno's"
                sSwedish = "Sekvensnummer"
                sDanish = "Sekvensnumre"
            Case 60106
                sNorwegian = "Bruk sekvensnumre fra importert fil"
                sEnglish = "Use sequencenumbers from imported file"
                sSwedish = "Använd sekvensnummer från den importerade filen"
                sDanish = "Brug sekvensnumre fra importeret fil"
            Case 60107
                sNorwegian = "Sekvensnumre samlet for hele filer"
                sEnglish = "Seqencenumbers total pr. file"
                sSwedish = "Sekvensnummer som samlats för hela filer"
                sDanish = "Sekvensnumre samlet for hele filer"
            Case 60108
                sNorwegian = "Sekvensnumre pr. klient"
                sEnglish = "Sequencenumbers pr. client"
                sSwedish = "Sekvensnummer per klient"
                sDanish = "Sekvensnumre pr. klient"
            Case 60109
                sNorwegian = "Velg profil"
                sEnglish = "Select profile"
                sSwedish = "Välj profil"
                sDanish = "Vælg profil"
            Case 60110
                sNorwegian = "Forrige sekvensnummer"
                sEnglish = "Previous sequencenumber"
                sSwedish = "Föregående sekvensnummer"
                sDanish = "Forrige sekvensnummer"
            Case 60111
                sNorwegian = "Finner ingen profiler med sekvensnummer!"
                sEnglish = "No profiles with sequencenumbers found!"
                sSwedish = "Hittar inga profiler med sekvensnummer!"
                sDanish = "Finder ingen profiler med sekvensnummer!"
            Case 60112
                sNorwegian = "Ingen OCR-profil funnet"
                sEnglish = "No OCR-profiles found"
                sSwedish = "Ingen OCR-profil hittades"
                sDanish = "Ingen OCR-profil fundet"
            Case 60113
                sNorwegian = "Flere enn en OCR-profil funnet. BabelBank velger den første. Kontakt Visual Banking hvis dette ikke er OK!"
                sEnglish = "More than one OCR-format. BabelBank selects first. Contact Visual Banking if not OK!"
                sSwedish = "Mer än en OCR-profil hittades. BabelBank väljer den första. Kontakta Visual Banking om detta inte är OK!"
                sDanish = "Der blev fundet mere end en OCR-profil. BabelBank vælger den første. Kontakt Visual Banking hvis dette ikke er OK!"
            Case 60114
                sNorwegian = "<Ikke i bruk>"
                sEnglish = "<Not in use>"
                sSwedish = "<Ikke i bruk>"
                sDanish = "<Ikke i bruk>"
            Case 60115
                sNorwegian = "Start: Angi startposisjon i KID (Hvis denne benyttes). Benyttes ikke ved splitting etter KID-lengde"
                sEnglish = "Start: Startposition in KID (If used). Not in use when splitting on KID-length"
                sSwedish = "Start: Ange startposition i OCR (om detta används). Används inte vid delning efter OCR-längd"
                sDanish = "Start: Angiv startposition i KID (Hvis denne benyttes). Benyttes ikke ved opdeling efter KID-længde"
            Case 60116
                sNorwegian = "Lengde: Angi lengde for KID, eller lengde for den del av KID som skal leses"
                sEnglish = "Length: Length for KID, or length of selected text in KID"
                sSwedish = "Längd: Ange längden på OCR eller längden av den del av OCR som ska läsas in"
                sDanish = "Længde: Angiv længde for KID, eller længde for den del af KID, der skal læses"
            Case 60117
                sNorwegian = "Verdi: Angi verdien for aktuell klient. Bruk evt. < > = i tilegg til verdi"
                sEnglish = "Value: Specify value for actual client. Use  < > = in additon to value"
                sSwedish = "Värde: Ange värdet för aktuell klient. Använd ev.  < > = utöver värde"
                sDanish = "Værdi: Angiv værdien for aktuel klient. Brug evt. < > = i tillæg til værdi"
            Case 60118
                sNorwegian = "Start"
                sEnglish = "Start"
                sSwedish = "Start"
                sDanish = "Start"
            Case 60119
                sNorwegian = "Lengde"
                sEnglish = "Length"
                sSwedish = "Längd"
                sDanish = "Længde"
            Case 60120
                sNorwegian = "Verdi"
                sEnglish = "Value"
                sSwedish = "Värde"
                sDanish = "Værdi"
            Case 60121
                sNorwegian = "Splitt på annet en kontonummer"
                sEnglish = "Split on other value than accountnumber"
                sSwedish = "Dela med annat kontonummer"
                sDanish = "Opdel efter andet end kontonummer"
            Case 60122
                sNorwegian = "Kun rapport"
                sEnglish = "Report Only"
                sSwedish = "Endast rapport"
                sDanish = "Kun rapport"
            Case 60123
                sNorwegian = "Inkluder spesialformater"
                sEnglish = "&Include specialformats"
                sSwedish = "Inkludera specialformat"
                sDanish = "Inkluder specialformater"
            Case 60124
                sNorwegian = "Ikke vis spesialformater"
                sEnglish = "&Do not show specialformats"
                sSwedish = "Visa inte specialformat"
                sDanish = "Vis ikke specialformater"
            Case 60125
                sNorwegian = "Lag snarvei for denne profilen"
                sEnglish = "Create shortcut for this profile"
                sSwedish = "Skapa genväg till den här profilen"
                sDanish = "Opret genvej for denne profil"
            Case 60126
                sNorwegian = "Telepay samle"
                sEnglish = "Telepay merge"
                sSwedish = "Samla med Telepay"
                sDanish = "Telepay samle"
            Case 60127
                sNorwegian = "Telepay splitte"
                sEnglish = "Telepay split"
                sSwedish = "Dela med Telepay"
                sDanish = "Telepay opdele"
            Case 60128
                sNorwegian = "OCR splitte"
                sEnglish = "OCR split"
                sSwedish = "OCR-delning"
                sDanish = "OCR opdele"
            Case 60129
                sNorwegian = "Konverter fra Dir.Rem til Telepay"
                sEnglish = "Convert Dir.Rem to Telepay"
                sSwedish = "Konvertera från Dir.Rem till Telepay"
                sDanish = "Konverter fra Dir.Rem til Telepay"
            Case 60130
                sNorwegian = "Hurtigprofiler"
                sEnglish = "Quickprofiles"
                sSwedish = "Snabbprofiler"
                sDanish = "Hurtigprofiler"
            Case 60131
                sNorwegian = "Telepay ut"
                sEnglish = "Telepay out"
                sSwedish = "Telepay ut"
                sDanish = "Telepay ud"
            Case 60132
                sNorwegian = "Vennligst oppgi filnavn"
                sEnglish = "Please specify filenames"
                sSwedish = "Ange filnamn"
                sDanish = "Angiv filnavn"
            Case 60133
                sNorwegian = "JCL-kommandoer i eksportfil"
                sEnglish = "JCL-commands in exportfile"
                sSwedish = "JCL-kommandon i exportfil"
                sDanish = "JCL-kommandoer i eksportfil"
            Case 60134
                sNorwegian = "JCL-kommandoer i starten av filen"
                sEnglish = "JCL-commands in start of file"
                sSwedish = "JCL-kommandon i början av filen"
                sDanish = "JCL-kommandoer i starten af filen"
            Case 60135
                sNorwegian = "JCL-kommandoer i slutten av filen"
                sEnglish = "JCL-commands at end of file"
                sSwedish = "JCL-kommandon i slutet av filen"
                sDanish = "JCL-kommandoer i slutningen af filen"
            Case 60136
                sNorwegian = "Bruk bokstavkombinasjonen CRLF for å markere ny linje."
                sEnglish = "Use capital letters CRLF to indicate new line."
                sSwedish = "Använd bokstavskombinationen CRLF för att markera en ny rad."
                sDanish = "Brug bogstavkombinationen CRLF for at markere ny linje."
            Case 60137
                sNorwegian = "&Spesial"
                sEnglish = "&Special"
                sSwedish = "&Special"
                sDanish = "&Special"
            Case 60138
                sNorwegian = "Velg klient(er) med Ctrl+Museklikk i venstre kolonne!"
                sEnglish = "Please select client(s) witn Ctrl+Click in left column!"
                sSwedish = "Välj klient(er) med Ctrl + Musklick i vänstra kolumnen!"
                sDanish = "Vælg klient(er) med Ctrl+Museklik i venstre kolonne!"
            Case 60139
                sNorwegian = "Ikke tillatt med * i utfilnavn!"
                sEnglish = "* not allowed in exportfilenames!"
                sSwedish = "Inte tillåtet * i utfilnavn!"
                sDanish = "Ikke tilladt med * i udfilnavn!"
            Case 60140
                sNorwegian = "Ikke tillatt med BÅDE $ og £ i samme filnavn"
                sEnglish = "Combination of BOTH $ and £ not allowed in filenames"
                sSwedish = "Inte tillåtet med BÅDE § och £ i samma filnamn"
                sDanish = "Ikke tilladt med BÅDE § og £ i samme filnavn"
            Case 60141
                sNorwegian = "£ kan brukes kun en gang i filnavn - Dette symboliserer hele klientnavnet"
                sEnglish = "£ only once in filename. This mimics the complete clientNAME in file/path."
                sSwedish = "£ kan endast användas en gång i filnamnet - Detta symboliserar hela klientnamnet"
                sDanish = "£ kan kun bruges en gang i filnavn - Dette symboliserer hele klientnavnet"
            Case 60142
                sNorwegian = "Betalerindetifikasjon"
                sEnglish = "Payers identification"
                sSwedish = "Betalaridentifikation"
                sDanish = "Betaleridentifikation"
            Case 60143
                sNorwegian = "Beskrivelse"
                sEnglish = "Description"
                sSwedish = "Beskrivning"
                sDanish = "Beskrivelse"
            Case 60144
                sNorwegian = "Fjern gamle filer før eksport"
                sEnglish = "Remove files before export"
                sSwedish = "Ta bort gamla filer före export"
                sDanish = "Fjern gamle filer før eksport"
            Case 60145
                sNorwegian = "Det finnes ingen profiler. Lag profiler først, før du setter opp klienter!"
                sEnglish = "You have no profiles saved. Please create one or more profiles before setting up clients!"
                sSwedish = "Det finns inga profiler. Skapa profiler först, innan du installerar klienter!"
                sDanish = "Der findes ingen profiler. Opret profiler først, før du opsætter klienter!"
            Case 60146
                sNorwegian = "Lagre sekvensoppsett ?"
                sEnglish = "Save sequencesetup ?"
                sSwedish = "Spara sekvensinställningen för %1?"
                sDanish = "Gemme sekvensopsætning for %1?"
            Case 60147
                sNorwegian = "Egendefinert"
                sEnglish = "Customized"
                sSwedish = "Egendefinierat"
                sDanish = "Egendefineret"
            Case 60148
                sNorwegian = "skjermtekst"
                sEnglish = "text to display"
                sSwedish = "skärmtext"
                sDanish = "skærmtekst"
            Case 60149
                sNorwegian = "Bank hovedbok"
                sEnglish = "Bank GL"
                sSwedish = "Bank-huvudbok"
                sDanish = "Bank hovedbog"
            Case 60150
                sNorwegian = "Motkonto"
                sEnglish = "ResultsAccount"
                sSwedish = "Motkonto"
                sDanish = "Modkonto"
            Case 60151
                sNorwegian = "Mønsterbeskrivelse: #=Numerisk tegn $=Alfanumerisk tegn   _=Blank  @=Ikke numerisk tegn   Andre tegn er 'ekte'"
                sEnglish = "#=Numeric sign    $=Alphanumeric sign    _ = Blank   @=Non numeric   Other signs are 'as is'"
                sSwedish = "Mönsterbeskrivning: #=siffra $=alfanumeriskt tecken _=@ tom = @=ickenumeriskt tecken Andra tecken är ''äkta''"
                sDanish = "Mønsterbeskrivelse: #=Numerisk tegn $=Alfanumerisk tegn   _=Blank  @=Ikke numerisk tegn   Andre tegn er 'ægte'"
            Case 60152
                sNorwegian = "Fakturabeskrivelser"
                sEnglish = "Invoicedescriptions"
                sSwedish = "Fakturabeskrivningar"
                sDanish = "Fakturabeskrivelser"
            Case 60153
                sNorwegian = "Velg først den beskrivelse du vil slette!"
                sEnglish = "Select description to delete"
                sSwedish = "Välj först den beskrivning som du vill ta bort!"
                sDanish = "Vælg først den beskrivelse du vil slette!"
            Case 60154
                sNorwegian = "Vil du slette fakturabeskrivelse"
                sEnglish = "Do You want to delete InvoiceDescription"
                sSwedish = "Vill du ta bort fakturabeskrivningen"
                sDanish = "Vil du slette fakturabeskrivelsen"
            Case 60155
                sNorwegian = "Slette fakturabeskrivelse"
                sEnglish = "Delete InvoiceDescription"
                sSwedish = "Ta bort fakturabeskrivning"
                sDanish = "Slette fakturabeskrivelse"
            Case 60156
                sNorwegian = "A&vstem"
                sEnglish = "&Match"
                sSwedish = "A&vstäm"
                sDanish = "A&fstem"
            Case 60157
                sNorwegian = "Oppsett for avstemming"
                sEnglish = "Setup for matching"
                sSwedish = "Inställning för avstämning"
                sDanish = "Opsætning for afstemning"
            Case 60158
                sNorwegian = "Automatisk avstemming"
                sEnglish = "Automatic matching"
                sSwedish = "Automatisk avstämning"
                sDanish = "Automatisk afstemning"
            Case 60159
                sNorwegian = "Påloggingsstreng"
                sEnglish = "Connectionstring"
                sSwedish = "Inloggningssträng"
                sDanish = "Pålogningsstreng"
            Case 60160
                sNorwegian = "Bruker ID"
                sEnglish = "User ID"
                sSwedish = "Användar-ID"
                sDanish = "Bruger ID"
            Case 60161
                sNorwegian = "Passord"
                sEnglish = "Password"
                sSwedish = "Lösenord"
                sDanish = "Kodeord"
            Case 60162
                sNorwegian = "Database"
                sEnglish = "Database"
                sSwedish = "Databas"
                sDanish = "Database"
            Case 60163
                sNorwegian = "Avstem OCR-poster"
                sEnglish = "Match OCR-payments"
                sSwedish = "Avstäm OCR-poster"
                sDanish = "Afstem OCR-poster"
            Case 60164
                sNorwegian = "KID-beskrivelse:   K = kundenummer    F = fakturanummer    S = selskap (klient)    0 = kontrollsiffer CDV10    1 = kontrollsiffer CDV11"
                sEnglish = "KID-pattern:    C = customerno    I = invoiceno    E = enterpriseno    0 = CDV 10    1 = CDV11"
                sSwedish = "OCR-beskrivning: K = kundnummer F = fakturanummer S = bolag (klienten) 0 = kontrollsiffra CDV10 1 = kontrollsiffra CDV11"
                sDanish = "KID-beskrivelse:   K = kundenummer    F = fakturanummer    S = selskab (klient)    0 = kontrolciffer CDV10    1 = kontrolciffer CDV11"
            Case 60165
                sNorwegian = "Eksport-KID"
                sEnglish = "Export KID"
                sSwedish = "Export-OCR"
                sDanish = "Eksport-KID"
            Case 60166
                sNorwegian = "OCR filens KID"
                sEnglish = "KID for OCR-file"
                sSwedish = "OCR-filens OCR"
                sDanish = "OCR-filens KID"
            Case 60167
                sNorwegian = "Uidentifisert"
                sEnglish = "Unidentified"
                sSwedish = "Oidentifierat"
                sDanish = "Uidentificeret"
            Case 60168
                sNorwegian = "1: Konto"
                sEnglish = "1: Account"
                sSwedish = "1: Konto"
                sDanish = "1: Konto"
            Case 60169
                sNorwegian = "2: Kundenr."
                sEnglish = "2: CustomerNo"
                sSwedish = "2: Kundnr."
                sDanish = "2: Kundenr."
            Case 60170
                sNorwegian = "3: KID"
                sEnglish = "3: KID"
                sSwedish = "3: OCR"
                sDanish = "3: KID"
            Case 60171
                sNorwegian = "Behandling av uidentifiserte"
                sEnglish = "Uidentified as:"
                sSwedish = "Behandling av oidentifierade"
                sDanish = "Behandling af uidentificerede"
            Case 60172
                sNorwegian = "Type uidentifiserte"
                sEnglish = "Type of unidentified"
                sSwedish = "Typ av oidentifierade"
                sDanish = "Type uidentificerede"
            Case 60173
                sNorwegian = "Velg KID-mønster for sletting"
                sEnglish = "Select KID-pattern to delete"
                sSwedish = "Välj OCR-mönster för borttagning"
                sDanish = "Vælg KID-mønster for sletning"
            Case 60174
                sNorwegian = "Vil du slette KID-mønster"
                sEnglish = "Do you want to delete KID-pattern"
                sSwedish = "Ta bort OCR-mönster"
                sDanish = "Vil du slette KID-mønster"
            Case 60175
                sNorwegian = "Benytt kun f=fakturanr k=kundenr s=selskap 0=CDV10 1=CDV11 !"
                sEnglish = "Please use i=invoice c=customerno e=enterprise 0=CDV10 1=CDV11"
                sSwedish = "Använd endast f = faktura k = kundnummer s = bolag 0=CDV10 1=CDV11 !"
                sDanish = "Benyt kun f=fakturanr k=kundenr s=selskab 0=CDV10 1=CDV11 !"
            Case 60176
                sNorwegian = "Avtale-ID"
                sEnglish = "Contract No"
                sSwedish = "Avtals-ID"
                sDanish = "Aftale-ID"
            Case 60177
                sNorwegian = "E-Post (Mail) oppsett"
                sEnglish = "Setup Email"
                sSwedish = "Inställning av e-post (mail)"
                sDanish = "E-mailopsætning"
            Case 60178
                sNorwegian = "BabelBank kan sende mail til mottakere for å varsle om splitting eller samling av filer. Du kan også sende supportmail direkte fra BabelBank. I automatiserte driftssituasjoner er det også mulig å sende mailer automatisk til en driftsoperatør."
                sEnglish = "BabelBank can send Email to notify of split or merge of paymentfiles. You are also able to send supportmailes form inside BabelBank. When BabelBank is run in automated mode, the program can send statusmails to an operator."
                sSwedish = "BabelBank kan skicka e-post till mottagare för att avisera en delning eller samling av filer. Du kan också skicka e-post för support direkt från BabelBank. Vid autodrift är det också möjligt att skicka e-post automatiskt till en operatör."
                sDanish = "BabelBank kan sende mail til modtagere for at varsle om opdeling eller samling af filer. Du kan også sende supportmail direkte fra BabelBank. I automatiserede driftssitutationer er det også muligt at sende mails automatisk til en driftsoperatør."
            Case 60179
                sNorwegian = "SMTP - Hvis mail skal kunne sendes automatisk, må du velge SMTP. BabelBank vil da opptre som en egen 'mailklient', uavhengig av om du har installert mailprogrammer som Outlook, Notes, etc."
                sEnglish = "SMTP - If you would like BabelBank to automatically send mailes, then select SMTP. BabelBank will then set up its own mailclient, independent of your ordinary mailclient (like Outlook, Notes, etc.)."
                sSwedish = "SMTP - Om din e-post ska skickas automatiskt väljer du SMTP. BabelBank kommer att fungera som en separat '' postklient'’, oavsett om du har installerat e-postprogram som Outlook eller Notes osv."
                sDanish = "SMTP - Hvis mail skal kunne sendes automatisk, skal du vælge SMTP. BabelBank vil da optræde som en egen 'mailklient', uafhængig af om du har installeret mailprogrammer som Outlook, Notes, etc."
            Case 60180
                sNorwegian = "MAPI - Ønsker du å se innholdet av mailene først, med mulighet for redigering, eller du ikke får SMTP til å virke, velger du MAPI. Mailene blir da sendt gjennom ditt e-post program (Outlook, Notes, andre). Du må selv manuelt aktivere sending i ditt e-post program"
                sEnglish = "MAPI - If you like to browse or edit the mailcontent before sending, or is unable to get SMTP to work, then choose MAPI. Emails from BabelBank is then sent through your mailclientprogram (like Outlook, Notes, etc.). You must manually activate the sendprocess in your mailprogram."
                sSwedish = "MAPI - Vill du se innehållet i de e-postmeddelandena först, med möjlighet till redigering, eller du inte får SMTP att fungera, väljer du MAPI. E-post kommer då att skickas via ett e-postprogram (Outlook, Notes, andra). Du måste manuellt aktivera utskick av e-post i ditt  e-postprogram"
                sDanish = "MAPI - Ønsker du at se indholdet af mailene først, med mulighed for redigering, eller hvis du ikke får SMTP til at virke, vælger du MAPI. Mailene bliver så sendt gennem dit e-mailprogram (Outlook, Notes, andre). Du må selv manuelt aktivere afsendelse i dit e-mailprogram"
            Case 60181
                sNorwegian = "Mailavsender ( @ )"
                sEnglish = "Mailsender ( $ )"
                sSwedish = "E-postavsändare (@)"
                sDanish = "Mailafsender ( @ )"
            Case 60182
                sNorwegian = "Visningsnavn avsender"
                sEnglish = "Displayname sender"
                sSwedish = "Visningsnamn på avsändaren"
                sDanish = "Visningsnavn afsender"
            Case 60183
                sNorwegian = "Avsender BabelBank  automail ( @ )"
                sEnglish = "Sender BabelBank automail ( @ )"
                sSwedish = "Avsändarens automatiska e-post hos BabelBank (@)"
                sDanish = "Afsender BabelBank  automail ( @ )"
            Case 60184
                sNorwegian = "Visningsnavn BabelBankavsender"
                sEnglish = "Displayname BabelBank automail"
                sSwedish = "Visningsnamn på BabelBank-avsändaren"
                sDanish = "Visningsnavn BabelBankafsender"
            Case 60185
                sNorwegian = "Mottaker av driftsmeldinger ( @ )"
                sEnglish = "Operator ( @ )"
                sSwedish = "Mottagare av driftsmeddelanden (@)"
                sDanish = "Modtager af driftsmeddelelser ( @ )"
            Case 60186
                sNorwegian = "Svar (reply)  mailadresse ( @ )"
                sEnglish = "Replyadress ( @ )"
                sSwedish = "Svarsadress (reply) i e-posten (@)"
                sDanish = "Svar (reply)  mailadresse ( @ )"
            Case 60187
                sNorwegian = "Vennligst@ikke_svar"
                sEnglish = "Please@Do_not_respond"
                sSwedish = "Kan@ej_besvaras"
                sDanish = "Venligst@ikke_svar"
            Case 60188
                sNorwegian = "SMTP Hostadresse (Hvis blank, forsøker BabelBank å finne denne automatisk)"
                sEnglish = "SMTP Hostadress (leave blank if unknown, BabelBank searches)"
                sSwedish = "SMTP- värdadress (om tom försöker BabelBank att hitta denna automatiskt)"
                sDanish = "SMTP Hostadresse (Hvis blank, forsøger BabelBank at finde denne automatisk)"
            Case 60189
                sNorwegian = "Testmail sendt, via SMTPhost"
                sEnglish = "Testmail sent, via SMTPhost"
                sSwedish = "E-posttest skickas via SMTPhost"
                sDanish = "Testmail sendt, via SMTPhost"
            Case 60190
                sNorwegian = "Feil i sending av testmail!"
                sEnglish = "Error in sending testmail"
                sSwedish = "Fel vid utskick av e-posttest!"
                sDanish = "Fejl i afsendelse af testmail!"
            Case 60191
                sNorwegian = "Testmail sendt med MAPI"
                sEnglish = "Testmaul sent using MAPI"
                sSwedish = "E-posttest skickas med MAPI"
                sDanish = "Testmail sendt med MAPI"
            Case 60192
                sNorwegian = "Testmail laget, sending kanselert av bruker"
                sEnglish = "Testmail created, sending canceled by user"
                sSwedish = "E-posttest skapades men utskicket avbröts av användaren"
                sDanish = "Testmail gemt, afsendelse annulleret af bruger"
            Case 60193
                sNorwegian = "Ugyldig filnavn"
                sEnglish = "Invalid filename"
                sSwedish = "Ogiltigt filnamn"
                sDanish = "Ugyldigt filnavn"
            Case 60194
                sNorwegian = "Kjør uten å vise driftsmeldinger (silent)"
                sEnglish = "Run in 'silent-mode'"
                sSwedish = "Kör utan att visa driftsmeddelanden (tyst)"
                sDanish = "Kør uden at vise driftsmeddelelser (silent)"
            Case 60195
                sNorwegian = "Logg"
                sEnglish = "Log"
                sSwedish = "Logg"
                sDanish = "Log"
            Case 60196
                sNorwegian = "Aktiver logging"
                sEnglish = "Activate logging"
                sSwedish = "Aktivera loggning"
                sDanish = "Aktiver logging"
            Case 60197
                sNorwegian = "Bruk Windows Eventlog"
                sEnglish = "Use Windows EventLog"
                sSwedish = "Använd Windows Eventlog"
                sDanish = "Brug Windows Eventlog"
            Case 60198
                sNorwegian = "Logg til fil"
                sEnglish = "Log to file"
                sSwedish = "Logga i fil"
                sDanish = "Log til fil"
            Case 60199
                sNorwegian = "Logging med mail"
                sEnglish = "Log events to mail"
                sSwedish = "Loggning med e-post"
                sDanish = "Logging med mail"
            Case 60200
                sNorwegian = "Loggfilnavn"
                sEnglish = "Logfilename"
                sSwedish = "Loggfilnamn"
                sDanish = "Logfilnavn"
            Case 60201
                sNorwegian = "Ny loggfil hver gang"
                sEnglish = "New logfile for each run"
                sSwedish = "Ny loggfil varje gång"
                sDanish = "Ny logfil hver gang"
            Case 60202
                sNorwegian = "Max. antall linjer"
                sEnglish = "Max. no of lines"
                sSwedish = "Max antalet rader"
                sDanish = "Maks. antal linjer"
            Case 60203
                sNorwegian = "Kun tillatt med tall"
                sEnglish = "Numbers only!"
                sSwedish = "Endast siffror tillåts"
                sDanish = "Kun tilladt med tal"
            Case 60204
                sNorwegian = "Konvertert kontonummer"
                sEnglish = "Converted account"
                sSwedish = "Konverterat kontonummer"
                sDanish = "Konverteret kontonummer"
            Case 60205
                sNorwegian = "Avstemmingsregler"
                sEnglish = "Matching rules"
                sSwedish = "Avstämningsregler"
                sDanish = "Afstemningsregler"
            Case 60206
                sNorwegian = "Navn:"
                sEnglish = "Name:"
                sSwedish = "Namn:"
                sDanish = "Navn:"
            Case 60207
                sNorwegian = "Adresse 1:"
                sEnglish = "Address 1:"
                sSwedish = "Adress 1:"
                sDanish = "Adresse 1:"
            Case 60208
                sNorwegian = "Kontoutdrag"
                sEnglish = "Statement"
                sSwedish = "Kontoutdrag"
                sDanish = "Kontoudskrift"
            Case 60209
                sNorwegian = "Land:"
                sEnglish = "Country:"
                sSwedish = "Land:"
                sDanish = "Land:"
            Case 60210
                sNorwegian = "Tekst:"
                sEnglish = "Text"
                sSwedish = "Text:"
                sDanish = "Tekst:"
            Case 60211
                sNorwegian = "Beløp:"
                sEnglish = "Amount:"
                sSwedish = "Belopp:"
                sDanish = "Beløb:"
            Case 60212
                sNorwegian = "Valutakode:"
                sEnglish = "ISO:"
                sSwedish = "Valutakod:"
                sDanish = "Valutakode:"
            Case 60213
                sNorwegian = "Referanse:"
                sEnglish = "Reference:"
                sSwedish = "Referens:"
                sDanish = "Reference:"
            Case 60214
                sNorwegian = "Innbetaling"
                sEnglish = "Payment"
                sSwedish = "Inbetalning"
                sDanish = "Indbetaling"
            Case 60215
                sNorwegian = "Faktura"
                sEnglish = "Invoice"
                sSwedish = "Faktura"
                sDanish = "Faktura"
            Case 60216
                sNorwegian = "Resultat"
                sEnglish = "Result"
                sSwedish = "Resultat"
                sDanish = "Resultat"
            Case 60217
                sNorwegian = "Type:"
                sEnglish = "Type:"
                sSwedish = "Typ:"
                sDanish = "Type:"
            Case 60218
                sNorwegian = "Rekkefølge:"
                sEnglish = "Order:"
                sSwedish = "Ordningsföljd:"
                sDanish = "Rækkefølge:"
            Case 60219
                sNorwegian = "Returfelter:"
                sEnglish = "Return fields:"
                sSwedish = "Returfält:"
                sDanish = "Returfelter:"
            Case 60220
                sNorwegian = "Betalt beløp = BB_Amount" & vbLf & "Valuta = BB_Currency" & vbLf & "Fakturanummer = BB_InvoiceIdentifier" & vbLf & "Kundenummer = BB_CustomerNo" & vbLf & "Klientnummer = BB_ClientNo" & vbLf & "Betalers konto = BB_AccountNo" & vbLf & "Betalers navn = BB_Name"
                sEnglish = "Paid amount = BB_Amount" & vbLf & "Currency = BB_Currency" & vbLf & "Invoicenumber = BB_InvoiceIdentifier" & vbLf & "Customernumber = BB_CustomerNo" & vbLf & "Clientnumber = BB_ClientNo" & vbLf & "Payers AccountNo = BB_AccountNo" & vbLf & "Payers name = BB_Name"
                sSwedish = "Betalt belopp = BB_Amount" & vbLf & "Valuta = BB_Currency" & vbLf & "Fakturanummer = BB_InvoiceIdentifier" & vbLf & "Kundnummer = BB_CustomerNo" & vbLf & "Klientnummer = BB_ClientNo" & vbLf & "Betalarens konto = BB_AccountNo" & vbLf & "Betalarens namn = BB_Name	"
                sDanish = "Betalt beløb = BB_Amount" & vbLf & "Valuta = BB_Currency" & vbLf & "Fakturanummer = BB_InvoiceIdentifier" & vbLf & "Kundenummer = BB_CustomerNo" & vbLf & "Klientnummer = BB_ClientNo" & vbLf & "Betalers konto = BB_AccountNo" & vbLf & "Betalers navn = BB_Name	"
            Case 60221
                sNorwegian = "Pålogging"
                sEnglish = "Connection"
                sSwedish = "Inloggning"
                sDanish = "Pålogning"
            Case 60222
                sNorwegian = "Parametere"
                sEnglish = "Parameteres"
                sSwedish = "Parametrar"
                sDanish = "Parametre"
            Case 60223
                sNorwegian = "Fakturanummer:"
                sEnglish = "InvoiceNo:"
                sSwedish = "Fakturanummer:"
                sDanish = "Fakturanummer:"
            Case 60224
                sNorwegian = "Kundenummer:"
                sEnglish = "CustomerNo:"
                sSwedish = "Kundnummer:"
                sDanish = "Kundenummer:"
            Case 60225
                sNorwegian = "Bet. navn:"
                sEnglish = "Payers name:"
                sSwedish = "Bet.namn:"
                sDanish = "Bet. navn:"
            Case 60226
                sNorwegian = "Oppsett avstemmingsregler"
                sEnglish = "Setup for matching"
                sSwedish = "Ställ in avstämningsregler"
                sDanish = "Opsætning af afstemningsregler"
            Case 60227
                sNorwegian = "SQL skrevet"
                sEnglish = "SQL Written"
                sSwedish = "SQL skrevs"
                sDanish = "SQL skrevet"
            Case 60228
                sNorwegian = "SQL tolket"
                sEnglish = "SQL Interpreted"
                sSwedish = "SQL tolkades"
                sDanish = "SQL tolket"
            Case 60229
                sNorwegian = "Innbetaling"
                sEnglish = "Payment"
                sSwedish = "Inbetalning"
                sDanish = "Indbetaling"
            Case 60230
                sNorwegian = "Avst.regel:"
                sEnglish = "Match.rules:"
                sSwedish = "Avst.regel:"
                sDanish = "Afst.regel:"
            Case 60231
                sNorwegian = "Bygg opp en query som BabelBank trenger for å foreta en avstemming. Feltene og rekkefølgen som BabelBank krever, finner du oppgitt under." & vbLf & "Noen av kriteriene i spørringen vil være variabler. Oppgi disse i henhold til beskrivelsen til høyre." & vbLf & "For å teste queryen oppgis testparametere i rammen oppe til høyre, og klikk på testknappen. Resultatet vil vises i rammen nederst." & vbLf & "Husk å angi enkeltfnutt rundt strengfelt. Ved å klikke på fanen 'SQL tolket' kan du se hvordan queryen blir tolket med angitte testparametere."
                sEnglish = "Build a query for BabelBank to use during matching. The fields and their order that BabelBank needs is specified under." & vbLf & "Some of the criterias in the query is variables. State them according to the description in the frame at the right." & vbLf & "To test the query, state the testparameteres in the upper rightmost frame, and click the testbutton.The result will be shown in the frame below." & vbLf & "Remember to use hyphen around stringvariables. You may see how the SQL, with testparameters, is interpreted by clicking the tab 'SQL interpreted'."
                sSwedish = "Konstruera en fråga som BabelBank behöver för avstämning. Fälten och ordningen som BabelBank kräver kan du hitta nedan." & vbLf & "" & vbLf & "" & vbLf & "Några av kriterierna i frågan kommer at vara variabler. Ange dessa enligt beskrivningen till höger. " & vbLf & "" & vbLf & "" & vbLf & "För att testa frågan anges testparametrarna i rutan upp till höger, och klicka på testknappen. Resultatet visa i rutan nederst." & vbLf & "" & vbLf & "" & vbLf & "Kom ihåg att använda enkla citattecken under strängfälten. Genom att klicka på fliken  'SQL tolket' kan du se hur frågan tolkas med angivna testparametrar.	"
                sDanish = "Opbyg query som BabelBank skal have for at foretage en afstemning. Felterne og rækkefølgen som BabelBank kræver, er angivet under." & vbLf & "Nogle af kriterierne i forespørgslen vil være variabler. Angiv disse i henhold til beskrivelsen til højre." & vbLf & "For at teste queryen skal du angive testparametre i rammen oppe til højre, og klikke på testknappen. Resultatet vil vises i rammen nederst." & vbLf & "Husk at sætte enkle anførselstegn rundt om strengfeltet. Ved at klikke på fanen 'SQL tolket' kan du se hvordan queryen bliver tolket med de angivne testparametre.	"
            Case 60232
                sNorwegian = "Variabler"
                sEnglish = "Variables"
                sSwedish = "Variabler"
                sDanish = "Variabler"
            Case 60233
                sNorwegian = "Akkumulér transaksjoner"
                sEnglish = "Accumulate payments"
                sSwedish = "Ackumulera transaktioner"
                sDanish = "Akkumulér transaktioner"
            Case 60234
                sNorwegian = "Avvis transaksjoner hvis de har negativt beløp"
                sEnglish = "Reject payments with negative amount"
                sSwedish = "Avvisa transaktioner om de har negativt belopp"
                sDanish = "Afvis transaktioner, hvis de har negativt beløb"
            Case 60235
                sNorwegian = "Avvis transaksjoner hvis de mangler postnummer eller poststed"
                sEnglish = "Reject payments without zipcode"
                sSwedish = "Avvisa transaktioner om de saknade postnummer eller ort"
                sDanish = "Afvis transaktioner, hvis de mangler postnummer eller by"
            Case 60236
                sNorwegian = "Avvis transaksjoner hvis mottakers konto er ugyldig (kun Norge)"
                sEnglish = "Reject payments with invalid accountno"
                sSwedish = "Avvisa transaktion om mottagarens konto är ogiltigt (endast Norge)"
                sDanish = "Afvis transaktioner, hvis modtagers konto er ugyldig (kun Norge)"
            Case 60237
                sNorwegian = "Endre dato på FORFALTE oppdrag til dagens dato"
                sEnglish = "Change date on OVERDUE payments to todays date"
                sSwedish = "Ändra datum på FÖRFALLNA uppdrag till dagens datum"
                sDanish = "Ændr dato på FORFALDNE opgaver til dags dato"
            Case 60238
                sNorwegian = "Endre forfallsdato til dagens dato for ALLE oppdrag"
                sEnglish = "Change date on ALL payments to todays date"
                sSwedish = "Ändra förfallodatum till dagens datum för ALLA uppdrag"
                sDanish = "Ændr forfaldsdato til dags dato for ALLE opgaver"
            Case 60239
                sNorwegian = "Behold klientinndeling i betalingsfilene"
                sEnglish = "Keep clientseparation in paymentfiles"
                sSwedish = "Behåll klientindelning i betalningsfiler"
                sDanish = "Behold klientinddeling i betalingsfilerne"
            Case 60240
                sNorwegian = "Avvis ved feil oppbygde oppdrag"
                sEnglish = "Reject file if not according to format"
                sSwedish = "Avvisa vid felkonstruerade uppdrag"
                sDanish = "Afvis opgaver opbygget ved fejl"
            Case 60241
                sNorwegian = "Foretaksnummer"
                sEnglish = "Companynumber"
                sSwedish = "Företagsnummer"
                sDanish = "Virksomhedsnummer"
            Case 60242
                sNorwegian = "Foretaksnr. pr klient"
                sEnglish = "Different companyno pr client"
                sSwedish = "Företagsnr per klient"
                sDanish = "Virksomhedsnr. pr klient"
            Case 60243
                sNorwegian = "KundeenhetsID"
                sEnglish = "Additional ID"
                sSwedish = "KundenhetsID"
                sDanish = "KundeenhedsID"
            Case 60244
                sNorwegian = "Divisjon"
                sEnglish = "Div/NachaNo"
                sSwedish = "Division"
                sDanish = "Division"
            Case 60245
                sNorwegian = "Myndighetsrapportering"
                sEnglish = "Regulatory reporting"
                sSwedish = "Myndighetsrapportering"
                sDanish = "Myndighedsrapportering"
            Case 60246
                sNorwegian = "Ta med massetranser (lønn, pensjon) i returfiler"
                sEnglish = "Include 'masspayments' (salary etc) in returnfiles"
                sSwedish = "Inkludera masstransaktioner (löner, pensioner) i returfiler"
                sDanish = "Tag massetransaktioner (løn, pension) med i returfiler"
            Case 60247
                sNorwegian = "Tekst for brukerefinert menyvalg 1"
                sEnglish = "Text for userdefined menuline 1"
                sSwedish = "Text för användardefinierat menyalternativ 1"
                sDanish = "Tekst for brugerdefineret menuvalg 1"
            Case 60248
                sNorwegian = "Tekst for brukerefinert menyvalg 2"
                sEnglish = "Text for userdefined menuline 2"
                sSwedish = "Text för användardefinierat menyalternativ 2"
                sDanish = "Tekst for brugerdefineret menuvalg 2"
            Case 60249
                sNorwegian = "Tekst for brukerefinert menyvalg 3"
                sEnglish = "Text for userdefined menuline 3"
                sSwedish = "Text för användardefinierat menyalternativ 3"
                sDanish = "Tekst for brugerdefineret menuvalg 3"
            Case 60250
                sNorwegian = "Tekst for Oppsett, Brukerdefinerte menyvalg"
                sEnglish = "Text for Setupmenu, Userdefined menuitems"
                sSwedish = "Text för inställning, användardefinierat menyalternativ"
                sDanish = "Tekst for Opsætning, Brugerdefinerede menuvalg"
            Case 60251
                sNorwegian = "Tekst for kolonne 3, ...mønster"
                sEnglish = "Text for column 3, ... pattern"
                sSwedish = "Text för kolumn 3,... mönster"
                sDanish = "Tekst for kolonne 3, ...mønster"
            Case 60252
                sNorwegian = "Tekst for kolonne 4, ...mønster"
                sEnglish = "Text for column 4, ... pattern"
                sSwedish = "Text för kolumn 4,... mönster"
                sDanish = "Tekst for kolonne 4, ...mønster"
            Case 60253
                sNorwegian = "Generalnota"
                sEnglish = "General note"
                sSwedish = "Saldobesked"
                sDanish = "Generalnota"
            Case 60254
                sNorwegian = "Tekst for Avstemmingsoppsett"
                sEnglish = "Text for Setup Matching"
                sSwedish = "Text för avstämningsinställning"
                sDanish = "Tekst for Afstemningsopsætning"
            Case 60255
                sNorwegian = "Slette generalnotabeskrivelse"
                sEnglish = "Delete Generalnote description"
                sSwedish = "Ta bort saldobeskedsbeskrivning"
                sDanish = "Slet generalnotabeskrivelse"
            Case 60256
                sNorwegian = "Kreditt konto:"
                sEnglish = "Credit account:"
                sSwedish = "Kreditkonto:"
                sDanish = "Kredit konto:"
            Case 60257
                sNorwegian = "Bokf.dato:"
                sEnglish = "Bookdate:"
                sSwedish = "Bokf.datum:"
                sDanish = "Bogf.dato:"
            Case 60258
                sNorwegian = "Benytt veiviser"
                sEnglish = "Use wizard"
                sSwedish = "Använd guiden"
                sDanish = "Benyt vejviser"
            Case 60259
                sNorwegian = "Innbetalinger, uavstemte"
                sEnglish = "Incoming payments, Unmatched"
                sSwedish = "Inbetalningar, oavstämda"
                sDanish = "Indbetalinger, uafstemte"
            Case 60260
                sNorwegian = "Innbetalinger, avstemte"
                sEnglish = "Incoming payments, Matched"
                sSwedish = "Inbetalningar, avstämda"
                sDanish = "Indbetalinger, afstemte"
            Case 60261
                sNorwegian = "Innbetalinger, kontrollrapport"
                sEnglish = "Incoming payments, Control"
                sSwedish = "Inbetalningar, kontrollrapport"
                sDanish = "Indbetalinger, kontrolrapport"
            Case 60262
                sNorwegian = "Betalinger, sumrapport"
                sEnglish = "Payments, summary"
                sSwedish = "Betalningar, summarapport"
                sDanish = "Betalinger, sumrapport"
            Case 60263
                sNorwegian = "Betalinger"
                sEnglish = "Payments"
                sSwedish = "Betalningar"
                sDanish = "Betalinger"
            Case 60264
                sNorwegian = "Betalinger, fakturanivå"
                sEnglish = "Payments, invoicelevel"
                sSwedish = "Betalningar, fakturanivå"
                sDanish = "Betalinger, fakturaniveau"
            Case 60265
                sNorwegian = "Avviste betalinger"
                sEnglish = "Rejected payments"
                sSwedish = "Avvisade betalningar"
                sDanish = "Afviste betalinger"
            Case 60266
                sNorwegian = "Rapporter for importerte filer"
                sEnglish = "Reports for imported files"
                sSwedish = "Rapport för importerade filer"
                sDanish = "Rapporter for importerede filer"
            Case 60267
                sNorwegian = "Rapporter for valgt profil"
                sEnglish = "Reports for selected profile"
                sSwedish = "Rapport för vald profil"
                sDanish = "Rapporter for valgt profil"
            Case 60268
                sNorwegian = "Her bestemmer du hvilke rapporter du vil skrive ut for aktuell profil."
                sEnglish = "Use this setup to select which reports you like to show for the current profile."
                sSwedish = "Här bestämmer du vilka rapporter du vill skriva ut den aktuella profilen."
                sDanish = "Her bestemmer du hvilke rapporter du vil udskrive for aktuel profil."
            Case 60269
                sNorwegian = "ikke i bruk"
                sEnglish = "not in use"
                sSwedish = "Ej i bruk"
                sDanish = "ikke i brug"
            Case 60270
                sNorwegian = "Vis på skjerm"
                sEnglish = "Preview on screen"
                sSwedish = "Visa på skärm"
                sDanish = "Vis på skærm"
            Case 60271
                sNorwegian = "Send til skriver"
                sEnglish = "Send to printer"
                sSwedish = "Skicka till skrivare"
                sDanish = "Send til printer"
            Case 60272
                sNorwegian = "Velg skriver ved utskrift"
                sEnglish = "Select printer each time"
                sSwedish = "Välj skrivare vid utskrift"
                sDanish = "Vælg printer ved udskrivning"
            Case 60273
                sNorwegian = "Eksport"
                sEnglish = "Export"
                sSwedish = "Export"
                sDanish = "Eksport"
            Case 60274
                sNorwegian = "Filnavn for eksport"
                sEnglish = "Exportfilename"
                sSwedish = "Filnamn för export"
                sDanish = "Filnavn for eksport"
            Case 60275
                sNorwegian = "Skriveroppsett"
                sEnglish = "Printersetup"
                sSwedish = "Skrivarinställning"
                sDanish = "Printeropsætning"
            Case 60276
                sNorwegian = "Klient %1 må knyttes til minst ett kontonummer"
                sEnglish = "Client %1 must be connected to at least one account"
                sSwedish = "Klient %1 måste kopplas till minst ett kontonummer"
                sDanish = "Klient %1 skal knyttes til mindst et kontonummer"
            Case 60277
                sNorwegian = "Rapportoverskrift"
                sEnglish = "Reportheading"
                sSwedish = "Rapportrubrik"
                sDanish = "Rapportoverskrift"
            Case 60278
                sNorwegian = "Sideskift"
                sEnglish = "Pagebreak"
                sSwedish = "Sidbrytning"
                sDanish = "Sideskift"
            Case 60279
                sNorwegian = "Pr konto og dato"
                sEnglish = "Pr. account and date"
                sSwedish = "Per konto och datum"
                sDanish = "Pr. konto og dato"
            Case 60280
                sNorwegian = "Sum kontoutdrag"
                sEnglish = "Statementtotals"
                sSwedish = "Summa kontoutdrag"
                sDanish = "Sum kontoudskrift"
            Case 60281
                sNorwegian = "Hver betaling"
                sEnglish = "Each payment"
                sSwedish = "Varje betalning"
                sDanish = "Hver betaling"
            Case 60282
                sNorwegian = "Sumnivå"
                sEnglish = "Totallevel"
                sSwedish = "Summeringsnivå"
                sDanish = "Sumniveau"
            Case 60283
                sNorwegian = "Ingen summmer"
                sEnglish = "No totals"
                sSwedish = "Ingen summering"
                sDanish = "Ingen summer"
            Case 60284
                sNorwegian = "Kun sluttsum"
                sEnglish = "Grand Total only"
                sSwedish = "Endast slutsumma"
                sDanish = "Kun slutsum"
            Case 60285
                sNorwegian = "Alle summer"
                sEnglish = "All totals"
                sSwedish = "Alla summor"
                sDanish = "Alle summer"
            Case 60286
                sNorwegian = "Vis detaljer"
                sEnglish = "Show details"
                sSwedish = "Visa detaljer"
                sDanish = "Vis detaljer"
            Case 60287
                sNorwegian = "Avansert rapportoppsett"
                sEnglish = "Advanced reportsetup"
                sSwedish = "Avancerad rapportinställning"
                sDanish = "Avanceret rapportopsætning"
            Case 60288
                sNorwegian = "Forhåndsvalgt skriver"
                sEnglish = "Preselected printer"
                sSwedish = "Förhandsvald skrivare"
                sDanish = "Forhåndsvalgt printer"
            Case 60289
                sNorwegian = "En fil for hver betalingstype"
                sEnglish = "One file pr paymenttype"
                sSwedish = "En fil för varje betalningstyp"
                sDanish = "En fil for hver betalingstype"
            Case 60290
                sNorwegian = "Bilde for korrigering av feil:"
                sEnglish = "Correction form:"
                sSwedish = "Bild för korrigering av fel:"
                sDanish = "Billede for korrigering af fejl:"
            Case 60291
                sNorwegian = "Både Feil og Varsler"
                sEnglish = "Both Errors and Warnings"
                sSwedish = "Både fel och varningar"
                sDanish = "Både Fejl og Advarsler"
            Case 60292
                sNorwegian = "Bare Feilmarkerte"
                sEnglish = "Errors only"
                sSwedish = "Bara felmarkerade"
                sDanish = "Kun Fejlmarkerede"
            Case 60293
                sNorwegian = "Ingen visning"
                sEnglish = "No corrections"
                sSwedish = "Ingen visning"
                sDanish = "Ingen visning"
            Case 60294
                sNorwegian = "Mailadresse support BabelBank"
                sEnglish = "Mailadress support BabelBank"
                sSwedish = "Stöd för e-postadress i BabelBank"
                sDanish = "Mailadresse support BabelBank"
            Case 60295
                sNorwegian = "CC Mailadresse (kopi) support"
                sEnglish = "CC Mailadress (copy) support"
                sSwedish = "Stöd för CC-e-postadress (kopia)"
                sDanish = "CC Mailadresse (kopi) support"
            Case 60296
                sNorwegian = "Sti for kontofil fra TBIW"
                sEnglish = "TBIW Accountsfile Path"
                sSwedish = "Sökväg till kontofil"
                sDanish = "Sti for kontofil"
            Case 60297
                sNorwegian = "Kurs:"
                sEnglish = "Exch.rate:"
                sSwedish = "Kurs:"
                sDanish = "Kurs:"
            Case 60298
                sNorwegian = "Kontonr.:"
                sEnglish = "Accountno.:"
                sSwedish = "Kontonr.:"
                sDanish = "Kontonr.:"
            Case 60299
                sNorwegian = "Postnr./Sted:"
                sEnglish = "ZIP/City:"
                sSwedish = "Postnr/Ort:"
                sDanish = "Postnr./by:"
            Case 60300
                sNorwegian = "Det er ikke angitt sti for backup!"
                sEnglish = "No backuppath specified"
                sSwedish = "Det finns ingen angiven sökväg för säkerhetskopiering!"
                sDanish = "Der er ikke angivet sti for backup!"
            Case 60301
                sNorwegian = "Kontobeløp:"
                sEnglish = "Amount Acc:"
                sSwedish = "Kontobelopp:"
                sDanish = "Kontobeløb:"
            Case 60302
                sNorwegian = "Krypter XML-fil til DNB"
                sEnglish = "Crypt XML-files to DNB"
                sSwedish = "Kryptera XML-fil till DNB"
                sDanish = "Krypter XML-fil til DNB"
            Case 60303
                sNorwegian = "Avansert oppsett"
                sEnglish = "Advanced setup"
                sSwedish = "Avanc. inställningar"
                sDanish = "Avanceret opsætning"
            Case 60304
                sNorwegian = "Betalingstype"
                sEnglish = "Paymenttype"
                sSwedish = "Betalningstyp"
                sDanish = "Betalingstype"
            Case 60305
                sNorwegian = "Rapporter"
                sEnglish = "Report"
                sSwedish = "Rapportera"
                sDanish = "Rapporter"
            Case 60306
                sNorwegian = "Det er ikke angitt sti for kontofil fra TBIW"
                sEnglish = "TBIW Accountsfile path not specified"
                sSwedish = "Det finns ingen sökväg till kontofilen från TBIW"
                sDanish = "Der er ikke angivet en sti for kontofil fra TBIW"
            Case 60307
                sNorwegian = "Det er ikke angitt sti for fil fra regnskap!"
                sEnglish = "No path specified for file from accountingsystem!"
                sSwedish = "Det finns ingen sökväg till fil från redovisning!"
                sDanish = "Der er ikke angivet en sti for fil fra regnskab!"
            Case 60308
                sNorwegian = "Det er ikke angitt korrekt sti for fil til bank!"
                sEnglish = "No path specified for file to bank!"
                sSwedish = "Inte rätt sökväg till fil för banken!"
                sDanish = "Der er ikke angivet en korrekt sti for fil til bank!"
            Case 60309
                sNorwegian = "Det er ikke angitt korrekt sti for kontofil!"
                sEnglish = "Path for accountsfile not correctly  specified!"
                sSwedish = "Inte rätt sökväg till kontofil!"
                sDanish = "Det er ikke angivet en korrekt sti for kontofil!"
            Case 60310
                sNorwegian = "Det er ikke angitt korrekt sti for backupkatalog!"
                sEnglish = "Path for backup no correctly specified!"
                sSwedish = "Inte rätt sökväg till säkerhetskopieringskatalog!"
                sDanish = "Der er ikke angivet en korrekt sti for backupkatalog!"
            Case 60311
                sNorwegian = "Vis"
                sEnglish = "Show"
                sSwedish = "Visa"
                sDanish = "Vis"
            Case 60312
                sNorwegian = "Kun feilmeldinger"
                sEnglish = "Errormessages only"
                sSwedish = "Endast felmeddelanden"
                sDanish = "Kun fejlmeddelelser"
            Case 60313
                sNorwegian = "Også varsler"
                sEnglish = "Warnings also"
                sSwedish = "Även aviseringar"
                sDanish = "Også advarsler"
            Case 60314
                sNorwegian = "Også infomeldinger"
                sEnglish = "Infomessages also"
                sSwedish = "Även infomeddelanden"
                sDanish = "Også infomeddelelser"
            Case 60315
                sNorwegian = "Alle meldinger"
                sEnglish = "All messages"
                sSwedish = "Alla meddelanden"
                sDanish = "Alle meddelelser"
            Case 60316
                sNorwegian = "Automatisk avstemming"
                sEnglish = "Automatic matching"
                sSwedish = "Automatisk avstämning"
                sDanish = "Automatisk afstemning"
            Case 60317
                sNorwegian = "Manuell avstemming"
                sEnglish = "Manual matching"
                sSwedish = "Manuell avstämning"
                sDanish = "Manuel afstemning"
            Case 60318
                sNorwegian = "Etterbehandling"
                sEnglish = "Post matching"
                sSwedish = "Efterbehandling"
                sDanish = "Efterbehandling"
            Case 60319
                sNorwegian = "Mønster:"
                sEnglish = "Pattern:"
                sSwedish = "Mönster:"
                sDanish = "Mønster:"
            Case 60320
                sNorwegian = "Spesial:"
                sEnglish = "Special:"
                sSwedish = "Special:"
                sDanish = "Special:"
            Case 60321
                sNorwegian = "BBRET_Amount, BBRET_Currency, BBRET_InvoiceNo, BBRET_CustomerNo, BBRET_ClientNo" & vbLf & "BBRET_MyField, BBRET_Freetext, BBRET_MatchID, BBRET_AkontoID, BBRET_GLID"
                sEnglish = "BBRET_Amount, BBRET_Currency, BBRET_InvoiceNo, BBRET_CustomerNo, BBRET_ClientNo" & vbLf & "BBRET_MyField, BBRET_Freetext, BBRET_MatchID, BBRET_AkontoID, BBRET_GLID"
                sSwedish = "BBRET_Amount, BBRET_Currency, BBRET_InvoiceNo, BBRET_CustomerNo, BBRET_ClientNo" & vbLf & "BBRET_MyField, BBRET_Freetext, BBRET_MatchID, BBRET_AkontoID, BBRET_GLID	"
                sDanish = "BBRET_Amount, BBRET_Currency, BBRET_InvoiceNo, BBRET_CustomerNo, BBRET_ClientNo" & vbLf & "BBRET_MyField, BBRET_Freetext, BBRET_MatchID, BBRET_AkontoID, BBRET_GLID	"
            Case 60322
                sNorwegian = "Innbetalinger med ID (OCR)"
                sEnglish = "Incoming payments with ID (OCR)"
                sSwedish = "Inbetalningar med ID (OCR)"
                sDanish = "Indbetalinger med ID (OCR)"
            Case 60323
                sNorwegian = "FIK-Innbetalinger"
                sEnglish = "Incoming FIK-payments"
                sSwedish = "FIK-betalningar"
                sDanish = "FIK-Indbetalinger"
            Case 60324
                sNorwegian = "Logg Start/Stopp meldinger"
                sEnglish = "Log Start/Stop events"
                sSwedish = "Meddelanden om loggstart/-stopp"
                sDanish = "Log Start/Stop meddelelser"
            Case 60325
                sNorwegian = "Navn på filer for oppdatering av regnskap/reskontro"
                sEnglish = "Exportfilenames"
                sSwedish = "Namn på filer för uppdatering av redovisning/reskontra"
                sDanish = "Navn på filer for opdatering af regnskab/samlekonto"
            Case 60326
                sNorwegian = "Returfiler til regnskap/reskontro"
                sEnglish = "Returnfiles to accounting/ledger-system"
                sSwedish = "Returfiler till redovisning/reskontra"
                sDanish = "Returfiler til regnskab/samlekonto"
            Case 60327
                sNorwegian = "Hovedbok"
                sEnglish = "General Ledger"
                sSwedish = "Huvudbok"
                sDanish = "Hovedbog"
            Case 60328
                sNorwegian = "OCR"
                sEnglish = "OCR"
                sSwedish = "OCR"
                sDanish = "OCR"
            Case 60329
                sNorwegian = "Autogiro"
                sEnglish = "Autogiro"
                sSwedish = "Autogiro"
                sDanish = "Autogiro"
            Case 60330
                sNorwegian = "Øvrige"
                sEnglish = "Rest"
                sSwedish = "Annat"
                sDanish = "Øvrige"
            Case 60331
                sNorwegian = "Vennligst fyll inn filnavn"
                sEnglish = "Please fill in filename"
                sSwedish = "Ange filnamn"
                sDanish = "Angiv filnavn"
            Case 60332
                sNorwegian = "Ugyldig filnavn"
                sEnglish = "Invalid filename"
                sSwedish = "Ogiltigt filnamn"
                sDanish = "Ugyldigt filnavn"
            Case 60333
                sNorwegian = "Vennligst velg format"
                sEnglish = "Please select a fileformat"
                sSwedish = "Välj format"
                sDanish = "Vælg format"
            Case 60334
                sNorwegian = "Vennligst velg filtype"
                sEnglish = "Please select filetype"
                sSwedish = "Välj filtyp"
                sDanish = "Vælg filtype"
            Case 60335
                sNorwegian = "Angi de filnavn som skal legges ut som grunnlag for oppdatering av regnskap/reskontro. Dette gjelder filer med avstemte og uavstemte poster, samt OCR-poster og eventuelt Autogiroposter. I tillegg kan det legges ut egen fil for oppdatering av hovedbok.  Spesialtegn: # erstattes av Database (se eget oppsett)  & erstattes av Banknavn."
                sEnglish = "Specify filenames which will be used to update accounting/ledger-system. These files holds both matched and unmatched payments,  OCR-payments and  Autogiropayments. There may also be a file to update General Ledger.  Specialcharacters: # is translated into Database (see special setup)  & is exchanged into Bankname."
                sSwedish = "Ange filnamn för underlag till uppdatering av redovisning/reskontra. Detta inkluderar filer med matchade och omatchade poster, samt OCR-poster och eventuella autogiroposter. Dessutom en egen fil användas för uppdatering av huvudboken. Specialtecken: # ersätts med databas (se separat vy), & ersätts med bankens namn."
                sDanish = "Angiv de filnavne, der skal lægges ud som grundlag for opdatering af regnskab/samlekonto. Dette gælder filer med afstemte og uafstemte poster, samt OCR-poster og eventuelt Autogiroposter. Derudover kan der lægges egen fil ud for opdatering af hovedbog.  Specialtegn: # erstattes af Database (se egen opsætning)  & erstattes af Banknavn."
            Case 60336
                sNorwegian = "Klient detaljer"
                sEnglish = "Client details"
                sSwedish = "Klientdetaljer"
                sDanish = "Klient detaljer"
            Case 60337
                sNorwegian = "áKonto mønster"
                sEnglish = "On account pattern"
                sSwedish = "aKonto-mönster"
                sDanish = "áconto mønster"
            Case 60338
                sNorwegian = "Hovedboks mønster"
                sEnglish = "General Ledger pattern"
                sSwedish = "Huvudboksmönster"
                sDanish = "Hovedboks mønster"
            Case 60339
                sNorwegian = "Hopp automatisk til neste post når differanse er null i manuell avstemming"
                sEnglish = "Jump to next payment when zero-difference in Manual Matching"
                sSwedish = "Gå till nästa post automatiskt när skillnaden är noll i manuell avstämning"
                sDanish = "Hop automatisk til næste post, når differencen er nul i manuel afstemning"
            Case 60340
                sNorwegian = "Velg kolonner for avstemte poster i manuell avstemming"
                sEnglish = "Show which columns for Matched payments in Manual Matching"
                sSwedish = "Välj kolumner för avstämda poster i manuell avstämning"
                sDanish = "Vælg kolonner for afstemte poster i manuel afstemning"
            Case 60341
                sNorwegian = "Avstemming"
                sEnglish = "Matching"
                sSwedish = "Avstämning"
                sDanish = "Afstemning"
            Case 60342
                sNorwegian = "Ny"
                sEnglish = "New"
                sSwedish = "Ny"
                sDanish = "Ny"
            Case 60343
                sNorwegian = "Lagre klient"
                sEnglish = "Save client"
                sSwedish = "Spara klient"
                sDanish = "Gem klient"
            Case 60344
                sNorwegian = "Hovedbokskonto"
                sEnglish = "General Ledger account"
                sSwedish = "Huvudbokskonto"
                sDanish = "Hovedbogskonto"
            Case 60345
                sNorwegian = "Kundereskontro"
                sEnglish = "Account Receivable"
                sSwedish = "Kundreskontra"
                sDanish = "Kundesamlekonto"
            Case 60346
                sNorwegian = "KID"
                sEnglish = "OCR code"
                sSwedish = "OCR"
                sDanish = "KID"
            Case 60347
                sNorwegian = "Fra ERP"
                sEnglish = "From ERP"
                sSwedish = "Från ERP"
                sDanish = "Fra ERP"
            Case 60348
                sNorwegian = "Kundenummer"
                sEnglish = "Customer number"
                sSwedish = "Kundnummer"
                sDanish = "Kundenummer"
            Case 60349
                sNorwegian = "Fakturanummer"
                sEnglish = "Invoice number"
                sSwedish = "Fakturanummer"
                sDanish = "Fakturanummer"
            Case 60350
                sNorwegian = "Debet"
                sEnglish = "Debit"
                sSwedish = "Debet"
                sDanish = "Debet"
            Case 60351
                sNorwegian = "Kredit"
                sEnglish = "Credit"
                sSwedish = "Kredit"
                sDanish = "Kredit"
            Case 60352
                sNorwegian = "Valuta"
                sEnglish = "Currency"
                sSwedish = "Valuta"
                sDanish = "Valuta"
            Case 60353
                sNorwegian = "Regnskapskonto"
                sEnglish = "Ledger account"
                sSwedish = "Redovisningskonto"
                sDanish = "Regnskabskonto"
            Case 60354
                sNorwegian = "Unik referanse"
                sEnglish = "Unique reference"
                sSwedish = "Unik referenc"
                sDanish = "Unik referanse"
            Case 60355
                sNorwegian = "Fritekst"
                sEnglish = "Freetext"
                sSwedish = "Fritext"
                sDanish = "Fritekst"
            Case 60356
                sNorwegian = "Kontonummer"
                sEnglish = "Account Number"
                sSwedish = "Kontonummer"
                sDanish = "Kontonummer"
            Case 60357
                sNorwegian = "Importerte filer"
                sEnglish = "Importfiles"
                sSwedish = "Importerade filer"
                sDanish = "Importerede filer"
            Case 60358
                sNorwegian = "Avvikskonti"
                sEnglish = "Deviation accounts"
                sSwedish = "Avvikelsekonton"
                sDanish = "Afvigelseskonti"
            Case 60359
                sNorwegian = "Kontotype"
                sEnglish = "Accounttype"
                sSwedish = "Kontotyp"
                sDanish = "Kontotype"
            Case 60360
                sNorwegian = "Mønstertype"
                sEnglish = "Patterntype"
                sSwedish = "Mönstertyp"
                sDanish = "Mønstertype"
            Case 60361
                sNorwegian = "Navn på mønstertype"
                sEnglish = "Patterntype name"
                sSwedish = "Namn på mönstertyp"
                sDanish = "Navn på mønstertype"
            Case 60362
                sNorwegian = "Mønster"
                sEnglish = "Patterns"
                sSwedish = "Mönster"
                sDanish = "Mønster"
            Case 60363
                sNorwegian = "Mønster"
                sEnglish = "Pattern"
                sSwedish = "Mönster"
                sDanish = "Mønster"
            Case 60364
                sNorwegian = "Lagre mønstertype"
                sEnglish = "Save Patterntype"
                sSwedish = "Spara mönstertyp"
                sDanish = "Gem mønstertype"
            Case 60365
                sNorwegian = "Reskontro"
                sEnglish = "Ledger"
                sSwedish = "Reskontra"
                sDanish = "Samlekonto"
            Case 60366
                sNorwegian = "Kunde"
                sEnglish = "Customer"
                sSwedish = "Kunde"
                sDanish = "Kunde"
            Case 60367
                sNorwegian = "Fra SQL"
                sEnglish = "From SQL"
                sSwedish = "Från SQL"
                sDanish = "Fra SQL"
            Case 60368
                sNorwegian = "Avstemming, oppsummering"
                sEnglish = "Matching, summary"
                sSwedish = "Avstämning, sammanfattning"
                sDanish = "Afstemning, opsummering"
            Case 60369
                sNorwegian = "Avstemming, aKonto"
                sEnglish = "Matching, on account"
                sSwedish = "Avstämning, aKonto"
                sDanish = "Afstemning, aconto"
            Case 60370
                sNorwegian = "Navn på tilkobling:"
                sEnglish = "Name of connection:"
                sSwedish = "Namn på anslutning:"
                sDanish = "Navn på tilkobling:"
            Case 60371
                sNorwegian = "Påloggingsstreng:"
                sEnglish = "Connectionstring:"
                sSwedish = "Inloggningssträng:"
                sDanish = "Pålogningsstreng:"
            Case 60372
                sNorwegian = "Bruker-ID:"
                sEnglish = "User ID:"
                sSwedish = "Användar-ID:"
                sDanish = "Bruger-ID:"
            Case 60373
                sNorwegian = "Passord:"
                sEnglish = "Password:"
                sSwedish = "Lösenord:"
                sDanish = "Kodeord:"
            Case 60374
                sNorwegian = "&Test"
                sEnglish = "&Test"
                sSwedish = "&Test"
                sDanish = "&Test"
            Case 60375
                sNorwegian = "Klientnummeret er benyttet for annen klient!"
                sEnglish = "This clientnumber is used for another client"
                sSwedish = "Klientnumret används för en annan klient!"
                sDanish = "Klientnummeret er benyttet til anden klient!"
            Case 60376
                sNorwegian = "Kontodetaljer"
                sEnglish = "Account details"
                sSwedish = "Kontodetaljer"
                sDanish = "Kontodetaljer"
            Case 60377
                sNorwegian = "Avtalegiro ID"
                sEnglish = "Avtalegiro ID"
                sSwedish = "Avtalsgiro-ID"
                sDanish = "Aftalegiro ID"
            Case 60378
                sNorwegian = "Ingen klient er valgt!"
                sEnglish = "No client selected!"
                sSwedish = "Ingen klient är vald!"
                sDanish = "Der er ikke valgt en klient!"
            Case 60379
                sNorwegian = "Dette kontonummer er benyttet før! (For klient %1)"
                sEnglish = "This accountno is already in use (Client %1)"
                sSwedish = "Detta kontonummer används redan! (För klient %1)"
                sDanish = "Dette kontonummer er benyttet før! (For klient %1)"
            Case 60380
                sNorwegian = "Dette klientnummer er benyttet før! (For %1)"
                sEnglish = "This clientno is already in use (Client %1)"
                sSwedish = "Detta klientnummer används redan! (För %1)"
                sDanish = "Dette klientnummer er benyttet før! (For %1)"
            Case 60381
                sNorwegian = "Slette"
                sEnglish = "Delete"
                sSwedish = "Ta bort"
                sDanish = "Slette"
            Case 60382
                sNorwegian = "Konto for uavstemte"
                sEnglish = "Unmatched account"
                sSwedish = "Konto för oavstämda"
                sDanish = "Konto for uafstemte"
            Case 60383
                sNorwegian = "Tilgjenglige profiler"
                sEnglish = "Available profiles"
                sSwedish = "Tillgängliga profiler"
                sDanish = "Tilgængelige profiler"
            Case 60384
                sNorwegian = "Valgte profiler"
                sEnglish = "Selected profiles"
                sSwedish = "Valda profiler"
                sDanish = "Valgte profiler"
            Case 60385
                sNorwegian = "Tilgjenglige varslinger"
                sEnglish = "Available notifications"
                sSwedish = "Tillgängliga aviseringar"
                sDanish = "Tilgængelige varslinger"
            Case 60386
                sNorwegian = "Valgte varslinger"
                sEnglish = "Selected notifications"
                sSwedish = "Valda aviseringar"
                sDanish = "Valgte varslinger"
            Case 60387
                sNorwegian = "Knytt klient til varslinger"
                sEnglish = "Connect client with notifications"
                sSwedish = "Koppla klient till aviseringar"
                sDanish = "Knyt klient til varslinger"
            Case 60388
                sNorwegian = "Knytt klient til profiler"
                sEnglish = "Connect client with profiles"
                sSwedish = "Koppla klient till profiler"
                sDanish = "Knyt klient til profiler"
            Case 60389
                sNorwegian = "Nattsafe, total"
                sEnglish = "Depositbox, total"
                sSwedish = "Nattsafe, summa"
                sDanish = "Natsafe, total"
            Case 60390
                sNorwegian = "Nattsafe, differanse"
                sEnglish = "Depositbox, differences only"
                sSwedish = "Nattsafe, differens"
                sDanish = "Natsafe, difference"
            Case 60391
                sNorwegian = "Slå sammen betalinger til samme mottaker"
                sEnglish = "Merge payments to the same benificary"
                sSwedish = "Slå ihop betalningar till samma mottagare"
                sDanish = "Slå betalinger til samme modtager sammen"
            Case 60392
                sNorwegian = "Velg eksportformat"
                sEnglish = "Select exportformat"
                sSwedish = "Välj exportformat"
                sDanish = "Vælg eksportformat"
            Case 60393
                sNorwegian = "SWIFT adresse"
                sEnglish = "SWIFT Address"
                sSwedish = "SWIFT-adress"
                sDanish = "SWIFT adresse"
            Case 60394
                sNorwegian = "Konto for kontantrabatt"
                sEnglish = "Cashdiscount account"
                sSwedish = "Konto för kassarabatt"
                sDanish = "Konto for kontantrabat"
            Case 60395
                sNorwegian = "Send e-mail med betalingsinformasjon"
                sEnglish = "Send e-mail with paymentinfo"
                sSwedish = "Skicka e-post med betalningsinformation"
                sDanish = "Send e-mail med betalingsinformation"
            Case 60396
                sNorwegian = "Skriv ut betalingsinformasjon til printer"
                sEnglish = "Write paymentinfo to printer"
                sSwedish = "Skriv ut betalningsinformation till skrivaren"
                sDanish = "Udskriv betalingsinformation til printer"
            Case 60397
                sNorwegian = "For betalinger med mer enn 140 tegn"
                sEnglish = "For payments with more than 140 chars."
                sSwedish = "För betalningar med mer än 140 tecken"
                sDanish = "Til betalinger med mere end 140 tegn"
            Case 60398
                sNorwegian = "For alle betalinger"
                sEnglish = "For all payments"
                sSwedish = "För alla betalningar"
                sDanish = "Til alle betalinger"
            Case 60399
                sNorwegian = "Mailadresse(r), adskilt med ;"
                sEnglish = "Mailaddresses, separated with ;"
                sSwedish = "E-postadress(er), avgränsade med ;"
                sDanish = "Mailadresse(r), adskilt med ;"
            Case 60400
                sNorwegian = "Word-template for printerfil"
                sEnglish = "Wordtemplate for printfile"
                sSwedish = "Word-mall för skrivarfil"
                sDanish = "Word-template for printerfil"
            Case 60401
                sNorwegian = "Momskode"
                sEnglish = "VATCode"
                sSwedish = "Momskod"
                sDanish = "Momskode"
            Case 60402
                sNorwegian = "Avrunding"
                sEnglish = "Adjustments"
                sSwedish = "Avrundning"
                sDanish = "Afrunding"
            Case 60403
                sNorwegian = "Forslagsavstem"
                sEnglish = "Propose match"
                sSwedish = "Avstämningsförslag"
                sDanish = "Foreslået afstemning"
            Case 60404
                sNorwegian = "Avstem OCR"
                sEnglish = "Include OCR"
                sSwedish = "Avstäm OCR"
                sDanish = "Afstem OCR"
            Case 60405
                sNorwegian = "Den krypterte SQL-setningen er for lang til å bli lagret i BabelBank." & vbLf & "Maksimumslengde for en krysptert SQL er %2 karakterer." & vbLf & "" & vbLf & "Din SQL-setning er %1 karakterer lang."
                sEnglish = "The crypted query stated is too long to be saved in BabelBank." & vbLf & "Maximum length for an encrypted SQL are %2 characters." & vbLf & "" & vbLf & "Your query is %1 characters long."
                sSwedish = "Den krypterade SQL-satsen är för lång för att lagras i BabelBank." & vbLf & "Maximal längd för en en krysptert SQL er %2 tecken." & vbLf & "" & vbLf & "Din SQL-sats är %1 tecken lång.	"
                sDanish = "Den krypterede SQL-sætning er for lang til at blive gemt i BabelBank." & vbLf & "Maksimumslængde for en krypteret SQL er %2 karakterer." & vbLf & "" & vbLf & "Din SQL-sætning er %1 karakterer lang.	"
            Case 60406
                sNorwegian = "KAN IKKE LAGRE REGELEN."
                sEnglish = "CAN'T SAVE THE RULE."
                sSwedish = "KAN INTE SPARA REGELN."
                sDanish = "KAN IKKE GEMME REGLEN."
            Case 60407
                sNorwegian = "Feltet 'Navn' er obligatorisk."
                sEnglish = "The field 'Name' is mandatory."
                sSwedish = "Fältet '’Namn'’ är obligatoriskt."
                sDanish = "Feltet 'Navn' er obligatorisk."
            Case 60408
                sNorwegian = "Feltet 'Type' er obligatorisk."
                sEnglish = "The field 'Type' is mandatory."
                sSwedish = "Fältet ''Typ'' är obligatoriskt."
                sDanish = "Feltet 'Type' er obligatorisk."
            Case 60409
                sNorwegian = "Når reglen er av type 1, må du koble regelen til et mønster." & vbLf & "" & vbLf & "Du kan legge til et nytt mønster i klientoppsettet."
                sEnglish = "When the query is of type 1, you should link the rule with a pattern." & vbLf & "" & vbLf & "You can add a new pattern in the client-setup."
                sSwedish = "När regeln är av typ 1 måste du koppla regeln till ett mönster." & vbLf & "" & vbLf & "Du kan lägga till ett nytt mönster i klientinställningen.	"
                sDanish = "Når reglen er type 1, skal du koble reglen til et mønster." & vbLf & "" & vbLf & "Du kan tilføje et nyt mønster i klientopsætningen.	"
            Case 60410
                sNorwegian = "Når regelen er av type 3, må du legge inn et unikt begrep i feltet 'Spesial'" & vbLf & "i henhold til informasjon fra din forhandler."
                sEnglish = "When the query is of type 3, you should add a text in the field 'Special'" & vbLf & "according to an agreement with your dealer."
                sSwedish = "När regeln är av typ 3 måste du lägga in ett unikt begrepp i fältet 'Special'" & vbLf & "i enlighet med informationen från din återförsäljare.	"
                sDanish = "Når reglen er type 3, skal du indtaste et unikt begreb i feltet 'Special'" & vbLf & "i henhold til informationen fra din forhandler.	"
            Case 60411
                sNorwegian = "En uventet feil oppsto:" & vbLf & "Kan ikke lagre regelen."
                sEnglish = "An unexcpected error occured:" & vbLf & "Can't save the rule"
                sSwedish = "Ett oväntat fel inträffade:" & vbLf & "Kan inte spara regeln.	"
                sDanish = "Der opstod en uventet fejl:" & vbLf & "Kan ikke gemme reglen.	"
            Case 60412
                sNorwegian = "Eksakt"
                sEnglish = "Exact"
                sSwedish = "Exakt"
                sDanish = "Eksakt"
            Case 60413
                sNorwegian = "Mail til DNB kontorer"
                sEnglish = "Mail to DNB branches"
                sSwedish = "E-post till DNB-kontor"
                sDanish = "Mail til DNB kontorer"
            Case 60414
                sNorwegian = "Slå sammen betalinger, og behold all informasjon til mottaker"
                sEnglish = "Merge payments, and keep all info to receiver"
                sSwedish = "Slå ihop betalningar och behålla all information till mottagare"
                sDanish = "Slå betalinger sammen, og behold alle informationer til modtager"
            Case 60415
                sNorwegian = "Slå sammen betalinger, uavhengig av om informasjon forsvinner"
                sEnglish = "Merge payments, regardless of informationloss"
                sSwedish = "Slå ihop oavsett om informationen försvinner"
                sDanish = "Slå betalinger sammen, uafhængig af om informationerne forsvinder"
            Case 60416
                sNorwegian = "Ikke slå sammen betalinger"
                sEnglish = "Do not merge payments"
                sSwedish = "Slå inte ihop betalningar"
                sDanish = "Slå ikke betalinger sammen"
            Case 60417
                sNorwegian = "Ingen DB-Profile er valgt!"
                sEnglish = "No DB_Profile selected!"
                sSwedish = "Ingen DB-profil är vald!"
                sDanish = "Ingen DB-Profiler er valgt!"
            Case 60418
                sNorwegian = "Gi mulighet for å endre alle data i korreksjonsbildet"
                sEnglish = "Open possibilty to edit all data in correctionform"
                sSwedish = "Ge möjlighet att ändra alla data i korrigeringsbild"
                sDanish = "Giv mulighed for at ændre alle data i korrektionsbilledet"
            Case 60419
                sNorwegian = "Avstemming, Hovedbok"
                sEnglish = "Matching - General Ledger"
                sSwedish = "Avstämning, huvudbok"
                sDanish = "Afstemning, Hovedbog"
            Case 60420
                sNorwegian = "Pr konto"
                sEnglish = "Pr account"
                sSwedish = "Per konto"
                sDanish = "Pr. konto"
            Case 60421
                sNorwegian = "Ikke bruk *.* som importfilnavn!"
                sEnglish = "Please do not use *.* as filename"
                sSwedish = "Använd inte *.* som importfilnamn!"
                sDanish = "Brug ikke *.* som importfilnavn!"
            Case 60422
                sNorwegian = "Ikke bruk * som del av filnavn til bank!"
                sEnglish = "Please do not use * as part of filename to bank"
                sSwedish = "Använd inte * som del av filnamn till bank!"
                sDanish = "Brug ikke * som del af filnavn til bank!"
            Case 60423
                sNorwegian = "Oppgi kun mappenavn for kontosti (ikke bruk *)"
                sEnglish = "Please specify path only for accountsfile (do not include *)"
                sSwedish = "Ange bara mappnamn i kontosökväg (använd inte *)"
                sDanish = "Angiv kun mappenavn for kontosti (brug ikke *)"
            Case 60424
                sNorwegian = "Oppgi kun mappenavn for backupsti (ikke bruk *)"
                sEnglish = "Please specify path only for backup (do not include *)"
                sSwedish = "Ange bara mappnamn i säkerhetskopieringssökväg (använd inte *)"
                sDanish = "Angiv kun mappenavn for backupsti (brug ikke *)"
            Case 60425
                sNorwegian = "Bilagsserie"
                sEnglish = "Voucher type"
                sSwedish = "Bilaga serie"
                sDanish = "Bilagsserie"
            Case 60426
                sNorwegian = "Er du sikker på at du ønsker å slette valgte regelen?"
                sEnglish = "Are You sure You want to delete the selected rule?"
                sSwedish = "Vill du ta bort den valda regeln?"
                sDanish = "Er du sikker på at du ønsker at slette den valgte regel?"
            Case 60427
                sNorwegian = "SLETT REGEL?"
                sEnglish = "DELETE RULE?"
                sSwedish = "TA BORT REGEL?"
                sDanish = "SLET REGEL?"
            Case 60428
                sNorwegian = "Bilagsnummer"
                sEnglish = "Voucher number"
                sSwedish = "Bilaganummer 2"
                sDanish = "Bilagsnummer 2"
            Case 60429
                sNorwegian = "Billagsserie 2"
                sEnglish = "Voucher type 2"
                sSwedish = "Verif.serie 2"
                sDanish = "Billagsserie 2"
            Case 60430
                sNorwegian = "Åpne poster"
                sEnglish = "Open items"
                sSwedish = "Öppna poster"
                sDanish = "Åbne poster"
            Case 60431
                sNorwegian = "Dagtotaler"
                sEnglish = "Day totals"
                sSwedish = "Dagsummor"
                sDanish = "Dagtotaler"
            Case 60432
                sNorwegian = "Ikke benytt"
                sEnglish = "Don't use"
                sSwedish = "Används inte"
                sDanish = "Anvend ikke"
            Case 60433
                sNorwegian = "Hent fra ERP"
                sEnglish = "Retrieve from ERP"
                sSwedish = "Hämta från ERP"
                sDanish = "Hent fra ERP"
            Case 60434
                sNorwegian = "Hent fra BB"
                sEnglish = "Retrieve from BB"
                sSwedish = "Hämta från  BB"
                sDanish = "Hent fra BB"
            Case 60435
                sNorwegian = "Angi om du ønsker at BabelBank skal legge på bilagsnummer på betalingene, og hvor BabelBank i så fall skal hente sist brukte bilagsnummer?" & vbLf & "Hvis du skal hente fra ERP, må du angi en SQL for å hente ut dette under 'Avstemmingsregler'." & vbLf & "Hvis du skal hente i fra BabelBank, må sist brukte bilagsnummer legges inn under."
                sEnglish = "Do You want BabelBank to add a vouchernumber to the payment, and if so, from where shall BabelBank retrieve the last used vouchernumber?" & vbLf & "You have to add a SQL under 'Matchingrules', if BB shall retrieve the voucherno from the ERP-system." & vbLf & "You have to enter the last used invoicenumber below, if the voucherno shall be retrieved from BB."
                sSwedish = "Ange om du vill att BabelBank ska lägga till bilaganummer för betalningarna och hur BabelBank i så fall ska hämta senast använda bilaganummer." & vbLf & "Hvis du skal hente fra ERP, må du angi en SQL for å hente ut dette under 'Avstemmingsregler'." & vbLf & "Hvis du skal hente i fra BabelBank, må sist brukte bilagsnummer legges inn under.	"
                sDanish = "Angiv om du ønsker at BabelBank skal tilføje bilagsnummer på betalingerne, og hvor BabelBank så skal hente sidst brugte bilagsnummer?" & vbLf & "Hvis du skal hente fra ERP, skal du angive en SQL for at hente det ud under 'Afstemningsregler'." & vbLf & "Hvis du skal hente fra BabelBank, skal sidst brugte bilagsnummer lægges ind under.	"
            Case 60436
                sNorwegian = "Autobokføring"
                sEnglish = "Autobooking"
                sSwedish = "Autobokföring"
                sDanish = "Autobogføring"
            Case 60437
                sNorwegian = "Velg fra felt"
                sEnglish = "From field"
                sSwedish = "Välj från fält"
                sDanish = "Vælg fra felt"
            Case 60438
                sNorwegian = "Feltets verdi"
                sEnglish = "Field value"
                sSwedish = "Fältvärde"
                sDanish = "Feltets værdi"
            Case 60439
                sNorwegian = "Bokfør til konto"
                sEnglish = "Book to accountno"
                sSwedish = "Bokför på konto"
                sDanish = "Bogfør til konto"
            Case 60440
                sNorwegian = "Informasjon til Norges Bank"
                sEnglish = "Information to the Central bank of Norway"
                sSwedish = "Information till Riksbanken"
                sDanish = "Information til Nationalbanken"
            Case 60441
                sNorwegian = "Denne standardkoden og teksten vil sendes til Norges Bank som en forklaring på hva betalingen gjelder, hvis ikke annen informasjon er angitt på betalingen." & vbLf & "Dette gjelder kun i de tilfellene hvor betalingen debiteres en konto i Norge og krediteres en konto i utlandet."
                sEnglish = "This standard code and text will be sent to the Central bank of Norway as an explanation of the reason to the payment, unless other information is stated on the payment." & vbLf & "This information only applies to payments which will be debited an account in Norway and credited an account abroad."
                sSwedish = "Standardkoden och texten kommer att skickas med en förklaring av vad betalningen avser, om inget annat anges på betalningen." & vbLf & "Detta gäller endast i de fall där betalningen debiteras ett konto i Sverige och krediteras ett konto i utlandet.	"
                sDanish = "Denne standardkode og tekst sendes til Nationalbanken som en forklaring på, hvad betalingen gælder for, hvis ikke anden information er angivet på betalingen." & vbLf & "" & vbLf & "Dette gælder kun i de tilfælde, hvor betalingen debiteres en konto i Danmark og krediteres en konto i udlandet.	"
            Case 60442
                sNorwegian = "Kode"
                sEnglish = "Code"
                sSwedish = "Kod"
                sDanish = "Kode"
            Case 60443
                sNorwegian = "Din standardtekst:"
                sEnglish = "Your standardtext:"
                sSwedish = "Din standardtext:"
                sDanish = "Din standardtekst:"
            Case 60446
                sNorwegian = "Vennligst legg inn en standardtekst som forklarer hva betalingen gjelder."
                sEnglish = "Please enter a standard text to explain the reason for the payments."
                sSwedish = "Ange en standardtext som förklarar vad betalningen avser."
                sDanish = "Angiv en standardtekst som forklarer hvad betalingen gælder for."
            Case 60447
                sNorwegian = "MANGLENDE INFORMASJON"
                sEnglish = "MISSING INFORMATION"
                sSwedish = "INFORMATION SOM SAKNAS"
                sDanish = "MANGLENDE INFORMATION"
            Case 60449
                sNorwegian = "Manuelt valg av filer"
                sEnglish = "Select files manually"
                sSwedish = "Manuellt val av filer"
                sDanish = "Manuelt valg af filer"
            Case 60450
                sNorwegian = "Ikke korrigér faktura"
                sEnglish = "Don't adjust invoice"
                sSwedish = "Inte rätt faktura"
                sDanish = "Korrigér ikke faktura"
            Case 60451
                sNorwegian = "Antall å trekke fra:"
                sEnglish = "No. to deduct:"
                sSwedish = "Antal som ska subtraheras:"
                sDanish = "Antal, der skal fratrækkes:"
            Case 60452
                sNorwegian = "Aktiv"
                sEnglish = "Active"
                sSwedish = "Aktiv"
                sDanish = "Aktiv"
            Case 60453
                sNorwegian = "Profilens foretaksnummer"
                sEnglish = "Profilespesific companyno"
                sSwedish = "Profilens företagsnummer"
                sDanish = "Profilens virksomhedsnummer"
            Case 60454
                sNorwegian = "Filter"
                sEnglish = "Filter"
                sSwedish = "Filter"
                sDanish = "Filter"
            Case 60455
                sNorwegian = "Lagre filter"
                sEnglish = "Save filter"
                sSwedish = "Spara filter"
                sDanish = "Gemme filter"
            Case 60456
                sNorwegian = "Tekst for brukerefinert menyvalg 4"
                sEnglish = "Text for userdefined menuline 4"
                sSwedish = "Text för användardefinierat menyalternativ 4"
                sDanish = "Tekst for brugerdefineret menuvalg 4"
            Case 60457
                sNorwegian = "Søketekst"
                sEnglish = "Search for"
                sSwedish = "Söktext"
                sDanish = "Søgetekst"
            Case 60458
                sNorwegian = "Antall pos. fra søketekst"
                sEnglish = "No of pos from searchtext"
                sSwedish = "Antal pos. från söktext"
                sDanish = "Antal tegn fra søgetekst"
            Case 60459
                sNorwegian = "Størrelsen skal angis i Mb og som et heltall."
                sEnglish = "The size must be stated as an integer."
                sSwedish = "Storleken måste anges i MB och som ett heltal."
                sDanish = "Størrelsen skal angives i Mb og som et heltal."
            Case 60460
                sNorwegian = "Feil verdi angitt."
                sEnglish = "Illegal value stated"
                sSwedish = "Fel värde angavs."
                sDanish = "Forkert værdi angivet."
            Case 60461
                sNorwegian = "Kombinasjonen av å sette '%4' til aktiv under" & vbLf & "Profiloppsett - %1 - %2," & vbLf & "samt å sette 'sekvensnummertype' til '%3'" & vbLf & "under Oppsett - Sekvensnumre er ulogisk." & vbLf & "" & vbLf & "Hvis du er i tvil, kontakt din forhandler."
                sEnglish = "The combination to set '%4' to true under" & vbLf & "Profilesetup - %1 - %2," & vbLf & "and to set the 'sequencenumber type' to '%3'" & vbLf & "under Setup - Sequencenumbers is unlogical." & vbLf & "" & vbLf & "If You are not sure, please contact Your dealer."
                sSwedish = "Kombinationen av att ställa in '%4' som aktiv under" & vbLf & "Profilinställning - %1 - %2," & vbLf & "och att sätta 'sekvensnummertyp' som '%3'" & vbLf & "under Inställning - Sekvensnummer är ologiskt." & vbLf & "" & vbLf & "Vid tvekan, kontakta din återförsäljare.	"
                sDanish = "Kombinationen af at sætte '%4' på aktiv under" & vbLf & "Profilopsætning - %1 - %2," & vbLf & "samt at sætte 'sekvensnummertype' på '%3'" & vbLf & "under Opsætning - Sekvensnumre er ulogisk." & vbLf & "" & vbLf & "Hvis du er i tvivl, kontakt din forhandler.	"
            Case 60462
                sNorwegian = "ULOGISK KOMBINASJON"
                sEnglish = "UNLOGICAL COMBINATION"
                sSwedish = "OLOGISK KOMBINATION"
                sDanish = "ULOGISK KOMBINATION"
            Case 60463
                sNorwegian = "Størrelsen må være på minimum 3 Mb."
                sEnglish = "The size must minimum be 3 Mb."
                sSwedish = "Storleken måste vara minst 3 MB."
                sDanish = "Størrelsen skal være på minimum 3 Mb."
            Case 60464
                sNorwegian = "Validering"
                sEnglish = "Validation"
                sSwedish = "Validering"
                sDanish = "Validering"
            Case 60465
                sNorwegian = "Felt:"
                sEnglish = "Field:"
                sSwedish = "Fält:"
                sDanish = "Felt:"
            Case 60466
                sNorwegian = "Når du setter opp en SQL for å validere feltverdier, må du koble SQL-en til feltet som skal valideres." & vbLf & "" & vbLf & "Feltene som kan valideres samsvarer med de som kan editeres." & vbLf & "Dette settes opp under menyvalget 'Avstemming' - 'Oppsett avstemming'."
                sEnglish = "When You add a SQL to validate fields, you must choose which field to validate." & vbLf & "" & vbLf & "The fields that may be validated corresponds with fields which are editable." & vbLf & "You may change this in the menu 'Matching' - 'Setup manual matching'."
                sSwedish = "När du skapar en SQL för att validera fältvärden måste SQL:n kopplas till fältet som ska valideras." & vbLf & "" & vbLf & "Fälten som kan valideras motsvarar de som kan redigeras." & vbLf & "Detta anges under menyalternativet 'Avstämning' - 'Inställning för avstämning'.	"
                sDanish = "Når du indstiller en SQL til at validere feltværdier, skal du koble SQL'en til det felt, der skal valideres." & vbLf & "" & vbLf & "Felterne, der kan valideres passer til dem, der kan editeres." & vbLf & "Dette indstilles under menuvalget 'Afstemning' - 'Opsætning af afstemning'.	"
            Case 60467
                sNorwegian = "Ikke gyldig"
                sEnglish = "Not valid"
                sSwedish = "Ogiltigt"
                sDanish = "Ikke gyldig"
            Case 60468
                sNorwegian = "Splitt ut avtalegiroavtaler i egen fil. Filen prefikses med FBO_."
                sEnglish = "Create seperate file for avtalegiroagreements. The file will be prefixed with FBO_."
                sSwedish = "Dela av avtalsgiroavtal till egen fil. Filen får prefix FBO_."
                sDanish = "Split aftalegiroaftaler ud i egen fil. Filen får præfikset FBO_."
            Case 60469
                sNorwegian = "Landkode mottakerkonto"
                sEnglish = "Countrycode payeeacc."
                sSwedish = "Landskod mottagarkonto"
                sDanish = "Landekode modtagerkonto"
            Case 60470
                sNorwegian = "Lagre autobooking"
                sEnglish = "Save autobooking"
                sSwedish = "Spara autoreservation"
                sDanish = "Gem autobooking"
            Case 60471
                sNorwegian = "Lagre KID-oppsett"
                sEnglish = "Save KID-settings"
                sSwedish = "Spara OCR-inställnings"
                sDanish = "Gem KID-opsætning"
            Case 60472
                sNorwegian = "KID oppsett"
                sEnglish = "KID settings"
                sSwedish = "OCR-inställnings"
                sDanish = "KID-opsætning"
            Case 60473
                sNorwegian = "Startpos. klientnummer"
                sEnglish = "Startpos. clientnumber"
                sSwedish = "Startpos. klientnummer"
                sDanish = "Startpos. klientnummer"
            Case 60474
                sNorwegian = "Lengde klientnummer"
                sEnglish = "Length clientnumber"
                sSwedish = "Längd klientnummer"
                sDanish = "Længde klientnummer"
            Case 60475
                sNorwegian = "Startpos. kundenummer"
                sEnglish = "Startpos. customernumber"
                sSwedish = "Startpos. kundnummer"
                sDanish = "Startpos. kundenummer"
            Case 60476
                sNorwegian = "Lengde kundenummer"
                sEnglish = "Length customernumber"
                sSwedish = "Längd kundnummer"
                sDanish = "Længde kundenummer"
            Case 60477
                sNorwegian = "Startpos. fakturanr."
                sEnglish = "Startpos. invoicenumber"
                sSwedish = "Startpos. fakturanr."
                sDanish = "Startpos. fakturanr."
            Case 60478
                sNorwegian = "Lengde fakturanr."
                sEnglish = "Length invoicenumber"
                sSwedish = "Längd fakturanr."
                sDanish = "Længde fakturanr."
            Case 60479
                sNorwegian = "Modulus kontroll (10/11/RE)"
                sEnglish = "Modulus control (10/11/RE)"
                sSwedish = "Modul-kontroll (10/11/RE)"
                sDanish = "Modulus kontrol (10/11/RE)"
            Case 60480
                sNorwegian = "Fast mønster"
                sEnglish = "Fixed pattern"
                sSwedish = "Fast mönster"
                sDanish = "Fast mønster"
            Case 60481
                sNorwegian = "KID lengde"
                sEnglish = "KID length"
                sSwedish = "OCR-längd"
                sDanish = "KID længde"
            Case 60482
                sNorwegian = "Positive beløp (fakturabetalinger)"
                sEnglish = "Positive amounts (invoice payments)"
                sSwedish = "Positiva belopp (fakturabetalningar)"
                sDanish = "Positive beløb (fakturabetalinger)"
            Case 60483
                sNorwegian = "Negative beløp (kreditnotabetalinger)"
                sEnglish = "Negative amounts (creditnote payments)"
                sSwedish = "Negativa belopp (kreditnotebetalningar)"
                sDanish = "Negative beløb (kreditnotabetalinger)"
            Case 60484
                sNorwegian = "Mønsteret er KID"
                sEnglish = "This pattern is KID"
                sSwedish = "Mönstret är OCR"
                sDanish = "Mønsteret er KID"
            Case 60485
                sNorwegian = "Bankref. (Kontoutdrag)"
                sEnglish = "Bankreference (Statement)"
                sSwedish = "Bankref. (kontoutdrag)"
                sDanish = "Bankref. (Kontoudskrift)"
            Case 60486
                sNorwegian = "Fra konto"
                sEnglish = "From account"
                sSwedish = "Från konto"
                sDanish = "Fra konto"
            Case 60487
                sNorwegian = "Til konto"
                sEnglish = "To account"
                sSwedish = "Till konto"
                sDanish = "Til konto"
            Case 60488
                sNorwegian = "Eget navn"
                sEnglish = "Own name"
                sSwedish = "Eget Namn"
                sDanish = "Eget navn"
            Case 60489
                sNorwegian = "Transaksjonstype"
                sEnglish = "Transactiontype"
                sSwedish = "Transaktionstyp"
                sDanish = "Transaktionstype"
            Case 60490
                sNorwegian = "Bankref. 1 (Betaling)"
                sEnglish = "Bankreference1 (Payment)"
                sSwedish = "Bankref. 1 (betalning)"
                sDanish = "Bankref. 1 (Betaling)"
            Case 60491
                sNorwegian = "Bankref. 2 (Betaling)"
                sEnglish = "Bankreference2 (Payment)"
                sSwedish = "Bankref. 2 (betalning)"
                sDanish = "Bankref. 2 (Betaling)"
            Case 60492
                sNorwegian = "Fritekst"
                sEnglish = "Freetext"
                sSwedish = "Fritext"
                sDanish = "Fritekst"
            Case 60493
                sNorwegian = "Eksportert"
                sEnglish = "Exported"
                sSwedish = "Exporterat"
                sDanish = "Eksporteret"
            Case 60494
                sNorwegian = "Måned i valutadato"
                sEnglish = "Month in valuedate"
                sSwedish = "Månad i valutadatum"
                sDanish = "Måned i valutadato"
            Case 60495
                sNorwegian = "Max. %1 betalinger pr fil"
                sEnglish = "Max. %1 payments pr file"
                sSwedish = "Max. %1 betalningar per fil"
                sDanish = "Maks. %1 betalinger pr. fil"
            Case 60496
                sNorwegian = "Velg først en profil for denne klienten"
                sEnglish = "Please select a profile for this client"
                sSwedish = "Välj först en profil för klienten"
                sDanish = "Vælg først en profil for denne klient"
            Case 60497
                sNorwegian = "KID navn"
                sEnglish = "KID name"
                sSwedish = "OCR-namn"
                sDanish = "KID navn"
            Case 60498
                sNorwegian = "KIDbeskrivelse: #=Numerisk tegn  Andre tegn er 'ekte'"
                sEnglish = "KIDdescription: #=Numeric sign  Other signs are  'real'"
                sSwedish = "OCR-beskrivning: # = siffra Andra tecken är ''äkta''"
                sDanish = "KIDbeskrivelse: #=Numerisk tegn  Andre tegn er 'ægte'"
            Case 60499
                sNorwegian = "Eksporter poster med negative beløp"
                sEnglish = "Export records with negative amounts"
                sSwedish = "Exportera poster med negativa belopp"
                sDanish = "Eksporter poster med negative beløb"
            Case 60500
                sNorwegian = "Gjør om feil oppdrag hvis mulig"
                sEnglish = "Change wrong-formatted payments (if possible)"
                sSwedish = "Gör om felaktiga uppdrag om möjligt"
                sDanish = "Lav forkert opgave om, hvis muligt"
            Case 60501
                sNorwegian = "Innkludér OCR-poster"
                sEnglish = "Include OCR-payments"
                sSwedish = "Inkludera OCR-poster"
                sDanish = "Inkludér OCR-poster"
            Case 60502
                sNorwegian = "Legg bilagsnr også på OCR-poster"
                sEnglish = "Set voucherno also on OCR-payments"
                sSwedish = "Lägg till bilaganr även på OCR-poster"
                sDanish = "Tilføj også bilagsnr. på OCR-poster"
            Case 60503
                sNorwegian = "Splitt opp KID i enkeltelementer"
                sEnglish = "Split KID into seperate parts"
                sSwedish = "Dela upp OCR i enskilda element"
                sDanish = "Opdel KID i enkeltelementer"
            Case 60504
                sNorwegian = "Ingen sidebrudd"
                sEnglish = "No pagebreak"
                sSwedish = "Ingen sidbrytning"
                sDanish = "Ingen sidebrud"
            Case 60505
                sNorwegian = "Legg til en teller på bilagsnummeret per innbetaling"
                sEnglish = "Add a counter to the vouchernumber for each payment"
                sSwedish = "Lägg till en bilaganummerräknare per betalning"
                sDanish = "Tilføj en tæller til bilagsnummeret pr. indbetaling"
            Case 60506
                sNorwegian = "Per faktura"
                sEnglish = "Per invoice"
                sSwedish = "Per faktura"
                sDanish = "Pr. faktura"
            Case 60507
                sNorwegian = "Legg inn informasjon om hvilke beløpsavvik (per valuta) som tillates. Avviksbeløpet vil posteres i henhold til oppsettet på avvikskontoen." & vbLf & "Avviket vil posteres automatisk, hvis en regel for automatisk avstemming er merket med Bruk avvik. Imidlertid vil betalingen kun bli forslagsavstemt, og må bli godkjent av en bruker før den posteres." & vbLf & "" & vbLf & "Hvis man ønsker å tillate dette avviket per faktura, så markeres dette ved å sette et merke i kolonnen 'Per faktura'. Det kan være nyttig ved øreavrunding og purregebyr."
                sEnglish = "Enter information about which deviations in amount (per currency) that should be allowed. The difference will be posted according to the setup of this deviationaccount." & vbLf & "The deviation will automatically be posted, if the rule under automatic matching is marked as 'Use adjustments'. However the payment will only be proposed (not fully) matched, and must be approved by a user in manual matching." & vbLf & "" & vbLf & "If You want to allow this deviation per invoice, mark this in the column named 'Per invoice'. This is useful for roundings and reminders."
                sSwedish = "Ange information om hur mycket avvikelse (per valuta) som är tillåten. Avvikelsebeloppet bokförs enligt inställningarna på avvikelsekontot." & vbLf & "" & vbLf & "Avvikelsen kommer att bokföras automatiskt om en en regel för automatisk avstängning är märkt med 'Använd avvikelse'. Betalningen blir dock ett avstämningsförslag och måste godkännas av en användare innan den bokförs." & vbLf & "" & vbLf & "" & vbLf & "" & vbLf & "Om man vill tillåta denna avvikelse per faktura så markerar man i kolumnen 'Per faktura'. Det kan vara användbart vid öresavrundning och förseningsavgifter.	"
                sDanish = "Angiv information om, hvilke beløbsafvigelser (pr. valuta) der tillades. Afvigelsesbeløbet posteres iht. opsætningen på afvigelseskontoen." & vbLf & "" & vbLf & "Afvigelsen posteres automatisk, hvis en regel for automatisk afstemning er mærket med Brug afvigelse'. Imidlertid vil betalingen kun blive foreslået afstemt, og skal godkendes af en bruger før den posteres." & vbLf & "" & vbLf & "" & vbLf & "" & vbLf & "Hvis man ønsker at tillade denne afvigelse pr. faktura, så markeres dette ved at sætte et mærke i kolonnen 'Pr. faktura'. Det kan være nyttigt ved øreafrunding og rykkergebyr.	"
            Case 60508
                sNorwegian = "Sett ACH-dato"
                sEnglish = "Make the ACH effective day"
                sSwedish = "Ange ACH-datum"
                sDanish = "Indstil ACH-dato"
            Case 60509
                sNorwegian = "dager fram fra dagens dato"
                sEnglish = "days forward from today's date"
                sSwedish = "dagar framåt från dagens datum"
                sDanish = "dage frem fra dags dato"
            Case 60510
                sNorwegian = "Slå av 'admin' menyvalg"
                sEnglish = "Disable 'admin' menuchoices"
                sSwedish = "Inaktivera menyalternativet ''admin’’"
                sDanish = "Slå menuvalget 'admin' fra"
            Case 60511
                sNorwegian = "Vil du slette KID-oppsett"
                sEnglish = "Delete KID-settings"
                sSwedish = "Vill du ta bort OCR-inställningen"
                sDanish = "Vil du slette KID-opsætningen"
            Case 60512
                sNorwegian = "Basisvaluta"
                sEnglish = "Base Currency"
                sSwedish = "Basvaluta"
                sDanish = "Basisvaluta"
            Case 60513
                sNorwegian = "Betalinger med 0-beløp"
                sEnglish = "Payments with 0-amount"
                sSwedish = "Betalningar med 0-belopp"
                sDanish = "Betalinger med 0-beløb"
            Case 60514
                sNorwegian = "Etter at du har lagt til et mønster kan du markere det som en KID, hvis du ønsker å benytte fakturadelen ved avstemming samt at det skal valideres mot beskrivelsen av KID." & vbLf & "Beskrivelsen av KID finner du under oppsettet av bankkonti."
                sEnglish = "After You have added the pattern, mark it as a KID if You want to use the invociepart of the KID when matching, and validate it against the description of the KIDs." & vbLf & "The description of the KID is found under the Account-setup."
                sSwedish = "När du har lagt till ett mönster kan du markera det som OCR om du vill använda fakturadelen vid avstämning och att den bör valideras mot beskrivningen av OCR." & vbLf & "Beskrivningen av OCR finns under inställningarna för bankkonton.	"
                sDanish = "Efter du har tilføjet et mønster kan du markere det som en KID, hvis du ønsker at benytte fakturadelen ved afstemning samt at det skal valideres med beskrivelsen af KID." & vbLf & "Beskrivelsen af KID finder du under opsætning af bankkonti.	"
            Case 60515
                sNorwegian = "Bruk langt datoformat"
                sEnglish = "Use longdate"
                sSwedish = "Använd långt datumformat"
                sDanish = "Brug langt datoformat"
            Case 60516
                sNorwegian = "Jeg betaler omkostninger i hjemland"
                sEnglish = "Charge me domestic"
                sSwedish = "Jag betalar kostnader i hemlandet"
                sDanish = "Jeg betaler omkostninger i hjemlandet"
            Case 60517
                sNorwegian = "Jeg betaler omkostninger i utlandet"
                sEnglish = "Charge me abroad"
                sSwedish = "Jag betalar kostnader i utlandet"
                sDanish = "Jeg betaler omkostninger i udlandet"
            Case 60518
                sNorwegian = "Filnavn for mappingfil"
                sEnglish = "Filename for mappingfile"
                sSwedish = "Filnamn på mappningsfil"
                sDanish = "Filnavn for mappingfil"
            Case 60519
                sNorwegian = "Mappingfil"
                sEnglish = "Mappingfile"
                sSwedish = "Mappningsfil"
                sDanish = "Mappingfil"
            Case 60520
                sNorwegian = "Filtype"
                sEnglish = "Filetype"
                sSwedish = "Filtyp"
                sDanish = "Filtype"
            Case 60521
                sNorwegian = "Hent fra mappingfil"
                sEnglish = "Load from mappingfile"
                sSwedish = "Hämta från mappningsfil"
                sDanish = "Hent fra mappingfil"
            Case 60522
                sNorwegian = "Skilletegn"
                sEnglish = "Delimiter"
                sSwedish = "Avgränsare"
                sDanish = "Skilletegn"
            Case 60523
                sNorwegian = "Omslutningstegn"
                sEnglish = "Surroundchar."
                sSwedish = "Omslutningstecken"
                sDanish = "Omslutningstegn"
            Case 60524
                sNorwegian = "Ant. overskriftslinjer"
                sEnglish = "No.of headerlines"
                sSwedish = "Antal rubrikrader"
                sDanish = "Ant. overskriftslinjer"
            Case 60525
                sNorwegian = "Feltnavn"
                sEnglish = "Fieldname"
                sSwedish = "Fältnamn"
                sDanish = "Feltnavn"
            Case 60526
                sNorwegian = "Format/Fasttekst"
                sEnglish = "Format/Fixed text"
                sSwedish = "Format/Fast text"
                sDanish = "Format/Fasttekst"
            Case 60527
                sNorwegian = "Adresse1"
                sEnglish = "Adress1"
                sSwedish = "Adress1"
                sDanish = "Adresse1"
            Case 60528
                sNorwegian = "Adresse2"
                sEnglish = "Adress2"
                sSwedish = "Adress2"
                sDanish = "Adresse2"
            Case 60529
                sNorwegian = "Postnummer"
                sEnglish = "Zipcode"
                sSwedish = "Postnummer"
                sDanish = "Postnummer"
            Case 60530
                sNorwegian = "By/sted"
                sEnglish = "City"
                sSwedish = "Stad/Ort"
                sDanish = "By/sted"
            Case 60531
                sNorwegian = "Betalingsdato"
                sEnglish = "Paymentdate"
                sSwedish = "Betalningsdatum"
                sDanish = "Betalingsdato"
            Case 60532
                sNorwegian = "Beløp"
                sEnglish = "Amount"
                sSwedish = "Belopp"
                sDanish = "Beløb"
            Case 60533
                sNorwegian = "Tusenskille"
                sEnglish = "Thousandsep."
                sSwedish = "Tusentalsavgränsare"
                sDanish = "Tusindtalsseparator"
            Case 60534
                sNorwegian = "Desimalskille"
                sEnglish = "Decimalsep."
                sSwedish = "Decimaltecken"
                sDanish = "Decimalseparator"
            Case 60535
                sNorwegian = "Bankkode"
                sEnglish = "Bankcode"
                sSwedish = "Bankkod"
                sDanish = "Bankkode"
            Case 60536
                sNorwegian = "Egenreferanse (betalers ref.)"
                sEnglish = "Ref. own (payers ref)"
                sSwedish = "Egenreferens (betalarens ref.)"
                sDanish = "Egenreference (betalers ref.)"
            Case 60537
                sNorwegian = "Melding til mottaker"
                sEnglish = "Notification of receiver"
                sSwedish = "Meddelande till mottagare"
                sDanish = "Meddelelse til modtager"
            Case 60538
                sNorwegian = "Mapping av betalingsfil"
                sEnglish = "Mapping of paymentfile"
                sSwedish = "Mappnings av betalningsfil"
                sDanish = "Mapping af betalingsfil"
            Case 60539
                sNorwegian = "Vil du lagre endring til mappingfil"
                sEnglish = "Save changes to the mappingfile"
                sSwedish = "Vill du spara ändringar i mappningsfil"
                sDanish = "Vil du gemme ændring til mappingfil"
            Case 60540
                sNorwegian = "Finner ikke mappingfilen"
                sEnglish = "Can not find the mappingfile"
                sSwedish = "Hittar inte mappningsfil"
                sDanish = "Kan ikke finde mappingfilen"
            Case 60541
                sNorwegian = "Oppsett signalfil"
                sEnglish = "Setup for siganlfile"
                sSwedish = "Ställ in signalfil"
                sDanish = "Opsætning af signalfil"
            Case 60542
                sNorwegian = "Du kan benytte dette valget hvis du ønsker at BabelBank skal lage en fil ved vellykket kjøring. Hvis BabelBank ikke avsluttes vellykket lages ingen fil." & vbLf & "Dette kan være nyttig hvis du ønsker å starte eller fortsette en prosess, som er startet utenfor BabelBank."
                sEnglish = "You may use this setup if You want to create a file if BabelBank end successfully. If BabelBank doesn't end successfully no file will be created." & vbLf & "This may be useful if You want to start or continue a process started outside BabelBank."
                sSwedish = "Du kan använda detta alternativ om du vill att BabelBank ska skapa en fil vid felfri körning. Om BabelBank inte avslutas felfritt skapas ingen fil." & vbLf & "Detta kan vara användbart om du vill starta eller fortsätta en process som har påbörjats utanför BabelBank.	"
                sDanish = "Du kan benytte dette valg, hvis du ønsker at BabelBank skal oprette en fil ved vellykket kørsel. Hvis BabelBank ikke afsluttes vellykket oprettes ingen fil." & vbLf & "Dette kan være nyttigt, hvis du ønsker at starte eller fortsætte en proces, som er startet uden for BabelBank.	"
            Case 60543
                sNorwegian = "Aktivér signalfil"
                sEnglish = "Activate a signalfile"
                sSwedish = "Aktivera signalfil"
                sDanish = "Aktivér signalfil"
            Case 60544
                sNorwegian = "&Signalfil"
                sEnglish = "&Signalfile"
                sSwedish = "&Signalfil"
                sDanish = "&Signalfil"
            Case 60545
                sNorwegian = "Validér filformat før import"
                sEnglish = "Validate fileformat before import"
                sSwedish = "Validera format före import"
                sDanish = "Validér filformat før import"
            Case 60546
                sNorwegian = "Sorter før sammenslåing"
                sEnglish = "Sort before merge"
                sSwedish = "Sortera före sammanslagning"
                sDanish = "Sorter før sammenlægning"
            Case 60547
                sNorwegian = "Lås BabelBank for bruk"
                sEnglish = "Lock BabelBank"
                sSwedish = "Lås BabelBank för användning"
                sDanish = "Lås BabelBank for brug"
            Case 60548
                sNorwegian = "Andre"
                sEnglish = "Other"
                sSwedish = "Annat"
                sDanish = "Andre"
            Case 60549
                sNorwegian = "Dette gebyroppsett skal overstyre eventuell gebyrsetting fra importert fil"
                sEnglish = "This setup for charges will override possible chargesinfo found in importfile"
                sSwedish = "Denna avgiftsinställning åsidosätter andra avgiftsinställningar från importerad fil"
                sDanish = "Denne gebyropsætning skal tilsiddesætte en eventuel gebyrindstilling fra importeret fil"
            Case 60550
                sNorwegian = "Filtrerte, slettede, betalinger"
                sEnglish = "Filtered, removed, payments"
                sSwedish = "Filtrerade, borttagna, betalningar"
                sDanish = "Filtrerede, slettede, betalinger"
            Case 60551
                sNorwegian = "Slå sammen betalinger, INGEN informasjon til mottaker"
                sEnglish = "Merge payments, leave NO information"
                sSwedish = "Slå ihop betalningar, ingen information till mottagaren"
                sDanish = "Slå betalinger sammen, INGEN information til modtager"
            Case 60552
                sNorwegian = "Regnskapsår (BB_Year)"
                sEnglish = "Accountingyear (BB_Year)"
                sSwedish = "Räkenskapsår (BB_Year)"
                sDanish = "Regnskabsår (BB_Year)"
            Case 60553
                sNorwegian = "Periode (BB_Period)"
                sEnglish = "Accountingperiod (BB_Period)"
                sSwedish = "Period (BB_Period)"
                sDanish = "Periode (BB_Period)"
            Case 60554
                sNorwegian = "BLZ-kode bank"
                sEnglish = "BLZ-code - bank"
                sSwedish = "BLZ-kod bank"
                sDanish = "BLZ-kode bank"
            Case 60555
                sNorwegian = "Kundenummer"
                sEnglish = "Customer number"
                sSwedish = "Kundnummer"
                sDanish = "Kundenummer"
            Case 60556
                sNorwegian = "Rapporttype"
                sEnglish = "Reporttype"
                sSwedish = "Rapporttyp"
                sDanish = "Rapporttype"
            Case 60557
                sNorwegian = "Rapportnavn"
                sEnglish = "Reportname"
                sSwedish = "Rapportnamn"
                sDanish = "Rapportnavn"
            Case 60558
                sNorwegian = "Sortering/brudd"
                sEnglish = "Sorting/break"
                sSwedish = "Sortering"
                sDanish = "Sortering"
            Case 60559
                sNorwegian = "Når skal rapporten kjøres"
                sEnglish = "When to run report"
                sSwedish = "När ska rapporten köras"
                sDanish = "Hvornår skal rapporten køres"
            Case 60560
                sNorwegian = "Inkluder KID (på innbetalinger)"
                sEnglish = "Include KID (on incoming payments)"
                sSwedish = "Inkludera OCR"
                sDanish = "Inkluder KID"
            Case 60561
                sNorwegian = "Rapporter på lagrede data"
                sEnglish = "Report on saved data"
                sSwedish = "Rapport om lagrade data"
                sDanish = "Rapporter på gemte data"
            Case 60562
                sNorwegian = "Lagre data før rapporten kjøres"
                sEnglish = "Save data before reportrun"
                sSwedish = "Spara data innan rapporten körs"
                sDanish = "Gem data før rapporten køres"
            Case 60563
                sNorwegian = "Ingen sortering/Ingen brudd"
                sEnglish = "No sorting/No breaks"
                sSwedish = "Ingen sortering/Ingen delning"
                sDanish = "Ingen sortering/Ingen brud"
            Case 60564
                sNorwegian = "Kontonummer, dato"
                sEnglish = "Accountnumber, date"
                sSwedish = "Kontonummer, datum"
                sDanish = "Kontonummer, dato"
            Case 60565
                sNorwegian = "Kontosummering"
                sEnglish = "Accounttotals"
                sSwedish = "Kontosammanfattning"
                sDanish = "Kontosummering"
            Case 60566
                sNorwegian = "Pr. betaling"
                sEnglish = "Pr. payment"
                sSwedish = "Per betalning"
                sDanish = "Pr. betaling"
            Case 60567
                sNorwegian = "Kontonummer"
                sEnglish = "Accountnumber"
                sSwedish = "Kontonummer"
                sDanish = "Kontonummer"
            Case 60568
                sNorwegian = "Klientnummer"
                sEnglish = "Clientnumber"
                sSwedish = "Klientnummer"
                sDanish = "Klientnummer"
            Case 60569
                sNorwegian = "Etter import"
                sEnglish = "After import"
                sSwedish = "Efter import"
                sDanish = "Efter import"
            Case 60570
                sNorwegian = "I spesialrutiner"
                sEnglish = "Inside specialroutines"
                sSwedish = "I specialrutiner"
                sDanish = "I specialrutiner"
            Case 60571
                sNorwegian = "Etter spesialrutiner"
                sEnglish = "After specialroutines"
                sSwedish = "Efter specialrutiner"
                sDanish = "Efter specialrutiner"
            Case 60572
                sNorwegian = "Etter OCR-eksport"
                sEnglish = "After OCR-export"
                sSwedish = "Efter OCR-export"
                sDanish = "Efter OCR-eksport"
            Case 60573
                sNorwegian = "Etter Autogiroeksport"
                sEnglish = "After Autogiroexport"
                sSwedish = "Efter autogiroexport"
                sDanish = "Efter Autogiroeksport"
            Case 60574
                sNorwegian = "Før eksport"
                sEnglish = "Before export"
                sSwedish = "Före export"
                sDanish = "Før eksport"
            Case 60575
                sNorwegian = "Etter eksport"
                sEnglish = "After export"
                sSwedish = "Efter export"
                sDanish = "Efter eksport"
            Case 60576
                sNorwegian = "Har du klientrapporter, så kan e-post adresser angis i klientoppsettet. Skal alle rapporter sendes til samme e-post adresse(r) så sett opp disse over, adskilt med ;"
                sEnglish = "For clientreports e-mail addresses can be set in the Client setupform. If the reports will be sent to the same receiver(s), set the e-mailaddress(es) here, separated by ;"
                sSwedish = "Har du klientrapporter så kan e-postadresser anges i klientkonfigurationen. Om alla rapporter skickas till samma e-postadress(er) ska dessa anges avgränsade med ;"
                sDanish = "Har du klientrapporter, så kan e-mailadresser angives i klientopsætningen. Skal alle rapporter sendes til samme e-mailadresse(r) så opsæt disse adskilt med ;"
            Case 60577
                sNorwegian = "Filnavn for klientrapporter angis med en eller flere $ (klientnr), eller £ (klientnavn)."
                sEnglish = "Filename for clientreports uses on ore more $ (clientno), or  £ (clientname)."
                sSwedish = "Filnamn för klientrapporter anges med ett eller flera § (klientnr) eller £ (klientnamn)."
                sDanish = "Filnavn for klientrapporter angives med en eller flere § (klientnr), eller £ (klientnavn)."
            Case 60578
                sNorwegian = "Det må være registrert en klient før du kan legge til konti. Det gjøres fra knappen ''Klienter'' i hovedbildet.'"
                sEnglish = "You must have at least one client before adding accounts. This can be done from the commandbutton ''Clients'' in the main form."
                sSwedish = "En klient måste registreras innan du kan lägga till konton. Detta görs med knappen ''Klienter'' i huvudvyn.'"
                sDanish = "Der skal være registreret en klient før du kan tilføje konti. Det gøres fra knappen ''Klienter'' i hovedbilledet.'"
            Case 60579
                sNorwegian = "Lagre denne profilen før du legger inn konti (Med OK, OK,OK)"
                sEnglish = "Please save this profile (with OK, OK, OK) before you add accounts !"
                sSwedish = "Spara den här profilen innan du anger konton (Med OK, OK, OK)"
                sDanish = "Gem denne profil før du tilføjer konti (Med OK, OK,OK)"
            Case 60580
                sNorwegian = "Slå sammen betalinger, KUN hvis kreditnotaer"
                sEnglish = "Merge payments, ONLY when credits are involved"
                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 60600

                sSwedish = "Slå ihop betalningar, ENDAST om kreditnotor"
                sDanish = "Slå betalinger sammen, KUN hvis kreditnotaer"
            Case 60581
                sNorwegian = "Divisjon"
                sEnglish = "Division"
                sSwedish = "Divisjon"
                sDanish = "Divisjon"
            Case 60582
                sNorwegian = "Avrunding (forslag)"
                sEnglish = "Adjustments (propose)"
                sSwedish = "Avrundning (förslag)"
                sDanish = "Afrunding (foreslået)"
            Case 60583
                sNorwegian = "Leverandørreskontro"
                sEnglish = "Account Payable"
                sSwedish = "Levearantörsreskontra"
                sDanish = "Leverandørreskontro"

            Case 60584
                sNorwegian = "Splitt OCR (uten kreditnota)"
                sEnglish = "Split OCR (no credits)"
                sSwedish = "Split OCR (utan kreditnota)"
                sDanish = "Split OCR (uden kreditnota)"
            Case 60585
                sNorwegian = "Splitt OCR (med kreditnota)"
                sEnglish = "Split OCR (with credits)"
                sSwedish = "Split OCR (med kreditnota)"
                sDanish = "Split OCR (med kreditnota)"
            Case 60586
                sNorwegian = "Ved blanding, lag struktruert"
                sEnglish = "When mix, redo to structured"
                sSwedish = "Ved blanding, lag strukturert"
                sDanish = "Ved blanding, lag strukturert"
            Case 60587
                sNorwegian = "IKKE gruppér ved eksport"
                sEnglish = "Do NOT group in exportfile"
                sSwedish = "INTE gruppér ved export"
                sDanish = "IKKE gruppér ved eksport"
            Case 60588
                sNorwegian = "Fjern BIC for SEPA betalinger"
                sEnglish = "Remove BIC for SEPA payments"
                sSwedish = "Ta bort BIC for SEPA betalningar"
                sDanish = "Fjern BIC for SEPA betalinger"

            Case 60589
                sNorwegian = "Ta kopi av database"
                sEnglish = "Make a copy of the database"
                sSwedish = "Ta kopia av databasen"
                sDanish = "Tag kopi af database"
            Case 60590
                sNorwegian = "Område for backup"
                sEnglish = "Path for backup"
                sSwedish = "Område för säkerhetskopiering"
                sDanish = "Område for backup"
            Case 60591
                sNorwegian = "Antall generasjoner av backup"
                sEnglish = "Number of backup generations"
                sSwedish = "Antall generationer av säkerhetskopian"
                sDanish = "Antal generationer af backup"
            Case 60592
                sNorwegian = "Før import"
                sEnglish = "Before import"
                sSwedish = "Före import"
                sDanish = "Før import"
            Case 60593
                sNorwegian = "Før eksport"
                sEnglish = "Before export"
                sSwedish = "Före export"
                sDanish = "Før eksport"
            Case 60594
                sNorwegian = "Etter eksport"
                sEnglish = "After export"
                sSwedish = "Efter export"
                sDanish = "Efter eksport"
            Case 60595
                sNorwegian = "AML Rapport"
                sEnglish = "AML Report"
                sSwedish = "AML Rapport"
                sDanish = "AML Rapport"
            Case 60596
                sNorwegian = "Tillat overføring i annen valuta enn fakturavaluta." & vbCrLf & "Test av dette er GODKJENT AV KUNDE."
                sEnglish = "Allow transfer in other currency than the invoicecurrency." & vbNewLine & "Tests have been APPROVED BY THE CUSTOMER."
                sSwedish = "Tillåt överföring i annan valuta än fakturavaluta." & vbNewLine & "Test av detta är GODKÄNT AV KUNDEN."
                sDanish = "Tillad overførsel til anden valuta end faktura valuta." & vbNewLine & "Test af dette er GODKENT AF KUNDEN."
            Case 60597
                sNorwegian = "Kundenummersøk"
                sEnglish = "Customer number search"
                sSwedish = "Kundnummersök"
                sDanish = "Kundenummersøk"
            Case 60598
                sNorwegian = "Klientgruppe"
                sEnglish = "ClientGroup"
                sSwedish = "Klientgrupp"
                sDanish = "Klientgruppe"



                '-------------------------------
            Case 60600
                sNorwegian = "Du må melde inn en konto for denne klienten"
                sEnglish = "Please set an account for this client"
                sSwedish = "Du måste registrera ett konto för den aktuella klient"
                sDanish = "Du skal tilmelde en konto for denne klienten"
            Case 60601
                sNorwegian = "Signalfil"
                sEnglish = "Signalfile"
                sSwedish = "Signalfil"
                sDanish = "Signalfil"
            Case 60602
                sNorwegian = "Du må velge en DB-Profile for denne klienten"
                sEnglish = "Please select a DB-Profile for this client."
                sSwedish = "Du måste välja en DB-Profile för den aktuella klient"
                sDanish = "Du skal vælge en DB-Profile for denne klienten"
            Case 60603
                sNorwegian = "Kan ikke finne den valgte profilen: %1"
                sEnglish = "Can't find the correct profile: %1"
                sSwedish = "Det går inte att hitta den valda profilen: %1"
                sDanish = "Kan ikke finde den valgte profil: %1"
            Case 60604
                sNorwegian = "SELECT del"
                sEnglish = "SELECT part"
                sSwedish = "SELECT del"
                sDanish = "SELECT del"
            Case 60605
                sNorwegian = "WHERE del"
                sEnglish = "WHERE part"
                sSwedish = "WHERE del"
                sDanish = "WHERE del"
            Case 60606
                sNorwegian = "ORDER del"
                sEnglish = "ORDER part"
                sSwedish = "ORDER del"
                sDanish = "ORDER del"
            Case 60607
                sNorwegian = "Tilbakestill til standard"
                sEnglish = "Reset to standard"
                sSwedish = "Återställ till standard"
                sDanish = "Nulstil til standard"
            Case 60608
                sNorwegian = "Alle nullinnbetalinger"
                sEnglish = "All zero payments"
                sSwedish = "Alla nollinbetalningar"
                sDanish = "Alle nulindbetalinger"
            Case 60609
                sNorwegian = "Nullbetalinger, ikke strukturert"
                sEnglish = "Zero payments, not structured"
                sSwedish = "Nollbetalningar - inte strukturerade"
                sDanish = "Nulbetalinger - ikke struktureret"
            Case 60610
                sNorwegian = "Valutafelt som skal brukes i betalingsfiler"
                sEnglish = "Currencyfield to be used in paymentfile"
                sSwedish = "Valutafält som ska användas i betalningsfiler"
                sDanish = "Valutafelt som skal bruges i betalingsfiler"
            Case 60611
                sNorwegian = "Vis alle"
                sEnglish = "Show all"
                sSwedish = "Visa alla"
                sDanish = "Vis alle"
            Case 60612
                sNorwegian = "Telepay debet Norge"
                sEnglish = "Telepay debit Norway"
                sSwedish = "Telepay debet Norge"
                sDanish = "Telepay debet Norge"
            Case 60613
                sNorwegian = "Telepayfil til bank"
                sEnglish = "Telepayfile to bank"
                sSwedish = "Telepayfil till bank"
                sDanish = "Telepayfil til bank"
            Case 60614
                sNorwegian = "Divisjon Telepay Norge"
                sEnglish = "Division Telepay Norway"
                sSwedish = "Division Telepay Norge"
                sDanish = "Division Telepay Norge"
            Case 60615
                sNorwegian = "Foretaksnr."
                sEnglish = "Enterprise no."
                sSwedish = "Företagsnr."
                sDanish = "Virksomhedsnr."
            Case 60616
                sNorwegian = "Bruk kun for valuta annet enn NOK"
                sEnglish = "Use only for currencies other than NOK"
                sSwedish = "Använd endast för annan valuta  än NOK"
                sDanish = "Brug kun til anden valuta end NOK"
            Case 60617
                sNorwegian = "Aktiver spesial-SQL"
                sEnglish = "Activate special-SQL"
                sSwedish = "Aktivera special-SQL"
                sDanish = "Aktiver speciel-SQL"
            Case 60618
                sNorwegian = "BabelBank er ikke tilpasset å teste om denne WCF-servicen kjører."
                sEnglish = "BabelBank is not programmed to chek if this WCF-service is running."
                sSwedish = "BabelBank är inte anpassade för att testa om denna WCF-tjänst körs."
                sDanish = "BabelBank er ikke tilpasset til at teste om denne WCF-service kører."
            Case 60619
                sNorwegian = "WCF-servicen kjører."
                sEnglish = "The WCF-service is running."
                sSwedish = "WCF-tjänsten körs."
                sDanish = "WCF-servicen kører."
            Case 60620
                sNorwegian = "WCF-servicen returnerte følgende feil:" & vbCrLf & "%1" & vbCrLf & vbCrLf & "Sjekk serviceadresse, bruker-ID og passord!"
                sEnglish = "The WCF-service returned the following error:" & vbCrLf & "%1" & vbCrLf & vbCrLf & "Check the serviceaddress, user-ID and password!"
                sSwedish = "WCF-tjänsten returnerte följande fel:" & vbCrLf & "%1" & vbCrLf & vbCrLf & "Check serviceadress, användar-ID och lösenord!"
                sDanish = "WCF-servicen returnerte følgende fejl:" & vbCrLf & "%1" & vbCrLf & vbCrLf & "Check serviceadresse, bruger-ID og password!"
            Case 60621
                sNorwegian = "Når spørringen er av type 2, så må man angi antall fakturaer som skal trekkes fra ved summeringen."
                sEnglish = "When the query is of type 2, you have to enter the number of invoices to deduct from the calculation."
                sSwedish = "När frågan är av typ 2, då måste man ange antalet fakturor som skall dras av från summeringen."
                sDanish = "Når forespørgslen er af type 2, så er du nødt til at angive antallet af fakturaer som skal fratrækkes summeringen."
            Case 60622
                sNorwegian = "Ved avslutning"
                sEnglish = "At the end"
                sSwedish = "Vid slutet"
                sDanish = "Ved afslutning"
            Case 60623
                sNorwegian = "Dagtotal sumrapport"
                sEnglish = "Day totals aggregated"
                sSwedish = "Dagtotal sumrapport"
                sDanish = "Dagtotal sumrapport"
            Case 60624
                sNorwegian = "Negative betalinger"
                sEnglish = "Negative payments"
                sSwedish = "Negativa betalningar"
                sDanish = "Negative betalinger"
            Case 60625
                sNorwegian = "Standard 'Purpose Code'"
                sEnglish = "Standard 'Purpose Code'"
                sSwedish = "Standard 'Purpose Code'"
                sDanish = "Standard 'Purpose Code'"
            Case 60626
                sNorwegian = "Pr. fil"
                sEnglish = "Pr. file"
                sSwedish = "Pr. fil"
                sDanish = "Pr. fil"
            Case 60627
                sNorwegian = "Alle"
                sEnglish = "All"
                sSwedish = "Alla"
                sDanish = "Alle"
            Case 60628
                sNorwegian = "Feltet 'Rekkefølge' er obligatorisk."
                sEnglish = "The field 'Order' is mandatory."
                sSwedish = "Fältet 'Rekkefølge' är obligatorisk."
                sDanish = "Feltet 'Rekkefølge' er obligatorisk."

                ' added 09.07.2015
            Case 60629
                sNorwegian = "Variabel del av MessageId"
                sEnglish = "Variable part of MessageID"
                sSwedish = "Variabel del av MessageId"
                sDanish = "Variabel del af MessageId"
            Case 60630
                sNorwegian = "SEPA Single postering"
                sEnglish = "SEPA Single posting"
                sSwedish = "SEPA Single postering"
                sDanish = "SEPA Single postering"
            Case 60631
                sNorwegian = "Slett i databasen etter nn dager"
                sEnglish = "Delete in database after nn days"
                sSwedish = "Ta bort i databasen efter nn dagar"
                sDanish = "Slet i databasen efter nn dage"
            Case 60632
                sNorwegian = "Mellomlagre betalingsdata"
                sEnglish = "Save paymentdata"
                sSwedish = "Gem betalningsdata "
                sDanish = "Spara betalingsdata"
            Case 60633
                sNorwegian = "Valideringsnivå"
                sEnglish = "Validationlevel"
                sSwedish = "Valideringsnivå"
                sDanish = "Valideringsniveau"
            Case 60634
                sNorwegian = "BabelBank lager unike referanser"
                sEnglish = "BabelBank creates unique references"
                sSwedish = "BabelBank skapar unika referenser"
                sDanish = "BabelBank skaber unikke referencer"
            Case 60635
                sNorwegian = "Ingen validering"
                sEnglish = "No validation"
                sSwedish = "Ingen validering"
                sDanish = "Ingen validering"
            Case 60636
                sNorwegian = "Normal validering"
                sEnglish = "Normal validation"
                sSwedish = "Normal validering"
                sDanish = "Normal validering"
            Case 60637
                sNorwegian = "Streng validering"
                sEnglish = "Tight validation"
                sSwedish = "Strikt validering"
                sDanish = "Streng validering"

            Case 60638
                sNorwegian = "Lagre dimensjonsoppsett"
                sEnglish = "Save dimensionsettings"
                sSwedish = "Gem dimensjonsoppsett"
                sDanish = "Spara dimensjonsoppsett"

                ' HUSK AT VI BRUKER LRS-KODER OGSÅ I VB6 - DET VAR BRUKT DOBLE KODER for 60617 og 60618, nå rettet av JP
                ' Disse ble nå 60623 og 60624
            Case 60639
                sNorwegian = "Bytt tekst"
                sEnglish = "Replace text"
                sSwedish = "Byta ut text"
                sDanish = "Erstatte tekst"
            Case 60640
                sNorwegian = "Søk etter"
                sEnglish = "Find"
                sSwedish = "Sök efter"
                sDanish = "Søg efter"
            Case 60641
                sNorwegian = "Bytt med"
                sEnglish = "Replace with"
                sSwedish = "Byt mot"
                sDanish = "Erstat med"
            Case 60642
                sNorwegian = "Finner ikke angitt søketekst."
                sEnglish = "Unable to find the searchfor text"
                sSwedish = "Hittar inte angett söktext."
                sDanish = "Finder ikke angivet søgetekst."
            Case 60643
                sNorwegian = "Filen %1 er ikke en billedfil. Filen skal være en 16 bit .bmp eller .ico-fil. Vennligst bruk en annen fil."
                sEnglish = "The file %1 is not a graphicsfile. The file needs to be a 16 bit .mpg or .ico file. Please use another file."
                sSwedish = "Filen% 1 är inte en bildfil. Filen ska vara en 16 bit .bmp eller .ico-fil. Vänligen använd en annan fil."
                sDanish = "Filen %1 er ikke en billedfil. Filen skal være en 16 bit .bmp eller .ico-fil. Anvend venligst en anden fil."
            Case 60644
                sNorwegian = "Standardtekst til nasjonalbank"
                sEnglish = "Standardtext for the National Bank"
                sSwedish = "Standardtext nationellbank"
                sDanish = "Standardtekst til nationalbank"
            Case 60645
                sNorwegian = "Separat fil for NO+SE+DK"
                sEnglish = "Separate file for NO+SE+DK"
                sSwedish = "Separat fil for NO+SE+DK"
                sDanish = "Separat fil for NO+SE+DK"
            Case 60646
                sNorwegian = "Sikre gyldig XML-fil"
                sEnglish = "Ensure valid XML-file"
                sSwedish = "Säkra giltig XML-fil"
                sDanish = "Sikre gyldig XML-fil"
            Case 60647
                sNorwegian = "Gjør om fra IBAN til BBAN for Norge"
                sEnglish = "Redo from IBAN to BBAN for Norway"
                sSwedish = "Gjör om från IBAN till BBAN för Norge"
                sDanish = "Omgøre fra IBAN til BBAN for Norge"
            Case 60648
                sNorwegian = "Gjør om fra IBAN til BBAN for Sverige"
                sEnglish = "Redo from IBAN to BBAN for Sweden"
                sSwedish = "Gjör om från IBAN till BBAN för Sverige"
                sDanish = "Omgøre fra IBAN til BBAN for Sverige"
            Case 60649
                sNorwegian = "Gjør om fra IBAN til BBAN for Danmark"
                sEnglish = "Redo from IBAN to BBAN for Denmark"
                sSwedish = "Gjör om från IBAN till BBAN för Danmark"
                sDanish = "Omgøre fra IBAN til BBAN for Danmark"
            Case 60650
                sNorwegian = "Tillat forskjell i fakturavalutasort og betalingsvalutasort"
                sEnglish = "Allow difference in invoicecurrency and paymentcurrency"
                sSwedish = "Tillåt skillnad i fakturavaluta och betalningsvaluta"
                sDanish = "Tillad forskel i fakturavaluta og betalingsvaluta"
            Case 60651
                sNorwegian = "VÅR Public key fil"
                sEnglish = "OUR Public key file"
                sSwedish = "VÅRAN Public key fil"
                sDanish = "VÅR Public key fil"
            Case 60652
                sNorwegian = "VÅR Private key fil"
                sEnglish = "OUR Private key file"
                sSwedish = "VÅRAN Private key fil"
                sDanish = "VÅR Private key fil"
            Case 60653
                sNorwegian = "Kryptér"
                sEnglish = "Encrypt"
                sSwedish = "Kryptera"
                sDanish = "Krypter"
            Case 60654
                sNorwegian = "Signer"
                sEnglish = "Sign"
                sSwedish = "Signera"
                sDanish = "Signer"
            Case 60655
                sNorwegian = "Algoritme"
                sEnglish = "Cypher"
                sSwedish = "Algoritm"
                sDanish = "Algoritme"
            Case 60656
                sNorwegian = "BANKENS Public key fil"
                sEnglish = "BANK Public key file"
                sSwedish = "BANKENS Public key fil"
                sDanish = "BANKENS Public key fil"
            Case 60657
                sNorwegian = "Lag nøkkelpar for signering"
                sEnglish = "Create keypair for signing"
                sSwedish = "Skapa nyckelpar för signering"
                sDanish = "Opret nøglepar til signering"
            Case 60658
                sNorwegian = "Du må oppgi filnavn for VÅR Public key fil"
                sEnglish = "Please speficy filename for OUR Public key file"
                sSwedish = "Du måste ange filnavn for VÅRAN Public key fil"
                sDanish = "Du skal indtaste filnavn for VÅR Public key fil"
            Case 60659
                sNorwegian = "Du må oppgi filnavn for VÅR Private key fil"
                sEnglish = "Please speficy filename for OUR Private key file"
                sSwedish = "Du måste ange filnavn for VÅRAN Private key fil"
                sDanish = "Du skal indtaste filnavn for VÅR Private key fil"
            Case 60660
                sNorwegian = "BrukerID"
                sEnglish = "UserID"
                sSwedish = "Användar ID"
                sDanish = "Bruger ID"
            Case 60661
                sNorwegian = "Du må oppgi bruker ID for nøkkelgenerering"
                sEnglish = "Please enter user ID for key generation"
                sSwedish = "Du måste ange användar-ID för nyckelgenerering"
                sDanish = "Du skal indtaste bruger-id til nøglegenerering"
            Case 60662
                sNorwegian = "Nøkkelpar opprettet"
                sEnglish = "Keypair generated"
                sSwedish = "Nyckelpar skapat"
                sDanish = "Nøglepar oprettet"
            Case 60663
                sNorwegian = "Eksporter public key til fil"
                sEnglish = "Export public key to file"
                sSwedish = "Exportera public key till fil"
                sDanish = "Eksporter public key til fil"
            Case 60664
                sNorwegian = "Passord"
                sEnglish = "Password"
                sSwedish = "Lösenord"
                sDanish = "Passord"
            Case 60665
                sNorwegian = "Ønsker du å eksportere din 'Public key' til en fil, som må sendes til banken?"
                sEnglish = "Will you like to export your 'Public key' to a file, which must be sent to the bank?"
                sSwedish = "Vill du exportera din 'Public key' till en fil, som måste skickas till banken?"
                sDanish = "Ønsker du at eksportere din 'Public key' til en fil, der skal sendes til banken?"
            Case 60666
                sNorwegian = "Er du helt sikker på at du vil lage nytt nøkkelpar?"
                sEnglish = "Are you absolutely sure you want to create a new keypair?"
                sSwedish = "Är du helt säker på att du vill skapa nye nyckler?"
                sDanish = "Er du helt sikker på du vil oprette ny nøgle?"
            Case 60667
                sNorwegian = "Dine tidligere nøkler laget %1 vil overskrives. Er det OK?"
                sEnglish = "Your previous keys created %1 will be overwritten. Is that OK?"
                sSwedish = "Dina tidigare nycklar som skapats %1 kommer att skrivas över. Är det OK?"
                sDanish = "Dine tidligere nøgler skabte %1 vil blive overskrevet. Er det OK?"
            Case 60668
                sNorwegian = "Du må oppgi passord"
                sEnglish = "Please enter password"
                sSwedish = "Du måste ange lösenord"
                sDanish = "Du skal indtaste passord"
            Case 60669
                sNorwegian = "Benytt en gyldig epost adresse som brukerID."
                sEnglish = "Please use a valid email address as UserID."
                sSwedish = "Använd en giltig e-postadress som användar-ID."
                sDanish = "Brug en gyldig e-mail-adresse som brugernavn."
            Case 60670
                sNorwegian = "Bekreft passord"
                sEnglish = "Confirm password"
                sSwedish = "Kontroller lösenord"
                sDanish = "Verifiser passord"
            Case 60671
                sNorwegian = "Passordene er ikke like!"
                sEnglish = "Password does not match!"
                sSwedish = "Lösenorden er inte lika."
                sDanish = "Passordene er ikke lige!"
            Case 60672
                sNorwegian = "Finner ikke bankens Public key fil!"
                sEnglish = "Banks public keyfile not found!"
                sSwedish = "Hittar inte bankens Public key fil!"
                sDanish = "Finder ikke bankens Public key fil!"
            Case 60673
                sNorwegian = "Importér bankens Public key fil"
                sEnglish = "Import bank's public key file"
                sSwedish = "Importéra bankens Public key fil"
                sDanish = "Importér bankens Public key fil"
            Case 60674
                sNorwegian = "Dekryptér"
                sEnglish = "Decrypt"
                sSwedish = "Dekryptér"
                sDanish = "Dekryptér"
            Case 60675
                sNorwegian = "Kontrollér signatur"
                sEnglish = "Check signature"
                sSwedish = "Kontrolér signatur"
                sDanish = "Kontrollér signatur"
            Case 60676
                sNorwegian = "Importér bankens sertifikatfil"
                sEnglish = "Import bank's certificate file"
                sSwedish = "Importéra bankens certifikat fil"
                sDanish = "Importér bankens certifikat fil"
            Case 60677
                sNorwegian = "BANKENS sertifikatfil"
                sEnglish = "BANK certificate"
                sSwedish = "BANKENS certificat fil"
                sDanish = "BANKENS certifikat fil"
            Case 60678
                sNorwegian = "Eksternt navn"
                sEnglish = "External name"
                sSwedish = "Eksternt namn"
                sDanish = "Eksternt navn"
            Case 60679
                sNorwegian = "Sertifikatets utløpsdato: "
                sEnglish = "Certificate Expiry Date: "
                sSwedish = "Certifikatets utgångsdatum: "
                sDanish = "Certifikatets udløbsdato: "
            Case 60680
                sNorwegian = "Merk betalinger til egne konti som konserninterne betalinger"
                sEnglish = "Mark payments to own account as internal payments"
                sSwedish = "Markera betalningar til egna konti som koncerninterna betalningar"
                sDanish = "Markér betalinger til egne konti som konserninterne betalinger"
            Case 60681
                sNorwegian = "Hvis sertifikat som fil, angi filnavn"
                sEnglish = "If certicate from file, specify filename"
                sSwedish = "Om certifikat som fil anger du filnamn"
                sDanish = "Hvis certifikat som fil, indtast filnavn"
            Case 60682
                sNorwegian = "IBAN konto kreditt"
                sEnglish = "IBAN account credit"
                sSwedish = "IBAN konto kredit"
                sDanish = "IBAN konto kredit"
            Case 60683
                sNorwegian = "Vil du lagre endringene i avstemmingsregel: "
                sEnglish = "Do you want to save the changes in the reconciliation rule: "
                sSwedish = "Vill du spara ändringarna i avstämningsregeln: "
                sDanish = "Vil du gemme ændringerne i forsoningsreglen: "
            Case 60684
                sNorwegian = "Tegnsett på fil"
                sEnglish = "Encoding of the file"
                sSwedish = "Teknsett på fil"
                sDanish = "Tegnsetting af fil"
            Case 60685
                sNorwegian = "Ikke spesifisert"
                sEnglish = "No specific encoding"
                sSwedish = "Ej specificerad"
                sDanish = "Ikke specificeret"
            Case 60686
                sNorwegian = "UTF-8 uten BOM"
                sEnglish = "UTF_8 without BOM"
                sSwedish = "UTF-8 utan BOM"
                sDanish = "UTF-8 uden BOM"
            Case 60687
                sNorwegian = "UTF-8 med BOM"
                sEnglish = "UTF_8 with BOM"
                sSwedish = "UTF-8 med BOM"
                sDanish = "UTF-8 med BOM"
            Case 60688
                sNorwegian = "Kundespesifikk"
                sEnglish = "Customer specific"
                sSwedish = "Kundspesifik"
                sDanish = "Kundespesifikk"
            Case 60689
                sNorwegian = "Konfidensiell"
                sEnglish = "Confidential"
                sSwedish = "Konfidentiell"
                sDanish = "Fortrolig"
            Case 60690
                sNorwegian = "Innbetalinger KID, avstemte"
                sEnglish = "Incoming payments KID, Matched"
                sSwedish = "Inbetalningar KID, avstämda"
                sDanish = "Indbetalinger KID, afstemte"
            Case 60691
                sNorwegian = "Personnummer (SOSE)"
                sEnglish = "Social security no. (SOSE)"
                sSwedish = "Personnummer (SOSE)"
                sDanish = "CPR nummer (SOSE)"
            Case 60692
                sNorwegian = "Dette oppsettet benyttes kun for Nordea." & vbCrLf & "Angi hvilken Filtype og Service ID som skal importeres i denne profilen."
                sEnglish = "This setup is used for Nordea." & vbCrLf & "Select which Filetype and ServiceID to be imported in this profile."
                sSwedish = "Denna inställning används för Nordea." & vbCrLf & "Välj vilken filtyp och service-ID som ska importeras i den här profilen."
                sDanish = "Denne opsætning bruges til Nordea." & vbCrLf & "Vælg hvilken filtype og service-id, der skal importeres i denne profil."
            Case 60693
                sNorwegian = "Spesifiser filinnhold"
                sEnglish = "Specify file content"
                sSwedish = "Ange filinnehåll"
                sDanish = "Angiv filindhold"
            Case 60694
                sNorwegian = "Filnavn ved feil"
                sEnglish = "Filename when error"
                sSwedish = "Filnamn vid fel"
                sDanish = "Filnavn ved fejl"

        End Select

        If bUseDanish Then
            sReturnString = sDanish
        ElseIf bUseSwedish Then
            sReturnString = sSwedish
        ElseIf bUseNorwegian Then
            sReturnString = sNorwegian
        Else
            sReturnString = sEnglish
        End If
        ' If no hit, then use English;
        If EmptyString(sReturnString) Then
            sReturnString = sEnglish
        End If
        ' If still not found
        If EmptyString(sReturnString) Then
            sReturnString = StringIndex & " languagestring is missing."
        End If

        sReturnString = Replace(sReturnString, "%1", Trim$(sString1))
        sReturnString = Replace(sReturnString, "%2", Trim$(sString2))
        sReturnString = Replace(sReturnString, "%3", Trim$(sString3))

        LRS = sReturnString


    End Function

End Module
