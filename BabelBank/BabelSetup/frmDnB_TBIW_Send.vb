Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmDnB_TBIW_Send
    Inherits System.Windows.Forms.Form

    Public bStatus As Boolean
    Private frmDnB_TBIW_Advanced As frmDnB_TBIW_Advanced
    Private frmWiz_Report As frmWiz_Report
    Private frmMapping As frmMapping
    Private frmEmail As frmEmail
    Private frmReportAdvanced As frmReportAdvanced
    Private frmReportPrinter As frmReportPrinter
    Private frmDnB_TBIW_MergePayments As frmDNB_TBIW_MergePayments
    Private frmLog As frmLog
    Private frmDnB_TBIW_Accounts As frmDnB_TBIW_Accounts
    Private frmDnB_TBIW_Charges As frmDnB_TBIW_Charges
    Private frmDnB_TBIW_NBcodes As frmDnB_TBIW_NBcodes
    Private frmDNB_TBIW_CurrencyField As frmDNB_TBIW_CurrencyField

    'UPGRADE_WARNING: Event chkBackup.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkBackup_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkBackup.CheckStateChanged
        BackupEnable()
    End Sub
    Public Sub Set_frmMapping(ByVal frm As frmMapping)
        frmMapping = frm
    End Sub
    Public Sub Set_frmEmail(ByVal frm As frmEmail)
        frmEmail = frm
    End Sub
    Public Sub Set_frms(ByVal frm1 As frmWiz_Report, ByVal frm2 As frmReportAdvanced, ByVal frm3 As frmReportPrinter, _
    ByVal frm4 As frmDnB_TBIW_Advanced, ByVal frm5 As frmMapping, ByVal frm6 As frmEmail, ByVal frm7 As frmDnB_TBIW_NBcodes, _
    ByVal frm8 As frmDNB_TBIW_MergePayments, ByVal frm9 As frmLog, ByVal frm10 As frmDnB_TBIW_Accounts, _
    ByVal frm11 As frmDnB_TBIW_Charges, ByVal frm12 As frmReportSQL, ByVal frm13 As frmDNB_TBIW_CurrencyField)
        frmWiz_Report = frm1
        frmReportAdvanced = frm2
        frmReportPrinter = frm3
        frmDnB_TBIW_Advanced = frm4
        frmMapping = frm5
        frmEmail = frm6
        frmDnB_TBIW_NBcodes = frm7
        frmDnB_TBIW_MergePayments = frm8
        frmLog = frm9
        frmDnB_TBIW_Accounts = frm10
        frmDnB_TBIW_Charges = frm11
        frmDNB_TBIW_CurrencyField = frm13
    End Sub
    Private Sub cmdAdvanced_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAdvanced.Click
        frmDnB_TBIW_Advanced.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(Me.Top) + 3000)
        frmDnB_TBIW_Advanced.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(Me.Left) + 500)
        frmDnB_TBIW_Advanced.Set_frms(frmDnB_TBIW_MergePayments, frmLog, frmDnB_TBIW_Accounts, _
        frmDnB_TBIW_Accounts, frmDnB_TBIW_Charges, frmDnB_TBIW_NBcodes, frmDNB_TBIW_CurrencyField)

        VB6.ShowForm(frmDnB_TBIW_Advanced, 1, Me)
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        Me.Hide()
        bStatus = False
    End Sub

    Private Sub cmdEMail_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdEMail.Click
        VB6.ShowForm(frmEmail, 1, Me)
    End Sub

    Private Sub cmdFileOpenAccount_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpenAccount.Click
        ' Hent filnavn
        Dim spath As String
        Dim sFile As String
        ' Keep filename after Browsing for folder;
        ' Must have \ and .
        If InStr(Me.txtAccountsPath.Text, "\") > 0 And InStr(Me.txtAccountsPath.Text, ".") > 0 Then
            sFile = Mid(Me.txtAccountsPath.Text, InStrRev(Me.txtAccountsPath.Text, "\"))
        ElseIf InStr(Me.txtAccountsPath.Text, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & Me.txtAccountsPath.Text
        Else
            sFile = ""
        End If

        ' Use BrowseForFolders, routine from VBNet ( i Utils)
        'spath = FolderPicker(Me, LRS(60010))   'Velg fil")
        ' spath ends with end of string - remove
        'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)

        ' Changed 18.10.05
        ' using a new function where we can pass a path
        'spath = FixPath((Me.txtAccountsPath.Text))
        spath = BrowseForFilesOrFolders(Me.txtAccountsPath.Text, Me, LRS(60010), False)

        If Len(spath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(spath, ".") = 0 Then
                Me.txtAccountsPath.Text = Replace(spath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                Me.txtAccountsPath.Text = spath
            End If
        End If
    End Sub

    Private Sub cmdFileOpenIn_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpenIn.Click
        ' Hent filnavn
        Dim spath As String
        Dim sFile As String
        ' Keep filename after Browsing for folder;
        ' Must have \ and .
        If InStr(Me.txtFromInternal.Text, "\") > 0 And InStr(Me.txtFromInternal.Text, ".") > 0 Then
            sFile = Mid(Me.txtFromInternal.Text, InStrRev(Me.txtFromInternal.Text, "\"))
        ElseIf InStr(Me.txtFromInternal.Text, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & Me.txtFromInternal.Text
        Else
            sFile = ""
        End If

        ' Use BrowseForFolders, routine from VBNet ( i Utils)
        'spath = FolderPicker(Me, LRS(60010))   'Velg fil")
        ' spath ends with end of string - remove
        'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)

        ' Changed 18.10.05
        ' using a new function where we can pass a path
        'spath = FixPath((Me.txtFromInternal.Text))'
        spath = BrowseForFilesOrFolders(Me.txtFromInternal.Text, Me, LRS(60010), True)

        If Len(spath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(spath, ".") = 0 Then
                Me.txtFromInternal.Text = Replace(spath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                Me.txtFromInternal.Text = spath
            End If
        End If
    End Sub

    Private Sub cmdFileOpenOut_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpenOut.Click
        ' Hent filnavn

        Dim spath As String
        Dim sFile As String
        ' Keep filename after Browsing for folder;
        ' Must have \ and .
        If InStr(Me.txtToBank.Text, "\") > 0 And InStr(Me.txtToBank.Text, ".") > 0 Then
            sFile = Mid(Me.txtToBank.Text, InStrRev(Me.txtToBank.Text, "\"))
        ElseIf InStr(Me.txtToBank.Text, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & Me.txtToBank.Text
        Else
            sFile = ""
        End If

        ' Use BrowseForFolders, routine from VBNet ( i Utils)
        'spath = FolderPicker(Me, LRS(60010))   'Velg fil")
        ' spath ends with end of string - remove
        'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)

        ' Changed 18.10.05
        ' using a new function where we can pass a path
        'spath = FixPath((Me.txtToBank.Text)) '
        spath = BrowseForFilesOrFolders(Me.txtToBank.Text, Me, LRS(60010), True)

        If Len(spath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(spath, ".") = 0 Then
                Me.txtToBank.Text = Replace(spath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                Me.txtToBank.Text = spath
            End If
        End If
    End Sub

    Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
        'Dim bOK As Boolean
        ' Show Helptopic for TBI setup
        'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(120))
    End Sub

    Private Sub cmdMappingFile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdMappingFile.Click
        ' if we have a mappingfile already set for the profile, find it

        frmMapping.cmbFileType.Items.Clear()
        ' fill listbox:
        frmMapping.cmbFileType.Items.Add("Separert (delimited)") 'lrs
        frmMapping.cmbFileType.Items.Add("Fastformat") 'lrs
        frmMapping.cmbFileType.Items.Add("Excel") 'lrs
        'frmMapping.cmbFileType.ListIndex = -1

        If Not EmptyString((frmMapping.txtMappingFile.Text)) Then
            frmMapping.FillFrmMapping()
        End If

        frmMapping.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(Me.Top) + 3000)
        frmMapping.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(Me.Left) + 500)
        VB6.ShowForm(frmMapping, 1, Me)

    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdOK.Click
        ' Check if chkBackup and txtBackupPath are filled
        If Me.chkBackup.CheckState = 1 And Trim(Me.txtBackupPath.Text) = "" Then
            MsgBox(LRS(60300), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "BabelBank")
        ElseIf Me.cmbFormat.SelectedIndex = -1 Then
            ' No informat selected,
            MsgBox(LRS(60053), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "BabelBank") 'Select a format for accountingfiles
        ElseIf Trim(Me.txtAccountsPath.Text) = "" Then
            ' No path for accountsfile filled in
            MsgBox(LRS(60306), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "BabelBank")

            ' validate filepaths, must include either : and \ or \\
        ElseIf InStr(txtFromInternal.Text, "\\") = 0 And InStr(txtFromInternal.Text, ":\") = 0 Then
            MsgBox(LRS(60307), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "BabelBank")
        ElseIf InStr(txtToBank.Text, "\\") = 0 And InStr(txtToBank.Text, ":\") = 0 Then
            MsgBox(LRS(60308), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "BabelBank")
        ElseIf InStr(txtAccountsPath.Text, "\\") = 0 And InStr(txtAccountsPath.Text, ":\") = 0 Then
            MsgBox(LRS(60309), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "BabelBank")
        ElseIf InStr(txtBackupPath.Text, "\\") = 0 And InStr(txtBackupPath.Text, ":\") = 0 Then
            MsgBox(LRS(60310), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "BabelBank")
        ElseIf InStr(txtFromInternal.Text, "\*.*") > 0 Then
            MsgBox(LRS(60421), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "BabelBank") ' Please do not use *.* as importfilename
        ElseIf InStr(txtToBank.Text, "*") > 0 Then
            MsgBox(LRS(60422), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "BabelBank") ' Please do not use * as part of filename to bank
        ElseIf InStr(txtAccountsPath.Text, "*") > 0 Then
            MsgBox(LRS(60423), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "BabelBank") ' Please specify path only (no *)
        ElseIf InStr(txtBackupPath.Text, "*") > 0 Then
            MsgBox(LRS(60424), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "BabelBank") ' Please specify path only (no *)
        Else
            Me.Hide()
            bStatus = True
        End If


    End Sub
    Private Sub cmdReport_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdReport.Click
        ' Hide wizard-buttons;
        frmWiz_Report.CmdBack.Visible = False
        frmWiz_Report.CmdNext.Visible = False
        frmWiz_Report.CmdCancel.Visible = False
        frmWiz_Report.CmdFinish.Enabled = True
        frmWiz_Report.CmdFinish.Visible = True
        frmWiz_Report.CmdFinish.Text = "&OK"

        frmWiz_Report.ShowDialog()
    End Sub
    Sub frmDnB_TBIW_Send_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        BackupEnable()
        FormvbStyle(Me, "dollar.jpg", 450)
        ' 28.12.2010 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next
        FormLRSCaptions(Me)
        ' Sett DnBNor TBI Sendprofile in forms caption:
        Me.Text = "DnB Connect " & LRS(60049)
        Me.lblHeading.Text = LRS(60049)  ' don't know why this one is not automatically lrs'ed
        'FormSelectAllTextboxs Me
    End Sub
    Private Sub BackupEnable()
        If chkBackup.CheckState = 1 Then
            lblDelDays.Enabled = True
            txtDelDays.Enabled = True
            lblBackupPath.Enabled = True
            txtBackupPath.Enabled = True
            cmdFileOpenBackup.Enabled = True
        Else
            lblDelDays.Enabled = False
            txtDelDays.Enabled = False
            lblBackupPath.Enabled = False
            txtBackupPath.Enabled = False
            cmdFileOpenBackup.Enabled = False
        End If

    End Sub
    Private Sub cmdFileOpenBackup_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpenBackup.Click
        ' Hent filnavn
        Dim spath As String

        ' Use BrowseForFolders, routine from VBNet ( i Utils)
        'spath = FolderPicker(Me, LRS(60010), False)  'Velg fil")
        ' spath ends with end of string - remove
        'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)

        ' Changed 18.10.05
        ' using a new function where we can pass a path
        'spath = FixPath((Me.txtBackupPath.Text))
        spath = BrowseForFilesOrFolders(Me.txtBackupPath.Text, Me, LRS(60010), True)

        If Len(spath) > 0 Then
            Me.txtBackupPath.Text = spath
        End If

    End Sub

    Private Sub txtAccountsPath_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAccountsPath.Leave
        Dim bx As Boolean

        bx = CheckPath((txtAccountsPath.Text), False, True)

    End Sub

    Private Sub txtBackupPath_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBackupPath.Leave
        Dim bx As Boolean

        'No need to state a path here
        bx = CheckPath((txtBackupPath.Text), False, False)

    End Sub

    Private Sub txtFromInternal_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFromInternal.Leave
        Dim bx As Boolean

        bx = CheckPath((txtFromInternal.Text), True, True)

    End Sub

    Private Sub txtToBank_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtToBank.Leave
        Dim bx As Boolean

        bx = CheckPath((txtToBank.Text), True, True)

    End Sub

    ' XNET 20.12.2010 added next Sub
    ' XNET 22.08.2013 Removed chkTelepayPlus_Click()


    'Sub GotoNextControl()
    '  Dim LastIndex As Long
    '  Dim ActiveTabIndex As Long
    '  Dim NumberOfControls As Long
    '  Dim Cntrl As Control
    '  Dim TempControl As Control
    '  Dim bControlHasATABIndex As Boolean
    '
    '  NumberOfControls = Screen.ActiveForm.Controls.Count
    '  LastIndex = NumberOfControls
    '  ActiveTabIndex = Screen.ActiveControl.TabIndex
    '  If ActiveTabIndex = LastIndex - 1 Then
    '    ActiveTabIndex = -1
    '  End If
    '  For Each Cntrl In Screen.ActiveForm.Controls
    '    With Cntrl
    '      bControlHasATABIndex = True
    '      On Error Resume Next
    '      If .TabIndex < -1 Then
    '        bControlHasATABIndex = False
    '      End If
    '      If bControlHasATABIndex Then
    '        If .Enabled And .TabIndex > ActiveTabIndex Then
    '          If .TabIndex < LastIndex Then
    '            Set TempControl = Cntrl
    '            LastIndex = .TabIndex
    '          End If
    '        End If
    '      End If
    '      On Error GoTo 0
    '    End With
    '  Next
    '  TempControl.SetFocus
    'End Sub
    '
End Class
