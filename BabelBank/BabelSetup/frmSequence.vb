﻿Option Strict Off
Option Explicit On
Friend Class frmSequence
    Inherits System.Windows.Forms.Form
    'Dim iFileSetup_ID As Integer
    Dim bSomeThingChanged As Boolean
    Dim bKeepBatch As Boolean
    Public bFormLoaded As Boolean
    Private bStatus As Boolean
    Private bCalledFromWizard As Boolean
    Private iFileSetup_ID As Integer
    Private oProfile As vbBabel.Profile
    Private WithEvents dgvTxtBox As DataGridViewTextBoxEditingControl
    Public Property Profile() As vbBabel.Profile
        Get
            Profile = oProfile
        End Get
        Set(ByVal Value As vbBabel.Profile)
            oProfile = Value
        End Set
    End Property
    Public Property CalledFromWizard() As Boolean
        Get
            CalledFromWizard = bCalledFromWizard
        End Get
        Set(ByVal Value As Boolean)
            bCalledFromWizard = Value
        End Set
    End Property
    Public Property Status() As Boolean
        Get
            Status = bStatus
        End Get
        Set(ByVal Value As Boolean)
            bStatus = Value
        End Set
    End Property
    Public Property FileSetup_ID() As Integer
        Get
            FileSetup_ID = iFileSetup_ID
        End Get
        Set(ByVal Value As Integer)
            iFileSetup_ID = Value
        End Set
    End Property
    Private Sub DataGridView1_EditingControlShowing(ByVal sender As Object, _
                   ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) _
                   Handles gridClients.EditingControlShowing

        Dim ctrl As Control = e.Control
        If TypeOf ctrl Is DataGridViewTextBoxEditingControl Then
            dgvTxtBox = DirectCast(ctrl, DataGridViewTextBoxEditingControl)
            'ElseIf TypeOf ctrl Is DataGridViewComboBoxEditingControl Then
            '    dgvCbxBox = DirectCast(ctrl, DataGridViewComboBoxEditingControl)
            '    'There are other types of editing control that you still need to handle
            '    'by adding more ElseIf's
        End If

    End Sub
    Private Sub frmSequence_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "")

    End Sub

    'UPGRADE_WARNING: Event optSequenceFromFile.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optSequenceFromFile_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optSequenceFromFile.CheckedChanged
        If eventSender.Checked Then
            If optSequenceFromFile.Checked = True Then
                Me.lblFileSequence.Enabled = False
                Me.txtSequenceNumber.Enabled = False
                Me.gridClients.Visible = False
            End If
            If bFormLoaded Then
                bSomeThingChanged = True
            End If
        End If
    End Sub
    Friend Function SetKeepBatch(ByRef b As Boolean) As Boolean

        bKeepBatch = b

    End Function
    'UPGRADE_WARNING: Event optSequencePrClient.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optSequencePrClient_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optSequencePrClient.CheckedChanged
        If eventSender.Checked Then
            If optSequencePrClient.Checked = True Then
                If Not bKeepBatch Then
                    MsgBox(Replace(Replace(Replace(LRS(60461, LRS(60063), LRS(55008)), "%3", LRS(60108)), "%4", LRS(60239)), "&", ""), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, LRS(60462))
                    'The combination to set 'Keep clients' to true under Profilesetup - Filename to bank - Advanced, ' and to set the 'sequencenumber type' to 'Use sequencenumber per client under' under Setup - SequencenoSetup       ' is unlogical.        'If You are not sure, please contact Your dealer.
                    'UNLOGICAL COMBINATION
                End If
                Me.lblFileSequence.Enabled = False
                Me.txtSequenceNumber.Enabled = False
                ' Fill spread with clients and sequencenumbers
                'FillSpreadSequence()
                'If optSequencePrFile.Checked = True And txtSequenceNumber.Visible Then
                ' txtSequenceNumber.Focus()
                'End If

                Me.gridClients.Visible = True
            Else
                'frmSequence.chkSequenceFromFile.Value = False
                '    frmSequence.lblFileSequence.Enabled = True
                '    frmSequence.txtSequenceNumber.Enabled = True


                Me.gridClients.Visible = False
            End If
            If bFormLoaded Then
                bSomeThingChanged = True
            End If

        End If
    End Sub
    'UPGRADE_WARNING: Event optSequencePrFile.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optSequencePrFile_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optSequencePrFile.CheckedChanged
        If eventSender.Checked Then
            If optSequencePrFile.Checked = True Then
                Me.lblFileSequence.Enabled = True
                Me.txtSequenceNumber.Enabled = True
                Me.gridClients.Visible = False
                If bFormLoaded And Me.txtSequenceNumber.Visible Then
                    Me.txtSequenceNumber.Focus()
                    Me.txtSequenceNumber.SelectionStart = 0
                    Me.txtSequenceNumber.SelectionLength = 4
                End If

            Else
                Me.lblFileSequence.Enabled = False
                Me.txtSequenceNumber.Enabled = False
            End If
            If bFormLoaded Then
                bSomeThingChanged = True
            End If

        End If
    End Sub
    Private Sub lstProfiles_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstProfiles.SelectedIndexChanged
        If bSomeThingChanged Then
            '    If MsgBox(LRS(60146), vbYesNo + vbDefaultButton1, "BabelBank") = vbYes Then
            SaveProfileSeq()
        End If

        ' show controls
        'frmSequence.optSequenceFromFile.Enabled = True
        'frmSequence.optSequencePrFile.Enabled = True
        'frmSequence.optSequencePrClient.Enabled = True
        Me.lblFileSequence.Enabled = True
        Me.txtSequenceNumber.Enabled = True
        Me.txtSequenceNumber.Text = CStr(0)
        Me.gridClients.Visible = False
        Me.cmdOK.Enabled = True

        ' Fill spread with clients and sequencenumbers
        FillSpreadSequence()
        If optSequencePrFile.Checked = True And txtSequenceNumber.Visible Then
            txtSequenceNumber.Focus()
        End If
        bSomeThingChanged = False
    End Sub
    Private Sub txtSequencenumber_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtSequenceNumber.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
        If KeyAscii < Asc("0") Or KeyAscii > Asc("9") Then
            KeyAscii = 0 ' Cancel the character.
            Beep() ' Sound error signal.
        End If
        bSomeThingChanged = True
        eventArgs.KeyChar = Chr(KeyAscii)
        If KeyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub
    'Private Sub sprclients_KeyPressEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxFPSpreadADO._DSpreadEvents_KeyPressEvent) Handles sprclients.KeyPressEvent
    ''If EventArgs.KeyAscii < Asc("0") Or EventArgs.KeyAscii > Asc("9") Then
    '    If e.KeyChar < Asc("0") Or e.KeyChar > Asc("9") Then
    '        EventArgs.KeyAscii = 0 ' Cancel the character.
    '        Beep() ' Sound error signal.
    '    End If
    '    bSomeThingChanged = True
    'End Sub
    Private Sub dgvTxtBox_KeyPress(ByVal sender As Object, _
                                   ByVal e As System.Windows.Forms.KeyPressEventArgs) _
                                   Handles dgvTxtBox.KeyPress
        'Do whatever you need to do for the keypress event
        If e.KeyChar < ChrW(48) Or e.KeyChar > ChrW(57) Then
            ' non numeric - don't bother about them
            Beep() ' Sound error signal.
            e.Handled = True
        Else
            bSomeThingChanged = True
        End If
    End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        If MsgBox(LRS(60011), MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, LRS(60025)) = MsgBoxResult.Yes Then
            ' Cancel, Yes
            bStatus = False
            bSomeThingChanged = False
            Me.Close()
        Else
            ' No Cancel, Continue

        End If
    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        If bSomeThingChanged Then
            bStatus = True
            'If bCalledFromWizard Then
            SaveProfileSeq()
            'End If
        Else
            bStatus = False
        End If
        bSomeThingChanged = False
        Me.Hide()

    End Sub

    Public Function FillSpreadSequence() As Boolean
        'Dim iFileSetup_ID As Integer
        Dim iCounter As Short
        Dim oFilesetup As Object
        Dim oClient As Object
        Dim sText As String
        Dim oRow As DataGridViewRow
        Dim oCell As DataGridViewCell

        If Not bCalledFromWizard Then
            If Me.lstProfiles.Items.Count = 1 Then
                ' only one profile
                If Me.lstProfiles.Items.Count > 0 Then
                    Me.lstProfiles.SelectedIndex = 0
                End If
            End If
            iCounter = -1
            sText = Me.lstProfiles.Text
            'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For Each oFilesetup In oProfile.FileSetups 'hvorfor er oprofile tom her ???
                'UPGRADE_WARNING: Couldn't resolve default property of object oFilesetup.ShortName. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If oFilesetup.ShortName = sText Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object oFilesetup.FileSetupOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    iFileSetup_ID = oFilesetup.FileSetupOut
                    Exit For
                End If
            Next oFilesetup
        End If

        'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        oFilesetup = oProfile.FileSetups(iFileSetup_ID)
        Me.SetKeepBatch(oFilesetup.KeepBatch)

        With Me.gridClients
            Me.gridClients.Rows.Clear()
            ' Fill spread with clients, and sequencenumbers if used
            'UPGRADE_WARNING: Couldn't resolve default property of object oFilesetup.Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            For Each oClient In oFilesetup.Clients
                oRow = New DataGridViewRow
                oRow.Height = 18
                oRow.DefaultCellStyle.SelectionBackColor = Color.White
                oRow.DefaultCellStyle.SelectionForeColor = Color.Black

                oCell = New DataGridViewTextBoxCell
                oCell.Value = oClient.name
                'oCell.ReadOnly = True
                oRow.Cells.Add(oCell)
                '.Rows(.RowCount).Cells(0).ReadOnly = True

                oCell = New DataGridViewTextBoxCell
                oCell.Value = oClient.SeqNoTotal
                oRow.Cells.Add(oCell)

                Me.gridClients.Rows.Add(oRow)

                ' Mark client as changed sequence
                'UPGRADE_WARNING: Couldn't resolve default property of object oClient.Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oClient.Status = vbBabel.Profile.CollectionStatus.SeqNoChange

            Next oClient

            If .RowCount = 0 Then
                ' no clients found for this profile, no need to show spread
                Me.gridClients.Visible = False
            Else
                Me.gridClients.CurrentCell = Me.gridClients.Item(0, 0)
            End If
        End With

        'UPGRADE_WARNING: Couldn't resolve default property of object oFilesetup.SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If oFilesetup.SeqnoType = 0 Then
            ' sequence from imported file
            Me.optSequenceFromFile.Checked = True
            'me.optSequencePrClient.Value = False
            Me.lblFileSequence.Enabled = False
            Me.txtSequenceNumber.Enabled = False
            'me.txtSequenceNumber.Text = 0
            Me.gridClients.Visible = False

            'UPGRADE_WARNING: Couldn't resolve default property of object oFilesetup.SeqnoType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        ElseIf oFilesetup.SeqnoType = 1 Then
            ' one sequenceseries pr file
            Me.optSequencePrFile.Checked = True
            'me.optSequencePrClient.Value = False
            Me.lblFileSequence.Enabled = True
            Me.txtSequenceNumber.Enabled = True
            'UPGRADE_WARNING: Couldn't resolve default property of object oFilesetup.SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.txtSequenceNumber.Text = oFilesetup.SeqNoTotal
            Me.txtSequenceNumber.SelectionStart = 0
            Me.txtSequenceNumber.SelectionLength = 4
            Me.gridClients.Visible = False
        Else
            ' sequence pr client
            '    me.optSequenceFromFile.Value = False
            Me.optSequencePrClient.Checked = True
            Me.lblFileSequence.Enabled = False
            Me.txtSequenceNumber.Enabled = False
            'UPGRADE_WARNING: Couldn't resolve default property of object oFilesetup.SeqNoTotal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.txtSequenceNumber.Text = oFilesetup.SeqNoTotal
            Me.gridClients.Visible = True
        End If

        'UPGRADE_WARNING: Couldn't resolve default property of object oFilesetup.KeepBatch. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        'Me.SetKeepBatch(oFilesetup.KeepBatch)
        Me.bFormLoaded = True

        FillSpreadSequence = True

    End Function
    Private Function SaveProfileSeq() As Boolean
        Dim oFilesetup As vbBabel.FileSetup
        Dim i As Short
        Dim iSeq As Short
        Dim sName As String


        'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If oProfile.FileSetups(iFileSetup_ID).FileSetupOut <> -1 Then
            'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sName = oProfile.FileSetups(oProfile.FileSetups(iFileSetup_ID).FileSetupOut).ShortName
        Else
            sName = " "
        End If
        If MsgBox(LRS(60146), MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then

            ' Mark as changed;
            'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oFilesetup = oProfile.FileSetups(iFileSetup_ID)
            oFilesetup.Status = vbBabel.Profile.CollectionStatus.Changed
            'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oProfile.Status = vbBabel.Profile.CollectionStatus.ChangeUnder
            If Me.optSequenceFromFile.Checked Then
                oFilesetup.SeqnoType = vbBabel.BabelFiles.SeqnoType.FromImportfile
            ElseIf Me.optSequencePrFile.Checked Then
                oFilesetup.SeqnoType = vbBabel.BabelFiles.SeqnoType.PerFilesetup
                oFilesetup.SeqNoTotal = CDbl(Me.txtSequenceNumber.Text)
            Else
                oFilesetup.SeqnoType = vbBabel.BabelFiles.SeqnoType.PerClient
                ' Must save sequencenos pr client here:
                ' Order in oFilesetup.Clients is the same as in spread, therefore:
                For i = 1 To Me.gridClients.RowCount
                    iSeq = Val(Me.gridClients.Rows(i - 1).Cells(1).Value)
                    oFilesetup.Clients(i).SeqNoTotal = iSeq
                Next i
                ' spreadshit
                'For i = 1 To frmSequence.sprClients.MaxRows
                ' frmSequence.sprClients.col = 2
                ' frmSequence.sprClients.row = i
                ' iSeq = Val(frmSequence.sprClients.Text)
                ' oFilesetup.Clients(i).SeqNoTotal = iSeq
                ' Next i
            End If
        End If

    End Function
    Private Sub gridClients_EditingControlShowing( _
      ByVal sender As Object, _
      ByVal e As System.Windows.Forms. _
      DataGridViewEditingControlShowingEventArgs) _
      Handles gridClients.EditingControlShowing

        '---restrict inputs on the fourth field---
        If Me.gridClients.CurrentCell.ColumnIndex = 1 And _
           Not e.Control Is Nothing Then
            Dim tb As TextBox = CType(e.Control, TextBox)

            '---add an event handler to the TextBox control---
            AddHandler tb.KeyPress, AddressOf TextBox_KeyPress

            ' test values, min 0, max 9999
            If Me.gridClients.CurrentCell.Value < 0 And Me.gridClients.CurrentCell.Value > 9999 Then
                Me.gridClients.CurrentCell.Value = 9999
            End If
        End If
    End Sub
    Private Sub TextBox_KeyPress( _
       ByVal sender As System.Object, _
       ByVal e As System.Windows.Forms.KeyPressEventArgs)

        ''---if textbox is empty and user pressed a decimal char---
        'If CType(sender, TextBox).Text = String.Empty And _
        '   e.KeyChar = Chr(46) Then
        '    e.Handled = True
        '    Return
        'End If
        ''---if textbox already has a decimal point---
        'If CType(sender, TextBox).Text.Contains(Chr(46)) And _
        '   e.KeyChar = Chr(46) Then
        '    e.Handled = True
        '    Return
        'End If
        '---if the key pressed is not a valid decimal number---
        If (Not (Char.IsDigit(e.KeyChar))) Then  ' allow digits
            e.Handled = True
        End If

        'If (Not (Char.IsDigit(e.KeyChar) Or _
        '   Char.IsControl(e.KeyChar))) And Asc(e.KeyChar) <> 8 Then  ' allow backspace and digits
        '    e.Handled = True
        'End If
    End Sub
    Private Sub gridClients_CellValidating(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles gridClients.CellValidating
        Dim cell As DataGridViewCell = Me.gridClients.Item(e.ColumnIndex, e.RowIndex)

        If cell.IsInEditMode Then
            Dim c As Control = gridClients.EditingControl

            If e.ColumnIndex = 1 Then
                ' seq no, from 0 to 9999
                c.Text = CleanInputNumber(c.Text)
                If Val(c.Text) < 0 Then
                    c.Text = "0"
                End If
                If Val(c.Text) > 9999 Then
                    c.Text = Mid(c.Text, 1, 4)
                End If
            End If
        End If
    End Sub
    'Private Sub OETrucksDataGridView_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles OETrucksDataGridView.CellFormatting
    '    Dim dgv As DataGridView = CType(sender, DataGridView)
    '    If dgv.CurrentRow Is Nothing Then Exit Sub
    '    ' get my colors from the grid's DefaultCellStyle 
    '    ' and RowsDefaultCellStyle
    '    Dim FGAll As Color = dgv.DefaultCellStyle.ForeColor
    '    Dim BGAll As Color = dgv.DefaultCellStyle.BackColor
    '    Dim FGSelRow As Color = dgv.RowsDefaultCellStyle.SelectionForeColor
    '    Dim BGSelRow As Color = dgv.RowsDefaultCellStyle.SelectionBackColor
    '    Dim FGCurCell As Color = dgv.DefaultCellStyle.SelectionForeColor
    '    Dim BGCurCell As Color = dgv.DefaultCellStyle.SelectionBackColor
    '    If dgv.Rows(e.RowIndex).Selected Then
    '        If dgv.CurrentCell.ColumnIndex = e.ColumnIndex Then
    '            ' current cell is selected
    '            e.CellStyle.SelectionForeColor = FGCurCell
    '            e.CellStyle.SelectionBackColor = BGCurCell
    '        Else
    '            ' current row is selected
    '            e.CellStyle.SelectionForeColor = FGSelRow
    '            e.CellStyle.SelectionBackColor = BGSelRow
    '        End If
    '    Else
    '        ' this row is not selected
    '        e.CellStyle.ForeColor = FGAll
    '        e.CellStyle.BackColor = BGAll
    '    End If
    'End Sub


End Class