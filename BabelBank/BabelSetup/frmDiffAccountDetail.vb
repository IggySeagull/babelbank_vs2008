Option Strict Off
Option Explicit On
Friend Class frmDiffAccountDetail
    Inherits System.Windows.Forms.Form
    Private bAnythingChanged As Boolean
    Private iOriginalDiffTypeValue As Short
    Private iDiffAccount_ID As Integer
    Private iClient_ID As Integer
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        If bAnythingChanged Then
            If MsgBox(LRS(60011) & " " & Trim(Me.txtDiffAccountName.Text) & " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then
                ' Hide without saving
                bAnythingChanged = False
                Me.Hide()
            End If
        Else
            bAnythingChanged = False
            Me.Hide()
        End If
    End Sub
    Public Sub SetDiffAccount_ID(ByVal iA_Id As Integer)
        iDiffAccount_ID = iA_Id
    End Sub
    Public Sub SetClient_ID(ByVal iC_Id As Integer)
        iClient_ID = iC_Id
    End Sub

    Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
        Dim bOK As Boolean
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdOK.Click
        Me.Hide()
    End Sub

    Sub frmDiffAccountDetail_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        FormvbStyle(Me, "dollar.jpg")
        FormLRSCaptions(Me)

        ' 11.01.2011 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next

        FormSelectAllTextboxs(Me)
        bAnythingChanged = False
        iOriginalDiffTypeValue = cmbDiffType.SelectedIndex
    End Sub
    Private Sub frmDiffAccountDetail_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        If UnloadMode = CloseReason.UserClosing Then
            ' user pressed red x
            Cancel = 1
            cmdCancel_Click(CmdCancel, New System.EventArgs())
        End If
        eventArgs.Cancel = Cancel
    End Sub
    Private Sub txtCurrencyCode_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCurrencyCode.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtDiffAccountName_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDiffAccountName.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtDiffPattern_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDiffPattern.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtDiffText_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDiffText.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub txtVATCode_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtVATCode.TextChanged
        bAnythingChanged = True
    End Sub
    Private Sub cmbDiffType_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbDiffType.SelectedIndexChanged
        If iOriginalDiffTypeValue <> cmbDiffType.SelectedIndex Then
            bAnythingChanged = True
        End If
    End Sub
    Private Sub chkUnmatchedAccount_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkUnmatchedAccount.CheckStateChanged
        bAnythingChanged = True
        If Me.chkUnmatchedAccount.CheckState = 1 Then
            ' Can't have both chkUnmatchedAccount and chkCashdiscountAccount checked
            Me.chkCashDiscountAccount.CheckState = System.Windows.Forms.CheckState.Unchecked
        End If
    End Sub
    Private Sub chkCashdiscountAccount_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkCashDiscountAccount.CheckStateChanged
        bAnythingChanged = True
        If Me.chkCashDiscountAccount.CheckState = 1 Then
            ' Can't have both chkUnmatchedAccount and chkCashdiscountAccount checked
            Me.chkUnmatchedAccount.CheckState = System.Windows.Forms.CheckState.Unchecked
        End If

    End Sub
    Public Function AnythingChanged() As Boolean
        AnythingChanged = bAnythingChanged
    End Function
    Public Function ResetAnythingChanged()
        bAnythingChanged = False
    End Function

    Private Sub cmdDimensions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDimensions.Click
        Dim frmDimensions As New frmDimensions
        frmDimensions.SetDiffAccount_ID(iDiffAccount_ID)
        frmDimensions.SetClient_ID(iClient_ID)
        frmDimensions.ShowDialog()
        frmDimensions.Close()
        frmDimensions = Nothing
    End Sub
End Class
