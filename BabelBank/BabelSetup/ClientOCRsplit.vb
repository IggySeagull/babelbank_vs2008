Option Strict Off
Option Explicit On
Module ClientOCRsplit
    '**********************************************************************************
    ' ClientOCRsplit is seldom or never been used -
    ' That's why we have not yet bodered to swap from Spread to Grid,
    ' Instead commented out references to Spread
    '**********************************************************************************


    '  OCR splitting p� lengde i kid, innhold av kid (Veum, NAFdata)
    '   - Felter i FileSeup:
    '   - SplitOnAccountNo, boolean, default=true
    '
    '   - felter i Clients
    '   - start
    '   - length
    '   - value
    '
    '   ex. klientnr utfra innhold i kid:
    'kid = 12300000
    'kid = 45600000
    'Start = 1
    'length = 3
    'Value = ""
    'gir klientnr 123 og 456
    '
    'ex. klientnr ut fra kidlengde:
    'Start = 0
    'length = 8
    'Value = ""
    'viser til den aktuelle klient innmeldt i setup for alle betalinger med kidlengde=8
    'Start = 0
    'length = 11
    'Value = ""
    'viser til den aktuelle klient i setup for alle betalinger med kidlengde 11
    '
    'ex p� klientnr hvor vi leser fast innhold i KID;
    'Start = 5
    'length = 1
    'Value = "9"
    'viser til den aktuelle klient for betalinger hvor kid har 9 i femte pos
    '
    'Start = 5
    'length = 1
    'Value = "<>9"
    'viser til den aktuelle klient for betalinger hvor kid er ulik 9 i femte pos
    '
    'SETUP:
    ' Lage eget bilde for � angi disse verdier.
    ' Kalles fra menyvalg Oppsett i BabelBank.exe
    ' Klienter m� v�re opprettet p� forh�nd (kan ha link for � opprette klient)
    ' Viser klientnr, kolonne for start, lengde, verdi, med litt ledetekster
    ' I filesetup : Angi under avansert om det skal splittes p� kontonr eller annet begrep
    ' Her kan det v�re en link til klientoppsett for splitting
    '
    '
    Public Const cCLIENTNO As Short = 0
    Public Const cNAME As Short = 1
    Public Const cSTART As Short = 2
    Public Const cLENGTH As Short = 3
    Public Const cVALUE As Short = 4

    Public bOCRSplitStatus As Boolean
    Private aClientsArray(,) As Object 'FIX - dim in showclients?
    Private frmClientOCRsplit As frmClientOCRsplit
    '	Public Function ShowClientsOCRSplit(ByRef oP As Object) As Boolean 'oP is a profile-object
    '		' Fill and show sprClients in frmClientsOCRsplit

    '		Dim i As Short
    '		Dim sClientNo As String
    '		Dim sxClientNo As String
    '        Dim aOCRProfiles(,) As Object
    '		'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
    '		'DOTNETT REDIM Dim aOCRProfiles() As Variant
    '		Dim iCounter As Short
    '		Dim iFileSetup_ID As Short
    '		Dim iMax As Short
    '		' added 14.04.2010
    '		Dim oClientSplit As ClientSplit

    '		iCounter = -1

    '		bOCRSplitStatus = False
    '		'Set Clients.oProfile = oP
    '		' changed 14.04.2010
    '		oClientSplit.Profile = oP

    '		' Find correct filesetup
    '		' Used only for Filesetups for OCR (EnumID = 1, FormatID=1)
    '		' Check if we have any Sendprofiles with OCR as outformat;
    '		'For Each oFilesetup In Clients.oProfile.FileSetups
    '		' changed 14.04.2010
    '		'UPGRADE_WARNING: Couldn't resolve default property of object oClientSplit.Profile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '		For	Each oFilesetup In oClientSplit.Profile.FileSetups

    '			If oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.OCR And oFilesetup.FileNameOut1 <> "" And oFilesetup.FromAccountingSystem = False And oFilesetup.NotSplitOnAccount = True Then
    '				iCounter = iCounter + 1
    '                ReDim aOCRProfiles(1, iCounter)
    '				'UPGRADE_WARNING: Couldn't resolve default property of object aOCRProfiles(0, iCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '				aOCRProfiles(0, iCounter) = oFilesetup.ShortName
    '				'UPGRADE_WARNING: Couldn't resolve default property of object aOCRProfiles(1, iCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '				aOCRProfiles(1, iCounter) = oFilesetup.Filesetup_ID
    '			End If
    '		Next oFilesetup
    '		If iCounter < 0 Then
    '			MsgBox(LRS(60112), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation) '"No OCR-profiles found"
    '		ElseIf iCounter = 0 Then 
    '			' One OCR-format
    '			'UPGRADE_WARNING: Couldn't resolve default property of object aOCRProfiles(1, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '			iFileSetup_ID = aOCRProfiles(1, 0)
    '		Else
    '			' MOre than one OCR-format, not impmented yet
    '			MsgBox(LRS(60113), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation) '"More than one OCR-format. BabelBank selects first. Contact Visual Banking if not OK!
    '			'UPGRADE_WARNING: Couldn't resolve default property of object aOCRProfiles(1, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '			iFileSetup_ID = aOCRProfiles(1, 0)
    '		End If

    '		' Import clients from database
    '		' Get the rest from collections
    '		' Show with spread

    '		' Import clients from database
    '		aClientsArray = dbSelectClientsFormat(iFileSetup_ID) 'dbSelectClients()

    '		' Get the rest from collections

    '		' Show with spread
    '		ClientsSpreadHeadings()

    '		With frmClientOCRsplit.sprClients
    '			.row = 0
    '			.MaxRows = 0

    '			If Not Array_IsEmpty(aClientsArray) Then

    '				'UPGRADE_WARNING: Couldn't resolve default property of object aClientsArray(cCLIENTNO, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '				sxClientNo = aClientsArray(cCLIENTNO, 0)

    '				iMax = UBound(aClientsArray, 2)
    '				For i = 0 To iMax

    '					'UPGRADE_WARNING: Couldn't resolve default property of object aClientsArray(cCLIENTNO, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '					sClientNo = aClientsArray(cCLIENTNO, i)

    '					.row = .row + 1
    '					.MaxRows = .row

    '					'----------
    '					' Client ID
    '					'----------
    '					.col = 1
    '					.CellType = SS_CELL_TYPE_STATIC_TEXT
    '					'UPGRADE_WARNING: Couldn't resolve default property of object aClientsArray(cCLIENTNO, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '					.Text = aClientsArray(cCLIENTNO, i)

    '					'-----------
    '					'Client NAME
    '					'-----------
    '					.col = 2
    '					.CellType = SS_CELL_TYPE_STATIC_TEXT
    '					'UPGRADE_WARNING: Couldn't resolve default property of object aClientsArray(cNAME, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '					.Text = aClientsArray(cNAME, i)


    '					'---------
    '					' Startpos
    '					'---------
    '					.col = 3
    '					.CellType = SS_CELL_TYPE_EDIT
    '					'UPGRADE_WARNING: Couldn't resolve default property of object aClientsArray(cSTART, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '					.Text = aClientsArray(cSTART, i)
    '					If .Text = "-1" Then
    '						.Text = LRS(60114) '"<Not in use>"
    '					End If

    '					'------
    '					'Length
    '					'------
    '					.col = 4
    '					.CellType = SS_CELL_TYPE_EDIT
    '					'UPGRADE_WARNING: Couldn't resolve default property of object aClientsArray(cLENGTH, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '					.Text = aClientsArray(cLENGTH, i)

    '					'------
    '					'Value
    '					'------
    '					.col = 5
    '					.CellType = SS_CELL_TYPE_EDIT
    '					'UPGRADE_WARNING: Couldn't resolve default property of object aClientsArray(cVALUE, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '					.Text = aClientsArray(cVALUE, i)

    '				Next i
    '			End If 'Array_ISEmpty ...
    '			.col = 3
    '			.row = 1
    '			.Action = FPSpreadADO.ActionConstants.ActionActiveCell

    '			'UPGRADE_NOTE: Refresh was upgraded to CtlRefresh. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    '            '.CtlRefresh()
    '		End With

    '		frmClientOCRsplit.lblStart.Text = LRS(60115)
    '		frmClientOCRsplit.lblLength.Text = LRS(60116)
    '		frmClientOCRsplit.lblValue.Text = LRS(60117)

    '		frmClientOCRsplit.ShowDialog()

    '		ShowClientsOCRSplit = bOCRSplitStatus

    '	End Function

    '	Private Sub ClientsSpreadHeadings()


    '		With frmClientOCRsplit.sprClients
    '			.ScrollBars = SS_SCROLLBAR_V_ONLY
    '			.ScrollBarExtMode = True ' show scrollbars only when needed
    '			.AllowMultiBlocks = False
    '			.EditEnterAction = SS_CELL_EDITMODE_EXIT_NEXT
    '			.EditModeReplace = True
    '			.EditModePermanent = True
    '			.ProcessTab = True
    '			.GrayAreaBackColor = System.Drawing.Color.White
    '			'UPGRADE_ISSUE: VBControlExtender method sprClients.BorderStyle was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    '			.BorderStyle = FPSpreadADO.BorderStyleConstants.BorderStyleNone


    '			.MaxCols = 5

    '			' Col headings and widths:
    '			.row = 0
    '			.MaxRows = 1

    '			.col = 1
    '			.Text = LRS(60027) '"KlientID"
    '			.set_ColWidth(-2, 6)

    '			.col = 2
    '			.Text = LRS(60028) '"Klientnavn"
    '			.set_ColWidth(-2, 13)

    '			.col = 3
    '			.Text = LRS(60118) '"Start"
    '			.set_ColWidth(-2, 10)

    '			.col = 4
    '			.Text = LRS(60119) '"Length"
    '			.set_ColWidth(-2, 10)

    '			.col = 5
    '			.Text = LRS(60120) '"Value"
    '			.set_ColWidth(-2, 15)


    '		End With

    '	End Sub

    '	Public Function PrintClientsOCRSplit() As Object
    '		' Fill spread sprPrint with clientsinfo from aClientsArray
    '		' Send to print

    '		' Import clients from database
    '		aClientsArray = VB6.CopyArray(dbSelectClients)

    '		With frmClientOCRsplit.sprClients 'Print


    '			' start printing;
    '			.PrintSmartPrint = True
    '			.PrintMarginLeft = 1440
    '			.PrintMarginTop = 1440
    '			.PrintMarginRight = 1440
    '			.PrintMarginBottom = 1440
    '			.PrintBorder = False
    '			.PrintRowHeaders = False
    '			.PrintGrid = False
    '			.PrintJobName = "BabelBank"
    '			.PrintHeader = "/c/fb1/fz""12""" & "BabelBank - " & LRS(60037) '"Klientoversikt"
    '			.PrintFooter = "/c/p /r BabelBank - Visual Banking AS"
    '			.Action = FPSpreadADO.ActionConstants.ActionSmartPrint



    '		End With

    '	End Function
End Module
