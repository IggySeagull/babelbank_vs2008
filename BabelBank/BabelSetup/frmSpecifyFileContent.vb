﻿Option Strict Off
Option Explicit On
Public Class frmSpecifyFileContent
    Inherits System.Windows.Forms.Form
    Dim oMyDAL As vbBabel.DAL = Nothing
    Dim sMySQL As String = ""
    Dim iRecordCounter As Integer = 0
    Private iCompany_ID As Integer
    Private iFileSetup_ID As Integer
    Dim bCreateEntries As Boolean

    Dim bSIF_ID1Exists As Boolean
    Dim bSIF_ID2Exists As Boolean
    Dim bSIF_ID3Exists As Boolean
    Dim bSIF_ID4Exists As Boolean
    Dim bSIF_ID5Exists As Boolean


    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        If Not oMyDAL Is Nothing Then
            oMyDAL.Close()
            oMyDAL = Nothing
        End If

        Me.Hide()

    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        SaveSetup()

        If Not oMyDAL Is Nothing Then
            oMyDAL.Close()
            oMyDAL = Nothing
        End If

        Me.Hide()
    End Sub

    Sub frmSpecifyFileContent_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg")
        FormLRSCaptions(Me)

        Me.cmbFileType1.Text = ""
        Me.cmbFileType1.Items.Clear()
        Me.cmbFileType1.Items.Add(AddExplaination("NDARCRAXMLO"))
        Me.cmbFileType1.Items.Add(AddExplaination("NDAREXXMLO"))
        Me.cmbFileType1.Items.Add(AddExplaination("NDARSTXMLO"))
        Me.cmbFileType1.Items.Add(AddExplaination("NDCAPXMLD54O"))
        Me.cmbFileType1.Items.Add(AddExplaination("NDCAPXMLI"))
        Me.cmbFileType1.Items.Add(AddExplaination("NDCAPXMLO"))
        Me.cmbFileType2.Text = ""
        Me.cmbFileType2.Items.Clear()
        Me.cmbFileType2.Items.Add(AddExplaination("NDARCRAXMLO"))
        Me.cmbFileType2.Items.Add(AddExplaination("NDAREXXMLO"))
        Me.cmbFileType2.Items.Add(AddExplaination("NDARSTXMLO"))
        Me.cmbFileType2.Items.Add(AddExplaination("NDCAPXMLD54O"))
        Me.cmbFileType2.Items.Add(AddExplaination("NDCAPXMLI"))
        Me.cmbFileType2.Items.Add(AddExplaination("NDCAPXMLO"))
        Me.cmbFileType3.Text = ""
        Me.cmbFileType3.Items.Clear()
        Me.cmbFileType3.Items.Add(AddExplaination("NDARCRAXMLO"))
        Me.cmbFileType3.Items.Add(AddExplaination("NDAREXXMLO"))
        Me.cmbFileType3.Items.Add(AddExplaination("NDARSTXMLO"))
        Me.cmbFileType3.Items.Add(AddExplaination("NDCAPXMLD54O"))
        Me.cmbFileType3.Items.Add(AddExplaination("NDCAPXMLI"))
        Me.cmbFileType3.Items.Add(AddExplaination("NDCAPXMLO"))
        Me.cmbFileType4.Text = ""
        Me.cmbFileType4.Items.Clear()
        Me.cmbFileType4.Items.Add(AddExplaination("NDARCRAXMLO"))
        Me.cmbFileType4.Items.Add(AddExplaination("NDAREXXMLO"))
        Me.cmbFileType4.Items.Add(AddExplaination("NDARSTXMLO"))
        Me.cmbFileType4.Items.Add(AddExplaination("NDCAPXMLD54O"))
        Me.cmbFileType4.Items.Add(AddExplaination("NDCAPXMLI"))
        Me.cmbFileType4.Items.Add(AddExplaination("NDCAPXMLO"))
        Me.cmbFileType5.Text = ""
        Me.cmbFileType5.Items.Clear()
        Me.cmbFileType5.Items.Add(AddExplaination("NDARCRAXMLO"))
        Me.cmbFileType5.Items.Add(AddExplaination("NDAREXXMLO"))
        Me.cmbFileType5.Items.Add(AddExplaination("NDARSTXMLO"))
        Me.cmbFileType5.Items.Add(AddExplaination("NDCAPXMLD54O"))
        Me.cmbFileType5.Items.Add(AddExplaination("NDCAPXMLI"))
        Me.cmbFileType5.Items.Add(AddExplaination("NDCAPXMLO"))

        Me.cmbFileType1.SelectedIndex = -1
        Me.cmbFileType2.SelectedIndex = -1
        Me.cmbFileType3.SelectedIndex = -1
        Me.cmbFileType4.SelectedIndex = -1
        Me.cmbFileType5.SelectedIndex = -1

        bSIF_ID1Exists = False
        bSIF_ID2Exists = False
        bSIF_ID3Exists = False
        bSIF_ID4Exists = False
        bSIF_ID5Exists = False

        oMyDAL = New vbBabel.DAL
        oMyDAL.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDAL.ConnectToDB() Then
            Throw New System.Exception(oMyDAL.ErrorMessage)
        End If

        'Populate the db with the 5 entries
        'First check if 
        bCreateEntries = False
        sMySQL = "SELECT Count(*) AS Occurance FROM SpecifyImportFiles WHERE Company_ID = " & iCompany_ID.ToString & _
        " AND FileSetup_ID = " & iFileSetup_ID.ToString
        oMyDAL.SQL = sMySQL
        If oMyDAL.Reader_Execute() Then
            If oMyDAL.Reader_HasRows Then
                Do While oMyDAL.Reader_ReadRecord
                    If CInt(oMyDAL.Reader_GetString("Occurance")) = 5 Then 'OK
                        'OK - No need to add new entries
                    Else
                        bCreateEntries = True
                    End If
                    Exit Do
                Loop

            Else
                iRecordCounter = 0
                Me.cmbFileType1.SelectedIndex = -1
            End If
        Else
            Throw New Exception(LRSCommon(45002) & oMyDAL.ErrorMessage)
        End If
        If bCreateEntries Then
            CreateEntries()
        End If

        sMySQL = "SELECT SIF_ID, FileType, ServiceID FROM SpecifyImportFiles WHERE Company_ID = " & iCompany_ID.ToString & _
        " AND FileSetup_ID = " & iFileSetup_ID.ToString & " ORDER BY SIF_Id"
        oMyDAL.SQL = sMySQL
        If oMyDAL.Reader_Execute() Then
            If oMyDAL.Reader_HasRows Then
                Do While oMyDAL.Reader_ReadRecord

                    If Not EmptyString(oMyDAL.Reader_GetString("ServiceID")) Then

                        Select Case CInt(oMyDAL.Reader_GetString("SIF_ID"))

                            Case 1
                                bSIF_ID1Exists = True
                                Me.cmbFileType1.SelectedIndex = FindSelectIndex(oMyDAL.Reader_GetString("Filetype"))
                                Me.txtServiceID1.Text = oMyDAL.Reader_GetString("ServiceID")

                            Case 2
                                bSIF_ID2Exists = True
                                Me.cmbFileType2.SelectedIndex = FindSelectIndex(oMyDAL.Reader_GetString("Filetype"))
                                Me.txtServiceID2.Text = oMyDAL.Reader_GetString("ServiceID")

                            Case 3
                                bSIF_ID3Exists = True
                                Me.cmbFileType3.SelectedIndex = FindSelectIndex(oMyDAL.Reader_GetString("Filetype"))
                                Me.txtServiceID3.Text = oMyDAL.Reader_GetString("ServiceID")

                            Case 4
                                bSIF_ID4Exists = True
                                Me.cmbFileType4.SelectedIndex = FindSelectIndex(oMyDAL.Reader_GetString("Filetype"))
                                Me.txtServiceID4.Text = oMyDAL.Reader_GetString("ServiceID")

                            Case 5
                                bSIF_ID5Exists = True
                                Me.cmbFileType5.SelectedIndex = FindSelectIndex(oMyDAL.Reader_GetString("Filetype"))
                                Me.txtServiceID5.Text = oMyDAL.Reader_GetString("ServiceID")

                        End Select

                    End If
                Loop

            Else
                iRecordCounter = 0
                Me.cmbFileType1.SelectedIndex = -1
            End If
        Else
            Throw New Exception(LRSCommon(45002) & oMyDAL.ErrorMessage)
        End If

    End Sub
    Private Function FindSelectIndex(ByVal sFileTypeValue As String) As Integer
        Dim iReturnValue As Integer = -1

        Select Case sFileTypeValue

            Case "NDARCRAXMLO"
                iReturnValue = 0

            Case "NDAREXXMLO"
                iReturnValue = 1

            Case "NDARSTXMLO"
                iReturnValue = 2

            Case "NDCAPXMLD54O"
                iReturnValue = 3

            Case "NDCAPXMLI"
                iReturnValue = 4

            Case "NDCAPXMLO"
                iReturnValue = 5

        End Select

        Return iReturnValue

    End Function

    Private Function AddExplaination(ByRef sFileType As String) As String
        Dim sReturnValue As String = ""

        Select Case sFileType

            Case "NDARCRAXMLO"
                sReturnValue = sFileType & "-Camt.054C"
            Case "NDAREXXMLO"
                sReturnValue = sFileType & "-Camt.053 Extended"
            Case "NDARSTXMLO"
                sReturnValue = sFileType & "-Camt.053"
            Case "NDCAPXMLD54O"
                sReturnValue = sFileType & "-Camt.054D"
            Case "NDCAPXMLI"
                sReturnValue = sFileType & "-Pain.001"
            Case "NDCAPXMLO"
                sReturnValue = sFileType & "-Pain.002"
            Case Else
                sReturnValue = "ERROR"

        End Select

        Return sReturnValue

    End Function
    Private Function RemoveExplaination(ByRef sFileType As String) As String
        Dim sReturnValue As String = ""
        Dim iPosition As Integer = -1
        Dim sTmp As String

        If sFileType.Contains("-") Then
            iPosition = sFileType.IndexOf("-")
            sTmp = sFileType.Substring(0, iPosition)

            Select Case sTmp

                Case "NDARCRAXMLO"
                    sReturnValue = sTmp
                Case "NDAREXXMLO"
                    sReturnValue = sTmp
                Case "NDARSTXMLO"
                    sReturnValue = sTmp
                Case "NDCAPXMLD54O"
                    sReturnValue = sTmp
                Case "NDCAPXMLI"
                    sReturnValue = sTmp
                Case "NDCAPXMLO"
                    sReturnValue = sTmp
                Case Else
                    sReturnValue = "ERROR"

            End Select
        Else
            sReturnValue = "ERROR"
        End If


        Return sReturnValue

    End Function
    Sub CreateEntries()
        Dim iCounter As Integer

        For iCounter = 1 To 5
            sMySQL = "INSERT INTO SpecifyImportFiles(Company_ID, FileSetup_ID, SIF_ID) "
            sMySQL = sMySQL & "VALUES(" & iCompany_ID.ToString & ", " & iFileSetup_ID.ToString & ", "
            sMySQL = sMySQL & iCounter.ToString & ")"

            oMyDAL.SQL = sMySQL

            If oMyDAL.ExecuteNonQuery Then
                If oMyDAL.RecordsAffected = 1 Then
                    'OK
                Else
                    Err.Raise(34222, "SpecifyFileContent.CreateEntries", "En uventet feil oppstod ved lagring filtypen." & vbCrLf & "SQL: " & sMySQL & vbCrLf & vbCrLf & "BabelBank avbrytes!")
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDAL.ErrorMessage)
            End If

        Next iCounter

    End Sub

    Sub UpdateBBDB(ByVal iSIF_ID As Integer, ByVal sFileType As String, ByVal sServiceID As String)
        Dim bRunInsert As Boolean = True

        Select Case iSIF_ID

            Case 1
                If bSIF_ID1Exists Then
                    bRunInsert = False
                End If

            Case 2
                If bSIF_ID2Exists Then
                    bRunInsert = False
                End If

            Case 3
                If bSIF_ID3Exists Then
                    bRunInsert = False
                End If

            Case 4
                If bSIF_ID4Exists Then
                    bRunInsert = False
                End If

            Case 5
                If bSIF_ID5Exists Then
                    bRunInsert = False
                End If

        End Select

        'If bRunInsert Then
        '    sMySQL = "INSERT INTO SpecifyImportFiles(Company_ID, FileSetup_ID, SIF_ID, FileType, ServiceID) "
        '    sMySQL = sMySQL & "VALUES(" & iCompany_ID.ToString & ", " & iFileSetup_ID.ToString & ", "
        '    sMySQL = sMySQL & iSIF_ID.ToString & ", '" & RemoveExplaination(sFileType) & "', '" & sServiceID & "')"
        'Else
        sMySQL = "UPDATE SpecifyImportFiles SET FileType = '" & RemoveExplaination(sFileType)
        sMySQL = sMySQL & "' , ServiceID = '" & sServiceID & "' WHERE Company_ID = "
        sMySQL = sMySQL & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString
        sMySQL = sMySQL & " AND SIF_ID = " & iSIF_ID.ToString
        'sMySQL = "UPDATE SpecifyImportFiles SET SIF_ID = " & iSIF_ID.ToString & ", FileType = '" & RemoveExplaination(sFileType)
        'sMySQL = sMySQL & "' , ServiceID = '" & sServiceID & "' WHERE Company_ID = "
        'sMySQL = sMySQL & iCompany_ID.ToString & " AND FileSetup_ID = " & iFileSetup_ID.ToString
        'End If

        oMyDAL.SQL = sMySQL

        If oMyDAL.ExecuteNonQuery Then
            If oMyDAL.RecordsAffected = 1 Then
                'OK
            Else
                Err.Raise(34222, "SpecifyFileContent.UpdateBBDB", "En uventet feil oppstod ved lagring filtypen." & vbCrLf & "SQL: " & sMySQL & vbCrLf & vbCrLf & "BabelBank avbrytes!")
            End If
        Else
            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDAL.ErrorMessage)
        End If

    End Sub
    Sub RemoveFromBBDB(ByVal iSIF_ID As Integer)

        sMySQL = "UPDATE SpecifyImportFiles SET FileType = '"
        sMySQL = sMySQL & "' , ServiceID = '' WHERE Company_ID = "
        sMySQL = sMySQL & iCompany_ID.ToString & " AND FileSetup_ID = " & iFilesetup_ID.ToString
        sMySQL = sMySQL & " AND SIF_ID = " & iSIF_ID.ToString

        oMyDAL.SQL = sMySQL

        If oMyDAL.ExecuteNonQuery Then
            If oMyDAL.RecordsAffected = 1 Then
                'OK
            Else
                Err.Raise(34223, "RemoveFromBBDB", "En uventet feil oppstod ved sletting av filtypen." & vbCrLf & "SQL: " & sMySQL & vbCrLf & vbCrLf & "Handling avbrytes!")
            End If
        Else
            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDAL.ErrorMessage)
        End If

    End Sub

    Sub SaveSetup()

        If Me.cmbFileType1.SelectedIndex > -1 And Not EmptyString(Me.txtServiceID1.Text) Then
            UpdateBBDB(1, Me.cmbFileType1.Text, Me.txtServiceID1.Text)
        Else
            RemoveFromBBDB(1)
        End If
        If Me.cmbFileType2.SelectedIndex > -1 And Not EmptyString(Me.txtServiceID2.Text) Then
            UpdateBBDB(2, Me.cmbFileType2.Text, Me.txtServiceID2.Text)
        Else
            RemoveFromBBDB(2)
        End If
        If Me.cmbFileType3.SelectedIndex > -1 And Not EmptyString(Me.txtServiceID3.Text) Then
            UpdateBBDB(3, Me.cmbFileType3.Text, Me.txtServiceID3.Text)
        Else
            RemoveFromBBDB(3)
        End If
        If Me.cmbFileType4.SelectedIndex > -1 And Not EmptyString(Me.txtServiceID4.Text) Then
            UpdateBBDB(4, Me.cmbFileType4.Text, Me.txtServiceID4.Text)
        Else
            RemoveFromBBDB(4)
        End If
        If Me.cmbFileType5.SelectedIndex > -1 And Not EmptyString(Me.txtServiceID5.Text) Then
            UpdateBBDB(5, Me.cmbFileType5.Text, Me.txtServiceID5.Text)
        Else
            RemoveFromBBDB(5)
        End If

    End Sub
    Public Sub Company_ID(ByVal i As Integer)
        iCompany_ID = i
    End Sub
    Public Sub FileSetup_ID(ByVal i As Integer)
        iFileSetup_ID = i
    End Sub

End Class