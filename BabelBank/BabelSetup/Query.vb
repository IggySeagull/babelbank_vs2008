Option Strict Off
Option Explicit On
' Johannes# 
<System.Runtime.InteropServices.ProgId("Query_NET.Query")> Public Class Query
    Private oMyERPDal As vbBabel.DAL = Nothing
    Private oProfile As vbBabel.Profile
	Public bStatus As Boolean
    Private Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Integer) As Integer
	Private oBabelFiles As vbbabel.BabelFiles
	Private oBabel As vbbabel.BabelFile
	Private oBatch As vbbabel.Batch
	Private oPayment As vbbabel.Payment
	Private oInvoice As vbbabel.Invoice
	Private oFreetext As vbbabel.Freetext
	Private lPaymentCounter, lBatchCounter, lInvoiceCounter As Integer
	Private iMatchWizard As Short
	'Dim aInvDesc() As String
	Dim sMatchSQL As String
	Dim nTotalMatchAmount As Double
	Dim aResultArray(,) As String
    Dim sOldAccount, sClientNo, sOldClientNo As String
	Dim bFirstPayment As Boolean
	Private iDBProfileID As Short
    Private aQueryArray(,,) As String
    Private Shared frmSQLStatements As frmSQLStatements
    Private Shared frmDatabase As frmDatabase

	'********* START PROPERTY SETTINGS ***********************
	Public WriteOnly Property Profile() As Object
		Set(ByVal Value As Object)
			If IsReference(Value) And Not TypeOf Value Is String Then
				oProfile = Value
			Else
				oProfile = Value
			End If
		End Set
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
	'UPGRADE_NOTE: class_initialize was upgraded to class_initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Sub Class_Initialize_Renamed()
		
		' When Setup is called, the code attempts
		' to find a local satellite DLL that will contain all the
		' resources.
        'If Not (LoadLocalizedResources("Setup")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
        'End If
        If RunTime() Then
            'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'App.HelpFile = My.Application.Info.DirectoryPath & "\babelbank_no.hlp"
        Else
            'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            'App.HelpFile = "c:\projects\babelbank\babelbank_no.hlp"
        End If
		
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	Public Function Start() As Boolean
		Dim bx As Boolean
        Dim aPatternArray(,) As String
		'Need some variables due to the function RestoreManualSetup
		'Only sColumnsToEnterData needed in this context
		Dim bJumpToNextFromSetup, bOmitPaymentsActivated As Boolean
		' Added 08.09.06
		Dim bDeductMatched As Boolean
		' Added 10.08.2007
		Dim bAllowDifferenceInCurrency As Boolean
        Dim sMatchedColumns, sColumnsToEnterData As String

        sMatchedColumns = ""
        sColumnsToEnterData = ""
        'Build an array consisting of the ConnectionStrings and Query agianst the ERP-system.
		
        aQueryArray = GetERPDBInfo((oProfile.Company_ID), 1)
        '25.07.2011 - KI - Moved the code below to retrieve the Pattern for the correct DB_Profile!
        'aPatternArray = GetPattern((oProfile.Company_ID))
        RestoreManualSetup(bJumpToNextFromSetup, sMatchedColumns, sColumnsToEnterData, bOmitPaymentsActivated, bDeductMatched, bAllowDifferenceInCurrency, , , , , , , True)
		

		'Log on to ERP-database
		If Not ShowDatabaseForm(aQueryArray, True) Then
			Start = False
			Exit Function
		End If


        frmSQLStatements = New frmSQLStatements

        '25.07.2011 - KI - Moved from above to retrieve the patterns for the correct DB-Profile
        aPatternArray = GetPattern((oProfile.Company_ID), iDBProfileID)

        bx = frmSQLStatements.SetProfile(oProfile)
		bx = frmSQLStatements.SetQuery(Me)
        'bx = frmSQLStatements.SetQueryArray(aQueryArray())
		bx = frmSQLStatements.SetiDBProfileID(iDBProfileID)
        bx = frmSQLStatements.SetPatternArray(aPatternArray)
		bx = frmSQLStatements.SetColumnsToEnterData(sColumnsToEnterData)

        frmSQLStatements.optAutomatic.Checked = True
        '
		frmSQLStatements.ShowDialog()
		'UPGRADE_NOTE: Object frmSQLStatements may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		frmSQLStatements = Nothing
		
		Start = True
		Exit Function
		
ErrorStart: 
		Start = False
		Err.Raise(Err.Number, "BabelSetup", Err.Description)
		
	End Function
    Friend Function UpdateQorder(ByRef bUp As Boolean, ByRef iNewIndex As Short) As String
        Dim oMyDalLocal As vbBabel.DAL
        Dim sErrorString As String
        Dim lRecordsAffected As Integer
        Dim sMySQL As String
        Dim sMatchType As String
        sErrorString = ""

        On Error GoTo ErrorUpdateQorder

        oMyDalLocal = New vbBabel.DAL
        oMyDalLocal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDalLocal.ConnectToDB() Then
            Throw New System.Exception(oMyDalLocal.ErrorMessage)
        End If

        If frmSQLStatements.optAutomatic.Checked = True Then
            sMatchType = "1"
        ElseIf frmSQLStatements.optManual.Checked = True Then
            sMatchType = "2"
        ElseIf frmSQLStatements.optAfter.Checked = True Then
            sMatchType = "3"
        ElseIf frmSQLStatements.optValidation.Checked = True Then
            sMatchType = "4"
        Else
            sMatchType = "5"
        End If

        'First set the selected rule to inewindex -1
        sMySQL = "UPDATE ERPQuery SET QOrder = -" & iNewIndex.ToString
        sMySQL = sMySQL & " WHERE Company_ID = " & oProfile.Company_ID.ToString
        sMySQL = sMySQL & " AND DBProfile_ID = " & iDBProfileID.ToString
        sMySQL = sMySQL & " AND MatchType = " & sMatchType
        If bUp Then
            sMySQL = sMySQL & " AND QOrder = " & Trim(Str(iNewIndex + 1))
        Else
            sMySQL = sMySQL & " AND QOrder = " & Trim(Str(iNewIndex - 1))
        End If

        oMyDalLocal.SQL = sMySQL
        If oMyDalLocal.ExecuteNonQuery Then
            System.Windows.Forms.Application.DoEvents()
            If oMyDalLocal.RecordsAffected > 0 Then
                'OK
            Else
                MsgBox(LRS(60411), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60406))
                'msgbox "An unexcpected error occured.Can 't save the rule
            End If
        Else
            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalLocal.ErrorMessage)
        End If

        'Then change the rule which now has the inewindex
        sMySQL = "UPDATE ERPQuery SET Qorder = "
        If bUp Then
            sMySQL = sMySQL & (iNewIndex + 1).ToString
        Else
            sMySQL = sMySQL & (iNewIndex - 1).ToString
        End If
        sMySQL = sMySQL & " WHERE Company_ID = " & oProfile.Company_ID.ToString
        sMySQL = sMySQL & " AND DBProfile_ID = " & Trim(Str(iDBProfileID))
        sMySQL = sMySQL & " AND MatchType = " & sMatchType
        sMySQL = sMySQL & " AND QOrder = " & iNewIndex.ToString

        oMyDalLocal.SQL = sMySQL
        If oMyDalLocal.ExecuteNonQuery Then
            System.Windows.Forms.Application.DoEvents()
            If oMyDalLocal.RecordsAffected > 0 Then
                'OK
            Else
                MsgBox(LRS(60411), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60406))
            End If
        Else
            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalLocal.ErrorMessage)
        End If

        'Then remove the - from the the selected rule
        sMySQL = "UPDATE ERPQuery SET QOrder = " & iNewIndex.ToString
        sMySQL = sMySQL & " WHERE Company_ID = " & oProfile.Company_ID.ToString
        sMySQL = sMySQL & " AND DBProfile_ID = " & iDBProfileID.ToString
        sMySQL = sMySQL & " AND MatchType = " & sMatchType
        sMySQL = sMySQL & " AND QOrder = -" & iNewIndex.ToString

        oMyDalLocal.SQL = sMySQL
        If oMyDalLocal.ExecuteNonQuery Then
            System.Windows.Forms.Application.DoEvents()
            If oMyDalLocal.RecordsAffected > 0 Then
                'OK
            Else
                MsgBox(LRS(60411), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60406))
                'msgbox "An unexcpected error occured.Can 't save the rule
            End If
        Else
            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalLocal.ErrorMessage)
        End If

        'The new queryArray is retrieved in the form by calling the optAutomatick_click etc .. events

        If Not oMyDalLocal Is Nothing Then
            oMyDalLocal.Close()
            oMyDalLocal = Nothing
        End If

        UpdateQorder = ""
        Exit Function

ErrorUpdateQorder:
        If Not oMyDalLocal Is Nothing Then
            oMyDalLocal.Close()
            oMyDalLocal = Nothing
        End If

        MsgBox(LRS(60411) & vbCrLf & Err.Description, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60406))
        'msgbox "An unexcpected error occured.  & err.description Can 't save the rule

    End Function

    Friend Function SaveMatchingRule(ByRef bNewRule As Boolean) As Boolean
        Dim oMyDalLocal As vbBabel.DAL
        Dim sErrorString As String
        Dim lRecordsAffected As Integer
        Dim iNextERPQueryID As Short
        Dim sMySQL As String
        Dim sQuery As String
        Dim sQuery9, sQuery7, sQuery5, sQuery3, sQuery1, sQuery2, sQuery4, sQuery6, sQuery8, sQuery10 As String
        Dim sQuery11, sQuery12 As String, sQuery13 As String
        Dim lCounter As Long = 0
        Dim lLength As Long = 0

        sErrorString = ""

        On Error GoTo ErrorSaveMatchingRule

        sQuery = frmSQLStatements.txtWriSQL.Text

        'Replace TAB, CRLF etc...
        sQuery = Replace(sQuery, Chr(8), "")
        sQuery = Replace(sQuery, Chr(9), "")
        'sQuery = Replace(sQuery, Chr(10), "")
        ' 30.10.2014 replace LF with a space. JANP has spent hours figuring out why Selects have been wrong when they are pasted and have crlf. try this way instead;
        sQuery = Replace(sQuery, Chr(10), " ")
        sQuery = Replace(sQuery, Chr(13), "")

        sQuery = Replace(BBEncrypt(sQuery, 3), "'", "@?@")

        If Len(sQuery) > 255 Then
            If Len(sQuery) > 510 Then
                If Len(sQuery) > 765 Then
                    If Len(sQuery) > 1020 Then
                        If Len(sQuery) > 1275 Then
                            If Len(sQuery) > 1530 Then
                                If Len(sQuery) > 1785 Then
                                    If Len(sQuery) > 2040 Then
                                        If Len(sQuery) > 2295 Then
                                            If Len(sQuery) > 2550 Then
                                                If Len(sQuery) > 2805 Then
                                                    If Len(sQuery) > 3060 Then
                                                        If Len(sQuery) > 3315 Then 'XokNET - 08.05.2012 - Added this IF
                                                            'MsgBox(LRS(60405, Trim$(Str(Len(sQuery))), "3315"), vbOKOnly + vbCritical, LRS(60406))
                                                            'Exit Function
                                                        Else
                                                            sQuery1 = Left$(sQuery, 255)
                                                            sQuery2 = Mid$(sQuery, 256, 255)
                                                            sQuery3 = Mid$(sQuery, 511, 255)
                                                            sQuery4 = Mid$(sQuery, 766, 255)
                                                            sQuery5 = Mid$(sQuery, 1021, 255)
                                                            sQuery6 = Mid$(sQuery, 1276, 255)
                                                            sQuery7 = Mid$(sQuery, 1531, 255)
                                                            sQuery8 = Mid$(sQuery, 1786, 255)
                                                            sQuery9 = Mid$(sQuery, 2041, 255)
                                                            sQuery10 = Mid$(sQuery, 2296, 255)
                                                            sQuery11 = Mid$(sQuery, 2551, 255)
                                                            sQuery12 = Mid$(sQuery, 2806, 255)
                                                            'XokNET - 08.05.2012 - Added Query13
                                                            sQuery13 = Mid$(sQuery, 3061)
                                                        End If

                                                    Else
                                                        sQuery1 = Left(sQuery, 255)
                                                        sQuery2 = Mid(sQuery, 256, 255)
                                                        sQuery3 = Mid(sQuery, 511, 255)
                                                        sQuery4 = Mid(sQuery, 766, 255)
                                                        sQuery5 = Mid(sQuery, 1021, 255)
                                                        sQuery6 = Mid(sQuery, 1276, 255)
                                                        sQuery7 = Mid(sQuery, 1531, 255)
                                                        sQuery8 = Mid(sQuery, 1786, 255)
                                                        sQuery9 = Mid(sQuery, 2041, 255)
                                                        sQuery10 = Mid(sQuery, 2296, 255)
                                                        sQuery11 = Mid(sQuery, 2551, 255)
                                                        sQuery12 = Mid(sQuery, 2806)
                                                        sQuery13 = vbNullString
                                                    End If
                                                Else
                                                    sQuery1 = Left(sQuery, 255)
                                                    sQuery2 = Mid(sQuery, 256, 255)
                                                    sQuery3 = Mid(sQuery, 511, 255)
                                                    sQuery4 = Mid(sQuery, 766, 255)
                                                    sQuery5 = Mid(sQuery, 1021, 255)
                                                    sQuery6 = Mid(sQuery, 1276, 255)
                                                    sQuery7 = Mid(sQuery, 1531, 255)
                                                    sQuery8 = Mid(sQuery, 1786, 255)
                                                    sQuery9 = Mid(sQuery, 2041, 255)
                                                    sQuery10 = Mid(sQuery, 2296, 255)
                                                    sQuery11 = Mid(sQuery, 2551)
                                                    sQuery12 = ""
                                                    sQuery13 = vbNullString
                                                End If
                                            Else
                                                sQuery1 = Left(sQuery, 255)
                                                sQuery2 = Mid(sQuery, 256, 255)
                                                sQuery3 = Mid(sQuery, 511, 255)
                                                sQuery4 = Mid(sQuery, 766, 255)
                                                sQuery5 = Mid(sQuery, 1021, 255)
                                                sQuery6 = Mid(sQuery, 1276, 255)
                                                sQuery7 = Mid(sQuery, 1531, 255)
                                                sQuery8 = Mid(sQuery, 1786, 255)
                                                sQuery9 = Mid(sQuery, 2041, 255)
                                                sQuery10 = Mid(sQuery, 2296)
                                                sQuery11 = ""
                                                sQuery12 = ""
                                                sQuery13 = vbNullString
                                            End If

                                        Else
                                            sQuery1 = Left(sQuery, 255)
                                            sQuery2 = Mid(sQuery, 256, 255)
                                            sQuery3 = Mid(sQuery, 511, 255)
                                            sQuery4 = Mid(sQuery, 766, 255)
                                            sQuery5 = Mid(sQuery, 1021, 255)
                                            sQuery6 = Mid(sQuery, 1276, 255)
                                            sQuery7 = Mid(sQuery, 1531, 255)
                                            sQuery8 = Mid(sQuery, 1786, 255)
                                            sQuery9 = Mid(sQuery, 2041)
                                            sQuery10 = ""
                                            sQuery11 = ""
                                            sQuery12 = ""
                                            sQuery13 = vbNullString
                                        End If
                                    Else
                                        sQuery1 = Left(sQuery, 255)
                                        sQuery2 = Mid(sQuery, 256, 255)
                                        sQuery3 = Mid(sQuery, 511, 255)
                                        sQuery4 = Mid(sQuery, 766, 255)
                                        sQuery5 = Mid(sQuery, 1021, 255)
                                        sQuery6 = Mid(sQuery, 1276, 255)
                                        sQuery7 = Mid(sQuery, 1531, 255)
                                        sQuery8 = Mid(sQuery, 1786)
                                        sQuery9 = ""
                                        sQuery10 = ""
                                        sQuery11 = ""
                                        sQuery12 = ""
                                        sQuery13 = vbNullString
                                    End If
                                Else
                                    sQuery1 = Left(sQuery, 255)
                                    sQuery2 = Mid(sQuery, 256, 255)
                                    sQuery3 = Mid(sQuery, 511, 255)
                                    sQuery4 = Mid(sQuery, 766, 255)
                                    sQuery5 = Mid(sQuery, 1021, 255)
                                    sQuery6 = Mid(sQuery, 1276, 255)
                                    sQuery7 = Mid(sQuery, 1531)
                                    sQuery8 = ""
                                    sQuery9 = ""
                                    sQuery10 = ""
                                    sQuery11 = ""
                                    sQuery12 = ""
                                    sQuery13 = vbNullString
                                End If
                            Else
                                sQuery1 = Left(sQuery, 255)
                                sQuery2 = Mid(sQuery, 256, 255)
                                sQuery3 = Mid(sQuery, 511, 255)
                                sQuery4 = Mid(sQuery, 766, 255)
                                sQuery5 = Mid(sQuery, 1021, 255)
                                sQuery6 = Mid(sQuery, 1276)
                                sQuery7 = ""
                                sQuery8 = ""
                                sQuery9 = ""
                                sQuery10 = ""
                                sQuery11 = ""
                                sQuery12 = ""
                                sQuery13 = vbNullString
                            End If
                        Else
                            sQuery1 = Left(sQuery, 255)
                            sQuery2 = Mid(sQuery, 256, 255)
                            sQuery3 = Mid(sQuery, 511, 255)
                            sQuery4 = Mid(sQuery, 766, 255)
                            sQuery5 = Mid(sQuery, 1021)
                            sQuery6 = ""
                            sQuery7 = ""
                            sQuery8 = ""
                            sQuery9 = ""
                            sQuery10 = ""
                            sQuery11 = ""
                            sQuery12 = ""
                            sQuery13 = vbNullString
                        End If
                    Else
                        sQuery1 = Left(sQuery, 255)
                        sQuery2 = Mid(sQuery, 256, 255)
                        sQuery3 = Mid(sQuery, 511, 255)
                        sQuery4 = Mid(sQuery, 766)
                        sQuery5 = ""
                        sQuery6 = ""
                        sQuery7 = ""
                        sQuery8 = ""
                        sQuery9 = ""
                        sQuery10 = ""
                        sQuery11 = ""
                        sQuery12 = ""
                        sQuery13 = vbNullString
                    End If
                Else
                    sQuery1 = Left(sQuery, 255)
                    sQuery2 = Mid(sQuery, 256, 255)
                    sQuery3 = Mid(sQuery, 511)
                    sQuery4 = ""
                    sQuery5 = ""
                    sQuery6 = ""
                    sQuery7 = ""
                    sQuery8 = ""
                    sQuery9 = ""
                    sQuery10 = ""
                    sQuery11 = ""
                    sQuery12 = ""
                    sQuery13 = vbNullString
                End If
            Else
                sQuery1 = Left(sQuery, 255)
                sQuery2 = Mid(sQuery, 256)
                sQuery3 = ""
                sQuery4 = ""
                sQuery5 = ""
                sQuery6 = ""
                sQuery7 = ""
                sQuery8 = ""
                sQuery9 = ""
                sQuery10 = ""
                sQuery11 = ""
                sQuery12 = ""
                sQuery13 = vbNullString
            End If
        Else
            sQuery1 = sQuery
            sQuery2 = ""
            sQuery3 = ""
            sQuery4 = ""
            sQuery5 = ""
            sQuery6 = ""
            sQuery7 = ""
            sQuery8 = ""
            sQuery9 = ""
            sQuery10 = ""
            sQuery11 = ""
            sQuery12 = ""
            sQuery13 = vbNullString
        End If

        If Len(frmSQLStatements.txtRuleName.Text) = 0 Then
            MsgBox(LRS(60407), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation, LRS(60406))
            'MsgBox "The field 'Name' is mandatory"
            frmSQLStatements.txtRuleName.Focus()
            SaveMatchingRule = False
            Exit Function
        End If
        If Len(frmSQLStatements.txtRuleType.Text) = 0 Then
            ' added 02.04.07 by JanP ! To be able to add Manual Seeks without cheating in Access
            If frmSQLStatements.GetMatchType = 2 Then
                frmSQLStatements.txtRuleType.Text = "2"
            Else
                If frmSQLStatements.GetMatchType = 4 Or frmSQLStatements.GetMatchType = 5 Then 'optValidation or optOther
                    frmSQLStatements.txtRuleType.Text = "0"
                Else
                    MsgBox(LRS(60408), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation, LRS(60406))
                    'MsgBox "The field 'Type' is mandatory"
                    frmSQLStatements.txtRuleType.Focus()
                    SaveMatchingRule = False
                    Exit Function
                End If
            End If 'frmSQLStatements.optManual.Value = True Then
        End If
        If EmptyString(frmSQLStatements.txtRuleOrder.Text) Then
            MsgBox(LRS(60628), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRS(60406))
            'MsgBox "The field 'Name' is mandatory"
            frmSQLStatements.txtRuleName.Focus()
            SaveMatchingRule = False
            Exit Function
        End If
        If frmSQLStatements.txtRuleType.Text = "1" Or frmSQLStatements.txtRuleType.Text = "101" Then
            'Don't test for manual rules
            'New code 12.05.2006 - Next line changed - not tested
            ' This is wrong when a new optButton is clicked then the .optManual is false (the new one true)
            ' even though it is a manual rule to be saved.
            If frmSQLStatements.GetMatchType = 1 Then 'optAutomatic  13.06.06: Changed from 2 to 1
                'Old code
                'If frmSQLStatements.optManual.Value = False Then
                'A Pattern must be selected
                If frmSQLStatements.cmbPatterns.SelectedIndex < 1 Then
                    'If EmptyString(frmSQLStatements.cmbPatterns.SelectedText) Then
                    MsgBox(LRS(60409), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRS(60406))
                    'MsgBox "When the query is of type 1, you should link the rule with an pattern." vbcrlf vbdrlf "You can add a new pattern in the client-setup.
                    SaveMatchingRule = False
                    Exit Function
                End If
            End If
        End If
        If frmSQLStatements.txtRuleType.Text = "2" Then
            If frmSQLStatements.GetMatchType = 1 Then 'optAutomatic  13.06.06: Changed from 2 to 1
                '25.07.2013 - Check that user has filled in a value in the field DescGroup - How many items to deduct
                If frmSQLStatements.txtSummarize.Text = "" Then
                    MsgBox(LRS(60621), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRS(60406))
                    'MsgBox "When the query is of type 2, you have to enter the number of invoices to deduct from the calculation." 
                    'frmSQLStatements.txtSummarize.Text.
                    SaveMatchingRule = False
                    Exit Function
                End If
            End If
        End If
        If frmSQLStatements.GetMatchType = 4 Then 'optValidation
            'A Field must be selected
            If frmSQLStatements.cmbPatterns.SelectedIndex < 1 Or frmSQLStatements.cmbPatterns.Text = LRS(60467) Then
                MsgBox(LRS(60466), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRS(60406))
                'MsgBox "When the query is of type 1, you should link the rule with an pattern." vbcrlf vbdrlf "You can add a new pattern in the client-setup.
                SaveMatchingRule = False
                Exit Function
            End If
        End If
        If frmSQLStatements.txtRuleType.Text = "3" Then
            If Len(frmSQLStatements.txtRuleSpecial.Text) = 0 Then
                MsgBox(LRS(60410), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRS(60406))
                'MsgBox "When the query is of type 3, you should add an unique name to the rule according to an agreement with your dealer."
                SaveMatchingRule = False
                Exit Function
            End If
        End If

        oMyDalLocal = New vbBabel.DAL
        oMyDalLocal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDalLocal.ConnectToDB() Then
            Throw New System.Exception(oMyDalLocal.ErrorMessage)
        End If

        If bNewRule Then
            'Find the next ERPQueryID
            sMySQL = "SELECT MAX(ERPQuery_ID) AS MaxID FROM ERPQuery WHERE Company_ID = " & oProfile.Company_ID.ToString
            sMySQL = sMySQL & " AND DBProfile_ID = " & iDBProfileID.ToString

            oMyDalLocal.SQL = sMySQL
            If oMyDalLocal.Reader_Execute() Then
                Do While oMyDalLocal.Reader_ReadRecord
                    iNextERPQueryID = CShort(oMyDalLocal.Reader_GetString("MaxID")) + 1
                Loop
            Else
                Throw New Exception(LRSCommon(45002) & oMyDalLocal.ErrorMessage)
            End If

            If 1 = 2 Then 'RunTime() Then 'MANDARIN
                sMySQL = "INSERT INTO ERPQuery(Company_ID, DBProfile_ID, ERPQuery_ID, Name, "
                sMySQL = sMySQL & "MatchType, DescGroup, Qorder, Type, SpecialMatch, Query1, Query2, Query3, Query4, Query5, Query6, Query7, Query8, Query9, Query10, Query11, Query12, Query13, UseAdjustment, ProposeMatch, IncludeOCR, UseAdjustmentFinal, CustomerNoSearch ) "
                sMySQL = sMySQL & "VALUES(" & oProfile.Company_ID.ToString & ", " & iDBProfileID.ToString
                sMySQL = sMySQL & ", " & iNextERPQueryID.ToString & ", '" & frmSQLStatements.txtRuleName.Text
                sMySQL = sMySQL & "', " & (frmSQLStatements.GetMatchType).ToString
                'Check the type of rule to find what info to store
                If frmSQLStatements.GetMatchType = 4 Then 'optValidation
                    'Old code
                    'sMySQL = sMySQL & ", DescGroup = " & Trim$(Str(frmSQLStatements.GetPatternIndex))
                    'New code 19.03.2007
                    sMySQL = sMySQL & ", " & Trim(Str(frmSQLStatements.GetPatternIndex))
                Else
                    If Trim(frmSQLStatements.txtRuleType.Text) = "2" Then
                        ' 02.04.2007 More kukk when adding new Manual Matching rule
                        ' Added next test;
                        If EmptyString((frmSQLStatements.txtSummarize.Text)) Then
                            sMySQL = sMySQL & ", 0"
                        Else
                            ' original code before 02.04.2007
                            sMySQL = sMySQL & ", " & Trim(frmSQLStatements.txtSummarize.Text)
                        End If
                    Else
                        sMySQL = sMySQL & ", " & Trim(Str(frmSQLStatements.GetPatternIndex))
                    End If
                End If
                '    If Len(frmSQLStatements.txtRuleDescription.Text) = 0 Then
                '        sMySQL = sMySQL & ", 0"
                '    Else
                '        sMySQL = sMySQL & ", " & frmSQLStatements.txtRuleDescription.Text
                '    End If
                sMySQL = sMySQL & ", " & frmSQLStatements.txtRuleOrder.Text
                sMySQL = sMySQL & ", " & frmSQLStatements.txtRuleType.Text & ", '" & frmSQLStatements.txtRuleSpecial.Text
                sMySQL = sMySQL & "', '" & sQuery1 & "', '" & sQuery2
                sMySQL = sMySQL & "', '" & sQuery3 & "', '" & sQuery4 & "', '" & sQuery5 & "', '" & sQuery6 & "', '" & sQuery7 & "', '" & sQuery8 & "', '" & sQuery9 & "', '" & sQuery10 & "', '" & sQuery11 & "', '" & sQuery12 & "', '" & sQuery13 & "', "
                If frmSQLStatements.chkUseAdjustments.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & Trim(Str(-1)) & ", "
                Else
                    sMySQL = sMySQL & Trim(Str(0)) & ", "
                End If
                If frmSQLStatements.chkProposeMatch.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & Trim(Str(-1)) & ", "
                Else
                    sMySQL = sMySQL & Trim(Str(0)) & ", "
                End If
                If frmSQLStatements.chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & Trim(Str(-1)) & ", "
                Else
                    sMySQL = sMySQL & Trim(Str(0)) & ", "
                End If
                If frmSQLStatements.chkUseAdjustmentsFinal.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & Trim(Str(-1)) & ", "
                Else
                    sMySQL = sMySQL & Trim(Str(0)) & ", "
                End If
                If frmSQLStatements.chkCustomerNo.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & Trim(Str(-1)) & ")"
                Else
                    sMySQL = sMySQL & Trim(Str(0)) & ")"
                End If

            Else
                sMySQL = "INSERT INTO ERPQuery(Company_ID, DBProfile_ID, ERPQuery_ID, Name, "
                sMySQL = sMySQL & "MatchType, DescGroup, Qorder, Type, SpecialMatch, UseAdjustment, ProposeMatch, UseAdjustmentFinal, CustomerNoSearch, IncludeOCR, Query1, Query2, Query3) "
                sMySQL = sMySQL & "VALUES(" & oProfile.Company_ID.ToString & ", " & iDBProfileID.ToString
                sMySQL = sMySQL & ", " & iNextERPQueryID.ToString & ", '" & frmSQLStatements.txtRuleName.Text
                sMySQL = sMySQL & "', " & (frmSQLStatements.GetMatchType).ToString
                'Check the type of rule to find what info to store
                If frmSQLStatements.GetMatchType = 4 Then 'optValidation
                    'Old code
                    'sMySQL = sMySQL & ", DescGroup = " & Trim$(Str(frmSQLStatements.GetPatternIndex))
                    'New code 19.03.2007
                    sMySQL = sMySQL & ", " & Trim(Str(frmSQLStatements.GetPatternIndex))
                Else
                    If Trim(frmSQLStatements.txtRuleType.Text) = "2" Then
                        ' 02.04.2007 More kukk when adding new Manual Matching rule
                        ' Added next test;
                        If EmptyString((frmSQLStatements.txtSummarize.Text)) Then
                            sMySQL = sMySQL & ", 0"
                        Else
                            ' original code before 02.04.2007
                            sMySQL = sMySQL & ", " & Trim(frmSQLStatements.txtSummarize.Text)
                        End If
                    Else
                        sMySQL = sMySQL & ", " & Trim(Str(frmSQLStatements.GetPatternIndex))
                    End If
                End If
                '    If Len(frmSQLStatements.txtRuleDescription.Text) = 0 Then
                '        sMySQL = sMySQL & ", 0"
                '    Else
                '        sMySQL = sMySQL & ", " & frmSQLStatements.txtRuleDescription.Text
                '    End If
                sMySQL = sMySQL & ", " & frmSQLStatements.txtRuleOrder.Text
                sMySQL = sMySQL & ", " & frmSQLStatements.txtRuleType.Text & ", '" & frmSQLStatements.txtRuleSpecial.Text & "', "
                'sMySQL = sMySQL & "', '" & sQuery1 & "', '" & sQuery2
                'sMySQL = sMySQL & "', '" & sQuery3 & "', '" & sQuery4 & "', '" & sQuery5 & "', '" & sQuery6 & "', '" & sQuery7 & "', '" & sQuery8 & "', '" & sQuery9 & "', '" & sQuery10 & "', '" & sQuery11 & "', '" & sQuery12 & "', '" & sQuery13 & "', "
                If frmSQLStatements.chkUseAdjustments.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & Trim(Str(-1)) & ", "
                Else
                    sMySQL = sMySQL & Trim(Str(0)) & ", "
                End If
                If frmSQLStatements.chkProposeMatch.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & Trim(Str(-1)) & ", "
                Else
                    sMySQL = sMySQL & Trim(Str(0)) & ", "
                End If
                If frmSQLStatements.chkUseAdjustmentsFinal.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & Trim(Str(-1)) & ", "
                Else
                    sMySQL = sMySQL & Trim(Str(0)) & ", "
                End If
                If frmSQLStatements.chkCustomerNo.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & Trim(Str(-1)) & ", "
                Else
                    sMySQL = sMySQL & Trim(Str(0)) & ", "
                End If
                If frmSQLStatements.chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & Trim(Str(-1))
                Else
                    sMySQL = sMySQL & Trim(Str(0))
                End If
                sMySQL = sMySQL & ", '', '', '')"
            End If
        Else
            If 1 = 2 Then 'RunTime() Then 'MANDARIN
                sMySQL = "UPDATE ERPQuery SET Name = '" & frmSQLStatements.txtRuleName.Text
                sMySQL = sMySQL & "', MatchType = " & (frmSQLStatements.GetMatchType).ToString
                If frmSQLStatements.GetMatchType = 1 Then
                    If Trim(frmSQLStatements.txtRuleType.Text) = "2" Then
                        sMySQL = sMySQL & ", DescGroup = " & Trim(frmSQLStatements.txtSummarize.Text)
                    Else
                        sMySQL = sMySQL & ", DescGroup = " & Trim(Str(frmSQLStatements.GetPatternIndex))
                    End If
                Else
                    If frmSQLStatements.GetMatchType = 4 Then 'optValidation
                        sMySQL = sMySQL & ", DescGroup = " & (frmSQLStatements.GetPatternIndex).ToString
                    Else
                        'No use of descgroup inless it is an automatic matching rule
                        sMySQL = sMySQL & ", DescGroup = 0 "
                    End If
                End If
                sMySQL = sMySQL & ", Qorder = " & frmSQLStatements.txtRuleOrder.Text
                sMySQL = sMySQL & ", Type = " & frmSQLStatements.txtRuleType.Text
                sMySQL = sMySQL & ", SpecialMatch = '" & frmSQLStatements.txtRuleSpecial.Text
                sMySQL = sMySQL & "', Query1 = '" & sQuery1
                sMySQL = sMySQL & "', Query2 = '" & sQuery2
                sMySQL = sMySQL & "', Query3 = '" & sQuery3
                sMySQL = sMySQL & "', Query4 = '" & sQuery4
                sMySQL = sMySQL & "', Query5 = '" & sQuery5
                sMySQL = sMySQL & "', Query6 = '" & sQuery6
                sMySQL = sMySQL & "', Query7 = '" & sQuery7
                sMySQL = sMySQL & "', Query8 = '" & sQuery8
                sMySQL = sMySQL & "', Query9 = '" & sQuery9
                sMySQL = sMySQL & "', Query10 = '" & sQuery10
                sMySQL = sMySQL & "', Query11 = '" & sQuery11
                sMySQL = sMySQL & "', Query12 = '" & sQuery12
                'XokNET - 08.05.2012 - Added Query13
                sMySQL = sMySQL & "', Query13 = '" & sQuery13
                If frmSQLStatements.chkUseAdjustments.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & "', UseAdjustment = " & Trim(Str(-1))
                Else
                    sMySQL = sMySQL & "', UseAdjustment = " & Trim(Str(0))
                End If
                If frmSQLStatements.chkUseAdjustmentsFinal.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & "', UseAdjustmentFinal = " & Trim(Str(-1))
                Else
                    sMySQL = sMySQL & "', UseAdjustmentFinal = " & Trim(Str(0))
                End If
                If frmSQLStatements.chkCustomerNo.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & "', CustomerNoSearch = " & Trim(Str(-1))
                Else
                    sMySQL = sMySQL & "', CustomerNoSearch = " & Trim(Str(0))
                End If
                If frmSQLStatements.chkProposeMatch.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & ", ProposeMatch = " & Trim(Str(-1))
                Else
                    sMySQL = sMySQL & ", ProposeMatch = " & Trim(Str(0))
                End If
                If frmSQLStatements.chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & ", IncludeOCR = " & Trim(Str(-1))
                Else
                    sMySQL = sMySQL & ", IncludeOCR = " & Trim(Str(0))
                End If
                sMySQL = sMySQL & " WHERE Company_ID = " & Trim(Str(oProfile.Company_ID))
                sMySQL = sMySQL & " AND DBProfile_ID = " & Trim(Str(iDBProfileID))
                sMySQL = sMySQL & " AND ERPQuery_ID = " & frmSQLStatements.txtRuleERPIndex.Text
            Else
                sMySQL = "UPDATE ERPQuery SET Name = '" & frmSQLStatements.txtRuleName.Text
                sMySQL = sMySQL & "', MatchType = " & (frmSQLStatements.GetMatchType).ToString
                If frmSQLStatements.GetMatchType = 1 Or frmSQLStatements.GetMatchType = 101 Then
                    If Trim(frmSQLStatements.txtRuleType.Text) = "2" Then
                        sMySQL = sMySQL & ", DescGroup = " & Trim(frmSQLStatements.txtSummarize.Text)
                    Else
                        sMySQL = sMySQL & ", DescGroup = " & Trim(Str(frmSQLStatements.GetPatternIndex))
                    End If
                Else
                    If frmSQLStatements.GetMatchType = 4 Then 'optValidation
                        sMySQL = sMySQL & ", DescGroup = " & (frmSQLStatements.GetPatternIndex).ToString
                    Else
                        'No use of descgroup inless it is an automatic matching rule
                        sMySQL = sMySQL & ", DescGroup = 0 "
                    End If
                End If
                sMySQL = sMySQL & ", Qorder = " & frmSQLStatements.txtRuleOrder.Text
                sMySQL = sMySQL & ", Type = " & frmSQLStatements.txtRuleType.Text
                sMySQL = sMySQL & ", SpecialMatch = '" & frmSQLStatements.txtRuleSpecial.Text
                If frmSQLStatements.chkUseAdjustments.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & "', UseAdjustment = " & Trim(Str(-1))
                Else
                    sMySQL = sMySQL & "', UseAdjustment = " & Trim(Str(0))
                End If
                If frmSQLStatements.chkUseAdjustmentsFinal.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & ", UseAdjustmentFinal = " & Trim(Str(-1))
                Else
                    sMySQL = sMySQL & ", UseAdjustmentFinal = " & Trim(Str(0))
                End If
                If frmSQLStatements.chkCustomerNo.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & ", CustomerNoSearch = " & Trim(Str(-1))
                Else
                    sMySQL = sMySQL & ", CustomerNoSearch = " & Trim(Str(0))
                End If
                If frmSQLStatements.chkProposeMatch.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & ", ProposeMatch = " & Trim(Str(-1))
                Else
                    sMySQL = sMySQL & ", ProposeMatch = " & Trim(Str(0))
                End If
                If frmSQLStatements.chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked Then
                    sMySQL = sMySQL & ", IncludeOCR = " & Trim(Str(-1))
                Else
                    sMySQL = sMySQL & ", IncludeOCR = " & Trim(Str(0))
                End If
                sMySQL = sMySQL & " WHERE Company_ID = " & Trim(Str(oProfile.Company_ID))
                sMySQL = sMySQL & " AND DBProfile_ID = " & Trim(Str(iDBProfileID))
                sMySQL = sMySQL & " AND ERPQuery_ID = " & frmSQLStatements.txtRuleERPIndex.Text

            End If
        End If

        oMyDalLocal.SQL = sMySQL
        If oMyDalLocal.ExecuteNonQuery Then
            System.Windows.Forms.Application.DoEvents()
            If oMyDalLocal.RecordsAffected > 0 Then
                'OK
            Else
                MsgBox(LRS(60411), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60406))
                'msgbox "An unexcpected error occured.Can 't save the rule
            End If
        Else
            Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalLocal.ErrorMessage)
        End If

        If 1 = 1 Then 'Not RunTime() Then
            'MANDARIN - Update the ERPQueryLine table

            'First delete from ERPQueryLine. The new rule will be inserted below
            If Not bNewRule Then
                sMySQL = "DELETE FROM ERPQueryLine"
                sMySQL = sMySQL & " WHERE Company_ID = " & oProfile.Company_ID.ToString
                sMySQL = sMySQL & " AND DBProfile_ID = " & iDBProfileID.ToString
                sMySQL = sMySQL & " AND ERPQuery_ID = " & frmSQLStatements.txtRuleERPIndex.Text
                oMyDalLocal.SQL = sMySQL
                If oMyDalLocal.ExecuteNonQuery Then
                    System.Windows.Forms.Application.DoEvents()
                    If oMyDalLocal.RecordsAffected > 0 Then
                        'OK
                    Else
                        MsgBox(LRS(60411), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60406))
                        'msgbox "An unexcpected error occured.Can 't save the rule
                    End If
                Else
                    Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalLocal.ErrorMessage)
                End If
                iNextERPQueryID = CInt(frmSQLStatements.txtRuleERPIndex.Text)
            End If
            'Then insert the SQL to the table ERPQueryLine.
            sMySQL = "INSERT INTO ERPQueryLine (Company_ID, DBProfile_ID, ERPQuery_ID, ERPQueryLine_ID, QueryLine) VALUES("
            lCounter = 0
            If Not sQuery Is Nothing Then
                Do While sQuery.Length > 0
                    lCounter = lCounter + 1
                    lLength = sQuery.Length
                    If lLength > 255 Then
                        lLength = 255
                    Else
                        lLength = lLength
                    End If
                    oMyDalLocal.SQL = sMySQL & oProfile.Company_ID.ToString & ", " & iDBProfileID.ToString & ", " & iNextERPQueryID.ToString & ", " & lCounter.ToString & ", '" & sQuery.Substring(0, CInt(lLength)) & "')"
                    If oMyDalLocal.ExecuteNonQuery Then
                        System.Windows.Forms.Application.DoEvents()
                        If oMyDalLocal.RecordsAffected > 0 Then
                            'OK
                        Else
                            MsgBox(LRS(60411), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60406))
                            'msgbox "An unexcpected error occured.Can 't save the rule
                        End If
                    Else
                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDalLocal.ErrorMessage)
                    End If
                    If lLength = sQuery.Length Then
                        sQuery = String.Empty
                    Else
                        sQuery = sQuery.Substring(CInt(lLength))
                    End If
                Loop
            End If
        End If


        SaveMatchingRule = True

        If Not oMyDalLocal Is Nothing Then
            oMyDalLocal.Close()
            oMyDalLocal = Nothing
        End If

        Exit Function

ErrorSaveMatchingRule:
        If Not oMyDalLocal Is Nothing Then
            oMyDalLocal.Close()
            oMyDalLocal = Nothing
        End If

        MsgBox(LRS(60411) & vbCrLf & Err.Description, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60406))
        'msgbox "An unexcpected error occured.  & err.description Can 't save the rule
        SaveMatchingRule = False

    End Function
    Friend Function ShowDatabaseForm(ByRef aLogOnInfo(,,) As String, ByRef bLogOn As Boolean) As Boolean
        Dim bx, bReturnValue As Boolean
        Dim lCounter As Integer
        Dim lAttempts As Integer
        Dim frmDatabase As New frmDatabase

        If bLogOn Then
            lAttempts = 3
        Else
            lAttempts = 1
        End If

        frmDatabase.Text = LRS(60162) 'Database
        frmDatabase.cmdCancel.Text = LRS(55002) '"&Avbryt"
        frmDatabase.cmdTest.Text = LRS(55010) '"&Test"
        frmDatabase.cmdUse.Text = LRS(55025) '"&Use"
        frmDatabase.lblConnectionString.Text = LRS(60159) 'Connectionstring
        frmDatabase.lblUserID.Text = LRS(60160) 'User ID
        frmDatabase.lblPassword.Text = LRS(60161) ' Password
        frmDatabase.SetCompanyID((oProfile.Company_ID))

        'Populate the possible providers
        frmDatabase.cmbProvider.Items.Insert(0, "OleDb")
        frmDatabase.cmbProvider.Items.Insert(1, "Odbc")
        frmDatabase.cmbProvider.Items.Insert(2, "OracleClient")
        frmDatabase.cmbProvider.Items.Insert(3, "SqlClient")
        frmDatabase.cmbProvider.Items.Insert(4, "SqlServerCe.3.5")
        frmDatabase.cmbProvider.Items.Insert(5, "WCF Service")

        'Populate the possible providers
        frmDatabase.cmbDatabaseType.Items.Insert(0, LRS(60548))
        frmDatabase.cmbDatabaseType.Items.Insert(1, "Microsoft Access")
        frmDatabase.cmbDatabaseType.Items.Insert(2, "Oracle")
        frmDatabase.cmbDatabaseType.Items.Insert(3, "Paradox")
        frmDatabase.cmbDatabaseType.Items.Insert(4, "Pervasive")
        frmDatabase.cmbDatabaseType.Items.Insert(5, "Progress")
        frmDatabase.cmbDatabaseType.Items.Insert(6, "SQL Server")

        'Check if any DB-profile exist
        If Array_IsEmpty(aLogOnInfo) Then
            bx = frmDatabase.SetDBProfileArray(aLogOnInfo)
            bReturnValue = False
            frmDatabase.cmbDatabase.Visible = False
            'frmDatabase.txtNewDatabase.Visible = True
            'Have to add info to the combobox, even if its hidden
            'frmDatabase.cmbDatabase.AddItem "Choose a Connection", 0
            'frmDatabase.cmbDatabase.AddItem aLogOnInfo(0, 4, 0), 1
            'If Not bLogOn Then
            '    frmDatabase.txtPassword.Text = aLogOnInfo(0, 2, 0) 'oProfile.ConPWD
            'bx = frmDatabase.SetLocalProfile(oProfile, True)
            'Else
            'frmDatabase.txtConnectionString.Enabled = False
            'frmDatabase.txtUserID.Enabled = False
            'bx = frmDatabase.SetLocalProfile(oProfile, False)
            'End If
            frmDatabase.ShowDialog()

            'Check if there are more than 1 DB-Profile
            'If so the user has to choose among them, if not connect to the one existing
        ElseIf UBound(aLogOnInfo, 1) > 0 Then
            bx = frmDatabase.SetDBProfileArray(aLogOnInfo)
            frmDatabase.lblProvider.Visible = False
            frmDatabase.cmbProvider.Visible = False
            frmDatabase.lblConnectionString.Visible = False
            frmDatabase.lblUserID.Visible = False
            frmDatabase.lblPassword.Visible = False
            frmDatabase.txtPassword.Visible = False
            frmDatabase.txtConnectionString.Visible = False
            frmDatabase.txtUserID.Visible = False
            frmDatabase.cmdOK.Enabled = False
            frmDatabase.cmdTest.Visible = False
            frmDatabase.cmdUse.Enabled = False
            frmDatabase.lblTestConnection.Visible = False
            frmDatabase.SetCompanyID((oProfile.Company_ID))
            frmDatabase.cmbDatabase.Items.Insert(0, "Choose a Connection")
            For lCounter = 0 To UBound(aLogOnInfo, 1)
                frmDatabase.cmbDatabase.Items.Insert(lCounter + 1, aLogOnInfo(lCounter, 4, 0))
            Next lCounter
            frmDatabase.cmbDatabase.SelectedIndex = 0
            frmDatabase.ShowDialog()
        Else
            bx = frmDatabase.SetDBProfileArray(aLogOnInfo)
            bReturnValue = False
            frmDatabase.txtPassword.SelectionStart = 0
            frmDatabase.txtPassword.SelectionLength = Len(frmDatabase.txtPassword.Text)
            frmDatabase.txtConnectionString.Text = aLogOnInfo(0, 0, 0) 'oProfile.ConString
            frmDatabase.txtUserID.Text = aLogOnInfo(0, 1, 0) 'oProfile.ConUID
            frmDatabase.cmbProvider.SelectedIndex = aLogOnInfo(0, 9, 0)
            frmDatabase.cmbDatabaseType.SelectedIndex = aLogOnInfo(0, 10, 0) 'Added 19.10.2017
            'Have to add info to the combobox, even if its hidden
            frmDatabase.cmbDatabase.Items.Insert(0, "Choose a Connection")
            frmDatabase.cmbDatabase.Items.Insert(1, aLogOnInfo(0, 4, 0))
            frmDatabase.cmbDatabase.SelectedIndex = 1
            'If Not bLogOn Then
            '    frmDatabase.txtPassword.Text = aLogOnInfo(0, 2, 0) 'oProfile.ConPWD
            'bx = frmDatabase.SetLocalProfile(oProfile, True)
            'Else
            'frmDatabase.txtConnectionString.Enabled = False
            'frmDatabase.txtUserID.Enabled = False
            'bx = frmDatabase.SetLocalProfile(oProfile, False)
            'End If
            frmDatabase.ShowDialog()
        End If

        If frmDatabase.PressedButton = -1 Then
            'User has cancelled the logon-procedure
            bReturnValue = False
        Else
            iDBProfileID = frmDatabase.cmbDatabase.SelectedIndex
            bReturnValue = ConnectERP(True, frmDatabase)


        End If

        frmDatabase.Close()
        'UPGRADE_NOTE: Object frmDatabase may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        frmDatabase = Nothing

        ShowDatabaseForm = bReturnValue

    End Function
    Friend Function ConnectERP(ByRef bLogOn As Boolean, ByVal frmDatabase As frmDatabase) As Boolean
        Dim bx, bReturnValue As Boolean
        Dim lCounter As Integer
        Dim lAttempts As Integer

        If bLogOn Then
            lAttempts = 3
        Else
            lAttempts = 1
        End If

        'Call frmSQLStatements.cmdWriConnection
        For lCounter = 1 To lAttempts

            bReturnValue = False
            '    frmDatabase.Caption = LRS(60162) 'Database
            '    frmDatabase.cmdCancel.Caption = LRS(55002) '"&Avbryt"
            '    frmDatabase.cmdTest.Caption = LRS(55010) '"&Test"
            '    frmDatabase.lblConnectionString = LRS(60159)  'Connectionstring
            '    frmDatabase.lblUserID = LRS(60160)  'User ID
            '    frmDatabase.lblPassword = LRS(60161)  ' Password
            '    frmDatabase.txtPassword.SelStart = 0
            '    frmDatabase.txtPassword.SelLength = Len(frmDatabase.txtPassword.Text)
            '    frmDatabase.txtConnectionString.Text = aLogOnInfo(0, 0, 0) 'oProfile.ConString
            '    frmDatabase.txtUserID.Text = aLogOnInfo(0, 1, 0) 'oProfile.ConUID
            '    If Not bLogOn Then
            '        frmDatabase.txtPassword.Text = aLogOnInfo(0, 2, 0) 'oProfile.ConPWD
            '        bx = frmDatabase.SetLocalProfile(oProfile, True)
            '    Else
            '        frmDatabase.txtConnectionString.Enabled = False
            '        frmDatabase.txtUserID.Enabled = False
            '        bx = frmDatabase.SetLocalProfile(oProfile, False)
            '    End If
            '
            If lCounter > 1 Then
                frmDatabase.ShowDialog()
            End If

            If frmDatabase.PressedButton = 1 Then 'OK


                'BABELERROR FIX: Rask l�sning for SI-data
                bx = False
                On Error Resume Next
                oMyERPDal = New vbBabel.DAL
                oMyERPDal.Connectionstring = frmDatabase.txtConnectionString.Text
                oMyERPDal.Provider = frmDatabase.cmbProvider.SelectedIndex
                oMyERPDal.UserID = frmDatabase.txtUserID.Text
                ' 16.12.2020 - added next If
                If EmptyString(frmDatabase.txtUserID.Text) Then
                    ' try to find UID in connectionstring
                    If InStr(frmDatabase.txtConnectionString.Text, "UID=") > 0 Then
                        Dim sTmp As String = ""
                        sTmp = Mid(frmDatabase.txtConnectionString.Text, InStr(frmDatabase.txtConnectionString.Text, "UID=") + 4)
                        sTmp = Left(sTmp, InStr(sTmp, ";") - 1)
                        oMyERPDal.UserID = sTmp
                    End If
                End If
                oMyERPDal.Password = frmDatabase.txtPassword.Text
                oMyERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                If Not oMyERPDal.ConnectToDB() Then
                    MsgBox("Can't connect to the database. Reason: " & vbCrLf & oMyERPDal.ErrorMessage & vbCrLf & vbCrLf & "Please try again.", MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation)
                Else
                    bReturnValue = True
                    Exit For
                End If

                '        Set conERPConnection = ConnectToERPDB(frmDatabase.txtConnectionString.Text, frmDatabase.txtUserID.Text, frmDatabase.txtPassword.Text, True, sErrorString, True)
                '
                '        If Len(sErrorString) = 0 Then
                '            If Not conERPConnection Is Nothing Then
                '                bReturnValue = True
                '                Exit For
                '            End If
                '        Else
                '            MsgBox "Can't connect to the database. Reason: " & vbCrLf & sErrorString & _
                ''                 vbCrLf & vbCrLf & "Please try again."
                '            'reset values
                '            sErrorString = ""
                '            'Unload frmDatabase
                '            'Set frmDatabase = Nothing
                '        End If
            Else 'Pressed Cancel
                bReturnValue = False
                Exit For
            End If
        Next lCounter

        If bReturnValue = False And lCounter = 4 Then
            MsgBox("Too many attempts to log on database." & vbCrLf & vbCrLf & "Choice abandoned.", MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation)
        End If

        frmDatabase.Close()
        frmDatabase.Dispose()

        ConnectERP = bReturnValue

    End Function
    Friend Function UpdateSQLInterpreted() As Boolean
        Dim sString As String
        Dim bx As Boolean

        With frmSQLStatements
            sString = .txtWriSQL.Text

            'BB_Amount
            sString = Replace(sString, "BB_Amount", .txtParaAmount.Text, , , CompareMethod.Text)
            'BB_InvoiceIdentifier
            sString = Replace(sString, "BB_InvoiceIdentifier", .txtParaInvoice.Text, , , CompareMethod.Text)
            'BB_CustomerNo
            sString = Replace(sString, "BB_CustomerNo", .txtParaCustomer.Text, , , CompareMethod.Text)
            'BB_Name
            sString = Replace(sString, "BB_Name", .txtParaName.Text, , , CompareMethod.Text)
            'BB_AccountNo
            sString = Replace(sString, "BB_AccountNo", .txtParaAccount.Text, , , CompareMethod.Text)
            'BB_ClientNo
            sString = Replace(sString, "BB_ClientNo", .txtParaClient.Text, , , CompareMethod.Text)
            'BB_Currency
            sString = Replace(sString, "BB_Currency", .txtParaCurrency.Text, , , CompareMethod.Text)
            'BB_Currency
            sString = Replace(sString, "BB_MyText", .txtParaMyText.Text, , , CompareMethod.Text)
            If Len(.txtParaDivLabel.Text) > 3 Then
                sString = Replace(sString, Trim(.txtParaDivLabel.Text), .txtParaDiv.Text, , , CompareMethod.Text)
            End If

            .txtIntSQL.Text = sString


        End With

    End Function
    Public Sub RTB_Color(ByVal rtb As RichTextBox, ByVal bDoUppercase As Boolean)
        Dim item As String

        '// Color text in RTB //
        Dim WordList As New System.Collections.Generic.List(Of String)
        Dim x, start, wrd As Integer
        Dim textColor As Color
        Dim AllWords() As System.Collections.Generic.List(Of String)

        Dim BlueWords As New System.Collections.Generic.List(Of String)
        Dim bfont As New Font(rtb.Font, FontStyle.Bold)
        Dim plainfont As New Font(rtb.Font, FontStyle.Regular)

        BlueWords.Add("SELECT ")
        BlueWords.Add(" WHERE ")
        BlueWords.Add(" GROUP BY")
        BlueWords.Add(" FROM ")
        BlueWords.Add("APPEND ")
        BlueWords.Add("UPDATE ")
        BlueWords.Add("DELETE ")
        BlueWords.Add("ORDER BY")
        BlueWords.Add(" UNION ")
        BlueWords.Add(" JOIN ")
        BlueWords.Add("INSERT INTO")
        BlueWords.Add("(")
        BlueWords.Add(")")
        BlueWords.Add(" AS ")
        BlueWords.Add(" CASE  ")
        BlueWords.Add(" WITH ")
        BlueWords.Add(" END ")
        BlueWords.Add(" WHEN ")
        BlueWords.Add(" ELSE ")
        BlueWords.Add(" THEN ")
        BlueWords.Add(" INNER ")
        BlueWords.Add(" OUTER ")

        Dim RedWords As New System.Collections.Generic.List(Of String)
        RedWords.Add("BB_MyText")
        RedWords.Add("BB_Amount")
        RedWords.Add("BB_CustomerNo")
        RedWords.Add("BB_ClientNo")
        RedWords.Add("BB_AccountNo")
        RedWords.Add("BB_MyAccountNo")
        RedWords.Add("BB_Currency")
        RedWords.Add("BB_Name")
        RedWords.Add("BB_ValueDate")
        RedWords.Add("BB_InvoiceIdentifier")
        RedWords.Add("BBRET_ClientNo")
        RedWords.Add("BBRET_MyField")
        RedWords.Add("BBRET_MyField2")
        RedWords.Add("BBRET_MyField3")
        RedWords.Add("BBRET_FreeText")
        RedWords.Add("BBRET_MatchID")
        RedWords.Add("BBRET_AKontoID")
        RedWords.Add("BBRET_GLID")
        RedWords.Add("BBRET_InvoiceNo")
        RedWords.Add("BBRET_Amount")
        RedWords.Add("BBRET_Currency")
        RedWords.Add("BBRET_CustomerNo")
        RedWords.Add("BBRET_Name")
        RedWords.Add("BBRET_Dim1")
        RedWords.Add("BBRET_Dim2")
        RedWords.Add("BBRET_Dim3")
        RedWords.Add("BBRET_Dim4")
        RedWords.Add("BBRET_Dim5")
        RedWords.Add("BBRET_Dim6")
        RedWords.Add("BBRET_Dim7")
        RedWords.Add("BBRET_Dim8")
        RedWords.Add("BBRET_Dim9")
        RedWords.Add("BBRET_Dim10")
        RedWords.Add("BBRET_Red")
        RedWords.Add("BBRET_Green")
        RedWords.Add("BBRET_Blue")
        RedWords.Add("BBRET_Yellow")

        ReDim AllWords(1) ' number of word lists loaded (zero based so 1 = 2 lists!)

        ' copy word lists to array list
        AllWords(0) = BlueWords
        AllWords(1) = RedWords

        ' First, set standard plain no bold, no color
        rtb.SelectionStart = 0
        rtb.SelectionLength = rtb.TextLength
        rtb.SelectionColor = rtb.ForeColor
        rtb.SelectionBackColor = rtb.BackColor
        rtb.SelectionFont = plainfont


        For Each item In BlueWords
            ' Gj�r om v�re SQL n�kkelord til store bokstaver;
            ' (f�r ikke .Find til � funke ellers, og det blir vel bra lesbart med store bokstaver ogs�)
            ' kom bort i et dustecase hvor et feltnavn har Case som en del av navnet, kan ikke da uppercase!!!
            rtb.Text = Replace(rtb.Text, item, item.ToUpper, , , CompareMethod.Text)
        Next item
        For Each item In RedWords
            ' Gj�r om v�re SQL n�kkelord til store bokstaver;
            ' (f�r ikke .Find til � funke ellers, og det blir vel bra lesbart med store bokstaver ogs�)
            ' Gj�r om til type BBRET_Amount
            rtb.Text = Replace(rtb.Text, item, item, , , CompareMethod.Text)
        Next item

        ' set word list & color
        For Each item In BlueWords

            textColor = Color.Green ' the color for these words
            ' find and color words ( MatchCase )
            For x = 1 To item.Length - 1 ' start at 1! , 0 = the color to use!
                Do Until wrd = -1
                    wrd = rtb.Find(item.ToUpper, start, RichTextBoxFinds.None)
                    If wrd <> -1 Then
                        rtb.SelectionColor = textColor
                        rtb.SelectionFont = bfont
                        start = wrd + 1
                    End If
                Loop
                wrd = 0
                start = 0
            Next

        Next

        For Each item In RedWords

            textColor = Color.Green ' the color for these words
            ' find and color words ( MatchCase )
            For x = 1 To item.Length - 1 ' start at 1! , 0 = the color to use!
                Do Until wrd = -1
                    wrd = rtb.Find(item.ToUpper, start, RichTextBoxFinds.None)
                    If wrd <> -1 Then
                        rtb.SelectionColor = textColor
                        'rtb.SelectionFont = bfont
                        start = wrd + 1
                    End If
                Loop
                wrd = 0
                start = 0
            Next

        Next

        'wrd = 0
        'start = 0
        'For Each item In RedWords
        '    textColor = Color.Red ' the color for these words
        '    ' find and color words ( MatchCase )
        '    'For x = 1 To item.Count - 1 ' start at 1! , 0 = the color to use!
        '    Do Until wrd = -1
        '        'wrd = rtb.Find(item, start, RichTextBoxFinds.None)
        '        ' Find er case-sensitive, s� da M� vi skrive helt likt. Bruk egen funksjon
        '        wrd = rtb.Text.IndexOf(item, start, Len(rtb.Text) - start, StringComparison.OrdinalIgnoreCase)
        '        If wrd <> -1 Then
        '            rtb.SelectionColor = textColor
        '            rtb.SelectionFont = bfont
        '            start = wrd + 1
        '        End If
        '    Loop
        '    wrd = 0
        '    start = 0
        '    'Next

        'Next

    End Sub
    Friend Function AddRestrictedNames(ByRef rtxtcontrol As System.Windows.Forms.RichTextBox, ByRef sText As String) As Boolean
        Dim iSelPos As Short

        With rtxtcontrol

            LockWindowUpdate(.Handle.ToInt32)

            'bReDraw = False
            iSelPos = .SelectionStart

            If iSelPos > 0 Then
                If Mid(.Text, iSelPos - 1, 1) <> " " Then
                    sText = " " & sText
                End If
            End If
            If Len(.Text) > iSelPos Then
                If Mid(.Text, iSelPos, 1) <> " " Then
                    sText = sText & " "
                End If
            End If

            .Text = Left(.Text, iSelPos - 1) & sText & Mid(.Text, iSelPos)

            .SelectionStart = iSelPos + Len(sText)

            LockWindowUpdate(0)

        End With

        AddRestrictedNames = True

    End Function

    Friend Function RunTheQuery() As Boolean
        'Dim rsQuery As adodb.Recordset
        Dim bx As Boolean
        Dim lItem, lRecord As Integer
        Dim iNoOfItems As Integer
        Dim aResultArray(,) As String
        Dim bStatus As Boolean
        Dim sHeadings As String

        Dim lRecordsAffected As Integer

        aResultArray = Nothing
        sHeadings = ""

        'Have to run different ways to update depending on the query.
        'If the query is an update, we want to see how many rows are affected by the update
        '  and presnt this result.
        'If it is an select, we want to retrieve a recordset, and present this result.

        bx = UpdateSQLInterpreted()


        On Error GoTo ErrorRunTheQuery

        If UCase(Left(frmSQLStatements.txtIntSQL.Text, 6)) = "UPDATE" Or UCase(Left(frmSQLStatements.txtIntSQL.Text, 6)) = "INSERT" Then

            oMyERPDal.SQL = frmSQLStatements.txtIntSQL.Text
            If oMyERPDal.ExecuteNonQuery Then
                ' 29.12.2015 JPS Rettet slik at vi ser hvor mange records som er oppdatert
                lRecordsAffected = oMyERPDal.RecordsAffected
                If lRecordsAffected > 0 Then
                    MsgBox("Antall oppdatert: " & Str(lRecordsAffected), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                Else
                    MsgBox("Ingen records oppdatert", MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyERPDal.ErrorMessage)
            End If

            ReDim aResultArray(0, 0)
            aResultArray(0, 0) = Trim(Str(lRecordsAffected))
            bStatus = ShowResultInGrid(1, "Records affected", aResultArray)

        Else

            oMyERPDal.SQL = frmSQLStatements.txtIntSQL.Text
            oMyERPDal.ShowErrorMessage = True

            If oMyERPDal.Reader_Execute() Then
                'If frmSQLStatements.txtIntSQL.Text.Contains("KJELL") Then
                '    MsgBox("DELOITTE: Kj�rt sp�rringen, ingen feil.")
                'End If
                If oMyERPDal.Reader_HasRows Then
                    'If frmSQLStatements.txtIntSQL.Text.Contains("KJELL") Then
                    '    MsgBox("DELOITTE: Recordsettet inneholder rader.")
                    'End If

                    iNoOfItems = oMyERPDal.Reader_FieldCount
                    'If frmSQLStatements.txtIntSQL.Text.Contains("KJELL") Then
                    '    MsgBox("DELOITTE: Recordsettet inneholder " & iNoOfItems.ToString & " kolonner.")
                    'End If
                    'iNoOfItems = rsQuery.Fields.Count
                    lRecord = -1
                    Do While oMyERPDal.Reader_ReadRecord
                        lRecord = lRecord + 1
                        ReDim Preserve aResultArray(iNoOfItems, lRecord)
                        For lItem = 0 To iNoOfItems
                            aResultArray(lItem, lRecord) = oMyERPDal.Reader_GetField(lItem)
                            'aResultArray(lItem, lRecord) = rsQuery.Fields.Item(lItem).Value
                            If lItem = 0 Then
                                sHeadings = oMyERPDal.Reader_GetFieldName(lItem)
                                'sHeadings = rsQuery.Fields.Item(lItem).Name
                            Else
                                sHeadings = sHeadings & "," & oMyERPDal.Reader_GetFieldName(lItem)
                                'sHeadings = sHeadings & "," & rsQuery.Fields.Item(lItem).Name
                            End If
                        Next lItem
                    Loop
                    'If frmSQLStatements.txtIntSQL.Text.Contains("KJELL") Then
                    '    MsgBox("DELOITTE: Har populert headingene.")
                    'End If

                    bStatus = ShowResultInGrid(iNoOfItems + 1, sHeadings, aResultArray) ', "DELOITTE")
                Else
                    MsgBox("No records found", MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                End If
                'Else
                ' No need to add an extra errormsg
                '    Throw New Exception(LRSCommon(45002) & oMyERPDal.ErrorMessage)
            End If

            oMyERPDal.ShowErrorMessage = True

        End If


        Exit Function

ErrorRunTheQuery:

        MsgBox(Err.Description, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, Err.Number & Err.Source)

    End Function

    Private Function SpreadResultHeadings(ByRef iNoofFields As Short, ByRef sHeadings As String, ByRef bRealHeading As Boolean) As Boolean
        Dim bx As Boolean
        Dim lCounter As Integer
        Dim sHeading As String
        Dim iSeperator As Short
        Dim lWidth As Integer
        Dim txtColumn As DataGridViewTextBoxColumn

        On Error GoTo ErrorSpreadResultHeadings

        With frmSQLStatements.gridResult
            If bRealHeading Then

                .Columns.Clear()
                .ScrollBars = ScrollBars.Both
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                .BorderStyle = BorderStyle.None
                .RowTemplate.Height = 16
                .BackgroundColor = Color.White
                .CellBorderStyle = DataGridViewCellBorderStyle.None
                '.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
                '.AutoSize = True
                .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
                .AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None

                'Spreadshit
                '.MaxRows = 0
                '.MaxCols = 0
                '.ScrollBars = SS_SCROLLBAR_BOTH
                '.ScrollBarExtMode = True ' show scrollbars only when needed
                '.OperationMode = SS_OP_MODE_ROWMODE 'SINGLE_SELECT
                'bx = .SetOddEvenRowColor(RGB(249, 253, 169), &H0, &HFFFFFF, &H0)
                '.BackColorStyle = FPSpreadADO.BackColorStyleConstants.BackColorStyleUnderGrid
                '.GridShowHoriz = False
                '.GridShowVert = False
                '.GrayAreaBackColor = System.Drawing.Color.White
                ''UPGRADE_ISSUE: VBControlExtender method fpspreadResult.BorderStyle was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                '.BorderStyle = FPSpreadADO.BorderStyleConstants.BorderStyleNone

                '.MaxCols = iNoofFields

                ' Col headings and widths:
                '.Row = 0
                '.MaxRows = 1
            Else
                '.Row = .MaxRows + 1
                '.MaxRows = .Row
            End If

            For lCounter = 1 To iNoofFields
                ' add columns
                iSeperator = InStr(1, sHeadings, ",")
                sHeading = Trim(Left(sHeadings, iSeperator - 1))
                sHeadings = Mid(sHeadings, iSeperator + 1)
                '.Col = lCounter
                '.Text = sHeading
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.HeaderText = sHeading
                txtColumn.DisplayIndex = lCounter - 1
                .Columns.Add(txtColumn)

            Next lCounter

        End With

        SpreadResultHeadings = True
        Exit Function

ErrorSpreadResultHeadings:
        SpreadResultHeadings = False

    End Function

    Private Function SpreadResultPopulate(ByRef aResult(,) As String, ByRef iRightJustifyColumn As Short) As Boolean
        ' Fill data into result spread:

        Dim lRowCounter As Integer
        Dim lColCounter As Integer
        Dim sString As String
        Dim lArrayLength As Integer

        On Error GoTo ErrorSpreadResultPopulate

        '-----
        With frmSQLStatements.gridResult

            'Sometimes we receive a onedimensional array, sometimes two-dimensinal
            sString = "KOKO"
            On Error Resume Next
            sString = aResult(0, 0)

            If sString = "KOKO" Then
                'One-dimensional
                For lRowCounter = 0 To UBound(aResult)

                    '.Row = lRowCounter + 1
                    '.MaxRows = .MaxRows + 1
                    .Rows.Add()

                    '.Col = 1
                    '.Text = aResult(lRowCounter, 0)
                    .Rows(lRowCounter).Cells(0).Value = aResult(lRowCounter, 0)

                Next lRowCounter


            Else
                ' Fill up from the array
                For lRowCounter = 0 To UBound(aResult, 2) 'lArrayLength

                    ' added 03.11.2017 - do not present more than 50 rows
                    If .Rows.Count > 100 Then
                        MsgBox("Viser kun de 100 f�rste rader - av totalt " & UBound(aResult, 2).ToString)
                        Exit For
                    End If

                    '.Row = lRowCounter + 1
                    '.MaxRows = .MaxRows + 1
                    .Rows.Add()

                    For lColCounter = 0 To UBound(aResult, 1)
                        '.Col = lColCounter + 1
                        '.Text = aResult(lColCounter, lRowCounter)
                        .Rows(lRowCounter).Cells(lColCounter).Value = aResult(lColCounter, lRowCounter)

                        If iRightJustifyColumn = lColCounter Then
                            '.TypeHAlign = SS_CELL_H_ALIGN_RIGHT '1 'TypeHAlignRight
                            .Rows(lRowCounter).Cells(lColCounter).Style.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                        '.CellType = SS_CELL_TYPE_FLOAT
                        '.TypeFloatSeparator = True

                    Next lColCounter

                Next lRowCounter

            End If

        End With

        SpreadResultPopulate = True
        Exit Function

ErrorSpreadResultPopulate:
        SpreadResultPopulate = False
    End Function



    Friend Function MoveNext(ByRef iStep As Short) As Boolean

        MsgBox("This function is not yet converted to Visual Basic.net", MsgBoxStyle.OkOnly)
        '    Dim sMySQL As String
        '    Dim bMatched As Boolean
        '    Dim oClient As vbbabel.Client
        '    Dim lCounter, lRowNumber As Integer
        '    Dim bx As Boolean
        '    Dim rsERP As New ADODB.Recordset

        '    iMatchWizard = iMatchWizard + 1

        '    Select Case iMatchWizard

        '        Case 1
        '            Dim aResultArray(0, 0) As Object

        '            If bFirstPayment Then
        '                bFirstPayment = False
        '            Else
        '                ChangePaymentInfo(1)
        '            End If

        '            bMatched = False

        '            If oPayment.I_Account <> sOldAccount Then
        '                'Find correct Client in the DB
        '                oClient = FindTestClient()
        '                sClientNo = oClient.ClientNo
        '                sOldAccount = oPayment.I_Account
        '                'KOREN - this must be replaced by retrieving info from Pattern
        '                'If sOldClientNo <> sClientNo Then
        '                'Find invoicedescroptions
        '                '    aInvDesc() = FindInvDescription(oClient)
        '                'End If
        '            End If

        '            'KOREN - this must be replaced by retrieving info from Pattern

        '            'If Array_IsEmpty(aInvDesc) Then
        '            frmMatchWizard.lblExplaination.Text = "Det er ikke oppgitt noen beskrivelse av fakturanummere."
        '            '        frmMatchWizard.lblExplaination.Caption = "No description of invoicenumbers is stated."
        '            'Else
        '            'Show results in spread
        '            '    bStatus = ShowResultInGrid(1, "Faktura-beskrivelse", aInvDesc())
        '            frmMatchWizard.lblExplaination.Text = "I BabelBank m� bruker beskrive hvilke fakturanummerserie som benyttes. Dette gj�res under klientoppsettet." & vbCrLf & "Beskrivelsene vises i resultatvinduet." & vbCrLf & vbCrLf & "BabelBank vil n� s�ke igjennom innbetalingens tekst, for � hente ut mulige fakturanummere som betaler har oppgitt."
        '            '        bStatus = ShowResultInGrid(1, "Invoice descriptions", aInvDesc())
        '            '        frmMatchWizard.lblExplaination.Caption = "In the Client-setup, You have to describe the invoice-number series You use" & _
        '            ''            vbCrLf & "The series are shown in the result-window." & vbCrLf & "BabelBank will now search the payments freetext to find possible invoicenumbers."
        '            'End If

        '        Case 2

        '            lCounter = 0
        '            'MATCH ON INVOICENUMBER

        '            'KOREN - this must be replaced by retrieving info from Pattern
        '            For lCounter = 0 To 0 'UBound(aInvDesc())
        '                oPayment.MATCH_NoOfLeadingZeros = 0
        '                'oPayment.MATCH_InvoiceDescription = aInvDesc(lCounter)
        '                oPayment.MATCH_ExtractInvoiceNo()
        '            Next lCounter

        '            For lCounter = 1 To oPayment.MATCH_PossibleNumberOfInvoices
        '                If lCounter = 1 Then
        '                    ReDim aResultArray(4, lCounter - 1)
        '                Else
        '                    ReDim Preserve aResultArray(4, lCounter - 1)
        '                End If
        '                'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.MATCH_InvoiceNumber. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(0, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                aResultArray(0, lCounter - 1) = oPayment.MATCH_InvoiceNumber
        '                'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(1, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                aResultArray(1, lCounter - 1) = ""
        '                'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(2, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                aResultArray(2, lCounter - 1) = ""
        '                'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(3, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                aResultArray(3, lCounter - 1) = ""
        '                'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(4, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                aResultArray(4, lCounter - 1) = ""
        '                oPayment.MATCH_NextInvoice()
        '            Next lCounter

        '            If oPayment.MATCH_PossibleNumberOfInvoices > 0 Then
        '                'Show results in spread
        '                bStatus = ShowResultInGrid(5, "Fakturanr.,Kundenr.,Bel�p,Valuta,Kommentarer", aResultArray)
        '                '        bStatus = ShowResultInGrid(5, "InvoiceNo,CustomerNo,Amount,Valuta,Comments", aResultArray())

        '                'KOREN - must be changed
        '                '        frmMatchWizard.lblExplaination.Caption = "BabelBank fant " & oPayment.MATCH_PossibleNumberOfInvoices & " mulig(e) fakturanummere i henhold til angitte beskrivelser." & vbCrLf & _
        '                ''            "BabelBank vil n� benytte SQL-en, " & oProfile.SQLStatements.Item(1).Name & " for � sjekke om fakturanummeret finnes i ERP-systemet." & vbCrLf & _
        '                ''            "Hvis BabelBank finner fakturanummeret vil vi hente ut n�dvendig avstemmingsinformasjon, vist i resultatetvinduet."



        '                '        frmMatchWizard.lblExplaination.Caption = "BabelBank found " & oPayment.MATCH_PossibleNumberOfInvoices & " possible invoice according to the description." & vbCrLf & _
        '                ''            "BabelBank will now use the SQL named, " & oProfile.SQLStatements.Item(1).Name & " to check if the invoicenumber exists in the ERP system, " & vbCrLf & _
        '                ''            "If it exists we will retrieve necessary matchinginformation, shown in the resultwindow."
        '            Else
        '                frmMatchWizard.lblExplaination.Text = "BabelBank fant ingen fakturanummer, under gjennoms�k av friteksten." & vbCrLf & "Vi fortsetter til neste avstemmingsregel."
        '                '        frmMatchWizard.lblExplaination.Caption = "BabelBank couldn't find any invoices matching the description." & vbCrLf & _
        '                ''            "We will continue to next matching rule."
        '                iMatchWizard = 5 'To step over the rest of the matching on invoiceno
        '            End If

        '        Case 3
        '            'KOREN
        '            'sMatchSQL = oProfile.SQLStatements.Item(1).SQLStatement
        '            If Left(sMatchSQL, 1) = "<" Then
        '                sMatchSQL = ""
        '            End If

        '            If Len(sMatchSQL) = 0 Then
        '                frmMatchWizard.lblExplaination.Text = "Ingen SQL for avstemming p� fakturanummer ble funnet."
        '                '        frmMatchWizard.lblExplaination.Caption = "No SQL for matching on invoicenumber is stated."
        '                iMatchWizard = 5 'To step over the rest of the matching on invoiceno
        '            Else

        '                nTotalMatchAmount = 0
        '                oPayment.MATCH_FirstInvoice()
        '                For lCounter = 1 To oPayment.MATCH_PossibleNumberOfInvoices

        '                    'InvoiceNo
        '                    sMySQL = Replace(sMatchSQL, "SQLInvoiceNo", Trim(oPayment.MATCH_InvoiceNumber), , , CompareMethod.Text)
        '                    'Client-number
        '                    sMySQL = Replace(sMySQL, "SQLClient", sClientNo, , , CompareMethod.Text)
        '                    'Valuta
        '                    If oPayment.PayType = "I" Then
        '                        sMySQL = Replace(sMySQL, "SQLValuta", Trim(oPayment.MON_InvoiceCurrency), , , CompareMethod.Text)
        '                    Else
        '                        sMySQL = Replace(sMySQL, "SQLValuta", "NOK", , , CompareMethod.Text)
        '                    End If

        '                    rsERP.Open(sMySQL, conERPConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

        '                    If rsERP.RecordCount = 1 Then
        '                        oPayment.MATCH_InvoiceAmount = rsERP.Fields.Item(0).Value 'Trim(Str(rsERP!u_NOKRest * 100))
        '                        oPayment.MATCH_InvoiceCustomerNo = rsERP.Fields.Item(1).Value 'rsERP!u_RESKNR
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(1, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(1, lCounter - 1) = rsERP.Fields.Item(1).Value 'CustomerNo
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(2, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(2, lCounter - 1) = VB6.Format(Val(rsERP.Fields.Item(0).Value) / 100, "Standard") 'Amount
        '                        If IsNumeric(rsERP.Fields.Item(0).Value) Then
        '                            nTotalMatchAmount = nTotalMatchAmount + rsERP.Fields.Item(0).Value
        '                        End If
        '                        If oPayment.PayType = "I" Then
        '                            'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(3, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                            aResultArray(3, lCounter - 1) = Trim(oPayment.MON_InvoiceCurrency) '"NOK"
        '                        Else
        '                            'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(3, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                            aResultArray(3, lCounter - 1) = "NOK"
        '                        End If
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(4, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(4, lCounter - 1) = ""
        '                    Else
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(1, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(1, lCounter - 1) = ""
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(2, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(2, lCounter - 1) = ""
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(3, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(3, lCounter - 1) = ""
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(4, lCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(4, lCounter - 1) = "Not found"
        '                    End If
        '                    'Close recordset
        '                    rsERP.Close()
        '                    'Continue to next invoice
        '                    oPayment.MATCH_NextInvoice()
        '                Next lCounter
        '                'Try to match payments
        '                bMatched = oPayment.MATCH_OnInvoiceNo(True, "", True)
        '                bx = oPayment.MATCH_Reset
        '                bStatus = ShowResultInGrid(5, "Fakturanr.,Kundenr.,Bel�p,Valuta,Kommentarer", aResultArray, 2)
        '                '        bStatus = ShowResultInGrid(5, "InvoiceNo,CustomerNo,Amount,Valuta,Comments", aResultArray(), 2)

        '                frmMatchWizard.lblExplaination.Text = "Resultat-vinduet viser n� resultatet av SQL-sp�rringen." & vbCrLf & "Hvis BabelBank fant fakturaen, viser vi n� avstemmingsinformasjonen p� fakturaen." & vbCrLf & "Hvis ikke, markeres dette ved � vise 'Not found' i kommentar-kolonnen." & vbCrLf & vbCrLf & "BabelBank vil n� summere alle bel�pene p� de fakturaene vi har funnet, for deretter � sjekke om totalsummen stemmer med innbetalt bel�p." & vbCrLf & "Hvis bel�pene er identiske, vil betalingene markeres som avstemt, og vi legger p� n�dvendif informasjon, f.eks. fakturanr. og kundernr." & vbCrLf & "Hvis det er mer enne en faktura som er betalt, vil BabelBank splitte opp betalingen i henhold til antall fakturaer."

        '                '        frmMatchWizard.lblExplaination.Caption = "The result-window will now show the result of the SQL-query." & vbCrLf & _
        '                '"If BabelBank found the invoice, we have shown information about the payment." & vbCrLf & _
        '                '"If not, we will mark the payment 'Not found' in the comment column" & vbCrLf & vbCrLf & _
        '                '"BabelBank will now summarize all amounts on the invoices found, and check if the amount equals the total payment." & vbCrLf & _
        '                '"If the amount match, we will mark the payment as matched and add necessary information, i.e. Invoiceno. and Customerno." & vbCrLf & _
        '                '"If there are more than one invoice paid, babelBank will splitt the payment into the correct number of invoices."
        '            End If

        '        Case 4
        '            lRowNumber = UBound(aResultArray, 2)
        '            'On Error Resume Next
        '            ReDim Preserve aResultArray(UBound(aResultArray, 1), lRowNumber + 1)
        '            'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(0, lRowNumber + 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '            aResultArray(0, lRowNumber + 1) = "Total amount"
        '            'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(2, lRowNumber + 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '            aResultArray(2, lRowNumber + 1) = VB6.Format(nTotalMatchAmount / 100, "Standard")

        '            If nTotalMatchAmount = oPayment.MON_TransferredAmount Then
        '                frmMatchWizard.lblExplaination.Text = "Bel�pene stemmer!." & vbCrLf & "BabelBank fortsetter til neste innbetaling."
        '                '        frmMatchWizard.lblExplaination.Caption = "The amount matches!." & vbCrLf & _
        '                ''            "We will continue to the next payment."
        '                iMatchWizard = 0
        '                bStatus = ShowResultInGrid(5, "Fakturnr.,Kundenr.,Bel�p,Valuta,Kommentarer", aResultArray, 2)
        '                '        bStatus = ShowResultInGrid(5, "InvoiceNo,CustomerNo,Amount,Valuta,Comments", aResultArray(), 2)
        '            Else
        '                frmMatchWizard.lblExplaination.Text = "Bel�pene stemmer ikke." & vbCrLf & vbCrLf & "Vi fortsetter til neste avstemmingsregel."
        '                bStatus = ShowResultInGrid(5, "Fakturnr.,Kundenr.,Bel�p,Valuta,Kommentarer", aResultArray, 2)
        '                '        frmMatchWizard.lblExplaination.Caption = "The amount doesn't match." & vbCrLf & vbCrLf & _
        '                ''            "We will continue to the next matchingrule."
        '                '        bStatus = ShowResultInGrid(5, "InvoiceNo,CustomerNo,Amount,Valuta,Comments", aResultArray(), 2)
        '            End If
        '            lInvoiceCounter = oPayment.Invoices.Count

        '        Case 5

        '            'KOREN
        '            'sMatchSQL = oProfile.SQLStatements.Item(2).SQLStatement
        '            If Left(sMatchSQL, 1) = "<" Then
        '                sMatchSQL = ""
        '            End If

        '            If Len(sMatchSQL) = 0 Then
        '                frmMatchWizard.lblExplaination.Text = "Fant ingen SQL-setning for avstemming p� bel�p og kontonummer."
        '                '        frmMatchWizard.lblExplaination.Caption = "No SQL for matching on amount and accountnumber is stated."
        '                iMatchWizard = 8 'To step over the rest of the matching on accountno
        '            Else
        '                ReDim aResultArray(5, 0)
        '                'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(0, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                aResultArray(0, 0) = VB6.Format(Val(CStr(oInvoice.MON_TransferredAmount)) / 100, "Standard")
        '                If oPayment.PayType = "I" Then
        '                    'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(1, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                    aResultArray(1, 0) = Trim(oPayment.MON_InvoiceCurrency) '"NOK"
        '                Else
        '                    'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(1, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                    aResultArray(1, 0) = "NOK"
        '                End If
        '                'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(2, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                aResultArray(2, 0) = oPayment.E_Account
        '                'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(3, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                aResultArray(3, 0) = ""
        '                'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(4, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                aResultArray(4, 0) = ""
        '                'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(5, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                aResultArray(5, 0) = ""

        '                bStatus = ShowResultInGrid(6, "Bel�p,Valuta,Kontonummer,Fakturnr.,Kundenr.,Kommentarer", aResultArray, 0)
        '                frmMatchWizard.lblExplaination.Text = "Babelbank vil fors�ke � avstemme innbetalingen " & "ved � benytte en kombinasjon av betalers kontonummer og bel�p." & vbCrLf & "Parameterene som benyttes er vist i resultatvinduet."
        '                '        frmMatchWizard.lblExplaination.Caption = "We'll try to match the payment " & _
        '                ''            "by using a combination of payers accountnumber and amount." & vbCrLf & _
        '                ''            "The parameters are shown in the result-window."

        '                '        frmMatchWizard.lblExplaination.Caption = "We'll try to match the payment " & _
        '                ''            "by using a combination of payers accountnumber and amount." & vbCrLf & _
        '                ''            "The parameters are shown in the result-window."
        '            End If

        '        Case 6

        '            For Each oInvoice In oPayment.Invoices
        '                'Replace Amount
        '                sMySQL = Replace(sMatchSQL, "SQLAmount", CStr(oInvoice.MON_TransferredAmount), , , CompareMethod.Text)
        '                'Replace SQLE_Account
        '                sMySQL = Replace(sMySQL, "SQLAccount", oPayment.E_Account, , , CompareMethod.Text)
        '                'Valuta
        '                If oPayment.PayType = "I" Then
        '                    sMySQL = Replace(sMySQL, "SQLValuta", Trim(oPayment.MON_InvoiceCurrency), , , CompareMethod.Text)
        '                Else
        '                    sMySQL = Replace(sMySQL, "SQLValuta", "NOK", , , CompareMethod.Text)
        '                End If
        '                'Client-number
        '                sMySQL = Replace(sMySQL, "SQLClient", sClientNo, , , CompareMethod.Text)
        '                rsERP.Open(sMySQL, conERPConnection, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

        '                If rsERP.RecordCount > 0 Then
        '                    rsERP.MoveFirst()
        '                    For lCounter = 0 To rsERP.RecordCount - 1
        '                        'If lCounter > 1 Then
        '                        'Gjort st�nt-endringer f�r presentasjon for Tore Paulsen.
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(0, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(0, lCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(0, lCounter) = aResultArray(0, 0)
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(1, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(1, lCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(1, lCounter) = aResultArray(1, 0)
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(2, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(2, lCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(2, lCounter) = aResultArray(2, 0)
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(3, lCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(3, lCounter) = rsERP.Fields.Item(0).Value 'InvoiceNo
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(4, lCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(4, lCounter) = rsERP.Fields.Item(1).Value 'CustomerNo
        '                        'UPGRADE_WARNING: Couldn't resolve default property of object aResultArray(5, lCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '                        aResultArray(5, lCounter) = ""
        '                        'rsERP.MoveNext
        '                        'End If
        '                    Next lCounter
        '                    If rsERP.RecordCount = 1 Then
        '                        oInvoice.MATCH_Matched = True
        '                        oInvoice.InvoiceNo = rsERP.Fields.Item(0).Value 'Trim(Str(rsERP!u_NOKRest * 100))
        '                        oInvoice.CustomerNo = rsERP.Fields.Item(1).Value 'rsERP!u_RESKNR
        '                        bMatched = True
        '                        frmMatchWizard.lblExplaination.Text = "BabelBank fant eksakt 1 faktura som som passet med " & "parameterene." & vbCrLf & "BabelBank vil legge p� n�dvendig informasjon, samt avstemme innbetalingen."
        '                        '                frmMatchWizard.lblExplaination.Caption = "BabelBank found exactly 1 invoice that matched the payment." & _
        '                        ''                    "the parameters." & vbCrLf & _
        '                        ''                    "BabelBank will add information to the payment and match it."
        '                        iMatchWizard = 0
        '                    Else
        '                        frmMatchWizard.lblExplaination.Text = "BabelBank fant ingen fakturaer i databasen som stemte overens med " & "parameterene." & vbCrLf & "Vi fortsetter til neste avstemmingsregel."
        '                        '                frmMatchWizard.lblExplaination.Caption = "BabelBank didn't find any invoices in the ERP-database that matched " & _
        '                        ''                    "the parameters." & vbCrLf & _
        '                        ''                    "We will continue to the next matchingrule."
        '                    End If
        '                    bStatus = ShowResultInGrid(6, "Bel�p,Valuta,Kontonummer,Fakturanr.,Kundenr.,Kommentarer", aResultArray, 0)
        '                Else
        '                    frmMatchWizard.lblExplaination.Text = "BabelBank fant ingen fakturaer i databasen som stemte overens med avstemmingsparameterene." & vbCrLf & "BabelBank vil fortsette med neste avstemmingsregel."
        '                    '            frmMatchWizard.lblExplaination.Caption = "BabelBank didn't find any invoices in the ERP-database that matched " & _
        '                    ''                "the parameters." & vbCrLf & _
        '                    ''                "We will continue to the next matchingrule."
        '                End If
        '                rsERP.Close()
        '                'bx = oPayment.MATCH_Reset
        '            Next oInvoice


        '        Case 7
        '            frmMatchWizard.lblExplaination.Text = "KOKO"
        '    End Select



    End Function

    Private Function FindTestClient() As vbBabel.Client
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim bAccountFound As Boolean

        oClient = Nothing
        On Error GoTo ErrorFindTestClient

        bAccountFound = False

        For Each oFilesetup In oProfile.FileSetups
            If oFilesetup.FromAccountingSystem = False Then
                If oFilesetup.Enum_ID = 501 Then
                    For Each oClient In oFilesetup.Clients
                        For Each oaccount In oClient.Accounts
                            If oaccount.Account = oBabel.Batches.Item(lBatchCounter).Payments.Item(lPaymentCounter).I_Account Then
                                bAccountFound = True
                                Exit For
                            End If
                        Next oaccount
                        If bAccountFound Then
                            Exit For
                        End If
                    Next oClient
                End If
            End If
            If bAccountFound Then
                Exit For
            End If
        Next oFilesetup

        If bAccountFound Then
            FindTestClient = oClient
        Else
            MsgBox("No corresponding client found for the accountnumber:" & vbCrLf & oBabel.Batches.Item(lBatchCounter).Payments.Item(lPaymentCounter).I_Account, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation)
            FindTestClient = Nothing
        End If

        oaccount = Nothing
        oClient = Nothing
        oFilesetup = Nothing

        Exit Function

ErrorFindTestClient:

        MsgBox("Error during retrieving correct client." & "Is a client registered?", MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation)

        oaccount = Nothing
        oClient = Nothing
        oFilesetup = Nothing

    End Function

    Private Function ShowResultInGrid(ByRef iNoOfColumns As Short, ByRef sHeading As String, ByRef aInformation(,) As String, Optional ByRef iRightJustifyColumn As Short = -1) As Boolean
        Dim bStatus As Boolean

        bStatus = SpreadResultHeadings(iNoOfColumns, sHeading & ",", True)
        If Not bStatus Then
            MsgBox("Can't show result of query. Error when creating headings. Reason: " & vbCrLf & Err.Description, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation)
        End If
        If Not aInformation Is Nothing Then   ' added this If 22.12.2020
            bStatus = SpreadResultPopulate(aInformation, iRightJustifyColumn)
        End If
        If Not bStatus Then
            MsgBox("Can't show result of query. Error when populating grid. Reason: " & vbCrLf & Err.Description, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation)
        Else
            frmSQLStatements.gridResult.Visible = True
        End If

    End Function
End Class
