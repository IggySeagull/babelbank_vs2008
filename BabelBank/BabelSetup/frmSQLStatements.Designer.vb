<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSQLStatements
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdWriDescription As System.Windows.Forms.Button
	Public WithEvents txtWriSQL As System.Windows.Forms.RichTextBox
    Public WithEvents cmdWriCancel As System.Windows.Forms.Button
	Public WithEvents cmdWriSave As System.Windows.Forms.Button
	Public WithEvents cmdWriTest As System.Windows.Forms.Button
	Public WithEvents lblWriVariables As System.Windows.Forms.Label
	Public WithEvents frameWriVariables As System.Windows.Forms.GroupBox
	Public WithEvents lblWriFieldsOrder As System.Windows.Forms.Label
	Public WithEvents lblWriHelpMe As System.Windows.Forms.Label
	Public WithEvents lblWriExplain As System.Windows.Forms.Label
	Public WithEvents lblWriSQL As System.Windows.Forms.Label
	Public WithEvents lblWriSQLName As System.Windows.Forms.Label
	Public WithEvents lblWriMandatory As System.Windows.Forms.Label
	Public WithEvents _frame1_0 As System.Windows.Forms.Panel
    Public WithEvents cmdIntConnection As System.Windows.Forms.Button
	Public WithEvents lblIntVariables As System.Windows.Forms.Label
	Public WithEvents frameIntVariables As System.Windows.Forms.GroupBox
	Public WithEvents cmdIntCancel As System.Windows.Forms.Button
	Public WithEvents cmdIntSave As System.Windows.Forms.Button
	Public WithEvents cmdIntTest As System.Windows.Forms.Button
	Public WithEvents lblIntFieldsOrder As System.Windows.Forms.Label
	Public WithEvents lblIntHelpMe As System.Windows.Forms.Label
	Public WithEvents lblIntMandatory As System.Windows.Forms.Label
	Public WithEvents lblIntSQLname As System.Windows.Forms.Label
	Public WithEvents lblIntSQL As System.Windows.Forms.Label
	Public WithEvents lblIntExplain As System.Windows.Forms.Label
	Public WithEvents _frame1_1 As System.Windows.Forms.Panel
	Public WithEvents optOther As System.Windows.Forms.RadioButton
	Public WithEvents cmdIcon As System.Windows.Forms.Button
	Public WithEvents optValidation As System.Windows.Forms.RadioButton
	Public WithEvents txtSummarize As System.Windows.Forms.TextBox
	Public WithEvents cmdDeleteRule As System.Windows.Forms.Button
	Public WithEvents chkIncludeOCR As System.Windows.Forms.CheckBox
    Public WithEvents chkUseAdjustmentsFinal As System.Windows.Forms.CheckBox
    Public WithEvents cmbPatterns As System.Windows.Forms.ComboBox
    Public WithEvents txtRuleSpecial As System.Windows.Forms.TextBox
    Public WithEvents txtRuleOrder As System.Windows.Forms.TextBox
    Public WithEvents txtRuleType As System.Windows.Forms.TextBox
    Public WithEvents txtRuleERPIndex As System.Windows.Forms.TextBox
    Public WithEvents txtRuleName As System.Windows.Forms.TextBox
    Public WithEvents cmdNewRule As System.Windows.Forms.Button
    Public WithEvents lstMatchingRules As System.Windows.Forms.ListBox
    Public WithEvents optManual As System.Windows.Forms.RadioButton
    Public WithEvents optAfter As System.Windows.Forms.RadioButton
    Public WithEvents optAutomatic As System.Windows.Forms.RadioButton
    Public WithEvents chkProposeMatch As System.Windows.Forms.CheckBox
    Public WithEvents imgIcon As System.Windows.Forms.PictureBox
    Public WithEvents imgArrowDown As System.Windows.Forms.PictureBox
    Public WithEvents imgArrowUp As System.Windows.Forms.PictureBox
    Public WithEvents lblSummarize As System.Windows.Forms.Label
    Public WithEvents lblPattern As System.Windows.Forms.Label
    Public WithEvents lblRuleType As System.Windows.Forms.Label
    Public WithEvents lblRuleOrder As System.Windows.Forms.Label
    Public WithEvents lblRuleSpecial As System.Windows.Forms.Label
    Public WithEvents lblRuleName As System.Windows.Forms.Label
    Public WithEvents frameQuery As System.Windows.Forms.GroupBox
    Public WithEvents txtPayInvAmount As System.Windows.Forms.TextBox
    Public WithEvents txtPayText As System.Windows.Forms.TextBox
    Public WithEvents cmdPayInvDesc As System.Windows.Forms.Button
    Public WithEvents lblPayInvAMount As System.Windows.Forms.Label
    Public WithEvents lblPayText As System.Windows.Forms.Label
    Public WithEvents framePayInvoice As System.Windows.Forms.GroupBox
    Public WithEvents txtExchRate As System.Windows.Forms.TextBox
    Public WithEvents txtAmount_NOK As System.Windows.Forms.TextBox
    Public WithEvents txtPayName As System.Windows.Forms.TextBox
    Public WithEvents txtPayAdr1 As System.Windows.Forms.TextBox
    Public WithEvents txtPayZip As System.Windows.Forms.TextBox
    Public WithEvents txtPayCity As System.Windows.Forms.TextBox
    Public WithEvents txtPayCountry As System.Windows.Forms.TextBox
    Public WithEvents txtPayAmount As System.Windows.Forms.TextBox
    Public WithEvents txtPayISO As System.Windows.Forms.TextBox
    Public WithEvents txtPayAccount As System.Windows.Forms.TextBox
    Public WithEvents txtPayRef As System.Windows.Forms.TextBox
    Public WithEvents lblExchRate As System.Windows.Forms.Label
    Public WithEvents lblAmount_NOK As System.Windows.Forms.Label
    Public WithEvents lblPayName As System.Windows.Forms.Label
    Public WithEvents lblPayAdr1 As System.Windows.Forms.Label
    Public WithEvents lblPayZip As System.Windows.Forms.Label
    Public WithEvents lblPayCountry As System.Windows.Forms.Label
    Public WithEvents lblPayAmount As System.Windows.Forms.Label
    Public WithEvents lblPayAccount As System.Windows.Forms.Label
    Public WithEvents lblPayRef As System.Windows.Forms.Label
    Public WithEvents lblPayISO As System.Windows.Forms.Label
    Public WithEvents framePayPayment As System.Windows.Forms.GroupBox
    Public WithEvents txtPayStateRef As System.Windows.Forms.TextBox
    Public WithEvents txtPayStateAmount As System.Windows.Forms.TextBox
    Public WithEvents txtPayBookdate As System.Windows.Forms.TextBox
    Public WithEvents txtPayCreditAccount As System.Windows.Forms.TextBox
    Public WithEvents lblPayBookdate As System.Windows.Forms.Label
    Public WithEvents lblPayStateAmount As System.Windows.Forms.Label
    Public WithEvents lblPayStateRef As System.Windows.Forms.Label
    Public WithEvents lblPayCreditAccount As System.Windows.Forms.Label
    Public WithEvents framePayStatement As System.Windows.Forms.GroupBox
    Public WithEvents _frame1_2 As System.Windows.Forms.Panel
    Public CommonDialog1Open As System.Windows.Forms.OpenFileDialog
    Public CommonDialog1Save As System.Windows.Forms.SaveFileDialog
    Public CommonDialog1Font As System.Windows.Forms.FontDialog
    Public CommonDialog1Color As System.Windows.Forms.ColorDialog
    Public CommonDialog1Print As System.Windows.Forms.PrintDialog
    Public WithEvents cmdEnd As System.Windows.Forms.Button
    Public WithEvents frameResult As System.Windows.Forms.GroupBox
    Public WithEvents txtParaDivLabel As System.Windows.Forms.TextBox
    Public WithEvents txtParaDiv As System.Windows.Forms.TextBox
    Public WithEvents txtParaMyText As System.Windows.Forms.TextBox
    Public WithEvents txtParaCurrency As System.Windows.Forms.TextBox
    Public WithEvents txtParaClient As System.Windows.Forms.TextBox
    Public WithEvents txtParaAccount As System.Windows.Forms.TextBox
    Public WithEvents txtParaName As System.Windows.Forms.TextBox
    Public WithEvents txtParaCustomer As System.Windows.Forms.TextBox
    Public WithEvents txtParaInvoice As System.Windows.Forms.TextBox
    Public WithEvents txtParaAmount As System.Windows.Forms.TextBox
    Public WithEvents lblParaMyText As System.Windows.Forms.Label
    Public WithEvents lblParaCurrency As System.Windows.Forms.Label
    Public WithEvents lblParaClient As System.Windows.Forms.Label
    Public WithEvents lblParaAccount As System.Windows.Forms.Label
    Public WithEvents lblParaName As System.Windows.Forms.Label
    Public WithEvents lblParaCustomer As System.Windows.Forms.Label
    Public WithEvents lblParaInvoice As System.Windows.Forms.Label
    Public WithEvents lblParaAmount As System.Windows.Forms.Label
    Public WithEvents frameParameters As System.Windows.Forms.GroupBox
    Public WithEvents frame1 As Microsoft.VisualBasic.Compatibility.VB6.PanelArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSQLStatements))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me._frame1_0 = New System.Windows.Forms.Panel
        Me.cmdReplace = New System.Windows.Forms.Button
        Me.txtIntSQL = New System.Windows.Forms.RichTextBox
        Me.cmdWriInterpreted = New System.Windows.Forms.Button
        Me.cmdWriDescription = New System.Windows.Forms.Button
        Me.txtWriSQL = New System.Windows.Forms.RichTextBox
        Me.cmdWriCancel = New System.Windows.Forms.Button
        Me.cmdWriSave = New System.Windows.Forms.Button
        Me.cmdWriTest = New System.Windows.Forms.Button
        Me.frameWriVariables = New System.Windows.Forms.GroupBox
        Me.lblWriVariables = New System.Windows.Forms.Label
        Me.lblWriFieldsOrder = New System.Windows.Forms.Label
        Me.lblWriHelpMe = New System.Windows.Forms.Label
        Me.lblWriExplain = New System.Windows.Forms.Label
        Me.lblWriSQL = New System.Windows.Forms.Label
        Me.lblWriSQLName = New System.Windows.Forms.Label
        Me.lblWriMandatory = New System.Windows.Forms.Label
        Me._frame1_1 = New System.Windows.Forms.Panel
        Me.cmdIntConnection = New System.Windows.Forms.Button
        Me.frameIntVariables = New System.Windows.Forms.GroupBox
        Me.lblIntVariables = New System.Windows.Forms.Label
        Me.cmdIntCancel = New System.Windows.Forms.Button
        Me.cmdIntSave = New System.Windows.Forms.Button
        Me.cmdIntTest = New System.Windows.Forms.Button
        Me.lblIntFieldsOrder = New System.Windows.Forms.Label
        Me.lblIntHelpMe = New System.Windows.Forms.Label
        Me.lblIntMandatory = New System.Windows.Forms.Label
        Me.lblIntSQLname = New System.Windows.Forms.Label
        Me.lblIntSQL = New System.Windows.Forms.Label
        Me.lblIntExplain = New System.Windows.Forms.Label
        Me.frameQuery = New System.Windows.Forms.GroupBox
        Me.chkCustomerNo = New System.Windows.Forms.CheckBox
        Me.chkUseAdjustments = New System.Windows.Forms.CheckBox
        Me.optOther = New System.Windows.Forms.RadioButton
        Me.cmdIcon = New System.Windows.Forms.Button
        Me.optValidation = New System.Windows.Forms.RadioButton
        Me.txtSummarize = New System.Windows.Forms.TextBox
        Me.cmdDeleteRule = New System.Windows.Forms.Button
        Me.chkIncludeOCR = New System.Windows.Forms.CheckBox
        Me.chkUseAdjustmentsFinal = New System.Windows.Forms.CheckBox
        Me.cmbPatterns = New System.Windows.Forms.ComboBox
        Me.txtRuleSpecial = New System.Windows.Forms.TextBox
        Me.txtRuleOrder = New System.Windows.Forms.TextBox
        Me.txtRuleType = New System.Windows.Forms.TextBox
        Me.txtRuleERPIndex = New System.Windows.Forms.TextBox
        Me.txtRuleName = New System.Windows.Forms.TextBox
        Me.cmdNewRule = New System.Windows.Forms.Button
        Me.lstMatchingRules = New System.Windows.Forms.ListBox
        Me.optManual = New System.Windows.Forms.RadioButton
        Me.optAfter = New System.Windows.Forms.RadioButton
        Me.optAutomatic = New System.Windows.Forms.RadioButton
        Me.chkProposeMatch = New System.Windows.Forms.CheckBox
        Me.imgIcon = New System.Windows.Forms.PictureBox
        Me.imgArrowDown = New System.Windows.Forms.PictureBox
        Me.imgArrowUp = New System.Windows.Forms.PictureBox
        Me.lblSummarize = New System.Windows.Forms.Label
        Me.lblPattern = New System.Windows.Forms.Label
        Me.lblRuleType = New System.Windows.Forms.Label
        Me.lblRuleOrder = New System.Windows.Forms.Label
        Me.lblRuleSpecial = New System.Windows.Forms.Label
        Me.lblRuleName = New System.Windows.Forms.Label
        Me._frame1_2 = New System.Windows.Forms.Panel
        Me.framePayInvoice = New System.Windows.Forms.GroupBox
        Me.txtPayInvAmount = New System.Windows.Forms.TextBox
        Me.txtPayText = New System.Windows.Forms.TextBox
        Me.cmdPayInvDesc = New System.Windows.Forms.Button
        Me.lblPayInvAMount = New System.Windows.Forms.Label
        Me.lblPayText = New System.Windows.Forms.Label
        Me.framePayPayment = New System.Windows.Forms.GroupBox
        Me.txtExchRate = New System.Windows.Forms.TextBox
        Me.txtAmount_NOK = New System.Windows.Forms.TextBox
        Me.txtPayName = New System.Windows.Forms.TextBox
        Me.txtPayAdr1 = New System.Windows.Forms.TextBox
        Me.txtPayZip = New System.Windows.Forms.TextBox
        Me.txtPayCity = New System.Windows.Forms.TextBox
        Me.txtPayCountry = New System.Windows.Forms.TextBox
        Me.txtPayAmount = New System.Windows.Forms.TextBox
        Me.txtPayISO = New System.Windows.Forms.TextBox
        Me.txtPayAccount = New System.Windows.Forms.TextBox
        Me.txtPayRef = New System.Windows.Forms.TextBox
        Me.lblExchRate = New System.Windows.Forms.Label
        Me.lblAmount_NOK = New System.Windows.Forms.Label
        Me.lblPayName = New System.Windows.Forms.Label
        Me.lblPayAdr1 = New System.Windows.Forms.Label
        Me.lblPayZip = New System.Windows.Forms.Label
        Me.lblPayCountry = New System.Windows.Forms.Label
        Me.lblPayAmount = New System.Windows.Forms.Label
        Me.lblPayAccount = New System.Windows.Forms.Label
        Me.lblPayRef = New System.Windows.Forms.Label
        Me.lblPayISO = New System.Windows.Forms.Label
        Me.framePayStatement = New System.Windows.Forms.GroupBox
        Me.txtPayStateRef = New System.Windows.Forms.TextBox
        Me.txtPayStateAmount = New System.Windows.Forms.TextBox
        Me.txtPayBookdate = New System.Windows.Forms.TextBox
        Me.txtPayCreditAccount = New System.Windows.Forms.TextBox
        Me.lblPayBookdate = New System.Windows.Forms.Label
        Me.lblPayStateAmount = New System.Windows.Forms.Label
        Me.lblPayStateRef = New System.Windows.Forms.Label
        Me.lblPayCreditAccount = New System.Windows.Forms.Label
        Me.CommonDialog1Open = New System.Windows.Forms.OpenFileDialog
        Me.CommonDialog1Save = New System.Windows.Forms.SaveFileDialog
        Me.CommonDialog1Font = New System.Windows.Forms.FontDialog
        Me.CommonDialog1Color = New System.Windows.Forms.ColorDialog
        Me.CommonDialog1Print = New System.Windows.Forms.PrintDialog
        Me.cmdEnd = New System.Windows.Forms.Button
        Me.frameResult = New System.Windows.Forms.GroupBox
        Me.gridResult = New System.Windows.Forms.DataGridView
        Me.frameParameters = New System.Windows.Forms.GroupBox
        Me.txtParaDivLabel = New System.Windows.Forms.TextBox
        Me.txtParaDiv = New System.Windows.Forms.TextBox
        Me.txtParaMyText = New System.Windows.Forms.TextBox
        Me.txtParaCurrency = New System.Windows.Forms.TextBox
        Me.txtParaClient = New System.Windows.Forms.TextBox
        Me.txtParaAccount = New System.Windows.Forms.TextBox
        Me.txtParaName = New System.Windows.Forms.TextBox
        Me.txtParaCustomer = New System.Windows.Forms.TextBox
        Me.txtParaInvoice = New System.Windows.Forms.TextBox
        Me.txtParaAmount = New System.Windows.Forms.TextBox
        Me.lblParaMyText = New System.Windows.Forms.Label
        Me.lblParaCurrency = New System.Windows.Forms.Label
        Me.lblParaClient = New System.Windows.Forms.Label
        Me.lblParaAccount = New System.Windows.Forms.Label
        Me.lblParaName = New System.Windows.Forms.Label
        Me.lblParaCustomer = New System.Windows.Forms.Label
        Me.lblParaInvoice = New System.Windows.Forms.Label
        Me.lblParaAmount = New System.Windows.Forms.Label
        Me.frame1 = New Microsoft.VisualBasic.Compatibility.VB6.PanelArray(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.lblLine1 = New System.Windows.Forms.Label
        Me._frame1_0.SuspendLayout()
        Me.frameWriVariables.SuspendLayout()
        Me._frame1_1.SuspendLayout()
        Me.frameIntVariables.SuspendLayout()
        Me.frameQuery.SuspendLayout()
        CType(Me.imgIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgArrowDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgArrowUp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._frame1_2.SuspendLayout()
        Me.framePayInvoice.SuspendLayout()
        Me.framePayPayment.SuspendLayout()
        Me.framePayStatement.SuspendLayout()
        Me.frameResult.SuspendLayout()
        CType(Me.gridResult, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.frameParameters.SuspendLayout()
        CType(Me.frame1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_frame1_0
        '
        Me._frame1_0.BackColor = System.Drawing.SystemColors.Control
        Me._frame1_0.Controls.Add(Me.cmdReplace)
        Me._frame1_0.Controls.Add(Me.txtIntSQL)
        Me._frame1_0.Controls.Add(Me.cmdWriInterpreted)
        Me._frame1_0.Controls.Add(Me.cmdWriDescription)
        Me._frame1_0.Controls.Add(Me.txtWriSQL)
        Me._frame1_0.Controls.Add(Me.cmdWriCancel)
        Me._frame1_0.Controls.Add(Me.cmdWriSave)
        Me._frame1_0.Controls.Add(Me.cmdWriTest)
        Me._frame1_0.Controls.Add(Me.frameWriVariables)
        Me._frame1_0.Controls.Add(Me.lblWriFieldsOrder)
        Me._frame1_0.Controls.Add(Me.lblWriHelpMe)
        Me._frame1_0.Controls.Add(Me.lblWriExplain)
        Me._frame1_0.Controls.Add(Me.lblWriSQL)
        Me._frame1_0.Controls.Add(Me.lblWriSQLName)
        Me._frame1_0.Controls.Add(Me.lblWriMandatory)
        Me._frame1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._frame1_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.frame1.SetIndex(Me._frame1_0, CType(0, Short))
        Me._frame1_0.Location = New System.Drawing.Point(200, 256)
        Me._frame1_0.Name = "_frame1_0"
        Me._frame1_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._frame1_0.Size = New System.Drawing.Size(735, 255)
        Me._frame1_0.TabIndex = 42
        Me._frame1_0.Text = "Tab1 - frame"
        '
        'cmdReplace
        '
        Me.cmdReplace.BackColor = System.Drawing.SystemColors.Control
        Me.cmdReplace.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdReplace.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdReplace.Location = New System.Drawing.Point(24, 198)
        Me.cmdReplace.Name = "cmdReplace"
        Me.cmdReplace.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdReplace.Size = New System.Drawing.Size(70, 21)
        Me.cmdReplace.TabIndex = 125
        Me.cmdReplace.Text = "60639-Bytt tekst"
        Me.cmdReplace.UseVisualStyleBackColor = False
        '
        'txtIntSQL
        '
        Me.txtIntSQL.Location = New System.Drawing.Point(100, 83)
        Me.txtIntSQL.Name = "txtIntSQL"
        Me.txtIntSQL.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.txtIntSQL.Size = New System.Drawing.Size(361, 113)
        Me.txtIntSQL.TabIndex = 124
        Me.txtIntSQL.Text = ""
        '
        'cmdWriInterpreted
        '
        Me.cmdWriInterpreted.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWriInterpreted.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWriInterpreted.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWriInterpreted.Location = New System.Drawing.Point(24, 175)
        Me.cmdWriInterpreted.Name = "cmdWriInterpreted"
        Me.cmdWriInterpreted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWriInterpreted.Size = New System.Drawing.Size(70, 21)
        Me.cmdWriInterpreted.TabIndex = 123
        Me.cmdWriInterpreted.Text = "Tolket"
        Me.cmdWriInterpreted.UseVisualStyleBackColor = False
        '
        'cmdWriDescription
        '
        Me.cmdWriDescription.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWriDescription.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWriDescription.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWriDescription.Location = New System.Drawing.Point(24, 152)
        Me.cmdWriDescription.Name = "cmdWriDescription"
        Me.cmdWriDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWriDescription.Size = New System.Drawing.Size(70, 21)
        Me.cmdWriDescription.TabIndex = 122
        Me.cmdWriDescription.Text = "55041-Description"
        Me.cmdWriDescription.UseVisualStyleBackColor = False
        '
        'txtWriSQL
        '
        Me.txtWriSQL.Location = New System.Drawing.Point(103, 83)
        Me.txtWriSQL.Name = "txtWriSQL"
        Me.txtWriSQL.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.txtWriSQL.Size = New System.Drawing.Size(361, 113)
        Me.txtWriSQL.TabIndex = 25
        Me.txtWriSQL.Text = ""
        '
        'cmdWriCancel
        '
        Me.cmdWriCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWriCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWriCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWriCancel.Location = New System.Drawing.Point(24, 129)
        Me.cmdWriCancel.Name = "cmdWriCancel"
        Me.cmdWriCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWriCancel.Size = New System.Drawing.Size(70, 21)
        Me.cmdWriCancel.TabIndex = 28
        Me.cmdWriCancel.Text = "55002 Can"
        Me.cmdWriCancel.UseVisualStyleBackColor = False
        '
        'cmdWriSave
        '
        Me.cmdWriSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWriSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWriSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWriSave.Location = New System.Drawing.Point(24, 106)
        Me.cmdWriSave.Name = "cmdWriSave"
        Me.cmdWriSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWriSave.Size = New System.Drawing.Size(70, 21)
        Me.cmdWriSave.TabIndex = 27
        Me.cmdWriSave.Text = "55011 Sav"
        Me.cmdWriSave.UseVisualStyleBackColor = False
        '
        'cmdWriTest
        '
        Me.cmdWriTest.BackColor = System.Drawing.SystemColors.Control
        Me.cmdWriTest.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdWriTest.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdWriTest.Location = New System.Drawing.Point(24, 83)
        Me.cmdWriTest.Name = "cmdWriTest"
        Me.cmdWriTest.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdWriTest.Size = New System.Drawing.Size(70, 21)
        Me.cmdWriTest.TabIndex = 26
        Me.cmdWriTest.Text = "55010 Test"
        Me.cmdWriTest.UseVisualStyleBackColor = False
        '
        'frameWriVariables
        '
        Me.frameWriVariables.BackColor = System.Drawing.SystemColors.Control
        Me.frameWriVariables.Controls.Add(Me.lblWriVariables)
        Me.frameWriVariables.ForeColor = System.Drawing.SystemColors.ControlText
        Me.frameWriVariables.Location = New System.Drawing.Point(480, 77)
        Me.frameWriVariables.Name = "frameWriVariables"
        Me.frameWriVariables.Padding = New System.Windows.Forms.Padding(0)
        Me.frameWriVariables.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.frameWriVariables.Size = New System.Drawing.Size(222, 118)
        Me.frameWriVariables.TabIndex = 61
        Me.frameWriVariables.TabStop = False
        Me.frameWriVariables.Text = "60232 - Variables"
        '
        'lblWriVariables
        '
        Me.lblWriVariables.BackColor = System.Drawing.SystemColors.Control
        Me.lblWriVariables.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWriVariables.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWriVariables.Location = New System.Drawing.Point(8, 16)
        Me.lblWriVariables.Name = "lblWriVariables"
        Me.lblWriVariables.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWriVariables.Size = New System.Drawing.Size(205, 89)
        Me.lblWriVariables.TabIndex = 62
        Me.lblWriVariables.Text = "60220 Variable definition"
        '
        'lblWriFieldsOrder
        '
        Me.lblWriFieldsOrder.BackColor = System.Drawing.SystemColors.Control
        Me.lblWriFieldsOrder.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWriFieldsOrder.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWriFieldsOrder.Location = New System.Drawing.Point(213, 197)
        Me.lblWriFieldsOrder.Name = "lblWriFieldsOrder"
        Me.lblWriFieldsOrder.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWriFieldsOrder.Size = New System.Drawing.Size(489, 46)
        Me.lblWriFieldsOrder.TabIndex = 120
        Me.lblWriFieldsOrder.Text = "60321 - BB_RET......"
        '
        'lblWriHelpMe
        '
        Me.lblWriHelpMe.BackColor = System.Drawing.SystemColors.Control
        Me.lblWriHelpMe.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWriHelpMe.Enabled = False
        Me.lblWriHelpMe.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWriHelpMe.Location = New System.Drawing.Point(134, 208)
        Me.lblWriHelpMe.Name = "lblWriHelpMe"
        Me.lblWriHelpMe.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWriHelpMe.Size = New System.Drawing.Size(73, 22)
        Me.lblWriHelpMe.TabIndex = 74
        Me.lblWriHelpMe.Text = "Written"
        Me.lblWriHelpMe.Visible = False
        '
        'lblWriExplain
        '
        Me.lblWriExplain.BackColor = System.Drawing.SystemColors.Control
        Me.lblWriExplain.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWriExplain.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWriExplain.Location = New System.Drawing.Point(6, 3)
        Me.lblWriExplain.Name = "lblWriExplain"
        Me.lblWriExplain.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWriExplain.Size = New System.Drawing.Size(722, 65)
        Me.lblWriExplain.TabIndex = 60
        Me.lblWriExplain.Text = "60231 - Explaination"
        '
        'lblWriSQL
        '
        Me.lblWriSQL.BackColor = System.Drawing.SystemColors.Control
        Me.lblWriSQL.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWriSQL.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWriSQL.Location = New System.Drawing.Point(24, 68)
        Me.lblWriSQL.Name = "lblWriSQL"
        Me.lblWriSQL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWriSQL.Size = New System.Drawing.Size(89, 15)
        Me.lblWriSQL.TabIndex = 59
        Me.lblWriSQL.Text = "60230 - Avst.regel:"
        '
        'lblWriSQLName
        '
        Me.lblWriSQLName.BackColor = System.Drawing.SystemColors.Control
        Me.lblWriSQLName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWriSQLName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWriSQLName.Location = New System.Drawing.Point(112, 68)
        Me.lblWriSQLName.Name = "lblWriSQLName"
        Me.lblWriSQLName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWriSQLName.Size = New System.Drawing.Size(361, 17)
        Me.lblWriSQLName.TabIndex = 58
        Me.lblWriSQLName.Text = "Name of query"
        '
        'lblWriMandatory
        '
        Me.lblWriMandatory.BackColor = System.Drawing.SystemColors.Control
        Me.lblWriMandatory.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWriMandatory.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWriMandatory.Location = New System.Drawing.Point(134, 196)
        Me.lblWriMandatory.Name = "lblWriMandatory"
        Me.lblWriMandatory.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWriMandatory.Size = New System.Drawing.Size(73, 22)
        Me.lblWriMandatory.TabIndex = 48
        Me.lblWriMandatory.Text = "60219 Return fields"
        '
        '_frame1_1
        '
        Me._frame1_1.BackColor = System.Drawing.SystemColors.Control
        Me._frame1_1.Controls.Add(Me.cmdIntConnection)
        Me._frame1_1.Controls.Add(Me.frameIntVariables)
        Me._frame1_1.Controls.Add(Me.cmdIntCancel)
        Me._frame1_1.Controls.Add(Me.cmdIntSave)
        Me._frame1_1.Controls.Add(Me.cmdIntTest)
        Me._frame1_1.Controls.Add(Me.lblIntFieldsOrder)
        Me._frame1_1.Controls.Add(Me.lblIntHelpMe)
        Me._frame1_1.Controls.Add(Me.lblIntMandatory)
        Me._frame1_1.Controls.Add(Me.lblIntSQLname)
        Me._frame1_1.Controls.Add(Me.lblIntSQL)
        Me._frame1_1.Controls.Add(Me.lblIntExplain)
        Me._frame1_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._frame1_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.frame1.SetIndex(Me._frame1_1, CType(1, Short))
        Me._frame1_1.Location = New System.Drawing.Point(200, 256)
        Me._frame1_1.Name = "_frame1_1"
        Me._frame1_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._frame1_1.Size = New System.Drawing.Size(735, 255)
        Me._frame1_1.TabIndex = 43
        Me._frame1_1.Text = "Tab2 - frame"
        Me._frame1_1.Visible = False
        '
        'cmdIntConnection
        '
        Me.cmdIntConnection.BackColor = System.Drawing.SystemColors.Control
        Me.cmdIntConnection.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdIntConnection.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdIntConnection.Location = New System.Drawing.Point(616, 197)
        Me.cmdIntConnection.Name = "cmdIntConnection"
        Me.cmdIntConnection.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdIntConnection.Size = New System.Drawing.Size(81, 21)
        Me.cmdIntConnection.TabIndex = 72
        Me.cmdIntConnection.Text = "60221 - Con"
        Me.cmdIntConnection.UseVisualStyleBackColor = False
        '
        'frameIntVariables
        '
        Me.frameIntVariables.BackColor = System.Drawing.SystemColors.Control
        Me.frameIntVariables.Controls.Add(Me.lblIntVariables)
        Me.frameIntVariables.ForeColor = System.Drawing.SystemColors.ControlText
        Me.frameIntVariables.Location = New System.Drawing.Point(480, 72)
        Me.frameIntVariables.Name = "frameIntVariables"
        Me.frameIntVariables.Padding = New System.Windows.Forms.Padding(0)
        Me.frameIntVariables.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.frameIntVariables.Size = New System.Drawing.Size(222, 113)
        Me.frameIntVariables.TabIndex = 70
        Me.frameIntVariables.TabStop = False
        Me.frameIntVariables.Text = "60232 - Variables"
        '
        'lblIntVariables
        '
        Me.lblIntVariables.BackColor = System.Drawing.SystemColors.Control
        Me.lblIntVariables.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblIntVariables.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblIntVariables.Location = New System.Drawing.Point(8, 16)
        Me.lblIntVariables.Name = "lblIntVariables"
        Me.lblIntVariables.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblIntVariables.Size = New System.Drawing.Size(205, 89)
        Me.lblIntVariables.TabIndex = 71
        Me.lblIntVariables.Text = "60220 - Variables definition"
        '
        'cmdIntCancel
        '
        Me.cmdIntCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdIntCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdIntCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdIntCancel.Location = New System.Drawing.Point(24, 160)
        Me.cmdIntCancel.Name = "cmdIntCancel"
        Me.cmdIntCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdIntCancel.Size = New System.Drawing.Size(65, 21)
        Me.cmdIntCancel.TabIndex = 65
        Me.cmdIntCancel.Text = "55002 Can"
        Me.cmdIntCancel.UseVisualStyleBackColor = False
        '
        'cmdIntSave
        '
        Me.cmdIntSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdIntSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdIntSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdIntSave.Location = New System.Drawing.Point(24, 132)
        Me.cmdIntSave.Name = "cmdIntSave"
        Me.cmdIntSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdIntSave.Size = New System.Drawing.Size(65, 21)
        Me.cmdIntSave.TabIndex = 64
        Me.cmdIntSave.Text = "55011 Sav"
        Me.cmdIntSave.UseVisualStyleBackColor = False
        '
        'cmdIntTest
        '
        Me.cmdIntTest.BackColor = System.Drawing.SystemColors.Control
        Me.cmdIntTest.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdIntTest.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdIntTest.Location = New System.Drawing.Point(24, 104)
        Me.cmdIntTest.Name = "cmdIntTest"
        Me.cmdIntTest.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdIntTest.Size = New System.Drawing.Size(65, 21)
        Me.cmdIntTest.TabIndex = 63
        Me.cmdIntTest.Text = "55010 Test"
        Me.cmdIntTest.UseVisualStyleBackColor = False
        '
        'lblIntFieldsOrder
        '
        Me.lblIntFieldsOrder.BackColor = System.Drawing.SystemColors.Control
        Me.lblIntFieldsOrder.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblIntFieldsOrder.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblIntFieldsOrder.Location = New System.Drawing.Point(104, 192)
        Me.lblIntFieldsOrder.Name = "lblIntFieldsOrder"
        Me.lblIntFieldsOrder.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblIntFieldsOrder.Size = New System.Drawing.Size(489, 33)
        Me.lblIntFieldsOrder.TabIndex = 68
        Me.lblIntFieldsOrder.Text = "60312 BBRET_ ...."
        '
        'lblIntHelpMe
        '
        Me.lblIntHelpMe.BackColor = System.Drawing.SystemColors.Control
        Me.lblIntHelpMe.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblIntHelpMe.Enabled = False
        Me.lblIntHelpMe.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblIntHelpMe.Location = New System.Drawing.Point(8, 208)
        Me.lblIntHelpMe.Name = "lblIntHelpMe"
        Me.lblIntHelpMe.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblIntHelpMe.Size = New System.Drawing.Size(73, 17)
        Me.lblIntHelpMe.TabIndex = 73
        Me.lblIntHelpMe.Text = "Interpreted"
        Me.lblIntHelpMe.Visible = False
        '
        'lblIntMandatory
        '
        Me.lblIntMandatory.BackColor = System.Drawing.SystemColors.Control
        Me.lblIntMandatory.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblIntMandatory.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblIntMandatory.Location = New System.Drawing.Point(24, 192)
        Me.lblIntMandatory.Name = "lblIntMandatory"
        Me.lblIntMandatory.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblIntMandatory.Size = New System.Drawing.Size(97, 17)
        Me.lblIntMandatory.TabIndex = 69
        Me.lblIntMandatory.Text = "60219 -Retfields"
        '
        'lblIntSQLname
        '
        Me.lblIntSQLname.BackColor = System.Drawing.SystemColors.Control
        Me.lblIntSQLname.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblIntSQLname.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblIntSQLname.Location = New System.Drawing.Point(104, 80)
        Me.lblIntSQLname.Name = "lblIntSQLname"
        Me.lblIntSQLname.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblIntSQLname.Size = New System.Drawing.Size(361, 17)
        Me.lblIntSQLname.TabIndex = 67
        Me.lblIntSQLname.Text = "NameOfQuery"
        '
        'lblIntSQL
        '
        Me.lblIntSQL.BackColor = System.Drawing.SystemColors.Control
        Me.lblIntSQL.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblIntSQL.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblIntSQL.Location = New System.Drawing.Point(24, 80)
        Me.lblIntSQL.Name = "lblIntSQL"
        Me.lblIntSQL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblIntSQL.Size = New System.Drawing.Size(89, 17)
        Me.lblIntSQL.TabIndex = 66
        Me.lblIntSQL.Text = "60230 - Avst.regel"
        '
        'lblIntExplain
        '
        Me.lblIntExplain.BackColor = System.Drawing.SystemColors.Control
        Me.lblIntExplain.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblIntExplain.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblIntExplain.Location = New System.Drawing.Point(6, 3)
        Me.lblIntExplain.Name = "lblIntExplain"
        Me.lblIntExplain.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblIntExplain.Size = New System.Drawing.Size(722, 68)
        Me.lblIntExplain.TabIndex = 57
        Me.lblIntExplain.Text = "60231 - Explaination"
        '
        'frameQuery
        '
        Me.frameQuery.BackColor = System.Drawing.SystemColors.Control
        Me.frameQuery.Controls.Add(Me.chkCustomerNo)
        Me.frameQuery.Controls.Add(Me.chkUseAdjustments)
        Me.frameQuery.Controls.Add(Me.optOther)
        Me.frameQuery.Controls.Add(Me.cmdIcon)
        Me.frameQuery.Controls.Add(Me.optValidation)
        Me.frameQuery.Controls.Add(Me.txtSummarize)
        Me.frameQuery.Controls.Add(Me.cmdDeleteRule)
        Me.frameQuery.Controls.Add(Me.chkIncludeOCR)
        Me.frameQuery.Controls.Add(Me.chkUseAdjustmentsFinal)
        Me.frameQuery.Controls.Add(Me.cmbPatterns)
        Me.frameQuery.Controls.Add(Me.txtRuleSpecial)
        Me.frameQuery.Controls.Add(Me.txtRuleOrder)
        Me.frameQuery.Controls.Add(Me.txtRuleType)
        Me.frameQuery.Controls.Add(Me.txtRuleERPIndex)
        Me.frameQuery.Controls.Add(Me.txtRuleName)
        Me.frameQuery.Controls.Add(Me.cmdNewRule)
        Me.frameQuery.Controls.Add(Me.lstMatchingRules)
        Me.frameQuery.Controls.Add(Me.optManual)
        Me.frameQuery.Controls.Add(Me.optAfter)
        Me.frameQuery.Controls.Add(Me.optAutomatic)
        Me.frameQuery.Controls.Add(Me.chkProposeMatch)
        Me.frameQuery.Controls.Add(Me.imgIcon)
        Me.frameQuery.Controls.Add(Me.imgArrowDown)
        Me.frameQuery.Controls.Add(Me.imgArrowUp)
        Me.frameQuery.Controls.Add(Me.lblSummarize)
        Me.frameQuery.Controls.Add(Me.lblPattern)
        Me.frameQuery.Controls.Add(Me.lblRuleType)
        Me.frameQuery.Controls.Add(Me.lblRuleOrder)
        Me.frameQuery.Controls.Add(Me.lblRuleSpecial)
        Me.frameQuery.Controls.Add(Me.lblRuleName)
        Me.frameQuery.ForeColor = System.Drawing.SystemColors.ControlText
        Me.frameQuery.Location = New System.Drawing.Point(200, 49)
        Me.frameQuery.Name = "frameQuery"
        Me.frameQuery.Padding = New System.Windows.Forms.Padding(0)
        Me.frameQuery.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.frameQuery.Size = New System.Drawing.Size(506, 190)
        Me.frameQuery.TabIndex = 114
        Me.frameQuery.TabStop = False
        Me.frameQuery.Text = "60205 - Matching queries"
        '
        'chkCustomerNo
        '
        Me.chkCustomerNo.AutoSize = True
        Me.chkCustomerNo.BackColor = System.Drawing.SystemColors.Control
        Me.chkCustomerNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkCustomerNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkCustomerNo.Location = New System.Drawing.Point(15, 144)
        Me.chkCustomerNo.Name = "chkCustomerNo"
        Me.chkCustomerNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkCustomerNo.Size = New System.Drawing.Size(176, 17)
        Me.chkCustomerNo.TabIndex = 129
        Me.chkCustomerNo.Text = "60597 Customer number search"
        Me.chkCustomerNo.UseVisualStyleBackColor = False
        '
        'chkUseAdjustments
        '
        Me.chkUseAdjustments.AutoSize = True
        Me.chkUseAdjustments.BackColor = System.Drawing.SystemColors.Control
        Me.chkUseAdjustments.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUseAdjustments.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUseAdjustments.Location = New System.Drawing.Point(374, 167)
        Me.chkUseAdjustments.Name = "chkUseAdjustments"
        Me.chkUseAdjustments.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUseAdjustments.Size = New System.Drawing.Size(88, 17)
        Me.chkUseAdjustments.TabIndex = 128
        Me.chkUseAdjustments.Text = "60582 Adjust"
        Me.chkUseAdjustments.UseVisualStyleBackColor = False
        '
        'optOther
        '
        Me.optOther.BackColor = System.Drawing.SystemColors.Control
        Me.optOther.Cursor = System.Windows.Forms.Cursors.Default
        Me.optOther.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optOther.Location = New System.Drawing.Point(447, 24)
        Me.optOther.Name = "optOther"
        Me.optOther.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optOther.Size = New System.Drawing.Size(55, 17)
        Me.optOther.TabIndex = 13
        Me.optOther.Text = "60548-Other"
        Me.optOther.UseVisualStyleBackColor = False
        '
        'cmdIcon
        '
        Me.cmdIcon.BackColor = System.Drawing.SystemColors.Control
        Me.cmdIcon.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdIcon.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdIcon.Location = New System.Drawing.Point(460, 131)
        Me.cmdIcon.Name = "cmdIcon"
        Me.cmdIcon.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdIcon.Size = New System.Drawing.Size(25, 24)
        Me.cmdIcon.TabIndex = 123
        Me.cmdIcon.Text = "..."
        Me.cmdIcon.UseVisualStyleBackColor = False
        '
        'optValidation
        '
        Me.optValidation.BackColor = System.Drawing.SystemColors.Control
        Me.optValidation.Cursor = System.Windows.Forms.Cursors.Default
        Me.optValidation.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optValidation.Location = New System.Drawing.Point(376, 24)
        Me.optValidation.Name = "optValidation"
        Me.optValidation.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optValidation.Size = New System.Drawing.Size(100, 17)
        Me.optValidation.TabIndex = 12
        Me.optValidation.Text = "60464 Validate"
        Me.optValidation.UseVisualStyleBackColor = False
        '
        'txtSummarize
        '
        Me.txtSummarize.AcceptsReturn = True
        Me.txtSummarize.BackColor = System.Drawing.SystemColors.Window
        Me.txtSummarize.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSummarize.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSummarize.Location = New System.Drawing.Point(128, 104)
        Me.txtSummarize.MaxLength = 0
        Me.txtSummarize.Name = "txtSummarize"
        Me.txtSummarize.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSummarize.Size = New System.Drawing.Size(25, 20)
        Me.txtSummarize.TabIndex = 20
        '
        'cmdDeleteRule
        '
        Me.cmdDeleteRule.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDeleteRule.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDeleteRule.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDeleteRule.Location = New System.Drawing.Point(407, 78)
        Me.cmdDeleteRule.Name = "cmdDeleteRule"
        Me.cmdDeleteRule.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDeleteRule.Size = New System.Drawing.Size(81, 25)
        Me.cmdDeleteRule.TabIndex = 15
        Me.cmdDeleteRule.Text = "55015 Delete"
        Me.cmdDeleteRule.UseVisualStyleBackColor = False
        '
        'chkIncludeOCR
        '
        Me.chkIncludeOCR.BackColor = System.Drawing.SystemColors.Control
        Me.chkIncludeOCR.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkIncludeOCR.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkIncludeOCR.Location = New System.Drawing.Point(374, 107)
        Me.chkIncludeOCR.Name = "chkIncludeOCR"
        Me.chkIncludeOCR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkIncludeOCR.Size = New System.Drawing.Size(120, 17)
        Me.chkIncludeOCR.TabIndex = 22
        Me.chkIncludeOCR.Text = "60404 Incl.OCR"
        Me.chkIncludeOCR.UseVisualStyleBackColor = False
        '
        'chkUseAdjustmentsFinal
        '
        Me.chkUseAdjustmentsFinal.AutoSize = True
        Me.chkUseAdjustmentsFinal.BackColor = System.Drawing.SystemColors.Control
        Me.chkUseAdjustmentsFinal.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUseAdjustmentsFinal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUseAdjustmentsFinal.Location = New System.Drawing.Point(374, 147)
        Me.chkUseAdjustmentsFinal.Name = "chkUseAdjustmentsFinal"
        Me.chkUseAdjustmentsFinal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUseAdjustmentsFinal.Size = New System.Drawing.Size(88, 17)
        Me.chkUseAdjustmentsFinal.TabIndex = 24
        Me.chkUseAdjustmentsFinal.Text = "60402 Adjust"
        Me.chkUseAdjustmentsFinal.UseVisualStyleBackColor = False
        '
        'cmbPatterns
        '
        Me.cmbPatterns.BackColor = System.Drawing.SystemColors.Window
        Me.cmbPatterns.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbPatterns.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPatterns.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbPatterns.Location = New System.Drawing.Point(144, 142)
        Me.cmbPatterns.Name = "cmbPatterns"
        Me.cmbPatterns.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbPatterns.Size = New System.Drawing.Size(92, 21)
        Me.cmbPatterns.TabIndex = 19
        '
        'txtRuleSpecial
        '
        Me.txtRuleSpecial.AcceptsReturn = True
        Me.txtRuleSpecial.BackColor = System.Drawing.SystemColors.Window
        Me.txtRuleSpecial.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRuleSpecial.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRuleSpecial.Location = New System.Drawing.Point(310, 142)
        Me.txtRuleSpecial.MaxLength = 0
        Me.txtRuleSpecial.Name = "txtRuleSpecial"
        Me.txtRuleSpecial.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRuleSpecial.Size = New System.Drawing.Size(49, 20)
        Me.txtRuleSpecial.TabIndex = 21
        '
        'txtRuleOrder
        '
        Me.txtRuleOrder.AcceptsReturn = True
        Me.txtRuleOrder.BackColor = System.Drawing.SystemColors.Window
        Me.txtRuleOrder.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRuleOrder.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRuleOrder.Location = New System.Drawing.Point(310, 118)
        Me.txtRuleOrder.MaxLength = 0
        Me.txtRuleOrder.Name = "txtRuleOrder"
        Me.txtRuleOrder.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRuleOrder.Size = New System.Drawing.Size(25, 20)
        Me.txtRuleOrder.TabIndex = 17
        '
        'txtRuleType
        '
        Me.txtRuleType.AcceptsReturn = True
        Me.txtRuleType.BackColor = System.Drawing.SystemColors.Window
        Me.txtRuleType.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRuleType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRuleType.Location = New System.Drawing.Point(72, 142)
        Me.txtRuleType.MaxLength = 0
        Me.txtRuleType.Name = "txtRuleType"
        Me.txtRuleType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRuleType.Size = New System.Drawing.Size(21, 20)
        Me.txtRuleType.TabIndex = 18
        '
        'txtRuleERPIndex
        '
        Me.txtRuleERPIndex.AcceptsReturn = True
        Me.txtRuleERPIndex.BackColor = System.Drawing.SystemColors.Window
        Me.txtRuleERPIndex.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRuleERPIndex.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRuleERPIndex.Location = New System.Drawing.Point(472, 16)
        Me.txtRuleERPIndex.MaxLength = 0
        Me.txtRuleERPIndex.Name = "txtRuleERPIndex"
        Me.txtRuleERPIndex.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRuleERPIndex.Size = New System.Drawing.Size(17, 20)
        Me.txtRuleERPIndex.TabIndex = 8
        Me.txtRuleERPIndex.TabStop = False
        Me.txtRuleERPIndex.Visible = False
        '
        'txtRuleName
        '
        Me.txtRuleName.AcceptsReturn = True
        Me.txtRuleName.BackColor = System.Drawing.SystemColors.Window
        Me.txtRuleName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRuleName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRuleName.Location = New System.Drawing.Point(72, 118)
        Me.txtRuleName.MaxLength = 0
        Me.txtRuleName.Name = "txtRuleName"
        Me.txtRuleName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRuleName.Size = New System.Drawing.Size(164, 20)
        Me.txtRuleName.TabIndex = 16
        '
        'cmdNewRule
        '
        Me.cmdNewRule.BackColor = System.Drawing.SystemColors.Control
        Me.cmdNewRule.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdNewRule.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNewRule.Location = New System.Drawing.Point(407, 49)
        Me.cmdNewRule.Name = "cmdNewRule"
        Me.cmdNewRule.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdNewRule.Size = New System.Drawing.Size(81, 25)
        Me.cmdNewRule.TabIndex = 14
        Me.cmdNewRule.TabStop = False
        Me.cmdNewRule.Text = "55012 New"
        Me.cmdNewRule.UseVisualStyleBackColor = False
        '
        'lstMatchingRules
        '
        Me.lstMatchingRules.BackColor = System.Drawing.SystemColors.Window
        Me.lstMatchingRules.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstMatchingRules.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstMatchingRules.Location = New System.Drawing.Point(12, 48)
        Me.lstMatchingRules.Name = "lstMatchingRules"
        Me.lstMatchingRules.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstMatchingRules.Size = New System.Drawing.Size(345, 56)
        Me.lstMatchingRules.TabIndex = 13
        '
        'optManual
        '
        Me.optManual.BackColor = System.Drawing.SystemColors.Control
        Me.optManual.Cursor = System.Windows.Forms.Cursors.Default
        Me.optManual.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optManual.Location = New System.Drawing.Point(149, 24)
        Me.optManual.Name = "optManual"
        Me.optManual.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optManual.Size = New System.Drawing.Size(140, 17)
        Me.optManual.TabIndex = 10
        Me.optManual.Text = "60317 Man matching"
        Me.optManual.UseVisualStyleBackColor = False
        '
        'optAfter
        '
        Me.optAfter.BackColor = System.Drawing.SystemColors.Control
        Me.optAfter.Cursor = System.Windows.Forms.Cursors.Default
        Me.optAfter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optAfter.Location = New System.Drawing.Point(271, 24)
        Me.optAfter.Name = "optAfter"
        Me.optAfter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optAfter.Size = New System.Drawing.Size(105, 17)
        Me.optAfter.TabIndex = 11
        Me.optAfter.Text = "60318-After matching"
        Me.optAfter.UseVisualStyleBackColor = False
        '
        'optAutomatic
        '
        Me.optAutomatic.BackColor = System.Drawing.SystemColors.Control
        Me.optAutomatic.Cursor = System.Windows.Forms.Cursors.Default
        Me.optAutomatic.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optAutomatic.Location = New System.Drawing.Point(12, 24)
        Me.optAutomatic.Name = "optAutomatic"
        Me.optAutomatic.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optAutomatic.Size = New System.Drawing.Size(145, 17)
        Me.optAutomatic.TabIndex = 9
        Me.optAutomatic.Text = "60316-Auto matching"
        Me.optAutomatic.UseVisualStyleBackColor = False
        '
        'chkProposeMatch
        '
        Me.chkProposeMatch.AutoSize = True
        Me.chkProposeMatch.BackColor = System.Drawing.SystemColors.Control
        Me.chkProposeMatch.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkProposeMatch.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkProposeMatch.Location = New System.Drawing.Point(374, 127)
        Me.chkProposeMatch.Name = "chkProposeMatch"
        Me.chkProposeMatch.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkProposeMatch.Size = New System.Drawing.Size(98, 17)
        Me.chkProposeMatch.TabIndex = 23
        Me.chkProposeMatch.Text = "60403 Propose"
        Me.chkProposeMatch.UseVisualStyleBackColor = False
        '
        'imgIcon
        '
        Me.imgIcon.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgIcon.Image = CType(resources.GetObject("imgIcon.Image"), System.Drawing.Image)
        Me.imgIcon.Location = New System.Drawing.Point(439, 136)
        Me.imgIcon.Name = "imgIcon"
        Me.imgIcon.Size = New System.Drawing.Size(16, 16)
        Me.imgIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgIcon.TabIndex = 125
        Me.imgIcon.TabStop = False
        '
        'imgArrowDown
        '
        Me.imgArrowDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.imgArrowDown.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgArrowDown.Image = CType(resources.GetObject("imgArrowDown.Image"), System.Drawing.Image)
        Me.imgArrowDown.Location = New System.Drawing.Point(368, 76)
        Me.imgArrowDown.Name = "imgArrowDown"
        Me.imgArrowDown.Size = New System.Drawing.Size(17, 17)
        Me.imgArrowDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgArrowDown.TabIndex = 126
        Me.imgArrowDown.TabStop = False
        '
        'imgArrowUp
        '
        Me.imgArrowUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.imgArrowUp.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgArrowUp.Image = CType(resources.GetObject("imgArrowUp.Image"), System.Drawing.Image)
        Me.imgArrowUp.Location = New System.Drawing.Point(368, 59)
        Me.imgArrowUp.Name = "imgArrowUp"
        Me.imgArrowUp.Size = New System.Drawing.Size(17, 17)
        Me.imgArrowUp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgArrowUp.TabIndex = 127
        Me.imgArrowUp.TabStop = False
        '
        'lblSummarize
        '
        Me.lblSummarize.BackColor = System.Drawing.SystemColors.Control
        Me.lblSummarize.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSummarize.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSummarize.Location = New System.Drawing.Point(12, 104)
        Me.lblSummarize.Name = "lblSummarize"
        Me.lblSummarize.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSummarize.Size = New System.Drawing.Size(97, 17)
        Me.lblSummarize.TabIndex = 121
        Me.lblSummarize.Text = "60451 No. to deduct"
        '
        'lblPattern
        '
        Me.lblPattern.BackColor = System.Drawing.SystemColors.Control
        Me.lblPattern.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPattern.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPattern.Location = New System.Drawing.Point(100, 144)
        Me.lblPattern.Name = "lblPattern"
        Me.lblPattern.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPattern.Size = New System.Drawing.Size(49, 17)
        Me.lblPattern.TabIndex = 119
        Me.lblPattern.Text = "60319 InDes"
        '
        'lblRuleType
        '
        Me.lblRuleType.BackColor = System.Drawing.SystemColors.Control
        Me.lblRuleType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRuleType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRuleType.Location = New System.Drawing.Point(12, 144)
        Me.lblRuleType.Name = "lblRuleType"
        Me.lblRuleType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRuleType.Size = New System.Drawing.Size(81, 17)
        Me.lblRuleType.TabIndex = 118
        Me.lblRuleType.Text = "60217 - Type"
        '
        'lblRuleOrder
        '
        Me.lblRuleOrder.BackColor = System.Drawing.SystemColors.Control
        Me.lblRuleOrder.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRuleOrder.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRuleOrder.Location = New System.Drawing.Point(240, 120)
        Me.lblRuleOrder.Name = "lblRuleOrder"
        Me.lblRuleOrder.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRuleOrder.Size = New System.Drawing.Size(81, 17)
        Me.lblRuleOrder.TabIndex = 117
        Me.lblRuleOrder.Text = "60218 - Order"
        '
        'lblRuleSpecial
        '
        Me.lblRuleSpecial.BackColor = System.Drawing.SystemColors.Control
        Me.lblRuleSpecial.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRuleSpecial.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRuleSpecial.Location = New System.Drawing.Point(240, 144)
        Me.lblRuleSpecial.Name = "lblRuleSpecial"
        Me.lblRuleSpecial.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRuleSpecial.Size = New System.Drawing.Size(73, 17)
        Me.lblRuleSpecial.TabIndex = 116
        Me.lblRuleSpecial.Text = "60320-Special"
        '
        'lblRuleName
        '
        Me.lblRuleName.BackColor = System.Drawing.SystemColors.Control
        Me.lblRuleName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRuleName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRuleName.Location = New System.Drawing.Point(12, 120)
        Me.lblRuleName.Name = "lblRuleName"
        Me.lblRuleName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRuleName.Size = New System.Drawing.Size(81, 17)
        Me.lblRuleName.TabIndex = 115
        Me.lblRuleName.Text = "60230 - Name"
        '
        '_frame1_2
        '
        Me._frame1_2.BackColor = System.Drawing.SystemColors.Control
        Me._frame1_2.Controls.Add(Me.framePayInvoice)
        Me._frame1_2.Controls.Add(Me.framePayPayment)
        Me._frame1_2.Controls.Add(Me.framePayStatement)
        Me._frame1_2.Cursor = System.Windows.Forms.Cursors.Default
        Me._frame1_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.frame1.SetIndex(Me._frame1_2, CType(2, Short))
        Me._frame1_2.Location = New System.Drawing.Point(200, 256)
        Me._frame1_2.Name = "_frame1_2"
        Me._frame1_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._frame1_2.Size = New System.Drawing.Size(735, 255)
        Me._frame1_2.TabIndex = 44
        Me._frame1_2.Text = "Tab3 - frame"
        Me._frame1_2.Visible = False
        '
        'framePayInvoice
        '
        Me.framePayInvoice.BackColor = System.Drawing.SystemColors.Control
        Me.framePayInvoice.Controls.Add(Me.txtPayInvAmount)
        Me.framePayInvoice.Controls.Add(Me.txtPayText)
        Me.framePayInvoice.Controls.Add(Me.cmdPayInvDesc)
        Me.framePayInvoice.Controls.Add(Me.lblPayInvAMount)
        Me.framePayInvoice.Controls.Add(Me.lblPayText)
        Me.framePayInvoice.ForeColor = System.Drawing.SystemColors.ControlText
        Me.framePayInvoice.Location = New System.Drawing.Point(0, 136)
        Me.framePayInvoice.Name = "framePayInvoice"
        Me.framePayInvoice.Padding = New System.Windows.Forms.Padding(0)
        Me.framePayInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.framePayInvoice.Size = New System.Drawing.Size(721, 89)
        Me.framePayInvoice.TabIndex = 103
        Me.framePayInvoice.TabStop = False
        Me.framePayInvoice.Text = "60215 - Invoice"
        '
        'txtPayInvAmount
        '
        Me.txtPayInvAmount.AcceptsReturn = True
        Me.txtPayInvAmount.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayInvAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayInvAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayInvAmount.Location = New System.Drawing.Point(424, 18)
        Me.txtPayInvAmount.MaxLength = 0
        Me.txtPayInvAmount.Name = "txtPayInvAmount"
        Me.txtPayInvAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayInvAmount.Size = New System.Drawing.Size(105, 20)
        Me.txtPayInvAmount.TabIndex = 108
        Me.txtPayInvAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPayText
        '
        Me.txtPayText.AcceptsReturn = True
        Me.txtPayText.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayText.Location = New System.Drawing.Point(80, 18)
        Me.txtPayText.MaxLength = 0
        Me.txtPayText.Multiline = True
        Me.txtPayText.Name = "txtPayText"
        Me.txtPayText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtPayText.Size = New System.Drawing.Size(257, 64)
        Me.txtPayText.TabIndex = 105
        '
        'cmdPayInvDesc
        '
        Me.cmdPayInvDesc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdPayInvDesc.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdPayInvDesc.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPayInvDesc.Location = New System.Drawing.Point(600, 48)
        Me.cmdPayInvDesc.Name = "cmdPayInvDesc"
        Me.cmdPayInvDesc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdPayInvDesc.Size = New System.Drawing.Size(105, 21)
        Me.cmdPayInvDesc.TabIndex = 104
        Me.cmdPayInvDesc.Text = "60152 - Invdesc"
        Me.cmdPayInvDesc.UseVisualStyleBackColor = False
        '
        'lblPayInvAMount
        '
        Me.lblPayInvAMount.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayInvAMount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayInvAMount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayInvAMount.Location = New System.Drawing.Point(360, 22)
        Me.lblPayInvAMount.Name = "lblPayInvAMount"
        Me.lblPayInvAMount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayInvAMount.Size = New System.Drawing.Size(73, 17)
        Me.lblPayInvAMount.TabIndex = 107
        Me.lblPayInvAMount.Text = "60211 -Amount"
        '
        'lblPayText
        '
        Me.lblPayText.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayText.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayText.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayText.Location = New System.Drawing.Point(8, 22)
        Me.lblPayText.Name = "lblPayText"
        Me.lblPayText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayText.Size = New System.Drawing.Size(65, 41)
        Me.lblPayText.TabIndex = 106
        Me.lblPayText.Text = "60210 - Text"
        '
        'framePayPayment
        '
        Me.framePayPayment.BackColor = System.Drawing.SystemColors.Control
        Me.framePayPayment.Controls.Add(Me.txtExchRate)
        Me.framePayPayment.Controls.Add(Me.txtAmount_NOK)
        Me.framePayPayment.Controls.Add(Me.txtPayName)
        Me.framePayPayment.Controls.Add(Me.txtPayAdr1)
        Me.framePayPayment.Controls.Add(Me.txtPayZip)
        Me.framePayPayment.Controls.Add(Me.txtPayCity)
        Me.framePayPayment.Controls.Add(Me.txtPayCountry)
        Me.framePayPayment.Controls.Add(Me.txtPayAmount)
        Me.framePayPayment.Controls.Add(Me.txtPayISO)
        Me.framePayPayment.Controls.Add(Me.txtPayAccount)
        Me.framePayPayment.Controls.Add(Me.txtPayRef)
        Me.framePayPayment.Controls.Add(Me.lblExchRate)
        Me.framePayPayment.Controls.Add(Me.lblAmount_NOK)
        Me.framePayPayment.Controls.Add(Me.lblPayName)
        Me.framePayPayment.Controls.Add(Me.lblPayAdr1)
        Me.framePayPayment.Controls.Add(Me.lblPayZip)
        Me.framePayPayment.Controls.Add(Me.lblPayCountry)
        Me.framePayPayment.Controls.Add(Me.lblPayAmount)
        Me.framePayPayment.Controls.Add(Me.lblPayAccount)
        Me.framePayPayment.Controls.Add(Me.lblPayRef)
        Me.framePayPayment.Controls.Add(Me.lblPayISO)
        Me.framePayPayment.ForeColor = System.Drawing.SystemColors.ControlText
        Me.framePayPayment.Location = New System.Drawing.Point(0, 44)
        Me.framePayPayment.Name = "framePayPayment"
        Me.framePayPayment.Padding = New System.Windows.Forms.Padding(0)
        Me.framePayPayment.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.framePayPayment.Size = New System.Drawing.Size(721, 93)
        Me.framePayPayment.TabIndex = 85
        Me.framePayPayment.TabStop = False
        Me.framePayPayment.Text = "60214 - Payment"
        '
        'txtExchRate
        '
        Me.txtExchRate.AcceptsReturn = True
        Me.txtExchRate.BackColor = System.Drawing.SystemColors.Window
        Me.txtExchRate.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtExchRate.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtExchRate.Location = New System.Drawing.Point(600, 66)
        Me.txtExchRate.MaxLength = 0
        Me.txtExchRate.Name = "txtExchRate"
        Me.txtExchRate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtExchRate.Size = New System.Drawing.Size(105, 20)
        Me.txtExchRate.TabIndex = 113
        '
        'txtAmount_NOK
        '
        Me.txtAmount_NOK.AcceptsReturn = True
        Me.txtAmount_NOK.BackColor = System.Drawing.SystemColors.Window
        Me.txtAmount_NOK.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAmount_NOK.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAmount_NOK.Location = New System.Drawing.Point(424, 64)
        Me.txtAmount_NOK.MaxLength = 0
        Me.txtAmount_NOK.Name = "txtAmount_NOK"
        Me.txtAmount_NOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAmount_NOK.Size = New System.Drawing.Size(105, 20)
        Me.txtAmount_NOK.TabIndex = 112
        Me.txtAmount_NOK.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPayName
        '
        Me.txtPayName.AcceptsReturn = True
        Me.txtPayName.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayName.Location = New System.Drawing.Point(80, 18)
        Me.txtPayName.MaxLength = 0
        Me.txtPayName.Name = "txtPayName"
        Me.txtPayName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayName.Size = New System.Drawing.Size(257, 20)
        Me.txtPayName.TabIndex = 94
        '
        'txtPayAdr1
        '
        Me.txtPayAdr1.AcceptsReturn = True
        Me.txtPayAdr1.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayAdr1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayAdr1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayAdr1.Location = New System.Drawing.Point(80, 42)
        Me.txtPayAdr1.MaxLength = 0
        Me.txtPayAdr1.Name = "txtPayAdr1"
        Me.txtPayAdr1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayAdr1.Size = New System.Drawing.Size(257, 20)
        Me.txtPayAdr1.TabIndex = 93
        '
        'txtPayZip
        '
        Me.txtPayZip.AcceptsReturn = True
        Me.txtPayZip.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayZip.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayZip.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayZip.Location = New System.Drawing.Point(80, 66)
        Me.txtPayZip.MaxLength = 0
        Me.txtPayZip.Name = "txtPayZip"
        Me.txtPayZip.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayZip.Size = New System.Drawing.Size(41, 20)
        Me.txtPayZip.TabIndex = 92
        '
        'txtPayCity
        '
        Me.txtPayCity.AcceptsReturn = True
        Me.txtPayCity.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayCity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayCity.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayCity.Location = New System.Drawing.Point(128, 66)
        Me.txtPayCity.MaxLength = 0
        Me.txtPayCity.Name = "txtPayCity"
        Me.txtPayCity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayCity.Size = New System.Drawing.Size(113, 20)
        Me.txtPayCity.TabIndex = 91
        '
        'txtPayCountry
        '
        Me.txtPayCountry.AcceptsReturn = True
        Me.txtPayCountry.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayCountry.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayCountry.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayCountry.Location = New System.Drawing.Point(296, 66)
        Me.txtPayCountry.MaxLength = 0
        Me.txtPayCountry.Name = "txtPayCountry"
        Me.txtPayCountry.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayCountry.Size = New System.Drawing.Size(41, 20)
        Me.txtPayCountry.TabIndex = 90
        '
        'txtPayAmount
        '
        Me.txtPayAmount.AcceptsReturn = True
        Me.txtPayAmount.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayAmount.Location = New System.Drawing.Point(424, 18)
        Me.txtPayAmount.MaxLength = 0
        Me.txtPayAmount.Name = "txtPayAmount"
        Me.txtPayAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayAmount.Size = New System.Drawing.Size(105, 20)
        Me.txtPayAmount.TabIndex = 89
        Me.txtPayAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPayISO
        '
        Me.txtPayISO.AcceptsReturn = True
        Me.txtPayISO.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayISO.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayISO.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayISO.Location = New System.Drawing.Point(600, 18)
        Me.txtPayISO.MaxLength = 0
        Me.txtPayISO.Name = "txtPayISO"
        Me.txtPayISO.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayISO.Size = New System.Drawing.Size(105, 20)
        Me.txtPayISO.TabIndex = 88
        '
        'txtPayAccount
        '
        Me.txtPayAccount.AcceptsReturn = True
        Me.txtPayAccount.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayAccount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayAccount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayAccount.Location = New System.Drawing.Point(424, 42)
        Me.txtPayAccount.MaxLength = 0
        Me.txtPayAccount.Name = "txtPayAccount"
        Me.txtPayAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayAccount.Size = New System.Drawing.Size(105, 20)
        Me.txtPayAccount.TabIndex = 87
        '
        'txtPayRef
        '
        Me.txtPayRef.AcceptsReturn = True
        Me.txtPayRef.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayRef.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayRef.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayRef.Location = New System.Drawing.Point(600, 42)
        Me.txtPayRef.MaxLength = 0
        Me.txtPayRef.Name = "txtPayRef"
        Me.txtPayRef.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayRef.Size = New System.Drawing.Size(105, 20)
        Me.txtPayRef.TabIndex = 86
        '
        'lblExchRate
        '
        Me.lblExchRate.BackColor = System.Drawing.SystemColors.Control
        Me.lblExchRate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExchRate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExchRate.Location = New System.Drawing.Point(536, 70)
        Me.lblExchRate.Name = "lblExchRate"
        Me.lblExchRate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExchRate.Size = New System.Drawing.Size(105, 17)
        Me.lblExchRate.TabIndex = 111
        Me.lblExchRate.Text = "60297 - ExchRate"
        '
        'lblAmount_NOK
        '
        Me.lblAmount_NOK.BackColor = System.Drawing.SystemColors.Control
        Me.lblAmount_NOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAmount_NOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAmount_NOK.Location = New System.Drawing.Point(360, 70)
        Me.lblAmount_NOK.Name = "lblAmount_NOK"
        Me.lblAmount_NOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAmount_NOK.Size = New System.Drawing.Size(161, 17)
        Me.lblAmount_NOK.TabIndex = 110
        Me.lblAmount_NOK.Text = "60301 - Amount_NOK"
        '
        'lblPayName
        '
        Me.lblPayName.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayName.Location = New System.Drawing.Point(8, 22)
        Me.lblPayName.Name = "lblPayName"
        Me.lblPayName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayName.Size = New System.Drawing.Size(73, 17)
        Me.lblPayName.TabIndex = 102
        Me.lblPayName.Text = "60206 - Name"
        '
        'lblPayAdr1
        '
        Me.lblPayAdr1.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayAdr1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayAdr1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayAdr1.Location = New System.Drawing.Point(8, 46)
        Me.lblPayAdr1.Name = "lblPayAdr1"
        Me.lblPayAdr1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayAdr1.Size = New System.Drawing.Size(73, 17)
        Me.lblPayAdr1.TabIndex = 101
        Me.lblPayAdr1.Text = "60207 - Adr1"
        '
        'lblPayZip
        '
        Me.lblPayZip.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayZip.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayZip.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayZip.Location = New System.Drawing.Point(8, 69)
        Me.lblPayZip.Name = "lblPayZip"
        Me.lblPayZip.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayZip.Size = New System.Drawing.Size(97, 17)
        Me.lblPayZip.TabIndex = 100
        Me.lblPayZip.Text = "60299 - Zip/City:"
        '
        'lblPayCountry
        '
        Me.lblPayCountry.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayCountry.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayCountry.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayCountry.Location = New System.Drawing.Point(248, 69)
        Me.lblPayCountry.Name = "lblPayCountry"
        Me.lblPayCountry.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayCountry.Size = New System.Drawing.Size(57, 17)
        Me.lblPayCountry.TabIndex = 99
        Me.lblPayCountry.Text = "60209 -Country"
        '
        'lblPayAmount
        '
        Me.lblPayAmount.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayAmount.Location = New System.Drawing.Point(360, 22)
        Me.lblPayAmount.Name = "lblPayAmount"
        Me.lblPayAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayAmount.Size = New System.Drawing.Size(73, 17)
        Me.lblPayAmount.TabIndex = 98
        Me.lblPayAmount.Text = "60211 -Amount"
        '
        'lblPayAccount
        '
        Me.lblPayAccount.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayAccount.Location = New System.Drawing.Point(360, 46)
        Me.lblPayAccount.Name = "lblPayAccount"
        Me.lblPayAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayAccount.Size = New System.Drawing.Size(73, 17)
        Me.lblPayAccount.TabIndex = 97
        Me.lblPayAccount.Text = "60298 - Acc.:"
        '
        'lblPayRef
        '
        Me.lblPayRef.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayRef.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayRef.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayRef.Location = New System.Drawing.Point(536, 46)
        Me.lblPayRef.Name = "lblPayRef"
        Me.lblPayRef.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayRef.Size = New System.Drawing.Size(73, 17)
        Me.lblPayRef.TabIndex = 96
        Me.lblPayRef.Text = "60213 - Ref"
        '
        'lblPayISO
        '
        Me.lblPayISO.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayISO.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayISO.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayISO.Location = New System.Drawing.Point(536, 22)
        Me.lblPayISO.Name = "lblPayISO"
        Me.lblPayISO.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayISO.Size = New System.Drawing.Size(73, 17)
        Me.lblPayISO.TabIndex = 95
        Me.lblPayISO.Text = "60212 - ISO"
        '
        'framePayStatement
        '
        Me.framePayStatement.BackColor = System.Drawing.SystemColors.Control
        Me.framePayStatement.Controls.Add(Me.txtPayStateRef)
        Me.framePayStatement.Controls.Add(Me.txtPayStateAmount)
        Me.framePayStatement.Controls.Add(Me.txtPayBookdate)
        Me.framePayStatement.Controls.Add(Me.txtPayCreditAccount)
        Me.framePayStatement.Controls.Add(Me.lblPayBookdate)
        Me.framePayStatement.Controls.Add(Me.lblPayStateAmount)
        Me.framePayStatement.Controls.Add(Me.lblPayStateRef)
        Me.framePayStatement.Controls.Add(Me.lblPayCreditAccount)
        Me.framePayStatement.ForeColor = System.Drawing.SystemColors.ControlText
        Me.framePayStatement.Location = New System.Drawing.Point(0, 0)
        Me.framePayStatement.Name = "framePayStatement"
        Me.framePayStatement.Padding = New System.Windows.Forms.Padding(0)
        Me.framePayStatement.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.framePayStatement.Size = New System.Drawing.Size(721, 45)
        Me.framePayStatement.TabIndex = 76
        Me.framePayStatement.TabStop = False
        Me.framePayStatement.Text = "60208 - Statement"
        '
        'txtPayStateRef
        '
        Me.txtPayStateRef.AcceptsReturn = True
        Me.txtPayStateRef.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayStateRef.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayStateRef.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayStateRef.Location = New System.Drawing.Point(600, 18)
        Me.txtPayStateRef.MaxLength = 0
        Me.txtPayStateRef.Name = "txtPayStateRef"
        Me.txtPayStateRef.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayStateRef.Size = New System.Drawing.Size(105, 20)
        Me.txtPayStateRef.TabIndex = 84
        Me.txtPayStateRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPayStateAmount
        '
        Me.txtPayStateAmount.AcceptsReturn = True
        Me.txtPayStateAmount.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayStateAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayStateAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayStateAmount.Location = New System.Drawing.Point(424, 18)
        Me.txtPayStateAmount.MaxLength = 0
        Me.txtPayStateAmount.Name = "txtPayStateAmount"
        Me.txtPayStateAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayStateAmount.Size = New System.Drawing.Size(105, 20)
        Me.txtPayStateAmount.TabIndex = 83
        Me.txtPayStateAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPayBookdate
        '
        Me.txtPayBookdate.AcceptsReturn = True
        Me.txtPayBookdate.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayBookdate.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayBookdate.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayBookdate.Location = New System.Drawing.Point(264, 18)
        Me.txtPayBookdate.MaxLength = 0
        Me.txtPayBookdate.Name = "txtPayBookdate"
        Me.txtPayBookdate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayBookdate.Size = New System.Drawing.Size(73, 20)
        Me.txtPayBookdate.TabIndex = 82
        Me.txtPayBookdate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPayCreditAccount
        '
        Me.txtPayCreditAccount.AcceptsReturn = True
        Me.txtPayCreditAccount.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayCreditAccount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayCreditAccount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayCreditAccount.Location = New System.Drawing.Point(80, 18)
        Me.txtPayCreditAccount.MaxLength = 0
        Me.txtPayCreditAccount.Name = "txtPayCreditAccount"
        Me.txtPayCreditAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayCreditAccount.Size = New System.Drawing.Size(121, 20)
        Me.txtPayCreditAccount.TabIndex = 78
        '
        'lblPayBookdate
        '
        Me.lblPayBookdate.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayBookdate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayBookdate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayBookdate.Location = New System.Drawing.Point(208, 22)
        Me.lblPayBookdate.Name = "lblPayBookdate"
        Me.lblPayBookdate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayBookdate.Size = New System.Drawing.Size(97, 17)
        Me.lblPayBookdate.TabIndex = 81
        Me.lblPayBookdate.Text = "60257 Bookdate"
        '
        'lblPayStateAmount
        '
        Me.lblPayStateAmount.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayStateAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayStateAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayStateAmount.Location = New System.Drawing.Point(360, 22)
        Me.lblPayStateAmount.Name = "lblPayStateAmount"
        Me.lblPayStateAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayStateAmount.Size = New System.Drawing.Size(73, 17)
        Me.lblPayStateAmount.TabIndex = 80
        Me.lblPayStateAmount.Text = "60211 Amount"
        '
        'lblPayStateRef
        '
        Me.lblPayStateRef.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayStateRef.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayStateRef.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayStateRef.Location = New System.Drawing.Point(536, 22)
        Me.lblPayStateRef.Name = "lblPayStateRef"
        Me.lblPayStateRef.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayStateRef.Size = New System.Drawing.Size(73, 17)
        Me.lblPayStateRef.TabIndex = 79
        Me.lblPayStateRef.Text = "60213 Ref"
        '
        'lblPayCreditAccount
        '
        Me.lblPayCreditAccount.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayCreditAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayCreditAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayCreditAccount.Location = New System.Drawing.Point(8, 22)
        Me.lblPayCreditAccount.Name = "lblPayCreditAccount"
        Me.lblPayCreditAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayCreditAccount.Size = New System.Drawing.Size(73, 17)
        Me.lblPayCreditAccount.TabIndex = 77
        Me.lblPayCreditAccount.Text = "60256 RecAcc"
        '
        'cmdEnd
        '
        Me.cmdEnd.BackColor = System.Drawing.SystemColors.Control
        Me.cmdEnd.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdEnd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdEnd.Location = New System.Drawing.Point(848, 664)
        Me.cmdEnd.Name = "cmdEnd"
        Me.cmdEnd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdEnd.Size = New System.Drawing.Size(89, 21)
        Me.cmdEnd.TabIndex = 39
        Me.cmdEnd.Text = "55060 - Avslutt"
        Me.cmdEnd.UseVisualStyleBackColor = False
        '
        'frameResult
        '
        Me.frameResult.BackColor = System.Drawing.SystemColors.Control
        Me.frameResult.Controls.Add(Me.gridResult)
        Me.frameResult.ForeColor = System.Drawing.SystemColors.ControlText
        Me.frameResult.Location = New System.Drawing.Point(200, 520)
        Me.frameResult.Name = "frameResult"
        Me.frameResult.Padding = New System.Windows.Forms.Padding(0)
        Me.frameResult.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.frameResult.Size = New System.Drawing.Size(735, 129)
        Me.frameResult.TabIndex = 45
        Me.frameResult.TabStop = False
        Me.frameResult.Text = "60216 - Result"
        '
        'gridResult
        '
        Me.gridResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridResult.Location = New System.Drawing.Point(15, 20)
        Me.gridResult.Name = "gridResult"
        Me.gridResult.Size = New System.Drawing.Size(706, 100)
        Me.gridResult.TabIndex = 47
        '
        'frameParameters
        '
        Me.frameParameters.BackColor = System.Drawing.SystemColors.Control
        Me.frameParameters.Controls.Add(Me.txtParaDivLabel)
        Me.frameParameters.Controls.Add(Me.txtParaDiv)
        Me.frameParameters.Controls.Add(Me.txtParaMyText)
        Me.frameParameters.Controls.Add(Me.txtParaCurrency)
        Me.frameParameters.Controls.Add(Me.txtParaClient)
        Me.frameParameters.Controls.Add(Me.txtParaAccount)
        Me.frameParameters.Controls.Add(Me.txtParaName)
        Me.frameParameters.Controls.Add(Me.txtParaCustomer)
        Me.frameParameters.Controls.Add(Me.txtParaInvoice)
        Me.frameParameters.Controls.Add(Me.txtParaAmount)
        Me.frameParameters.Controls.Add(Me.lblParaMyText)
        Me.frameParameters.Controls.Add(Me.lblParaCurrency)
        Me.frameParameters.Controls.Add(Me.lblParaClient)
        Me.frameParameters.Controls.Add(Me.lblParaAccount)
        Me.frameParameters.Controls.Add(Me.lblParaName)
        Me.frameParameters.Controls.Add(Me.lblParaCustomer)
        Me.frameParameters.Controls.Add(Me.lblParaInvoice)
        Me.frameParameters.Controls.Add(Me.lblParaAmount)
        Me.frameParameters.ForeColor = System.Drawing.SystemColors.ControlText
        Me.frameParameters.Location = New System.Drawing.Point(712, 49)
        Me.frameParameters.Name = "frameParameters"
        Me.frameParameters.Padding = New System.Windows.Forms.Padding(0)
        Me.frameParameters.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.frameParameters.Size = New System.Drawing.Size(225, 190)
        Me.frameParameters.TabIndex = 40
        Me.frameParameters.TabStop = False
        Me.frameParameters.Text = "60222 - Parameters"
        '
        'txtParaDivLabel
        '
        Me.txtParaDivLabel.AcceptsReturn = True
        Me.txtParaDivLabel.BackColor = System.Drawing.SystemColors.Control
        Me.txtParaDivLabel.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtParaDivLabel.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtParaDivLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtParaDivLabel.Location = New System.Drawing.Point(12, 156)
        Me.txtParaDivLabel.MaxLength = 0
        Me.txtParaDivLabel.Name = "txtParaDivLabel"
        Me.txtParaDivLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtParaDivLabel.Size = New System.Drawing.Size(105, 13)
        Me.txtParaDivLabel.TabIndex = 37
        Me.txtParaDivLabel.Text = "BB_"
        '
        'txtParaDiv
        '
        Me.txtParaDiv.AcceptsReturn = True
        Me.txtParaDiv.BackColor = System.Drawing.SystemColors.Window
        Me.txtParaDiv.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtParaDiv.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtParaDiv.Location = New System.Drawing.Point(119, 156)
        Me.txtParaDiv.MaxLength = 0
        Me.txtParaDiv.Name = "txtParaDiv"
        Me.txtParaDiv.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtParaDiv.Size = New System.Drawing.Size(97, 20)
        Me.txtParaDiv.TabIndex = 38
        '
        'txtParaMyText
        '
        Me.txtParaMyText.AcceptsReturn = True
        Me.txtParaMyText.BackColor = System.Drawing.SystemColors.Window
        Me.txtParaMyText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtParaMyText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtParaMyText.Location = New System.Drawing.Point(119, 139)
        Me.txtParaMyText.MaxLength = 0
        Me.txtParaMyText.Name = "txtParaMyText"
        Me.txtParaMyText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtParaMyText.Size = New System.Drawing.Size(97, 20)
        Me.txtParaMyText.TabIndex = 36
        '
        'txtParaCurrency
        '
        Me.txtParaCurrency.AcceptsReturn = True
        Me.txtParaCurrency.BackColor = System.Drawing.SystemColors.Window
        Me.txtParaCurrency.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtParaCurrency.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtParaCurrency.Location = New System.Drawing.Point(119, 122)
        Me.txtParaCurrency.MaxLength = 0
        Me.txtParaCurrency.Name = "txtParaCurrency"
        Me.txtParaCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtParaCurrency.Size = New System.Drawing.Size(97, 20)
        Me.txtParaCurrency.TabIndex = 35
        '
        'txtParaClient
        '
        Me.txtParaClient.AcceptsReturn = True
        Me.txtParaClient.BackColor = System.Drawing.SystemColors.Window
        Me.txtParaClient.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtParaClient.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtParaClient.Location = New System.Drawing.Point(119, 105)
        Me.txtParaClient.MaxLength = 0
        Me.txtParaClient.Name = "txtParaClient"
        Me.txtParaClient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtParaClient.Size = New System.Drawing.Size(97, 20)
        Me.txtParaClient.TabIndex = 34
        '
        'txtParaAccount
        '
        Me.txtParaAccount.AcceptsReturn = True
        Me.txtParaAccount.BackColor = System.Drawing.SystemColors.Window
        Me.txtParaAccount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtParaAccount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtParaAccount.Location = New System.Drawing.Point(119, 88)
        Me.txtParaAccount.MaxLength = 0
        Me.txtParaAccount.Name = "txtParaAccount"
        Me.txtParaAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtParaAccount.Size = New System.Drawing.Size(97, 20)
        Me.txtParaAccount.TabIndex = 33
        '
        'txtParaName
        '
        Me.txtParaName.AcceptsReturn = True
        Me.txtParaName.BackColor = System.Drawing.SystemColors.Window
        Me.txtParaName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtParaName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtParaName.Location = New System.Drawing.Point(119, 71)
        Me.txtParaName.MaxLength = 0
        Me.txtParaName.Name = "txtParaName"
        Me.txtParaName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtParaName.Size = New System.Drawing.Size(97, 20)
        Me.txtParaName.TabIndex = 32
        '
        'txtParaCustomer
        '
        Me.txtParaCustomer.AcceptsReturn = True
        Me.txtParaCustomer.BackColor = System.Drawing.SystemColors.Window
        Me.txtParaCustomer.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtParaCustomer.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtParaCustomer.Location = New System.Drawing.Point(119, 54)
        Me.txtParaCustomer.MaxLength = 0
        Me.txtParaCustomer.Name = "txtParaCustomer"
        Me.txtParaCustomer.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtParaCustomer.Size = New System.Drawing.Size(97, 20)
        Me.txtParaCustomer.TabIndex = 31
        '
        'txtParaInvoice
        '
        Me.txtParaInvoice.AcceptsReturn = True
        Me.txtParaInvoice.BackColor = System.Drawing.SystemColors.Window
        Me.txtParaInvoice.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtParaInvoice.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtParaInvoice.Location = New System.Drawing.Point(119, 37)
        Me.txtParaInvoice.MaxLength = 0
        Me.txtParaInvoice.Name = "txtParaInvoice"
        Me.txtParaInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtParaInvoice.Size = New System.Drawing.Size(97, 20)
        Me.txtParaInvoice.TabIndex = 30
        '
        'txtParaAmount
        '
        Me.txtParaAmount.AcceptsReturn = True
        Me.txtParaAmount.BackColor = System.Drawing.SystemColors.Window
        Me.txtParaAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtParaAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtParaAmount.Location = New System.Drawing.Point(119, 20)
        Me.txtParaAmount.MaxLength = 0
        Me.txtParaAmount.Name = "txtParaAmount"
        Me.txtParaAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtParaAmount.Size = New System.Drawing.Size(97, 20)
        Me.txtParaAmount.TabIndex = 29
        '
        'lblParaMyText
        '
        Me.lblParaMyText.BackColor = System.Drawing.SystemColors.Control
        Me.lblParaMyText.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblParaMyText.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblParaMyText.Location = New System.Drawing.Point(9, 139)
        Me.lblParaMyText.Name = "lblParaMyText"
        Me.lblParaMyText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblParaMyText.Size = New System.Drawing.Size(105, 17)
        Me.lblParaMyText.TabIndex = 0
        Me.lblParaMyText.Text = "BB_MyText"
        '
        'lblParaCurrency
        '
        Me.lblParaCurrency.BackColor = System.Drawing.SystemColors.Control
        Me.lblParaCurrency.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblParaCurrency.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblParaCurrency.Location = New System.Drawing.Point(10, 122)
        Me.lblParaCurrency.Name = "lblParaCurrency"
        Me.lblParaCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblParaCurrency.Size = New System.Drawing.Size(105, 17)
        Me.lblParaCurrency.TabIndex = 1
        Me.lblParaCurrency.Text = "BB_Currency"
        '
        'lblParaClient
        '
        Me.lblParaClient.BackColor = System.Drawing.SystemColors.Control
        Me.lblParaClient.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblParaClient.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblParaClient.Location = New System.Drawing.Point(10, 105)
        Me.lblParaClient.Name = "lblParaClient"
        Me.lblParaClient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblParaClient.Size = New System.Drawing.Size(105, 17)
        Me.lblParaClient.TabIndex = 2
        Me.lblParaClient.Text = "BB_ClientNo"
        '
        'lblParaAccount
        '
        Me.lblParaAccount.BackColor = System.Drawing.SystemColors.Control
        Me.lblParaAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblParaAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblParaAccount.Location = New System.Drawing.Point(10, 88)
        Me.lblParaAccount.Name = "lblParaAccount"
        Me.lblParaAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblParaAccount.Size = New System.Drawing.Size(105, 17)
        Me.lblParaAccount.TabIndex = 3
        Me.lblParaAccount.Text = "BB_AccountNo"
        '
        'lblParaName
        '
        Me.lblParaName.BackColor = System.Drawing.SystemColors.Control
        Me.lblParaName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblParaName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblParaName.Location = New System.Drawing.Point(10, 71)
        Me.lblParaName.Name = "lblParaName"
        Me.lblParaName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblParaName.Size = New System.Drawing.Size(105, 17)
        Me.lblParaName.TabIndex = 4
        Me.lblParaName.Text = "BB_Name"
        '
        'lblParaCustomer
        '
        Me.lblParaCustomer.BackColor = System.Drawing.SystemColors.Control
        Me.lblParaCustomer.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblParaCustomer.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblParaCustomer.Location = New System.Drawing.Point(10, 54)
        Me.lblParaCustomer.Name = "lblParaCustomer"
        Me.lblParaCustomer.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblParaCustomer.Size = New System.Drawing.Size(105, 17)
        Me.lblParaCustomer.TabIndex = 5
        Me.lblParaCustomer.Text = "BB_CustomerNo"
        '
        'lblParaInvoice
        '
        Me.lblParaInvoice.BackColor = System.Drawing.SystemColors.Control
        Me.lblParaInvoice.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblParaInvoice.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblParaInvoice.Location = New System.Drawing.Point(10, 37)
        Me.lblParaInvoice.Name = "lblParaInvoice"
        Me.lblParaInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblParaInvoice.Size = New System.Drawing.Size(105, 17)
        Me.lblParaInvoice.TabIndex = 6
        Me.lblParaInvoice.Text = "BB_InvoiceIdentifier"
        '
        'lblParaAmount
        '
        Me.lblParaAmount.BackColor = System.Drawing.SystemColors.Control
        Me.lblParaAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblParaAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblParaAmount.Location = New System.Drawing.Point(10, 21)
        Me.lblParaAmount.Name = "lblParaAmount"
        Me.lblParaAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblParaAmount.Size = New System.Drawing.Size(89, 17)
        Me.lblParaAmount.TabIndex = 7
        Me.lblParaAmount.Text = "BB_Amount"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(944, 24)
        Me.MenuStrip1.TabIndex = 116
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 654)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(925, 1)
        Me.lblLine1.TabIndex = 117
        Me.lblLine1.Text = "Label1"
        '
        'frmSQLStatements
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(944, 690)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me._frame1_0)
        Me.Controls.Add(Me._frame1_1)
        Me.Controls.Add(Me.frameQuery)
        Me.Controls.Add(Me._frame1_2)
        Me.Controls.Add(Me.cmdEnd)
        Me.Controls.Add(Me.frameResult)
        Me.Controls.Add(Me.frameParameters)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(11, 37)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmSQLStatements"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "60226 - Query setup"
        Me._frame1_0.ResumeLayout(False)
        Me.frameWriVariables.ResumeLayout(False)
        Me._frame1_1.ResumeLayout(False)
        Me.frameIntVariables.ResumeLayout(False)
        Me.frameQuery.ResumeLayout(False)
        Me.frameQuery.PerformLayout()
        CType(Me.imgIcon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgArrowDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgArrowUp, System.ComponentModel.ISupportInitialize).EndInit()
        Me._frame1_2.ResumeLayout(False)
        Me.framePayInvoice.ResumeLayout(False)
        Me.framePayInvoice.PerformLayout()
        Me.framePayPayment.ResumeLayout(False)
        Me.framePayPayment.PerformLayout()
        Me.framePayStatement.ResumeLayout(False)
        Me.framePayStatement.PerformLayout()
        Me.frameResult.ResumeLayout(False)
        CType(Me.gridResult, System.ComponentModel.ISupportInitialize).EndInit()
        Me.frameParameters.ResumeLayout(False)
        Me.frameParameters.PerformLayout()
        CType(Me.frame1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gridResult As System.Windows.Forms.DataGridView
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Public WithEvents cmdWriInterpreted As System.Windows.Forms.Button
    Public WithEvents txtIntSQL As System.Windows.Forms.RichTextBox
    Public WithEvents cmdReplace As System.Windows.Forms.Button
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents chkUseAdjustments As System.Windows.Forms.CheckBox
    Public WithEvents chkCustomerNo As System.Windows.Forms.CheckBox
#End Region 
End Class
