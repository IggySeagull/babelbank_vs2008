Option Strict Off
Option Explicit On
Friend Class frmWiz_Report
    Inherits System.Windows.Forms.Form
    Private frmReportAdvanced As frmReportAdvanced
    Private frmReportPrinter As frmReportPrinter
    Private frmReportSQL As frmReportSQL
    Dim oFilesetup As vbBabel.FileSetup
    Private listitem As New _MyListBoxItem
    'Dim oReport As vbBabel.Report is Public in Setup.vb
    Public Sub SetfrmReportAdvanced(ByVal frm As frmReportAdvanced)
        ' pass frmReportAdvanced to frmWiz_Reports
        frmReportAdvanced = frm
    End Sub
    Public Sub PassfrmReportPrinter(ByVal frm As frmReportPrinter)
        ' pass frmreportprinter to frmWiz_Reports
        frmReportPrinter = frm
    End Sub
    Public Sub PassfrmReportSQL(ByVal frm As frmReportSQL)
        ' pass frmreportsql to frmWiz_Reports
        frmReportSQL = frm
    End Sub
    Public Sub PassFilesetup(ByRef oFS As vbBabel.FileSetup)
        oFilesetup = oFS
    End Sub
    Private Sub CmdFinish_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdFinish.Click
        Me.Hide()
    End Sub
    Private Sub frmWiz_Report_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control

        FormvbStyle(Me, "dollar.jpg")
        ' 28.12.2010 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next

        FormLRSCaptions(Me)
        ' lblHeading does not translate lrs i function above !!! Why ???
        Me.lblHeading.Text = LRS(60267)
    End Sub
    Private Sub CmdBack_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdBack.Click
        Me.Hide()
        WizardMove((-1))
    End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        Me.Hide()
        WizardMove((-99))
    End Sub
    Private Sub CmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdNext.Click
        Me.Hide()
        WizardMove((1))
    End Sub
    Private Sub cmdChange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdChange.Click
        Dim i, j As Integer
        Dim sMySQL As String = ""
        Dim bActivateSpecialSQL As Boolean = False
        Dim bSQLServer As Boolean = False

        If Me.lstReport.Items.Count > 0 And Me.lstReport.SelectedIndex > -1 Then
            ' change an existing report
            'i = VB6.GetItemData(Me.lstReport, Me.lstReport.SelectedIndex)
            ' 12.07.2019
            i = Me.lstReport.Items(Me.lstReport.SelectedIndex).itemdata
            'oReport = oFilesetup.Reports.Item(i)
            ' must use Index, because this is what we have saved in ItemData
            For j = 0 To Me.lstReport.Items.Count
                If oFilesetup.Reports.Item(j + 1).Index = i Then
                    oReport = oFilesetup.Reports.Item(j + 1)
                    Exit For
                End If
            Next

            'find values from oFilesetup to position lstboxes

            With frmReportAdvanced
                For i = 0 To .lstReport.Items.Count - 1
                    ' Set Reporttype;
                    ' ---------------
                    'If VB6.GetItemData(.lstReport, i) = oReport.ReportNo Then
                    ' 12.07.2019
                    If .lstReport.Items(i).itemdata = oReport.ReportNo Then
                        frmReportAdvanced.lstReport.SelectedIndex = i
                        Exit For
                    End If
                Next i

                For i = 0 To .lstReportWhen.Items.Count - 1
                    ' Set when to run report;
                    ' -----------------------
                    'If VB6.GetItemData(.lstReportWhen, i) = oReport.ReportWhen Then
                    ' 12.07.2019
                    If .lstReportWhen.Items(i).itemdata = oReport.ReportWhen Then
                        .lstReportWhen.SelectedIndex = i
                        Exit For
                    End If
                Next i
                If .lstReportWhen.SelectedIndex = -1 Then
                    .lstReportWhen.SelectedIndex = .lstReportWhen.Items.Count - 1
                End If

                ' Set sorting;
                ' -----------------------
                .lstSorting.SelectedIndex = oReport.ReportSort ' - 1
                If .lstSorting.SelectedIndex = -1 Then
                    .lstSorting.SelectedIndex = 0 '.lstSorting.Items.Count - 1
                End If

                ' then the rest of values in frmReportAdvanced;
                If oReport.ExportType <> "" Then
                    .chkExport.Checked = True
                    Select Case oReport.ExportType
                        Case "PDF"
                            .optPDF.Checked = True
                        Case "RTF"
                            .optRTF.Checked = True
                        Case "TXT"
                            .optTXT.Checked = True
                        Case "HTML"
                            .optHTML.Checked = True
                        Case "TIFF"
                            .optTIFF.Checked = True
                        Case "XLS"
                            .optXLS.Checked = True
                        Case Else
                            .optPDF.Checked = True
                    End Select
                    .txtFilename.Text = oReport.ExportFilename
                    .ShowExportControls(True)
                Else
                    .chkExport.Checked = False
                    .txtFilename.Text = ""
                    '.fraExport.Visible = False
                    .ShowExportControls(False)
                End If
                .txteMail.Text = oReport.EMailAdresses
                .chkeMail.Checked = oReport.EMail

                .chkIncludeKID.Checked = oReport.IncludeOCR
                .chkLongDate.Checked = oReport.LongDate
                .chkPreview.Checked = oReport.Preview
                .chkPrint.Checked = oReport.Printer
                If .chkPrint.Checked Then
                    .cmdPrinter.Enabled = True
                Else
                    .cmdPrinter.Enabled = False
                End If

                .chkReportOnDatabase.Checked = oReport.ReportOnDatabase
                If oReport.ReportOnDatabase Then
                    .chkSaveBeforeReport.Checked = oReport.SaveBeforeReport
                Else
                    .chkSaveBeforeReport.Checked = False
                End If
                .txtHeading.Text = oReport.Heading
                .txtReportName.Text = Trim(oReport.ReportName)
            End With
            ' fill frmReportSQL with SQL-settings
            sMySQL = oReport.ReportSQL
            bSQLServer = BB_UseSQLServer()
            If EmptyString(sMySQL) Then '07.01.2015 - Added Else
                sMySQL = RetrieveSQLForReports(bSQLServer, oReport.ReportNo, oReport.ReportSort, oReport.IncludeOCR, oReport.SaveBeforeReport)
            Else
                sMySQL = Replace(sMySQL, "@?@", "'")
            End If
            With frmReportSQL
                bActivateSpecialSQL = oReport.ActivateSpecialSQL
                .chkActivateSQL.Checked = bActivateSpecialSQL
                If InStr(UCase(sMySQL), "WHERE") > 0 Then
                    .txtSelect.Text = Strings.Left(sMySQL, InStr(UCase(sMySQL), "WHERE") - 1)
                    If InStr(UCase(sMySQL), "ORDER") > 0 Then
                        .txtWhere.Text = Strings.Mid(sMySQL, InStr(UCase(sMySQL), "WHERE"), InStr(UCase(sMySQL), "ORDER") - InStr(UCase(sMySQL), "WHERE") - 1)
                        .txtOrderBy.Text = Strings.Mid(sMySQL, InStr(UCase(sMySQL), "ORDER"))
                    Else
                        .txtWhere.Text = Strings.Mid(sMySQL, InStr(UCase(sMySQL), "WHERE"))
                        .txtOrderBy.Text = ""
                    End If
                Else
                    .txtSelect.Text = ""
                    .txtWhere.Text = ""
                    .txtOrderBy.Text = ""
                    .chkActivateSQL.Checked = False
                End If
                .Text = "SQL " & oReport.ReportName
            End With

            frmReportAdvanced.ShowDialog()
            ' save the results from frmReportAdvanced into oReport
            If frmReportAdvanced.DialogResult = 1 Or frmReportSQL.DialogResult = Windows.Forms.DialogResult.OK Or frmReportPrinter.DialogResult = Windows.Forms.DialogResult.OK Then
                SavefrmAdvancedToFilesetup(False)
                ' Update reportname in lstReports
                'Me.lstReport.Items(Me.lstReport.SelectedIndex) = Trim(frmReportAdvanced.txtReportName.Text)
                If EmptyString(frmReportAdvanced.txtReportName.Text) Then
                    If frmReportAdvanced.lstReport.SelectedIndex > 0 Then
                        'Me.lstReport.Items(Me.lstReport.SelectedIndex) = New VB6.ListBoxItem(frmReportAdvanced.lstReport.SelectedItem.ToString, oReport.Index)
                        ' 12.07.2019
                        listitem = New _MyListBoxItem
                        listitem.ItemString = frmReportAdvanced.lstReport.SelectedItem.itemstring
                        listitem.ItemData = oReport.Index
                        'Me.lstReport.Items(Me.lstReport.SelectedIndex) = New VB6.ListBoxItem(frmReportAdvanced.lstReport.SelectedItem.ToString, oReport.Index)
                        Me.lstReport.Items(Me.lstReport.SelectedIndex) = listitem
                    End If
                Else
                    'Me.lstReport.Items(Me.lstReport.SelectedIndex) = New VB6.ListBoxItem(frmReportAdvanced.txtReportName.Text, oReport.Index)
                    ' 12.07.2019
                    listitem = New _MyListBoxItem
                    listitem.ItemString = frmReportAdvanced.txtReportName.Text
                    listitem.ItemData = oReport.Index
                    Me.lstReport.Items(Me.lstReport.SelectedIndex) = listitem
                End If
            End If
        End If
    End Sub
    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click

        ' postition to defaults for new reports
        frmReportAdvanced.txtReportName.Text = ""
        frmReportAdvanced.txtHeading.Text = ""
        frmReportAdvanced.lstReport.SelectedIndex = 0
        frmReportAdvanced.lstReport.SelectAll()
        frmReportAdvanced.lstReportWhen.SelectedIndex = 6 ' After Export
        'frmReportAdvanced.lstReportWhen.SelectAll()
        frmReportAdvanced.lstSorting.SelectedIndex = 0 '1
        'frmReportAdvanced.lstSorting.SelectAll()
        frmReportAdvanced.chkExport.Checked = False
        'frmReportAdvanced.fraExport.Visible = False
        frmReportAdvanced.ShowExportControls(False)
        frmReportAdvanced.chkIncludeKID.Checked = False
        frmReportAdvanced.chkLongDate.Checked = False
        frmReportAdvanced.chkPreview.Checked = True
        frmReportAdvanced.chkPrint.Checked = False
        frmReportAdvanced.cmdPrinter.Enabled = False
        frmReportAdvanced.chkReportOnDatabase.Checked = True '' TODO avhengig av rapport
        frmReportAdvanced.chkSaveBeforeReport.Checked = True  ' TODO avhengig av rapport
        frmReportAdvanced.chkeMail.Checked = False
        frmReportAdvanced.txteMail.Text = ""

        ' fill frmReportSQL with SQL-settings
        With frmReportSQL
            .txtSelect.Text = ""
            .txtWhere.Text = ""
            .txtOrderBy.Text = ""
            .chkActivateSQL.Checked = False
            .Text = "SQL"
        End With

        ' Which controls are visible - reportdependant

        frmReportAdvanced.ShowDialog()
        ' save the results from frmReportAdvanced into oReport
        If frmReportAdvanced.DialogResult = 1 Then
            SavefrmAdvancedToFilesetup(True)
        End If
        ' add to listbox inf frmwiz_report (here):
        'Me.lstReport.Items.Add(New VB6.ListBoxItem(frmReportAdvanced.txtReportName.Text, 0))
        If EmptyString(frmReportAdvanced.txtReportName.Text) Then
            If frmReportAdvanced.lstReport.SelectedIndex > 0 Then
                'Me.lstReport.Items.Add(New VB6.ListBoxItem(frmReportAdvanced.lstReport.SelectedItem.ToString, oReport.Index))
                ' 12.07.2019
                listitem = New _MyListBoxItem
                listitem.ItemString = frmReportAdvanced.lstReport.Items(frmReportAdvanced.lstReport.SelectedIndex).itemstring
                listitem.ItemData = oReport.Index
                Me.lstReport.Items.Add(listitem)

            End If
        Else
            'Me.lstReport.Items.Add(New VB6.ListBoxItem(frmReportAdvanced.txtReportName.Text, oReport.Index))
            ' 12.07.2019
            listitem = New _MyListBoxItem
            listitem.ItemString = frmReportAdvanced.txtReportName.Text
            listitem.ItemData = oReport.Index
            Me.lstReport.Items.Add(listitem)

        End If

    End Sub
    Private Sub SavefrmAdvancedToFilesetup(ByVal bNewReport As Boolean)
        Dim sQuery As String
        If bNewReport Then
            oReport = oFilesetup.Reports.Add()
        Else
            For Each oReport In oFilesetup.Reports
                If oReport.Index = Me.lstReport.Items(Me.lstReport.SelectedIndex).ItemData Then
                    Exit For
                End If
            Next
        End If

        With frmReportAdvanced
            If frmReportAdvanced.DialogResult = Windows.Forms.DialogResult.OK Then
                'oReport.ReportNo = VB6.GetItemData(.lstReport, .lstReport.SelectedIndex)
                ' 12.07.2019
                oReport.ReportNo = .lstReport.Items(.lstReport.SelectedIndex).itemdata
                oReport.ReportName = Trim(.txtReportName.Text)
                oReport.Preview = .chkPreview.Checked
                oReport.Printer = .chkPrint.Checked
                oReport.LongDate = .chkLongDate.Checked
                oReport.IncludeOCR = .chkIncludeKID.Checked

                oReport.SaveBeforeReport = .chkSaveBeforeReport.Checked
                oReport.ReportOnDatabase = .chkReportOnDatabase.Checked

                If .chkExport.Checked Then
                    If .optPDF.Checked Then
                        oReport.ExportType = "PDF"
                    ElseIf .optRTF.Checked Then
                        oReport.ExportType = "RTF"
                    ElseIf .optTIFF.Checked Then
                        oReport.ExportType = "TIFF"
                    ElseIf .optTXT.Checked Then
                        oReport.ExportType = "TXT"
                    ElseIf .optXLS.Checked Then
                        oReport.ExportType = "XLS"
                    ElseIf .optHTML.Checked Then
                        oReport.ExportType = "HTML"
                    End If
                Else
                    oReport.ExportType = ""
                End If
                oReport.ExportFilename = .txtFilename.Text
                oReport.EMailAdresses = .txteMail.Text
                oReport.EMail = .chkeMail.Checked
                oReport.ReportSort = .lstSorting.SelectedIndex '+ 1
                'oReport.ReportWhen = VB6.GetItemData(.lstReportWhen, .lstReportWhen.SelectedIndex)
                ' 12.07.2019
                oReport.ReportWhen = .lstReportWhen.Items(.lstReportWhen.SelectedIndex).itemdata
                oReport.BreakLevel = .lstSorting.SelectedIndex
                oReport.Heading = .txtHeading.Text
            End If

            If frmReportSQL.DialogResult = Windows.Forms.DialogResult.OK Then
                ' ta med verdier fra frmReportSQL
                'Replace TAB, CRLF etc...
                sQuery = Trim(frmReportSQL.txtSelect.Text) & " " & Trim(frmReportSQL.txtWhere.Text) & " " & Trim(frmReportSQL.txtOrderBy.Text)
                sQuery = Replace(sQuery, Chr(8), "")
                sQuery = Replace(sQuery, Chr(9), "")
                sQuery = Replace(sQuery, Chr(10), "")
                sQuery = Replace(sQuery, Chr(13), "")
                ' gj�r om ' til @?@
                sQuery = Replace(sQuery, "'", "@?@")

                oReport.ReportSQL = sQuery
                oReport.ActivateSpecialSQL = frmReportSQL.chkActivateSQL.Checked
            End If

            If frmReportPrinter.DialogResult = Windows.Forms.DialogResult.OK Then
                ' from frmReportPrinter    
                oReport.AskForPrinter = frmReportPrinter.chkSelectPrinter.Checked
                oReport.PrinterName = frmReportPrinter.lstPrinters.Items(frmReportPrinter.lstPrinters.SelectedIndex).ToString
            End If
        End With
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        Dim iItem As Integer
        Dim i As Integer
        ' remove a report;
        Dim iSelIndex As Integer
        If Me.lstReport.Items.Count > 0 And Me.lstReport.SelectedIndex > -1 Then
            For Each oReport In oFilesetup.Reports
                If oReport.Index = Me.lstReport.Items(Me.lstReport.SelectedIndex).ItemData Then
                    Exit For
                End If
            Next
            iItem = Int(oReport.Index)
            ' remove from oFilesetup
            'oFilesetup.Reports.Remove(iItem)  'Int(oReport.Index))
            For i = oFilesetup.Reports.Count To 1 Step -1
                oReport = oFilesetup.Reports.Item(i)
                If oReport.Index = iItem Then
                    oFilesetup.Reports.Remove(i)
                    Exit For
                End If
            Next
            iSelIndex = Me.lstReport.SelectedIndex
            ' remove from listbox
            Me.lstReport.Items.RemoveAt(iSelIndex)   'Me.lstReport.SelectedIndex)

            ' "refill" listbox with reports
            With Me.lstReport
                ' Fill in selected reports only
                .Items.Clear()
                For Each oReport In oFilesetup.Reports
                    If EmptyString(oReport.ReportName) Then
                        '.Items.Add(New VB6.ListBoxItem(FindReportName(oReport.ReportNo), oReport.Index))
                        ' 12.07.2019
                        listitem = New _MyListBoxItem
                        listitem.ItemString = FindReportName(oReport.ReportNo)
                        listitem.ItemData = oReport.Index
                        Me.lstReport.Items.Add(listitem)

                    Else
                        '.Items.Add(New VB6.ListBoxItem(Trim(oReport.ReportName), oReport.Index))
                        ' 12.07.2019
                        listitem = New _MyListBoxItem
                        listitem.ItemString = oReport.ReportName
                        listitem.ItemData = oReport.Index
                        Me.lstReport.Items.Add(listitem)
                    End If
                Next
            End With
        End If
    End Sub

    Private Sub lstReport_Doubleclick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstReport.DoubleClick
        ' select, and go to Advanced, as the Changebutton
        Me.cmdChange_Click(sender, e)
    End Sub

End Class
