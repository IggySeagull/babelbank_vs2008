Option Strict Off
Option Explicit On
Module DatabaseUtils
	
    Public Function FetchBanks() As String(,)
        ' Connect to database
        ' Return array with banknames and ids
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim aBankArray(,) As String
        Dim sMyFile As String
        Dim i As Short

        Try
            ReDim aBankArray(1, 0)  ' changed 27.07.2010 from (0, 0)

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "Select Name, Bank_ID from bank order by name"
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then
                i = 0
                Do While oMyDal.Reader_ReadRecord
                    ReDim Preserve aBankArray(1, i)
                    aBankArray(0, i) = oMyDal.Reader_GetString("Name")
                    aBankArray(1, i) = oMyDal.Reader_GetString("Bank_ID")
                    i = i + 1
                Loop
            Else
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: FetchBanks" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return aBankArray

    End Function
    Public Function SaveNewBankToDatabase(ByRef iID As Short, ByRef sName As String) As Boolean
        ' Create a new record in banktable
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True

        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "INSERT INTO Bank(Bank_ID, Name, Country) VALUES (" & iID.ToString & ", '" & sName & "', 1 )"

            oMyDal.SQL = sMySQL

            If oMyDal.ExecuteNonQuery Then
                If oMyDal.RecordsAffected = 1 Then
                    bReturnValue = True
                Else
                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: SaveNewBankToDatabase" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function
    Function dbUpdateDnBNORTBINotification(ByRef aTmp() As String) As Boolean
        ' Update Database, table Notification, for DnBNOR TBI Only

        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True

        Dim sCompanyID As String
        Dim i As Short
        Dim nNext_ID As Short

        Try

            sCompanyID = "1" 'TODO: Hardcoded CompanyID
            nNext_ID = 0

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ' First, delete all DnBNORTBI-notifications from table;
            ' Does this notification exist in table?
            sMySQL = "DELETE * FROM Notification WHERE DnBNORTBI= True"

            oMyDal.SQL = sMySQL

            If oMyDal.ExecuteNonQuery Then
                'OK
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If

            ' Find highest used Notification_ID;
            sMySQL = "SELECT MAX(Notification_ID) AS MaxID FROM Notification WHERE Company_ID =" & sCompanyID
            oMyDal.SQL = sMySQL
            nNext_ID = 0
            If oMyDal.Reader_Execute() Then
                Do While oMyDal.Reader_ReadRecord
                    'nNext_ID = CDbl(oMyDal.Reader_GetString("Notification_ID"))
                    nNext_ID = CDbl(oMyDal.Reader_GetString("MaxID"))
                Loop
            Else
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
            End If

            nNext_ID = nNext_ID + 1

            For i = 0 To UBound(aTmp)
                ' If mailaddress (@ in aTmp), then aupdate Email_Adress
                ' Otherwise, if DnBNOR in aTmp, then update Name, to indicate Branch
                If InStr(aTmp(i), "@") > 0 Then
                    ' Create new record in database, indicating mailaddress
                    sMySQL = "INSERT INTO Notification(Company_ID,Notification_ID,EMail_Adress,DnBNORTBI) VALUES("
                    sMySQL = sMySQL & sCompanyID & ", " & nNext_ID & ", '" & aTmp(i) & "', True)"
                ElseIf Not EmptyString(aTmp(i)) Then
                    ' Create new record in database, indication DnNNOR branch
                    sMySQL = "INSERT INTO Notification(Company_ID,Notification_ID,Name,DnBNORTBI) VALUES("
                    sMySQL = sMySQL & sCompanyID & ", " & nNext_ID & ", '" & aTmp(i) & "', True)"
                End If
                If Not EmptyString(aTmp(i)) Then
                    nNext_ID = nNext_ID + 1

                    If oMyDal.ExecuteNonQuery Then
                        'OK
                    Else
                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                    End If
                End If
            Next i

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: dbUpdateDnBNORTBINotification" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        Return bReturnValue

    End Function

    Public Function dbGetClients(ByRef oMyDal As vbBabel.DAL, Optional ByRef iClient_ID As Short = 0) As Boolean
        ' --------------------------------------------------------
        ' Returns a DAL with a reader recordset from client-table
        ' --------------------------------------------------------

        Dim sMySQL As String
        Dim sCompanyID As String

        Try
            sCompanyID = "1" 'TODO: Hardcoded CompanyID

            If iClient_ID = 0 Then
                sMySQL = "SELECT * FROM Client WHERE Company_ID = " & sCompanyID & " ORDER BY ClientNo"
            Else
                sMySQL = "SELECT * FROM Client WHERE Company_ID = " & sCompanyID & " AND Client_ID = " & iClient_ID.ToString
            End If
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then
                'OK
            Else
                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
            End If

        Catch ex As Exception

            Throw New Exception("Function: dbGetClients" & vbCrLf & ex.Message)

        End Try

        Return True

    End Function
    Public Sub dbGetProfiles(ByRef oMyDal As vbBabel.DAL, ByRef iClient_ID As Short)
        ' -------------------------------------------------------------------------
        ' Creates and returns a recordset with selected profiles for current client
        ' -------------------------------------------------------------------------
        Dim sMySQL As String
        Dim sCompanyID As String

        sCompanyID = "1" 'TODO: Hardcoded Company_ID

        sMySQL = "Select Filesetup_ID, ShortName, FromAccountingSys, FilesetupOut, FormatOutID1, FormatOutId2, FormatOutId3, FormatOutId4, AutoMatch FROM ClientFormat CF, FileSetup F WHERE CF.Company_ID = " & sCompanyID & " AND CF.Client_ID = " & iClient_ID & " AND cf.FormatIn = f.Filesetup_ID ORDER BY F.Shortname"

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Sub
    Public Sub dbGetClientNotifications(ByRef oMyDal As vbBabel.DAL, ByRef iClient_ID As Short, Optional ByVal bExcludeClient As Boolean = False)
        ' -------------------------------------------------------------------------
        ' Creates and returns a recordset with selected Notifications for current client
        ' -------------------------------------------------------------------------
        Dim sMySQL As String = ""
        Dim sCompanyID As String

        sCompanyID = "1" 'TODO: Hardcoded Company_ID

        ' Fill a recordset with records from clientprofile and filesetup tables
        If iClient_ID = 0 Then
            'Select all notifications
            sMySQL = "Select Name, EMail_Adress, Notification_ID FROM Notification "
            sMySQL = sMySQL & "WHERE Company_ID=" & sCompanyID
            sMySQL = sMySQL & " ORDER BY Name"
        ElseIf bExcludeClient Then
            ' Notification for all clients minus the specified one
            'sMySQL = "Select CN.Client_ID, CN.Notification_ID, "
            'sMySQL = sMySQL & "N.Name, N.Email_Adress FROM ClientNotifications CN, Notification N "
            'sMySQL = sMySQL & "WHERE CN.Company_ID = " & sCompanyID
            'sMySQL = sMySQL & " AND CN.Company_ID = N.Company_ID"
            'sMySQL = sMySQL & " AND CN.Notification_ID = N.Notification_ID"
            'sMySQL = sMySQL & " AND CN.Client_ID = " & iClient_ID.ToString & " ORDER BY N.Name"
            sMySQL = "Select Name, EMail_Adress, Notification_ID "
            sMySQL = sMySQL & "FROM Notification WHERE Company_ID = " & sCompanyID & " AND Notification_ID NOT IN ("
            sMySQL = sMySQL & "Select CN.Notification_ID FROM ClientNotifications CN, Notification N "
            sMySQL = sMySQL & "WHERE CN.Company_ID = " & sCompanyID & " AND CN.Company_ID = N.Company_ID AND CN.Notification_ID = N.Notification_ID AND CN.Client_ID = " & iClient_ID.ToString & ") "
            sMySQL = sMySQL & "ORDER BY Name"
        Else
            ' Notifications for one specific client
            sMySQL = "Select CN.Client_ID, CN.Notification_ID, "
            sMySQL = sMySQL & "N.Name, N.EMail_Adress FROM ClientNotifications CN, Notification N "
            sMySQL = sMySQL & "WHERE CN.Company_ID = " & sCompanyID
            sMySQL = sMySQL & " AND CN.Company_ID = N.Company_ID"
            sMySQL = sMySQL & " AND CN.Notification_ID = N.Notification_ID"
            sMySQL = sMySQL & " AND CN.Client_ID = " & iClient_ID.ToString & " ORDER BY N.Name"
        End If

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Sub
    Public Sub dbGetAllProfiles(ByRef oMyDal As vbBabel.DAL, ByRef iClient_ID As Short)
        ' -------------------------------------------------------------------------
        ' Creates and returns a recordset with all profiles, except selected ones
        ' for current client
        ' -------------------------------------------------------------------------
        Dim sMySQL As String = ""
        Dim sCompanyID As String

        sCompanyID = "1" 'TODO: Hardcoded Company_ID

        sMySQL = "Select Filesetup_ID, ShortName, FromAccountingSys, FilesetupOut, FormatOutID1, FormatOutId2, FormatOutId3, FormatOutId4, AutoMatch "
        sMySQL = sMySQL & "FROM FileSetup WHERE Company_ID = " & sCompanyID & " AND (FilenameIn1 <> '' OR FilenameIn2 <> '' OR FilenameIn3 <> '') AND Filesetup_ID NOT IN "
        sMySQL = sMySQL & "(Select DISTINCT FormatIn from ClientFormat CF, FileSetup F WHERE CF.Company_ID = " & sCompanyID & " AND CF.Client_ID = " & iClient_ID.ToString & " AND cf.FormatIn = f.Filesetup_ID) ORDER BY Shortname"

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Sub
    Public Sub dbGetAccounts(ByRef oMyDal As vbBabel.DAL, ByRef iClient_ID As Short)
        ' --------------------------------------------------------
        ' Creates and returns a recordset from accounts-table
        ' --------------------------------------------------------
        Dim sMySQL As String
        Dim sCompanyID As String

        sCompanyID = "1" 'TODO: Hardcoded Company_ID

        ' Fill the reader with records from account table for current client
        sMySQL = "SELECT * FROM Account WHERE Company_ID=" & sCompanyID & " AND Client_ID = " & iClient_ID.ToString & " ORDER BY Account"

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Sub
    Public Sub dbGetMatch_Diff(ByRef oMyDal As vbBabel.DAL, ByRef iDiffAccount_ID As Short, ByRef iNext_ID As Short)
        ' --------------------------------------------------------
        ' Creates and returns a recordset from MatchDiff-table
        ' --------------------------------------------------------
        Dim sMySQL As String
        Dim sCompanyID As String
        Dim i As Short

        sCompanyID = "1" 'TODO: Hardcoded Company_ID
        ' Find highest MatchDiff_ID;
        iNext_ID = 0
        sMySQL = "SELECT MAX(MatchDiff_ID) AS MaxID FROM Match_Diff WHERE Company_ID =" & sCompanyID
        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            Do While oMyDal.Reader_ReadRecord
                iNext_ID = CShort(oMyDal.Reader_GetString("MaxID"))
            Loop
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

        'Select MatchDiff accounts
        sMySQL = "SELECT * FROM Match_Diff WHERE Company_ID = " & sCompanyID & " AND MATCH_DiffaccountID = " & iDiffAccount_ID.ToString & " ORDER BY Currency"

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

        iNext_ID = iNext_ID + 1

    End Sub
    Public Sub dbGetDnBNORTBINotifications(ByRef oMyDal As vbBabel.DAL)
        ' --------------------------------------------------------
        ' Creates a recordset from Notification-table
        ' returns recordset with Notification_ID,Name,email_adress
        ' for Notifications marked DnBNORTBI
        ' --------------------------------------------------------
        Dim sMySQL As String
        Dim sCompanyID As String

        sCompanyID = "1" 'TODO: Hardcoded Company_ID

        ' Fill a recordset with records for this client
        sMySQL = "SELECT * FROM Notification WHERE Company_ID = " & sCompanyID & " AND DnBNORTBI = True ORDER BY EMail_Adress"

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Sub
    Public Sub dbGetDiffAccounts(ByRef oMyDal As vbBabel.DAL, ByRef iClient_ID As Short)
        ' -------------------------------------------------------------
        ' Creates and returns a recordset from MATCH_DiffAccounts-table
        ' -------------------------------------------------------------
        Dim sMySQL As String

        Dim sCompanyID As String

        sCompanyID = "1" 'TODO: Hardcoded Company_ID

        ' Fill the reader with records from MATCH_DiffAccounts table for current client
        sMySQL = "SELECT * FROM MATCH_DiffAccounts WHERE Company_ID = " & sCompanyID & " AND Client_ID = " & iClient_ID.ToString & " ORDER BY Name"
        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Sub
    Public Sub dbGetAutoBook(ByRef oMyDal As vbBabel.DAL, ByRef iClient_ID As Short, Optional ByVal iAutoBook_ID As Short = 0)
        ' -------------------------------------------------------------
        ' Creates and returns a recordset from AutoBook-table
        ' -------------------------------------------------------------
        Dim sMySQL As String
        Dim sCompanyID As String

        sCompanyID = "1" 'TODO: Hardcoded Company_ID

        ' Fill the reader with records from AutoBook table for current client
        sMySQL = "SELECT AutoBook_ID, Name, ToAccount, BookText, Type, Active, ProposeMatch, VATCode FROM AutoBook WHERE Company_ID = " & sCompanyID & " AND Client_ID = " & iClient_ID.ToString
        If iAutoBook_ID > 0 Then
            sMySQL = sMySQL & " AND AutoBook_ID = " & iAutoBook_ID.ToString
        End If
        sMySQL = sMySQL & " ORDER BY AutoBook_ID"

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Sub
    Public Sub dbGetAutoBookDetail(ByRef oMyDal As vbBabel.DAL, ByRef iClient_ID As Short, ByRef iAutoBook_ID As Short, Optional ByVal iAutoBookDetail_ID As Short = 0)
        ' -------------------------------------------------------------
        ' Creates and returns a recordset from AutoBookDetail-table
        ' -------------------------------------------------------------
        Dim sMySQL As String
        Dim sCompanyID As String

        sCompanyID = "1" 'Hardcoded CompanyID

        ' Fill the reader with records from AutoBookDetail table for current client and autobook
        sMySQL = "SELECT AutoBookDetail_ID, SelectOnField, ValueInField FROM AutoBookDetail WHERE Company_ID = " & sCompanyID & " AND Client_ID = " & iClient_ID.ToString & " AND AutoBook_ID = " & iAutoBook_ID.ToString
        If iAutoBookDetail_ID > 0 Then
            sMySQL = sMySQL & " AND AutoBookDetail_ID = " & iAutoBookDetail_ID.ToString
        End If
        sMySQL = sMySQL & " ORDER BY AutoBookDetail_ID"

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Sub
    Public Sub dbGetFilter(ByRef oMyDal As vbBabel.DAL, ByRef iFileSetup_ID As Short, Optional ByVal iFilter_ID As Short = 0)
        ' -------------------------------------------------------------
        ' Creates and returns a recordset from Filter-table
        ' -------------------------------------------------------------
        Dim sMySQL As String

        Dim sCompanyID As String

        sCompanyID = "1" 'Hardcoded CompanyID

        ' Fill a recordset with records from Filter table for current client
        sMySQL = "SELECT * FROM Filter WHERE Company_ID = " & sCompanyID & " AND Filesetup_ID = " & iFileSetup_ID.ToString
        If iFilter_ID > 0 Then
            sMySQL = sMySQL & " AND Filter_ID = " & iFilter_ID.ToString
        End If
        sMySQL = sMySQL & " ORDER BY Filter_ID"

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Sub
    Public Sub dbGetDBProfiles(ByRef oMyDal As vbBabel.DAL)
        ' -------------------------------------------------------------------------
        ' Creates and returns a recordset with all DBProfiles
        ' -------------------------------------------------------------------------
        Dim sMySQL As String
        Dim sCompanyID As String

        sCompanyID = "1" 'TODO: Hardcoded Company_ID

        sMySQL = "Select * FROM DB_Profile WHERE Company_ID = " & sCompanyID

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Sub
    Public Sub dbGetPatternGroups(ByRef oMyDal As vbBabel.DAL, ByRef iDBProfile_ID As Short)
        ' -------------------------------------------------------------
        ' Creates and returns a recordset from PatternGroups
        ' -------------------------------------------------------------
        Dim sMySQL As String
        Dim sCompanyID As String

        sCompanyID = "1" 'TODO: Hardcoded Company_ID

        ' Fill the reader with records from PatternGroups table
        ' for current DBProfile, or for all
        ' if iDBProfile_ID = 99, then all dbprofiles are selected
        If iDBProfile_ID = 99 Then
            sMySQL = "SELECT * FROM PatternGroup WHERE Company_ID = " & sCompanyID & " ORDER BY Name"
        Else
            sMySQL = "SELECT * FROM PatternGroup WHERE Company_ID = " & sCompanyID & " AND DBProfile_ID = " & iDBProfile_ID.ToString & " ORDER BY Name"
        End If


        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Sub
    Public Sub dbGetPatterns(ByRef oMyDal As vbBabel.DAL, ByRef iDBProfile_ID As Short, ByRef iPatternGroup_ID As Short, ByRef iClient_ID As Short)
        ' -------------------------------------------------------------
        ' Creates and returns a recordset from Pattern
        ' -------------------------------------------------------------
        Dim sMySQL As String
        Dim sCompanyID As String

        sCompanyID = "1" 'TODO: Hardcoded Company_ID

        'Retrieves only the fields needed
        sMySQL = "SELECT Description, Pattern_ID, KID FROM PatternGroup PG, Pattern P WHERE "
        sMySQL = sMySQL & "P.Company_ID = PG.Company_ID AND P.PatternGroup_ID = PG.PatternGroup_ID AND P.DBProfile_ID = PG.DBProfile_ID "
        sMySQL = sMySQL & "AND P.Company_ID = " & sCompanyID
        sMySQL = sMySQL & " AND P.Client_ID =" & iClient_ID.ToString
        sMySQL = sMySQL & " AND P.PatternGroup_ID =" & iPatternGroup_ID.ToString
        sMySQL = sMySQL & " AND P.DBProfile_ID = " & iDBProfile_ID.ToString & " ORDER BY Description"

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            'OK
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

    End Sub
End Module
