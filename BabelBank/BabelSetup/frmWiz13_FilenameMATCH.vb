Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmWiz13_FilenameMatch
	Inherits System.Windows.Forms.Form
	Private aFilenames(,) As String
	'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
	'DOTNETT REDIM Private aFilenames() As String
	Private iCurrentList As Short
    Public Sub LetFilenames(ByRef af(,) As String)
        ' get initial filenames in array passed from setup
        Dim i As Short
        If Array_IsEmpty(af) Then
            Dim aFilenames(2, 0) As Object
            'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            aFilenames(2, 0) = "0000"
        Else
            ReDim Preserve aFilenames(2, UBound(af, 2))
            For i = 0 To UBound(af, 2)
                'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(0, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aFilenames(0, i) = af(0, i) ' Filename
                'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(1, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aFilenames(1, i) = af(1, i) ' Format
                'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aFilenames(2, i) = af(2, i) ' Filetypes
                lstFiles.Items.Add("")
            Next i
        End If
        UpdateFilesList()
        If Me.lstFiles.Items.Count > 0 Then
            Me.lstFiles.SelectedIndex = 0
        End If
    End Sub
    Public Function GetFilenames() As String(,)
        GetFilenames = aFilenames
    End Function
	
	'UPGRADE_WARNING: Event chkGL.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub chkGL_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkGL.CheckStateChanged
		Dim aFilenames As Object
		If Me.chkGL.CheckState = 1 Then
			If CheckIfFiletypeIsUsed(1) Then
				Me.chkAutogiro.CheckState = System.Windows.Forms.CheckState.Unchecked
				Me.chkOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
				Me.chkRest.CheckState = System.Windows.Forms.CheckState.Unchecked
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aFilenames(2, iCurrentList) = "1" & Mid(aFilenames(2, iCurrentList), 2)
				'UpdateCurrentFile
			Else
				chkGL.CheckState = System.Windows.Forms.CheckState.Unchecked
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aFilenames(2, iCurrentList) = "0" & Mid(aFilenames(2, iCurrentList), 2)
			End If
		Else
			chkGL.CheckState = System.Windows.Forms.CheckState.Unchecked
			'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			aFilenames(2, iCurrentList) = "0" & Mid(aFilenames(2, iCurrentList), 2)
		End If
	End Sub
	Private Function CheckIfFiletypeIsUsed(ByRef iPos As Short) As Boolean
		Dim aFilenames As Object
		' Is filetype used for other files?
		Dim i As Short
		Dim bRetvalue As Boolean
		bRetvalue = True
		For i = 0 To UBound(aFilenames, 2)
			If i <> iCurrentList Then
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				If Mid(aFilenames(2, i), iPos, 1) = "1" Then
					bRetvalue = False
					MsgBox("Filtypen er valgt for en av de andre filene!", MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation)
				End If
			End If
		Next i
		CheckIfFiletypeIsUsed = bRetvalue
	End Function
	'UPGRADE_WARNING: Event chkOCR.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub chkOCR_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkOCR.CheckStateChanged
		Dim aFilenames As Object
		If Me.chkOCR.CheckState = 1 Then
			If CheckIfFiletypeIsUsed(2) Then
				Me.chkAutogiro.CheckState = System.Windows.Forms.CheckState.Unchecked
				Me.chkGL.CheckState = System.Windows.Forms.CheckState.Unchecked
				Me.chkRest.CheckState = System.Windows.Forms.CheckState.Unchecked
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aFilenames(2, iCurrentList) = VB.Left(aFilenames(2, iCurrentList), 1) & "1" & Mid(aFilenames(2, iCurrentList), 3)
				'UpdateCurrentFile
			Else
				chkOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aFilenames(2, iCurrentList) = VB.Left(aFilenames(2, iCurrentList), 1) & "0" & Mid(aFilenames(2, iCurrentList), 3)
			End If
		Else
			chkOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
			'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			aFilenames(2, iCurrentList) = VB.Left(aFilenames(2, iCurrentList), 1) & "0" & Mid(aFilenames(2, iCurrentList), 3)
		End If
		
	End Sub
	'UPGRADE_WARNING: Event chkAutogiro.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub chkAutogiro_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkAutogiro.CheckStateChanged
		Dim aFilenames As Object
		If Me.chkAutogiro.CheckState = 1 Then
			If CheckIfFiletypeIsUsed(3) Then
				Me.chkGL.CheckState = System.Windows.Forms.CheckState.Unchecked
				Me.chkOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
				Me.chkRest.CheckState = System.Windows.Forms.CheckState.Unchecked
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aFilenames(2, iCurrentList) = VB.Left(aFilenames(2, iCurrentList), 2) & "1" & Mid(aFilenames(2, iCurrentList), 4)
				'UpdateCurrentFile
			Else
				chkAutogiro.CheckState = System.Windows.Forms.CheckState.Unchecked
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aFilenames(2, iCurrentList) = VB.Left(aFilenames(2, iCurrentList), 2) & "0" & Mid(aFilenames(2, iCurrentList), 4)
			End If
		Else
			chkAutogiro.CheckState = System.Windows.Forms.CheckState.Unchecked
			'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			aFilenames(2, iCurrentList) = VB.Left(aFilenames(2, iCurrentList), 2) & "0" & Mid(aFilenames(2, iCurrentList), 4)
		End If
		
	End Sub
	'UPGRADE_WARNING: Event chkRest.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub chkRest_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkRest.CheckStateChanged
		Dim aFilenames As Object
		If Me.chkRest.CheckState = 1 Then
			If CheckIfFiletypeIsUsed(4) Then
				Me.chkAutogiro.CheckState = System.Windows.Forms.CheckState.Unchecked
				Me.chkGL.CheckState = System.Windows.Forms.CheckState.Unchecked
				Me.chkOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aFilenames(2, iCurrentList) = VB.Left(aFilenames(2, iCurrentList), 3) & "1"
				'UpdateCurrentFile
			Else
				chkRest.CheckState = System.Windows.Forms.CheckState.Unchecked
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aFilenames(2, iCurrentList) = VB.Left(aFilenames(2, iCurrentList), 3) & "0"
			End If
		Else
			chkRest.CheckState = System.Windows.Forms.CheckState.Unchecked
			'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			aFilenames(2, iCurrentList) = VB.Left(aFilenames(2, iCurrentList), 3) & "0"
		End If
		
	End Sub
	Private Sub cmdAddFile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAddFile.Click
		' Add a new outputfile
		Dim bValid As Boolean
		Dim ixCurrentList As Short
		
		ixCurrentList = iCurrentList
		
		bValid = True
		If Me.lstFiles.Items.Count < 4 Then
			' Validate current file;
			If Me.lstFiles.Items.Count > 0 Then
				iCurrentList = ixCurrentList ' To validate previous one, not this
				'UPGRADE_WARNING: Couldn't resolve default property of object ValidateFile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				bValid = ValidateFile
				
			End If
			
			If bValid Then
				Me.txtFilename.Visible = True
				Me.cmdFileOpen.Visible = True
				Me.lstFormats.Visible = True
				
				Me.lstFiles.Items.Add("-")
				iCurrentList = Me.lstFiles.Items.Count - 1
				
				If Me.lstFiles.Items.Count > 1 Then
					Dim aFilenames(2, UBound(aFilenames, 2) + 1) As Object
				End If
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(0, iCurrentList). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aFilenames(0, iCurrentList) = "<Filename>"
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, iCurrentList). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aFilenames(2, iCurrentList) = "0000"
				Me.txtFilename.Text = LRS(60057) '"<filename>"
				Me.txtFilename.SelectionStart = 0
				Me.txtFilename.SelectionLength = Len(aFilenames(0, iCurrentList))
				'Me.lstFiles.ListIndex = Me.lstFiles.ListCount - 1
				Me.txtFilename.Focus()
			End If
		End If
	End Sub
	
	Private Sub cmdRemoveFile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRemoveFile.Click
		Dim aFilenames As Object
		Dim i As Short
		' Remove from array if exists;
		For i = Me.lstFiles.SelectedIndex + 1 To UBound(aFilenames, 2)
			' Move info "one step down" in array
			'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			aFilenames(0, i - 1) = aFilenames(0, i)
			'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			aFilenames(1, i - 1) = aFilenames(1, i)
			'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			aFilenames(2, i - 1) = aFilenames(2, i)
		Next i
		' remove last element from array;
		ReDim Preserve aFilenames(2, UBound(aFilenames, 2) - 1)
		Me.lstFiles.Items.RemoveAt((Me.lstFiles.SelectedIndex))
		If Me.lstFiles.Items.Count > 0 Then
			iCurrentList = 0
			Me.lstFiles.SelectedIndex = 0
		End If
	End Sub
	
	Private Sub frmWiz13_FilenameMatch_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
			cmdCancel_Click(cmdCancel, New System.EventArgs())
		End If
		eventArgs.Cancel = Cancel
	End Sub
	
	'UPGRADE_WARNING: Event lstFiles.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub lstFiles_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstFiles.SelectedIndexChanged
		Dim aFilenames As Object
		Dim bValid As Boolean
		bValid = True
		' Validate current file;
		If iCurrentList > 0 Then
			'UPGRADE_WARNING: Couldn't resolve default property of object ValidateFile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			bValid = ValidateFile
			
		End If
		If bValid Then
			Me.txtFilename.Visible = True
			Me.cmdFileOpen.Visible = True
			Me.lstFormats.Visible = True
			
			iCurrentList = Me.lstFiles.SelectedIndex
			
			' Show filename etc i textbox, commandboxes below
			'UpdateFilesList
			UpdateCurrentFile()
			Me.txtFilename.SelectionStart = 0
			Me.txtFilename.SelectionLength = Len(aFilenames(0, iCurrentList))
			'    Me.txtFilename.SetFocus
		Else
			' stick to current files
			Me.lstFiles.SelectedIndex = iCurrentList
		End If
	End Sub
	
	'UPGRADE_WARNING: Event txtFilename.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtFilename_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFilename.TextChanged
		Dim aFilenames As Object
		' Update listbox with filenames, fileformats, filetypes
		'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		aFilenames(0, iCurrentList) = Me.txtFilename.Text
		UpdateFilesList()
	End Sub
	
	Public Sub UpdateFilesList()
		Dim aFilenames As Object
		' Show updated fileinfo in lstFiles
		Dim i As Short
		
		If Me.lstFiles.Items.Count > 0 And iCurrentList > -1 Then
			For i = 0 To UBound(aFilenames, 2)
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				VB6.SetItemString(Me.lstFiles, i, VB.Right(aFilenames(0, i), 35) & Chr(9) & VB.Left(aFilenames(1, i), 25))
			Next i
		End If
		
		' Update listboxes for format and filetype for current item:
		UpdateCurrentFile()
	End Sub
	Public Function ValidateFile() As Object
		Dim aFilenames As Object
		' check if necessary info is filled in
		Dim bValid As Boolean
		bValid = True
		'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(0, iCurrentList). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If aFilenames(0, iCurrentList) = vbNullString Then
			bValid = False
			MsgBox(LRS(60331), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation) '("Please fill in filename")  'lrs(
			'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ElseIf InStr(aFilenames(0, iCurrentList), "\\") = 0 And InStr(aFilenames(0, iCurrentList), ":\") = 0 Then 
			bValid = False
			MsgBox(LRS(60332), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation) '"Not valid filename"  'lrs(
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(1, iCurrentList). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If aFilenames(1, iCurrentList) = vbNullString Then
			bValid = False
			MsgBox(LRS(60333), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation) '"Please select a format!"  'lrs(
		End If
		If Me.chkAutogiro.CheckState = 0 And Me.chkOCR.CheckState = 0 And Me.chkRest.CheckState = 0 And Me.chkGL.CheckState = 0 Then
			bValid = False
			MsgBox(LRS(60334), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation) '"Please select a filetype!"  'lrs(
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object ValidateFile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ValidateFile = bValid
		
	End Function
	Public Sub UpdateCurrentFile()
		Dim aFilenames As Object
		' Show updated fileinfo in txt and listboxes
		Dim i As Short
		
		If Me.lstFiles.Items.Count > 0 And iCurrentList > -1 Then
			' Update listboxes for format and filetype for current item:
			'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Me.txtFilename.Text = aFilenames(0, iCurrentList)
			' Format:
			'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(1, iCurrentList). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If aFilenames(1, iCurrentList) <> "" Then
				For i = 0 To Me.lstFormats.Items.Count - 1
					'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					If InStr(aFilenames(1, iCurrentList), VB6.GetItemString(Me.lstFormats, i)) > 0 Then
						Me.lstFormats.SelectedIndex = i
						Exit For
					End If
				Next i
			End If
			
			'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, iCurrentList). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If aFilenames(2, iCurrentList) <> "" Then
				' Mark filetypes
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Me.chkGL.CheckState = Val(Mid(aFilenames(2, iCurrentList), 1, 1))
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Me.chkOCR.CheckState = Val(Mid(aFilenames(2, iCurrentList), 2, 1))
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Me.chkAutogiro.CheckState = Val(Mid(aFilenames(2, iCurrentList), 3, 1))
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Me.chkRest.CheckState = Val(Mid(aFilenames(2, iCurrentList), 4, 1))
			Else
				Me.chkGL.CheckState = System.Windows.Forms.CheckState.Unchecked
				Me.chkOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
				Me.chkAutogiro.CheckState = System.Windows.Forms.CheckState.Unchecked
				Me.chkRest.CheckState = System.Windows.Forms.CheckState.Unchecked
			End If
		End If
	End Sub
	
	'UPGRADE_WARNING: Event lstFormats.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub lstFormats_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstFormats.SelectedIndexChanged
		Dim aFilenames As Object
		'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		aFilenames(1, iCurrentList) = VB6.GetItemString(Me.lstFormats, Me.lstFormats.SelectedIndex)
		' Update listbox with filenames, fileformats, filetypes
		UpdateFilesList()
	End Sub
	
	Private Sub frmWiz13_FilenameMatch_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		FormvbStyle(Me, "dollar.jpg", 550)
		FormLRSCaptions(Me)
	End Sub
	Private Sub CmdBack_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdBack.Click
		Me.Hide()
		WizardMove((-1))
	End Sub
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
		WizardMove((-99))
	End Sub
	Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
		' Hent filnavn
		Dim spath As String
		Dim sFile As String
		' Keep filename after Browsing for folder;
		' Must have \ and .
		If InStr(Me.txtFilename.Text, "\") > 0 And InStr(Me.txtFilename.Text, ".") > 0 Then
			sFile = Mid(Me.txtFilename.Text, InStrRev(Me.txtFilename.Text, "\"))
		ElseIf InStr(Me.txtFilename.Text, ".") > 0 Then 
			' . only, keep all;
			sFile = "\" & Me.txtFilename.Text
		Else
			sFile = ""
		End If
		
		' Use BrowseForFolders, routine from VBNet ( i Utils)
		'spath = FolderPicker(Me, LRS(60010))   'Velg fil")
		' spath ends with end of string - remove
		'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)
		
		' Changed 18.10.05
		' using a new function where we can pass a path
		spath = FixPath((Me.txtFilename.Text))
		spath = BrowseForFolderByPIDL(spath, Me, LRS(60010), True)
		
		' If BrowseForFolderByPIDL returns empty path, then keep the old one;
		If Len(spath) > 0 Then
			' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
			If InStr(spath, ".") = 0 Then
				Me.txtFilename.Text = Replace(spath & sFile, "\\", "\")
			Else
				' Possibly file selected
				Me.txtFilename.Text = spath
			End If
		End If
		
	End Sub
	
	Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
        ' Show Helptopic
		'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(115))
		
	End Sub
	Private Sub CmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdNext.Click
		Dim aFilenames As Object
		Dim bContinue As Boolean
		Dim i As Short
		bContinue = True
		
		If ValidateFile Then ' Check last file
			' Validate filenames:
			' No * in out-filenames!
			' No combination of $ and �
			For i = 0 To UBound(aFilenames, 2)
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				If InStr(aFilenames(0, i), "*") > 0 Then
					MsgBox(LRS(60139), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation)
					bContinue = False
				End If
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				If (InStr(aFilenames(0, i), "$") > 0 And InStr(aFilenames(0, i), "�") > 0) Then
					MsgBox(LRS(60140), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation)
					bContinue = False
				End If
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				If Len(aFilenames(0, i)) - Len(Replace(aFilenames(0, i), "�", "")) > 1 Then
					MsgBox(LRS(60141), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation)
					bContinue = False
				End If
				
				' Either :\ or \\ or // in filename/path;
				'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				If bContinue And Len(Trim(aFilenames(0, i))) > 0 Then
					'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					If InStr(aFilenames(0, i), ":\") = 0 And InStr(aFilenames(0, i), "\\") = 0 And InStr(aFilenames(0, i), "//") = 0 Then
						'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(0, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						MsgBox(LRS(60193) & " " & aFilenames(0, i), MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation) ' Invalid filename!
						bContinue = False
						Me.txtFilename.Focus()
					End If
				End If
			Next i
			
			If bContinue Then
				Me.Hide()
				WizardMove((-131 + 14))
			End If
		End If
	End Sub
End Class
