<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMatch
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents chkValidate As System.Windows.Forms.CheckBox
	Public WithEvents chkMatchOCR As System.Windows.Forms.CheckBox
	Public WithEvents chkAutoMatch As System.Windows.Forms.CheckBox
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkValidate = New System.Windows.Forms.CheckBox
        Me.chkMatchOCR = New System.Windows.Forms.CheckBox
        Me.chkAutoMatch = New System.Windows.Forms.CheckBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.cmdSpecifyFileContent = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'chkValidate
        '
        Me.chkValidate.BackColor = System.Drawing.SystemColors.Control
        Me.chkValidate.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkValidate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkValidate.Location = New System.Drawing.Point(213, 125)
        Me.chkValidate.Name = "chkValidate"
        Me.chkValidate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkValidate.Size = New System.Drawing.Size(281, 25)
        Me.chkValidate.TabIndex = 4
        Me.chkValidate.Text = "60545 - Validate fileformat before import"
        Me.chkValidate.UseVisualStyleBackColor = False
        '
        'chkMatchOCR
        '
        Me.chkMatchOCR.BackColor = System.Drawing.SystemColors.Control
        Me.chkMatchOCR.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkMatchOCR.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkMatchOCR.Location = New System.Drawing.Point(247, 100)
        Me.chkMatchOCR.Name = "chkMatchOCR"
        Me.chkMatchOCR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkMatchOCR.Size = New System.Drawing.Size(205, 25)
        Me.chkMatchOCR.TabIndex = 3
        Me.chkMatchOCR.Text = "Match OCR-payments"
        Me.chkMatchOCR.UseVisualStyleBackColor = False
        '
        'chkAutoMatch
        '
        Me.chkAutoMatch.BackColor = System.Drawing.SystemColors.Control
        Me.chkAutoMatch.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkAutoMatch.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAutoMatch.Location = New System.Drawing.Point(214, 77)
        Me.chkAutoMatch.Name = "chkAutoMatch"
        Me.chkAutoMatch.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAutoMatch.Size = New System.Drawing.Size(281, 25)
        Me.chkAutoMatch.TabIndex = 2
        Me.chkAutoMatch.Text = "Automatic matching"
        Me.chkAutoMatch.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(420, 243)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 1
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(332, 243)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 235)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(490, 1)
        Me.lblLine1.TabIndex = 58
        Me.lblLine1.Text = "Label1"
        '
        'cmdSpecifyFileContent
        '
        Me.cmdSpecifyFileContent.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSpecifyFileContent.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSpecifyFileContent.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSpecifyFileContent.Location = New System.Drawing.Point(214, 156)
        Me.cmdSpecifyFileContent.Name = "cmdSpecifyFileContent"
        Me.cmdSpecifyFileContent.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSpecifyFileContent.Size = New System.Drawing.Size(238, 25)
        Me.cmdSpecifyFileContent.TabIndex = 59
        Me.cmdSpecifyFileContent.Text = "60693 - Specify file content"
        Me.cmdSpecifyFileContent.UseVisualStyleBackColor = False
        '
        'frmMatch
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(509, 277)
        Me.Controls.Add(Me.cmdSpecifyFileContent)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.chkValidate)
        Me.Controls.Add(Me.chkMatchOCR)
        Me.Controls.Add(Me.chkAutoMatch)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmMatch"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Avansert oppsett"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents cmdSpecifyFileContent As System.Windows.Forms.Button
#End Region 
End Class
