<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDnB_TBIW_NBcodes
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents txtExplanation As System.Windows.Forms.TextBox
	Public WithEvents cmbCode As System.Windows.Forms.ComboBox
	Public WithEvents lblDescription As System.Windows.Forms.Label
    Public WithEvents lblExplanation As System.Windows.Forms.Label
	Public WithEvents lblCode As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdOK = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.txtExplanation = New System.Windows.Forms.TextBox
        Me.cmbCode = New System.Windows.Forms.ComboBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblExplanation = New System.Windows.Forms.Label
        Me.lblCode = New System.Windows.Forms.Label
        Me.chkForCurrencyOnly = New System.Windows.Forms.CheckBox
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(466, 285)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 5
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(386, 285)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 4
        Me.CmdCancel.TabStop = False
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'txtExplanation
        '
        Me.txtExplanation.AcceptsReturn = True
        Me.txtExplanation.BackColor = System.Drawing.SystemColors.Window
        Me.txtExplanation.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtExplanation.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtExplanation.Location = New System.Drawing.Point(204, 208)
        Me.txtExplanation.MaxLength = 0
        Me.txtExplanation.Name = "txtExplanation"
        Me.txtExplanation.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtExplanation.Size = New System.Drawing.Size(334, 20)
        Me.txtExplanation.TabIndex = 1
        '
        'cmbCode
        '
        Me.cmbCode.BackColor = System.Drawing.SystemColors.Window
        Me.cmbCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbCode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbCode.Location = New System.Drawing.Point(204, 160)
        Me.cmbCode.Name = "cmbCode"
        Me.cmbCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbCode.Size = New System.Drawing.Size(334, 21)
        Me.cmbCode.TabIndex = 0
        '
        'lblDescription
        '
        Me.lblDescription.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescription.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescription.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescription.Location = New System.Drawing.Point(204, 37)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescription.Size = New System.Drawing.Size(334, 65)
        Me.lblDescription.TabIndex = 6
        Me.lblDescription.Text = "60441 - This standard code and text will ....."
        '
        'lblExplanation
        '
        Me.lblExplanation.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplanation.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplanation.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplanation.Location = New System.Drawing.Point(204, 190)
        Me.lblExplanation.Name = "lblExplanation"
        Me.lblExplanation.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplanation.Size = New System.Drawing.Size(145, 17)
        Me.lblExplanation.TabIndex = 3
        Me.lblExplanation.Text = "60443 - Explanation"
        '
        'lblCode
        '
        Me.lblCode.BackColor = System.Drawing.SystemColors.Control
        Me.lblCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCode.Location = New System.Drawing.Point(204, 141)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCode.Size = New System.Drawing.Size(129, 17)
        Me.lblCode.TabIndex = 2
        Me.lblCode.Text = "60442 - Code"
        '
        'chkForCurrencyOnly
        '
        Me.chkForCurrencyOnly.Location = New System.Drawing.Point(208, 113)
        Me.chkForCurrencyOnly.Name = "chkForCurrencyOnly"
        Me.chkForCurrencyOnly.Size = New System.Drawing.Size(329, 23)
        Me.chkForCurrencyOnly.TabIndex = 8
        Me.chkForCurrencyOnly.Text = "60616 Use only for currencies other than NOK"
        Me.chkForCurrencyOnly.UseVisualStyleBackColor = True
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 277)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(535, 1)
        Me.lblLine1.TabIndex = 57
        Me.lblLine1.Text = "Label1"
        '
        'frmDnB_TBIW_NBcodes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(562, 318)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.chkForCurrencyOnly)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.txtExplanation)
        Me.Controls.Add(Me.cmbCode)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.lblExplanation)
        Me.Controls.Add(Me.lblCode)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmDnB_TBIW_NBcodes"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "60440 - Information to Norwegian Statebank"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkForCurrencyOnly As System.Windows.Forms.CheckBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
