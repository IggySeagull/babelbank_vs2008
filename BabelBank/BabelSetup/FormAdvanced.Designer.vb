<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmAdvanced
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtBackupPath As System.Windows.Forms.TextBox
	Public WithEvents cmdFileOpen As System.Windows.Forms.Button
	Public WithEvents chkRedoWrong As System.Windows.Forms.CheckBox
	Public WithEvents chkSplitAfter450 As System.Windows.Forms.CheckBox
	Public WithEvents cmdStructuredText As System.Windows.Forms.Button
	Public WithEvents optErrorsOnly As System.Windows.Forms.RadioButton
	Public WithEvents optNone As System.Windows.Forms.RadioButton
	Public WithEvents optErrorsWarnings As System.Windows.Forms.RadioButton
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents chkCompanyNoOnClient As System.Windows.Forms.CheckBox
	Public WithEvents cmdJCL As System.Windows.Forms.Button
	Public WithEvents txtAdditionalNo As System.Windows.Forms.TextBox
	Public WithEvents txtDivision As System.Windows.Forms.TextBox
	Public WithEvents txtCompanyNo As System.Windows.Forms.TextBox
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdNBCodes As System.Windows.Forms.Button
	Public WithEvents chkRejectWrong As System.Windows.Forms.CheckBox
	Public WithEvents chkKeepClients As System.Windows.Forms.CheckBox
	Public WithEvents chkDateAll As System.Windows.Forms.CheckBox
	Public WithEvents chkDateDue As System.Windows.Forms.CheckBox
	Public WithEvents chkAccount As System.Windows.Forms.CheckBox
	Public WithEvents chkNoCity As System.Windows.Forms.CheckBox
	Public WithEvents chkNegativeAmount As System.Windows.Forms.CheckBox
	Public WithEvents chkAccumulation As System.Windows.Forms.CheckBox
	Public WithEvents lblBackupPath As System.Windows.Forms.Label
    Public WithEvents lblAdditionalNo As System.Windows.Forms.Label
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents lblDivision As System.Windows.Forms.Label
	Public WithEvents lblCompanyNo As System.Windows.Forms.Label
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdvanced))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtBackupPath = New System.Windows.Forms.TextBox
        Me.cmdFileOpen = New System.Windows.Forms.Button
        Me.chkRedoWrong = New System.Windows.Forms.CheckBox
        Me.chkSplitAfter450 = New System.Windows.Forms.CheckBox
        Me.cmdStructuredText = New System.Windows.Forms.Button
        Me.Frame1 = New System.Windows.Forms.GroupBox
        Me.optErrorsOnly = New System.Windows.Forms.RadioButton
        Me.optNone = New System.Windows.Forms.RadioButton
        Me.optErrorsWarnings = New System.Windows.Forms.RadioButton
        Me.chkCompanyNoOnClient = New System.Windows.Forms.CheckBox
        Me.cmdJCL = New System.Windows.Forms.Button
        Me.txtAdditionalNo = New System.Windows.Forms.TextBox
        Me.txtDivision = New System.Windows.Forms.TextBox
        Me.txtCompanyNo = New System.Windows.Forms.TextBox
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdNBCodes = New System.Windows.Forms.Button
        Me.chkRejectWrong = New System.Windows.Forms.CheckBox
        Me.chkKeepClients = New System.Windows.Forms.CheckBox
        Me.chkDateAll = New System.Windows.Forms.CheckBox
        Me.chkDateDue = New System.Windows.Forms.CheckBox
        Me.chkAccount = New System.Windows.Forms.CheckBox
        Me.chkNoCity = New System.Windows.Forms.CheckBox
        Me.chkNegativeAmount = New System.Windows.Forms.CheckBox
        Me.chkAccumulation = New System.Windows.Forms.CheckBox
        Me.lblBackupPath = New System.Windows.Forms.Label
        Me.lblAdditionalNo = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblDivision = New System.Windows.Forms.Label
        Me.lblCompanyNo = New System.Windows.Forms.Label
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.cmbEncoding = New System.Windows.Forms.ComboBox
        Me.Frame1.SuspendLayout()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtBackupPath
        '
        Me.txtBackupPath.AcceptsReturn = True
        Me.txtBackupPath.BackColor = System.Drawing.SystemColors.Window
        Me.txtBackupPath.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBackupPath.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBackupPath.Location = New System.Drawing.Point(280, 408)
        Me.txtBackupPath.MaxLength = 150
        Me.txtBackupPath.Name = "txtBackupPath"
        Me.txtBackupPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBackupPath.Size = New System.Drawing.Size(253, 20)
        Me.txtBackupPath.TabIndex = 13
        '
        'cmdFileOpen
        '
        Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
        Me.cmdFileOpen.Location = New System.Drawing.Point(546, 408)
        Me.cmdFileOpen.Name = "cmdFileOpen"
        Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen.TabIndex = 28
        Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen.UseVisualStyleBackColor = False
        '
        'chkRedoWrong
        '
        Me.chkRedoWrong.BackColor = System.Drawing.SystemColors.Control
        Me.chkRedoWrong.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkRedoWrong.Enabled = False
        Me.chkRedoWrong.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRedoWrong.Location = New System.Drawing.Point(176, 199)
        Me.chkRedoWrong.Name = "chkRedoWrong"
        Me.chkRedoWrong.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkRedoWrong.Size = New System.Drawing.Size(425, 17)
        Me.chkRedoWrong.TabIndex = 8
        Me.chkRedoWrong.Text = "60500 - Gj�r om feil oppdrag (hvis mulig)"
        Me.chkRedoWrong.UseVisualStyleBackColor = False
        '
        'chkSplitAfter450
        '
        Me.chkSplitAfter450.BackColor = System.Drawing.SystemColors.Control
        Me.chkSplitAfter450.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkSplitAfter450.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkSplitAfter450.Location = New System.Drawing.Point(176, 219)
        Me.chkSplitAfter450.Name = "chkSplitAfter450"
        Me.chkSplitAfter450.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkSplitAfter450.Size = New System.Drawing.Size(354, 17)
        Me.chkSplitAfter450.TabIndex = 9
        Me.chkSplitAfter450.Text = "60495 -  Max. 999 betalinger pr fil"
        Me.chkSplitAfter450.UseVisualStyleBackColor = False
        Me.chkSplitAfter450.Visible = False
        '
        'cmdStructuredText
        '
        Me.cmdStructuredText.BackColor = System.Drawing.SystemColors.Control
        Me.cmdStructuredText.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdStructuredText.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdStructuredText.Location = New System.Drawing.Point(442, 377)
        Me.cmdStructuredText.Name = "cmdStructuredText"
        Me.cmdStructuredText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdStructuredText.Size = New System.Drawing.Size(147, 21)
        Me.cmdStructuredText.TabIndex = 27
        Me.cmdStructuredText.Text = "59038-Strukturert fakturanummer"
        Me.cmdStructuredText.UseVisualStyleBackColor = False
        Me.cmdStructuredText.Visible = False
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.SystemColors.Control
        Me.Frame1.Controls.Add(Me.optErrorsOnly)
        Me.Frame1.Controls.Add(Me.optNone)
        Me.Frame1.Controls.Add(Me.optErrorsWarnings)
        Me.Frame1.Enabled = False
        Me.Frame1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Frame1.Location = New System.Drawing.Point(165, 251)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.Padding = New System.Windows.Forms.Padding(0)
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(454, 48)
        Me.Frame1.TabIndex = 23
        Me.Frame1.TabStop = False
        Me.Frame1.Text = "60290 - Bilde for korrigering av feil"
        '
        'optErrorsOnly
        '
        Me.optErrorsOnly.BackColor = System.Drawing.SystemColors.Control
        Me.optErrorsOnly.Cursor = System.Windows.Forms.Cursors.Default
        Me.optErrorsOnly.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optErrorsOnly.Location = New System.Drawing.Point(159, 17)
        Me.optErrorsOnly.Name = "optErrorsOnly"
        Me.optErrorsOnly.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optErrorsOnly.Size = New System.Drawing.Size(130, 24)
        Me.optErrorsOnly.TabIndex = 26
        Me.optErrorsOnly.TabStop = True
        Me.optErrorsOnly.Text = "60292 - Kun feil"
        Me.optErrorsOnly.UseVisualStyleBackColor = False
        '
        'optNone
        '
        Me.optNone.BackColor = System.Drawing.SystemColors.Control
        Me.optNone.Cursor = System.Windows.Forms.Cursors.Default
        Me.optNone.Enabled = False
        Me.optNone.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optNone.Location = New System.Drawing.Point(297, 16)
        Me.optNone.Name = "optNone"
        Me.optNone.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optNone.Size = New System.Drawing.Size(149, 25)
        Me.optNone.TabIndex = 25
        Me.optNone.TabStop = True
        Me.optNone.Text = "60293 - Ingen visning"
        Me.optNone.UseVisualStyleBackColor = False
        '
        'optErrorsWarnings
        '
        Me.optErrorsWarnings.BackColor = System.Drawing.SystemColors.Control
        Me.optErrorsWarnings.Cursor = System.Windows.Forms.Cursors.Default
        Me.optErrorsWarnings.Enabled = False
        Me.optErrorsWarnings.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optErrorsWarnings.Location = New System.Drawing.Point(12, 16)
        Me.optErrorsWarnings.Name = "optErrorsWarnings"
        Me.optErrorsWarnings.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optErrorsWarnings.Size = New System.Drawing.Size(140, 24)
        Me.optErrorsWarnings.TabIndex = 24
        Me.optErrorsWarnings.TabStop = True
        Me.optErrorsWarnings.Text = "60291 - B�de Feil og Varsler"
        Me.optErrorsWarnings.UseVisualStyleBackColor = False
        '
        'chkCompanyNoOnClient
        '
        Me.chkCompanyNoOnClient.BackColor = System.Drawing.SystemColors.Control
        Me.chkCompanyNoOnClient.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkCompanyNoOnClient.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkCompanyNoOnClient.Location = New System.Drawing.Point(442, 307)
        Me.chkCompanyNoOnClient.Name = "chkCompanyNoOnClient"
        Me.chkCompanyNoOnClient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkCompanyNoOnClient.Size = New System.Drawing.Size(175, 14)
        Me.chkCompanyNoOnClient.TabIndex = 21
        Me.chkCompanyNoOnClient.Text = "Foretaksnr. pr klient"
        Me.chkCompanyNoOnClient.UseVisualStyleBackColor = False
        Me.chkCompanyNoOnClient.Visible = False
        '
        'cmdJCL
        '
        Me.cmdJCL.BackColor = System.Drawing.SystemColors.Control
        Me.cmdJCL.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdJCL.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdJCL.Location = New System.Drawing.Point(442, 324)
        Me.cmdJCL.Name = "cmdJCL"
        Me.cmdJCL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdJCL.Size = New System.Drawing.Size(147, 21)
        Me.cmdJCL.TabIndex = 20
        Me.cmdJCL.Text = "&JCL"
        Me.cmdJCL.UseVisualStyleBackColor = False
        '
        'txtAdditionalNo
        '
        Me.txtAdditionalNo.AcceptsReturn = True
        Me.txtAdditionalNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtAdditionalNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAdditionalNo.Enabled = False
        Me.txtAdditionalNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAdditionalNo.Location = New System.Drawing.Point(280, 350)
        Me.txtAdditionalNo.MaxLength = 0
        Me.txtAdditionalNo.Name = "txtAdditionalNo"
        Me.txtAdditionalNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAdditionalNo.Size = New System.Drawing.Size(145, 20)
        Me.txtAdditionalNo.TabIndex = 11
        '
        'txtDivision
        '
        Me.txtDivision.AcceptsReturn = True
        Me.txtDivision.BackColor = System.Drawing.SystemColors.Window
        Me.txtDivision.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDivision.Enabled = False
        Me.txtDivision.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDivision.Location = New System.Drawing.Point(280, 377)
        Me.txtDivision.MaxLength = 0
        Me.txtDivision.Name = "txtDivision"
        Me.txtDivision.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDivision.Size = New System.Drawing.Size(145, 20)
        Me.txtDivision.TabIndex = 12
        '
        'txtCompanyNo
        '
        Me.txtCompanyNo.AcceptsReturn = True
        Me.txtCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtCompanyNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCompanyNo.Enabled = False
        Me.txtCompanyNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCompanyNo.Location = New System.Drawing.Point(280, 324)
        Me.txtCompanyNo.MaxLength = 0
        Me.txtCompanyNo.Name = "txtCompanyNo"
        Me.txtCompanyNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCompanyNo.Size = New System.Drawing.Size(145, 20)
        Me.txtCompanyNo.TabIndex = 10
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(144, 448)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 15
        Me.CmdHelp.Text = "&Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(280, 448)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 16
        Me.CmdCancel.Text = "&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(431, 448)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 17
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdNBCodes
        '
        Me.cmdNBCodes.BackColor = System.Drawing.SystemColors.Control
        Me.cmdNBCodes.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdNBCodes.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNBCodes.Location = New System.Drawing.Point(442, 350)
        Me.cmdNBCodes.Name = "cmdNBCodes"
        Me.cmdNBCodes.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdNBCodes.Size = New System.Drawing.Size(147, 21)
        Me.cmdNBCodes.TabIndex = 14
        Me.cmdNBCodes.Text = "&Bruk standardinnstillinger"
        Me.cmdNBCodes.UseVisualStyleBackColor = False
        Me.cmdNBCodes.Visible = False
        '
        'chkRejectWrong
        '
        Me.chkRejectWrong.BackColor = System.Drawing.SystemColors.Control
        Me.chkRejectWrong.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkRejectWrong.Enabled = False
        Me.chkRejectWrong.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRejectWrong.Location = New System.Drawing.Point(176, 178)
        Me.chkRejectWrong.Name = "chkRejectWrong"
        Me.chkRejectWrong.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkRejectWrong.Size = New System.Drawing.Size(425, 17)
        Me.chkRejectWrong.TabIndex = 7
        Me.chkRejectWrong.Text = "Avvis feilmarkerte transaksjoner?"
        Me.chkRejectWrong.UseVisualStyleBackColor = False
        '
        'chkKeepClients
        '
        Me.chkKeepClients.BackColor = System.Drawing.SystemColors.Control
        Me.chkKeepClients.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkKeepClients.Enabled = False
        Me.chkKeepClients.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkKeepClients.Location = New System.Drawing.Point(176, 158)
        Me.chkKeepClients.Name = "chkKeepClients"
        Me.chkKeepClients.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkKeepClients.Size = New System.Drawing.Size(425, 17)
        Me.chkKeepClients.TabIndex = 6
        Me.chkKeepClients.Text = "Behold klientinndeling i filen"
        Me.chkKeepClients.UseVisualStyleBackColor = False
        '
        'chkDateAll
        '
        Me.chkDateAll.BackColor = System.Drawing.SystemColors.Control
        Me.chkDateAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkDateAll.Enabled = False
        Me.chkDateAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkDateAll.Location = New System.Drawing.Point(176, 137)
        Me.chkDateAll.Name = "chkDateAll"
        Me.chkDateAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkDateAll.Size = New System.Drawing.Size(425, 17)
        Me.chkDateAll.TabIndex = 5
        Me.chkDateAll.Text = "Endre forfallsdato til dagens dato for ALLE oppdrag"
        Me.chkDateAll.UseVisualStyleBackColor = False
        '
        'chkDateDue
        '
        Me.chkDateDue.BackColor = System.Drawing.SystemColors.Control
        Me.chkDateDue.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkDateDue.Enabled = False
        Me.chkDateDue.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkDateDue.Location = New System.Drawing.Point(176, 117)
        Me.chkDateDue.Name = "chkDateDue"
        Me.chkDateDue.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkDateDue.Size = New System.Drawing.Size(425, 17)
        Me.chkDateDue.TabIndex = 4
        Me.chkDateDue.Text = "Endre dato p� FORFALTE oppdrag til dagens dato"
        Me.chkDateDue.UseVisualStyleBackColor = False
        '
        'chkAccount
        '
        Me.chkAccount.BackColor = System.Drawing.SystemColors.Control
        Me.chkAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkAccount.Enabled = False
        Me.chkAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAccount.Location = New System.Drawing.Point(176, 97)
        Me.chkAccount.Name = "chkAccount"
        Me.chkAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAccount.Size = New System.Drawing.Size(425, 17)
        Me.chkAccount.TabIndex = 3
        Me.chkAccount.Text = "Avvis transaksjoner hvis mottakers konto er ugyldig (kun Norge)"
        Me.chkAccount.UseVisualStyleBackColor = False
        '
        'chkNoCity
        '
        Me.chkNoCity.BackColor = System.Drawing.SystemColors.Control
        Me.chkNoCity.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkNoCity.Enabled = False
        Me.chkNoCity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkNoCity.Location = New System.Drawing.Point(176, 76)
        Me.chkNoCity.Name = "chkNoCity"
        Me.chkNoCity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkNoCity.Size = New System.Drawing.Size(425, 17)
        Me.chkNoCity.TabIndex = 2
        Me.chkNoCity.Text = "Avvis transaksjoners hvis de mangler postnummer eller poststed"
        Me.chkNoCity.UseVisualStyleBackColor = False
        '
        'chkNegativeAmount
        '
        Me.chkNegativeAmount.BackColor = System.Drawing.SystemColors.Control
        Me.chkNegativeAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkNegativeAmount.Enabled = False
        Me.chkNegativeAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkNegativeAmount.Location = New System.Drawing.Point(176, 56)
        Me.chkNegativeAmount.Name = "chkNegativeAmount"
        Me.chkNegativeAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkNegativeAmount.Size = New System.Drawing.Size(425, 17)
        Me.chkNegativeAmount.TabIndex = 1
        Me.chkNegativeAmount.Text = "Avvis transaksjoner hvis de har negativt bel�p"
        Me.chkNegativeAmount.UseVisualStyleBackColor = False
        '
        'chkAccumulation
        '
        Me.chkAccumulation.BackColor = System.Drawing.SystemColors.Control
        Me.chkAccumulation.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkAccumulation.Enabled = False
        Me.chkAccumulation.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAccumulation.Location = New System.Drawing.Point(176, 35)
        Me.chkAccumulation.Name = "chkAccumulation"
        Me.chkAccumulation.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAccumulation.Size = New System.Drawing.Size(425, 17)
        Me.chkAccumulation.TabIndex = 0
        Me.chkAccumulation.Text = "Akkumul�r transaksjoner"
        Me.chkAccumulation.UseVisualStyleBackColor = False
        '
        'lblBackupPath
        '
        Me.lblBackupPath.BackColor = System.Drawing.SystemColors.Control
        Me.lblBackupPath.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBackupPath.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBackupPath.Location = New System.Drawing.Point(176, 409)
        Me.lblBackupPath.Name = "lblBackupPath"
        Me.lblBackupPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBackupPath.Size = New System.Drawing.Size(97, 19)
        Me.lblBackupPath.TabIndex = 29
        Me.lblBackupPath.Text = "Backupkatalog"
        '
        'lblAdditionalNo
        '
        Me.lblAdditionalNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdditionalNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdditionalNo.Enabled = False
        Me.lblAdditionalNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdditionalNo.Location = New System.Drawing.Point(176, 356)
        Me.lblAdditionalNo.Name = "lblAdditionalNo"
        Me.lblAdditionalNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdditionalNo.Size = New System.Drawing.Size(93, 19)
        Me.lblAdditionalNo.TabIndex = 22
        Me.lblAdditionalNo.Text = "Kundeenhetsid"
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(107, 307)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(21, 16)
        Me._Image1_0.TabIndex = 30
        Me._Image1_0.TabStop = False
        '
        'lblDivision
        '
        Me.lblDivision.BackColor = System.Drawing.SystemColors.Control
        Me.lblDivision.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDivision.Enabled = False
        Me.lblDivision.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDivision.Location = New System.Drawing.Point(176, 382)
        Me.lblDivision.Name = "lblDivision"
        Me.lblDivision.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDivision.Size = New System.Drawing.Size(93, 20)
        Me.lblDivision.TabIndex = 19
        Me.lblDivision.Text = "Divisjon"
        '
        'lblCompanyNo
        '
        Me.lblCompanyNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblCompanyNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompanyNo.Enabled = False
        Me.lblCompanyNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompanyNo.Location = New System.Drawing.Point(176, 327)
        Me.lblCompanyNo.Name = "lblCompanyNo"
        Me.lblCompanyNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompanyNo.Size = New System.Drawing.Size(93, 19)
        Me.lblCompanyNo.TabIndex = 18
        Me.lblCompanyNo.Text = "Foretaksnummer"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(18, 440)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(595, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'cmbEncoding
        '
        Me.cmbEncoding.Enabled = False
        Me.cmbEncoding.FormattingEnabled = True
        Me.cmbEncoding.Location = New System.Drawing.Point(280, 356)
        Me.cmbEncoding.Name = "cmbEncoding"
        Me.cmbEncoding.Size = New System.Drawing.Size(121, 21)
        Me.cmbEncoding.TabIndex = 11
        Me.cmbEncoding.Visible = False
        '
        'frmAdvanced
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(631, 480)
        Me.Controls.Add(Me.cmbEncoding)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtBackupPath)
        Me.Controls.Add(Me.cmdFileOpen)
        Me.Controls.Add(Me.chkRedoWrong)
        Me.Controls.Add(Me.chkSplitAfter450)
        Me.Controls.Add(Me.cmdStructuredText)
        Me.Controls.Add(Me.Frame1)
        Me.Controls.Add(Me.chkCompanyNoOnClient)
        Me.Controls.Add(Me.cmdJCL)
        Me.Controls.Add(Me.txtAdditionalNo)
        Me.Controls.Add(Me.txtDivision)
        Me.Controls.Add(Me.txtCompanyNo)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdNBCodes)
        Me.Controls.Add(Me.chkRejectWrong)
        Me.Controls.Add(Me.chkKeepClients)
        Me.Controls.Add(Me.chkDateAll)
        Me.Controls.Add(Me.chkDateDue)
        Me.Controls.Add(Me.chkAccount)
        Me.Controls.Add(Me.chkNoCity)
        Me.Controls.Add(Me.chkNegativeAmount)
        Me.Controls.Add(Me.chkAccumulation)
        Me.Controls.Add(Me.lblBackupPath)
        Me.Controls.Add(Me.lblAdditionalNo)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblDivision)
        Me.Controls.Add(Me.lblCompanyNo)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAdvanced"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Avansert oppsett"
        Me.Frame1.ResumeLayout(False)
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents cmbEncoding As System.Windows.Forms.ComboBox
#End Region 
End Class
