Option Strict Off
Option Explicit On
Friend Class frmLog
	Inherits System.Windows.Forms.Form
	
	'UPGRADE_WARNING: Event chkLog.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub chkLog_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkLog.CheckStateChanged
        EnableDisable()
    End Sub
    Private Sub EnableDisable()
        If chkLog.CheckState Then
            Me.optWinLog.Enabled = True
            Me.OptFileLog.Enabled = True
            Me.optMailLog.Enabled = True
            Me.lblFilename.Enabled = True
            Me.lblMaxLines.Enabled = True
            Me.txtFilename.Enabled = True
            Me.txtMaxLines.Enabled = True
            Me.chkOverwrite.Enabled = True
            Me.chkShowStartStop.Enabled = True
            Me.cmdFileOpen.Enabled = True
        Else
            Me.optWinLog.Enabled = False
            Me.OptFileLog.Enabled = False
            Me.optMailLog.Enabled = False
            Me.lblFilename.Enabled = False
            Me.lblMaxLines.Enabled = False
            Me.txtFilename.Enabled = False
            Me.txtMaxLines.Enabled = False
            Me.chkOverwrite.Enabled = False
            Me.chkShowStartStop.Enabled = False
            Me.cmdFileOpen.Enabled = False
        End If

    End Sub

    Private Sub frmLog_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "")
        FormLRSCaptions(Me)
        EnableDisable()
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Me.Hide()
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.Hide()
        SaveLog()
    End Sub

    'UPGRADE_WARNING: Event OptFileLog.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub OptFileLog_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles OptFileLog.CheckedChanged
        If eventSender.Checked Then
            If OptFileLog.Checked Then
                Me.lblFilename.Enabled = True
                Me.lblMaxLines.Enabled = True
                Me.txtFilename.Enabled = True
                Me.txtMaxLines.Enabled = True
                Me.chkOverwrite.Enabled = True
                Me.chkShowStartStop.Enabled = True
                Me.cmdFileOpen.Enabled = True
            End If
        End If
    End Sub

    'UPGRADE_WARNING: Event optMailLog.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optMailLog_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optMailLog.CheckedChanged
        If eventSender.Checked Then
            If optMailLog.Checked Then
                Me.lblFilename.Enabled = False
                Me.lblMaxLines.Enabled = False
                Me.txtFilename.Enabled = False
                Me.txtMaxLines.Enabled = False
                Me.chkOverwrite.Enabled = False
                Me.chkShowStartStop.Enabled = True
                Me.cmdFileOpen.Enabled = False
            End If

        End If
    End Sub

    'UPGRADE_WARNING: Event optWinLog.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optWinLog_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optWinLog.CheckedChanged
        If eventSender.Checked Then
            If optWinLog.Checked Then
                Me.lblFilename.Enabled = False
                Me.lblMaxLines.Enabled = False
                Me.txtFilename.Enabled = False
                Me.txtMaxLines.Enabled = False
                Me.chkOverwrite.Enabled = False
                Me.chkShowStartStop.Enabled = True
                Me.cmdFileOpen.Enabled = False
            End If
        End If
    End Sub
    Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
        ' Find filename
        Dim spath As String
        Dim sFile As String
        ' Keep filename after Browsing for folder;
        ' Must have \ and .
        If InStr(Me.txtFilename.Text, "\") > 0 And InStr(Me.txtFilename.Text, ".") > 0 Then
            sFile = Mid(Me.txtFilename.Text, InStrRev(Me.txtFilename.Text, "\"))
        ElseIf InStr(Me.txtFilename.Text, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & Me.txtFilename.Text
        Else
            sFile = ""
        End If

        ' Use BrowseForFolders, routine from VBNet ( i Utils)
        'spath = FolderPicker(Me, LRS(60010))   'Velg fil")
        ' spath ends with end of string - remove
        'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)

        ' Changed 18.10.05
        ' using a new function where we can pass a path
        'spath = FixPath((Me.txtFilename.Text))
        spath = BrowseForFilesOrFolders(Me.txtFilename.Text, Me, LRS(60010), True)

        If Len(spath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(spath, ".") = 0 Then
                Me.txtFilename.Text = Replace(spath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                Me.txtFilename.Text = spath
            End If
        End If
    End Sub

    Private Sub txtMaxLines_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtMaxLines.Validating
        Dim bKeepFocus As Boolean = eventArgs.Cancel

        If Not IsNumeric(Me.txtMaxLines.Text) Then
            MsgBox(LRS(60203), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Only numerics allowed
            bKeepFocus = True
        End If

        eventArgs.Cancel = bKeepFocus
    End Sub

    Private Sub SaveLog()
        oFilesetup.Log = CBool(Me.chkLog.CheckState)
        oFilesetup.LogType = IIf(Me.optWinLog.Checked, 1, IIf(Me.OptFileLog.Checked, 2, 3))
        oFilesetup.LogFilename = Trim(Me.txtFilename.Text)
        oFilesetup.LogMaxLines = Val(Me.txtMaxLines.Text)
        oFilesetup.LogOverwrite = CBool(Me.chkOverwrite.CheckState)
        oFilesetup.LogShowStartStop = CBool(Me.chkShowStartStop.CheckState)
        'Select Case Me.lstDetail.SelectedIndex
        '    Case 0
        '        oFilesetup.LogDetails = 1 ' Errors
        '    Case 1
        '        oFilesetup.LogDetails = 2 ' Warnings
        '    Case 2
        '        oFilesetup.LogDetails = 4 ' Info
        '    Case 3
        '        oFilesetup.LogDetails = 8 ' All
        'End Select
        ' changed 12.09.2018
        Select Case Me.lstDetail.SelectedIndex
            Case 0
                oFilesetup.LogDetails = 2 ' Errors
            Case 1
                oFilesetup.LogDetails = 4 ' Warnings
            Case 2
                oFilesetup.LogDetails = 8 ' Info
            Case 3
                oFilesetup.LogDetails = 16 ' All
        End Select

    End Sub
   
    Private Sub txtFilename_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFilename.Leave

        CheckPath(txtFilename.Text, True, True)

    End Sub
End Class
