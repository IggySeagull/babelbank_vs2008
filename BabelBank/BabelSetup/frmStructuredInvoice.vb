Option Strict Off
Option Explicit On
Friend Class frmStructuredInvoice
    Inherits System.Windows.Forms.Form
    'Private frmClientSettings As frmClientSettings

    Private bAnythingChanged As Boolean

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click

        If bAnythingChanged Then
            If MsgBox(LRS(60011), MsgBoxStyle.YesNo + MsgBoxStyle.Question, "BabelBank") = MsgBoxResult.Yes Then
                ' Hide without saving
                bAnythingChanged = False
                Me.Hide()
            End If
        Else
            bAnythingChanged = False
            Me.Hide()
        End If
    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdOK.Click
        Me.Hide()
        SaveStructuredInvoice()

    End Sub

    Sub frmStructuredInvoice_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        FormvbStyle(Me, "dollar.jpg")
        FormLRSCaptions(Me)

        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next

        FormSelectAllTextboxs(Me)
        bAnythingChanged = False

    End Sub

    'Private Sub frmStructuredInvoice_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    ' Dim Cancel As Boolean = EventArgs.Cancel
    ' Dim UnloadMode As System.Windows.Forms.CloseReason = EventArgs.CloseReason
    '		If UnloadMode = 0 Then
    '			Cancel = 1
    '			cmdCancel_Click(cmdCancel, New System.EventArgs())
    '		End If
    '		eventArgs.Cancel = Cancel
    '	End Sub
    '
    'UPGRADE_WARNING: Event txtSearchtext.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtSearchtext_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSearchText.TextChanged
        'frmClientSettings.set_AnythingChanged(True)
        bAnythingChanged = True
    End Sub
    'UPGRADE_WARNING: Event txtNoOfPos.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtNoOfPos_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNoOfPos.TextChanged
        'frmClientSettings.bAnythingChanged = True
        bAnythingChanged = True
    End Sub
    'UPGRADE_WARNING: Event txtSearchtextNegative.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtSearchtextNegative_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSearchTextNegative.TextChanged
        'frmClientSettings.bAnythingChanged = True
        bAnythingChanged = True
    End Sub
    'UPGRADE_WARNING: Event txtNoOfPosNegative.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtNoOfPosNegative_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNoOfPosNegative.TextChanged
        'frmClientSettings.bAnythingChanged = True
        bAnythingChanged = True
    End Sub
    Sub SaveStructuredInvoice()
        oFilesetup.StructText = Me.txtSearchText.Text
        oFilesetup.StructInvoicePos = Val(Me.txtNoOfPos.Text)
        oFilesetup.StructTextNegative = Me.txtSearchTextNegative.Text
        oFilesetup.StructInvoicePosNegative = Val(Me.txtNoOfPosNegative.Text)
    End Sub

End Class
