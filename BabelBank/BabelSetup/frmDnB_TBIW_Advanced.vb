Option Strict Off
Option Explicit On

Friend Class frmDnB_TBIW_Advanced
    Inherits System.Windows.Forms.Form
    Private frmDnB_TBIW_Accounts As frmDnB_TBIW_Accounts
    Private frmDnB_TBIW_Charges As frmDnB_TBIW_Charges
    Private frmLog As frmLog
    Private frmDnB_TBIW_MailPrint As frmDnB_TBIW_MailPrint
    Private frmDnB_TBIW_MergePayments As frmDnB_TBIW_MergePayments
    Private frmDnB_TBIW_NBcodes As frmDnB_TBIW_NBcodes
    Private frmSpecial As New frmSpecial
    Private frmDNB_TBIW_CurrencyField As frmDNB_TBIW_CurrencyField
    Public Sub Set_frms(ByVal frm1 As frmDnB_TBIW_MergePayments, ByVal frm2 As frmLog, ByVal frm3 As frmDnB_TBIW_Accounts, _
                       ByVal frm4 As frmDnB_TBIW_Accounts, ByVal frm5 As frmDnB_TBIW_Charges, ByVal frm6 As frmDnB_TBIW_NBcodes, ByVal frm7 As frmDNB_TBIW_CurrencyField)
        frmDnB_TBIW_MergePayments = frm1
        frmLog = frm2
        frmDnB_TBIW_Accounts = frm3
        frmDnB_TBIW_Accounts = frm4
        frmDnB_TBIW_Charges = frm5
        frmDnB_TBIW_NBcodes = frm6
        frmDNB_TBIW_CurrencyField = frm7

    End Sub
    Private Sub CmdAccounts_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdAccounts.Click
        frmDnB_TBIW_Accounts.ShowDialog()
    End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
	End Sub
	
	Private Sub cmdCharges_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCharges.Click
		VB6.ShowForm(frmDnB_TBIW_Charges, 1, Me)
	End Sub
	
	Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
        ' Show Helptopic for TBI setup
		'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(120))
		
	End Sub
	
	Private Sub cmdLog_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdLog.Click
		VB6.ShowForm(frmLog, 1, Me)
	End Sub
	
	Private Sub cmdMailPrint_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdMailPrint.Click
        VB6.ShowForm(frmDnB_TBIW_MailPrint, 1, Me)
    End Sub
	
	Private Sub cmdMerge_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdMerge.Click
		VB6.ShowForm(frmDnB_TBIW_MergePayments, 1, Me)
	End Sub
	
	Private Sub CmdNBCodes_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdNBCodes.Click
		
		'frmDnB_TBIW_NBcodes.SetFileSetup oFilesetup
		VB6.ShowForm(frmDnB_TBIW_NBcodes, 1, Me)
        'frmDnB_TBIW_NBcodes.Close()
		
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		Me.Hide()
	End Sub
	
	Private Sub cmdSpecial_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSpecial.Click
		frmSpecial.Text = LRS(60137)
		frmSpecial.lblSpecial.Text = Mid(LRS(60137), 2)
		frmSpecial.CmdCancel.Text = LRS(55002)
		frmSpecial.txtSpecial.Text = Trim(oFilesetup.TreatSpecial & Space(100))
		VB6.ShowForm(frmSpecial, 1, Me)
	End Sub
	
	
	Private Sub frmDnB_TBIW_Advanced_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		FormvbStyle(Me, "dollar.jpg", 400)
		FormLRSCaptions(Me)
	End Sub
	
	'UPGRADE_WARNING: Event optErrorsOnly.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub optErrorsOnly_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optErrorsOnly.CheckedChanged
		If eventSender.Checked Then
			If optErrorsOnly.Checked = True Then
				optErrorsWarnings.Checked = False
				optNone.Checked = False
                ' xnet 20.08.2012
                optShowAll.Checked = False
            End If
		End If
	End Sub
	
	'UPGRADE_WARNING: Event optErrorsWarnings.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub optErrorsWarnings_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optErrorsWarnings.CheckedChanged
		If eventSender.Checked Then
			If optErrorsWarnings.Checked = True Then
				optErrorsOnly.Checked = False
				optNone.Checked = False
                ' xnet 20.08.2012
                optShowAll.Checked = False
            End If
		End If
	End Sub
	
	'UPGRADE_WARNING: Event optNone.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub optNone_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optNone.CheckedChanged
		If eventSender.Checked Then
			If optNone.Checked = True Then
				optErrorsOnly.Checked = False
				optErrorsWarnings.Checked = False
                ' xnet 20.08.2012
                optShowAll.Checked = False
            End If
		End If
	End Sub

    Private Sub optShowAll_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles optShowAll.MouseUp
        Dim sResponse As String
        If optShowAll.Checked = True Then
            optErrorsOnly.Checked = False
            optErrorsWarnings.Checked = False
            optNone.Checked = False
            ' ask user for code to allow this;
            ' code is "DNBCONNECTEDIT"
            sResponse = Trim$(InputBox("Code: ", "BabelBank"))  '"Please input accesscode given by DNB"))

            If sResponse = "DNBCONNECTEDIT" Then
                ' OK
            Else
                ' wrong code
                optErrorsOnly.Checked = True
                optErrorsWarnings.Checked = False
                optNone.Checked = False
                optShowAll.Checked = False
            End If
        End If

    End Sub
    ' XNET 30.01.2013 - will need a passcode to be able to save this
    ' From now on this checkbox gives possibility to edit all values in frmCorrect
    Private Sub chkSplitAfter450_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chkSplitAfter450.MouseUp
        Dim sResponse As String

        If chkSplitAfter450.Checked = True Then
            ' ask user for code to allow this;
            ' code is "DNBCONNECTCHANGE"
            sResponse = Trim$(InputBox("Code: ", "BabelBank"))  '"Please input accesscode given by DNB"))

            If sResponse = "DNBCONNECTCHANGE" Then
                ' OK
            Else
                ' wrong code
                Me.chkSplitAfter450.Checked = False
            End If
        End If

    End Sub
    Private Sub cmdCurrencyField_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCurrencyField.Click
        VB6.ShowForm(frmDNB_TBIW_CurrencyField, 1, Me)
        frmDNB_TBIW_CurrencyField = Nothing
    End Sub
End Class
