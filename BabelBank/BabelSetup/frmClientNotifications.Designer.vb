<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmClientNotifications
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdRemoveDiffAccount As System.Windows.Forms.Button
	Public WithEvents cmdAddEasy As System.Windows.Forms.Button
	Public WithEvents txtName As System.Windows.Forms.TextBox
	Public WithEvents txtMailAddress As System.Windows.Forms.TextBox
	Public WithEvents cmdUnselectAll As System.Windows.Forms.Button
	Public WithEvents cmdUnselectOne As System.Windows.Forms.Button
	Public WithEvents cmdSelectAll As System.Windows.Forms.Button
	Public WithEvents cmdSelectOne As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents lstSelected As System.Windows.Forms.ListBox
	Public WithEvents lstPossible As System.Windows.Forms.ListBox
	Public WithEvents lblName As System.Windows.Forms.Label
	Public WithEvents lbMailAddress As System.Windows.Forms.Label
	Public WithEvents lblSelected As System.Windows.Forms.Label
	Public WithEvents lblPossible As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdRemoveDiffAccount = New System.Windows.Forms.Button
        Me.cmdAddEasy = New System.Windows.Forms.Button
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtMailAddress = New System.Windows.Forms.TextBox
        Me.cmdUnselectAll = New System.Windows.Forms.Button
        Me.cmdUnselectOne = New System.Windows.Forms.Button
        Me.cmdSelectAll = New System.Windows.Forms.Button
        Me.cmdSelectOne = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lstSelected = New System.Windows.Forms.ListBox
        Me.lstPossible = New System.Windows.Forms.ListBox
        Me.lblName = New System.Windows.Forms.Label
        Me.lbMailAddress = New System.Windows.Forms.Label
        Me.lblSelected = New System.Windows.Forms.Label
        Me.lblPossible = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdRemoveDiffAccount
        '
        Me.cmdRemoveDiffAccount.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemoveDiffAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRemoveDiffAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRemoveDiffAccount.Location = New System.Drawing.Point(361, 208)
        Me.cmdRemoveDiffAccount.Name = "cmdRemoveDiffAccount"
        Me.cmdRemoveDiffAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRemoveDiffAccount.Size = New System.Drawing.Size(73, 21)
        Me.cmdRemoveDiffAccount.TabIndex = 15
        Me.cmdRemoveDiffAccount.TabStop = False
        Me.cmdRemoveDiffAccount.Text = "55015-Remove"
        Me.cmdRemoveDiffAccount.UseVisualStyleBackColor = False
        '
        'cmdAddEasy
        '
        Me.cmdAddEasy.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddEasy.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAddEasy.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAddEasy.Location = New System.Drawing.Point(361, 234)
        Me.cmdAddEasy.Name = "cmdAddEasy"
        Me.cmdAddEasy.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAddEasy.Size = New System.Drawing.Size(73, 21)
        Me.cmdAddEasy.TabIndex = 12
        Me.cmdAddEasy.TabStop = False
        Me.cmdAddEasy.Text = "55018-Add"
        Me.cmdAddEasy.UseVisualStyleBackColor = False
        '
        'txtName
        '
        Me.txtName.AcceptsReturn = True
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtName.Location = New System.Drawing.Point(152, 208)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtName.Size = New System.Drawing.Size(203, 20)
        Me.txtName.TabIndex = 0
        '
        'txtMailAddress
        '
        Me.txtMailAddress.AcceptsReturn = True
        Me.txtMailAddress.BackColor = System.Drawing.SystemColors.Window
        Me.txtMailAddress.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMailAddress.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMailAddress.Location = New System.Drawing.Point(152, 232)
        Me.txtMailAddress.MaxLength = 0
        Me.txtMailAddress.Name = "txtMailAddress"
        Me.txtMailAddress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMailAddress.Size = New System.Drawing.Size(203, 20)
        Me.txtMailAddress.TabIndex = 1
        '
        'cmdUnselectAll
        '
        Me.cmdUnselectAll.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUnselectAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdUnselectAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdUnselectAll.Location = New System.Drawing.Point(394, 153)
        Me.cmdUnselectAll.Name = "cmdUnselectAll"
        Me.cmdUnselectAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdUnselectAll.Size = New System.Drawing.Size(39, 28)
        Me.cmdUnselectAll.TabIndex = 9
        Me.cmdUnselectAll.Text = "<<-"
        Me.cmdUnselectAll.UseVisualStyleBackColor = False
        '
        'cmdUnselectOne
        '
        Me.cmdUnselectOne.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUnselectOne.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdUnselectOne.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdUnselectOne.Location = New System.Drawing.Point(394, 120)
        Me.cmdUnselectOne.Name = "cmdUnselectOne"
        Me.cmdUnselectOne.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdUnselectOne.Size = New System.Drawing.Size(39, 28)
        Me.cmdUnselectOne.TabIndex = 8
        Me.cmdUnselectOne.Text = "<-"
        Me.cmdUnselectOne.UseVisualStyleBackColor = False
        '
        'cmdSelectAll
        '
        Me.cmdSelectAll.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSelectAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSelectAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSelectAll.Location = New System.Drawing.Point(394, 87)
        Me.cmdSelectAll.Name = "cmdSelectAll"
        Me.cmdSelectAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSelectAll.Size = New System.Drawing.Size(39, 28)
        Me.cmdSelectAll.TabIndex = 7
        Me.cmdSelectAll.Text = "->>"
        Me.cmdSelectAll.UseVisualStyleBackColor = False
        '
        'cmdSelectOne
        '
        Me.cmdSelectOne.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSelectOne.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSelectOne.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSelectOne.Location = New System.Drawing.Point(394, 54)
        Me.cmdSelectOne.Name = "cmdSelectOne"
        Me.cmdSelectOne.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSelectOne.Size = New System.Drawing.Size(39, 28)
        Me.cmdSelectOne.TabIndex = 6
        Me.cmdSelectOne.Text = "->"
        Me.cmdSelectOne.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(666, 275)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 4
        Me.CmdCancel.TabStop = False
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(743, 275)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 5
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lstSelected
        '
        Me.lstSelected.BackColor = System.Drawing.SystemColors.Window
        Me.lstSelected.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstSelected.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstSelected.Items.AddRange(New Object() {"lstSelected"})
        Me.lstSelected.Location = New System.Drawing.Point(442, 28)
        Me.lstSelected.Name = "lstSelected"
        Me.lstSelected.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstSelected.Size = New System.Drawing.Size(373, 173)
        Me.lstSelected.TabIndex = 3
        '
        'lstPossible
        '
        Me.lstPossible.BackColor = System.Drawing.SystemColors.Window
        Me.lstPossible.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstPossible.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstPossible.Items.AddRange(New Object() {"lstPossible"})
        Me.lstPossible.Location = New System.Drawing.Point(12, 28)
        Me.lstPossible.Name = "lstPossible"
        Me.lstPossible.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstPossible.Size = New System.Drawing.Size(373, 173)
        Me.lstPossible.TabIndex = 2
        '
        'lblName
        '
        Me.lblName.BackColor = System.Drawing.SystemColors.Control
        Me.lblName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblName.Location = New System.Drawing.Point(13, 211)
        Me.lblName.Name = "lblName"
        Me.lblName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblName.Size = New System.Drawing.Size(130, 19)
        Me.lblName.TabIndex = 14
        Me.lblName.Text = "60206-Name"
        '
        'lbMailAddress
        '
        Me.lbMailAddress.BackColor = System.Drawing.SystemColors.Control
        Me.lbMailAddress.Cursor = System.Windows.Forms.Cursors.Default
        Me.lbMailAddress.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbMailAddress.Location = New System.Drawing.Point(13, 235)
        Me.lbMailAddress.Name = "lbMailAddress"
        Me.lbMailAddress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbMailAddress.Size = New System.Drawing.Size(130, 19)
        Me.lbMailAddress.TabIndex = 13
        Me.lbMailAddress.Text = "60035-Mailaddress"
        '
        'lblSelected
        '
        Me.lblSelected.BackColor = System.Drawing.SystemColors.Control
        Me.lblSelected.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSelected.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSelected.Location = New System.Drawing.Point(325, 9)
        Me.lblSelected.Name = "lblSelected"
        Me.lblSelected.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSelected.Size = New System.Drawing.Size(220, 18)
        Me.lblSelected.TabIndex = 11
        Me.lblSelected.Text = "60386 - Valgte varslinger"
        '
        'lblPossible
        '
        Me.lblPossible.BackColor = System.Drawing.SystemColors.Control
        Me.lblPossible.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPossible.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPossible.Location = New System.Drawing.Point(15, 9)
        Me.lblPossible.Name = "lblPossible"
        Me.lblPossible.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPossible.Size = New System.Drawing.Size(239, 18)
        Me.lblPossible.TabIndex = 10
        Me.lblPossible.Text = "60385 - Tilgjenglige varslinger"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 266)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(805, 1)
        Me.lblLine1.TabIndex = 57
        Me.lblLine1.Text = "Label1"
        '
        'frmClientNotifications
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(825, 303)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdRemoveDiffAccount)
        Me.Controls.Add(Me.cmdAddEasy)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.txtMailAddress)
        Me.Controls.Add(Me.cmdUnselectAll)
        Me.Controls.Add(Me.cmdUnselectOne)
        Me.Controls.Add(Me.cmdSelectAll)
        Me.Controls.Add(Me.cmdSelectOne)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lstSelected)
        Me.Controls.Add(Me.lstPossible)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.lbMailAddress)
        Me.Controls.Add(Me.lblSelected)
        Me.Controls.Add(Me.lblPossible)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmClientNotifications"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "60387 - Knytt klient til varslinger"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
