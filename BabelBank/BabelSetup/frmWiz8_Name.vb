Option Strict Off
Option Explicit On

Friend Class frmWiz8_Name
    Inherits System.Windows.Forms.Form
    Private frmLog As New frmLog
    Private frmSignalFile As New frmSignalFile
    'Public Sub SetfrmLog(ByVal frm As frmLog)
    ' ' pass frmLog to frmWiz_Reports
    '     frmLog = frm
    ' End Sub
    'Public Sub SetfrmSignalFile(ByVal frm As frmSignalFile)
    ' ' pass frmSignalFile to frmWiz_Reports
    '     frmSignalFile = frm
    'End Sub
    Private Sub CmdBack_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdBack.Click
        Me.Hide()
        WizardMove((-1))
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        Me.Hide()
        WizardMove((-99))
    End Sub

    Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
        ' Show Helptopic
        'UPGRADE_ISSUE: App property App.HelpFile was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(110))

    End Sub

    Private Sub cmdLog_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdLog.Click
        ' frmLog:
        frmLog.Text = LRS(60195) 'Log
        frmLog.chkLog.Text = LRS(60196) ' Activate logging
        frmLog.optWinLog.Text = LRS(60197) ' Windows EventLog
        frmLog.OptFileLog.Text = LRS(60198) ' Log events to file
        frmLog.optMailLog.Text = LRS(60199) ' Log events to mail
        frmLog.lblFilename.Text = LRS(60200) ' Filnavn
        frmLog.chkOverwrite.Text = LRS(60201) ' New logfile each time
        frmLog.chkShowStartStop.Text = LRS(60324)
        frmLog.lblMaxLines.Text = LRS(60202) ' Max number of lines
        frmLog.chkLog.CheckState = bVal(oFilesetup.Log)
        'If oFilesetup.LogType = 1 Then
        ' frmLog.optWinLog.Checked = True
        ' ElseIf oFilesetup.LogType = 2 Then
        frmLog.OptFileLog.Checked = True
        'Else
        'frmLog.optMailLog.Checked = True
        'End If
        frmLog.txtFilename.Text = oFilesetup.LogFilename
        frmLog.txtMaxLines.Text = Str(oFilesetup.LogMaxLines)
        frmLog.chkOverwrite.CheckState = bVal(oFilesetup.LogOverwrite)
        frmLog.chkShowStartStop.CheckState = bVal(oFilesetup.LogShowStartStop)
        ' Fill listbox for selecting details-level;
        frmLog.lstDetail.Items.Clear()
        frmLog.lstDetail.Items.Add(LRS(60312)) ' Errormessages only
        frmLog.lstDetail.Items.Add(LRS(60313)) ' also warnings
        frmLog.lstDetail.Items.Add(LRS(60314)) ' also info
        frmLog.lstDetail.Items.Add(LRS(60315)) ' all messages
        Select Case oFilesetup.LogDetails
            Case 1
                frmLog.lstDetail.SelectedIndex = 0
            Case 2
                frmLog.lstDetail.SelectedIndex = 1
            Case 4
                frmLog.lstDetail.SelectedIndex = 2
            Case 8
                frmLog.lstDetail.SelectedIndex = 3
        End Select

        VB6.ShowForm(frmLog, 1, Me)
    End Sub

    Private Sub cmdSignalFile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSignalFile.Click
        'New 01.07.2008 - Added form for adding a signalfile
        frmSignalFile.Text = LRS(60541) 'Setup for signalfile
        'You may use this setup if You want to create a file if BabelBank end successfully. If BabelBank doesn't end successfully no file will be created.
        'This may be useful if You want to start or continue a process started outside BabelBank.
        frmSignalFile.lblExplaination.Text = LRS(60542) '
        frmSignalFile.chkActivate.Text = LRS(60543) 'Activate a signalfile
        frmSignalFile.lblFilename.Text = LRS(60200) ' Filnavn
        frmSignalFile.chkActivate.CheckState = bVal(oFilesetup.UseSignalFile)
        frmSignalFile.txtFilename.Text = oFilesetup.FilenameSignalFile

        VB6.ShowForm(frmSignalFile, 1, Me)
    End Sub

    Private Sub frmWiz8_Name_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        FormvbStyle(Me, "dollar.jpg")
        ' 28.12.2010 Had to change to get transparent heading, see next If
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next
    End Sub

    Private Sub CmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdNext.Click
        Me.Hide()
        WizardMove((1))
    End Sub
    'Private Sub cmdShortcut_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdShortcut.Click
    '   If Len(Trim(Me.txtProfilename.Text)) > 0 Then
    '      CreateShortcut((Me.txtProfilename).Text, "BabelBank")
    ' End If
    'End Sub

    Private Sub frmWiz8_Name_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
            cmdCancel_Click(CmdCancel, New System.EventArgs())
        End If
        eventArgs.Cancel = Cancel
    End Sub

    Private Sub txtProfilename_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtProfilename.Enter
        txtProfilename.SelectionStart = 0
        txtProfilename.SelectionLength = Len(txtProfilename.Text)
    End Sub
End Class
