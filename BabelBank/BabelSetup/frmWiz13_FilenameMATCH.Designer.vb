<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmWiz13_FilenameMatch
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdRemoveFile As System.Windows.Forms.Button
	Public WithEvents cmdAddFile As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents CmdFinish As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdNext As System.Windows.Forms.Button
	Public WithEvents CmdBack As System.Windows.Forms.Button
	Public WithEvents chkRest As System.Windows.Forms.CheckBox
	Public WithEvents chkAutogiro As System.Windows.Forms.CheckBox
	Public WithEvents chkOCR As System.Windows.Forms.CheckBox
	Public WithEvents chkGL As System.Windows.Forms.CheckBox
	Public WithEvents lstFormats As System.Windows.Forms.ComboBox
	Public WithEvents lstFiles As System.Windows.Forms.ListBox
	Public WithEvents cmdFileOpen As System.Windows.Forms.Button
	Public WithEvents chkRemove As System.Windows.Forms.CheckBox
	Public WithEvents chkWarning As System.Windows.Forms.CheckBox
	Public WithEvents chkBackup As System.Windows.Forms.CheckBox
	Public WithEvents txtFilename As System.Windows.Forms.TextBox
	Public WithEvents chkOverwrite As System.Windows.Forms.CheckBox
	Public WithEvents fraToAccount As System.Windows.Forms.GroupBox
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents lblHeading As System.Windows.Forms.Label
	Public WithEvents lblDescr As System.Windows.Forms.Label
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmWiz13_FilenameMatch))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.cmdRemoveFile = New System.Windows.Forms.Button
		Me.cmdAddFile = New System.Windows.Forms.Button
		Me.CmdHelp = New System.Windows.Forms.Button
		Me.CmdFinish = New System.Windows.Forms.Button
		Me.CmdCancel = New System.Windows.Forms.Button
		Me.CmdNext = New System.Windows.Forms.Button
		Me.CmdBack = New System.Windows.Forms.Button
		Me.fraToAccount = New System.Windows.Forms.GroupBox
		Me.chkRest = New System.Windows.Forms.CheckBox
		Me.chkAutogiro = New System.Windows.Forms.CheckBox
		Me.chkOCR = New System.Windows.Forms.CheckBox
		Me.chkGL = New System.Windows.Forms.CheckBox
		Me.lstFormats = New System.Windows.Forms.ComboBox
		Me.lstFiles = New System.Windows.Forms.ListBox
		Me.cmdFileOpen = New System.Windows.Forms.Button
		Me.chkRemove = New System.Windows.Forms.CheckBox
		Me.chkWarning = New System.Windows.Forms.CheckBox
		Me.chkBackup = New System.Windows.Forms.CheckBox
		Me.txtFilename = New System.Windows.Forms.TextBox
		Me.chkOverwrite = New System.Windows.Forms.CheckBox
		Me._Image1_0 = New System.Windows.Forms.PictureBox
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.lblHeading = New System.Windows.Forms.Label
		Me.lblDescr = New System.Windows.Forms.Label
		Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(components)
		Me.fraToAccount.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Text = "Returfiler til regnskap"
		Me.ClientSize = New System.Drawing.Size(620, 333)
		Me.Location = New System.Drawing.Point(4, 23)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmWiz13_FilenameMatch"
		Me.cmdRemoveFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdRemoveFile.Text = "55014-Remove File"
		Me.cmdRemoveFile.Size = New System.Drawing.Size(100, 21)
		Me.cmdRemoveFile.Location = New System.Drawing.Point(69, 242)
		Me.cmdRemoveFile.TabIndex = 21
		Me.cmdRemoveFile.BackColor = System.Drawing.SystemColors.Control
		Me.cmdRemoveFile.CausesValidation = True
		Me.cmdRemoveFile.Enabled = True
		Me.cmdRemoveFile.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdRemoveFile.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdRemoveFile.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdRemoveFile.TabStop = True
		Me.cmdRemoveFile.Name = "cmdRemoveFile"
		Me.cmdAddFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdAddFile.Text = "55013-Add File"
		Me.cmdAddFile.Size = New System.Drawing.Size(100, 21)
		Me.cmdAddFile.Location = New System.Drawing.Point(69, 217)
		Me.cmdAddFile.TabIndex = 20
		Me.cmdAddFile.BackColor = System.Drawing.SystemColors.Control
		Me.cmdAddFile.CausesValidation = True
		Me.cmdAddFile.Enabled = True
		Me.cmdAddFile.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdAddFile.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdAddFile.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdAddFile.TabStop = True
		Me.cmdAddFile.Name = "cmdAddFile"
		Me.CmdHelp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CmdHelp.Text = "&Hjelp"
		Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
		Me.CmdHelp.Location = New System.Drawing.Point(144, 303)
		Me.CmdHelp.TabIndex = 10
		Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
		Me.CmdHelp.CausesValidation = True
		Me.CmdHelp.Enabled = True
		Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
		Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CmdHelp.TabStop = True
		Me.CmdHelp.Name = "CmdHelp"
		Me.CmdFinish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CmdFinish.Text = "F&ullf�r"
		Me.CmdFinish.Enabled = False
		Me.CmdFinish.Size = New System.Drawing.Size(73, 21)
		Me.CmdFinish.Location = New System.Drawing.Point(531, 304)
		Me.CmdFinish.TabIndex = 14
		Me.CmdFinish.Visible = False
		Me.CmdFinish.BackColor = System.Drawing.SystemColors.Control
		Me.CmdFinish.CausesValidation = True
		Me.CmdFinish.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CmdFinish.Cursor = System.Windows.Forms.Cursors.Default
		Me.CmdFinish.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CmdFinish.TabStop = True
		Me.CmdFinish.Name = "CmdFinish"
		Me.CmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CmdCancel.Text = "&Avbryt"
		Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
		Me.CmdCancel.Location = New System.Drawing.Point(289, 303)
		Me.CmdCancel.TabIndex = 11
		Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.CmdCancel.CausesValidation = True
		Me.CmdCancel.Enabled = True
		Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CmdCancel.TabStop = True
		Me.CmdCancel.Name = "CmdCancel"
		Me.CmdNext.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CmdNext.Text = "&Neste >"
		Me.AcceptButton = Me.CmdNext
		Me.CmdNext.Size = New System.Drawing.Size(73, 21)
		Me.CmdNext.Location = New System.Drawing.Point(439, 303)
		Me.CmdNext.TabIndex = 13
		Me.CmdNext.BackColor = System.Drawing.SystemColors.Control
		Me.CmdNext.CausesValidation = True
		Me.CmdNext.Enabled = True
		Me.CmdNext.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CmdNext.Cursor = System.Windows.Forms.Cursors.Default
		Me.CmdNext.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CmdNext.TabStop = True
		Me.CmdNext.Name = "CmdNext"
		Me.CmdBack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CmdBack.Text = "< &Forrige"
		Me.CmdBack.Size = New System.Drawing.Size(73, 21)
		Me.CmdBack.Location = New System.Drawing.Point(365, 303)
		Me.CmdBack.TabIndex = 12
		Me.CmdBack.BackColor = System.Drawing.SystemColors.Control
		Me.CmdBack.CausesValidation = True
		Me.CmdBack.Enabled = True
		Me.CmdBack.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CmdBack.Cursor = System.Windows.Forms.Cursors.Default
		Me.CmdBack.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CmdBack.TabStop = True
		Me.CmdBack.Name = "CmdBack"
		Me.fraToAccount.Text = "60326 - Returfiler til regnskap/reskontro"
		Me.fraToAccount.Size = New System.Drawing.Size(445, 177)
		Me.fraToAccount.Location = New System.Drawing.Point(172, 113)
		Me.fraToAccount.TabIndex = 15
		Me.fraToAccount.BackColor = System.Drawing.SystemColors.Control
		Me.fraToAccount.Enabled = True
		Me.fraToAccount.ForeColor = System.Drawing.SystemColors.ControlText
		Me.fraToAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.fraToAccount.Visible = True
		Me.fraToAccount.Padding = New System.Windows.Forms.Padding(0)
		Me.fraToAccount.Name = "fraToAccount"
		Me.chkRest.Text = "60330-�vrige"
		Me.chkRest.Size = New System.Drawing.Size(107, 15)
		Me.chkRest.Location = New System.Drawing.Point(335, 77)
		Me.chkRest.TabIndex = 5
		Me.chkRest.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkRest.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkRest.BackColor = System.Drawing.SystemColors.Control
		Me.chkRest.CausesValidation = True
		Me.chkRest.Enabled = True
		Me.chkRest.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkRest.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkRest.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkRest.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkRest.TabStop = True
		Me.chkRest.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkRest.Visible = True
		Me.chkRest.Name = "chkRest"
		Me.chkAutogiro.Text = "60329-Autogiro"
		Me.chkAutogiro.Size = New System.Drawing.Size(107, 15)
		Me.chkAutogiro.Location = New System.Drawing.Point(335, 57)
		Me.chkAutogiro.TabIndex = 4
		Me.chkAutogiro.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkAutogiro.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkAutogiro.BackColor = System.Drawing.SystemColors.Control
		Me.chkAutogiro.CausesValidation = True
		Me.chkAutogiro.Enabled = True
		Me.chkAutogiro.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkAutogiro.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkAutogiro.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkAutogiro.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkAutogiro.TabStop = True
		Me.chkAutogiro.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkAutogiro.Visible = True
		Me.chkAutogiro.Name = "chkAutogiro"
		Me.chkOCR.Text = "60328-OCR"
		Me.chkOCR.Size = New System.Drawing.Size(107, 15)
		Me.chkOCR.Location = New System.Drawing.Point(335, 36)
		Me.chkOCR.TabIndex = 18
		Me.chkOCR.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkOCR.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkOCR.BackColor = System.Drawing.SystemColors.Control
		Me.chkOCR.CausesValidation = True
		Me.chkOCR.Enabled = True
		Me.chkOCR.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkOCR.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkOCR.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkOCR.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkOCR.TabStop = True
		Me.chkOCR.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkOCR.Visible = True
		Me.chkOCR.Name = "chkOCR"
		Me.chkGL.Text = "60327-Hovedbok"
		Me.chkGL.Size = New System.Drawing.Size(107, 15)
		Me.chkGL.Location = New System.Drawing.Point(335, 17)
		Me.chkGL.TabIndex = 3
		Me.chkGL.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkGL.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkGL.BackColor = System.Drawing.SystemColors.Control
		Me.chkGL.CausesValidation = True
		Me.chkGL.Enabled = True
		Me.chkGL.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkGL.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkGL.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkGL.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkGL.TabStop = True
		Me.chkGL.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkGL.Visible = True
		Me.chkGL.Name = "chkGL"
		Me.lstFormats.Size = New System.Drawing.Size(181, 21)
		Me.lstFormats.Location = New System.Drawing.Point(246, 106)
		Me.lstFormats.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.lstFormats.TabIndex = 2
		Me.lstFormats.Visible = False
		Me.lstFormats.BackColor = System.Drawing.SystemColors.Window
		Me.lstFormats.CausesValidation = True
		Me.lstFormats.Enabled = True
		Me.lstFormats.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lstFormats.IntegralHeight = True
		Me.lstFormats.Cursor = System.Windows.Forms.Cursors.Default
		Me.lstFormats.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lstFormats.Sorted = False
		Me.lstFormats.TabStop = True
		Me.lstFormats.Name = "lstFormats"
		Me.lstFiles.Size = New System.Drawing.Size(314, 85)
		Me.lstFiles.Location = New System.Drawing.Point(10, 15)
		Me.lstFiles.TabIndex = 0
		Me.lstFiles.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lstFiles.BackColor = System.Drawing.SystemColors.Window
		Me.lstFiles.CausesValidation = True
		Me.lstFiles.Enabled = True
		Me.lstFiles.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lstFiles.IntegralHeight = True
		Me.lstFiles.Cursor = System.Windows.Forms.Cursors.Default
		Me.lstFiles.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.lstFiles.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lstFiles.Sorted = False
		Me.lstFiles.TabStop = True
		Me.lstFiles.Visible = True
		Me.lstFiles.MultiColumn = False
		Me.lstFiles.Name = "lstFiles"
		Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
		Me.cmdFileOpen.Location = New System.Drawing.Point(214, 104)
		Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
		Me.cmdFileOpen.TabIndex = 19
		Me.cmdFileOpen.Visible = False
		Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
		Me.cmdFileOpen.CausesValidation = True
		Me.cmdFileOpen.Enabled = True
		Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdFileOpen.TabStop = True
		Me.cmdFileOpen.Name = "cmdFileOpen"
		Me.chkRemove.Text = "60144 - Fjern gamle filer f�r eksport"
		Me.chkRemove.Size = New System.Drawing.Size(211, 17)
		Me.chkRemove.Location = New System.Drawing.Point(215, 152)
		Me.chkRemove.TabIndex = 9
		Me.chkRemove.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkRemove.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkRemove.BackColor = System.Drawing.SystemColors.Control
		Me.chkRemove.CausesValidation = True
		Me.chkRemove.Enabled = True
		Me.chkRemove.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkRemove.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkRemove.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkRemove.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkRemove.TabStop = True
		Me.chkRemove.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkRemove.Visible = True
		Me.chkRemove.Name = "chkRemove"
		Me.chkWarning.Text = "60068 - Varsle hvis fil finnes"
		Me.chkWarning.Size = New System.Drawing.Size(196, 17)
		Me.chkWarning.Location = New System.Drawing.Point(11, 152)
		Me.chkWarning.TabIndex = 7
		Me.chkWarning.CheckState = System.Windows.Forms.CheckState.Checked
		Me.chkWarning.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkWarning.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkWarning.BackColor = System.Drawing.SystemColors.Control
		Me.chkWarning.CausesValidation = True
		Me.chkWarning.Enabled = True
		Me.chkWarning.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkWarning.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkWarning.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkWarning.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkWarning.TabStop = True
		Me.chkWarning.Visible = True
		Me.chkWarning.Name = "chkWarning"
		Me.chkBackup.Text = "60066 - Legg i backupkatalog"
		Me.chkBackup.Size = New System.Drawing.Size(196, 13)
		Me.chkBackup.Location = New System.Drawing.Point(11, 130)
		Me.chkBackup.TabIndex = 6
		Me.chkBackup.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkBackup.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkBackup.BackColor = System.Drawing.SystemColors.Control
		Me.chkBackup.CausesValidation = True
		Me.chkBackup.Enabled = True
		Me.chkBackup.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkBackup.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkBackup.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkBackup.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkBackup.TabStop = True
		Me.chkBackup.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkBackup.Visible = True
		Me.chkBackup.Name = "chkBackup"
		Me.txtFilename.AutoSize = False
		Me.txtFilename.Size = New System.Drawing.Size(200, 19)
		Me.txtFilename.Location = New System.Drawing.Point(11, 106)
		Me.txtFilename.Maxlength = 255
		Me.txtFilename.TabIndex = 1
		Me.txtFilename.Text = "<60057 - Filename>"
		Me.txtFilename.Visible = False
		Me.txtFilename.AcceptsReturn = True
		Me.txtFilename.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFilename.BackColor = System.Drawing.SystemColors.Window
		Me.txtFilename.CausesValidation = True
		Me.txtFilename.Enabled = True
		Me.txtFilename.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFilename.HideSelection = True
		Me.txtFilename.ReadOnly = False
		Me.txtFilename.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFilename.MultiLine = False
		Me.txtFilename.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFilename.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFilename.TabStop = True
		Me.txtFilename.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtFilename.Name = "txtFilename"
		Me.chkOverwrite.Text = "60067 - Skriv over hvis fil finnes"
		Me.chkOverwrite.Size = New System.Drawing.Size(210, 16)
		Me.chkOverwrite.Location = New System.Drawing.Point(214, 128)
		Me.chkOverwrite.TabIndex = 8
		Me.chkOverwrite.CheckState = System.Windows.Forms.CheckState.Checked
		Me.chkOverwrite.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkOverwrite.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkOverwrite.BackColor = System.Drawing.SystemColors.Control
		Me.chkOverwrite.CausesValidation = True
		Me.chkOverwrite.Enabled = True
		Me.chkOverwrite.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkOverwrite.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkOverwrite.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkOverwrite.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkOverwrite.TabStop = True
		Me.chkOverwrite.Visible = True
		Me.chkOverwrite.Name = "chkOverwrite"
		Me._Image1_0.Size = New System.Drawing.Size(32, 56)
		Me._Image1_0.Location = New System.Drawing.Point(0, 0)
		Me._Image1_0.Enabled = True
		Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._Image1_0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me._Image1_0.Visible = True
		Me._Image1_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Image1_0.Name = "_Image1_0"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(254, 153, 21)
		Me.Line1.X1 = 8
		Me.Line1.X2 = 600
		Me.Line1.Y1 = 289
		Me.Line1.Y2 = 289
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.lblHeading.Text = "60325 - Navn p� filer for oppdatering av regnskap/reskontro"
		Me.lblHeading.Size = New System.Drawing.Size(417, 17)
		Me.lblHeading.Location = New System.Drawing.Point(183, 17)
		Me.lblHeading.TabIndex = 17
		Me.lblHeading.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
		Me.lblHeading.Enabled = True
		Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblHeading.UseMnemonic = True
		Me.lblHeading.Visible = True
		Me.lblHeading.AutoSize = False
		Me.lblHeading.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblHeading.Name = "lblHeading"
		Me.lblDescr.Text = "60335 - Angi de filnavn som skal legges ut som grunnlag for oppdatering av regnskap/reskontro. Dette gjelder filer med avstemte og uavstemte poster, samt OCR-poster og eventuelt Autogiroposter. I tillegg kan det legges ut egen fil for oppdatering av hovedbok.  Spesialtegn: # erstattes av Database (se eget oppsett)  & erstattes av Banknavn."
		Me.lblDescr.Size = New System.Drawing.Size(417, 71)
		Me.lblDescr.Location = New System.Drawing.Point(183, 41)
		Me.lblDescr.TabIndex = 16
		Me.lblDescr.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblDescr.BackColor = System.Drawing.SystemColors.Control
		Me.lblDescr.Enabled = True
		Me.lblDescr.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblDescr.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblDescr.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblDescr.UseMnemonic = True
		Me.lblDescr.Visible = True
		Me.lblDescr.AutoSize = False
		Me.lblDescr.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblDescr.Name = "lblDescr"
		Me.Controls.Add(cmdRemoveFile)
		Me.Controls.Add(cmdAddFile)
		Me.Controls.Add(CmdHelp)
		Me.Controls.Add(CmdFinish)
		Me.Controls.Add(CmdCancel)
		Me.Controls.Add(CmdNext)
		Me.Controls.Add(CmdBack)
		Me.Controls.Add(fraToAccount)
		Me.Controls.Add(_Image1_0)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.Controls.Add(lblHeading)
		Me.Controls.Add(lblDescr)
		Me.Controls.Add(ShapeContainer1)
		Me.fraToAccount.Controls.Add(chkRest)
		Me.fraToAccount.Controls.Add(chkAutogiro)
		Me.fraToAccount.Controls.Add(chkOCR)
		Me.fraToAccount.Controls.Add(chkGL)
		Me.fraToAccount.Controls.Add(lstFormats)
		Me.fraToAccount.Controls.Add(lstFiles)
		Me.fraToAccount.Controls.Add(cmdFileOpen)
		Me.fraToAccount.Controls.Add(chkRemove)
		Me.fraToAccount.Controls.Add(chkWarning)
		Me.fraToAccount.Controls.Add(chkBackup)
		Me.fraToAccount.Controls.Add(txtFilename)
		Me.fraToAccount.Controls.Add(chkOverwrite)
		Me.Image1.SetIndex(_Image1_0, CType(0, Short))
		CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.fraToAccount.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
