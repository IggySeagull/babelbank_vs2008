'' TODO - modMXQuery M� skrives om, eller vi m� finne en erstatning !!!!
'Option Strict Off
'Option Explicit On
'Module modMXQuery
'	'**********************************************************
'	'   The DNS & MXQuery code in this module was adapted from
'	'   Gregg Housh's MX.OCX code. Many thanks to Gregg for
'	'   his fine work.
'	'**********************************************************

'	' winsock
'	Private Const DNS_RECURSION As Byte = 1
'	Private Const AF_INET As Short = 2
'	Private Const SOCKET_ERROR As Short = -1
'	Private Const ERROR_BUFFER_OVERFLOW As Short = 111
'	Private Const SOCK_DGRAM As Short = 2
'	Private Const INADDR_NONE As Integer = &HFFFFFFFF
'	Private Const INADDR_ANY As Integer = &H0
'	' registry access
'	Private Const REG_SZ As Short = 1
'	Private Const ERROR_SUCCESS As Short = 0
'	Private Const HKEY_CLASSES_ROOT As Integer = &H80000000
'	Private Const HKEY_CURRENT_USER As Integer = &H80000001
'	Private Const HKEY_LOCAL_MACHINE As Integer = &H80000002
'	Private Const KEY_QUERY_VALUE As Integer = &H1
'	Private Const KEY_ENUMERATE_SUB_KEYS As Integer = &H8
'	Private Const KEY_NOTIFY As Integer = &H10
'	Private Const READ_CONTROL As Integer = &H20000
'	Private Const SYNCHRONIZE As Integer = &H100000
'	Private Const STANDARD_RIGHTS_READ As Integer = READ_CONTROL
'	Private Const KEY_READ As Boolean = STANDARD_RIGHTS_READ Or KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY

'	' winsock
'	Private Structure WSADATA
'		Dim wVersion As Short
'		Dim wHighVersion As Short
'		<VBFixedArray(256)> Dim szDescription() As Byte
'		<VBFixedArray(128)> Dim szSystemStatus() As Byte
'		Dim iMaxSockets As Short
'		Dim iMaxUdpDg As Short
'		Dim lpVendorInfo As Integer

'		'UPGRADE_TODO: "Initialize" must be called to initialize instances of this structure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
'		Public Sub Initialize()
'			ReDim szDescription(256)
'			ReDim szSystemStatus(128)
'		End Sub
'	End Structure

'	Private Structure DNS_HEADER
'		Dim qryID As Short
'		Dim options As Byte
'		Dim response As Byte
'		Dim qdcount As Short
'		Dim ancount As Short
'		Dim nscount As Short
'		Dim arcount As Short
'	End Structure

'	Private Structure IP_ADDRESS_STRING
'		<VBFixedArray(4 * 4 - 1)> Dim IpAddressStr() As Byte

'		'UPGRADE_TODO: "Initialize" must be called to initialize instances of this structure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
'		Public Sub Initialize()
'			ReDim IpAddressStr(4 * 4 - 1)
'		End Sub
'	End Structure

'	Private Structure IP_MASK_STRING
'		<VBFixedArray(4 * 4 - 1)> Dim IpMaskString() As Byte

'		'UPGRADE_TODO: "Initialize" must be called to initialize instances of this structure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
'		Public Sub Initialize()
'			ReDim IpMaskString(4 * 4 - 1)
'		End Sub
'	End Structure

'	Private Structure IP_ADDR_STRING
'		'UPGRADE_NOTE: Next was upgraded to Next_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
'		Dim Next_Renamed As Integer
'		'UPGRADE_WARNING: Arrays in structure IpAddress may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
'		Dim IpAddress As IP_ADDRESS_STRING
'		'UPGRADE_WARNING: Arrays in structure IpMask may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
'		Dim IpMask As IP_MASK_STRING
'		Dim Context As Integer

'		'UPGRADE_TODO: "Initialize" must be called to initialize instances of this structure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
'		Public Sub Initialize()
'			IpAddress.Initialize()
'			IpMask.Initialize()
'		End Sub
'	End Structure

'	Private Structure FIXED_INFO
'		<VBFixedArray(128 + 4 - 1)> Dim HostName() As Byte
'		<VBFixedArray(128 + 4 - 1)> Dim DomainName() As Byte
'		Dim CurrentDnsServer As Integer
'		Dim DnsServerList As IP_ADDR_STRING
'		Dim NodeType As Integer
'		<VBFixedArray(256 + 4 - 1)> Dim ScopeId() As Byte
'		Dim EnableRouting As Integer
'		Dim EnableProxy As Integer
'		Dim EnableDns As Integer

'		'UPGRADE_TODO: "Initialize" must be called to initialize instances of this structure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B4BFF9E0-8631-45CF-910E-62AB3970F27B"'
'		Public Sub Initialize()
'			ReDim HostName(128 + 4 - 1)
'			ReDim DomainName(128 + 4 - 1)
'			DnsServerList.Initialize()
'			ReDim ScopeId(256 + 4 - 1)
'		End Sub
'	End Structure

'	Private Structure SOCKADDR
'		Dim sin_family As Short
'		Dim sin_port As Short
'		Dim sin_addr As Integer
'		'UPGRADE_WARNING: Fixed-length string size must fit in the buffer. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
'		<VBFixedString(8),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray,SizeConst:=8)> Public sin_zero() As Char
'	End Structure

'	Private Structure HostEnt
'		Dim h_name As Integer
'		Dim h_aliases As Integer
'		Dim h_addrtype As Short
'		Dim h_length As Short
'		Dim h_addr_list As Integer
'	End Structure

'	' registry
'	Private Structure FILETIME
'		Dim dwLowDateTime As Integer
'		Dim dwHighDateTime As Integer
'	End Structure

'	' public type for passing DNS info
'	Public Structure DNS_INFO
'		Dim Servers() As String
'		Dim Count As Integer
'		Dim LocalDomain As String
'		Dim RootDomain As String
'	End Structure

'	' used below
'	Public Structure MX_RECORD
'		Dim Server As String
'		Dim Pref As Short
'	End Structure

'	' public type for passing MX info
'	Public Structure MX_INFO
'		Dim Best As String
'		Dim Domain As String
'		Dim List() As MX_RECORD
'		Dim Count As Integer
'	End Structure

'	'UPGRADE_WARNING: Arrays in structure DNS may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
'	Public DNS As DNS_INFO
'	'UPGRADE_WARNING: Arrays in structure MX may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
'	Public MX As MX_INFO


'	' API prototypes

'	' winsock, 'wsock32.dll' used instead of 'ws2_32.dll' for wider compatibility
'	Private Declare Function gethostbyname Lib "wsock32.dll" (ByVal host_name As String) As Integer
'	Private Declare Function gethostbyaddr Lib "ws2_32.dll" (ByRef addr As Integer, ByVal addr_len As Integer, ByVal addr_type As Integer) As Integer
'	Private Declare Function inet_addr Lib "wsock32.dll" (ByVal cp As String) As Integer
'	'UPGRADE_WARNING: Structure SOCKADDR may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
'	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
'    'Private Declare Function recvfrom Lib "wsock32.dll" (ByVal s As Integer, ByRef buf As Any, ByVal buflen As Integer, ByVal flags As Integer, ByRef from As SOCKADDR, ByRef fromlen As Integer) As Integer
'    Private Declare Function recvfrom Lib "wsock32.dll" (ByVal s As Integer, ByRef buf As String, ByVal buflen As Integer, ByVal flags As Integer, ByRef from As SOCKADDR, ByRef fromlen As Integer) As Integer
'	Private Declare Function socket Lib "wsock32.dll" (ByVal af As Integer, ByVal s_type As Integer, ByVal protocol As Integer) As Integer
'	Private Declare Function htons Lib "wsock32.dll" (ByVal hostshort As Integer) As Short
'	Private Declare Function ntohs Lib "wsock32.dll" (ByVal netshort As Integer) As Short
'	'UPGRADE_WARNING: Structure SOCKADDR may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
'	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
'    'Private Declare Function sendto Lib "wsock32.dll" (ByVal s As Integer, ByRef buf As Any, ByVal buflen As Integer, ByVal flags As Integer, ByRef to_addr As SOCKADDR, ByVal tolen As Integer) As Integer
'    Private Declare Function sendto Lib "wsock32.dll" (ByVal s As Integer, ByRef buf As String, ByVal buflen As Integer, ByVal flags As Integer, ByRef to_addr As SOCKADDR, ByVal tolen As Integer) As Integer
'	Private Declare Function WSAGetLastError Lib "wsock32.dll" () As Integer
'	'UPGRADE_WARNING: Structure WSADATA may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
'	Private Declare Function WSAStartup Lib "wsock32.dll" (ByVal wVersionRequired As Integer, ByRef lpWSAData As WSADATA) As Integer
'	Private Declare Function WSACleanup Lib "wsock32.dll" () As Integer

'	' Registry access
'	Private Declare Function RegOpenKeyEx Lib "advapi32.dll"  Alias "RegOpenKeyExA"(ByVal hKey As Integer, ByVal lpSubKey As String, ByVal ulOptions As Integer, ByVal samDesired As Integer, ByRef phkResult As Integer) As Integer
'	Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Integer) As Integer
'	Private Declare Function RegQueryValueEx Lib "advapi32.dll"  Alias "RegQueryValueExA"(ByVal hKey As Integer, ByVal lpValueName As String, ByVal lpReserved As Integer, ByRef lpType As Integer, ByVal lpData As String, ByRef lpcbData As Integer) As Integer
'	'UPGRADE_WARNING: Structure FILETIME may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
'	Private Declare Function RegEnumKeyEx Lib "advapi32.dll"  Alias "RegEnumKeyExA"(ByVal hKey As Integer, ByVal dwIndex As Integer, ByVal lpName As String, ByRef lpcbName As Integer, ByVal lpReserved As Integer, ByVal lpClass As String, ByRef lpcbClass As Integer, ByRef lpftLastWriteTime As FILETIME) As Integer

'	' misc
'	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
'    'Private Declare Function GetNetworkParams Lib "iphlpapi.dll" (ByRef pFixedInfo As Any, ByRef pOutBufLen As Integer) As Integer
'    Private Declare Function GetNetworkParams Lib "iphlpapi.dll" (ByRef pFixedInfo As String, ByRef pOutBufLen As Integer) As Integer
'	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
'	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
'    'Private Declare Sub CopyMemory Lib "kernel32"  Alias "RtlMoveMemory"(ByRef Destination As Any, ByRef Source As Any, ByVal Length As Integer)
'    Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (ByRef Destination As FIXED_INFO, ByRef Source As String, ByVal Length As Integer)
'    Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (ByRef Destination As IP_ADDR_STRING, ByRef Source As String, ByVal Length As Integer)
'    Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (ByRef Destination As Byte, ByRef Source As String, ByVal Length As Integer)
'	Private Declare Function GetModuleHandle Lib "kernel32"  Alias "GetModuleHandleA"(ByVal lpModuleName As String) As Integer
'	Private Declare Function LoadLibrary Lib "kernel32"  Alias "LoadLibraryA"(ByVal lpLibFileName As String) As Integer
'	Private Declare Function GetProcAddress Lib "kernel32" (ByVal hModule As Integer, ByVal lpProcName As String) As Integer
'	Private Declare Function FreeLibrary Lib "kernel32" (ByVal hLibModule As Integer) As Integer

'	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
'    'Private Declare Function lstrlen Lib "kernel32"  Alias "lstrlenA"(ByRef lpString As Any) As Integer 'RRC V3.6.6 06022006
'    Private Declare Function lstrlen Lib "kernel32" Alias "lstrlenA" (ByRef lpString As String) As Integer 'RRC V3.6.6 06022006


'	Public Sub GetDNSInfo()

'		' get the DNS servers and the local IP Domain name

'		Dim sBuffer As String
'		Dim sDNSBuff As String
'		Dim sDomainBuff As String
'		Dim sKey As String
'		Dim lngFixedInfoNeeded As Integer
'		Dim bytFixedInfoBuffer() As Byte
'		'UPGRADE_WARNING: Arrays in structure udtFixedInfo may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
'		Dim udtFixedInfo As FIXED_INFO
'		Dim lngIpAddrStringPtr As Integer
'		Dim udtIpAddrString As IP_ADDR_STRING
'		Dim strDnsIpAddress As String
'		Dim nRet As Integer
'		Dim sTmp() As String
'		Dim i As Integer

'		' get dns servers with the new GetNetworkParams call (only works on 98/ME/2000)
'		' if GetNetworkParams is not supported then try reading from the registry
'		If Exported("iphlpapi.dll", "GetNetworkParams") Then
'            nRet = GetNetworkParams("", lngFixedInfoNeeded)
'            If nRet = ERROR_BUFFER_OVERFLOW Then
'                ReDim bytFixedInfoBuffer(lngFixedInfoNeeded)
'                nRet = GetNetworkParams(bytFixedInfoBuffer(0), lngFixedInfoNeeded)
'                'UPGRADE_WARNING: Couldn't resolve default property of object udtFixedInfo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
'                CopyMemory(udtFixedInfo, bytFixedInfoBuffer(0), Len(udtFixedInfo))
'                With udtFixedInfo
'                    ' get the DNS servers
'                    'UPGRADE_ISSUE: VarPtr function is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="367764E5-F3F8-4E43-AC3E-7FE0B5E074E2"'
'                    lngIpAddrStringPtr = VarPtr(.DnsServerList)
'                    Do While lngIpAddrStringPtr
'                        'UPGRADE_WARNING: Couldn't resolve default property of object udtIpAddrString. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
'                        CopyMemory(udtIpAddrString, lngIpAddrStringPtr, Len(udtIpAddrString))
'                        With udtIpAddrString
'                            'UPGRADE_ISSUE: Constant vbUnicode was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
'                            strDnsIpAddress = StripTerminator(StrConv(System.Text.UnicodeEncoding.Unicode.GetString(.IpAddress.IpAddressStr), VbStrConv.None))  ', vbUnicode))
'                            sDNSBuff = sDNSBuff & strDnsIpAddress & ","
'                            lngIpAddrStringPtr = .Next_Renamed
'                        End With
'                    Loop
'                    ' get the ip domain name
'                    'UPGRADE_ISSUE: Constant vbUnicode was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
'                    sDomainBuff = StripTerminator(StrConv(System.Text.UnicodeEncoding.Unicode.GetString(.DomainName), VbStrConv.None))  ' vbUnicode))
'                End With
'            End If
'        End If

'        ' if GetNetworkParams didn't get the data we need,
'        ' try known locations in the registry for DNS & domain info
'        If Len(sDNSBuff) = 0 Or Len(sDomainBuff) = 0 Then

'            ' DNS servers configured through Network control panel applet (95/98/ME)
'            sKey = "System\CurrentControlSet\Services\VxD\MSTCP"
'            sBuffer = GetRegStr(HKEY_LOCAL_MACHINE, sKey, "NameServer", "")
'            If Len(sBuffer) Then sDNSBuff = sBuffer & ","
'            sBuffer = GetRegStr(HKEY_LOCAL_MACHINE, sKey, "Domain", "")
'            If Len(sBuffer) Then sDomainBuff = sBuffer

'            ' DNS servers configured through Network control panel applet (NT/2000)
'            sKey = "SYSTEM\CurrentControlSet\Services\Tcpip\Parameters"
'            sBuffer = GetRegStr(HKEY_LOCAL_MACHINE, sKey, "NameServer", "")
'            If Len(sBuffer) Then sDNSBuff = sBuffer & ","
'            sBuffer = GetRegStr(HKEY_LOCAL_MACHINE, sKey, "Domain", "")
'            If Len(sBuffer) Then sDomainBuff = sBuffer

'            ' DNS servers configured DHCP (NT/2000/XP)
'            sKey = "SYSTEM\CurrentControlSet\Services\Tcpip\Parameters"
'            sBuffer = GetRegStr(HKEY_LOCAL_MACHINE, sKey, "DhcpNameServer", "")
'            If Len(sBuffer) Then sDNSBuff = sBuffer & ","
'            sBuffer = GetRegStr(HKEY_LOCAL_MACHINE, sKey, "DHCPDomain", "")
'            If Len(sBuffer) Then sDomainBuff = sBuffer

'            ' DNS servers configured through Network control panel applet (XP)
'            sKey = "SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces"
'            sTmp = EnumRegKey(HKEY_LOCAL_MACHINE, sKey)
'            For i = 0 To UBound(sTmp)
'                sBuffer = GetRegStr(HKEY_LOCAL_MACHINE, sKey & "\" & sTmp(i), "NameServer", "")
'                If Len(sBuffer) Then sDNSBuff = sBuffer & ","
'                sBuffer = GetRegStr(HKEY_LOCAL_MACHINE, sKey & "\" & sTmp(i), "Domain", "")
'                If Len(sBuffer) Then sDomainBuff = sBuffer
'            Next

'            ' DNS servers configured DHCP (95/98/ME)
'            ' *** haven't found one ***

'        End If

'        ' get rid of any space delimiters (2000)
'        sDNSBuff = Replace(sDNSBuff, " ", ",")

'        ' trim any trailing commas
'        If Right(sDNSBuff, 1) = "," Then sDNSBuff = Left(sDNSBuff, Len(sDNSBuff) - 1)

'        ' load our type struc
'        DNS.Servers = Split(sDNSBuff, ",")
'        DNS.Count = UBound(DNS.Servers) + 1
'        DNS.LocalDomain = sDomainBuff

'        ' cheap trick
'        If sDomainBuff = "" And DNS.Count > 0 Then
'            sDomainBuff = GetRemoteHostName(DNS.Servers(0))
'            nRet = InStr(sDomainBuff, ".")
'            If nRet Then
'                DNS.LocalDomain = Mid(sDomainBuff, nRet + 1)
'            End If
'        End If

'        sTmp = Split(sDomainBuff, ".")
'        nRet = UBound(sTmp)
'        If nRet > 0 Then
'            DNS.RootDomain = sTmp(nRet - 1) & "." & sTmp(nRet)
'        Else
'            DNS.RootDomain = sDomainBuff
'        End If

'    End Sub

'    Public Function MX_Query(ByVal ms_Domain As String) As String

'        ' Performs the actual IP work to contact the DNS server,
'        ' calls the other functions to parse and return the
'        ' best server to send email through

'        'UPGRADE_WARNING: Arrays in structure StartupData may need to be initialized before they can be used. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="814DF224-76BD-4BB4-BFFB-EA359CB9FC48"'
'        Dim StartupData As WSADATA
'        Dim SocketBuffer As SOCKADDR
'        Dim IpAddr As Integer
'        Dim iRC As Short
'        Dim dnsHead As DNS_HEADER
'        Dim iSock As Short
'        Dim dnsQuery() As Byte
'        Dim sQName As String
'        Dim dnsQueryNdx As Short
'        Dim iTemp As Short
'        Dim iNdx As Short
'        Dim dnsReply(2048) As Byte
'        Dim iAnCount As Short
'        'Dim dwFlags As Integer


'        MX.Count = 0
'        MX.Best = ""
'        ReDim MX.List(0)

'        ' if DNSInfo hasn't been called, call it now
'        If DNS.Count = 0 Then GetDNSInfo()

'        ' check to see that we found a dns server
'        If DNS.Count = 0 Then
'            ' problem
'            Err.Raise(20000, "MXQuery", "No DNS entries found, MX Query cannot contine.")
'            Exit Function
'        End If

'        ' if null was passed in then use the local domain name
'        If Len(ms_Domain) = 0 Then ms_Domain = DNS.LocalDomain

'        ' validate domain name
'        If Len(ms_Domain) < 5 Then
'            Err.Raise(20000, "MXQuery", "No Valid Domain Specified")
'            Exit Function
'        End If

'        MX.Domain = ms_Domain

'        ' Initialize the Winsock, request v1.1
'        If WSAStartup(&H101, StartupData) <> ERROR_SUCCESS Then
'            iRC = WSACleanup
'            Exit Function
'        End If

'        ' Create a socket
'        iSock = socket(AF_INET, SOCK_DGRAM, 0)
'        If iSock = SOCKET_ERROR Then Exit Function

'        ' convert the IP address string to a network ordered long
'        IpAddr = GetHostByNameAlias(DNS.Servers(0))
'        If IpAddr = -1 Then Exit Function

'        ' Setup the connnection parameters
'        SocketBuffer.sin_family = AF_INET
'        SocketBuffer.sin_port = htons(53)
'        SocketBuffer.sin_addr = IpAddr
'        SocketBuffer.sin_zero = New String(Chr(0), 8)

'        ' Set the DNS parameters
'        dnsHead.qryID = htons(&H11DF)
'        dnsHead.options = DNS_RECURSION
'        dnsHead.qdcount = htons(1)
'        dnsHead.ancount = 0
'        dnsHead.nscount = 0
'        dnsHead.arcount = 0

'        dnsQueryNdx = 0

'        ReDim dnsQuery(4000)

'        ' Setup the dns structure to send the query in
'        ' First goes the DNS header information
'        'UPGRADE_WARNING: Couldn't resolve default property of object dnsHead. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
'        'CopyMemory(dnsQuery(dnsQueryNdx), dnsHead, 12)
'        dnsQueryNdx = dnsQueryNdx + 12

'        ' Then the domain name (as a QNAME)
'        sQName = MakeQName(MX.Domain)
'        iNdx = 0
'        While (iNdx < Len(sQName))
'            dnsQuery(dnsQueryNdx + iNdx) = Asc(Mid(sQName, iNdx + 1, 1))
'            iNdx = iNdx + 1
'        End While

'        dnsQueryNdx = dnsQueryNdx + Len(sQName)

'        ' Null terminate the string
'        dnsQuery(dnsQueryNdx) = &H0
'        dnsQueryNdx = dnsQueryNdx + 1

'        ' The type of query (15 means MX query)
'        iTemp = htons(15)
'        CopyMemory(dnsQuery(dnsQueryNdx), iTemp, Len(iTemp))
'        dnsQueryNdx = dnsQueryNdx + Len(iTemp)

'        ' The class of query (1 means INET)
'        iTemp = htons(1)
'        CopyMemory(dnsQuery(dnsQueryNdx), iTemp, Len(iTemp))
'        dnsQueryNdx = dnsQueryNdx + Len(iTemp)

'        ReDim Preserve dnsQuery(dnsQueryNdx - 1)
'        ' Send the query to the DNS server
'        iRC = sendto(iSock, dnsQuery(0), dnsQueryNdx + 1, 0, SocketBuffer, Len(SocketBuffer))
'        If (iRC = SOCKET_ERROR) Or (iRC = 0) Then
'            Err.Raise(20000, "MXQuery", "Problem sending MX query")
'            iRC = WSACleanup
'            Exit Function
'        End If

'        ' Wait for answer from the DNS server
'        iRC = recvfrom(iSock, dnsReply(0), 2048, 0, SocketBuffer, Len(SocketBuffer))
'        If (iRC = SOCKET_ERROR) Or (iRC = 0) Then
'            Err.Raise(20000, "MXQuery", "Problem receiving MX query")
'            iRC = WSACleanup
'            Exit Function
'        End If

'        ' Get the number of answers
'        CopyMemory(iAnCount, dnsReply(6), 2)
'        iAnCount = ntohs(iAnCount)

'        iRC = WSACleanup

'        If iAnCount Then
'            ' Parse the answer buffer
'            MX_Query = GetMXName(dnsReply, 12, iAnCount)

'        Else
'            ' if we didn't find anything and we are part of
'            ' a sub domain, go up one level and try again
'            ' the last pass is at the root domain level
'            If InStr(MX.Domain, DNS.RootDomain) > 1 Then
'                MX.Domain = Mid(MX.Domain, InStr(MX.Domain, ".") + 1)
'                MX_Query = MX_Query(MX.Domain)
'            End If
'        End If

'    End Function

'    Private Sub ParseName(ByRef dnsReply() As Byte, ByRef iNdx As Short, ByRef sName As String)

'        ' Parse the server name out of the MX record, returns it in variable sName.
'        ' iNdx is also modified to point to the end of the parsed structure.

'        Dim iCompress As Short ' Compression index (index to original buffer)
'        Dim iChCount As Short ' Character count (number of chars to read from buffer)

'        ' While we dont encounter a null char (end-of-string specifier)
'        While (dnsReply(iNdx) <> 0)
'            ' Read the next character in the stream (length specifier)
'            iChCount = dnsReply(iNdx)
'            ' If our length specifier is 192 (0xc0) we have a compressed string
'            If (iChCount = 192) Then
'                ' Read the location of the rest of the string (offset into buffer)
'                iCompress = dnsReply(iNdx + 1)
'                ' Call ourself again, this time with the offset of the compressed string
'                ParseName(dnsReply, iCompress, sName)
'                ' Step over the compression indicator and compression index
'                iNdx = iNdx + 2
'                ' After a compressed string, we are done
'                Exit Sub
'            End If

'            ' Move to next char
'            iNdx = iNdx + 1
'            ' While we should still be reading chars
'            While (iChCount)
'                ' add the char to our string
'                sName = sName & Chr(dnsReply(iNdx))
'                iChCount = iChCount - 1
'                iNdx = iNdx + 1
'            End While
'            ' If the next char isn't null then the string continues, so add the dot
'            If (dnsReply(iNdx) <> 0) Then sName = sName & "."
'        End While

'    End Sub

'    Private Function GetMXName(ByRef dnsReply() As Byte, ByRef iNdx As Short, ByRef iAnCount As Short) As String

'        ' Parses the buffer returned by the DNS server, returns the best
'        ' MX server (lowest preference number), iNdx is modified to point
'        ' to the current buffer position (should be the end of the buffer
'        ' by the end, unless a record other than MX is found)

'        'Dim iChCount As Short ' Character counter
'        Dim sTemp As String ' Holds the original query string
'        Dim iBestPref As Short ' Holds the "best" preference number (lowest)
'        Dim iMXCount As Short


'        MX.Count = 0
'        MX.Best = ""
'        ReDim MX.List(0)

'        iMXCount = 0
'        iBestPref = -1

'        ParseName(dnsReply, iNdx, sTemp)

'        ' Step over null
'        iNdx = iNdx + 2

'        ' Step over 6 bytes, not sure what the 6 bytes are, but
'        ' all other documentation shows steping over these 6 bytes
'        iNdx = iNdx + 6

'        Dim sName As String
'        Dim iPref As Short
'        While (iAnCount)
'            ' Check to make sure we received an MX record
'            If (dnsReply(iNdx) = 15) Then

'                sName = ""

'                ' Step over the last half of the integer that specifies the record type (1 byte)
'                ' Step over the RR Type, RR Class, TTL (3 integers - 6 bytes)
'                iNdx = iNdx + 1 + 6

'                ' Step over the MX data length specifier (1 integer - 2 bytes)
'                iNdx = iNdx + 2

'                CopyMemory(iPref, dnsReply(iNdx), 2)
'                iPref = ntohs(iPref)
'                ' Step over the MX preference value (1 integer - 2 bytes)
'                iNdx = iNdx + 2

'                ' Have to step through the byte-stream, looking for 0xc0 or 192 (compression char)
'                ParseName(dnsReply, iNdx, sName)

'                If Trim(sName) <> "" Then
'                    iMXCount = iMXCount + 1
'                    ReDim Preserve MX.List(iMXCount - 1)
'                    MX.List(iMXCount - 1).Server = sName
'                    MX.List(iMXCount - 1).Pref = iPref
'                    MX.Count = iMXCount
'                    If (iBestPref = -1 Or iPref < iBestPref) Then
'                        iBestPref = iPref
'                        MX.Best = sName
'                    End If
'                End If
'                ' Step over 3 useless bytes
'                iNdx = iNdx + 3
'            Else
'                GetMXName = MX.Best
'                SortMX(MX.List)
'                Exit Function
'            End If
'            iAnCount = iAnCount - 1
'        End While

'        SortMX(MX.List)

'        GetMXName = MX.Best

'    End Function

'    Private Function MakeQName(ByRef sDomain As String) As String

'        ' Takes sDomain and converts it to the QNAME-type string.
'        ' QNAME is how a DNS server expects the string.
'        '
'        ' Example:  Pass -        mail.com
'        '           Returns -     &H4mail&H3com
'        '                          ^      ^
'        '                          |______|____ These two are character counters, they count
'        '                                       the number of characters appearing after them

'        Dim iQCount As Short ' Character count (between dots)
'        Dim iNdx As Short ' Index into sDomain string
'        Dim iCount As Short ' Total chars in sDomain string
'        Dim sQName As String ' QNAME string
'        Dim sDotName As String ' Temp string for chars between dots
'        Dim sChar As String ' Single char from sDomain string

'        iNdx = 1
'        iQCount = 0
'        iCount = Len(sDomain)

'        ' While we haven't hit end-of-string
'        While (iNdx <= iCount)
'            ' Read a single char from our domain
'            sChar = Mid(sDomain, iNdx, 1)
'            ' If the char is a dot, then put our character count and the part of the string
'            If (sChar = ".") Then
'                sQName = sQName & Chr(iQCount) & sDotName
'                iQCount = 0
'                sDotName = ""
'            Else
'                sDotName = sDotName & sChar
'                iQCount = iQCount + 1
'            End If
'            iNdx = iNdx + 1
'        End While

'        sQName = sQName & Chr(iQCount) & sDotName

'        MakeQName = sQName

'    End Function

'    Private Function GetHostByNameAlias(ByVal sHostName As String) As Integer

'        'Return IP address as a long, in network byte order

'        Dim phe As Integer
'        'Dim heDestHost As HostEnt
'        'Dim addrList As Integer
'        Dim retIP As Integer

'        retIP = inet_addr(sHostName)

'        If retIP = INADDR_NONE Then
'            phe = gethostbyname(sHostName)
'            If phe <> 0 Then
'                'UPGRADE_ISSUE: LenB function is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="367764E5-F3F8-4E43-AC3E-7FE0B5E074E2"'
'                'UPGRADE_WARNING: Couldn't resolve default property of object heDestHost. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
'                'CopyMemory(heDestHost, phe, LenB(heDestHost))
'                'CopyMemory(addrList, heDestHost.h_addr_list, 4)
'                'CopyMemory(retIP, addrList, heDestHost.h_length)
'            Else
'                retIP = INADDR_NONE
'            End If
'        End If

'        GetHostByNameAlias = retIP

'    End Function

'    Private Function StripTerminator(ByVal strString As String) As String

'        ' strip off trailing NULL's from API calls

'        Dim intZeroPos As Short

'        intZeroPos = InStr(strString, vbNullChar)

'        If intZeroPos > 1 Then
'            StripTerminator = Trim(Left(strString, intZeroPos - 1))
'        ElseIf intZeroPos = 1 Then
'            StripTerminator = ""
'        Else
'            StripTerminator = strString
'        End If

'    End Function

'	'UPGRADE_NOTE: Default was upgraded to Default_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
'	Private Function GetRegStr(ByRef hKeyRoot As Integer, ByVal sKeyName As String, ByVal sValueName As String, Optional ByVal Default_Renamed As String = "") As String

'		Dim lRet As Integer
'		Dim hKey As Integer
'		Dim lType As Integer
'		Dim lBytes As Integer
'		Dim sBuff As String

'		' in case there's a permissions violation
'		On Error GoTo Err_Reg

'		' Assume failure and set return to Default
'		GetRegStr = Default_Renamed

'		' Open the key
'		lRet = RegOpenKeyEx(hKeyRoot, sKeyName, 0, KEY_READ, hKey)
'		If lRet = ERROR_SUCCESS Then

'			' Determine the buffer size
'			lRet = RegQueryValueEx(hKey, sValueName, 0, lType, sBuff, lBytes)
'			If lRet = ERROR_SUCCESS Then
'				' size the buffer & call again
'				If lBytes > 0 Then
'					sBuff = Space(lBytes)
'					lRet = RegQueryValueEx(hKey, sValueName, 0, lType, sBuff, Len(sBuff))
'					If lRet = ERROR_SUCCESS Then
'						' Trim NULL and return
'						GetRegStr = Left(sBuff, lBytes - 1)
'					End If
'				End If
'			End If
'			Call RegCloseKey(hKey)
'		End If

'		Exit Function

'Err_Reg: 

'		If hKey Then Call RegCloseKey(hKey)

'	End Function

'	Private Function EnumRegKey(ByRef hKeyRoot As Integer, ByRef sKeyName As String) As String()


'		Dim lRet As Integer
'		Dim ft As FILETIME
'		Dim hKey As Integer
'		Dim CurIdx As Integer
'		Dim KeyName As String
'		Dim ClassName As String
'		Dim KeyLen As Integer
'		Dim ClassLen As Integer
'		Dim RESERVED As Integer
'		Dim sEnum() As String

'		On Error GoTo Err_Enum

'		' initialize array
'		EnumRegKey = Split("", "")

'		' Open the key
'		lRet = RegOpenKeyEx(hKeyRoot, sKeyName, 0, KEY_READ, hKey)
'		If lRet <> ERROR_SUCCESS Then Exit Function

'		' the key opened so get all the sub keys
'		Do 
'			' get each sub key until lRet = error
'			KeyLen = 2000
'			ClassLen = 2000
'			KeyName = New String(Chr(0), KeyLen)
'			ClassName = New String(Chr(0), ClassLen)
'			lRet = RegEnumKeyEx(hKey, CurIdx, KeyName, KeyLen, RESERVED, ClassName, ClassLen, ft)

'			If lRet = ERROR_SUCCESS Then
'				ReDim Preserve sEnum(CurIdx)
'				sEnum(CurIdx) = Left(KeyName, KeyLen)
'			End If

'			CurIdx = CurIdx + 1

'		Loop While lRet = ERROR_SUCCESS

'Err_Enum: 

'		EnumRegKey = VB6.CopyArray(sEnum)
'		If hKey Then Call RegCloseKey(hKey)

'	End Function

'	Private Function Exported(ByVal ModuleName As String, ByVal ProcName As String) As Boolean

'		' see if the api supports a call

'		Dim hModule As Integer
'		Dim lpProc As Integer
'		Dim FreeLib As Boolean

'		' check to see if the module is already
'		' mapped into this process.
'		hModule = GetModuleHandle(ModuleName)
'		If hModule = 0 Then
'			' not mapped, load the module into this process.
'			hModule = LoadLibrary(ModuleName)
'			FreeLib = True
'		End If

'		' check the procedure address to verify it's exported.
'		If hModule Then
'			lpProc = GetProcAddress(hModule, ProcName)
'			Exported = (lpProc <> 0)
'		End If

'		' unload library if we loaded it here.
'		If FreeLib Then Call FreeLibrary(hModule)

'	End Function

'	Private Sub SortMX(ByRef arr() As MX_RECORD, Optional ByVal bSortDesc As Boolean = False)

'		' simple bubble sort

'		Dim ValMX As MX_RECORD
'		Dim index As Integer
'		Dim firstItem As Integer
'		Dim indexLimit As Integer
'		Dim lastSwap As Integer

'		firstItem = LBound(arr)
'		lastSwap = UBound(arr)

'		Do 
'			indexLimit = lastSwap - 1
'			lastSwap = 0
'			For index = firstItem To indexLimit
'				ValMX.Pref = arr(index).Pref
'				ValMX.Server = arr(index).Server
'				If (ValMX.Pref > arr(index + 1).Pref) Xor bSortDesc Then
'					' if the items are not in order, swap them
'					arr(index).Pref = arr(index + 1).Pref
'					arr(index).Server = arr(index + 1).Server
'					arr(index + 1).Pref = ValMX.Pref
'					arr(index + 1).Server = ValMX.Server
'					lastSwap = index
'				End If
'			Next 
'		Loop While lastSwap

'	End Sub

'	Public Function GetRemoteHostName(ByVal strIpAddress As String) As String

'        'Dim udtHostEnt As HostEnt ' HOSTENT structure
'		Dim lngPtrHostEnt As Integer ' pointer to HOSTENT
'		Dim lngInetAddr As Integer ' address as a Long value
'		Dim strHostName As String ' string buffer for host name

'		' initialize the buffer
'		strHostName = New String(Chr(0), 256)

'		' Convert IP address to Long
'		lngInetAddr = inet_addr(strIpAddress)
'		If lngInetAddr = INADDR_NONE Then Exit Function

'		' Get the HostEnt structure pointer
'		lngPtrHostEnt = gethostbyaddr(lngInetAddr, 4, AF_INET)
'		If lngPtrHostEnt = 0 Then Exit Function


'		' Copy data into the HostEnt structure
'		'UPGRADE_ISSUE: LenB function is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="367764E5-F3F8-4E43-AC3E-7FE0B5E074E2"'
'		'UPGRADE_WARNING: Couldn't resolve default property of object udtHostEnt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
'        'CopyMemory(udtHostEnt, lngPtrHostEnt, LenB(udtHostEnt))

'		'CopyMemory ByVal strHostName, ByVal udtHostEnt.h_name, Len(strHostName)  'RRC V3.6.6 06022006
'        'CopyMemory(strHostName, udtHostEnt.h_name, lstrlen(udtHostEnt.h_name)) 'RRC V3.6.6 06022006

'		GetRemoteHostName = StripTerminator(strHostName)

'	End Function
'    Public Function VarPtr(ByVal e As Object) As Integer
'        'Dim GC As GCHandle = GCHandle.Alloc(e, GCHandleType.Pinned)
'        'Dim GC2 As Integer = GC.AddrOfPinnedObject.ToInt32
'        'GC.Free()
'        'Return GC2
'    End Function
'End Module
