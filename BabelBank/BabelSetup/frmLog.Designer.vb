<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmLog
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents chkShowStartStop As System.Windows.Forms.CheckBox
	Public WithEvents lstDetail As System.Windows.Forms.ListBox
	Public WithEvents cmdFileOpen As System.Windows.Forms.Button
	Public WithEvents txtMaxLines As System.Windows.Forms.TextBox
	Public WithEvents chkOverwrite As System.Windows.Forms.CheckBox
	Public WithEvents txtFilename As System.Windows.Forms.TextBox
	Public WithEvents optMailLog As System.Windows.Forms.RadioButton
	Public WithEvents OptFileLog As System.Windows.Forms.RadioButton
	Public WithEvents optWinLog As System.Windows.Forms.RadioButton
	Public WithEvents chkLog As System.Windows.Forms.CheckBox
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents lblDetail As System.Windows.Forms.Label
	Public WithEvents lblMaxLines As System.Windows.Forms.Label
	Public WithEvents lblFilename As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLog))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkShowStartStop = New System.Windows.Forms.CheckBox
        Me.lstDetail = New System.Windows.Forms.ListBox
        Me.cmdFileOpen = New System.Windows.Forms.Button
        Me.txtMaxLines = New System.Windows.Forms.TextBox
        Me.chkOverwrite = New System.Windows.Forms.CheckBox
        Me.txtFilename = New System.Windows.Forms.TextBox
        Me.optMailLog = New System.Windows.Forms.RadioButton
        Me.OptFileLog = New System.Windows.Forms.RadioButton
        Me.optWinLog = New System.Windows.Forms.RadioButton
        Me.chkLog = New System.Windows.Forms.CheckBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblDetail = New System.Windows.Forms.Label
        Me.lblMaxLines = New System.Windows.Forms.Label
        Me.lblFilename = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'chkShowStartStop
        '
        Me.chkShowStartStop.BackColor = System.Drawing.SystemColors.Control
        Me.chkShowStartStop.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkShowStartStop.Enabled = False
        Me.chkShowStartStop.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkShowStartStop.Location = New System.Drawing.Point(14, 200)
        Me.chkShowStartStop.Name = "chkShowStartStop"
        Me.chkShowStartStop.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkShowStartStop.Size = New System.Drawing.Size(179, 36)
        Me.chkShowStartStop.TabIndex = 10
        Me.chkShowStartStop.Text = "Vis Start/Stopp meldinger"
        Me.chkShowStartStop.UseVisualStyleBackColor = False
        '
        'lstDetail
        '
        Me.lstDetail.BackColor = System.Drawing.SystemColors.Window
        Me.lstDetail.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstDetail.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstDetail.Location = New System.Drawing.Point(250, 169)
        Me.lstDetail.Name = "lstDetail"
        Me.lstDetail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstDetail.Size = New System.Drawing.Size(170, 56)
        Me.lstDetail.TabIndex = 9
        '
        'cmdFileOpen
        '
        Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen.Enabled = False
        Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
        Me.cmdFileOpen.Location = New System.Drawing.Point(395, 135)
        Me.cmdFileOpen.Name = "cmdFileOpen"
        Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen.TabIndex = 7
        Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen.UseVisualStyleBackColor = False
        '
        'txtMaxLines
        '
        Me.txtMaxLines.AcceptsReturn = True
        Me.txtMaxLines.BackColor = System.Drawing.SystemColors.Window
        Me.txtMaxLines.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMaxLines.Enabled = False
        Me.txtMaxLines.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMaxLines.Location = New System.Drawing.Point(124, 169)
        Me.txtMaxLines.MaxLength = 0
        Me.txtMaxLines.Name = "txtMaxLines"
        Me.txtMaxLines.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMaxLines.Size = New System.Drawing.Size(64, 20)
        Me.txtMaxLines.TabIndex = 8
        '
        'chkOverwrite
        '
        Me.chkOverwrite.BackColor = System.Drawing.SystemColors.Control
        Me.chkOverwrite.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkOverwrite.Enabled = False
        Me.chkOverwrite.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkOverwrite.Location = New System.Drawing.Point(208, 58)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkOverwrite.Size = New System.Drawing.Size(164, 20)
        Me.chkOverwrite.TabIndex = 2
        Me.chkOverwrite.Text = "Ny loggfil hver gang"
        Me.chkOverwrite.UseVisualStyleBackColor = False
        '
        'txtFilename
        '
        Me.txtFilename.AcceptsReturn = True
        Me.txtFilename.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilename.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilename.Enabled = False
        Me.txtFilename.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilename.Location = New System.Drawing.Point(124, 135)
        Me.txtFilename.MaxLength = 100
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilename.Size = New System.Drawing.Size(267, 20)
        Me.txtFilename.TabIndex = 6
        '
        'optMailLog
        '
        Me.optMailLog.BackColor = System.Drawing.SystemColors.Window
        Me.optMailLog.Cursor = System.Windows.Forms.Cursors.Default
        Me.optMailLog.Enabled = False
        Me.optMailLog.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optMailLog.Location = New System.Drawing.Point(272, 88)
        Me.optMailLog.Name = "optMailLog"
        Me.optMailLog.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optMailLog.Size = New System.Drawing.Size(122, 34)
        Me.optMailLog.TabIndex = 5
        Me.optMailLog.TabStop = True
        Me.optMailLog.Text = "Logg til mail"
        Me.optMailLog.UseVisualStyleBackColor = False
        Me.optMailLog.Visible = False
        '
        'OptFileLog
        '
        Me.OptFileLog.BackColor = System.Drawing.SystemColors.Window
        Me.OptFileLog.Cursor = System.Windows.Forms.Cursors.Default
        Me.OptFileLog.Enabled = False
        Me.OptFileLog.ForeColor = System.Drawing.SystemColors.ControlText
        Me.OptFileLog.Location = New System.Drawing.Point(140, 88)
        Me.OptFileLog.Name = "OptFileLog"
        Me.OptFileLog.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.OptFileLog.Size = New System.Drawing.Size(122, 34)
        Me.OptFileLog.TabIndex = 4
        Me.OptFileLog.TabStop = True
        Me.OptFileLog.Text = "Logg til fil"
        Me.OptFileLog.UseVisualStyleBackColor = False
        Me.OptFileLog.Visible = False
        '
        'optWinLog
        '
        Me.optWinLog.BackColor = System.Drawing.SystemColors.Window
        Me.optWinLog.Cursor = System.Windows.Forms.Cursors.Default
        Me.optWinLog.Enabled = False
        Me.optWinLog.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optWinLog.Location = New System.Drawing.Point(12, 88)
        Me.optWinLog.Name = "optWinLog"
        Me.optWinLog.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optWinLog.Size = New System.Drawing.Size(122, 34)
        Me.optWinLog.TabIndex = 3
        Me.optWinLog.TabStop = True
        Me.optWinLog.Text = "Windows EventLog"
        Me.optWinLog.UseVisualStyleBackColor = False
        Me.optWinLog.Visible = False
        '
        'chkLog
        '
        Me.chkLog.BackColor = System.Drawing.SystemColors.Control
        Me.chkLog.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkLog.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLog.Location = New System.Drawing.Point(12, 58)
        Me.chkLog.Name = "chkLog"
        Me.chkLog.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLog.Size = New System.Drawing.Size(165, 20)
        Me.chkLog.TabIndex = 1
        Me.chkLog.Text = "Aktiver logging"
        Me.chkLog.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(209, 240)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 12
        Me.cmdCancel.Text = "55002 &Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(120, 240)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblDetail
        '
        Me.lblDetail.BackColor = System.Drawing.SystemColors.Control
        Me.lblDetail.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDetail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDetail.Location = New System.Drawing.Point(199, 172)
        Me.lblDetail.Name = "lblDetail"
        Me.lblDetail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDetail.Size = New System.Drawing.Size(48, 35)
        Me.lblDetail.TabIndex = 13
        Me.lblDetail.Text = "60311 Show"
        '
        'lblMaxLines
        '
        Me.lblMaxLines.BackColor = System.Drawing.SystemColors.Control
        Me.lblMaxLines.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMaxLines.Enabled = False
        Me.lblMaxLines.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMaxLines.Location = New System.Drawing.Point(12, 169)
        Me.lblMaxLines.Name = "lblMaxLines"
        Me.lblMaxLines.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMaxLines.Size = New System.Drawing.Size(101, 19)
        Me.lblMaxLines.TabIndex = 9
        Me.lblMaxLines.Text = "Max. antall linjer"
        '
        'lblFilename
        '
        Me.lblFilename.BackColor = System.Drawing.SystemColors.Control
        Me.lblFilename.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilename.Enabled = False
        Me.lblFilename.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilename.Location = New System.Drawing.Point(12, 135)
        Me.lblFilename.Name = "lblFilename"
        Me.lblFilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilename.Size = New System.Drawing.Size(96, 16)
        Me.lblFilename.TabIndex = 6
        Me.lblFilename.Text = "Filnavn"
        '
        'frmLog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(427, 274)
        Me.Controls.Add(Me.chkShowStartStop)
        Me.Controls.Add(Me.lstDetail)
        Me.Controls.Add(Me.cmdFileOpen)
        Me.Controls.Add(Me.txtMaxLines)
        Me.Controls.Add(Me.chkOverwrite)
        Me.Controls.Add(Me.txtFilename)
        Me.Controls.Add(Me.optMailLog)
        Me.Controls.Add(Me.OptFileLog)
        Me.Controls.Add(Me.optWinLog)
        Me.Controls.Add(Me.chkLog)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lblDetail)
        Me.Controls.Add(Me.lblMaxLines)
        Me.Controls.Add(Me.lblFilename)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmLog"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Log"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#End Region 
End Class
