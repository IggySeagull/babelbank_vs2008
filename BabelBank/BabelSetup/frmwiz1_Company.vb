Option Strict Off
Option Explicit On

Friend Class frmWiz1_Company
    Inherits System.Windows.Forms.Form
    Private frmCustomized As New frmCustomized
    Private frmEmail As New frmEmail
    Private frmTestCompany As New frmTestCompany

    Public bStatus As Boolean
    Private Sub chkBackup_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkBackup.CheckStateChanged
        BackupEnable()
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        Me.Hide()
        bStatus = False
        WizardMove((-99))
    End Sub

    Private Sub cmdCustom_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCustom.Click
        ' User can customize 1, 2 or 3 menuchoices in Setup-menu
        frmCustomized.Text = LRS(60147)
        frmCustomized.txtCustom1.Text = oProfile.Custom1Text
        frmCustomized.txtCustom2.Text = oProfile.Custom2Text
        frmCustomized.txtCustom3.Text = oProfile.Custom3Text
        frmCustomized.txtCustom4.Text = oProfile.Custom4Text
        If Not IsNothing(Trim(oProfile.InvoiceDescText)) Then
            frmCustomized.txtInvoiceDescription.Text = oProfile.InvoiceDescText
        End If
        If Not IsNothing(Trim(oProfile.GeneralNoteText)) Then
            frmCustomized.txtGeneralNote.Text = oProfile.GeneralNoteText
        End If

        frmCustomized.cmdCancel.Text = LRS(55002) '"&Avbryt"
        frmCustomized.txtInvoiceDescription.SelectionStart = 0
        frmCustomized.txtGeneralNote.SelectionStart = 0
        frmCustomized.txtCustom1.SelectionStart = 0
        frmCustomized.txtCustom2.SelectionStart = 0
        frmCustomized.txtCustom3.SelectionStart = 0
        frmCustomized.txtCustom4.SelectionStart = 0
        frmCustomized.txtInvoiceDescription.SelectionLength = Len(Trim(oProfile.InvoiceDescText))
        frmCustomized.txtGeneralNote.SelectionLength = Len(Trim(oProfile.GeneralNoteText))
        frmCustomized.txtCustom1.SelectionLength = Len(Trim(oProfile.Custom1Text))
        frmCustomized.txtCustom2.SelectionLength = Len(Trim(oProfile.Custom2Text))
        frmCustomized.txtCustom3.SelectionLength = Len(Trim(oProfile.Custom3Text))
        frmCustomized.txtCustom4.SelectionLength = Len(Trim(oProfile.Custom4Text))


        VB6.ShowForm(frmCustomized, 1, Me)
    End Sub
    Private Sub cmdEMail_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdEMail.Click
        'FormvbStyle frmEmail
        'FormLRSCaptions frmemail
        ' new 13.11.02
        ' added e-mail smtp functionality

        If Format(Date.Today, "yyyyMMdd").Substring(0, 6) = "201606" Then
            Dim oBackup As vbBabel.BabelFileHandling
            Dim BX As Boolean
            MsgBox("Da skal vi teste CreateObject")

            oBackup = New vbBabel.BabelFileHandling  '23.05.2017 CreateObject ("vbBabel.BabelFileHandling")
            oBackup.BackupPath = "\\babel-App-test\Babelaks\filer"
            oBackup.SourceFile = "\\babel-App-test\Babelaks\filer\koko.txt"
            oBackup.DeleteOriginalFile = True
            oBackup.FilesetupID = 8
            oBackup.InOut = "I"
            oBackup.FilenameNo = 8 'FilenameOut1 or 2 or 3
            oBackup.Client = vbNullString
            oBackup.TheFilenameHasACounter = False
            oBackup.BankAccounting = "B"

            bx = oBackup.CreateBackup()
            oBackup = Nothing

            MsgBox("Ferdig med test!")
        End If

        If oProfile.EmailSMTP Then
            frmEmail.optMAPI.Checked = False
            frmEmail.optSMTP.Checked = True
        Else
            frmEmail.optMAPI.Checked = True
            frmEmail.optSMTP.Checked = False
        End If
        frmEmail.txtSender.Text = Trim(oProfile.EmailSender)
        frmEmail.txtDisplayName.Text = Trim(oProfile.EmailDisplayName)
        frmEmail.txtAutoSender.Text = Trim(oProfile.EmailAutoSender)
        frmEmail.txtAutoDisplayName.Text = Trim(oProfile.EmailAutoDisplayName)
        frmEmail.txtAutoReceiver.Text = Trim(oProfile.EmailAutoReceiver)
        frmEmail.txtReplyAdress.Text = Trim(oProfile.EmailReplyAdress)
        frmEmail.txtSMTPHost.Text = Trim(oProfile.EmailSMTPHost)
        frmEmail.txtSender.SelectionLength = Len(Trim(oProfile.EmailSender))
        frmEmail.txtDisplayName.SelectionLength = Len(Trim(oProfile.EmailDisplayName))
        frmEmail.txtAutoSender.SelectionLength = Len(Trim(oProfile.EmailAutoSender))
        frmEmail.txtAutoDisplayName.SelectionLength = Len(Trim(oProfile.EmailAutoDisplayName))
        frmEmail.txtAutoReceiver.SelectionLength = Len(Trim(oProfile.EmailAutoReceiver))
        frmEmail.txtReplyAdress.SelectionLength = Len(Trim(oProfile.EmailReplyAdress))
        frmEmail.txtSMTPHost.SelectionLength = Len(Trim(oProfile.EmailSMTPHost))
        frmEmail.txtSupport.Text = Trim(oProfile.EmailSupport)
        frmEmail.txtCCSupport.Text = Trim(oProfile.EmailCCSupport)
        frmEmail.txtSupport.SelectionLength = Len(Trim(oProfile.EmailSupport))
        frmEmail.txtCCSupport.SelectionLength = Len(Trim(oProfile.EmailCCSupport))

        VB6.ShowForm(frmEmail, 1, Me)
    End Sub
    Private Sub CmdHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdHelp.Click
        ' Show Helptopic for Company (0103)
        'bOK = WinHelp(Handle.ToInt32, App.HelpFile, HELP_CONTEXT, CInt(103))
    End Sub
    Private Sub cmdTest_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdTest.Click
        'LoadTestCompany()

        frmTestCompany.chkRunTempCode.CheckState = IIf(oProfile.RunTempCode, 1, 0)
        frmTestCompany.txtDebugCode.Text = CStr(oProfile.DebugCode)

        VB6.ShowForm(frmTestCompany, 1, Me)
    End Sub
    Sub frmWiz1_Company_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        BackupEnable()
        FormvbStyle(Me, "dollar.jpg", 500)
        FormLRSCaptions(Me)
        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(180, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next

        'Added 10.12.2020
        If Not oProfile Is Nothing Then
            If oProfile.Custom1Text.Trim.ToUpper = "SAFE MODE" Then
                If oProfile.Custom1Value.Trim.ToUpper = "AKTIV" Or oProfile.Custom1Value.Trim.ToUpper = "ACTIVE" Then
                    Me.chkDisableAdminMenus.Text = "Deaktivér AML"
                    Me.chkLockBabelBank.Text = "Deaktivér Rett betaler"
                End If
            End If
        End If

    End Sub
    Private Sub CmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdNext.Click
        ' Check if chkBackup and txtBackupPath are filled

        If Me.chkBackup.CheckState = 1 And Trim(Me.txtBackupPath.Text) = "" Then
            MsgBox(LRS(60300), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "BabelBank")
        Else
            Me.Hide()
            bStatus = True

            ' save companyinfo to collection
            ' Error if ' is used, like Haakon VII's gt.
            oProfile.CompanyName = Replace(Me.txtCompany.Text, "'", "")
            oProfile.CompanyAdr1 = Replace(Me.txtAdress1.Text, "'", "")
            oProfile.CompanyAdr2 = Replace(Me.txtAdress2.Text, "'", "")
            oProfile.CompanyAdr3 = Replace(Me.txtAdress3.Text, "'", "")
            oProfile.CompanyZip = Me.txtZip.Text
            oProfile.CompanyCity = Replace(Me.txtCity.Text, "'", "")
            oProfile.CompanyNo = Me.txtCompanyNo.Text
            oProfile.Division = Me.txtDivision.Text.Trim
            oProfile.AdditionalNo = Me.txtAdditionalNo.Text
            oProfile.BaseCurrency = Me.txtBaseCurrency.Text
            oProfile.Backup = CBool(Me.chkBackup.CheckState)
            oProfile.DelDays = CShort(Me.txtDelDays.Text)
            oProfile.BackupPath = Me.txtBackupPath.Text
            oProfile.MaxSizeDB = CInt(Me.txtMaxSizeDB.Text)  ' 17.11.2020 from cShort
            oProfile.DisableAdminMenus = CBool(Me.chkDisableAdminMenus.CheckState)
            oProfile.LockBabelBank = CBool(Me.chkLockBabelBank.CheckState)

            ' added 20.06.2017
            oProfile.CreateBackup = CBool(Me.chkMakeCopyBeforeExport.CheckState)
            oProfile.BckBeforeImport = CBool(Me.chkBeforeImport.CheckState)
            oProfile.BckBeforeExport = CBool(Me.chkBeforeExport.CheckState)
            oProfile.BckAfterExport = CBool(Me.chkAfterExport.CheckState)
            oProfile.ExportBackuppath = Me.txtPathForBackup.Text
            oProfile.ExportBackupGenerations = Trim(Str(Me.cmbNoOfBackups.SelectedIndex + 1))

            oProfile.Status = vbBabel.Profile.CollectionStatus.Changed
            WizardMove((1))

        End If
    End Sub
    Private Sub BackupEnable()
        If chkBackup.CheckState = 1 Then
            lblDelDays.Enabled = True
            txtDelDays.Enabled = True
            lblBackupPath.Enabled = True
            txtBackupPath.Enabled = True
            cmdFileOpen.Enabled = True
        Else
            lblDelDays.Enabled = False
            txtDelDays.Enabled = False
            lblBackupPath.Enabled = False
            txtBackupPath.Enabled = False
            cmdFileOpen.Enabled = False
        End If

    End Sub
    Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
        ' Hent filnavn
        Dim spath As String

        ' Use BrowseForFolders, routine from VBNet ( i Utils)
        'spath = FolderPicker(Me, LRS(60010), False)  'Velg fil")
        ' spath ends with end of string - remove
        'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)

        ' Changed 18.10.05
        ' using a new function where we can pass a path
        'spath = FixPath((Me.txtBackupPath.Text))
        '31.12.2010
        spath = BrowseForFilesOrFolders(Me.txtBackupPath.Text, Me, LRS(60010), False)

        ' If browseforfilesorfolders returns empty path, then keep the old one;
        If Len(spath) > 0 Then
            Me.txtBackupPath.Text = spath
        End If

    End Sub


    Private Sub frmWiz1_Company_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason

        If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
            cmdCancel_Click(CmdCancel, New System.EventArgs())
        End If

        eventArgs.Cancel = Cancel
    End Sub

    Private Sub txtCompany_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCompany.Enter
        txtCompany.SelectionStart = 0
        txtCompany.SelectionLength = Len(txtCompany.Text)
    End Sub
    Private Sub txtAdress1_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAdress1.Enter
        txtAdress1.SelectionStart = 0
        txtAdress1.SelectionLength = Len(txtAdress1.Text)
    End Sub
    Private Sub txtAdress2_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAdress2.Enter
        txtAdress2.SelectionStart = 0
        txtAdress2.SelectionLength = Len(txtAdress2.Text)
    End Sub
    Private Sub txtAdress3_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAdress3.Enter
        txtAdress3.SelectionStart = 0
        txtAdress3.SelectionLength = Len(txtAdress3.Text)
    End Sub
    Private Sub txtzip_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtZip.Enter
        txtZip.SelectionStart = 0
        txtZip.SelectionLength = Len(txtZip.Text)
    End Sub
    Private Sub txtcity_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCity.Enter
        txtCity.SelectionStart = 0
        txtCity.SelectionLength = Len(txtCity.Text)
    End Sub
    Private Sub txtcompanyno_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCompanyNo.Enter
        txtCompanyNo.SelectionStart = 0
        txtCompanyNo.SelectionLength = Len(txtCompanyNo.Text)
    End Sub
    Private Sub txtDivision_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDivision.Enter
        txtDivision.SelectionStart = 0
        txtDivision.SelectionLength = Len(txtDivision.Text)
    End Sub
    Private Sub txtadditionalno_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAdditionalNo.Enter
        txtAdditionalNo.SelectionStart = 0
        txtAdditionalNo.SelectionLength = Len(txtAdditionalNo.Text)
    End Sub
    Private Sub txtBaseCurrency_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBaseCurrency.Enter
        txtBaseCurrency.SelectionStart = 0
        txtBaseCurrency.SelectionLength = Len(txtBaseCurrency.Text)
    End Sub
    Private Sub txtdeldays_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDelDays.Enter
        txtDelDays.SelectionStart = 0
        txtDelDays.SelectionLength = Len(txtDelDays.Text)
    End Sub
    Private Sub txtbackuppath_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBackupPath.Enter
        txtBackupPath.SelectionStart = 0
        txtBackupPath.SelectionLength = Len(txtBackupPath.Text)
    End Sub
    Private Sub txtMaxSizeDB_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMaxSizeDB.Enter
        txtMaxSizeDB.SelectionStart = 0
        txtMaxSizeDB.SelectionLength = Len(txtMaxSizeDB.Text)
    End Sub
    Private Sub txtMaxSizeDB_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtMaxSizeDB.KeyDown
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000

        If KeyCode = Keys.Enter Then  'vbCrLf Then
            ValidatetxtMaxsizeDB()
        End If

    End Sub
    Private Sub txtMaxSizeDB_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMaxSizeDB.Leave

        ValidatetxtMaxsizeDB()

    End Sub
    Private Sub ValidatetxtMaxsizeDB()

        If Not vbIsNumeric((txtMaxSizeDB.Text), "0123456789") Then
            MsgBox(LRS(60459), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, LRS(60460))
            txtMaxSizeDB.Focus()
        ElseIf Val(txtMaxSizeDB.Text) < 3 Then
            MsgBox(LRS(60463), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, LRS(60460))
            txtMaxSizeDB.Text = "3"
            txtMaxSizeDB.Focus()
        End If

    End Sub
    Private Sub chkMakeCopyBeforeExport_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkMakeCopyBeforeExport.CheckStateChanged

        If Me.chkMakeCopyBeforeExport.CheckState = System.Windows.Forms.CheckState.Checked Then
            Me.txtPathForBackup.Enabled = True
            Me.lblPathForBackup.Enabled = True
            Me.cmbNoOfBackups.Enabled = True
            Me.lblNoOfBackups.Enabled = True
            Me.chkBeforeImport.Enabled = True
            Me.chkBeforeExport.Enabled = True
            Me.chkAfterExport.Enabled = True
            Me.cmdFileExportBackup.Enabled = True
        Else
            Me.txtPathForBackup.Enabled = False
            Me.lblPathForBackup.Enabled = False
            Me.cmbNoOfBackups.Enabled = False
            Me.lblNoOfBackups.Enabled = False
            Me.chkBeforeImport.Enabled = False
            Me.chkBeforeExport.Enabled = False
            Me.chkAfterExport.Enabled = False
            Me.cmdFileExportBackup.Enabled = False
        End If

    End Sub
    Private Sub cmdFileExportBackup_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileExportBackup.Click
        ' Hent filnavn
        Dim sPath As String

        sPath = BrowseForFilesOrFolders(Me.txtPathForBackup.Text, Me, LRS(60010), True)
        ' If browseforfilesorfolders returns empty path, then keep the old one;
        If Len(sPath) > 0 Then
            Me.txtPathForBackup.Text = sPath
        End If

    End Sub

End Class
