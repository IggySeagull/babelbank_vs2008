﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClientDetailISO20022
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.OK_Button = New System.Windows.Forms.Button
        Me.Cancel_Button = New System.Windows.Forms.Button
        Me.chkAdjustToSchema = New System.Windows.Forms.CheckBox
        Me.lblLine3 = New System.Windows.Forms.Label
        Me.chkIBANToBBANNorway = New System.Windows.Forms.CheckBox
        Me.chkIBANTOBBANSweden = New System.Windows.Forms.CheckBox
        Me.chkIBANToBBANDenmark = New System.Windows.Forms.CheckBox
        Me.chkAllowDifferenceInCurrency = New System.Windows.Forms.CheckBox
        Me.lblHeading = New System.Windows.Forms.Label
        Me.chkISOSRedoFreetextToStruct = New System.Windows.Forms.CheckBox
        Me.chkISOSplitOCRWithCredits = New System.Windows.Forms.CheckBox
        Me.chkISOSplitOCR = New System.Windows.Forms.CheckBox
        Me.chkISODoNotGroup = New System.Windows.Forms.CheckBox
        Me.chkISORemoveBICInSEPA = New System.Windows.Forms.CheckBox
        Me.chkUseTransferCurrency = New System.Windows.Forms.CheckBox
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(383, 376)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "55002-&Avbryt"
        '
        'chkAdjustToSchema
        '
        Me.chkAdjustToSchema.BackColor = System.Drawing.SystemColors.Control
        Me.chkAdjustToSchema.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkAdjustToSchema.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAdjustToSchema.Location = New System.Drawing.Point(232, 67)
        Me.chkAdjustToSchema.Name = "chkAdjustToSchema"
        Me.chkAdjustToSchema.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAdjustToSchema.Size = New System.Drawing.Size(294, 18)
        Me.chkAdjustToSchema.TabIndex = 60
        Me.chkAdjustToSchema.Text = "60646 - AdjustToSchema"
        Me.chkAdjustToSchema.UseVisualStyleBackColor = False
        '
        'lblLine3
        '
        Me.lblLine3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine3.Location = New System.Drawing.Point(10, 368)
        Me.lblLine3.Name = "lblLine3"
        Me.lblLine3.Size = New System.Drawing.Size(515, 1)
        Me.lblLine3.TabIndex = 61
        Me.lblLine3.Text = "Label1"
        '
        'chkIBANToBBANNorway
        '
        Me.chkIBANToBBANNorway.BackColor = System.Drawing.SystemColors.Control
        Me.chkIBANToBBANNorway.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkIBANToBBANNorway.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkIBANToBBANNorway.Location = New System.Drawing.Point(232, 90)
        Me.chkIBANToBBANNorway.Name = "chkIBANToBBANNorway"
        Me.chkIBANToBBANNorway.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkIBANToBBANNorway.Size = New System.Drawing.Size(294, 18)
        Me.chkIBANToBBANNorway.TabIndex = 62
        Me.chkIBANToBBANNorway.Text = "60647 - Redo from IBAN to BBAN for Norway"
        Me.chkIBANToBBANNorway.UseVisualStyleBackColor = False
        '
        'chkIBANTOBBANSweden
        '
        Me.chkIBANTOBBANSweden.BackColor = System.Drawing.SystemColors.Control
        Me.chkIBANTOBBANSweden.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkIBANTOBBANSweden.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkIBANTOBBANSweden.Location = New System.Drawing.Point(232, 113)
        Me.chkIBANTOBBANSweden.Name = "chkIBANTOBBANSweden"
        Me.chkIBANTOBBANSweden.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkIBANTOBBANSweden.Size = New System.Drawing.Size(294, 18)
        Me.chkIBANTOBBANSweden.TabIndex = 63
        Me.chkIBANTOBBANSweden.Text = "60648 - Redo from IBAN to BBAN for Sweden"
        Me.chkIBANTOBBANSweden.UseVisualStyleBackColor = False
        '
        'chkIBANToBBANDenmark
        '
        Me.chkIBANToBBANDenmark.BackColor = System.Drawing.SystemColors.Control
        Me.chkIBANToBBANDenmark.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkIBANToBBANDenmark.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkIBANToBBANDenmark.Location = New System.Drawing.Point(232, 136)
        Me.chkIBANToBBANDenmark.Name = "chkIBANToBBANDenmark"
        Me.chkIBANToBBANDenmark.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkIBANToBBANDenmark.Size = New System.Drawing.Size(294, 18)
        Me.chkIBANToBBANDenmark.TabIndex = 64
        Me.chkIBANToBBANDenmark.Text = "60649 - Redo from IBAN to BBAN for Denmark"
        Me.chkIBANToBBANDenmark.UseVisualStyleBackColor = False
        '
        'chkAllowDifferenceInCurrency
        '
        Me.chkAllowDifferenceInCurrency.BackColor = System.Drawing.SystemColors.Control
        Me.chkAllowDifferenceInCurrency.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkAllowDifferenceInCurrency.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAllowDifferenceInCurrency.Location = New System.Drawing.Point(232, 159)
        Me.chkAllowDifferenceInCurrency.Name = "chkAllowDifferenceInCurrency"
        Me.chkAllowDifferenceInCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAllowDifferenceInCurrency.Size = New System.Drawing.Size(294, 18)
        Me.chkAllowDifferenceInCurrency.TabIndex = 65
        Me.chkAllowDifferenceInCurrency.Text = "60650 - Allow difference in invoicecurrency and paymentcurrency"
        Me.chkAllowDifferenceInCurrency.UseVisualStyleBackColor = False
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.Color.Transparent
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblHeading.Location = New System.Drawing.Point(142, 21)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(105, 20)
        Me.lblHeading.TabIndex = 66
        Me.lblHeading.Text = "ISO 20022"
        '
        'chkISOSRedoFreetextToStruct
        '
        Me.chkISOSRedoFreetextToStruct.BackColor = System.Drawing.SystemColors.Control
        Me.chkISOSRedoFreetextToStruct.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkISOSRedoFreetextToStruct.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkISOSRedoFreetextToStruct.Location = New System.Drawing.Point(232, 228)
        Me.chkISOSRedoFreetextToStruct.Name = "chkISOSRedoFreetextToStruct"
        Me.chkISOSRedoFreetextToStruct.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkISOSRedoFreetextToStruct.Size = New System.Drawing.Size(221, 18)
        Me.chkISOSRedoFreetextToStruct.TabIndex = 69
        Me.chkISOSRedoFreetextToStruct.Text = "60586 - When mix, redo to structured"
        Me.chkISOSRedoFreetextToStruct.UseVisualStyleBackColor = False
        '
        'chkISOSplitOCRWithCredits
        '
        Me.chkISOSplitOCRWithCredits.BackColor = System.Drawing.SystemColors.Control
        Me.chkISOSplitOCRWithCredits.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkISOSplitOCRWithCredits.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkISOSplitOCRWithCredits.Location = New System.Drawing.Point(232, 205)
        Me.chkISOSplitOCRWithCredits.Name = "chkISOSplitOCRWithCredits"
        Me.chkISOSplitOCRWithCredits.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkISOSplitOCRWithCredits.Size = New System.Drawing.Size(221, 18)
        Me.chkISOSplitOCRWithCredits.TabIndex = 68
        Me.chkISOSplitOCRWithCredits.Text = "60585 - Split OCR (with credits)"
        Me.chkISOSplitOCRWithCredits.UseVisualStyleBackColor = False
        '
        'chkISOSplitOCR
        '
        Me.chkISOSplitOCR.BackColor = System.Drawing.SystemColors.Control
        Me.chkISOSplitOCR.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkISOSplitOCR.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkISOSplitOCR.Location = New System.Drawing.Point(232, 182)
        Me.chkISOSplitOCR.Name = "chkISOSplitOCR"
        Me.chkISOSplitOCR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkISOSplitOCR.Size = New System.Drawing.Size(221, 18)
        Me.chkISOSplitOCR.TabIndex = 67
        Me.chkISOSplitOCR.Text = "60584 - Split OCR (no credits)"
        Me.chkISOSplitOCR.UseVisualStyleBackColor = False
        '
        'chkISODoNotGroup
        '
        Me.chkISODoNotGroup.BackColor = System.Drawing.SystemColors.Control
        Me.chkISODoNotGroup.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkISODoNotGroup.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkISODoNotGroup.Location = New System.Drawing.Point(232, 252)
        Me.chkISODoNotGroup.Name = "chkISODoNotGroup"
        Me.chkISODoNotGroup.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkISODoNotGroup.Size = New System.Drawing.Size(221, 18)
        Me.chkISODoNotGroup.TabIndex = 70
        Me.chkISODoNotGroup.Text = "60587 - Do NOT group in exportfile"
        Me.chkISODoNotGroup.UseVisualStyleBackColor = False
        '
        'chkISORemoveBICInSEPA
        '
        Me.chkISORemoveBICInSEPA.BackColor = System.Drawing.SystemColors.Control
        Me.chkISORemoveBICInSEPA.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkISORemoveBICInSEPA.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkISORemoveBICInSEPA.Location = New System.Drawing.Point(232, 276)
        Me.chkISORemoveBICInSEPA.Name = "chkISORemoveBICInSEPA"
        Me.chkISORemoveBICInSEPA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkISORemoveBICInSEPA.Size = New System.Drawing.Size(221, 18)
        Me.chkISORemoveBICInSEPA.TabIndex = 71
        Me.chkISORemoveBICInSEPA.Text = "60588 - Remove BIC for SEPA payments"
        Me.chkISORemoveBICInSEPA.UseVisualStyleBackColor = False
        '
        'chkUseTransferCurrency
        '
        Me.chkUseTransferCurrency.BackColor = System.Drawing.SystemColors.Control
        Me.chkUseTransferCurrency.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUseTransferCurrency.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUseTransferCurrency.Location = New System.Drawing.Point(232, 300)
        Me.chkUseTransferCurrency.Name = "chkUseTransferCurrency"
        Me.chkUseTransferCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUseTransferCurrency.Size = New System.Drawing.Size(221, 52)
        Me.chkUseTransferCurrency.TabIndex = 72
        Me.chkUseTransferCurrency.Text = "60596 - Allow transfer in other currency than the invoicecurrency. Tests have bee" & _
            "n approved by the customer."""
        Me.chkUseTransferCurrency.UseVisualStyleBackColor = False
        '
        'frmClientDetailISO20022
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(541, 417)
        Me.ControlBox = False
        Me.Controls.Add(Me.chkUseTransferCurrency)
        Me.Controls.Add(Me.chkISORemoveBICInSEPA)
        Me.Controls.Add(Me.chkISODoNotGroup)
        Me.Controls.Add(Me.chkISOSRedoFreetextToStruct)
        Me.Controls.Add(Me.chkISOSplitOCRWithCredits)
        Me.Controls.Add(Me.chkISOSplitOCR)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.chkAllowDifferenceInCurrency)
        Me.Controls.Add(Me.chkIBANToBBANDenmark)
        Me.Controls.Add(Me.chkIBANTOBBANSweden)
        Me.Controls.Add(Me.chkIBANToBBANNorway)
        Me.Controls.Add(Me.lblLine3)
        Me.Controls.Add(Me.chkAdjustToSchema)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmClientDetailISO20022"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ISO 20022"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Public WithEvents chkAdjustToSchema As System.Windows.Forms.CheckBox
    Friend WithEvents lblLine3 As System.Windows.Forms.Label
    Public WithEvents chkIBANToBBANNorway As System.Windows.Forms.CheckBox
    Public WithEvents chkIBANTOBBANSweden As System.Windows.Forms.CheckBox
    Public WithEvents chkIBANToBBANDenmark As System.Windows.Forms.CheckBox
    Public WithEvents chkAllowDifferenceInCurrency As System.Windows.Forms.CheckBox
    Public WithEvents lblHeading As System.Windows.Forms.Label
    Public WithEvents chkISOSRedoFreetextToStruct As System.Windows.Forms.CheckBox
    Public WithEvents chkISOSplitOCRWithCredits As System.Windows.Forms.CheckBox
    Public WithEvents chkISOSplitOCR As System.Windows.Forms.CheckBox
    Public WithEvents chkISODoNotGroup As System.Windows.Forms.CheckBox
    Public WithEvents chkISORemoveBICInSEPA As System.Windows.Forms.CheckBox
    Public WithEvents chkUseTransferCurrency As System.Windows.Forms.CheckBox

End Class
