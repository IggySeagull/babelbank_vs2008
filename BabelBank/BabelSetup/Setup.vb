Option Strict Off
Option Explicit On

Module Setup
    '-----------------------------------------------------------
    Private frmWiz1_Company As frmWiz1_Company
    Private frmWiz2_WhichProfile As frmWiz2_WhichProfile
    Private frmCustomized As frmCustomized
    Private frmDatabase As frmDatabase
    Private frmWiz3_FormatFromAccount As frmWiz3_FormatFromAccount
    Private frmWiz4_FilenameAccount As frmWiz4_FilenameAccount
    Private frmSpecial As frmSpecial
    Private frmFilter As frmFilter
    Private frmWiz5_FormatToBank As frmWiz5_FormatToBank
    Private frmWiz6_FilenameToBank As frmWiz6_FilenameToBank
    Private frmWiz_Report As frmWiz_Report
    Private frmReportAdvanced As frmReportAdvanced
    Private frmReportPrinter As frmReportPrinter
    Private frmReportSQL As frmReportSQL
    Private frmWiz8_Name As frmWiz8_Name
    Private frmWiz9_Continue As frmWiz9_Continue
    Private frmWiz10_FormatFromBank As frmWiz10_FormatFromBank
    Private frmWiz11_FilenameFromBank As frmWiz11_FilenameFromBank
    Private frmMatch As frmMatch
    Private frmWiz12_FormatToAccount As frmWiz12_FormatToAccount
    Private frmWiz13_FilenameToAccount As frmWiz13_FilenameToAccount
    'Private frmWiz13_FilenameMatch As frmWiz13_FilenameMatch
    Private frmWiz15_NameReturn As frmWiz15_NameReturn
    'Private frmQuickProfiles As frmQuickProfiles
    'Private frmStructuredInvoice As frmStructuredInvoice
    Private frmEmail As frmEmail
    Private frmAdvanced As frmAdvanced
    Private frmAdvancedReturn As frmAdvancedReturn
    Private frmTestCompany As frmTestCompany
    Private frmDnB_TBIW_Send As frmDnB_TBIW_Send
    Private frmMapping As frmMapping
    Private frmDnB_TBIW_Advanced As frmDnB_TBIW_Advanced
    Private frmDnB_TBIW_MailPrint As frmDnB_TBIW_MailPrint
    Private frmDnB_TBIW_Charges As frmDnB_TBIW_Charges
    Private frmDnB_TBIW_MergePayments As frmDnB_TBIW_MergePayments
    Private frmDnB_TBIW_Accounts As frmDnB_TBIW_Accounts
    Private frmDNB_TBIW_CurrencyField As frmDNB_TBIW_CurrencyField
    Private frmSecurity As frmSecurity
    Private frmSecureEnvelope As frmSecureEnvelope

    Public oProfile As vbbabel.Profile
    Public oFilesetup As vbbabel.FileSetup
    Public oReport As vbbabel.Report
    Public iWizardStepNo As Short ' Counter for wizard form number
    Private iWiz6Run As Short ' counter for how many times we have run wizard 6
    Private iWiz11Run As Short ' counter for how many times we have run wizard 11
    Private iWiz13Run As Short ' counter for how many times we have run wizard 13
    Private iPreviousJump As Short ' To track if user pressed previous
    Private bReturnProfileOnly As Boolean 'true if only returnprofile
    Private bSendProfileOnly As Boolean ' true if only sendprofile
    Private bMATCHProfile As Boolean

    Private aFormatsAndProfiles(,) As Object
    Private sSendProfileName As String
    Private sReturnProfileName As String
    Private bNewProfile As Boolean ' Setup called without a profilename
    Private aBankArray(,) As String
    Private aFilenames(,) As String

    Public Const cNAME As Short = 0
    Public Const cFormat_ID As Short = 1
    Public Const cInfoNewClient As Short = 2
    Public Const cKeepBatch As Short = 3
    Public Const cCtrlNegative As Short = 4
    Public Const cCtrlZip As Short = 5
    Public Const cCtrlAccount As Short = 6
    Public Const cCountry As Short = 7
    Public Const cTextFileimp1 As Short = 8
    Public Const cTextFileimp2 As Short = 9
    Public Const cTextFileimp3 As Short = 10
    Public Const cEnumID As Short = 11
    Public Const cFromAccountingSystem As Short = 12
    Public Const cPaymentout As Short = 13
    'Public Const cEnumID As Integer = 14
    Public Const cLabelFileimp1 As Short = 15
    Public Const cLabelFileimp2 As Short = 16
    Public Const cLabelFileimp3 As Short = 17
    Public Const cTextFileexp1 As Short = 18
    Public Const cTextFileexp2 As Short = 19
    Public Const cTextFileexp3 As Short = 20
    Public Const cLabelFileexp1 As Short = 21
    Public Const cLabelFileexp2 As Short = 22
    Public Const cLabelFileexp3 As Short = 23
    Public Const cVersion As Short = 24
    'UPGRADE_NOTE: cType was upgraded to cType_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Public Const cType_Renamed As Short = 25
    Public Const cReturnMass As Short = 26
    Private listitem As _MyListBoxItem
    Public Function ShowButtonCaptions(ByRef frm As System.Windows.Forms.Form) As Boolean
        ' Update captions with correct language (called from FormLoad in all wizards
        ' And center form in screen
        Dim ctl As System.Windows.Forms.Control


        For Each ctl In frm.Controls 'frm.Controls

            If ctl.Name = "CmdHelp" Then
                ctl.Text = LRS(55001)
            End If
            If ctl.Name = "CmdCancel" Then
                ctl.Text = LRS(55002)
            End If
            If ctl.Name = "CmdBack" Then
                ctl.Text = LRS(55003)
            End If
            If ctl.Name = "CmdNext" Then
                ctl.Text = LRS(55004)
            End If
            If ctl.Name = "CmdFinish" Then
                ctl.Text = LRS(55005)
            End If
        Next ctl

        frm.Left = VB6.TwipsToPixelsX((VB6.PixelsToTwipsX(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width) - VB6.PixelsToTwipsX(frm.Width)) / 2) ' Center form horizontally.
        frm.Top = VB6.TwipsToPixelsY((VB6.PixelsToTwipsY(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height) - VB6.PixelsToTwipsY(frm.Height)) / 2)

        ShowButtonCaptions = True
    End Function
    'UPGRADE_NOTE: Main was upgraded to Main_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Public Function Main_Renamed(ByRef oP As Object, ByRef iID As Short) As Boolean
        'Dim Printer As New Printer 'oP is a profile-object
        Dim i As Short
        Dim Style As Short
        Dim iLRS As Integer
        Dim bStatus, bx As Boolean
        Dim nElement As Integer
        Dim nFirstActive As Integer
        Dim iFilesetupOut_ID As Integer
        Dim bAutoMatch As Boolean = False
        Dim bBckBeforeExport, bCreateBackup, bBckBeforeImport, bBckAfterExport As Boolean
        Dim sExportBackuppath As String
        Dim iExportBackupGenerations As Short

        bStatus = False

        'frmWiz1_Company = New frmWiz1_Company
        'frmWiz2_WhichProfile = New frmWiz2_WhichProfile
        'frmCustomized = New frmCustomized
        'frmDatabase = New frmDatabase
        'frmWiz3_FormatFromAccount = New frmWiz3_FormatFromAccount
        'frmWiz4_FilenameAccount = New frmWiz4_FilenameAccount
        'frmSpecial = New frmSpecial
        'frmFilter = New frmFilter
        'frmWiz5_FormatToBank = New frmWiz5_FormatToBank

        'frmJCL = New frmJCL
        'frmWiz_Report = New frmWiz_Report
        'frmReportAdvanced = New frmReportAdvanced
        'frmReportPrinter = New frmReportPrinter
        'frmWiz8_Name = New frmWiz8_Name
        'frmWiz9_Continue = New frmWiz9_Continue
        'frmWiz10_FormatFromBank = New frmWiz10_FormatFromBank
        'frmWiz11_FilenameFromBank = New frmWiz11_FilenameFromBank
        'frmMatch = New frmMatch
        'frmWiz12_FormatToAccount = New frmWiz12_FormatToAccount
        'frmWiz13_FilenameToAccount = New frmWiz13_FilenameToAccount
        'frmWiz13_FilenameMatch = New frmWiz13_FilenameMatch
        'frmWiz15_NameReturn = New frmWiz15_NameReturn
        'frmQuickProfiles = New frmQuickProfiles  ' TODO Not in use ?
        'frmStructuredInvoice = New frmStructuredInvoice
        'frmEmail = New frmEmail
        'frmAdvanced = New frmAdvanced
        'frmAdvancedReturn = New frmAdvancedReturn
        ''frmTestCompany = New frmTestCompany
        'frmDnB_TBIW_Send = New frmDnB_TBIW_Send
        'frmMapping = New frmMapping
        'frmDnB_TBIW_Advanced = New frmDnB_TBIW_Advanced
        'frmDnB_TBIW_MailPrint = New frmDnB_TBIW_MailPrint
        'frmDnB_TBIW_Charges = New frmDnB_TBIW_Charges
        'frmDnB_TBIW_MergePayments = New frmDnB_TBIW_MergePayments
        'frmDnB_TBIW_Accounts = New frmDnB_TBIW_Accounts
        'frmDnB_TBIW_NBCodes = New frmDnB_TBIW_NBcodes


        oProfile = oP
        'inFileSetup_ID = iID

        If iID = -1 Then
            ' New profile !
            ' -------------
            ' Always create two profiles, one FromAccounting=true, one false

            ' add first new filesetup to filesetups, FromAccounting=True
            oFilesetup = oProfile.FileSetups.Add(CStr(oProfile.FileSetups.Count))

            'Initial values for FromAccounting-part;
            oFilesetup.Status = vbBabel.Profile.CollectionStatus.NewCol
            oProfile.Status = vbBabel.Profile.CollectionStatus.NewCol
            'inFileSetup_ID = oProfile.FileSetups.Count
            oFilesetup.FileSetup_ID = oProfile.FileSetups.Count
            ' FilesetupOut is the next created Filesetup, the FromAccounting=False-part
            ' Assumes at this stage that we have only one sendprofile/one returnprofile
            oFilesetup.FileSetupOut = oProfile.FileSetups.Count + 1
            oFilesetup.Bank_ID = 0
            oFilesetup.FromAccountingSystem = True
            ' Update backupin/out with backupflag from company
            oFilesetup.BackupIn = oProfile.Backup
            oFilesetup.BackupOut = oProfile.Backup
            oFilesetup.Setup_Send = True
            oFilesetup.Setup_NewSend = True
            oFilesetup.Setup_InitSend = True
            oProfile.FileSetups.Setup_CurrentSend = oFilesetup.FileSetup_ID

            ' add second new filesetup to filesetups, FromAccounting=False
            oFilesetup = oProfile.FileSetups.Add(CStr(oProfile.FileSetups.Count))

            'Initial values for not FromAccounting-part;
            oFilesetup.Status = vbBabel.Profile.CollectionStatus.NewCol
            oProfile.Status = vbBabel.Profile.CollectionStatus.NewCol
            oFilesetup.FileSetup_ID = oProfile.FileSetups.Count
            ' FilesetupOut is the previously created Filesetup, the FromAccounting=True-part
            ' Assumes at this stage that we have only one sendprofile/one returnprofile
            oFilesetup.FileSetupOut = oProfile.FileSetups.Count - 1
            oFilesetup.Bank_ID = 0
            oFilesetup.FromAccountingSystem = False
            ' Update backupin/out with backupflag from company
            oFilesetup.BackupIn = oProfile.Backup
            oFilesetup.BackupOut = oProfile.Backup
            oFilesetup.Setup_Return = True
            oFilesetup.Setup_NewReturn = True
            oFilesetup.Setup_InitReturn = True
            oProfile.FileSetups.Setup_CurrentReturn = oFilesetup.FileSetup_ID

            bReturnProfileOnly = False
            bNewProfile = True
            sSendProfileName = ""

        Else
            ' we have a filesetupid passed from BabelbankEXE
            bNewProfile = False

            oFilesetup = oProfile.FileSetups(iID)
            If oFilesetup.FromAccountingSystem = False Then
                bReturnProfileOnly = True
                sReturnProfileName = oProfile.FileSetups(iID).ShortName

                If oFilesetup.AutoMatch Then '.FormatOutID1 <> 0 Then
                    bMATCHProfile = True
                End If
                ' FIX: CHANGE BACK!!!!
                bMATCHProfile = False

                oFilesetup.Setup_Return = True
                oFilesetup.Setup_NewReturn = False
                oFilesetup.Setup_InitReturn = True
                oProfile.FileSetups.Setup_CurrentReturn = oFilesetup.FileSetup_ID

                ' Mark the other involved filesetups;
                MarkInvolvedFileSetups() '(oFilesetup)


            Else
                sSendProfileName = oProfile.FileSetups(iID).ShortName
                bReturnProfileOnly = False
                oFilesetup.Setup_Send = True
                oFilesetup.Setup_NewSend = False
                oFilesetup.Setup_InitSend = True
                oProfile.FileSetups.Setup_CurrentSend = oFilesetup.FileSetup_ID

                ' Mark the other involved filesetups;
                MarkInvolvedFileSetups() '(oFilesetup)

            End If

            oFilesetup.Status = vbBabel.Profile.CollectionStatus.Changed
            oProfile.Status = vbBabel.Profile.CollectionStatus.Changed


        End If
        Dim aIndexOutFormats(0) As Object ' index pointing to aFormatsAndProfiles

        If bNewProfile = False And oFilesetup.FromAccountingSystem = False Then
            ' Change profile, returnprofile
            'ReDim aOutFilesetups(1, 0)
            ''aOutFilesetups(0, 0) = oFilesetup.Format_ID
            'aOutFilesetups(1, 0) = inFileSetup_ID
            iWizardStepNo = 10 'jump to returnprofile
        ElseIf bNewProfile = False And oFilesetup.FromAccountingSystem = False Then
            ' Change profile, sendprofile
            iWizardStepNo = 3
        Else
            ' New profile
            iWizardStepNo = 1
        End If

        ' Start Wizard:
        Do While True

            Select Case iWizardStepNo

                Case 1
                    ' --------------------------------------
                    ' Companyinfo
                    ' --------------------------------------
                    ' Only show wiz1 (company) if not previously saved:
                    If oProfile.CompanyName = "" Or iPreviousJump = -1 Then
                        If frmWiz1_Company Is Nothing Then 'gj�r dette for alle frm, og lukk de helt i bunn !!!
                            frmWiz1_Company = New frmWiz1_Company
                        End If
                        If frmEmail Is Nothing Then
                            frmEmail = New frmEmail
                        End If
                        frmWiz1_Company.Text = LRS(60001) & " " & sSendProfileName 'Profilename
                        ' Fill up with companyinfo
                        frmWiz1_Company.lblHeading.Text = LRS(60038) 'Companyinformation
                        frmWiz1_Company.lblCompany.Text = LRS(60039) 'Companyname
                        frmWiz1_Company.lblAdress1.Text = LRS(60040) ' Adress:
                        frmWiz1_Company.lblAdress2.Text = LRS(60040) ' Adress:
                        frmWiz1_Company.lblAdress3.Text = LRS(60040) ' Adress:
                        frmWiz1_Company.lblZip.Text = LRS(60041) ' Zipcode:
                        frmWiz1_Company.lblCity.Text = LRS(60042) ' City:
                        frmWiz1_Company.lblCompanyNo.Text = LRS(60043) 'CompanyNo:
                        frmWiz1_Company.lblAdditionalNo.Text = LRS(60044) 'CustomerID:
                        frmWiz1_Company.chkBackup.Text = LRS(60045) 'Backup:
                        frmWiz1_Company.lblDelDays.Text = LRS(60046) 'Delete after no of days:
                        frmWiz1_Company.lblBackupPath.Text = LRS(60047) 'Backuppath:
                        frmWiz1_Company.lblMaxSizeDB.Text = LRSCommon(40001) 'Maximum size for BabelBank's database:

                        ' New 15.08.02 JanP
                        If frmCustomized Is Nothing Then
                            frmCustomized = New frmCustomized
                        End If
                        ' User can customize 1, 2 or 3 menuchoices in Setup-menu
                        frmCustomized.Text = LRS(60147)
                        'frmCustomized.lblCustom1 = LRS(60147) & LRS(60148) & " 1"  ' Egendefinert skjermtekst 1
                        'frmCustomized.lblCustom2 = LRS(60147) & LRS(60148) & " 2"
                        'frmCustomized.lblCustom3 = LRS(60147) & LRS(60148) & " 3"

                        frmCustomized.txtCustom1.Text = oProfile.Custom1Text
                        frmCustomized.txtCustom2.Text = oProfile.Custom2Text
                        frmCustomized.txtCustom3.Text = oProfile.Custom3Text
                        frmCustomized.txtCustom4.Text = oProfile.Custom4Text
                        frmCustomized.txtInvoiceDescription.Text = oProfile.InvoiceDescText
                        frmCustomized.txtGeneralNote.Text = oProfile.GeneralNoteText

                        frmCustomized.txtInvoiceDescription.SelectionStart = 0
                        frmCustomized.txtGeneralNote.SelectionStart = 0
                        frmCustomized.txtCustom1.SelectionStart = 0
                        frmCustomized.txtCustom2.SelectionStart = 0
                        frmCustomized.txtCustom3.SelectionStart = 0
                        frmCustomized.txtCustom4.SelectionStart = 0
                        frmCustomized.txtInvoiceDescription.SelectionLength = Len(Trim(oProfile.InvoiceDescText))
                        frmCustomized.txtGeneralNote.SelectionLength = Len(Trim(oProfile.GeneralNoteText))
                        frmCustomized.txtCustom1.SelectionLength = Len(Trim(oProfile.Custom1Text))
                        frmCustomized.txtCustom2.SelectionLength = Len(Trim(oProfile.Custom2Text))
                        frmCustomized.txtCustom3.SelectionLength = Len(Trim(oProfile.Custom3Text))
                        frmCustomized.txtCustom4.SelectionLength = Len(Trim(oProfile.Custom4Text))

                        ' New 25.10.02
                        ' Form for connectionstring, etc to database
                        If frmDatabase Is Nothing Then
                            frmDatabase = New frmDatabase
                        End If
                        frmDatabase.txtConnectionString.Text = Trim(oProfile.ConString)
                        frmDatabase.txtUserID.Text = Trim(oProfile.ConUID)
                        frmDatabase.txtPassword.Text = Trim(oProfile.ConPWD)

                        'New 24.02.03 by Kjell
                        'New way to set oProfile in the database-form
                        'bx = frmDatabase.SetLocalProfile(oProfile)
                        'Set frmDatabase.oLocalProfile = oProfile

                        Fill_frmEmail(oProfile, frmEmail)


                        ShowButtonCaptions(frmWiz1_Company)

                        frmWiz1_Company.txtCompany.Text = oProfile.CompanyName
                        frmWiz1_Company.txtAdress1.Text = oProfile.CompanyAdr1
                        frmWiz1_Company.txtAdress2.Text = oProfile.CompanyAdr2
                        frmWiz1_Company.txtAdress3.Text = oProfile.CompanyAdr3
                        frmWiz1_Company.txtZip.Text = oProfile.CompanyZip
                        frmWiz1_Company.txtCity.Text = oProfile.CompanyCity
                        frmWiz1_Company.txtCompanyNo.Text = oProfile.CompanyNo
                        frmWiz1_Company.txtDivision.Text = oProfile.Division.Trim
                        frmWiz1_Company.txtAdditionalNo.Text = oProfile.AdditionalNo
                        frmWiz1_Company.txtBaseCurrency.Text = oProfile.BaseCurrency
                        frmWiz1_Company.chkBackup.CheckState = bVal(oProfile.Backup)
                        frmWiz1_Company.txtDelDays.Text = CStr(oProfile.DelDays)
                        frmWiz1_Company.txtBackupPath.Text = oProfile.BackupPath
                        frmWiz1_Company.txtMaxSizeDB.Text = Trim(Str(oProfile.MaxSizeDB))
                        frmWiz1_Company.chkDisableAdminMenus.CheckState = bVal(oProfile.DisableAdminMenus)
                        frmWiz1_Company.chkLockBabelBank.CheckState = bVal(oProfile.LockBabelBank)

                        GetBBExportBackupInfo(bCreateBackup, bBckBeforeImport, bBckBeforeExport, bBckAfterExport, sExportBackuppath, iExportBackupGenerations)
                        If bCreateBackup Then
                            frmWiz1_Company.chkMakeCopyBeforeExport.CheckState = System.Windows.Forms.CheckState.Checked
                        Else
                            frmWiz1_Company.chkMakeCopyBeforeExport.CheckState = System.Windows.Forms.CheckState.Unchecked
                        End If
                        If bBckBeforeImport Then
                            frmWiz1_Company.chkBeforeImport.CheckState = System.Windows.Forms.CheckState.Checked
                        Else
                            frmWiz1_Company.chkBeforeImport.CheckState = System.Windows.Forms.CheckState.Unchecked
                        End If
                        If bBckBeforeExport Then
                            frmWiz1_Company.chkBeforeExport.CheckState = System.Windows.Forms.CheckState.Checked
                        Else
                            frmWiz1_Company.chkBeforeExport.CheckState = System.Windows.Forms.CheckState.Unchecked
                        End If
                        If bBckAfterExport Then
                            frmWiz1_Company.chkAfterExport.CheckState = System.Windows.Forms.CheckState.Checked
                        Else
                            frmWiz1_Company.chkAfterExport.CheckState = System.Windows.Forms.CheckState.Unchecked
                        End If
                        frmWiz1_Company.txtPathForBackup.Text = sExportBackuppath
                        For i = 1 To 10
                            frmWiz1_Company.cmbNoOfBackups.Items.Add(Trim(Str(i)))
                        Next i
                        If iExportBackupGenerations > 0 Then
                            frmWiz1_Company.cmbNoOfBackups.SelectedIndex = iExportBackupGenerations - 1
                        Else
                            frmWiz1_Company.cmbNoOfBackups.SelectedIndex = 0
                        End If

                        frmWiz1_Company.ShowDialog()

                    Else
                        ' companyinfo filled in before - dont show
                        iWizardStepNo = 2

                    End If

                Case 2
                    ' --------------------------------------
                    ' Send- or return-profile
                    ' --------------------------------------
                    ' At this point we may have added, and skipped to, another
                    ' oFileSetup-collection, ex. if we have pressed "Previous"
                    ' Therefore:
                    If frmWiz2_WhichProfile Is Nothing Then
                        frmWiz2_WhichProfile = New frmWiz2_WhichProfile
                    End If
                    ' DEBUG:
                    If Not RunTime() Then
                        frmWiz2_WhichProfile.optMatch.Visible = True
                        frmWiz2_WhichProfile.lblExplainMatch.Visible = True
                    End If

                    If bReturnProfileOnly Then
                        ' When Back is pressed from returnprofile only
                        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)
                        frmWiz2_WhichProfile.optReturn.Checked = True
                    Else
                        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                        frmWiz2_WhichProfile.optSend.Checked = True
                    End If


                    frmWiz2_WhichProfile.Text = LRS(60001) & " " & oFilesetup.ShortName
                    frmWiz2_WhichProfile.lblHeading.Text = LRS(60048) ' Create send og returnprofile
                    frmWiz2_WhichProfile.optSend.Text = LRS(60049) ' Sendprofile
                    frmWiz2_WhichProfile.optReturn.Text = LRS(60050) ' Returnprofil
                    frmWiz2_WhichProfile.lblExplainSend.Text = LRS(60051) ' A sendprofile...
                    frmWiz2_WhichProfile.lblExplainReturn.Text = LRS(60052) ' A returnprofile...
                    ShowButtonCaptions(frmWiz2_WhichProfile)

                    frmWiz2_WhichProfile.ShowDialog()

                    ' ---------------------------------------
                    ' SENDPROFILE from here:
                    ' ---------------------------------------
                Case 3
                    ' --------------------------------------
                    ' Choose formats for sendprofile
                    ' --------------------------------------
                    ' At this point we may have added, and skipped to, another
                    ' oFileSetup-collection, ex. if we have pressed "Previous"
                    ' Therefore:
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                    If frmWiz3_FormatFromAccount Is Nothing Then
                        frmWiz3_FormatFromAccount = New frmWiz3_FormatFromAccount
                    End If

                    frmWiz3_FormatFromAccount.Text = LRS(60001) & " " & oFilesetup.ShortName
                    frmWiz3_FormatFromAccount.lblHeading.Text = LRS(60053) ' Select a format for files from accounting
                    ShowButtonCaptions(frmWiz3_FormatFromAccount)


                    ' Fill array with formats, from format table
                    If oFilesetup.Enum_ID > 999 Then
                        frmWiz3_FormatFromAccount.FillListbox((True))
                    Else
                        frmWiz3_FormatFromAccount.FillListbox((False))
                    End If

                    frmWiz3_FormatFromAccount.ShowDialog()


                Case 4
                    ' ------------------------------------------------
                    ' Choose filename for import from accountingsystem
                    ' ------------------------------------------------
                    ' At this point we may have added, and skipped to, another
                    ' oFileSetup-collection, ex. if we have pressed "Previous"
                    ' Therefore:
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                    If frmWiz4_FilenameAccount Is Nothing Then
                        frmWiz4_FilenameAccount = New frmWiz4_FilenameAccount
                    End If
                    frmWiz4_FilenameAccount.Text = LRS(60001) & " " & oFilesetup.ShortName
                    frmWiz4_FilenameAccount.lblHeading.Text = LRS(60054)
                    frmWiz4_FilenameAccount.lblDescr.Text = LRS(60055)
                    frmWiz4_FilenameAccount.fraFromInternal.Text = LRS(60056)
                    'frmWiz4_FilenameAccount.lblFromInternal.Caption = LRS(60057)

                    frmWiz4_FilenameAccount.chkBackup.Text = LRS(60060)
                    frmWiz4_FilenameAccount.chkManual.Text = LRS(60449)
                    ShowButtonCaptions(frmWiz4_FilenameAccount)

                    frmWiz4_FilenameAccount.txtFromInternal.Text = oFilesetup.FileNameIn1
                    frmWiz4_FilenameAccount.chkBackup.CheckState = bVal(oFilesetup.BackupIn)
                    frmWiz4_FilenameAccount.chkManual.CheckState = bVal(oFilesetup.Manual)

                    ' Find correct formatname;
                    For i = 0 To UBound(aFormatsAndProfiles, 2)
                        'If aFormatsAndProfiles(cFormat_ID, i) = oFilesetup.Format_ID Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If aFormatsAndProfiles(cEnumID, i) = oFilesetup.Enum_ID Then
                            ' Filename labels dependent on format:
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            iLRS = Val(aFormatsAndProfiles(cLabelFileimp1, i))
                            frmWiz4_FilenameAccount.lblFromInternal.Text = LRS(iLRS)
                        End If
                    Next i
                    frmWiz4_FilenameAccount.txtFromInternal.SelectionStart = 0
                    frmWiz4_FilenameAccount.txtFromInternal.SelectionLength = Len(Trim(oFilesetup.FileNameIn1))

                    'New 20.09.2007
                    'Pass format to frmWiz4_FilenameAccount
                    frmWiz4_FilenameAccount.SetFormatID((oFilesetup.Enum_ID))

                    ' Load text into frmSpecial
                    If frmSpecial Is Nothing Then
                        frmSpecial = New frmSpecial
                    End If
                    frmSpecial.Text = LRS(60137)
                    frmSpecial.lblSpecial.Text = Mid(LRS(60137), 2)
                    frmSpecial.cmdCancel.Text = LRS(55002)
                    frmSpecial.txtSpecial.Text = Trim(oFilesetup.TreatSpecial & Space(100))
                    frmWiz4_FilenameAccount.cmdSpecial.Text = LRS(60137)
                    frmWiz4_FilenameAccount.cmdFilter.Text = LRS(59035)

                    If frmFilter Is Nothing Then
                        frmFilter = New frmFilter
                    End If
                    frmFilter.iFileSetup_ID = oFilesetup.FileSetup_ID

                    ' added in vb.net
                    frmWiz4_FilenameAccount.SetfrmSpecial(frmSpecial)
                    frmWiz4_FilenameAccount.SetfrmFilter(frmFilter)
                    frmWiz4_FilenameAccount.txtFromInternal.Focus()
                    frmWiz4_FilenameAccount.ShowDialog()

                Case 5
                    ' --------------------------------------
                    ' Choose formats for sendprofile, to bank
                    ' --------------------------------------
                    ' At this stage we have come to the "not FromAccounting-part" of the filesetup
                    ' Goto the first "not FromAccounting-part"
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)

                    If frmWiz5_FormatToBank Is Nothing Then
                        frmWiz5_FormatToBank = New frmWiz5_FormatToBank
                    End If
                    frmWiz5_FormatToBank.Text = LRS(60001) & " " & oProfile.FileSetups((oFilesetup.FileSetupOut)).ShortName

                    frmWiz5_FormatToBank.lblHeading.Text = LRS(60061) 'Choose a format for file to bank
                    frmWiz5_FormatToBank.lblFlere.Text = LRS(60062) ' No of times
                    frmWiz5_FormatToBank.cmdFlere.Text = LRS(55006) 'Same format more than once
                    frmWiz5_FormatToBank.cmdChooseProfile.Text = LRS(55007) 'Select format from another profile
                    ShowButtonCaptions(frmWiz5_FormatToBank)

                    '
                    ' Fill array with formats, from format table
                    If oFilesetup.Enum_ID > 999 Then
                        frmWiz5_FormatToBank.FillListbox((True))
                    Else
                        frmWiz5_FormatToBank.FillListbox((False))
                    End If

                    frmWiz5_FormatToBank.ShowDialog()

                Case 6
                    ' ------------------------------------------------
                    ' Choose filename for export to bank
                    ' ------------------------------------------------
                    ' We are still at the "not FromAccounting-part" of the filesetup
                    ' Goto the first "not FromAccounting-part"
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)

                    iWiz6Run = 0 ' 0-based
                    If frmWiz6_FilenameToBank Is Nothing Then
                        frmWiz6_FilenameToBank = New frmWiz6_FilenameToBank
                    End If
                    If frmAdvanced Is Nothing Then
                        frmAdvanced = New frmAdvanced
                    End If
                    If frmSecurity Is Nothing Then
                        frmSecurity = New frmSecurity
                    End If
                    If frmSecureEnvelope Is Nothing Then
                        frmSecureEnvelope = New frmSecureEnvelope
                    End If

                    frmWiz6_FilenameToBank.Text = LRS(60001) & " " & oProfile.FileSetups((oFilesetup.FileSetupOut)).ShortName
                    frmWiz6_FilenameToBank.lblHeading.Text = LRS(60063) ' Filenames for files to bank
                    frmWiz6_FilenameToBank.lblDescr.Text = LRS(60064) ' Filenames for files to bank
                    frmWiz6_FilenameToBank.fraToBank.Text = LRS(60065) ' File to bank
                    'frmWiz6_FilenameToBank.lblToBank.Caption = LRS(60057) ' FIlename

                    frmWiz6_FilenameToBank.chkBackup.Text = LRS(60066) '
                    frmWiz6_FilenameToBank.chkOverwrite.Text = LRS(60067) '
                    frmWiz6_FilenameToBank.chkWarning.Text = LRS(60068) '
                    frmWiz6_FilenameToBank.chkRemove.Text = LRS(60144) '
                    frmWiz6_FilenameToBank.lblBankname.Text = LRS(60069)
                    frmWiz6_FilenameToBank.cmdAdvanced.Text = LRS(55008) ' Advanced options
                    frmWiz6_FilenameToBank.cmdSequence.Text = LRS(60105) ' Sequencenumbers

                    If oFilesetup.Enum_ID = 15 Then ' DnB TBIW XML
                        ' Disable Bank, enable AccountsPath
                        frmWiz6_FilenameToBank.lblBankname.Visible = False
                        frmWiz6_FilenameToBank.cmbBankname.Visible = False
                        frmWiz6_FilenameToBank.lblAccountspath.Text = LRS(60296) ' Accountsfile Path
                        frmWiz6_FilenameToBank.lblAccountspath.Visible = True
                        frmWiz6_FilenameToBank.txtAccountsPath.Visible = True
                        frmWiz6_FilenameToBank.txtAccountsPath.Text = Trim(oProfile.AccountsFile)
                        frmWiz6_FilenameToBank.txtAccountsPath.SelectionLength = Len(Trim(oProfile.AccountsFile))
                        frmWiz6_FilenameToBank.txtAccountsPath.SelectionStart = 0
                    End If
                    ShowButtonCaptions(frmWiz6_FilenameToBank)


                    ' Find correct formatname;
                    For i = 0 To UBound(aFormatsAndProfiles, 2)
                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If aFormatsAndProfiles(cEnumID, i) = oFilesetup.Enum_ID Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cNAME, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            frmWiz6_FilenameToBank.lblDescr.Text = LRS(60002) & " " + aFormatsAndProfiles(cNAME, i) & vbCrLf & LRS(60064) 'Velg filnavn for
                            ' Filename labels dependent on format:
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            iLRS = Val(aFormatsAndProfiles(cLabelFileexp1, i))
                            frmWiz6_FilenameToBank.lblToBank.Text = LRS(iLRS) ' FIlename
                        End If
                    Next i
                    frmWiz6_FilenameToBank.txtToBank.Text = Trim(oFilesetup.FileNameOut1)
                    frmWiz6_FilenameToBank.chkBackup.CheckState = bVal(oFilesetup.BackupOut)
                    frmWiz6_FilenameToBank.chkOverwrite.CheckState = bVal(oFilesetup.OverWriteOut)
                    frmWiz6_FilenameToBank.chkWarning.CheckState = bVal(oFilesetup.WarningOut)
                    frmWiz6_FilenameToBank.chkRemove.CheckState = bVal(oFilesetup.RemoveOldOut)

                    ' fill combo with banknames:
                    aBankArray = VB6.CopyArray(FetchBanks)
                    frmWiz6_FilenameToBank.cmbBankname.Items.Clear()
                    For i = 0 To UBound(aBankArray, 2)
                        frmWiz6_FilenameToBank.cmbBankname.Items.Add(aBankArray(0, i))
                        ' Mark previously selected
                        If CDbl(aBankArray(1, i)) = oFilesetup.Bank_ID Then
                            frmWiz6_FilenameToBank.cmbBankname.SelectedIndex = i
                        End If
                    Next i

                    frmWiz6_FilenameToBank.txtToBank.SelectionStart = 0
                    frmWiz6_FilenameToBank.txtToBank.SelectionLength = Len(Trim(frmWiz6_FilenameToBank.txtToBank.Text))

                    frmWiz6_FilenameToBank.ShowDialog()


                Case 7
                    '-----------------------------------------
                    ' Reports
                    ' ----------------------------------------
                    ' We're now back to the "FromAccounting=True" part
                    ' Therefore:
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)

                    ' set all parameters in all frm's for Reportsetup, and show dialog
                    ReportSetupAndShow()
                    frmWiz_Report.ShowDialog()

                Case 8
                    ' ------------------------------------------------
                    ' Give name to sendprofile
                    ' ------------------------------------------------

                    ' Still at the "FromAccounting=True" part
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                    If frmWiz8_Name Is Nothing Then
                        frmWiz8_Name = New frmWiz8_Name
                    End If
                    frmWiz8_Name.Text = LRS(60001) & " " & oFilesetup.ShortName
                    frmWiz8_Name.lblHeading.Text = LRS(60077)
                    frmWiz8_Name.lblProfilename.Text = LRS(60078)
                    frmWiz8_Name.lblDescription.Text = LRS(60143)
                    frmWiz8_Name.chkSilent.Text = LRS(60194)
                    frmWiz8_Name.cmdSignalFile.Text = LRS(60544)
                    If Not bNewProfile Then
                        frmWiz8_Name.txtProfilename.Text = oFilesetup.ShortName
                        frmWiz8_Name.txtDescription.Text = Trim(Left(oFilesetup.Description & Space(255), 255))
                    Else
                        frmWiz8_Name.txtProfilename.Text = "<profilename>" ' changed 05.01.2011Strip(Left(frmWiz3_FormatFromAccount.lstFormats.Text, 50), " ") & LRS(60003) 'Send
                    End If
                    ShowButtonCaptions(frmWiz8_Name)
                    frmWiz8_Name.txtProfilename.SelectionStart = 0
                    frmWiz8_Name.txtProfilename.SelectionLength = Len(Trim(oFilesetup.Description))
                    frmWiz8_Name.chkSilent.CheckState = bVal(oFilesetup.Silent)



                    ' added in vb.net
                    'frmWiz8_Name.SetfrmLog(frmLog)
                    'frmWiz8_Name.SetfrmSignalFile(frmSignalFile)
                    frmWiz8_Name.ShowDialog()

                Case 9
                    ' ------------------------------------------------
                    ' Continue to returnprofile ?
                    ' ------------------------------------------------
                    'frmWiz9_Continue.Caption = LRS(60084) 'Continue to returnprofile
                    ' changed 18.01.2010 - Leave setup
                    If frmWiz9_Continue Is Nothing Then
                        frmWiz9_Continue = New frmWiz9_Continue
                    End If
                    frmWiz9_Continue.Text = LRS(60083) 'Leave setup
                    'frmWiz9_Continue.lblHeading.Caption = LRS(60079)  'Keep on to returnprofile ?
                    frmWiz9_Continue.lblHeading.Text = LRS(60083) ' Leave setup
                    frmWiz9_Continue.lblDescr.Text = LRS(60080) ' ...
                    frmWiz9_Continue.optContinueReturnProfile.Text = LRS(60081) ' Continue to returnprofile
                    frmWiz9_Continue.optContinueSendProfile.Text = LRS(60082) 'Create NEW setup
                    frmWiz9_Continue.optEnd.Text = LRS(60083) 'Leave setup
                    ' added 18.01.2010
                    frmWiz9_Continue.CmdNext.Visible = False
                    frmWiz9_Continue.CmdFinish.Visible = True
                    frmWiz9_Continue.CmdFinish.Enabled = True

                    ShowButtonCaptions(frmWiz9_Continue)

                    ' If sendprofile only, and change, then position to Finish-choice;
                    If bNewProfile = False And Len(Trim(oProfile.FileSetups((oFilesetup.FileSetupOut)).FileNameIn1)) = 0 And Len(Trim(oProfile.FileSetups((oFilesetup.FileSetupOut)).FileNameIn2)) = 0 And Len(Trim(oProfile.FileSetups((oFilesetup.FileSetupOut)).FileNameIn3)) = 0 Then

                        frmWiz9_Continue.optEnd.Checked = True
                    End If
                    frmWiz9_Continue.ShowDialog()



                    ' ------------------------------------------------
                    ' RETURNPROFILE from here
                    ' ------------------------------------------------

                Case 10
                    ' ------------------------------------------------
                    ' Format for returnprofil,
                    ' used when we have no sendprofile
                    ' ------------------------------------------------

                    ' At this stage we have come to the "not FromAccounting-part" of the filesetup
                    ' Goto the first "not FromAccounting-part"
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)

                    If frmWiz10_FormatFromBank Is Nothing Then
                        frmWiz10_FormatFromBank = New frmWiz10_FormatFromBank
                    End If
                    frmWiz10_FormatFromBank.Text = LRS(60001) & " " & oFilesetup.ShortName
                    frmWiz10_FormatFromBank.lblHeading.Text = LRS(60085) ' Select format for file from bank
                    ShowButtonCaptions(frmWiz10_FormatFromBank)

                    ' Fill array with formats, from format table
                    If oFilesetup.Enum_ID > 999 Then
                        frmWiz10_FormatFromBank.FillListbox((True))
                    Else
                        frmWiz10_FormatFromBank.FillListbox((False))
                    End If
                    frmWiz10_FormatFromBank.ShowDialog()


                Case 11
                    ' ------------------------------------------------
                    ' Filename from bank
                    ' ------------------------------------------------
                    ' We are still at the "not FromAccounting-part" of the filesetup
                    ' Goto the first "not FromAccounting-part"
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)

                    ' Fill array with formats, from format table (in case we haven't used wiz.10
                    aFormatsAndProfiles = LoadFormatsAndProfiles(False, True) ' 12.09.06 changed from False) 'False = not from accounting

                    If frmWiz11_FilenameFromBank Is Nothing Then
                        frmWiz11_FilenameFromBank = New frmWiz11_FilenameFromBank
                    End If
                    If frmMatch Is Nothing Then
                        frmMatch = New frmMatch
                    End If
                    If frmFilter Is Nothing Then
                        frmFilter = New frmFilter
                    End If

                    If frmSecurity Is Nothing Then
                        frmSecurity = New frmSecurity
                    End If
                    If frmSecureEnvelope Is Nothing Then
                        frmSecureEnvelope = New frmSecureEnvelope
                    End If

                    If bReturnProfileOnly Then
                        frmWiz11_FilenameFromBank.Text = LRS(60001) & " " & oFilesetup.ShortName
                    Else
                        frmWiz11_FilenameFromBank.Text = LRS(60001) & " " & oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend).ShortName
                    End If
                    frmWiz11_FilenameFromBank.lblHeading.Text = LRS(60086) 'Filenames from bank
                    frmWiz11_FilenameFromBank.lblDescr.Text = LRS(60087) ' Specify  ....
                    frmWiz11_FilenameFromBank.fraFromBank.Text = LRS(60088) ' Files from bank
                    frmWiz11_FilenameFromBank.chkBackup.Text = LRS(60091)
                    frmWiz11_FilenameFromBank.cmdSpecial.Text = LRS(60137)
                    frmWiz11_FilenameFromBank.lblBankname.Text = LRS(60069)

                    '05.09.2008 - removed the next If because of new functionality in frmMatch
                    'If oFilesetup.Enum_ID = 501 Or oFilesetup.Enum_ID = 29 Or oFilesetup.Enum_ID = 31 Then
                    ' CREMUL, MT940_Swift, DNB_TBUK_Incoming
                    '''''frmWiz11_FilenameFromBank.cmdMatch.Visible = True
                    frmWiz11_FilenameFromBank.cmdMatch.Text = LRS(55008) ' Advanced options
                    'End If

                    ShowButtonCaptions(frmWiz11_FilenameFromBank)

                    iWiz11Run = 0 ' 0-based
                    ' Find correct formatname;
                    For i = 0 To UBound(aFormatsAndProfiles, 2)
                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If aFormatsAndProfiles(cEnumID, i) = oFilesetup.Enum_ID Then

                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cNAME, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            frmWiz11_FilenameFromBank.lblDescr.Text = LRS(60002) & " " + aFormatsAndProfiles(cNAME, i) 'Velg filnavn for
                            ' Filename labels dependent on format:
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            iLRS = Val(aFormatsAndProfiles(cLabelFileimp1, i))
                            If iLRS = 0 Then
                                ' Hide filenamelabel and text if not to be used:
                                frmWiz11_FilenameFromBank.lblFilename1.Visible = False
                                frmWiz11_FilenameFromBank.txtFilename1.Visible = False
                                frmWiz11_FilenameFromBank.cmdFileOpen1.Visible = False
                            Else
                                frmWiz11_FilenameFromBank.lblFilename1.Text = LRS(iLRS)
                            End If
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            iLRS = Val(aFormatsAndProfiles(cLabelFileimp2, i))
                            If iLRS = 0 Then
                                ' Hide filenamelabel and text if not to be used:
                                frmWiz11_FilenameFromBank.lblFilename2.Visible = False
                                frmWiz11_FilenameFromBank.txtFilename2.Visible = False
                                frmWiz11_FilenameFromBank.cmdFileOpen2.Visible = False
                            Else
                                frmWiz11_FilenameFromBank.lblFilename2.Text = LRS(iLRS)
                            End If
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            iLRS = Val(aFormatsAndProfiles(cLabelFileimp3, i))
                            If iLRS = 0 Then
                                ' Hide filenamelabel and text if not to be used:
                                frmWiz11_FilenameFromBank.lblFilename3.Visible = False
                                frmWiz11_FilenameFromBank.txtFilename3.Visible = False
                                frmWiz11_FilenameFromBank.cmdFileOpen3.Visible = False
                            Else
                                frmWiz11_FilenameFromBank.lblFilename3.Text = LRS(iLRS)
                            End If

                            Exit For
                        End If
                    Next i

                    If oFilesetup.FileNameIn1 <> "" Then
                        frmWiz11_FilenameFromBank.txtFilename1.Text = Trim(oFilesetup.FileNameIn1)
                        frmWiz11_FilenameFromBank.txtFilename1.SelectionStart = 0
                        frmWiz11_FilenameFromBank.txtFilename1.SelectionLength = Len(Trim(oFilesetup.FileNameIn1))
                    End If
                    If oFilesetup.FileNameIn2 <> "" Then
                        frmWiz11_FilenameFromBank.txtFilename2.Text = Trim(oFilesetup.FileNameIn2)
                        frmWiz11_FilenameFromBank.txtFilename2.SelectionStart = 0
                        frmWiz11_FilenameFromBank.txtFilename2.SelectionLength = Len(Trim(oFilesetup.FileNameIn2))
                    End If
                    If oFilesetup.FileNameIn3 <> "" Then
                        frmWiz11_FilenameFromBank.txtFilename3.Text = Trim(oFilesetup.FileNameIn3)
                        frmWiz11_FilenameFromBank.txtFilename3.SelectionStart = 0
                        frmWiz11_FilenameFromBank.txtFilename3.SelectionLength = Len(Trim(oFilesetup.FileNameIn3))
                    End If

                    frmWiz11_FilenameFromBank.chkBackup.CheckState = bVal(oFilesetup.BackupIn)

                    ' fill combo with banknames:
                    aBankArray = VB6.CopyArray(FetchBanks)
                    frmWiz11_FilenameFromBank.cmbBankname.Items.Clear()
                    For i = 0 To UBound(aBankArray, 2)
                        frmWiz11_FilenameFromBank.cmbBankname.Items.Add((aBankArray(0, i)))
                        ' Mark previously selected
                        If CDbl(aBankArray(1, i)) = oFilesetup.Bank_ID Then
                            frmWiz11_FilenameFromBank.cmbBankname.SelectedIndex = i
                        End If
                    Next i
                    frmFilter.iFileSetup_ID = oFilesetup.FileSetup_ID
                    frmWiz11_FilenameFromBank.SetfrmFilter_11(frmFilter)
                    frmWiz11_FilenameFromBank.ShowDialog()

                Case 12
                    If bMATCHProfile Then
                        ' Use special filesetup form for matchprofile
                        ' CHANGE BACK TO 131 !!! iWizardStepNo = 131
                        iWizardStepNo = 131
                    Else
                        ' ------------------------------------------
                        ' Choose formats for returnprofile, to account
                        ' Used only when no sendprofile is involved
                        ' ------------------------------------------
                        ' We are now back to the "FromAccounting-part" of the filesetup
                        ' Goto the first "FromAccounting-part"
                        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)

                        If frmWiz12_FormatToAccount Is Nothing Then
                            frmWiz12_FormatToAccount = New frmWiz12_FormatToAccount
                        End If
                        If bReturnProfileOnly Then
                            If oFilesetup.FileSetupOut > 0 Then
                                frmWiz12_FormatToAccount.Text = LRS(60001) & " " & oProfile.FileSetups((oFilesetup.FileSetupOut)).ShortName
                            Else
                                frmWiz12_FormatToAccount.Text = LRS(60001)
                            End If
                        Else
                            ' no sendprofile
                            frmWiz12_FormatToAccount.Text = LRS(60001) & " " & oFilesetup.ShortName
                        End If
                        frmWiz12_FormatToAccount.lblHeading.Text = LRS(60092) 'Choose a format for file to accountingsystem
                        frmWiz12_FormatToAccount.lblFlere.Text = LRS(60062) ' No of times
                        frmWiz12_FormatToAccount.cmdFlere.Text = LRS(55006) 'Same format more than once
                        frmWiz12_FormatToAccount.cmdChooseProfile.Text = LRS(55007) 'Select format from another profile
                        ShowButtonCaptions(frmWiz12_FormatToAccount)


                        ' Fill array with formats, from format table
                        If oFilesetup.Enum_ID > 999 Then
                            frmWiz12_FormatToAccount.FillListbox((True))
                        Else
                            frmWiz12_FormatToAccount.FillListbox((False))
                        End If
                        frmWiz12_FormatToAccount.ShowDialog()
                    End If

                Case 13
                    If bMATCHProfile Then
                        ' Use special filesetup form for matchprofile
                        ' CHANGE BACK TO 131 !!! iWizardStepNo = 131
                        iWizardStepNo = 131
                    Else
                        ' ------------------------------------------------
                        ' Filename To Account
                        ' ------------------------------------------------
                        ' We are still at the "FromAccounting-part" of the filesetup
                        ' Goto the first "FromAccounting-part"
                        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                        If bReturnProfileOnly Then
                            ' no sendprofile
                            If oFilesetup.FileSetupOut > 0 Then
                                frmWiz12_FormatToAccount.Text = LRS(60001) & " " & oProfile.FileSetups((oFilesetup.FileSetupOut)).ShortName
                            Else
                                frmWiz12_FormatToAccount.Text = LRS(60001)
                            End If
                        Else
                            ' no sendprofile
                            frmWiz12_FormatToAccount.Text = LRS(60001) & " " & oFilesetup.ShortName
                        End If

                        If Not bReturnProfileOnly Then
                            ' Fill array with formats, from format table
                            aFormatsAndProfiles = LoadFormatsAndProfiles(True, False) 'true = from accountingsystem
                        End If

                        If frmWiz13_FilenameToAccount Is Nothing Then
                            frmWiz13_FilenameToAccount = New frmWiz13_FilenameToAccount
                        End If
                        frmAdvancedReturn = New frmAdvancedReturn

                        frmWiz13_FilenameToAccount.lblHeading.Text = LRS(60093) ' Filenames for returnfiles to accountingsystem
                        frmWiz13_FilenameToAccount.lblDescr.Text = LRS(60094) ' Filenames for files to account
                        frmWiz13_FilenameToAccount.fraToAccount.Text = LRS(60095) ' File to bank
                        frmWiz13_FilenameToAccount.chkBackup.Text = LRS(60066) '
                        frmWiz13_FilenameToAccount.chkOverwrite.Text = LRS(60067) '
                        frmWiz13_FilenameToAccount.chkWarning.Text = LRS(60068) '
                        frmWiz13_FilenameToAccount.chkRemove.Text = LRS(60144) '
                        frmWiz13_FilenameToAccount.cmdAdvanced.Text = LRS(55008) ' Advanced options
                        frmWiz13_FilenameToAccount.lblBankname.Text = LRS(60069)  ' added 11.01.2016
                        ShowButtonCaptions(frmWiz13_FilenameToAccount)

                        iWiz13Run = 0 ' 0-based

                        ' Find correct formatname;
                        For i = 0 To UBound(aFormatsAndProfiles, 2)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If aFormatsAndProfiles(cEnumID, i) = oFilesetup.Enum_ID Then

                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cNAME, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                frmWiz13_FilenameToAccount.lblDescr.Text = LRS(60002) & " " + aFormatsAndProfiles(cNAME, i) & vbCrLf & LRS(60094) 'Velg filnavn for

                                ' Filename labels dependent on format, and
                                '(new 15.11.02) if for Telepay filename have been used in Wiz11
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                iLRS = Val(aFormatsAndProfiles(cLabelFileexp1, i))
                                ' new 15.11.02:
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cNAME, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If iLRS = 0 Or (aFormatsAndProfiles(cNAME, i) = "Telepay" Or aFormatsAndProfiles(cNAME, i) = "Telepay2") And Len(Trim(frmWiz11_FilenameFromBank.txtFilename1.Text)) = 0 Then
                                    ' Hide filenamelabel and text if not to be used:
                                    frmWiz13_FilenameToAccount.lblFilename1.Visible = False
                                    frmWiz13_FilenameToAccount.txtFilename1.Visible = False
                                    frmWiz13_FilenameToAccount.cmdFileOpen1.Visible = False
                                Else

                                    frmWiz13_FilenameToAccount.lblFilename1.Text = LRS(iLRS)
                                    frmWiz13_FilenameToAccount.txtFilename1.Text = Trim(oFilesetup.FileNameOut1)
                                    frmWiz13_FilenameToAccount.txtFilename1.SelectionStart = 0
                                    frmWiz13_FilenameToAccount.txtFilename1.SelectionLength = Len(Trim(oFilesetup.FileNameOut1))
                                End If
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                iLRS = Val(aFormatsAndProfiles(cLabelFileexp2, i))
                                'If iLRS = 0 Then
                                ' new 15.11.02:
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cNAME, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If iLRS = 0 Or (aFormatsAndProfiles(cNAME, i) = "Telepay" Or aFormatsAndProfiles(cNAME, i) = "Telepay2") And Len(Trim(frmWiz11_FilenameFromBank.txtFilename2.Text)) = 0 Then
                                    ' Hide filenamelabel and text if not to be used:
                                    frmWiz13_FilenameToAccount.lblFilename2.Visible = False
                                    frmWiz13_FilenameToAccount.txtFilename2.Visible = False
                                    frmWiz13_FilenameToAccount.cmdFileOpen2.Visible = False
                                Else
                                    frmWiz13_FilenameToAccount.lblFilename2.Text = LRS(iLRS)
                                    frmWiz13_FilenameToAccount.txtFilename2.Text = Trim(oFilesetup.FileNameOut2)
                                    frmWiz13_FilenameToAccount.txtFilename2.SelectionStart = 0
                                    frmWiz13_FilenameToAccount.txtFilename2.SelectionLength = Len(Trim(oFilesetup.FileNameOut2))
                                End If
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                iLRS = Val(aFormatsAndProfiles(cLabelFileexp3, i))
                                'If iLRS = 0 Then
                                ' new 15.11.02:
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cNAME, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If iLRS = 0 Or (aFormatsAndProfiles(cNAME, i) = "Telepay" Or aFormatsAndProfiles(cNAME, i) = "Telepay2") And Len(Trim(frmWiz11_FilenameFromBank.txtFilename3.Text)) = 0 Then
                                    ' Hide filenamelabel and text if not to be used:
                                    frmWiz13_FilenameToAccount.lblFilename3.Visible = False
                                    frmWiz13_FilenameToAccount.txtFilename3.Visible = False
                                    frmWiz13_FilenameToAccount.cmdFileOpen3.Visible = False
                                Else
                                    frmWiz13_FilenameToAccount.lblFilename3.Text = LRS(iLRS)
                                    frmWiz13_FilenameToAccount.txtFilename3.Text = Trim(oFilesetup.FileNameOut3)
                                    frmWiz13_FilenameToAccount.txtFilename3.SelectionStart = 0
                                    frmWiz13_FilenameToAccount.txtFilename3.SelectionLength = Len(Trim(oFilesetup.FileNameOut3))
                                End If

                                Exit For
                            End If
                        Next i

                        ' XNET 14.09.2011
                        ' Added Bank (both label and combobox) to frmWiz13 - Must do likewise in .NET
                        ' fill combo with banknames:
                        aBankArray = FetchBanks()
                        frmWiz13_FilenameToAccount.cmbBankname.Items.Clear()
                        For i = 0 To UBound(aBankArray, 2)
                            frmWiz13_FilenameToAccount.cmbBankname.Items.Add(aBankArray(0, i))
                            ' Mark previously selected
                            If aBankArray(1, i) = oFilesetup.Bank_ID Then
                                frmWiz13_FilenameToAccount.cmbBankname.SelectedIndex = i
                            End If
                        Next i

                        frmWiz13_FilenameToAccount.chkBackup.CheckState = bVal(oFilesetup.BackupOut)
                        frmWiz13_FilenameToAccount.chkOverwrite.CheckState = bVal(oFilesetup.OverWriteOut)
                        frmWiz13_FilenameToAccount.chkWarning.CheckState = bVal(oFilesetup.WarningOut)
                        frmWiz13_FilenameToAccount.chkRemove.CheckState = bVal(oFilesetup.RemoveOldOut)
                        frmWiz13_FilenameToAccount.ShowDialog()
                    End If

                Case 131
                    ' ------------------------------------------------
                    ' Filename To Account for MATCHING
                    ' ------------------------------------------------
                    ' We are still at the "FromAccounting-part" of the filesetup
                    ' Goto the first "FromAccounting-part"
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                    ' Fill array with formats, from format table
                    'aFormatsAndProfiles = LoadFormatsAndProfiles(True, False)  'true = from accountingsystem
                    'ShowButtonCaptions(frmWiz13_FilenameMatch)
                    'With frmWiz13_FilenameMatch

                    '               ' Fill with formats
                    '               ReDim aFormatsAndProfiles(0, 0) ' empty array, in case we skip back and forth
                    '               ' Fill array with formats, from format table
                    '               aFormatsAndProfiles = LoadFormatsAndProfiles(True, True) 'from accounting,

                    '               .lstFormats.Items.Clear()
                    '               ' fill comboboxwith formats:
                    '               For i = 0 To UBound(aFormatsAndProfiles, 2)
                    '                   'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                   .lstFormats.Items.Add((aFormatsAndProfiles(0, i) + Space(100) + Str(aFormatsAndProfiles(1, i))))
                    '               Next i


                    '               ' Present in listbox already set filenames:
                    '               ' Fill .aFilenames with full path/filename, format, and type(s)
                    '               .lstFiles.Items.Clear()
                    '               If Len(Trim(oFilesetup.FileNameOut1)) > 0 Then
                    '                   Dim aFilenames(2, 0) As Object
                    '                   'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(0, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                   aFilenames(0, UBound(aFilenames, 2)) = oFilesetup.FileNameOut1
                    '                   For i = 0 To UBound(aFormatsAndProfiles, 2)
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(1, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       If aFormatsAndProfiles(1, i) = oFilesetup.FormatOutID1 Then
                    '                           ' Show name for format
                    '                           'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                           'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(1, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                           aFilenames(1, UBound(aFilenames, 2)) = aFormatsAndProfiles(0, i) & Space(100) & Str(aFormatsAndProfiles(1, i))
                    '                           Exit For
                    '                       End If
                    '                   Next i

                    '                   If oFilesetup.MATCHOutGL = 1 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = "0"
                    '                   End If
                    '                   If oFilesetup.MATCHOutOCR = 1 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "0"
                    '                   End If
                    '                   If oFilesetup.MATCHOutAutogiro = 1 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "0"
                    '                   End If
                    '                   If oFilesetup.MATCHOutRest = 1 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "0"
                    '                   End If
                    '               End If
                    '               If Len(Trim(oFilesetup.FileNameOut2)) > 0 Then
                    '                   ReDim Preserve aFilenames(2, 1)
                    '                   'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(0, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                   aFilenames(0, UBound(aFilenames, 2)) = oFilesetup.FileNameOut2
                    '                   For i = 0 To UBound(aFormatsAndProfiles, 2)
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(1, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       If aFormatsAndProfiles(1, i) = oFilesetup.FormatOutID2 Then
                    '                           ' Show name for format
                    '                           'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                           'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(1, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                           aFilenames(1, UBound(aFilenames, 2)) = aFormatsAndProfiles(0, i) & Space(100) & Str(aFormatsAndProfiles(1, i))
                    '                           Exit For
                    '                       End If
                    '                   Next i

                    '                   If oFilesetup.MATCHOutGL = 2 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = "0"
                    '                   End If
                    '                   If oFilesetup.MATCHOutOCR = 2 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "0"
                    '                   End If
                    '                   If oFilesetup.MATCHOutAutogiro = 2 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "0"
                    '                   End If
                    '                   If oFilesetup.MATCHOutRest = 2 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "0"
                    '                   End If
                    '               End If
                    '               If Len(Trim(oFilesetup.FileNameOut3)) > 0 Then
                    '                   ReDim Preserve aFilenames(2, 2)
                    '                   'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(0, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                   aFilenames(0, UBound(aFilenames, 2)) = oFilesetup.FileNameOut3
                    '                   For i = 0 To UBound(aFormatsAndProfiles, 2)
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(1, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       If aFormatsAndProfiles(1, i) = oFilesetup.FormatOutID3 Then
                    '                           ' Show name for format
                    '                           'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                           'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(1, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                           aFilenames(1, UBound(aFilenames, 2)) = aFormatsAndProfiles(0, i) & Space(100) & Str(aFormatsAndProfiles(1, i))
                    '                           Exit For
                    '                       End If
                    '                   Next i

                    '                   If oFilesetup.MATCHOutGL = 3 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = "0"
                    '                   End If
                    '                   If oFilesetup.MATCHOutOCR = 3 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "0"
                    '                   End If
                    '                   If oFilesetup.MATCHOutAutogiro = 3 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "0"
                    '                   End If
                    '                   If oFilesetup.MATCHOutRest = 3 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "0"
                    '                   End If
                    '               End If
                    '               If Len(Trim(oFilesetup.FilenameOut4)) > 0 Then
                    '                   ReDim Preserve aFilenames(2, 3)
                    '                   'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(0, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                   aFilenames(0, UBound(aFilenames, 2)) = oFilesetup.FilenameOut4
                    '                   For i = 0 To UBound(aFormatsAndProfiles, 2)
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(1, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       If aFormatsAndProfiles(1, i) = oFilesetup.FormatOutID4 Then
                    '                           ' Show name for format
                    '                           'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                           'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(1, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                           aFilenames(1, UBound(aFilenames, 2)) = aFormatsAndProfiles(0, i) & Space(100) & Str(aFormatsAndProfiles(1, i))
                    '                           Exit For
                    '                       End If
                    '                   Next i

                    '                   If oFilesetup.MATCHOutGL = 4 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = "0"
                    '                   End If
                    '                   If oFilesetup.MATCHOutOCR = 4 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "0"
                    '                   End If
                    '                   If oFilesetup.MATCHOutAutogiro = 4 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "0"
                    '                   End If
                    '                   If oFilesetup.MATCHOutRest = 4 Then
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "1"
                    '                   Else
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       'UPGRADE_WARNING: Couldn't resolve default property of object aFilenames(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                       aFilenames(2, UBound(aFilenames, 2)) = aFilenames(2, UBound(aFilenames, 2)) & "0"
                    '                   End If
                    '               End If

                    '               .LetFilenames(aFilenames)

                    '               frmWiz13_FilenameMatch.chkBackup.CheckState = bVal(oFilesetup.BackupOut)
                    '               frmWiz13_FilenameMatch.chkOverwrite.CheckState = bVal(oFilesetup.OverWriteOut)
                    '               frmWiz13_FilenameMatch.chkWarning.CheckState = bVal(oFilesetup.WarningOut)
                    '               frmWiz13_FilenameMatch.chkRemove.CheckState = bVal(oFilesetup.RemoveOldOut)

                    '               frmWiz13_FilenameMatch.ShowDialog()
                    'End With

                Case 14
                    '-----------------------------------------
                    ' Reports returnfiles
                    ' ----------------------------------------
                    ' At this stage we are back to the "not FromAccounting-part" of the filesetup
                    ' Goto the first "not FromAccounting-part"
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)

                    ' set all parameters in all frm's for Reportsetup, and show dialog
                    ReportSetupAndShow()
                    frmWiz_Report.ShowDialog()

                Case 15
                    ' ------------------------------------------------
                    ' Name for returnprofile
                    ' ------------------------------------------------
                    ' We are still at the "not FromAccounting-part" of the filesetup
                    ' Goto the first "not FromAccounting-part"
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)
                    If frmWiz15_NameReturn Is Nothing Then
                        frmWiz15_NameReturn = New frmWiz15_NameReturn
                    End If
                    frmWiz15_NameReturn.Text = LRS(60001) & " " & oFilesetup.ShortName
                    frmWiz15_NameReturn.lblHeading.Text = LRS(60099)
                    frmWiz15_NameReturn.lblProfilename.Text = LRS(60078)
                    frmWiz15_NameReturn.lblDescription.Text = LRS(60143)
                    frmWiz15_NameReturn.chkSilent.Text = LRS(60194)
                    frmWiz15_NameReturn.chkSilent.CheckState = bVal(oFilesetup.Silent)
                    frmWiz15_NameReturn.cmdSignalFile.Text = LRS(60544)
                    ShowButtonCaptions(frmWiz15_NameReturn)

                    If bNewProfile Then
                        If bReturnProfileOnly Then
                            frmWiz15_NameReturn.txtProfilename.Text = Strip(Left(frmWiz10_FormatFromBank.lstFormats.Text, 50), " ") & LRS(60004) 'Retur
                        Else
                            'frmWiz15_NameReturn.txtProfilename.Text = Strip(Left(frmWiz5_FormatToBank.lstFormats.Text, 50), " ") & LRS(60004) 'Retur
                            ' 15.07.2019
                            frmWiz15_NameReturn.txtProfilename.Text = Strip(Left(frmWiz5_FormatToBank.lstFormats.Items(frmWiz5_FormatToBank.lstFormats.SelectedIndex).itemstring, 50), " ") & LRS(60004) 'Retur
                        End If
                    Else
                        frmWiz15_NameReturn.txtProfilename.Text = oFilesetup.ShortName
                        frmWiz15_NameReturn.txtDescription.Text = Trim(oFilesetup.Description)
                    End If
                    frmWiz15_NameReturn.txtProfilename.SelectionStart = 0
                    frmWiz15_NameReturn.txtProfilename.SelectionLength = Len(Trim(oFilesetup.Description))


                    frmWiz15_NameReturn.ShowDialog()

                    'Case 90
                    '    ' From frmQuickProfiles
                    '    ' Save values from this form;
                    '    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                    '    oFilesetup.FileNameIn1 = frmQuickProfiles.txtInFilename1.Text
                    '    oFilesetup.BackupIn = CBool(frmQuickProfiles.chkBackup.CheckState)
                    '    oFilesetup.ShortName = frmQuickProfiles.txtProfilename.Text

                    '    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)
                    '    oFilesetup.ShortName = frmQuickProfiles.txtProfilename.Text & "2"
                    '    oFilesetup.FileNameOut1 = frmQuickProfiles.txtOutFilename1.Text
                    '    oFilesetup.OverWriteOut = CBool(frmQuickProfiles.chkOverwrite.CheckState)
                    '    oFilesetup.WarningOut = CBool(frmQuickProfiles.chkWarning.CheckState)
                    '    ' ok - finished
                    '    bStatus = True
                    '    Exit Do



                Case 99
                    ' added to set AutoMatch on "both" filesetups in a pair
                    iFilesetupOut_ID = oFilesetup.FileSetupOut

                    If oFilesetup.FromAccountingSystem Then
                        bAutoMatch = oProfile.FileSetups.Item(iFilesetupOut_ID).AutoMatch
                        oFilesetup.AutoMatch = bAutoMatch
                    Else
                        bAutoMatch = oFilesetup.AutoMatch
                        oProfile.FileSetups.Item(iFilesetupOut_ID).AutoMatch = bAutoMatch
                    End If

                    ' Check status on FileSetups
                    For Each oFilesetup In oProfile.FileSetups
                        If oFilesetup.Setup_InitSend = True And oFilesetup.Setup_Send = False Or oFilesetup.Setup_InitReturn = True And oFilesetup.Setup_Return = False Then
                            If frmWiz9_Continue.optEnd.Checked = True And oFilesetup.FromAccountingSystem = False Then
                                ' don't delete returnpart if user has skipped returnpart of setup!
                            Else
                                oFilesetup.Status = vbBabel.Profile.CollectionStatus.Deleted
                            End If
                        End If
                    Next oFilesetup


                    ' ok - finished
                    bStatus = True
                    Exit Do


                Case 0
                    ' user has canceled
                    Style = MsgBoxStyle.YesNo + MsgBoxStyle.Critical + MsgBoxStyle.DefaultButton2 ' Define buttons.
                    If MsgBox(LRS(60005), Style, "BabelBank") = MsgBoxResult.Yes Then 'Avslutt Wizard
                        Exit Do ' exit loop, clean ut, quit the app
                        bStatus = False
                    Else
                        iWizardStepNo = 1
                    End If

                Case Else
                    Exit Do

            End Select
        Loop

        ' unload all forms:
        'frmWiz1_Company.Close()
        'frmWiz2_WhichProfile.Close()
        'frmWiz3_FormatFromAccount.Close()
        'frmWiz4_FilenameAccount.Close()
        'frmWiz5_FormatToBank.Close()
        ''frmWiz6_FilenameToBank.Close()
        'frmWiz6_FilenameToBank = Nothing
        'frmWiz8_Name.Close()
        'frmWiz9_Continue.Close()
        'frmWiz10_FormatFromBank.Close()
        'frmWiz11_FilenameFromBank.Close()
        'frmWiz12_FormatToAccount.Close()
        'frmWiz13_FilenameToAccount.Close()
        'frmWiz15_NameReturn.Close()

        frmWiz1_Company = Nothing
        frmWiz2_WhichProfile = Nothing
        frmWiz3_FormatFromAccount = Nothing
        frmWiz4_FilenameAccount = Nothing
        frmWiz5_FormatToBank = Nothing
        frmWiz6_FilenameToBank = Nothing
        frmWiz8_Name = Nothing
        frmWiz9_Continue = Nothing
        frmWiz10_FormatFromBank = Nothing
        frmWiz11_FilenameFromBank = Nothing
        frmWiz12_FormatToAccount = Nothing
        frmWiz13_FilenameToAccount = Nothing
        frmWiz15_NameReturn = Nothing
        frmSpecial = Nothing
        frmFilter = Nothing
        frmWiz_Report = Nothing
        frmReportAdvanced = Nothing
        frmReportPrinter = Nothing
        frmAdvanced = Nothing
        frmSecurity = Nothing
        frmSecureEnvelope = Nothing

        If bStatus Then
            bStatus = CheckNoOfFormats() ' Check license for LimitFormats
        End If

        Main_Renamed = bStatus

        ' Save 5 last versions of profile database into db_ins, underneath path with BB_Database
        If bStatus Then
            '    If RunTime Then
            '        smydatabasepath = App.path
            '    Else
            '        smydatabasepath = "c:\projects\babelbank"
            '    End If
            If BB_UseSQLServer() Then
                bx = MakeACopyOfBBDB(Nothing, False, vbBabel.BabelFiles.BackupType.FromSetup, , oProfile.BackupPath)
            Else
                bx = MakeACopyOfBBDB(Nothing, False, vbBabel.BabelFiles.BackupType.FromSetup)
            End If

        End If


    End Function
    Public Sub WizardMove(ByRef iJump As Short)
        ' Jump back and forth, or cancel
        ' called from the different Wizards
        Dim i As Short
        Dim j As Short
        Dim iLRS As Integer
        Dim sText As String
        Dim nNoOfTimes As Short
        Dim bValid As Boolean ' is the current wizard OK ?
        Dim nElement As Short
        Dim bx As Boolean ' dummy
        Dim bReportOnly As Boolean
        Dim iNextReturn As Short
        Dim iNextSend As Short
        Dim iNoOfFormats As Short
        Dim iFormat_ID As Short
        Dim sDescription As String
        Dim bSilent As Boolean

        bValid = True


        ' First, save whatever you like from Wizard loosing focus:
        Select Case iWizardStepNo

            Case 1
                ' --------------------------------------
                ' Companyinfo
                ' --------------------------------------
                If iJump <> -99 Then
                    ' save companyinfo to collection
                    ' Error if ' is used, like Haakon VII's gt.
                    'oProfile.CompanyName = Replace(frmWiz1_Company.txtCompany.Text, "'", "")
                    'oProfile.CompanyAdr1 = Replace(frmWiz1_Company.txtAdress1.Text, "'", "")
                    'oProfile.CompanyAdr2 = Replace(frmWiz1_Company.txtAdress2.Text, "'", "")
                    'oProfile.CompanyAdr3 = Replace(frmWiz1_Company.txtAdress3.Text, "'", "")
                    'oProfile.CompanyZip = frmWiz1_Company.txtZip.Text
                    'oProfile.CompanyCity = Replace(frmWiz1_Company.txtCity.Text, "'", "")
                    'oProfile.CompanyNo = frmWiz1_Company.txtCompanyNo.Text
                    'oProfile.AdditionalNo = frmWiz1_Company.txtAdditionalNo.Text
                    'oProfile.BaseCurrency = frmWiz1_Company.txtBaseCurrency.Text
                    'oProfile.Backup = CBool(frmWiz1_Company.chkBackup.CheckState)
                    'oProfile.DelDays = CShort(frmWiz1_Company.txtDelDays.Text)
                    'oProfile.BackupPath = frmWiz1_Company.txtBackupPath.Text
                    'oProfile.MaxSizeDB = CShort(frmWiz1_Company.txtMaxSizeDB.Text)
                    'oProfile.DisableAdminMenus = CBool(frmWiz1_Company.chkDisableAdminMenus.CheckState)
                    'oProfile.LockBabelBank = CBool(frmWiz1_Company.chkLockBabelBank.CheckState)
                    'oProfile.Status = vbBabel.Profile.CollectionStatus.Changed

                    bValid = True
                    If bReturnProfileOnly Then
                        ' If calling profile is returnprofile, then jump to return ..
                        iJump = 9 ' goes to 10
                    Else
                        iJump = 1
                    End If
                End If

            Case 2
                ' --------------------------------------
                ' Send- or return-profile
                ' --------------------------------------
                bValid = True
                ' Check options send og returnprofile:
                If iJump = 1 Then
                    ' next
                    If frmWiz2_WhichProfile.optSend.Checked = False Then

                        If frmWiz2_WhichProfile.optMatch.Checked Then
                            bMATCHProfile = True
                        End If

                        ' do not create sendprofile
                        bReturnProfileOnly = True
                        ' Jump the first "not FromAccounting-part"
                        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)
                        oFilesetup.FromAccountingSystem = False ' return only

                        ' continue with return (10)
                        ' Returnprofile or MATCHprofile
                        iWizardStepNo = 9 'Jump to 10 (9+iJump=10)
                    End If
                ElseIf iJump = 90 Then
                    ' Comes from QuickProfiles
                    iWizardStepNo = 90
                    iJump = 0

                End If

            Case 3
                ' ------------------------------------------------
                ' Choose formats for sendprofile, from accounting
                ' ------------------------------------------------

                ' Validate:
                ' ---------
                ' Must select format:
                If iJump > 0 Then
                    ' --------
                    ' validate
                    ' --------
                    If frmWiz3_FormatFromAccount.lstFormats.SelectedItems.Count = 0 Then
                        MsgBox(LRS(60006), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) '("Du m� velge et format")
                        bValid = False
                    End If
                    If bValid Then
                        ' Which format/profile was Selected
                        'nElement = frmWiz3_FormatFromAccount.lstFormats.ListIndex
                        ' Update oProfile:
                        If frmWiz3_FormatFromAccount.lstFormats.SelectedItems.Count > 0 Then
                            'iFormat_ID = Val(Right(VB6.GetItemString(frmWiz3_FormatFromAccount.lstFormats, frmWiz3_FormatFromAccount.lstFormats.SelectedIndex), 4))
                            ' 15.07.2019
                            iFormat_ID = Val(Right(frmWiz3_FormatFromAccount.lstFormats.Text, 4))
                        End If
                        For nElement = 0 To UBound(aFormatsAndProfiles, 2)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cFormat_ID, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If aFormatsAndProfiles(cFormat_ID, nElement) = iFormat_ID Then
                                Exit For
                            End If
                        Next

                        'nElement = frmWiz3_FormatFromAccount.lstFormats.ListIndex
                        'oFilesetup is informat-record in FileSetup
                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cFormat_ID, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If oFilesetup.Format_ID <> aFormatsAndProfiles(cFormat_ID, nElement) Then

                            ' changed informat, update with defaults from selected format
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cFormat_ID, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.Format_ID = aFormatsAndProfiles(cFormat_ID, nElement)

                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cInfoNewClient, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.InfoNewClient = aFormatsAndProfiles(cInfoNewClient, nElement)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cKeepBatch, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.KeepBatch = aFormatsAndProfiles(cKeepBatch, nElement)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cCtrlNegative, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.CtrlNegative = aFormatsAndProfiles(cCtrlNegative, nElement)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cCtrlZip, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.CtrlZip = aFormatsAndProfiles(cCtrlZip, nElement)
                            'FIX:oFileSetup.ctrlaccount = aFormatsAndProfiles(6, nElement)
                            'FIX:oFileSetup.country = aFormatsAndProfiles(7, nElement)
                            If Not IsDBNull(aFormatsAndProfiles(cTextFileimp1, nElement)) Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cTextFileimp1, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                ' Changed 27.09.2011 to not have <profilename>2 listed in returnfiles list
                                If bNewProfile Then
                                    oFilesetup.FileNameIn1 = aFormatsAndProfiles(cTextFileimp1, nElement)
                                End If
                            End If
                            'Also update with filenames for returnpart of profile (are needed later, when creating returnprofile)
                            If Not IsDBNull(aFormatsAndProfiles(cTextFileexp1, nElement)) Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cTextFileexp1, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oFilesetup.FileNameOut1 = aFormatsAndProfiles(cTextFileexp1, nElement)
                            End If
                            If Not IsDBNull(aFormatsAndProfiles(cTextFileexp2, nElement)) Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cTextFileexp2, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oFilesetup.FileNameOut2 = aFormatsAndProfiles(cTextFileexp2, nElement)
                            End If
                            If Not IsDBNull(aFormatsAndProfiles(cTextFileexp3, nElement)) Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cTextFileexp3, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oFilesetup.FileNameOut3 = aFormatsAndProfiles(cTextFileexp3, nElement)
                            End If

                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.Enum_ID = aFormatsAndProfiles(cEnumID, nElement)
                            '' New after EDI-formats are added:
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cVersion, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.Version = aFormatsAndProfiles(cVersion, nElement)
                            If Not IsNothing(aFormatsAndProfiles(cReturnMass, nElement)) Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cReturnMass, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oFilesetup.ReturnMass = aFormatsAndProfiles(cReturnMass, nElement)
                            Else
                                oFilesetup.ReturnMass = True
                            End If
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cType_Renamed, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If aFormatsAndProfiles(cType_Renamed, nElement) = "EDI" Then
                                oFilesetup.EDIFormat = True
                            End If

                        End If
                    End If
                End If

            Case 4
                ' ------------------------------------------------
                ' Choose filename for import from accountingsystem
                ' ------------------------------------------------
                If iJump > 0 Then
                    ' --------
                    ' validate
                    ' --------
                    If IsNothing(frmWiz4_FilenameAccount.txtFromInternal.Text) Then
                        MsgBox(LRS(60007), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) '("Du m� velge et filnavn")
                        bValid = False
                    End If

                    If bValid Then
                        oFilesetup.FileNameIn1 = frmWiz4_FilenameAccount.txtFromInternal.Text
                        oFilesetup.BackupIn = CBool(frmWiz4_FilenameAccount.chkBackup.CheckState)
                        oFilesetup.Manual = CBool(frmWiz4_FilenameAccount.chkManual.CheckState)

                        '
                        'FEIL!Re Reporting: Must put ReportFlags to the "outpart of profile"
                        ' This is because we might need reporting both on returnfiles and
                        ' on sendfiles, and have only one "flag" in database for reporting
                        ' of each file
                        'oProfile.FileSetups(oFilesetup.FileSetupOut).ReportToPrint1 = CBool(frmWiz4_FilenameAccount.chkPrinter1.Value)
                        'oProfile.FileSetups(oFilesetup.FileSetupOut).ReportToScreen1 = CBool(frmWiz4_FilenameAccount.chkScreen1.Value)

                    End If
                End If

            Case 5
                ' ----------------------------------------
                ' Choose formats for sendprofile, to bank
                ' ----------------------------------------

                If iJump > 0 Then

                    ' Which formats/profiles was Selected
                    ' Loop through listbox:

                    If frmWiz5_FormatToBank.lstFormats.SelectedItems.Count = 0 Then
                        ' No need to select outformat if only for report;
                        If MsgBox(LRS(60104), MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Setup") = MsgBoxResult.Yes Then ' Profile for reporting only ?
                            iJump = 3 ' Skip rest of setup, go to namespesification (wizard 8)
                            oFilesetup.FileSetupOut = oFilesetup.FileSetup_ID
                        Else ' User chose No.
                            bValid = False
                        End If
                    End If

                    If bValid And iJump <> 3 Then
                        iNoOfFormats = 0

                        For i = 0 To frmWiz5_FormatToBank.lstFormats.Items.Count - 1
                            If frmWiz5_FormatToBank.lstFormats.GetSelected(i) = True Then
                                iNoOfFormats = iNoOfFormats + 1

                                ' Check if same format is used more than one time:
                                nNoOfTimes = 1
                                sText = frmWiz5_FormatToBank.lstFormats.Items(i).itemstring  ' 15.07.2019
                                If InStr(sText, "*") > 0 Then
                                    nNoOfTimes = Val(Mid(sText, InStr(sText, "*") + 1, 2))
                                End If

                                For j = 1 To nNoOfTimes

                                    'If Not IsNothing(VB6.GetItemData(frmWiz5_FormatToBank.lstFormats, i)) ThenC
                                    'If VB6.GetItemData(frmWiz5_FormatToBank.lstFormats, i) > 0 Then
                                    ' 15.07.2019
                                    If frmWiz5_FormatToBank.lstFormats.Items(i).itemdata > 0 Then
                                        ' itemdata holds filesetup_id
                                        ' no need to add if we had an outformat before
                                        'oFilesetup = oProfile.FileSetups(VB6.GetItemData(frmWiz5_FormatToBank.lstFormats, i))
                                        ' 15.07.2019
                                        oFilesetup = oProfile.FileSetups(frmWiz5_FormatToBank.lstFormats.Items(i).itemdata)
                                        oFilesetup.Status = vbBabel.Profile.CollectionStatus.Changed

                                    Else
                                        ' NEW profile
                                        ' do not add if we have been here before .. (user pressed previous)
                                        ' Changed 29.08.06, removed test. Caused problems when Previosu was pressed If iPreviousJump > -1 Then
                                        If iNoOfFormats > 1 Then
                                            ' First "fromAccounting=False"-part have already been added,
                                            ' when setup was started
                                            oFilesetup = oProfile.FileSetups.Add(CStr(oProfile.FileSetups.Count))

                                            '---- Test on status after wizard 6 - used only there!
                                            oFilesetup.Status = vbBabel.Profile.CollectionStatus.NewCol
                                            oFilesetup.FileSetup_ID = oProfile.FileSetups.Count
                                            oFilesetup.Bank_ID = 0
                                            oFilesetup.FromAccountingSystem = False
                                            ' Update backupin/out with backupflag from company
                                            oFilesetup.BackupIn = oProfile.Backup
                                            oFilesetup.BackupOut = oProfile.Backup
                                            oFilesetup.Setup_Return = True
                                            oFilesetup.Setup_NewReturn = True
                                            oFilesetup.Setup_InitReturn = False
                                            oProfile.FileSetups.Setup_CurrentReturn = oFilesetup.FileSetup_ID

                                        End If

                                        oFilesetup.Format_ID = aFormatsAndProfiles(cFormat_ID, i)
                                        ' Fill inn with format defaults:
                                        oFilesetup.InfoNewClient = aFormatsAndProfiles(cInfoNewClient, i)
                                        oFilesetup.KeepBatch = aFormatsAndProfiles(cKeepBatch, i)
                                        oFilesetup.CtrlNegative = aFormatsAndProfiles(cCtrlNegative, i)
                                        oFilesetup.CtrlZip = aFormatsAndProfiles(cCtrlZip, i)
                                        'fix:oFileSetup.ctrlaccount = aFormatsAndProfiles(6, i)
                                        ' NO Need: oFileSetup.country = aFormatsAndProfiles(7, i)
                                        ' Filenames:
                                        If Not IsDBNull(aFormatsAndProfiles(cTextFileexp1, i)) Then
                                            oFilesetup.FileNameOut1 = aFormatsAndProfiles(cTextFileexp1, i)
                                        End If
                                        'Also update with filenames for returnpart of profile (are needed later, when creating returnprofile)
                                        If Not IsDBNull(aFormatsAndProfiles(cTextFileimp1, i)) Then
                                            ' Changed 27.09.2011 to not have <profilename>2 listed in returnfiles list
                                            If bNewProfile Then
                                                oFilesetup.FileNameIn1 = aFormatsAndProfiles(cTextFileimp1, i)
                                            End If

                                        End If
                                        If Not IsDBNull(aFormatsAndProfiles(cTextFileimp2, i)) Then
                                            ' Changed 27.09.2011 to not have <profilename>2 listed in returnfiles list
                                            If bNewProfile Then
                                                oFilesetup.FileNameIn2 = aFormatsAndProfiles(cTextFileimp2, i)
                                            End If
                                        End If
                                        If Not IsDBNull(aFormatsAndProfiles(cTextFileimp3, i)) Then
                                            ' Changed 27.09.2011 to not have <profilename>2 listed in returnfiles list
                                            If bNewProfile Then
                                                oFilesetup.FileNameIn3 = aFormatsAndProfiles(cTextFileimp3, i)
                                            End If
                                        End If

                                        oFilesetup.Enum_ID = aFormatsAndProfiles(cEnumID, i)
                                        '' New after EDI-formats are added:
                                        oFilesetup.Version = aFormatsAndProfiles(cVersion, i)
                                        ''''''oFilesetup.ReturnMass = aFormatsAndProfiles(cReturnMass, i)
                                        If aFormatsAndProfiles(cType_Renamed, i) = "EDI" Then
                                            oFilesetup.EDIFormat = True
                                        End If



                                        If frmWiz5_FormatToBank.lstFormats.SelectedItems.Count > 1 Then
                                            oFilesetup.FileSetupOut = -1 ' if several outformats
                                        Else
                                            oFilesetup.FileSetupOut = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend).FileSetup_ID ' only one outformat
                                        End If

                                        'Else
                                        '    ' User pressed previous, dont add to filesetup
                                        '    ' Goto the first "not FromAccounting-part"
                                        '    Set oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)
                                        'End If

                                    End If

                                Next j
                            Else
                                ' Not selected

                            End If
                        Next i


                        If frmWiz5_FormatToBank.lstFormats.SelectedItems.Count > 1 Then
                            ' more than one outformat, several filesetups, mark with -1
                            oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend).FileSetupOut = -1
                        End If

                    End If 'bValid

                    ' If user has select Report Only, then skip Wiz 6, filenames;
                    If oFilesetup.Enum_ID = 999 Then
                        'iJump = iJump + 2
                        iJump = iJump + 1
                        '''''oProfile.FileSetups(oFilesetup.FileSetupOut).ReportToPrint1 = True
                    End If

                End If ' iJump > 0

            Case 6
                ' ------------------------------------------------
                ' Choose filename for export to bank
                ' ------------------------------------------------
                If iJump <> -1 And iJump <> -99 Then
                    Do
                        'FIX: REMOVE next line
                        'If iWiz6Run < UBound(aIndexOutFormats, 1) + 1 Then
                        ' run through all selected outformats (to bank)
                        ' update collections with new formats out;

                        '        If oFileSetup.Status = -9 Then
                        '            oFileSetup.Status = NewCol
                        '            oProfile.Status = NewCol
                        '        Else
                        '            oFileSetup.Status = Changed
                        '            oProfile.Status = Changed
                        '        End If

                        ' Update collections;
                        oFilesetup.FileNameOut1 = frmWiz6_FilenameToBank.txtToBank.Text
                        'FIX: Update database and collections: oFileSetup.Bankname = frmWiz6_FilenameToBank.cmbBankname.Text
                        oFilesetup.BackupOut = CBool(frmWiz6_FilenameToBank.chkBackup.CheckState)
                        oFilesetup.OverWriteOut = CBool(frmWiz6_FilenameToBank.chkOverwrite.CheckState)
                        oFilesetup.WarningOut = CBool(frmWiz6_FilenameToBank.chkWarning.CheckState)
                        oFilesetup.RemoveOldOut = CBool(frmWiz6_FilenameToBank.chkRemove.CheckState)

                        ' Save Path for accountsfile for DnB TBIW XML;
                        sText = Trim(frmWiz6_FilenameToBank.txtAccountsPath.Text)
                        ' Filepath only, not filename
                        ' Check if . in last part of filepath
                        sText = Trim(Mid(sText, InStrRev(sText, "\") + 1))

                        If InStr(sText, ".") > 0 Then
                            ' Assume filename as last part, remove!
                            sText = frmWiz6_FilenameToBank.txtAccountsPath.Text
                            sText = Left(sText, InStrRev(sText, "\") - 1)
                        Else
                            sText = frmWiz6_FilenameToBank.txtAccountsPath.Text
                        End If
                        oProfile.AccountsFile = sText



                        ' Save bankid:
                        j = 1000 ' Own defined banks from id 1000
                        ' Check if this one is used before:
                        For i = 0 To UBound(aBankArray, 2)
                            If aBankArray(0, i) = frmWiz6_FilenameToBank.cmbBankname.Text Then
                                j = CShort(aBankArray(1, i))
                                Exit For
                            End If
                            If CDbl(aBankArray(1, i)) >= j Then
                                j = CDbl(aBankArray(1, i)) + 1
                            End If
                        Next i
                        If i > UBound(aBankArray, 2) Then
                            ' no match, make new bankapperance;
                            If frmWiz6_FilenameToBank.cmbBankname.Text = "" Then
                                j = 0
                                frmWiz6_FilenameToBank.cmbBankname.Text = LRS(60008) 'Ingen bank"
                            End If
                            If j > 999 Then
                                bx = SaveNewBankToDatabase(j, (frmWiz6_FilenameToBank.cmbBankname.Text))
                            End If
                        End If
                        oFilesetup.Bank_ID = j

                        iWiz6Run = iWiz6Run + 1
                        ' keep on with wizard 6 until all outformats are handled
                        'FIX: REMOVE next line
                        'If iWiz6Run > UBound(aIndexOutFormats, 1) Then
                        'FIXED INSTEAD:
                        'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups.Setup_GoToNextReturn(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        iNextReturn = oProfile.FileSetups.Setup_GoToNextReturn()
                        If iNextReturn = -1 Then
                            iJump = 0
                            iWizardStepNo = 7
                            Exit Do
                        End If

                        'FIX: REMOVE next line:
                        'Set oFilesetup = oProfile.FileSetups(aOutFilesetups(1, iWiz6Run))
                        'FIXED INSTEAD
                        oFilesetup = oProfile.FileSetups(iNextReturn)
                        ' show wizard 6 for next outformat:
                        ' Find correct formatname;
                        For j = 0 To UBound(aFormatsAndProfiles, 2)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If aFormatsAndProfiles(cEnumID, i) = oFilesetup.Enum_ID Then

                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cNAME, j). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                frmWiz6_FilenameToBank.lblDescr.Text = LRS(60002) & " " + aFormatsAndProfiles(cNAME, j) '"Velg filnavn for "
                                ' Filename labels dependent on format:
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                iLRS = Val(aFormatsAndProfiles(cLabelFileexp1, j))
                                frmWiz6_FilenameToBank.lblToBank.Text = LRS(iLRS) ' FIlename

                                Exit For
                            End If
                        Next j
                        frmWiz6_FilenameToBank.txtToBank.Text = oFilesetup.FileNameOut1
                        frmWiz6_FilenameToBank.chkBackup.CheckState = bVal(oFilesetup.BackupOut)
                        frmWiz6_FilenameToBank.chkOverwrite.CheckState = bVal(oFilesetup.OverWriteOut)
                        frmWiz6_FilenameToBank.chkWarning.CheckState = bVal(oFilesetup.WarningOut)
                        frmWiz6_FilenameToBank.chkRemove.CheckState = bVal(oFilesetup.RemoveOldOut)
                        frmWiz6_FilenameToBank.ShowDialog()
                        'Else
                        '    Exit Do
                        'End If
                    Loop

                End If

            Case 7
                '-----------------------------------------
                ' Rapporter sendefiler
                ' ----------------------------------------
                frmWiz_Report.Close()
                frmWiz_Report = Nothing
                frmReportAdvanced = Nothing
                frmReportPrinter = Nothing

            Case 8
                ' ------------------------------------------------
                ' Give name to sendprofile
                ' ------------------------------------------------
                ' Save profilename sendprofile

                sSendProfileName = frmWiz8_Name.txtProfilename.Text
                sDescription = Trim(frmWiz8_Name.txtDescription.Text)
                bSilent = CBool(frmWiz8_Name.chkSilent.CheckState)


                If iJump > 0 Then
                    ' Test shortname, no spaces, not start with "RETUR, SEND, ALL"
                    If InStr(sSendProfileName, " ") > 0 Then
                        MsgBox(LRS(60102), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Illegal characters in profilename
                        bValid = False
                    End If
                    If bValid And InStr(sSendProfileName, "\") > 0 Then
                        MsgBox(LRS(60102), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Illegal characters in profilename
                        bValid = False
                    End If
                    If bValid And InStr(sSendProfileName, "/") > 0 Then
                        MsgBox(LRS(60102), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Illegal characters in profilename
                        bValid = False
                    End If


                    If UCase(Left(sSendProfileName, 5)) = "RETUR" Or UCase(Left(sSendProfileName, 4)) = "SEND" Or UCase(Left(sSendProfileName, 3)) = "ALL" Then
                        MsgBox(LRS(60103), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' RETUR, SEND, ALL not allowed in start of profilename
                        bValid = False
                    End If

                    ' Check for unique profilename:
                    For Each oFilesetup In oProfile.FileSetups
                        If UCase(sSendProfileName) = UCase(oFilesetup.ShortName) And oFilesetup.Setup_Send = False Then
                            MsgBox(LRS(60100), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) 'Must have unique profilename
                            bValid = False
                            Exit For
                        End If
                    Next oFilesetup

                    If bValid Then
                        ' Update all sendprofiles with profilename;
                        ' Reset "next-counter"
                        oProfile.FileSetups.Setup_GoToNextSend(0)
                        Do
                            'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups.Setup_GoToNextSend. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            iNextSend = oProfile.FileSetups.Setup_GoToNextSend
                            If iNextSend = -1 Then
                                Exit Do
                            End If
                            oFilesetup = oProfile.FileSetups(iNextSend)
                            oFilesetup.ShortName = sSendProfileName
                            oFilesetup.Description = sDescription
                            oFilesetup.Silent = bSilent

                        Loop
                    End If


                End If

            Case 9
                ' ------------------------------------------------
                ' Continue to returnprofile ?
                ' ------------------------------------------------
                If frmWiz9_Continue.optContinueReturnProfile.Checked = True Then
                    '-----------------------------------------
                    ' ok, continue as normal, to returnprofile
                    '-----------------------------------------
                    bSendProfileOnly = False

                ElseIf frmWiz9_Continue.optContinueSendProfile.Checked = True Then
                    '------------------------------------------------------
                    ' User has decided to run setup for another sendprofile
                    '------------------------------------------------------
                    iWizardStepNo = 3
                    iJump = 0
                    If bNewProfile Then
                        bSendProfileOnly = True
                    End If
                ElseIf frmWiz9_Continue.optEnd.Checked = True Then
                    '------------------------------------------------
                    ' User has decided to NOT continue to returnpart:
                    '------------------------------------------------

                    iJump = 99 'WizardStepNo = 99
                    If bNewProfile Then
                        bSendProfileOnly = True

                        ' Remove out-filenames from "in-part" of sendprofile
                        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                        oFilesetup.FileNameOut1 = ""
                        oFilesetup.FileNameOut2 = ""
                        oFilesetup.FileNameOut3 = ""

                        ' must give name to outpart of filesetup as well
                        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)
                        'Give same name as inpart, but add "2"
                        oFilesetup.ShortName = sSendProfileName & "2"
                        ' Remove in-filenames from "outpart" of sendprofile;
                        oFilesetup.FileNameIn1 = ""
                        oFilesetup.FileNameIn2 = ""
                        oFilesetup.FileNameIn3 = ""
                    Else
                        ' Keep returnprofile as is
                    End If
                End If

                ' ------------------------------------------------
                ' RETURNPROFILE form here
                ' ------------------------------------------------

            Case 10
                ' ------------------------------------------------
                ' Format for returnprofil,
                ' used when we have no sendprofile
                ' ------------------------------------------------

                ' Validate:
                ' ---------
                ' Must select format:
                If iJump > 0 Then
                    ' --------
                    ' validate
                    ' --------
                    If frmWiz10_FormatFromBank.lstFormats.SelectedItems.Count = 0 Then
                        MsgBox(LRS(60006), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) '("Please select a format")
                        bValid = False
                    End If

                    If bValid Then
                        ' Which format/profile was Selected
                        ' Update oProfile:
                        'nElement = frmWiz10_FormatFromBank.lstFormats.ListIndex
                        If frmWiz10_FormatFromBank.lstFormats.SelectedItems.Count > 0 Then
                            'iFormat_ID = Val(Right(VB6.GetItemString(frmWiz10_FormatFromBank.lstFormats, frmWiz10_FormatFromBank.lstFormats.SelectedIndex), 4))
                            ' 15.07.2019
                            iFormat_ID = Val(Right(frmWiz10_FormatFromBank.lstFormats.Text, 4))
                        End If
                        For nElement = 0 To UBound(aFormatsAndProfiles, 2) ' Var 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cFormat_ID, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If aFormatsAndProfiles(cFormat_ID, nElement) = iFormat_ID Then
                                Exit For
                            End If
                        Next

                        'oFilesetup is returnformat-record in FileSetup
                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cFormat_ID, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If oFilesetup.Format_ID <> aFormatsAndProfiles(cFormat_ID, nElement) Then

                            ' changed informat, update with defaults from selected format
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cFormat_ID, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.Format_ID = aFormatsAndProfiles(cFormat_ID, nElement)

                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cInfoNewClient, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.InfoNewClient = aFormatsAndProfiles(cInfoNewClient, nElement)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cKeepBatch, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.KeepBatch = aFormatsAndProfiles(cKeepBatch, nElement)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cCtrlNegative, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.CtrlNegative = aFormatsAndProfiles(cCtrlNegative, nElement)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cCtrlZip, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.CtrlZip = aFormatsAndProfiles(cCtrlZip, nElement)
                            'FIX:oFileSetup.ctrlaccount = aFormatsAndProfiles(6, nElement)
                            'FIX:oFileSetup.country = aFormatsAndProfiles(7, nElement)
                            If Not IsDBNull(aFormatsAndProfiles(cTextFileimp1, nElement)) Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cTextFileimp1, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oFilesetup.FileNameIn1 = aFormatsAndProfiles(cTextFileimp1, nElement)
                            End If
                            If Not IsDBNull(aFormatsAndProfiles(cTextFileimp2, nElement)) Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cTextFileimp2, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oFilesetup.FileNameIn2 = aFormatsAndProfiles(cTextFileimp2, nElement)
                            End If
                            If Not IsDBNull(aFormatsAndProfiles(cTextFileimp3, nElement)) Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cTextFileimp3, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oFilesetup.FileNameIn3 = aFormatsAndProfiles(cTextFileimp3, nElement)
                            End If
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.Enum_ID = aFormatsAndProfiles(cEnumID, nElement)
                            '' New after EDI-formats are added:
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cVersion, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            oFilesetup.Version = aFormatsAndProfiles(cVersion, nElement)
                            ''''''oFilesetup.ReturnMass = aFormatsAndProfiles(cReturnMass, nElement)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cType_Renamed, nElement). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If aFormatsAndProfiles(cType_Renamed, nElement) = "EDI" Then
                                oFilesetup.EDIFormat = True
                            End If

                            'If bReturnProfileOnly Then
                            ' fill into aindexOutFormats chosen format from bank!
                            '    aIndexOutFormats(0) = frmWiz10_FormatFromBank.lstFormats.ListIndex
                            'End If


                        End If
                    End If
                End If

            Case 11
                ' ------------------------------------------------
                ' Filenamefrom bank
                ' ------------------------------------------------

                If iJump <> -1 And iJump <> -99 Then
                    Do
                        'FIX: REMOVE next line
                        'If iWiz11Run < UBound(aIndexOutFormats, 1) + 1 Then


                        ' run through all outformats for sendprofile

                        ' Update collections for previous shom wizard11;
                        oFilesetup.FileNameIn1 = frmWiz11_FilenameFromBank.txtFilename1.Text
                        oFilesetup.FileNameIn2 = frmWiz11_FilenameFromBank.txtFilename2.Text
                        oFilesetup.FileNameIn3 = frmWiz11_FilenameFromBank.txtFilename3.Text
                        oFilesetup.BackupIn = CBool(frmWiz11_FilenameFromBank.chkBackup.CheckState)

                        ' Save bankid:
                        j = 1000 ' Own defined banks from id 1000
                        ' Check if this one is used before:
                        For i = 0 To UBound(aBankArray, 2)
                            If aBankArray(0, i) = frmWiz11_FilenameFromBank.cmbBankname.Text Then
                                j = CShort(aBankArray(1, i))
                                Exit For
                            End If
                            If CDbl(aBankArray(1, i)) >= j Then
                                j = CDbl(aBankArray(1, i)) + 1
                            End If
                        Next i
                        If i > UBound(aBankArray, 2) Then
                            ' no match, make new bankapperance;
                            If frmWiz11_FilenameFromBank.cmbBankname.Text = "" Then
                                j = 0
                                frmWiz11_FilenameFromBank.cmbBankname.Text = LRS(60008) 'Ingen bank"
                            End If
                            If j > 999 Then
                                bx = SaveNewBankToDatabase(j, (frmWiz11_FilenameFromBank.cmbBankname.Text))
                            End If
                        End If
                        oFilesetup.Bank_ID = j



                        iWiz11Run = iWiz11Run + 1

                        '        If Not bReturnProfileOnly Then
                        '        'FIX: Assumes only one outformat when sendprofile is involved
                        '        ' This might not be true!
                        '            iJump = 1
                        '            Exit Do
                        '        End If

                        'FIX: REMOVE next line
                        'If iWiz11Run > UBound(aIndexOutFormats, 1) Then
                        'FIXED INSTEAD
                        'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups.Setup_GoToNextReturn. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        iNextReturn = oProfile.FileSetups.Setup_GoToNextReturn
                        If iNextReturn = -1 Then
                            ' no more returnprofiles to handle, carry on to next wizard
                            iJump = 0
                            iWizardStepNo = 12
                            Exit Do
                        End If

                        ' keep on with wizard 10 until all bankformats are handled

                        ' find correct oFilesetup:
                        'FIX: REMOVE next line
                        'Set oFilesetup = oProfile.FileSetups(aOutFilesetups(1, iWiz11Run))
                        'FIXED INSTEAD
                        oFilesetup = oProfile.FileSetups(iNextReturn)

                        ' show wizard 10 for next outformat:
                        ' Find correct formatname;
                        For j = 0 To UBound(aFormatsAndProfiles, 2)
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If aFormatsAndProfiles(cEnumID, i) = oFilesetup.Enum_ID Then

                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cNAME, j). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                frmWiz11_FilenameFromBank.lblDescr.Text = LRS(60002) & " " + aFormatsAndProfiles(cNAME, j) '"Velg filnavn for "
                                Exit For
                            End If
                        Next j

                        If oFilesetup.FileNameIn1 = "" And InStr(oFilesetup.FileNameOut1, "...") = 0 Then
                            frmWiz11_FilenameFromBank.txtFilename1.Text = Left(oFilesetup.FileNameOut1, InStrRev(oFilesetup.FileNameOut1, "\"))
                        End If
                        frmWiz11_FilenameFromBank.chkBackup.CheckState = bVal(oFilesetup.BackupIn)
                        frmWiz11_FilenameFromBank.Text = LRS(60001) & " " & sReturnProfileName 'Profilnavn
                        frmWiz11_FilenameFromBank.ShowDialog()
                        'Else
                        '    Exit Do
                        'End If
                    Loop

                End If

            Case 12
                ' -----------------------------------------------
                ' Choose formats for returnprofile, to accounting
                ' Used only when no sendprofile is involved
                ' -----------------------------------------------
                ' Which formats/profiles was Selected
                ' Loop through listbox:
                'FIX REMOVE next line
                'ReDim aIndexOutReturnFormats(0)  ' index pointing to aFormatsAndProfiles

                If iJump > 0 Then
                    'If frmWiz12_FormatToAccount.lstFormats.SelCount = 0 Then
                    If oFilesetup.Format_ID = 999 Then
                        ' Test if user is allowed to skip outformat (report only profile)
                        bReportOnly = True
                        If frmWiz11_FilenameFromBank.txtFilename1.Text <> "" Then
                            ' filename1 given,
                        End If
                        If frmWiz11_FilenameFromBank.txtFilename2.Text <> "" Then
                            ' filename2 given,
                        End If
                        If frmWiz11_FilenameFromBank.txtFilename3.Text <> "" Then
                            ' filename3 given,
                        End If

                        '        If bReportOnly Then
                        '            'If bReturnProfileOnly Then
                        '             '   bOneFormatOnly = True
                        '            'End If
                        '            If MsgBox(LRS(60104), vbYesNo, "Setup") = vbYes Then  ' Profile for reporting only ?
                        '                iJump = 3 ' Skip rest of setup, go to namespesification (wizard 15)
                        '                oFilesetup.FileSetupOut = oFilesetup.Filesetup_ID
                        '            Else   ' User chose No.
                        '                bValid = False
                        '                MsgBox LRS(60006)  '("Du m� velge et format")
                        '            End If
                        '        Else
                        '            bValid = False
                        '            MsgBox LRS(60006)  '("Du m� velge et format")
                        '        End If

                    End If

                    If bValid And iJump <> 3 Then
                        For i = 0 To frmWiz12_FormatToAccount.lstFormats.Items.Count - 1
                            If frmWiz12_FormatToAccount.lstFormats.GetSelected(i) = True Then
                                ' Check if same format is used more than one time:
                                nNoOfTimes = 1
                                'sText = VB6.GetItemString(frmWiz12_FormatToAccount.lstFormats, i)
                                ' 15.07.2019
                                sText = frmWiz12_FormatToAccount.lstFormats.Items(i).itemstring
                                If InStr(sText, "*") > 0 Then
                                    nNoOfTimes = Val(Mid(sText, InStr(sText, "*") + 1, 2))
                                End If

                                For j = 1 To nNoOfTimes

                                    oFilesetup.Format_ID = aFormatsAndProfiles(cFormat_ID, i)

                                    If bNewProfile Then
                                        ' Fill in with format defaults:
                                        If Not IsDBNull(aFormatsAndProfiles(cTextFileexp1, i)) Then
                                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cTextFileexp1, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            oFilesetup.FileNameOut1 = aFormatsAndProfiles(cTextFileexp1, i)
                                        End If
                                        If Not IsDBNull(aFormatsAndProfiles(cTextFileexp2, i)) Then
                                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cTextFileexp2, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            oFilesetup.FileNameOut2 = aFormatsAndProfiles(cTextFileexp2, i)
                                        End If
                                        If Not IsDBNull(aFormatsAndProfiles(cTextFileexp3, i)) Then
                                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cTextFileexp3, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            oFilesetup.FileNameOut3 = aFormatsAndProfiles(cTextFileexp3, i)
                                        End If

                                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cInfoNewClient, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        oFilesetup.InfoNewClient = aFormatsAndProfiles(cInfoNewClient, i)
                                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cKeepBatch, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        oFilesetup.KeepBatch = aFormatsAndProfiles(cKeepBatch, i)
                                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cCtrlNegative, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        oFilesetup.CtrlNegative = aFormatsAndProfiles(cCtrlNegative, i)
                                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cCtrlZip, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        oFilesetup.CtrlZip = aFormatsAndProfiles(cCtrlZip, i)
                                        If Not IsNothing(aFormatsAndProfiles(cReturnMass, i)) Then
                                            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cReturnMass, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                            oFilesetup.ReturnMass = aFormatsAndProfiles(cReturnMass, i)
                                        Else
                                            oFilesetup.ReturnMass = True
                                        End If
                                        'fix:oFileSetup.ctrlaccount = aFormatsAndProfiles(6, i)
                                        ' NO Need: oFileSetup.country = aFormatsAndProfiles(7, i)
                                    End If
                                    oFilesetup.Enum_ID = aFormatsAndProfiles(cEnumID, i)
                                    '' New after EDI-formats are added:
                                    oFilesetup.Version = aFormatsAndProfiles(cVersion, i)
                                    If aFormatsAndProfiles(cType_Renamed, i) = "EDI" Then
                                        oFilesetup.EDIFormat = True
                                    End If

                                    If frmWiz12_FormatToAccount.lstFormats.SelectedItems.Count > 1 Then
                                        oFilesetup.FileSetupOut = -1 ' if several outformats
                                    Else
                                        'FIX: REMOVE next lines
                                        'oFilesetup.FileSetupOut = inFileSetup_ID ' only one outformat
                                        ' Set filesetupout to corresponding return-part (fromaccounting=false-part)
                                        'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups.Setup_GotoInitReturn. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        oFilesetup.FileSetupOut = oProfile.FileSetups.Setup_GotoInitReturn
                                    End If
                                    'Else
                                    '    ' User pressed previous, dont add to filesetup
                                    '    'FIX: REMOVE next lines
                                    '    'oFilesetup.FileSetupOut = inFileSetup_ID ' only one outformat
                                    '
                                    '    Set oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                                    '    oFilesetup.FileSetupOut = oProfile.FileSetups.Setup_GotoInitReturn
                                    'End If

                                    'End If

                                Next j
                            Else
                                ' Not selected
                                ' Test if this format was used in the calling returnprofile,
                                ' but is now unchecked by user:
                                'FIX: REMOVE next lines
                                'If frmWiz12_FormatToAccount.lstFormats.ItemData(i) <> 0 Then
                                ' aha, this format was used before.
                                ' adjust aOutReturnFilesetups:

                                'For j = 0 To UBound(aOutReturnFilesetups, 2)
                                '    If frmWiz12_FormatToAccount.lstFormats.ItemData(i) = aOutReturnFilesetups(0, j) Then
                                '        aOutReturnFilesetups(0, j) = -1
                                '        aOutReturnFilesetups(1, j) = -1
                                '    End If
                                'Next j
                                'End If

                            End If
                        Next i
                        ' possible deleted items in aOutReturnFilesetups
                        'aOutReturnFilesetups = aReorganize(aOutReturnFilesetups)


                    End If 'bValid
                    ' If user has select Report Only, then skip Wiz 12, filenames;
                    If oFilesetup.Enum_ID = 999 Then
                        'iJump = iJump + 2
                        iJump = iJump + 1
                    End If

                End If ' iJump > 0

            Case 13
                ' ------------------------------------------------
                ' Filename To Account
                ' ------------------------------------------------
                ' Update collections for previously shown wizard11;


                If bReturnProfileOnly Then
                    ' ------------------------
                    ' no sendprofile involved:
                    ' might be several formats
                    ' ------------------------

                    If iJump <> -1 And iJump <> -99 Then
                        Do

                            'FIX: REMOVE next line
                            'If iWiz13Run < UBound(aIndexOutReturnFormats, 1) + 1 Then
                            ' run through all selected outformats (to account)
                            ' update collections with new formats out;
                            'Set oFilesetup = oProfile.FileSetups(aOutReturnFilesetups(1, iWiz13Run))
                            'FIXED 03.10.2001
                            'Set oFilesetup = oProfile.FileSetups(oProfile.FileSetups(aOutReturnFilesetups(1, iWiz13Run)).FileSetupOut)

                            oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                            ' Uppdate collections;
                            oFilesetup.FileNameOut1 = frmWiz13_FilenameToAccount.txtFilename1.Text
                            oFilesetup.FileNameOut2 = frmWiz13_FilenameToAccount.txtFilename2.Text
                            oFilesetup.FileNameOut3 = frmWiz13_FilenameToAccount.txtFilename3.Text
                            oFilesetup.BackupOut = CBool(frmWiz13_FilenameToAccount.chkBackup.CheckState)
                            oFilesetup.OverWriteOut = CBool(frmWiz13_FilenameToAccount.chkOverwrite.CheckState)
                            oFilesetup.WarningOut = CBool(frmWiz13_FilenameToAccount.chkWarning.CheckState)
                            oFilesetup.RemoveOldOut = CBool(frmWiz13_FilenameToAccount.chkRemove.CheckState)
                            'oFilesetup.Bank_ID = 0
                            ' 04.04.05
                            ' Pick bank from "in-profile;
                            oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)
                            i = oFilesetup.Bank_ID
                            oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                            oFilesetup.Bank_ID = i

                            ' added 13.09.06
                            ' changed 03.07.2009 - may be set in frmAdvancedReturn for Telepay2
                            If oFilesetup.Enum_ID <> vbBabel.BabelFiles.FileType.Telepay2 Then
                                oFilesetup.KeepBatch = True ' Let it always be true for returnfiles
                            End If

                            ' XNET 14.09.2011 - added BankName
                            ' Save bankid:
                            j = 1000 ' Own defined banks from id 1000
                            ' Check if this one is used before:
                            For i = 0 To UBound(aBankArray, 2)
                                If aBankArray(0, i) = frmWiz13_FilenameToAccount.cmbBankname.Text Then
                                    j = aBankArray(1, i)
                                    Exit For
                                End If
                                If aBankArray(1, i) >= j Then
                                    j = aBankArray(1, i) + 1
                                End If
                            Next i
                            If i > UBound(aBankArray, 2) Then
                                ' no match, make new bankapperance;
                                If frmWiz13_FilenameToAccount.cmbBankname.Text = "" Then
                                    j = 0
                                    frmWiz13_FilenameToAccount.cmbBankname.Text = LRS(60008)  'Ingen bank"
                                End If
                                If j > 999 Then
                                    bx = SaveNewBankToDatabase(j, frmWiz13_FilenameToAccount.cmbBankname.Text)
                                End If
                            End If
                            oFilesetup.Bank_ID = j

                            iWiz13Run = iWiz13Run + 1

                            ' keep on with wizard 13 until all outformats are handled
                            'FIX: REMOVE next line
                            'If iWiz13Run > UBound(aIndexOutReturnFormats, 1) Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups.Setup_GoToNextSend. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            iNextSend = oProfile.FileSetups.Setup_GoToNextSend
                            If iNextSend = -1 Then
                                iJump = 0
                                iWizardStepNo = 14
                                Exit Do
                            End If

                            'FIX: REMOVE next line
                            'Set oFilesetup = oProfile.FileSetups(aOutReturnFilesetups(1, iWiz13Run))
                            'FIXED INSTEAD
                            oFilesetup = oProfile.FileSetups(iNextSend)

                            ' show wizard 13 for next outformat:
                            ' Find correct formatname;
                            For j = 0 To UBound(aFormatsAndProfiles, 2)
                                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If aFormatsAndProfiles(cEnumID, i) = oFilesetup.Enum_ID Then

                                    'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cNAME, j). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    frmWiz13_FilenameToAccount.lblDescr.Text = LRS(60002) & " " + aFormatsAndProfiles(cNAME, j) '"Velg filnavn for "


                                    ' Filename labels dependent on format:
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    iLRS = Val(aFormatsAndProfiles(cLabelFileexp1, j))
                                    If iLRS = 0 Then
                                        ' Hide filenamelabel and text if not to be used:
                                        frmWiz13_FilenameToAccount.lblFilename1.Visible = False
                                        frmWiz13_FilenameToAccount.txtFilename1.Visible = False
                                        frmWiz13_FilenameToAccount.cmdFileOpen1.Visible = False
                                    Else
                                        frmWiz13_FilenameToAccount.lblFilename1.Text = LRS(iLRS)
                                    End If
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    iLRS = Val(aFormatsAndProfiles(cLabelFileexp2, j))
                                    If iLRS = 0 Then
                                        ' Hide filenamelabel and text if not to be used:
                                        frmWiz13_FilenameToAccount.lblFilename2.Visible = False
                                        frmWiz13_FilenameToAccount.txtFilename2.Visible = False
                                        frmWiz13_FilenameToAccount.cmdFileOpen2.Visible = False
                                    Else
                                        frmWiz13_FilenameToAccount.lblFilename2.Text = LRS(iLRS)
                                    End If
                                    'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    iLRS = Val(aFormatsAndProfiles(cLabelFileexp3, j))
                                    If iLRS = 0 Then
                                        ' Hide filenamelabel and text if not to be used:
                                        frmWiz13_FilenameToAccount.lblFilename3.Visible = False
                                        frmWiz13_FilenameToAccount.txtFilename3.Visible = False
                                        frmWiz13_FilenameToAccount.cmdFileOpen3.Visible = False
                                    Else
                                        frmWiz13_FilenameToAccount.lblFilename3.Text = LRS(iLRS)
                                    End If


                                    Exit For
                                End If
                            Next j

                            frmWiz13_FilenameToAccount.txtFilename1.Text = oFilesetup.FileNameOut1
                            frmWiz13_FilenameToAccount.txtFilename2.Text = oFilesetup.FileNameOut2
                            frmWiz13_FilenameToAccount.txtFilename3.Text = oFilesetup.FileNameOut3

                            frmWiz13_FilenameToAccount.chkBackup.CheckState = bVal(oFilesetup.BackupOut)
                            frmWiz13_FilenameToAccount.chkOverwrite.CheckState = bVal(oFilesetup.OverWriteOut)
                            frmWiz13_FilenameToAccount.chkWarning.CheckState = bVal(oFilesetup.WarningOut)
                            frmWiz13_FilenameToAccount.chkRemove.CheckState = bVal(oFilesetup.RemoveOldOut)

                            frmWiz13_FilenameToAccount.ShowDialog()
                            'Else
                            '    Exit Do
                            'End If
                        Loop

                    End If



                Else
                    ' ---------------------
                    ' sendprofile involved
                    ' asumes only one format
                    ' ----------------------


                    oFilesetup.FileNameOut1 = frmWiz13_FilenameToAccount.txtFilename1.Text
                    oFilesetup.FileNameOut2 = frmWiz13_FilenameToAccount.txtFilename2.Text
                    oFilesetup.FileNameOut3 = frmWiz13_FilenameToAccount.txtFilename3.Text
                    oFilesetup.BackupOut = CBool(frmWiz13_FilenameToAccount.chkBackup.CheckState)
                    oFilesetup.OverWriteOut = CBool(frmWiz13_FilenameToAccount.chkOverwrite.CheckState)
                    oFilesetup.WarningOut = CBool(frmWiz13_FilenameToAccount.chkWarning.CheckState)
                    oFilesetup.RemoveOldOut = CBool(frmWiz13_FilenameToAccount.chkRemove.CheckState)
                    oFilesetup.Bank_ID = 0

                End If

            Case 131
                ' ------------------------------------------------
                ' Filename To Account for MATCHING
                ' ------------------------------------------------
                ' Update collections for previously shown wizard11;

                'If iJump <> -1 And iJump <> -99 Then

                '	aFilenames = VB6.CopyArray(frmWiz13_FilenameMatch.GetFilenames)

                '	oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)
                '	' Set FormatOutID1 on both "parts" of profile, to later detect that it is a MatchingProfile
                '	oFilesetup.FormatOutID1 = CShort(Trim(Right(aFilenames(1, 0), 3)))

                '	oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                '	' Update collections;
                '	oFilesetup.FileNameOut1 = aFilenames(0, 0)
                '	oFilesetup.FormatOutID1 = CShort(Trim(Right(aFilenames(1, 0), 3)))
                '	oFilesetup.Format_ID = CShort(Trim(Right(aFilenames(1, 0), 3)))

                '	oFilesetup.MATCHOutGL = CShort("0")
                '	oFilesetup.MATCHOutOCR = CShort("0")
                '	oFilesetup.MATCHOutAutogiro = CShort("0")
                '	oFilesetup.MATCHOutRest = CShort("0")

                '	' Fake Enum_ID, not used
                '	oFilesetup.Enum_ID = 900

                '	' Save which filetypes we are exporting for file 1:
                '	If Mid(aFilenames(2, 0), 1, 1) = "1" Then
                '		oFilesetup.MATCHOutGL = CShort("1")
                '	End If
                '	If Mid(aFilenames(2, 0), 2, 1) = "1" Then
                '		oFilesetup.MATCHOutOCR = CShort("1")
                '	End If
                '	If Mid(aFilenames(2, 0), 3, 1) = "1" Then
                '		oFilesetup.MATCHOutAutogiro = CShort("1")
                '	End If
                '	If Mid(aFilenames(2, 0), 4, 1) = "1" Then
                '		oFilesetup.MATCHOutRest = CShort("1")
                '	End If

                '	If UBound(aFilenames, 2) > 0 Then
                '		oFilesetup.FileNameOut2 = aFilenames(0, 1)
                '		oFilesetup.FormatOutID2 = CShort(Trim(Right(aFilenames(1, 1), 3)))
                '		' Save which filetypes we are exporting for file 2:
                '		If Mid(aFilenames(2, 1), 1, 1) = "1" Then
                '			oFilesetup.MATCHOutGL = CShort("2")
                '		End If
                '		If Mid(aFilenames(2, 1), 2, 1) = "1" Then
                '			oFilesetup.MATCHOutOCR = CShort("2")
                '		End If
                '		If Mid(aFilenames(2, 1), 3, 1) = "1" Then
                '			oFilesetup.MATCHOutAutogiro = CShort("2")
                '		End If
                '		If Mid(aFilenames(2, 1), 4, 1) = "1" Then
                '			oFilesetup.MATCHOutRest = CShort("2")
                '		End If
                '	Else
                '		oFilesetup.FileNameOut2 = ""
                '		oFilesetup.FormatOutID2 = CShort("0")
                '	End If
                '	If UBound(aFilenames, 2) > 1 Then
                '		oFilesetup.FileNameOut3 = aFilenames(0, 2)
                '		oFilesetup.FormatOutID3 = CShort(Trim(Right(aFilenames(1, 2), 3)))
                '		' Save which filetypes we are exporting for file 3:
                '		If Mid(aFilenames(2, 2), 1, 1) = "1" Then
                '			oFilesetup.MATCHOutGL = CShort("3")
                '		End If
                '		If Mid(aFilenames(2, 2), 2, 1) = "1" Then
                '			oFilesetup.MATCHOutOCR = CShort("3")
                '		End If
                '		If Mid(aFilenames(2, 2), 3, 1) = "1" Then
                '			oFilesetup.MATCHOutAutogiro = CShort("3")
                '		End If
                '		If Mid(aFilenames(2, 2), 4, 1) = "1" Then
                '			oFilesetup.MATCHOutRest = CShort("3")
                '		End If
                '	Else
                '		oFilesetup.FileNameOut3 = ""
                '		oFilesetup.FormatOutID3 = CShort("0")
                '	End If
                '	If UBound(aFilenames, 2) > 2 Then
                '		oFilesetup.FilenameOut4 = aFilenames(0, 3)
                '		oFilesetup.FormatOutID4 = CShort(Trim(Right(aFilenames(1, 3), 3)))
                '		' Save which filetypes we are exporting for file 4:
                '		If Mid(aFilenames(2, 3), 1, 1) = "1" Then
                '			oFilesetup.MATCHOutGL = CShort("4")
                '		End If
                '		If Mid(aFilenames(2, 3), 2, 1) = "1" Then
                '			oFilesetup.MATCHOutOCR = CShort("4")
                '		End If
                '		If Mid(aFilenames(2, 3), 3, 1) = "1" Then
                '			oFilesetup.MATCHOutAutogiro = CShort("4")
                '		End If
                '		If Mid(aFilenames(2, 3), 4, 1) = "1" Then
                '			oFilesetup.MATCHOutRest = CShort("4")
                '		End If
                '	Else
                '		oFilesetup.FilenameOut4 = ""
                '		oFilesetup.FormatOutID4 = CShort("0")
                '	End If

                '	oFilesetup.BackupOut = CBool(frmWiz13_FilenameMatch.chkBackup.CheckState)
                '	oFilesetup.OverWriteOut = CBool(frmWiz13_FilenameMatch.chkOverwrite.CheckState)
                '	oFilesetup.WarningOut = CBool(frmWiz13_FilenameMatch.chkWarning.CheckState)
                '	oFilesetup.RemoveOldOut = CBool(frmWiz13_FilenameMatch.chkRemove.CheckState)
                '	oFilesetup.Bank_ID = 0

                '	'iNextSend = oProfile.FileSetups.Setup_GoToNextSend
                '	'If iNextSend = -1 Then
                '	'    iJump = 0
                '	'    iWizardStepNo = 14
                '	'    'Exit Do
                '	'End If

                '	'Set oFilesetup = oProfile.FileSetups(iNextSend)
                'End If

            Case 14
                '-----------------------------------------
                ' Reports for returnfiles
                ' ----------------------------------------
                ' Update outFormats Returnprofile:
                'For i = 0 To UBound(aIndexOutReturnFormats, 1)

                'Set oFilesetup = oProfile.FileSetups(aOutReturnFilesetups(1, i))

                ' Update for all returnprofiles (FromAccounting=False-part)
                ' Reset "next-counter"
                oProfile.FileSetups.Setup_GoToNextReturn(0)
                frmWiz_Report.Close()
                frmWiz_Report = Nothing
                frmReportAdvanced = Nothing
                frmReportPrinter = Nothing

            Case 15
                ' ------------------------------------------------
                ' Name for returnprofile
                ' ------------------------------------------------
                ' Save profilename returnprofile
                sReturnProfileName = frmWiz15_NameReturn.txtProfilename.Text
                sDescription = Trim(frmWiz15_NameReturn.txtDescription.Text)
                bSilent = CBool(frmWiz15_NameReturn.chkSilent.CheckState)

                If iJump > 0 Then
                    ' Test shortname, no spaces, not start with "RETUR, SEND, ALL"
                    If InStr(sReturnProfileName, " ") > 0 Then
                        MsgBox(LRS(60102), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Illegal characters in profilename
                        bValid = False
                    End If
                    If bValid And InStr(sReturnProfileName, "\") > 0 Then
                        MsgBox(LRS(60102), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Illegal characters in profilename
                        bValid = False
                    End If
                    If bValid And InStr(sReturnProfileName, "/") > 0 Then
                        MsgBox(LRS(60102), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Illegal characters in profilename
                        bValid = False
                    End If

                    If UCase(Left(sReturnProfileName, 5)) = "RETUR" Or UCase(Left(sReturnProfileName, 4)) = "SEND" Or UCase(Left(sReturnProfileName, 3)) = "ALL" Then
                        MsgBox(LRS(60103), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' RETUR, SEND, ALL not allowed in start of profilename
                        bValid = False
                    End If

                    ' Check for unique profilename:
                    For Each oFilesetup In oProfile.FileSetups
                        If UCase(sReturnProfileName) = UCase(oFilesetup.ShortName) And oFilesetup.Setup_Return = False Then
                            MsgBox(LRS(60100), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) 'Must have unique profilename
                            bValid = False
                            Exit For
                        End If
                    Next oFilesetup


                    ' Update name for returnprofile(s)
                    If bValid Then
                        ' Reset "next-counter"
                        oProfile.FileSetups.Setup_GoToNextReturn(0)
                        i = 1
                        Do
                            'UPGRADE_WARNING: Couldn't resolve default property of object oProfile.FileSetups.Setup_GoToNextReturn. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            iNextReturn = oProfile.FileSetups.Setup_GoToNextReturn
                            If iNextReturn = -1 Then
                                Exit Do
                            End If
                            oFilesetup = oProfile.FileSetups(iNextReturn)
                            oFilesetup.ShortName = sReturnProfileName
                            oFilesetup.Description = sDescription
                            oFilesetup.Silent = bSilent
                            oFilesetup.FileSetupOut = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend).FileSetup_ID

                            If i = 1 Then
                                ' No need to update if sameformat (one record in FileSetup)
                                ' Changed 26.10.01 by JanP
                                '''''''''If Not (oProfile.FileSetups.Setup_SameFormat = True And bReturnProfileOnly = True) Then
                                ' must also update for "fromAccounting=True-part", but only once (i=1)
                                oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                                ' Update sendprofiles FileSetupOut;
                                ' If only on returnprofile, use Filesetup_ID, otherwise -1
                                If oProfile.FileSetups.Setup_NoOfReturns = 1 Then
                                    oFilesetup.FileSetupOut = oProfile.FileSetups(iNextReturn).FileSetup_ID
                                Else
                                    oFilesetup.FileSetupOut = -1
                                End If

                                If bReturnProfileOnly And oFilesetup.Setup_NewSend = True Then
                                    'Give same name as returnprofile, but add "2"
                                    oFilesetup.ShortName = sReturnProfileName & "2"
                                    ' Remove in-filenames from "inpart" of returnprofile;
                                    oFilesetup.FileNameIn1 = ""
                                    oFilesetup.FileNameIn2 = ""
                                    oFilesetup.FileNameIn3 = ""
                                End If
                                'End If
                            End If
                            i = i + 1
                        Loop
                    End If
                End If



        End Select

        If bValid Then
            ' If not valid, keep on with same wizard!

            ' Next, jump to the next/previous wizard
            iWizardStepNo = iWizardStepNo + iJump
            If iJump = -99 Then
                ' canceled by user
                iWizardStepNo = 0
                '...
                ' set status for Setup to -99, no save, reload old elements
            End If
            If iJump = 99 Then
                ' save sendprofile
                ' SaveSendprofile()
                '...
                ' set status for Setup to 99, save
                'Exit Sub 'End
                iWizardStepNo = 99
            End If
        End If

        ' If sendprofile involved, skip formats for returnfile
        ' (Wiz10 and 12)
        If Not bReturnProfileOnly Then
            ' If sendprofile involved, no need to ask for formats for returnprofile
            If iWizardStepNo = 10 Then
                iWizardStepNo = iWizardStepNo + iJump ' either next or previous wizard
            End If
            If iWizardStepNo = 12 Then
                iWizardStepNo = iWizardStepNo + IIf(iJump = -1, -1, 1) ' either next or previous wizard
            End If
        Else
            ' returnprofile only
            If iWizardStepNo = 9 And iJump = -1 Then
                ' skip all sendprofilestuff
                iWizardStepNo = 2
            End If
        End If
        iPreviousJump = iJump

    End Sub
    Function LoadFormatsAndProfiles(ByVal bFromAccountingSystem As Boolean, ByVal bIncludeSpecialFormats As Boolean, Optional ByRef DnBNORTBI As Boolean = False) As Object(,)
        'Dim ArrayFormat(,) As Object
        ' Fill listbox with formats, and profiles.
        ' bFromAccountingSystem = true if we only want formats type fromaccountingsystem
        ' sPaymentType = "in" for incoming payments, out for outgoing, empty for both
        ' bWithProfiles = true if we also want to show profiles in array

        Dim sPaymentType As String
        ReDim aFormatsAndProfiles(0, 0)

        sPaymentType = "" ' Not in use

        'UPGRADE_WARNING: Couldn't resolve default property of object ArrayFormat(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        aFormatsAndProfiles = ArrayFormat(bFromAccountingSystem, sPaymentType, bIncludeSpecialFormats, DnBNORTBI)
        'i = UBound(aFormatsAndProfiles, 2) + 1

        'If bWithProfiles Then
        '    ' Must also add sendprofiles to the same array, which will be uses in listbox
        '    For Each oFileSetup In oProfile.FileSetups
        '        If oFileSetup.FromAccountingSystem = bFromAccountingSystem Then
        '            ' redim preserve only on last element !
        '            ReDim Preserve aFormatsAndProfiles(13, i)
        '            aFormatsAndProfiles(0, i) = "Profil " + oFileSetup.ShortName
        '            aFormatsAndProfiles(1, i) = "Profil " + Str(oFileSetup.FileSetup_ID)
        '        End If
        '    Next
        'End If
        LoadFormatsAndProfiles = VB6.CopyArray(aFormatsAndProfiles)


    End Function
    'Sub SaveDatabase()
    ' 24.02.2003, call to this function is removed
    'oProfile.ConString = Trim$(frmDatabase.txtConnectionString.Text)
    'oProfile.ConUID = Trim$(frmDatabase.txtUserID.Text)
    'oProfile.ConPWD = pwCrypt(Trim$(frmDatabase.txtPassword.Text))
    'End Sub
    Sub SaveEMailSettings(ByVal frm As frmEmail)
        ' new 13.11.02
        ' added e-mail smtp functionality
        oProfile.EmailSMTP = frm.optSMTP.Checked
        oProfile.EmailSender = Trim(frm.txtSender.Text)
        oProfile.EmailDisplayName = Trim(frm.txtDisplayName.Text)
        oProfile.EmailAutoSender = Trim(frm.txtAutoSender.Text)
        oProfile.EmailAutoDisplayName = Trim(frm.txtAutoDisplayName.Text)
        oProfile.EmailAutoReceiver = Trim(frm.txtAutoReceiver.Text)
        oProfile.EmailReplyAdress = Trim(frm.txtReplyAdress.Text)
        oProfile.EmailSMTPHost = Trim(frm.txtSMTPHost.Text)
        ' New 13.05.03
        oProfile.EmailSupport = Trim(frm.txtSupport.Text)
        oProfile.EmailCCSupport = Trim(frm.txtCCSupport.Text)

    End Sub
    Sub LoadfrmSecurity()
        ' fill into controls in frmSecurity
        frmSecurity.txtfilenamePublicKey.Text = oFilesetup.secFileNamePublicKey
        frmSecurity.chkEncrypt.CheckState = bVal(oFilesetup.secEncrypt)
        frmSecurity.chkSign.CheckState = bVal(oFilesetup.secSign)
        frmSecurity.chkConfidential.CheckState = bVal(oFilesetup.secConfidential)
        frmSecurity.cmbCypher.SelectedText = oFilesetup.secCypher
        frmSecurity.cmbHash.SelectedText = oFilesetup.secHash
        frmSecurity.txtFilenameBankPublicKey.Text = oFilesetup.secFileNameBankPublicKey
        frmSecurity.cmbCompression.SelectedText = oFilesetup.secCompression
        frmSecurity.txtUserid.Text = oFilesetup.secUserID

        If Not EmptyString(oFilesetup.secKeyCreationDate) Then
            frmSecurity.lblKeyCreationDate.Text = LRS(60662) & " " & StringToDate(oFilesetup.secKeyCreationDate).ToString
        End If
        'frmSecurity.txtPassword.Text = oFilesetup.secPassword


        frmSecurity.cmbCypher.Items.Clear()
        frmSecurity.cmbCypher.Items.Add("AES_256")
        frmSecurity.cmbCypher.SelectedIndex = frmSecurity.cmbCypher.FindString(oFilesetup.secCypher)

        frmSecurity.cmbHash.Items.Clear()
        frmSecurity.cmbHash.Items.Add("SHA256")
        frmSecurity.cmbHash.SelectedIndex = frmSecurity.cmbHash.FindString(oFilesetup.secHash)

        frmSecurity.cmbCompression.Items.Clear()
        frmSecurity.cmbCompression.Items.Add("ZIP")
        frmSecurity.cmbCompression.Items.Add("UNCOMPRESSED")
        frmSecurity.cmbCompression.SelectedIndex = frmSecurity.cmbCompression.FindString(oFilesetup.secCompression)

        frmSecurity.PassFilesetupTofrmSecurity(oFilesetup)
        frmSecurity.ShowDialog()

    End Sub
    Sub LoadfrmSecureEnvelope()
        ' fill into controls in frmSecureEnvelope
        frmSecureEnvelope.chkSecureEnvelope.CheckState = bVal(oFilesetup.secSecureEnvelope)
        If Not EmptyString(oFilesetup.secCompression) Then
            frmSecureEnvelope.chkGZIP.CheckState = 1  'bVal(oFilesetup.secCompression)
        End If
        frmSecureEnvelope.txtSenderID.Text = oFilesetup.secUserID
        frmSecureEnvelope.PassFilesetupTofrmSecurity(oFilesetup)
        frmSecureEnvelope.txtfilenamePublicKey.Text = oFilesetup.secFileNamePublicKey
        frmSecureEnvelope.txtPassword.Text = BBDecrypt(oFilesetup.secPassword, 8197)
        frmSecureEnvelope.ShowDialog()

    End Sub
    Sub LoadfrmSecurityReturn()
        ' fill into controls in frmSecurity
        ' must search to see if we have a Filesetup with securitysetup from a sendprofile.
        ' if so, use that one.
        ' ONLY ONE "PAIR" OF SECURITYSETUP ALLOWED SO FAR. Mut have a menu to be able to have several.
        Dim oFileSetupContra As vbBabel.FileSetup
        Dim oFS As vbBabel.FileSetup


        For Each oFS In oProfile.FileSetups
            If oFS.secEncrypt = True Or oFS.secSign = True Or oFS.secSecureEnvelope = True Then
                oFileSetupContra = oFS
                Exit For
            End If
        Next

        If Not oFileSetupContra Is Nothing Then
            frmSecurity.txtfilenamePublicKey.Text = oFileSetupContra.secFileNamePublicKey
            frmSecurity.chkEncrypt.CheckState = bVal(oFileSetupContra.secEncrypt)
            frmSecurity.chkSign.CheckState = bVal(oFileSetupContra.secSign)
            frmSecurity.chkConfidential.CheckState = bVal(oFileSetupContra.secConfidential)
            frmSecurity.cmbCypher.SelectedText = oFileSetupContra.secCypher
            frmSecurity.cmbHash.SelectedText = oFileSetupContra.secHash
            frmSecurity.txtFilenameBankPublicKey.Text = oFileSetupContra.secFileNameBankPublicKey
            frmSecurity.cmbCompression.SelectedText = oFileSetupContra.secCompression
            frmSecurity.txtUserid.Text = oFileSetupContra.secUserID

            If Not EmptyString(oFileSetupContra.secKeyCreationDate) Then
                frmSecurity.lblKeyCreationDate.Text = LRS(60662) & " " & StringToDate(oFileSetupContra.secKeyCreationDate).ToString
            End If
            'frmSecurity.txtPassword.Text = oFilesetupContra.secPassword
        Else
            ' no Sendprofile with crypting - use current profile
            If Not oFilesetup Is Nothing Then
                frmSecurity.txtfilenamePublicKey.Text = oFilesetup.secFileNamePublicKey
                frmSecurity.chkEncrypt.CheckState = bVal(oFilesetup.secEncrypt)
                frmSecurity.chkSign.CheckState = bVal(oFilesetup.secSign)
                frmSecurity.chkConfidential.CheckState = bVal(oFilesetup.secConfidential)
                frmSecurity.cmbCypher.SelectedText = oFilesetup.secCypher
                frmSecurity.cmbHash.SelectedText = oFilesetup.secHash
                frmSecurity.txtFilenameBankPublicKey.Text = oFilesetup.secFileNameBankPublicKey
                frmSecurity.cmbCompression.SelectedText = oFilesetup.secCompression
                frmSecurity.txtUserid.Text = oFilesetup.secUserID
                oFileSetupContra = oFilesetup
            End If

        End If

        frmSecurity.cmbCypher.Items.Clear()
        frmSecurity.cmbCypher.Items.Add("AES_256")

        frmSecurity.cmbHash.Items.Clear()
        frmSecurity.cmbHash.Items.Add("SHA256")

        frmSecurity.cmbCompression.Items.Clear()
        frmSecurity.cmbCompression.Items.Add("ZIP")
        frmSecurity.cmbCompression.Items.Add("UNCOMPRESSED")

        frmSecurity.PassFilesetupTofrmSecurity(oFileSetupContra)
        frmSecurity.IsReturnProfile(True)

        If Not oFileSetupContra Is Nothing Then
            frmSecurity.cmbCypher.SelectedIndex = frmSecurity.cmbCypher.FindString(oFileSetupContra.secCypher)
            frmSecurity.cmbHash.SelectedIndex = frmSecurity.cmbHash.FindString(oFileSetupContra.secHash)
            frmSecurity.cmbCompression.SelectedIndex = frmSecurity.cmbCompression.FindString(oFileSetupContra.secCompression)
        End If
        frmSecurity.ShowDialog()

    End Sub
    Sub LoadFormatAdvanced()
        Dim bx As Boolean

        ' From Wizard6_FilenameBank or ...
        ' Show form with advanced settings. Use formatdefaults as defaults
        ''''frmAdvanced = New frmAdvanced

        frmAdvanced.chkAccumulation.Text = LRS(60233)
        frmAdvanced.chkNegativeAmount.Text = LRS(60234)
        frmAdvanced.chkNoCity.Text = LRS(60235)
        frmAdvanced.chkAccount.Text = LRS(60236)
        frmAdvanced.chkDateDue.Text = LRS(60237)
        frmAdvanced.chkDateAll.Text = LRS(60238)
        frmAdvanced.chkKeepClients.Text = LRS(60239)
        frmAdvanced.chkRejectWrong.Text = LRS(60240)
        frmAdvanced.chkRedoWrong.Text = LRS(60500)
        frmAdvanced.lblCompanyNo.Text = LRS(60241)
        frmAdvanced.chkCompanyNoOnClient.Text = LRS(60242)
        frmAdvanced.lblAdditionalNo.Text = LRS(60243)
        frmAdvanced.lblDivision.Text = LRS(60244)
        'LUCKNOW
        frmAdvanced.cmdNBCodes.Text = LRS(60245)
        frmAdvanced.CmdCancel.Text = LRS(55002)
        frmAdvanced.lblBackupPath.Text = LRS(60047) 'Backuppath:

        'VENT frmAdvanced.Caption
        'frmAdvanced.chkAccumulation.Caption
        'frmAdvanced.chkNegativeAmount.Caption
        'frmAdvanced.chkNoCity.Caption
        'frmAdvanced.chkAccount.Caption
        'frmAdvanced.chkDateDue.Caption
        'frmAdvanced.chkDateAll.Caption
        'frmAdvanced.chkKeepClients.Caption
        'frmAdvanced.chkRejectWrong.Caption

        'frmAdvanced.cmdStandard.Caption

        'frmAdvanced.lblAdditionalNo.Caption
        'frmAdvanced.lblCompanyNo.Caption
        'frmAdvanced.lblDivision.Caption

        frmAdvanced.Frame1.Visible = False ' 04.05.06 For TBI XML only

        ' Enable/disable dependent of Format:
        Select Case oFilesetup.Enum_ID

            Case 1 'OCR

            Case 2
            Case 4, 8, 33 ' Telepay og TBIO, Telepay2
                ' FromAccountingSystem

                frmAdvanced.chkDateDue.Enabled = True
                frmAdvanced.chkDateAll.Enabled = True
                frmAdvanced.chkNegativeAmount.Enabled = True
                frmAdvanced.chkNoCity.Enabled = True
                frmAdvanced.chkAccount.Enabled = True
                frmAdvanced.chkKeepClients.Enabled = True
                frmAdvanced.chkRejectWrong.Enabled = True
                frmAdvanced.chkRedoWrong.Enabled = True
                frmAdvanced.lblCompanyNo.Enabled = True
                frmAdvanced.txtCompanyNo.Enabled = True
                'frmAdvanced.chkCompanyNoOnClient.Enabled = True
                frmAdvanced.lblDivision.Enabled = True
                frmAdvanced.txtDivision.Enabled = True


                frmAdvanced.chkDateDue.CheckState = bVal(oFilesetup.ChangeDateDue) ' changed 04.05.06
                frmAdvanced.chkDateAll.CheckState = bVal(oFilesetup.ChangeDateAll) ' changed 04.05.06
                frmAdvanced.chkNegativeAmount.CheckState = bVal(oFilesetup.CtrlNegative)
                frmAdvanced.chkNoCity.CheckState = bVal(oFilesetup.CtrlZip)
                frmAdvanced.chkRejectWrong.CheckState = bVal(oFilesetup.RejectWrong)
                frmAdvanced.chkRedoWrong.CheckState = bVal(oFilesetup.RedoWrong)
                frmAdvanced.chkAccount.CheckState = bVal(oFilesetup.CtrlAccount)
                frmAdvanced.chkKeepClients.CheckState = bVal(oFilesetup.KeepBatch)
                If oFilesetup.CompanyNo <> "" Then
                    frmAdvanced.txtCompanyNo.Text = oFilesetup.CompanyNo
                Else
                    frmAdvanced.txtCompanyNo.Text = oProfile.CompanyNo
                End If
                frmAdvanced.cmdNBCodes.Visible = True
                '    frmAdvanced.chkCompanyNoOnClient.Value = bVal(oFilesetup.CompanyNoOnClient)
                '    If frmAdvanced.chkCompanyNoOnClient.Value = True Then
                '        frmAdvanced.txtCompanyNo.Enabled = False
                '    End If

                frmAdvanced.txtDivision.Text = oFilesetup.Division
                bx = frmAdvanced.SetSeqNoType((oFilesetup.SeqnoType))

                If oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.Telepay2 Then
                    frmAdvanced.cmdStructuredText.Visible = True
                End If
            Case 5
            Case 6
                ' XNET 30.10.2012 - added avtalegiro
            Case 7, vbBabel.BabelFiles.FileType.Dirrem, vbBabel.BabelFiles.FileType.Avtalegiro '18.11.2009 added Dirrem
                ' Autogiro Engangs
                ' FromAccountingSystem

                frmAdvanced.lblAdditionalNo.Enabled = True
                frmAdvanced.txtAdditionalNo.Enabled = True
                If oFilesetup.AdditionalNo <> "" Then
                    frmAdvanced.txtAdditionalNo.Text = oFilesetup.AdditionalNo
                Else
                    frmAdvanced.txtCompanyNo.Text = oProfile.AdditionalNo
                End If

                'Case 8  ' Telepay TBIO
                '    ' Open AdditionalNo, use as PayersAccount, where accountno has more than 11 digits
                '    frmAdvanced.lblAdditionalNo.Enabled = True
                '    frmAdvanced.lblAdditionalNo.Caption = LRS(60036)  ' Account
                '    frmAdvanced.txtAdditionalNo.Enabled = True
                '    frmAdvanced.txtAdditionalNo.Text = oFilesetup.AdditionalNo

            Case 9 'Erhvervsgiro
                frmAdvanced.lblCompanyNo.Enabled = False
                frmAdvanced.lblDivision.Enabled = False
                frmAdvanced.txtCompanyNo.Enabled = False
                frmAdvanced.txtDivision.Enabled = False
                frmAdvanced.lblAdditionalNo.Enabled = True
                frmAdvanced.txtAdditionalNo.Enabled = True
                frmAdvanced.lblAdditionalNo.Text = LRS(60142)
                frmAdvanced.txtAdditionalNo.Text = oFilesetup.AdditionalNo

            Case 15
                ' DnB TBIW XML-format;
                frmAdvanced.Frame1.Visible = True ' 04.05.06 For TBI XML only
                frmAdvanced.chkKeepClients.Text = LRS(60289) ' One file pr paymenttype
                frmAdvanced.chkKeepClients.Enabled = True

                frmAdvanced.Frame1.Enabled = True
                frmAdvanced.optNone.Enabled = True
                frmAdvanced.optErrorsOnly.Enabled = True
                frmAdvanced.optErrorsWarnings.Enabled = True
                frmAdvanced.chkKeepClients.CheckState = bVal(oFilesetup.KeepBatch)

            Case 54
                ' Bankdata 4.5 DK (SEB Danmark)
                frmAdvanced.chkSplitAfter450.CheckState = bVal(oFilesetup.SplitAfter450)
                frmAdvanced.chkSplitAfter450.Visible = True
                frmAdvanced.chkSplitAfter450.Text = LRS(60495, "999") ' Max. %1 payments in file

                'XNET 08.11.2010
            Case vbBabel.BabelFiles.FileType.GiroDirekt_Plusgiro
                frmAdvanced.lblCompanyNo.Enabled = True
                frmAdvanced.txtCompanyNo.Enabled = True

                'XNET 30.11.2010
            Case vbBabel.BabelFiles.FileType.DTAZV
                frmAdvanced.lblCompanyNo.Text = LRS(60554)
                frmAdvanced.lblCompanyNo.Enabled = True
                frmAdvanced.txtCompanyNo.Enabled = True
                frmAdvanced.lblAdditionalNo.Text = LRS(60555)
                frmAdvanced.lblAdditionalNo.Enabled = True
                frmAdvanced.txtAdditionalNo.Enabled = True
                ' XNET 09.12.2010 added next 5 lines because Ice had missed them
                If oFilesetup.CompanyNo <> "" Then
                    frmAdvanced.txtCompanyNo.Text = oFilesetup.CompanyNo
                Else
                    frmAdvanced.txtCompanyNo.Text = oProfile.CompanyNo
                End If
                ' XokNET 03.03.2011 added AdditionalNo
                frmAdvanced.txtAdditionalNo.Text = oFilesetup.AdditionalNo

            Case 502, 10 'PAYMUL og Leverant�rsbetalingar
                ' �pne for CompanyNo
                frmAdvanced.lblCompanyNo.Enabled = True
                frmAdvanced.txtCompanyNo.Enabled = True
                If oFilesetup.CompanyNo <> "" Then
                    frmAdvanced.txtCompanyNo.Text = oFilesetup.CompanyNo
                Else
                    frmAdvanced.txtCompanyNo.Text = oProfile.CompanyNo
                End If
                If oFilesetup.AdditionalNo <> "" Then
                    frmAdvanced.txtAdditionalNo.Text = oFilesetup.AdditionalNo
                Else
                    frmAdvanced.txtCompanyNo.Text = oProfile.AdditionalNo
                End If


            Case 1011 'eComerce (sannsynligvis ikke n�dvendig)
                frmAdvanced.lblCompanyNo.Enabled = False
                frmAdvanced.lblDivision.Enabled = False
                frmAdvanced.txtCompanyNo.Enabled = False
                frmAdvanced.txtDivision.Enabled = False
                frmAdvanced.lblAdditionalNo.Enabled = True
                frmAdvanced.txtAdditionalNo.Enabled = True
                frmAdvanced.lblAdditionalNo.Text = LRS(60142)
                frmAdvanced.txtAdditionalNo.Text = oFilesetup.AdditionalNo

            Case vbBabel.BabelFiles.FileType.Pain001
                frmAdvanced.lblCompanyNo.Enabled = True
                frmAdvanced.txtCompanyNo.Enabled = True
                frmAdvanced.lblDivision.Enabled = True
                frmAdvanced.txtDivision.Enabled = True
                '03.09.2018 - Added this
                frmAdvanced.lblAdditionalNo.Enabled = True
                frmAdvanced.lblAdditionalNo.Text = LRS(60684)
                frmAdvanced.txtAdditionalNo.Enabled = False
                frmAdvanced.txtAdditionalNo.Visible = False
                frmAdvanced.cmbEncoding.Items.Clear()
                frmAdvanced.cmbEncoding.Items.Add(LRS(60685))
                frmAdvanced.cmbEncoding.Items.Add("ASCII")
                frmAdvanced.cmbEncoding.Items.Add(LRS(60686))
                frmAdvanced.cmbEncoding.Items.Add(LRS(60687))
                frmAdvanced.cmbEncoding.SelectedIndex = oFilesetup.FileEncoding
                frmAdvanced.cmbEncoding.Location = New System.Drawing.Point(280, 350) ' Me.txtNotification.Location
                frmAdvanced.cmbEncoding.Visible = True
                frmAdvanced.cmbEncoding.Enabled = True
                'frmAdvanced.txtAdditionalNo.Bounds.Location.X = 280


                If oFilesetup.CompanyNo <> "" Then
                    frmAdvanced.txtCompanyNo.Text = oFilesetup.CompanyNo
                Else
                    frmAdvanced.txtCompanyNo.Text = oProfile.CompanyNo
                End If
                frmAdvanced.txtDivision.Text = oFilesetup.Division

        End Select

        ' Added 05.12.06
        frmAdvanced.txtBackupPath.Text = oFilesetup.BackupPath

        frmAdvanced.ShowDialog() ', Me

    End Sub
    Sub LoadFormatAdvancedReturn()
        ' Show form with advanced settings for returnfiles. Use formatdefaults as defaults

        frmAdvancedReturn.chkRejectWrong.Text = LRS(60240)
        frmAdvancedReturn.lblCompanyNo.Text = LRS(60241)
        frmAdvancedReturn.chkCompanyNoOnClient.Text = LRS(60242)
        frmAdvancedReturn.CmdCancel.Text = LRS(55002)
        frmAdvancedReturn.chkReturnMass.Text = LRS(60246)
        frmAdvancedReturn.lblAdditionalNo.Text = LRS(60243)
        frmAdvancedReturn.lblDelDays.Text = LRS(60046) 'Delete after no of days:
        frmAdvancedReturn.lblBackupPath.Text = LRS(60047) 'Backuppath:
        ' added 01.02.07 - changed 30.03.2007 from frmAdvanced to frmAdvancedReturn
        frmAdvancedReturn.chkNegativeAmount.Text = LRS(60234)
        ' added 01.02.07
        frmAdvancedReturn.chkExtractFromKID.Text = LRS(60503)
        'added 03.07.2009
        frmAdvancedReturn.chkKeepClients.Text = LRS(60239)

        frmAdvancedReturn.chkRejectWrong.Enabled = True
        frmAdvancedReturn.chkReturnMass.Enabled = True

        frmAdvancedReturn.chkRejectWrong.CheckState = bVal(oFilesetup.RejectWrong)
        frmAdvancedReturn.chkReturnMass.CheckState = bVal(oFilesetup.ReturnMass)

        ' added 01.02.07
        ' Enable/disable dependent of Format:
        Select Case oFilesetup.Enum_ID
            Case 1, 1061, vbBabel.BabelFiles.FileType.SG_SaldoFil_Kunde, vbBabel.BabelFiles.FileType.SG_FakturaHistorikkFil_Kunde 'OCR, SGClientReporting (1061)
                frmAdvancedReturn.chkNegativeAmount.Enabled = True
        End Select

        ' 03.07.2009 changed from Format_id to Enum_id
        If oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.OCR Then
            frmAdvancedReturn.chkReturnMass.Visible = False
            frmAdvancedReturn.chkReturnMass.Enabled = False
            With frmAdvancedReturn.chkSplitFBO
                .TabIndex = frmAdvancedReturn.chkReturnMass.TabIndex
                .Left = VB6.TwipsToPixelsX(2655)
                .Top = VB6.TwipsToPixelsY(1515)
                .Width = VB6.TwipsToPixelsX(6375)
                .Text = LRS(60468)
                .CheckState = bVal(oFilesetup.SplitOCRandFBO)
                .Visible = True
                .Enabled = True
            End With

            ' added 03.07.2009
        ElseIf oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.Telepay2 Then
            frmAdvancedReturn.chkKeepClients.Enabled = True

        End If

        If oFilesetup.CompanyNo <> "" Then
            frmAdvancedReturn.txtCompanyNo.Text = oFilesetup.CompanyNo
        Else
            frmAdvancedReturn.txtCompanyNo.Text = oProfile.CompanyNo
        End If
        'frmAdvanced.chkCompanyNoOnClient.Value = bVal(oFilesetup.CompanyNoOnClient)
        'If frmAdvanced.chkCompanyNoOnClient.Value = True Then
        '    frmAdvanced.txtCompanyNo.Enabled = False
        'End If
        If oFilesetup.DelDays > 0 Then
            frmAdvancedReturn.txtDelDays.Text = CStr(oFilesetup.DelDays)
        End If
        ' added 01.02.07
        frmAdvancedReturn.chkNegativeAmount.CheckState = bVal(oFilesetup.CtrlNegative)

        frmAdvancedReturn.chkExtractFromKID.CheckState = bVal(oFilesetup.ExtractInfofromKID)

        ' Added 05.12.06
        frmAdvancedReturn.txtBackupPath.Text = oFilesetup.BackupPath

        ' added 03.07.2009
        frmAdvancedReturn.chkKeepClients.CheckState = bVal(oFilesetup.KeepBatch)

        ' XNET 10.02.2011 - added next If - Don't know why this one was not there in the first place !
        If oFilesetup.AdditionalNo <> "" Then
            frmAdvancedReturn.txtAdditionalNo.Text = oFilesetup.AdditionalNo
        Else
            '16.12.2020 - The old code has to be wrong.
            'Old code
            'frmAdvancedReturn.txtCompanyNo.Text = oProfile.AdditionalNo
            'New code
            frmAdvancedReturn.txtAdditionalNo.Text = oProfile.AdditionalNo
        End If

        frmAdvancedReturn.ShowDialog() ', Me

    End Sub
    Sub SavefrmSecurity(ByVal oFS As vbBabel.FileSetup)
        ' 05.01.2017
        ' Save values from frmSecurity

        Dim ks As DidiSoft.Pgp.KeyStore = New DidiSoft.Pgp.KeyStore("local.keystore", "VB00998776878787")  ' random
        Dim key1 As DidiSoft.Pgp.KeyPairInformation

        oFS.secFileNamePublicKey = frmSecurity.txtfilenamePublicKey.Text.Trim
        oFS.secHash = frmSecurity.cmbHash.SelectedItem.ToString.Trim
        oFS.secCypher = frmSecurity.cmbCypher.SelectedItem.ToString.Trim
        oFS.secEncrypt = CBool(frmSecurity.chkEncrypt.CheckState)
        oFS.secSign = CBool(frmSecurity.chkSign.CheckState)
        oFS.secconfidential = CBool(frmSecurity.chkConfidential.CheckState)
        oFS.secFileNameBankPublicKey = frmSecurity.txtFilenameBankPublicKey.Text.Trim
        If frmSecurity.cmbCompression.SelectedIndex > -1 Then
            oFS.secCompression = frmSecurity.cmbCompression.SelectedItem.ToString.Trim
        End If
        ' save password
        oFS.secPassword = BBEncrypt(frmSecurity.txtPassword.Text, 8197)

        If frmSecurity.ImportBankPublicKey Then
            ' For PGP crypting or signing
            ' Import banks public key to BabelBank?
            key1 = ks.ImportPublicKey(frmSecurity.txtFilenameBankPublicKey.Text)
            ' put BANK public key into filesetup
            oFS.secBANKPublicKey = ks.ExportPublicKeyAsString("")
        End If

    End Sub
    Sub SavefrmSecureEnvelope(ByVal oFS As vbBabel.FileSetup)

        oFS.secUserID = frmSecureEnvelope.txtSenderID.Text
        oFS.secSecureEnvelope = CBool(frmSecureEnvelope.chkSecureEnvelope.CheckState)
        'oFS.secCompression = CBool(frmSecureEnvelope.chkGZIP.CheckState)
        If frmSecureEnvelope.chkGZIP.CheckState Then
            oFS.secCompression = "GZIP"
        Else
            oFS.secCompression = ""
        End If
        oFS.secFileNamePublicKey = frmSecureEnvelope.txtfilenamePublicKey.Text.Trim
        oFS.secPassword = BBEncrypt(frmSecureEnvelope.txtPassword.Text, 8197)

    End Sub
    Sub SaveFormatAdvanced()
        ' Update from frmAdvanced:
        oFilesetup.ChangeDateDue = CBool(frmAdvanced.chkDateDue.CheckState)
        oFilesetup.ChangeDateAll = CBool(frmAdvanced.chkDateAll.CheckState)
        oFilesetup.CtrlNegative = CBool(frmAdvanced.chkNegativeAmount.CheckState)
        oFilesetup.CtrlZip = CBool(frmAdvanced.chkNoCity.CheckState)
        oFilesetup.RejectWrong = CBool(frmAdvanced.chkRejectWrong.CheckState)
        oFilesetup.RedoWrong = CBool(frmAdvanced.chkRedoWrong.CheckState)
        oFilesetup.CtrlAccount = CBool(frmAdvanced.chkAccount.CheckState)
        oFilesetup.KeepBatch = CBool(frmAdvanced.chkKeepClients.CheckState)
        oFilesetup.Division = UCase(frmAdvanced.txtDivision.Text)
        oFilesetup.CompanyNo = UCase(frmAdvanced.txtCompanyNo.Text)
        'oFilesetup.CompanyNoOnClient = CBool(frmAdvanced.chkCompanyNoOnClient.Value)
        oFilesetup.AdditionalNo = UCase(frmAdvanced.txtAdditionalNo.Text)
        If frmAdvanced.optNone.Checked = True Then
            oFilesetup.ShowCorrections = 0
        ElseIf frmAdvanced.optErrorsOnly.Checked = True Then
            oFilesetup.ShowCorrections = 1
        Else
            oFilesetup.ShowCorrections = 2
        End If
        oFilesetup.SplitAfter450 = CBool(frmAdvanced.chkSplitAfter450.CheckState)
        'added 05.12.06
        oFilesetup.BackupPath = Trim(frmAdvanced.txtBackupPath.Text)
        'Added 03.09.2018

        oFilesetup.FileEncoding = frmAdvanced.cmbEncoding.SelectedIndex

    End Sub

    Sub SaveFormatAdvancedReturn()

        ' Update from frmAdvanced:
        oFilesetup.RejectWrong = CBool(frmAdvancedReturn.chkRejectWrong.CheckState)
        oFilesetup.ReturnMass = CBool(frmAdvancedReturn.chkReturnMass.CheckState)
        oFilesetup.CompanyNo = UCase(frmAdvancedReturn.txtCompanyNo.Text)
        'oFilesetup.CompanyNoOnClient = CBool(frmAdvanced.chkCompanyNoOnClient.Value)
        oFilesetup.AdditionalNo = UCase(frmAdvancedReturn.txtAdditionalNo.Text)
        oFilesetup.DelDays = Val(frmAdvancedReturn.txtDelDays.Text)
        oFilesetup.SplitOCRandFBO = CBool(frmAdvancedReturn.chkSplitFBO.CheckState)
        ' added 01.02.07
        oFilesetup.CtrlNegative = CBool(frmAdvancedReturn.chkNegativeAmount.CheckState)
        'Added 30.03.2007
        oFilesetup.ExtractInfofromKID = CBool(frmAdvancedReturn.chkExtractFromKID.CheckState)
        ' added 03.07.2009
        oFilesetup.KeepBatch = CBool(frmAdvancedReturn.chkKeepClients.CheckState)

        'added 05.12.06
        oFilesetup.BackupPath = Trim(frmAdvancedReturn.txtBackupPath.Text)

    End Sub

    'Sub UseFormatStandardSettings()
    '' Enable/disable dependent of Format:
    'Select Case aFormatsAndProfiles(cEnumID, aIndexOutFormats(iWiz6Run)) 'enum_id
    'Case 1  'OCR
    '        frmAdvanced.chkAccount.Value = 0
    'Case 2
    'Case 3
    'Case 4 ' Telepay
    'If aFormatsAndProfiles(cFromAccountingSystem, aIndexOutFormats(iWiz6Run)) = True Then
    '    ' FromAccountingSystem
    '    frmAdvanced.chkDateDue.Value = 0
    '    frmAdvanced.chkDateAll.Value = 0
    '    frmAdvanced.chkNegativeAmount.Value = 1
    '    frmAdvanced.chkNoCity.Value = 1
    '    frmAdvanced.chkAccount.Value = 1
    '    frmAdvanced.chkKeepClients.Value = 1
    '    frmAdvanced.chkRejectWrong.Value = 0
    '    frmAdvanced.txtCompanyNo.Text = ""
    '    frmAdvanced.txtDivision.Text = ""
    'Else
    '    ' Return
    '
    'End If
    'Case 5
    'Case 6
    'Case 7
    'Case 8
    'Case 501  'Cremul fra BBS
    '        frmAdvanced.chkAccount.Value = 0
    '
    'End Select
    '
    'End Sub
    Public Sub SetupSequence()
        '------------------------------------------------------------
        ' Change sequenceNo
        '------------------------------------------------------------
        Dim oSequenceSetup As Object
        Dim bSequenceSetup As Boolean

        oSequenceSetup = New Sequence

        oSequenceSetup.Profile = oProfile
        oSequenceSetup.Filesetup_ID = oFilesetup.FileSetup_ID
        bSequenceSetup = oSequenceSetup.Start()


        'If bSequenceSetup = True Then
        '   Set oProfile = oSequenceSetup.Profile


        '  oProfile.Save (1)

        'Else
        ' Run new load to make sure no errors are carried on
        '   oProfile.Load (1)
        'End If


    End Sub

    Function FindOutFormats(Optional ByRef lReturnProfile As Boolean = False) As Short()
        ' FIX: Remove this function !!!!
        ' Find from Clients-collection which Outformats correspond to current oFilesetup
        Dim oClient As Object 'Client
        Dim aOutFilesetups(,) As Integer
        'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
        'DOTNETT REDIM Dim aOutFilesetups() As Integer
        Dim iFormatOut As Short
        Dim iFormat_ID As Short
        Dim i0Old, i0, i, i1, i1Old As Short
        Dim iCounter As Short
        Dim bFound As Boolean

        iCounter = -1
        ReDim aOutFilesetups(1, 0)

        ' If only one outformat, FilesetupOut holds the format, use this one then:
        If oFilesetup.FileSetupOut <> -1 Then
            If lReturnProfile Then
                'UPGRADE_WARNING: Couldn't resolve default property of object aOutFilesetups(1, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aOutFilesetups(1, 0) = oFilesetup.FileSetup_ID
                ' Find Format_ID for outformat:
                'UPGRADE_WARNING: Couldn't resolve default property of object aOutFilesetups(0, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aOutFilesetups(0, 0) = oProfile.FileSetups((oFilesetup.FileSetupOut)).Format_ID

            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object aOutFilesetups(1, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aOutFilesetups(1, 0) = oFilesetup.FileSetupOut
                ' Find Format_ID for outformat:
                'UPGRADE_WARNING: Couldn't resolve default property of object aOutFilesetups(0, 0). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aOutFilesetups(0, 0) = oProfile.FileSetups(aOutFilesetups(1, 0)).Format_ID

            End If

        Else

            For Each oClient In oFilesetup.Clients
                'UPGRADE_WARNING: Couldn't resolve default property of object oClient.FormatOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                iFormatOut = oClient.FormatOut
                'UPGRADE_WARNING: Couldn't resolve default property of object oClient.Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If oClient.Status <> vbBabel.Profile.CollectionStatus.Deleted Then

                    If iCounter > 0 Then
                        ' Search array to check if we have this format:
                        For i = 0 To UBound(aOutFilesetups, 1) - 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object aOutFilesetups(1, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If iFormatOut = aOutFilesetups(1, i) Then
                                bFound = True
                                Exit For
                            End If
                        Next i
                    Else
                        bFound = False
                    End If


                    If Not bFound Then
                        ' Find Format_ID for outformat:
                        iFormat_ID = oProfile.FileSetups(iFormatOut).Format_ID

                        If UBound(aOutFilesetups, 1) > 0 Then
                            iCounter = iCounter + 1
                            ReDim Preserve aOutFilesetups(1, iCounter)
                        End If

                        ' Make sure array is sorted, on format_id (1)
                        If iCounter = 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object aOutFilesetups(0, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aOutFilesetups(0, i) = iFormat_ID
                            'UPGRADE_WARNING: Couldn't resolve default property of object aOutFilesetups(1, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            aOutFilesetups(1, i) = iFormatOut
                        Else
                            For i = 0 To UBound(aOutFilesetups, 2)
                                'UPGRADE_WARNING: Couldn't resolve default property of object aOutFilesetups(0, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If aOutFilesetups(0, i) > iFormat_ID Then
                                    ' we must insert new item in i
                                    Exit For
                                End If
                            Next i
                            i0Old = iFormat_ID
                            i1Old = iFormatOut
                            ' Move rest down;
                            For i = i To UBound(aOutFilesetups, 2)
                                'UPGRADE_WARNING: Couldn't resolve default property of object aOutFilesetups(0, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                i0 = aOutFilesetups(0, i)
                                'UPGRADE_WARNING: Couldn't resolve default property of object aOutFilesetups(1, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                i1 = aOutFilesetups(1, i)
                                'UPGRADE_WARNING: Couldn't resolve default property of object aOutFilesetups(0, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aOutFilesetups(0, i) = i0Old
                                'UPGRADE_WARNING: Couldn't resolve default property of object aOutFilesetups(1, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                aOutFilesetups(1, i) = i1Old
                                i0Old = i0
                                i1Old = i1
                            Next i
                        End If
                    End If
                End If
            Next oClient
        End If

        ' Find Format_ID for outformats:
        'For i = 0 To iCounter
        '    aOutFilesetups(0, i) = oProfile.FileSetups(aOutFilesetups(1, i)).Format_ID
        'Next i

        FindOutFormats = VB6.CopyArray(aOutFilesetups)

    End Function
    Sub MarkInvolvedFileSetups() '(oFilesetup As FileSetup)
        ' Marks connected FileSetups, either when there are only one connected, og
        ' if more than one, checks in Clients-Collection
        Dim oClient As Object 'Client
        Dim iFormat_ID As Short
        Dim bFromAccounting As Boolean
        Dim xFormat_Id As Short

        bFromAccounting = oFilesetup.FromAccountingSystem
        xFormat_Id = -1

        ' If only one outformat, FilesetupOut holds the format, use this one then:
        If oFilesetup.FileSetupOut <> -1 Then
            If oFilesetup.FromAccountingSystem = True Then
                ' This is a sendprofile. Mark corresponding returnprofile
                oProfile.FileSetups((oFilesetup.FileSetupOut)).Setup_Return = True
                oProfile.FileSetups((oFilesetup.FileSetupOut)).Setup_InitReturn = True
                ' Also, mark as changed for correct update in vbBabel.Company.Save
                oProfile.FileSetups((oFilesetup.FileSetupOut)).Status = vbBabel.Profile.CollectionStatus.Changed
            Else
                ' This is a returnprofile. Mark corresponding sendprofile
                oProfile.FileSetups((oFilesetup.FileSetupOut)).Setup_Send = True
                oProfile.FileSetups((oFilesetup.FileSetupOut)).Setup_InitSend = True
                ' Also, mark as changed for correct update in vbBabel.Company.Save
                oProfile.FileSetups((oFilesetup.FileSetupOut)).Status = vbBabel.Profile.CollectionStatus.Changed
            End If
        Else

            For Each oClient In oFilesetup.Clients
                'UPGRADE_WARNING: Couldn't resolve default property of object oClient.Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If oClient.Status <> vbBabel.Profile.CollectionStatus.Deleted Then
                    If bFromAccounting Then
                        ' we have a sendprofile
                        ' find corresponding returnprofiles format_id
                        'UPGRADE_WARNING: Couldn't resolve default property of object oClient.FormatOut. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        iFormat_ID = oClient.FormatOut
                        oProfile.FileSetups(iFormat_ID).Setup_Return = True
                        ' Also, mark as changed for correct update in vbBabel.Company.Save
                        oProfile.FileSetups((oFilesetup.FileSetupOut)).Status = vbBabel.Profile.CollectionStatus.Changed

                        If iFormat_ID < xFormat_Id Then
                            ' set corresponding filesetup with lowest filesetup_id as init
                            oProfile.FileSetups(iFormat_ID).Setup_InitReturn = True
                            xFormat_Id = iFormat_ID
                        End If
                    Else
                        ' we have a returnprofile
                        ' find corresponding sendprofiles format_id
                        'UPGRADE_WARNING: Couldn't resolve default property of object oClient.FormatIn. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        iFormat_ID = oClient.FormatIn
                        oProfile.FileSetups(iFormat_ID).Setup_Send = True
                        ' Also, mark as changed for correct update in vbBabel.Company.Save
                        oProfile.FileSetups((oFilesetup.FileSetupOut)).Status = vbBabel.Profile.CollectionStatus.Changed

                        If iFormat_ID < xFormat_Id Then
                            ' set corresponding filesetup with lowest filesetup_id as init
                            oProfile.FileSetups(iFormat_ID).Setup_InitSend = True
                            xFormat_Id = iFormat_ID
                        End If
                    End If
                End If
            Next oClient
        End If


    End Sub

    Function aReorganize(ByRef aInArray(,) As Short) As Object
        ' delete items marked -1 from oOutFileSetups
        Dim aOutArray(,) As Integer
        'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
        'DOTNETT REDIM Dim aOutArray() As Integer
        Dim iOne As Short
        Dim iNull As Short
        Dim iCounter As Short
        Dim i As Short
        ReDim aOutArray(0, 0)

        iCounter = 0
        For i = 0 To UBound(aInArray, 2)
            If aInArray(0, i) <> -1 Then
                iNull = aInArray(0, i)
                iOne = aInArray(1, i)
                ReDim aOutArray(1, iCounter)
                'UPGRADE_WARNING: Couldn't resolve default property of object aOutArray(0, iCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aOutArray(0, iCounter) = iNull
                'UPGRADE_WARNING: Couldn't resolve default property of object aOutArray(1, iCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                aOutArray(1, iCounter) = iOne
                iCounter = iCounter + 1
            End If
        Next i
        'UPGRADE_WARNING: Couldn't resolve default property of object aReorganize. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        aReorganize = VB6.CopyArray(aOutArray)

    End Function
    'Public Sub quickProfiles(ByRef iSelected As Short)
    '    ' Show form frmQuickProfiles
    '    ' Adjust form according to iSelected
    '    ' 1 = Telepay Merge
    '    ' 2 = Telepay Split
    '    ' 3 = OCR split
    '    ' 4 = Convert Dir.Rem to Telepay
    '    '-----------------------------------------------------------
    '    Dim nElementNo As Integer
    '    Dim iLRS As Integer

    '    ' Fill up aFormatsAndProfiles; ( for In part)
    '    aFormatsAndProfiles = LoadFormatsAndProfiles(True, False) 'true = from accountingsystem
    '    Select Case iSelected
    '        Case 0
    '            ' 0 = Telepay Merge
    '            ' -----------------
    '            oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
    '            oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.Telepay
    '            oFilesetup.Format_ID = 6
    '            ' Needs InFilename1 and OutFilename1

    '            ' Put in default values for Telepay;
    '            ' Find Telepay;
    '            For nElementNo = 0 To UBound(aFormatsAndProfiles, 2)
    '                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, nElementNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                If aFormatsAndProfiles(cEnumID, nElementNo) = oFilesetup.Enum_ID Then
    '                    If bNewProfile Then
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cInfoNewClient, nElementNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        oFilesetup.InfoNewClient = aFormatsAndProfiles(cInfoNewClient, nElementNo)
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cKeepBatch, nElementNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        oFilesetup.KeepBatch = aFormatsAndProfiles(cKeepBatch, nElementNo)
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cCtrlNegative, nElementNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        oFilesetup.CtrlNegative = aFormatsAndProfiles(cCtrlNegative, nElementNo)
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cCtrlZip, nElementNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        oFilesetup.CtrlZip = aFormatsAndProfiles(cCtrlZip, nElementNo)
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cTextFileimp1, nElementNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        oFilesetup.FileNameIn1 = aFormatsAndProfiles(cTextFileimp1, nElementNo)
    '                    End If
    '                End If
    '            Next


    '            frmQuickProfiles.Text = LRS(60001) & " " & oFilesetup.ShortName
    '            frmQuickProfiles.lblHeading.Text = LRS(60064)
    '            frmQuickProfiles.lblDescr.Text = LRS(60055)
    '            frmQuickProfiles.fraIn.Text = LRS(60056)
    '            frmQuickProfiles.chkBackup.Text = LRS(60060)
    '            frmQuickProfiles.txtInFilename1.Text = Trim(oFilesetup.FileNameIn1)
    '            ' Filename labels dependent on format:
    '            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            iLRS = Val(aFormatsAndProfiles(cLabelFileimp1, nElementNo - 1))
    '            frmQuickProfiles.lblInFilename1.Text = LRS(iLRS)
    '            frmQuickProfiles.txtInFilename1.SelectionStart = 0
    '            frmQuickProfiles.txtInFilename1.SelectionLength = Len(Trim(oFilesetup.FileNameIn1))
    '            frmQuickProfiles.lblProfilename.Text = LRS(60078)
    '            frmQuickProfiles.txtProfilename.Text = oFilesetup.ShortName



    '            ' Filenames to bank;
    '            oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)
    '            oFilesetup.Enum_ID = 4
    '            oFilesetup.Format_ID = 7
    '            ' Fill up aFormatsAndProfiles; ( for outpart)
    '            aFormatsAndProfiles = LoadFormatsAndProfiles(False, False) 'false = not from accountingsystem
    '            ' Find Telepay;
    '            For nElementNo = 0 To UBound(aFormatsAndProfiles, 2)
    '                'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, nElementNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                If aFormatsAndProfiles(cEnumID, nElementNo) = oFilesetup.Enum_ID Then
    '                    If bNewProfile Then
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cInfoNewClient, nElementNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        oFilesetup.InfoNewClient = aFormatsAndProfiles(cInfoNewClient, nElementNo)
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cKeepBatch, nElementNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        oFilesetup.KeepBatch = aFormatsAndProfiles(cKeepBatch, nElementNo)
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cCtrlNegative, nElementNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        oFilesetup.CtrlNegative = aFormatsAndProfiles(cCtrlNegative, nElementNo)
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cCtrlZip, nElementNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        oFilesetup.CtrlZip = aFormatsAndProfiles(cCtrlZip, nElementNo)
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cTextFileexp1, nElementNo). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        oFilesetup.FileNameOut1 = aFormatsAndProfiles(cTextFileexp1, nElementNo)
    '                    End If
    '                End If
    '            Next

    '            frmQuickProfiles.fraOut.Text = LRS(60063)
    '            frmQuickProfiles.chkOverwrite.Text = LRS(60067)
    '            frmQuickProfiles.chkOverwrite.CheckState = bVal(oFilesetup.OverWriteOut)
    '            frmQuickProfiles.chkWarning.Text = LRS(60068)
    '            frmQuickProfiles.chkWarning.CheckState = bVal(oFilesetup.WarningOut)
    '            frmQuickProfiles.txtOutFilename1.Text = Trim(oFilesetup.FileNameOut1)
    '            ' Filename for outfile:
    '            'iLRS = Val(aFormatsAndProfiles(cLabelFileexp1, nElementNo - 1))
    '            frmQuickProfiles.lblOutFilename1.Text = LRS(60131)
    '            frmQuickProfiles.txtOutFilename1.SelectionStart = 0
    '            frmQuickProfiles.txtOutFilename1.SelectionLength = Len(Trim(oFilesetup.FileNameOut1))


    '        Case 1
    '            ' 1 = Telepay Split
    '        Case 2
    '            ' 2 = OCR split
    '        Case 3
    '            ' 3 = Convert Dir.Rem to Telepay
    '    End Select

    '    frmQuickProfiles.Top = frmWiz2_WhichProfile.Top 'To "hide" wizard 2
    '    frmWiz2_WhichProfile.Hide()
    '    frmQuickProfiles.ShowDialog()
    'End Sub
    Private Function CheckNoOfFormats() As Boolean
        ' Count no of used formats, and check against license
        Dim aEnums() As Short
        Dim sEnum As Short
        Dim bFound As Boolean
        'Dim oLicense As Object 'vbbabel.License
        Dim oLicense As New vbBabel.License
        Dim i As Short
        ' ReDim aEnums(0) This causes problems with 0 in first element


        'oLicense = CreateObject ("vbbabel.license")

        For Each oFilesetup In oProfile.FileSetups
            sEnum = oFilesetup.Enum_ID
            If sEnum <> 999 Then '999 = Kun rapport
                bFound = False
                If Not Array_IsEmpty(aEnums) Then
                    For i = 0 To UBound(aEnums)
                        'If aEnums(i) = sEnum Then
                        ' 13.07.06 - allowed shift from Telepay to Telepay2
                        If aEnums(i) = sEnum Or (aEnums(i) = 4 And sEnum = 33) Or (aEnums(i) = 33 And sEnum = 4) Then
                            bFound = True
                            Exit For
                        End If
                    Next
                Else
                    bFound = False
                End If
                If Not bFound Then
                    If Array_IsEmpty(aEnums) Then
                        ReDim aEnums(0)
                        aEnums(0) = sEnum
                    Else
                        ReDim Preserve aEnums(UBound(aEnums) + 1)
                        aEnums(UBound(aEnums)) = sEnum
                    End If
                End If
            End If
        Next oFilesetup


        'UPGRADE_WARNING: Couldn't resolve default property of object oLicense.LicLimitFormats. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If (UBound(aEnums) + 1) > oLicense.LicLimitFormats Then
            ' Versions before 1.00.25 have no limit in formatsm therefor
            'UPGRADE_WARNING: Couldn't resolve default property of object oLicense.LicLimitFormats. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If oLicense.LicLimitFormats > 0 Then
                CheckNoOfFormats = False
                MsgBox(LRS(10061), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation)
            Else
                CheckNoOfFormats = True
            End If
        Else
            CheckNoOfFormats = True
        End If
        oLicense = Nothing


    End Function
    Public Function DnB_TBIW_SendProfile(ByRef oP As Object, ByRef iID As Short) As Boolean
        Dim frmLog As frmLog
        Dim frmSignalFile As frmSignalFile
        Dim frmDnB_TBIW_NBcodes As frmDnB_TBIW_NBcodes
        'Dim Printer As Printer
        Dim oMyDal As vbBabel.DAL = Nothing

        Dim i As Short
        'Dim Style As Integer
        'Dim iLRS As Long
        'Dim iNext As Integer
        Dim bStatus As Boolean
        'Dim sFormat_ID As String
        'Dim sEnum_ID
        'Dim oFs As New FileSystemObject
        'Dim sMyDatabasePath As String
        'Dim bx As Boolean
        Dim nElement As Integer
        Dim nFirstActive As Integer
        Dim sSendProfileName As String
        Dim sText As String
        Dim sMyFile As String
        Dim sTmp As String
        Dim aTmp() As String

        bStatus = False
        oProfile = oP

        If iID = -1 Then
            ' New profile !
            ' -------------
            ' Always create two profiles, one FromAccounting=true, one false

            ' add first new filesetup to filesetups, FromAccounting=True
            oFilesetup = oProfile.FileSetups.Add(CStr(oProfile.FileSetups.Count))

            'Initial values for FromAccounting-part;
            oFilesetup.Status = vbBabel.Profile.CollectionStatus.NewCol
            oProfile.Status = vbBabel.Profile.CollectionStatus.NewCol
            oFilesetup.FileSetup_ID = oProfile.FileSetups.Count
            ' Assumes at this stage that we have only one sendprofile/one returnprofile
            oFilesetup.FileSetupOut = oProfile.FileSetups.Count + 1
            oFilesetup.Bank_ID = 1 'DnB
            oFilesetup.FromAccountingSystem = True
            oFilesetup.BackupIn = True
            oFilesetup.BackupOut = True
            oFilesetup.Setup_Send = True
            oFilesetup.Setup_NewSend = True
            oFilesetup.Setup_InitSend = True
            oProfile.FileSetups.Setup_CurrentSend = oFilesetup.FileSetup_ID


            ' add second new filesetup to filesetups, FromAccounting=False
            oFilesetup = oProfile.FileSetups.Add(CStr(oProfile.FileSetups.Count))

            'Initial values for not FromAccounting-part;
            oFilesetup.Status = vbBabel.Profile.CollectionStatus.NewCol
            oProfile.Status = vbBabel.Profile.CollectionStatus.NewCol
            oFilesetup.FileSetup_ID = oProfile.FileSetups.Count
            ' FilesetupOut is the previously created Filesetup, the FromAccounting=True-part
            ' Assumes at this stage that we have only one sendprofile/one returnprofile
            oFilesetup.FileSetupOut = oProfile.FileSetups.Count - 1
            oFilesetup.Bank_ID = 1 'DnB
            oFilesetup.FromAccountingSystem = False
            oFilesetup.BackupIn = True
            oFilesetup.BackupOut = True
            oFilesetup.Setup_Return = True
            oFilesetup.Setup_NewReturn = True
            oFilesetup.Setup_InitReturn = True
            ' XNET 22.08.2013 Removed Crypt
            'oFilesetup.Crypt = True ' Default Crypt is ON
            oFilesetup.SplitAfter450 = False 'Default don't split
            oFilesetup.KeepBatch = True
            oFilesetup.FileNameOut1 = "tbi.tbd"
            oProfile.FileSetups.Setup_CurrentReturn = oFilesetup.FileSetup_ID

            bReturnProfileOnly = False
            bNewProfile = True

        Else
            ' we have a filesetupid passed from BabelbankEXE
            bNewProfile = False

            oFilesetup = oProfile.FileSetups(iID)
            If oFilesetup.FromAccountingSystem = False Then
                bReturnProfileOnly = True
                sReturnProfileName = oProfile.FileSetups(iID).ShortName

                oFilesetup.Setup_Return = True
                oFilesetup.Setup_NewReturn = False
                oFilesetup.Setup_InitReturn = True
                oProfile.FileSetups.Setup_CurrentReturn = oFilesetup.FileSetup_ID

                ' Mark the other involved filesetups;
                MarkInvolvedFileSetups() '(oFilesetup)


            Else
                sSendProfileName = oProfile.FileSetups(iID).ShortName
                bReturnProfileOnly = False
                oFilesetup.Setup_Send = True
                oFilesetup.Setup_NewSend = False
                oFilesetup.Setup_InitSend = True
                oProfile.FileSetups.Setup_CurrentSend = oFilesetup.FileSetup_ID

                ' Mark the other involved filesetups;
                MarkInvolvedFileSetups() '(oFilesetup)

            End If

            oFilesetup.Status = vbBabel.Profile.CollectionStatus.Changed
            oProfile.Status = vbBabel.Profile.CollectionStatus.Changed

        End If

        ' Fill special form for DnB_TBIW Send Setup
        ' New 31.03.06
        ' May have different Companynames for each profile (Ref Daniel in Singapore)
        frmDnB_TBIW_Send = New frmDnB_TBIW_Send
        frmMapping = New frmMapping
        frmDnB_TBIW_Advanced = New frmDnB_TBIW_Advanced
        frmDnB_TBIW_MailPrint = New frmDnB_TBIW_MailPrint
        frmDnB_TBIW_Charges = New frmDnB_TBIW_Charges
        frmDnB_TBIW_MergePayments = New frmDnB_TBIW_MergePayments
        frmDnB_TBIW_Accounts = New frmDnB_TBIW_Accounts

        frmLog = New frmLog
        frmSignalFile = New frmSignalFile
        frmWiz_Report = New frmWiz_Report
        frmReportAdvanced = New frmReportAdvanced
        FillAvailableReports(frmReportAdvanced.lstReport)
        frmReportPrinter = New frmReportPrinter
        frmEmail = New frmEmail
        frmDnB_TBIW_NBcodes = New frmDnB_TBIW_NBcodes
        'Printer = New Printer

        If Not EmptyString((oFilesetup.Description)) Then
            frmDnB_TBIW_Send.txtCompany.Text = oFilesetup.Description
        Else
            frmDnB_TBIW_Send.txtCompany.Text = oProfile.CompanyName
        End If
        frmDnB_TBIW_Send.txtCompanyNo.Text = oProfile.CompanyNo
        frmDnB_TBIW_Send.chkBackup.CheckState = bVal(oProfile.Backup)
        frmDnB_TBIW_Send.txtDelDays.Text = CStr(oProfile.DelDays)
        ' XNET 23.01.2012
        ' Can now have different backuppaths for each profile
        'frmDnB_TBIW_Send.txtBackupPath.Text = oProfile.BackupPath
        If Not EmptyString(oFilesetup.BackupPath) Then
            frmDnB_TBIW_Send.txtBackupPath.Text = oFilesetup.BackupPath
        Else
            frmDnB_TBIW_Send.txtBackupPath.Text = oProfile.BackupPath
        End If

        ' Fill values from "FilenameIn-part", FromAccounting=True-part
        ' Select correct Filesetup, the "FileIN-part" first
        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)

        ' 26.06.2008, added Mappingfile
        frmMapping.txtMappingFile.Text = Trim(oFilesetup.MappingFileName)


        ' In format
        ' Fill array with formats, from format table
        aFormatsAndProfiles = LoadFormatsAndProfiles(True, True, True) 'from accounting, specialformats

        ' test
        ' Empty listbox, in case we skip back and forth ..
        frmDnB_TBIW_Send.cmbFormat.Items.Clear()
        ' fill listbox:
        For i = 0 To UBound(aFormatsAndProfiles, 2)
            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            frmDnB_TBIW_Send.cmbFormat.Items.Add((aFormatsAndProfiles(0, i) + Space(100) + Str(aFormatsAndProfiles(1, i))))
            ' if called with existing sendprofile, show informat in listbox:
            'UPGRADE_WARNING: Couldn't resolve default property of object aFormatsAndProfiles(cEnumID, i). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If aFormatsAndProfiles(cEnumID, i) = oFilesetup.Enum_ID Then
                frmDnB_TBIW_Send.cmbFormat.SelectedIndex = frmDnB_TBIW_Send.cmbFormat.Items.Count - 1
            End If
        Next i
        frmDnB_TBIW_Send.txtFromInternal.Text = oFilesetup.FileNameIn1
        frmDnB_TBIW_Send.chkBackupIn.CheckState = bVal(oFilesetup.BackupIn)

        ' set all parameters in all frm's for Reportsetup, and show dialog
        ReportSetupAndShow()

        ' Save Path for accountsfile for DnB TBIW XML;
        frmDnB_TBIW_Send.txtAccountsPath.Text = oProfile.AccountsFile

        '21.09.2017 added next
        frmDnB_TBIW_Advanced.chkToOwnAccount.CheckState = bVal(oFilesetup.Accumulation)

        ' Then, Select correct Filesetup, the "FileOUT-part"
        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)

        'added 03.09.09
        frmDnB_TBIW_Send.txtDivision.Text = oFilesetup.Division

        ' File to DnB
        frmDnB_TBIW_Send.txtToBank.Text = oFilesetup.FileNameOut1
        frmDnB_TBIW_Send.chkBackupOut.CheckState = bVal(oFilesetup.BackupOut)


        ' 10.02.06: Added functionalty to have separate EnterpriseNos for each profile (Singapore)
        'If Not EmptyString(oFilesetup.CompanyNo) Then
        '    ' Fill in only if set in profile.
        '    frmDnB_TBIW_Advanced.txtCompanyNo.Text = oFilesetup.CompanyNo
        'End If
        ' 24.03.06 Changed this funcionality with Profilespecific companyno.
        ' No uses txtCompanyNo i "main" setuppicture for DnBTBI, also for profilespecific companyno
        ' frmDnB_TBIW_Advanced.txtCompanyNo is thus removed
        If Not EmptyString((oFilesetup.CompanyNo)) Then
            ' Fill in if set for this profile.
            frmDnB_TBIW_Send.txtCompanyNo.Text = oFilesetup.CompanyNo
        End If


        ' added 02.01.2009
        ' Can now select Telepay+ format (Nettbank Denmark)
        ' XNET 22.08.2013 Removed next If/else/endif because we have removed chkTelepayPlus

        ' Profilename for sendprofile
        ' Must go back to the "FromAccounting=True" part
        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
        frmDnB_TBIW_Send.txtProfilename.Text = oFilesetup.ShortName

        Fill_frmEmail(oProfile, frmEmail)
        Fill_frmDnB_TBIW_Accounts(oFilesetup)

        ' Fill frmDnB_TBIW_Advanced
        ' DnB TBIW XML-format;
        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)
        ' XNET 22.08.2013 Removed chkCrypt
        frmDnB_TBIW_Advanced.chkSplitAfter450.CheckState = bVal(oFilesetup.SplitAfter450)

        ' The corrections-part is also saved in the "fileout"-part of profile

        ' xnet 20.08.2012 take whole If-block
        If oFilesetup.ShowCorrections = 0 Then
            frmDnB_TBIW_Advanced.optNone.Checked = True
            frmDnB_TBIW_Advanced.optErrorsOnly.Checked = False
            frmDnB_TBIW_Advanced.optErrorsWarnings.Checked = False
            frmDnB_TBIW_Advanced.optShowAll.Checked = False
        ElseIf oFilesetup.ShowCorrections = 1 Then
            frmDnB_TBIW_Advanced.optNone.Checked = False
            frmDnB_TBIW_Advanced.optErrorsOnly.Checked = True
            frmDnB_TBIW_Advanced.optErrorsWarnings.Checked = False
            frmDnB_TBIW_Advanced.optShowAll.Checked = False
        ElseIf oFilesetup.ShowCorrections = 3 Then
            frmDnB_TBIW_Advanced.optNone.Checked = False
            frmDnB_TBIW_Advanced.optErrorsOnly.Checked = False
            frmDnB_TBIW_Advanced.optErrorsWarnings.Checked = False
            frmDnB_TBIW_Advanced.optShowAll.Checked = True
        Else
            frmDnB_TBIW_Advanced.optNone.Checked = False
            frmDnB_TBIW_Advanced.optErrorsOnly.Checked = False
            frmDnB_TBIW_Advanced.optErrorsWarnings.Checked = True
            frmDnB_TBIW_Advanced.optShowAll.Checked = False
        End If
        ' end xnet 20.08.2012

        ' Also KeepBatch (True=One file pr paymenttype, False=One file togehter)
        ' is saved in the "fileout-part" of profile
        ' XNET 22.08.2013 Removed KeepBatch/chkKeepClients


        '-----------------------------------
        ' From frmDnB_TBIW_MailPrint
        '-----------------------------------
        'frmDnB_TBIW_MailPrint.txtMailAddresses.Text = oFilesetup.MailAddress

        ' Mailaddresses stored in Notification, and set in oClient
        ' Set all mailaddresses from clients for this Filesetup in txtMailAddresses

        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If
        With frmDnB_TBIW_MailPrint

            ' Get recordset with all mailaddresses for DnBNORTBI only
            dbGetDnBNORTBINotifications(oMyDal)
            .txtMailAddresses.Text = ""
            If oMyDal.Reader_HasRows Then
                Do While oMyDal.Reader_ReadRecord
                    ' If EMailaddress is filled, then put it into .txtMailAddresses
                    If Not oMyDal.Reader_GetString("EMail_Adress") = "" Then
                        .txtMailAddresses.Text = .txtMailAddresses.Text & ";" & oMyDal.Reader_GetString("EMail_Adress")
                    Else
                        ' If no mailaddress, assume DnBNOR Denmark, DnBNOR Great Britain, etc.,
                        ' and select in lstDnBNOR
                        For i = 0 To .lstDnBNOR.Items.Count - 1
                            'If VB6.GetItemString(.lstDnBNOR, i) = oMyDal.Reader_GetString("Name") Then
                            ' 15.07.2019 - er denne under OK?
                            If .lstDnBNOR.Items(i) = oMyDal.Reader_GetString("Name") Then
                                .lstDnBNOR.SetItemChecked(i, True)
                            End If
                        Next i
                    End If
                Loop
            End If

            ' remove leading ;
            .txtMailAddresses.Text = Mid(.txtMailAddresses.Text, 2)
            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

        End With

        frmDnB_TBIW_MailPrint.txtWordTemplate.Text = oFilesetup.FilenameOut4

        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)

        'If Len(frmDnB_TBIW_MailPrint.txtMailAddresses.Text) > 0 Then
        ' frmDnB_TBIW_Charges
        If oFilesetup.ChargeMeDomestic = True Then
            frmDnB_TBIW_Charges.chkChargeMeDomestic.CheckState = System.Windows.Forms.CheckState.Checked
        End If
        If oFilesetup.ChargeMeAbroad = True Then
            frmDnB_TBIW_Charges.chkChargeMeAbroad.CheckState = System.Windows.Forms.CheckState.Checked
        End If
        If oFilesetup.ChargesFromSetup = True Then
            frmDnB_TBIW_Charges.chkChargesFromSetup.CheckState = System.Windows.Forms.CheckState.Checked
        End If
        ' XokNET 23.06.2014
        ' Added next line AND added new label and textbox to Purposecode
        frmDnB_TBIW_Charges.txtPurposeCode.Text = oFilesetup.StandardPurposeCode


        If oFilesetup.NotificationMail Then
            frmDnB_TBIW_MailPrint.chkMail.CheckState = System.Windows.Forms.CheckState.Checked
            frmDnB_TBIW_MailPrint.txtMailAddresses.Enabled = True
            frmDnB_TBIW_MailPrint.lstDnBNOR.Enabled = True
        Else
            frmDnB_TBIW_MailPrint.chkMail.CheckState = System.Windows.Forms.CheckState.Unchecked
            frmDnB_TBIW_MailPrint.txtMailAddresses.Enabled = False
            frmDnB_TBIW_MailPrint.lstDnBNOR.Enabled = False
        End If
        'If Len(oFilesetup.FilenameOut4) > 0 Then
        If oFilesetup.NotificationPrint Then
            frmDnB_TBIW_MailPrint.chkPrint.CheckState = System.Windows.Forms.CheckState.Checked
            frmDnB_TBIW_MailPrint.txtWordTemplate.Enabled = True
            frmDnB_TBIW_MailPrint.cmdFileOpenTemplate.Enabled = True
        Else
            frmDnB_TBIW_MailPrint.chkPrint.CheckState = System.Windows.Forms.CheckState.Unchecked
            frmDnB_TBIW_MailPrint.txtWordTemplate.Enabled = False
            frmDnB_TBIW_MailPrint.cmdFileOpenTemplate.Enabled = False
        End If
        If oFilesetup.ReportAllPayments Then
            frmDnB_TBIW_MailPrint.opt140.Checked = False
            frmDnB_TBIW_MailPrint.optAll.Checked = True
        Else
            frmDnB_TBIW_MailPrint.opt140.Checked = True
            frmDnB_TBIW_MailPrint.optAll.Checked = False
        End If
        '--- End frmDnB_TBIW_Mailprint -----------------------------

        '--- From frmDnB_Mergepayments -----------------------------
        If oFilesetup.MergePayments = 1 Then
            frmDnB_TBIW_MergePayments.optMergeKeepInfo.Checked = True
        ElseIf oFilesetup.MergePayments = 2 Then
            frmDnB_TBIW_MergePayments.otpMergeOptimal.Checked = True
            ' added 19.03.2009
        ElseIf oFilesetup.MergePayments = 3 Then
            frmDnB_TBIW_MergePayments.optMergeNoInfo.Checked = True
            ' XokNET 24.08.2011, added Mergeoption for creditnotes only
        ElseIf oFilesetup.MergePayments = 4 Then
            frmDnB_TBIW_MergePayments.optMergeCreditsOnly.checked = True
        Else
            ' Do not merge
            frmDnB_TBIW_MergePayments.optDoNotMerge.Checked = True
        End If
        If oFilesetup.Sorting > 0 Then
            ' added 29.10.2008, sorting before merge
            frmDnB_TBIW_MergePayments.chkSort.CheckState = System.Windows.Forms.CheckState.Checked
        Else
            frmDnB_TBIW_MergePayments.chkSort.CheckState = System.Windows.Forms.CheckState.Unchecked
        End If
        '--- End frmDnB_Mergepayments -----------------------------

        oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Clear()
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("Not in use"))
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("General Payment"))
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("FI Payment"))
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("ACH Payments"))
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("Salary"))
        'New 07.02.2005 - added Reference Payments
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("Reference Payments"))
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("Check")) ' Added 06.10.05
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("US General/ACH")) ' Added 07.04.2009
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("Local low value")) ' Added 16.03.2010
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("Local high value")) ' Added 16.03.2010
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("Local check")) ' Added 16.03.2010
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("SEPA"))  ' Added 24.10.2013
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("IPB low value")) ' Added 16.05.2019
        frmDnB_TBIW_Advanced.cmbPaymentType.Items.Add(("IPB high value")) ' Added 16.05.2019
        Select Case Trim(oFilesetup.PaymentType)
            Case "GEN"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 1
            Case "FIK"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 2
            Case "ACH"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 3
            Case "SAL"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 4
                'New 07.02.2005 - added Reference Payments
            Case "REF"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 5
                ' New 06.10.05 - added Check
            Case "CHECK"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 6
                ' New 07.04.2009
            Case "USGENACH"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 7
                ' added 16.03.2010
            Case "LOCALLOW"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 8
            Case "LOCALHIGH"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 9
            Case "LOCALCHECK"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 10
            Case "SEPA"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 11
            Case "IPBLOW"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 12
            Case "IPBHIGH"
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 13

            Case Else
                ' default Not in use
                frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex = 0
        End Select
        frmDnB_TBIW_Advanced.chkSilent.CheckState = bVal(oFilesetup.Silent)
        ' added 20070731
        frmDnB_TBIW_Advanced.txtAddDays.Text = CStr(oFilesetup.AddDays)

        ' added 03.02.2016 for ISO 20022
        If oFilesetup.ISO20022 = True Then
            frmDnB_TBIW_Advanced.optPain001.Checked = True
            frmDnB_TBIW_Advanced.optTelepayPlus.Checked = False
        Else
            frmDnB_TBIW_Advanced.optPain001.Checked = False
            frmDnB_TBIW_Advanced.optTelepayPlus.Checked = True
        End If

        ' Show optPain.001 only when Licensecode "X" is set
        If IsThisDNBXMLIntegrator() Then
            frmDnB_TBIW_Advanced.optPain001.Visible = True
        Else
            frmDnB_TBIW_Advanced.optPain001.Visible = False
        End If

        ' frmLog under frmDnB_TBIW_Advanced:
        frmLog.Text = LRS(60195) 'Log
        frmLog.chkLog.Text = LRS(60196) ' Activate logging
        frmLog.optWinLog.Text = LRS(60197) ' Windows EventLog
        frmLog.OptFileLog.Text = LRS(60198) ' Log events to file
        frmLog.optMailLog.Text = LRS(60199) ' Log events to mail
        frmLog.lblFilename.Text = LRS(60200) ' Filnavn
        frmLog.chkOverwrite.Text = LRS(60201) ' New logfile each time
        frmLog.chkShowStartStop.Text = LRS(60324)
        frmLog.lblMaxLines.Text = LRS(60202) ' Max number of lines
        frmLog.chkLog.CheckState = bVal(oFilesetup.Log)
        If oFilesetup.LogType = 1 Then
            frmLog.optWinLog.Checked = True
        ElseIf oFilesetup.LogType = 2 Then
            frmLog.OptFileLog.Checked = True
        Else
            frmLog.optMailLog.Checked = True
        End If
        frmLog.txtFilename.Text = oFilesetup.LogFilename
        frmLog.txtMaxLines.Text = Str(oFilesetup.LogMaxLines)
        frmLog.chkOverwrite.CheckState = bVal(oFilesetup.LogOverwrite)
        frmLog.chkShowStartStop.CheckState = bVal(oFilesetup.LogShowStartStop)
        ' Fill listbox for selecting details-level;
        frmLog.lstDetail.Items.Clear()
        frmLog.lstDetail.Items.Add(LRS(60312)) ' Errormessages only
        frmLog.lstDetail.Items.Add(LRS(60313)) ' also warnings
        frmLog.lstDetail.Items.Add(LRS(60314)) ' also info
        frmLog.lstDetail.Items.Add(LRS(60315)) ' all messages
        Select Case oFilesetup.LogDetails
            Case 1
                frmLog.lstDetail.SelectedIndex = 0
            Case 2
                frmLog.lstDetail.SelectedIndex = 1
            Case 4
                frmLog.lstDetail.SelectedIndex = 2
            Case 8
                frmLog.lstDetail.SelectedIndex = 3
        End Select




        ' Set Selstart and sellength for all text-controls
        FormSelectAllTextboxs(frmDnB_TBIW_Send)
        ' added 20070731
        FormSelectAllTextboxs(frmDnB_TBIW_Advanced)

        Do While bStatus = False
            '============================
            frmDnB_TBIW_Send.Set_frms(frmWiz_Report, frmReportAdvanced, frmReportPrinter, frmDnB_TBIW_Advanced, _
                                      frmMapping, frmEmail, frmDnB_TBIW_NBcodes, frmDnB_TBIW_MergePayments, _
                                      frmLog, frmDnB_TBIW_Accounts, frmDnB_TBIW_Charges, frmReportSQL, frmDNB_TBIW_CurrencyField)
            frmDnB_TBIW_Send.ShowDialog()
            '============================

            '----------------------
            ' SAVE if not canceled;
            '----------------------
            If frmDnB_TBIW_Send.bStatus Then
                bStatus = frmDnB_TBIW_Send.bStatus

                ' frmDnB_TBIW_SendProfile
                oProfile.CompanyName = frmDnB_TBIW_Send.txtCompany.Text
                oProfile.CompanyNo = frmDnB_TBIW_Send.txtCompanyNo.Text
                oProfile.Backup = CBool(frmDnB_TBIW_Send.chkBackup.CheckState)
                oProfile.DelDays = Val(frmDnB_TBIW_Send.txtDelDays.Text)
                oProfile.BackupPath = frmDnB_TBIW_Send.txtBackupPath.Text

                ' Select correct Filesetup, the "FileIN-part" first
                oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                ' New 24.03.06
                ' Also set companyno for current profile
                oFilesetup.CompanyNo = frmDnB_TBIW_Send.txtCompanyNo.Text
                '-end new 24.03.06---------------------------------------
                ' XokNET 23.01.2012
                ' Also set backuppath for current profile
                oFilesetup.BackupPath = frmDnB_TBIW_Send.txtBackupPath.Text

                ' 31.03.06
                ' Added possibilty to have different Companynames for each profile (ref Daniel in Singapore)
                oFilesetup.Description = frmDnB_TBIW_Send.txtCompany.Text
                '-end new 31.03.06---------------------------------------
                ' File from accounting
                'oFilesetup.Format_ID = Val(Right(VB6.GetItemString(frmDnB_TBIW_Send.cmbFormat, frmDnB_TBIW_Send.cmbFormat.SelectedIndex), 4))
                ' 15.07.2019
                oFilesetup.Format_ID = Val(Right(frmDnB_TBIW_Send.cmbFormat.Text, 4))

                oFilesetup.Enum_ID = aFormatsAndProfiles(cEnumID, frmDnB_TBIW_Send.cmbFormat.SelectedIndex)
                oFilesetup.FileNameIn1 = frmDnB_TBIW_Send.txtFromInternal.Text
                oFilesetup.BackupIn = CBool(frmDnB_TBIW_Send.chkBackupIn.CheckState)

                ' 26.06.2008, added Mappingfile
                oFilesetup.MappingFileName = Trim(frmMapping.txtMappingFile.Text)

                ' Then, Select correct Filesetup, the "FileOUT-part"
                oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)

                ' XNET 30.01.2012 - also fill in backup in "filename-in part"
                oFilesetup.BackupPath = frmDnB_TBIW_Send.txtBackupPath.Text

                'added 03.09.2009
                oFilesetup.Division = frmDnB_TBIW_Send.txtDivision.Text

                ' New 24.03.06
                ' Also set companyno for current profile
                oFilesetup.CompanyNo = frmDnB_TBIW_Send.txtCompanyNo.Text
                '-end new 24.03.06---------------------------------------

                ' 31.03.06
                ' Added possibilty to have different Companynames for each profile (ref Daniel in Singapore)
                oFilesetup.Description = frmDnB_TBIW_Send.txtCompany.Text
                '-end new 31.03.06---------------------------------------

                ' XNET 22.08.2013 Removed comments and next IF/else/endig because we have removed chkTelepayPlus_Click()
                ' 04.02.2016 -
                ' Option to create ISO20022 Pain.001
                If frmDnB_TBIW_Advanced.optPain001.Checked Then
                    oFilesetup.Format_ID = 254 ' Pain.001
                    oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.Pain001 'Pain.001
                    oFilesetup.Bank_ID = vbBabel.BabelFiles.Bank.DnB
                Else
                    oFilesetup.Format_ID = 205 ' DNB TelepayPlus
                    oFilesetup.Enum_ID = 71 'DNB TelepayPlus
                End If
                ' File to DnB
                oFilesetup.FileNameOut1 = frmDnB_TBIW_Send.txtToBank.Text
                oFilesetup.Bank_ID = 1 '"Den Norske Bank"
                oFilesetup.BackupOut = CBool(frmDnB_TBIW_Send.chkBackupOut.CheckState)

                ' Save Path for accountsfile for DnB TBIW XML;
                sText = Trim(frmDnB_TBIW_Send.txtAccountsPath.Text)
                ' Filepath only, not filename
                ' Check if . in last part of filepath
                sText = Trim(Mid(sText, InStrRev(sText, "\") + 1))

                If InStr(sText, ".") > 0 Then
                    ' Assume filename as last part, remove!
                    sText = frmDnB_TBIW_Send.txtAccountsPath.Text
                    sText = Left(sText, InStrRev(sText, "\") - 1)
                Else
                    sText = frmDnB_TBIW_Send.txtAccountsPath.Text
                End If
                oProfile.AccountsFile = sText

                ' Some defaults ...
                ' Warning if file exists
                oFilesetup.WarningOut = True
                ' Overwrite if file is present (first: Warning)
                oFilesetup.OverWriteOut = True
                oFilesetup.RemoveOldOut = False

                ' Save profilename sendprofile
                ' Must go back to the "FromAccounting=True" part
                oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                sSendProfileName = frmDnB_TBIW_Send.txtProfilename.Text
                ' Test shortname, no spaces, not start with "RETUR, SEND, ALL"
                If InStr(sSendProfileName, " ") > 0 Then
                    MsgBox(LRS(60102), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Illegal characters in profilename
                    bStatus = False
                End If
                If bStatus And InStr(sSendProfileName, "\") > 0 Then
                    MsgBox(LRS(60102), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Illegal characters in profilename
                    bStatus = False
                End If
                If bStatus And InStr(sSendProfileName, "/") > 0 Then
                    MsgBox(LRS(60102), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' Illegal characters in profilename
                    bStatus = False
                End If

                If UCase(Left(sSendProfileName, 5)) = "RETUR" Or UCase(Left(sSendProfileName, 4)) = "SEND" Or UCase(Left(sSendProfileName, 3)) = "ALL" Then
                    MsgBox(LRS(60103), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) ' RETUR, SEND, ALL not allowed in start of profilename
                    bStatus = False
                End If

                ' Check for unique profilename:
                For Each oFilesetup In oProfile.FileSetups
                    If UCase(sSendProfileName) = UCase(oFilesetup.ShortName) And oFilesetup.Setup_Send = False Then
                        MsgBox(LRS(60100), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation) 'Must have unique profilename
                        bStatus = False
                        Exit For
                    End If
                Next oFilesetup
                oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)
                ' frmAdvanced
                ' 21.09.2017 added setup for ToOwnaccount
                oFilesetup.Accumulation = bVal(frmDnB_TBIW_Advanced.chkToOwnAccount.CheckState)

                If bStatus Then
                    oFilesetup.ShortName = sSendProfileName
                    ' Also, give a name for "Returnprofile"
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)
                    oFilesetup.ShortName = sSendProfileName & "2"
                End If

                ' frmEmail
                SaveEMailSettings(frmEmail)


                ' Crypt-flag and SplitAfter450 is set on the "fileout-part" of profile
                oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)
                ' XNET 22.08.2013 Removed chkCrypt
                oFilesetup.SplitAfter450 = bVal(frmDnB_TBIW_Advanced.chkSplitAfter450.CheckState)
                ' The Correctcions-part is also saved in the "fileout-part"
                If frmDnB_TBIW_Advanced.optNone.Checked = True Then
                    oFilesetup.ShowCorrections = 0
                ElseIf frmDnB_TBIW_Advanced.optErrorsOnly.Checked = True Then
                    oFilesetup.ShowCorrections = 1
                    ' xnet 20.08.2012 added next ElseIf
                ElseIf frmDnB_TBIW_Advanced.optShowAll.Checked = True Then
                    oFilesetup.ShowCorrections = 3
                Else
                    oFilesetup.ShowCorrections = 2
                End If

                ' added 03.02.2016
                If frmDnB_TBIW_Advanced.optPain001.Checked Then
                    oFilesetup.ISO20022 = True
                Else
                    oFilesetup.ISO20022 = False
                End If


                ' XNET 22.08.2013 Removed KeepBatch/chkKeepClients

                ' 10.02.06: Added functionalty to have separate EnterpriseNos for each profile (Singapore)
                'If Not EmptyString(frmDnB_TBIW_Advanced.txtCompanyNo.Text) Then
                ' Removed again 24.03.06. use txtCompanyNo i main TBI-setup
                'oFilesetup.CompanyNo = Trim(frmDnB_TBIW_Advanced.txtCompanyNo.Text)
                'End If

                '-----------------------------------
                ' From frmDnB_TBIW_MailPrint
                '-----------------------------------
                ' All settings from frmDnBTBI_mailPrint on the "fileout-part" of profile
                oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitReturn)

                ' Mailaddresses stored in Notification, and set in oClient
                ' Save all mailaddresses from clients for this Filesetup in txtMailAddresses
                ' Open BabelBanks database
                With frmDnB_TBIW_MailPrint
                    ' Run through all mailaddresses, and insert into table Notification

                    ReDim aTmp(0)
                    For i = 1 To 999
                        ' run through all mailaddresses
                        If InStr(.txtMailAddresses.Text, ";") = 0 Then
                            ' No ; only one mailaddress
                            aTmp(0) = Trim(.txtMailAddresses.Text)
                            Exit For
                        End If
                        sTmp = xDelim(.txtMailAddresses.Text, ";", i)
                        If sTmp = "DELIMERROR" Or EmptyString(sTmp) Then
                            Exit For
                        Else
                            If i = 1 Then
                                aTmp(0) = sTmp
                            Else
                                ReDim Preserve aTmp(UBound(aTmp) + 1)
                                aTmp(i - 1) = sTmp
                            End If
                        End If
                    Next i
                    ' Then update selected DnBNOR Branches from lstDnBNOR;
                    For i = 0 To .lstDnBNOR.Items.Count - 1
                        If .lstDnBNOR.GetItemChecked(i) Then
                            If UBound(aTmp) = 0 Then
                                ' Zero or one address from .txtMailAddresses
                                If EmptyString(aTmp(0)) Then
                                    'aTmp(0) = VB6.GetItemString(.lstDnBNOR, i)
                                    ' 15.07.2019
                                    aTmp(0) = .lstDnBNOR.Items(i).ToString
                                Else
                                    ReDim Preserve aTmp(UBound(aTmp) + 1)
                                    'aTmp(UBound(aTmp)) = VB6.GetItemString(.lstDnBNOR, i)
                                    ' 15.07.2019
                                    aTmp(UBound(aTmp)) = .lstDnBNOR.Items(i).ToString
                                End If
                            Else
                                ' More than on address from .txtMailAddresses
                                ReDim Preserve aTmp(UBound(aTmp) + 1)
                                'aTmp(UBound(aTmp)) = VB6.GetItemString(.lstDnBNOR, i)
                                ' 15.07.2019
                                aTmp(UBound(aTmp)) = .lstDnBNOR.Items(i).ToString
                            End If
                        End If
                    Next i
                    ' Add to table Notification if not present
                    dbUpdateDnBNORTBINotification(aTmp)

                    oFilesetup.FilenameOut4 = Trim(.txtWordTemplate.Text)

                    ' Shift to the "filein-part" of profile
                    oFilesetup = oProfile.FileSetups(oProfile.FileSetups.Setup_GotoInitSend)

                    oFilesetup.NotificationMail = bVal(.chkMail.CheckState)
                    oFilesetup.NotificationPrint = bVal(.chkPrint.CheckState)
                    oFilesetup.ReportAllPayments = IIf(frmDnB_TBIW_MailPrint.opt140.Checked, False, True)

                End With

                ' Save from frm_TBIW_Charges
                oFilesetup.ChargesFromSetup = bVal(frmDnB_TBIW_Charges.chkChargesFromSetup.CheckState)
                oFilesetup.ChargeMeDomestic = bVal(frmDnB_TBIW_Charges.chkChargeMeDomestic.CheckState)
                oFilesetup.ChargeMeAbroad = bVal(frmDnB_TBIW_Charges.chkChargeMeAbroad.CheckState)
                ' XokNET 23.06.2014
                ' Added next line AND added new label and textbox to frmDnB_TBIW_Charges
                oFilesetup.StandardPurposeCode = frmDnB_TBIW_Charges.txtPurposeCode.Text


                ' Save from DnB_TBIW_MergePayments
                If frmDnB_TBIW_MergePayments.optDoNotMerge.Checked Then
                    oFilesetup.MergePayments = 0
                ElseIf frmDnB_TBIW_MergePayments.optMergeKeepInfo.Checked Then
                    oFilesetup.MergePayments = 1
                ElseIf frmDnB_TBIW_MergePayments.otpMergeOptimal.Checked Then
                    oFilesetup.MergePayments = 2
                    ' added 19.03.2009
                ElseIf frmDnB_TBIW_MergePayments.optMergeNoInfo.Checked Then
                    oFilesetup.MergePayments = 3
                    ' XokNET 24.08.2011, added Mergeoption for creditnotes only
                ElseIf frmDnB_TBIW_MergePayments.optMergeCreditsOnly.Checked Then
                    oFilesetup.MergePayments = 4
                End If
                If frmDnB_TBIW_MergePayments.chkSort.CheckState = 1 Then
                    oFilesetup.Sorting = 1
                Else
                    oFilesetup.Sorting = 0
                End If

                Select Case frmDnB_TBIW_Advanced.cmbPaymentType.SelectedIndex
                    Case 1
                        oFilesetup.PaymentType = "GEN"
                    Case 2
                        oFilesetup.PaymentType = "FIK"
                    Case 3
                        oFilesetup.PaymentType = "ACH"
                    Case 4
                        oFilesetup.PaymentType = "SAL"
                    Case 5
                        oFilesetup.PaymentType = "REF"
                    Case 6
                        oFilesetup.PaymentType = "CHECK" ' Added 06.10.05
                    Case 7
                        oFilesetup.PaymentType = "USGENACH" ' added 07.04.2009
                    Case 8
                        oFilesetup.PaymentType = "LOCALLOW" ' added 16.03.2010
                    Case 9
                        oFilesetup.PaymentType = "LOCALHIGH" ' added 16.03.2010
                    Case 10
                        oFilesetup.PaymentType = "LOCALCHECK" ' added 16.03.2010
                    Case 11
                        oFilesetup.PaymentType = "SEPA" ' added 06.03.2014
                    Case 12
                        oFilesetup.PaymentType = "IPBLOW" ' added 16.05.2019
                    Case 13
                        oFilesetup.PaymentType = "IPBHIGH" ' added 16.05.2019
                    Case Else
                        'default Not in use
                        oFilesetup.PaymentType = ""
                End Select
                ' added 20070731
                oFilesetup.AddDays = Val(frmDnB_TBIW_Advanced.txtAddDays.Text)

                ' Reportsettings; taken already, as they are placed in oFilesetup, with changes
                '----------------
                ' release all previously saved reports in ofilesetup;

                oFilesetup.Silent = bVal(frmDnB_TBIW_Advanced.chkSilent.CheckState)

                ' added 03.02.2016
                If frmDnB_TBIW_Advanced.optPain001.Checked Then
                    oFilesetup.ISO20022 = True
                Else
                    oFilesetup.ISO20022 = False
                End If

                ' frmLog under frmDnB_TBIW_Advanced:
                'SaveLog()


            Else
                ' User canceled
                Exit Do
            End If
        Loop
        DnB_TBIW_SendProfile = bStatus

        frmSignalFile = Nothing
        frmWiz_Report.Close()
        frmWiz_Report = Nothing
        frmReportAdvanced = Nothing
        frmReportPrinter = Nothing
        frmEmail = Nothing
        'Printer = Nothing

    End Function
    Private Function Fill_frmEmail(ByRef oProfile As Object, ByVal frmEmail As frmEmail) As Boolean
        ' frmEmail;

        ' new 13.11.02
        ' added e-mail smtp functionality
        If oProfile.EmailSMTP Then
            frmEmail.optMAPI.Checked = False
            frmEmail.optSMTP.Checked = True
        Else
            frmEmail.optMAPI.Checked = True
            frmEmail.optSMTP.Checked = False
        End If
        frmEmail.txtSender.Text = Trim(oProfile.EmailSender)
        frmEmail.txtDisplayName.Text = Trim(oProfile.EmailDisplayName)
        frmEmail.txtAutoSender.Text = Trim(oProfile.EmailAutoSender)
        frmEmail.txtAutoDisplayName.Text = Trim(oProfile.EmailAutoDisplayName)
        frmEmail.txtAutoReceiver.Text = Trim(oProfile.EmailAutoReceiver)
        frmEmail.txtReplyAdress.Text = Trim(oProfile.EmailReplyAdress)
        frmEmail.txtSMTPHost.Text = Trim(oProfile.EmailSMTPHost)
        frmEmail.txtSender.SelectionLength = Len(Trim(oProfile.EmailSender))
        frmEmail.txtDisplayName.SelectionLength = Len(Trim(oProfile.EmailDisplayName))
        frmEmail.txtAutoSender.SelectionLength = Len(Trim(oProfile.EmailAutoSender))
        frmEmail.txtAutoDisplayName.SelectionLength = Len(Trim(oProfile.EmailAutoDisplayName))
        frmEmail.txtAutoReceiver.SelectionLength = Len(Trim(oProfile.EmailAutoReceiver))
        frmEmail.txtReplyAdress.SelectionLength = Len(Trim(oProfile.EmailReplyAdress))
        frmEmail.txtSMTPHost.SelectionLength = Len(Trim(oProfile.EmailSMTPHost))
        ' new 13.05.03:
        If Len(Trim(oProfile.EmailSupport)) = 0 Then
            frmEmail.txtSupport.Text = "support@visualbanking.net"
        Else
            frmEmail.txtSupport.Text = Trim(oProfile.EmailSupport)
        End If
        frmEmail.txtCCSupport.Text = Trim(oProfile.EmailCCSupport)
        frmEmail.txtSupport.SelectionLength = Len(Trim(oProfile.EmailSupport))
        frmEmail.txtCCSupport.SelectionLength = Len(Trim(oProfile.EmailCCSupport))
        Fill_frmEmail = True
    End Function
    'Private Function MATCHFileType(iType As Integer) As String
    '' Find FileType for filenameout1,2,3,4 for Matchfiles
    'If oFilesetup.MATCHOutGL = iType Then
    '    MATCHFileType = "Hovedbok" 'LRS(
    'ElseIf oFilesetup.MATCHOutOCR = iType Then
    '    MATCHFileType = "OCR"
    'ElseIf oFilesetup.MATCHOutAutogiro = iType Then
    '    MATCHFileType = "Autogiro"
    'ElseIf oFilesetup.MATCHOutRest = iType Then
    '    MATCHFileType = "�vrige"   'lrs(
    'Else
    '    MATCHFileType = "FiletypeError!"
    'End If
    'End Function
    Private Sub Fill_frmDnB_TBIW_Accounts(ByRef oFilesetup As vbbabel.FileSetup)
        Dim oMyDAL As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iRecordCounter As Integer = 0
        'Dim listitem As New _MyListBoxItem

        oMyDAL = New vbBabel.DAL
        oMyDAL.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDAL.ConnectToDB() Then
            Throw New System.Exception(oMyDAL.ErrorMessage)
        End If

        'sMySQL = "SELECT A.Client_ID, A.Account_ID, A.Account, A.GLAccount, A.GLResultsAccount, A.ConvertedAccount, A.ContractNo, A.AvtaleGiroID, A.SWIFTAddr, A.AccountCountryCode, A.CurrencyCode FROM Account A, Client C, ClientFormat F WHERE C.Company_ID = " & oProfile.Company_ID.ToString & " AND F.FormatIn = " & oFilesetup.FileSetup_ID.ToString & " AND F.Client_ID = C.Client_ID AND F.Company_ID = C.Company_ID AND C.Client_ID = A.Client_ID AND C.Company_ID = A.Company_ID ORDER BY Account"
        ' XokNET 07.09.2011 Changed in SQL
        sMySQL = "SELECT A.Client_ID, A.Account_ID, A.Account, A.GLAccount, A.GLResultsAccount, A.ConvertedAccount, A.ContractNo, A.AvtaleGiroID, A.SWIFTAddr, A.AccountCountryCode, A.CurrencyCode, A.DebitAccountCountryCode FROM Account A, Client C, ClientFormat F WHERE C.Company_ID = " & Trim$(Str(oProfile.Company_ID)) & _
        " AND F.FormatIn =" & Str(oFilesetup.FileSetup_ID) & " AND F.Client_ID = C.Client_ID AND F.Company_ID = C.Company_ID AND C.Client_ID = A.Client_ID AND C.Company_ID = A.Company_ID ORDER BY Account"

        oMyDAL.SQL = sMySQL
        If oMyDAL.Reader_Execute() Then
            With frmDnB_TBIW_Accounts
                ' Fill accountslist
                .lstAccounts.Items.Clear()
                If oMyDAL.Reader_HasRows Then
                    Do While oMyDAL.Reader_ReadRecord
                        iRecordCounter = iRecordCounter + 1
                        ' XokNET 07.09.2011
                        '.lstAccounts.Items.Add(PadRight(oMyDAL.Reader_GetString("Account"), 100, " ") & Chr(9) & oMyDAL.Reader_GetString("GLAccount") & Chr(9) & oMyDAL.Reader_GetString("GLResultsAccount") & Chr(9) & oMyDAL.Reader_GetString("ConvertedAccount") & Chr(9) & oMyDAL.Reader_GetString("ContractNo") & Chr(9) & oMyDAL.Reader_GetString("AvtaleGiroID") & Chr(9) & oMyDAL.Reader_GetString("SWIFTAddr") & Chr(9) & oMyDAL.Reader_GetString("AccountCountryCode") & Chr(9) & oMyDAL.Reader_GetString("CurrencyCode") &  Chr(9) & oMyDAL.Reader_GetString("Client_ID"))
                        '.lstAccounts.Items.Add(PadRight(oMyDAL.Reader_GetString("Account"), 100, " ") & Chr(9) & oMyDAL.Reader_GetString("GLAccount") & Chr(9) & oMyDAL.Reader_GetString("GLResultsAccount") & Chr(9) & oMyDAL.Reader_GetString("ConvertedAccount") & Chr(9) & oMyDAL.Reader_GetString("ContractNo") & Chr(9) & oMyDAL.Reader_GetString("AvtaleGiroID") & Chr(9) & oMyDAL.Reader_GetString("SWIFTAddr") & Chr(9) & oMyDAL.Reader_GetString("AccountCountryCode") & Chr(9) & oMyDAL.Reader_GetString("CurrencyCode") & Chr(9) & oMyDAL.Reader_GetString("DebitAccountCountryCode") & Chr(9) & oMyDAL.Reader_GetString("Client_ID"))
                        'VB6.SetItemData(.lstAccounts, .lstAccounts.Items.Count - 1, CInt(oMyDAL.Reader_GetString("Account_ID")))
                        ' 11.07.2019
                        listitem = New _MyListBoxItem
                        listitem.ItemString = PadRight(oMyDAL.Reader_GetString("Account"), 100, " ") & Chr(9) & oMyDAL.Reader_GetString("GLAccount") & Chr(9) & oMyDAL.Reader_GetString("GLResultsAccount") & Chr(9) & oMyDAL.Reader_GetString("ConvertedAccount") & Chr(9) & oMyDAL.Reader_GetString("ContractNo") & Chr(9) & oMyDAL.Reader_GetString("AvtaleGiroID") & Chr(9) & oMyDAL.Reader_GetString("SWIFTAddr") & Chr(9) & oMyDAL.Reader_GetString("AccountCountryCode") & Chr(9) & oMyDAL.Reader_GetString("CurrencyCode") & Chr(9) & oMyDAL.Reader_GetString("DebitAccountCountryCode") & Chr(9) & oMyDAL.Reader_GetString("Client_ID")
                        listitem.ItemData = CInt(oMyDAL.Reader_GetString("Account_ID"))
                        .lstAccounts.Items.Add(listitem)

                    Loop
                    If iRecordCounter = 1 Then
                        .lstAccounts.SelectedIndex = 0
                    Else
                        .lstAccounts.SelectedIndex = -1
                    End If

                Else
                    iRecordCounter = 0
                    .lstAccounts.SelectedIndex = -1
                End If
            End With
        Else
            Throw New Exception(LRSCommon(45002) & oMyDAL.ErrorMessage)
        End If

        If Not oMyDAL Is Nothing Then
            oMyDAL.Close()
            oMyDAL = Nothing
        End If

    End Sub
    Private Sub FillAvailableReports(ByVal lst As ComboBox)
        Dim stmp As String
        lst.Items.Clear()

        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(1), 1))
        ''stmp = VB6.GetItemData(lst, 0)
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(2), 2))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(3), 3))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(4), 4))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(5), 5))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(7), 7))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(11), 11))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(12), 12))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(503), 503))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(504), 504))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(505), 505))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(506), 506))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(507), 507))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(508), 508))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(509), 509))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(510), 510))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(512), 512))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(603), 603))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(912), 912))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(950), 950))
        'lst.Items.Add(New VB6.ListBoxItem(FindReportName(990), 990))
        ' 15.07.2019

        lst.Items.Add(New _MyListBoxItem(FindReportName(1), 1))
        lst.Items.Add(New _MyListBoxItem(FindReportName(2), 2))
        lst.Items.Add(New _MyListBoxItem(FindReportName(3), 3))
        lst.Items.Add(New _MyListBoxItem(FindReportName(4), 4))
        lst.Items.Add(New _MyListBoxItem(FindReportName(5), 5))
        lst.Items.Add(New _MyListBoxItem(FindReportName(7), 7))
        lst.Items.Add(New _MyListBoxItem(FindReportName(11), 11))
        lst.Items.Add(New _MyListBoxItem(FindReportName(12), 12))
        lst.Items.Add(New _MyListBoxItem(FindReportName(503), 503))
        lst.Items.Add(New _MyListBoxItem(FindReportName(504), 504))
        lst.Items.Add(New _MyListBoxItem(FindReportName(505), 505))
        lst.Items.Add(New _MyListBoxItem(FindReportName(506), 506))
        lst.Items.Add(New _MyListBoxItem(FindReportName(507), 507))
        lst.Items.Add(New _MyListBoxItem(FindReportName(508), 508))
        lst.Items.Add(New _MyListBoxItem(FindReportName(509), 509))
        lst.Items.Add(New _MyListBoxItem(FindReportName(510), 510))
        lst.Items.Add(New _MyListBoxItem(FindReportName(512), 512))
        lst.Items.Add(New _MyListBoxItem(FindReportName(603), 603))
        lst.Items.Add(New _MyListBoxItem(FindReportName(912), 912))
        lst.Items.Add(New _MyListBoxItem(FindReportName(950), 950))
        lst.Items.Add(New _MyListBoxItem(FindReportName(990), 990))
        lst.Items.Add(New _MyListBoxItem(FindReportName(1001), 1001))

    End Sub
    Public Function FindReportName(ByVal iNo As Integer) As String

        Select Case iNo
            Case 1
                FindReportName = LRS(60262) & " (001)"
            Case 2
                FindReportName = LRS(60263) & " (002)" ' Payments
            Case 3
                FindReportName = LRS(60264) & " (003)" ' Invoicelevel
            Case 4
                FindReportName = LRS(60322) & " (004)" ' IncomingOCR
            Case 5
                FindReportName = LRS(60323) & " (005)" ' IncomingFIK
            Case 7
                FindReportName = "Incoming ACH/Lockbox" & " (007)" ' Incoming ACH/Lockbox (US)
            Case 11
                FindReportName = LRS(60265) & " (011)" ' Rejected payments
            Case 12
                FindReportName = "CONTRL" & " (012)" ' CONTRL-report
            Case 503
                FindReportName = LRS(60259) & " (503)" ' Incoming payments, Unmatched
            Case 504
                FindReportName = LRS(60260) & " (504)" ' Incoming payments, Matched
            Case 505
                FindReportName = LRS(60261) & " (505)" ' Incoming payments, Control
            Case 506
                FindReportName = LRS(60368) & " (506)" ' Matching, summary
            Case 507
                FindReportName = LRS(60369) & " (507)" ' Matching, aKonto
            Case 508
                FindReportName = LRS(60419) & " (508)" ' Matching, Hovedbok
            Case 509
                FindReportName = LRS(60430) & " (509)" ' OpenItems
            Case 510
                FindReportName = LRS(60431) & " (510)" ' Day Totals
            Case 512
                FindReportName = LRS(60623) & " (512)" ' DayTotals Aggregated Added 24.04.2014 for Visma Collectors
            Case 602
                FindReportName = LRS(60390) & " (602)" ' Nattsafe, differanse
            Case 603
                FindReportName = LRS(60595) & " (603)" ' AML Rapport
            Case 912
                FindReportName = LRS(60550) & " (912)" ' Filtered (removed) payments
            Case 950
                FindReportName = Replace(LRS(60137), "&", "") & " (950)" ' Special
            Case 990
                FindReportName = LRS(60037) & " (990)" ' Clientslist
            Case 1001
                FindReportName = LRS(60690) & " (1001)" ' Incoming KID Payments, matched - Added 21.04.2020
            Case Else
                FindReportName = "Report: " & iNo.ToString & " is not available!"
        End Select
    End Function
    Private Sub ReportSetupAndShow()
        If frmWiz_Report Is Nothing Then
            frmWiz_Report = New frmWiz_Report
        End If
        If frmReportAdvanced Is Nothing Then
            frmReportAdvanced = New frmReportAdvanced
            FillAvailableReports(frmReportAdvanced.lstReport)
        End If
        If frmReportPrinter Is Nothing Then
            frmReportPrinter = New frmReportPrinter
        End If
        If frmReportSQL Is Nothing Then
            frmReportSQL = New frmReportSQL
        End If
        frmWiz_Report.SetfrmReportAdvanced(frmReportAdvanced)
        frmWiz_Report.PassfrmReportPrinter(frmReportPrinter)


        With frmWiz_Report.lstReport
            ' Fill in selected reports only
            .Items.Clear()
            For Each oReport In oFilesetup.Reports
                If EmptyString(oReport.ReportName) Then
                    .Items.Add(New _MyListBoxItem(FindReportName(oReport.ReportNo), oReport.Index))
                Else
                    .Items.Add(New _MyListBoxItem(Trim(oReport.ReportName), oReport.Index))
                End If
            Next
            ' at this stage we normally have the oReport object we want.
            ' but if no report preselected, then create an object
            If oReport Is Nothing Then
                oReport = New vbBabel.Report
            End If

        End With

        frmReportAdvanced.lstSorting.Items.Clear()
        frmReportAdvanced.lstSorting.Items.Add(LRS(60563))   '"LRS-Ingen sortering/Ingen brudd") 'NOBREAK
        frmReportAdvanced.lstSorting.Items.Add(LRS(60564))   '"LRS-Kontonummer, dato") 'BREAK_ON_ACCOUNT (and per day)
        frmReportAdvanced.lstSorting.Items.Add(LRS(60565))   '"LRS-Kontosummering")BREAK_ON_BATCH
        frmReportAdvanced.lstSorting.Items.Add(LRS(60566))   '"LRS-Pr. betaling")BREAK_ON_PAYMENT
        frmReportAdvanced.lstSorting.Items.Add(LRS(60567))   '"LRS-Kontonummer")BREAK_ON_ACCOUNTONLY
        frmReportAdvanced.lstSorting.Items.Add(LRS(60568))  '"LRS-Klientnummer")BREAK_ON_CLIENT
        frmReportAdvanced.lstSorting.Items.Add(LRS(60626))  '"LRS-Pr. fil")BREAK_ON_FILE


        frmReportAdvanced.lstReportWhen.Items.Clear()
        'frmReportAdvanced.lstReportWhen.Items.Add(New VB6.ListBoxItem(LRS(60569), 1000))   '"LRS-Etter import", 1000))
        'frmReportAdvanced.lstReportWhen.Items.Add(New VB6.ListBoxItem(LRS(60570), 1100))   '"LRS-I spesialrutiner", 1100))
        'frmReportAdvanced.lstReportWhen.Items.Add(New VB6.ListBoxItem(LRS(60571), 1200))   '"LRS-Etter spesialrutiner", 1200))
        'frmReportAdvanced.lstReportWhen.Items.Add(New VB6.ListBoxItem(LRS(60572), 2000))  '"LRS-Etter OCR eksport", 2000))
        'frmReportAdvanced.lstReportWhen.Items.Add(New VB6.ListBoxItem(LRS(60573), 2100)) '"LRS-Etter Autogiro eksport", 2100))
        'frmReportAdvanced.lstReportWhen.Items.Add(New VB6.ListBoxItem(LRS(60574), 4000))  '"LRS-F�r eksport", 4000))
        'frmReportAdvanced.lstReportWhen.Items.Add(New VB6.ListBoxItem(LRS(60575), 4010)) '"LRS-Etter eksport", 4010))
        'frmReportAdvanced.lstReportWhen.Items.Add(New VB6.ListBoxItem(LRS(60622), 5000)) '"LRS-At the end", 5000))
        ' 12.07.2019
        frmReportAdvanced.lstReportWhen.Items.Add(New _MyListBoxItem(LRS(60569), 1000))   '"LRS-Etter import", 1000))
        frmReportAdvanced.lstReportWhen.Items.Add(New _MyListBoxItem(LRS(60570), 1100))   '"LRS-I spesialrutiner", 1100))
        frmReportAdvanced.lstReportWhen.Items.Add(New _MyListBoxItem(LRS(60571), 1200))   '"LRS-Etter spesialrutiner", 1200))
        frmReportAdvanced.lstReportWhen.Items.Add(New _MyListBoxItem(LRS(60572), 2000))  '"LRS-Etter OCR eksport", 2000))
        frmReportAdvanced.lstReportWhen.Items.Add(New _MyListBoxItem(LRS(60573), 2100)) '"LRS-Etter Autogiro eksport", 2100))
        frmReportAdvanced.lstReportWhen.Items.Add(New _MyListBoxItem(LRS(60574), 4000))  '"LRS-F�r eksport", 4000))
        frmReportAdvanced.lstReportWhen.Items.Add(New _MyListBoxItem(LRS(60575), 4010)) '"LRS-Etter eksport", 4010))
        frmReportAdvanced.lstReportWhen.Items.Add(New _MyListBoxItem(LRS(60622), 5000)) '"LRS-At the end", 5000))

        '20.11.2018 - Added next
        frmReportAdvanced.lstReportWhen.Items.Add(New _MyListBoxItem(LRS(60688), 9999)) '"Customer specific", 9999))

        frmReportAdvanced.PassfrmReportPrinter(frmReportPrinter)
        frmReportAdvanced.PassfrmReportSQL(frmReportSQL)
        frmWiz_Report.PassfrmReportSQL(frmReportSQL)
        frmWiz_Report.PassFilesetup(oFilesetup)
        'frmWiz_Report.ShowDialog()

    End Sub
End Module
