<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSignalFile
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents txtFilename As System.Windows.Forms.TextBox
	Public WithEvents cmdFileOpen As System.Windows.Forms.Button
	Public WithEvents chkActivate As System.Windows.Forms.CheckBox
	Public WithEvents lblExplaination As System.Windows.Forms.Label
    Public WithEvents lblFilename As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSignalFile))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.txtFilename = New System.Windows.Forms.TextBox
        Me.cmdFileOpen = New System.Windows.Forms.Button
        Me.chkActivate = New System.Windows.Forms.CheckBox
        Me.lblExplaination = New System.Windows.Forms.Label
        Me.lblFilename = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.lblFilenameError = New System.Windows.Forms.Label
        Me.txtFilenameError = New System.Windows.Forms.TextBox
        Me.cmdFileOpenError = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(266, 223)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 30)
        Me.cmdOK.TabIndex = 5
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(355, 223)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 30)
        Me.cmdCancel.TabIndex = 4
        Me.cmdCancel.Text = "55002 &Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'txtFilename
        '
        Me.txtFilename.AcceptsReturn = True
        Me.txtFilename.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilename.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilename.Enabled = False
        Me.txtFilename.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilename.Location = New System.Drawing.Point(98, 150)
        Me.txtFilename.MaxLength = 0
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilename.Size = New System.Drawing.Size(309, 20)
        Me.txtFilename.TabIndex = 2
        '
        'cmdFileOpen
        '
        Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen.Enabled = False
        Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
        Me.cmdFileOpen.Location = New System.Drawing.Point(415, 150)
        Me.cmdFileOpen.Name = "cmdFileOpen"
        Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen.TabIndex = 1
        Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen.UseVisualStyleBackColor = False
        '
        'chkActivate
        '
        Me.chkActivate.BackColor = System.Drawing.SystemColors.Control
        Me.chkActivate.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkActivate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkActivate.Location = New System.Drawing.Point(12, 120)
        Me.chkActivate.Name = "chkActivate"
        Me.chkActivate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkActivate.Size = New System.Drawing.Size(165, 20)
        Me.chkActivate.TabIndex = 0
        Me.chkActivate.Text = "Aktiver signalfil"
        Me.chkActivate.UseVisualStyleBackColor = False
        '
        'lblExplaination
        '
        Me.lblExplaination.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplaination.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplaination.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplaination.Location = New System.Drawing.Point(9, 58)
        Me.lblExplaination.Name = "lblExplaination"
        Me.lblExplaination.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplaination.Size = New System.Drawing.Size(408, 61)
        Me.lblExplaination.TabIndex = 6
        Me.lblExplaination.Text = "Explain the use of Signalfiles"
        '
        'lblFilename
        '
        Me.lblFilename.BackColor = System.Drawing.SystemColors.Control
        Me.lblFilename.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilename.Enabled = False
        Me.lblFilename.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilename.Location = New System.Drawing.Point(11, 155)
        Me.lblFilename.Name = "lblFilename"
        Me.lblFilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilename.Size = New System.Drawing.Size(80, 16)
        Me.lblFilename.TabIndex = 3
        Me.lblFilename.Text = "60057 Filnavn"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(16, 219)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(420, 1)
        Me.lblLine1.TabIndex = 79
        Me.lblLine1.Text = "Label1"
        '
        'lblFilenameError
        '
        Me.lblFilenameError.BackColor = System.Drawing.SystemColors.Control
        Me.lblFilenameError.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilenameError.Enabled = False
        Me.lblFilenameError.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilenameError.Location = New System.Drawing.Point(11, 180)
        Me.lblFilenameError.Name = "lblFilenameError"
        Me.lblFilenameError.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilenameError.Size = New System.Drawing.Size(80, 16)
        Me.lblFilenameError.TabIndex = 80
        Me.lblFilenameError.Text = "60694 Fil error"
        '
        'txtFilenameError
        '
        Me.txtFilenameError.AcceptsReturn = True
        Me.txtFilenameError.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilenameError.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilenameError.Enabled = False
        Me.txtFilenameError.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilenameError.Location = New System.Drawing.Point(98, 175)
        Me.txtFilenameError.MaxLength = 0
        Me.txtFilenameError.Name = "txtFilenameError"
        Me.txtFilenameError.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilenameError.Size = New System.Drawing.Size(309, 20)
        Me.txtFilenameError.TabIndex = 81
        '
        'cmdFileOpenError
        '
        Me.cmdFileOpenError.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpenError.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpenError.Enabled = False
        Me.cmdFileOpenError.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpenError.Image = CType(resources.GetObject("cmdFileOpenError.Image"), System.Drawing.Image)
        Me.cmdFileOpenError.Location = New System.Drawing.Point(415, 175)
        Me.cmdFileOpenError.Name = "cmdFileOpenError"
        Me.cmdFileOpenError.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpenError.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpenError.TabIndex = 82
        Me.cmdFileOpenError.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpenError.UseVisualStyleBackColor = False
        '
        'frmSignalFile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(451, 256)
        Me.Controls.Add(Me.cmdFileOpenError)
        Me.Controls.Add(Me.txtFilenameError)
        Me.Controls.Add(Me.lblFilenameError)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.txtFilename)
        Me.Controls.Add(Me.cmdFileOpen)
        Me.Controls.Add(Me.chkActivate)
        Me.Controls.Add(Me.lblExplaination)
        Me.Controls.Add(Me.lblFilename)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmSignalFile"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Signalfil"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents lblFilenameError As System.Windows.Forms.Label
    Public WithEvents txtFilenameError As System.Windows.Forms.TextBox
    Public WithEvents cmdFileOpenError As System.Windows.Forms.Button
#End Region 
End Class
