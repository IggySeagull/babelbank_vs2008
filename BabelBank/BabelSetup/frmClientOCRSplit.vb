Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
'**********************************************************************************
' This form is seldom or never been used -
' That's why we have not yet bodered to swap from Spread to Grid,
' Instead commented out references to Spread
'**********************************************************************************
Friend Class frmClientOCRsplit
	Inherits System.Windows.Forms.Form
	Dim iCurrentRow As Integer
	Private Sub frmClientOCRsplit_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		FormvbStyle(Me, "")
	End Sub
	
	Private Sub cmdPrint_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPrint.Click
		' test
        'PrintClientsOCRSplit()
		
	End Sub
    'Sub sprClients_LeaveCell(ByVal eventSender As System.Object, ByVal eventArgs As AxFPSpreadADO._DSpreadEvents_LeaveCellEvent) Handles sprClients.LeaveCell
    '	' fires when we are leaving a cell.
    '	' Keep track of which one is the new row:
    '	With Me.sprClients

    '		If eventArgs.NewRow > eventArgs.row Then
    '			' skipped to new row, move cursor to col 3, because 1 and 2 are static (no edit)
    '			eventArgs.NewCol = 3
    '			iCurrentRow = eventArgs.NewRow
    '		End If
    '	End With

    'End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		'If MsgBox("Vil du avslutte uten lagring ?", vbYesNo + vbDefaultButton2, "Klienter") = vbYes Then
		If MsgBox(LRS(60011), MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, LRS(60016)) = MsgBoxResult.Yes Then
			' Cancel, Yes
			ClientOCRsplit.bOCRSplitStatus = False
			Me.Close()
		Else
			' No Cancel, Continue
			
		End If
		
	End Sub
	Private Sub frmClientOCRsplit_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
			If MsgBox(LRS(60011), MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, LRS(60016)) = MsgBoxResult.Yes Then
				' End form without saving
				ClientOCRsplit.bOCRSplitStatus = False
				Cancel = False
			Else
				' Save first! Do not end form
				Cancel = True
			End If
		End If
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmdSave_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSave.Click
		
        'Dim oFilesetup As Object 'FileSetup
        'Dim oClient As Object 'Client
        'Dim sClientNo As String
        'Dim sClientName As String
        'Dim sCompanyID As String
        'Dim iStart As Short
        'Dim iLength As Short
        'Dim sValue As String
        'Dim i As Short
        '' added 14.04.2010
        'Dim oClientSplit As ClientSplit
        'sCompanyID = "1"
        'ClientOCRsplit.bOCRSplitStatus = True
		
		
        'With Me.sprClients

        '	For i = 1 To .MaxRows

        '		.col = 1
        '		.row = i
        '		sClientNo = .Text

        '		.col = 2
        '		sClientName = .Text

        '		.col = 3
        '		If Not IsNumeric(.Text) Then
        '			iStart = -1
        '		Else
        '			iStart = Val(.Text)
        '		End If

        '		.col = 4
        '		If Not IsNumeric(.Text) Then
        '			iLength = -1
        '		Else
        '			iLength = Val(.Text)
        '		End If

        '		.col = 5
        '		sValue = .Text

        '		' Prefix with = if no sign given;
        '		If VB.Left(sValue, 1) <> "=" And VB.Left(sValue, 1) <> "<" And VB.Left(sValue, 1) <> ">" Then
        '			sValue = "=" & sValue
        '		End If

        '		'Update in oProfile, for this client, in ALL FileSetups
        '		'For Each oFilesetup In Clients.oProfile.FileSetups
        '		' Changed 14.04.2010 after having deleted Client.Bas due to upgrade to DotNet
        '		'UPGRADE_WARNING: Couldn't resolve default property of object oClientSplit.Profile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '		For	Each oFilesetup In oClientSplit.Profile.FileSetups
        '			'UPGRADE_WARNING: Couldn't resolve default property of object oFilesetup.Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '			For	Each oClient In oFilesetup.Clients

        '				'UPGRADE_WARNING: Couldn't resolve default property of object oClient.ClientNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '				If oClient.ClientNo = sClientNo Then
        '					' Update in profile;
        '					'UPGRADE_WARNING: Couldn't resolve default property of object oClient.KIDStart. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '					oClient.KIDStart = iStart
        '					'UPGRADE_WARNING: Couldn't resolve default property of object oClient.KIDLength. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '					oClient.KIDLength = iLength
        '					'UPGRADE_WARNING: Couldn't resolve default property of object oClient.KIDValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '					oClient.KIDValue = sValue

        '					'UPGRADE_WARNING: Couldn't resolve default property of object oClient.Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '					oClient.Status = vbBabel.Profile.CollectionStatus.Changed
        '					'UPGRADE_WARNING: Couldn't resolve default property of object oFilesetup.Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '					oFilesetup.Status = vbBabel.Profile.CollectionStatus.ChangeUnder
        '					'Clients.oProfile.Status = ChangeUnder
        '					' 14.04.2010 changed due to DotNet
        '					'UPGRADE_WARNING: Couldn't resolve default property of object oClientSplit.Profile.Status. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '					oClientSplit.Profile.Status = vbBabel.Profile.CollectionStatus.ChangeUnder
        '				End If
        '			Next oClient
        '		Next oFilesetup


        '	Next i

        'End With

        If ClientOCRsplit.bOCRSplitStatus = True Then
            Me.Close()
        End If
		
	End Sub
	
    '	Sub sprClients_EditModeEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxFPSpreadADO._DSpreadEvents_EditModeEvent) Handles sprClients.EditModeEvent


    '		With sprClients

    '			If eventArgs.Mode <> 1 And eventArgs.col = .MaxCols Then
    '                '.eventArgs.col = 3 ' Skip first two colsm no edit there ..
    '                '.eventArgs.row = .eventArgs.row + 1
    '                '.Action = FPSpreadADO.ActionConstants.ActionActiveCell

    '			End If

    '		End With

    '	End Sub
End Class
