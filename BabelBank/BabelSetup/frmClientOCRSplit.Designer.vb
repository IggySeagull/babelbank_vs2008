<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmClientOCRsplit
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    'Public WithEvents sprPrint As AxFPSpreadADO.AxfpSpread
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdSave As System.Windows.Forms.Button
	Public WithEvents cmdPrint As System.Windows.Forms.Button
    Public WithEvents lblValue As System.Windows.Forms.Label
	Public WithEvents lblLength As System.Windows.Forms.Label
	Public WithEvents lblStart As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClientOCRsplit))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        'Me.sprPrint = New AxFPSpreadADO.AxfpSpread
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdSave = New System.Windows.Forms.Button
        Me.cmdPrint = New System.Windows.Forms.Button
        Me.lblValue = New System.Windows.Forms.Label
        Me.lblLength = New System.Windows.Forms.Label
        Me.lblStart = New System.Windows.Forms.Label
        'CType(Me.sprPrint, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sprPrint
        '
        'Me.sprPrint.DataSource = Nothing
        'Me.sprPrint.Location = New System.Drawing.Point(0, 432)
        'Me.sprPrint.Name = "sprPrint"
        'Me.sprPrint.OcxState = CType(resources.GetObject("sprPrint.OcxState"), System.Windows.Forms.AxHost.State)
        'Me.sprPrint.Size = New System.Drawing.Size(33, 33)
        'Me.sprPrint.TabIndex = 4
        'Me.sprPrint.Visible = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(304, 428)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(105, 25)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "&Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdSave
        '
        Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Location = New System.Drawing.Point(196, 428)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSave.Size = New System.Drawing.Size(105, 25)
        Me.cmdSave.TabIndex = 2
        Me.cmdSave.Text = "&Lagre"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'cmdPrint
        '
        Me.cmdPrint.BackColor = System.Drawing.SystemColors.Control
        Me.cmdPrint.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdPrint.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPrint.Location = New System.Drawing.Point(88, 428)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdPrint.Size = New System.Drawing.Size(105, 25)
        Me.cmdPrint.TabIndex = 1
        Me.cmdPrint.Text = "&Utskrift"
        Me.cmdPrint.UseVisualStyleBackColor = False
        '
        'lblValue
        '
        Me.lblValue.BackColor = System.Drawing.SystemColors.Control
        Me.lblValue.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblValue.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblValue.Location = New System.Drawing.Point(24, 408)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblValue.Size = New System.Drawing.Size(737, 17)
        Me.lblValue.TabIndex = 7
        Me.lblValue.Text = "Verdi: Angi verdien for aktuell klient. Bruk evt. < > = i tilegg til verdi"
        '
        'lblLength
        '
        Me.lblLength.BackColor = System.Drawing.SystemColors.Control
        Me.lblLength.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLength.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLength.Location = New System.Drawing.Point(24, 384)
        Me.lblLength.Name = "lblLength"
        Me.lblLength.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLength.Size = New System.Drawing.Size(737, 17)
        Me.lblLength.TabIndex = 6
        Me.lblLength.Text = "Lengde: Angi lengde for KID, eller lengde for den del av KID som skal leses "
        '
        'lblStart
        '
        Me.lblStart.BackColor = System.Drawing.SystemColors.Control
        Me.lblStart.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblStart.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblStart.Location = New System.Drawing.Point(24, 360)
        Me.lblStart.Name = "lblStart"
        Me.lblStart.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblStart.Size = New System.Drawing.Size(737, 17)
        Me.lblStart.TabIndex = 5
        Me.lblStart.Text = "Start: Angi startposisjon i KID (Hvis denne benyttes). Benyttes ikke ved splittin" & _
            "g etter KID-lengde "
        '
        'frmClientOCRsplit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(491, 463)
        'Me.Controls.Add(Me.sprPrint)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.cmdPrint)
        Me.Controls.Add(Me.lblValue)
        Me.Controls.Add(Me.lblLength)
        Me.Controls.Add(Me.lblStart)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmClientOCRsplit"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Klienter"
        'CType(Me.sprPrint, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class
