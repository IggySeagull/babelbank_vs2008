Option Strict Off
Option Explicit On

Friend Class frmReportAdvanced
    Inherits System.Windows.Forms.Form
    ' 0, NOBREAK          : No pagebreaks (only on full page)
    ' 1, BREAK_ON_ACCOUNT : Break on productiondate + CreditAccount
    ' 2, BREAK_ON_BATCH   : Break on productiondate + CreditAccount + iBatches
    ' 3, BREAK_ON_PAYMENT : Break on productiondate + CreditAccount + iBatches + iPayments
    ' 1, BREAK_ON_ACCOUNTONLY : CreditAccount
    Dim nLastItem As Integer
    Private frmReportPrinter As frmReportPrinter
    Private frmReportSQL As frmReportSQL
    Private Sub frmReportAdvanced_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim iReportNo As Integer
        FormvbStyle(Me, "dollar.jpg", 400)
        FormLRSCaptions(Me)

        'iReportNo = VB6.GetItemData(Me.lstReport, Me.lstReport.SelectedIndex)
        ' 12.07.2019
        iReportNo = Me.lstReport.Items(Me.lstReport.SelectedIndex).itemdata

        ' Prepore setup with settings for each individual report;

        ' Report on saved data or collections ?
        ' -------------------------------------
        Select Case iReportNo
            Case 510, 512, 12 '16.05.2017 - Added 12
                ' Report on database only;
                ' - 12, 510, 512
                Me.chkReportOnDatabase.Enabled = False
                Me.chkSaveBeforeReport.Enabled = False
                Me.chkReportOnDatabase.Checked = True
                Me.chkSaveBeforeReport.Checked = True


            Case 912, 990, 1001 '16.05.2017 - Removed 12
                ' report in collections only;
                ' - 912
                ' - 990
                Me.chkReportOnDatabase.Enabled = False
                Me.chkSaveBeforeReport.Enabled = False
                Me.chkReportOnDatabase.Checked = False
                Me.chkSaveBeforeReport.Checked = False

            Case Else
                Me.chkReportOnDatabase.Enabled = True
                Me.chkSaveBeforeReport.Enabled = True

        End Select

        ' Include OCR
        ' -----------
        'Rapport 004, 005, 012, 505, 510 og 512 fjern valg om inkluder OCR
        If iReportNo = 4 Or iReportNo = 5 Or iReportNo = 12 Or iReportNo = 505 Or iReportNo = 510 Or iReportNo = 512 Or iReportNo = 1001 Then
            Me.chkIncludeKID.Enabled = False
            Me.chkIncludeKID.Checked = True  ' always IncludeKID on
        Else
            Me.chkIncludeKID.Enabled = True
        End If

        ' Break
        ' -----
        If iReportNo = 12 Or iReportNo = 506 Or iReportNo = 952 Then
            Me.lstSorting.SelectedIndex = 0  ' = No break
            Me.lstSorting.Enabled = False
        Else
            Me.lstSorting.Enabled = True
        End If


    End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Me.DialogResult = 0
        Me.Hide()
    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.DialogResult = 1
        Me.Hide()
    End Sub
    Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
        ' Hent filnavn
        Dim spath As String
        Dim sFile As String
        ' Keep filename after Browsing for folder;
        ' Must have \ and .
        If InStr(Me.txtFilename.Text, "\") > 0 And InStr(Me.txtFilename.Text, ".") > 0 Then
            sFile = Mid(Me.txtFilename.Text, InStrRev(Me.txtFilename.Text, "\"))
        ElseIf InStr(Me.txtFilename.Text, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & Me.txtFilename.Text
        Else
            sFile = ""
        End If

        '31.12.2010
        spath = BrowseForFilesOrFolders(Me.txtFilename.Text, Me, LRS(60010), True)

        If Len(spath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(spath, ".") = 0 Then
                Me.txtFilename.Text = Replace(spath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                Me.txtFilename.Text = spath
            End If
        End If
    End Sub
    Public Sub PassfrmReportPrinter(ByVal frm As frmReportPrinter)
        ' pass frmreportprinter to frmWiz_ReportAdvanced
        frmReportPrinter = frm
    End Sub
    Public Sub PassfrmReportSQL(ByVal frm As frmReportSQL)
        ' pass frmreportsql to frmWiz_ReportAdvanced
        frmReportSQL = frm
    End Sub
    Private Sub chkExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExport.Click
        ShowExportControls(Me.chkExport.Checked)
    End Sub
    Public Sub ShowExportControls(ByVal bOn As Boolean)
        If bOn Then
            Me.optHTML.Visible = True
            Me.optPDF.Visible = True
            Me.optRTF.Visible = True
            Me.optTIFF.Visible = True
            Me.optTXT.Visible = True
            Me.optXLS.Visible = True
            Me.lblFilename.Visible = True
            Me.txtFilename.Visible = True
            Me.cmdFileOpen.Visible = True
            If Me.optHTML.Checked = False And Me.optPDF.Checked = False And Me.optRTF.Checked = False And _
                Me.optTIFF.Checked = False And Me.optTXT.Checked = False And Me.optXLS.Checked = False Then
                ' must mark on, set default to PDF
                Me.optPDF.Checked = True
            End If
        Else
            Me.optHTML.Visible = False
            Me.optPDF.Visible = False
            Me.optRTF.Visible = False
            Me.optTIFF.Visible = False
            Me.optTXT.Visible = False
            Me.optXLS.Visible = False
            Me.lblFilename.Visible = False
            Me.txtFilename.Visible = False
            Me.cmdFileOpen.Visible = False
        End If


    End Sub
    Private Sub cmdPrinter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrinter.Click
        ' Fill frmReportPrinter
        'Dim oPrinter As Printer
        Dim pkInstalledPrinters As String = ""
        Dim i As Integer = 0

        frmReportPrinter.chkSelectPrinter.Checked = oReport.AskForPrinter

        ' List all available printers
        frmReportPrinter.lstPrinters.Items.Clear()
        For i = 0 To System.Drawing.Printing.PrinterSettings.InstalledPrinters.Count - 1
            pkInstalledPrinters = System.Drawing.Printing.PrinterSettings.InstalledPrinters.Item(i)
            frmReportPrinter.lstPrinters.Items.Add(pkInstalledPrinters)
            If Trim(oReport.PrinterName) = pkInstalledPrinters Then
                frmReportPrinter.lstPrinters.SelectedIndex = frmReportPrinter.lstPrinters.Items.Count - 1
            End If

        Next

        'frmReportPrinter.lstPrinters.Items.Clear()
        'If Printers.Count > 0 Then
        '    For Each oPrinter In Printers
        '        frmReportPrinter.lstPrinters.Items.Add((oPrinter.DeviceName))
        '        If Trim(oReport.PrinterName) = oPrinter.DeviceName Then
        '            frmReportPrinter.lstPrinters.SelectedIndex = frmReportPrinter.lstPrinters.Items.Count - 1
        '        End If
        '    Next oPrinter
        ' Add a default line to be able to turn off preselected printer!
        frmReportPrinter.lstPrinters.Items.Add("Default")
        If frmReportPrinter.lstPrinters.SelectedIndex < 0 Then
            frmReportPrinter.lstPrinters.SelectedIndex = frmReportPrinter.lstPrinters.Items.Count - 1
        End If
        If Me.chkPrint.Checked Then
            Me.cmdPrinter.Enabled = True
        Else
            Me.cmdPrinter.Enabled = False
        End If
        'End If

        frmReportPrinter.ShowDialog()
    End Sub
    Private Sub chkPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPrint.Click
        If chkPrint.Checked Then
            Me.cmdPrinter.Enabled = True
        Else
            Me.cmdPrinter.Enabled = False
        End If
    End Sub
    Private Sub cmdSQL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSQL.Click
        'frmReportSQL.PassReportNo(VB6.GetItemData(Me.lstReport, Me.lstReport.SelectedIndex))
        'frmReportSQL.PassSortOrder(VB6.GetItemData(Me.lstSorting, Me.lstSorting.SelectedIndex))
        ' 12.07.2019
        frmReportSQL.PassReportNo(Me.lstReport.Items(Me.lstReport.SelectedIndex).itemdata)
        frmReportSQL.PassSortOrder(Me.lstSorting.SelectedIndex)   ' 12.07.2019 - endret, men tror vi bruker SelectedIndex, ikke Itemdata

        frmReportSQL.PassIncludeOCR(Me.chkIncludeKID.Checked)
        frmReportSQL.PassSavebeforeReport(Me.chkSaveBeforeReport.Checked)

        frmReportSQL.ShowDialog()
    End Sub

    Private Sub lstReport_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstReport.SelectedIndexChanged
        Dim iReportNo As Integer

        'iReportNo = VB6.GetItemData(Me.lstReport, Me.lstReport.SelectedIndex)
        ' 12.07.2019
        iReportNo = Me.lstReport.Items(Me.lstReport.SelectedIndex).itemdata

        ' Prepore setup with settings for each individual report;

        ' Report on saved data or collections ?
        ' -------------------------------------
        Select Case iReportNo '18.05.2017 - Added 12
            Case 510, 512, 12
                ' Report on database only;
                ' - 12, 510, 512
                Me.chkReportOnDatabase.Enabled = False
                Me.chkSaveBeforeReport.Enabled = False
                Me.chkReportOnDatabase.Checked = True
                Me.chkSaveBeforeReport.Checked = True

            Case 912, 990, 1001 '18.05.2017 - Removed 12
                ' report in collections only;
                ' - 912
                ' - 990
                Me.chkReportOnDatabase.Visible = False
                Me.chkSaveBeforeReport.Visible = False
                Me.chkReportOnDatabase.Checked = False
                Me.chkSaveBeforeReport.Checked = False

                If iReportNo = 912 Then
                    ' Only "In specialroutines
                    Me.lstReportWhen.SelectedIndex = 1
                End If


            Case Else
                Me.chkReportOnDatabase.Visible = True
                Me.chkSaveBeforeReport.Visible = True

        End Select


        ' Include OCR
        ' -----------
        'Rapport 004, 005, 012, 505, 510 og 512 fjern valg om inkluder OCR
        If iReportNo = 4 Or iReportNo = 5 Or iReportNo = 12 Or iReportNo = 505 Or iReportNo = 510 Or iReportNo = 512 Or iReportNo = 1001 Then
            Me.chkIncludeKID.Enabled = False
            Me.chkIncludeKID.Checked = True  ' always IncludeKID on
        Else
            Me.chkIncludeKID.Enabled = True
        End If

        ' Break
        ' -----
        If iReportNo = 12 Or iReportNo = 506 Or iReportNo = 952 Then
            Me.lstSorting.SelectedIndex = 0  ' = No break
            Me.lstSorting.Enabled = False
        Else
            Me.lstSorting.Enabled = True
        End If

    End Sub

    Private Sub chkReportOnDatabase_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkReportOnDatabase.CheckedChanged
        If Me.chkReportOnDatabase.Checked Then
            Me.chkSaveBeforeReport.Visible = True
        Else
            Me.chkSaveBeforeReport.Visible = False
            Me.chkSaveBeforeReport.Checked = False
        End If
    End Sub

    Private Sub chkeMail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkeMail.CheckedChanged
        If Me.chkeMail.Checked Then
            ' show lbleMail (@) and txteMail
            Me.lbleMail.Visible = True
            Me.txteMail.Visible = True
            Me.lblExplain.Text = LRS(60576)  '"Har du klientrapporter, s� kan e-post adresser angis i klientoppsettet. Skal alle rapporter sendes til samme e-post adresse(r) s� sett opp disse over, adskilt med ;"
        Else
            Me.lbleMail.Visible = False
            Me.txteMail.Visible = False
            If Me.chkExport.Checked Then
                Me.lblExplain.Text = LRS(60577)  '"Filnavn for klientrapporter angis med en eller flere $ (klientnr), eller � (klientnavn)."
            Else
                Me.lblExplain.Text = ""
            End If
        End If
    End Sub

    Private Sub chkExport_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExport.CheckedChanged
        If Me.chkExport.Checked Then
            Me.lblExplain.Text = LRS(60576)  '"Har du klientrapporter, s� kan e-post adresser angis i klientoppsettet. Skal alle rapporter sendes til samme e-post adresse(r) s� sett opp disse over, adskilt med ;"
        Else
            Me.lblExplain.Text = ""
        End If
    End Sub

End Class
