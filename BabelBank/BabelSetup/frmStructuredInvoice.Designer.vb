<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmStructuredInvoice
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtSearchTextNegative As System.Windows.Forms.TextBox
	Public WithEvents txtNoOfPosNegative As System.Windows.Forms.TextBox
	Public WithEvents txtNoOfPos As System.Windows.Forms.TextBox
	Public WithEvents txtSearchText As System.Windows.Forms.TextBox
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdOK As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
    Public WithEvents lblNegative As System.Windows.Forms.Label
	Public WithEvents lblSearchTextNegative As System.Windows.Forms.Label
	Public WithEvents lblNoOfPosNegative As System.Windows.Forms.Label
	Public WithEvents lblPositive As System.Windows.Forms.Label
	Public WithEvents lblNoOfPos As System.Windows.Forms.Label
	Public WithEvents lblSearchText As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtSearchTextNegative = New System.Windows.Forms.TextBox
        Me.txtNoOfPosNegative = New System.Windows.Forms.TextBox
        Me.txtNoOfPos = New System.Windows.Forms.TextBox
        Me.txtSearchText = New System.Windows.Forms.TextBox
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdOK = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.lblNegative = New System.Windows.Forms.Label
        Me.lblSearchTextNegative = New System.Windows.Forms.Label
        Me.lblNoOfPosNegative = New System.Windows.Forms.Label
        Me.lblPositive = New System.Windows.Forms.Label
        Me.lblNoOfPos = New System.Windows.Forms.Label
        Me.lblSearchText = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.lblLine2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtSearchTextNegative
        '
        Me.txtSearchTextNegative.AcceptsReturn = True
        Me.txtSearchTextNegative.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchTextNegative.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSearchTextNegative.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSearchTextNegative.Location = New System.Drawing.Point(343, 177)
        Me.txtSearchTextNegative.MaxLength = 50
        Me.txtSearchTextNegative.Name = "txtSearchTextNegative"
        Me.txtSearchTextNegative.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSearchTextNegative.Size = New System.Drawing.Size(182, 20)
        Me.txtSearchTextNegative.TabIndex = 2
        '
        'txtNoOfPosNegative
        '
        Me.txtNoOfPosNegative.AcceptsReturn = True
        Me.txtNoOfPosNegative.BackColor = System.Drawing.SystemColors.Window
        Me.txtNoOfPosNegative.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNoOfPosNegative.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNoOfPosNegative.Location = New System.Drawing.Point(343, 202)
        Me.txtNoOfPosNegative.MaxLength = 2
        Me.txtNoOfPosNegative.Name = "txtNoOfPosNegative"
        Me.txtNoOfPosNegative.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNoOfPosNegative.Size = New System.Drawing.Size(30, 20)
        Me.txtNoOfPosNegative.TabIndex = 3
        '
        'txtNoOfPos
        '
        Me.txtNoOfPos.AcceptsReturn = True
        Me.txtNoOfPos.BackColor = System.Drawing.SystemColors.Window
        Me.txtNoOfPos.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNoOfPos.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNoOfPos.Location = New System.Drawing.Point(343, 115)
        Me.txtNoOfPos.MaxLength = 2
        Me.txtNoOfPos.Name = "txtNoOfPos"
        Me.txtNoOfPos.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNoOfPos.Size = New System.Drawing.Size(30, 20)
        Me.txtNoOfPos.TabIndex = 1
        '
        'txtSearchText
        '
        Me.txtSearchText.AcceptsReturn = True
        Me.txtSearchText.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSearchText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSearchText.Location = New System.Drawing.Point(343, 89)
        Me.txtSearchText.MaxLength = 50
        Me.txtSearchText.Name = "txtSearchText"
        Me.txtSearchText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSearchText.Size = New System.Drawing.Size(182, 20)
        Me.txtSearchText.TabIndex = 0
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(368, 240)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 5
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdOK
        '
        Me.CmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.CmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdOK.Location = New System.Drawing.Point(447, 240)
        Me.CmdOK.Name = "CmdOK"
        Me.CmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdOK.Size = New System.Drawing.Size(73, 21)
        Me.CmdOK.TabIndex = 6
        Me.CmdOK.Text = "&OK"
        Me.CmdOK.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(292, 240)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 4
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "55001 - &Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'lblNegative
        '
        Me.lblNegative.BackColor = System.Drawing.SystemColors.Control
        Me.lblNegative.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNegative.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNegative.Location = New System.Drawing.Point(174, 151)
        Me.lblNegative.Name = "lblNegative"
        Me.lblNegative.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNegative.Size = New System.Drawing.Size(345, 20)
        Me.lblNegative.TabIndex = 13
        Me.lblNegative.Text = "60483 - Negative amounts (creditnotepayments)"
        '
        'lblSearchTextNegative
        '
        Me.lblSearchTextNegative.BackColor = System.Drawing.SystemColors.Control
        Me.lblSearchTextNegative.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSearchTextNegative.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSearchTextNegative.Location = New System.Drawing.Point(174, 177)
        Me.lblSearchTextNegative.Name = "lblSearchTextNegative"
        Me.lblSearchTextNegative.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSearchTextNegative.Size = New System.Drawing.Size(147, 19)
        Me.lblSearchTextNegative.TabIndex = 12
        Me.lblSearchTextNegative.Text = "60457 - Search for"
        '
        'lblNoOfPosNegative
        '
        Me.lblNoOfPosNegative.BackColor = System.Drawing.SystemColors.Control
        Me.lblNoOfPosNegative.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNoOfPosNegative.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNoOfPosNegative.Location = New System.Drawing.Point(174, 202)
        Me.lblNoOfPosNegative.Name = "lblNoOfPosNegative"
        Me.lblNoOfPosNegative.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNoOfPosNegative.Size = New System.Drawing.Size(160, 22)
        Me.lblNoOfPosNegative.TabIndex = 11
        Me.lblNoOfPosNegative.Text = "60458 - No of pos from searchtext"
        '
        'lblPositive
        '
        Me.lblPositive.BackColor = System.Drawing.SystemColors.Control
        Me.lblPositive.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPositive.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPositive.Location = New System.Drawing.Point(173, 64)
        Me.lblPositive.Name = "lblPositive"
        Me.lblPositive.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPositive.Size = New System.Drawing.Size(345, 20)
        Me.lblPositive.TabIndex = 10
        Me.lblPositive.Text = "60482 - Positive amounts (Invoicepayments)"
        '
        'lblNoOfPos
        '
        Me.lblNoOfPos.BackColor = System.Drawing.SystemColors.Control
        Me.lblNoOfPos.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNoOfPos.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNoOfPos.Location = New System.Drawing.Point(174, 115)
        Me.lblNoOfPos.Name = "lblNoOfPos"
        Me.lblNoOfPos.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNoOfPos.Size = New System.Drawing.Size(160, 22)
        Me.lblNoOfPos.TabIndex = 9
        Me.lblNoOfPos.Text = "60458 - No of pos from searchtext"
        '
        'lblSearchText
        '
        Me.lblSearchText.BackColor = System.Drawing.SystemColors.Control
        Me.lblSearchText.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSearchText.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSearchText.Location = New System.Drawing.Point(174, 90)
        Me.lblSearchText.Name = "lblSearchText"
        Me.lblSearchText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSearchText.Size = New System.Drawing.Size(147, 19)
        Me.lblSearchText.TabIndex = 8
        Me.lblSearchText.Text = "60457 - Search for"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(178, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(344, 17)
        Me.lblHeading.TabIndex = 7
        Me.lblHeading.Text = "59038 - Structured invoicenumber"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(172, 143)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(350, 1)
        Me.lblLine1.TabIndex = 79
        Me.lblLine1.Text = "Label1"
        '
        'lblLine2
        '
        Me.lblLine2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine2.Location = New System.Drawing.Point(10, 234)
        Me.lblLine2.Name = "lblLine2"
        Me.lblLine2.Size = New System.Drawing.Size(510, 1)
        Me.lblLine2.TabIndex = 80
        Me.lblLine2.Text = "Label1"
        '
        'frmStructuredInvoice
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(534, 274)
        Me.Controls.Add(Me.lblLine2)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtSearchTextNegative)
        Me.Controls.Add(Me.txtNoOfPosNegative)
        Me.Controls.Add(Me.txtNoOfPos)
        Me.Controls.Add(Me.txtSearchText)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdOK)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.lblNegative)
        Me.Controls.Add(Me.lblSearchTextNegative)
        Me.Controls.Add(Me.lblNoOfPosNegative)
        Me.Controls.Add(Me.lblPositive)
        Me.Controls.Add(Me.lblNoOfPos)
        Me.Controls.Add(Me.lblSearchText)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStructuredInvoice"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "59038 - Structured invoicenumber"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents lblLine2 As System.Windows.Forms.Label
#End Region 
End Class
