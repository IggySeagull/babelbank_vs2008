Option Strict Off
Option Explicit On
Friend Class frmMatch
    Inherits System.Windows.Forms.Form
    Private frmSpecifyFileContent As frmSpecifyFileContent
    Private iCompany_ID As Integer
    Private iFileSetup_ID As Integer

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Me.Hide()
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        SaveMatch()
        Me.Hide()
    End Sub

    Private Sub frmMatch_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg")
        FormLRSCaptions(Me)
    End Sub
    Sub SaveMatch()
        oFilesetup.AutoMatch = CBool(Me.chkAutoMatch.CheckState)
        oFilesetup.MatchOCR = CBool(Me.chkMatchOCR.CheckState)
        oFilesetup.ValidateFormatAtImport = CBool(Me.chkValidate.CheckState)
    End Sub

    Private Sub cmdSpecifyFileContent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSpecifyFileContent.Click
        '04.01.2022 - Added
        Dim frmSpecifyFileContent As New frmSpecifyFileContent

        frmSpecifyFileContent.Company_ID(iCompany_ID)
        frmSpecifyFileContent.FileSetup_ID(iFileSetup_ID)
        frmSpecifyFileContent.ShowDialog()

    End Sub
    Public Sub Company_ID(ByVal i As Integer)
        iCompany_ID = i
    End Sub
    Public Sub FileSetup_ID(ByVal i As Integer)
        iFileSetup_ID = i
    End Sub

End Class
