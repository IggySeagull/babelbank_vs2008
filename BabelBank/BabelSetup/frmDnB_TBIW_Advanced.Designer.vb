<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDnB_TBIW_Advanced
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdCharges As System.Windows.Forms.Button
	Public WithEvents txtAddDays As System.Windows.Forms.TextBox
	Public WithEvents CmdAccounts As System.Windows.Forms.Button
    Public WithEvents CmdNBCodes As System.Windows.Forms.Button
	Public WithEvents chkSplitAfter450 As System.Windows.Forms.CheckBox
	Public WithEvents cmdMerge As System.Windows.Forms.Button
	Public WithEvents cmdMailPrint As System.Windows.Forms.Button
	Public WithEvents cmdSpecial As System.Windows.Forms.Button
	Public WithEvents cmbPaymentType As System.Windows.Forms.ComboBox
	Public WithEvents chkSilent As System.Windows.Forms.CheckBox
	Public WithEvents cmdLog As System.Windows.Forms.Button
	Public WithEvents optErrorsOnly As System.Windows.Forms.RadioButton
	Public WithEvents optNone As System.Windows.Forms.RadioButton
	Public WithEvents optErrorsWarnings As System.Windows.Forms.RadioButton
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents lblAddDays2 As System.Windows.Forms.Label
	Public WithEvents lblAddDays As System.Windows.Forms.Label
    Public WithEvents lblPayType As System.Windows.Forms.Label
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdCharges = New System.Windows.Forms.Button
        Me.txtAddDays = New System.Windows.Forms.TextBox
        Me.CmdAccounts = New System.Windows.Forms.Button
        Me.CmdNBCodes = New System.Windows.Forms.Button
        Me.chkSplitAfter450 = New System.Windows.Forms.CheckBox
        Me.cmdMerge = New System.Windows.Forms.Button
        Me.cmdMailPrint = New System.Windows.Forms.Button
        Me.cmdSpecial = New System.Windows.Forms.Button
        Me.cmbPaymentType = New System.Windows.Forms.ComboBox
        Me.chkSilent = New System.Windows.Forms.CheckBox
        Me.cmdLog = New System.Windows.Forms.Button
        Me.Frame1 = New System.Windows.Forms.GroupBox
        Me.optShowAll = New System.Windows.Forms.RadioButton
        Me.optErrorsOnly = New System.Windows.Forms.RadioButton
        Me.optNone = New System.Windows.Forms.RadioButton
        Me.optErrorsWarnings = New System.Windows.Forms.RadioButton
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblAddDays2 = New System.Windows.Forms.Label
        Me.lblAddDays = New System.Windows.Forms.Label
        Me.lblPayType = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.cmdCurrencyField = New System.Windows.Forms.Button
        Me.optTelepayPlus = New System.Windows.Forms.RadioButton
        Me.optPain001 = New System.Windows.Forms.RadioButton
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.chkToOwnAccount = New System.Windows.Forms.CheckBox
        Me.Frame1.SuspendLayout()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdCharges
        '
        Me.cmdCharges.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCharges.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCharges.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCharges.Location = New System.Drawing.Point(563, 192)
        Me.cmdCharges.Name = "cmdCharges"
        Me.cmdCharges.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCharges.Size = New System.Drawing.Size(85, 20)
        Me.cmdCharges.TabIndex = 24
        Me.cmdCharges.TabStop = False
        Me.cmdCharges.Text = "55045 &Charges"
        Me.cmdCharges.UseVisualStyleBackColor = False
        '
        'txtAddDays
        '
        Me.txtAddDays.AcceptsReturn = True
        Me.txtAddDays.BackColor = System.Drawing.SystemColors.Window
        Me.txtAddDays.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAddDays.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAddDays.Location = New System.Drawing.Point(359, 101)
        Me.txtAddDays.MaxLength = 1
        Me.txtAddDays.Name = "txtAddDays"
        Me.txtAddDays.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAddDays.Size = New System.Drawing.Size(21, 20)
        Me.txtAddDays.TabIndex = 4
        '
        'CmdAccounts
        '
        Me.CmdAccounts.BackColor = System.Drawing.SystemColors.Control
        Me.CmdAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdAccounts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdAccounts.Location = New System.Drawing.Point(563, 167)
        Me.CmdAccounts.Name = "CmdAccounts"
        Me.CmdAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdAccounts.Size = New System.Drawing.Size(85, 20)
        Me.CmdAccounts.TabIndex = 21
        Me.CmdAccounts.TabStop = False
        Me.CmdAccounts.Text = "60012 Konto"
        Me.CmdAccounts.UseVisualStyleBackColor = False
        '
        'CmdNBCodes
        '
        Me.CmdNBCodes.BackColor = System.Drawing.SystemColors.Control
        Me.CmdNBCodes.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdNBCodes.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdNBCodes.Location = New System.Drawing.Point(563, 66)
        Me.CmdNBCodes.Name = "CmdNBCodes"
        Me.CmdNBCodes.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdNBCodes.Size = New System.Drawing.Size(85, 20)
        Me.CmdNBCodes.TabIndex = 14
        Me.CmdNBCodes.TabStop = False
        Me.CmdNBCodes.Text = "55024 &Codes NB"
        Me.CmdNBCodes.UseVisualStyleBackColor = False
        '
        'chkSplitAfter450
        '
        Me.chkSplitAfter450.BackColor = System.Drawing.SystemColors.Control
        Me.chkSplitAfter450.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkSplitAfter450.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkSplitAfter450.Location = New System.Drawing.Point(204, 55)
        Me.chkSplitAfter450.Name = "chkSplitAfter450"
        Me.chkSplitAfter450.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkSplitAfter450.Size = New System.Drawing.Size(354, 17)
        Me.chkSplitAfter450.TabIndex = 2
        Me.chkSplitAfter450.Text = "60418 - Gi mulighet for � endre alle data i korreksjonsbilde"
        Me.chkSplitAfter450.UseVisualStyleBackColor = False
        '
        'cmdMerge
        '
        Me.cmdMerge.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMerge.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdMerge.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdMerge.Location = New System.Drawing.Point(563, 91)
        Me.cmdMerge.Name = "cmdMerge"
        Me.cmdMerge.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdMerge.Size = New System.Drawing.Size(85, 20)
        Me.cmdMerge.TabIndex = 15
        Me.cmdMerge.TabStop = False
        Me.cmdMerge.Text = "55021 &Merge"
        Me.cmdMerge.UseVisualStyleBackColor = False
        '
        'cmdMailPrint
        '
        Me.cmdMailPrint.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMailPrint.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdMailPrint.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdMailPrint.Location = New System.Drawing.Point(563, 17)
        Me.cmdMailPrint.Name = "cmdMailPrint"
        Me.cmdMailPrint.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdMailPrint.Size = New System.Drawing.Size(85, 20)
        Me.cmdMailPrint.TabIndex = 13
        Me.cmdMailPrint.TabStop = False
        Me.cmdMailPrint.Text = "55020 &Mail/Print"
        Me.cmdMailPrint.UseVisualStyleBackColor = False
        Me.cmdMailPrint.Visible = False
        '
        'cmdSpecial
        '
        Me.cmdSpecial.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSpecial.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSpecial.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSpecial.Location = New System.Drawing.Point(563, 142)
        Me.cmdSpecial.Name = "cmdSpecial"
        Me.cmdSpecial.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSpecial.Size = New System.Drawing.Size(85, 20)
        Me.cmdSpecial.TabIndex = 17
        Me.cmdSpecial.TabStop = False
        Me.cmdSpecial.Text = "60137 Spes"
        Me.cmdSpecial.UseVisualStyleBackColor = False
        '
        'cmbPaymentType
        '
        Me.cmbPaymentType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbPaymentType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbPaymentType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbPaymentType.Location = New System.Drawing.Point(359, 127)
        Me.cmbPaymentType.Name = "cmbPaymentType"
        Me.cmbPaymentType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbPaymentType.Size = New System.Drawing.Size(133, 21)
        Me.cmbPaymentType.TabIndex = 5
        '
        'chkSilent
        '
        Me.chkSilent.BackColor = System.Drawing.SystemColors.Control
        Me.chkSilent.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkSilent.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkSilent.Location = New System.Drawing.Point(204, 75)
        Me.chkSilent.Name = "chkSilent"
        Me.chkSilent.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkSilent.Size = New System.Drawing.Size(345, 17)
        Me.chkSilent.TabIndex = 3
        Me.chkSilent.Text = "60194 - Kj�r uten � vise driftsmeldinger (silent)"
        Me.chkSilent.UseVisualStyleBackColor = False
        '
        'cmdLog
        '
        Me.cmdLog.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLog.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdLog.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdLog.Location = New System.Drawing.Point(563, 116)
        Me.cmdLog.Name = "cmdLog"
        Me.cmdLog.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdLog.Size = New System.Drawing.Size(85, 20)
        Me.cmdLog.TabIndex = 16
        Me.cmdLog.TabStop = False
        Me.cmdLog.Text = "60195 &Logg"
        Me.cmdLog.UseVisualStyleBackColor = False
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.Color.Transparent
        Me.Frame1.Controls.Add(Me.optShowAll)
        Me.Frame1.Controls.Add(Me.optErrorsOnly)
        Me.Frame1.Controls.Add(Me.optNone)
        Me.Frame1.Controls.Add(Me.optErrorsWarnings)
        Me.Frame1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Frame1.Location = New System.Drawing.Point(193, 211)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.Padding = New System.Windows.Forms.Padding(0)
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(454, 56)
        Me.Frame1.TabIndex = 19
        Me.Frame1.TabStop = False
        Me.Frame1.Text = "60290 - Bilde for korrigering av feil"
        '
        'optShowAll
        '
        Me.optShowAll.BackColor = System.Drawing.Color.Transparent
        Me.optShowAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.optShowAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optShowAll.Location = New System.Drawing.Point(331, 17)
        Me.optShowAll.Name = "optShowAll"
        Me.optShowAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optShowAll.Size = New System.Drawing.Size(95, 36)
        Me.optShowAll.TabIndex = 10
        Me.optShowAll.TabStop = True
        Me.optShowAll.Text = "60611 Vis alle"
        Me.optShowAll.UseVisualStyleBackColor = False
        '
        'optErrorsOnly
        '
        Me.optErrorsOnly.BackColor = System.Drawing.Color.Transparent
        Me.optErrorsOnly.Cursor = System.Windows.Forms.Cursors.Default
        Me.optErrorsOnly.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optErrorsOnly.Location = New System.Drawing.Point(124, 17)
        Me.optErrorsOnly.Name = "optErrorsOnly"
        Me.optErrorsOnly.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optErrorsOnly.Size = New System.Drawing.Size(97, 36)
        Me.optErrorsOnly.TabIndex = 8
        Me.optErrorsOnly.TabStop = True
        Me.optErrorsOnly.Text = "60292 - Kun feil"
        Me.optErrorsOnly.UseVisualStyleBackColor = False
        '
        'optNone
        '
        Me.optNone.BackColor = System.Drawing.Color.Transparent
        Me.optNone.Cursor = System.Windows.Forms.Cursors.Default
        Me.optNone.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optNone.Location = New System.Drawing.Point(227, 17)
        Me.optNone.Name = "optNone"
        Me.optNone.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optNone.Size = New System.Drawing.Size(95, 36)
        Me.optNone.TabIndex = 9
        Me.optNone.TabStop = True
        Me.optNone.Text = "60293 - Ingen visning"
        Me.optNone.UseVisualStyleBackColor = False
        '
        'optErrorsWarnings
        '
        Me.optErrorsWarnings.BackColor = System.Drawing.Color.Transparent
        Me.optErrorsWarnings.Cursor = System.Windows.Forms.Cursors.Default
        Me.optErrorsWarnings.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optErrorsWarnings.Location = New System.Drawing.Point(12, 17)
        Me.optErrorsWarnings.Name = "optErrorsWarnings"
        Me.optErrorsWarnings.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optErrorsWarnings.Size = New System.Drawing.Size(106, 36)
        Me.optErrorsWarnings.TabIndex = 7
        Me.optErrorsWarnings.TabStop = True
        Me.optErrorsWarnings.Text = "60291 - B�de Feil og Varsler"
        Me.optErrorsWarnings.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(412, 291)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 18
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "55001 - &Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(488, 291)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 10
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(564, 291)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 11
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblAddDays2
        '
        Me.lblAddDays2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAddDays2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAddDays2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAddDays2.Location = New System.Drawing.Point(384, 104)
        Me.lblAddDays2.Name = "lblAddDays2"
        Me.lblAddDays2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAddDays2.Size = New System.Drawing.Size(173, 17)
        Me.lblAddDays2.TabIndex = 23
        Me.lblAddDays2.Text = "60509-days forward from today's date"
        '
        'lblAddDays
        '
        Me.lblAddDays.BackColor = System.Drawing.SystemColors.Control
        Me.lblAddDays.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAddDays.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAddDays.Location = New System.Drawing.Point(204, 105)
        Me.lblAddDays.Name = "lblAddDays"
        Me.lblAddDays.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAddDays.Size = New System.Drawing.Size(151, 17)
        Me.lblAddDays.TabIndex = 22
        Me.lblAddDays.Text = "60508-Make the ACH effective day "
        '
        'lblPayType
        '
        Me.lblPayType.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayType.Location = New System.Drawing.Point(204, 129)
        Me.lblPayType.Name = "lblPayType"
        Me.lblPayType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayType.Size = New System.Drawing.Size(172, 17)
        Me.lblPayType.TabIndex = 12
        Me.lblPayType.Text = "60304 - Betalingstype"
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(7, 248)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(32, 19)
        Me._Image1_0.TabIndex = 25
        Me._Image1_0.TabStop = False
        '
        'cmdCurrencyField
        '
        Me.cmdCurrencyField.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCurrencyField.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCurrencyField.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCurrencyField.Location = New System.Drawing.Point(562, 42)
        Me.cmdCurrencyField.Name = "cmdCurrencyField"
        Me.cmdCurrencyField.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCurrencyField.Size = New System.Drawing.Size(85, 20)
        Me.cmdCurrencyField.TabIndex = 27
        Me.cmdCurrencyField.TabStop = False
        Me.cmdCurrencyField.Text = "55047 &Currencyfield"
        Me.cmdCurrencyField.UseVisualStyleBackColor = False
        Me.cmdCurrencyField.Visible = False
        '
        'optTelepayPlus
        '
        Me.optTelepayPlus.BackColor = System.Drawing.Color.Transparent
        Me.optTelepayPlus.Cursor = System.Windows.Forms.Cursors.Default
        Me.optTelepayPlus.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optTelepayPlus.Location = New System.Drawing.Point(359, 154)
        Me.optTelepayPlus.Name = "optTelepayPlus"
        Me.optTelepayPlus.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optTelepayPlus.Size = New System.Drawing.Size(157, 17)
        Me.optTelepayPlus.TabIndex = 29
        Me.optTelepayPlus.TabStop = True
        Me.optTelepayPlus.Text = "Telepay Plus"
        Me.optTelepayPlus.UseVisualStyleBackColor = False
        '
        'optPain001
        '
        Me.optPain001.BackColor = System.Drawing.Color.Transparent
        Me.optPain001.Cursor = System.Windows.Forms.Cursors.Default
        Me.optPain001.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optPain001.Location = New System.Drawing.Point(204, 154)
        Me.optPain001.Name = "optPain001"
        Me.optPain001.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optPain001.Size = New System.Drawing.Size(150, 17)
        Me.optPain001.TabIndex = 28
        Me.optPain001.TabStop = True
        Me.optPain001.Text = "Pain.001"
        Me.optPain001.UseVisualStyleBackColor = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 280)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(640, 1)
        Me.lblLine1.TabIndex = 57
        Me.lblLine1.Text = "Label1"
        '
        'chkToOwnAccount
        '
        Me.chkToOwnAccount.AutoSize = True
        Me.chkToOwnAccount.Location = New System.Drawing.Point(204, 182)
        Me.chkToOwnAccount.Name = "chkToOwnAccount"
        Me.chkToOwnAccount.Size = New System.Drawing.Size(224, 17)
        Me.chkToOwnAccount.TabIndex = 58
        Me.chkToOwnAccount.Text = "60680 - Mark as Payment to own account"
        Me.chkToOwnAccount.UseVisualStyleBackColor = True
        '
        'frmDnB_TBIW_Advanced
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(656, 320)
        Me.Controls.Add(Me.chkToOwnAccount)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.optTelepayPlus)
        Me.Controls.Add(Me.optPain001)
        Me.Controls.Add(Me.cmdCurrencyField)
        Me.Controls.Add(Me.cmdCharges)
        Me.Controls.Add(Me.txtAddDays)
        Me.Controls.Add(Me.CmdAccounts)
        Me.Controls.Add(Me.CmdNBCodes)
        Me.Controls.Add(Me.chkSplitAfter450)
        Me.Controls.Add(Me.cmdMerge)
        Me.Controls.Add(Me.cmdMailPrint)
        Me.Controls.Add(Me.cmdSpecial)
        Me.Controls.Add(Me.cmbPaymentType)
        Me.Controls.Add(Me.chkSilent)
        Me.Controls.Add(Me.cmdLog)
        Me.Controls.Add(Me.Frame1)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lblAddDays2)
        Me.Controls.Add(Me.lblAddDays)
        Me.Controls.Add(Me.lblPayType)
        Me.Controls.Add(Me._Image1_0)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDnB_TBIW_Advanced"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "60303 - Avansert oppsett"
        Me.Frame1.ResumeLayout(False)
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents cmdCurrencyField As System.Windows.Forms.Button
    Public WithEvents optShowAll As System.Windows.Forms.RadioButton
    Public WithEvents optTelepayPlus As System.Windows.Forms.RadioButton
    Public WithEvents optPain001 As System.Windows.Forms.RadioButton
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents chkToOwnAccount As System.Windows.Forms.CheckBox
#End Region
End Class
