﻿Public Class _MyListBoxItem
    '11.06.2019 add New overloaded methods to accept parameters on creation of object for simplicity

    Dim m_ItemData As Integer
    Dim m_ItemString As String = ""
    Public Sub New(Optional ByVal sItemString As String = "", Optional ByVal iItemData As Integer = 0)
        m_ItemString = sItemString
        m_ItemData = iItemData
    End Sub


    Public Property ItemData() As Integer

        Get
            Return m_ItemData
        End Get

        Set(ByVal value As Integer)
            m_ItemData = value
        End Set

    End Property


    Public Property ItemString() As String

        Get
            Return m_ItemString
        End Get

        Set(ByVal value As String)
            m_ItemString = value
        End Set

    End Property


    'override the ToString to display the ItemString value in the combo box
    Public Overrides Function ToString() As String
        Return m_ItemString
    End Function


    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        'check to see if the objects to string value matches the ItemString
        Return obj.ToString = m_ItemString
    End Function

End Class
