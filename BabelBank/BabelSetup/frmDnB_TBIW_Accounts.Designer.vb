<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDnB_TBIW_Accounts
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdShowAccounts As System.Windows.Forms.Button
	Public WithEvents lstAccounts As System.Windows.Forms.ListBox
    Public WithEvents lblAccounts As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdShowAccounts = New System.Windows.Forms.Button
        Me.lstAccounts = New System.Windows.Forms.ListBox
        Me.lblAccounts = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(460, 181)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 3
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdShowAccounts
        '
        Me.cmdShowAccounts.BackColor = System.Drawing.SystemColors.Control
        Me.cmdShowAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdShowAccounts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdShowAccounts.Location = New System.Drawing.Point(460, 66)
        Me.cmdShowAccounts.Name = "cmdShowAccounts"
        Me.cmdShowAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdShowAccounts.Size = New System.Drawing.Size(73, 21)
        Me.cmdShowAccounts.TabIndex = 1
        Me.cmdShowAccounts.TabStop = False
        Me.cmdShowAccounts.Text = "55016-Show"
        Me.cmdShowAccounts.UseVisualStyleBackColor = False
        '
        'lstAccounts
        '
        Me.lstAccounts.BackColor = System.Drawing.SystemColors.Window
        Me.lstAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstAccounts.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstAccounts.Location = New System.Drawing.Point(223, 66)
        Me.lstAccounts.Name = "lstAccounts"
        Me.lstAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstAccounts.Size = New System.Drawing.Size(229, 69)
        Me.lstAccounts.TabIndex = 0
        '
        'lblAccounts
        '
        Me.lblAccounts.BackColor = System.Drawing.SystemColors.Control
        Me.lblAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAccounts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAccounts.Location = New System.Drawing.Point(222, 48)
        Me.lblAccounts.Name = "lblAccounts"
        Me.lblAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAccounts.Size = New System.Drawing.Size(173, 14)
        Me.lblAccounts.TabIndex = 2
        Me.lblAccounts.Text = "60012 - Accounts"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(191, 173)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(350, 1)
        Me.lblLine1.TabIndex = 57
        Me.lblLine1.Text = "Label1"
        '
        'frmDnB_TBIW_Accounts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(545, 209)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdShowAccounts)
        Me.Controls.Add(Me.lstAccounts)
        Me.Controls.Add(Me.lblAccounts)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmDnB_TBIW_Accounts"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "60012 - Accounts"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region
End Class
