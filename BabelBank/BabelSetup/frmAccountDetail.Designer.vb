<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmAccountDetail
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtCurrencyCode As System.Windows.Forms.TextBox
	Public WithEvents cmdOCRSettings As System.Windows.Forms.Button
	Public WithEvents txtAccountCountryCode As System.Windows.Forms.TextBox
	Public WithEvents txtSWIFTAdr As System.Windows.Forms.TextBox
	Public WithEvents txtResultGL As System.Windows.Forms.TextBox
	Public WithEvents txtBankGL As System.Windows.Forms.TextBox
	Public WithEvents txtAvtalegiro As System.Windows.Forms.TextBox
	Public WithEvents txtConverted As System.Windows.Forms.TextBox
	Public WithEvents txtAccountNo As System.Windows.Forms.TextBox
	Public WithEvents txtContractNo As System.Windows.Forms.TextBox
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdOK As System.Windows.Forms.Button
	Public WithEvents CmdHelp As System.Windows.Forms.Button
	Public WithEvents lblCurrencyCode As System.Windows.Forms.Label
	Public WithEvents lblAccountCountryCode As System.Windows.Forms.Label
	Public WithEvents lblSWIFTAddress As System.Windows.Forms.Label
	Public WithEvents lblResultGL As System.Windows.Forms.Label
	Public WithEvents lblBankGL As System.Windows.Forms.Label
	Public WithEvents lblAvtalegiro As System.Windows.Forms.Label
	Public WithEvents lblConverted As System.Windows.Forms.Label
	Public WithEvents lblContractNo As System.Windows.Forms.Label
    Public WithEvents lblAccountNo As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtCurrencyCode = New System.Windows.Forms.TextBox
        Me.cmdOCRSettings = New System.Windows.Forms.Button
        Me.txtAccountCountryCode = New System.Windows.Forms.TextBox
        Me.txtSWIFTAdr = New System.Windows.Forms.TextBox
        Me.txtResultGL = New System.Windows.Forms.TextBox
        Me.txtBankGL = New System.Windows.Forms.TextBox
        Me.txtAvtalegiro = New System.Windows.Forms.TextBox
        Me.txtConverted = New System.Windows.Forms.TextBox
        Me.txtAccountNo = New System.Windows.Forms.TextBox
        Me.txtContractNo = New System.Windows.Forms.TextBox
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdOK = New System.Windows.Forms.Button
        Me.CmdHelp = New System.Windows.Forms.Button
        Me.lblCurrencyCode = New System.Windows.Forms.Label
        Me.lblAccountCountryCode = New System.Windows.Forms.Label
        Me.lblSWIFTAddress = New System.Windows.Forms.Label
        Me.lblResultGL = New System.Windows.Forms.Label
        Me.lblBankGL = New System.Windows.Forms.Label
        Me.lblAvtalegiro = New System.Windows.Forms.Label
        Me.lblConverted = New System.Windows.Forms.Label
        Me.lblContractNo = New System.Windows.Forms.Label
        Me.lblAccountNo = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.txtDebitAccountCountryCode = New System.Windows.Forms.TextBox
        Me.lblDebitAccountCountryCode = New System.Windows.Forms.Label
        Me.cmdDimensionsBank = New System.Windows.Forms.Button
        Me.cmdDimensionsResult = New System.Windows.Forms.Button
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.lblLine2 = New System.Windows.Forms.Label
        Me.lblLine3 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtCurrencyCode
        '
        Me.txtCurrencyCode.AcceptsReturn = True
        Me.txtCurrencyCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtCurrencyCode.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCurrencyCode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCurrencyCode.Location = New System.Drawing.Point(351, 261)
        Me.txtCurrencyCode.MaxLength = 3
        Me.txtCurrencyCode.Name = "txtCurrencyCode"
        Me.txtCurrencyCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCurrencyCode.Size = New System.Drawing.Size(30, 20)
        Me.txtCurrencyCode.TabIndex = 8
        '
        'cmdOCRSettings
        '
        Me.cmdOCRSettings.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOCRSettings.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOCRSettings.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOCRSettings.Location = New System.Drawing.Point(351, 291)
        Me.cmdOCRSettings.Name = "cmdOCRSettings"
        Me.cmdOCRSettings.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOCRSettings.Size = New System.Drawing.Size(134, 21)
        Me.cmdOCRSettings.TabIndex = 21
        Me.cmdOCRSettings.Text = "55040 &OCR-oppsett"
        Me.cmdOCRSettings.UseVisualStyleBackColor = False
        '
        'txtAccountCountryCode
        '
        Me.txtAccountCountryCode.AcceptsReturn = True
        Me.txtAccountCountryCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtAccountCountryCode.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAccountCountryCode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAccountCountryCode.Location = New System.Drawing.Point(22, 149)
        Me.txtAccountCountryCode.MaxLength = 255
        Me.txtAccountCountryCode.Name = "txtAccountCountryCode"
        Me.txtAccountCountryCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAccountCountryCode.Size = New System.Drawing.Size(134, 20)
        Me.txtAccountCountryCode.TabIndex = 5
        Me.txtAccountCountryCode.Visible = False
        '
        'txtSWIFTAdr
        '
        Me.txtSWIFTAdr.AcceptsReturn = True
        Me.txtSWIFTAdr.BackColor = System.Drawing.SystemColors.Window
        Me.txtSWIFTAdr.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSWIFTAdr.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSWIFTAdr.Location = New System.Drawing.Point(351, 236)
        Me.txtSWIFTAdr.MaxLength = 255
        Me.txtSWIFTAdr.Name = "txtSWIFTAdr"
        Me.txtSWIFTAdr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSWIFTAdr.Size = New System.Drawing.Size(134, 20)
        Me.txtSWIFTAdr.TabIndex = 7
        '
        'txtResultGL
        '
        Me.txtResultGL.AcceptsReturn = True
        Me.txtResultGL.BackColor = System.Drawing.SystemColors.Window
        Me.txtResultGL.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtResultGL.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtResultGL.Location = New System.Drawing.Point(351, 119)
        Me.txtResultGL.MaxLength = 50
        Me.txtResultGL.Name = "txtResultGL"
        Me.txtResultGL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtResultGL.Size = New System.Drawing.Size(134, 20)
        Me.txtResultGL.TabIndex = 2
        '
        'txtBankGL
        '
        Me.txtBankGL.AcceptsReturn = True
        Me.txtBankGL.BackColor = System.Drawing.SystemColors.Window
        Me.txtBankGL.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBankGL.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBankGL.Location = New System.Drawing.Point(351, 93)
        Me.txtBankGL.MaxLength = 50
        Me.txtBankGL.Name = "txtBankGL"
        Me.txtBankGL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBankGL.Size = New System.Drawing.Size(134, 20)
        Me.txtBankGL.TabIndex = 1
        '
        'txtAvtalegiro
        '
        Me.txtAvtalegiro.AcceptsReturn = True
        Me.txtAvtalegiro.BackColor = System.Drawing.SystemColors.Window
        Me.txtAvtalegiro.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAvtalegiro.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAvtalegiro.Location = New System.Drawing.Point(351, 210)
        Me.txtAvtalegiro.MaxLength = 255
        Me.txtAvtalegiro.Name = "txtAvtalegiro"
        Me.txtAvtalegiro.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAvtalegiro.Size = New System.Drawing.Size(134, 20)
        Me.txtAvtalegiro.TabIndex = 6
        '
        'txtConverted
        '
        Me.txtConverted.AcceptsReturn = True
        Me.txtConverted.BackColor = System.Drawing.SystemColors.Window
        Me.txtConverted.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtConverted.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtConverted.Location = New System.Drawing.Point(352, 146)
        Me.txtConverted.MaxLength = 50
        Me.txtConverted.Name = "txtConverted"
        Me.txtConverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtConverted.Size = New System.Drawing.Size(134, 20)
        Me.txtConverted.TabIndex = 3
        '
        'txtAccountNo
        '
        Me.txtAccountNo.AcceptsReturn = True
        Me.txtAccountNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtAccountNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAccountNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAccountNo.Location = New System.Drawing.Point(351, 57)
        Me.txtAccountNo.MaxLength = 35
        Me.txtAccountNo.Name = "txtAccountNo"
        Me.txtAccountNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAccountNo.Size = New System.Drawing.Size(134, 20)
        Me.txtAccountNo.TabIndex = 0
        '
        'txtContractNo
        '
        Me.txtContractNo.AcceptsReturn = True
        Me.txtContractNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtContractNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtContractNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtContractNo.Location = New System.Drawing.Point(351, 185)
        Me.txtContractNo.MaxLength = 255
        Me.txtContractNo.Name = "txtContractNo"
        Me.txtContractNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtContractNo.Size = New System.Drawing.Size(134, 20)
        Me.txtContractNo.TabIndex = 4
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(445, 330)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 10
        Me.CmdCancel.Text = "55002-&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdOK
        '
        Me.CmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.CmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdOK.Location = New System.Drawing.Point(524, 330)
        Me.CmdOK.Name = "CmdOK"
        Me.CmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdOK.Size = New System.Drawing.Size(73, 21)
        Me.CmdOK.TabIndex = 11
        Me.CmdOK.Text = "&OK"
        Me.CmdOK.UseVisualStyleBackColor = False
        '
        'CmdHelp
        '
        Me.CmdHelp.BackColor = System.Drawing.SystemColors.Control
        Me.CmdHelp.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdHelp.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdHelp.Location = New System.Drawing.Point(369, 330)
        Me.CmdHelp.Name = "CmdHelp"
        Me.CmdHelp.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdHelp.Size = New System.Drawing.Size(73, 21)
        Me.CmdHelp.TabIndex = 9
        Me.CmdHelp.TabStop = False
        Me.CmdHelp.Text = "55001 - &Hjelp"
        Me.CmdHelp.UseVisualStyleBackColor = False
        '
        'lblCurrencyCode
        '
        Me.lblCurrencyCode.BackColor = System.Drawing.SystemColors.Control
        Me.lblCurrencyCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCurrencyCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCurrencyCode.Location = New System.Drawing.Point(195, 263)
        Me.lblCurrencyCode.Name = "lblCurrencyCode"
        Me.lblCurrencyCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCurrencyCode.Size = New System.Drawing.Size(147, 18)
        Me.lblCurrencyCode.TabIndex = 22
        Me.lblCurrencyCode.Text = "60212 - CurencyCode"
        '
        'lblAccountCountryCode
        '
        Me.lblAccountCountryCode.BackColor = System.Drawing.SystemColors.Control
        Me.lblAccountCountryCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAccountCountryCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAccountCountryCode.Location = New System.Drawing.Point(30, 129)
        Me.lblAccountCountryCode.Name = "lblAccountCountryCode"
        Me.lblAccountCountryCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAccountCountryCode.Size = New System.Drawing.Size(125, 26)
        Me.lblAccountCountryCode.TabIndex = 20
        Me.lblAccountCountryCode.Text = "60469 - Default country code receiver"
        Me.lblAccountCountryCode.Visible = False
        '
        'lblSWIFTAddress
        '
        Me.lblSWIFTAddress.BackColor = System.Drawing.SystemColors.Control
        Me.lblSWIFTAddress.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSWIFTAddress.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSWIFTAddress.Location = New System.Drawing.Point(194, 240)
        Me.lblSWIFTAddress.Name = "lblSWIFTAddress"
        Me.lblSWIFTAddress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSWIFTAddress.Size = New System.Drawing.Size(147, 18)
        Me.lblSWIFTAddress.TabIndex = 19
        Me.lblSWIFTAddress.Text = "60393 - SWIFTAdress"
        '
        'lblResultGL
        '
        Me.lblResultGL.BackColor = System.Drawing.SystemColors.Control
        Me.lblResultGL.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblResultGL.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblResultGL.Location = New System.Drawing.Point(194, 119)
        Me.lblResultGL.Name = "lblResultGL"
        Me.lblResultGL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblResultGL.Size = New System.Drawing.Size(152, 22)
        Me.lblResultGL.TabIndex = 18
        Me.lblResultGL.Text = "60150 - Resultsacc"
        '
        'lblBankGL
        '
        Me.lblBankGL.BackColor = System.Drawing.SystemColors.Control
        Me.lblBankGL.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBankGL.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBankGL.Location = New System.Drawing.Point(194, 94)
        Me.lblBankGL.Name = "lblBankGL"
        Me.lblBankGL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBankGL.Size = New System.Drawing.Size(147, 19)
        Me.lblBankGL.TabIndex = 17
        Me.lblBankGL.Text = "60149 - Bank GL"
        '
        'lblAvtalegiro
        '
        Me.lblAvtalegiro.BackColor = System.Drawing.SystemColors.Control
        Me.lblAvtalegiro.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAvtalegiro.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAvtalegiro.Location = New System.Drawing.Point(194, 213)
        Me.lblAvtalegiro.Name = "lblAvtalegiro"
        Me.lblAvtalegiro.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAvtalegiro.Size = New System.Drawing.Size(147, 18)
        Me.lblAvtalegiro.TabIndex = 16
        Me.lblAvtalegiro.Text = "60377 -  AvtalegiroID"
        '
        'lblConverted
        '
        Me.lblConverted.BackColor = System.Drawing.SystemColors.Control
        Me.lblConverted.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblConverted.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblConverted.Location = New System.Drawing.Point(194, 146)
        Me.lblConverted.Name = "lblConverted"
        Me.lblConverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblConverted.Size = New System.Drawing.Size(147, 19)
        Me.lblConverted.TabIndex = 15
        Me.lblConverted.Text = "60204 - Converted Ac"
        '
        'lblContractNo
        '
        Me.lblContractNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblContractNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblContractNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblContractNo.Location = New System.Drawing.Point(194, 188)
        Me.lblContractNo.Name = "lblContractNo"
        Me.lblContractNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblContractNo.Size = New System.Drawing.Size(145, 18)
        Me.lblContractNo.TabIndex = 14
        Me.lblContractNo.Text = "60176 - ContractNo"
        '
        'lblAccountNo
        '
        Me.lblAccountNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblAccountNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAccountNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAccountNo.Location = New System.Drawing.Point(194, 60)
        Me.lblAccountNo.Name = "lblAccountNo"
        Me.lblAccountNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAccountNo.Size = New System.Drawing.Size(144, 17)
        Me.lblAccountNo.TabIndex = 13
        Me.lblAccountNo.Text = "60356-AccountNo"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(194, 21)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(307, 17)
        Me.lblHeading.TabIndex = 12
        Me.lblHeading.Text = "60376 - Account details"
        '
        'txtDebitAccountCountryCode
        '
        Me.txtDebitAccountCountryCode.AcceptsReturn = True
        Me.txtDebitAccountCountryCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtDebitAccountCountryCode.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDebitAccountCountryCode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDebitAccountCountryCode.Location = New System.Drawing.Point(455, 261)
        Me.txtDebitAccountCountryCode.MaxLength = 10
        Me.txtDebitAccountCountryCode.Name = "txtDebitAccountCountryCode"
        Me.txtDebitAccountCountryCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDebitAccountCountryCode.Size = New System.Drawing.Size(30, 20)
        Me.txtDebitAccountCountryCode.TabIndex = 9
        '
        'lblDebitAccountCountryCode
        '
        Me.lblDebitAccountCountryCode.BackColor = System.Drawing.SystemColors.Control
        Me.lblDebitAccountCountryCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDebitAccountCountryCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDebitAccountCountryCode.Location = New System.Drawing.Point(387, 264)
        Me.lblDebitAccountCountryCode.Name = "lblDebitAccountCountryCode"
        Me.lblDebitAccountCountryCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDebitAccountCountryCode.Size = New System.Drawing.Size(67, 19)
        Me.lblDebitAccountCountryCode.TabIndex = 25
        Me.lblDebitAccountCountryCode.Text = "60209 - Country"
        '
        'cmdDimensionsBank
        '
        Me.cmdDimensionsBank.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDimensionsBank.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDimensionsBank.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDimensionsBank.Location = New System.Drawing.Point(492, 92)
        Me.cmdDimensionsBank.Name = "cmdDimensionsBank"
        Me.cmdDimensionsBank.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDimensionsBank.Size = New System.Drawing.Size(107, 21)
        Me.cmdDimensionsBank.TabIndex = 26
        Me.cmdDimensionsBank.Text = "55048 &Dimensjoner"
        Me.cmdDimensionsBank.UseVisualStyleBackColor = False
        '
        'cmdDimensionsResult
        '
        Me.cmdDimensionsResult.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDimensionsResult.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDimensionsResult.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDimensionsResult.Location = New System.Drawing.Point(492, 119)
        Me.cmdDimensionsResult.Name = "cmdDimensionsResult"
        Me.cmdDimensionsResult.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDimensionsResult.Size = New System.Drawing.Size(107, 21)
        Me.cmdDimensionsResult.TabIndex = 27
        Me.cmdDimensionsResult.Text = "55048 &Dimensjoner"
        Me.cmdDimensionsResult.UseVisualStyleBackColor = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(175, 84)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(420, 1)
        Me.lblLine1.TabIndex = 28
        Me.lblLine1.Text = "Label1"
        '
        'lblLine2
        '
        Me.lblLine2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine2.Location = New System.Drawing.Point(175, 174)
        Me.lblLine2.Name = "lblLine2"
        Me.lblLine2.Size = New System.Drawing.Size(420, 1)
        Me.lblLine2.TabIndex = 29
        Me.lblLine2.Text = "Label2"
        '
        'lblLine3
        '
        Me.lblLine3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine3.Location = New System.Drawing.Point(15, 322)
        Me.lblLine3.Name = "lblLine3"
        Me.lblLine3.Size = New System.Drawing.Size(580, 1)
        Me.lblLine3.TabIndex = 30
        Me.lblLine3.Text = "Label3"
        '
        'frmAccountDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(606, 356)
        Me.Controls.Add(Me.lblLine3)
        Me.Controls.Add(Me.lblLine2)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdDimensionsResult)
        Me.Controls.Add(Me.cmdDimensionsBank)
        Me.Controls.Add(Me.txtDebitAccountCountryCode)
        Me.Controls.Add(Me.lblDebitAccountCountryCode)
        Me.Controls.Add(Me.txtCurrencyCode)
        Me.Controls.Add(Me.cmdOCRSettings)
        Me.Controls.Add(Me.txtAccountCountryCode)
        Me.Controls.Add(Me.txtSWIFTAdr)
        Me.Controls.Add(Me.txtResultGL)
        Me.Controls.Add(Me.txtBankGL)
        Me.Controls.Add(Me.txtAvtalegiro)
        Me.Controls.Add(Me.txtConverted)
        Me.Controls.Add(Me.txtAccountNo)
        Me.Controls.Add(Me.txtContractNo)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdOK)
        Me.Controls.Add(Me.CmdHelp)
        Me.Controls.Add(Me.lblCurrencyCode)
        Me.Controls.Add(Me.lblAccountCountryCode)
        Me.Controls.Add(Me.lblSWIFTAddress)
        Me.Controls.Add(Me.lblResultGL)
        Me.Controls.Add(Me.lblBankGL)
        Me.Controls.Add(Me.lblAvtalegiro)
        Me.Controls.Add(Me.lblConverted)
        Me.Controls.Add(Me.lblContractNo)
        Me.Controls.Add(Me.lblAccountNo)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAccountDetail"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "60376 - Account details"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents txtDebitAccountCountryCode As System.Windows.Forms.TextBox
    Public WithEvents lblDebitAccountCountryCode As System.Windows.Forms.Label
    Public WithEvents cmdDimensionsBank As System.Windows.Forms.Button
    Public WithEvents cmdDimensionsResult As System.Windows.Forms.Button
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents lblLine2 As System.Windows.Forms.Label
    Friend WithEvents lblLine3 As System.Windows.Forms.Label
#End Region
End Class
