Option Strict Off
Option Explicit On
Module WriteOCRBankgirot
	
    Dim sLine As String ' en output-linje
    ' Have to dim some variables for
    ' - batchnivå TK50: total amount, No of transactions
	Dim nBatchSumAmount, nBatchNoTransactions As Double
	' - batchnivå TK50: total amount, No of transactions
	Dim nFileSumAmount, nFileNoTransactions As Double
	
	
	Function WriteOCRBankgirotFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef iFilenameInNo As Short, ByRef sCompanyNo As String, ByRef sClientNo As String, ByRef bFalseOCR As Boolean, ByRef bOCRTransactions As Boolean) As Boolean
		
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
		Dim oBatch As Batch
		Dim oPayment, oTempPayment As Payment
		Dim oInvoice As Invoice
		Dim oFilesetup As FileSetup
		Dim oClient As Client
		Dim oaccount As Account
		
		Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
		Dim bFileFromBank As Boolean
		Dim sNewOwnref As String
		Dim sI_EnterpriseNo As String
		Dim dProductionDate As Date
		Dim bProductionDateSet As Boolean
		Dim sOldAccountNo, sContractNo As String
		Dim bFoundContractNo, bAccountFound As Boolean
		Dim bTK00Written As Boolean
		Dim bTK20Written As Boolean
		Dim sSpecial As String
		
        Try

            nBatchSumAmount = 0
            nBatchNoTransactions = 0
            nFileSumAmount = 0
            nFileNoTransactions = 0
            bTK00Written = False
            dProductionDate = DateSerial(CInt("1990"), CInt("01"), CInt("01"))
            bProductionDateSet = False
            sOldAccountNo = ""

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                If oBabel.VB_FilenameInNo = iFilenameInNo Then

                    bExportoBabel = False
                    'Have to go through each batch-object to see if we have objects that shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments


                            'New code 07.02.2006 - Regarding FalseOCR

                            '28.10.2008 changed to IsOCR
                            If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And IsOCR((oPayment.PayCode))) Or (bFalseOCR And Not bOCRTransactions And Not IsOCR((oPayment.PayCode))) Then
                                'Old code
                                'If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And oPayment.PayCode = "510") Or (bFalseOCR And Not bOCRTransactions And oPayment.PayCode <> "510") Then
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                    If Not bMultiFiles Then
                                        bExportoBabel = True
                                    Else
                                        'If oPayment.I_Account = sI_Account Then
                                        ' 01.10.2018 – changed above If to:
                                        If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoBabel = True
                                                End If
                                            Else
                                                bExportoBabel = True
                                            End If
                                        End If
                                    End If
                                End If
                            Else
                                ' 29.03.06 Changed by JanP
                                ' For Jotun, we may have a batch with mix of OCR and non-ocr
                                'Exit For
                            End If 'bFalseOCR
                            If bExportoBabel Then
                                Exit For
                            End If

                            'Old code
                            '
                            '                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            '                    If Not bMultiFiles Then
                            '                       bExportoBabel = True
                            '                    Else
                            '                        If oPayment.I_Account = sI_Account Then
                            '                            If InStr(oPayment.REF_Own, "&?") Then
                            '                                'Set in the part of the OwnRef that BabelBank is using
                            '                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                            '                            Else
                            '                                sNewOwnref = ""
                            '                            End If
                            '                            If sNewOwnref = sOwnRef Then
                            '                                bExportoBabel = True
                            '                            End If
                            '                        End If
                            '                    End If
                            '                End If
                            '                If bExportoBabel Then
                            '                    Exit For
                            '                End If
                        Next oPayment
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oBatch

                    If bExportoBabel Then

                        'Set if the file was created int the accounting system or not.
                        bFileFromBank = oBabel.FileFromBank
                        'i = 0 'FIXED Moved by Janp under case 2

                        'Have to write the start-file record
                        If Not bTK00Written Then
                            If oBabel.ImportFormat = BabelFiles.FileType.OCR_Bankgirot Then
                                If sCompanyNo <> "" Then
                                    sI_EnterpriseNo = sCompanyNo
                                Else
                                    sI_EnterpriseNo = oBatch.I_EnterpriseNo
                                End If
                            Else
                                sI_EnterpriseNo = sCompanyNo
                            End If
                            'Productiondate is written in the file, save the productiondate
                            If Not bProductionDateSet Then
                                dProductionDate = StringToDate((oBabel.DATE_Production))
                                bProductionDateSet = True
                            End If

                            sLine = wr_TK00(dProductionDate, sI_EnterpriseNo)
                            oFile.WriteLine((sLine))
                            sLine = wr_TK10()
                            oFile.WriteLine((sLine))
                            bTK00Written = True
                        End If

                        'Loop through all Batch objs. in BabelFile obj.
                        For Each oBatch In oBabel.Batches
                            bExportoBatch = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oPayment In oBatch.Payments

                                '28.10.2008 changed to IsOCR
                                If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And IsOCR((oPayment.PayCode))) Or (bFalseOCR And Not bOCRTransactions And Not IsOCR((oPayment.PayCode))) Then
                                    'Old code
                                    'If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And oPayment.PayCode = "510") Or (bFalseOCR And Not bOCRTransactions And oPayment.PayCode <> "510") Then
                                    'If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    ' Changed by JanP 07.01.04
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then
                                        If Not bMultiFiles Then
                                            bExportoBatch = True
                                        Else
                                            'If oPayment.I_Account = sI_Account Then
                                            ' 01.10.2018 – changed above If to:
                                            If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                                If Len(oPayment.VB_ClientNo) > 0 Then
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        bExportoBatch = True
                                                    End If
                                                Else
                                                    bExportoBatch = True
                                                End If
                                            End If
                                        End If
                                    End If
                                Else
                                    ' Changed 29.03.06 by JanP due to mix of KID / nonKID
                                    'Exit For
                                End If 'falseOCR

                                'Old code

                                '                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                '                        If Not bMultiFiles Then
                                '                            bExportoBatch = True
                                '                        Else
                                '                            If oPayment.I_Account = sI_Account Then
                                '                                If InStr(oPayment.REF_Own, "&?") Then
                                '                                    'Set in the part of the OwnRef that BabelBank is using
                                '                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                '                                Else
                                '                                    sNewOwnref = ""
                                '                                End If
                                '                                If sNewOwnref = sOwnRef Then
                                '                                    bExportoBatch = True
                                '                                End If
                                '                            End If
                                '                        End If
                                '                    End If
                                'If true exit the loop, and we have data to export


                                If bExportoBatch Then
                                    Exit For
                                End If
                            Next oPayment


                            If bExportoBatch Then

                                For Each oPayment In oBatch.Payments

                                    'New code 07.02.2006
                                    bExportoPayment = False
                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile

                                    '28.10.2008 changed to IsOCR
                                    If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And IsOCR((oPayment.PayCode))) Or (bFalseOCR And Not bOCRTransactions And Not IsOCR((oPayment.PayCode))) Then
                                        'Old code
                                        'If Not bFalseOCR Or (bFalseOCR And bOCRTransactions And oPayment.PayCode = "510") Or (bFalseOCR And Not bOCRTransactions And oPayment.PayCode <> "510") Then
                                        'If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        ' Changed by JanP 07.01.04
                                        If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then

                                            If Not bMultiFiles Then
                                                bExportoPayment = True
                                            Else
                                                'If oPayment.I_Account = sI_Account Then
                                                ' 01.10.2018 – changed above If to:
                                                If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                                    If Len(oPayment.VB_ClientNo) > 0 Then
                                                        If oPayment.VB_ClientNo = sClientNo Then
                                                            bExportoPayment = True
                                                        End If
                                                    Else
                                                        bExportoPayment = True
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If 'falseOCR

                                    'Old code
                                    '                        'Have to go through the payment-object to see if we have objects thatt shall
                                    '                        ' be exported to this exportfile
                                    '                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    '                            If Not bMultiFiles Then
                                    '                                bExportoPayment = True
                                    '                            Else
                                    '                                If oPayment.I_Account = sI_Account Then
                                    '                                    If InStr(oPayment.REF_Own, "&?") Then
                                    '                                        'Set in the part of the OwnRef that BabelBank is using
                                    '                                        sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                    '                                    Else
                                    '                                        sNewOwnref = ""
                                    '                                    End If
                                    '                                    If sNewOwnref = sOwnRef Then
                                    '                                        bExportoPayment = True
                                    '                                    End If
                                    '                                End If
                                    '                            End If
                                    '                        End If

                                    If bExportoPayment Then

                                        If oPayment.I_Account <> sOldAccountNo Then
                                            If oPayment.VB_ProfileInUse Then 'And oPayment.I_Client = "" Then
                                                sContractNo = ""
                                                bAccountFound = False
                                                bFoundContractNo = False
                                                For Each oFilesetup In oPayment.VB_Profile.FileSetups
                                                    For Each oClient In oFilesetup.Clients
                                                        For Each oaccount In oClient.Accounts
                                                            '10.09.2019
                                                            If oaccount.Account = oPayment.I_Account Or oaccount.ConvertedAccount = oPayment.I_Account Then
                                                                bAccountFound = True
                                                                sContractNo = oaccount.ContractNo
                                                                sSpecial = oFilesetup.TreatSpecial
                                                                If Len(sContractNo) > 0 Then
                                                                    bFoundContractNo = True
                                                                    Exit For
                                                                End If
                                                            End If
                                                            If bFoundContractNo Then
                                                                Exit For
                                                            End If
                                                        Next oaccount
                                                        If bFoundContractNo Then
                                                            Exit For
                                                        End If
                                                    Next oClient
                                                    If bFoundContractNo Then
                                                        Exit For
                                                    End If
                                                Next oFilesetup
                                                If Not bAccountFound Then
                                                    Err.Raise(35028, , LRS(35028, oPayment.I_Account) & vbCrLf & LRS(35004, "Bankgirot OCR") & vbCrLf & LRS(35005, sFilenameOut) & vbCrLf & vbCrLf & LRS(10017))
                                                    '35023: No %2 (Servicebyrånummer/Servicebyrånamn) stated for account: %1.
                                                    'Unable to create a correct OCR-file.
                                                Else
                                                    If Not bFoundContractNo Then
                                                        Err.Raise(35023, , LRS(35023, oPayment.I_Account, "Servicebyrånummer") & vbCrLf & LRS(35004, "Bankgirot OCR") & vbCrLf & LRS(35005, sFilenameOut) & vbCrLf & vbCrLf & LRS(10017))
                                                        '35023: No %2 (Servicebyrånummer) stated for account: %1.
                                                        'Unable to create a correct OCR-file.
                                                    End If
                                                End If
                                            End If 'oPayment.VB_ProfileInUse
                                        End If 'oPayment.I_Account <> sOldAccountNo Then

                                        'Have to write the start-file record
                                        If Not bTK00Written Then
                                            If oBabel.ImportFormat = BabelFiles.FileType.OCR_Bankgirot Then
                                                If sCompanyNo <> "" Then
                                                    sI_EnterpriseNo = sCompanyNo
                                                Else
                                                    sI_EnterpriseNo = oBatch.I_EnterpriseNo
                                                End If
                                            Else
                                                sI_EnterpriseNo = sCompanyNo
                                            End If
                                            'Productiondate is written in the file, save the productiondate
                                            If Not bProductionDateSet Then
                                                dProductionDate = StringToDate((oBabel.DATE_Production))
                                                bProductionDateSet = True
                                            End If

                                            sLine = wr_TK00(dProductionDate, sContractNo)
                                            oFile.WriteLine((sLine))
                                            sLine = wr_TK10()
                                            oFile.WriteLine((sLine))
                                            bTK00Written = True
                                        End If

                                        For Each oInvoice In oPayment.Invoices

                                            If Not bTK20Written Then
                                                sLine = wr_TK20(oPayment)
                                                oFile.WriteLine((sLine))
                                                sLine = wr_TK30(oPayment)
                                                oFile.WriteLine((sLine))
                                                bTK20Written = True
                                                oTempPayment = oPayment
                                                'Have to set this, because we need the info outside the For .. next oPayment-loop
                                            End If

                                            nBatchSumAmount = nBatchSumAmount + oInvoice.MON_TransferredAmount
                                            nBatchNoTransactions = nBatchNoTransactions + 1

                                            sLine = wr_TK40(oPayment, oInvoice)
                                            oFile.WriteLine((sLine))

                                        Next oInvoice

                                        oPayment.Exported = True

                                    End If 'bExportoPayment

                                Next oPayment

                                sLine = wr_TK50(oTempPayment)
                                oFile.WriteLine((sLine))
                                bTK20Written = False
                                'UPGRADE_NOTE: Object oTempPayment may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                oTempPayment = Nothing

                                nFileSumAmount = nFileSumAmount + nBatchSumAmount
                                nFileNoTransactions = nFileNoTransactions + nBatchNoTransactions
                                nBatchSumAmount = 0
                                nBatchNoTransactions = 0

                            End If 'bExportoBatch

                        Next oBatch

                    End If 'bExportoBabel

                End If 'oBabel.VB_FilenameInNo = iFilenameInNo

            Next oBabel

            If nFileNoTransactions > 0 Then
                sLine = wr_TK90(dProductionDate, sI_EnterpriseNo)
                oFile.WriteLine((sLine))
                oFile.Close()
                oFile = Nothing
            Else
                oFile.Close()
                oFile = Nothing
                oFs.DeleteFile((sFilenameOut))
            End If

        Catch ex As Exception

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            Throw New vbBabel.Payment.PaymentException("Function: WriteOCRBankgirotFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteOCRBankgirotFile = True

    End Function
	
	Private Function wr_TK00(ByRef dProductionDate As Date, ByRef sContractNo As String) As String
		Dim sLine As String
		
		sLine = "00"
		sLine = sLine & PadLeft(sContractNo, 6, " ") 'Servicebyrånummer if the customer has joint delivery with another customernumber.
		sLine = sLine & Space(41)
		If dProductionDate <> DateSerial(CInt("1990"), CInt("01"), CInt("01")) Then
            sLine = sLine & VB6.Format(dProductionDate, "YYMMDD") 'Productiondate
        Else
            sLine = sLine & VB6.Format(Now, "YYMMDD")
        End If
        sLine = sLine & Space(16) 'Filler
        sLine = sLine & "BANKGIROT" 'Fixed

        wr_TK00 = sLine

    End Function
    Private Function wr_TK10() As String
        Dim sLine As String

        sLine = "10" & Space(78)
        wr_TK10 = sLine

    End Function
    Private Function wr_TK90(ByRef dProductionDate As Date, ByRef sContractNo As String) As String
        Dim sLine As String

        sLine = "90"
        sLine = sLine & PadLeft(sContractNo, 6, " ") 'Servicebyrånummer if the customer has joint delivery with another customernumber.
        sLine = sLine & Space(10) 'Filler
        If dProductionDate <> DateSerial(CInt("1990"), CInt("01"), CInt("01")) Then
            sLine = sLine & VB6.Format(dProductionDate, "YYMMDD") 'Productiondate
        Else
            sLine = sLine & VB6.Format(Now, "YYMMDD")
        End If
        sLine = sLine & PadLeft(Trim(Str(nFileNoTransactions)), 7, "0") 'Total number of transactions for this file
        sLine = sLine & PadLeft(Trim(Str(nFileSumAmount)), 15, "0") 'Total amount for this file
        sLine = sLine & Space(34) 'Filler

        wr_TK90 = sLine

    End Function
    Private Function wr_TK20(ByRef oPayment As Payment) As String
        Dim sLine As String

        sLine = "20"
        sLine = sLine & Space(6)
        sLine = sLine & PadLeft(oPayment.I_Account, 10, " ") 'Credit account
        sLine = sLine & Space(62)
        wr_TK20 = sLine

    End Function
    Private Function wr_TK30(ByRef oPayment As Payment) As String
        Dim sLine As String

        sLine = "30"
        sLine = sLine & Space(6)
        sLine = sLine & PadLeft(oPayment.I_Account, 10, " ") 'Credit account
        '10.09.2019 - Changed from CDate to StringToDate
        If StringToDate(oPayment.DATE_Payment) <> DateSerial(CInt("1990"), CInt("01"), CInt("01")) Then
            sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Payment)), "YYMMDD") ''Treatmentday'
        Else
            sLine = sLine & VB6.Format(Now, "YYMMDD")
        End If
        sLine = sLine & Space(46)
        sLine = sLine & Space(10) 'Hänvisningsbankgironummer, if position 9-18 is a Postgironumber
        wr_TK30 = sLine

    End Function

    Private Function wr_TK50(ByRef oPayment As Payment) As String

        sLine = "50"
        sLine = sLine & Space(6)
        sLine = sLine & PadLeft(oPayment.I_Account, 10, " ") 'Credit account
        '10.09.2019 - Changed from CDate to StringToDate
        If StringToDate(oPayment.DATE_Payment) <> DateSerial(CInt("1990"), CInt("01"), CInt("01")) Then
            sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Payment)), "YYMMDD") ''Treatmentday'
        Else
            sLine = sLine & VB6.Format(Now, "YYMMDD")
        End If
        sLine = sLine & PadLeft(Trim(Str(nBatchNoTransactions)), 7, "0") 'Total number of transactions for this accountnumber and "treatmentday"
        sLine = sLine & PadLeft(Trim(Str(nBatchSumAmount)), 15, "0") 'Total amount for this accountnumber and "treatmentday"
        sLine = sLine & Space(24) 'Filler
        sLine = sLine & Space(10) 'Hänvisningsbankgironummer, if position 9-18 is a Postgironumber
        wr_TK50 = sLine


    End Function
	
	Private Function wr_TK40(ByRef oPayment As Payment, ByRef oInvoice As Invoice) As String
		Dim sStringAmount As String
		Dim sSignCharacter As String
		
		sLine = "40"
		sLine = sLine & PadLeft(oInvoice.Unique_Id, 25, " ")
		If oPayment.MON_TransferredAmount < 0 Then
			sStringAmount = Trim(Str(oInvoice.MON_TransferredAmount))
			Select Case Right(sStringAmount, 1)
				Case "0"
					sSignCharacter = "-"
				Case "1"
					sSignCharacter = "J"
				Case "2"
					sSignCharacter = "K"
				Case "3"
					sSignCharacter = "L"
				Case "4"
					sSignCharacter = "M"
				Case "5"
					sSignCharacter = "N"
				Case "6"
					sSignCharacter = "O"
				Case "7"
					sSignCharacter = "P"
				Case "8"
					sSignCharacter = "Q"
				Case "9"
					sSignCharacter = "R"
			End Select
			sStringAmount = Left(sStringAmount, Len(sStringAmount) - 1)
			sLine = sLine & PadLeft(sStringAmount & sSignCharacter, 13, "0")
		Else
			sLine = sLine & PadLeft(Trim(Str(oInvoice.MON_TransferredAmount)), 13, "0")
		End If
		sLine = sLine & " " 'Filler
		'If oPayment.PayCode = Something Then
		'sLine = sLine & "LB" 'LB, if the payment is from the LB-routine 'FIX, how do we do this
		'else
		sLine = sLine & "  "
		'endif
		sLine = sLine & Space(5) 'Filler
		'If oPayment.PayCode = Something Then
		'sLine = sLine & PadLeft(oPayment.E_Account, 10, "0") 'Debit accountNo or Bangirots ID (løpenummer)
		'else
		sLine = sLine & PadLeft(Trim(Str(nFileNoTransactions + nBatchNoTransactions)), 10, "0")
		'endif
		sLine = sLine & Space(22)
		wr_TK40 = sLine
		
	End Function
End Module
