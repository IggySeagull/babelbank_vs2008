Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("Freetext_NET.Freetext")> Public Class Freetext
	
	Private nIndex As Double
	Dim iQualifier As Short
	Dim iCol As Short
	Dim iRow As Short
	Dim sText As String
	
	'Public Key As String - 05.08.2008 Removed this. Probably not in use
	
	'********* START PROPERTY SETTINGS ***********************
	Public Property Index() As Double
		Get
			Index = nIndex
		End Get
		Set(ByVal Value As Double)
			nIndex = Value
		End Set
	End Property
	Public Property Text() As String
		Get
			Text = sText
		End Get
		Set(ByVal Value As String)
            sText = Value
        End Set
	End Property
	Public Property Qualifier() As Short
		Get
			Qualifier = iQualifier
		End Get
		Set(ByVal Value As Short)
			' 1 - Freetext
			' 2 - Text on bankstatement
			' 3 - Text automatically added from BabelBank
			' 4 - Text added by user
			iQualifier = Value
		End Set
	End Property
	Public Property Col() As Short
		Get
			Col = iCol
		End Get
		Set(ByVal Value As Short)
			iCol = Value
		End Set
	End Property
	Public Property Row() As Short
		Get
			Row = iRow
		End Get
		Set(ByVal Value As Short)
			iRow = Value
		End Set
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
    Public Function SaveData(ByVal bSQLServer As Boolean, ByRef myBBDBConnectionAccess As System.Data.OleDb.OleDbConnection, ByRef myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand, ByRef bMatchedItemsOnly As Boolean, ByRef lBabelFile_ID As Integer, ByRef lBatch_ID As Integer, ByRef lPayment_ID As Integer, ByRef lInvoice_ID As Integer, ByRef lMyID As Integer, ByRef lCompany_ID As Integer, ByRef bUseCommitAgainstBBDB As Boolean) As Boolean
        Dim sMySQL As String = ""
        Dim sErrorString As String = ""
        Dim bReturnValue As Boolean

        On Error GoTo ERRSaveData

        sMySQL = "INSERT INTO [Freetext](Company_ID, Babelfile_ID, Batch_ID, Payment_ID, "
        sMySQL = sMySQL & "Invoice_ID, Freetext_ID, XIndex, Qualifier, XText) "
        sMySQL = sMySQL & "VALUES(" & Trim(Str(lCompany_ID)) & ", " & Trim(Str(lBabelFile_ID))
        sMySQL = sMySQL & ", " & Trim(Str(lBatch_ID)) & ", " & Trim(Str(lPayment_ID))
        sMySQL = sMySQL & ", " & Trim(Str(lInvoice_ID)) & ", " & Trim(Str(lMyID))
        sMySQL = sMySQL & ", " & Trim(Str(nIndex)) & ", " & Trim(Str(iQualifier))
        sMySQL = sMySQL & ", '" & Left$(ReplaceIllegalCharactersInString(Replace(Replace(sText, vbCr, ""), vbLf, "")), 70) 'XNET - 31.07.2012 - Removed vbCR and vbLf
        sMySQL = sMySQL & "')"

        'sMySQL = "INSERT INTO FREETEXT(Company_ID, BabelFile_ID, Batch_ID, Payment_ID, Invoice_ID, Freetext_ID, Xindex, Qualifier, XText) VALUES(1, 1, 1, 1, 1, 1, 1, 'KOKO"

        myBBDB_AccessCommand.CommandText = sMySQL
        If Not myBBDB_AccessCommand.ExecuteNonQuery() = 1 Then
            Err.Raise(14000, "Freetext", sErrorString & vbCrLf & vbCrLf & "SQL: " & sMySQL)
        Else
            bReturnValue = True
            'MsgBox "Babelfile saving OK"
        End If
        'If Not ExecuteTheSQL(cnProfile, sMySQL, sErrorString, bUseCommitAgainstBBDB) Then
        '    MsgBox(sErrorString & vbCrLf & vbCrLf & "SQL: " & sMySQL)
        '    'Removed 10.01.2006 Why here when we don't return false in the other collections
        '    'bReturnValue = False
        'Else
        '    bReturnValue = True
        'End If

        SaveData = bReturnValue

        Exit Function

ERRSaveData:
        Err.Raise(999, "Freetext", Err.Description & vbCrLf & vbCrLf & "SQL: " & sMySQL)

    End Function
    Public Function RetrieveData(ByVal bSQLServer As Boolean, ByRef myBBDB_AccessFreetextReader As System.Data.OleDb.OleDbDataReader) As Boolean

        Try

            'Retrieve and save the data to the local variables
            nIndex = myBBDB_AccessFreetextReader.GetInt32(0) 'Index
            iQualifier = myBBDB_AccessFreetextReader.GetInt32(1) 'StatusCode
            '29.04.2016 - Added RetrieveIllegalCharactersInString(
            sText = RetrieveIllegalCharactersInString(myBBDB_AccessFreetextReader.GetString(2)) 'StatusCode

            '            RetrieveData = True

            '            Exit Function

            'errorRetrieveData:
            '            Err.Raise(999, "Freetext", Err.Description)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)

        Finally

        End Try

        Return True

    End Function

	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		iQualifier = 1
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
End Class
