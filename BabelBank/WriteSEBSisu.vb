﻿Option Explicit On
' Har tatt utgangspunkt i WriteLeverantorsbetalningar som er ganske likt SEBSisu
Module WriteSEBSisu
    Dim sLine As String ' en output-linje
    Dim oBatch As Batch
    ' - filenivå: sum beløp, antall records, antall transer, siste dato
    '             disse påvirkes nedover i nivåene
    Dim nFileSumAmount As Double, nFileNoRecords As Double
    Dim iPayNumber As Integer
    Dim nFileSumAmountSEK As Double
    Dim bFileStartWritten As Boolean
    Function WriteSEB_Sisu(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sAdditionalID As String) As Boolean

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim i As Double, j As Double, k As Double, l As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sNewOwnref As String
        Dim sTxt As String
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bInternational As Boolean
        Dim bThereAreDomestic As Boolean
        Dim bThereAreInternational As Boolean
        Dim sOldAccount As String

        Dim sOldDebitAccount As String
        Dim oGroupPayment As vbBabel.GroupPayments

        Dim lCounter2 As Long
        Dim bSenderRecordIssued As Boolean

        ' lag en outputfil
        Try


            ' ------------------------------------------------------
            ' Organization of export;
            ' - we need to export all payments from
            ' - each senderaccount
            ' - each paymentdate
            ' in a 1 to 6 recordsequence.
            ' We will use the GroupPayments class to organize this
            ' ------------------------------------------------------

            oGroupPayment = New vbBabel.GroupPayments



            nFileNoRecords = 0

            ' New 15.02.06
            ' Did not write fileheader if we run more than once without leaving BabelBank
            If Len(Dir(sFilenameOut)) > 0 Then
                bFileStartWritten = True
            Else
                bFileStartWritten = False
            End If
            '-end 15.02.06 --------------

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            ' Possible with file with one domestic and one International batch
            ' Runs first all payments to create a Domestic batch
            ' Then a new run for International payments.
            ' This way there may be a nonsorted mix, which will be grouped in the exportfile
            bInternational = False
            ' First analyze, if there are domestic and or international payments in oBabelFiles
            bThereAreDomestic = False
            bThereAreInternational = False

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                'Only export statuscode 00 or 02. The others don't give any sense here
                If (oBabel.StatusCode = "00" And Not oBabel.FileFromBank) _
                    Or oBabel.StatusCode = "02" Then
                    'Have to go through each batch-object to see if we have objects thatt shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False And Not oPayment.Exported Then
                                    If Not bMultiFiles Then
                                        bExportoBabel = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = vbNullString
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBabel = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If
                        Next
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                Else
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            oPayment.Exported = True
                        Next oPayment
                    Next oBatch
                End If 'If oBabel.StatusCode = "00" Or oBabel.StatusCode = "02" Then

                If bExportoBabel Then

                    '            If sOldDebitAccount <> oPayment.I_Account Then
                    '                sOldDebitAccount = oPayment.I_Account
                    '                bFileStartWritten = False
                    '
                    '                ' Write endrecord for previous debit Bankgiro
                    '                If nFileNoRecords > 0 Then
                    '                    sLine = WriteSEBSisuFileEnd(oBabelFiles(1))
                    '                    oFile.WriteLine (sLine)
                    '                End If
                    '
                    '            End If

                    If Not bFileStartWritten Then
                        ' write only once!
                        sLine = WriteSEBSisuDateRecord(oBabel)
                        oFile.WriteLine(sLine)

                        iPayNumber = 0
                        bFileStartWritten = True
                    End If

                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches

                        ' Group on senders account and paymentdate
                        oGroupPayment.SetGroupingParameters(True, True, False, False, False, False)

                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile


                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False And Not oPayment.Exported Then

                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = vbNullString
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next


                        If bExportoBatch Then
                            i = i + 1


                            ' Because of grouping, we need to take this tour several times, until all payments exported
                            For lCounter2 = 1 To oBatch.Payments.Count
                                bSenderRecordIssued = False

                                ' TODO Visma -
                                ' er usikker på om dette skal settes her - MÅ TESTES MED KOMPLISERT CASE !!!!!
                                oGroupPayment.Batch = oBatch
                                oGroupPayment.MarkIdenticalPayments()

                                j = 0
                                For Each oPayment In oBatch.Payments
                                    ' Specialmark is set for those payments with this "runs" I_Account and Date
                                    If oPayment.SpecialMark And Not oPayment.Exported Then
                                        If Not bSenderRecordIssued Then
                                            ' add a sender-record pr "run" -
                                            ' that is when either senderaccount or paymentdate changes
                                            sLine = WriteSEBSisu_SenderRecord(oBabel, oBatch, oPayment.DATE_Payment)
                                            oFile.WriteLine(sLine)
                                            bSenderRecordIssued = True
                                        End If


                                        bExportoPayment = False
                                        'Have to go through the payment-object to see if we have objects thatt shall
                                        ' be exported to this exportfile
                                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                            'Don't export payments that have been cancelled
                                            If oPayment.SpecialMark And oPayment.Cancel = False And Not oPayment.Exported Then

                                                If Not bMultiFiles Then
                                                    bExportoPayment = True
                                                Else
                                                    If oPayment.I_Account = sI_Account Then
                                                        If InStr(oPayment.REF_Own, "&?") Then
                                                            'Set in the part of the OwnRef that BabelBank is using
                                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                        Else
                                                            sNewOwnref = vbNullString
                                                        End If
                                                        If sNewOwnref = sOwnRef Then
                                                            bExportoPayment = True
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                oPayment.Exported = True
                                            End If 'If oPayment.Cancel = False Then
                                        End If


                                        j = j + 1

                                        For Each oInvoice In oPayment.Invoices
                                            iPayNumber = iPayNumber + 1
                                            sLine = WriteSEBSisu_Namnpost(oPayment)
                                            oFile.WriteLine(sLine)
                                            sLine = WriteSEBSisu_Adresspost(oPayment)
                                            oFile.WriteLine(sLine)
                                            sLine = WriteSEBSisu_Bankpost(oPayment)
                                            oFile.WriteLine(sLine)

                                            ' XOKNET 26.07.2013 - adds freetext for this invoice
                                            sTxt = vbNullString
                                            For Each oFreeText In oInvoice.Freetexts
                                                sTxt = sTxt & " " & Trim$(oFreeText.Text)
                                            Next
                                            sTxt = Trim(sTxt)

                                            ' XOKNET 26.07.2013 - two runs of invoice/riksbank, to be able to pass ownref for use in returnfiles
                                            ' 1. run
                                            sLine = WriteSEBSisu_VoucherRecord(oPayment, oInvoice, sTxt, False)
                                            oFile.WriteLine(sLine)
                                            sLine = WriteSEBSisu_Riksbankpost(oInvoice)
                                            oFile.WriteLine(sLine)

                                            ' added option to not use returnfiles (for Visma) - then no need for 2. run
                                            If oBabel.UseReturnFiles Then
                                                ' 2. run
                                                If Not EmptyString(oInvoice.REF_Own) Then
                                                    sLine = WriteSEBSisu_VoucherRecord(oPayment, oInvoice, oInvoice.REF_Own, True)
                                                    oFile.WriteLine(sLine)
                                                    sLine = WriteSEBSisu_Riksbankpost(oInvoice)
                                                    oFile.WriteLine(sLine)
                                                End If
                                            End If
                                            ' XokNET 05.02.2015 added next line
                                            oInvoice.Exported = True

                                        Next oInvoice
                                        oPayment.Exported = True
                                    End If   'If oPayment.SpecialMark And Not oPayment.Exported Then

                                Next ' payment
                                If nFileNoRecords > 0 Then
                                    sLine = WriteSEBSisu_Avstamningspost(oBabelFiles(1))
                                    oFile.WriteLine(sLine)
                                    nFileNoRecords = 0
                                    bSenderRecordIssued = False
                                End If

                            Next lCounter2
                        End If

                    Next 'batch
                End If
            Next 'Babelfile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteSEB_Sisu" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteSEB_Sisu = True

    End Function
    Function WriteSEBSisuDateRecord(ByVal oBabelFile As BabelFile) As String
        Dim sLine As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileSumAmountSEK = 0
        nFileNoRecords = 1

        sLine = "D"        ' 1
        If oBabelFile.Batches.Count > 0 Then
            If oBabelFile.Batches(1).Payments.Count > 0 Then
                ' Avsändarens kontonummer
                sLine = sLine & PadLeft(Replace(Replace(oBabelFile.Batches(1).Payments(1).I_Account, "-", ""), " ", ""), 11, "0") '2-12
                sLine = sLine & VB6.Format(Date.Today, "YYMMDD")  '13-18
                sLine = sLine & "SESISUVERSION2" '19-32
                sLine = sLine & New String("0", 48)
            End If
        End If

        WriteSEBSisuDateRecord = sLine

    End Function
    Function WriteSEBSisu_SenderRecord(ByVal oBabelFile As BabelFile, ByVal oBatch As Batch, ByVal sDate As String) As String
        Dim sLine As String
        ' XokNET 20.03.2015 added next
        Dim sSenderAdr As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileSumAmountSEK = 0
        nFileNoRecords = nFileNoRecords + 1

        sLine = "1"        ' 1
        If oBatch.Payments.Count > 0 Then
            If oBatch.Payments.Count > 0 Then
                ' Avsändarkonto
                sLine = sLine & PadLeft(Replace(Replace(oBatch.Payments(1).I_Account, "-", ""), " ", "0"), 11, "0") '2-12
                If oBatch.VB_ProfileInUse Then
                    sLine = sLine & PadRight(UCase(oBatch.VB_Profile.CompanyName), 25, " ") '13-37
                    sLine = sLine & PadRight(oBatch.VB_Profile.CompanyZip & " " & oBatch.VB_Profile.CompanyCity, 35, " ") '38-72
                Else
                    If IsThisVismaIntegrator Then
                        sLine = sLine & PadRight(UCase(oBabelFile.Visma_FrmName), 25, " ") '13-37
                        'sLine = sLine & Space(35) '38-72
                        'XokNET 20.03.2015 added senders address
                        If oBatch.Payments.Count > 0 Then
                            sSenderAdr = Left$(Trim(oBatch.Payments(1).I_Adr1) + " " & Trim(oBatch.Payments(1).I_Adr2) & " " & Trim(oBatch.Payments(1).I_Zip) & " " & Trim(oBatch.Payments(1).I_City), 35)
                        Else
                            sSenderAdr = ""
                        End If
                        sLine = sLine & PadRight(sSenderAdr, 35, " ")
                    Else
                        sLine = sLine & Space(25) '13-37
                        sLine = sLine & Space(35) '38-72
                    End If
                End If
                sLine = sLine & Mid$(sDate, 3) ' Betalningsdatum YYMMDD
                sLine = sLine & "0"    '79 Merge, set meringing ok = "0"
                sLine = sLine & Space(1)    '80, reserved
            End If
        End If

        WriteSEBSisu_SenderRecord = sLine

    End Function

    Function WriteSEBSisu_Namnpost(ByVal oPayment As Payment) As String
        Dim sLine As String
        nFileNoRecords = nFileNoRecords + 1

        sLine = "2"        '1
        If Not EmptyString(oPayment.Invoices(1).SupplierNo) Then
            sLine = sLine & PadLeft(oPayment.Invoices(1).SupplierNo, 7, "0")   ' 2-8  Løpenr mottager
        Else
            sLine = sLine & PadLeft(Str(iPayNumber), 7, "0")  ' 2-8  Løpenr mottager
        End If

        sLine = sLine & PadRight(UCase(oPayment.E_Name), 30, " ")  '9-38  Mottakernamn
        sLine = sLine & Space(33)                           '39-71  Mottakernamn2
        sLine = sLine & Space(7)                            '72-78
        sLine = sLine & "0"                                 '79 Choice of debitaccount
        ' Fee code
        ' 0 Sender pays Swedish charges
        ' 1 If express payments, sender pays swedish, receiver other
        ' 2 All charges paid by receiver
        ' 3 All charges paid by sender
        If oPayment.MON_ChargeMeDomestic = True And oPayment.MON_ChargeMeAbroad = False Then
            ' Shared
            sLine = sLine & "0"                                 '80 Fee code
        ElseIf oPayment.MON_ChargeMeDomestic = False And oPayment.MON_ChargeMeAbroad = False Then
            sLine = sLine & "2"                                 '80 Fee code
        ElseIf oPayment.MON_ChargeMeDomestic = True And oPayment.MON_ChargeMeAbroad = True Then
            sLine = sLine & "3"                                 '80 Fee code
        Else
            sLine = sLine & "0"                                 '80 Fee code
        End If

        WriteSEBSisu_Namnpost = sLine

    End Function
    Function WriteSEBSisu_Adresspost(ByVal oPayment As Payment) As String
        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1

        sLine = "3"        '1
        If Not EmptyString(oPayment.Invoices(1).SupplierNo) Then
            sLine = sLine & PadLeft(oPayment.Invoices(1).SupplierNo, 7, "0")   ' 2-8  Løpenr mottager
        Else
            sLine = sLine & PadLeft(Str(iPayNumber), 7, "0")   ' 2-8  Løpenr mottager
        End If
        ' Payment form
        ' 0 = SEB sends cheque to receivers;
        If EmptyString(oPayment.E_Account) Then
            sLine = sLine & "0"         '9 Payment form
        Else
            sLine = sLine & "1"         '9 Payment form
        End If
        If oPayment.Priority Then
            ' 1 = Express
            sLine = sLine & "1"         '10 Payment method
        ElseIf oPayment.ToOwnAccount Or oPayment.PayCode = "403" Then
            ' 2 = Intracompany
            sLine = sLine & "2"         '10 Payment method
        Else
            sLine = sLine & "0"         '10 Payment method
        End If
        sLine = sLine & "  "         '11-12 reserved

        sLine = sLine & PadRight(UCase(Trim$(oPayment.E_Adr1) & " " & Trim$(oPayment.E_Adr2)), 30, " ")  '13-42  Mottaker adr1
        If Len(Trim$(oPayment.E_Adr3)) > 0 Then
            sLine = sLine & PadRight(UCase(oPayment.E_Adr3), 33, " ")  '43-75
        ElseIf Len(Trim$(oPayment.E_Adr1)) + 1 + Len(Trim$(oPayment.E_Adr2)) > 30 Then
            sLine = sLine & PadRight(UCase(oPayment.E_Adr2), 33, " ")  '43-75
        Else
            sLine = sLine & Space(33)
        End If

        sLine = sLine & "  "     ' 76-77
        sLine = sLine & PadRight(UCase(oPayment.E_CountryCode), 2, " ")  ' 78-79 Countrycode
        sLine = sLine & Space(1) ' 80
        WriteSEBSisu_Adresspost = sLine

    End Function
    Function WriteSEBSisu_Bankpost(ByVal oPayment As Payment) As String
        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1

        sLine = "4"        '1
        If Not EmptyString(oPayment.Invoices(1).SupplierNo) Then
            sLine = sLine & PadLeft(oPayment.Invoices(1).SupplierNo, 7, "0")   ' 2-8  Løpenr mottager
        Else
            sLine = sLine & PadLeft(Str(iPayNumber), 7, "0")   ' 2-8  Løpenr mottager
        End If
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCode, 12, " ")  '9-20  SWIFT
        sLine = sLine & PadRight(Replace(oPayment.E_Account, " ", ""), 30, " ") '21-50 Leverantørkonto
        If Not EmptyString(oPayment.BANK_BranchNo) Then
            sLine = sLine & PadRight("//" & oPayment.BANK_BranchNo, 30, " ")  '51-80 Clearingcode eller Bankens navn
        Else
            sLine = sLine & PadRight(Trim$(oPayment.BANK_Name) & " " & Trim$(oPayment.BANK_Adr1), 30, " ")  '51-80 Clearingcode eller Bankens navn
        End If

        WriteSEBSisu_Bankpost = sLine

    End Function

    Function WriteSEBSisu_VoucherRecord(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sTxt As String, ByVal bZeroAmount As Boolean) As String  ' XOKNET 26.07.2013
        Dim sLine As String
        Dim sAmount As String

        If Not bZeroAmount Then
            nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
            nFileSumAmountSEK = nFileSumAmountSEK + oInvoice.MON_LocalAmount
        End If
        nFileNoRecords = nFileNoRecords + 1

        sLine = "5"        '1
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadLeft(oInvoice.SupplierNo, 7, "0")   ' 2-8  Løpenr mottager
        Else
            sLine = sLine & PadLeft(Str(iPayNumber), 7, "0")  ' 2-8  Løpenr mottager
        End If
        sLine = sLine & PadRight(sTxt, 25, " ")             '9-33 Optional reference

        sAmount = LTrim(Str(Math.Abs(oInvoice.MON_LocalAmount)))                          '34-44 Beløp i SEK
        If oInvoice.MON_LocalAmount < 0 Then
            ' signed amount if creditnotes
            ' Use last char as signed if negativve amounts
            Select Case Right$(sAmount, 1)
                Case "0"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "-"
                Case "1"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "J"
                Case "2"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "K"
                Case "3"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "L"
                Case "4"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "M"
                Case "5"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "N"
                Case "6"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "O"
                Case "7"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "P"
                Case "8"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "Q"
                Case "9"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "R"
            End Select
        End If
        If bZeroAmount Then
            sAmount = 0
        End If
        sLine = sLine & PadLeft(sAmount, 11, "0")                            '34-44 Beløp i SEK

        sLine = sLine & New String(" ", 10) '45-54 Ikke i bruk her
        sLine = sLine & PadRight(UCase(oPayment.MON_InvoiceCurrency), 3, " ") '55-57 Valutakod
        ' Bruk den dato vi har. Den får heller settes riktig i importrutinen eller TreatSpecial
        If oPayment.MON_InvoiceAmount < 0 Then
            sLine = sLine & Mid$(oPayment.DATE_Payment, 3, 6)         '58-63 Siste bevakningsdag
        Else
            sLine = sLine & New String("0", 6) '58-63 not in use for positive amounts
        End If
        ' bankspesifikt
        sLine = sLine & "00"                           '64-65

        sAmount = LTrim(Str(Math.Abs(oInvoice.MON_InvoiceAmount)))
        If oInvoice.MON_InvoiceAmount < 0 Then
            ' signed amount if creditnotes
            ' Use last char as signed
            Select Case Right$(sAmount, 1)
                Case "0"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "-"
                Case "1"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "J"
                Case "2"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "K"
                Case "3"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "L"
                Case "4"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "M"
                Case "5"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "N"
                Case "6"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "O"
                Case "7"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "P"
                Case "8"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "Q"
                Case "9"
                    sAmount = Left$(sAmount, Len(sAmount) - 1) & "R"
            End Select
        End If
        If bZeroAmount Then
            sAmount = 0
        End If
        sLine = sLine & PadLeft(sAmount, 13, "0")  '66-78 FakturaBelopp, 2 des.

        sLine = sLine & "0"     ' 79  Accounting period
        ' XOKNET 26.07.2013
        sLine = sLine & "0"     ' 80  Conversioncode

        WriteSEBSisu_VoucherRecord = sLine

    End Function
    Function WriteSEBSisu_Riksbankpost(ByVal oInvoice As Invoice) As String
        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1

        sLine = "R"        '1
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadLeft(oInvoice.SupplierNo, 7, "0")   ' 2-8  Løpenr mottager
        Else
            sLine = sLine & PadLeft(Str(iPayNumber), 7, "0")  ' 2-8  Løpenr mottager
        End If
        sLine = sLine & Space(1)        '9

        sLine = sLine & PadLeft(Trim(oInvoice.STATEBANK_Code), 3, " ")   '10-12  Riksbankkode
        sLine = sLine & Space(68)       '13-80, Bankspecific, not used by Föreningssparbanken

        WriteSEBSisu_Riksbankpost = sLine

    End Function

    Function WriteSEBSisu_Avstamningspost(ByVal oBabelFile As BabelFile) As String
        Dim sLine As String
        nFileNoRecords = nFileNoRecords + 1

        sLine = "6"        ' 1
        If oBabelFile.Batches.Count > 0 Then
            If oBabelFile.Batches(1).Payments.Count > 0 Then
                ' Avsändarnummer
                'sLine = sLine & PadLeft(Replace(Replace(oBabelFile.Batches(1).Payments(1).I_Account, "-", ""), " ", ""), 7, "0") '2-8
                sLine = sLine & New String("0", 7)
                sLine = sLine & PadLeft(nFileSumAmountSEK, 12, "0") ' 9-20 Sum reskontrobeløp
                sLine = sLine & Space(12)                        ' 21-32 Total no of ID-posts, Blank so far
                sLine = sLine & PadLeft(nFileNoRecords, 12, "0") ' 33-44 Total antall poster
                sLine = sLine & Space(19)                        ' 45-63 Blank
                sLine = sLine & PadLeft(nFileSumAmount, 15, "0") ' 64-78 Hashtotal fakturabeløp
                sLine = sLine & Space(2)    '79-80
            End If
        End If
        'pad to 80 with 0
        sLine = PadLine(sLine, 80, "0")

        WriteSEBSisu_Avstamningspost = sLine

    End Function

End Module
