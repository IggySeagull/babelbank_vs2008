<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_952_Statistics
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rp_952_Statistics))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.subAutoMatched = New DataDynamics.ActiveReports.SubReport
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.Line3 = New DataDynamics.ActiveReports.Line
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grMainHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapegrMainHeader = New DataDynamics.ActiveReports.Shape
        Me.lblDateInterval = New DataDynamics.ActiveReports.Label
        Me.lblPayments = New DataDynamics.ActiveReports.Label
        Me.lblPercent = New DataDynamics.ActiveReports.Label
        Me.lblAmount = New DataDynamics.ActiveReports.Label
        Me.lblInvoices = New DataDynamics.ActiveReports.Label
        Me.lblPercentInv = New DataDynamics.ActiveReports.Label
        Me.grMainFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.grAccumulatedHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.shapegrAccumulatedHeader = New DataDynamics.ActiveReports.Shape
        Me.lblTotal = New DataDynamics.ActiveReports.Label
        Me.txtTotalCount = New DataDynamics.ActiveReports.TextBox
        Me.lblOCR = New DataDynamics.ActiveReports.Label
        Me.lblAutogiro = New DataDynamics.ActiveReports.Label
        Me.lblOther = New DataDynamics.ActiveReports.Label
        Me.txtTotalPercent = New DataDynamics.ActiveReports.TextBox
        Me.txtTotalAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtOCRCount = New DataDynamics.ActiveReports.TextBox
        Me.txtAGCount = New DataDynamics.ActiveReports.TextBox
        Me.txtOtherCount = New DataDynamics.ActiveReports.TextBox
        Me.txtOCRPercent = New DataDynamics.ActiveReports.TextBox
        Me.txtAGPercent = New DataDynamics.ActiveReports.TextBox
        Me.txtOtherPercent = New DataDynamics.ActiveReports.TextBox
        Me.txtOCRAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtAGAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtOtherAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblAuto = New DataDynamics.ActiveReports.Label
        Me.txtAutoCount = New DataDynamics.ActiveReports.TextBox
        Me.txtAutoPercent = New DataDynamics.ActiveReports.TextBox
        Me.txtAutoAmount = New DataDynamics.ActiveReports.TextBox
        Me.lblProposed = New DataDynamics.ActiveReports.Label
        Me.lblFinal = New DataDynamics.ActiveReports.Label
        Me.txtAutoProposed = New DataDynamics.ActiveReports.TextBox
        Me.txtAutoFinal = New DataDynamics.ActiveReports.TextBox
        Me.txtAutoPercentInv = New DataDynamics.ActiveReports.TextBox
        Me.grAccumulatedFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.shapegrAccumulatedFooter = New DataDynamics.ActiveReports.Shape
        Me.lblUnPostedAuto = New DataDynamics.ActiveReports.Label
        Me.txtUnPostedAutoCount = New DataDynamics.ActiveReports.TextBox
        Me.txtUnPostedAutoPercent = New DataDynamics.ActiveReports.TextBox
        Me.txtUnPostedAutoAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtUnPostedAutoFinal = New DataDynamics.ActiveReports.TextBox
        Me.txtUnPostedAutoPercentInv = New DataDynamics.ActiveReports.TextBox
        Me.lblManual = New DataDynamics.ActiveReports.Label
        Me.txtManualCount = New DataDynamics.ActiveReports.TextBox
        Me.txtManualFinal = New DataDynamics.ActiveReports.TextBox
        Me.lblUnMatched = New DataDynamics.ActiveReports.Label
        Me.txtUnMatchedCount = New DataDynamics.ActiveReports.TextBox
        Me.txtUnMatchedAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtUnMatchedFinal = New DataDynamics.ActiveReports.TextBox
        Me.subManualMatched = New DataDynamics.ActiveReports.SubReport
        Me.TotalSum = New DataDynamics.ActiveReports.Field
        Me.shapegrAccumulated2Footer = New DataDynamics.ActiveReports.Shape
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDateInterval, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPercent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblInvoices, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPercentInv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblOCR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblAutogiro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblOther, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalPercent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOCRCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAGCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOtherCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOCRPercent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAGPercent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOtherPercent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOCRAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAGAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOtherAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblAuto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAutoCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAutoPercent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAutoAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblProposed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAutoProposed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAutoFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAutoPercentInv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblUnPostedAuto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnPostedAutoCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnPostedAutoPercent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnPostedAutoAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnPostedAutoFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnPostedAutoPercentInv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblManual, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtManualCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtManualFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblUnMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnMatchedCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnMatchedAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnMatchedFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.subAutoMatched})
        Me.Detail.Height = 0.1770833!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'subAutoMatched
        '
        Me.subAutoMatched.Border.BottomColor = System.Drawing.Color.Black
        Me.subAutoMatched.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.subAutoMatched.Border.LeftColor = System.Drawing.Color.Black
        Me.subAutoMatched.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.subAutoMatched.Border.RightColor = System.Drawing.Color.Black
        Me.subAutoMatched.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.subAutoMatched.Border.TopColor = System.Drawing.Color.Black
        Me.subAutoMatched.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.subAutoMatched.CloseBorder = False
        Me.subAutoMatched.Height = 0.188!
        Me.subAutoMatched.Left = 0.0!
        Me.subAutoMatched.Name = "subAutoMatched"
        Me.subAutoMatched.Report = Nothing
        Me.subAutoMatched.ReportName = "rp_Sub_952_AutoMatch"
        Me.subAutoMatched.Top = 0.0!
        Me.subAutoMatched.Width = 9.8!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.Line3, Me.rptInfoPageHeaderDate})
        Me.PageHeader.Height = 0.4006945!
        Me.PageHeader.Name = "PageHeader"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Border.BottomColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Border.LeftColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Border.RightColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Border.TopColor = System.Drawing.Color.Black
        Me.lblrptHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 1.279861!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "text-align: center; font-size: 18pt; "
        Me.lblrptHeader.Text = "Statistics"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'Line3
        '
        Me.Line3.Border.BottomColor = System.Drawing.Color.Black
        Me.Line3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.LeftColor = System.Drawing.Color.Black
        Me.Line3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.RightColor = System.Drawing.Color.Black
        Me.Line3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.TopColor = System.Drawing.Color.Black
        Me.Line3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Height = 0.0!
        Me.Line3.Left = 0.0!
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.39375!
        Me.Line3.Width = 6.299305!
        Me.Line3.X1 = 0.0!
        Me.Line3.X2 = 6.299305!
        Me.Line3.Y1 = 0.39375!
        Me.Line3.Y2 = 0.39375!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageHeaderDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 8.0!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right; "
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter.Height = 0.2604167!
        Me.PageFooter.Name = "PageFooter"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageFooterDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterNO.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 5.9375!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right; "
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.Border.BottomColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.Border.LeftColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.Border.RightColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.Border.TopColor = System.Drawing.Color.Black
        Me.rptInfoPageCounterGB.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 7.5!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right; "
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grMainHeader
        '
        Me.grMainHeader.CanShrink = True
        Me.grMainHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapegrMainHeader, Me.lblDateInterval, Me.lblPayments, Me.lblPercent, Me.lblAmount, Me.lblInvoices, Me.lblPercentInv})
        Me.grMainHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grMainHeader.Height = 0.78125!
        Me.grMainHeader.Name = "grMainHeader"
        Me.grMainHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'shapegrMainHeader
        '
        Me.shapegrMainHeader.BackColor = System.Drawing.Color.LightGray
        Me.shapegrMainHeader.Border.BottomColor = System.Drawing.Color.Black
        Me.shapegrMainHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrMainHeader.Border.LeftColor = System.Drawing.Color.Black
        Me.shapegrMainHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrMainHeader.Border.RightColor = System.Drawing.Color.Black
        Me.shapegrMainHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrMainHeader.Border.TopColor = System.Drawing.Color.Black
        Me.shapegrMainHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrMainHeader.Height = 0.33!
        Me.shapegrMainHeader.Left = 7.0!
        Me.shapegrMainHeader.LineStyle = DataDynamics.ActiveReports.LineStyle.Transparent
        Me.shapegrMainHeader.Name = "shapegrMainHeader"
        Me.shapegrMainHeader.RoundingRadius = 9.999999!
        Me.shapegrMainHeader.Top = 0.45!
        Me.shapegrMainHeader.Width = 2.5!
        '
        'lblDateInterval
        '
        Me.lblDateInterval.Border.BottomColor = System.Drawing.Color.Black
        Me.lblDateInterval.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDateInterval.Border.LeftColor = System.Drawing.Color.Black
        Me.lblDateInterval.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDateInterval.Border.RightColor = System.Drawing.Color.Black
        Me.lblDateInterval.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDateInterval.Border.TopColor = System.Drawing.Color.Black
        Me.lblDateInterval.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDateInterval.Height = 0.375!
        Me.lblDateInterval.HyperLink = Nothing
        Me.lblDateInterval.Left = 0.0!
        Me.lblDateInterval.Name = "lblDateInterval"
        Me.lblDateInterval.Style = "font-weight: bold; font-size: 12pt; "
        Me.lblDateInterval.Text = "lblDateInterval"
        Me.lblDateInterval.Top = 0.0!
        Me.lblDateInterval.Width = 4.5!
        '
        'lblPayments
        '
        Me.lblPayments.Border.BottomColor = System.Drawing.Color.Black
        Me.lblPayments.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPayments.Border.LeftColor = System.Drawing.Color.Black
        Me.lblPayments.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPayments.Border.RightColor = System.Drawing.Color.Black
        Me.lblPayments.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPayments.Border.TopColor = System.Drawing.Color.Black
        Me.lblPayments.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPayments.Height = 0.15!
        Me.lblPayments.HyperLink = Nothing
        Me.lblPayments.Left = 4.2!
        Me.lblPayments.Name = "lblPayments"
        Me.lblPayments.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.lblPayments.Text = "lblPayments"
        Me.lblPayments.Top = 0.563!
        Me.lblPayments.Width = 0.8!
        '
        'lblPercent
        '
        Me.lblPercent.Border.BottomColor = System.Drawing.Color.Black
        Me.lblPercent.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPercent.Border.LeftColor = System.Drawing.Color.Black
        Me.lblPercent.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPercent.Border.RightColor = System.Drawing.Color.Black
        Me.lblPercent.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPercent.Border.TopColor = System.Drawing.Color.Black
        Me.lblPercent.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPercent.Height = 0.15!
        Me.lblPercent.HyperLink = Nothing
        Me.lblPercent.Left = 5.0625!
        Me.lblPercent.Name = "lblPercent"
        Me.lblPercent.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.lblPercent.Text = "lblPercent"
        Me.lblPercent.Top = 0.5625!
        Me.lblPercent.Width = 0.7!
        '
        'lblAmount
        '
        Me.lblAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.lblAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.lblAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAmount.Border.RightColor = System.Drawing.Color.Black
        Me.lblAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAmount.Border.TopColor = System.Drawing.Color.Black
        Me.lblAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAmount.Height = 0.15!
        Me.lblAmount.HyperLink = Nothing
        Me.lblAmount.Left = 5.75!
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.lblAmount.Text = "lblAmount"
        Me.lblAmount.Top = 0.5625!
        Me.lblAmount.Width = 1.0!
        '
        'lblInvoices
        '
        Me.lblInvoices.Border.BottomColor = System.Drawing.Color.Black
        Me.lblInvoices.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblInvoices.Border.LeftColor = System.Drawing.Color.Black
        Me.lblInvoices.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblInvoices.Border.RightColor = System.Drawing.Color.Black
        Me.lblInvoices.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblInvoices.Border.TopColor = System.Drawing.Color.Black
        Me.lblInvoices.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblInvoices.Height = 0.15!
        Me.lblInvoices.HyperLink = Nothing
        Me.lblInvoices.Left = 7.4375!
        Me.lblInvoices.Name = "lblInvoices"
        Me.lblInvoices.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.lblInvoices.Text = "lblInvoices"
        Me.lblInvoices.Top = 0.5625!
        Me.lblInvoices.Width = 1.0!
        '
        'lblPercentInv
        '
        Me.lblPercentInv.Border.BottomColor = System.Drawing.Color.Black
        Me.lblPercentInv.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPercentInv.Border.LeftColor = System.Drawing.Color.Black
        Me.lblPercentInv.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPercentInv.Border.RightColor = System.Drawing.Color.Black
        Me.lblPercentInv.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPercentInv.Border.TopColor = System.Drawing.Color.Black
        Me.lblPercentInv.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPercentInv.Height = 0.15!
        Me.lblPercentInv.HyperLink = Nothing
        Me.lblPercentInv.Left = 8.4375!
        Me.lblPercentInv.Name = "lblPercentInv"
        Me.lblPercentInv.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.lblPercentInv.Text = "lblPercentInv"
        Me.lblPercentInv.Top = 0.5625!
        Me.lblPercentInv.Width = 0.7!
        '
        'grMainFooter
        '
        Me.grMainFooter.CanShrink = True
        Me.grMainFooter.Height = 0.04166667!
        Me.grMainFooter.Name = "grMainFooter"
        '
        'grAccumulatedHeader
        '
        Me.grAccumulatedHeader.CanShrink = True
        Me.grAccumulatedHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapegrAccumulatedHeader, Me.lblTotal, Me.txtTotalCount, Me.lblOCR, Me.lblAutogiro, Me.lblOther, Me.txtTotalPercent, Me.txtTotalAmount, Me.txtOCRCount, Me.txtAGCount, Me.txtOtherCount, Me.txtOCRPercent, Me.txtAGPercent, Me.txtOtherPercent, Me.txtOCRAmount, Me.txtAGAmount, Me.txtOtherAmount, Me.lblAuto, Me.txtAutoCount, Me.txtAutoPercent, Me.txtAutoAmount, Me.lblProposed, Me.lblFinal, Me.txtAutoProposed, Me.txtAutoFinal, Me.txtAutoPercentInv})
        Me.grAccumulatedHeader.DataField = "BreakField"
        Me.grAccumulatedHeader.Height = 1.25!
        Me.grAccumulatedHeader.Name = "grAccumulatedHeader"
        Me.grAccumulatedHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'shapegrAccumulatedHeader
        '
        Me.shapegrAccumulatedHeader.BackColor = System.Drawing.Color.LightGray
        Me.shapegrAccumulatedHeader.Border.BottomColor = System.Drawing.Color.Black
        Me.shapegrAccumulatedHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulatedHeader.Border.LeftColor = System.Drawing.Color.Black
        Me.shapegrAccumulatedHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulatedHeader.Border.RightColor = System.Drawing.Color.Black
        Me.shapegrAccumulatedHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulatedHeader.Border.TopColor = System.Drawing.Color.Black
        Me.shapegrAccumulatedHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulatedHeader.Height = 1.45!
        Me.shapegrAccumulatedHeader.Left = 7.0!
        Me.shapegrAccumulatedHeader.LineStyle = DataDynamics.ActiveReports.LineStyle.Transparent
        Me.shapegrAccumulatedHeader.Name = "shapegrAccumulatedHeader"
        Me.shapegrAccumulatedHeader.RoundingRadius = 9.999999!
        Me.shapegrAccumulatedHeader.Top = -0.1!
        Me.shapegrAccumulatedHeader.Width = 2.5!
        '
        'lblTotal
        '
        Me.lblTotal.Border.BottomColor = System.Drawing.Color.Black
        Me.lblTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotal.Border.LeftColor = System.Drawing.Color.Black
        Me.lblTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotal.Border.RightColor = System.Drawing.Color.Black
        Me.lblTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotal.Border.TopColor = System.Drawing.Color.Black
        Me.lblTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotal.Height = 0.15!
        Me.lblTotal.HyperLink = Nothing
        Me.lblTotal.Left = 0.125!
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Style = "font-weight: bold; font-size: 8.25pt; "
        Me.lblTotal.Text = "lblTotal"
        Me.lblTotal.Top = 0.0!
        Me.lblTotal.Width = 1.313!
        '
        'txtTotalCount
        '
        Me.txtTotalCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalCount.CanShrink = True
        Me.txtTotalCount.DataField = "TotalCount"
        Me.txtTotalCount.Height = 0.15!
        Me.txtTotalCount.Left = 3.8125!
        Me.txtTotalCount.Name = "txtTotalCount"
        Me.txtTotalCount.OutputFormat = resources.GetString("txtTotalCount.OutputFormat")
        Me.txtTotalCount.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtTotalCount.Text = "txtTotalCount"
        Me.txtTotalCount.Top = 0.0!
        Me.txtTotalCount.Width = 1.0!
        '
        'lblOCR
        '
        Me.lblOCR.Border.BottomColor = System.Drawing.Color.Black
        Me.lblOCR.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblOCR.Border.LeftColor = System.Drawing.Color.Black
        Me.lblOCR.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblOCR.Border.RightColor = System.Drawing.Color.Black
        Me.lblOCR.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblOCR.Border.TopColor = System.Drawing.Color.Black
        Me.lblOCR.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblOCR.Height = 0.15!
        Me.lblOCR.HyperLink = Nothing
        Me.lblOCR.Left = 0.4375!
        Me.lblOCR.Name = "lblOCR"
        Me.lblOCR.Style = "font-size: 8.25pt; "
        Me.lblOCR.Text = "lblOCR"
        Me.lblOCR.Top = 0.3125!
        Me.lblOCR.Width = 2.0!
        '
        'lblAutogiro
        '
        Me.lblAutogiro.Border.BottomColor = System.Drawing.Color.Black
        Me.lblAutogiro.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAutogiro.Border.LeftColor = System.Drawing.Color.Black
        Me.lblAutogiro.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAutogiro.Border.RightColor = System.Drawing.Color.Black
        Me.lblAutogiro.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAutogiro.Border.TopColor = System.Drawing.Color.Black
        Me.lblAutogiro.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAutogiro.Height = 0.15!
        Me.lblAutogiro.HyperLink = Nothing
        Me.lblAutogiro.Left = 0.438!
        Me.lblAutogiro.Name = "lblAutogiro"
        Me.lblAutogiro.Style = "font-size: 8.25pt; "
        Me.lblAutogiro.Text = "lblAutogiro"
        Me.lblAutogiro.Top = 0.463!
        Me.lblAutogiro.Width = 2.0!
        '
        'lblOther
        '
        Me.lblOther.Border.BottomColor = System.Drawing.Color.Black
        Me.lblOther.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblOther.Border.LeftColor = System.Drawing.Color.Black
        Me.lblOther.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblOther.Border.RightColor = System.Drawing.Color.Black
        Me.lblOther.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblOther.Border.TopColor = System.Drawing.Color.Black
        Me.lblOther.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblOther.Height = 0.15!
        Me.lblOther.HyperLink = Nothing
        Me.lblOther.Left = 0.438!
        Me.lblOther.Name = "lblOther"
        Me.lblOther.Style = "font-size: 8.25pt; "
        Me.lblOther.Text = "lblOther"
        Me.lblOther.Top = 0.613!
        Me.lblOther.Width = 2.0!
        '
        'txtTotalPercent
        '
        Me.txtTotalPercent.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalPercent.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalPercent.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalPercent.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalPercent.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalPercent.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalPercent.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalPercent.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalPercent.CanShrink = True
        Me.txtTotalPercent.Height = 0.15!
        Me.txtTotalPercent.Left = 4.8125!
        Me.txtTotalPercent.Name = "txtTotalPercent"
        Me.txtTotalPercent.OutputFormat = resources.GetString("txtTotalPercent.OutputFormat")
        Me.txtTotalPercent.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtTotalPercent.Text = "txtTotalPercent"
        Me.txtTotalPercent.Top = 0.0!
        Me.txtTotalPercent.Width = 0.7!
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalAmount.CanGrow = False
        Me.txtTotalAmount.CanShrink = True
        Me.txtTotalAmount.DataField = "TotalAmount"
        Me.txtTotalAmount.Height = 0.15!
        Me.txtTotalAmount.Left = 5.75!
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat")
        Me.txtTotalAmount.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtTotalAmount.Text = "txtTotalAmount"
        Me.txtTotalAmount.Top = 0.0!
        Me.txtTotalAmount.Width = 1.0!
        '
        'txtOCRCount
        '
        Me.txtOCRCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtOCRCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtOCRCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtOCRCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtOCRCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRCount.CanShrink = True
        Me.txtOCRCount.DataField = "OCRCount"
        Me.txtOCRCount.Height = 0.15!
        Me.txtOCRCount.Left = 3.8125!
        Me.txtOCRCount.Name = "txtOCRCount"
        Me.txtOCRCount.OutputFormat = resources.GetString("txtOCRCount.OutputFormat")
        Me.txtOCRCount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtOCRCount.Text = "txtOCRCount"
        Me.txtOCRCount.Top = 0.3125!
        Me.txtOCRCount.Width = 1.0!
        '
        'txtAGCount
        '
        Me.txtAGCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAGCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAGCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAGCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAGCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtAGCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAGCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtAGCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAGCount.CanShrink = True
        Me.txtAGCount.DataField = "AGCount"
        Me.txtAGCount.Height = 0.15!
        Me.txtAGCount.Left = 3.813!
        Me.txtAGCount.Name = "txtAGCount"
        Me.txtAGCount.OutputFormat = resources.GetString("txtAGCount.OutputFormat")
        Me.txtAGCount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtAGCount.Text = "txtAGCount"
        Me.txtAGCount.Top = 0.463!
        Me.txtAGCount.Width = 1.0!
        '
        'txtOtherCount
        '
        Me.txtOtherCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtOtherCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOtherCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtOtherCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOtherCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtOtherCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOtherCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtOtherCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOtherCount.CanShrink = True
        Me.txtOtherCount.DataField = "OtherCount"
        Me.txtOtherCount.Height = 0.15!
        Me.txtOtherCount.Left = 3.813!
        Me.txtOtherCount.Name = "txtOtherCount"
        Me.txtOtherCount.OutputFormat = resources.GetString("txtOtherCount.OutputFormat")
        Me.txtOtherCount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtOtherCount.Text = "txtOtherCount"
        Me.txtOtherCount.Top = 0.613!
        Me.txtOtherCount.Width = 1.0!
        '
        'txtOCRPercent
        '
        Me.txtOCRPercent.Border.BottomColor = System.Drawing.Color.Black
        Me.txtOCRPercent.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRPercent.Border.LeftColor = System.Drawing.Color.Black
        Me.txtOCRPercent.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRPercent.Border.RightColor = System.Drawing.Color.Black
        Me.txtOCRPercent.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRPercent.Border.TopColor = System.Drawing.Color.Black
        Me.txtOCRPercent.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRPercent.CanShrink = True
        Me.txtOCRPercent.DataField = "= (OCRCount / TotalCount) * 100"
        Me.txtOCRPercent.Height = 0.15!
        Me.txtOCRPercent.Left = 4.8125!
        Me.txtOCRPercent.Name = "txtOCRPercent"
        Me.txtOCRPercent.OutputFormat = resources.GetString("txtOCRPercent.OutputFormat")
        Me.txtOCRPercent.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtOCRPercent.Text = "txtOCRPercent"
        Me.txtOCRPercent.Top = 0.3125!
        Me.txtOCRPercent.Width = 0.7!
        '
        'txtAGPercent
        '
        Me.txtAGPercent.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAGPercent.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAGPercent.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAGPercent.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAGPercent.Border.RightColor = System.Drawing.Color.Black
        Me.txtAGPercent.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAGPercent.Border.TopColor = System.Drawing.Color.Black
        Me.txtAGPercent.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAGPercent.CanShrink = True
        Me.txtAGPercent.DataField = "= (AGCount / TotalCount) * 100"
        Me.txtAGPercent.Height = 0.15!
        Me.txtAGPercent.Left = 4.813!
        Me.txtAGPercent.Name = "txtAGPercent"
        Me.txtAGPercent.OutputFormat = resources.GetString("txtAGPercent.OutputFormat")
        Me.txtAGPercent.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtAGPercent.Text = "txtAGPercent"
        Me.txtAGPercent.Top = 0.463!
        Me.txtAGPercent.Width = 0.7!
        '
        'txtOtherPercent
        '
        Me.txtOtherPercent.Border.BottomColor = System.Drawing.Color.Black
        Me.txtOtherPercent.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOtherPercent.Border.LeftColor = System.Drawing.Color.Black
        Me.txtOtherPercent.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOtherPercent.Border.RightColor = System.Drawing.Color.Black
        Me.txtOtherPercent.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOtherPercent.Border.TopColor = System.Drawing.Color.Black
        Me.txtOtherPercent.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOtherPercent.CanShrink = True
        Me.txtOtherPercent.DataField = "= (OtherCount / TotalCount) * 100"
        Me.txtOtherPercent.Height = 0.15!
        Me.txtOtherPercent.Left = 4.813!
        Me.txtOtherPercent.Name = "txtOtherPercent"
        Me.txtOtherPercent.OutputFormat = resources.GetString("txtOtherPercent.OutputFormat")
        Me.txtOtherPercent.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtOtherPercent.Text = "txtOtherPercent"
        Me.txtOtherPercent.Top = 0.613!
        Me.txtOtherPercent.Width = 0.7!
        '
        'txtOCRAmount
        '
        Me.txtOCRAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtOCRAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtOCRAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtOCRAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtOCRAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOCRAmount.CanGrow = False
        Me.txtOCRAmount.CanShrink = True
        Me.txtOCRAmount.DataField = "OCRAmount"
        Me.txtOCRAmount.Height = 0.15!
        Me.txtOCRAmount.Left = 5.75!
        Me.txtOCRAmount.Name = "txtOCRAmount"
        Me.txtOCRAmount.OutputFormat = resources.GetString("txtOCRAmount.OutputFormat")
        Me.txtOCRAmount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtOCRAmount.Text = "txtOCRAmount"
        Me.txtOCRAmount.Top = 0.3125!
        Me.txtOCRAmount.Width = 1.0!
        '
        'txtAGAmount
        '
        Me.txtAGAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAGAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAGAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAGAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAGAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtAGAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAGAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtAGAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAGAmount.CanGrow = False
        Me.txtAGAmount.CanShrink = True
        Me.txtAGAmount.DataField = "AGAmount"
        Me.txtAGAmount.Height = 0.15!
        Me.txtAGAmount.Left = 5.75!
        Me.txtAGAmount.Name = "txtAGAmount"
        Me.txtAGAmount.OutputFormat = resources.GetString("txtAGAmount.OutputFormat")
        Me.txtAGAmount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtAGAmount.Text = "txtAGAmount"
        Me.txtAGAmount.Top = 0.463!
        Me.txtAGAmount.Width = 1.0!
        '
        'txtOtherAmount
        '
        Me.txtOtherAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtOtherAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOtherAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtOtherAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOtherAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtOtherAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOtherAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtOtherAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtOtherAmount.CanGrow = False
        Me.txtOtherAmount.CanShrink = True
        Me.txtOtherAmount.DataField = "OtherAmount"
        Me.txtOtherAmount.Height = 0.15!
        Me.txtOtherAmount.Left = 5.75!
        Me.txtOtherAmount.Name = "txtOtherAmount"
        Me.txtOtherAmount.OutputFormat = resources.GetString("txtOtherAmount.OutputFormat")
        Me.txtOtherAmount.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtOtherAmount.Text = "txtOtherAmount"
        Me.txtOtherAmount.Top = 0.613!
        Me.txtOtherAmount.Width = 1.0!
        '
        'lblAuto
        '
        Me.lblAuto.Border.BottomColor = System.Drawing.Color.Black
        Me.lblAuto.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAuto.Border.LeftColor = System.Drawing.Color.Black
        Me.lblAuto.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAuto.Border.RightColor = System.Drawing.Color.Black
        Me.lblAuto.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAuto.Border.TopColor = System.Drawing.Color.Black
        Me.lblAuto.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAuto.Height = 0.15!
        Me.lblAuto.HyperLink = Nothing
        Me.lblAuto.Left = 0.6875!
        Me.lblAuto.Name = "lblAuto"
        Me.lblAuto.Style = "font-weight: bold; font-size: 8.25pt; "
        Me.lblAuto.Text = "lblAuto"
        Me.lblAuto.Top = 1.0625!
        Me.lblAuto.Width = 3.125!
        '
        'txtAutoCount
        '
        Me.txtAutoCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAutoCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAutoCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtAutoCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtAutoCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoCount.CanShrink = True
        Me.txtAutoCount.DataField = "AutoCount"
        Me.txtAutoCount.Height = 0.15!
        Me.txtAutoCount.Left = 3.8125!
        Me.txtAutoCount.Name = "txtAutoCount"
        Me.txtAutoCount.OutputFormat = resources.GetString("txtAutoCount.OutputFormat")
        Me.txtAutoCount.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtAutoCount.Text = "txtAutoCount"
        Me.txtAutoCount.Top = 1.0625!
        Me.txtAutoCount.Width = 1.0!
        '
        'txtAutoPercent
        '
        Me.txtAutoPercent.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAutoPercent.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoPercent.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAutoPercent.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoPercent.Border.RightColor = System.Drawing.Color.Black
        Me.txtAutoPercent.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoPercent.Border.TopColor = System.Drawing.Color.Black
        Me.txtAutoPercent.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoPercent.CanShrink = True
        Me.txtAutoPercent.DataField = "= (AutoCount / OtherCount) * 100 "
        Me.txtAutoPercent.Height = 0.15!
        Me.txtAutoPercent.Left = 4.8125!
        Me.txtAutoPercent.Name = "txtAutoPercent"
        Me.txtAutoPercent.OutputFormat = resources.GetString("txtAutoPercent.OutputFormat")
        Me.txtAutoPercent.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtAutoPercent.Text = "txtAutoPercent"
        Me.txtAutoPercent.Top = 1.0625!
        Me.txtAutoPercent.Width = 0.7!
        '
        'txtAutoAmount
        '
        Me.txtAutoAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAutoAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAutoAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtAutoAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtAutoAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoAmount.CanShrink = True
        Me.txtAutoAmount.DataField = "AutoAmount"
        Me.txtAutoAmount.Height = 0.15!
        Me.txtAutoAmount.Left = 5.75!
        Me.txtAutoAmount.Name = "txtAutoAmount"
        Me.txtAutoAmount.OutputFormat = resources.GetString("txtAutoAmount.OutputFormat")
        Me.txtAutoAmount.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtAutoAmount.Text = "txtAutoAmount"
        Me.txtAutoAmount.Top = 1.0625!
        Me.txtAutoAmount.Width = 1.0!
        '
        'lblProposed
        '
        Me.lblProposed.Border.BottomColor = System.Drawing.Color.Black
        Me.lblProposed.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblProposed.Border.LeftColor = System.Drawing.Color.Black
        Me.lblProposed.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblProposed.Border.RightColor = System.Drawing.Color.Black
        Me.lblProposed.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblProposed.Border.TopColor = System.Drawing.Color.Black
        Me.lblProposed.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblProposed.Height = 0.15!
        Me.lblProposed.HyperLink = Nothing
        Me.lblProposed.Left = 7.25!
        Me.lblProposed.Name = "lblProposed"
        Me.lblProposed.Style = "text-align: center; font-size: 8.25pt; "
        Me.lblProposed.Text = "lblProposed"
        Me.lblProposed.Top = 0.75!
        Me.lblProposed.Width = 0.7!
        '
        'lblFinal
        '
        Me.lblFinal.Border.BottomColor = System.Drawing.Color.Black
        Me.lblFinal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblFinal.Border.LeftColor = System.Drawing.Color.Black
        Me.lblFinal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblFinal.Border.RightColor = System.Drawing.Color.Black
        Me.lblFinal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblFinal.Border.TopColor = System.Drawing.Color.Black
        Me.lblFinal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblFinal.Height = 0.15!
        Me.lblFinal.HyperLink = Nothing
        Me.lblFinal.Left = 7.9375!
        Me.lblFinal.Name = "lblFinal"
        Me.lblFinal.Style = "text-align: center; font-size: 8.25pt; "
        Me.lblFinal.Text = "lblFinal"
        Me.lblFinal.Top = 0.75!
        Me.lblFinal.Width = 0.7!
        '
        'txtAutoProposed
        '
        Me.txtAutoProposed.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAutoProposed.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoProposed.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAutoProposed.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoProposed.Border.RightColor = System.Drawing.Color.Black
        Me.txtAutoProposed.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoProposed.Border.TopColor = System.Drawing.Color.Black
        Me.txtAutoProposed.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoProposed.CanShrink = True
        Me.txtAutoProposed.DataField = "AutoProposed"
        Me.txtAutoProposed.Height = 0.15!
        Me.txtAutoProposed.Left = 7.0625!
        Me.txtAutoProposed.Name = "txtAutoProposed"
        Me.txtAutoProposed.OutputFormat = resources.GetString("txtAutoProposed.OutputFormat")
        Me.txtAutoProposed.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtAutoProposed.Text = "txtAutoProposed"
        Me.txtAutoProposed.Top = 1.0625!
        Me.txtAutoProposed.Width = 0.7!
        '
        'txtAutoFinal
        '
        Me.txtAutoFinal.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAutoFinal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoFinal.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAutoFinal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoFinal.Border.RightColor = System.Drawing.Color.Black
        Me.txtAutoFinal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoFinal.Border.TopColor = System.Drawing.Color.Black
        Me.txtAutoFinal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoFinal.CanShrink = True
        Me.txtAutoFinal.DataField = "AutoFinal"
        Me.txtAutoFinal.Height = 0.15!
        Me.txtAutoFinal.Left = 7.75!
        Me.txtAutoFinal.Name = "txtAutoFinal"
        Me.txtAutoFinal.OutputFormat = resources.GetString("txtAutoFinal.OutputFormat")
        Me.txtAutoFinal.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtAutoFinal.Text = "txtAutoFinal"
        Me.txtAutoFinal.Top = 1.0625!
        Me.txtAutoFinal.Width = 0.7!
        '
        'txtAutoPercentInv
        '
        Me.txtAutoPercentInv.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAutoPercentInv.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoPercentInv.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAutoPercentInv.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoPercentInv.Border.RightColor = System.Drawing.Color.Black
        Me.txtAutoPercentInv.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoPercentInv.Border.TopColor = System.Drawing.Color.Black
        Me.txtAutoPercentInv.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAutoPercentInv.CanShrink = True
        Me.txtAutoPercentInv.DataField = "= ((AutoProposed + AutoFinal) / (AutoProposed + AutoFinal + UnPostedAutoFinal)) *" & _
            " 100"
        Me.txtAutoPercentInv.Height = 0.15!
        Me.txtAutoPercentInv.Left = 8.4375!
        Me.txtAutoPercentInv.Name = "txtAutoPercentInv"
        Me.txtAutoPercentInv.OutputFormat = resources.GetString("txtAutoPercentInv.OutputFormat")
        Me.txtAutoPercentInv.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtAutoPercentInv.Text = "txtAutoPercentInv"
        Me.txtAutoPercentInv.Top = 1.0625!
        Me.txtAutoPercentInv.Width = 0.7!
        '
        'grAccumulatedFooter
        '
        Me.grAccumulatedFooter.CanShrink = True
        Me.grAccumulatedFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapegrAccumulated2Footer, Me.shapegrAccumulatedFooter, Me.lblUnPostedAuto, Me.txtUnPostedAutoCount, Me.txtUnPostedAutoPercent, Me.txtUnPostedAutoAmount, Me.txtUnPostedAutoFinal, Me.txtUnPostedAutoPercentInv, Me.lblManual, Me.txtManualCount, Me.txtManualFinal, Me.lblUnMatched, Me.txtUnMatchedCount, Me.txtUnMatchedAmount, Me.txtUnMatchedFinal, Me.subManualMatched})
        Me.grAccumulatedFooter.Height = 1.385417!
        Me.grAccumulatedFooter.Name = "grAccumulatedFooter"
        '
        'shapegrAccumulatedFooter
        '
        Me.shapegrAccumulatedFooter.BackColor = System.Drawing.Color.LightGray
        Me.shapegrAccumulatedFooter.Border.BottomColor = System.Drawing.Color.Black
        Me.shapegrAccumulatedFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulatedFooter.Border.LeftColor = System.Drawing.Color.Black
        Me.shapegrAccumulatedFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulatedFooter.Border.RightColor = System.Drawing.Color.Black
        Me.shapegrAccumulatedFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulatedFooter.Border.TopColor = System.Drawing.Color.Black
        Me.shapegrAccumulatedFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulatedFooter.Height = 0.913!
        Me.shapegrAccumulatedFooter.Left = 7.0!
        Me.shapegrAccumulatedFooter.LineStyle = DataDynamics.ActiveReports.LineStyle.Transparent
        Me.shapegrAccumulatedFooter.Name = "shapegrAccumulatedFooter"
        Me.shapegrAccumulatedFooter.RoundingRadius = 9.999999!
        Me.shapegrAccumulatedFooter.Top = -0.08!
        Me.shapegrAccumulatedFooter.Width = 2.5!
        '
        'lblUnPostedAuto
        '
        Me.lblUnPostedAuto.Border.BottomColor = System.Drawing.Color.Black
        Me.lblUnPostedAuto.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblUnPostedAuto.Border.LeftColor = System.Drawing.Color.Black
        Me.lblUnPostedAuto.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblUnPostedAuto.Border.RightColor = System.Drawing.Color.Black
        Me.lblUnPostedAuto.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblUnPostedAuto.Border.TopColor = System.Drawing.Color.Black
        Me.lblUnPostedAuto.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblUnPostedAuto.Height = 0.15!
        Me.lblUnPostedAuto.HyperLink = Nothing
        Me.lblUnPostedAuto.Left = 1.1875!
        Me.lblUnPostedAuto.Name = "lblUnPostedAuto"
        Me.lblUnPostedAuto.Style = "font-weight: bold; font-size: 8.25pt; "
        Me.lblUnPostedAuto.Text = "lblUnPostedAuto"
        Me.lblUnPostedAuto.Top = 0.125!
        Me.lblUnPostedAuto.Width = 2.5!
        '
        'txtUnPostedAutoCount
        '
        Me.txtUnPostedAutoCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoCount.CanShrink = True
        Me.txtUnPostedAutoCount.DataField = "UnPostedAutoCount"
        Me.txtUnPostedAutoCount.Height = 0.15!
        Me.txtUnPostedAutoCount.Left = 3.813!
        Me.txtUnPostedAutoCount.Name = "txtUnPostedAutoCount"
        Me.txtUnPostedAutoCount.OutputFormat = resources.GetString("txtUnPostedAutoCount.OutputFormat")
        Me.txtUnPostedAutoCount.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtUnPostedAutoCount.Text = "txtUnPostedAutoCount"
        Me.txtUnPostedAutoCount.Top = 0.125!
        Me.txtUnPostedAutoCount.Width = 1.0!
        '
        'txtUnPostedAutoPercent
        '
        Me.txtUnPostedAutoPercent.Border.BottomColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoPercent.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoPercent.Border.LeftColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoPercent.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoPercent.Border.RightColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoPercent.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoPercent.Border.TopColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoPercent.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoPercent.CanShrink = True
        Me.txtUnPostedAutoPercent.DataField = "= (UnPostedAutoCount / OtherCount) * 100 "
        Me.txtUnPostedAutoPercent.Height = 0.15!
        Me.txtUnPostedAutoPercent.Left = 4.813!
        Me.txtUnPostedAutoPercent.Name = "txtUnPostedAutoPercent"
        Me.txtUnPostedAutoPercent.OutputFormat = resources.GetString("txtUnPostedAutoPercent.OutputFormat")
        Me.txtUnPostedAutoPercent.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtUnPostedAutoPercent.Text = "txtUnPostedAutoPercent"
        Me.txtUnPostedAutoPercent.Top = 0.125!
        Me.txtUnPostedAutoPercent.Width = 0.7!
        '
        'txtUnPostedAutoAmount
        '
        Me.txtUnPostedAutoAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoAmount.CanShrink = True
        Me.txtUnPostedAutoAmount.DataField = "UnPostedAutoAmount"
        Me.txtUnPostedAutoAmount.Height = 0.15!
        Me.txtUnPostedAutoAmount.Left = 5.75!
        Me.txtUnPostedAutoAmount.Name = "txtUnPostedAutoAmount"
        Me.txtUnPostedAutoAmount.OutputFormat = resources.GetString("txtUnPostedAutoAmount.OutputFormat")
        Me.txtUnPostedAutoAmount.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtUnPostedAutoAmount.Text = "txtUnPostedAutoAmount"
        Me.txtUnPostedAutoAmount.Top = 0.125!
        Me.txtUnPostedAutoAmount.Width = 1.0!
        '
        'txtUnPostedAutoFinal
        '
        Me.txtUnPostedAutoFinal.Border.BottomColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoFinal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoFinal.Border.LeftColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoFinal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoFinal.Border.RightColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoFinal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoFinal.Border.TopColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoFinal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoFinal.CanShrink = True
        Me.txtUnPostedAutoFinal.DataField = "UnPostedAutoFinal"
        Me.txtUnPostedAutoFinal.Height = 0.15!
        Me.txtUnPostedAutoFinal.Left = 7.75!
        Me.txtUnPostedAutoFinal.Name = "txtUnPostedAutoFinal"
        Me.txtUnPostedAutoFinal.OutputFormat = resources.GetString("txtUnPostedAutoFinal.OutputFormat")
        Me.txtUnPostedAutoFinal.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtUnPostedAutoFinal.Text = "txtUnPostedAutoFinal"
        Me.txtUnPostedAutoFinal.Top = 0.125!
        Me.txtUnPostedAutoFinal.Width = 0.7!
        '
        'txtUnPostedAutoPercentInv
        '
        Me.txtUnPostedAutoPercentInv.Border.BottomColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoPercentInv.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoPercentInv.Border.LeftColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoPercentInv.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoPercentInv.Border.RightColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoPercentInv.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoPercentInv.Border.TopColor = System.Drawing.Color.Black
        Me.txtUnPostedAutoPercentInv.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnPostedAutoPercentInv.CanShrink = True
        Me.txtUnPostedAutoPercentInv.DataField = "= ((UnPostedAutoFinal) / (AutoProposed + AutoFinal + UnPostedAutoFinal)) * 100"
        Me.txtUnPostedAutoPercentInv.Height = 0.15!
        Me.txtUnPostedAutoPercentInv.Left = 8.4375!
        Me.txtUnPostedAutoPercentInv.Name = "txtUnPostedAutoPercentInv"
        Me.txtUnPostedAutoPercentInv.OutputFormat = resources.GetString("txtUnPostedAutoPercentInv.OutputFormat")
        Me.txtUnPostedAutoPercentInv.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtUnPostedAutoPercentInv.Text = "txtUnPostedAutoPercentInv"
        Me.txtUnPostedAutoPercentInv.Top = 0.125!
        Me.txtUnPostedAutoPercentInv.Width = 0.7!
        '
        'lblManual
        '
        Me.lblManual.Border.BottomColor = System.Drawing.Color.Black
        Me.lblManual.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblManual.Border.LeftColor = System.Drawing.Color.Black
        Me.lblManual.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblManual.Border.RightColor = System.Drawing.Color.Black
        Me.lblManual.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblManual.Border.TopColor = System.Drawing.Color.Black
        Me.lblManual.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblManual.Height = 0.15!
        Me.lblManual.HyperLink = Nothing
        Me.lblManual.Left = 0.8125!
        Me.lblManual.Name = "lblManual"
        Me.lblManual.Style = "font-weight: bold; font-size: 8.25pt; "
        Me.lblManual.Text = "lblManual"
        Me.lblManual.Top = 0.5625!
        Me.lblManual.Width = 1.5!
        '
        'txtManualCount
        '
        Me.txtManualCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtManualCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtManualCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtManualCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtManualCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtManualCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtManualCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtManualCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtManualCount.CanShrink = True
        Me.txtManualCount.DataField = "ManualCount"
        Me.txtManualCount.Height = 0.15!
        Me.txtManualCount.Left = 3.813!
        Me.txtManualCount.Name = "txtManualCount"
        Me.txtManualCount.OutputFormat = resources.GetString("txtManualCount.OutputFormat")
        Me.txtManualCount.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtManualCount.Text = "txtManualCount"
        Me.txtManualCount.Top = 0.563!
        Me.txtManualCount.Width = 1.0!
        '
        'txtManualFinal
        '
        Me.txtManualFinal.Border.BottomColor = System.Drawing.Color.Black
        Me.txtManualFinal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtManualFinal.Border.LeftColor = System.Drawing.Color.Black
        Me.txtManualFinal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtManualFinal.Border.RightColor = System.Drawing.Color.Black
        Me.txtManualFinal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtManualFinal.Border.TopColor = System.Drawing.Color.Black
        Me.txtManualFinal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtManualFinal.CanShrink = True
        Me.txtManualFinal.DataField = "ManualFinal"
        Me.txtManualFinal.Height = 0.15!
        Me.txtManualFinal.Left = 7.75!
        Me.txtManualFinal.Name = "txtManualFinal"
        Me.txtManualFinal.OutputFormat = resources.GetString("txtManualFinal.OutputFormat")
        Me.txtManualFinal.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtManualFinal.Text = "txtManualFinal"
        Me.txtManualFinal.Top = 0.5625!
        Me.txtManualFinal.Width = 0.7!
        '
        'lblUnMatched
        '
        Me.lblUnMatched.Border.BottomColor = System.Drawing.Color.Black
        Me.lblUnMatched.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblUnMatched.Border.LeftColor = System.Drawing.Color.Black
        Me.lblUnMatched.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblUnMatched.Border.RightColor = System.Drawing.Color.Black
        Me.lblUnMatched.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblUnMatched.Border.TopColor = System.Drawing.Color.Black
        Me.lblUnMatched.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblUnMatched.Height = 0.15!
        Me.lblUnMatched.HyperLink = Nothing
        Me.lblUnMatched.Left = 0.8125!
        Me.lblUnMatched.Name = "lblUnMatched"
        Me.lblUnMatched.Style = "font-weight: bold; font-size: 8.25pt; "
        Me.lblUnMatched.Text = "lblUnMatched"
        Me.lblUnMatched.Top = 1.125!
        Me.lblUnMatched.Width = 1.5!
        '
        'txtUnMatchedCount
        '
        Me.txtUnMatchedCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtUnMatchedCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnMatchedCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtUnMatchedCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnMatchedCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtUnMatchedCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnMatchedCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtUnMatchedCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnMatchedCount.CanShrink = True
        Me.txtUnMatchedCount.DataField = "UnMatchededCount"
        Me.txtUnMatchedCount.Height = 0.15!
        Me.txtUnMatchedCount.Left = 3.813!
        Me.txtUnMatchedCount.Name = "txtUnMatchedCount"
        Me.txtUnMatchedCount.OutputFormat = resources.GetString("txtUnMatchedCount.OutputFormat")
        Me.txtUnMatchedCount.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtUnMatchedCount.Text = "txtUnMatchedCount"
        Me.txtUnMatchedCount.Top = 1.125!
        Me.txtUnMatchedCount.Width = 1.0!
        '
        'txtUnMatchedAmount
        '
        Me.txtUnMatchedAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtUnMatchedAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnMatchedAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtUnMatchedAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnMatchedAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtUnMatchedAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnMatchedAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtUnMatchedAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnMatchedAmount.CanShrink = True
        Me.txtUnMatchedAmount.DataField = "UnMatchedAmount"
        Me.txtUnMatchedAmount.Height = 0.15!
        Me.txtUnMatchedAmount.Left = 5.75!
        Me.txtUnMatchedAmount.Name = "txtUnMatchedAmount"
        Me.txtUnMatchedAmount.OutputFormat = resources.GetString("txtUnMatchedAmount.OutputFormat")
        Me.txtUnMatchedAmount.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; vertical-align: top; "
        Me.txtUnMatchedAmount.Text = "txtUnMatchedAmount"
        Me.txtUnMatchedAmount.Top = 1.125!
        Me.txtUnMatchedAmount.Width = 1.0!
        '
        'txtUnMatchedFinal
        '
        Me.txtUnMatchedFinal.Border.BottomColor = System.Drawing.Color.Black
        Me.txtUnMatchedFinal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnMatchedFinal.Border.LeftColor = System.Drawing.Color.Black
        Me.txtUnMatchedFinal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnMatchedFinal.Border.RightColor = System.Drawing.Color.Black
        Me.txtUnMatchedFinal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnMatchedFinal.Border.TopColor = System.Drawing.Color.Black
        Me.txtUnMatchedFinal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUnMatchedFinal.CanShrink = True
        Me.txtUnMatchedFinal.DataField = "UnMatchedFinal"
        Me.txtUnMatchedFinal.Height = 0.15!
        Me.txtUnMatchedFinal.Left = 7.75!
        Me.txtUnMatchedFinal.Name = "txtUnMatchedFinal"
        Me.txtUnMatchedFinal.OutputFormat = resources.GetString("txtUnMatchedFinal.OutputFormat")
        Me.txtUnMatchedFinal.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtUnMatchedFinal.Text = "txtUnMatchedFinal"
        Me.txtUnMatchedFinal.Top = 1.125!
        Me.txtUnMatchedFinal.Width = 0.7!
        '
        'subManualMatched
        '
        Me.subManualMatched.Border.BottomColor = System.Drawing.Color.Black
        Me.subManualMatched.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.subManualMatched.Border.LeftColor = System.Drawing.Color.Black
        Me.subManualMatched.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.subManualMatched.Border.RightColor = System.Drawing.Color.Black
        Me.subManualMatched.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.subManualMatched.Border.TopColor = System.Drawing.Color.Black
        Me.subManualMatched.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.subManualMatched.CloseBorder = False
        Me.subManualMatched.Height = 0.188!
        Me.subManualMatched.Left = 0.0!
        Me.subManualMatched.Name = "subManualMatched"
        Me.subManualMatched.Report = Nothing
        Me.subManualMatched.ReportName = "rp_Sub_952_AutoMatch"
        Me.subManualMatched.Top = 0.8125!
        Me.subManualMatched.Width = 9.813!
        '
        'TotalSum
        '
        Me.TotalSum.DefaultValue = "100"
        Me.TotalSum.FieldType = DataDynamics.ActiveReports.FieldTypeEnum.None
        Me.TotalSum.Formula = "3+3"
        Me.TotalSum.Name = "TotalSum"
        Me.TotalSum.Tag = Nothing
        '
        'shapegrAccumulated2Footer
        '
        Me.shapegrAccumulated2Footer.BackColor = System.Drawing.Color.LightGray
        Me.shapegrAccumulated2Footer.Border.BottomColor = System.Drawing.Color.Black
        Me.shapegrAccumulated2Footer.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulated2Footer.Border.LeftColor = System.Drawing.Color.Black
        Me.shapegrAccumulated2Footer.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulated2Footer.Border.RightColor = System.Drawing.Color.Black
        Me.shapegrAccumulated2Footer.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulated2Footer.Border.TopColor = System.Drawing.Color.Black
        Me.shapegrAccumulated2Footer.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulated2Footer.Height = 0.35!
        Me.shapegrAccumulated2Footer.Left = 7.0!
        Me.shapegrAccumulated2Footer.LineStyle = DataDynamics.ActiveReports.LineStyle.Transparent
        Me.shapegrAccumulated2Footer.Name = "shapegrAccumulated2Footer"
        Me.shapegrAccumulated2Footer.RoundingRadius = 9.999999!
        Me.shapegrAccumulated2Footer.Top = 1.01!
        Me.shapegrAccumulated2Footer.Width = 2.5!
        '
        'rp_952_Statistics
        '
        Me.MasterReport = False
        Me.CalculatedFields.Add(Me.TotalSum)
        Me.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Landscape
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 9.854167!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.grMainHeader)
        Me.Sections.Add(Me.grAccumulatedHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grAccumulatedFooter)
        Me.Sections.Add(Me.grMainFooter)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'Times New Roman'; font-style: italic; font-variant: inherit; font-w" & _
                    "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; " & _
                    "", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("", "Heading4", "Normal"))
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDateInterval, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPercent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblInvoices, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPercentInv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblOCR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblAutogiro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblOther, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalPercent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOCRCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAGCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOtherCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOCRPercent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAGPercent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOtherPercent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOCRAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAGAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOtherAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblAuto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAutoCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAutoPercent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAutoAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblProposed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblFinal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAutoProposed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAutoFinal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAutoPercentInv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblUnPostedAuto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnPostedAutoCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnPostedAutoPercent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnPostedAutoAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnPostedAutoFinal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnPostedAutoPercentInv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblManual, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtManualCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtManualFinal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblUnMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnMatchedCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnMatchedAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnMatchedFinal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader
    Friend WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Friend WithEvents Line3 As DataDynamics.ActiveReports.Line
    Friend WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter
    Friend WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents grMainHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents grMainFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents grAccumulatedHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents lblTotal As DataDynamics.ActiveReports.Label
    Friend WithEvents grAccumulatedFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents TotalSum As DataDynamics.ActiveReports.Field
    Friend WithEvents lblDateInterval As DataDynamics.ActiveReports.Label
    Friend WithEvents lblPayments As DataDynamics.ActiveReports.Label
    Friend WithEvents lblPercent As DataDynamics.ActiveReports.Label
    Friend WithEvents lblAmount As DataDynamics.ActiveReports.Label
    Friend WithEvents lblInvoices As DataDynamics.ActiveReports.Label
    Friend WithEvents lblPercentInv As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTotalCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblOCR As DataDynamics.ActiveReports.Label
    Friend WithEvents lblAutogiro As DataDynamics.ActiveReports.Label
    Friend WithEvents lblOther As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTotalPercent As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTotalAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtOCRCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtAGCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtOtherCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtOCRPercent As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtAGPercent As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtOtherPercent As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtOCRAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtAGAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtOtherAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblAuto As DataDynamics.ActiveReports.Label
    Friend WithEvents txtAutoCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtAutoPercent As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtAutoAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblProposed As DataDynamics.ActiveReports.Label
    Friend WithEvents lblFinal As DataDynamics.ActiveReports.Label
    Friend WithEvents txtAutoProposed As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtAutoFinal As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtAutoPercentInv As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblUnPostedAuto As DataDynamics.ActiveReports.Label
    Friend WithEvents txtUnPostedAutoCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtUnPostedAutoPercent As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtUnPostedAutoAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtUnPostedAutoFinal As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtUnPostedAutoPercentInv As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblManual As DataDynamics.ActiveReports.Label
    Friend WithEvents txtManualCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtManualFinal As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblUnMatched As DataDynamics.ActiveReports.Label
    Friend WithEvents txtUnMatchedCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtUnMatchedAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtUnMatchedFinal As DataDynamics.ActiveReports.TextBox
    Friend WithEvents shapegrMainHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents shapegrAccumulatedHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents shapegrAccumulatedFooter As DataDynamics.ActiveReports.Shape
    Friend WithEvents subManualMatched As DataDynamics.ActiveReports.SubReport
    Protected Friend WithEvents subAutoMatched As DataDynamics.ActiveReports.SubReport
    Friend WithEvents shapegrAccumulated2Footer As DataDynamics.ActiveReports.Shape
End Class
