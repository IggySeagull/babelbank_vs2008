﻿' XokNET 16.09.2014 - New module
Option Strict Off
Option Explicit On
Module WriteCorporateFilePayments_SE


    Dim nFileSumAmount As Double
    Dim iPayNumber As Integer
    Dim sAmountCurrency As String
    Dim sBetalningsTyp As String
    Dim sLocalErrorCode As String
    Public Function WriteCorporateFilePayments(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal iFilenameInNo As Integer, ByVal sCompanyNo As String, ByVal sAdditionalNo As String, ByVal sClientNo As String) As Boolean

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i As Double, j As Double, k As Double
        Dim bFirstLine As Boolean
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oClient As Client
        Dim oaccount As Account
        Dim oInvoice As Invoice
        Dim oFreetext As FreeText
        Dim iCount As Integer
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bFileStartWritten As Boolean
        Dim sOldAccount As String
        Dim sOld_E_Account As String
        Dim sOldPayType As String
        Dim sOldSupplierNo As String
        Dim sAccountCurrency As String
        Dim bFileFromBank As Boolean

        Dim nPaymentIndex As Double
        Dim bFoundClient As Boolean
        Dim sTxt As String
        Dim sTxt1 As String
        Dim sTxt2 As String
        Dim bInternational As Boolean
        Dim sOldCurrency As String
        ' XNET 21.04.2015
        Dim sOldTransferCurrency As String
        Dim bThereAreCreditNotes As Boolean

        Try

            ' TESTCODE - REMOVE !!!!!!!!!
            ' do some special treatment to be able to test different kind of data
            'For Each oBabel In oBabelFiles
            '    For Each oBatch In oBabel.Batches
            '        For Each oPayment In oBatch.Payments
            '
            '            ' SEK for innlandsbetalinger
            '            'oPayment.MON_InvoiceCurrency = "SEK"
            '            ' test utbetalingsanvisning/check
            '            'oPayment.E_Account = ""
            '
            '            ' Type bankgirot
            '            'oPayment.E_AccountType = SE_Bankgiro
            '            'oPayment.E_Account = "50052976"
            '
            '            'oPayment.E_AccountType = SE_Bankaccount
            '            'oPayment.E_Account = "31447723686"
            '
            '            ' INT
            '            ' Riksbankkod
            '            For Each oInvoice In oPayment.Invoices
            '                oInvoice.STATEBANK_Code = "140"
            '            Next
            '            'oPayment.BANK_Name = "BANKNAME"
            '            'oPayment.BANK_Adr1 = "ADR1"
            '            'oPayment.BANK_Adr2 = "ADR2"
            '            'oPayment.BANK_Adr3 = "ADR3"
            '
            '        Next
            '    Next
            'Next

            ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            sLocalErrorCode = "100"
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            sOldAccount = "---------------------------"
            sOld_E_Account = "----------------------------"
            sOldSupplierNo = "--------------------------------"
            sOldPayType = "-"
            iPayNumber = 0

            For Each oBabel In oBabelFiles
                bExportoBabel = False ' Not needed if no specialchecking is present
                sLocalErrorCode = "110"
                If oBabel.BabelfileContainsPaymentsAccordingToQualifiers(iFilenameInNo, iFormat_ID, bMultiFiles, sClientNo, True, sOwnRef) Then
                    bExportoBabel = True
                End If

                If bExportoBabel Then
                    'Set if the file was created int the accounting system or not.
                    bFileFromBank = oBabel.FileFromBank
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        ' Added next If, because it would bomb further down for empty batches
                        If oBatch.Payments.Count > 0 Then
                            sLocalErrorCode = "120"
                            If oBatch.BatchContainsPaymentsAccordingToQualifiers(iFormat_ID, bMultiFiles, sClientNo, nPaymentIndex, True, sOwnRef) Then
                                bExportoBatch = True
                            End If

                            If bExportoBatch Then
                                For Each oPayment In oBatch.Payments

                                    bExportoPayment = False
                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        ' XokNET 30.01.2014 added next If
                                        If Not oPayment.Exported Then
                                            If Not bMultiFiles Then
                                                bExportoPayment = True
                                            Else
                                                'If oPayment.I_Account = sI_Account Then
                                                ' 01.10.2018 – changed above If to:
                                                If oPayment.I_Account = sI_Account Or EmptyString(sI_Account) Then
                                                    If oPayment.REF_Own = sOwnRef Then
                                                        bExportoPayment = True
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If

                                    If bExportoPayment Then

                                        ' must test for change in debitaccount
                                        ' or change between Domestic and Int
                                        ' and test if change in pay currency
                                        ' ------------------------------------
                                        sLocalErrorCode = "130"
                                        'If oPayment.I_Account <> sOldAccount Or oPayment.PayType <> sOldPayType Or _
                                        'oPayment.MON_InvoiceCurrency <> sOldCurrency Or oPayment.MON_AccountCurrency <> sOldAccountCurrency Then
                                        ' XNET 21.04.2015
                                        If oPayment.I_Account <> sOldAccount Or oPayment.PayType <> sOldPayType Or _
                                        oPayment.MON_InvoiceCurrency <> sOldCurrency Or oPayment.MON_TransferCurrency <> sOldTransferCurrency Then

                                            If oPayment.VB_ProfileInUse Then
                                                For Each oClient In oPayment.VB_Profile.FileSetups(iFormat_ID).Clients
                                                    For Each oaccount In oClient.Accounts
                                                        If oaccount.Account = oPayment.I_Account Or Trim$(oaccount.ConvertedAccount) = oPayment.I_Account Then
                                                            sAccountCurrency = oaccount.CurrencyCode
                                                            bFoundClient = True
                                                            Exit For
                                                        End If
                                                    Next
                                                    If bFoundClient Then
                                                        Exit For
                                                    End If
                                                Next
                                            Else
                                                'If Not EmptyString(oPayment.MON_AccountCurrency) Then
                                                ' XNET 28.04.2015  - 1.88.74
                                                ' sAccountCurrency not in use
                                                If Not EmptyString(oPayment.MON_TransferCurrency) Then
                                                    ' XNET 21.04.2015  - 1.88.73
                                                    sAccountCurrency = oPayment.MON_TransferCurrency
                                                Else
                                                    sAccountCurrency = "SEK"    ' TODO Visma assume SEK-account. What to do if EUR-account ?
                                                End If
                                            End If
                                            sOldCurrency = oPayment.MON_InvoiceCurrency
                                            sOldTransferCurrency = oPayment.MON_TransferCurrency


                                            ' If changes in account, paytype or currency, we need to add new headerrecord,
                                            ' Innledningspost (MH00 or MH01)

                                            ' But must first test if we have written any records before, and the add a Avslutningspost
                                            If iPayNumber > 0 Then
                                                If oPayment.StatusCode = "00" Then   ' 17.12.2019 added statuscode = "00"
                                                    sLine = WriteAvslutningspost_MT00()
                                                    oFile.WriteLine(sLine)
                                                Else
                                                    sLine = WriteAvslutningspost_MT01()
                                                    oFile.WriteLine(sLine)
                                                End If
                                            End If

                                            ' 10.12.2019 - added test for returnfiles 
                                            If IsIBANNumber(oPayment.I_Account, False) Or oPayment.StatusCode = "02" Then
                                                sLine = WriteInnledningspost2_MH01(oBatch, oPayment, sCompanyNo)
                                                oFile.WriteLine(sLine)
                                            Else
                                                sLine = WriteInnledningspost1_MH00(oBatch, oPayment, sCompanyNo)
                                                oFile.WriteLine(sLine)
                                            End If

                                        End If

                                        ' we need to know if there are any creditnotes amongst the invoices, to see if we must use CNDB/CNKR-records
                                        bThereAreCreditNotes = False
                                        For Each oInvoice In oPayment.Invoices
                                            If oInvoice.MON_InvoiceAmount < 0 Then
                                                bThereAreCreditNotes = True
                                                Exit For
                                            End If
                                        Next oInvoice

                                        iPayNumber = iPayNumber + 1 ' Løpenummer pr betaling
                                        If oPayment.PayType = "I" Then
                                            ' ------------------------------
                                            ' International
                                            ' ------------------------------
                                            sLocalErrorCode = "140"

                                            ' 10.12.2019 - test if Returfiles or not
                                            If oPayment.StatusCode = "00" Then
                                                'PI00 Betalningsinstruktion
                                                sLine = WriteIntBetalning_PI00(oPayment)
                                                oFile.WriteLine(sLine)
                                            Else
                                                'DR00 Payment record
                                                sLine = WritePayment_DR00(oPayment)
                                                oFile.WriteLine(sLine)
                                                'DR01 - ExchangeRate record
                                                sLine = WriteExchange_DR01(oPayment)
                                                oFile.WriteLine(sLine)
                                            End If

                                            'BA00 Avsändarens egna noteringar
                                            sLine = WriteDomIntAvsandarnoteringar_BA00(oPayment)
                                            oFile.WriteLine(sLine)

                                            'BE00 Betalningsmottagarens konto
                                            sLine = WriteIntMottagarKonto_BE00(oPayment)
                                            oFile.WriteLine(sLine)

                                            'BE01 Betalningsmottagarens namn och land
                                            sLine = WriteIntMottagarNamn_BE01(oPayment)
                                            oFile.WriteLine(sLine)

                                            'BE03 Betalningsmottagarens gatuadress (obligatorisk vid utbetalning)
                                            sLine = WriteDomIntMottagarGatuadress_BE03(oPayment)
                                            oFile.WriteLine(sLine)

                                            If bThereAreCreditNotes And oPayment.Invoices.Count < 5 And Not EmptyString(oPayment.E_Account) Then  ' max 5 CNKR/CNDB
                                                ' IB-poster angis før eventuelle CNKR/CNDB-poster, MEN ETTER BM99-poster !!!

                                                'IB01 Mottagande banks namn (vid SWIFT betalning)
                                                sLine = WriteIntMottagerBank_IB01(oPayment)
                                                oFile.WriteLine(sLine)

                                                'IB02 Mottagande banks ortsadress
                                                sLine = WriteIntMottagerBankOrt_IB02(oPayment)
                                                oFile.WriteLine(sLine)

                                                'IB03 Mottagande banks adress
                                                sLine = WriteIntMottagerBankAdress_IB03(oPayment)
                                                oFile.WriteLine(sLine)
                                            End If

                                            sOldAccount = oPayment.I_Account
                                            sOldPayType = "I"
                                        Else
                                            ' ------------------------------
                                            ' Domestic
                                            ' ------------------------------
                                            sLocalErrorCode = "150"

                                            ' 10.12.2019 - test if Returfiles or not
                                            If oPayment.StatusCode = "00" Then
                                                sLine = WriteDomBetalning_PI00(oPayment)
                                                oFile.WriteLine(sLine)
                                            Else
                                                'DR00 Payment record
                                                sLine = WritePayment_DR00(oPayment)
                                                oFile.WriteLine(sLine)
                                            End If

                                            sOldAccount = oPayment.I_Account
                                            sOldPayType = "D"

                                            sLine = WriteDomIntAvsandarnoteringar_BA00(oPayment)
                                            oFile.WriteLine(sLine)

                                            ' No BE01 for Plusgiro payments, Bankgiropayments
                                            If sBetalningsTyp <> "00" And sBetalningsTyp <> "05" Then
                                                sLine = WriteDomMottagarNamn_BE01(oPayment)
                                                oFile.WriteLine(sLine)
                                            End If

                                            ' BE02 and BE03 only for Utbetalingsanvisning
                                            If sBetalningsTyp = "02" Then
                                                sLine = WriteDomMottagarOrt_BE02(oPayment)
                                                oFile.WriteLine(sLine)

                                                sLine = WriteDomIntMottagarGatuadress_BE03(oPayment)
                                                oFile.WriteLine(sLine)
                                            End If

                                        End If

                                        For Each oInvoice In oPayment.Invoices
                                            sLocalErrorCode = "160"
                                            If Not oInvoice.Exported Then

                                                ' Find all freetext;
                                                ' ------------------
                                                sTxt = ""
                                                For Each oFreetext In oInvoice.Freetexts
                                                    sTxt = sTxt & Trim$(oFreetext.Text)
                                                Next oFreetext


                                                If oPayment.PayType = "I" Then
                                                    ' ------------------------------
                                                    ' International
                                                    ' ------------------------------
                                                    bInternational = True
                                                    ' Max 140
                                                    sTxt = Left$(sTxt, 140)


                                                    If bThereAreCreditNotes And oPayment.Invoices.Count < 5 And Not EmptyString(oPayment.E_Account) Then  ' max 5 CNKR/CNDB
                                                        sLocalErrorCode = "170"
                                                        sLine = WriteDomRedovisning_CNKR_CNDB(oInvoice)
                                                        oFile.WriteLine(sLine)

                                                    Else
                                                        sLocalErrorCode = "180"
                                                        If Len(Trim(oInvoice.Unique_Id)) > 0 Then
                                                            sTxt1 = oInvoice.Unique_Id
                                                            sTxt2 = ""
                                                            sTxt = ""
                                                        ElseIf Len(Trim(oInvoice.InvoiceNo)) > 0 Then
                                                            sTxt1 = oInvoice.InvoiceNo
                                                            sTxt2 = ""
                                                            sTxt = ""

                                                        Else
                                                            sTxt1 = Left$(sTxt, 35)
                                                            sTxt2 = Mid$(sTxt, 36, 35)
                                                            sTxt = Mid$(sTxt, 71)
                                                        End If

                                                        ' Max 2 BM99-poster for utland
                                                        sLine = WriteDomIntMeddelande_BM99(sTxt1, sTxt2)
                                                        oFile.WriteLine(sLine)

                                                        If Len(sTxt) > 0 Then
                                                            sTxt1 = Left$(sTxt, 35)
                                                            sTxt2 = Mid$(sTxt, 36, 35)
                                                            sLine = WriteDomIntMeddelande_BM99(sTxt1, sTxt2)
                                                            oFile.WriteLine(sLine)
                                                        End If

                                                    End If

                                                Else
                                                    ' ------------------------------
                                                    ' Domestic
                                                    ' ------------------------------
                                                    bInternational = False

                                                    ' Max 350
                                                    sTxt = Left$(sTxt, 350)

                                                    If (sBetalningsTyp = "00" Or sBetalningsTyp = "05") And bThereAreCreditNotes _
                                                         And oPayment.Invoices.Count < 300 Then ' max 300 CNKR/CNDB
                                                        sLocalErrorCode = "190"
                                                        sLine = WriteDomRedovisning_CNKR_CNDB(oInvoice)
                                                        oFile.WriteLine(sLine)

                                                    Else
                                                        sLocalErrorCode = "195"
                                                        'If sBetalningsTyp = "00" Or sBetalningsTyp = "05" Then
                                                        If Len(Trim(oInvoice.Unique_Id)) > 0 Then
                                                            sTxt1 = oInvoice.Unique_Id
                                                            sTxt2 = ""
                                                            sTxt = ""
                                                        ElseIf Len(Trim(oInvoice.InvoiceNo)) > 0 Then
                                                            sTxt1 = oInvoice.InvoiceNo
                                                            sTxt2 = ""
                                                            sTxt = ""

                                                        Else
                                                            sTxt1 = Left$(sTxt, 35)
                                                            sTxt2 = Mid$(sTxt, 36, 35)
                                                            sTxt = Mid$(sTxt, 71)
                                                        End If

                                                        If sBetalningsTyp = "09" Then
                                                            ' Bankkontoisetting, no more meddelande
                                                            sTxt1 = ""
                                                            sTxt2 = ""
                                                            sTxt = ""
                                                        End If

                                                        j = 0
                                                        Do While Len(sTxt1) > 0
                                                            sLine = WriteDomIntMeddelande_BM99(sTxt1, sTxt2)
                                                            oFile.WriteLine(sLine)

                                                            sTxt1 = Left$(sTxt, 35)
                                                            sTxt2 = Mid$(sTxt, 36, 35)
                                                            sTxt = Mid$(sTxt, 71)
                                                        Loop
                                                    End If
                                                End If

                                            End If ' If Not oInvoice.Exported Then
                                            oInvoice.Exported = True
                                        Next oInvoice

                                        If oPayment.PayType = "I" And sBetalningsTyp = "03" And Not EmptyString(oPayment.E_Account) Then
                                            If Not (bThereAreCreditNotes And oPayment.Invoices.Count < 5 And Not EmptyString(oPayment.E_Account)) Then  ' max 5 CNKR/CNDB
                                                ' IB-poster angis før eventuelle CNKR/CNDB-poster, MEN ETTER BM99-poster !!!

                                                ' IB-records AFTER BM99-records

                                                'IB01 Mottagande banks namn (vid SWIFT betalning)
                                                sLine = WriteIntMottagerBank_IB01(oPayment)
                                                oFile.WriteLine(sLine)

                                                'IB02 Mottagande banks ortsadress
                                                sLine = WriteIntMottagerBankOrt_IB02(oPayment)
                                                oFile.WriteLine(sLine)

                                                'IB03 Mottagande banks adress
                                                sLine = WriteIntMottagerBankAdress_IB03(oPayment)
                                                oFile.WriteLine(sLine)
                                            End If
                                        End If

                                        oPayment.Exported = True

                                    End If 'If bExportoPayment Then

                                Next ' oPayment
                            End If 'If bExportoBatch Then
                        End If 'If oBatch.Payments.Count > 0 Then

                    Next 'oBatch
                End If 'If bExportoBabel Then
            Next 'oBabelfile

            If iPayNumber > 0 Then
                If oPayment.StatusCode = "00" Then   ' 17.12.2019 added statuscode = "00"
                    sLine = WriteAvslutningspost_MT00()
                    oFile.WriteLine(sLine)
                Else
                    sLine = WriteAvslutningspost_MT01()
                    oFile.WriteLine(sLine)
                End If
            End If

            sLocalErrorCode = "199"

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteCorporateFilePayments" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteCorporateFilePayments = True

    End Function
    Function WriteInnledningspost1_MH00(ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal sCompanyNo As String) As String
        Dim sLine As String

        sLocalErrorCode = "200"
        ' Reset file-totals:
        nFileSumAmount = 0
        iPayNumber = 0

        sLine = "MH00"                                          ' 1-4
        sLine = sLine & Space(8)                                ' 5-12

        If EmptyString(sCompanyNo) Then
            sLine = sLine & PadRight(oBatch.I_EnterpriseNo, 10, " ") '13-22 Org.nr
        Else
            sLine = sLine & PadRight(sCompanyNo, 10, " ")   '13-22 Org.nr
        End If
        sLine = sLine & Space(12)                               '23-34
        sLine = sLine & PadRight(oPayment.I_Account, 10, " ")   '35-44 Avsenders kontonr
        If EmptyString(oPayment.MON_InvoiceCurrency) Then
            oPayment.MON_InvoiceCurrency = "SEK"
        End If
        sLine = sLine & oPayment.MON_InvoiceCurrency            '45-47 Betalingsvaluta
        sLine = sLine & Space(6)                                '48-53
        If EmptyString(oPayment.MON_AccountCurrency) Then
            oPayment.MON_AccountCurrency = "SEK"
        End If
        'sLine = sLine & oPayment.MON_AccountCurrency            '54-56 Kontoens valutaficka
        ' XNET 21.04.2015 endret til TransferCurrency - in Visma this means PayISO
        sLine = sLine & oPayment.MON_TransferCurrency            '54-56 Kontoens valutaficka
        sLine = sLine & Space(24)                               '57-80

        WriteInnledningspost1_MH00 = UCase(sLine)

    End Function
    Function WriteInnledningspost2_MH01(ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal sCompanyNo As String) As String
        Dim sLine As String

        sLocalErrorCode = "205"
        ' Reset file-totals:
        nFileSumAmount = 0
        iPayNumber = 0

        sLine = "MH01"                                          ' 1-4
        If oPayment.StatusCode <> "00" Then
            sLine = sLine & "DEBADV  "                          ' 5-12

            If EmptyString(sCompanyNo) Then
                sLine = sLine & PadRight(oBatch.I_EnterpriseNo, 10, " ") '13-22 Org.nr
            Else
                sLine = sLine & PadRight(sCompanyNo, 10, " ")   '13-22 Org.nr
            End If
            sLine = sLine & Space(4)                                '23-26

            sLine = sLine & PadRight(oPayment.I_Account, 10, " ")   '27-36 Avsenders kontonr
            sLine = sLine & oPayment.DATE_Payment                   '37-44  ÅÅÅÅMMDD

            sLine = sLine & Space(3)                                '45–47 Account statement number, optional
            sLine = sLine & Space(3)                                '48–50 SEK/EUR (PlusGirot internal)
            sLine = sLine & Space(15)                               '51–65 Blank space/Reserve
            sLine = sLine & oPayment.MON_TransferCurrency            '66–68 Currency pocket SEK/EUR
            sLine = sLine & Space(12)                               '69–80 Blank space/Reserve
        Else
            sLine = sLine & Space(8)                            ' 5-12

            If EmptyString(sCompanyNo) Then
                sLine = sLine & PadRight(oBatch.I_EnterpriseNo, 10, " ") '13-22 Org.nr
            Else
                sLine = sLine & PadRight(sCompanyNo, 10, " ")   '13-22 Org.nr
            End If
            sLine = sLine & Space(2)                                '23-25
            If EmptyString(oPayment.MON_InvoiceCurrency) Then
                oPayment.MON_InvoiceCurrency = "SEK"
            End If
            sLine = sLine & oPayment.MON_InvoiceCurrency            '26-28 Betalingsvaluta
            sLine = sLine & Space(6)                                '29-34
            sLine = sLine & PadRight(oPayment.I_Account, 35, " ")   '35-69 Avsenders IBAN kontonr
            If EmptyString(oPayment.MON_AccountCurrency) Then
                oPayment.MON_AccountCurrency = "SEK"
            End If
            'sLine = sLine & oPayment.MON_AccountCurrency            '70-72 Kontoens valutaficka
            ' XNET 21.04.2015 endret til TransferCurrency - in Visma this means PayISO
            sLine = sLine & oPayment.MON_TransferCurrency            '70-72 Kontoens valutaficka
            sLine = sLine & Space(8)                                '73-80
        End If
        WriteInnledningspost2_MH01 = UCase(sLine)

    End Function
    Function WriteDomBetalning_PI00(ByVal oPayment As Payment) As String
        Dim sLine As String
        Dim bToBankAccount As Boolean

        sLocalErrorCode = "210"
        bToBankAccount = False

        ' Add to Batch-totals:
        nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount

        sLine = "PI00"                                          '1-4

        ' 28.01.2015'
        ' Moved Utbetalningskort til toppen, da blank mottakerkonto uansett vil overstyre type konto
        If EmptyString(oPayment.E_Account) Then
            sLine = sLine & "02"                                '5-6 Utbetalingskort
            sBetalningsTyp = "02"

        ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Then
            sLine = sLine & "00"                                '5-6 Plusgiro
            sBetalningsTyp = "00"
        ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro Then
            sLine = sLine & "05"                                '5-6 Bankgiro
            sBetalningsTyp = "05"
        ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount Then
            ' to bankaccount
            If oPayment.PayCode = "201" Then
                sLine = sLine & "06"                            '5-6 Pensjon til bankkonto
                sBetalningsTyp = "06"
            ElseIf oPayment.PayCode = "203" Then
                sLine = sLine & "08"                            '5-6 Lønn til bankkonto
                sBetalningsTyp = "08"
            Else
                sLine = sLine & "09"                            '5-6 Utbetaling til bankkonto
                sBetalningsTyp = "09"
                bToBankAccount = True
            End If
        Else
            sLine = sLine & "00"                                '5-6 Default to Plusgiro
            sBetalningsTyp = "00"
        End If

        If oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount Then
            ' clearingcode, 5 siffer for Swedbank, ellers 4
            If Left$(oPayment.E_Account, 1) = "8" Then
                sLine = sLine & Left$(oPayment.E_Account, 5)    '7-11 Clearingcode
                sLine = sLine & PadRight(Mid$(oPayment.E_Account, 6), 11, " ")  '12-22
            Else
                sLine = sLine & Left$(oPayment.E_Account, 4) & " " '7-11 Clearingcode
                sLine = sLine & PadRight(Mid$(oPayment.E_Account, 5), 11, " ")  '12-22
            End If
        Else
            sLine = sLine & Space(5)                                '7-11
            sLine = sLine & PadRight(oPayment.E_Account, 11, " ")   '12-22
        End If
        sLine = sLine & Space(2)                                '23-24
        sLine = sLine & oPayment.DATE_Payment                   '25-32  ÅÅÅÅMMDD
        sLine = sLine & PadLeft(Str(oPayment.MON_InvoiceAmount), 13, "0")  '33-45 Belopp, i øre/cent
        If bToBankAccount Then
            If oPayment.Invoices.Count > 0 Then
                If oPayment.Invoices(1).Freetexts.Count > 0 Then
                    sLine = sLine & PadRight(Left$(oPayment.Invoices(1).Freetexts(1).Text, 12), 20, " ") '46-65 Meddelande
                Else
                    sLine = sLine & Space(20)                    '46-65 Meddelande
                End If
            Else
                sLine = sLine & Space(20)                        '46-65 Meddelande
            End If
            sLine = sLine & Space(15)                               '66-80
        Else
            sLine = sLine & Space(25)                               '46-70 Meddelande (bruk egne records isteden!)
            sLine = sLine & Space(10)                               '71-80
        End If
        WriteDomBetalning_PI00 = UCase(sLine)

    End Function
    Function WriteDomIntAvsandarnoteringar_BA00(ByVal oPayment As Payment) As String
        Dim sLine As String
        sLocalErrorCode = "220"

        sLine = "BA00"                                          '1-4
        sLine = sLine & PadRight(oPayment.Invoices(1).REF_Own, 18, " ")     '5-22 Egna noteringar
        sLine = sLine & Space(9)                                '25-31
        sLine = sLine & PadRight(oPayment.Invoices(1).REF_Own, 35, " ")     '32-66 Egen referens
        sLine = sLine & Space(14)                                '67-80

        WriteDomIntAvsandarnoteringar_BA00 = UCase(sLine)

    End Function
    Function WriteDomMottagarNamn_BE01(ByVal oPayment As Payment) As String
        Dim sLine As String
        sLocalErrorCode = "230"

        sLine = "BE01"                                         '1-4
        sLine = sLine & Space(18)                               '5-22
        sLine = sLine & PadRight(oPayment.E_Name, 35, " ")      '23-57 Navn
        sLine = sLine & Space(23)                               '58-80

        WriteDomMottagarNamn_BE01 = UCase(sLine)

    End Function
    Function WriteDomMottagarOrt_BE02(ByVal oPayment As Payment) As String
        Dim sLine As String
        sLocalErrorCode = "240"

        sLine = "BE02"                                         '1-4
        sLine = sLine & PadRight(oPayment.E_Zip, 9, " ")        '5-13 Postnr
        sLine = sLine & PadRight(oPayment.E_City, 25, " ")      '14-38 Poststed
        sLine = sLine & Space(42)                               '39-80

        WriteDomMottagarOrt_BE02 = UCase(sLine)

    End Function
    Function WriteDomIntMeddelande_BM99(ByVal sTxt1 As String, ByVal sTxt2 As String) As String
        Dim sLine As String
        sLocalErrorCode = "250"

        sLine = "BM99"                                         '1-4
        sLine = sLine & PadRight(sTxt1, 35, " ")               '5-39 Meddelande 1
        sLine = sLine & PadRight(sTxt2, 35, " ")               '40-74 Meddelande 2
        sLine = sLine & Space(6)                               '75-80

        WriteDomIntMeddelande_BM99 = UCase(sLine)

    End Function
    Function WriteDomRedovisning_CNKR_CNDB(ByVal oInvoice As Invoice) As String
        Dim sLine As String
        sLocalErrorCode = "260"

        If oInvoice.MON_InvoiceAmount < 0 Then
            sLine = "CNKR"                                          '1-4
        Else
            sLine = "CNDB"                                          '1-4
        End If
        If Len(oInvoice.Unique_Id) > 0 Then
            sLine = sLine & PadRight(oInvoice.Unique_Id, 35, " ")   '5-39 Meddelande 1
        Else
            sLine = sLine & PadRight(oInvoice.InvoiceNo, 35, " ")   '5-39 Meddelande 1
        End If
        sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 13, "0")  '40-52 Belopp, i øre/cent
        sLine = sLine & PadRight(oInvoice.REF_Own, 28, " ")         '53-80 Egen reference

        WriteDomRedovisning_CNKR_CNDB = UCase(sLine)

    End Function
    Function WriteAvslutningspost_MT00() As String
        Dim sLine As String
        sLocalErrorCode = "270"

        sLine = "MT00"                                           ' 1-4
        sLine = sLine & Space(25)                                ' 5-29
        sLine = sLine & PadLeft(LTrim(Str(iPayNumber)), 7, "0")  '30-36 Antall betalningar
        sLine = sLine & PadLeft(Str(nFileSumAmount), 15, "0")    '37-51 TotalBelopp, i øre/cent
        sLine = sLine & Space(29)                                '52-80

        WriteAvslutningspost_MT00 = UCase(sLine)

    End Function
    Function WriteAvslutningspost_MT01() As String
        Dim sLine As String
        sLocalErrorCode = "270"

        sLine = "MT01"                                           ' 1-4
        sLine = sLine & PadLeft(LTrim(Str(iPayNumber)), 7, "0")  ' 5-11 Antall betalningar
        sLine = sLine & PadLeft(Str(nFileSumAmount), 15, "0")    '12-26 TotalBelopp, i øre/cent
        sLine = sLine & Space(54)                                '27-80

        WriteAvslutningspost_MT01 = UCase(sLine)

    End Function
    Function WriteIntBetalning_PI00(ByVal oPayment As Payment) As String
        Dim sLine As String
        Dim bToBankAccount As Boolean
        sLocalErrorCode = "280"

        bToBankAccount = False

        ' Add to Batch-totals:
        nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount

        sLine = "PI00"                                          '1-4
        sLine = sLine & "03"                                    '5-6 Plusgiro
        sBetalningsTyp = "03"
        sLine = sLine & Space(18)                               '7-24
        sLine = sLine & oPayment.DATE_Payment                   '25-32  ÅÅÅÅMMDD
        sLine = sLine & PadLeft(Str(oPayment.MON_InvoiceAmount), 13, "0")  '33-45 Belopp, i øre/cent
        sLine = sLine & Space(21)                               '46-66
        If oPayment.Invoices.Count > 0 Then
            sLine = sLine & PadRight(oPayment.Invoices(1).STATEBANK_Code, 3, " ") '67-69 Riksbankkode
        Else
            sLine = sLine & Space(3)                            '67-69
        End If
        sLine = sLine & Space(1)                                '70
        If oPayment.Priority Then
            sLine = sLine & "E"                                 '71 E=Express
        ElseIf oPayment.ToOwnAccount Then
            sLine = sLine & "S"                                 '71 S=Intercompany
        Else
            sLine = sLine & "N"                                 '71 N=Normal
        End If
        sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ") '72-74 Valuta
        sLine = sLine & Space(2)                                '75-76
        If oPayment.MON_ChargeMeAbroad Then
            sLine = sLine & "O"                                 '77 Betaler tar alle avgifter
        Else
            sLine = sLine & "B"                                 '77 Delte avgifter
        End If
        If oPayment.MON_InvoiceCurrency <> oPayment.MON_TransferCurrency Then
            sLine = sLine & PadRight(oPayment.MON_TransferCurrency, 3, " ") '78-80 Overføringsvaluta, hvis ulik fra fakturavaluta
        Else
            sLine = sLine & Space(3)                            '78-80
        End If
        WriteIntBetalning_PI00 = UCase(sLine)

    End Function
    Function WriteIntMottagarKonto_BE00(ByVal oPayment As Payment) As String
        Dim sLine As String
        sLocalErrorCode = "290"

        sLine = "BE00"                                          '1-4
        sLine = sLine & Space(18)                               '5-22
        If EmptyString(oPayment.E_Account) Then
            sLine = sLine & "CHK"                               '23-25 Check
        Else
            sLine = sLine & "SWT"                               '23-25 Swift
        End If
        If oPayment.MergeThis Then
            sLine = sLine & "S"                                 '26 S=Samsortering
        Else
            sLine = sLine & " "                                 '26
        End If
        sLine = sLine & Space(19)                               '27-45
        sLine = sLine & PadRight(oPayment.E_Account, 35, " ")   '46-80 Mottakers kontonr

        WriteIntMottagarKonto_BE00 = UCase(sLine)

    End Function
    Function WriteIntMottagarNamn_BE01(ByVal oPayment As Payment) As String
        Dim sLine As String
        sLocalErrorCode = "300"

        sLine = "BE01"                                          '1-4
        'sLine = sLine & Space(18)                               '5-22
        'sLine = sLine & Left(oPayment.I_Account, 4) & Space(1)  '5-9  regnr
        'sLine = sLine & PadRight(Mid(oPayment.I_Account, 5), 13, " ")   ' 10-22 I_Account, incl. regnr

        ' 17.12.2019 - endret til å dele regnr og kontonr
        ' OG - her skal det vel være mottakers konto???
        If IsIBANNumber(oPayment.E_Account, False, False) = Scripting.Tristate.TristateTrue Then
            sLine = sLine & Mid(oPayment.E_Account, 5, 4) & Space(1)  '5-9  regnr
            sLine = sLine & PadRight(Mid(oPayment.E_Account, 9), 13, " ")   ' 10-22 I_Account, incl. regnr
        ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro Then
            sLine = sLine & "BGC  "                             '5-9  regnr
            sLine = sLine & PadRight(oPayment.E_Account, 13, " ")   ' 10-22 I_Account, incl. regnr
        ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Then
            sLine = sLine & "9962 "                             '5-9  regnr
            sLine = sLine & PadRight(oPayment.E_Account, 13, " ")   ' 10-22 I_Account, incl. regnr
        Else
            sLine = sLine & Left(oPayment.E_Account, 4) & Space(1)  '5-9  regnr
            sLine = sLine & PadRight(Mid(oPayment.E_Account, 5), 13, " ")   ' 10-22 I_Account, incl. regnr
        End If

        sLine = sLine & PadRight(oPayment.E_Name, 35, " ")      '23-57 Navn
        'if Bank countrycode unknown, try to pick from BIC
        If EmptyString(oPayment.BANK_CountryCode) Then
            oPayment.BANK_CountryCode = Mid$(oPayment.BANK_SWIFTCode, 5, 2)
        End If
        sLine = sLine & PadRight(oPayment.BANK_CountryCode, 3, " ") '58-60 BANK Countrycode
        sLine = sLine & Space(20)                               '61-80

        WriteIntMottagarNamn_BE01 = UCase(sLine)

    End Function
    Function WriteDomIntMottagarGatuadress_BE03(ByVal oPayment As Payment) As String
        Dim sLine As String
        sLocalErrorCode = "310"

        sLine = "BE03"                                          '1-4
        sLine = sLine & PadRight(oPayment.E_Adr1, 35, " ")      '5-39 adr1
        sLine = sLine & PadRight(oPayment.E_Adr2, 35, " ")      '40-74 adr2
        sLine = sLine & Space(6)                                '75-80

        WriteDomIntMottagarGatuadress_BE03 = UCase(sLine)

    End Function
    Function WriteIntMottagerBank_IB01(ByVal oPayment As Payment) As String
        Dim sLine As String
        sLocalErrorCode = "320"

        sLine = "IB01"                                          '1-4
        sLine = sLine & Space(3)                                '5-7
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCode, 11, " ") '8-18 Swift
        sLine = sLine & Space(9)                                '19-27
        sLine = sLine & PadRight(oPayment.BANK_Name, 35, " ")   '28-62 Banknavn

        ' XOKNET 31.07.2016 - Ikke ta med landkode dersom BANK_Countrycode ikke er fylt inn:
        If Not EmptyString(oPayment.BANK_CountryCode) Then
            ' Bankkod for visse land;
            Select Case oPayment.BANK_CountryCode
                Case "AU"
                    ' Australia - BSB
                    sLine = sLine & PadRight("//AU" & oPayment.BANK_BranchNo, 15, " ")  ' 63-77 Bankcode
                Case "CA"
                    ' Canada - CanadianClearing CC
                    sLine = sLine & PadRight("//CC" & oPayment.BANK_BranchNo, 15, " ")  ' 63-77 Bankcode
                Case "ZA"
                    ' South Africa - Sortcode
                    sLine = sLine & PadRight("//ZA" & oPayment.BANK_BranchNo, 15, " ")  ' 63-77 Bankcode
                Case "US"
                    ' US - Fedwire
                    sLine = sLine & PadRight("//FW" & oPayment.BANK_BranchNo, 15, " ")  ' 63-77 Bankcode
                Case Else
                    ' for other countries, blank
                    sLine = sLine & Space(15)                           '63-77
            End Select
        Else
            sLine = sLine & Space(15)                           '63-77
        End If
        sLine = sLine & Space(3)                                '78-80

        WriteIntMottagerBank_IB01 = UCase(sLine)

    End Function

    Function WriteIntMottagerBankOrt_IB02(ByVal oPayment As Payment) As String
        Dim sLine As String
        sLocalErrorCode = "330"

        sLine = "IB02"                                           '1-4
        sLine = sLine & Space(13)                                '5-17
        sLine = sLine & PadRight(oPayment.BANK_Adr3, 35, " ")    '18-52 bankcity
        sLine = sLine & Space(28)                                '53-80

        WriteIntMottagerBankOrt_IB02 = UCase(sLine)

    End Function
    Function WriteIntMottagerBankAdress_IB03(ByVal oPayment As Payment) As String
        Dim sLine As String
        sLocalErrorCode = "340"

        sLine = "IB03"                                           '1-4
        sLine = sLine & PadRight(oPayment.BANK_Adr1, 35, " ")    '5-39 bank adr1
        sLine = sLine & PadRight(oPayment.BANK_Adr2, 35, " ")    '40-74 bank adr2
        sLine = sLine & Space(6)                                '75-80

        WriteIntMottagerBankAdress_IB03 = UCase(sLine)

    End Function
    Function WritePayment_DR00(ByVal oPayment As Payment) As String
        Dim sLine As String
        Dim bToBankAccount As Boolean
        sLocalErrorCode = "281"

        sBetalningsTyp = "03"

        ' Add to Batch-totals:
        nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount

        sLine = "DR00"                                          '1-4

        'sLine = sLine & PadRight(oPayment.I_Account, 18, " ")   '5-9 og 10-22 I_Account, incl. regnr
        ' 17.12.2019 - endret til å dele regnr og kontonr
        ' OG - her skal det vel være mottakers konto???
        If IsIBANNumber(oPayment.E_Account, False, False) = Scripting.Tristate.TristateTrue Then
            sLine = sLine & Mid(oPayment.E_Account, 5, 4) & Space(1)  '5-9  regnr
            sLine = sLine & PadRight(Mid(oPayment.E_Account, 9), 13, " ")   ' 10-22 I_Account, incl. regnr
        ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro Then
            sLine = sLine & "BGC  "                             '5-9  regnr
            sLine = sLine & PadRight(oPayment.E_Account, 13, " ")   ' 10-22 I_Account, incl. regnr
        ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Then
            sLine = sLine & "9962 "                             '5-9  regnr
            sLine = sLine & PadRight(oPayment.E_Account, 13, " ")   ' 10-22 I_Account, incl. regnr
        Else
            sLine = sLine & Left(oPayment.E_Account, 4) & Space(1)  '5-9  regnr
            sLine = sLine & PadRight(Mid(oPayment.E_Account, 5), 13, " ")   ' 10-22 I_Account, incl. regnr
        End If
        sLine = sLine & PadLeft(Str(oPayment.MON_InvoiceAmount), 13, "0")  '23-35 Belopp, i øre/cent
        sLine = sLine & Space(8)                               '36-43 Reg.Id (not filled in)
        sLine = sLine & Space(8)                               '44-51 Sign.Id (not filled in)
        sLine = sLine & PadRight(oPayment.REF_Bank1, 16, " ")  ' 52–67 Archive and/or document ID.
        sLine = sLine & Space(7)                               ' 68–74 Blank space/Reserve
        If oPayment.PayType = "I" Then
            sLine = sLine & "U"
        Else
            sLine = sLine & "I"
        End If
        sLine = sLine & Space(2)                               '76-77
        sLine = sLine & "E"                                    '78
        sLine = sLine & "N"                                    '79
        sLine = sLine & Space(1)                               '80
        WritePayment_DR00 = UCase(sLine)

    End Function
    Function WriteExchange_DR01(ByVal oPayment As Payment) As String
        Dim sLine As String
        sLocalErrorCode = "282"

        sLine = "DR01"                                          '1-4
        sLine = sLine & Space(18)   '5–22 Blank space/Reserve
        sLine = sLine & PadLeft(Str(oPayment.MON_InvoiceAmount), 13, "0")  '23–35 Ordered amount, without decimal point. Öre/cent.
        sLine = sLine & PadLeft(CStr(oPayment.MON_LocalExchRate), 9, "0")         '36–44 Exchange rate
        sLine = sLine & oPayment.MON_TransferCurrency                              '45–47 Currency code for ordered amount. SEK/EUR.
       
        sLine = sLine & Space(33)                               '48–80 Blank space/Reserve

        WriteExchange_DR01 = UCase(sLine)

    End Function




End Module

