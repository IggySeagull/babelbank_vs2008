Option Strict Off
Option Explicit On
Module WriteDirRem
	
	'Dim oFs As Object
	'Dim oFile As TextStream
	Dim sLine As String ' en output-linje
	Dim oBatch As Batch
	' Vi m� dimme noen variable for
	' - batchniv�: sum bel�p, antall records, antall transer, f�rste dato, siste dato
	Dim BatchSumAmount As Decimal
	Dim BatchNoRecords, BatchNoTransactions As Double
	Dim dBatchLastDate, dBatchFirstDate As Date
	' - fileniv�: sum bel�p, antall records, antall transer, siste dato
	'             disse p�virkes nedover i niv�ene
	Dim nFileNoRecords, nFileSumAmount, nFileNoTransactions As Double
	Dim dFileFirstDate As Date
	'Dim nTransactionNo
    Function WriteDirRemFile(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef sAdditionalID As String, ByRef sClientNo As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim k, i, j, l As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim iTransactionNo As Double
        Dim sNewOwnref As String
        Dim sPayCode As String
        Dim nLine As Short
        Dim nCol As Short
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim bFileStartWritten As Boolean
        ' added 01.06.2010
        Dim sOldAccount As String
        sOldAccount = ""


        ' lag en outputfil
        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                'Only export statuscode 00 or 02. The others don't give any sense in DirRem
                If (oBabel.StatusCode = "00" And Not oBabel.FileFromBank) Or oBabel.StatusCode = "02" Then
                    'Have to go through each batch-object to see if we have objects thatt shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoBabel = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBabel = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If
                        Next oPayment
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oBatch
                Else
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            oPayment.Exported = True
                        Next oPayment
                    Next oBatch
                End If 'If oBabel.StatusCode = "00" Or oBabel.StatusCode = "02" Then

                If bExportoBabel Then
                    If Not bFileStartWritten Then
                        ' write only once!
                        ' When merging Client-files, skip this one except for first client!
                        sLine = WriteDirRemFileStart(oBabel, sAdditionalID)
                        oFile.WriteLine((sLine))
                        bFileStartWritten = True
                    End If

                    i = 0
                    iTransactionNo = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBatch = True
                                            End If
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment


                        If bExportoBatch Then
                            i = i + 1

                            ' added 01.06.2010 and 07.06.2010 to take care of several accounts within one batch (like Eidsiva returnfile)
                            If oBatch.Payments.Count > 0 Then
                                sLine = WriteDirRemBatchStart(oBatch, oBatch.Payments.Item(1))
                                oFile.WriteLine((sLine))
                                sOldAccount = oBatch.Payments.Item(1).I_Account
                            End If

                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    If oPayment.Cancel = False Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoPayment = True
                                                End If
                                            End If
                                        End If
                                    Else
                                        ' added next If 19.11.2009
                                        If oBabel.Special <> "FIRSTSECURITIES" Then
                                            oPayment.Exported = True
                                        End If
                                    End If 'If oPayment.Cancel = False Then
                                End If

                                If bExportoPayment Then
                                    j = j + 1

                                    ' added 01.06.2010 to take care of several accounts within one batch (like Eidsiva returnfile)
                                    If sOldAccount <> oPayment.I_Account Then
                                        ' must create a "batch-end record" (88-record) and a new "batch-start record" (20-record)
                                        sLine = WriteDirRemBatchEnd()
                                        oFile.WriteLine((sLine))
                                        sLine = WriteDirRemBatchStart(oBatch, oPayment)
                                        oFile.WriteLine((sLine))
                                        sOldAccount = oPayment.I_Account
                                    End If

                                    ' Add to transactionno used in paymentone/two
                                    iTransactionNo = iTransactionNo + 1

                                    sLine = WriteDirRemPaymentOne(oPayment, iTransactionNo)
                                    oFile.WriteLine((sLine))
                                    sLine = WriteDirRemPaymentTwo(oPayment, iTransactionNo)
                                    oFile.WriteLine((sLine))
                                    'Remove later: ExportCheck
                                    oPayment.Exported = True
                                    'If it is a return file,
                                    'The following recordtypes are not exported.
                                    If oBabel.FileFromBank = False Then

                                        ' transtype from Babelbanks internal set of codes
                                        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "DIRREM")

                                        If sPayCode = "03" Or sPayCode = "04" Then
                                            ' There may / may not be adress-spesifications on payment level
                                            sLine = WriteDirRemAdressOne(oPayment, iTransactionNo)
                                            oFile.WriteLine((sLine))
                                            sLine = WriteDirRemAdressTwo(oPayment, iTransactionNo)
                                            oFile.WriteLine((sLine))
                                        End If


                                        If sPayCode = "16" Or sPayCode = "17" Or sPayCode = "03" Or sPayCode = "04" Then
                                            ' underspec kid or creditnote or underspec - advice

                                            ' invoice(s)
                                            k = 0
                                            For Each oInvoice In oPayment.Invoices
                                                k = k + 1
                                                If sPayCode = "16" Or sPayCode = "17" Then
                                                    ' underspec kid or creditnote
                                                    sLine = WriteDirRemUnderSpesifikasjon(oPayment, oInvoice, iTransactionNo)
                                                    oFile.WriteLine((sLine))
                                                ElseIf sPayCode = "03" Or sPayCode = "04" Then
                                                    ' underspec - advice

                                                    l = 1
                                                    nCol = 1 ' which column in output
                                                    nLine = 1 ' which line in output
                                                    For Each oFreeText In oInvoice.Freetexts
                                                        ' g� gjennom alle freetext-er
                                                        l = l + 1


                                                        sLine = WriteDirRemSpesifikasjon(oPayment, oFreeText, iTransactionNo, nCol, nLine)
                                                        oFile.WriteLine((sLine))

                                                        If nLine = 21 Then
                                                            ' skriv f�rst alt i f�rste kolonne, deretter fra spes. 22 i kolonne 2
                                                            nCol = 2
                                                            nLine = 1
                                                        Else
                                                            nLine = nLine + 1
                                                        End If

                                                        ' max 42 spesifications in one payment
                                                        If l > 42 Then
                                                            Exit For
                                                        End If
                                                    Next oFreeText 'oFreetext
                                                End If

                                            Next oInvoice 'invoice

                                        End If
                                    Else
                                        'Theese recordtypes are not exported in the returnfile
                                    End If
                                End If

                            Next oPayment ' payment

                            sLine = WriteDirRemBatchEnd()
                            oFile.WriteLine((sLine))
                        End If

                    Next oBatch 'batch

                    ' Moved to next lines out of for/next, gave several fileends
                    ' when merging files (Clientfiles)
                    'sLine = WriteDirRemFileEnd()
                    'oFile.WriteLine (sLine)

                End If
            Next oBabel 'Babelfile

            ' Added 17.08.2007, for Hafslund special files with Avtalegiro only
            If UCase(oBabelFiles(1).Special) = "HAFSLUND" And nFileNoRecords = 0 Then
                ' do nothing, 89-record is included in oImportedLines
            Else
                ' as pre 17.08.2007
                sLine = WriteDirRemFileEnd()
                oFile.WriteLine((sLine))
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteDirRemFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteDirRemFile = True

    End Function
    Function WriteDirRemFileStart(ByRef oBabelFile As BabelFile, ByRef sAdditionalID As String) As String
        ' ok jp
        Dim sLine As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileNoRecords = 1
        nFileNoTransactions = 0
        dFileFirstDate = System.Date.FromOADate(Now.ToOADate + 1000)

        sLine = "NY000010"

        If oBabelFile.FileFromBank = False Then
            ' husk evt ledende 0-er!
            If sAdditionalID <> "" Then
                sLine = sLine & PadLeft(sAdditionalID, 8, "0")
            Else
                sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Sender, 1, 8), 8, "0") ' dataavsender
            End If
            sLine = sLine & PadLeft(Mid(oBabelFile.File_id, 1, 7), 7, "0") ' forsendelsesnr

            If oBabelFile.Special = "FIRSTSECURITIES" Then ' added 18.11.2009
                sLine = sLine & "00001212"
            Else
                If Len(oBabelFile.IDENT_Receiver) = 0 Then
                    sLine = sLine & "00008080"
                Else
                    sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Receiver, 1, 8), 8, "0") ' datamottaker
                End If
            End If
        Else
            ' returfil fra bank/BBS
            If Len(oBabelFile.IDENT_Receiver) = 0 Then
                sLine = sLine & "00008080"
            Else
                sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Receiver, 1, 8), 8, "0") ' dataavsender
            End If
            sLine = sLine & PadLeft(Mid(oBabelFile.File_id, 1, 7), 7, "0") ' forsendelsesnr
            If sAdditionalID <> "" Then
                sLine = sLine & PadLeft(sAdditionalID, 8, "0")
            Else
                sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Sender, 1, 8), 8, "0") ' datamottaker
            End If
        End If
        'pad to 80 with 0
        sLine = PadLine(sLine, 80, "0")

        WriteDirRemFileStart = sLine

    End Function
	Function WriteDirRemPaymentOne(ByRef oPayment As Payment, ByRef iTransactionNo As Double) As String
		
		Dim sLine As String
		Dim sPayCode As String
		
		
		' Add to Batch-totals:
		BatchSumAmount = BatchSumAmount + oPayment.MON_InvoiceAmount
		BatchNoRecords = BatchNoRecords + 2 'both 30 and 31, payment1 and 2
		BatchNoTransactions = BatchNoTransactions + 1
		
		
		If StringToDate((oPayment.DATE_Payment)) > dBatchLastDate Then
			dBatchLastDate = StringToDate((oPayment.DATE_Payment))
		End If
		If StringToDate((oPayment.DATE_Payment)) < dBatchFirstDate Then
			dBatchFirstDate = StringToDate((oPayment.DATE_Payment))
		End If
		If StringToDate((oPayment.DATE_Payment)) < dFileFirstDate Then
			dFileFirstDate = StringToDate((oPayment.DATE_Payment))
		End If
		
		sLine = "NY04" ' dirrem always 04
		' transtype from Babelbanks internal set of codes
		
		sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "DIRREM")
		' 15.04.2009 Special situation with giropayment (no account): Infiles has 04, returnfiles from BBS has 05 !!!!
		If sPayCode = "04" And Val(oPayment.StatusCode) > 0 Then
			sPayCode = "05"
		End If
		sLine = sLine & sPayCode
		sLine = sLine & "30"
		sLine = sLine & PadLeft(Str(iTransactionNo), 7, "0") ' transnr
		
		If (StringToDate((oPayment.DATE_Payment)) > (System.Date.FromOADate(Now.ToOADate - 100))) And (StringToDate((oPayment.DATE_Payment)) < (System.Date.FromOADate(Now.ToOADate + 365))) Then
			' if we have a legal paymentdate, use it
            sLine = sLine & VB6.Format(StringToDate((oPayment.DATE_Payment)), "ddmmyy")
        Else
            ' no legal paymentdate - use todays date
            sLine = sLine & VB6.Format(Now, "ddmmyy")
        End If
        ' Changed 12.01.06
        'If sPayCode = "160" Then
        If oPayment.PayCode = "160" Then
            ' Changed 12.01.06
            sLine = sLine & PadRight(oPayment.REF_Bank1, 11, " ") ' no receivers account, use Gironumber
        Else
            ' we know the an accountno for receiver
            sLine = sLine & PadLeft(oPayment.E_Account, 11, "0")
        End If
        sLine = sLine & PadLeft(Str(oPayment.MON_InvoiceAmount), 17, "0")
        If sPayCode = "12" Then
            ' if 12, only one kid for each payment
            ' if 16 or 17, kid in several underspesifications
            'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.Invoices().Unique_Id. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sLine = sLine & PadLeft(oPayment.Invoices(1).Unique_Id, 25, " ")
        Else
            ' Either no KID, og 16 or 17 with KID in underspesification(s)
            sLine = sLine & New String(" ", 25)
        End If
        sLine = PadLine(sLine, 80, "0") ' filler

        WriteDirRemPaymentOne = sLine

    End Function
    Function WriteDirRemPaymentTwo(ByRef oPayment As Payment, ByRef iTransactionNo As Double) As String

        Dim sLine As String
        Dim sPayCode As String

        sLine = "NY04"
        ' transtype from Babelbanks internal set of codes
        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "DIRREM")
        sLine = sLine & sPayCode
        sLine = sLine & "31"
        sLine = sLine & PadLeft(Str(iTransactionNo), 7, "0") ' transnr
        sLine = sLine & PadRight(Left(oPayment.E_Name, 10), 10, " ") 'shortname
        If oPayment.Invoices.Count > 0 Then
            sLine = sLine & PadRightWithoutTrim(oPayment.Invoices.Item(1).REF_Own, 25, " ") 'Egenref
        Else
            sLine = sLine & Space(25) 'Egenref
        End If
        sLine = sLine & PadRight(oPayment.Text_E_Statement, 25, " ") 'fremmedref, 51-75
        sLine = PadLine(sLine, 80, "0") ' filler

        WriteDirRemPaymentTwo = sLine

    End Function
    Function WriteDirRemAdressOne(ByRef oPayment As Payment, ByRef iTransactionNo As Double) As String

        Dim sLine As String
        Dim sPayCode As String

        BatchNoRecords = BatchNoRecords + 1
        sLine = "NY04"
        ' transtype from Babelbanks internal set of codes
        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "DIRREM")
        sLine = sLine & sPayCode ' either 03 or 04
        sLine = sLine & "40"
        sLine = sLine & PadLeft(Str(iTransactionNo), 7, "0") ' transnr

        sLine = sLine & PadRight(oPayment.E_Name, 30, " ") 'longname
        sLine = sLine & Left(oPayment.E_Zip, 4)
        sLine = sLine & New String(" ", 3)
        sLine = sLine & PadRight(oPayment.E_City, 25, " ")
        sLine = PadLine(sLine, 80, "0") ' filler

        WriteDirRemAdressOne = sLine

    End Function
    Function WriteDirRemAdressTwo(ByRef oPayment As Payment, ByRef iTransactionNo As Double) As String

        Dim sLine As String
        Dim sPayCode As String
        BatchNoRecords = BatchNoRecords + 1
        sLine = "NY04"
        ' transtype from Babelbanks internal set of codes
        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "DIRREM")
        sLine = sLine & sPayCode ' either 03 or 04
        sLine = sLine & "41"
        sLine = sLine & PadLeft(Str(iTransactionNo), 7, "0") ' transnr

        sLine = sLine & PadRight(oPayment.E_Adr1, 30, " ")
        sLine = sLine & PadRight(oPayment.E_Adr2, 30, " ")
        sLine = sLine & PadRight(oPayment.E_CountryCode, 3, " ")
        sLine = PadLine(sLine, 80, "0") ' filler

        WriteDirRemAdressTwo = sLine

    End Function
    Function WriteDirRemSpesifikasjon(ByRef oPayment As Payment, ByRef oFreeText As Freetext, ByRef iTransactionNo As Double, ByRef nCol As Short, ByRef nLine As Short) As String

        Dim sLine As String
        Dim sPayCode As String
        BatchNoRecords = BatchNoRecords + 1

        sLine = "NY04"
        ' transtype from Babelbanks internal set of codes
        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "DIRREM")
        sLine = sLine & sPayCode
        sLine = sLine & "49"
        sLine = sLine & PadLeft(Str(iTransactionNo), 7, "0") ' transnr

        ' New 19.09.05 - Added line and col-pos from DirRem-import, stored in .row and .col
        If oFreeText.Row > 0 And oFreeText.Col > 0 Then
            sLine = sLine & PadLeft(Str(oFreeText.Row), 3, "0")
            sLine = sLine & Trim(Str(oFreeText.Col))
        Else
            sLine = sLine & PadLeft(Str(nLine), 3, "0")
            sLine = sLine & Trim(Str(nCol))
        End If
        sLine = sLine & Left(oFreeText.Text & New String(" ", 40), 40) ' ikke bruk padright, da den ogs� trimmer ledende blanke!
        sLine = PadLine(sLine, 80, "0") ' filler

        WriteDirRemSpesifikasjon = sLine

    End Function

    Function WriteDirRemUnderSpesifikasjon(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef iTransactionNo As Double) As String

        Dim sLine As String
        Dim sPayCode As String
        BatchNoRecords = BatchNoRecords + 1

        sLine = "NY04"
        ' transtype from Babelbanks internal set of codes
        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "DIRREM")
        If oInvoice.MON_InvoiceAmount >= 0 Then
            ' kid
            sLine = sLine & "16"
        Else
            ' kreditnota
            sLine = sLine & "17"
        End If
        sLine = sLine & "50"
        sLine = sLine & PadLeft(Str(iTransactionNo), 7, "0") ' transnr

        If oInvoice.MON_InvoiceAmount >= 0 Then
            ' kid
            sLine = sLine & PadLeft(oInvoice.Unique_Id, 25, " ")
            sLine = sLine & PadLeft(Str(oInvoice.MON_InvoiceAmount), 17, "0")
        Else ' kreditnota
            ' changes 17.11.2017 - if KID, use it
            If Not EmptyString(oInvoice.Unique_Id) Then
                sLine = sLine & PadLeft(oInvoice.Unique_Id, 25, " ")
            Else
                If oInvoice.Freetexts.Count > 0 Then
                    sLine = sLine & PadLeft(oInvoice.Freetexts(1).Text, 25, " ")
                Else
                    sLine = sLine & Space(25)
                End If
            End If
        sLine = sLine & PadLeft(Str(oInvoice.MON_InvoiceAmount * -1), 17, "0")
        End If


        sLine = PadLine(sLine, 80, "0") ' filler

        WriteDirRemUnderSpesifikasjon = sLine

    End Function
    Function WriteDirRemFileEnd() As String '(oBabelFile As BabelFile) As String
        ' ok jp
        Dim sLine As String
        nFileNoRecords = nFileNoRecords + 1 ' also count 89-recs

        sLine = "NY000089"

        sLine = sLine & PadLeft(Str(nFileNoTransactions), 8, "0") ' No of transactions in file (bel�pspost1 + 2 = 1)
        sLine = sLine & PadLeft(Str(nFileNoRecords), 8, "0") ' No of records in file (including 20 and 88)
        sLine = sLine & PadLeft(Str(nFileSumAmount), 17, "0") ' Total amount in file
        sLine = sLine & VB6.Format(dFileFirstDate, "ddmmyy") 'First date in file

        ' pad with 0 to 80
        sLine = PadLine(sLine, 80, "0")

        WriteDirRemFileEnd = sLine

    End Function
    Function WriteDirRemBatchStart(ByRef oBatch As Batch, ByRef oPayment As Payment) As String
        ' 07.06.2010 added oPayment as parameter, and used oPayment.I_Account further down

        Dim sLine As String
        ' Reset Batch-totals:
        BatchSumAmount = 0
        BatchNoRecords = 1
        BatchNoTransactions = 0
        dBatchLastDate = System.Date.FromOADate(Now.ToOADate - 1000)
        dBatchFirstDate = System.Date.FromOADate(Now.ToOADate + 1000)

        sLine = "NY040020"
        sLine = sLine & PadLeft(Mid(oBatch.REF_Own, 1, 9), 9, "0") ' avtaleid
        sLine = sLine & PadLeft(Mid(oBatch.Batch_ID, 1, 7), 7, "0") ' oppdragsnr

        'sLine = sLine & PadLeft(Mid(oBatch.Payments(1).I_Account, 1, 11), 11, "0") ' oppdragskonto
        ' changed 07.06.2010
        sLine = sLine & PadLeft(Mid(oPayment.I_Account, 1, 11), 11, "0") ' oppdragskonto

        sLine = PadLine(sLine, 80, "0")
        WriteDirRemBatchStart = sLine

    End Function

    Function WriteDirRemBatchEnd() As String '(oBatch) As String
        'ok jps

        Dim sLine As String

        BatchNoRecords = BatchNoRecords + 1 'Also count 88-recs

        ' Add to File-totals:
        nFileSumAmount = nFileSumAmount + BatchSumAmount
        nFileNoRecords = nFileNoRecords + BatchNoRecords
        nFileNoTransactions = nFileNoTransactions + BatchNoTransactions

        sLine = "NY040088"

        sLine = sLine & PadLeft(Str(BatchNoTransactions), 8, "0") ' No of transactions in batch (bel�pspost1 + 2 = 1)
        sLine = sLine & PadLeft(Str(BatchNoRecords), 8, "0") ' No of records in batch (including 20 and 88)
        sLine = sLine & PadLeft(Str(BatchSumAmount), 17, "0") ' Total amount in batch
        sLine = sLine & VB6.Format(dBatchFirstDate, "ddmmyy") ' First date in batch,DDMMYY
        sLine = sLine & VB6.Format(dBatchLastDate, "ddmmyy") ' First date in batch, DDMMYY
        sLine = PadLine(sLine, 80, "0")
        WriteDirRemBatchEnd = sLine

    End Function
End Module
