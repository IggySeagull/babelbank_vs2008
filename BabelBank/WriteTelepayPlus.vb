Option Strict Off
Option Explicit On
Module WriteTelepayPlus
    Dim oFs As New Scripting.FileSystemObject
    Dim oFile As Scripting.TextStream
    Dim sLine As String ' en output-linje

    Private sI_EnterpriseNo As String
    Private sTransDate As String
    Private bDomestic As Boolean
    Private bCurrentIsTKIO As Boolean
    Private bLastWasTKIO As Boolean
    'This variable show if the importfile we are working with is created from the
    ' accounting system, or is a returnfile. The variable is set during reading the
    ' BabelFile-object.
    Private bFileFromBank As Boolean
    '29.07.2008 - Changed the variable ClientNumber from Integer to string
    Private sClientNumber As String, sImportFilename As String
    'Old Code
    'Private nClientNumber As Integer, sImportFilename As String
    Private iStructuredPayment As Integer 'added 02.05.06
    Private iNoOfBETFOR22_23 As Integer 'added 03.05.06
    Private iNoOfTextLinesInBETFOR23 As Integer 'added 03.05.06

    Function WriteTelepayPlusFile(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal iFilenameInNo As Integer, ByVal sCompanyNo As String, ByVal sBranch As String, ByVal sVersion As String, ByVal SurroundWith0099 As Boolean, ByVal bUnix As Boolean, ByVal nSequenceNumberStart As Double, ByVal nSequenceNumberEnd As Double, ByVal sClientNo As String, ByVal sSpecial As String, ByVal eBank As BabelFiles.Bank, ByVal iFiletype As BabelFiles.FileType, ByVal bExportInternationalsOnly As Boolean) As Boolean

        Dim i As Double, j As Double, k As Double
        Dim bFirstLine As Boolean
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oClient As Client
        Dim oaccount As vbBabel.Account
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim sNewOwnref As String
        Dim bWriteBETFOR00 As Boolean 'Used with the sequencenumbering
        Dim bWriteBETFOR99Later As Boolean 'Used with the sequencenumbering
        Dim bLastBatchWasDomestic As Boolean 'Used to check if we change to/from domestic/intenational
        Dim bFirstBatch As Boolean 'True until we write the first BETFOR00
        Dim bFirstMassTransaction As Boolean, bWriteOK As Boolean
        Dim iCount As Integer
        Dim nNoOfBETFOR As Double, nSequenceNoTotal As Double
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim nClientNo As Double, bFoundClient As Boolean
        Dim xAccount As String
        Dim bExportMassRecords As Boolean ' New 25.10.01 by janp - default: No masstrans for returnfiles
        Dim bLastPaymentWasMass As Boolean
        Dim sDummy As String
        Dim nInvoiceCounter As Double
        Dim iFilenameCounter As Integer

        'To use with summarizing masspayment
        Dim oMassPayment As Payment, oMassInvoice As Invoice, bSumMassPayment As Boolean
        Dim nMassTransferredAmount As Double, sPayment_ID As String, lMassCounter As Long
        Dim bContinueSummarize As Boolean
        ' XokNET 19.07.2011
        Dim sOldAccount As String
        ' XOKNET 20.09.2012
        Dim iFirstRealPaymentInBatch As Integer
        ' XOKNET 04.02.2013
        Dim sOldDate As String
        ' XNET 27.11.2013
        Dim iMaxNoOfInvoiceRecords As Integer
        Dim iNoOfProcessedInvoiceRecords As Integer

        Try

            oFs = New Scripting.FileSystemObject
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            nInvoiceCounter = 0
            iFilenameCounter = 2  ' Next file is f.ex. Tiltb.2

            bExportoBabel = False
            bWriteBETFOR00 = True
            bWriteBETFOR99Later = False
            bFirstBatch = True
            nSequenceNoTotal = 1  'new by JanP 25.10.01

            For Each oBabel In oBabelFiles

                If oBabel.VB_FilenameInNo = iFilenameInNo Then

                    bExportoBabel = False
                    'Have to go through each batch-object to see if we have objects that shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then

                                ' XOKNET 20.09.2012 Major change
                                ' Have problems when mix of international and domestic. Instead of k�dding with batches, simply export Int in one run, and Dom in next run
                                ' Then, added next If/Else
                                If (bExportInternationalsOnly = True And oPayment.PayType <> "I") Or (bExportInternationalsOnly = False And oPayment.PayType = "I") Then
                                    ' do NOT export
                                    bExportoBabel = False
                                Else
                                    ' XOKNET 29.02.2012 - Major change!
                                    ' DNB cannot treat debits from Norwegian accounts as TelepayPlus
                                    ' Then, we have to produce "normal" Telepayfiles for those
                                    ' Can be deleivered in same physical file as TelepayPlus
                                    ' Thus, added next If
                                    ' 05.04.2019 - LocalLow and LocalHigh Australia is moved to Norway, and must be treated as International
                                    If oPayment.I_CountryCode = "NO" And IsIPBAustralia(oPayment) = False And IsIPBSingapore(oPayment) = False Then
                                        bExportoBabel = False
                                    Else
                                        If Not bMultiFiles Then
                                            bExportoBabel = True
                                        Else
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = vbNullString
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoBabel = True
                                                End If
                                            End If
                                        End If
                                    End If

                                    ' added 28.02.2019
                                    ' NHST_LONN for Singapore will be exported to an OCBC bank file
                                    If sSpecial = "NHST_LONN" And oPayment.VoucherType = "OCB" And oPayment.I_CountryCode = "SG" Then
                                        bExportoBabel = False
                                    End If
                                End If
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If
                        Next
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next


                    If bExportoBabel Then
                        'Set if the file was created int the accounting system or not.
                        bFileFromBank = oBabel.FileFromBank
                        sImportFilename = oBabel.FilenameIn
                        'i = 0 'FIXED Moved by Janp under case 2


                        'Loop through all Batch objs. in BabelFile obj.
                        For Each oBatch In oBabel.Batches
                            bExportoBatch = False
                            ' XOKNET 20.09.2012 - must use this value further down - not be dependant upon 1 payment in batch
                            iFirstRealPaymentInBatch = 1

                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile
                            For Each oPayment In oBatch.Payments
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    ' XOKNET 20.09.2012 Major change
                                    ' Have problems when mix of international and domestic. Instead of k�dding with batches, simply export Int in one run, and Dom in next run
                                    ' Then, added next If/Else
                                    If (bExportInternationalsOnly = True And oPayment.PayType <> "I") Or (bExportInternationalsOnly = False And oPayment.PayType = "I") Then
                                        ' do NOT export
                                        bExportoBatch = False
                                    Else

                                        ' XOKNET 29.02.2012 - Major change!
                                        ' DNB cannot treat debits from Norwegian accounts as TelepayPlus
                                        ' Then, we have to produce "normal" Telepayfiles for those
                                        ' Can be deleivered in same physical file as TelepayPlus
                                        ' Thus, added next If
                                        ' 05.04.2019 - LocalLow and LocalHigh Australia is moved to Norway, and must be treated as International
                                        If oPayment.I_CountryCode = "NO" And IsIPBAustralia(oPayment) = False And IsIPBSingapore(oPayment) = False Then
                                            bExportoBatch = False
                                        Else
                                            If Not bMultiFiles Then
                                                bExportoBatch = True
                                                ' XOKNET 20.09.2012 - must use this value further down - not be dependant upon 1 payment in batch
                                                iFirstRealPaymentInBatch = oPayment.Index
                                            Else
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    If InStr(oPayment.REF_Own, "&?") Then
                                                        'Set in the part of the OwnRef that BabelBank is using
                                                        sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                    Else
                                                        sNewOwnref = vbNullString
                                                    End If
                                                    If sNewOwnref = sOwnRef Then
                                                        bExportoBatch = True
                                                        ' XOKNET 20.09.2012 - must use this value further down - not be dependant upon 1 payment in batch
                                                        iFirstRealPaymentInBatch = oPayment.Index

                                                    End If
                                                End If
                                            End If
                                        End If
                                        ' added 28.02.2019
                                        ' NHST_LONN for Singapore will be exported to an OCBC bank file
                                        If sSpecial = "NHST_LONN" And oPayment.VoucherType = "OCB" And oPayment.I_CountryCode = "SG" Then
                                            bExportoBatch = False
                                        End If

                                    End If
                                End If
                                ' XOKNET 31.01.2012 New If
                                ' XOKNET 23.10.2012 - Commented next If as NHST will use TelepayPlus for all payments
                                '                    If sSpecial = "NHST_LONN" Then
                                '                        ' check if any payments in this batch is for TP+
                                '                        If oPayment.VoucherType <> "TP+" Then
                                '                            bExportoBatch = False
                                '                        End If
                                '                    End If
                                'If true exit the loop, and we have data to export
                                If bExportoBatch Then
                                    Exit For
                                End If
                            Next


                            If bExportoBatch Then

                                ' New by Janp 25.10.01
                                ' Optional if Mass-trans will be exported for returnfiles.
                                ' Default = False
                                ' In filesetup, test ReturnMass
                                If oBatch.VB_ProfileInUse = True Then
                                    If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True Then
                                        bExportMassRecords = oBatch.VB_Profile.FileSetups(iFormat_ID).ReturnMass
                                    Else
                                        bExportMassRecords = True
                                    End If
                                Else
                                    bExportMassRecords = True
                                End If


                                ' New by JanP 25.10.01
                                If Not (bExportMassRecords = False And (oPayment.PayType = "M" Or oPayment.PayType = "S")) Then
                                    ' Don't put mass-payments into returnfiles (unless user has asked for it)
                                    If oBatch.VB_ProfileInUse Then
                                        If bFirstBatch Or oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch = True Then
                                            ' First batch in file, then write betfor00
                                            bWriteBETFOR00 = True
                                        Else
                                            ' Not first batch, and masstrans, don't write betfor00
                                            bWriteBETFOR00 = False
                                        End If
                                    Else
                                        If bFirstBatch Then
                                            bWriteBETFOR00 = True
                                        Else
                                            bWriteBETFOR00 = False
                                        End If
                                    End If

                                Else
                                    ' No export of mass-trans, but check if first betfor00:
                                    If bFirstBatch Then
                                        ' First batch in file, then write betfor00
                                        bWriteBETFOR00 = True
                                    Else
                                        ' Not first batch, and masstrans, don't write betfor00
                                        bWriteBETFOR00 = False
                                    End If
                                End If

                                ' XOKNET 08.02.2012 - for NHST_LONN, TP+, find Divisjon in setup, divisjon
                                If sSpecial = "NHST_LONN" Then
                                    sBranch = oBatch.VB_Profile.FileSetups(iFormat_ID).Division
                                End If

                                'Check if we shall use the original sequenceno. or not
                                'First check if we use a profile
                                If oBatch.VB_ProfileInUse = True Then

                                    'Then if it is a return-file
                                    If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True Then
                                        bFirstLine = True
                                    Else
                                        Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                            'Use the sequenceno from oBabel
                                            Case 0
                                                If oBatch.SequenceNoStart = -1 Then
                                                    'FIXERROR: Babelerror No sequenceno imported
                                                Else
                                                    nSequenceNoTotal = oBatch.SequenceNoStart
                                                    ' New by JanP 25.10.01
                                                    ' If we have a file with several betfor00/99, then we must
                                                    ' check if any betfor99 is pending, and deduct 1 from sequence, for the
                                                    ' pending betfor99 (iSeqenceNoTotal is added 1 when the pending
                                                    ' BETFOR99 is written;
                                                    If bWriteBETFOR99Later Then
                                                        nSequenceNoTotal = nSequenceNoTotal - 1
                                                    End If
                                                End If
                                                'Use the seqno at filesetup level
                                            Case 1
                                                'If it isn't the first client, just keep on counting.
                                                If bWriteBETFOR00 Then
                                                    ' New by JanP 24.10.01
                                                    If bFirstBatch Then
                                                        'Only for the first batch if we have one sequenceseries!
                                                        nSequenceNoTotal = oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal + 1
                                                        If nSequenceNoTotal > 9999 Then
                                                            nSequenceNoTotal = 0
                                                        End If
                                                    End If
                                                End If
                                                'Use seqno per client per filesetup
                                            Case 2
                                                bFoundClient = False
                                                i = 0 ' New by Janp 25.10.01 Sets how day sequence will be counted

                                                'Normal situation
                                                For Each oClient In oBatch.VB_Profile.FileSetups(iFormat_ID).Clients
                                                    For Each oaccount In oClient.Accounts
                                                        ' XOKNET 20.09.2012 - must use this value further down - not be dependant upon 1 payment in batch
                                                        'If oaccount.Account = oBatch.Payments(1).I_Account Then
                                                        If oaccount.Account = oBatch.Payments(iFirstRealPaymentInBatch).I_Account Then
                                                            nSequenceNoTotal = oClient.SeqNoTotal + 1
                                                            If nSequenceNoTotal > 9999 Then
                                                                nSequenceNoTotal = 0
                                                            End If
                                                            nClientNo = oClient.Index
                                                            '29.07.2008 - Changed the variable ClientNumber from Integer to string
                                                            sClientNumber = oClient.ClientNo
                                                            bFoundClient = True
                                                            Exit For
                                                        End If
                                                    Next
                                                    If bFoundClient Then
                                                        Exit For
                                                    End If
                                                Next
                                        End Select
                                    End If
                                Else
                                    'New 07.06.2006
                                    nSequenceNoTotal = nSequenceNumberStart + 1
                                    If nSequenceNoTotal > 9999 Then
                                        nSequenceNoTotal = 0
                                    End If
                                    bFirstLine = True  'new by JanP 25.10.01
                                End If


                                'Used in every applicationheader, TBII = Domestic, TBIU = International.
                                ' XOKNET 02.02.2012 - changes due to DNB now can take TelepayPlus for all countries
                                '                    If oBatch.Payments.Count = 0 Then
                                '                        bDomestic = True
                                '                    ElseIf Not oBatch.Payments(1).BANK_I_Domestic Then
                                '                        bDomestic = False
                                '                        bCurrentIsTKIO = True
                                '                    ElseIf oBatch.Payments(1).PayType = "I" Then
                                '                        bDomestic = False
                                '                         bCurrentIsTKIO = False
                                '                    Else
                                '                        bDomestic = True
                                '                         bCurrentIsTKIO = False
                                '                    End If
                                If oBatch.Payments.Count = 0 Then
                                    bDomestic = True
                                    ' XOKNET 20.09.2012 - must use this value further down - not be dependant upon 1 payment in batch
                                    'ElseIf oBatch.Payments(1).BANK_I_Domestic Then
                                ElseIf oBatch.Payments(iFirstRealPaymentInBatch).BANK_I_Domestic Then
                                    bDomestic = True
                                Else
                                    ' XOKNET 27.03.2012, added If
                                    If oPayment.PayType = "S" Then
                                        bDomestic = True
                                    Else
                                        bDomestic = False
                                    End If
                                End If
                                ' XOKNET 02.02.2012 - changes due to DNB now can take TelepayPlus for all countries
                                ' TKIO only when debet swift is not DNB

                                ' XOKNET 20.09.2012 - must use this value further down - not be dependant upon 1 payment in batch
                                'If Left$(oBatch.Payments(1).BANK_I_SWIFTCode, 3) <> "DNB" Then
                                If Left$(oBatch.Payments(iFirstRealPaymentInBatch).BANK_I_SWIFTCode, 3) <> "DNB" Then
                                    bCurrentIsTKIO = True
                                Else
                                    bCurrentIsTKIO = False
                                End If


                                'Check if it is a change between domestic and international payment, and if
                                ' the bWriteBETFOR99Later flag is true (indicates that KeepBatch is set to false in
                                ' the database
                                If bLastBatchWasDomestic <> bDomestic Then
                                    If bWriteBETFOR99Later Then
                                        bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                        nNoOfBETFOR = 0
                                    End If
                                    ' New by JanP 25.10.01
                                    If Not (bExportMassRecords = False And (oPayment.PayType = "M" Or oPayment.PayType = "S")) Then
                                        bWriteBETFOR00 = True
                                        bWriteBETFOR99Later = False
                                    Else
                                        'bWriteBETFOR00 = False  'Keep as is
                                        bWriteBETFOR99Later = False
                                    End If
                                End If

                                ' New by JanP 29.10.01
                                If oBatch.Payments.Count = 0 Then
                                    bLastBatchWasDomestic = True
                                    ' XOKNET 20.09.2012 - must use this value further down - not be dependant upon 1 payment in batch
                                    'ElseIf oBatch.Payments(1).PayType = "I" Then
                                ElseIf oBatch.Payments(iFirstRealPaymentInBatch).PayType = "I" Then
                                    bLastBatchWasDomestic = False
                                Else
                                    bLastBatchWasDomestic = True
                                End If



                                'Used in every applicationheader
                                If Not oBatch.DATE_Production = "19900101" Then
                                    sTransDate = Right$(oBatch.DATE_Production, 4) '(oBatch.DATE_Production, "MMDD")
                                Else
                                    'If not imported set like computerdate
                                    sTransDate = Format(Now, "MMDD")
                                End If
                                'bFirstLine is used to set the sequenceno in the aplicationheader. If true seqno=1
                                'bFirstLine = False
                                'bFirstMassTransaction is used as a flag to treat the first mass transaction specially
                                'We have to create a BETFOR21-transaction before the first mass transaction (BETFOR22).
                                bFirstMassTransaction = True

                                'Reset count of BETFOR-records
                                If bWriteBETFOR00 Then
                                    nNoOfBETFOR = 0
                                    If oBatch.VB_ProfileInUse Then
                                        If oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType = 1 Or _
                                            oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem Then
                                            ' JanP 25.10.01 added ....FromAccountingSystem=True
                                            If bFirstBatch Then
                                                bFirstLine = True
                                            Else
                                                ' Keep on with daysequence until file end
                                                bFirstLine = False
                                            End If
                                        Else
                                            ' Reset day sequence for each betfor00
                                            bFirstLine = True
                                        End If
                                    Else
                                        If bFirstBatch Then
                                            bFirstLine = True
                                        Else
                                            ' Keep on with daysequence until file end
                                            bFirstLine = False
                                        End If
                                    End If
                                End If
                                i = i + 1

                                'Get the companyno either from a property passed from BabelExport, or
                                ' from the imported file
                                If sCompanyNo <> vbNullString Then
                                    sI_EnterpriseNo = PadLeft(sCompanyNo, 11, "0")
                                Else
                                    sI_EnterpriseNo = PadLeft(oBatch.I_EnterpriseNo, 11, "0")
                                End If
                                'BETFOR00

                                If bWriteBETFOR00 Then
                                    nNoOfBETFOR = 0
                                    sLine = WriteBETFOR00(oBatch, bFirstLine, nSequenceNoTotal, sBranch, sCompanyNo, sSpecial)
                                    bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                    bFirstBatch = False
                                    ' XOKNET 16.12.2011 - added next, must reset after BETFOR00
                                    bFirstMassTransaction = True

                                    ' XOKNET 20.09.2012 - must use this value further down - not be dependant upon 1 payment in batch
                                    'xAccount = oBatch.Payments(1).I_Account  ' Spec. for 0099-format
                                    xAccount = oBatch.Payments(iFirstRealPaymentInBatch).I_Account  ' Spec. for 0099-format

                                End If
                                j = 0
                                k = 0
                                lMassCounter = 1

                                For Each oPayment In oBatch.Payments

                                    ' XNET 27.11.2013
                                    ' Created a new "splitsolution" where we first establish how many BETFOR04/23 we can have in each BETFOR01/21
                                    ' The number is based on type of payment, based on rules received by H�kon Belt, DNB, NOv 26th 2013
                                    iMaxNoOfInvoiceRecords = CalculateMaxNoOfInvoices(oPayment)

                                    If Not RunTime() Then
                                        ' test for NHST
                                        If oPayment.E_Name = "Lee E Denslow" Then
                                            j = j
                                        End If
                                    End If

                                    '---------------------------------------
                                    ' Special for betfor0099-format;
                                    ' Surround each account with BETFOR00/99
                                    ' Ref NorgesGruppen
                                    '---------------------------------------
                                    If SurroundWith0099 Then
                                        If oPayment.I_Account <> xAccount Then
                                            ' 04.07.05: Changed from  > 0, to awoid Records with only BETFOR00/BETFOR99
                                            'If nNoOfBETFOR > 0 Then
                                            If nNoOfBETFOR > 1 Then
                                                sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion)
                                                bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                nNoOfBETFOR = 0

                                                sLine = WriteBETFOR00(oBatch, bFirstLine, nSequenceNoTotal, sBranch, sCompanyNo, sSpecial, oPayment.VB_ClientNo)
                                                bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            End If
                                            xAccount = oPayment.I_Account  ' Spec. for 0099-format
                                        End If
                                    End If

                                    '-------------------------------------------------------------
                                    ' Test if it is a TKIO-payment or not, and
                                    ' whether there is a change between TKIO and ordinary Telepay
                                    ' XOKNET 02.02.2012 - only outside DNB is TKIO
                                    'If oPayment.BANK_I_Domestic Then
                                    '    bCurrentIsTKIO = False
                                    'Else
                                    '    bCurrentIsTKIO = True
                                    'End If
                                    If Left$(oPayment.BANK_I_SWIFTCode, 3) <> "DNB" Then
                                        bCurrentIsTKIO = True
                                    Else
                                        bCurrentIsTKIO = False
                                    End If

                                    ' added 22.06.2022
                                    If sSpecial = "NHST_LONN" And oPayment.VoucherType = "OCB" And oPayment.I_CountryCode = "SG" Then
                                        bCurrentIsTKIO = False
                                    End If
                                    ' added 22.06.2022
                                    ' NHST_LONN for US will be exported in Nacha file
                                    If sSpecial = "NHST_LONN" And oPayment.I_CountryCode = "US" Then
                                        bCurrentIsTKIO = False
                                    End If


                                    ' Check if there is a change between TKIO and Telepay
                                    If bCurrentIsTKIO <> bLastWasTKIO Then
                                        ' Change, must write BETFOR99/00
                                        ' 04.07.05: Changed from  > 0, to awoid Records with only BETFOR00/BETFOR99
                                        If nNoOfBETFOR > 1 Then
                                            sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion)
                                            bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            nNoOfBETFOR = 0

                                            ' XOKNET 30.01.2012 - do not write BETFOR00 before we know we have payments !!!
                                            '---------sLine = WriteBETFOR00(oBatch, bFirstLine, nSequenceNoTotal, sBranch, sCompanyNo, sSpecial)
                                            '-------------bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            bWriteBETFOR00 = True

                                            ' XOKNET 16.12.2011 - added next, must reset after BETFOR00
                                            bFirstMassTransaction = True


                                        End If
                                    End If
                                    bLastWasTKIO = bCurrentIsTKIO
                                    '---------------------------------------------------------------

                                    bExportoPayment = False
                                    bLastPaymentWasMass = False

                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    ' added 16.12.2008 - do not export payments with Exported = True
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                        ' XOKNET 20.09.2012 Major change
                                        ' Have problems when mix of international and domestic. Instead of k�dding with batches, simply export Int in one run, and Dom in next run
                                        ' Then, added next If/Else
                                        If (bExportInternationalsOnly = True And oPayment.PayType <> "I") Or (bExportInternationalsOnly = False And oPayment.PayType = "I") Then
                                            ' do NOT export
                                            bExportoPayment = False
                                        Else

                                            ' XOKNET 29.02.2012 - Major change!
                                            ' DNB cannot treat debits from Norwegian accounts as TelepayPlus
                                            ' Then, we have to produce "normal" Telepayfiles for those
                                            ' Can be deleivered in same physical file as TelepayPlus
                                            ' Thus, added next If
                                            ' 05.04.2019 - LocalLow and LocalHigh Australia is moved to Norway, and must be treated as International
                                            If oPayment.I_CountryCode = "NO" And IsIPBAustralia(oPayment) = False And IsIPBSingapore(oPayment) = False Then
                                                bExportoPayment = False
                                            Else

                                                If Not bMultiFiles Then
                                                    bExportoPayment = True
                                                Else
                                                    If oPayment.VB_ClientNo = sClientNo Then
                                                        If InStr(oPayment.REF_Own, "&?") Then
                                                            'Set in the part of the OwnRef that BabelBank is using
                                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                        Else
                                                            sNewOwnref = vbNullString
                                                        End If
                                                        If sNewOwnref = sOwnRef Then
                                                            bExportoPayment = True
                                                        End If
                                                    End If
                                                End If
                                            End If
                                            ' added 28.02.2019
                                            ' NHST_LONN for Singapore will be exported to an OCBC bank file
                                            If sSpecial = "NHST_LONN" And oPayment.VoucherType = "OCB" And oPayment.I_CountryCode = "SG" Then
                                                bExportoPayment = False
                                                bCurrentIsTKIO = bLastWasTKIO
                                            End If
                                            ' added 22.06.2022
                                            ' NHST_LONN for US will be exported in Nacha file
                                            If sSpecial = "NHST_LONN" And oPayment.I_CountryCode = "US" Then
                                                bExportoPayment = False
                                                bCurrentIsTKIO = bLastWasTKIO
                                            End If

                                        End If
                                    End If


                                    If bExportoPayment Then
                                        j = j + 1


                                        ' 29.11.2013 - Moved the "For/Next" Invoice-loop here
                                        ' This loop takes care of handling a max no of BETFOR04/23
                                        ' Loops up here again when max no of BETFOR04/23 has been reached for a payment
                                        ' but we still have more invoices to export. Then we must create a new
                                        ' BETFOR01/21, and then more BETFOR04/23.

                                        iNoOfProcessedInvoiceRecords = 0

                                        For Each oInvoice In oPayment.Invoices
                                            If iNoOfProcessedInvoiceRecords = 0 Then
                                                'Test what sort of transaction
                                                If oPayment.PayType = "I" Or oPayment.BANK_I_Domestic = False Then
                                                    'International payment, or TKIO-payment
                                                    'XOKNET 18.11.2010 - added oBatch to parameters
                                                    sLine = WriteBETFOR01(oBatch, oPayment, nSequenceNoTotal, sCompanyNo, iFormat_ID, sSpecial)
                                                    bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                    sLine = WriteBETFOR02(oPayment, nSequenceNoTotal, sCompanyNo)
                                                    bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                    sLine = WriteBETFOR03(oPayment, nSequenceNoTotal, sCompanyNo)
                                                    bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                    'Remove later: ExportCheck
                                                    oPayment.Exported = True
                                                    'Have to reset masstrans flag
                                                    bFirstMassTransaction = True
                                                ElseIf oPayment.PayType = "M" Or oPayment.PayType = "S" Then
                                                    If oPayment.Payment_ID <> sPayment_ID Then
                                                        bFirstMassTransaction = True
                                                    End If

                                                    'XOKNET Major error corrected 19.07.2011
                                                    ' If we have masstransfers, and more than one debitaccount, they will all be charged the
                                                    ' same debitaccount !!! No new BETFOR21 was created
                                                    ' Added next If:
                                                    If oPayment.I_Account <> sOldAccount Then
                                                        bFirstMassTransaction = True
                                                        sOldAccount = oPayment.I_Account
                                                    End If
                                                    ' XOKNET 04.02.2013 - change for Masstransactions, must also test if date changes!
                                                    If oPayment.DATE_Payment <> sOldDate Then
                                                        bFirstMassTransaction = True
                                                        sOldDate = oPayment.DATE_Payment
                                                    End If

                                                    If bFirstMassTransaction Then
                                                        'New by JanP 25.10.01 - Option to turn off export of masstranser for returnfiles
                                                        If bExportMassRecords Then
                                                            'New code 10.07.2002
                                                            'Have to sumarize alle invoices/payments, to put the
                                                            'amount in Betfor21.
                                                            nMassTransferredAmount = 0
                                                            'Assumes that all BETFOR22 records in the imported file within
                                                            ' a BETFOR21 has the same PAYMENT_ID (also Invoice_ID).
                                                            sPayment_ID = oPayment.Payment_ID
                                                            bContinueSummarize = True
                                                            'Reset counter for BETFOR22-records
                                                            k = 0
                                                            Do While bContinueSummarize
                                                                If lMassCounter <= oBatch.Payments.Count Then
                                                                    oMassPayment = oBatch.Payments.Item(lMassCounter)
                                                                    'Have to go through the payment-object to see if the next payment also is
                                                                    ' a masspayment from same account, paymentdate.today, filenameout and ownref
                                                                    'If so add them together
                                                                    If oMassPayment.VB_FilenameOut_ID = iFormat_ID And (oMassPayment.PayType = "S" Or oMassPayment.PayType = "M") Then
                                                                        If oMassPayment.I_Account = sI_Account Then
                                                                            If oMassPayment.Payment_ID = sPayment_ID Then
                                                                                If InStr(oMassPayment.REF_Own, "&?") Then
                                                                                    'Set in the part of the OwnRef that BabelBank is using
                                                                                    sNewOwnref = Right(oMassPayment.REF_Own, Len(oMassPayment.REF_Own) - InStr(oMassPayment.REF_Own, "&?") - 1)
                                                                                Else
                                                                                    sNewOwnref = vbNullString
                                                                                End If
                                                                                If sNewOwnref = sOwnRef Then
                                                                                    bSumMassPayment = True
                                                                                End If
                                                                            Else
                                                                                Exit Do
                                                                            End If
                                                                        Else
                                                                            Exit Do
                                                                        End If
                                                                    Else
                                                                        Exit Do
                                                                    End If
                                                                    If bSumMassPayment Then
                                                                        For Each oMassInvoice In oMassPayment.Invoices
                                                                            nMassTransferredAmount = nMassTransferredAmount + oMassInvoice.MON_TransferredAmount
                                                                            lMassCounter = lMassCounter + 1
                                                                        Next
                                                                    End If
                                                                Else
                                                                    Exit Do
                                                                End If
                                                            Loop  'oMassPayment

                                                            'End new code

                                                            'BETFOR21 for Masstrans
                                                            sLine = WriteBETFOR21Mass(oPayment, nSequenceNoTotal, sCompanyNo, nMassTransferredAmount, iFormat_ID)
                                                            nInvoiceCounter = 0  ' added 20.08.2009
                                                            'First mass-transaction treated
                                                            bFirstMassTransaction = False
                                                            bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                            'Remove later: ExportCheck
                                                            ' added 28.02.2019
                                                            ' NHST_LONN for Singapore will be exported to an OCBC bank file
                                                            If sSpecial = "NHST_LONN" And oPayment.VoucherType = "OCB" And oPayment.I_CountryCode = "SG" Then
                                                                oPayment.Exported = False
                                                            Else
                                                                oPayment.Exported = True
                                                            End If

                                                        End If
                                                    End If
                                                Else
                                                    'BETFOR21 for invoicetrans
                                                    'XOKNET 18.11.2010 - added oBatch to parameters
                                                    sLine = WriteBETFOR21Invoice(oBatch, oPayment, nSequenceNoTotal, sCompanyNo, iFormat_ID, sSpecial)
                                                    nInvoiceCounter = 0  ' added 20.08.2009
                                                    'Have to reset masstrans flag
                                                    bFirstMassTransaction = True
                                                    bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)

                                                    ' XOKNET 21.01.2013 - Added BETFOR35
                                                    ' Removed BETFOR35 (Ultimate debitor), since DNB can't handle this, YET (will be reintroduced later)
                                                    'sLine = WriteBETFOR35(oPayment, nSequenceNoTotal, sCompanyNo, False)
                                                    'bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)

                                                    ' XOKNET 20.01.2012 - added BETFOR36
                                                    sLine = WriteBETFOR36(oPayment, nSequenceNoTotal, sCompanyNo)
                                                    bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                    'Remove later: ExportCheck
                                                    oPayment.Exported = True
                                                End If

                                            End If  'If iNoOfProcessedInvoiceRecords = 0 Then


                                            'k = 0  Only one invoice pr payment for masspayments!
                                            'If its a payment to an own account we shall just create a BETFOR21, not 22 or 23
                                            ' Changed 19,10.05 BUT this goes for Domestic only. For Int., create BETFOR04!!!!


                                            'If oPayment.ToOwnAccount = False Or oPayment.PayType = "I" Then
                                            ' XNET 07.12.2013 Linja over var helt natta, endret til;
                                            ' XNET 18.02.2014 - added  And oPayment.BANK_AccountCountryCode = "NO" -  outside NO we can have notifications also for internals
                                            If Not (oPayment.ToOwnAccount And oPayment.PayType = "D" And oPayment.BANK_AccountCountryCode = "NO") Then
                                                ' 29.11.2013 - moved next line up
                                                'For Each oInvoice In oPayment.Invoices
                                                'k will be used as a sequential number.
                                                k = k + 1
                                                If oPayment.PayType = "I" Then
                                                    'Will create a BETFOR04 transaction
                                                    ' 21.10.2009 - added loop for freetext, to cover several freetexts in on e invoice
                                                    iCount = 0
                                                    If oInvoice.Freetexts.Count > 0 Then
                                                        ' XOKNET 30.07.2012
                                                        ' Had problems when we had full freetext of 40 pos, and BETFOR04 holds only 35
                                                        ' Instead, take all freetext into one string, and use 35 chars from that string each time
                                                        sDummy = ""
                                                        For Each oFreetext In oInvoice.Freetexts
                                                            sDummy = sDummy & Trim(oFreetext.Text)
                                                        Next oFreetext
                                                        Do
                                                            iCount = iCount + 1
                                                            sLine = WriteBETFOR04(oPayment, oInvoice, k, nSequenceNoTotal, sCompanyNo, Left$(sDummy, 35), iCount)
                                                            iNoOfProcessedInvoiceRecords = iNoOfProcessedInvoiceRecords + 1
                                                            bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                            nInvoiceCounter = nInvoiceCounter + 1
                                                            sDummy = Mid$(sDummy, 36)
                                                            If EmptyString(sDummy) Then
                                                                Exit Do
                                                            End If
                                                        Loop
                                                    Else
                                                        ' special situation where we have no freetext, must despite that create one BETFOR04
                                                        iCount = iCount + 1
                                                        sLine = WriteBETFOR04(oPayment, oInvoice, k, nSequenceNoTotal, sCompanyNo, vbNullString, iCount)
                                                        iNoOfProcessedInvoiceRecords = iNoOfProcessedInvoiceRecords + 1
                                                        bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                        nInvoiceCounter = nInvoiceCounter + 1
                                                    End If

                                                ElseIf oPayment.PayType = "M" Or oPayment.PayType = "S" Then
                                                    'New by JanP 25.10.01 - Option to turn off export of masstranser for returnfiles
                                                    If bExportMassRecords Then
                                                        'Will create a BETFOR22 transaction
                                                        sLine = WriteBETFOR22(oPayment, oInvoice, k, nSequenceNoTotal, sCompanyNo, iFormat_ID)
                                                        iNoOfProcessedInvoiceRecords = iNoOfProcessedInvoiceRecords + 1
                                                        bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)

                                                        ' XokNET 03.10.2014
                                                        ' Added BETFOR42 for long names or long ultimate creditorname
                                                        ' Not in use before 05.09.2015
                                                        'If oPayment.DATE_Payment > "20150905" Then
                                                        ' XOKNET 17.09.2015, Endret pga at DNB ikke kan ta imot BETFOR42
                                                        If oPayment.DATE_Payment > "20151201" Then
                                                            ' Not for salarypayments!
                                                            If oPayment.PayType = "M" Then
                                                                If Len(oPayment.E_Name) > 30 Or Len(oPayment.UltimateE_Name) > 35 Then
                                                                    sLine = WriteBETFOR42(oPayment, nSequenceNoTotal, sCompanyNo)
                                                                    bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                                ElseIf oPayment.PayType = "S" Then
                                                                    ' XOKNET 07.12.2015 - also for Salary, but not UltimateCreditor
                                                                    If Len(oPayment.E_Name) > 30 Then
                                                                        sLine = WriteBETFOR42(oPayment, nSequenceNoTotal, sCompanyNo)
                                                                        bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                                    End If

                                                                End If
                                                            End If
                                                        End If
                                                        ' added 28.02.2019
                                                        ' NHST_LONN for Singapore will be exported to an OCBC bank file
                                                        If sSpecial = "NHST_LONN" And oPayment.VoucherType = "OCB" And oPayment.I_CountryCode = "SG" Then
                                                            oPayment.Exported = False
                                                        Else
                                                            oPayment.Exported = True
                                                        End If
                                                    End If
                                                    bLastPaymentWasMass = True
                                                Else
                                                    'Will create a BETFOR23 transaction
                                                    iCount = 0
                                                    Do While iCount - 1 < oInvoice.Freetexts.Count
                                                        If iCount = 0 Then
                                                            iCount = 1
                                                        End If
                                                        If Val(oPayment.StatusCode) > 99 Then
                                                            If Val(oBabel.BankByCode) = BabelFiles.Bank.DnB Then 'And oPayment.Invoices.Count = 0 Then
                                                                ' Changed 14.11.2003
                                                                ' Sometimes DnB has a BETFOR23, sometimes not for deleted transactions
                                                                If oInvoice.Freetexts.Count > 0 Or oInvoice.Unique_Id <> vbNullString Then
                                                                    If bFileFromBank Then
                                                                        oPayment.StatusCode = "02"
                                                                    Else
                                                                        oPayment.StatusCode = "00"
                                                                    End If
                                                                    sLine = WriteBETFOR23(oPayment, oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo, iFormat_ID, False)
                                                                    iNoOfProcessedInvoiceRecords = iNoOfProcessedInvoiceRecords + 1
                                                                    bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                                Else
                                                                    Exit For  ' No KID, no Freetext, write no betfor23
                                                                End If
                                                            Else
                                                                If bFileFromBank Then
                                                                    oPayment.StatusCode = "02"
                                                                Else
                                                                    oPayment.StatusCode = "00"
                                                                End If
                                                                sLine = WriteBETFOR23(oPayment, oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo, iFormat_ID, False)
                                                                iNoOfProcessedInvoiceRecords = iNoOfProcessedInvoiceRecords + 1
                                                                bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                                nInvoiceCounter = nInvoiceCounter + 1
                                                            End If
                                                        Else
                                                            ' 02.07.2009 added the TelepayPlus-part regarding FI75, Giro01
                                                            'If oPayment.PayCode = "301" And (Left$(oInvoice.Unique_Id, 2) = "04" Or Left$(oInvoice.Unique_Id, 2) = "71" Or Left$(oInvoice.Unique_Id, 2) = "75") And nInvoiceCounter = 0 Then
                                                            If oPayment.PayCode = "301" And (Left$(oInvoice.Unique_Id, 2) = "04" Or Left$(oInvoice.Unique_Id, 2) = "71" Or Left$(oInvoice.Unique_Id, 2) = "75") And nInvoiceCounter = 0 Then
                                                                sLine = WriteBETFOR23(oPayment, oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo, iFormat_ID, False)
                                                                ' added 09.07.2010 to also handle Norwegian domestics with kid in Telepay+
                                                                ' XOKNET 02.11.2011 - also added FI, Referencepayments
                                                            ElseIf oPayment.PayCode = "301" And (oPayment.BANK_AccountCountryCode = "NO" Or oPayment.BANK_AccountCountryCode = "FI") Then
                                                                sLine = WriteBETFOR23(oPayment, oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo, iFormat_ID, False)
                                                                ' added 26.08.2010 for Swedish local payments, with KID. Will have paycode 150 or 190, to see if Bankgiro or Plusgiro
                                                            ElseIf Not EmptyString(oPayment.Invoices.Item(1).Unique_Id) And oPayment.BANK_AccountCountryCode = "SE" Then
                                                                sLine = WriteBETFOR23(oPayment, oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo, iFormat_ID, False)

                                                            Else
                                                                sLine = WriteBETFOR23(oPayment, oInvoice, k, iCount, nSequenceNoTotal, sCompanyNo, iFormat_ID, True)
                                                            End If
                                                            iNoOfProcessedInvoiceRecords = iNoOfProcessedInvoiceRecords + 1
                                                            bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                                            nInvoiceCounter = nInvoiceCounter + 1

                                                        End If
                                                        ' added 04.12.06 to jump out when structured invoices
                                                        If iStructuredPayment Then
                                                            ' added 02.07.2009, to let FIK75 also write its freetext
                                                            If Not (Left$(oInvoice.Unique_Id, 2) = "75") Then
                                                                Exit Do
                                                            End If
                                                        End If

                                                    Loop
                                                End If
                                                '29.11.2013 Moved Next oInvoice further down
                                                ' Next oInvoice
                                            End If

                                            ' 29.11.2013
                                            ' Test if we have reached max no of allowed BETFOR04/23 ?
                                            If iNoOfProcessedInvoiceRecords >= iMaxNoOfInvoiceRecords Then
                                                iNoOfProcessedInvoiceRecords = 0
                                                ' will force writing of BETFOR01/23
                                            End If

                                            '29.11.2013 Moved Next oInvoice here
                                        Next oInvoice
                                    End If 'bExportoPayment
                                Next ' payment


                                ' Added by janP 25.10.01
                                'If bExportMassRecords Or bWriteBETFOR99Later Then
                                If Not (bExportMassRecords = False And bLastPaymentWasMass) Then
                                    ' XOKNET 30.01.2012 - added next If
                                    If nNoOfBETFOR > 0 Then

                                        sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion)
                                        If oBatch.VB_ProfileInUse = True Then
                                            If oBatch.VB_Profile.FileSetups(iFormat_ID).KeepBatch = True Then
                                                bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                            Else
                                                bWriteBETFOR99Later = True
                                                ' added 06.07.2009
                                                ' we must subtract 1 from the daysequence, because we have been into WriteBETFOR99, without writing the line!
                                                ApplicationHeader(False, "-1")
                                            End If
                                        Else
                                            bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                        End If
                                    End If
                                Else
                                    '    bWriteBETFOR99Later = True
                                    ' Masspayments and bExportMassRecords=False,
                                    ' Test if we had any non-mass payments before.
                                    ' 04.07.05: Changed from  > 0, to awoid Records with only BETFOR00/BETFOR99
                                    'If nNoOfBETFOR > 0 Then
                                    If nNoOfBETFOR > 1 Then
                                        ' We had invoice-records before the mass-records.
                                        ' Must write Betfor99
                                        sLine = WriteBETFOR99(oBatch, nNoOfBETFOR, nSequenceNoTotal, sCompanyNo, sVersion)
                                        bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)
                                    End If
                                End If
                                'We have to save the last used sequenceno. in some cases
                                'First check if we use a profile
                                If oBatch.VB_ProfileInUse = True Then
                                    'Then if it is a return-file.
                                    If oBatch.VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = True Then
                                        'No action

                                    Else
                                        Select Case oBatch.VB_Profile.FileSetups(iFormat_ID).SeqnoType
                                            'Use the sequenceno from oBabel
                                            Case 0
                                                'No action
                                                'Use the seqno at filesetup level
                                            Case 1
                                                If bWriteBETFOR99Later Then
                                                    oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal
                                                    sDummy = ApplicationHeader(False, "-1")
                                                Else
                                                    ' XOKNET 08.02.2012 - do not save sequence in TP+ for NHST_LONN
                                                    If sSpecial <> "NHST_LONN" Then
                                                        oBatch.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = nSequenceNoTotal - 1
                                                    End If
                                                End If
                                                oBatch.VB_Profile.Status = 2
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 1
                                                'We have to omit BETFOR00 in the future (if we don't change between domestic/international)
                                                bWriteBETFOR00 = False
                                                ' New by JanP 26.11.01
                                                ' Must also reduce day-sequence by one, since it has been added
                                                ' one in WriteBetfor99, which will never be used!
                                                ' ??????? WRONG ????? Moved to bWriteBETFOR99Later, up!sDummy = ApplicationHeader(False, "-1")
                                                'Use seqno per client per filesetup
                                            Case 2
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).SeqNoTotal = nSequenceNoTotal - 1
                                                oBatch.VB_Profile.Status = 2
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Status = 2
                                                oBatch.VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).Status = 1
                                        End Select
                                    End If
                                End If

                            End If
                            'bLastBatchWasDomestic = False 'Changed by JanP 29.10
                            'bFirstBatch = False  ' Changed by Janp 25.10
                        Next 'batch

                    End If
                End If

            Next 'Babelfile

            'Have to write BETFOR99 if keepbatch is true
            If bWriteBETFOR99Later Then
                bWriteOK = WriteLine(sLine, nNoOfBETFOR, nSequenceNoTotal, bUnix)

                'Added 09.08.2006
                'If seqno at clientlevel and keepbatches is set to false then we we save
                ' nSequenceNoTotal - 1, but when BETFOR99 is written we must save nSequenceNoTotal
                'First, check if we have at least 1 Babelfile
                If oBabelFiles.Count > 0 Then
                    'Then check if we use a profile
                    If oBabelFiles.Item(1).VB_ProfileInUse = True Then
                        'Then if it is a send-file.
                        If oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).FromAccountingSystem = False Then

                            'Then if it is seqno at the client-level
                            If oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).SeqnoType = 2 Then
                                oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).SeqNoTotal = nSequenceNoTotal - 1
                                oBabelFiles.Item(1).VB_Profile.Status = 2
                                oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).Status = 2
                                oBabelFiles.Item(1).VB_Profile.FileSetups(iFormat_ID).Clients(nClientNo).Status = 1
                            End If
                        End If
                    End If
                End If
            End If

            'For those who use vbBabel
            nSequenceNumberEnd = nSequenceNoTotal

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteTelepayPlus" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteTelepayPlusFile = True

    End Function
    Private Function WriteBETFOR00(ByVal oBatch As Batch, ByVal bFirstLine As Boolean, ByVal nSequenceNoTotal As Double, ByVal sBranch As String, ByVal sCompanyNo As String, ByVal sSpecial As String, Optional ByVal sClientNo As String = "") As String

        Dim sLine As String

        sLine = vbNullString
        'Build AH, send Returncode
        'bFirstLine is used to set the sequenceno in the aplicationheader.
        ' Added 06.07.06 due to problems with Nordea-files

        '29.04.2015 - Kjell and Jan-Petter agrees that the statuscode should ALWAYS be retrieved from Batch for BETFOR00/99
        sLine = ApplicationHeader(bFirstLine, oBatch.StatusCode)
        'If oBatch.Payments.Count > 0 Then
        '    sLine = ApplicationHeader(bFirstLine, oBatch.Payments(1).StatusCode)
        'Else
        '    sLine = ApplicationHeader(bFirstLine, oBatch.StatusCode)
        'End If
        sLine = sLine & "BETFOR00"

        ' XOKNET 17.02.2012 - pick EnterpriseNo for TELEPAYPlus from inputfile for NHST_LONN
        If sSpecial = "NHST_LONN" Then
            If EmptyString(oBatch.I_EnterpriseNo) Then
                sI_EnterpriseNo = PadLeft(sCompanyNo, 11, "0")
            Else
                sI_EnterpriseNo = PadLeft(oBatch.I_EnterpriseNo, 11, "0")
                sCompanyNo = ""
            End If
            sLine = sLine & sI_EnterpriseNo
        Else
            If sCompanyNo <> vbNullString Then
                sLine = sLine & PadLeft(sCompanyNo, 11, "0")
            Else
                sLine = sLine & sI_EnterpriseNo
            End If
        End If
        ' XOKNET end
        '---------

        If sBranch <> vbNullString Then
            'Uses Branch from the database
            ' New 04.11.05 to be able to redo from branch to blank branch
            If UCase(sBranch) = "INGEN" Or UCase(sBranch) = "BLANK" Then
                sBranch = Space(11)
            End If
            sLine = sLine & PadRight(sBranch, 11, " ")
        Else
            sLine = sLine & PadRight(oBatch.I_Branch, 11, " ")
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & Space(6)
        If Len(oBatch.DATE_Production) = 8 And oBatch.DATE_Production <> "19900101" Then
            sLine = sLine & Right$(oBatch.DATE_Production, 4) 'oBatch.DATE_Production
        Else
            sLine = sLine & Format(Now, "MMDD")
        End If
        sLine = sLine & Space(10)  ' Password, 85-94
        sLine = sLine & "VERSJON002"
        sLine = sLine & Space(10)
        sLine = sLine & PadRight(oBatch.OperatorID, 11, " ") '115-125  ' 01.04.2009, changed from padleft to padright
        'No use of SIGILL
        sLine = sLine & Space(1)
        sLine = sLine & StrDup(26, "0")
        sLine = sLine & Space(1)
        sLine = sLine & Space(143)

        If Len(oBatch.REF_Own) > 0 Then
            sLine = sLine & PadRight(oBatch.REF_Own, 15, " ")  ' 13.09.06 changed from padleft
        Else
            sLine = sLine & Space(15)  ' Egenreferanse - Added 27.05.04
        End If
        sLine = sLine & Space(9)

        ' xOKnet 12.11.2010 added 480 blanks for Telepay+
        sLine = sLine & Space(480)

        WriteBETFOR00 = sLine

    End Function
    'XOKNET 18.11.2010 - added oBatch to parameters
    Private Function WriteBETFOR01(ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String, ByVal iFormat_ID As Integer, ByVal sSpecial As String) As String
        Dim sErrorMessage As String
        Dim sLine As String

        ' XOKNET 10.07.2013 - Max 999 BETFOR01
        iNoOfBETFOR22_23 = iNoOfBETFOR22_23 + 1
        If iNoOfBETFOR22_23 > 999 Then
            sErrorMessage = LRS(35043, oPayment.MON_InvoiceCurrency, "TelepayPlus")
            ' XNET 26.11.2013 Uses %1 properly
            sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, vbNullString) & sImportFilename
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, vbNullString) & oPayment.MON_InvoiceAmount / 100
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, vbNullString) & oPayment.E_Name
            Err.Raise(35043, , sErrorMessage)
        End If

        sLine = vbNullString
        sLine = ApplicationHeader(False, oPayment.StatusCode)
        sLine = sLine & "BETFOR01"
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If

        ' 04.08.2010 changed next if tp include swift DNBNO
        If Left$(oPayment.BANK_I_SWIFTCode, 6) <> "DNBANO" Then  ' 30.12.2008 If TelepayPlus, account is in pos 347
            ' TKIO-format has I_Account in BETFOR02, pos 245-279
            sLine = sLine & "00000000000"
        Else
            ' Telepay "classic"
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")

        '13.01.2009 - added the IF below due to Francis
        If oPayment.VB_ProfileInUse Then
            ' 04.05.06 Added new test about date
            If oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateAll Then
                ' Set all dates to todays date
                ' 04.01.2008
                ' Changed to give us next bankday, instead of today.
                oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "NO", 0)) 'datetostring(Date)
            End If
        End If

        ' 04.05.06 Added new test about date
        If StringToDate(oPayment.DATE_Payment) <= Date.Today Then
            '13.01.2009 - added the IF below due to Francis
            If oPayment.VB_ProfileInUse Then
                If oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateDue Then
                    ' Set date to todays date
                    ' 04.01.2008
                    ' Changed to give us next bankday, instead of today.
                    oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "NO", 0)) 'datetostring(Date)
                End If
            End If
        End If

        sLine = sLine & Right$(oPayment.DATE_Payment, 6)
        sLine = sLine & Left$(oPayment.REF_Own + Space(30), 30)
        If Len(Trim$(oPayment.MON_TransferCurrency)) <> 3 Then
            sLine = sLine & "   " 'Transf.cur will be equal to inv.cur.
        Else
            sLine = sLine & Trim$(oPayment.MON_TransferCurrency)
        End If
        If Len(Trim$(oPayment.MON_InvoiceCurrency)) <> 3 Then
            sErrorMessage = LRS(35009, oPayment.MON_InvoiceCurrency, "TelepayPlus")
            ' XNET 26.11.2013 Uses %1 properly
            sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, vbNullString) & sImportFilename
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, vbNullString) & oPayment.MON_InvoiceAmount
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
            sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, vbNullString) & oPayment.E_Name
            Err.Raise(35009, , sErrorMessage)
        Else
            sLine = sLine & Trim$(oPayment.MON_InvoiceCurrency)
        End If
        If oPayment.MON_ChargeMeAbroad = True Then
            sLine = sLine & "OUR"
        Else
            sLine = sLine & "BEN"
        End If
        If oPayment.MON_ChargeMeDomestic = True Then
            sLine = sLine & "OUR"
        Else
            sLine = sLine & "BEN"
        End If
        ' This is notification by receiving bank
        ' 05.12.06 added Or oPayment.ToOwnAccount
        If oPayment.NOTI_NotificationParty = 2 Or oPayment.ToOwnAccount Then
            ' new 19.01.05 for TEEKAY
            If oPayment.ToOwnAccount Then
                sLine = sLine & PadRight("ACCTRN", 30, " ")
            Else
                ' Also notification by RECEIVING BANK
                Select Case oPayment.NOTI_NotificationType
                    Case "PHONE", "TELEFON"
                        sLine = sLine & "PHONE " & PadRight(oPayment.NOTI_NotificationIdent, 24, " ")
                    Case "TELEX"
                        sLine = sLine & "TELEX " & PadRight(oPayment.NOTI_NotificationIdent, 24, " ")
                    Case "FAX"
                        sLine = sLine & "FAX " & PadRight(oPayment.NOTI_NotificationIdent, 26, " ")
                    Case "OTHER"
                        sLine = sLine & "OTHER " & PadRight(oPayment.NOTI_NotificationIdent, 26, " ")
                    Case Else
                        sLine = sLine & PadRight(oPayment.NOTI_NotificationMessageToBank, 30, " ")
                End Select
            End If
        Else
            sLine = sLine & Space(30)
        End If
        'sLine = sLine & PadRight(oPayment.NOTI_NotificationMessageToBank, 30, " ")
        If oPayment.Priority = True Then
            ' XOKNET 03.09.2012 - Ingen hastemerking p� betalinger til Egen konto - BUG i DNB, Ref H�kon Belt
            If oPayment.ToOwnAccount Then
                sLine = sLine & " "
            Else
                sLine = sLine & "J"
            End If
        Else
            sLine = sLine & " "
        End If
        sLine = sLine & PadLeft(Int(oPayment.ERA_ExchRateAgreed), 4, "0") '160
        ' Changed 21.10.05 - gave wrong results
        'sLine = sLine & PadRight(oPayment.ERA_ExchRateAgreed - Int(oPayment.ERA_ExchRateAgreed), 4, "0")
        sLine = sLine & PadLeft(oPayment.ERA_ExchRateAgreed * 10000 - (Int(oPayment.ERA_ExchRateAgreed) * 10000), 4, "0")

        sLine = sLine & PadRight(oPayment.FRW_ForwardContractNo, 6, " ")  '168
        ' changed 20.09.05 - Gave wrong results!
        sLine = sLine & PadLeft(Int(oPayment.FRW_ForwardContractRate), 4, "0") '174
        'sLine = sLine & PadRight(oPayment.FRW_ForwardContractRate - Int(oPayment.FRW_ForwardContractRate), 4, "0")
        sLine = sLine & PadLeft(oPayment.FRW_ForwardContractRate * 10000 - (Int(oPayment.FRW_ForwardContractRate) * 10000), 4, "0")
        If oPayment.Cheque = BabelFiles.ChequeType.noCheque Then
            sLine = sLine & " "  '182
        ElseIf oPayment.Cheque = BabelFiles.ChequeType.SendToPayer Then
            sLine = sLine & "2"  ' 27.05.04 Changed from 0
        ElseIf oPayment.Cheque = BabelFiles.ChequeType.SendToReceiver Then
            sLine = sLine & "1"
        End If
        '    If oPayment.DATE_Value <> "19900101" Then
        '        sLine = sLine & Right$(oPayment.DATE_Value, 6)
        '    Else
        'Not logical in Norway, hardcode spaces
        sLine = sLine & Space(6) ' 183 - Valutering mottakende bank
        '    End If
        sLine = sLine & Space(2) '189
        If oPayment.StatusCode <> "02" Then
            sLine = sLine & "000000000000" '191 reell kurs
        Else
            If oPayment.MON_TransferCurrency = oPayment.MON_AccountCurrency Then
                If oPayment.MON_TransferCurrency <> "NOK" Then
                    'If so use the local exchangerate.
                    sLine = sLine & PadLeft(Int(oPayment.MON_LocalExchRate), 4, "0")
                    sLine = sLine & PadRight(Mid$(Replace(Math.Round(oPayment.MON_LocalExchRate - Int(oPayment.MON_LocalExchRate), 8), ",", vbNullString), 2), 8, "0")
                Else
                    sLine = sLine & "000000000000"
                End If
            ElseIf oPayment.MON_AccountExchRate <> oPayment.MON_LocalExchRate Then
                sLine = sLine & PadLeft(Int(oPayment.MON_AccountExchRate), 4, "0")
                sLine = sLine & PadRight(Mid$(Replace(Math.Round(oPayment.MON_AccountExchRate - Int(oPayment.MON_AccountExchRate), 8), ",", vbNullString), 2), 8, "0")
            Else
                sLine = sLine & "000000000000"
            End If
        End If
        sLine = sLine & PadLeft(oPayment.REF_Bank2, 12, " ")  '203 Effektueringsref2
        If oPayment.MON_AccountAmount = 0 Then   'Belastest bel�p 215-230
            sLine = sLine & StrDup(16, "0")
        Else
            sLine = sLine & PadLeft(Str(oPayment.MON_AccountAmount), 16, "0")
        End If
        If oPayment.MON_TransferredAmount = 0 Or oPayment.StatusCode <> "02" Then
            sLine = sLine & StrDup(16, "0") '231 Overf�rt bel�p
        Else
            sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 16, "0")
        End If
        '29.07.2008 - Changed the variable ClientNumber from Integer to string
        If Len(sClientNumber) > 0 Then
            sLine = sLine & PadLeft(oPayment.I_Client, 5, " ") '247 Klient
        Else
            sLine = sLine & Space(5) '247 Klient
        End If
        sLine = sLine & PadLeft(oPayment.REF_Bank1, 6, "0") '252 Effektueringsref.1
        sLine = sLine & PadRight(oPayment.ERA_DealMadeWith, 6, " ") '258 Avtalt med
        If oPayment.Cancel = True Then
            sLine = sLine & "S" '264 Slettekode
        Else
            sLine = sLine & Space(1)
        End If
        sLine = sLine & Space(1)  '265 Clearingcode, not in use
        If oPayment.DATE_Value = "19900101" Then
            'Removed next IF-statement, because nothing is written if statuscode <> "02"
            'If oPayment.StatusCode = "02" Then
            sLine = sLine & StrDup(6, "0")  '266 Valuteringsdato
            'End If
        Else
            sLine = sLine & Right$(oPayment.DATE_Value, 6) 'Format(oPayment.DATE_Value, "YYMMDD")
        End If
        sLine = sLine & PadLeft(Format(oPayment.MON_ChargesAmount, "###0"), 9, "0")   '272 Provisjon, 2 desimaler
        If oPayment.StatusCode <> "02" Then
            sLine = sLine & "000000000000"  '281 Kurs mot NOK
        Else
            sLine = sLine & PadLeft(Int(oPayment.MON_LocalExchRate), 4, "0")
            sLine = sLine & PadRight(Mid(Replace(Math.Round(oPayment.MON_LocalExchRate - Int(oPayment.MON_LocalExchRate), 8), ",", vbNullString), 2), 8, "0")
        End If
        sLine = sLine & Space(1)  ' 293 Slette�rsak
        sLine = sLine & Space(16) '294 Bestillt overf�rt bel�p
        sLine = sLine & Space(1) '310 Info vedr�rende prising
        sLine = sLine & Space(10)

        ' 30.12.2008 added 160 chars for Telepay+
        ' 321 - 335 C2B PAYMENT ID DEB , Unique payment ID debit side from C2B -  WHAT IS THIS ????
        sLine = sLine & Space(15)   ' not used so far
        ' 336 - 346 INPS DEBIT SWIFT, Identifies the payor�s bank by its SWIFT address.
        sLine = sLine & PadRight(oPayment.BANK_I_SWIFTCode, 11, " ")
        ' 347 - 381 INPS DEBIT ACCOUNT NUMBER , Payor Account Number
        ' use this one if Telepay+
        ' 11.05.2009: But NOT for TKIO (debit account in another bank than DnBNOR NO, SE, DK
        If bCurrentIsTKIO = False Then
            sLine = sLine & PadRight(oPayment.I_Account, 35, " ")
        Else
            sLine = sLine & Space(35)
        End If
        ' 382 ONHOLD-INFO-IN-C2B WHAT IS THIS ????
        sLine = sLine & Space(1)
        ' Discussions i DnBNOR if pos 383-384 or 47-448 for Paymentcode
        ' 18.06.2009 Use 383 after order from Bjarne Skadsem
        ' ' PAYMENT CODE    383;383 2   ALPHA Equal to transaction type in TBI+  21Z pos. 129;130. Indicates payment type. E.g FI 71 = 21
        ' Must use a converstiontable between BabelBank codes and TBI-codes
        ' XOKNET 03.01.2013 added parameter for paytype tp bbGetPayCodeTelepayPlus
        sLine = sLine & bbGetPayCodeTelepayPlus(oPayment, "I")

        ' XOKNET 15.11.2010 from here to end of function
        sLine = sLine & Space(15)   ' Lump_sum_ref    385;399 15  ALPHA
        'XokNET 02.05.2014
        '02.05.2014 added Utlimate debitor
        sLine = sLine & PadRight(oPayment.UltimateI_Name, 35, " ")   ' Ordering Customer/Ultimate Debitor-name 400;434 35  ALPHA
        sLine = sLine & Space(35)   ' Ordering Customer/Ultimate Debitor-street   435;469 35  ALPHA
        sLine = sLine & Space(16)   ' Ordering Customer/Ultimate Debitor-Building Number  470;485 16  ALPHA
        sLine = sLine & Space(35)   ' Ordering Customer/Ultimate Debitor-place/city   486;520 35  ALPHA
        sLine = sLine & Space(9)    ' Ordering Customer/Ultimate Debitor-postcode.    521;529 9   ALPHA
        sLine = sLine & Space(2)    ' Ordering Customer/Ultimate Debitor-country  530;531 2   ALPHA
        If Not EmptyString(oPayment.REF_EndToEnd) Then
            sLine = sLine & PadRight(oPayment.REF_EndToEnd, 35, " ")  ' Posteringstekst/Ben. Ref. /End to end ref.  532;566
            ' XokNET 02.05.2014 - use Text_E_Statement as end_to_endref if no EndToEndref
        ElseIf Not EmptyString(oPayment.Text_E_Statement) Then
            sLine = sLine & PadRight(oPayment.Text_E_Statement, 35, " ")  ' Posteringstekst/Ben. Ref. /End to end ref.  532;566
        Else
            ' XokNET 16.07.2014 Do NOT ise Ref_Own as Ref_EndToEnd
            'sLine = sLine & PadRight(oPayment.REF_Own, 35, " ")  ' Posteringstekst/Ben. Ref. /End to end ref.  532;566
            sLine = sLine & Space(35)  ' Posteringstekst/Ben. Ref. /End to end ref.  532;566
        End If

        sLine = sLine & PadRight(oBatch.REF_Own, 35, " ")    'OWN REFERENCE ORDER (B-Level)   567;601 35  ALPHA
        sLine = sLine & Space(2)   'Tilgangstype/Access type    602;603 2   ALPHA
        sLine = sLine & PadRight(oPayment.REF_Own, 35, " ")    'Customer own referance(C-level) 604;638 35
        ' XokNET 23.06.2014 Changes from here to end of function
        'Reason for rejection/Avvisningsaarsak   639;641 3   ALPHA
        sLine = sLine & Space(3) ' 639-641
        ' Category Purpose    642;645 4   ALPHA
        sLine = sLine & PadRight(oPayment.CategoryPurposeCode, 4, " ") ' 639-642
        ' Purpose 646;649 4   ALPHA
        sLine = sLine & PadRight(oPayment.PurposeCode, 4, " ")  '646-649

        sLine = sLine & Space(151)   'RESERVED    650;800 154 ALPHA

        WriteBETFOR01 = UCase(sLine)


    End Function
    Private Function WriteBETFOR02(ByVal oPayment As Payment, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String) As String
        Dim sAccount As String
        Dim sLine As String

        sLine = vbNullString
        sLine = ApplicationHeader(False, oPayment.StatusCode)
        sLine = sLine & "BETFOR02"
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        ' 04.08.2010 changed next if tp include swift DNBNO
        If Left$(oPayment.BANK_I_SWIFTCode, 6) <> "DNBANO" Then  ' 30.12.2008 If TelepayPlus, account is in pos 347
            ' TKIO-format has I_Account in BETFOR02, pos 245-279
            sLine = sLine & "00000000000"
        Else
            ' Telepay "classic"
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        End If

        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")  '75

        ' XOKNET 27.02.2013
        ' For payments with both BIC and  babelfiles.bankbranchtype.sortcode, remove BIC, as this causes problems in DNB. Only for GBP
        ' 16.03.2018 - Kjell Chr DNB says: Keep both BIC and Sortcode
        ' removed next IF
        'If oPayment.MON_InvoiceCurrency = "GBP" And (oPayment.BANK_BranchType = BabelFiles.BankBranchType.SortCode Or (oPayment.BANK_BranchNo <> "" And oPayment.BANK_CountryCode = "GB")) And Not EmptyString(oPayment.BANK_SWIFTCode) Then
        '    ' remove BIC
        '    oPayment.BANK_SWIFTCode = ""
        '    oPayment.BANK_BranchType = BabelFiles.BankBranchType.SortCode
        'End If

        sLine = sLine & PadRight(oPayment.BANK_SWIFTCode, 11, " ") '81

        sLine = sLine & PadRight(oPayment.BANK_Name, 35, " ") '92
        sLine = sLine & PadRight(oPayment.BANK_Adr1, 35, " ") '127
        sLine = sLine & PadRight(oPayment.BANK_Adr2, 35, " ") '162
        sLine = sLine & PadRight(oPayment.BANK_Adr3, 35, " ") '197
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCodeCorrBank, 11, " ") '232
        If Len(oPayment.BANK_CountryCode) <> 2 Then
            'FIXERROR Babelerror, warning not compulsory?
            sLine = sLine & "  " '243
        Else
            sLine = sLine & oPayment.BANK_CountryCode
        End If

        ' Reserved space for branchNo, 245,15
        ' In Telepay format, must add SC, FW, etc
        'XOKNET 29.02.2012 -
        ' It is better to NOT change oPayment.BANK_BranchNo, because this destroys the errorhandling later on
        ' Instead, add FW, SC, etc to sLine !!!
        Select Case oPayment.BANK_BranchType
            Case BabelFiles.BankBranchType.Fedwire
                sLine = sLine & PadRight("FW" & oPayment.BANK_BranchNo, 15, " ")
            Case BabelFiles.BankBranchType.SortCode
                sLine = sLine & PadRight("SC" & oPayment.BANK_BranchNo, 15, " ")
            Case BabelFiles.BankBranchType.Bankleitzahl
                sLine = sLine & PadRight("BL" & oPayment.BANK_BranchNo, 15, " ")
            Case BabelFiles.BankBranchType.Chips
                sLine = sLine & PadRight("CH" & oPayment.BANK_BranchNo, 15, " ")
            Case BabelFiles.BankBranchType.CC
                sLine = sLine & PadRight("CC" & oPayment.BANK_BranchNo, 15, " ")
            Case Else
                sLine = sLine & PadRight(oPayment.BANK_BranchNo, 15, " ")
        End Select
        ' OLD CODE
        'Select Case oPayment.BANK_BranchType
        'Case babelfiles.bankbranchtype.fedwire
        '    oPayment.BANK_BranchNo = "FW" & oPayment.BANK_BranchNo
        'Case  babelfiles.bankbranchtype.sortcode
        '    oPayment.BANK_BranchNo = "SC" & oPayment.BANK_BranchNo
        'Case  babelfiles.bankbranchtype.Bankleitzahl
        '    oPayment.BANK_BranchNo = "BL" & oPayment.BANK_BranchNo
        'Case  babelfiles.bankbranchtype.chips
        '    oPayment.BANK_BranchNo = "CH" & oPayment.BANK_BranchNo
        '' XokNET 12.10.2011 added babelfiles.bankbranchtype.cc
        'Case babelfiles.bankbranchtype.cc
        '    oPayment.BANK_BranchNo = "CC" & oPayment.BANK_BranchNo
        'End Select
        '
        'sLine = sLine & PadRight(oPayment.BANK_BranchNo, 15, " ")

        If bCurrentIsTKIO Then
            sAccount = Trim$(oPayment.I_Account)
            sLine = sLine & PadRight(sAccount, 35, " ") '260
        Else
            sLine = sLine & Space(35)
        End If

        sLine = sLine & Space(26) '295

        'XOKNET 09.02.2012 From here to end of function
        'Intermediary Bank name  321;355 35
        sLine = sLine & PadRight(oPayment.BANK_NameAddressCorrBank1, 35, " ")
        'Intermediary Bank address 1 356;390 35
        sLine = sLine & PadRight(oPayment.BANK_NameAddressCorrBank2, 35, " ")
        'Intermediary Bank address 2 391;425 35
        sLine = sLine & PadRight(oPayment.BANK_NameAddressCorrBank3, 35, " ")
        'Intermediary Bank address 3 426;460 35
        sLine = sLine & PadRight(oPayment.BANK_NameAddressCorrBank4, 35, " ")

        'Intermediary Bank code  461;483 23
        sLine = sLine & PadRight(oPayment.BANK_BranchNoCorrBank, 23, " ")
        'Remitting bank message  484;623 140
        sLine = sLine & PadRight(oPayment.NOTI_NotificationMessageToBank, 140, " ")
        'Intermediary Bank address, Country Code 624;625 2
        sLine = sLine & PadRight(oPayment.BANK_CountryCodeCorrBank, 2, " ")
        sLine = sLine & Space(175)

        WriteBETFOR02 = UCase(sLine)


    End Function
    Private Function WriteBETFOR03(ByVal oPayment As Payment, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String) As String

        Dim sLine As String

        sLine = vbNullString
        sLine = ApplicationHeader(False, oPayment.StatusCode)
        sLine = sLine & "BETFOR03"
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo  '49
        End If
        If Left$(oPayment.BANK_I_SWIFTCode, 6) <> "DNBANO" Then  ' 30.12.2008 If TelepayPlus, account is in pos 347
            ' TKIO-format has I_Account in BETFOR02, pos 260-294
            sLine = sLine & "00000000000"  '60
        Else
            ' Telepay "classic"
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")  '75
        ' XokNET 18.10.2011
        ' If A CHEQUE, remove it from accountNo, DnB will not use it
        If oPayment.E_Account = "A CHEQUE" Then
            oPayment.E_Account = ""
        End If
        ' XokNET 06.03.2015
        ' If Financiell payments, and no creditaccount, then set BIC in Creditaccountfield;
        If oPayment.PayCode = "406" Then
            If EmptyString(oPayment.E_Account) Then
                oPayment.E_Account = oPayment.BANK_SWIFTCode
            End If
        End If
        sLine = sLine & PadRight(oPayment.E_Account, 35, " ")  '81
        sLine = sLine & PadRight(oPayment.E_Name, 35, " ")     '116
        sLine = sLine & PadRight(oPayment.E_Adr1, 35, " ")     '151
        sLine = sLine & PadRight(oPayment.E_Adr2, 35, " ")     '186
        sLine = sLine & PadRight(oPayment.E_Adr3, 35, " ")     '221
        If Len(oPayment.E_CountryCode) <> 2 Then
            If Len(oPayment.BANK_CountryCode) = 2 Then
                sLine = sLine & oPayment.BANK_CountryCode      '256
            Else
                'Maybe we should raise an error!
                sLine = sLine & "  "
            End If
        Else
            sLine = sLine & oPayment.E_CountryCode
        End If

        ' This is notification by payers bank
        If oPayment.NOTI_NotificationParty = 1 Or oPayment.NOTI_NotificationParty = 3 Then
            'Receiver shall be notified by payors bank
            If oPayment.NOTI_NotificationType = "TELEX" Then
                If oPayment.NOTI_NotificationIdent = vbNullString Or oPayment.NOTI_NotificationAttention = vbNullString Then
                    sLine = sLine & Space(41)
                    'FIX: Something is wrong. Some fields are compulsory. Issue a warning.
                Else
                    sLine = sLine & "T"  '258
                    'Both Telex countrycode and number are stored in Ident.
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 20, " ") '259
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationAttention, 20, " ") '279
                End If
            ElseIf oPayment.NOTI_NotificationType = "FAX" Then
                If oPayment.NOTI_NotificationIdent = vbNullString Or oPayment.NOTI_NotificationAttention = vbNullString Then
                    sLine = sLine & Space(41)
                    'FIX: Something is wrong. Some fields are compulsory. Issue a warning.
                Else
                    sLine = sLine & "F" & "  "
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 18, " ")
                    sLine = sLine & PadRight(oPayment.NOTI_NotificationAttention, 20, " ")
                End If
            Else
                sLine = sLine & Space(41)
            End If
        Else
            'No notification by payors bank
            sLine = sLine & Space(41)
        End If
        sLine = sLine & Space(22) '299

        sLine = sLine & PadRight(oPayment.UltimateE_Name, 35, " ")  ' Ultimate Creditor Name  321;355 35  ALPHA
        sLine = sLine & Space(35)  ' Utlimate Creditor Street    356;390 35  ALPHA
        sLine = sLine & Space(16)  ' Utlimate Creditor Building Number   391;406 16  ALPHA
        sLine = sLine & Space(35)  ' Ultimate Creditor place/city    407;441 35  ALPHA
        sLine = sLine & Space(9)   ' Ultimate Creditor Postcode  442;450 9   ALPHA
        sLine = sLine & Space(2)   ' Ultimate Creditor CountryCode   451;452 2   ALPHA

        ' XOKNET 11.06.2015 - DNB added two fields in TelepayPlus BETFOR03
        sLine = sLine & PadRight(oPayment.E_Name, 140, " ")             'Creditor Name   453;592 140  ALPHA
        sLine = sLine & PadRight(oPayment.UltimateE_Name, 140, " ")   'Ultimate Creditor Name  593;732 140  ALPHA
        sLine = sLine & Space(68) ' RESERVED    733;800 68  ALPHA

        WriteBETFOR03 = UCase(sLine)

    End Function
    Private Function WriteBETFOR04(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal iSerialNo As Double, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String, ByVal sText As String, ByVal iCount As Integer) As String

        Dim sLine As String

        'iCount = 0
        sLine = vbNullString
        sLine = ApplicationHeader(False, oInvoice.StatusCode)
        sLine = sLine & "BETFOR04"
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo  '49
        End If
        ' 04.08.2010 changed next if tp include swift DNBNO
        If Left$(oPayment.BANK_I_SWIFTCode, 6) <> "DNBANO" Then ' 30.12.2008 If TelepayPlus, account is in pos 347
            ' TKIO-format has I_Account in BETFOR02, pos 260-294
            sLine = sLine & "00000000000"   '60
        Else
            ' Telepay "classic"
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        sLine = sLine & PadRight(oInvoice.Invoice_ID, 6, " ")  '75
        'Changed 21.10.2009 - now allows several BETFOR04 to cover freetext
        sLine = sLine & Left(sText + Space(35), 35) '81

        sLine = sLine & Left$(oInvoice.REF_Own + Space(35), 35) '116
        ' added test 21.10.2009
        If iCount = 1 Then
            sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0") '151
        Else
            sLine = sLine & PadLeft("0", 15, "0") '151
        End If
        'If oInvoice.MON_InvoiceAmount < 0 Then  ' changed 21.10.2009
        If oInvoice.MON_InvoiceAmount < 0 And iCount = 1 Then ' changed 21.10.2009
            sLine = sLine & "K"  '166
        Else
            sLine = sLine & "D"
        End If

        sLine = sLine & PadRight(oInvoice.STATEBANK_Code, 6, " ")  '167
        sLine = sLine & PadRight(oInvoice.STATEBANK_Text, 60, " ") '173

        'FIX: To own account is not imported from Telepay
        ' Endret 19.01.05, ref TEEKAY
        If oPayment.ToOwnAccount Then
            sLine = sLine & "J"             '233 Til egen konto
        Else
            sLine = sLine & Space(1)        '233 Til egen konto
        End If

        sLine = sLine & Space(1)        '234
        sLine = sLine & StrDup(6, "0")  '235
        sLine = sLine & Space(1)        '241
        sLine = sLine & StrDup(6, "0")  '242
        sLine = sLine & Space(45)       '248
        sLine = sLine & Space(1)       '293 KID Utland - not implemented yet
        If oInvoice.StatusCode = "00" Then
            sLine = sLine & "000"
        Else
            sLine = sLine & PadLeft(Str(iSerialNo), 3, "0") '294
        End If
        sLine = sLine & Space(24)   '397-320

        ' XOKNET 15.11.2010 from here to end of function
        sLine = sLine & Space(480)

        WriteBETFOR04 = UCase(sLine)


    End Function
    'XOKNET 18.11.2010 - added oBatch to parameters
    Private Function WriteBETFOR21Invoice(ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String, ByVal iFormat_ID As Integer, ByVal sSpecial As String) As String

        Dim sLine As String
        Dim sPayCode As String, iCount As Integer
        Dim oInvoice As Invoice
        Dim sErrorMessage As String

        ' Reset some controlvalues for each payment:
        '------------------------------------
        iStructuredPayment = -1
        iNoOfBETFOR22_23 = 0
        iNoOfTextLinesInBETFOR23 = 0


        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "TELEPAY")

        ' added 06.07.2009 for Telepay+ with FIK/Girocard
        If oPayment.Invoices.Count > 0 Then
            ' added next If 09.08.2010 - do not mess with Norwegian domestic KIDs
            If oPayment.BANK_AccountCountryCode <> "NO" Then
                Select Case Left$(oPayment.Invoices.Item(1).Unique_Id, 2)
                    Case "01" ' Girocard 01
                        sPayCode = "602" ' If�lge Bjarne Skadsem
                    Case "73" ' FIK73
                        sPayCode = "602" ' If�lge Bjarne Skadsem
                End Select
            End If
        End If
        ' "New" PayCode 603 when no account specified
        If oPayment.E_Account = "00000000000" Or Len(oPayment.E_Account) = 0 Then
            sPayCode = "603"
        End If

        sLine = vbNullString
        sLine = ApplicationHeader(False, oPayment.StatusCode)
        sLine = sLine & "BETFOR21"  '41
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0") '49
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        ' 04.08.2010 changed next if tp include swift DNBNO
        If Left$(oPayment.BANK_I_SWIFTCode, 6) <> "DNBANO" Then ' 30.12.2008 If TelepayPlus, account is in pos 347
            sLine = sLine & StrDup(11, "0") '60
        Else
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")  '60
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71

        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")  '75

        ' 04.05.06 Added new test about date
        If oPayment.VB_ProfileInUse Then
            If oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateAll Then
                ' Set all dates to todays date
                ' 04.01.2008
                ' Changed to give us next bankday, instead of today.
                oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "NO", 0)) 'datetostring(Date)
            End If
        End If

        If oPayment.VB_ProfileInUse Then
            ' 04.05.06 Added new test about date
            If StringToDate(oPayment.DATE_Payment) <= Date.Today Then
                If oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateDue Then
                    ' Set date to todays date
                    ' 04.01.2008
                    ' Changed to give us next bankday, instead of today.
                    oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "NO", 0)) 'datetostring(Date)
                End If
            End If
        End If

        sLine = sLine & Right$(oPayment.DATE_Payment, 6)       '81
        sLine = sLine & Left$(oPayment.REF_Own + Space(30), 30) '87
        sLine = sLine & Space(1) '117
        'FIXERROR: Babelerror if E_Accoun longer than 11 or alfanumeric
        ' 05.07.2010 Added oPayment.BANK_AccountCountryCode = "NO" And oPayment.BANK_CountryCode = "NO"
        If oPayment.E_Account = "00000000000" Or Len(oPayment.E_Account) = 0 Or oPayment.PayCode = "160" And _
            oPayment.BANK_AccountCountryCode = "NO" And oPayment.BANK_CountryCode = "NO" Then
            sLine = sLine & "00000000019"
            sPayCode = "603"
        Else
            ' 05.07.2010 Added if-test oPayment.BANK_AccountCountryCode = "NO" And oPayment.BANK_CountryCode = "NO"
            If oPayment.BANK_AccountCountryCode = "NO" And oPayment.BANK_CountryCode = "NO" Then
                sLine = sLine & PadRight(oPayment.E_Account, 11, " ") '118
            Else
                sLine = sLine & Space(11) '118
            End If
        End If

        ' added 04.05.06 - Check accountno
        If oPayment.VB_ProfileInUse Then
            If oPayment.VB_Profile.FileSetups(iFormat_ID).CtrlAccount Then
                If Not CheckAccountNoNOR(oPayment.E_Account) Then
                    sErrorMessage = LRS(35047, oPayment.E_Account, "Telepay2")
                    ' XNET 26.11.2013 Uses %1 properly
                    sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, vbNullString) & sImportFilename
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, vbNullString) & oPayment.MON_InvoiceAmount / 100
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, vbNullString) & oPayment.E_Name
                    Err.Raise(35047, , sErrorMessage)
                End If
            End If
        End If
        sLine = sLine & PadRight(oPayment.E_Name, 30, " ") '129
        sLine = sLine & PadRight(oPayment.E_Adr1, 30, " ") '159
        sLine = sLine & PadRight(oPayment.E_Adr2, 30, " ") '189
        sLine = sLine & PadRight(oPayment.E_Zip, 4, " ") '219

        ' 24.02.2016
        ' We may have situations where the importformat fills Adr1-3, and not E_City.
        ' In these cases, put info from E_Adr3 into E_City
        If EmptyString(oPayment.E_City) And Not EmptyString(oPayment.E_Adr3) Then
            sLine = sLine & PadRight(oPayment.E_Adr3, 26, " ") '223
        Else
            sLine = sLine & PadRight(oPayment.E_City, 26, " ") '223
        End If
        'The amount if its a payment to own account
        ' XokNET 04.07.2014 -
        ' Kontrabeskjed fra DNB - Bel�pet skal st� i BETFOR21 for betaling til egen konto - og KUN i BETFOR21
        If oPayment.ToOwnAccount = True Then
            iCount = 0
            For Each oInvoice In oPayment.Invoices
                iCount = iCount + 1
                If iCount > 1 Then
                    'FIXERROR: Can't be more than 1 invoice when it is a payment to own account
                Else
                    sLine = sLine & PadLeft(Str(oInvoice.MON_InvoiceAmount), 15, "0")
                End If
            Next
        Else
            sLine = sLine & New String("0", 15) '249
        End If
        sLine = sLine & sPayCode '264


        If oPayment.ToOwnAccount = True Then
            sLine = sLine & "E"
        Else
            sLine = sLine & "F"  '267
        End If
        If oPayment.Cancel = True Then
            sLine = sLine & "S"
        Else
            sLine = sLine & Space(1)  '268
        End If
        'If this file shall be sent to the bank the amount shall be set to 0.
        If oPayment.StatusCode = "00" Then 'Amount = 0 Then
            sLine = sLine & StrDup(15, "0")  '269
        ElseIf oPayment.StatusCode = vbNullString Then
            'The imported file was not Telepay
            If Not bFileFromBank Then
                sLine = sLine & StrDup(15, "0")
            Else
                sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 15, "0")
            End If
        Else
            sLine = sLine & PadLeft(Str(oPayment.MON_TransferredAmount), 15, "0")
        End If

        If Len(oPayment.I_Client) > 0 Then
            sLine = sLine & PadRight(oPayment.I_Client, 5, " ")  '284
        Else
            sLine = sLine & StrDup(5, "0") '284 Klientreferanse
        End If
        If oPayment.DATE_Value = "19900101" Or oPayment.StatusCode = "00" Then
            sLine = sLine & StrDup(6, "0") '289
        Else
            sLine = sLine & Right$(oPayment.DATE_Value, 6) 'Format(oPayment.DATE_Value, "YYMMDD")
        End If
        'Valutering mottakende bank - sett denne p� innsendingsfiler
        'hvis Date_Value er satt:
        If oPayment.StatusCode = "00" Then '
            sLine = sLine & StrDup(6, "0") '295
            'sLine = sLine & Right$(oPayment.DATE_Value, 6) '295
        Else
            sLine = sLine & StrDup(6, "0") '295
        End If

        sLine = sLine & Space(1)  '301 Slette�rsak
        sLine = sLine & Space(9)  '302
        sLine = sLine & Space(10) '311 Blankettnummer - not in use in BabelBank p.t.

        ' 30.12.2008 added 160 chars for Telepay+
        ' 321 - 335 C2B PAYMENT ID DEB , Unique payment ID debit side from C2B -  WHAT IS THIS ????
        sLine = sLine & Space(15)   ' not used so far
        ' 336 - 346 INPS DEBIT SWIFT, Identifies the payor�s bank by its SWIFT address.
        sLine = sLine & PadRight(oPayment.BANK_I_SWIFTCode, 11, " ")
        ' 347 - 381 INPS DEBIT ACCOUNT NUMBER , Payor Account Number
        ' use this one if Telepay+
        sLine = sLine & PadRight(oPayment.I_Account, 35, " ")

        ' XOKNET 27.02.2013
        ' 16.03.2018 - From Kjell Chr: Keep both BIC and sortcode, CC, etc !!!!
        '' For payments with both BIC and  babelfiles.bankbranchtype.sortcode, remove BIC, as this causes problems in DNB
        'If (oPayment.BANK_BranchType = BabelFiles.BankBranchType.SortCode Or (oPayment.BANK_BranchNo <> "" And oPayment.BANK_CountryCode = "GB")) And Not EmptyString(oPayment.BANK_SWIFTCode) Then
        '    ' remove BIC
        '    oPayment.BANK_SWIFTCode = ""
        '    oPayment.BANK_BranchType = BabelFiles.BankBranchType.SortCode
        'End If
        '' XOKNET 27.02.2013
        '' For payments with both BIC and CanadaClearing, remove BIC, as this causes problems in DNB
        'If (oPayment.BANK_BranchType = BabelFiles.BankBranchType.CC Or (oPayment.BANK_BranchNo <> "" And oPayment.BANK_CountryCode = "CA")) And Not EmptyString(oPayment.BANK_SWIFTCode) Then
        '    ' remove BIC
        '    oPayment.BANK_SWIFTCode = ""
        '    oPayment.BANK_BranchType = BabelFiles.BankBranchType.CC
        'End If
        '' XOKNET 27.02.2013
        '' For payments with both BIC and Australian  babelfiles.bankbranchtype.bsb, remove BIC, as this causes problems in DNB
        'If (oPayment.BANK_BranchType = BabelFiles.BankBranchType.BSB Or (oPayment.BANK_BranchNo <> "" And oPayment.BANK_CountryCode = "AU")) And Not EmptyString(oPayment.BANK_SWIFTCode) Then
        '    ' remove BIC
        '    oPayment.BANK_SWIFTCode = ""
        '    oPayment.BANK_BranchType = BabelFiles.BankBranchType.BSB
        'End If
        '' XOKNET 27.02.2013
        '' For payments with both BIC and babelfiles.bankbranchtype.fedwire, remove BIC, as this causes problems in DNB
        'If (oPayment.BANK_BranchType = BabelFiles.BankBranchType.Fedwire Or (oPayment.BANK_BranchNo <> "" And oPayment.BANK_CountryCode = "US")) And Not EmptyString(oPayment.BANK_SWIFTCode) Then
        '    ' remove BIC
        '    oPayment.BANK_SWIFTCode = ""
        '    oPayment.BANK_BranchType = BabelFiles.BankBranchType.Fedwire
        'End If

        ' 382 - 392 INPS CREDIT SWIFT, Identifies the payee's bank by its SWIFT address.
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCode, 11, " ")
        ' 393 - 427 INPS CREDIT ACCOUNT NUMBER, Payee's Account Number
        ' added 14.09.2009 - Must test for empty to account, and put 00000000019 as accountno
        If (oPayment.E_Account = "00000000000" Or Len(oPayment.E_Account) = 0 Or _
            oPayment.PayCode = "160") And Not oPayment.ToOwnAccount Then
            ' XokNET 30.03.2011 - added And Not oPayment.ToOwnAccount
            sLine = sLine & PadRight("00000000019", 35, " ")
        Else
            sLine = sLine & PadRight(oPayment.E_Account, 35, " ")
        End If
        ' 428 - 442 LUMP_SUM_REF, This reference is populated for grouping purpose, in order to have Telepay book a lump sum.
        sLine = sLine & Space(15)
        ' 443 ONHOLD-INFO-IN-C2B WHAT IS THIS ????
        sLine = sLine & Space(1)

        ' next 2 fields added 05.02.2009
        ' CURRENCY    444;446 3   ALPHA
        sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ")
        ' PAYMENT CODE    447;448 2   ALPHA   Equal to transaction type in TBI+  21Z pos. 129;130. Indicates payment type. E.g FI 71 = 21
        ' Must use a converstiontable between BabelBank codes and TBI-codes
        ' XOKNET 03.01.2013 added parameter for paytype tp bbGetPayCodeTelepayPlus
        'sLine = sLine & bbGetPayCodeTelepayPlus(oPayment, "D") '447-448
        sLine = sLine & bbGetPayCodeTelepayPlus(oPayment, "D")

        ' 23.10.2009
        ' Added an extra Zip (Postcode) field, to handle long zips, like Swedish 5 digits
        sLine = sLine & PadRight(oPayment.E_Zip, 9, " ") ' 449-457

        ' XOKNET 12.11.2010 Changes from here to end of function
        sLine = sLine & PadRight(oBatch.REF_Own, 35, " ") ' OWN REFERENCE ORDER (B-Level)   458;492 35  ALPHA
        sLine = sLine & PadRight(oPayment.E_CountryCode, 2, " ")  ' Beneficiary Country code    493;494 2   ALPHA   Country Code BEN
        sLine = sLine & Space(132)  'Reserved    495;626

        If Not EmptyString(oPayment.REF_EndToEnd) Then
            sLine = sLine & PadRight(oPayment.REF_EndToEnd, 35, " ")  ' Posteringstekst/Ben. Ref. /End to end ref.  627;661
            ' XOKNET 06.05.2012 - use Text_E_Statement as end_to_endref if no EndToEndref
        ElseIf Not EmptyString(oPayment.Text_E_Statement) Then
            sLine = sLine & PadRight(oPayment.Text_E_Statement, 35, " ")  ' Posteringstekst/Ben. Ref. /End to end ref.  627;661
        Else
            sLine = sLine & Space(35)  ' Posteringstekst/Ben. Ref. /End to end ref.  627;661
        End If
        sLine = sLine & "IN"  'Tilgangstype/Access type    662;663 IN for Domestic
        sLine = sLine & PadRight(oPayment.REF_Own, 35, " ")    'Customer own referance(C-level) 664;698 35

        ' XOKNET 13.12.2010, added next "block", ta have prefix for branchcode
        ' Reserved space for branchNo, 245,15
        ' In Telepay format, must add SC, FW, etc
        'XOKNET 29.02.2012 -
        ' It is better to NOT change oPayment.BANK_BranchNo, because this destroys the errorhandling later on
        ' Instead, add FW, SC, etc to sLine !!!

        ' XOKNET 14.08.2013 - Bankcode extended from 15 to 23, now is from 699 - 721
        ' changes in several lines below
        Select Case oPayment.BANK_BranchType
            Case BabelFiles.BankBranchType.Fedwire
                sLine = sLine & PadRight("FW" & oPayment.BANK_BranchNo, 23, " ")
            Case BabelFiles.BankBranchType.SortCode
                sLine = sLine & PadRight("SC" & oPayment.BANK_BranchNo, 23, " ")
            Case BabelFiles.BankBranchType.Bankleitzahl
                sLine = sLine & PadRight("BL" & oPayment.BANK_BranchNo, 23, " ")
            Case BabelFiles.BankBranchType.Chips
                sLine = sLine & PadRight("CH" & oPayment.BANK_BranchNo, 23, " ")
            Case BabelFiles.BankBranchType.CC
                sLine = sLine & PadRight("CC" & oPayment.BANK_BranchNo, 23, " ")
                ' XOKNET 23.02.2013 (San Juan) added  babelfiles.bankbranchtype.bsb, to have AU in front of bankcode for Australia
            Case BabelFiles.BankBranchType.BSB
                sLine = sLine & PadRight("AU" & oPayment.BANK_BranchNo, 23, " ")
            Case Else
                sLine = sLine & PadRight(oPayment.BANK_BranchNo, 23, " ")
        End Select

        ' XOKNET 14.08.2013 - added Priorityflag in pos 725
        'sLine = sLine & Space(87)    ' RESERVED    714:800 87  ALPHA
        sLine = sLine & Space(3)        ' 722-724
        ' XokNET 02.05.2014 Changes from here to end of function
        If oPayment.Priority Then
            sLine = sLine & "J" '725
        Else
            sLine = sLine & " " '725
        End If
        ' Category purpose
        sLine = sLine & PadRight(oPayment.CategoryPurposeCode, 4, " ") ' 726-729
        ' Purposecode
        sLine = sLine & PadRight(oPayment.PurposeCode, 4, " ")  '730-733

        sLine = sLine & Space(67) ' 734-800

        WriteBETFOR21Invoice = sLine
    End Function
    Private Function WriteBETFOR21Mass(ByVal oPayment As Payment, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String, ByVal nTransferredAmount As Double, ByVal iFormat_ID As Integer) As String

        Dim sLine As String
        Dim sPayCode As String ', iCount As Integer
        Dim sErrorMessage As String

        ' Reset some controlvalues for each payment:
        '------------------------------------
        iStructuredPayment = -1
        iNoOfBETFOR22_23 = 0
        iNoOfTextLinesInBETFOR23 = 0

        sLine = vbNullString
        'FIX: Have to add some parameters, i.e. Returncode
        sLine = ApplicationHeader(False, oPayment.StatusCode)
        sLine = sLine & "BETFOR21"
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo  '49
        End If

        sPayCode = bbGetPayCode(Trim(oPayment.PayCode), "TELEPAY", "M")

        If sPayCode = "621" Then
            ' Mass payments, use Ownref as text on receivers statement
            ' Need to put Ownref into Babels Text_E_Statement
            If Len(oPayment.Text_E_Statement) > 0 Then
                oPayment.REF_Own = oPayment.Text_E_Statement
            End If
        End If

        ' 04.08.2010 changed next if tp include swift DNBNO
        If Left$(oPayment.BANK_I_SWIFTCode, 6) <> "DNBANO" Then  ' 30.12.2008 If TelepayPlus, account is in pos 347
            sLine = sLine & StrDup(11, "0") '60
        Else
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")  '60
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")  '75

        ' 04.05.06 Added new test about date
        If oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateAll Then
            ' Set all dates to todays date
            ' 04.01.2008
            ' Changed to give us next bankday, instead of today.
            oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "NO", 0)) 'datetostring(Date)
        End If

        ' 04.05.06 Added new test about date
        If StringToDate(oPayment.DATE_Payment) <= Date.Today Then
            If oPayment.VB_Profile.FileSetups(iFormat_ID).ChangeDateDue Then
                ' Set date to todays date
                ' 04.01.2008
                ' Changed to give us next bankday, instead of today.
                oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "NO", 0)) 'datetostring(Date)
            End If
        End If

        sLine = sLine & Right$(oPayment.DATE_Payment, 6) '81
        ' XokNET 15.11.2011 -
        ' oPayment.Ref_own in masspayments are normally not a good thing, since they normally will be info for first mass (first betfor22)
        'sLine = sLine & Left$(oPayment.REF_Own + Space(30), 30) '87
        sLine = sLine & Left$(Format(Now, "yyyyMMdd_hhmmss") + Space(30), 30)
        sLine = sLine & Space(1) '117
        sLine = sLine & "00000000000" '118
        ' added 04.05.06 - Check accountno
        If oPayment.VB_Profile.FileSetups(iFormat_ID).CtrlAccount Then
            If Not CheckAccountNoNOR(oPayment.E_Account) Then
                sErrorMessage = LRS(35047, oPayment.E_Account, "TelepayPlus")
                ' XNET 26.11.2013 Uses %1 properly
                sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, vbNullString) & sImportFilename
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, vbNullString) & oPayment.MON_InvoiceAmount / 100
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, vbNullString) & oPayment.E_Name
                Err.Raise(35047, , sErrorMessage)
            End If
        End If

        sLine = sLine & Space(30) '129
        sLine = sLine & Space(30) '159
        sLine = sLine & Space(30) '189
        sLine = sLine & "0000" '219
        sLine = sLine & Space(26) '223
        'The amount if its a payment to own account
        sLine = sLine & StrDup(15, "0") '249

        sLine = sLine & sPayCode '264
        If oPayment.PayType = "S" Then 'Salary
            sLine = sLine & "L"  '267
        Else
            sLine = sLine & "M"
        End If
        If oPayment.Cancel = True Then
            sLine = sLine & "S"  '268
        Else
            sLine = sLine & Space(1)
        End If
        If oPayment.StatusCode <> "02" Then 'No amount in betfor21 in sendfiles
            sLine = sLine & StrDup(15, "0") ' 269
        Else
            sLine = sLine & PadLeft(Str(nTransferredAmount), 15, "0")
        End If
        If Len(oPayment.I_Client) > 0 Then
            sLine = sLine & PadRight(oPayment.I_Client, 5, " ")  '284
        Else
            sLine = sLine & StrDup(5, "0") '284 Klientreferanse
        End If

        If oPayment.DATE_Value = "19900101" Or oPayment.StatusCode = "00" Then
            sLine = sLine & StrDup(6, "0") '289
        Else
            sLine = sLine & Right$(oPayment.DATE_Value, 6) 'Format(oPayment.DATE_Value, "YYMMDD")
        End If

        'Valutering mottakende bank - sett denne p� innsendingsfiler
        'hvis Date_Value er satt:
        If oPayment.StatusCode = "00" Then '
            'sLine = sLine & Right$(oPayment.DATE_Value, 6) '295
            sLine = sLine & StrDup(6, "0") '295
        Else
            sLine = sLine & StrDup(6, "0") '295
        End If

        sLine = sLine & Space(1)  '301 Slette�rsak
        sLine = sLine & Space(9)  '302
        sLine = sLine & Space(10) '311 Blankettnummer - not in use in BabelBank p.t.

        ' 30.12.2008 added 160 chars for Telepay+
        ' 321 - 335 C2B PAYMENT ID DEB , Unique payment ID debit side from C2B -  WHAT IS THIS ????
        sLine = sLine & Space(15)   ' not used so far
        ' 336 - 346 INPS DEBIT SWIFT, Identifies the payor�s bank by its SWIFT address.
        sLine = sLine & PadRight(oPayment.BANK_I_SWIFTCode, 11, " ")
        ' 347 - 381 PAYORS (sender, payer) ACCOUNT NUMBER , Payor Account Number
        ' use this one if Telepay+
        sLine = sLine & PadRight(oPayment.I_Account, 35, " ")
        ' 382 - 392 INPS CREDIT SWIFT, Identifies the payee�s (recievers) bank by its SWIFT address.
        'XOKNET 17.12.2010 removed next line, replaced with spaces
        'sLine = sLine & PadRight(oPayment.BANK_SWIFTCode, 11, " ")  '' H�RES HELT SPR�TT UT !!
        sLine = sLine & Space(11)
        ' 393 - 427 INPS CREDIT ACCOUNT NUMBER , Payees (recievers) Account Number
        ' use this one if Telepay+
        'sLine = sLine & PadRight(oPayment.E_Account, 35, " ")  ' H�RES HELT SPR�TT UT !!
        ' jps 15.05.2009 Yes, det var helt spr�tt! Det skal ikke v�re noe her for massebetalinger !!!!
        sLine = sLine & Space(35)
        ' 428 - 442 LUMP_SUM_REF - This reference is populated for grouping purpose, in order to have Telepay book a lump sum.
        ' What to put here? Total for payment ?
        'sLine = sLine & PadLeft(Str(nTransferredAmount), 15, "0")
        ' XOKNET 15.03.2012 - Lumpsumref er Alpha, causes problems at DNB, add spaces !
        sLine = sLine & Space(15)

        ' 443 ONHOLD-INFO-IN-C2B WHAT IS THIS ????
        sLine = sLine & Space(1)

        ' next 2 fields added 05.02.2009
        ' CURRENCY    444;446 3   ALPHA
        sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ")

        ' PAYMENT CODE    447;448 2   ALPHA   Equal to transaction type in TBI+  21Z pos. 129;130. Indicates payment type. E.g FI 71 = 21
        ' Must use a converstiontable between BabelBank codes and TBI-codes
        ' XOKNET 03.01.2013 added parameter for paytype tp bbGetPayCodeTelepayPlus
        'sLine = sLine & bbGetPayCodeTelepayPlus(oPayment, "M")
        sLine = sLine & bbGetPayCodeTelepayPlus(oPayment, "M")

        ' XOKNET 12.11.2010 Changes from here to end of function
        'XOKNET 17.12.2010 removed next line, replaced with spaces
        'sLine = sLine & PadRight(oPayment.E_Zip, 9, " ")  ' POSTCODE    449;457 9   ALPHA
        sLine = sLine & Space(9)          ' POSTCODE    449;457 9   ALPHA

        ' XokNET 15.11.2011 -
        ' oPayment.Ref_own in masspayments are normally not a good thing, since they normally will be info for first mass (first betfor22)
        'sLine = sLine & PadRight(oPayment.REF_Own, 35, " ")  ' OWN REFERENCE ORDER (B-Level)   458;492 35  ALPHA
        sLine = sLine & PadRight(Format(Now, "yyyyMMdd_hhmmss"), 35, " ")  ' OWN REFERENCE ORDER (B-Level)   458;492 35  ALPHA
        sLine = sLine & PadRight(oPayment.E_CountryCode, 2, " ")  ' Beneficiary Country code    493;494 2   ALPHA   Country Code BEN
        sLine = sLine & Space(132)   ' Reserved    495;626 132     Reserved until the HUB changes it's format

        If Not EmptyString(oPayment.REF_EndToEnd) Then
            sLine = sLine & PadRight(oPayment.REF_EndToEnd, 35, " ")  ' Posteringstekst/Ben. Ref. /End to end ref.  627;661
        Else
            ' XokNET 15.11.2011 -
            ' oPayment.Ref_own in masspayments are normally not a good thing, since they normally will be info for first mass (first betfor22)
            'sLine = sLine & PadRight(oPayment.REF_Own, 35, " ")  ' Posteringstekst/Ben. Ref. /End to end ref.  627;661
            sLine = sLine & PadRight(Format(Now, "yyyyMMdd_hhmmss"), 35, " ")  ' Posteringstekst/Ben. Ref. /End to end ref.  627;661
        End If
        sLine = sLine & "IN"  'Tilgangstype/Access type    662;663 IN for Domestic
        ' XokNET 15.11.2011 -
        ' oPayment.Ref_own in masspayments are normally not a good thing, since they normally will be info for first mass (first betfor22)
        'sLine = sLine & PadRight(oPayment.REF_Own, 35, " ")  ' Customer own referance(C-level) 664;698 35
        sLine = sLine & PadRight(Format(Now, "yyyyMMdd_hhmmss"), 35, " ")  ' Customer own referance(C-level) 664;698 35

        ' XOKNET 13.12.2010, added next "block", ta have prefix for branchcode
        ' Reserved space for branchNo, 245,15
        ' In Telepay format, must add SC, FW, etc
        'XOKNET 29.02.2012 -
        ' It is better to NOT change oPayment.BANK_BranchNo, because this destroys the errorhandling later on
        ' Instead, add FW, SC, etc to sLine !!!
        Select Case oPayment.BANK_BranchType
            Case BabelFiles.BankBranchType.Fedwire
                sLine = sLine & PadRight("FW" & oPayment.BANK_BranchNo, 15, " ")
            Case BabelFiles.BankBranchType.SortCode
                sLine = sLine & PadRight("SC" & oPayment.BANK_BranchNo, 15, " ")
            Case BabelFiles.BankBranchType.Bankleitzahl
                sLine = sLine & PadRight("BL" & oPayment.BANK_BranchNo, 15, " ")
            Case BabelFiles.BankBranchType.Chips
                sLine = sLine & PadRight("CH" & oPayment.BANK_BranchNo, 15, " ")
            Case BabelFiles.BankBranchType.CC
                sLine = sLine & PadRight("CC" & oPayment.BANK_BranchNo, 15, " ")
            Case Else
                sLine = sLine & PadRight(oPayment.BANK_BranchNo, 15, " ")
        End Select
        ' old code
        'Select Case oPayment.BANK_BranchType
        'Case babelfiles.bankbranchtype.fedwire
        '    oPayment.BANK_BranchNo = "FW" & oPayment.BANK_BranchNo
        'Case  babelfiles.bankbranchtype.sortcode
        '    oPayment.BANK_BranchNo = "SC" & oPayment.BANK_BranchNo
        'Case  babelfiles.bankbranchtype.Bankleitzahl
        '    oPayment.BANK_BranchNo = "BL" & oPayment.BANK_BranchNo
        'Case  babelfiles.bankbranchtype.chips
        '    oPayment.BANK_BranchNo = "CH" & oPayment.BANK_BranchNo
        '' XokNET 23.01.2012 Added babelfiles.bankbranchtype.cc
        'Case babelfiles.bankbranchtype.cc
        '    oPayment.BANK_BranchNo = "CC" & oPayment.BANK_BranchNo
        'End Select
        '' XokNET 15.11.2011 - no use for bankcode in BETFOR21 mass
        ''sLine = sLine & PadRight(oPayment.BANK_BranchNo, 15, " ")   'BANK CODE   699:713 15  ALPHA
        ' end old code

        ' XOKNET 21.08.2012 - Enig Isingrud - linjen under skal ikke v�re med. Den lagde ingen feil, da vi kapper senere, men fjernes n�.
        'sLine = sLine & Space(15)   'BANK CODE    699:713 15  ALPHA

        sLine = sLine & Space(87)   ' 714-800

        WriteBETFOR21Mass = UCase(sLine)

    End Function
    Private Function WriteBETFOR35(ByVal oPayment As Payment, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String, ByVal bMassPayment As Boolean) As String
        Dim sAccount As String
        Dim sLine As String

        sLine = vbNullString
        sLine = ApplicationHeader(False, oPayment.StatusCode)
        sLine = sLine & "BETFOR35"
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        If Left$(oPayment.BANK_I_SWIFTCode, 6) <> "DNBANO" Then
            sLine = sLine & "00000000000"
        Else
            ' Telepay "classic"
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        End If

        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")  '75

        ' 81-452:
        ' Details about Ultimate debtor and utliamte creditor
        ' XNET 02.05.2014 added Ultimate debitor and ultimate creditor
        sLine = sLine & PadRight(oPayment.UltimateI_Name, 35, " ") 'Ordering Customer/Ultimate Debitor-name 81;115  35  ALPHA
        sLine = sLine & Space(205) '116-320
        If Not bMassPayment Then
            sLine = sLine & PadRight(oPayment.UltimateE_Name, 35, " ") 'Utlimate Creditor Name  321;355 35  ALPHA
        Else
            sLine = sLine & Space(35) 'Utlimate Creditor Name  321;355 35  ALPHA
        End If
        sLine = sLine & Space(97) '356-452

        sLine = sLine & PadRight(oPayment.E_Name, 35, " ") '453 Name
        sLine = sLine & PadRight(oPayment.E_Adr1, 35, " ") '488 Address1
        sLine = sLine & PadRight(oPayment.E_Adr2, 35, " ") '523 Address2
        ' can have either City or Adr3 from import. Depends on the importformat
        If Not EmptyString(oPayment.E_City) Then
            sLine = sLine & PadRight(oPayment.E_City, 35, " ") '558 City
        Else
            sLine = sLine & PadRight(oPayment.E_Adr3, 35, " ") '558 City
        End If
        ' XOKNET 11.06.2015 DNB added extended UltimateDebtor
        sLine = sLine & PadRight(oPayment.UltimateI_Name, 140, " ") 'Ultimate Debtor Name    593;732 140 *ALPHA
        sLine = sLine & Space(68) '733

        WriteBETFOR35 = UCase(sLine)


    End Function

    ' XokNET 20.01.2012 - Added BETFOR36
    Private Function WriteBETFOR36(ByVal oPayment As Payment, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String) As String
        Dim sAccount As String
        Dim sLine As String

        sLine = vbNullString
        sLine = ApplicationHeader(False, oPayment.StatusCode)
        sLine = sLine & "BETFOR36"
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo
        End If
        ' 04.08.2010 changed next if tp include swift DNBNO
        If Left$(oPayment.BANK_I_SWIFTCode, 6) <> "DNBANO" Then  ' 30.12.2008 If TelepayPlus, account is in pos 347
            ' TKIO-format has I_Account in BETFOR02, pos 245-279
            sLine = sLine & "00000000000"
        Else
            ' Telepay "classic"
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        End If

        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")  '75

        ' Credit bank
        sLine = sLine & PadRight(oPayment.BANK_Name, 35, " ") '81
        sLine = sLine & PadRight(oPayment.BANK_Adr1, 35, " ") '116
        sLine = sLine & PadRight(oPayment.BANK_Adr2, 35, " ") '151
        sLine = sLine & PadRight(oPayment.BANK_Adr3, 35, " ") '186
        If Len(oPayment.BANK_CountryCode) <> 2 Then
            sLine = sLine & "  " '221
        Else
            sLine = sLine & oPayment.BANK_CountryCode   '221
        End If

        ' Intermediary bank
        sLine = sLine & PadRight(oPayment.BANK_NameAddressCorrBank1, 35, " ") '223
        sLine = sLine & PadRight(oPayment.BANK_NameAddressCorrBank2, 35, " ") '258
        sLine = sLine & PadRight(oPayment.BANK_NameAddressCorrBank3, 35, " ") '293
        sLine = sLine & PadRight(oPayment.BANK_NameAddressCorrBank4, 35, " ") '338

        ' In Telepay format, must add SC, FW, etc
        Select Case oPayment.BANK_BranchNoCorrBank
            Case BabelFiles.BankBranchType.Fedwire
                oPayment.BANK_BranchNoCorrBank = "FW" & oPayment.BANK_BranchNo
            Case BabelFiles.BankBranchType.SortCode
                oPayment.BANK_BranchNoCorrBank = "SC" & oPayment.BANK_BranchNo
            Case BabelFiles.BankBranchType.Bankleitzahl
                oPayment.BANK_BranchNoCorrBank = "BL" & oPayment.BANK_BranchNo
            Case BabelFiles.BankBranchType.Chips
                oPayment.BANK_BranchNoCorrBank = "CH" & oPayment.BANK_BranchNo
            Case BabelFiles.BankBranchType.CC
                oPayment.BANK_BranchNoCorrBank = "CC" & oPayment.BANK_BranchNo
        End Select
        sLine = sLine & PadRight(oPayment.BANK_BranchNoCorrBank, 23, " ")             '363
        sLine = sLine & PadRight(oPayment.BANK_SWIFTCodeCorrBank, 11, " ")            '386

        ' Charges
        If oPayment.MON_ChargeMeAbroad = True Then
            sLine = sLine & "OUR"   ' 397
        Else
            sLine = sLine & "BEN"   ' 397
        End If
        If oPayment.MON_ChargeMeDomestic = True Then
            sLine = sLine & "OUR"   ' 400
        Else
            sLine = sLine & "BEN"   ' 400
        End If

        sLine = sLine & PadRight(oPayment.ERA_DealMadeWith, 35, " ") 'DEAL MADE WITH  403;437
        sLine = sLine & PadRight(oPayment.ERA_ExchRateAgreed, 8, " ") 'AGREED EXCHANGE RATE    438;445
        sLine = sLine & PadRight(oPayment.FRW_ForwardContractNo, 20, " ") ''FORWARD CONTRACT NO.    446;465
        sLine = sLine & PadRight(oPayment.NOTI_NotificationType, 1, " ")  'Mesage type 466;466
        sLine = sLine & PadRight(oPayment.NOTI_NotificationAttention, 100, " ") 'Message address 467;516 + 517;566
        sLine = sLine & PadRight(oPayment.NOTI_NotificationMessageToBank, 140, " ") 'Remitting bank message  567;706
        If Len(oPayment.NOTI_NotificationType) > 1 Then ' PHONE, TELEX, OTHER
            sLine = sLine & PadRight(oPayment.NOTI_NotificationType & oPayment.NOTI_NotificationIdent, 30, " ") 'NOTIFICATION INDICATOR  707;736
        Else
            sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 30, " ")  'NOTIFICATION INDICATOR  707;736
        End If
        ' XOKNET 09.02.2012, added
        ' Intermediary Bank address, Country Code 737;738 2
        sLine = sLine & PadRight(oPayment.BANK_CountryCodeCorrBank, 2, " ")
        ' XOKNET 09.02.2012, changed from 64
        sLine = sLine & Space(62) '739

        WriteBETFOR36 = UCase(sLine)


    End Function

    Private Function WriteBETFOR23(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal iSerialNo As Double, ByRef iCount As Integer, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String, ByVal iFormat_ID As Integer, ByVal bForceText As Boolean) As String

        Dim sLine As String
        'Dim oFreeText As FreeText
        Dim iCountLocal As Integer
        Dim bLocalStructured As Boolean
        Dim sErrorMessage As String
        Dim bWriteAmountFor73 As Boolean

        ' Max 999 BETFOR23 (added 03.05.06)
        iNoOfBETFOR22_23 = iNoOfBETFOR22_23 + 1
        If iNoOfBETFOR22_23 > 999 Then
            If oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong Then  ' 22.05.06 added if test
                sErrorMessage = LRS(35043, oPayment.MON_InvoiceCurrency, "TelepayPlus")
                ' XNET 26.11.2013 Uses %1 properly
                sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, vbNullString) & sImportFilename
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, vbNullString) & oPayment.MON_InvoiceAmount / 100
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, vbNullString) & oPayment.E_Name
                Err.Raise(35043, , sErrorMessage)
            End If
        End If

        sLine = vbNullString
        sLine = ApplicationHeader(False, oInvoice.StatusCode)
        sLine = sLine & "BETFOR23"  '41
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo  '49
        End If
        ' 04.08.2010 changed next if tp include swift DNBNO
        If Left$(oPayment.BANK_I_SWIFTCode, 6) <> "DNBANO" Then  ' 30.12.2008 If TelepayPlus, account is in pos 347
            sLine = sLine & StrDup(11, "0")                                '60
        Else
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")          '60
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        If oInvoice.StatusCode = "00" Then
            sLine = sLine & Space(6)  '75
        Else
            sLine = sLine & PadRight(oInvoice.Invoice_ID, 6, " ")  '75
        End If

        'Can't have both Freetext and KID
        iCountLocal = 0

        ' 05.02.2009 - added bForceText, to be able to for FI 73/75 or Giro01 to also have a BETFOR23 with notificationtext
        If Not EmptyString(oInvoice.Unique_Id) And bForceText = False Then
            'KID stated
            sLine = sLine & Space(120) '81
            ' 09.08.2010 - changed next if, to reflect differences between Norwegian KIDs and Danish FIKs
            ' 26.08.2010 added test for "SE"
            ' XokNET 02.11.2011 added test for "FI
            If oPayment.BANK_AccountCountryCode <> "NO" And oPayment.BANK_AccountCountryCode <> "SE" _
            And oPayment.BANK_AccountCountryCode <> "FI" Then
                ' 05.02.2009 for Telepayplus, do not include fi-type (71,73,75, or Giro 01,04,15) in Telepayfile
                sLine = sLine & PadRight(Mid$(oInvoice.Unique_Id, 3), 27, " ") '201
            Else
                ' Norway domestic; use all from Unique ID
                ' XokNET 02.11.2011 OR NO OR SE domestic OR FI domestic
                sLine = sLine & PadRight(oInvoice.Unique_Id, 27, " ")  '201
            End If
            'To get out of the loop in WriteTelepay2File
            ' 02.07.2009 - having problems with FIK73 in Telepay+
            If Left$(oInvoice.Unique_Id, 2) = "75" Then
                ' do not add to icount, because this gives us problem whit amount = 0.00 further down !!
            Else
                ' as pre 02.07.2009
                iCount = oInvoice.Freetexts.Count + 2
            End If
            bLocalStructured = True

        ElseIf Not EmptyString(oInvoice.InvoiceNo) And bForceText = False Then
            'Invoiceno stated, and no KID
            sLine = sLine & Space(147)   '81
            ' To get out of the loop in WriteTelepay2File
            ' Creates error if more than two freetexts in use !!! iCount = oInvoice.Freetexts.Count + 2
            bLocalStructured = True

        Else
            'NO KID or invoiceno stated
            If oInvoice.Freetexts.Count >= iCount Then
                For iCountLocal = 1 To 3
                    If oInvoice.Freetexts.Count >= iCount Then
                        ' 20.09.05 -  Changed, in case freetext.text is not 40 pos
                        'sLine = sLine & oInvoice.Freetexts(iCount).Text
                        sLine = sLine & PadRight(Left$(oInvoice.Freetexts(iCount).Text, 40), 40, " ")
                        iCount = iCount + 1
                        iNoOfTextLinesInBETFOR23 = iNoOfTextLinesInBETFOR23 + 1
                    End If
                Next iCountLocal
                sLine = PadLine(sLine, 227, " ") '81 - 227
                ' Added new test of 25 * 40 chars text in BETFOR23
                If iNoOfTextLinesInBETFOR23 > 25 Then
                    If oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong Then  ' 22.05.06 added if test
                        ' Reject!
                        sErrorMessage = LRS(35045, oPayment.MON_InvoiceCurrency, "TelepayPlus")
                        ' XNET 26.11.2013 Uses %1 properly
                        sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, vbNullString) & sImportFilename
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, vbNullString) & oPayment.MON_InvoiceAmount / 100
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                        sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, vbNullString) & oPayment.E_Name
                        Err.Raise(35045, , sErrorMessage)
                    End If
                End If
            Else
                'If no freetext is given, then add a "."
                sLine = sLine & "." & Space(146)  '81
            End If
            bLocalStructured = False

        End If

        ' 02.05.06 Added test for mix of structured/unstructured
        If iStructuredPayment > -1 Then ' iStructured=-1 for each new payment
            If iStructuredPayment = 0 And bLocalStructured = True Or _
                iStructuredPayment = 1 And bLocalStructured = False Then
                If oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong Then  ' 22.05.06 added if test
                    ' error - mix of structured/unstructured
                    sErrorMessage = LRS(35042, oPayment.MON_InvoiceCurrency, "TelepayPlus")
                    ' XNET 26.11.2013 Uses %1 properly
                    sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, vbNullString) & sImportFilename
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, vbNullString) & oPayment.MON_InvoiceAmount / 100
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                    sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, vbNullString) & oPayment.E_Name
                    Err.Raise(35042, , sErrorMessage)
                End If
            End If
        End If
        '---------- end test mix -------------------------------
        If bLocalStructured = True Then
            iStructuredPayment = 1
        Else
            iStructuredPayment = 0
        End If

        sLine = sLine & Left$(oInvoice.REF_Own + Space(30), 30) '228 Egenref

        ' 02.07.2009 added next iftest for FIK75
        If Left$(oInvoice.Unique_Id, 2) = "75" And bLocalStructured = False Then
            sLine = sLine & New String("0", 15)
        ElseIf iCount > 4 Then
            sLine = sLine & New String("0", 15)
        Else
            ' XokNET 15.08.2014 -
            ' Kontrabeskjed fra DNB - Bel�pet skal st� i BETFOR21 for betaling til egen konto - og KUN i BETFOR21
            If oPayment.ToOwnAccount = True Then
                sLine = sLine & New String("0", 15) '258
            Else
                sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0") '258
            End If
        End If
        If iCount > 4 Then
            sLine = sLine & "D"  '273
        ElseIf oInvoice.MON_InvoiceAmount < 0 Then
            sLine = sLine & "K"
        Else
            sLine = sLine & "D"
        End If
        If Len(Trim$(oInvoice.InvoiceNo)) > 0 And Len(Trim$(oInvoice.Unique_Id)) = 0 Then
            ' Use Fakturanr only when KID is not in use
            sLine = sLine & PadRight(oInvoice.InvoiceNo, 20, " ") '274
        Else
            sLine = sLine & Space(20) '274
        End If

        If oInvoice.StatusCode = "00" Then
            sLine = sLine & "000"  '294
        ElseIf oInvoice.StatusCode = vbNullString Then
            'The imported file was not Telepay
            If Not bFileFromBank = True Then
                sLine = sLine & "000"
            Else
                sLine = sLine & PadLeft(Str(iSerialNo), 3, "0")
            End If
        Else
            sLine = sLine & PadLeft(Str(iSerialNo), 3, "0")
        End If
        sLine = sLine & Space(1)   ' 297 Slette�rsak, not implemented

        If Len(Trim$(oInvoice.CustomerNo)) > 0 And Len(Trim$(oInvoice.Unique_Id)) = 0 Then
            ' Use Kundenr only when KID is not in use
            sLine = sLine & PadRight(oInvoice.CustomerNo, 15, " ") '298
        Else
            sLine = sLine & Space(15) '298
        End If

        'sLine = sLine & Space(8)  '313 Fakturadato - not implemented
        ' XOKNET 14.12.2010 Added Invoicedate
        sLine = sLine & PadRight(oInvoice.InvoiceDate, 8, " ") '313 Fakturadato -

        ' XOKNET 12.11.2010 changes from here to end of function
        sLine = sLine & Space(6)    ' Not relevant? C2B ID CRE  321;326 6   NUMERIC Unique invoice ID credit side from Corporate2Bank
        sLine = sLine & Space(132)  'Reserved    327;458 132
        sLine = sLine & Space(342)  'RESERVED    459;800 342 ALPHA


        WriteBETFOR23 = UCase(sLine)


    End Function
    Private Function WriteBETFOR22(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal iSerialNo As Double, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String, ByVal iFormat_ID As Integer) As String

        Dim sLine As String
        Dim sErrorMessage As String
        ' XOKNET 12.11.2010
        Dim oFreetext As Freetext
        Dim sTmp As String

        ' Max 9999 BETFOR22
        iNoOfBETFOR22_23 = iNoOfBETFOR22_23 + 1
        If iNoOfBETFOR22_23 > 9999 Then
            If oPayment.VB_Profile.FileSetups(iFormat_ID).RejectWrong Then  ' 22.05.06 added if test
                sErrorMessage = LRS(35044, oPayment.MON_InvoiceCurrency, "TelepayPlus")
                ' XNET 26.11.2013 Uses %1 properly
                sErrorMessage = sErrorMessage & vbCrLf & vbCrLf & LRS(35001, vbNullString) & sImportFilename
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35030) & oPayment.I_Account
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35032) & oPayment.REF_Own
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35002, vbNullString) & oPayment.MON_InvoiceAmount / 100
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35031) & oPayment.E_Account
                sErrorMessage = sErrorMessage & vbCrLf & LRS(35003, vbNullString) & oPayment.E_Name
                Err.Raise(35044, , sErrorMessage)
            End If
        End If

        sLine = vbNullString
        sLine = ApplicationHeader(False, oInvoice.StatusCode)
        sLine = sLine & "BETFOR22"  '41
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo  '49
        End If
        ' 04.08.2010 changed next if tp include swift DNBNO
        If Left$(oPayment.BANK_I_SWIFTCode, 6) <> "DNBANO" Then ' 30.12.2008 If TelepayPlus, account is in pos 347
            sLine = sLine & StrDup(11, "0")                                '60
        Else
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")          '60
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        If oInvoice.StatusCode = "00" Then
            sLine = sLine & Space(6)                           '75
        Else
            sLine = sLine & PadLeft(oInvoice.Invoice_ID, 6, " ")   '75
        End If
        ' XokNET 15.11.2011
        ' Only Norwegian accounts in pos 81
        If oPayment.BANK_CountryCode = "NO" Then
            sLine = sLine & PadRight(oPayment.E_Account, 11, " ")  '81
        Else
            sLine = sLine & StrDup(11, "0") '81
        End If

        sLine = sLine & PadRight(oPayment.E_Name, 30, " ")     '92
        sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0") '122
        If oPayment.Cancel = True Then
            sLine = sLine & "S"  '137
        Else
            sLine = sLine & Space(1)
        End If
        sLine = sLine & Left$(oInvoice.REF_Own + Space(35), 35)  '138
        sLine = sLine & Space(110) '173
        sLine = sLine & PadRight(oInvoice.Extra1, 10, " ") '283 Egenreferanse2
        If oInvoice.StatusCode = "00" Or Len(Trim(oInvoice.StatusCode)) = 0 Then
            sLine = sLine & "0000"  '293
        Else
            sLine = sLine & PadLeft(Str(iSerialNo), 4, "0")
        End If
        sLine = sLine & Space(1)  ' 297 Slette�rsak, not implemented
        sLine = sLine & Space(23) '

        ' 30.12.2008 added 160 chars for Telepay+
        ' 321 - 326 C2B PAYMENT ID DEB , Unique payment ID debit side from C2B -  WHAT IS THIS ????
        sLine = sLine & Space(2)   ' 321-322 not used so far
        ' Purposecode
        sLine = sLine & PadRight(oPayment.PurposeCode, 4, " ")  '323-326

        ' 327 - 337 INPS CREDIT SWIFT, Identifies the payor�s bank by its SWIFT address.
        ' XOKNET 13.12.2010 - for UK, use Branch ( babelfiles.bankbranchtype.sortcode) before BIC

        'XOKNET 06.01.2011 removed And Not emptystring(oPayment.BANK_SWIFTCode)
        'If oPayment.BANK_CountryCode = "GB" And Not emptystring(oPayment.BANK_BranchNo) Then
        ' XOKNET 15.10.2012 - must also test for channel islands
        If (oPayment.BANK_CountryCode = "GB" Or oPayment.BANK_CountryCode = "JE" Or oPayment.BANK_CountryCode = "GG" Or oPayment.BANK_CountryCode = "IM" Or oPayment.BANK_CountryCode = "GI") And Not EmptyString(oPayment.BANK_BranchNo) Then
            'If oPayment.BANK_CountryCode = "GB" And Not emptystring(oPayment.BANK_SWIFTCode) And Not emptystring(oPayment.BANK_BranchNo) Then
            ' priority on  babelfiles.bankbranchtype.sortcode;
            ' XOKNET 07.11.2011 added code for "SC"
            sLine = sLine & PadRight("SC" & Replace(oPayment.BANK_BranchNo, "SC", ""), 11, " ")
            ' XokNET 19.09.2011 Added BankID for Singapore
        ElseIf oPayment.BANK_CountryCode = "SG" And Not EmptyString(oPayment.BANK_BranchNo) Then
            sLine = sLine & PadRight(oPayment.BANK_BranchNo, 11, " ")
        Else
            ' as pre 13.12.2010
            sLine = sLine & PadRight(oPayment.BANK_SWIFTCode, 11, " ")
        End If
        ' 338 - 372 INPS CREDIT ACCOUNT NUMBER , Payor Account Number
        ' use this one if Telepay+
        sLine = sLine & PadRight(oPayment.E_Account, 35, " ")
        ' 373 ONHOLD-INFO-IN-C2B WHAT IS THIS ????
        sLine = sLine & Space(1)

        sLine = sLine & PadRight(oPayment.REF_EndToEnd, 35, " ")  ' Posteringstekst/Ben. Ref. /End to end ref.  374;408        sTmp = ""
        For Each oFreetext In oInvoice.Freetexts
            sTmp = sTmp & Trim$(oFreetext.Text)
        Next oFreetext
        sLine = sLine & PadRight(sTmp, 140, " ")  ' Remittance information  409;548 140 ALPHA
        sLine = sLine & Space(12)    'Social Security number  549;560 12  ALPHA
        sLine = sLine & PadRight(oPayment.E_Adr1, 35, " ")  '  Ben street name 561;595 35  ALPHA   If "AdrLine" (unstructured) is used in Pain.001 then
        ' XOKNET 17.12.2010 changed next line
        'sLine = sLine & Space(16)    'Ben Building number 596;611 16  ALPHA   customer can only use 2 * 35 positions. Must be
        sLine = sLine & PadRight(oPayment.E_Adr2, 16, " ")     'Ben Building number 596;611 16  ALPHA
        ' 24.02.2016
        ' We may have situations where the importformat fills Adr1-3, and not E_City.
        ' In these cases, put info from E_Adr3 into E_City
        If EmptyString(oPayment.E_City) And Not EmptyString(oPayment.E_Adr3) Then
            sLine = sLine & PadRight(oPayment.E_Adr3, 35, " ")  ' Ben. Place/city 612;646 35  ALPHA   mapped into 530;654 and 581;615
        Else
            sLine = sLine & PadRight(oPayment.E_City, 35, " ")  ' Ben. Place/city 612;646 35  ALPHA   mapped into 530;654 and 581;615
        End If
        sLine = sLine & PadRight(oPayment.E_Zip, 9, " ")  ' Ben. Postcode   647;655 9   ALPHA
        sLine = sLine & PadRight(oPayment.E_CountryCode, 2, " ") ' Ben. Country    656;657 2   ALPHA
        sLine = sLine & PadRight(oPayment.UltimateE_Name, 35, " ")    'Ultimate Creditor-name  658;692 35  ALPHA
        sLine = sLine & Space(35)    'Ultimate Creditor-street name   693;727 35  ALPHA   If "AdrLine" (unstructured) is used in Pain.001 then
        sLine = sLine & Space(16)    'Ultimate Creditor-Building number   728;743 16  ALPHA   customer can only use 2 * 35 positions. Must be
        sLine = sLine & Space(35)    'Ultimate Creditor-place/city    744;778 35  ALPHA   mapped into 530;654 and 581;615
        sLine = sLine & Space(9)    'Ultimate Creditor-postcode  779;787 9   ALPHA   When mass payments, Ultimate Creditor must be used in BETFOR22 and NOT in BETFOR35.
        sLine = sLine & Space(2)    'Ultimate Creditor-country   788;789 2   ALPHA
        sLine = sLine & Space(11)    'EPP CRE ID  790;800 11  ALPHA

        WriteBETFOR22 = UCase(sLine)

    End Function
    Private Function WriteBETFOR99(ByVal oBatch As Batch, ByVal nBETFORnumber As Double, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String, ByVal sVersion As String) As String

        Dim sLine As String
        sLine = vbNullString
        'Build AH, send Returncode
        If bLastWasTKIO Then
            ' Changed06.07.06 due to problems with Nordea-returnfiles
            '29.04.2015 - Kjell and Jan-Petter agrees that the statuscode should ALWAYS be retrieved from Batch for BETFOR00/99
            sLine = ApplicationHeader(False, oBatch.StatusCode, True, True) 'TBIO in applicationheader
            'sLine = ApplicationHeader(False, IIf(oBatch.Payments.Count > 0, oBatch.Payments(1).StatusCode, oBatch.StatusCode), True, True) 'TKIO in applicationheader
        Else
            ' Changed06.07.06 due to problems with Nordea-returnfiles

            '29.04.2015 - Kjell and Jan-Petter agrees that the statuscode should ALWAYS be retrieved from Batch for BETFOR00/99
            sLine = ApplicationHeader(False, oBatch.StatusCode, True, False)
            'sLine = ApplicationHeader(False, IIf(oBatch.Payments.Count > 0, oBatch.Payments(1).StatusCode, oBatch.StatusCode), True, False)
        End If
        sLine = sLine & "BETFOR99"  '41
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo '49
        End If
        sLine = sLine & Space(11) '60
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0")  '71
        sLine = sLine & Space(6) '75
        If oBatch.DATE_Production <> "19900101" Then
            sLine = sLine & Right$(oBatch.DATE_Production, 4) 'Format(oBatch.DATE_Production, "MMDD")
        Else
            sLine = sLine & Format(Now, "MMDD")  '81
        End If
        sLine = sLine & Space(4) '85
        sLine = sLine & Space(15) '89
        sLine = sLine & PadLeft(Str(nBETFORnumber + 1), 5, "0") '104
        'FIX: No use of AEGIS or SIGILL
        sLine = sLine & Space(163) '109
        sLine = sLine & Space(4)   '272
        sLine = sLine & Space(1)   '276
        sLine = sLine & Space(1)   '277
        sLine = sLine & Space(1)   '278
        sLine = sLine & Space(18)  '279


        ' Version software
        sLine = sLine + PadRight("BABELBANKP" + Replace(sVersion, ".", ""), 16, " ")
        sLine = PadRight(sLine, 320, " ") ' Fill until 320-record is ok

        ' XOKNET 12.11.2010 added 480 blanks for Telepay+
        sLine = sLine & Space(480)

        WriteBETFOR99 = (sLine)

    End Function
    Function ApplicationHeader(ByVal bFirstLine As Boolean, ByVal sStatusCode As String, Optional ByVal bISBetfor99 As Boolean = False, Optional ByVal TKIOfor99 As Boolean = False) As String

        Dim sAH As String
        Static nSequenceNo As Double

        If sStatusCode = "-1" Then
            ' reduce nsequenceno by 1 because of unwritten records
            '(new bay janP 26.11.01
            nSequenceNo = nSequenceNo - 1
            sAH = vbNullString
        Else

            If bFirstLine = True Then
                nSequenceNo = 1
            Else
                nSequenceNo = nSequenceNo + 1
            End If
            If sStatusCode = vbNullString Then
                'The imported file was not Telepay
                If Not bFileFromBank Then
                    sStatusCode = "00"
                Else
                    sStatusCode = "02"
                End If
            End If

            sAH = vbNullString
            sAH = "AH2"
            sAH = sAH & sStatusCode
            If sStatusCode = "00" Then
                If bISBetfor99 Then
                    If TKIOfor99 Then
                        sAH = sAH & "TKIO"
                    Else
                        If bDomestic Then
                            sAH = sAH & "TKII"
                        Else
                            sAH = sAH & "TKIU"
                        End If
                    End If
                Else
                    If bCurrentIsTKIO Then
                        sAH = sAH & "TKIO"
                    Else
                        If bDomestic Then
                            sAH = sAH & "TKII"
                        Else
                            sAH = sAH & "TKIU"
                        End If
                    End If
                End If
            Else
                If bISBetfor99 Then
                    If TKIOfor99 Then
                        sAH = sAH & "TKRO"
                    Else
                        If bDomestic Then
                            sAH = sAH & "TKRI"
                        Else
                            sAH = sAH & "TKRU"
                        End If
                    End If
                Else
                    If bCurrentIsTKIO Then
                        sAH = sAH & "TKRO"
                    Else
                        If bDomestic Then
                            sAH = sAH & "TKRI"
                        Else
                            sAH = sAH & "TKRU"
                        End If
                    End If
                End If
            End If

            If Trim$(sTransDate) = vbNullString Then
                sAH = sAH & Format(Now(), "MMDD")
            Else
                sAH = sAH & sTransDate
            End If
            'FIX: Enter Sequenceno. per day
            sAH = sAH & PadLeft(Str(nSequenceNo), 6, "0")
            sAH = sAH & Space(8)
            'FIX: Enter User_ID
            sAH = sAH & Space(11)
            sAH = sAH & "10"  ' XOKNET 12.11.2010 changed from 6 to 10 (lines)
        End If
        ApplicationHeader = sAH

    End Function

    Private Function WriteLine(ByVal sLine As String, ByRef nNoOfBETFOR As Double, ByRef nSequenceNoTotal As Double, ByVal bUnix As Boolean) As Boolean
        ' 12.08.02 JaNp
        ' Added bUnix
        ' if bUnix is set to true, then only LineFeed is added at the end of each line,
        ' instead of the ordinary CRLF
        If bUnix Then
            oFile.Write(Mid$(sLine, 1, 80) & vbLf)
            oFile.Write(Mid$(sLine, 81, 80) & vbLf)
            oFile.Write(Mid$(sLine, 161, 80) & vbLf)
            oFile.Write(Mid$(sLine, 241, 80) & vbLf)
            ' 30.12.2008 two extra lines for Telepay+
            oFile.Write(Mid$(sLine, 321, 80) & vbLf)
            oFile.Write(Mid$(sLine, 401, 80) & vbLf)
            'XOKNET 17.11.2010 added 4 more lines, total of 10
            oFile.Write(Mid$(sLine, 481, 80) & vbLf)
            oFile.Write(Mid$(sLine, 561, 80) & vbLf)
            oFile.Write(Mid$(sLine, 641, 80) & vbLf)
            oFile.Write(Mid$(sLine, 721, 80) & vbLf)


        Else
            oFile.WriteLine(Mid$(sLine, 1, 80))
            oFile.WriteLine(Mid$(sLine, 81, 80))
            oFile.WriteLine(Mid$(sLine, 161, 80))
            oFile.WriteLine(Mid$(sLine, 241, 80))
            ' 30.12.2008 two extra lines for Telepay+
            oFile.WriteLine(Mid$(sLine, 321, 80))
            oFile.WriteLine(Mid$(sLine, 401, 80))
            'XOKNET 17.11.2010 added 4 more lines, total of 10
            oFile.WriteLine(Mid$(sLine, 481, 80))
            oFile.WriteLine(Mid$(sLine, 561, 80))
            oFile.WriteLine(Mid$(sLine, 641, 80))
            oFile.WriteLine(Mid$(sLine, 721, 80))
        End If

        nNoOfBETFOR = nNoOfBETFOR + 1
        nSequenceNoTotal = nSequenceNoTotal + 1
        If nSequenceNoTotal > 9999 Then
            nSequenceNoTotal = 0
        End If

    End Function
    ' XNET 27.11.2013
    ' Created a new "splitsolution" where we first establish how many BETFOR04/23 we can have in each BETFOR01/21
    ' The number is based on type of payment, based on rules received by H�kon Belt, DNB, NOv 26th 2013
    Private Function CalculateMaxNoOfInvoices(ByVal oPayment As Payment) As Integer
        '-----------------------------------------------------------------------------------------------------
        'Cross Border
        'SEPA betaling =140 pos. 4 * BETFOR04
        'Norsk debet = 34965 pos. 999 * BETFOR04 (Separate letter)
        'Debet konto andre land = 140 pos. 4 * BETFOR04
        '
        'Domestic
        'Norge = 120 pos. 1 * BETFOR23
        'Sverige = 120 pos.  1 * BETFOR23
        'Danmark = 1435 pos. 41 * BETFOR23
        'SEPA = 140 pos.  2 * BETFOR23
        '
        'Sverige OCR = Kun 1 forekomst per BETFOR21/23
        'Finland Reference Payment = Kun 1 forekomst per BETFOR21/23
        'Norge KID = 999 * BETFOR23
        'Danmark FI 71 Kreditor referanse = Kun 1 forekomst per BETFOR21/23
        'Danmark FI 73 Melding = 1435 pos. 41 * BETFOR23
        'Danmark FI 75 71 Kreditor referanse = Kun 1 forekomst per BETFOR21/23, Melding = 1435 pos. 41 * BETFOR23
        'Danmark Giro 01 Melding = 140 pos. 2 * BETFOR23
        'Danmark Giro 04 Kreditor referanse = Kun 1 forekomst per BETFOR21/23
        '--------------------------------------------------------------------------------------------------------
        Dim iMaxNoOfInvoiceRecords As Integer
        Dim iActualNoOfInvoices As Integer
        Dim iActualNoOfCredits As Integer
        Dim oInvoice As Invoice

        iMaxNoOfInvoiceRecords = 1  ' default

        If oPayment.PayType = "I" And oPayment.BANK_I_Domestic = False Then
            ' Cross Border
            If IsSepaPayment(oPayment) Then
                ' SEPA betaling =140 pos. 4 * BETFOR04
                iMaxNoOfInvoiceRecords = 4
            ElseIf oPayment.BANK_AccountCountryCode = "NO" Then
                'Norsk debet = 34965 pos. 999 * BETFOR04 (Separate letter)
                iMaxNoOfInvoiceRecords = 999
            Else
                'Debet konto andre land = 140 pos. 4 * BETFOR04
                iMaxNoOfInvoiceRecords = 4
            End If
        ElseIf oPayment.PayType = "M" Or oPayment.PayType = "S" Then
            ' Masstransfer
            If oPayment.BANK_CountryCode = "GB" Then
                ' BACS, max 9999
                iMaxNoOfInvoiceRecords = 9999
            ElseIf oPayment.BANK_CountryCode = "SG" Then
                ' eGiro or Salary, BETFOR22
                iMaxNoOfInvoiceRecords = 9999
            Else
                ' at this stage, default mass to 9999
                iMaxNoOfInvoiceRecords = 9999
            End If

        Else
            ' Domestic

            ' Structured
            If IsOCR(oPayment.PayCode) Or oPayment.Structured = True Then
                If oPayment.BANK_CountryCode = "SE" Or oPayment.BANK_CountryCode = "FI" Then
                    'Sverige OCR = Kun 1 forekomst per BETFOR21/23
                    'Finland Reference Payment = Max 1, eller hvis kreditnota, Max 8 + 1 en kredit endret 25.02.2014
                    iMaxNoOfInvoiceRecords = 1
                ElseIf oPayment.BANK_CountryCode = "NO" Then
                    'Norge KID = 999 * BETFOR23
                    iMaxNoOfInvoiceRecords = 1
                ElseIf oPayment.BANK_CountryCode = "FI" Then    ' added 25.02.2014
                    For Each oInvoice In oPayment.Invoices
                        If oInvoice.MON_InvoiceAmount < 0 Then
                            iActualNoOfCredits = iActualNoOfCredits + 1
                        End If
                    Next oInvoice
                    If iActualNoOfCredits > 0 Then
                        iMaxNoOfInvoiceRecords = 9
                    Else
                        iMaxNoOfInvoiceRecords = 1
                    End If
                ElseIf oPayment.BANK_CountryCode = "DK" Then
                    'Danmark FI 71 Kreditor referanse = Kun 1 forekomst per BETFOR21/23
                    'Danmark FI 73 Melding = 1435 pos. 41 * BETFOR23
                    'Danmark FI 75 71 Kreditor referanse = Kun 1 forekomst per BETFOR21/23, Melding = 1435 pos. 41 * BETFOR23
                    'Danmark Giro 01 Melding = 140 pos. 2 * BETFOR23
                    'Danmark Giro 04 Kreditor referanse = Kun 1 forekomst per BETFOR21/23

                    ' Kortartkode as first 2 digits in UniqueID
                    ' Test on first invoice;
                    Select Case Left$(oPayment.Invoices(1).Unique_Id, 2)
                        Case "71"
                            iMaxNoOfInvoiceRecords = 1
                        Case "73"
                            iMaxNoOfInvoiceRecords = 41
                        Case "75"
                            iMaxNoOfInvoiceRecords = 41
                        Case "01"
                            iMaxNoOfInvoiceRecords = 2
                        Case "04"
                            iMaxNoOfInvoiceRecords = 1
                        Case Else
                            ' are there any more ? Default to 1
                            iMaxNoOfInvoiceRecords = 1
                    End Select
                Else
                    ' other counties with structured ? Default to 1
                    iMaxNoOfInvoiceRecords = 1
                End If
            Else
                ' unstructured
                'Norge = 120 pos. 1 * BETFOR23
                'Sverige = 120 pos.  1 * BETFOR23
                'Danmark = 1435 pos. 41 * BETFOR23
                'SEPA = 140 pos.  2 * BETFOR23
                If oPayment.BANK_CountryCode = "NO" Then
                    iMaxNoOfInvoiceRecords = 1
                ElseIf oPayment.BANK_CountryCode = "SE" Then
                    iMaxNoOfInvoiceRecords = 1
                ElseIf oPayment.BANK_CountryCode = "DK" Then
                    iMaxNoOfInvoiceRecords = 41
                ElseIf oPayment.BANK_CountryCode = "SG" Then
                    ' Check or Meps
                    iMaxNoOfInvoiceRecords = 4
                ElseIf oPayment.BANK_CountryCode = "US" Then
                    iMaxNoOfInvoiceRecords = 4
                ElseIf oPayment.BANK_CountryCode = "GB" Then
                    iMaxNoOfInvoiceRecords = 4

                ElseIf IsSepaPayment(oPayment) Then
                    iMaxNoOfInvoiceRecords = 2
                Else
                    ' any more? Default to 2 (140 like in swift)
                    iMaxNoOfInvoiceRecords = 2
                End If
            End If

        End If

        ' For Locals in CA and AU we can test otherwise;
        If oPayment.BANK_CountryCode = "CA" Or oPayment.BANK_CountryCode = "AU" Then
            If oPayment.DnBNORTBIPayType = "LOCALLOW" Then
                '- 84      CA  Domestic        ACH (Low value pmt AFT-ACH pmt)
                iMaxNoOfInvoiceRecords = 1
            ElseIf oPayment.DnBNORTBIPayType = "LOCALHIGH" Then
                '-85      CA  Domestic        Wire (High value pmt LVTS)
                iMaxNoOfInvoiceRecords = 4
            ElseIf oPayment.DnBNORTBIPayType = "LOCALCHECK" Then
                iMaxNoOfInvoiceRecords = 4
            End If
        End If

        CalculateMaxNoOfInvoices = iMaxNoOfInvoiceRecords
    End Function

    ' XokNET 03.10.2014 Added BETFOR42
    Private Function WriteBETFOR42(ByVal oPayment As Payment, ByVal nSequenceNoTotal As Double, ByVal sCompanyNo As String) As String
        Dim sLine As String

        sLine = vbNullString
        sLine = ApplicationHeader(False, oPayment.StatusCode)
        sLine = sLine & "BETFOR42"
        If sCompanyNo <> vbNullString Then
            sLine = sLine & PadLeft(sCompanyNo, 11, "0")
        Else
            sLine = sLine & sI_EnterpriseNo  '49
        End If
        If Left$(oPayment.BANK_I_SWIFTCode, 6) <> "DNBANO" Then  ' 30.12.2008 If TelepayPlus, account is in pos 347
            ' TKIO-format has I_Account in BETFOR02, pos 260-294
            sLine = sLine & "00000000000"  '60
        Else
            ' Telepay "classic"
            sLine = sLine & PadRight(oPayment.I_Account, 11, "0")
        End If
        sLine = sLine & PadLeft(Str(nSequenceNoTotal), 4, "0") '71
        sLine = sLine & PadRight(oPayment.Payment_ID, 6, " ")  '75

        sLine = sLine & PadRight(oPayment.E_Name, 140, " ")     '81-220
        sLine = sLine & Space(100)                             '221-320

        sLine = sLine & PadRight(oPayment.UltimateE_Name, 140, " ")  ' 321-460
        sLine = sLine & Space(340)  ' 461-800

        WriteBETFOR42 = UCase(sLine)

    End Function


End Module
