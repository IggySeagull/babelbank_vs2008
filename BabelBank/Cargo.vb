Option Strict Off
Option Explicit On
Public Class Cargo

    Private bRejectsExists As Boolean
    Private eBank As vbBabel.BabelFiles.Bank
    Private sFilenameIn As String
    Private lLastPayment_ID As Integer
    Private sI_Account As String
    Private sI_Name As String
    Private sE_Name As String
    Private sE_Account As String
    Private sMONCurrency As String
    Private nMONInvoiceAmount As Double
    Private sREF As String
    Private sPayCode As String
    Dim sPayType As String
    Private dDate_Payment As Date
    Private sSpecial As String
    Private sFreetext As String
    ' New 16.09.05 to collect imported lines
    Private oImportedLines As vbBabel.ImportedLines
    ' Added 03.08.2007
    Private sStatusCode As String
    Private sTemp As String
    ' added 26.05.2009
    Private sBANK_I_SWIFTCode As String
    Private bCredit As Boolean
    Private bDebit As Boolean 'Added 09.05.2019 for Camt.053
    Private eCountryName As vbBabel.BabelFiles.CountryName

    '********* START PROPERTY SETTINGS ***********************
    Public Property ImportedLines() As vbBabel.ImportedLines
        Get
            ImportedLines = oImportedLines
        End Get
        Set(ByVal Value As vbBabel.ImportedLines)
            'UPGRADE_WARNING: IsObject has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Not IsReference(Value) Then
                RaiseInvalidObject()
            End If
            'Store reference
            oImportedLines = Value
        End Set
    End Property
    Public Property RejectsExists() As Boolean
        Get
            RejectsExists = bRejectsExists
        End Get
        Set(ByVal Value As Boolean)
            bRejectsExists = Value
        End Set
    End Property
    Public Property Bank() As vbBabel.BabelFiles.Bank
        Get
            Bank = eBank
        End Get
        Set(ByVal Value As vbBabel.BabelFiles.Bank)
            eBank = Value
        End Set
    End Property
    Public Property FilenameIn() As String
        Get
            FilenameIn = sFilenameIn
        End Get
        Set(ByVal Value As String)
            sFilenameIn = CStr(Value)
        End Set
    End Property
    Public Property Temp() As String
        Get
            Temp = sTemp
        End Get
        Set(ByVal Value As String)
            sTemp = Value
        End Set
    End Property
    Public Property LastPayment_ID() As Integer
        Get
            LastPayment_ID = lLastPayment_ID
        End Get
        Set(ByVal Value As Integer)
            lLastPayment_ID = Value
        End Set
    End Property
    Public Property MON_InvoiceAmount() As Double
        Get
            MON_InvoiceAmount = nMONInvoiceAmount
        End Get
        Set(ByVal Value As Double)
            nMONInvoiceAmount = Value
        End Set
    End Property
    Public Property I_Account() As String
        Get
            I_Account = sI_Account
        End Get
        Set(ByVal Value As String)
            sI_Account = Value
        End Set
    End Property
    Public Property I_Name() As String
        Get
            I_Name = sI_Name
        End Get
        Set(ByVal Value As String)
            sI_Name = Value
        End Set
    End Property
    Public Property E_Name() As String
        Get
            E_Name = sE_Name
        End Get
        Set(ByVal Value As String)
            sE_Name = Value
        End Set
    End Property
    Public Property E_Account() As String
        Get
            E_Account = sE_Account
        End Get
        Set(ByVal Value As String)
            sE_Account = Value
        End Set
    End Property
    Public Property MONCurrency() As String
        Get
            MONCurrency = sMONCurrency
        End Get
        Set(ByVal Value As String)
            sMONCurrency = Value
        End Set
    End Property
    Public Property REF() As String
        Get
            REF = sREF
        End Get
        Set(ByVal Value As String)
            sREF = Value
        End Set
    End Property
    Public Property Special() As String
        Get
            Special = sSpecial
        End Get
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public Property FreeText() As String
        Get
            FreeText = sFreetext
        End Get
        Set(ByVal Value As String)
            sFreetext = Value
        End Set
    End Property
    Public Property PayCode() As String
        Get
            PayCode = sPayCode
        End Get
        Set(ByVal Value As String)
            sPayCode = Value
        End Set
    End Property
    Public Property PayType() As String
        Get
            PayType = sPayType
        End Get
        Set(ByVal Value As String)
            sPayType = Value
        End Set
    End Property
    Public Property DATE_Payment() As String
        Get
            DATE_Payment = DateToString(dDate_Payment)
        End Get
        Set(ByVal Value As String)

            If StringContainsValidDate(CStr(Value)) Then
                dDate_Payment = StringToDate(CStr(Value))
            Else
                Err.Raise(30010, , Replace(Replace(Replace(LRS(30010), "%1", "DATE_Payment"), "%2", "Cargo"), "%3", CStr(Value)))
                'msgbox "Feil i dato som legges inn i %1 DATE_Production i klassen %2 BabelFile"
                '& vbcrlf & Dato %3 cstr(NewVal) er ikke gyldig.
                '& vbcrlf & vbBabel benytter formatet YYYYMMDD
            End If
        End Set
    End Property
    Public Property StatusCode() As String
        Get
            StatusCode = sStatusCode
        End Get
        Set(ByVal Value As String)
            sStatusCode = Value
        End Set
    End Property
    Public Property Credit() As Boolean
        Get
            Credit = bCredit
        End Get
        Set(ByVal Value As Boolean)
            bCredit = Value
        End Set
    End Property
    Public Property Debit() As Boolean 'Added 09.05.2019
        Get
            Debit = bDebit
        End Get
        Set(ByVal Value As Boolean)
            bDebit = Value
        End Set
    End Property
    Public Property CountryName() As vbBabel.BabelFiles.CountryName
        Get
            CountryName = eCountryName
        End Get
        Set(ByVal Value As vbBabel.BabelFiles.CountryName)
            eCountryName = Value
        End Set
    End Property

    '********* END PROPERTY SETTINGS ***********************
    Public Property BANK_I_SWIFTCode() As String
        Get
            BANK_I_SWIFTCode = sBANK_I_SWIFTCode
        End Get
        Set(ByVal Value As String)
            sBANK_I_SWIFTCode = Value
        End Set
    End Property

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()

        bRejectsExists = False
        eBank = BabelFiles.Bank.No_Bank_Specified
        lLastPayment_ID = 0
        sI_Account = ""
        sMONCurrency = ""
        nMONInvoiceAmount = 0
        sREF = ""
        sPayCode = ""
        sPayType = ""
        sSpecial = ""
        sI_Account = ""
        sBANK_I_SWIFTCode = ""
        bCredit = False
        eCountryName = BabelFiles.CountryName.Unknown

        oImportedLines = New ImportedLines
    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
End Class
