<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_1005_DnBNORFinans_CREMUL
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(rp_1005_DnBNORFinans_CREMUL))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtE_Name = New DataDynamics.ActiveReports.TextBox
        Me.txtE_Account = New DataDynamics.ActiveReports.TextBox
        Me.txtKID = New DataDynamics.ActiveReports.TextBox
        Me.txtREF_Bank1 = New DataDynamics.ActiveReports.TextBox
        Me.txtDATE_Value = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_P_InvoiceCurrency = New DataDynamics.ActiveReports.TextBox
        Me.txtMON_P_TransferredAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtREF_Bank2 = New DataDynamics.ActiveReports.TextBox
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grBabelFileHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.lblFilename = New DataDynamics.ActiveReports.Label
        Me.txtFilename = New DataDynamics.ActiveReports.TextBox
        Me.lblDate_Book = New DataDynamics.ActiveReports.Label
        Me.lblAmountStatement = New DataDynamics.ActiveReports.Label
        Me.txtDATE_Book = New DataDynamics.ActiveReports.TextBox
        Me.txtAmountStatement = New DataDynamics.ActiveReports.TextBox
        Me.lblClient = New DataDynamics.ActiveReports.Label
        Me.lblI_Account = New DataDynamics.ActiveReports.Label
        Me.lblNoOfPayments = New DataDynamics.ActiveReports.Label
        Me.txtNoOfPayments = New DataDynamics.ActiveReports.TextBox
        Me.txtI_Account = New DataDynamics.ActiveReports.TextBox
        Me.txtClientName = New DataDynamics.ActiveReports.TextBox
        Me.lineBBFileHeaderUpper = New DataDynamics.ActiveReports.Line
        Me.lineBBFileHeaderLower = New DataDynamics.ActiveReports.Line
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.grBabelFileFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.lblBabelfooterAmount = New DataDynamics.ActiveReports.Label
        Me.txtBabelfooterAmount = New DataDynamics.ActiveReports.TextBox
        Me.lineBabelAmountlbl1 = New DataDynamics.ActiveReports.Line
        Me.lineBabelAmountlbl2 = New DataDynamics.ActiveReports.Line
        Me.lineBabelAmount1 = New DataDynamics.ActiveReports.Line
        Me.lineBabelAmount2 = New DataDynamics.ActiveReports.Line
        Me.GroupBatchHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.lblE_Account = New DataDynamics.ActiveReports.Label
        Me.lblKID = New DataDynamics.ActiveReports.Label
        Me.lblREF_Bank1 = New DataDynamics.ActiveReports.Label
        Me.lblREF_Bank2 = New DataDynamics.ActiveReports.Label
        Me.lblDateValue = New DataDynamics.ActiveReports.Label
        Me.lblE_Name = New DataDynamics.ActiveReports.Label
        Me.lblPcur = New DataDynamics.ActiveReports.Label
        Me.lblAmount = New DataDynamics.ActiveReports.Label
        Me.LineE_Account = New DataDynamics.ActiveReports.Line
        Me.lineKID = New DataDynamics.ActiveReports.Line
        Me.lineREF_Bank1 = New DataDynamics.ActiveReports.Line
        Me.lineREF_Bank2 = New DataDynamics.ActiveReports.Line
        Me.lineDateValue = New DataDynamics.ActiveReports.Line
        Me.lineE_Name = New DataDynamics.ActiveReports.Line
        Me.lineCur = New DataDynamics.ActiveReports.Line
        Me.Line1 = New DataDynamics.ActiveReports.Line
        Me.GroupBatchFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.lblBatchRefBank = New DataDynamics.ActiveReports.Label
        Me.lblBatchfooterAmount = New DataDynamics.ActiveReports.Label
        Me.txtBatchfooterAmount = New DataDynamics.ActiveReports.TextBox
        Me.lineBatchRefBankH = New DataDynamics.ActiveReports.Line
        Me.lineBatchAmountLabelH = New DataDynamics.ActiveReports.Line
        Me.lineBacthAmountH = New DataDynamics.ActiveReports.Line
        Me.lineBatchRefBankU = New DataDynamics.ActiveReports.Line
        Me.lineBatchAmountLabelU = New DataDynamics.ActiveReports.Line
        Me.lineBacthAmountU = New DataDynamics.ActiveReports.Line
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
        CType(Me.txtE_Name, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtE_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtKID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREF_Bank1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Value, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_P_InvoiceCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMON_P_TransferredAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtREF_Bank2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblFilename, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDate_Book, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblAmountStatement, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Book, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAmountStatement, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNoOfPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBabelfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBabelfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblE_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblKID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblREF_Bank1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblREF_Bank2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDateValue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblE_Name, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPcur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchRefBank, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtE_Name, Me.txtE_Account, Me.txtKID, Me.txtREF_Bank1, Me.txtDATE_Value, Me.txtMON_P_InvoiceCurrency, Me.txtMON_P_TransferredAmount, Me.txtREF_Bank2})
        Me.Detail.Height = 0.1875!
        Me.Detail.Name = "Detail"
        '
        'txtE_Name
        '
        Me.txtE_Name.CanGrow = False
        Me.txtE_Name.DataField = "E_Name"
        Me.txtE_Name.Height = 0.15!
        Me.txtE_Name.Left = 5.7!
        Me.txtE_Name.Name = "txtE_Name"
        Me.txtE_Name.Style = "font-family: Arial; font-size: 8pt; ddo-char-set: 1"
        Me.txtE_Name.Text = "txtE_Name"
        Me.txtE_Name.Top = 0.0!
        Me.txtE_Name.Width = 2.146!
        '
        'txtE_Account
        '
        Me.txtE_Account.DataField = "E_Account"
        Me.txtE_Account.Height = 0.15!
        Me.txtE_Account.Left = 0.0!
        Me.txtE_Account.Name = "txtE_Account"
        Me.txtE_Account.Style = "font-size: 8pt"
        Me.txtE_Account.Text = "txtE_Account"
        Me.txtE_Account.Top = 0.0!
        Me.txtE_Account.Width = 1.178!
        '
        'txtKID
        '
        Me.txtKID.DataField = "Unique_ID"
        Me.txtKID.Height = 0.15!
        Me.txtKID.Left = 1.178!
        Me.txtKID.Name = "txtKID"
        Me.txtKID.Style = "font-size: 8pt"
        Me.txtKID.Text = "txtKID"
        Me.txtKID.Top = 0.0!
        Me.txtKID.Visible = False
        Me.txtKID.Width = 1.431!
        '
        'txtREF_Bank1
        '
        Me.txtREF_Bank1.DataField = "REF_Bank1"
        Me.txtREF_Bank1.Height = 0.15!
        Me.txtREF_Bank1.Left = 2.732!
        Me.txtREF_Bank1.Name = "txtREF_Bank1"
        Me.txtREF_Bank1.Style = "font-size: 8pt"
        Me.txtREF_Bank1.Text = "txtREF_Bank1"
        Me.txtREF_Bank1.Top = 0.0!
        Me.txtREF_Bank1.Width = 1.096!
        '
        'txtDATE_Value
        '
        Me.txtDATE_Value.DataField = "DATE_Value"
        Me.txtDATE_Value.Height = 0.15!
        Me.txtDATE_Value.Left = 5.0!
        Me.txtDATE_Value.Name = "txtDATE_Value"
        Me.txtDATE_Value.OutputFormat = resources.GetString("txtDATE_Value.OutputFormat")
        Me.txtDATE_Value.Style = "font-size: 8pt"
        Me.txtDATE_Value.Text = "txtDATE_Value"
        Me.txtDATE_Value.Top = 0.0!
        Me.txtDATE_Value.Width = 0.7!
        '
        'txtMON_P_InvoiceCurrency
        '
        Me.txtMON_P_InvoiceCurrency.DataField = "MON_InvoiceCurrency"
        Me.txtMON_P_InvoiceCurrency.Height = 0.15!
        Me.txtMON_P_InvoiceCurrency.Left = 8.201!
        Me.txtMON_P_InvoiceCurrency.Name = "txtMON_P_InvoiceCurrency"
        Me.txtMON_P_InvoiceCurrency.Style = "font-size: 8pt"
        Me.txtMON_P_InvoiceCurrency.Text = "PCur"
        Me.txtMON_P_InvoiceCurrency.Top = 0.0!
        Me.txtMON_P_InvoiceCurrency.Width = 0.3!
        '
        'txtMON_P_TransferredAmount
        '
        Me.txtMON_P_TransferredAmount.DataField = "MON_InvoiceAmount"
        Me.txtMON_P_TransferredAmount.Height = 0.15!
        Me.txtMON_P_TransferredAmount.Left = 8.610001!
        Me.txtMON_P_TransferredAmount.Name = "txtMON_P_TransferredAmount"
        Me.txtMON_P_TransferredAmount.OutputFormat = resources.GetString("txtMON_P_TransferredAmount.OutputFormat")
        Me.txtMON_P_TransferredAmount.Style = "font-size: 8pt; text-align: right"
        Me.txtMON_P_TransferredAmount.Text = "txtMON_P_TransferredAmount"
        Me.txtMON_P_TransferredAmount.Top = 0.0!
        Me.txtMON_P_TransferredAmount.Width = 0.999999!
        '
        'txtREF_Bank2
        '
        Me.txtREF_Bank2.DataField = "REF_Bank2"
        Me.txtREF_Bank2.Height = 0.15!
        Me.txtREF_Bank2.Left = 3.922!
        Me.txtREF_Bank2.Name = "txtREF_Bank2"
        Me.txtREF_Bank2.Style = "font-size: 8pt"
        Me.txtREF_Bank2.Text = "txtREF_Bank2"
        Me.txtREF_Bank2.Top = 0.0!
        Me.txtREF_Bank2.Width = 1.023!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 8.856!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right"
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 7.298!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right"
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grBabelFileHeader
        '
        Me.grBabelFileHeader.CanShrink = True
        Me.grBabelFileHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblFilename, Me.txtFilename, Me.lblDate_Book, Me.lblAmountStatement, Me.txtDATE_Book, Me.txtAmountStatement, Me.lblClient, Me.lblI_Account, Me.lblNoOfPayments, Me.txtNoOfPayments, Me.txtI_Account, Me.txtClientName, Me.lineBBFileHeaderUpper, Me.lineBBFileHeaderLower, Me.lblrptHeader, Me.rptInfoPageHeaderDate})
        Me.grBabelFileHeader.DataField = "Breakfield"
        Me.grBabelFileHeader.Height = 1.535833!
        Me.grBabelFileHeader.KeepTogether = True
        Me.grBabelFileHeader.Name = "grBabelFileHeader"
        Me.grBabelFileHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'lblFilename
        '
        Me.lblFilename.Height = 0.273!
        Me.lblFilename.HyperLink = Nothing
        Me.lblFilename.Left = 0.0000001192093!
        Me.lblFilename.Name = "lblFilename"
        Me.lblFilename.Style = "font-size: 14.25pt; font-weight: bold; text-align: left"
        Me.lblFilename.Text = "lblFilename"
        Me.lblFilename.Top = 0.453!
        Me.lblFilename.Width = 1.313!
        '
        'txtFilename
        '
        Me.txtFilename.CanGrow = False
        Me.txtFilename.DataField = "Filename"
        Me.txtFilename.Height = 0.273!
        Me.txtFilename.Left = 1.313!
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.Style = "font-size: 14.25pt; font-weight: bold; text-align: left"
        Me.txtFilename.Text = "txtFilename"
        Me.txtFilename.Top = 0.453!
        Me.txtFilename.Width = 6.485001!
        '
        'lblDate_Book
        '
        Me.lblDate_Book.Height = 0.19!
        Me.lblDate_Book.HyperLink = Nothing
        Me.lblDate_Book.Left = 0.0000001192093!
        Me.lblDate_Book.Name = "lblDate_Book"
        Me.lblDate_Book.Style = "font-size: 10pt"
        Me.lblDate_Book.Text = "lblDate_Book"
        Me.lblDate_Book.Top = 1.006!
        Me.lblDate_Book.Width = 1.313!
        '
        'lblAmountStatement
        '
        Me.lblAmountStatement.Height = 0.19!
        Me.lblAmountStatement.HyperLink = Nothing
        Me.lblAmountStatement.Left = 0.0000001192093!
        Me.lblAmountStatement.Name = "lblAmountStatement"
        Me.lblAmountStatement.Style = "font-size: 10pt"
        Me.lblAmountStatement.Text = "lblAmountStatement"
        Me.lblAmountStatement.Top = 1.196!
        Me.lblAmountStatement.Width = 1.313!
        '
        'txtDATE_Book
        '
        Me.txtDATE_Book.CanShrink = True
        Me.txtDATE_Book.DataField = "DATE_Book"
        Me.txtDATE_Book.Height = 0.19!
        Me.txtDATE_Book.Left = 1.446!
        Me.txtDATE_Book.Name = "txtDATE_Book"
        Me.txtDATE_Book.OutputFormat = resources.GetString("txtDATE_Book.OutputFormat")
        Me.txtDATE_Book.Style = "font-size: 10pt"
        Me.txtDATE_Book.Text = "txtDATE_Book"
        Me.txtDATE_Book.Top = 1.006!
        Me.txtDATE_Book.Width = 1.823!
        '
        'txtAmountStatement
        '
        Me.txtAmountStatement.CanShrink = True
        Me.txtAmountStatement.DataField = "AmountStatement"
        Me.txtAmountStatement.Height = 0.19!
        Me.txtAmountStatement.Left = 1.446!
        Me.txtAmountStatement.Name = "txtAmountStatement"
        Me.txtAmountStatement.OutputFormat = resources.GetString("txtAmountStatement.OutputFormat")
        Me.txtAmountStatement.Style = "font-size: 10pt"
        Me.txtAmountStatement.Text = "txtAmountStatement"
        Me.txtAmountStatement.Top = 1.196!
        Me.txtAmountStatement.Width = 1.823!
        '
        'lblClient
        '
        Me.lblClient.Height = 0.19!
        Me.lblClient.HyperLink = Nothing
        Me.lblClient.Left = 3.495!
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Style = "font-size: 10pt"
        Me.lblClient.Text = "lblClient"
        Me.lblClient.Top = 0.816!
        Me.lblClient.Width = 1.0!
        '
        'lblI_Account
        '
        Me.lblI_Account.Height = 0.19!
        Me.lblI_Account.HyperLink = Nothing
        Me.lblI_Account.Left = 3.5!
        Me.lblI_Account.Name = "lblI_Account"
        Me.lblI_Account.Style = "font-size: 10pt"
        Me.lblI_Account.Text = "lblI_Account"
        Me.lblI_Account.Top = 1.006!
        Me.lblI_Account.Width = 1.0!
        '
        'lblNoOfPayments
        '
        Me.lblNoOfPayments.Height = 0.19!
        Me.lblNoOfPayments.HyperLink = Nothing
        Me.lblNoOfPayments.Left = 3.5!
        Me.lblNoOfPayments.Name = "lblNoOfPayments"
        Me.lblNoOfPayments.Style = "font-size: 10pt"
        Me.lblNoOfPayments.Text = "lblNoOfPayments"
        Me.lblNoOfPayments.Top = 1.196!
        Me.lblNoOfPayments.Width = 1.0!
        '
        'txtNoOfPayments
        '
        Me.txtNoOfPayments.DataField = "NoOfPayments"
        Me.txtNoOfPayments.Height = 0.19!
        Me.txtNoOfPayments.Left = 4.525!
        Me.txtNoOfPayments.Name = "txtNoOfPayments"
        Me.txtNoOfPayments.Style = "font-size: 10pt; font-weight: normal"
        Me.txtNoOfPayments.Text = "txtNoOfPayments"
        Me.txtNoOfPayments.Top = 1.196!
        Me.txtNoOfPayments.Width = 1.9!
        '
        'txtI_Account
        '
        Me.txtI_Account.DataField = "I_Account"
        Me.txtI_Account.Height = 0.19!
        Me.txtI_Account.Left = 4.525!
        Me.txtI_Account.Name = "txtI_Account"
        Me.txtI_Account.Style = "font-size: 10pt; font-weight: normal"
        Me.txtI_Account.Text = "txtI_Account"
        Me.txtI_Account.Top = 1.006!
        Me.txtI_Account.Width = 1.9!
        '
        'txtClientName
        '
        Me.txtClientName.CanGrow = False
        Me.txtClientName.DataField = "Client"
        Me.txtClientName.Height = 0.19!
        Me.txtClientName.Left = 4.525!
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Style = "font-size: 10pt"
        Me.txtClientName.Text = "txtClientName"
        Me.txtClientName.Top = 0.816!
        Me.txtClientName.Width = 1.9!
        '
        'lineBBFileHeaderUpper
        '
        Me.lineBBFileHeaderUpper.Height = 0.0!
        Me.lineBBFileHeaderUpper.Left = 0.0!
        Me.lineBBFileHeaderUpper.LineWeight = 1.0!
        Me.lineBBFileHeaderUpper.Name = "lineBBFileHeaderUpper"
        Me.lineBBFileHeaderUpper.Top = 0.0!
        Me.lineBBFileHeaderUpper.Width = 10.114!
        Me.lineBBFileHeaderUpper.X1 = 0.0!
        Me.lineBBFileHeaderUpper.X2 = 10.114!
        Me.lineBBFileHeaderUpper.Y1 = 0.0!
        Me.lineBBFileHeaderUpper.Y2 = 0.0!
        '
        'lineBBFileHeaderLower
        '
        Me.lineBBFileHeaderLower.Height = 0.0!
        Me.lineBBFileHeaderLower.Left = 0.0000001192093!
        Me.lineBBFileHeaderLower.LineWeight = 1.0!
        Me.lineBBFileHeaderLower.Name = "lineBBFileHeaderLower"
        Me.lineBBFileHeaderLower.Top = 1.451!
        Me.lineBBFileHeaderLower.Width = 10.114!
        Me.lineBBFileHeaderLower.X1 = 0.0000001192093!
        Me.lineBBFileHeaderLower.X2 = 10.114!
        Me.lineBBFileHeaderLower.Y1 = 1.451!
        Me.lineBBFileHeaderLower.Y2 = 1.451!
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 2.609!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "font-size: 18pt; text-align: center"
        Me.lblrptHeader.Text = "Innbetalinger"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'grBabelFileFooter
        '
        Me.grBabelFileFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblBabelfooterAmount, Me.txtBabelfooterAmount, Me.lineBabelAmountlbl1, Me.lineBabelAmountlbl2, Me.lineBabelAmount1, Me.lineBabelAmount2})
        Me.grBabelFileFooter.Height = 0.25!
        Me.grBabelFileFooter.Name = "grBabelFileFooter"
        '
        'lblBabelfooterAmount
        '
        Me.lblBabelfooterAmount.Height = 0.15!
        Me.lblBabelfooterAmount.HyperLink = Nothing
        Me.lblBabelfooterAmount.Left = 7.565001!
        Me.lblBabelfooterAmount.Name = "lblBabelfooterAmount"
        Me.lblBabelfooterAmount.Style = "font-size: 8pt; ddo-char-set: 0"
        Me.lblBabelfooterAmount.Text = "lblBabelfooterAmount"
        Me.lblBabelfooterAmount.Top = 0.0!
        Me.lblBabelfooterAmount.Width = 0.936!
        '
        'txtBabelfooterAmount
        '
        Me.txtBabelfooterAmount.DataField = "MON_InvoiceAmount"
        Me.txtBabelfooterAmount.Height = 0.15!
        Me.txtBabelfooterAmount.Left = 8.610001!
        Me.txtBabelfooterAmount.Name = "txtBabelfooterAmount"
        Me.txtBabelfooterAmount.OutputFormat = resources.GetString("txtBabelfooterAmount.OutputFormat")
        Me.txtBabelfooterAmount.Style = "font-size: 8pt; text-align: right; ddo-char-set: 0"
        Me.txtBabelfooterAmount.SummaryGroup = "grBabelFileHeader"
        Me.txtBabelfooterAmount.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtBabelfooterAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtBabelfooterAmount.Text = "txtBabelfooterAmount"
        Me.txtBabelfooterAmount.Top = 0.0!
        Me.txtBabelfooterAmount.Width = 1.0!
        '
        'lineBabelAmountlbl1
        '
        Me.lineBabelAmountlbl1.Height = 0.0!
        Me.lineBabelAmountlbl1.Left = 7.242001!
        Me.lineBabelAmountlbl1.LineWeight = 1.0!
        Me.lineBabelAmountlbl1.Name = "lineBabelAmountlbl1"
        Me.lineBabelAmountlbl1.Top = 0.15!
        Me.lineBabelAmountlbl1.Width = 1.258001!
        Me.lineBabelAmountlbl1.X1 = 7.242001!
        Me.lineBabelAmountlbl1.X2 = 8.500002!
        Me.lineBabelAmountlbl1.Y1 = 0.15!
        Me.lineBabelAmountlbl1.Y2 = 0.15!
        '
        'lineBabelAmountlbl2
        '
        Me.lineBabelAmountlbl2.Height = 0.0!
        Me.lineBabelAmountlbl2.Left = 7.242001!
        Me.lineBabelAmountlbl2.LineWeight = 1.0!
        Me.lineBabelAmountlbl2.Name = "lineBabelAmountlbl2"
        Me.lineBabelAmountlbl2.Top = 0.17!
        Me.lineBabelAmountlbl2.Width = 1.258001!
        Me.lineBabelAmountlbl2.X1 = 7.242001!
        Me.lineBabelAmountlbl2.X2 = 8.500002!
        Me.lineBabelAmountlbl2.Y1 = 0.17!
        Me.lineBabelAmountlbl2.Y2 = 0.17!
        '
        'lineBabelAmount1
        '
        Me.lineBabelAmount1.Height = 0.0!
        Me.lineBabelAmount1.Left = 8.610001!
        Me.lineBabelAmount1.LineWeight = 1.0!
        Me.lineBabelAmount1.Name = "lineBabelAmount1"
        Me.lineBabelAmount1.Top = 0.15!
        Me.lineBabelAmount1.Width = 1.0!
        Me.lineBabelAmount1.X1 = 8.610001!
        Me.lineBabelAmount1.X2 = 9.610001!
        Me.lineBabelAmount1.Y1 = 0.15!
        Me.lineBabelAmount1.Y2 = 0.15!
        '
        'lineBabelAmount2
        '
        Me.lineBabelAmount2.Height = 0.0!
        Me.lineBabelAmount2.Left = 8.610001!
        Me.lineBabelAmount2.LineWeight = 1.0!
        Me.lineBabelAmount2.Name = "lineBabelAmount2"
        Me.lineBabelAmount2.Top = 0.17!
        Me.lineBabelAmount2.Width = 1.000001!
        Me.lineBabelAmount2.X1 = 8.610001!
        Me.lineBabelAmount2.X2 = 9.610002!
        Me.lineBabelAmount2.Y1 = 0.17!
        Me.lineBabelAmount2.Y2 = 0.17!
        '
        'GroupBatchHeader
        '
        Me.GroupBatchHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblE_Account, Me.lblKID, Me.lblREF_Bank1, Me.lblREF_Bank2, Me.lblDateValue, Me.lblE_Name, Me.lblPcur, Me.lblAmount, Me.LineE_Account, Me.lineKID, Me.lineREF_Bank1, Me.lineREF_Bank2, Me.lineDateValue, Me.lineE_Name, Me.lineCur, Me.Line1})
        Me.GroupBatchHeader.DataField = "AccountRef"
        Me.GroupBatchHeader.Height = 0.2594444!
        Me.GroupBatchHeader.Name = "GroupBatchHeader"
        '
        'lblE_Account
        '
        Me.lblE_Account.Height = 0.19!
        Me.lblE_Account.HyperLink = Nothing
        Me.lblE_Account.Left = 0.0!
        Me.lblE_Account.Name = "lblE_Account"
        Me.lblE_Account.Style = "font-size: 8.25pt; text-decoration: none"
        Me.lblE_Account.Text = "lblE_Account"
        Me.lblE_Account.Top = 0.0!
        Me.lblE_Account.Width = 1.178!
        '
        'lblKID
        '
        Me.lblKID.Height = 0.19!
        Me.lblKID.HyperLink = Nothing
        Me.lblKID.Left = 1.178!
        Me.lblKID.Name = "lblKID"
        Me.lblKID.Style = "font-size: 8.25pt; text-decoration: none"
        Me.lblKID.Text = "lblKID"
        Me.lblKID.Top = 0.0!
        Me.lblKID.Width = 1.448!
        '
        'lblREF_Bank1
        '
        Me.lblREF_Bank1.Height = 0.19!
        Me.lblREF_Bank1.HyperLink = Nothing
        Me.lblREF_Bank1.Left = 2.609!
        Me.lblREF_Bank1.Name = "lblREF_Bank1"
        Me.lblREF_Bank1.Style = "font-size: 8.25pt; text-decoration: none"
        Me.lblREF_Bank1.Text = "lblREF_Bank1"
        Me.lblREF_Bank1.Top = 0.0!
        Me.lblREF_Bank1.Width = 1.084!
        '
        'lblREF_Bank2
        '
        Me.lblREF_Bank2.Height = 0.19!
        Me.lblREF_Bank2.HyperLink = Nothing
        Me.lblREF_Bank2.Left = 3.693!
        Me.lblREF_Bank2.Name = "lblREF_Bank2"
        Me.lblREF_Bank2.Style = "font-size: 8.25pt; text-decoration: none"
        Me.lblREF_Bank2.Text = "lblREF_Bank2"
        Me.lblREF_Bank2.Top = 0.0!
        Me.lblREF_Bank2.Width = 1.084!
        '
        'lblDateValue
        '
        Me.lblDateValue.Height = 0.19!
        Me.lblDateValue.HyperLink = Nothing
        Me.lblDateValue.Left = 5.0!
        Me.lblDateValue.Name = "lblDateValue"
        Me.lblDateValue.Style = "font-size: 8.25pt; text-decoration: none"
        Me.lblDateValue.Text = "lblDateValue"
        Me.lblDateValue.Top = 0.0!
        Me.lblDateValue.Width = 0.8!
        '
        'lblE_Name
        '
        Me.lblE_Name.Height = 0.19!
        Me.lblE_Name.HyperLink = Nothing
        Me.lblE_Name.Left = 5.8!
        Me.lblE_Name.Name = "lblE_Name"
        Me.lblE_Name.Style = "font-size: 8.25pt; text-decoration: none"
        Me.lblE_Name.Text = "lblE_Name"
        Me.lblE_Name.Top = 0.0!
        Me.lblE_Name.Width = 2.2!
        '
        'lblPcur
        '
        Me.lblPcur.Height = 0.19!
        Me.lblPcur.HyperLink = Nothing
        Me.lblPcur.Left = 7.955!
        Me.lblPcur.Name = "lblPcur"
        Me.lblPcur.Style = "font-size: 8.25pt; text-decoration: none"
        Me.lblPcur.Text = "lblPCur"
        Me.lblPcur.Top = 0.0!
        Me.lblPcur.Width = 0.6039987!
        '
        'lblAmount
        '
        Me.lblAmount.Height = 0.19!
        Me.lblAmount.HyperLink = Nothing
        Me.lblAmount.Left = 8.559!
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Style = "font-size: 8.25pt; text-decoration: none"
        Me.lblAmount.Text = "lblAmount"
        Me.lblAmount.Top = 0.0!
        Me.lblAmount.Width = 1.313!
        '
        'LineE_Account
        '
        Me.LineE_Account.Height = 0.0!
        Me.LineE_Account.Left = 0.0!
        Me.LineE_Account.LineWeight = 1.0!
        Me.LineE_Account.Name = "LineE_Account"
        Me.LineE_Account.Top = 0.21!
        Me.LineE_Account.Width = 1.1!
        Me.LineE_Account.X1 = 0.0!
        Me.LineE_Account.X2 = 1.1!
        Me.LineE_Account.Y1 = 0.21!
        Me.LineE_Account.Y2 = 0.21!
        '
        'lineKID
        '
        Me.lineKID.Height = 0.0!
        Me.lineKID.Left = 1.178!
        Me.lineKID.LineWeight = 1.0!
        Me.lineKID.Name = "lineKID"
        Me.lineKID.Top = 0.21!
        Me.lineKID.Width = 1.385!
        Me.lineKID.X1 = 1.178!
        Me.lineKID.X2 = 2.563!
        Me.lineKID.Y1 = 0.21!
        Me.lineKID.Y2 = 0.21!
        '
        'lineREF_Bank1
        '
        Me.lineREF_Bank1.Height = 0.0!
        Me.lineREF_Bank1.Left = 2.626!
        Me.lineREF_Bank1.LineWeight = 1.0!
        Me.lineREF_Bank1.Name = "lineREF_Bank1"
        Me.lineREF_Bank1.Top = 0.21!
        Me.lineREF_Bank1.Width = 0.997!
        Me.lineREF_Bank1.X1 = 2.626!
        Me.lineREF_Bank1.X2 = 3.623!
        Me.lineREF_Bank1.Y1 = 0.21!
        Me.lineREF_Bank1.Y2 = 0.21!
        '
        'lineREF_Bank2
        '
        Me.lineREF_Bank2.Height = 0.0!
        Me.lineREF_Bank2.Left = 3.693!
        Me.lineREF_Bank2.LineWeight = 1.0!
        Me.lineREF_Bank2.Name = "lineREF_Bank2"
        Me.lineREF_Bank2.Top = 0.21!
        Me.lineREF_Bank2.Width = 1.024!
        Me.lineREF_Bank2.X1 = 3.693!
        Me.lineREF_Bank2.X2 = 4.717!
        Me.lineREF_Bank2.Y1 = 0.21!
        Me.lineREF_Bank2.Y2 = 0.21!
        '
        'lineDateValue
        '
        Me.lineDateValue.Height = 0.0!
        Me.lineDateValue.Left = 5.0!
        Me.lineDateValue.LineWeight = 1.0!
        Me.lineDateValue.Name = "lineDateValue"
        Me.lineDateValue.Top = 0.21!
        Me.lineDateValue.Width = 0.6750002!
        Me.lineDateValue.X1 = 5.0!
        Me.lineDateValue.X2 = 5.675!
        Me.lineDateValue.Y1 = 0.21!
        Me.lineDateValue.Y2 = 0.21!
        '
        'lineE_Name
        '
        Me.lineE_Name.Height = 0.0!
        Me.lineE_Name.Left = 5.8!
        Me.lineE_Name.LineWeight = 1.0!
        Me.lineE_Name.Name = "lineE_Name"
        Me.lineE_Name.Top = 0.21!
        Me.lineE_Name.Width = 1.9!
        Me.lineE_Name.X1 = 5.8!
        Me.lineE_Name.X2 = 7.7!
        Me.lineE_Name.Y1 = 0.21!
        Me.lineE_Name.Y2 = 0.21!
        '
        'lineCur
        '
        Me.lineCur.Height = 0.0!
        Me.lineCur.Left = 7.861001!
        Me.lineCur.LineWeight = 1.0!
        Me.lineCur.Name = "lineCur"
        Me.lineCur.Top = 0.21!
        Me.lineCur.Width = 0.638999!
        Me.lineCur.X1 = 7.861001!
        Me.lineCur.X2 = 8.5!
        Me.lineCur.Y1 = 0.21!
        Me.lineCur.Y2 = 0.21!
        '
        'Line1
        '
        Me.Line1.Height = 0.0!
        Me.Line1.Left = 8.559!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 0.21!
        Me.Line1.Width = 1.250001!
        Me.Line1.X1 = 8.559!
        Me.Line1.X2 = 9.809001!
        Me.Line1.Y1 = 0.21!
        Me.Line1.Y2 = 0.21!
        '
        'GroupBatchFooter
        '
        Me.GroupBatchFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblBatchRefBank, Me.lblBatchfooterAmount, Me.txtBatchfooterAmount, Me.lineBatchRefBankH, Me.lineBatchAmountLabelH, Me.lineBacthAmountH, Me.lineBatchRefBankU, Me.lineBatchAmountLabelU, Me.lineBacthAmountU})
        Me.GroupBatchFooter.Height = 0.2604167!
        Me.GroupBatchFooter.Name = "GroupBatchFooter"
        '
        'lblBatchRefBank
        '
        Me.lblBatchRefBank.DataField = "AccountRef"
        Me.lblBatchRefBank.Height = 0.15!
        Me.lblBatchRefBank.HyperLink = Nothing
        Me.lblBatchRefBank.Left = 5.488!
        Me.lblBatchRefBank.Name = "lblBatchRefBank"
        Me.lblBatchRefBank.Style = "font-size: 8pt; ddo-char-set: 0"
        Me.lblBatchRefBank.Text = "lblBatchRefBank"
        Me.lblBatchRefBank.Top = 0.04!
        Me.lblBatchRefBank.Width = 1.65!
        '
        'lblBatchfooterAmount
        '
        Me.lblBatchfooterAmount.Height = 0.15!
        Me.lblBatchfooterAmount.HyperLink = Nothing
        Me.lblBatchfooterAmount.Left = 7.242001!
        Me.lblBatchfooterAmount.Name = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Style = "font-size: 8pt; ddo-char-set: 0"
        Me.lblBatchfooterAmount.Text = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Top = 0.04!
        Me.lblBatchfooterAmount.Width = 1.258999!
        '
        'txtBatchfooterAmount
        '
        Me.txtBatchfooterAmount.DataField = "MON_InvoiceAmount"
        Me.txtBatchfooterAmount.Height = 0.15!
        Me.txtBatchfooterAmount.Left = 8.610001!
        Me.txtBatchfooterAmount.Name = "txtBatchfooterAmount"
        Me.txtBatchfooterAmount.OutputFormat = resources.GetString("txtBatchfooterAmount.OutputFormat")
        Me.txtBatchfooterAmount.Style = "font-size: 8pt; text-align: right; ddo-char-set: 0"
        Me.txtBatchfooterAmount.SummaryGroup = "GroupBatchHeader"
        Me.txtBatchfooterAmount.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtBatchfooterAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtBatchfooterAmount.Text = "txtBatchfooterAmount"
        Me.txtBatchfooterAmount.Top = 0.04!
        Me.txtBatchfooterAmount.Width = 1.0!
        '
        'lineBatchRefBankH
        '
        Me.lineBatchRefBankH.Height = 0.0!
        Me.lineBatchRefBankH.Left = 5.488!
        Me.lineBatchRefBankH.LineWeight = 1.0!
        Me.lineBatchRefBankH.Name = "lineBatchRefBankH"
        Me.lineBatchRefBankH.Top = 0.0!
        Me.lineBatchRefBankH.Width = 1.65!
        Me.lineBatchRefBankH.X1 = 5.488!
        Me.lineBatchRefBankH.X2 = 7.138!
        Me.lineBatchRefBankH.Y1 = 0.0!
        Me.lineBatchRefBankH.Y2 = 0.0!
        '
        'lineBatchAmountLabelH
        '
        Me.lineBatchAmountLabelH.Height = 0.0!
        Me.lineBatchAmountLabelH.Left = 7.242001!
        Me.lineBatchAmountLabelH.LineWeight = 1.0!
        Me.lineBatchAmountLabelH.Name = "lineBatchAmountLabelH"
        Me.lineBatchAmountLabelH.Top = 0.0!
        Me.lineBatchAmountLabelH.Width = 1.257999!
        Me.lineBatchAmountLabelH.X1 = 7.242001!
        Me.lineBatchAmountLabelH.X2 = 8.5!
        Me.lineBatchAmountLabelH.Y1 = 0.0!
        Me.lineBatchAmountLabelH.Y2 = 0.0!
        '
        'lineBacthAmountH
        '
        Me.lineBacthAmountH.Height = 0.0!
        Me.lineBacthAmountH.Left = 8.610001!
        Me.lineBacthAmountH.LineWeight = 1.0!
        Me.lineBacthAmountH.Name = "lineBacthAmountH"
        Me.lineBacthAmountH.Top = 0.0!
        Me.lineBacthAmountH.Width = 1.0!
        Me.lineBacthAmountH.X1 = 8.610001!
        Me.lineBacthAmountH.X2 = 9.610001!
        Me.lineBacthAmountH.Y1 = 0.0!
        Me.lineBacthAmountH.Y2 = 0.0!
        '
        'lineBatchRefBankU
        '
        Me.lineBatchRefBankU.Height = 0.0!
        Me.lineBatchRefBankU.Left = 5.488!
        Me.lineBatchRefBankU.LineWeight = 1.0!
        Me.lineBatchRefBankU.Name = "lineBatchRefBankU"
        Me.lineBatchRefBankU.Top = 0.19!
        Me.lineBatchRefBankU.Width = 1.65!
        Me.lineBatchRefBankU.X1 = 5.488!
        Me.lineBatchRefBankU.X2 = 7.138!
        Me.lineBatchRefBankU.Y1 = 0.19!
        Me.lineBatchRefBankU.Y2 = 0.19!
        '
        'lineBatchAmountLabelU
        '
        Me.lineBatchAmountLabelU.Height = 0.0!
        Me.lineBatchAmountLabelU.Left = 7.242001!
        Me.lineBatchAmountLabelU.LineWeight = 1.0!
        Me.lineBatchAmountLabelU.Name = "lineBatchAmountLabelU"
        Me.lineBatchAmountLabelU.Top = 0.19!
        Me.lineBatchAmountLabelU.Width = 1.258001!
        Me.lineBatchAmountLabelU.X1 = 7.242001!
        Me.lineBatchAmountLabelU.X2 = 8.500002!
        Me.lineBatchAmountLabelU.Y1 = 0.19!
        Me.lineBatchAmountLabelU.Y2 = 0.19!
        '
        'lineBacthAmountU
        '
        Me.lineBacthAmountU.Height = 0.0!
        Me.lineBacthAmountU.Left = 8.610001!
        Me.lineBacthAmountU.LineWeight = 1.0!
        Me.lineBacthAmountU.Name = "lineBacthAmountU"
        Me.lineBacthAmountU.Top = 0.19!
        Me.lineBacthAmountU.Width = 1.000001!
        Me.lineBacthAmountU.X1 = 8.610001!
        Me.lineBacthAmountU.X2 = 9.610002!
        Me.lineBacthAmountU.Y1 = 0.19!
        Me.lineBacthAmountU.Y2 = 0.19!
        '
        'PageHeader1
        '
        Me.PageHeader1.Height = 0.2951389!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageCounterGB, Me.rptInfoPageFooterDate})
        Me.PageFooter1.Height = 0.25!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'rp_1005_DnBNORFinans_CREMUL
        '
        Me.MasterReport = False
        Me.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Landscape
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 10.11458!
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.grBabelFileHeader)
        Me.Sections.Add(Me.GroupBatchHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.GroupBatchFooter)
        Me.Sections.Add(Me.grBabelFileFooter)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.txtE_Name, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtE_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtKID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREF_Bank1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Value, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_P_InvoiceCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMON_P_TransferredAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtREF_Bank2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblFilename, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDate_Book, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblAmountStatement, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Book, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAmountStatement, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNoOfPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBabelfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBabelfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblE_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblKID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblREF_Bank1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblREF_Bank2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDateValue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblE_Name, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPcur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchRefBank, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents grBabelFileFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents txtE_Name As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtE_Account As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtKID As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtREF_Bank1 As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtDATE_Value As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMON_P_InvoiceCurrency As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMON_P_TransferredAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtREF_Bank2 As DataDynamics.ActiveReports.TextBox
    Private WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents GroupBatchHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents GroupBatchFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents lblFilename As DataDynamics.ActiveReports.Label
    Private WithEvents txtFilename As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblDate_Book As DataDynamics.ActiveReports.Label
    Private WithEvents lblAmountStatement As DataDynamics.ActiveReports.Label
    Private WithEvents txtDATE_Book As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtAmountStatement As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblClient As DataDynamics.ActiveReports.Label
    Private WithEvents lblI_Account As DataDynamics.ActiveReports.Label
    Private WithEvents lblNoOfPayments As DataDynamics.ActiveReports.Label
    Private WithEvents txtNoOfPayments As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtI_Account As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtClientName As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblBabelfooterAmount As DataDynamics.ActiveReports.Label
    Private WithEvents txtBabelfooterAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblE_Account As DataDynamics.ActiveReports.Label
    Private WithEvents lblKID As DataDynamics.ActiveReports.Label
    Private WithEvents lblREF_Bank1 As DataDynamics.ActiveReports.Label
    Private WithEvents lblREF_Bank2 As DataDynamics.ActiveReports.Label
    Private WithEvents lblDateValue As DataDynamics.ActiveReports.Label
    Private WithEvents lblE_Name As DataDynamics.ActiveReports.Label
    Private WithEvents lblPcur As DataDynamics.ActiveReports.Label
    Private WithEvents lblAmount As DataDynamics.ActiveReports.Label
    Private WithEvents LineE_Account As DataDynamics.ActiveReports.Line
    Private WithEvents lblBatchRefBank As DataDynamics.ActiveReports.Label
    Private WithEvents lblBatchfooterAmount As DataDynamics.ActiveReports.Label
    Private WithEvents txtBatchfooterAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents lineBBFileHeaderUpper As DataDynamics.ActiveReports.Line
    Private WithEvents lineBBFileHeaderLower As DataDynamics.ActiveReports.Line
    Private WithEvents lineKID As DataDynamics.ActiveReports.Line
    Private WithEvents lineREF_Bank1 As DataDynamics.ActiveReports.Line
    Private WithEvents lineREF_Bank2 As DataDynamics.ActiveReports.Line
    Private WithEvents lineDateValue As DataDynamics.ActiveReports.Line
    Private WithEvents lineE_Name As DataDynamics.ActiveReports.Line
    Private WithEvents lineCur As DataDynamics.ActiveReports.Line
    Private WithEvents Line1 As DataDynamics.ActiveReports.Line
    Friend WithEvents grBabelFileHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    Private WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Private WithEvents lineBatchRefBankH As DataDynamics.ActiveReports.Line
    Private WithEvents lineBatchAmountLabelH As DataDynamics.ActiveReports.Line
    Private WithEvents lineBacthAmountH As DataDynamics.ActiveReports.Line
    Private WithEvents lineBatchRefBankU As DataDynamics.ActiveReports.Line
    Private WithEvents lineBatchAmountLabelU As DataDynamics.ActiveReports.Line
    Private WithEvents lineBacthAmountU As DataDynamics.ActiveReports.Line
    Private WithEvents lineBabelAmountlbl1 As DataDynamics.ActiveReports.Line
    Private WithEvents lineBabelAmountlbl2 As DataDynamics.ActiveReports.Line
    Private WithEvents lineBabelAmount1 As DataDynamics.ActiveReports.Line
    Private WithEvents lineBabelAmount2 As DataDynamics.ActiveReports.Line
End Class
