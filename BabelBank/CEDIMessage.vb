Option Strict Off
Option Explicit On
Friend Class CEDIMessage
	
	Private mrAppAdmImport As CAppAdmImport
	Private mrXmlDoc As MSXML2.DOMDocument40
	Private mrEDISettingsImport As CEdiSettingsImport
	Friend Function AddXMLHeader(ByRef sSegment As String) As MSXML2.IXMLDOMElement
		Dim node As MSXML2.IXMLDOMElement
		Dim root As MSXML2.IXMLDOMElement
		Dim setNode As MSXML2.IXMLDOMElement
		Dim sElement As String
		
		On Error GoTo errorHandler
		
		'07.07.2008
		If Not EmptyString(sSegment) Then
			If Left(sSegment, mrEDISettingsImport.SegmentLength) <> mrEDISettingsImport.Start Then
				'FIX: Error info
			End If
		End If
		setNode = mrEDISettingsImport.GetElementNode("//HEADER")
		'UPGRADE_WARNING: Couldn't resolve default property of object setNode.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		sElement = setNode.getAttribute("start")
		root = mrXmlDoc.documentElement
		
		'07.07.2008
		If EmptyString(sSegment) Then
			node = mrAppAdmImport.AddXMLElement(root, "UNA", -1, -1)
			node.setAttribute("elementsep", mrEDISettingsImport.ElementSep)
			node.setAttribute("groupsep", mrEDISettingsImport.GroupSep)
			node.setAttribute("decimalsep", mrEDISettingsImport.DecimalSep)
			node.setAttribute("escape", mrEDISettingsImport.EscapeChar)
			node.setAttribute("segmentsep", mrEDISettingsImport.SegmentSep)
			
		Else
			If sElement = Left(sSegment, mrEDISettingsImport.SegmentLength) Then
				node = mrAppAdmImport.AddXMLElement(root, Left(sSegment, mrEDISettingsImport.SegmentLength), -1, -1)
				node.setAttribute("elementsep", Mid(sSegment, mrEDISettingsImport.SegmentLength + 1, 1))
				mrEDISettingsImport.ElementSep = Mid(sSegment, mrEDISettingsImport.SegmentLength + 1, 1)
				node.setAttribute("groupsep", Mid(sSegment, mrEDISettingsImport.SegmentLength + 2, 1))
				mrEDISettingsImport.GroupSep = Mid(sSegment, mrEDISettingsImport.SegmentLength + 2, 1)
				node.setAttribute("decimalsep", Mid(sSegment, mrEDISettingsImport.SegmentLength + 3, 1))
				mrEDISettingsImport.DecimalSep = Mid(sSegment, mrEDISettingsImport.SegmentLength + 3, 1)
				node.setAttribute("escape", Mid(sSegment, mrEDISettingsImport.SegmentLength + 4, 1))
				mrEDISettingsImport.EscapeChar = Mid(sSegment, mrEDISettingsImport.SegmentLength + 4, 1)
				node.setAttribute("segmentsep", Mid(sSegment, mrEDISettingsImport.SegmentLength + 6, 1))
				mrEDISettingsImport.SegmentSep = Mid(sSegment, mrEDISettingsImport.SegmentLength + 6, 1)
			End If
		End If
		
		AddXMLHeader = node
		Exit Function
errorHandler: 
		
		'UPGRADE_NOTE: Object AddXMLHeader may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		AddXMLHeader = Nothing
		'#If CDEBUG Then
		'    'sgBox Err.Description
		'#End If
		
	End Function
	Public Function Init(ByRef rAppAdmImport As CAppAdmImport, ByRef rXmlDoc As MSXML2.DOMDocument26, ByRef rEDISettingsImport As CEdiSettingsImport) As Boolean
		
		On Error GoTo errorHandler
		
		mrAppAdmImport = rAppAdmImport
		
		If rXmlDoc Is Nothing Then GoTo errorHandler
		mrXmlDoc = rXmlDoc
		
		If rEDISettingsImport Is Nothing Then GoTo errorHandler
		mrEDISettingsImport = rEDISettingsImport
		
		'mbInitOk = True
		
		Init = True
		
		Exit Function
		
errorHandler: 
		
		Init = False
		
	End Function
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		
		'mbInitOk = False
		
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
End Class
