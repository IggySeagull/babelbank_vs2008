<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmCorrect
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdPrint As System.Windows.Forms.Button
    'Public WithEvents DTDate_Payment As AxMSComCtl2.AxDTPicker
	Public WithEvents cmdENameAdress As System.Windows.Forms.Button
	Public WithEvents cmdCancelThis As System.Windows.Forms.Button
	Public WithEvents cmdOtherInfo As System.Windows.Forms.Button
	Public WithEvents cmdBankInfo As System.Windows.Forms.Button
	Public WithEvents txtTotalAmount As System.Windows.Forms.TextBox
	Public WithEvents txtNoOfPayments As System.Windows.Forms.TextBox
	Public WithEvents txtPayType As System.Windows.Forms.TextBox
	Public WithEvents txtPayOwnRef As System.Windows.Forms.TextBox
	Public WithEvents txtCompanyNo As System.Windows.Forms.TextBox
	Public WithEvents txtI_Name As System.Windows.Forms.TextBox
	Public WithEvents txtFreetext As System.Windows.Forms.TextBox
	Public WithEvents txtInvOwnRef As System.Windows.Forms.TextBox
	Public WithEvents txtInvMON_InvoiceAmount As System.Windows.Forms.TextBox
	Public WithEvents cmdDelete As System.Windows.Forms.Button
	Public WithEvents cmdNext As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents chkCheque As System.Windows.Forms.CheckBox
	Public WithEvents txtMON_InvoiceCurrency As System.Windows.Forms.TextBox
	Public WithEvents txtE_Name_Adress As System.Windows.Forms.TextBox
	Public WithEvents txtE_Account As System.Windows.Forms.TextBox
	Public WithEvents txtPayMON_InvoiceAmount As System.Windows.Forms.TextBox
	Public WithEvents txtI_Account As System.Windows.Forms.TextBox
	Public WithEvents txtFilename As System.Windows.Forms.TextBox
	Public WithEvents _imgExclamation_19 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_18 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_18 As System.Windows.Forms.PictureBox
	Public WithEvents lblFreetext As System.Windows.Forms.Label
	Public WithEvents lblInvOwnRef As System.Windows.Forms.Label
	Public WithEvents lblInvMON_InvoiceAmount As System.Windows.Forms.Label
	Public WithEvents lblPayType As System.Windows.Forms.Label
    Public WithEvents lblPayOwnRef As System.Windows.Forms.Label
	Public WithEvents lblCompanyNo As System.Windows.Forms.Label
	Public WithEvents lblI_Name As System.Windows.Forms.Label
	Public WithEvents lblTotalAmount As System.Windows.Forms.Label
	Public WithEvents lblNoOfPayments As System.Windows.Forms.Label
	Public WithEvents _imgExclamation_17 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_16 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_17 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_15 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_16 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_15 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_8 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_11 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_13 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_1 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_3 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_5 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_7 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_9 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_10 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_12 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_2 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_4 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_6 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_0 As System.Windows.Forms.PictureBox
	Public WithEvents lblNoOfErrors As System.Windows.Forms.Label
	Public WithEvents lblErrMessage As System.Windows.Forms.Label
	Public WithEvents _Image1_0 As System.Windows.Forms.PictureBox
    Public WithEvents lblE_Name_Adress As System.Windows.Forms.Label
	Public WithEvents lblE_Account As System.Windows.Forms.Label
	Public WithEvents lblMON_InvoiceCurrency As System.Windows.Forms.Label
	Public WithEvents lblPayMON_InvoiceAmount As System.Windows.Forms.Label
	Public WithEvents lblDATE_Payment As System.Windows.Forms.Label
	Public WithEvents lblI_Account As System.Windows.Forms.Label
    Public WithEvents lblFilename As System.Windows.Forms.Label
	Public WithEvents _imgExclamation_1 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_2 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_3 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_4 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_5 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_6 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_7 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_8 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_9 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_10 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_11 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_12 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_13 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_14 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_14 As System.Windows.Forms.PictureBox
	Public WithEvents _imgExclamation_0 As System.Windows.Forms.PictureBox
	Public WithEvents _Image2_19 As System.Windows.Forms.PictureBox
	Public WithEvents Image1 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	Public WithEvents Image2 As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	Public WithEvents imgExclamation As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCorrect))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdPrint = New System.Windows.Forms.Button
        Me.cmdENameAdress = New System.Windows.Forms.Button
        Me.cmdCancelThis = New System.Windows.Forms.Button
        Me.cmdOtherInfo = New System.Windows.Forms.Button
        Me.cmdBankInfo = New System.Windows.Forms.Button
        Me.txtTotalAmount = New System.Windows.Forms.TextBox
        Me.txtNoOfPayments = New System.Windows.Forms.TextBox
        Me.txtPayType = New System.Windows.Forms.TextBox
        Me.txtPayOwnRef = New System.Windows.Forms.TextBox
        Me.txtCompanyNo = New System.Windows.Forms.TextBox
        Me.txtI_Name = New System.Windows.Forms.TextBox
        Me.txtFreetext = New System.Windows.Forms.TextBox
        Me.txtInvOwnRef = New System.Windows.Forms.TextBox
        Me.txtInvMON_InvoiceAmount = New System.Windows.Forms.TextBox
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.cmdNext = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.chkCheque = New System.Windows.Forms.CheckBox
        Me.txtMON_InvoiceCurrency = New System.Windows.Forms.TextBox
        Me.txtE_Name_Adress = New System.Windows.Forms.TextBox
        Me.txtE_Account = New System.Windows.Forms.TextBox
        Me.txtPayMON_InvoiceAmount = New System.Windows.Forms.TextBox
        Me.txtI_Account = New System.Windows.Forms.TextBox
        Me.txtFilename = New System.Windows.Forms.TextBox
        Me._imgExclamation_19 = New System.Windows.Forms.PictureBox
        Me._Image2_18 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_18 = New System.Windows.Forms.PictureBox
        Me.lblFreetext = New System.Windows.Forms.Label
        Me.lblInvOwnRef = New System.Windows.Forms.Label
        Me.lblInvMON_InvoiceAmount = New System.Windows.Forms.Label
        Me.lblPayType = New System.Windows.Forms.Label
        Me.lblPayOwnRef = New System.Windows.Forms.Label
        Me.lblCompanyNo = New System.Windows.Forms.Label
        Me.lblI_Name = New System.Windows.Forms.Label
        Me.lblTotalAmount = New System.Windows.Forms.Label
        Me.lblNoOfPayments = New System.Windows.Forms.Label
        Me._imgExclamation_17 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_16 = New System.Windows.Forms.PictureBox
        Me._Image2_17 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_15 = New System.Windows.Forms.PictureBox
        Me._Image2_16 = New System.Windows.Forms.PictureBox
        Me._Image2_15 = New System.Windows.Forms.PictureBox
        Me._Image2_8 = New System.Windows.Forms.PictureBox
        Me._Image2_11 = New System.Windows.Forms.PictureBox
        Me._Image2_13 = New System.Windows.Forms.PictureBox
        Me._Image2_1 = New System.Windows.Forms.PictureBox
        Me._Image2_3 = New System.Windows.Forms.PictureBox
        Me._Image2_5 = New System.Windows.Forms.PictureBox
        Me._Image2_7 = New System.Windows.Forms.PictureBox
        Me._Image2_9 = New System.Windows.Forms.PictureBox
        Me._Image2_10 = New System.Windows.Forms.PictureBox
        Me._Image2_12 = New System.Windows.Forms.PictureBox
        Me._Image2_2 = New System.Windows.Forms.PictureBox
        Me._Image2_4 = New System.Windows.Forms.PictureBox
        Me._Image2_6 = New System.Windows.Forms.PictureBox
        Me._Image2_0 = New System.Windows.Forms.PictureBox
        Me.lblNoOfErrors = New System.Windows.Forms.Label
        Me.lblErrMessage = New System.Windows.Forms.Label
        Me._Image1_0 = New System.Windows.Forms.PictureBox
        Me.lblE_Name_Adress = New System.Windows.Forms.Label
        Me.lblE_Account = New System.Windows.Forms.Label
        Me.lblMON_InvoiceCurrency = New System.Windows.Forms.Label
        Me.lblPayMON_InvoiceAmount = New System.Windows.Forms.Label
        Me.lblDATE_Payment = New System.Windows.Forms.Label
        Me.lblI_Account = New System.Windows.Forms.Label
        Me.lblFilename = New System.Windows.Forms.Label
        Me._imgExclamation_1 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_2 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_3 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_4 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_5 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_6 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_7 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_8 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_9 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_10 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_11 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_12 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_13 = New System.Windows.Forms.PictureBox
        Me._Image2_14 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_14 = New System.Windows.Forms.PictureBox
        Me._imgExclamation_0 = New System.Windows.Forms.PictureBox
        Me._Image2_19 = New System.Windows.Forms.PictureBox
        Me.Image1 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.Image2 = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.imgExclamation = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        Me.txtDate_Payment = New System.Windows.Forms.TextBox
        CType(Me._imgExclamation_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._imgExclamation_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._Image2_19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgExclamation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdPrint
        '
        Me.cmdPrint.BackColor = System.Drawing.SystemColors.Control
        Me.cmdPrint.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdPrint.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPrint.Location = New System.Drawing.Point(300, 534)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdPrint.Size = New System.Drawing.Size(80, 25)
        Me.cmdPrint.TabIndex = 42
        Me.cmdPrint.TabStop = False
        Me.cmdPrint.Text = "55003 - Print"
        Me.cmdPrint.UseVisualStyleBackColor = False
        '
        'cmdENameAdress
        '
        Me.cmdENameAdress.BackColor = System.Drawing.SystemColors.Control
        Me.cmdENameAdress.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdENameAdress.Enabled = False
        Me.cmdENameAdress.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdENameAdress.Location = New System.Drawing.Point(5, 197)
        Me.cmdENameAdress.Name = "cmdENameAdress"
        Me.cmdENameAdress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdENameAdress.Size = New System.Drawing.Size(134, 32)
        Me.cmdENameAdress.TabIndex = 40
        Me.cmdENameAdress.TabStop = False
        Me.cmdENameAdress.Text = "55019 - Change Name/Adress"
        Me.cmdENameAdress.UseVisualStyleBackColor = False
        '
        'cmdCancelThis
        '
        Me.cmdCancelThis.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancelThis.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancelThis.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancelThis.Location = New System.Drawing.Point(546, 534)
        Me.cmdCancelThis.Name = "cmdCancelThis"
        Me.cmdCancelThis.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancelThis.Size = New System.Drawing.Size(80, 25)
        Me.cmdCancelThis.TabIndex = 39
        Me.cmdCancelThis.TabStop = False
        Me.cmdCancelThis.Text = "55018 - Cancel this"
        Me.cmdCancelThis.UseVisualStyleBackColor = False
        '
        'cmdOtherInfo
        '
        Me.cmdOtherInfo.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOtherInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOtherInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOtherInfo.Location = New System.Drawing.Point(499, 285)
        Me.cmdOtherInfo.Name = "cmdOtherInfo"
        Me.cmdOtherInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOtherInfo.Size = New System.Drawing.Size(200, 18)
        Me.cmdOtherInfo.TabIndex = 16
        Me.cmdOtherInfo.TabStop = False
        Me.cmdOtherInfo.Text = "60094 - Other information"
        Me.cmdOtherInfo.UseVisualStyleBackColor = False
        '
        'cmdBankInfo
        '
        Me.cmdBankInfo.BackColor = System.Drawing.SystemColors.Control
        Me.cmdBankInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdBankInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdBankInfo.Location = New System.Drawing.Point(146, 286)
        Me.cmdBankInfo.Name = "cmdBankInfo"
        Me.cmdBankInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdBankInfo.Size = New System.Drawing.Size(201, 17)
        Me.cmdBankInfo.TabIndex = 15
        Me.cmdBankInfo.TabStop = False
        Me.cmdBankInfo.Text = "60093 - Bank information"
        Me.cmdBankInfo.UseVisualStyleBackColor = False
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.AcceptsReturn = True
        Me.txtTotalAmount.BackColor = System.Drawing.SystemColors.Window
        Me.txtTotalAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtTotalAmount.Enabled = False
        Me.txtTotalAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtTotalAmount.Location = New System.Drawing.Point(145, 84)
        Me.txtTotalAmount.MaxLength = 0
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtTotalAmount.Size = New System.Drawing.Size(101, 20)
        Me.txtTotalAmount.TabIndex = 1
        Me.txtTotalAmount.Text = "<B.MON_InvoiceAmount>"
        '
        'txtNoOfPayments
        '
        Me.txtNoOfPayments.AcceptsReturn = True
        Me.txtNoOfPayments.BackColor = System.Drawing.SystemColors.Window
        Me.txtNoOfPayments.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNoOfPayments.Enabled = False
        Me.txtNoOfPayments.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNoOfPayments.Location = New System.Drawing.Point(499, 84)
        Me.txtNoOfPayments.MaxLength = 0
        Me.txtNoOfPayments.Name = "txtNoOfPayments"
        Me.txtNoOfPayments.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNoOfPayments.Size = New System.Drawing.Size(45, 20)
        Me.txtNoOfPayments.TabIndex = 2
        Me.txtNoOfPayments.Text = "    4"
        Me.txtNoOfPayments.Visible = False
        '
        'txtPayType
        '
        Me.txtPayType.AcceptsReturn = True
        Me.txtPayType.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayType.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayType.Enabled = False
        Me.txtPayType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayType.Location = New System.Drawing.Point(499, 255)
        Me.txtPayType.MaxLength = 0
        Me.txtPayType.Name = "txtPayType"
        Me.txtPayType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayType.Size = New System.Drawing.Size(200, 20)
        Me.txtPayType.TabIndex = 11
        Me.txtPayType.Text = "<P.PayType>"
        '
        'txtPayOwnRef
        '
        Me.txtPayOwnRef.AcceptsReturn = True
        Me.txtPayOwnRef.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayOwnRef.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayOwnRef.Enabled = False
        Me.txtPayOwnRef.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayOwnRef.Location = New System.Drawing.Point(499, 133)
        Me.txtPayOwnRef.MaxLength = 0
        Me.txtPayOwnRef.Name = "txtPayOwnRef"
        Me.txtPayOwnRef.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayOwnRef.Size = New System.Drawing.Size(191, 20)
        Me.txtPayOwnRef.TabIndex = 6
        Me.txtPayOwnRef.Text = "<P.REF_Own>"
        '
        'txtCompanyNo
        '
        Me.txtCompanyNo.AcceptsReturn = True
        Me.txtCompanyNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtCompanyNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCompanyNo.Enabled = False
        Me.txtCompanyNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCompanyNo.Location = New System.Drawing.Point(499, 112)
        Me.txtCompanyNo.MaxLength = 0
        Me.txtCompanyNo.Name = "txtCompanyNo"
        Me.txtCompanyNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCompanyNo.Size = New System.Drawing.Size(200, 20)
        Me.txtCompanyNo.TabIndex = 5
        Me.txtCompanyNo.Text = "<B.CompanyNo>"
        '
        'txtI_Name
        '
        Me.txtI_Name.AcceptsReturn = True
        Me.txtI_Name.BackColor = System.Drawing.SystemColors.Window
        Me.txtI_Name.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtI_Name.Enabled = False
        Me.txtI_Name.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtI_Name.Location = New System.Drawing.Point(145, 113)
        Me.txtI_Name.MaxLength = 0
        Me.txtI_Name.Name = "txtI_Name"
        Me.txtI_Name.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtI_Name.Size = New System.Drawing.Size(201, 20)
        Me.txtI_Name.TabIndex = 3
        Me.txtI_Name.Text = "<P.I_Name>"
        '
        'txtFreetext
        '
        Me.txtFreetext.AcceptsReturn = True
        Me.txtFreetext.BackColor = System.Drawing.SystemColors.Window
        Me.txtFreetext.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFreetext.Enabled = False
        Me.txtFreetext.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFreetext.Location = New System.Drawing.Point(145, 339)
        Me.txtFreetext.MaxLength = 0
        Me.txtFreetext.Multiline = True
        Me.txtFreetext.Name = "txtFreetext"
        Me.txtFreetext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFreetext.Size = New System.Drawing.Size(304, 35)
        Me.txtFreetext.TabIndex = 14
        Me.txtFreetext.Text = "<Freetext/KID>"
        '
        'txtInvOwnRef
        '
        Me.txtInvOwnRef.AcceptsReturn = True
        Me.txtInvOwnRef.BackColor = System.Drawing.SystemColors.Window
        Me.txtInvOwnRef.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtInvOwnRef.Enabled = False
        Me.txtInvOwnRef.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtInvOwnRef.Location = New System.Drawing.Point(500, 314)
        Me.txtInvOwnRef.MaxLength = 0
        Me.txtInvOwnRef.Name = "txtInvOwnRef"
        Me.txtInvOwnRef.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtInvOwnRef.Size = New System.Drawing.Size(200, 20)
        Me.txtInvOwnRef.TabIndex = 13
        Me.txtInvOwnRef.Text = "<I.REF_Own>"
        '
        'txtInvMON_InvoiceAmount
        '
        Me.txtInvMON_InvoiceAmount.AcceptsReturn = True
        Me.txtInvMON_InvoiceAmount.BackColor = System.Drawing.SystemColors.Window
        Me.txtInvMON_InvoiceAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtInvMON_InvoiceAmount.Enabled = False
        Me.txtInvMON_InvoiceAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtInvMON_InvoiceAmount.Location = New System.Drawing.Point(145, 315)
        Me.txtInvMON_InvoiceAmount.MaxLength = 0
        Me.txtInvMON_InvoiceAmount.Name = "txtInvMON_InvoiceAmount"
        Me.txtInvMON_InvoiceAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtInvMON_InvoiceAmount.Size = New System.Drawing.Size(92, 20)
        Me.txtInvMON_InvoiceAmount.TabIndex = 12
        Me.txtInvMON_InvoiceAmount.Text = "<I.MON_InvoiceAmount"
        Me.txtInvMON_InvoiceAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cmdDelete
        '
        Me.cmdDelete.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDelete.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDelete.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDelete.Location = New System.Drawing.Point(464, 534)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDelete.Size = New System.Drawing.Size(80, 25)
        Me.cmdDelete.TabIndex = 19
        Me.cmdDelete.TabStop = False
        Me.cmdDelete.Text = "55014 - Delete"
        Me.cmdDelete.UseVisualStyleBackColor = False
        '
        'cmdNext
        '
        Me.cmdNext.BackColor = System.Drawing.SystemColors.Control
        Me.cmdNext.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdNext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNext.Location = New System.Drawing.Point(382, 534)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdNext.Size = New System.Drawing.Size(80, 25)
        Me.cmdNext.TabIndex = 18
        Me.cmdNext.TabStop = False
        Me.cmdNext.Text = "55013 - Next"
        Me.cmdNext.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(628, 534)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(80, 25)
        Me.cmdCancel.TabIndex = 20
        Me.cmdCancel.TabStop = False
        Me.cmdCancel.Text = "55001 - Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'chkCheque
        '
        Me.chkCheque.BackColor = System.Drawing.SystemColors.Control
        Me.chkCheque.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkCheque.Enabled = False
        Me.chkCheque.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkCheque.Location = New System.Drawing.Point(145, 255)
        Me.chkCheque.Name = "chkCheque"
        Me.chkCheque.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkCheque.Size = New System.Drawing.Size(207, 21)
        Me.chkCheque.TabIndex = 17
        Me.chkCheque.TabStop = False
        Me.chkCheque.Text = "60104 - Cheque payment"
        Me.chkCheque.UseVisualStyleBackColor = False
        '
        'txtMON_InvoiceCurrency
        '
        Me.txtMON_InvoiceCurrency.AcceptsReturn = True
        Me.txtMON_InvoiceCurrency.BackColor = System.Drawing.SystemColors.Window
        Me.txtMON_InvoiceCurrency.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMON_InvoiceCurrency.Enabled = False
        Me.txtMON_InvoiceCurrency.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMON_InvoiceCurrency.Location = New System.Drawing.Point(499, 234)
        Me.txtMON_InvoiceCurrency.MaxLength = 0
        Me.txtMON_InvoiceCurrency.Name = "txtMON_InvoiceCurrency"
        Me.txtMON_InvoiceCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMON_InvoiceCurrency.Size = New System.Drawing.Size(200, 20)
        Me.txtMON_InvoiceCurrency.TabIndex = 10
        Me.txtMON_InvoiceCurrency.Text = "<MON_InvoiceCurrency"
        '
        'txtE_Name_Adress
        '
        Me.txtE_Name_Adress.AcceptsReturn = True
        Me.txtE_Name_Adress.BackColor = System.Drawing.SystemColors.Window
        Me.txtE_Name_Adress.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtE_Name_Adress.Enabled = False
        Me.txtE_Name_Adress.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtE_Name_Adress.Location = New System.Drawing.Point(145, 169)
        Me.txtE_Name_Adress.MaxLength = 0
        Me.txtE_Name_Adress.Multiline = True
        Me.txtE_Name_Adress.Name = "txtE_Name_Adress"
        Me.txtE_Name_Adress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtE_Name_Adress.Size = New System.Drawing.Size(201, 83)
        Me.txtE_Name_Adress.TabIndex = 7
        Me.txtE_Name_Adress.Text = "<P.E_Name/Adress>"
        '
        'txtE_Account
        '
        Me.txtE_Account.AcceptsReturn = True
        Me.txtE_Account.BackColor = System.Drawing.SystemColors.Window
        Me.txtE_Account.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtE_Account.Enabled = False
        Me.txtE_Account.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtE_Account.Location = New System.Drawing.Point(497, 169)
        Me.txtE_Account.MaxLength = 0
        Me.txtE_Account.Name = "txtE_Account"
        Me.txtE_Account.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtE_Account.Size = New System.Drawing.Size(200, 20)
        Me.txtE_Account.TabIndex = 8
        Me.txtE_Account.Text = "<E_Account>"
        '
        'txtPayMON_InvoiceAmount
        '
        Me.txtPayMON_InvoiceAmount.AcceptsReturn = True
        Me.txtPayMON_InvoiceAmount.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayMON_InvoiceAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPayMON_InvoiceAmount.Enabled = False
        Me.txtPayMON_InvoiceAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPayMON_InvoiceAmount.Location = New System.Drawing.Point(499, 212)
        Me.txtPayMON_InvoiceAmount.MaxLength = 0
        Me.txtPayMON_InvoiceAmount.Name = "txtPayMON_InvoiceAmount"
        Me.txtPayMON_InvoiceAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPayMON_InvoiceAmount.Size = New System.Drawing.Size(200, 20)
        Me.txtPayMON_InvoiceAmount.TabIndex = 9
        Me.txtPayMON_InvoiceAmount.Text = "<P.MON_InvoiceAmount>"
        '
        'txtI_Account
        '
        Me.txtI_Account.AcceptsReturn = True
        Me.txtI_Account.BackColor = System.Drawing.SystemColors.Window
        Me.txtI_Account.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtI_Account.Enabled = False
        Me.txtI_Account.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtI_Account.Location = New System.Drawing.Point(145, 136)
        Me.txtI_Account.MaxLength = 0
        Me.txtI_Account.Name = "txtI_Account"
        Me.txtI_Account.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtI_Account.Size = New System.Drawing.Size(201, 20)
        Me.txtI_Account.TabIndex = 4
        Me.txtI_Account.Text = "<P.I_Account>"
        '
        'txtFilename
        '
        Me.txtFilename.AcceptsReturn = True
        Me.txtFilename.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilename.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilename.Enabled = False
        Me.txtFilename.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilename.Location = New System.Drawing.Point(145, 62)
        Me.txtFilename.MaxLength = 0
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilename.Size = New System.Drawing.Size(545, 20)
        Me.txtFilename.TabIndex = 0
        Me.txtFilename.Text = "<Import Filename>"
        '
        '_imgExclamation_19
        '
        Me._imgExclamation_19.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_19.Image = CType(resources.GetObject("_imgExclamation_19.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_19, CType(19, Short))
        Me._imgExclamation_19.Location = New System.Drawing.Point(16, 487)
        Me._imgExclamation_19.Name = "_imgExclamation_19"
        Me._imgExclamation_19.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_19.TabIndex = 43
        Me._imgExclamation_19.TabStop = False
        Me._imgExclamation_19.Visible = False
        '
        '_Image2_18
        '
        Me._Image2_18.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_18.Image = CType(resources.GetObject("_Image2_18.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_18, CType(18, Short))
        Me._Image2_18.Location = New System.Drawing.Point(126, 257)
        Me._Image2_18.Name = "_Image2_18"
        Me._Image2_18.Size = New System.Drawing.Size(18, 18)
        Me._Image2_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_18.TabIndex = 44
        Me._Image2_18.TabStop = False
        Me._Image2_18.Visible = False
        '
        '_imgExclamation_18
        '
        Me._imgExclamation_18.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_18.Image = CType(resources.GetObject("_imgExclamation_18.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_18, CType(18, Short))
        Me._imgExclamation_18.Location = New System.Drawing.Point(126, 256)
        Me._imgExclamation_18.Name = "_imgExclamation_18"
        Me._imgExclamation_18.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_18.TabIndex = 45
        Me._imgExclamation_18.TabStop = False
        Me._imgExclamation_18.Visible = False
        '
        'lblFreetext
        '
        Me.lblFreetext.BackColor = System.Drawing.SystemColors.Control
        Me.lblFreetext.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFreetext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFreetext.Location = New System.Drawing.Point(7, 344)
        Me.lblFreetext.Name = "lblFreetext"
        Me.lblFreetext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFreetext.Size = New System.Drawing.Size(107, 16)
        Me.lblFreetext.TabIndex = 38
        Me.lblFreetext.Text = "60073 - Freetext/KID"
        '
        'lblInvOwnRef
        '
        Me.lblInvOwnRef.BackColor = System.Drawing.SystemColors.Control
        Me.lblInvOwnRef.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblInvOwnRef.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblInvOwnRef.Location = New System.Drawing.Point(360, 317)
        Me.lblInvOwnRef.Name = "lblInvOwnRef"
        Me.lblInvOwnRef.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblInvOwnRef.Size = New System.Drawing.Size(107, 16)
        Me.lblInvOwnRef.TabIndex = 37
        Me.lblInvOwnRef.Text = "60072 - OwnRef"
        '
        'lblInvMON_InvoiceAmount
        '
        Me.lblInvMON_InvoiceAmount.BackColor = System.Drawing.SystemColors.Control
        Me.lblInvMON_InvoiceAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblInvMON_InvoiceAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblInvMON_InvoiceAmount.Location = New System.Drawing.Point(7, 317)
        Me.lblInvMON_InvoiceAmount.Name = "lblInvMON_InvoiceAmount"
        Me.lblInvMON_InvoiceAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblInvMON_InvoiceAmount.Size = New System.Drawing.Size(107, 16)
        Me.lblInvMON_InvoiceAmount.TabIndex = 36
        Me.lblInvMON_InvoiceAmount.Text = "60060 - Amount"
        '
        'lblPayType
        '
        Me.lblPayType.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayType.Location = New System.Drawing.Point(361, 260)
        Me.lblPayType.Name = "lblPayType"
        Me.lblPayType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayType.Size = New System.Drawing.Size(118, 16)
        Me.lblPayType.TabIndex = 35
        Me.lblPayType.Text = "60091 - PayType"
        '
        'lblPayOwnRef
        '
        Me.lblPayOwnRef.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayOwnRef.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayOwnRef.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayOwnRef.Location = New System.Drawing.Point(360, 139)
        Me.lblPayOwnRef.Name = "lblPayOwnRef"
        Me.lblPayOwnRef.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayOwnRef.Size = New System.Drawing.Size(120, 16)
        Me.lblPayOwnRef.TabIndex = 34
        Me.lblPayOwnRef.Text = "60072 - Ownref"
        '
        'lblCompanyNo
        '
        Me.lblCompanyNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblCompanyNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompanyNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompanyNo.Location = New System.Drawing.Point(360, 115)
        Me.lblCompanyNo.Name = "lblCompanyNo"
        Me.lblCompanyNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompanyNo.Size = New System.Drawing.Size(121, 16)
        Me.lblCompanyNo.TabIndex = 33
        Me.lblCompanyNo.Text = "60095 - CompanyNo"
        '
        'lblI_Name
        '
        Me.lblI_Name.BackColor = System.Drawing.SystemColors.Control
        Me.lblI_Name.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblI_Name.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblI_Name.Location = New System.Drawing.Point(7, 112)
        Me.lblI_Name.Name = "lblI_Name"
        Me.lblI_Name.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblI_Name.Size = New System.Drawing.Size(107, 16)
        Me.lblI_Name.TabIndex = 32
        Me.lblI_Name.Text = "60090 - Payer"
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.BackColor = System.Drawing.SystemColors.Control
        Me.lblTotalAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTotalAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTotalAmount.Location = New System.Drawing.Point(7, 88)
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTotalAmount.Size = New System.Drawing.Size(107, 16)
        Me.lblTotalAmount.TabIndex = 31
        Me.lblTotalAmount.Text = "60096 - Total amount"
        '
        'lblNoOfPayments
        '
        Me.lblNoOfPayments.BackColor = System.Drawing.SystemColors.Control
        Me.lblNoOfPayments.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNoOfPayments.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNoOfPayments.Location = New System.Drawing.Point(360, 88)
        Me.lblNoOfPayments.Name = "lblNoOfPayments"
        Me.lblNoOfPayments.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNoOfPayments.Size = New System.Drawing.Size(120, 16)
        Me.lblNoOfPayments.TabIndex = 30
        Me.lblNoOfPayments.Text = "60059 - No of payments"
        Me.lblNoOfPayments.Visible = False
        '
        '_imgExclamation_17
        '
        Me._imgExclamation_17.BackColor = System.Drawing.Color.Transparent
        Me._imgExclamation_17.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_17.Image = CType(resources.GetObject("_imgExclamation_17.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_17, CType(17, Short))
        Me._imgExclamation_17.Location = New System.Drawing.Point(479, 286)
        Me._imgExclamation_17.Name = "_imgExclamation_17"
        Me._imgExclamation_17.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_17.TabIndex = 46
        Me._imgExclamation_17.TabStop = False
        Me._imgExclamation_17.Visible = False
        '
        '_imgExclamation_16
        '
        Me._imgExclamation_16.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_16.Image = CType(resources.GetObject("_imgExclamation_16.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_16, CType(16, Short))
        Me._imgExclamation_16.Location = New System.Drawing.Point(125, 286)
        Me._imgExclamation_16.Name = "_imgExclamation_16"
        Me._imgExclamation_16.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_16.TabIndex = 47
        Me._imgExclamation_16.TabStop = False
        Me._imgExclamation_16.Visible = False
        '
        '_Image2_17
        '
        Me._Image2_17.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_17.Image = CType(resources.GetObject("_Image2_17.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_17, CType(17, Short))
        Me._Image2_17.Location = New System.Drawing.Point(479, 286)
        Me._Image2_17.Name = "_Image2_17"
        Me._Image2_17.Size = New System.Drawing.Size(18, 18)
        Me._Image2_17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_17.TabIndex = 48
        Me._Image2_17.TabStop = False
        Me._Image2_17.Visible = False
        '
        '_imgExclamation_15
        '
        Me._imgExclamation_15.BackColor = System.Drawing.Color.Transparent
        Me._imgExclamation_15.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_15.Image = CType(resources.GetObject("_imgExclamation_15.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_15, CType(15, Short))
        Me._imgExclamation_15.Location = New System.Drawing.Point(125, 342)
        Me._imgExclamation_15.Name = "_imgExclamation_15"
        Me._imgExclamation_15.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_15.TabIndex = 49
        Me._imgExclamation_15.TabStop = False
        Me._imgExclamation_15.Visible = False
        '
        '_Image2_16
        '
        Me._Image2_16.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_16.Image = CType(resources.GetObject("_Image2_16.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_16, CType(16, Short))
        Me._Image2_16.Location = New System.Drawing.Point(125, 285)
        Me._Image2_16.Name = "_Image2_16"
        Me._Image2_16.Size = New System.Drawing.Size(18, 19)
        Me._Image2_16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_16.TabIndex = 50
        Me._Image2_16.TabStop = False
        Me._Image2_16.Visible = False
        '
        '_Image2_15
        '
        Me._Image2_15.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_15.Image = CType(resources.GetObject("_Image2_15.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_15, CType(15, Short))
        Me._Image2_15.Location = New System.Drawing.Point(125, 340)
        Me._Image2_15.Name = "_Image2_15"
        Me._Image2_15.Size = New System.Drawing.Size(18, 19)
        Me._Image2_15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_15.TabIndex = 51
        Me._Image2_15.TabStop = False
        Me._Image2_15.Visible = False
        '
        '_Image2_8
        '
        Me._Image2_8.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_8.Image = CType(resources.GetObject("_Image2_8.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_8, CType(8, Short))
        Me._Image2_8.Location = New System.Drawing.Point(479, 169)
        Me._Image2_8.Name = "_Image2_8"
        Me._Image2_8.Size = New System.Drawing.Size(18, 18)
        Me._Image2_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_8.TabIndex = 52
        Me._Image2_8.TabStop = False
        Me._Image2_8.Visible = False
        '
        '_Image2_11
        '
        Me._Image2_11.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_11.Image = CType(resources.GetObject("_Image2_11.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_11, CType(11, Short))
        Me._Image2_11.Location = New System.Drawing.Point(479, 234)
        Me._Image2_11.Name = "_Image2_11"
        Me._Image2_11.Size = New System.Drawing.Size(18, 18)
        Me._Image2_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_11.TabIndex = 53
        Me._Image2_11.TabStop = False
        Me._Image2_11.Visible = False
        '
        '_Image2_13
        '
        Me._Image2_13.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_13.Image = CType(resources.GetObject("_Image2_13.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_13, CType(13, Short))
        Me._Image2_13.Location = New System.Drawing.Point(125, 315)
        Me._Image2_13.Name = "_Image2_13"
        Me._Image2_13.Size = New System.Drawing.Size(18, 18)
        Me._Image2_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_13.TabIndex = 54
        Me._Image2_13.TabStop = False
        Me._Image2_13.Visible = False
        '
        '_Image2_1
        '
        Me._Image2_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_1.Image = CType(resources.GetObject("_Image2_1.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_1, CType(1, Short))
        Me._Image2_1.Location = New System.Drawing.Point(125, 85)
        Me._Image2_1.Name = "_Image2_1"
        Me._Image2_1.Size = New System.Drawing.Size(18, 18)
        Me._Image2_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_1.TabIndex = 55
        Me._Image2_1.TabStop = False
        Me._Image2_1.Visible = False
        '
        '_Image2_3
        '
        Me._Image2_3.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_3.Image = CType(resources.GetObject("_Image2_3.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_3, CType(3, Short))
        Me._Image2_3.Location = New System.Drawing.Point(125, 113)
        Me._Image2_3.Name = "_Image2_3"
        Me._Image2_3.Size = New System.Drawing.Size(18, 18)
        Me._Image2_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_3.TabIndex = 56
        Me._Image2_3.TabStop = False
        Me._Image2_3.Visible = False
        '
        '_Image2_5
        '
        Me._Image2_5.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_5.Image = CType(resources.GetObject("_Image2_5.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_5, CType(5, Short))
        Me._Image2_5.Location = New System.Drawing.Point(479, 112)
        Me._Image2_5.Name = "_Image2_5"
        Me._Image2_5.Size = New System.Drawing.Size(18, 18)
        Me._Image2_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_5.TabIndex = 57
        Me._Image2_5.TabStop = False
        Me._Image2_5.Visible = False
        '
        '_Image2_7
        '
        Me._Image2_7.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_7.Image = CType(resources.GetObject("_Image2_7.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_7, CType(7, Short))
        Me._Image2_7.Location = New System.Drawing.Point(125, 169)
        Me._Image2_7.Name = "_Image2_7"
        Me._Image2_7.Size = New System.Drawing.Size(18, 18)
        Me._Image2_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_7.TabIndex = 58
        Me._Image2_7.TabStop = False
        Me._Image2_7.Visible = False
        '
        '_Image2_9
        '
        Me._Image2_9.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_9.Image = CType(resources.GetObject("_Image2_9.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_9, CType(9, Short))
        Me._Image2_9.Location = New System.Drawing.Point(479, 191)
        Me._Image2_9.Name = "_Image2_9"
        Me._Image2_9.Size = New System.Drawing.Size(18, 18)
        Me._Image2_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_9.TabIndex = 59
        Me._Image2_9.TabStop = False
        Me._Image2_9.Visible = False
        '
        '_Image2_10
        '
        Me._Image2_10.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_10.Image = CType(resources.GetObject("_Image2_10.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_10, CType(10, Short))
        Me._Image2_10.Location = New System.Drawing.Point(479, 212)
        Me._Image2_10.Name = "_Image2_10"
        Me._Image2_10.Size = New System.Drawing.Size(18, 18)
        Me._Image2_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_10.TabIndex = 60
        Me._Image2_10.TabStop = False
        Me._Image2_10.Visible = False
        '
        '_Image2_12
        '
        Me._Image2_12.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_12.Image = CType(resources.GetObject("_Image2_12.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_12, CType(12, Short))
        Me._Image2_12.Location = New System.Drawing.Point(479, 255)
        Me._Image2_12.Name = "_Image2_12"
        Me._Image2_12.Size = New System.Drawing.Size(18, 18)
        Me._Image2_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_12.TabIndex = 61
        Me._Image2_12.TabStop = False
        Me._Image2_12.Visible = False
        '
        '_Image2_2
        '
        Me._Image2_2.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_2.Image = CType(resources.GetObject("_Image2_2.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_2, CType(2, Short))
        Me._Image2_2.Location = New System.Drawing.Point(479, 84)
        Me._Image2_2.Name = "_Image2_2"
        Me._Image2_2.Size = New System.Drawing.Size(18, 18)
        Me._Image2_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_2.TabIndex = 62
        Me._Image2_2.TabStop = False
        Me._Image2_2.Visible = False
        '
        '_Image2_4
        '
        Me._Image2_4.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_4.Image = CType(resources.GetObject("_Image2_4.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_4, CType(4, Short))
        Me._Image2_4.Location = New System.Drawing.Point(125, 136)
        Me._Image2_4.Name = "_Image2_4"
        Me._Image2_4.Size = New System.Drawing.Size(18, 18)
        Me._Image2_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_4.TabIndex = 63
        Me._Image2_4.TabStop = False
        Me._Image2_4.Visible = False
        '
        '_Image2_6
        '
        Me._Image2_6.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_6.Image = CType(resources.GetObject("_Image2_6.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_6, CType(6, Short))
        Me._Image2_6.Location = New System.Drawing.Point(479, 133)
        Me._Image2_6.Name = "_Image2_6"
        Me._Image2_6.Size = New System.Drawing.Size(18, 18)
        Me._Image2_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_6.TabIndex = 64
        Me._Image2_6.TabStop = False
        Me._Image2_6.Visible = False
        '
        '_Image2_0
        '
        Me._Image2_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_0.Image = CType(resources.GetObject("_Image2_0.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_0, CType(0, Short))
        Me._Image2_0.Location = New System.Drawing.Point(125, 62)
        Me._Image2_0.Name = "_Image2_0"
        Me._Image2_0.Size = New System.Drawing.Size(18, 18)
        Me._Image2_0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_0.TabIndex = 65
        Me._Image2_0.TabStop = False
        Me._Image2_0.Visible = False
        '
        'lblNoOfErrors
        '
        Me.lblNoOfErrors.BackColor = System.Drawing.SystemColors.Control
        Me.lblNoOfErrors.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNoOfErrors.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNoOfErrors.Location = New System.Drawing.Point(7, 534)
        Me.lblNoOfErrors.Name = "lblNoOfErrors"
        Me.lblNoOfErrors.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNoOfErrors.Size = New System.Drawing.Size(292, 34)
        Me.lblNoOfErrors.TabIndex = 29
        Me.lblNoOfErrors.Text = "No of errors"
        '
        'lblErrMessage
        '
        Me.lblErrMessage.BackColor = System.Drawing.SystemColors.Window
        Me.lblErrMessage.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblErrMessage.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblErrMessage.Location = New System.Drawing.Point(39, 488)
        Me.lblErrMessage.Name = "lblErrMessage"
        Me.lblErrMessage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblErrMessage.Size = New System.Drawing.Size(668, 44)
        Me.lblErrMessage.TabIndex = 28
        '
        '_Image1_0
        '
        Me._Image1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.SetIndex(Me._Image1_0, CType(0, Short))
        Me._Image1_0.Location = New System.Drawing.Point(671, 458)
        Me._Image1_0.Name = "_Image1_0"
        Me._Image1_0.Size = New System.Drawing.Size(32, 27)
        Me._Image1_0.TabIndex = 66
        Me._Image1_0.TabStop = False
        '
        'lblE_Name_Adress
        '
        Me.lblE_Name_Adress.BackColor = System.Drawing.SystemColors.Control
        Me.lblE_Name_Adress.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblE_Name_Adress.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblE_Name_Adress.Location = New System.Drawing.Point(7, 171)
        Me.lblE_Name_Adress.Name = "lblE_Name_Adress"
        Me.lblE_Name_Adress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblE_Name_Adress.Size = New System.Drawing.Size(107, 16)
        Me.lblE_Name_Adress.TabIndex = 27
        Me.lblE_Name_Adress.Text = "60071 - Receiver"
        '
        'lblE_Account
        '
        Me.lblE_Account.BackColor = System.Drawing.SystemColors.Control
        Me.lblE_Account.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblE_Account.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblE_Account.Location = New System.Drawing.Point(361, 173)
        Me.lblE_Account.Name = "lblE_Account"
        Me.lblE_Account.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblE_Account.Size = New System.Drawing.Size(117, 16)
        Me.lblE_Account.TabIndex = 26
        Me.lblE_Account.Text = "60064 - To Account"
        '
        'lblMON_InvoiceCurrency
        '
        Me.lblMON_InvoiceCurrency.BackColor = System.Drawing.SystemColors.Control
        Me.lblMON_InvoiceCurrency.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMON_InvoiceCurrency.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMON_InvoiceCurrency.Location = New System.Drawing.Point(360, 238)
        Me.lblMON_InvoiceCurrency.Name = "lblMON_InvoiceCurrency"
        Me.lblMON_InvoiceCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMON_InvoiceCurrency.Size = New System.Drawing.Size(117, 16)
        Me.lblMON_InvoiceCurrency.TabIndex = 25
        Me.lblMON_InvoiceCurrency.Text = "60066 - Currency"
        '
        'lblPayMON_InvoiceAmount
        '
        Me.lblPayMON_InvoiceAmount.BackColor = System.Drawing.SystemColors.Control
        Me.lblPayMON_InvoiceAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayMON_InvoiceAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPayMON_InvoiceAmount.Location = New System.Drawing.Point(360, 216)
        Me.lblPayMON_InvoiceAmount.Name = "lblPayMON_InvoiceAmount"
        Me.lblPayMON_InvoiceAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayMON_InvoiceAmount.Size = New System.Drawing.Size(117, 16)
        Me.lblPayMON_InvoiceAmount.TabIndex = 24
        Me.lblPayMON_InvoiceAmount.Text = "60060 - Amount"
        '
        'lblDATE_Payment
        '
        Me.lblDATE_Payment.BackColor = System.Drawing.SystemColors.Control
        Me.lblDATE_Payment.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDATE_Payment.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDATE_Payment.Location = New System.Drawing.Point(360, 195)
        Me.lblDATE_Payment.Name = "lblDATE_Payment"
        Me.lblDATE_Payment.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDATE_Payment.Size = New System.Drawing.Size(117, 16)
        Me.lblDATE_Payment.TabIndex = 23
        Me.lblDATE_Payment.Text = "60092 - Payment Date"
        '
        'lblI_Account
        '
        Me.lblI_Account.BackColor = System.Drawing.SystemColors.Control
        Me.lblI_Account.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblI_Account.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblI_Account.Location = New System.Drawing.Point(7, 139)
        Me.lblI_Account.Name = "lblI_Account"
        Me.lblI_Account.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblI_Account.Size = New System.Drawing.Size(107, 16)
        Me.lblI_Account.TabIndex = 22
        Me.lblI_Account.Text = "60070 - From account"
        '
        'lblFilename
        '
        Me.lblFilename.BackColor = System.Drawing.SystemColors.Control
        Me.lblFilename.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilename.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilename.Location = New System.Drawing.Point(7, 63)
        Me.lblFilename.Name = "lblFilename"
        Me.lblFilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilename.Size = New System.Drawing.Size(107, 16)
        Me.lblFilename.TabIndex = 21
        Me.lblFilename.Text = "60061 - Filename"
        '
        '_imgExclamation_1
        '
        Me._imgExclamation_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_1.Image = CType(resources.GetObject("_imgExclamation_1.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_1, CType(1, Short))
        Me._imgExclamation_1.Location = New System.Drawing.Point(124, 83)
        Me._imgExclamation_1.Name = "_imgExclamation_1"
        Me._imgExclamation_1.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_1.TabIndex = 67
        Me._imgExclamation_1.TabStop = False
        Me._imgExclamation_1.Visible = False
        '
        '_imgExclamation_2
        '
        Me._imgExclamation_2.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_2.Image = CType(resources.GetObject("_imgExclamation_2.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_2, CType(2, Short))
        Me._imgExclamation_2.Location = New System.Drawing.Point(479, 86)
        Me._imgExclamation_2.Name = "_imgExclamation_2"
        Me._imgExclamation_2.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_2.TabIndex = 68
        Me._imgExclamation_2.TabStop = False
        Me._imgExclamation_2.Visible = False
        '
        '_imgExclamation_3
        '
        Me._imgExclamation_3.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_3.Image = CType(resources.GetObject("_imgExclamation_3.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_3, CType(3, Short))
        Me._imgExclamation_3.Location = New System.Drawing.Point(126, 114)
        Me._imgExclamation_3.Name = "_imgExclamation_3"
        Me._imgExclamation_3.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_3.TabIndex = 69
        Me._imgExclamation_3.TabStop = False
        Me._imgExclamation_3.Visible = False
        '
        '_imgExclamation_4
        '
        Me._imgExclamation_4.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_4.Image = CType(resources.GetObject("_imgExclamation_4.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_4, CType(4, Short))
        Me._imgExclamation_4.Location = New System.Drawing.Point(126, 137)
        Me._imgExclamation_4.Name = "_imgExclamation_4"
        Me._imgExclamation_4.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_4.TabIndex = 70
        Me._imgExclamation_4.TabStop = False
        Me._imgExclamation_4.Visible = False
        '
        '_imgExclamation_5
        '
        Me._imgExclamation_5.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_5.Image = CType(resources.GetObject("_imgExclamation_5.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_5, CType(5, Short))
        Me._imgExclamation_5.Location = New System.Drawing.Point(479, 114)
        Me._imgExclamation_5.Name = "_imgExclamation_5"
        Me._imgExclamation_5.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_5.TabIndex = 71
        Me._imgExclamation_5.TabStop = False
        Me._imgExclamation_5.Visible = False
        '
        '_imgExclamation_6
        '
        Me._imgExclamation_6.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_6.Image = CType(resources.GetObject("_imgExclamation_6.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_6, CType(6, Short))
        Me._imgExclamation_6.Location = New System.Drawing.Point(479, 134)
        Me._imgExclamation_6.Name = "_imgExclamation_6"
        Me._imgExclamation_6.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_6.TabIndex = 72
        Me._imgExclamation_6.TabStop = False
        Me._imgExclamation_6.Visible = False
        '
        '_imgExclamation_7
        '
        Me._imgExclamation_7.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_7.Image = CType(resources.GetObject("_imgExclamation_7.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_7, CType(7, Short))
        Me._imgExclamation_7.Location = New System.Drawing.Point(126, 169)
        Me._imgExclamation_7.Name = "_imgExclamation_7"
        Me._imgExclamation_7.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_7.TabIndex = 73
        Me._imgExclamation_7.TabStop = False
        Me._imgExclamation_7.Visible = False
        '
        '_imgExclamation_8
        '
        Me._imgExclamation_8.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_8.Image = CType(resources.GetObject("_imgExclamation_8.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_8, CType(8, Short))
        Me._imgExclamation_8.Location = New System.Drawing.Point(479, 171)
        Me._imgExclamation_8.Name = "_imgExclamation_8"
        Me._imgExclamation_8.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_8.TabIndex = 74
        Me._imgExclamation_8.TabStop = False
        Me._imgExclamation_8.Visible = False
        '
        '_imgExclamation_9
        '
        Me._imgExclamation_9.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_9.Image = CType(resources.GetObject("_imgExclamation_9.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_9, CType(9, Short))
        Me._imgExclamation_9.Location = New System.Drawing.Point(478, 191)
        Me._imgExclamation_9.Name = "_imgExclamation_9"
        Me._imgExclamation_9.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_9.TabIndex = 75
        Me._imgExclamation_9.TabStop = False
        Me._imgExclamation_9.Visible = False
        '
        '_imgExclamation_10
        '
        Me._imgExclamation_10.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_10.Image = CType(resources.GetObject("_imgExclamation_10.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_10, CType(10, Short))
        Me._imgExclamation_10.Location = New System.Drawing.Point(479, 213)
        Me._imgExclamation_10.Name = "_imgExclamation_10"
        Me._imgExclamation_10.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_10.TabIndex = 76
        Me._imgExclamation_10.TabStop = False
        Me._imgExclamation_10.Visible = False
        '
        '_imgExclamation_11
        '
        Me._imgExclamation_11.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_11.Image = CType(resources.GetObject("_imgExclamation_11.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_11, CType(11, Short))
        Me._imgExclamation_11.Location = New System.Drawing.Point(479, 234)
        Me._imgExclamation_11.Name = "_imgExclamation_11"
        Me._imgExclamation_11.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_11.TabIndex = 77
        Me._imgExclamation_11.TabStop = False
        Me._imgExclamation_11.Visible = False
        '
        '_imgExclamation_12
        '
        Me._imgExclamation_12.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_12.Image = CType(resources.GetObject("_imgExclamation_12.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_12, CType(12, Short))
        Me._imgExclamation_12.Location = New System.Drawing.Point(479, 255)
        Me._imgExclamation_12.Name = "_imgExclamation_12"
        Me._imgExclamation_12.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_12.TabIndex = 78
        Me._imgExclamation_12.TabStop = False
        Me._imgExclamation_12.Visible = False
        '
        '_imgExclamation_13
        '
        Me._imgExclamation_13.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_13.Image = CType(resources.GetObject("_imgExclamation_13.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_13, CType(13, Short))
        Me._imgExclamation_13.Location = New System.Drawing.Point(127, 315)
        Me._imgExclamation_13.Name = "_imgExclamation_13"
        Me._imgExclamation_13.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_13.TabIndex = 79
        Me._imgExclamation_13.TabStop = False
        Me._imgExclamation_13.Visible = False
        '
        '_Image2_14
        '
        Me._Image2_14.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_14.Image = CType(resources.GetObject("_Image2_14.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_14, CType(14, Short))
        Me._Image2_14.Location = New System.Drawing.Point(479, 314)
        Me._Image2_14.Name = "_Image2_14"
        Me._Image2_14.Size = New System.Drawing.Size(18, 18)
        Me._Image2_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_14.TabIndex = 80
        Me._Image2_14.TabStop = False
        Me._Image2_14.Visible = False
        '
        '_imgExclamation_14
        '
        Me._imgExclamation_14.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_14.Image = CType(resources.GetObject("_imgExclamation_14.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_14, CType(14, Short))
        Me._imgExclamation_14.Location = New System.Drawing.Point(478, 313)
        Me._imgExclamation_14.Name = "_imgExclamation_14"
        Me._imgExclamation_14.Size = New System.Drawing.Size(18, 19)
        Me._imgExclamation_14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_14.TabIndex = 81
        Me._imgExclamation_14.TabStop = False
        Me._imgExclamation_14.Visible = False
        '
        '_imgExclamation_0
        '
        Me._imgExclamation_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._imgExclamation_0.Image = CType(resources.GetObject("_imgExclamation_0.Image"), System.Drawing.Image)
        Me.imgExclamation.SetIndex(Me._imgExclamation_0, CType(0, Short))
        Me._imgExclamation_0.Location = New System.Drawing.Point(125, 62)
        Me._imgExclamation_0.Name = "_imgExclamation_0"
        Me._imgExclamation_0.Size = New System.Drawing.Size(18, 16)
        Me._imgExclamation_0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._imgExclamation_0.TabIndex = 82
        Me._imgExclamation_0.TabStop = False
        Me._imgExclamation_0.Visible = False
        '
        '_Image2_19
        '
        Me._Image2_19.Cursor = System.Windows.Forms.Cursors.Default
        Me._Image2_19.Image = CType(resources.GetObject("_Image2_19.Image"), System.Drawing.Image)
        Me.Image2.SetIndex(Me._Image2_19, CType(19, Short))
        Me._Image2_19.Location = New System.Drawing.Point(16, 486)
        Me._Image2_19.Name = "_Image2_19"
        Me._Image2_19.Size = New System.Drawing.Size(18, 18)
        Me._Image2_19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me._Image2_19.TabIndex = 83
        Me._Image2_19.TabStop = False
        Me._Image2_19.Visible = False
        '
        'txtDate_Payment
        '
        Me.txtDate_Payment.AcceptsReturn = True
        Me.txtDate_Payment.BackColor = System.Drawing.SystemColors.Window
        Me.txtDate_Payment.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDate_Payment.Enabled = False
        Me.txtDate_Payment.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDate_Payment.Location = New System.Drawing.Point(497, 191)
        Me.txtDate_Payment.MaxLength = 0
        Me.txtDate_Payment.Name = "txtDate_Payment"
        Me.txtDate_Payment.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDate_Payment.Size = New System.Drawing.Size(87, 20)
        Me.txtDate_Payment.TabIndex = 85
        Me.txtDate_Payment.Text = "<Date_Payment>"
        '
        'frmCorrect
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(715, 573)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtDate_Payment)
        Me.Controls.Add(Me.cmdPrint)
        Me.Controls.Add(Me.cmdENameAdress)
        Me.Controls.Add(Me.cmdCancelThis)
        Me.Controls.Add(Me.cmdOtherInfo)
        Me.Controls.Add(Me.cmdBankInfo)
        Me.Controls.Add(Me.txtTotalAmount)
        Me.Controls.Add(Me.txtNoOfPayments)
        Me.Controls.Add(Me.txtPayType)
        Me.Controls.Add(Me.txtPayOwnRef)
        Me.Controls.Add(Me.txtCompanyNo)
        Me.Controls.Add(Me.txtI_Name)
        Me.Controls.Add(Me.txtFreetext)
        Me.Controls.Add(Me.txtInvOwnRef)
        Me.Controls.Add(Me.txtInvMON_InvoiceAmount)
        Me.Controls.Add(Me.cmdDelete)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.chkCheque)
        Me.Controls.Add(Me.txtMON_InvoiceCurrency)
        Me.Controls.Add(Me.txtE_Name_Adress)
        Me.Controls.Add(Me.txtE_Account)
        Me.Controls.Add(Me.txtPayMON_InvoiceAmount)
        Me.Controls.Add(Me.txtI_Account)
        Me.Controls.Add(Me.txtFilename)
        Me.Controls.Add(Me._imgExclamation_19)
        Me.Controls.Add(Me._Image2_18)
        Me.Controls.Add(Me._imgExclamation_18)
        Me.Controls.Add(Me.lblFreetext)
        Me.Controls.Add(Me.lblInvOwnRef)
        Me.Controls.Add(Me.lblInvMON_InvoiceAmount)
        Me.Controls.Add(Me.lblPayType)
        Me.Controls.Add(Me.lblPayOwnRef)
        Me.Controls.Add(Me.lblCompanyNo)
        Me.Controls.Add(Me.lblI_Name)
        Me.Controls.Add(Me.lblTotalAmount)
        Me.Controls.Add(Me.lblNoOfPayments)
        Me.Controls.Add(Me._imgExclamation_17)
        Me.Controls.Add(Me._imgExclamation_16)
        Me.Controls.Add(Me._Image2_17)
        Me.Controls.Add(Me._imgExclamation_15)
        Me.Controls.Add(Me._Image2_16)
        Me.Controls.Add(Me._Image2_15)
        Me.Controls.Add(Me._Image2_8)
        Me.Controls.Add(Me._Image2_11)
        Me.Controls.Add(Me._Image2_13)
        Me.Controls.Add(Me._Image2_1)
        Me.Controls.Add(Me._Image2_3)
        Me.Controls.Add(Me._Image2_5)
        Me.Controls.Add(Me._Image2_7)
        Me.Controls.Add(Me._Image2_9)
        Me.Controls.Add(Me._Image2_10)
        Me.Controls.Add(Me._Image2_12)
        Me.Controls.Add(Me._Image2_2)
        Me.Controls.Add(Me._Image2_4)
        Me.Controls.Add(Me._Image2_6)
        Me.Controls.Add(Me._Image2_0)
        Me.Controls.Add(Me.lblNoOfErrors)
        Me.Controls.Add(Me.lblErrMessage)
        Me.Controls.Add(Me._Image1_0)
        Me.Controls.Add(Me.lblE_Name_Adress)
        Me.Controls.Add(Me.lblE_Account)
        Me.Controls.Add(Me.lblMON_InvoiceCurrency)
        Me.Controls.Add(Me.lblPayMON_InvoiceAmount)
        Me.Controls.Add(Me.lblDATE_Payment)
        Me.Controls.Add(Me.lblI_Account)
        Me.Controls.Add(Me.lblFilename)
        Me.Controls.Add(Me._imgExclamation_1)
        Me.Controls.Add(Me._imgExclamation_2)
        Me.Controls.Add(Me._imgExclamation_3)
        Me.Controls.Add(Me._imgExclamation_4)
        Me.Controls.Add(Me._imgExclamation_5)
        Me.Controls.Add(Me._imgExclamation_6)
        Me.Controls.Add(Me._imgExclamation_7)
        Me.Controls.Add(Me._imgExclamation_8)
        Me.Controls.Add(Me._imgExclamation_9)
        Me.Controls.Add(Me._imgExclamation_10)
        Me.Controls.Add(Me._imgExclamation_11)
        Me.Controls.Add(Me._imgExclamation_12)
        Me.Controls.Add(Me._imgExclamation_13)
        Me.Controls.Add(Me._Image2_14)
        Me.Controls.Add(Me._imgExclamation_14)
        Me.Controls.Add(Me._imgExclamation_0)
        Me.Controls.Add(Me._Image2_19)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmCorrect"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "60138 - Errors/Warnings"
        CType(Me._imgExclamation_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image1_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._imgExclamation_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._Image2_19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgExclamation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents txtDate_Payment As System.Windows.Forms.TextBox
#End Region
End Class