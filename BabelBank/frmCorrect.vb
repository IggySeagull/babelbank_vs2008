Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmCorrect
    Inherits System.Windows.Forms.Form
    Public BabelFiles_Renamed As vbBabel.BabelFiles
    Public Filename As String
    Public iReturnValue As Short ' 1 - 4 given button pressed by user
    Private frmCorrectBank As frmCorrectBank
    Private frmCorrectOther As frmCorrectOther
    Private frmCorrectNameAddress As frmCorrectNameAddress
    Private Sub thisForm_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        e.Graphics.DrawLine(Pens.Orange, 9, 106, 701, 106)
        e.Graphics.DrawLine(Pens.Orange, 9, 160, 701, 160)
        e.Graphics.DrawLine(Pens.Orange, 9, 283, 701, 283)
        e.Graphics.DrawLine(Pens.Orange, 9, 304, 701, 304)
    End Sub

    Public Sub PassfrmCorrectBank(ByVal frm As frmCorrectBank)
        ' pass frmCorrectBank to frmCorrect
        frmCorrectBank = frm
    End Sub
    Public Sub PassfrmCorrectOther(ByVal frm As frmCorrectOther)
        ' pass frmCorrectOther to frmCorrect
        frmCorrectOther = frm
    End Sub
    Public Sub PassfrmCorrectNameAddress(ByVal frm As frmCorrectNameAddress)
        ' pass frmCorrectNameAddress to frmCorrect
        frmCorrectNameAddress = frm
    End Sub

    Private Sub cmdBankInfo_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdBankInfo.Click
        'frmCorrectBank.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(Me.Top) + 1500)
        'frmCorrectBank.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(Me.Left) + 100)
        VB6.ShowForm(frmCorrectBank, 1, Me)
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        'If MsgBox("Vil du avslutte uten lagring ?", vbYesNo + vbDefaultButton2) = vbYes Then
        If MsgBox(LRS(60119), MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2) = MsgBoxResult.Yes Then
            ' Cancel, Yes
            Me.Hide()
            iReturnValue = 3
        Else
            ' No Cancel, Continue
        End If

    End Sub

    Private Sub cmdCancelThis_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancelThis.Click
        'If MsgBox("Vil du avslutte uten lagring ?", vbYesNo + vbDefaultButton2) = vbYes Then
        If MsgBox(LRS(60119), MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2) = MsgBoxResult.Yes Then
            ' Cancel, Yes
            Me.Hide()
            iReturnValue = 4
        Else
            ' No Cancel, Continue
        End If

    End Sub
    Private Sub cmdDelete_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDelete.Click
        Me.Hide()
        iReturnValue = 2
    End Sub

    Private Sub cmdENameAdress_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdENameAdress.Click
        ' removed05.09.2019 Dim frmCorrectNameAddress As New frmCorrectNameAddress
        Dim sNameAdress As String

        frmCorrectNameAddress.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(Me.Top) + 3000)
        frmCorrectNameAddress.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(Me.Left) + 100)
        frmCorrectNameAddress.txtName.SelectionLength = Len(frmCorrectNameAddress.txtName.Text)
        frmCorrectNameAddress.txtAdr1.SelectionLength = Len(frmCorrectNameAddress.txtAdr1.Text)
        frmCorrectNameAddress.txtAdr2.SelectionLength = Len(frmCorrectNameAddress.txtAdr2.Text)
        frmCorrectNameAddress.txtAdr3.SelectionLength = Len(frmCorrectNameAddress.txtAdr3.Text)
        frmCorrectNameAddress.txtZIP.SelectionLength = Len(frmCorrectNameAddress.txtZIP.Text)
        frmCorrectNameAddress.txtCity.SelectionLength = Len(frmCorrectNameAddress.txtCity.Text)

        VB6.ShowForm(frmCorrectNameAddress, 1, Me)

        ' Update Name/Adress in frmCorrect;
        sNameAdress = frmCorrectNameAddress.txtName.Text & vbCrLf & frmCorrectNameAddress.txtAdr1.Text & vbCrLf & frmCorrectNameAddress.txtAdr2.Text
        sNameAdress = sNameAdress & vbCrLf & frmCorrectNameAddress.txtAdr3.Text & vbCrLf & frmCorrectNameAddress.txtZIP.Text
        sNameAdress = sNameAdress & " " & frmCorrectNameAddress.txtCity.Text ' & vbCrLf & frmCorrectNameAddress.E_CountryCode
        Me.txtE_Name_Adress.Text = sNameAdress

    End Sub

    Private Sub cmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdNext.Click
        Me.Hide()
        iReturnValue = 1
    End Sub

    Private Sub cmdOtherInfo_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOtherInfo.Click
        frmCorrectOther.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(Me.Top) + 1500)
        frmCorrectOther.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(Me.Left) + 100)
        VB6.ShowForm(frmCorrectOther, 1, Me)
    End Sub

    Private Sub cmdPrint_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPrint.Click
        Me.Hide()
        iReturnValue = 5
    End Sub

    Private Sub frmCorrect_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "")
        FormLRSCaptions(Me)
        'Me.lblErrMessage.BackColor = vbRed  'vbHighlight

    End Sub
    Private Sub sprInvoices_GotFocus()
        ' Release errormessage
        'Me.lblErrMessage.Caption = Space(80)
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
    End Sub
    Private Sub cmdCancel_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Enter
        ' Release errormessage
        Me.lblErrMessage.Text = Space(80)
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
    End Sub
    Private Sub cmdCancelThis_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancelThis.Enter
        ' Release errormessage
        Me.lblErrMessage.Text = Space(80)
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
    End Sub
    Private Sub cmdDelete_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDelete.Enter
        ' Release errormessage
        Me.lblErrMessage.Text = Space(80)
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
    End Sub
    Private Sub cmdNext_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdNext.Enter
        ' Release errormessage
        Me.lblErrMessage.Text = Space(80)
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
    End Sub
    Private Sub txtFilename_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFilename.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtFilename.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtTotalAmount_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTotalAmount.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtTotalAmount.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtNoOfpayments_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNoOfPayments.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtNoOfPayments.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtI_Name_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtI_Name.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtI_Name.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub

    Private Sub txtI_Account_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtI_Account.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtI_Account.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtCompanyNo_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCompanyNo.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtCompanyNo.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub

    Private Sub txtPayOwnRef_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPayOwnRef.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtPayOwnRef.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtE_Name_Adress_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtE_Name_Adress.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtE_Name_Adress.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtE_Account_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtE_Account.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtE_Account.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    'Private Sub DTDate_PaymentNew_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
    '    ' x Display errormessage for this field, stored in control.tag;
    '    Me.lblErrMessage.Text = dtDate_PaymentNew.Tag
    '    PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
    '    'SelectAllText
    'End Sub
    'Private Sub txtDATE_Payment_LostFocus()
    '' If Cancel has got focus, dont bother testing;
    'If UCase(Me.ActiveControl.Name) <> "CMDCANCEL" And _
    ''UCase(Me.ActiveControl.Name) <> "CMDCANCEL_THIS" And _
    ''UCase(Me.ActiveControl.Name) <> "CMDDELETE" Then
    '    ' Check if date is not older than todays date
    '    If CDate(Me.txtDATE_Payment.Text) < Date Then
    '        MsgBox LRS(60145) ' The paymentdate stated is overdue
    '        Me.txtDATE_Payment.SetFocus
    '    End If
    'End If
    'End Sub

    Private Sub txtPayMON_InvoiceAmount_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPayMON_InvoiceAmount.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtPayMON_InvoiceAmount.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtMON_InvoiceCurrency_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMON_InvoiceCurrency.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtMON_InvoiceCurrency.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtPayType_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPayType.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtPayType.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtInvMON_InvoiceAmount_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtInvMON_InvoiceAmount.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtInvMON_InvoiceAmount.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtInvMON_InvoiceAmount_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtInvMON_InvoiceAmount.Validating
        Dim Cancel As Boolean = eventArgs.Cancel

        ' When changed, fill spread with updated info
        Dim nRow As Double
        Dim nTotal As Double

        'UPGRADE_ISSUE: TextBox property txtInvMON_InvoiceAmount.DataChanged was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        'TODO - Spreadstuff
        'If txtInvMON_InvoiceAmount.DataChanged And Cancel = False Then
        'UPGRADE_WARNING: Couldn't resolve default property of object Me.sprInvoices. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'


        'With Me.sprInvoices
        '    'UPGRADE_WARNING: Couldn't resolve default property of object Me.sprInvoices. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '    .Col = 1
        '    ' lets hope we're still in the same row as we left ...
        '    'UPGRADE_WARNING: Couldn't resolve default property of object Me.sprInvoices. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '    .Text = Val(Me.txtInvMON_InvoiceAmount.Text)
        '    ' For amounts, must also refresh Payment_MONInvoiceAmount;
        '    ' Calculate total in spread;
        '    'UPGRADE_WARNING: Couldn't resolve default property of object Me.sprInvoices. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '    .Col = 1
        '    'UPGRADE_WARNING: Couldn't resolve default property of object Me.sprInvoices. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '    For nRow = 1 To .MaxRows
        '        'UPGRADE_WARNING: Couldn't resolve default property of object Me.sprInvoices. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '        .Row = nRow
        '        'UPGRADE_WARNING: Couldn't resolve default property of object Me.sprInvoices. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '        nTotal = nTotal + Val(.Text)
        '    Next nRow
        '    'Update forms txtPayMON_InvoiceAmount
        '    Me.txtPayMON_InvoiceAmount.Text = Str(nTotal)
        'End With
        'End If

        eventArgs.Cancel = Cancel
    End Sub

    Private Sub txtInvOwnRef_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtInvOwnRef.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtInvOwnRef.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtFreetext_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFreetext.Enter
        ' x Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtFreetext.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub


    '-------------------------------------------------------
    'Private Sub txtE_CountryCode_GotFocus()
    ' Display errormessage for this field, stored in control.tag;
    'Me.lblErrMessage.Caption = txtE_CountryCode.Tag
    'SelectAllText
    'End Sub

    Sub sprInvoices_LeaveRow(ByVal Row As Integer, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Integer, ByVal NewRowIsLast As Integer, ByRef Cancel As Boolean)
        Dim sprInvoices As Object

        With sprInvoices
            If NewRow > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object sprInvoices.Col. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .Col = 1 ' Amount
                'UPGRADE_WARNING: Couldn't resolve default property of object sprInvoices.Row. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .Row = NewRow
                'UPGRADE_WARNING: Couldn't resolve default property of object sprInvoices.Text. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.txtInvMON_InvoiceAmount.Text = .Text
                'UPGRADE_WARNING: Couldn't resolve default property of object sprInvoices.Col. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .Col = 2 ' REF_Own
                'UPGRADE_WARNING: Couldn't resolve default property of object sprInvoices.Text. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.txtInvOwnRef.Text = .Text
                'UPGRADE_WARNING: Couldn't resolve default property of object sprInvoices.Col. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .Col = 3 ' Freetext/KID
                'UPGRADE_WARNING: Couldn't resolve default property of object sprInvoices.Text. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.txtFreetext.Text = .Text

            End If
            '.SetFocus
        End With

    End Sub


    Private Function PrepareErrorMessage() As Object
        ' Icon for Error or Warning, skip first pos in Text
        If VB.Left(Me.lblErrMessage.Text, 1) = "E" Then
            Me.Image2(19).Visible = True
            Me.imgExclamation(19).Visible = False
            Me.lblErrMessage.ForeColor = System.Drawing.Color.Red
        ElseIf VB.Left(Me.lblErrMessage.Text, 1) = "W" Then
            ' Warning
            Me.Image2(19).Visible = False
            Me.imgExclamation(19).Visible = True
            Me.lblErrMessage.ForeColor = System.Drawing.Color.Red 'vbGreen
        End If
        Me.lblErrMessage.Text = Mid(Me.lblErrMessage.Text, 2)


    End Function
End Class