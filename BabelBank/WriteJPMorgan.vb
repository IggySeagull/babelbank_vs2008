﻿Option Strict Off
Option Explicit On
Module WriteJPMorgan

    Dim sLine As String ' en output-linje
    Dim oBatch As Batch
    ' - filenivå: sum beløp, antall records, antall transer, siste dato
    '             disse påvirkes nedover i nivåene
    Dim nFileSumAmount As Double = 0, nFileNoRecords As Double = 0, nFileNoTransactions As Double = 0
    Dim sPayNumber As String
    Dim bFileStartWritten As Boolean = False
    Dim sHeaderAccount As String  'added 23.05.2016

    Function WriteJPMorgan_Norway(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sCompanyID As String, ByRef sClientNo As String, ByRef sOwnRef As String) As Boolean

        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim i As Double, j As Double, k As Double, l As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim sNewOwnref As String
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean

        ' lag en outputfil
        Try
            nFileNoRecords = 0

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles

                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        'XOKNET - 29.08.2013 - Added And Not oPayment.Exported
                        If oPayment.Cancel = False And Not oPayment.Exported Then ' XOKNET 25.11.2010 added this line and End if
                            Exit For
                        End If 'oPayment.Cancel = False Then ' XOKNET 25.11.2010 added this line and End if
                    Next oPayment
                Next oBatch
            Next oBabel

            For Each oBabel In oBabelFiles
                bExportoBabel = False
                'Only export statuscode 00 or 02. The others don't give any sense in LeverantørsBetalningar
                If (oBabel.StatusCode = "00" And Not oBabel.FileFromBank) _
                    Or oBabel.StatusCode = "02" Then
                    'Have to go through each batch-object to see if we have objects thatt shall
                    ' be exported to this exportfile
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments

                            '04.05.2018 - Added next IF, to be able to export several clients
                            If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoBabel = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBabel = True
                                            End If
                                        End If
                                    End If
                                Else

                                End If
                            End If
                            If bExportoBabel Then
                                Exit For
                            End If

                            'Old code
                            'If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            '    'Don't export payments that have been cancelled
                            '    'XOKNET - 29.08.2013 - Added And Not oPayment.Exported

                            '    If oPayment.Cancel = False And Not oPayment.Exported Then
                            '        bExportoBabel = True
                            '    End If 'If oPayment.Cancel = False Then
                            'End If
                            'If bExportoBabel Then
                            '    Exit For
                            'End If
                        Next
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                Else
                    For Each oBatch In oBabel.Batches
                        For Each oPayment In oBatch.Payments
                            oPayment.Exported = True
                        Next oPayment
                    Next oBatch
                End If 'If oBabel.StatusCode = "00" Or oBabel.StatusCode = "02" Then

                If bExportoBabel Then
                    If Not bFileStartWritten Then
                        ' write only once!

                        ' Possible with file with one domestic and one International batch
                        'Runs first all payments to create a Domestic batch
                        sLine = WriteJPMorganFileHeader(sCompanyID)  ' XOKNET 08.07.2015 added last parameter
                        oFile.WriteLine(sLine)
                    End If
                    bFileStartWritten = True
                End If


                i = 0
                'Loop through all Batch objs. in BabelFile obj.
                For Each oBatch In oBabel.Batches
                    bExportoBatch = False
                    'Have to go through the payment-object to see if we have objects thatt shall
                    ' be exported to this exportfile
                    For Each oPayment In oBatch.Payments

                        '04.05.2018 - Added next IF, to be able to export several clients
                        If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                            If oPayment.Cancel = False Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If InStr(oPayment.REF_Own, "&?") Then
                                            'Set in the part of the OwnRef that BabelBank is using
                                            sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                        Else
                                            sNewOwnref = ""
                                        End If
                                        If sNewOwnref = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            Else

                            End If
                        End If
                        If bExportoBatch Then
                            Exit For
                        End If


                        'Old code
                        'If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                        '    'Don't export payments that have been cancelled
                        '    'XOKNET - 29.08.2013 - Added And Not oPayment.Exported
                        '    If oPayment.Cancel = False And Not oPayment.Exported Then
                        '        bExportoBatch = True
                        '    End If 'If oPayment.Cancel = False Then
                        'End If
                        ''If true exit the loop, and we have data to export
                        'If bExportoBatch Then
                        '    Exit For
                        'End If
                    Next


                    If bExportoBatch Then
                        i = i + 1

                        j = 0
                        For Each oPayment In oBatch.Payments
                            bExportoPayment = False
                            'Have to go through the payment-object to see if we have objects thatt shall
                            ' be exported to this exportfile

                            '04.05.2018 - Added next IF, to be able to export several clients
                            If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported Then
                                If oPayment.Cancel = False Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                Else

                                End If
                            End If

                            'Old code
                            'If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            '    'Don't export payments that have been cancelled
                            '    'XOKNET - 29.08.2013 - Added And Not oPayment.Exported
                            '    If oPayment.Cancel = False And Not oPayment.Exported Then
                            '        bExportoPayment = True
                            '    End If 'If oPayment.Cancel = False Then
                            'End If

                            If bExportoPayment Then
                                j = j + 1

                                For Each oInvoice In oPayment.Invoices

                                    If Not oInvoice.Exported Then

                                        sLine = WriteJPMorganTransactionRecord(oBatch, oPayment, oInvoice)
                                        oFile.WriteLine(sLine)

                                    End If ' If Not oInvoice.Exported Then
                                    oInvoice.Exported = True
                                Next oInvoice

                                oPayment.Exported = True
                            End If   '                            If bExportoPayment Then

                        Next ' payment

                    End If

                Next 'batch
            Next 'Babelfile

            If nFileNoRecords > 0 Then
                sLine = WriteJPMorganFileTrailer()
                oFile.WriteLine(sLine)
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteJPMorgan_Norway" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
            End If
            oFile = Nothing

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteJPMorgan_Norway = True

    End Function
    Function WriteJPMorganFileHeader(ByVal sCompanyID As String) As String
        Dim sLine As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileNoRecords = 1
        nFileNoTransactions = 0

        sLine = "FH,"        ' 1
        sLine = sLine & sCompanyID & "," '2
        sLine = sLine & Format(Date.Today, "yyyyMMdd") & "," '3 File create date YYYYMMDD
        sLine = sLine & Format(Now, "HHmmss") & ","   '4 create time
        sLine = sLine & "01100"    '5 Fileversion 11

        WriteJPMorganFileHeader = sLine

    End Function
    Function WriteJPMorganTransactionRecord(ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal oInvoice As Invoice) As String
        Dim sLine As String = ""
        Dim oFreetext As Freetext
        Dim sTxt As String = ""

        ' Add to Batch-totals:
        nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
        nFileNoRecords = nFileNoRecords + 1
        nFileNoTransactions = nFileNoTransactions + 1

        If EmptyString(oInvoice.Unique_Id) Then
            For Each oFreetext In oInvoice.Freetexts
                sTxt = sTxt & " " & Trim$(oFreetext.Text)
            Next
        End If
        sTxt = Trim(sTxt)

        sLine = "TR,"
        'CheckForValidCharacters(s, True, True, False, True, "/-?:().,'+", , "", False) '1
        ' Puts accoutno in field 2 as we have no unique ID
        sLine = sLine & Left(CheckForValidCharacters(oPayment.E_Account, True, True, False, True, "/-?:().,'+", , "", False), 16) & ","   '2 Originators reference
        sLine = sLine & oPayment.DATE_Payment & ","                 '3 Value date
        sLine = sLine & "NO,"                                       '4 NO=Norway
        sLine = sLine & ","                                         '5 reserved
        ' to account
        ' if bban no, then put first 4 in field 6, next 7 in field 7
        ' if iban, then blank in field 6, complete iban in field 7
        If oPayment.E_Account.Length = 11 Then
            sLine = sLine & oPayment.E_Account.Substring(0, 4) & ","    '6 Bankcode
            sLine = sLine & oPayment.E_Account.Substring(4, 7) & ","    '7 Bankaccount
        Else
            sLine = sLine & ","                                         '6 Bankcode
            sLine = sLine & oPayment.E_Account & ","                    '7 Bankaccount
        End If
        sLine = sLine & ","                                             '8 reserved
        sLine = sLine & Str(Math.Abs(oInvoice.MON_InvoiceAmount)).Trim & "," '9 Transaction amount
        sLine = sLine & "NOK,"                                          '10 Currency
        sLine = sLine & "GIR,"                                          '11 Payment method
        sLine = sLine & "01,"                                           '12 Transactiontype 01 Direct credit
        If oPayment.PayType = "S" Then
            sLine = sLine & "PRL,"                                      '13 Transaction category PRL = Salary
            ' legg Lønn som tekst dersom ingen tekst finnes i importfil
            If Len(sTxt) = 0 Then
                sTxt = "Salary"
            End If
        Else
            sLine = sLine & ","
        End If
        sLine = sLine & oPayment.I_Account & ","                        '14 Originators account (in JP Morgan London)
        sLine = sLine & ","                                             '15 reserved
        sLine = sLine & ","                                             '16 reserved
        sLine = sLine & Left(CheckForValidCharacters(oPayment.E_Name, True, True, False, True, "/-?:().,'+", , "", False), 70) & ","   '17 Beneficiary name
        sLine = sLine & ","                                             '18 reserved
        sLine = sLine & ","                                             '19 reserved
        sLine = sLine & ","                                             '20 reserved
        sLine = sLine & ","                                             '21 reserved
        sLine = sLine & ","                                             '22 reserved
        sLine = sLine & ","                                             '23 reserved
        sLine = sLine & ","                                             '24 reserved
        sLine = sLine & ","                                             '25 reserved
        sLine = sLine & ","                                             '26 reserved
        sLine = sLine & ","                                             '27 reserved

        If oPayment.PayCode <> "301" Then
            sLine = sLine & Left(CheckForValidCharacters(sTxt, True, True, False, True, "/-?:().,'+", , "", False), 70) & ","       '28 Senders text 1-70
            sLine = sLine & Mid(CheckForValidCharacters(sTxt, True, True, False, True, "/-?:().,'+", , "", False), 71, 70) & ","    '29 Senders text 71
            sLine = sLine & Mid(CheckForValidCharacters(sTxt, True, True, False, True, "/-?:().,'+", , "", False), 141, 70) & ","   '30 Senders text 141
            sLine = sLine & Mid(CheckForValidCharacters(sTxt, True, True, False, True, "/-?:().,'+", , "", False), 211, 70) & ","   '31 Senders text 211
        Else
            ' if KID, then blank text
            sLine = sLine & ","                                             '28 reserved
            sLine = sLine & ","                                             '29 reserved
            sLine = sLine & ","                                             '30 reserved
            sLine = sLine & ","                                             '31 reserved
        End If

        sLine = sLine & ","                                             '32 reserved
        sLine = sLine & ","                                             '33 reserved
        sLine = sLine & ","                                             '34 reserved
        sLine = sLine & ","                                             '35 reserved
        sLine = sLine & ","                                             '36 reserved
        sLine = sLine & ","                                             '37 reserved
        sLine = sLine & ","                                             '38 reserved
        sLine = sLine & ","                                             '39 reserved
        sLine = sLine & ","                                             '40 reserved
        sLine = sLine & ","                                             '41 reserved
        sLine = sLine & Mid(CheckForValidCharacters(sTxt, True, True, False, True, "/-?:().,'+", , "", False), 281, 70) & ","   '42 Senders text 281
        sLine = sLine & ","                                             '43 reserved
        sLine = sLine & ","                                             '44 reserved
        sLine = sLine & ","                                             '45 reserved
        sLine = sLine & ","                                             '46 reserved
        sLine = sLine & ","                                             '47 reserved
        sLine = sLine & ","                                             '48 reserved
        sLine = sLine & ","                                             '49 reserved
        sLine = sLine & ","                                             '50 reserved
        sLine = sLine & ","                                             '51 reserved
        sLine = sLine & ","                                             '52 reserved
        sLine = sLine & ","                                             '53 reserved
        sLine = sLine & Trim(oInvoice.Unique_Id)                        '54 KID


        WriteJPMorganTransactionRecord = sLine

    End Function
    Function WriteJPMorganFileTrailer() As String
        Dim sLine As String
        nFileNoRecords = nFileNoRecords + 1
        sLine = "FT,"        ' 1-2
        sLine = sLine & PadLeft(Str(nFileNoTransactions), 10, "0") & "," '3-12
        sLine = sLine & PadLeft(Str(nFileNoRecords), 10, "0") & "," '13-22
        sLine = sLine & PadLeft(Str(Math.Abs(nFileSumAmount)), 25, "0")  '23-47

        WriteJPMorganFileTrailer = sLine

    End Function


End Module
