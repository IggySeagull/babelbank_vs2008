<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDiminishFreetext
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents txtTextOutsideLimit As System.Windows.Forms.TextBox
	Public WithEvents txtTextWithinLimit As System.Windows.Forms.TextBox
    Public WithEvents lblExplaination As System.Windows.Forms.Label
	Public WithEvents lblPaymentInfo As System.Windows.Forms.Label
	Public WithEvents lblTextOutsideLimit As System.Windows.Forms.Label
	Public WithEvents lblTextWithinLimit As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.txtTextOutsideLimit = New System.Windows.Forms.TextBox
        Me.txtTextWithinLimit = New System.Windows.Forms.TextBox
        Me.lblExplaination = New System.Windows.Forms.Label
        Me.lblPaymentInfo = New System.Windows.Forms.Label
        Me.lblTextOutsideLimit = New System.Windows.Forms.Label
        Me.lblTextWithinLimit = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(512, 432)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(80, 25)
        Me.cmdOK.TabIndex = 7
        Me.cmdOK.TabStop = False
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(424, 432)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(80, 25)
        Me.cmdCancel.TabIndex = 6
        Me.cmdCancel.TabStop = False
        Me.cmdCancel.Text = "55001 - Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'txtTextOutsideLimit
        '
        Me.txtTextOutsideLimit.AcceptsReturn = True
        Me.txtTextOutsideLimit.BackColor = System.Drawing.SystemColors.GrayText
        Me.txtTextOutsideLimit.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtTextOutsideLimit.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtTextOutsideLimit.Location = New System.Drawing.Point(104, 232)
        Me.txtTextOutsideLimit.MaxLength = 0
        Me.txtTextOutsideLimit.Multiline = True
        Me.txtTextOutsideLimit.Name = "txtTextOutsideLimit"
        Me.txtTextOutsideLimit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtTextOutsideLimit.Size = New System.Drawing.Size(425, 97)
        Me.txtTextOutsideLimit.TabIndex = 3
        Me.txtTextOutsideLimit.Text = "Text1"
        '
        'txtTextWithinLimit
        '
        Me.txtTextWithinLimit.AcceptsReturn = True
        Me.txtTextWithinLimit.BackColor = System.Drawing.SystemColors.Window
        Me.txtTextWithinLimit.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtTextWithinLimit.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtTextWithinLimit.Location = New System.Drawing.Point(104, 112)
        Me.txtTextWithinLimit.MaxLength = 0
        Me.txtTextWithinLimit.Multiline = True
        Me.txtTextWithinLimit.Name = "txtTextWithinLimit"
        Me.txtTextWithinLimit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtTextWithinLimit.Size = New System.Drawing.Size(425, 89)
        Me.txtTextWithinLimit.TabIndex = 1
        Me.txtTextWithinLimit.Text = "Text1"
        '
        'lblExplaination
        '
        Me.lblExplaination.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplaination.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplaination.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplaination.Location = New System.Drawing.Point(104, 56)
        Me.lblExplaination.Name = "lblExplaination"
        Me.lblExplaination.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplaination.Size = New System.Drawing.Size(425, 33)
        Me.lblExplaination.TabIndex = 5
        Me.lblExplaination.Text = "Explaination"
        '
        'lblPaymentInfo
        '
        Me.lblPaymentInfo.BackColor = System.Drawing.SystemColors.Control
        Me.lblPaymentInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPaymentInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPaymentInfo.Location = New System.Drawing.Point(104, 344)
        Me.lblPaymentInfo.Name = "lblPaymentInfo"
        Me.lblPaymentInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPaymentInfo.Size = New System.Drawing.Size(425, 73)
        Me.lblPaymentInfo.TabIndex = 4
        Me.lblPaymentInfo.Text = "64048 - Payment information:"
        '
        'lblTextOutsideLimit
        '
        Me.lblTextOutsideLimit.BackColor = System.Drawing.SystemColors.Control
        Me.lblTextOutsideLimit.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTextOutsideLimit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTextOutsideLimit.Location = New System.Drawing.Point(104, 216)
        Me.lblTextOutsideLimit.Name = "lblTextOutsideLimit"
        Me.lblTextOutsideLimit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTextOutsideLimit.Size = New System.Drawing.Size(193, 17)
        Me.lblTextOutsideLimit.TabIndex = 2
        Me.lblTextOutsideLimit.Text = "64047 - Message that will be removed:"
        '
        'lblTextWithinLimit
        '
        Me.lblTextWithinLimit.BackColor = System.Drawing.SystemColors.Control
        Me.lblTextWithinLimit.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTextWithinLimit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTextWithinLimit.Location = New System.Drawing.Point(104, 96)
        Me.lblTextWithinLimit.Name = "lblTextWithinLimit"
        Me.lblTextWithinLimit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTextWithinLimit.Size = New System.Drawing.Size(193, 17)
        Me.lblTextWithinLimit.TabIndex = 0
        Me.lblTextWithinLimit.Text = "64046 - Message to beneficiary:"
        '
        'frmDiminishFreetext
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(604, 466)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.txtTextOutsideLimit)
        Me.Controls.Add(Me.txtTextWithinLimit)
        Me.Controls.Add(Me.lblExplaination)
        Me.Controls.Add(Me.lblPaymentInfo)
        Me.Controls.Add(Me.lblTextOutsideLimit)
        Me.Controls.Add(Me.lblTextWithinLimit)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmDiminishFreetext"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "64045 - Change message to beneficiary"
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class
