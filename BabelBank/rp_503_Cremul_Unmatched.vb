Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

'Things to do when implementing this report in BB
'Search for 'LRS(, and replace the text with the LRS
'Search for vbBabel.MatchType. and replace the number stated with the correct Enum

Public Class rp_503_Cremul_Unmatched

    'NBNBNBNBNBNB
    'If this report is to be used, we have to alter the calculation of the totals like we do on rp_504
    'Don't use the suumarization in AR, but calculate the totals in the code
    'NBNBNBNBNBNB
    Dim iCompany_ID As Integer
    Dim iBabelfile_ID As Integer
    Dim iBatch_ID As Integer
    Dim iPayment_ID As Integer
    Dim iInvoice_ID As Integer
    Dim sOldI_Account As String
    Dim sI_Account As String
    Dim sClientInfo As String
    Dim sClientName As String
    Dim oBabelLog As vbLog.vbLogging
    Dim bLog As Boolean = False

    Dim nCalculatedMacthedAmount As Double
    Dim nCalculatedUnMacthedAmount As Double
    'Dim nACalculatedMacthedAmount As Double
    'Dim nACalculatedUnMacthedAmount As Double
    Dim nInvoiceCounter As Double
    Dim bValid As Boolean

    Dim sBreakField As String
    Dim nTotalAmount As Double
    Dim bBreak As Boolean
    Dim sOrderBy As String
    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean

    Dim oBabelFiles As vbBabel.BabelFiles
    Dim oBabelFile As vbBabel.BabelFile
    Dim oBatch As vbBabel.Batch
    Dim oPayment As vbBabel.Payment
    Dim oInvoice As vbBabel.Invoice
    Dim oFreeText As vbBabel.Freetext
    Dim bFirstTime As Boolean
    Dim bFirstVoucherFreetext As Boolean = True

    Dim sReportName As String
    Dim sSpecial As String
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim sClientNumber As String
    Dim bReportOnSelectedItems As Boolean
    Dim bEmptyReport As Boolean
    Dim bSQLServer As Boolean = False
    Dim bIncludeOCR As Boolean
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean
    Dim nLogCounter As Double = 0

    Private sBBRET_CustomerNoLabel As String
    Private sBBRET_InvoiceNoLabel As String
    Private sBBRET_FilterOnLabel As String
    Private sBBRET_FilterOffLabel As String
    Private sBBRET_ClientNoLabel As String
    Private sBBRET_ExchangeRateLabel As String
    Private sBBRET_VendroNoLabel As String
    Private sBBRET_CurrencyLabel As String
    Private sBBRET_GeneralLedgerLabel As String
    Private sBBRET_VoucherNoLabel As String
    Private sBBRET_NameLabel As String
    Private sBBRET_FreetextLabel As String
    Private sBBRET_CurrencyAmountLabel As String
    Private sBBRET_Currency2Label As String
    Private sBBRET_DiscountLabel As String
    Private sBBRET_BackPaymentLabel As String
    Private sBBRET_MyFieldLabel As String
    Private sBBRET_MyField2Label As String
    Private sBBRET_MyField3Label As String
    Private sBBRET_Dim1Label As String
    Private sBBRET_Dim2Label As String
    Private sBBRET_Dim3Label As String
    Private sBBRET_Dim4Label As String
    Private sBBRET_Dim5Label As String
    Private sBBRET_Dim6Label As String
    Private sBBRET_Dim7Label As String
    Private sBBRET_Dim8Label As String
    Private sBBRET_Dim9Label As String
    Private sBBRET_Dim10Label As String
    Private Sub rp_503_Cremul_Unmatched_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        'Set the different breaklevels

        Fields.Add("BreakField")
        If Not bReportFromDatabase Then
            Fields.Add("Grouping")
            Fields.Add("Company_ID")
            Fields.Add("Babelfile_ID")
            Fields.Add("Batch_ID")
            Fields.Add("Payment_ID")
            Fields.Add("Invoice_ID")
            Fields.Add("Filename")
            Fields.Add("StatementAmount")
            Fields.Add("Client")
            Fields.Add("DATE_Production")
            Fields.Add("BatchRef")
            Fields.Add("I_Account")
            'Fields.Add("Paycode")
            Fields.Add("MON_InvoiceAmount")
            Fields.Add("MON_InvoiceCurrency")
            Fields.Add("MON_TransferredAmount")
            Fields.Add("MON_TransferCurrency")
            Fields.Add("MON_OriginallyPaidAmount")
            Fields.Add("MON_OriginallyPaidCurrency")
            Fields.Add("MON_AccountAmount")
            Fields.Add("MON_AccountCurrency")
            Fields.Add("MON_LocalExchRate")
            Fields.Add("ChargesAbroad")
            Fields.Add("ChargesDomestic")
            Fields.Add("StatusCode")
            Fields.Add("StatusText")
            Fields.Add("E_Name")
            Fields.Add("E_Adr1")
            Fields.Add("E_Adr2")
            Fields.Add("E_Adr3")
            Fields.Add("E_Zip")
            Fields.Add("E_City")
            Fields.Add("E_Account")
            Fields.Add("E_Name")
            Fields.Add("REF_Bank1")
            'Fields.Add("Unique_ID")
            Fields.Add("I_Name")
            Fields.Add("I_Adr1")
            Fields.Add("I_Adr2")
            Fields.Add("I_Adr3")
            Fields.Add("I_Zip")
            Fields.Add("I_City")
            Fields.Add("REF_Bank1")
            Fields.Add("REF_Bank2")
            Fields.Add("REF_Own")
            Fields.Add("DATE_Value")
            Fields.Add("DATE_Payment")
            Fields.Add("PayCode")
            Fields.Add("PayType")
            Fields.Add("BANK_SWIFTCode")
            Fields.Add("BANK_BranchNo")
            Fields.Add("BANK_BranchType")
            Fields.Add("VoucherNo")
            Fields.Add("BabelBank_ID")
            Fields.Add("ExtraD")
            Fields.Add("Extra1")
            Fields.Add("MATCH_UseOriginalAmountInMatching")
            Fields.Add("ImportFormat")
            Fields.Add("MyField")
            Fields.Add("Unique_ID")
            Fields.Add("InvoiceStatusCode")
            Fields.Add("InvoiceNumber")
            Fields.Add("CustomerNumber")
            Fields.Add("InvoiceAmount")
            Fields.Add("Bank")
            Fields.Add("MATCH_Type")
            Fields.Add("Match_ID")
            Fields.Add("Matched")
            Fields.Add("Final")
            Fields.Add("InvoiceLabel")
            Fields.Add("CustomerLabel")
            Fields.Add("HowMatched")
            Fields.Add("PMATCH_Matched")
        End If

        If bLog Then
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("Report_503:  Run DataInitialize", 8) 'vbLogEventTypeInformationDetail
        End If

    End Sub

    Private Sub rp_503_Cremul_Unmatched_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        If Not bReportFromDatabase Then
            Me.Cancel()
        End If
        If bLog Then
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("Report_503:  No data to report", 8) 'vbLogEventTypeInformationDetail
        End If
    End Sub

    Private Sub rp_503_Cremul_Unmatched_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportStart

        If bLog Then 'Added 09.02.2018
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("Report_503:  I starten av ReportStart", 8) 'vbLogEventTypeInformationDetail
        End If

        bShowBatchfooterTotals = True
        bFirstTime = True

        ' Find user-specific labels
        RetrieveLabels(Nothing, "1", sBBRET_CustomerNoLabel, sBBRET_InvoiceNoLabel, sBBRET_ClientNoLabel, _
                              sBBRET_VendroNoLabel, sBBRET_CurrencyLabel, sBBRET_ExchangeRateLabel, sBBRET_GeneralLedgerLabel, sBBRET_VoucherNoLabel, sBBRET_NameLabel, _
                              sBBRET_FreetextLabel, sBBRET_CurrencyAmountLabel, sBBRET_Currency2Label, sBBRET_DiscountLabel, sBBRET_BackPaymentLabel, sBBRET_MyFieldLabel, _
                              sBBRET_MyField2Label, sBBRET_MyField3Label, sBBRET_Dim1Label, sBBRET_Dim2Label, sBBRET_Dim3Label, sBBRET_Dim4Label, _
                              sBBRET_Dim5Label, sBBRET_Dim6Label, sBBRET_Dim7Label, sBBRET_Dim8Label, sBBRET_Dim9Label, sBBRET_Dim10Label)

        Select Case iBreakLevel
            Case 0 'NOBREAK
                bShowBatchfooterTotals = False
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False

            Case 1 'BREAK_ON_ACCOUNT (and per day)
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case 2 'BREAK_ON_BATCH
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case 3 'BREAK_ON_PAYMENT
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case 4 'BREAK_ON_ACCOUNTONLY
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case 5 'BREAK_ON_CLIENT 'New
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = False

            Case 999
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True

            Case Else 'NOBREAK
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False

        End Select

        If bLog Then 'Added 09.02.2018
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("Report_503:  Etter setting av Pagebreak", 8) 'vbLogEventTypeInformationDetail
        End If

        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtDATE_Production.OutputFormat = "D"
            Me.txtDATE_Value.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtDATE_Production.OutputFormat = "d"
            Me.txtDATE_Value.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = LRS(40112) '  Unposted payments
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'Me.rptInfoPageCounterGB.Left = 4
        'End If

        'Set the caption of the labels

        'grBatchHeader
        'Me.lblFilename.Text = LRS(40001) '"Filnavn:" 
        Me.lblDate_Production.Text = LRS(40027) '"Prod.dato:" 
        Me.lblClient.Text = LRS(40030) '"Klient:" 
        Me.lblI_Account.Text = LRS(40020) '"Mottakerkonto:" 

        'grPaymentReceiverHeader
        Me.lblReceiver.Text = LRS(48011) '"Mottaker:" 
        Me.lblPayor.Text = LRS(40108) '"Betaler:" 

        'grPaymentHeader
        Me.lblE_Account.Text = LRS(40013) '"Kontonr.:" 
        Me.lblREF_Bank2.Text = LRS(40011) '"Arkivref.:" 
        Me.lblREF_Bank1.Text = LRS(40012) '"Blankettref.:" 
        Me.lblBatchRef.Text = LRS(40045) '"Kontoref:" 
        Me.lblDATE_Value.Text = LRS(40003) '"Val.dato:" 
        Me.lblVoucherNo.Text = LRS(40068) '"Bilagsnummer:" 
        ' 14.09.2015 Userspecific labels
        If Not EmptyString(sBBRET_VoucherNoLabel) Then
            Me.lblVoucherNo.Text = sBBRET_VoucherNoLabel
        End If
        Me.lblAmountStatement.Text = LRS(40004) '"Bel�p konto:" 

        'grPaymentInternationalHeader
        Me.lblMON_OriginallyPaidAmount.Text = LRS(40037) '"Opprinnelig bel�p:" 
        Me.lblMON_InvoiceAmount.Text = LRS(40036) '"Postert bel�p:" 
        Me.lblMON_AccountAmount.Text = LRS(40038) '"Kontobel�p:" 
        Me.lblExchangeRate.Text = LRS(40035) '"Kurs:" 
        Me.lblChargesAbroad.Text = LRS(40043) '"Avsenderbankens omkostninger:" 
        Me.lblChargesDomestic.Text = LRS(40042) '"Mottakerbankens omkostninger:" 

        'Detail
        Me.lblConcerns.Text = LRS(40010) '"Bel�pet gjelder:" "Bel�pet gjelder:" 

        'grPaymentFooter
        Me.lblTotalPayment.Text = LRS(60089) '"Innbetalt" 
        Me.lblMatchedAmount.Text = LRS(60088) ' Posted
        Me.lblUnmatchedAmount.Text = LRS(40029) 'Unposted

        'grBatchFooter
        Me.lblBatchfooterAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblBatchfooterNoofPayments.Text = LRS(40105) '"Deltotal - Antall:" 

        'grBabelFileFooter
        Me.lblTotalAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblTotalNoOfPayments.Text = LRS(40106) '"Total - Antall:" 

        ' Special cases;
        If sSpecial = "LINDORFF" Then
            Me.lblExtraD.Text = "Kildekode"
            Me.lblExtra1.Text = "Innbet.kode"
            Me.lblMyField.Text = "Rolle"
        Else
            Me.lblExtraD.Visible = False
            Me.txtExtraD.Visible = False
            Me.lblExtra1.Visible = False
            Me.txtExtra1.Visible = False
            Me.lblMyField.Visible = False
            Me.txtMyField.Visible = False
        End If
        '*******VB6 Code

        ''Me.lblFileSum = LRS(40028)

        sOldI_Account = ""
        sClientInfo = LRS(40107) '"Ingen klientinfo angitt" 
        sBreakField = ""
        bBreak = True

        If Not bShowBatchfooterTotals Then
            Me.grBatchFooter.Visible = False
        End If
        If Not bShowDATE_Production Then
            Me.txtDATE_Production.Visible = False
        End If
        If Not bShowClientName Then
            Me.txtClientName.Visible = False
        End If
        If Not bShowI_Account Then
            Me.txtI_Account.Visible = False
        End If

        If bLog Then
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("Report_503:  ReportStart er ferdig kj�rt", 8) 'vbLogEventTypeInformationDetail
        End If

    End Sub
    Private Sub grPaymentHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grPaymentHeader.Format
        Dim pLocation As System.Drawing.PointF

        nInvoiceCounter = 0
        'Added 29.09.2014
        Me.txtMatchedAmount.Value = 0
        Me.txtUnmatchedAmount.Value = 0
        Me.txtTotalPayment.Value = 0


        If EmptyString(Me.txtE_City.Text) Then
            pLocation.X = 0
            pLocation.Y = 0.45
            Me.txtE_Adr3.Location = pLocation
            Me.txtE_Adr3.Visible = True
            Me.txtE_Zip.Visible = False
            Me.txtE_City.Visible = False
        Else
            Me.txtE_Adr3.Visible = False
            Me.txtE_Zip.Visible = True
            Me.txtE_City.Visible = True
        End If

        If bLog Then
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("Report_503:  grPaymentHeader ferdig kj�rt.", 8) 'vbLogEventTypeInformationDetail
        End If

    End Sub
    Private Sub rp_503_Cremul_Unmatched_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        'The Fields collection should never be accessed outside the DataInitialize and FetchData events
        Dim sDATE_Production As String
        Dim sDATE_Value As String
        Dim bContinue As Boolean
        Dim bExportPayment As Boolean
        'Dim pLocation As System.Drawing.PointF

        ' Counters for items in the different Babel collections
        Static lBabelFiles As Long
        Static lLastUsedBabelFilesItem As Long
        Static lBatches As Long
        Static lPayments As Long
        Static lInvoices As Long
        Static lFreetexts As Long
        ' Counters for last item in use in the different Babel collections
        ' This way we do not need to set the different objects each time,
        ' just when an item-value has changed
        Static xCreditAccount As String
        Static xDate As String

        nLogCounter = nLogCounter + 1

        If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 10 = Math.Round(nLogCounter / 10, 0)) Then
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("Report_503:  I starten av FetchData. Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
        End If

        Me.lblConcerns.Visible = False 'Never in use

        If Not bReportFromDatabase Then

            If bFirstTime Then
                bFirstTime = False
                lBabelFiles = 1
                lBatches = 1
                lPayments = 1
                bFirstVoucherFreetext = True
                lInvoices = 1
                lFreetexts = 0
                xCreditAccount = "xxxx"
                xDate = "x"
            End If

            lFreetexts = lFreetexts + 1

            ' Spin through collections to find next suitbable record before any other
            ' proecessing is done
            ' Position to correct items in collections
            Do Until lBabelFiles > oBabelFiles.Count
                ' Try to set as few times as possible
                If oBabelFile Is Nothing Then
                    oBabelFile = oBabelFiles(lBabelFiles)
                    lLastUsedBabelFilesItem = lBabelFiles
                Else
                    'If lBabelFiles <> oBabelFile.Index Then
                    If lLastUsedBabelFilesItem <> lBabelFiles Then
                        lBatches = 1
                        lPayments = 1
                        bFirstVoucherFreetext = True
                        lInvoices = 1
                        ' Added 19.05.2004
                        xCreditAccount = "xxxx"
                        xDate = "x"

                        oBabelFile = oBabelFiles(lBabelFiles)
                        lLastUsedBabelFilesItem = lBabelFiles  'oBabelFile.Index
                    End If
                End If

                Do Until lBatches > oBabelFile.Batches.Count
                    oBatch = oBabelFile.Batches(lBatches)

                    ' Find the next payment in this batch which is original
                    Do Until lPayments > oBatch.Payments.Count

                        oPayment = oBatch.Payments.Item(lPayments)

                        bExportPayment = False

                        'Sometimes there are restrictions on which payments to export
                        If oPayment.MATCH_Matched < vbBabel.BabelFiles.MatchStatus.Matched Then
                            If bIncludeOCR Then
                                bExportPayment = True
                            Else
                                If IsOCR(oPayment.PayCode) Then
                                    bExportPayment = False
                                ElseIf IsAutogiro(oPayment.PayCode) Then
                                    bExportPayment = False
                                Else
                                    bExportPayment = True
                                End If
                            End If
                        Else
                            bExportPayment = False
                        End If

                        If bReportOnSelectedItems And bExportPayment Then
                            If oPayment.ToSpecialReport = False Then
                                bExportPayment = False
                            End If
                        End If

                        '26.06.2018 - wHY THIS if? iT DOESN'T DO ANYTHING!!!!
                        If bExportPayment Then
                            If Not EmptyString(sClientNumber) Then
                                If oPayment.VB_ClientNo = sClientNumber Then
                                    bExportPayment = True
                                End If
                            Else
                                bExportPayment = True
                            End If
                        End If

                        ' Added 12.11.07
                        ' Check if it is a clientseparated report, and if so, correct client
                        '21.04.2015 - Added next IF
                        If Not EmptyString(sClientNumber) Then
                            If Not oPayment.VB_ClientNo.Trim = sClientNumber Then
                                bExportPayment = False
                            End If
                        End If

                        If bExportPayment Then

                            ' Postiton to a, final invoice
                            ' Find the next invoice in this payment which is original;
                            Do Until lInvoices > oPayment.Invoices.Count
                                bContinue = False

                                oInvoice = oPayment.Invoices.Item(lInvoices)

                                bContinue = True

                                '29.05.2015 - Added for Bama (don't show matched payments on the unmatched report.
                                'If oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.PartlyMatched And oInvoice.MATCH_Matched = True Then
                                '    bContinue = False
                                'End If

                                'If (Not oInvoice.MATCH_Matched And oInvoice.MATCH_Final) Or oInvoice.MATCH_Original Then
                                'If (Not oInvoice.MATCH_Matched And oInvoice.MATCH_Final) Or oInvoice.MATCH_Original Then
                                ' bContinue = True
                                'Else
                                'bContinue = False
                                'End If

                                'If oInvoice.MATCH_Original Then
                                '    If lFreetexts <= oInvoice.Freetexts.Count Then
                                '        bContinue = True
                                '        oFreeText = oInvoice.Freetexts.Item(lFreetexts)
                                '        'lFreetexts = lFreetexts + 1
                                '        Exit Do
                                '    End If
                                '    If oInvoice.Freetexts.Count = 0 And lFreetexts = 1 Then
                                '        bContinue = True
                                '        Exit Do
                                '    End If

                                'End If

                                If bContinue Then
                                    lInvoices = lInvoices + 1
                                    Exit Do
                                Else
                                    lInvoices = lInvoices + 1
                                End If
                                'lFreetexts = 1
                            Loop 'lInvoices
                        Else
                            bContinue = False
                            'lPayments = lPayments + 1
                            'lInvoices = 1
                        End If 'If frmViewer.CorrectReportClient(oPayment) Then

                        If bContinue Then
                            Exit Do
                        End If

                        lPayments = lPayments + 1
                        lInvoices = 1
                        bFirstVoucherFreetext = True

                    Loop 'lPayments

                    If bContinue Then
                        Exit Do
                    End If
                    lBatches = lBatches + 1
                    lPayments = 1
                Loop ' lBatches
                If bContinue Then
                    Exit Do
                End If
                lBabelFiles = lBabelFiles + 1
                lBatches = 1
            Loop ' lBabelFiles

            If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                oBabelLog.Heading = "BabelBank info: "
                oBabelLog.AddLogEvent("Report_503:  Ferdig med � loope gjennom kolleksjonene. Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
            End If

            If lBabelFiles > oBabelFiles.Count Then
                eArgs.EOF = True
                lBabelFiles = 0 ' to reset statics next time !

                Exit Sub
            End If

            If oBabelFile.VB_ProfileInUse Then
                Me.Fields("Grouping").Value = Trim(Str(oPayment.VB_Profile.Company_ID)) & "-" & Trim(Str(oBabelFile.Index)) & "-" & Trim(Str(oBatch.Index)) & "-" & Trim(Str(oPayment.Index))
                Me.Fields("Company_ID").Value = oPayment.VB_Profile.Company_ID
            Else
                Me.Fields("Grouping").Value = "1-" & Trim(Str(oBabelFile.Index)) & "-" & Trim(Str(oBatch.Index)) & "-" & Trim(Str(oPayment.Index))
                Me.Fields("Company_ID").Value = "1"
            End If
            Me.Fields("Babelfile_ID").Value = oBabelFile.Index
            Me.Fields("Batch_ID").Value = oBatch.Index
            Me.Fields("Payment_ID").Value = oPayment.Index
            Me.Fields("Invoice_ID").Value = oInvoice.Index
            Me.Fields("Filename").Value = Right(oBabelFile.FilenameIn, 20)
            Me.Fields("StatementAmount").Value = oBatch.MON_InvoiceAmount / 100
            Me.Fields("Client").Value = oPayment.VB_Client
            Me.Fields("DATE_Production").Value = oBabelFile.DATE_Production
            Me.Fields("BatchRef").Value = oBatch.REF_Bank
            Me.Fields("I_Account").Value = oPayment.I_Account
            'Me.Fields("Paycode").Value = oPayment.PayCode
            Me.Fields("MON_InvoiceAmount").Value = oPayment.MON_InvoiceAmount / 100
            Me.Fields("MON_InvoiceCurrency").Value = oPayment.MON_InvoiceCurrency
            If oPayment.MON_TransferredAmount = 0 Then
                Me.Fields("MON_TransferredAmount").Value = oPayment.MON_InvoiceAmount / 100
                Me.Fields("MON_TransferCurrency").Value = oPayment.MON_InvoiceCurrency
            Else
                Me.Fields("MON_TransferredAmount").Value = oPayment.MON_TransferredAmount / 100
                Me.Fields("MON_TransferCurrency").Value = oPayment.MON_TransferCurrency
            End If
            'me.txtBatchfooterAmount
            Me.Fields("MON_OriginallyPaidAmount").Value = oPayment.MON_OriginallyPaidAmount / 100
            Me.Fields("MON_OriginallyPaidCurrency").Value = oPayment.MON_OriginallyPaidCurrency
            Me.Fields("MON_AccountAmount").Value = oPayment.MON_AccountAmount / 100
            Me.Fields("MON_AccountCurrency").Value = oPayment.MON_AccountCurrency
            Me.Fields("MON_LocalExchRate").Value = oPayment.MON_LocalExchRate
            If oPayment.MON_AccountAmount = 0 Then
                Me.Fields("ChargesAbroad").Value = 0
            Else
                'Added 28.05.2015 - because outging payments like Telepay has Origanlamount set to 0
                If oPayment.MON_OriginallyPaidAmount > 0 Then
                    Me.Fields("ChargesAbroad").Value = (oPayment.MON_OriginallyPaidAmount - oPayment.MON_AccountAmount) / 100
                Else
                    Me.Fields("ChargesAbroad").Value = 0
                End If
                'Me.Fields("ChargesAbroad").Value = (oPayment.MON_OriginallyPaidAmount - oPayment.MON_AccountAmount) / 100
            End If

            If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                oBabelLog.Heading = "BabelBank info: "
                oBabelLog.AddLogEvent("Report_503:  Ferdig med � sette inn bel�p. Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
            End If

            Me.Fields("ChargesDomestic").Value = oPayment.MON_ChargesAmount / 100
            Me.Fields("StatusCode").Value = oPayment.StatusCode
            Me.Fields("StatusText").Value = oPayment.StatusText
            Me.Fields("E_Name").Value = oPayment.E_Name
            Me.Fields("E_Adr1").Value = oPayment.E_Adr1
            Me.Fields("E_Adr2").Value = oPayment.E_Adr2
            Me.Fields("E_Adr3").Value = oPayment.E_Adr3
            Me.Fields("E_Zip").Value = oPayment.E_Zip
            Me.Fields("E_City").Value = oPayment.E_City
            Me.Fields("E_Account").Value = oPayment.E_Account
            Me.Fields("E_Name").Value = oPayment.E_Name
            Me.Fields("REF_Bank1").Value = oPayment.REF_Bank1
            'Me.Fields("Unique_ID").Value = oInvoice.Unique_Id
            Me.Fields("I_Name").Value = oPayment.I_Name
            Me.Fields("I_Adr1").Value = oPayment.I_Adr1
            Me.Fields("I_Adr2").Value = oPayment.I_Adr2
            Me.Fields("I_Adr3").Value = oPayment.I_Adr3
            Me.Fields("I_Zip").Value = oPayment.I_Zip
            Me.Fields("I_City").Value = oPayment.I_City
            Me.Fields("REF_Bank1").Value = oPayment.REF_Bank1
            Me.Fields("REF_Bank2").Value = oPayment.REF_Bank2
            Me.Fields("REF_Own").Value = oPayment.REF_Own
            Me.Fields("DATE_Value").Value = oPayment.DATE_Value
            Me.Fields("DATE_Payment").Value = oPayment.DATE_Payment
            Me.Fields("PayCode").Value = oPayment.PayCode
            Me.Fields("PayType").Value = oPayment.PayType
            Me.Fields("PMATCH_Matched").Value = oPayment.MATCH_Matched

            Me.Fields("BANK_SWIFTCode").Value = oPayment.BANK_SWIFTCode
            Me.Fields("BANK_BranchNo").Value = oPayment.BANK_BranchNo
            Me.Fields("BANK_BranchType").Value = oPayment.BANK_BranchType
            Me.Fields("VoucherNo").Value = oPayment.VoucherNo
            Me.Fields("BabelBank_ID").Value = oPayment.Unique_PaymentID
            Me.Fields("ExtraD").Value = oPayment.ExtraD1
            Me.Fields("Extra1").Value = oInvoice.Extra1
            Me.Fields("MATCH_UseOriginalAmountInMatching").Value = oPayment.MATCH_UseoriginalAmountInMatching
            Me.Fields("ImportFormat").Value = oPayment.ImportFormat
            Me.Fields("MyField").Value = oInvoice.MyField
            Me.Fields("Unique_ID").Value = oInvoice.Unique_Id
            If oInvoice.MATCH_Final Then
                Me.Fields("InvoiceStatusCode").Value = oInvoice.StatusCode
                Me.Fields("InvoiceNumber").Value = oInvoice.InvoiceNo
                Me.Fields("CustomerNumber").Value = oInvoice.CustomerNo
                Me.Fields("InvoiceAmount").Value = oInvoice.MON_InvoiceAmount / 100
            Else
                Me.Fields("InvoiceStatusCode").Value = ""
                Me.Fields("InvoiceNumber").Value = ""
                Me.Fields("CustomerNumber").Value = ""
                Me.Fields("InvoiceAmount").Value = ""
            End If

            Me.Fields("Bank").Value = oBabelFile.BankByCode

            If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                oBabelLog.Heading = "BabelBank info: "
                oBabelLog.AddLogEvent("Report_503:  F�r test av MATCH_Final. Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
            End If

            '03.12.2012 - New fields
            If oInvoice.MATCH_Final Then
                Me.Fields("InvoiceLabel").Value = LRS(40051) '"Fakturanr."
                If Not EmptyString(sBBRET_InvoiceNoLabel) Then
                    Me.Fields("InvoiceLabel").Value = sBBRET_InvoiceNoLabel
                End If
                'Me.Fields("CustomerLabel").Value = "Kundenr."
                Me.Fields("CustomerLabel").Value = LRS(40073) '"Kundenr."
                If Not EmptyString(sBBRET_CustomerNoLabel) Then
                    Me.Fields("CustomerLabel").Value = sBBRET_CustomerNoLabel
                End If
            Else
                Me.Fields("InvoiceLabel").Value = ""
                Me.Fields("CustomerLabel").Value = ""
            End If
            Me.Fields("Matched").Value = oInvoice.MATCH_Matched
            Me.Fields("Final").Value = oInvoice.MATCH_Final
            Me.Fields("HowMatched").Value = oPayment.MATCH_HowMatched

            eArgs.EOF = False

        End If

        If Not eArgs.EOF Then

            Select Case iBreakLevel
                Case 0 'NOBREAK
                    Me.Fields("BreakField").Value = ""
                    'Me.lblFilename.Visible = False
                    'Me.txtFilename.Visible = False

                Case 1 'BREAK_ON_ACCOUNT (and per day)
                    'Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Production").Value
                    ' 03.02.2016 changed to DATE_Payment
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Payment").Value

                Case 2 'BREAK_ON_BATCH
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value))

                Case 3 'BREAK_ON_PAYMENT
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value)) & "-" & Trim$(Str(Me.Fields("Payment_ID").Value))

                Case 4 'BREAK_ON_ACCOUNTONLY
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value

                Case 5 'BREAK_ON_CLIENT 'New
                    Me.Fields("BreakField").Value = Me.Fields("Client").Value

                Case 999 'Added 06.01.2015 - Use the breaklevel from the special SQL
                    'The break is set in the SQL

                Case Else
                    Me.Fields("BreakField").Value = ""

            End Select

            ' ''If bBreak Then
            ' ''    nACalculatedMacthedAmount = nCalculatedMacthedAmount
            ' ''    nACalculatedUnMacthedAmount = nCalculatedUnMacthedAmount
            ' ''    nCalculatedMacthedAmount = 0
            ' ''    nCalculatedUnMacthedAmount = 0
            ' ''    bBreak = False
            ' ''End If
            ' ''If sBreakField <> Me.Fields("Grouping").Value Then
            ' ''    sBreakField = Me.Fields("Grouping").Value
            ' ''    bBreak = True
            ' ''End If

            'Store the keys for usage in the subreport(s)
            iCompany_ID = Me.Fields("Company_ID").Value
            iBabelfile_ID = Me.Fields("Babelfile_ID").Value
            iBatch_ID = Me.Fields("Batch_ID").Value
            iPayment_ID = Me.Fields("Payment_ID").Value
            iInvoice_ID = Me.Fields("Invoice_ID").Value

            If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                oBabelLog.Heading = "BabelBank info: "
                oBabelLog.AddLogEvent("Report_503:  Stored keys for usage in the subreports. Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
            End If

            'START WORKING HERE
            If Not IsDBNull(Me.Fields("I_Account").Value) Then
                sI_Account = Me.Fields("I_Account").Value
            Else
                sI_Account = ""
            End If
            If sI_Account <> sOldI_Account And Not EmptyString(sI_Account) Then
                sClientInfo = FindClientName(sI_Account)
                sOldI_Account = sI_Account
            End If

            Me.txtClientName.Value = sClientInfo

            If Me.Fields("Paytype").Value = "I" Then
                Me.grPaymentInternationalHeader.Visible = True
            Else
                Me.grPaymentInternationalHeader.Visible = False
            End If

            sDATE_Production = Me.Fields("DATE_Production").Value
            Me.txtDATE_Production.Value = DateSerial(CInt(Left$(sDATE_Production, 4)), CInt(Mid$(sDATE_Production, 5, 2)), CInt(Right$(sDATE_Production, 2)))

            sDATE_Value = Me.Fields("DATE_Value").Value
            Me.txtDATE_Value.Value = DateSerial(CInt(Left$(sDATE_Value, 4)), CInt(Mid$(sDATE_Value, 5, 2)), CInt(Right$(sDATE_Value, 2)))

            If Me.Fields("Paycode").Value = "602" Then
                Me.txtE_Name.Value = "GIROMAIL"
            Else
                Me.txtE_Name.Value = Me.Fields("E_Name").Value
            End If

            If sSpecial = "SG FINANS" Then '28.04.2020 - Added this IF
                If IsOCR(Me.Fields("Paycode").Value) Then
                    Me.Fields("BabelBank_ID").Value = "KID" & Me.Fields("BabelBank_ID").Value
                Else
                    Me.Fields("BabelBank_ID").Value = "OSL" & Me.Fields("BabelBank_ID").Value
                End If
            End If

            'If Me.Fields("PMATCH_Matched").Value > vbBabel.BabelFiles.MatchStatus.NotMatched Then
            '    If Me.Fields("MATCH_Type").Value = 0 Then 'vbBabel.MatchType.MatchedOnInvoice Then
            '        Me.txtInvoice.Visible = True
            '        Me.txtCustomer.Visible = True
            '        If Me.Fields("InvoiceLabel").Value = "BB_InvoiceLabel" Then
            '            Me.txtInvoice.Value = LRS(40051) & " " & Me.Fields("InvoiceNumber").Value
            '            'Me.txtInvoice.Value = "Fakturanr." & " " & Me.Fields("InvoiceNumber").Value
            '        Else
            '            Me.txtInvoice.Value = Me.Fields("InvoiceLabel").Value & " " & Me.Fields("InvoiceNumber").Value
            '        End If
            '        If sSpecial = "LINDORFF" Then
            '            Me.txtCustomer.Value = Me.Fields("CustomerNumber").Value
            '            ' added next elseif for Sismo 24.04.2007
            '        ElseIf sSpecial = "SISMO" Then
            '            Me.txtInvoice.Value = "KID/Krav: " & Me.Fields("Match_ID").Value
            '            Me.txtCustomer.Value = "F�dselsnr.: " & Me.Fields("CustomerNumber").Value
            '        Else
            '            If Me.Fields("CustomerLabel").Value = "BB_CustomerLabel" Then
            '                Me.txtCustomer.Value = LRS(40073) & " " & Me.Fields("CustomerNumber").Value
            '                'Me.txtCustomer.Value = "Kundenr." & " " & Me.Fields("CustomerNumber").Value
            '            Else
            '                Me.txtCustomer.Value = Me.Fields("CustomerLabel").Value & " " & Me.Fields("CustomerNumber").Value
            '            End If
            '        End If
            '    ElseIf Me.Fields("MATCH_Type").Value = 1 Then 'vbBabel.MatchType.MatchedOnCustomer Then
            '        Me.txtInvoice.Visible = False
            '        Me.txtCustomer.Visible = True
            '        Me.txtInvoice.Value = ""
            '        If Me.Fields("CustomerLabel").Value = "BB_CustomerLabel" Then
            '            Me.txtCustomer.Value = LRS(40073) & " " & Me.Fields("CustomerNumber").Value
            '            'Me.txtCustomer.Value = "Kundenr." & " " & Me.Fields("CustomerNumber").Value
            '        Else
            '            Me.txtCustomer.Value = Me.Fields("CustomerLabel").Value & " " & Me.Fields("CustomerNumber").Value
            '        End If
            '    ElseIf Me.Fields("MATCH_Type").Value = 2 Then 'vbBabel.MatchType.MatchedOnGL Then
            '        Me.txtInvoice.Visible = True
            '        Me.txtCustomer.Visible = False
            '        If Me.Fields("InvoiceLabel").Value = "BB_GLLabel" Then
            '            Me.txtInvoice.Value = LRS(40013) & " " & Me.Fields("Match_ID").Value
            '            'Me.txtInvoice.Value = "Kontonr.:" & " " & Me.Fields("Match_ID").Value
            '        Else
            '            Me.txtInvoice.Value = Me.Fields("InvoiceLabel").Value & " " & Me.Fields("InvoiceNumber").Value
            '        End If
            '        Me.txtCustomer.Text = ""
            '    ElseIf Me.Fields("MATCH_Type").Value = 3 Then 'vbBabel.MatchType.MatchedOnSupplier Then
            '        Me.txtInvoice.Visible = True
            '        Me.txtCustomer.Visible = True
            '        If Me.txtInvoice.Text <> "" Then
            '            Me.txtInvoice.Value = LRS(40051) & " " & Me.Fields("InvoiceNumber").Value
            '            'Me.txtInvoice.Value = "Fakturanr." & " " & Me.Fields("InvoiceNumber").Value
            '        Else
            '            Me.txtInvoice.Value = ""
            '        End If
            '        Me.txtCustomer.Value = LRS(40078) & " " & Me.Fields("CustomerNumber").Value
            '        'Me.txtCustomer.Value = "Leverand�rnr." & " " & Me.Fields("CustomerNumber").Value
            '    Else
            '        Me.txtInvoice.Visible = False
            '        Me.txtCustomer.Visible = False
            '        Me.txtInvoice.Value = ""
            '        Me.txtCustomer.Value = ""
            '    End If

            '    If Me.Fields("MATCH_UseOriginalAmountInMatching").Value = True Then
            '        Me.txtInvoiceCurrency.Value = Me.Fields("MON_OriginallyPaidCurrency").Value
            '        Me.lblOriginalAMountUsed.Value = LRS(40113) '"BabelBank har benyttet opprinnelig betalt bel�p." 
            '    Else
            '        Me.txtInvoiceCurrency.Value = Me.Fields("MON_InvoiceCurrency").Value
            '        Me.lblOriginalAMountUsed.Visible = False
            '    End If

            '    If Me.Fields("ImportFormat").Value = vbBabel.BabelFiles.FileType.DanskeBankTeleService Then
            '        Me.txtUniqueID.Value = "FIK: " & Me.Fields("Unique_Id").Value
            '    Else
            '        Me.txtUniqueID.Value = Me.Fields("Unique_Id").Value
            '    End If
            '    Me.Detail.Visible = True
            'Else
            Me.txtInvoice.Visible = False
            If sSpecial = "LINDORFF" Then '28.05.2018 - Added this IF
                If Not EmptyString(Me.Fields("Match_ID").Value) Then
                    Me.txtCustomer.Value = Me.Fields("Match_ID").Value
                    Me.txtCustomer.Visible = True
                Else
                    Me.txtCustomer.Visible = False
                End If
            Else
                Me.txtCustomer.Visible = False
            End If
            '14.09.2015 - Added next IF
            If Not EmptyString(Me.Fields("Unique_ID").Value) Then
                'If oPayment.Invoices.Count = 1 Then
                Me.txtUniqueID.Visible = True
                Me.txtUniqueID.Value = "KID: " & Me.Fields("Unique_ID").Value
                'Else
                'Me.txtUniqueID.Visible = False
                'End If
            Else
                Me.txtUniqueID.Visible = False
            End If
            Me.txtInvoiceCurrency.Visible = False
            Me.txtInvoiceAmount.Visible = False
            Me.lblOriginalAMountUsed.Visible = False
            'End If

            ' New 31.03.2004
            ' Changes due to structured payments:
            'NB This code from vb6 is not implemented. Probably not important at all
            ' '' ''If oPayment.PayCode = 629 Then
            ' '' ''    ' Assume no freetext, show structured InvoiceNo/CreditNO and Amount
            ' '' ''    If Trim$(oInvoice.Unique_Id) <> "" Then
            ' '' ''        ' KID
            ' '' ''        If oInvoice.MON_InvoiceAmount < 0 Then
            ' '' ''            ' KID with credit
            ' '' ''            lblConcerns.Caption = LRS(40053)  'CreditKID
            ' '' ''        Else
            ' '' ''            lblConcerns.Caption = LRS(40046)  'KID
            ' '' ''        End If
            ' '' ''        Fields("fldFreeText").Value = oInvoice.Unique_Id
            ' '' ''    Else
            ' '' ''        ' No KID
            ' '' ''        If oInvoice.MON_InvoiceAmount < 0 Then
            ' '' ''            ' Kreditnota
            ' '' ''            lblConcerns.Caption = LRS(40052)
            ' '' ''        Else
            ' '' ''            ' Fakturanr.
            ' '' ''            lblConcerns.Caption = LRS(40051)
            ' '' ''        End If
            ' '' ''        Fields("fldFreeText").Value = oInvoice.InvoiceNo
            ' '' ''    End If  'Trim$(oInvoice.Unique_Id) <> "" Then
            ' '' ''    ' Only show amount once for each invoice
            ' '' ''    txtInvAmount.Visible = True
            ' '' ''    Fields("fldInvAmount").Value =VB6.Format(oInvoice.MON_InvoiceAmount / 100, "##,#00.00")
            ' '' ''Else
            ' '' ''    ' Not structured

            ' '' ''    lblConcerns.Caption = LRS(40010)
            ' '' ''    ' If KID as well as freetext, show KID;
            ' '' ''    If Trim$(oInvoice.Unique_Id) <> "" Then
            ' '' ''THIS IS IMPLEMENTED        If oBabelFile.ImportFormat = DanskeBankTeleService Then
            ' '' ''THIS IS IMPLEMENTED            Fields("fldID").Value = "FIK: " & oInvoice.Unique_Id
            ' '' ''        Else
            ' '' ''            If oInvoice.MON_InvoiceAmount < 0 Then
            ' '' ''                ' KID with credit
            ' '' ''                Fields("fldID").Value = LRS(40053) & " " & oInvoice.Unique_Id
            ' '' ''            Else
            ' '' ''                ' KID
            ' '' ''                Fields("fldID").Value = LRS(40046) & " " & oInvoice.Unique_Id
            ' '' ''            End If
            ' '' ''        End If
            ' '' ''    Else
            ' '' ''        Fields("fldID").Value = ""
            ' '' ''    End If  'Trim$(oInvoice.Unique_Id) <> ""
            ' '' ''End If 'oPayment.PayCode = 629 Then

            'More SPECIAL-stuff
            If sSpecial = "SG FINANS" Then
                ' 27.11.2007 Do not show if no info in i_Name:
                If IsDBNull(Me.Fields("I_Name").Value) Then
                    If IsDBNull(Me.Fields("I_Adr1").Value) Then
                        Me.grPaymentReceiverHeader.Visible = False
                    ElseIf EmptyString(Me.Fields("I_Adr1").Value) Then
                        Me.grPaymentReceiverHeader.Visible = False
                    Else
                        Me.grPaymentReceiverHeader.Visible = True
                    End If
                ElseIf IsDBNull(Me.Fields("I_Adr1").Value) Then
                    If EmptyString(Me.Fields("I_Name").Value) Then
                        Me.grPaymentReceiverHeader.Visible = False
                    Else
                        Me.grPaymentReceiverHeader.Visible = True
                    End If
                ElseIf EmptyString(Me.Fields("I_Name").Value) And EmptyString(Me.Fields("I_Adr1").Value) Then
                    Me.grPaymentReceiverHeader.Visible = False
                Else
                    Me.grPaymentReceiverHeader.Visible = True
                End If
            Else
                Me.grPaymentReceiverHeader.Visible = False
            End If

            'If oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.PartlyMatched And oInvoice.MATCH_Matched = True Then
            If Me.Fields("Matched").Value = False Then
                bValid = True
            Else
                bValid = False
            End If

            If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                oBabelLog.Heading = "BabelBank info: "
                oBabelLog.AddLogEvent("Report_503:  Before setting colors. Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
            End If

            If bValid Then
                Me.txtInvoice.ForeColor = System.Drawing.Color.Black
                Me.txtCustomer.ForeColor = System.Drawing.Color.Black
                Me.txtUniqueID.ForeColor = System.Drawing.Color.Black
                Me.txtInvoiceCurrency.ForeColor = System.Drawing.Color.Black
                Me.txtInvoiceAmount.ForeColor = System.Drawing.Color.Black
                Me.lblExtra1.ForeColor = System.Drawing.Color.Black
                Me.txtExtra1.ForeColor = System.Drawing.Color.Black
                Me.lblMyField.ForeColor = System.Drawing.Color.Black
                Me.txtMyField.ForeColor = System.Drawing.Color.Black
            Else
                'Set text info in the detailsection to grey
                Me.txtInvoice.ForeColor = System.Drawing.Color.Gray
                Me.txtCustomer.ForeColor = System.Drawing.Color.Gray
                Me.txtUniqueID.ForeColor = System.Drawing.Color.Gray
                Me.txtInvoiceCurrency.ForeColor = System.Drawing.Color.Gray
                Me.txtInvoiceAmount.ForeColor = System.Drawing.Color.Gray
                Me.lblExtra1.ForeColor = System.Drawing.Color.Gray
                Me.txtExtra1.ForeColor = System.Drawing.Color.Gray
                Me.lblMyField.ForeColor = System.Drawing.Color.Gray
                Me.txtMyField.ForeColor = System.Drawing.Color.Gray
            End If

           
        End If

        If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("Report_503:  End of FetchData. Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
        End If

    End Sub

    Private Sub Detail_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail.Format
        Dim rptVoucherFreetext As New rp_Sub_Freetext()
        Dim childFreetextDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        Dim sMySQL As String
        Dim sLabel As String
        Dim lTextCounter As Long
        Dim sText As String
        Dim pLocation As System.Drawing.PointF

        'If sSpecial = "LINDORFF" Then
        '    nInvoiceCounter = nInvoiceCounter + 1
        '    Me.txtInvoice.Value =VB6.Format(nInvoiceCounter, "###") & " Saksnr: " & Me.txtInvoice.Value
        'End If

        'If EmptyString(Me.txtUniqueID.Value) Then
        '    Me.txtUniqueID.Visible = False
        'Else
        '    If sSpecial = "SG FINANS" Then
        '        Me.txtUniqueID.Visible = False
        '    Else
        '        Me.txtUniqueID.Value = "KID: " & Me.txtUniqueID.Value
        '        Me.txtCustomer.Width = 1.75
        '        Me.txtUniqueID.Visible = True
        '    End If
        'End If

        If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("Report_503:  Start of Detail.Format. Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
        End If

        '14.09.2015 - Added next IF
        If Not EmptyString(Me.Fields("Unique_ID").Value) Then
            pLocation.X = 0
            pLocation.Y = 0

            Me.txtUniqueID.Location = pLocation
        End If

        sLabel = LRS(40114) '"Bilagstekst:" 
        sText = String.Empty

        If bReportFromDatabase Then
            childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
            sMySQL = "SELECT XText AS [Freetext], '" & sLabel & "' AS Label FROM [Freetext] WHERE Company_ID = " & Trim$(Str(iCompany_ID))
            sMySQL = sMySQL & " AND BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
            sMySQL = sMySQL & " AND Batch_ID = " & Trim$(Str(iBatch_ID))
            sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
            sMySQL = sMySQL & " AND Invoice_ID = " & Trim$(Str(iInvoice_ID))
            sMySQL = sMySQL & " AND (Qualifier = 1 or Qualifier = 4) ORDER BY Freetext_ID ASC" '28.05.2018 - Added "or Qualifier = 4" for Lindorff 

            If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                oBabelLog.Heading = "BabelBank info: "
                oBabelLog.AddLogEvent("Report_503:  Before running subReportFreetext (report from database). Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
            End If

            childFreetextDataSource.SQL = sMySQL
            rptVoucherFreetext.DataSource = childFreetextDataSource
            Me.SubRptVoucherFreetext.Report = rptVoucherFreetext

            If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                oBabelLog.Heading = "BabelBank info: "
                oBabelLog.AddLogEvent("Report_503:  After running subReportFreetext (report from database). Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
            End If

            '01.06.2015 - Added next IF
            If Not bValid Then
                rptVoucherFreetext.SetColorToGrey(True)
            End If
        Else
            If oInvoice.Freetexts.Count > 0 And oInvoice.MATCH_Original = True Then
                sText = ""
                For Each oFreeText In oInvoice.Freetexts
                    If oFreeText.Qualifier = 1 Then
                        sText = sText & oFreeText.Text
                        sText = sText & vbCrLf 'Add carriagereturn and line feed to the end
                        lTextCounter = lTextCounter + 1
                    End If
                Next oFreeText
                'Remove last vbCrLf
                If sText.Length > 1 Then
                    sText = Left(sText, Len(sText) - 2)
                End If

                '14.09.2015 - Added next IF
                If sText.Length > 0 Then
                    If bFirstVoucherFreetext Then
                        rptVoucherFreetext.Label = LRS(40010) '"Bel�pet gjelder:" 
                        bFirstVoucherFreetext = False
                    Else
                        rptVoucherFreetext.Label = String.Empty 'Hides the label as well
                    End If
                    rptVoucherFreetext.Text = sText
                    rptVoucherFreetext.ReportFromDatabase = False
                    '01.06.2015 - Added next IF
                    If Not bValid Then
                        rptVoucherFreetext.SetColorToGrey(True)
                    End If
                    'Me.SubRptFreetext.Report = rptVoucherFreetext

                    If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                        oBabelLog.Heading = "BabelBank info: "
                        oBabelLog.AddLogEvent("Report_503:  Before running subReportFreetext (report on collections). Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
                    End If

                    childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
                    childFreetextDataSource.SQL = "SELECT * FROM Company"
                    rptVoucherFreetext.DataSource = childFreetextDataSource
                    Me.SubRptVoucherFreetext.Visible = True
                    Me.SubRptVoucherFreetext.Report = rptVoucherFreetext

                    If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                        oBabelLog.Heading = "BabelBank info: "
                        oBabelLog.AddLogEvent("Report_503:  Before running subReportFreetext (report on collections). Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
                    End If

                Else
                    Me.SubRptVoucherFreetext.Visible = False
                End If
            Else
                'rptVoucherFreetext.ReportFromDatabase = False
                'rptVoucherFreetext.Text = ""
                'Me.SubRptFreetext.Report = rptVoucherFreetext
                Me.SubRptVoucherFreetext.Visible = False
            End If

            ''NEW WAY - marked Boasson Hagen in the report - Old way markes Hushovd
            'If Me.Fields("Matched").Value = True And Me.Fields("Final").Value = True Then
            '    'nCalculatedMacthedAmount = nCalculatedMacthedAmount + Me.txtInvoiceAmount.Value
            '    Me.txtUnmatchedAmount.Value = Me.txtUnmatchedAmount.Value + 0
            '    Me.txtMatchedAmount.Value = Me.txtMatchedAmount.Value + Me.txtInvoiceAmount.Value
            '    Me.txtTotalPayment.Value = Me.txtTotalPayment.Value + Me.txtInvoiceAmount.Value
            'Else
            '    If Me.Fields("Final").Value = True Then
            '        Me.txtUnmatchedAmount.Value = Me.txtUnmatchedAmount.Value + Me.txtInvoiceAmount.Value
            '        Me.txtMatchedAmount.Value = Me.txtMatchedAmount.Value + 0
            '        Me.txtTotalPayment.Value = Me.txtTotalPayment.Value + Me.txtInvoiceAmount.Value
            '    End If
            'End If
            'If bValid Then
            '    Me.txtMatchedBatchAmount.Value = Me.txtMatchedBatchAmount.Value + Me.txtInvoiceAmount.Value
            '    Me.txtMatchedBatchCount.Value = Me.txtMatchedBatchCount.Value + 1
            '    Me.txtMatchedTotalAmount.Value = Me.txtMatchedTotalAmount.Value + Me.txtInvoiceAmount.Value
            '    Me.txtMatchedTotalCount.Value = Me.txtMatchedTotalCount.Value + 1
            'End If

        End If

        If Me.Fields("Matched").Value = True And Me.Fields("Final").Value = True Then
            'nCalculatedMacthedAmount = nCalculatedMacthedAmount + Me.txtInvoiceAmount.Value
            Me.txtUnmatchedAmount.Value = Me.txtUnmatchedAmount.Value + 0
            Me.txtMatchedAmount.Value = Me.txtMatchedAmount.Value + Me.txtInvoiceAmount.Value
            Me.txtTotalPayment.Value = Me.txtTotalPayment.Value + Me.txtInvoiceAmount.Value
        Else
            If Me.Fields("Final").Value = True Then
                Me.txtUnmatchedAmount.Value = Me.txtUnmatchedAmount.Value + Me.txtInvoiceAmount.Value
                Me.txtMatchedAmount.Value = Me.txtMatchedAmount.Value + 0
                Me.txtTotalPayment.Value = Me.txtTotalPayment.Value + Me.txtInvoiceAmount.Value
            End If
        End If

        If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("Report_503:  End of Detial.Format. Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
        End If

    End Sub

    Private Sub grFreetextHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grFreetextHeader.Format
        Dim rptFreetext As New rp_Sub_Freetext()
        Dim childFreetextDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        'Dim rptFreetext2 As New rp_Sub_Freetext()
        'Dim childFreetextDataSource2 As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        Dim sLabel As String
        Dim sMySQL As String
        Dim sText As String = String.Empty

        If bReportFromDatabase Then

            'Show text on statement
            sLabel = LRS(40054)  ' Text on statement
            childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
            sMySQL = "SELECT XText AS [Freetext], '" & sLabel & "' AS Label FROM [Freetext] WHERE Company_ID = " & Trim$(Str(iCompany_ID))
            sMySQL = sMySQL & " AND BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
            sMySQL = sMySQL & " AND Batch_ID = " & Trim$(Str(iBatch_ID))
            sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
            If bSQLServer Then
                sMySQL = sMySQL & " AND Qualifier = 2 AND Len(LTrim(XText)) <> 0 ORDER BY Invoice_ID ASC, Freetext_ID ASC"
            Else
                sMySQL = sMySQL & " AND Qualifier = 2 AND Trim(XText) <> '' ORDER BY Invoice_ID ASC, Freetext_ID ASC"
            End If

            If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                oBabelLog.Heading = "BabelBank info: "
                oBabelLog.AddLogEvent("Report_503:  Before running subReportFreetext - text on statement (report on database). Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
            End If

            childFreetextDataSource.SQL = sMySQL
            rptFreetext.DataSource = childFreetextDataSource
            Me.SubRptStatementText.Report = rptFreetext

            If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                oBabelLog.Heading = "BabelBank info: "
                oBabelLog.AddLogEvent("Report_503:  After running subReportFreetext - text on statement (report on database). Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
            End If

            'childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
            'sMySQL = "SELECT XText AS Freetext, '' AS Label FROM Freetext WHERE Company_ID = " & Trim$(Str(iCompany_ID))
            'sMySQL = sMySQL & " AND BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
            'sMySQL = sMySQL & " AND Batch_ID = " & Trim$(Str(iBatch_ID))
            'sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
            'sMySQL = sMySQL & " AND Qualifier = 1 AND Trim(XText) <> '' ORDER BY Invoice_ID ASC, Freetext_ID ASC"

            'childFreetextDataSource.SQL = sMySQL
            'rptFreetext.DataSource = childFreetextDataSource
            ''rptFreetext.sh
            'Me.SubRptFreetext.Report = rptFreetext
        Else
            If Not EmptyString(oPayment.Text_I_Statement) Or Not EmptyString(oPayment.Text_E_Statement) Then '19.05.2018 - Added Text_E_Statement
                If Not EmptyString(oPayment.Text_I_Statement) Then
                    sText = oPayment.Text_I_Statement.Trim
                Else
                    sText = oPayment.Text_E_Statement.Trim
                End If

                rptFreetext.Label = LRS(40054) 'Text on statement 
                rptFreetext.Text = sText
                rptFreetext.ReportFromDatabase = False
                'Me.SubRptFreetext.Report = rptVoucherFreetext

                childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
                childFreetextDataSource.SQL = "SELECT * FROM Company"

                If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                    oBabelLog.Heading = "BabelBank info: "
                    oBabelLog.AddLogEvent("Report_503:  Before running subReportFreetext - text on statement (report on collections). Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
                End If


                rptFreetext.DataSource = childFreetextDataSource
                Me.SubRptStatementText.Visible = True
                Me.SubRptStatementText.Report = rptFreetext

                If bLog Then 'And (nLogCounter = 1 Or nLogCounter / 100 = Math.Round(nLogCounter / 100, 0)) Then
                    oBabelLog.Heading = "BabelBank info: "
                    oBabelLog.AddLogEvent("Report_503:  After running subReportFreetext - text on statement (report on collections). Logcounter = " & nLogCounter.ToString, 8) 'vbLogEventTypeInformationDetail
                End If

            Else
                rptFreetext.ReportFromDatabase = False
                rptFreetext.Text = String.Empty
                rptFreetext.Label = String.Empty
                Me.SubRptStatementText.Visible = False
                Me.SubRptStatementText.Report = rptFreetext
            End If
        End If

    End Sub

    Private Sub grPaymentFooter_BeforePrint(ByVal sender As Object, ByVal e As System.EventArgs) Handles grPaymentFooter.BeforePrint
        'Me.txtMatchedAmount.Value = nCalculatedMacthedAmount
        'Me.txtUnmatchedAmount.Value = nCalculatedUnMacthedAmount
    End Sub

    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property BabelLog() As vbLog.vbLogging
        Set(ByVal Value As vbLog.vbLogging)
            oBabelLog = Value
            bLog = True
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            oBabelFiles = Value
        End Set
    End Property
    Public WriteOnly Property ClientNumber() As String
        Set(ByVal Value As String)
            sClientNumber = Value
        End Set
    End Property
    Public WriteOnly Property IncludeOCR() As Boolean
        Set(ByVal Value As Boolean)
            bIncludeOCR = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Public WriteOnly Property ReportOnSelectedItems() As Boolean   ' added 27.09.2017
        Set(ByVal Value As Boolean)
            bReportOnSelectedItems = Value
        End Set
    End Property
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bLog Then
            oBabelLog.Heading = "BabelBank info: "
            oBabelLog.AddLogEvent("Report_503:  In rptPrint_ReportEnd.", 8) 'vbLogEventTypeInformationDetail
        End If

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class
