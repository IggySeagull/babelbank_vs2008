Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmCorrectBank
    Inherits System.Windows.Forms.Form
    Private Sub thisForm_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        e.Graphics.DrawLine(Pens.Orange, 9, 207, 702, 207)
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.Hide()
    End Sub

    Private Sub frmCorrectBank_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "", 400)
        FormLRSCaptions(Me)
        'Me.lblErrMessage.BackColor = System.Drawing.Color.Cyan 'vbHighlight
    End Sub

    Private Sub txtBank_ID_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBank_ID.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtBank_ID.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub cmbBank_IDType_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbBank_IDType.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = cmbBank_IDType.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtBank_Adr1_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBank_Adr1.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtBank_Adr1.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtBank_Adr2_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBank_Adr2.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtBank_Adr2.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtBank_Adr3_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBank_Adr3.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtBank_Adr3.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtBank_Adr4_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBank_Adr4.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtBank_Adr4.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtBank_Info_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtBank_Info.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtBank_Info.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtCorrBank_ID_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCorrBank_ID.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtCorrBank_ID.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub cmbCorrBank_IDType_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbCorrBank_IDType.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = cmbCorrBank_IDType.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtCorrBank_Adr1_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCorrBank_Adr1.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtCorrBank_Adr1.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtCorrBank_Adr2_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCorrBank_Adr2.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtCorrBank_Adr2.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtCorrBank_Adr3_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCorrBank_Adr3.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtCorrBank_Adr3.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub txtCorrBank_Adr4_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCorrBank_Adr4.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = txtCorrBank_Adr4.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
        SelectAllText()
    End Sub
    Private Sub chkChargeMeDomestic_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkChargeMeDomestic.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = Me.chkChargeMeDomestic.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
    End Sub
    Private Sub chkChargeMeAbroad_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkChargeMeAbroad.Enter
        'x  Display errormessage for this field, stored in control.tag;
        Me.lblErrMessage.Text = Me.chkChargeMeAbroad.Tag
        PrepareErrorMessage() ' Icon for Error or Warning, skip first pos in Text
    End Sub
    Private Function PrepareErrorMessage() As Object
        ' Icon for Error or Warning, skip first pos in Text
        If VB.Left(Me.lblErrMessage.Text, 1) = "E" Then
            Me.Image2(15).Visible = True
            Me.imgExclamation(15).Visible = False
            Me.lblErrMessage.ForeColor = System.Drawing.Color.Red
        ElseIf VB.Left(Me.lblErrMessage.Text, 1) = "W" Then
            ' Warning
            Me.Image2(15).Visible = False
            Me.imgExclamation(15).Visible = True
            Me.lblErrMessage.ForeColor = System.Drawing.Color.Red 'vbGreen
        End If
        Me.lblErrMessage.Text = Mid(Me.lblErrMessage.Text, 2)


    End Function
End Class
