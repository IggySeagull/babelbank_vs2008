<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_sub_Documentation
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(rp_sub_Documentation))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtFreetext = New DataDynamics.ActiveReports.TextBox
        Me.txtLabel = New DataDynamics.ActiveReports.TextBox
        CType(Me.txtFreetext, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLabel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtFreetext, Me.txtLabel})
        Me.Detail.Height = 0.2!
        Me.Detail.Name = "Detail"
        '
        'txtFreetext
        '
        Me.txtFreetext.Border.BottomColor = System.Drawing.Color.Black
        Me.txtFreetext.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFreetext.Border.LeftColor = System.Drawing.Color.Black
        Me.txtFreetext.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFreetext.Border.RightColor = System.Drawing.Color.Black
        Me.txtFreetext.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFreetext.Border.TopColor = System.Drawing.Color.Black
        Me.txtFreetext.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFreetext.CanShrink = True
        Me.txtFreetext.DataField = "Freetext"
        Me.txtFreetext.Height = 0.15!
        Me.txtFreetext.Left = 1.75!
        Me.txtFreetext.Name = "txtFreetext"
        Me.txtFreetext.Style = "font-size: 8pt; "
        Me.txtFreetext.Text = "Freetext"
        Me.txtFreetext.Top = 0.0!
        Me.txtFreetext.Width = 3.813!
        '
        'txtLabel
        '
        Me.txtLabel.Border.BottomColor = System.Drawing.Color.Black
        Me.txtLabel.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtLabel.Border.LeftColor = System.Drawing.Color.Black
        Me.txtLabel.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtLabel.Border.RightColor = System.Drawing.Color.Black
        Me.txtLabel.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtLabel.Border.TopColor = System.Drawing.Color.Black
        Me.txtLabel.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtLabel.CanShrink = True
        Me.txtLabel.DataField = "Label"
        Me.txtLabel.Height = 0.15!
        Me.txtLabel.Left = 0.0!
        Me.txtLabel.Name = "txtLabel"
        Me.txtLabel.Style = "font-size: 8pt; "
        Me.txtLabel.Text = "txtLabel"
        Me.txtLabel.Top = 0.0!
        Me.txtLabel.Width = 1.0!
        '
        'rp_sub_Documentation
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black; ", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic; ", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ", "Heading3", "Normal"))
        CType(Me.txtFreetext, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLabel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents txtFreetext As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtLabel As DataDynamics.ActiveReports.TextBox
End Class
