Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_007_IncomingACHLockbox
    Dim iCompany_ID As Integer
    Dim iBabelfile_ID As Integer
    Dim iBatch_ID As Integer
    Dim iPayment_ID As Integer
    Dim iInvoice_ID As Integer
    Dim sOldI_Account As String
    Dim sI_Account As String
    Dim sClientInfo As String
    Dim sClientName As String
    Dim sPayType As String

    Dim nTotalAmount As Double
    Dim sOrderBy As String
    Dim bShowBatchfooterTotals As Boolean
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim bShowI_Account As Boolean
    Dim bShowFilename As Boolean
    Dim bACHAndOriginalInvoice As Boolean

    Dim sReportName As String
    Dim sSpecial As String
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim bPrintReport As Boolean
    Dim bSQLServer As Boolean = False
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean

    Private Sub rp_007_IncomingACHLockbox_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        'Set the different breaklevels

        Fields.Add("BreakField")

    End Sub

    Private Sub rp_007_IncomingACHLockbox_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        Me.Cancel()
    End Sub

    Private Sub rp_007_IncomingACHLockbox_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportStart

        bShowBatchfooterTotals = True

        Select Case iBreakLevel
            Case 0 'NOBREAK
                bShowBatchfooterTotals = False
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = False

            Case 1 'BREAK_ON_ACCOUNT (and per day)
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 2 'BREAK_ON_BATCH
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 3 'BREAK_ON_PAYMENT
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 4 'BREAK_ON_ACCOUNTONLY
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = False

            Case 5 'BREAK_ON_CLIENT 'New
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = False
                bShowFilename = False

            Case 6 'BREAK_ON_FILE 'Added 09.09.2014
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = True

            Case 999
                bShowDATE_Production = True
                bShowClientName = True
                bShowI_Account = True
                bShowFilename = True

            Case Else 'NOBREAK
                bShowDATE_Production = True
                bShowClientName = False
                bShowI_Account = False
                bShowFilename = False

        End Select

        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtDATE_Production.OutputFormat = "D"
            Me.txtDATE_Value.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtDATE_Production.OutputFormat = "d"
            Me.txtDATE_Value.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = LRS(40034)   ' Remittance Payments 
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'Me.rptInfoPageCounterGB.Left = 4
        'End If

        'Set the caption of the labels

        'grBatchHeader
        'Me.lblFilename.Text = LRS(40001) '"Filnavn:" 
        Me.lblDate_Production.Text = LRS(40027) '"Prod.dato:" 
        Me.lblClient.Text = LRS(40030) '"Klient:" 
        Me.lblI_Account.Text = LRS(40020) '"Mottakerkonto:" 

        'grPaymentHeader
        Me.lblOrderingCompany.Text = LRS(40088) '"Ordering Company" 
        Me.lblCheckNumber.Text = LRS(40083) '"Sjekknummer:" 
        Me.lblCheckDate.Text = LRS(40079) '"Bankinfo:" 
        Me.lblREF_Own.Text = LRS(40082) '"Check DDA Number:" 
        Me.lblREF_Bank2.Text = LRS(40084) & ":" '"Sjekkdato" 
        Me.lblDATE_Value.Text = LRS(40023) 'Posting date

        'Detail
        Me.lblConcerns.Text = LRS(40010) '"Bel�pet gjelder:" 

        'grPaymentFooter

        'grBatchFooter
        Me.lblTotalCreditAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblTotalDebitAmount.Text = LRS(40105) '"Deltotal - Antall:" 

        'grBabelFileFooter
        Me.lblTotalAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblTotalNoOfPayments.Text = LRS(40106) '"Total - Antall:" 

        ' Special cases;
        '*******VB6 Code

        ''Me.lblFileSum = LRS(40028)

        sOldI_Account = ""
        sClientInfo = LRS(40107) '"Ingen klientinfo angitt" 

        If Not bShowBatchfooterTotals Then
            Me.grBatchFooter.Visible = False
        End If
        If Not bShowDATE_Production Then
            Me.txtDATE_Production.Visible = False
        End If
        If Not bShowClientName Then
            Me.txtClientName.Visible = False
        End If
        If Not bShowI_Account Then
            Me.txtI_Account.Visible = False
        End If
        If Not bShowFilename Then
            Me.txtFilename.Visible = False
        End If

    End Sub
    Private Sub rp_007_IncomingACHLockbox_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        'The Fields collection should never be accessed outside the DataInitialize and FetchData events
        Dim sDATE_Production As String
        Dim sDATE_Value As String
        Dim iTopAdjustment As Integer
        Dim sTemp As String

        iTopAdjustment = 0
        sTemp = ""

        If Not eArgs.EOF Then

            Select Case iBreakLevel
                Case 0 'NOBREAK
                    Me.Fields("BreakField").Value = ""
                    'Me.lblFilename.Visible = False
                    'Me.txtFilename.Visible = False

                Case 1 'BREAK_ON_ACCOUNT (and per day)
                    'Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Production").Value
                    ' 03.02.2016 changed to DATE_Payment
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value & Me.Fields("DATE_Payment").Value

                Case 2 'BREAK_ON_BATCH
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value))

                Case 3 'BREAK_ON_PAYMENT
                    Me.Fields("BreakField").Value = Trim$(Str(Me.Fields("BabelFile_ID").Value)) & "-" & Trim$(Str(Me.Fields("Batch_ID").Value)) & "-" & Trim$(Str(Me.Fields("Payment_ID").Value))

                Case 4 'BREAK_ON_ACCOUNTONLY
                    Me.Fields("BreakField").Value = Me.Fields("I_Account").Value

                Case 5 'BREAK_ON_CLIENT 'New
                    Me.Fields("BreakField").Value = Me.Fields("Client").Value

                Case 6 'BREAK_ON_FILE 'Added 09.09.2014
                    Me.Fields("BreakField").Value = Me.Fields("BabelFile_ID").Value.ToString

                Case 999 'Added 06.01.2015 - Use the breaklevel from the special SQL
                    'The break is set in the SQL

                Case Else
                    Me.Fields("BreakField").Value = ""

            End Select

            'Store the keys for usage in the subreport(s)
            iCompany_ID = Me.Fields("Company_ID").Value
            iBabelfile_ID = Me.Fields("Babelfile_ID").Value
            iBatch_ID = Me.Fields("Batch_ID").Value
            iPayment_ID = Me.Fields("Payment_ID").Value
            iInvoice_ID = Me.Fields("Invoice_ID").Value
            'START WORKING HERE

            If Not IsDBNull(Me.Fields("I_Account").Value) Then
                sI_Account = Me.Fields("I_Account").Value
            Else
                sI_Account = ""
            End If
            If sI_Account <> sOldI_Account And Not EmptyString(sI_Account) Then
                sClientInfo = FindClientName(sI_Account)
                sOldI_Account = sI_Account
            End If

            Me.txtClientName.Value = sClientInfo

            sDATE_Production = Me.Fields("DATE_Production").Value
            Me.txtDATE_Production.Value = DateSerial(CInt(Left$(sDATE_Production, 4)), CInt(Mid$(sDATE_Production, 5, 2)), CInt(Right$(sDATE_Production, 2)))

            sDATE_Value = Me.Fields("DATE_Value").Value
            Me.txtDATE_Value.Value = DateSerial(CInt(Left$(sDATE_Value, 4)), CInt(Mid$(sDATE_Value, 5, 2)), CInt(Right$(sDATE_Value, 2)))

            'If Not IsDBNull(Me.Fields("REF_Own").Value) Then
            '    Me.txtReceivingCompanyID.Text = LRS(40089) & ": " & Me.Fields("REF_Own").Value 'Rec. Company ID
            'Else
            '    Me.txtReceivingCompanyID.Text = LRS(40089) & ": " 
            'End If

            sPayType = Me.Fields("DnBNORTBIPayType").Value

            If Me.Fields("MON_TransferredAmount").Value <> 0 Then
                Me.txtMON_P_TransferredAmount.Text = Me.Fields("MON_TransferredAmount").Value
                If sPayType = "MT940" Then
                    If Me.Fields("MATCH_Original").Value = True And Me.Fields("NoOfFreetexts").Value < 2 Then
                        Me.txtTotalDebitAmount.Text = 0  ' Always 0 for the MT940 we need
                        Me.txtTotalCreditAmount.Text = Me.Fields("MON_TransferredAmount").Value
                    Else
                        Me.txtTotalDebitAmount.Text = 0
                        Me.txtTotalCreditAmount.Text = 0
                    End If
                ElseIf Me.Fields("MATCH_Original").Value = True And Me.Fields("NoOfFreetexts").Value < 2 Then
                    If Me.Fields("VoucherType").Value = "22" Or Me.Fields("VoucherType").Value = "32" Or Me.Fields("VoucherType").Value = "42" Then
                        Me.txtTotalDebitAmount.Text = 0
                        Me.txtTotalCreditAmount.Text = Me.Fields("MON_TransferredAmount").Value
                    Else
                        Me.txtTotalDebitAmount.Text = Me.Fields("MON_TransferredAmount").Value
                        Me.txtTotalCreditAmount.Text = 0
                    End If
                ElseIf Me.Fields("MATCH_Original").Value = False And sPayType = "LBX" And iInvoice_ID Then
                    Me.txtTotalDebitAmount.Text = 0  ' Always 0 for LBX
                    Me.txtTotalCreditAmount.Text = Me.Fields("MON_TransferredAmount").Value
                Else
                    Me.txtTotalDebitAmount.Text = 0
                    Me.txtTotalCreditAmount.Text = 0
                End If
            Else
                Me.txtMON_P_TransferredAmount.Text = Me.Fields("MON_InvoiceAmount").Value
                If Me.Fields("MATCH_Original").Value = True And Me.Fields("NoOfFreetexts").Value < 2 Then
                    Me.txtTotalDebitAmount.Text = Me.Fields("MON_TransferredAmount").Value
                Else
                    Me.txtTotalDebitAmount.Text = 0
                End If
            End If

            'sPayType = FixNull(Me.Fields("CustomerNumber"))

            If Me.Fields("VoucherType").Value = "22" Or Me.Fields("VoucherType").Value = "32" Or Me.Fields("VoucherType").Value = "42" Then
                Me.txtDebitCredit.Text = "C"
            ElseIf Me.Fields("VoucherType").Value = "27" Or Me.Fields("VoucherType").Value = "37" Or Me.Fields("VoucherType").Value = "47" Then
                Me.txtDebitCredit.Text = "D"
            Else
                Me.txtDebitCredit.Text = "?"
            End If

            If Me.Fields("MON_AccountCurrency").Value = Me.Fields("MON_InvoiceCurrency").Value Or Me.Fields("MON_AccountCurrency").Value = "" Then
                Me.txtMON_AccountCurrency.Visible = False
                Me.txtMON_AccountAmount.Visible = False
            Else
                ' Show accountcurrency/amount if different from invoice
                Me.txtMON_AccountCurrency.Visible = True
                Me.txtMON_AccountAmount.Visible = True
            End If

            bACHAndOriginalInvoice = False
            If sPayType = "ACH" Then
                If Me.Fields("MATCH_Original").Value = True Then
                    bACHAndOriginalInvoice = True
                End If
                ShowACHCaptions()
                Me.txtLockboxnumber.Text = bbGetPayCodeString(Me.Fields("PayCode").Value)  ' CCD og CTX
                Me.txtExecutionRef.Text = Me.Fields("ExtraD1").Value
                Me.txtBIC.Text = Me.Fields("ExtraD2").Value  ' Ordering Comapny Desc Entry
                Me.txtCheckDate.Text = Me.Fields("BANK_BranchNo").Value & AddCDV7Digit(Me.Fields("BANK_BranchNo").Value)  ' Originating BANKID
                Me.txtBANK_Branch.Text = Me.Fields("REF_Bank2").Value  ' Receiving Bank_ID

                Me.txtReceivingCompanyID.Text = LRS(40089) & ": " & Me.Fields("REF_Own").Value 'Rec. Company ID

            ElseIf sPayType = "MT940" Then
                ShowMT940Captions()
                'Me.txtLockboxnumber.Text = bbGetPayCodeString(oPayment.PayCode)  ' CCD og CTX
                'Me.txtREF_Bank1.Text = oPayment.ExtraD1
                'Me.txtBIC.Text = oPayment.ExtraD2  ' Ordering Comapny Desc Entry
                'Me.txtCheckDate.Text = oPayment.BANK_BranchNo & AddCDV7Digit(oPayment.BANK_BranchNo)  ' Originating BANKID
                'Me.txtBANK_Branch.Text = oPayment.REF_Bank2  ' Receiving Bank_ID
                Me.txtReceivingCompanyID.Text = LRS(40096) & ": " & Me.Fields("REF_Own").Value 'Ref. account owner

            Else
                ' Lockbox
                ShowLockBoxCaptions()
                Me.txtLockboxnumber.Text = Me.Fields("VoucherNo").Value
                Me.txtBIC.Text = Me.Fields("REF_Own").Value
                'Me.txtREF_Bank1.Text = oPayment.REF_Bank2
                Me.txtCheckDate.Text = Me.Fields("ExtraD2").Value
                Me.txtItemNumber.Text = Me.Fields("ExtraD3").Value    ' Item number
                Me.txtBatchNumber.Text = Me.Fields("Batch_ID").Value    ' Batch number
                'Me.txtBANK_Branch.Text = oPayment.BANK_Name & IIf(Not EmptyString(oPayment.BANK_SWIFTCode), "   BIC:" & oPayment.BANK_SWIFTCode, "") & " " & oPayment.BANK_BranchNo ''''& AddCDV7Digit(oPayment.BANK_BranchNo)  ' Ordering bank ID
                Me.txtExecutionRef.Text = Me.Fields("BANK_Name").Value & IIf(Not EmptyString(Me.Fields("BANK_SWIFTCode").Value), "   BIC:" & Me.Fields("BANK_SWIFTCode").Value, "") & " " & Me.Fields("BANK_BranchNo").Value ''''& AddCDV7Digit(oPayment.BANK_BranchNo)  ' Ordering bank ID

            End If

            'Private Sub PreGroupTotals() '(sAccount As String, sDate As String, iBabelFiles As Long, iBatches As Long, iPayments As Long)
            '    Dim oLocalBabelFile As vbBabel.BabelFile
            '    ' totals to show in group header
            '    cTotalDebitAmount = 0
            '    cTotalCreditAmount = 0
            '    nNoOfPayments = 0

            '    ' Total up all payments;
            '    For Each oLocalBabelFile In oBabelFiles
            '        For Each oBatch In oLocalBabelFile.Batches
            '            For Each oPayment In oBatch.Payments
            '                If oPayment.VoucherType = "22" Or oPayment.VoucherType = "32" Or oPayment.VoucherType = "42" _
            '                Or oPayment.DnBNORTBIPayType = "LBX" Then
            '                    cTotalCreditAmount = cTotalCreditAmount + oPayment.MON_InvoiceAmount
            '                Else
            '                    cTotalDebitAmount = cTotalDebitAmount + oPayment.MON_InvoiceAmount
            '                End If
            '            Next oPayment
            '        Next oBatch
            '    Next oLocalBabelFile

        End If
    End Sub

    Private Sub Detail_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail.Format
        Dim rptVoucherFreetext As New rp_Sub_Freetext()
        Dim childFreetextDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()

        'If EmptyString(Me.txtUniqueID.Value) Then
        '    Me.txtUniqueID.Visible = False
        'Else
        '    If sSpecial = "SG FINANS" Then
        '        Me.txtUniqueID.Visible = False
        '    Else
        '        Me.txtUniqueID.Value = "KID: " & Me.txtUniqueID.Value
        '        Me.txtCustomer.Width = 1.75
        '        Me.txtUniqueID.Visible = True
        '    End If
        'End If

    End Sub
    Private Sub grPaymentHeader_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles grPaymentHeader.Format
        Dim rptFreetextSpecial As New rp_Sub_Freetext()
        Dim childFreetextSpecialDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        Dim sMySQL As String

        If Not Me.Fields("MATCH_Original").Value = True Or sPayType = "MT940" Then
            childFreetextSpecialDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
            sMySQL = "SELECT XText AS Freetext, '' AS Label FROM Freetext WHERE Company_ID = " & Trim$(Str(iCompany_ID))
            sMySQL = sMySQL & " AND BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
            sMySQL = sMySQL & " AND Batch_ID = " & Trim$(Str(iBatch_ID))
            sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
            sMySQL = sMySQL & " AND Qualifier < 3 ORDER BY Invoice_ID ASC, Freetext_ID ASC"

            childFreetextSpecialDataSource.SQL = sMySQL
            rptFreetextSpecial.DataSource = childFreetextSpecialDataSource
            Me.SubReportFreetextSpecial.Report = rptFreetextSpecial
        End If

    End Sub

    Private Sub grFreetextHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grFreetextHeader.Format
        Dim rptFreetext As New rp_Sub_Freetext()
        Dim childFreetextDataSource As New DataDynamics.ActiveReports.DataSources.OleDBDataSource()
        Dim sMySQL As String

        If bACHAndOriginalInvoice Then
            childFreetextDataSource.ConnectionString = CType(Me.DataSource, DataDynamics.ActiveReports.DataSources.OleDBDataSource).ConnectionString
            sMySQL = "SELECT XText AS Freetext, '' AS Label FROM Freetext WHERE Company_ID = " & Trim$(Str(iCompany_ID))
            sMySQL = sMySQL & " AND BabelFile_ID = " & Trim$(Str(iBabelfile_ID))
            sMySQL = sMySQL & " AND Batch_ID = " & Trim$(Str(iBatch_ID))
            sMySQL = sMySQL & " AND Payment_ID = " & Trim$(Str(iPayment_ID))
            sMySQL = sMySQL & " AND Qualifier < 3 ORDER BY Invoice_ID ASC, Freetext_ID ASC"

            childFreetextDataSource.SQL = sMySQL
            rptFreetext.DataSource = childFreetextDataSource
            Me.SubRptFreetext.Report = rptFreetext
        End If

    End Sub
    Private Sub ShowLockBoxCaptions()
        ' set caption for labels when we have lockbox payments
        Me.lblDate_Production.Value = LRS(40027) '"Prod.date" 
        Me.lblArchiveRef.Value = LRS(40092) '"Ordering Bank ID" 
        Me.lblCheckNumber.Value = LRS(40083) '"Check Number:" 
        Me.lblCheckDate.Value = LRS(40084) '"Check Date:" 
        Me.lblREF_Own.Value = LRS(40082) '"Check DDA Number:" 
        'Me.lblREF_Bank2.Value = LRS(40092) & ":"  ' Ordering Bank ID not in use for lockbox
        Me.lblDATE_Value.Value = LRS(40023) '"Posting Date:" 
        Me.lblLockboxno.Value = "LockboxNo:"

        Me.lblREF_Bank2.Visible = False
        Me.txtBANK_Branch.Visible = False
        Me.lblBatchNumber.Visible = True
        Me.txtBatchNumber.Visible = True
        Me.lblItemNumber.Visible = True
        Me.txtItemNumber.Visible = True

        Me.txtExecutionRef.Visible = True
        Me.txtLockboxnumber.Visible = True
        Me.txtCheckDate.Visible = True
        Me.txtBIC.Visible = True

        Me.lblLockboxno.Visible = True
        Me.lblCheckNumber.Visible = True
        Me.lblCheckDate.Visible = True
        Me.lblReceivingCompany.Visible = True
        Me.lblREF_Own.Visible = True
        Me.lblArchiveRef.Visible = True

        Me.txtReceivingCompanyID.Visible = False
        Me.txtDebitCredit.Visible = False
    End Sub
    Private Sub ShowMT940Captions()
        ' set caption for labels when we have lockbox payments
        Me.lblDate_Production.Value = LRS(40027) '"Prod.date" 

        Me.lblREF_Bank2.Visible = False
        Me.txtBANK_Branch.Visible = False
        Me.lblBatchNumber.Visible = False
        Me.txtBatchNumber.Visible = False
        Me.lblItemNumber.Visible = False
        Me.txtItemNumber.Visible = False
        Me.txtExecutionRef.Visible = False
        Me.txtLockboxnumber.Visible = False
        Me.txtCheckDate.Visible = False
        Me.txtBIC.Visible = False


        Me.lblLockboxno.Visible = False
        Me.lblCheckNumber.Visible = False
        Me.lblCheckDate.Visible = False
        Me.lblReceivingCompany.Visible = False
        Me.lblREF_Own.Visible = False
        Me.lblArchiveRef.Visible = False

        Me.txtReceivingCompanyID.Visible = True
        Me.txtDebitCredit.Visible = False
    End Sub
    Private Sub ShowACHCaptions()
        ' set caption for labels when we have ACH payments
        Me.lblDate_Production.Value = LRS(40027) '"Prod.date" 
        Me.lblArchiveRef.Value = LRS(40091) '"Ordering Company ID" 
        Me.lblCheckNumber.Value = LRS(40085) ' Trace number
        Me.lblCheckDate.Value = LRS(40092)  ' Ordering Bank ID
        Me.lblREF_Own.Value = LRS(40093)  ' Ordering Comapny Entry
        Me.lblREF_Bank2.Value = LRS(40086) 'Receiving Bank ID
        Me.lblDATE_Value.Value = LRS(40023) '"Posting Date:" 
        Me.lblLockboxno.Value = LRS(40090) ' Standard Entry Class

        Me.lblReceivingCompany.Value = LRS(40087) ' Receiving Company

        Me.lblREF_Bank2.Visible = True
        Me.txtBANK_Branch.Visible = True
        Me.lblBatchNumber.Visible = False
        Me.txtBatchNumber.Visible = False
        Me.lblItemNumber.Visible = False
        Me.txtItemNumber.Visible = False

        Me.txtExecutionRef.Visible = True
        Me.txtLockboxnumber.Visible = True
        Me.txtCheckDate.Visible = True
        Me.txtBIC.Visible = True

        Me.lblLockboxno.Visible = True
        Me.lblCheckNumber.Visible = True
        Me.lblCheckDate.Visible = True
        Me.lblReceivingCompany.Visible = True
        Me.lblREF_Own.Visible = True
        Me.lblArchiveRef.Visible = True

        Me.txtReceivingCompanyID.Visible = True
        Me.txtDebitCredit.Visible = True

    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub grBatchHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grBatchHeader.Format
        Dim pLocation As System.Drawing.PointF
        Dim pSize As System.Drawing.SizeF

        If Me.txtFilename.Visible Then
            pLocation.X = 1.5
            pLocation.Y = 0.3
            Me.txtFilename.Location = pLocation
            pSize.Height = 0.19
            pSize.Width = 4.5
            Me.txtFilename.Size = pSize
            pLocation.X = 0.125
            pLocation.Y = 0.3
            Me.lblI_Account.Location = pLocation
            Me.lblI_Account.Value = LRS(20007)
            Me.txtI_Account.Visible = False
        End If

    End Sub
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class
