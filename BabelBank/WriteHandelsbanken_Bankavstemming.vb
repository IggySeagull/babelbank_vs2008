Option Strict Off
Option Explicit On
Module WriteHandelsbanken_Bankavstemming
	' Handelsbanken NOs Bankavstemmingsformat
	'----------------------------------------
	
	Dim sLine As String ' en output-linje
    Function WriteHandelsbanken_Bankavst(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef sCompanyNo As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim i As Short

        ' lag en outputfil
        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            'Don't export payments that have been cancelled, or previously exported
                            If oPayment.Cancel = False And oPayment.Exported = False Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        bExportoBabel = True
                                    End If
                                End If 'If oPayment.DnBNORTBIPayType = "ACH" Or EmptyString(oPayment.DnBNORTBIPayType) Then
                            End If 'If oPayment.Cancel = False Then
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch


                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False And oPayment.Exported = False Then
                                    If Not bMultiFiles Then
                                        bExportoBatch = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment


                        If bExportoBatch Then
                            ' for each oBatch, write a fileheader (HH)
                            sLine = WriteHB_Bankavstemming_FileHeader(oBatch)
                            '--------------------------------------------------
                            oFile.WriteLine((sLine))

                            ' for each batch, write a balancerecord
                            sLine = WriteHB_Bankavstemming_BatchHeader(oBatch)
                            '----------------------------------------------------------------------------
                            oFile.WriteLine((sLine))

                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    If oPayment.Cancel = False And oPayment.Exported = False Then
                                        If Not bMultiFiles Then
                                            bExportoPayment = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If 'If oPayment.Cancel = False Then
                                End If

                                If bExportoPayment Then
                                    i = 0
                                    For Each oInvoice In oPayment.Invoices
                                        i = i + 1
                                        sLine = WriteHB_Bankavstemming_TransactionRecord(oPayment, oInvoice)
                                        '-------------------------------------------------------------------
                                        oFile.WriteLine((sLine))
                                    Next oInvoice
                                    oPayment.Exported = True
                                End If

                            Next oPayment ' payment
                        End If

                    Next oBatch 'batch
                End If
            Next oBabel 'Babelfile

        Catch ex As Exception

            '26.06.2019 - F�r overgang til ny feilh�ndtering ble ikke ny feilmelding kastet
            Throw New vbBabel.Payment.PaymentException("Function: WriteHandelsbanken_Bankavst" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteHandelsbanken_Bankavst = True

    End Function
	Function WriteHB_Bankavstemming_FileHeader(ByRef oBatch As vbBabel.Batch) As String
		Dim sLine As String
		
		sLine = "HH" ' 1. Recordtype HH,         1-2
		sLine = sLine & oBatch.DATE_Production ' 2. Rundate, YYYYMMDD      3-10
		
		WriteHB_Bankavstemming_FileHeader = sLine
		
	End Function
	Function WriteHB_Bankavstemming_BatchHeader(ByRef oBatch As Batch) As String
		' Saldorecord
		' -----------
		Dim sLine As String
		Dim sDate As String
		Dim oPayment As Payment
		Dim sTmp As String
		
		' Different formats for domestic and international saldorecords
		If EmptyString((oBatch.Version)) Then
			' find currencycode from payment (accounttransactions)
			If oBatch.Payments.Count > 0 Then
				If oBatch.Payments.Item(1).MON_AccountCurrency = "NOK" Then
					sTmp = "Domestic"
				ElseIf Not EmptyString(oBatch.Payments.Item(1).MON_AccountCurrency) And oBatch.Payments.Item(1).MON_AccountCurrency <> "NOK" Then 
					sTmp = "International"
				ElseIf oBatch.Payments.Item(1).MON_InvoiceCurrency = "NOK" Then 
					sTmp = "Domestic"
				Else
					sTmp = "International"
				End If
			Else
				' Not enough info, but defaults to domestic
				sTmp = "Domestic"
			End If
		Else
			' set in import
			If oBatch.Version = "International" Then
				sTmp = "International"
			Else
				sTmp = "Domestic"
			End If
		End If
		
		
		If sTmp = "Domestic" Then
			sLine = "S-" ' 1. Recordtype        1-2
		Else
			sLine = "VS" ' 1. Recordtype        1-2
		End If
		' find account from payment
		If oBatch.Payments.Count > 0 Then
			If Not EmptyString(oBatch.Payments.Item(1).I_Account) Then
				sLine = sLine & PadRight(oBatch.Payments.Item(1).I_Account, 11, " ") '2. Accountno 3-13
			Else
				sLine = sLine & PadRight(oBatch.I_Branch, 11, " ") ' 2. Accountno 3-13
			End If
		Else
			' no payments
			sLine = sLine & PadRight(oBatch.I_Branch, 11, " ") ' 2. Accountno 3-13
		End If
		
		'24.06.2010 - Added ABS() around all amounts to write, because the sign is in a seperate field
		If sTmp = "Domestic" Then
			' Domestic
			' --------
			If oBatch.MON_BalanceOUT < 0 Then
				sLine = sLine & "-" ' 3. Sign,          14-14
			Else
				sLine = sLine & "+" ' 3. Sign,          14-14
			End If
			sLine = sLine & PadLeft(CStr(System.Math.Abs(oBatch.MON_BalanceOUT)), 13, " ") ' 4. Saldo          15-27
			If oBatch.MON_BalanceOUTInterest < 0 Then
				sLine = sLine & "-" ' 5. Sign,          28-28
			Else
				sLine = sLine & "+" ' 5. Sign,          28-28
			End If
			sLine = sLine & PadLeft(CStr(System.Math.Abs(oBatch.MON_BalanceOUTInterest)), 13, " ") ' 6. Disponibel saldo 29-41
		Else
			' International
			' -------------
			If oBatch.MON_BalanceOUTNOK < 0 Then
				sLine = sLine & "-" ' 3. Sign,          14-14
			Else
				sLine = sLine & "+" ' 3. Sign,          14-14
			End If
			sLine = sLine & PadLeft(CStr(System.Math.Abs(oBatch.MON_BalanceOUTNOK)), 13, " ") ' 4. Saldo          15-27
			If oBatch.MON_BalanceOUTInterestNOK < 0 Then
				sLine = sLine & "-" ' 5. Sign,          28-28
			Else
				sLine = sLine & "+" ' 5. Sign,          28-28
			End If
			sLine = sLine & PadLeft(CStr(System.Math.Abs(oBatch.MON_BalanceOUTInterestNOK)), 13, " ") ' 6. Disponibel saldo 29-41
			' find accountcurrency
			If Len(oBatch.REF_Bank) = CDbl("3") Then
				sLine = sLine & oBatch.REF_Bank ' 7. Currencycode 42-44
			Else
				' find from payments
				If oBatch.Payments.Count > 1 Then
					sLine = sLine & oBatch.Payments.Item(1).MON_InvoiceCurrency ' 7. Currencycode 42-44
				Else
					sLine = sLine & "   "
				End If
			End If
			sLine = sLine & "+" & PadLeft(CStr(oBatch.CurrencyRate), 9, " ") ' 8. Currencyrate, last 4 are decimals 46-54
			If oBatch.MON_BalanceOUT < 0 Then
				sLine = sLine & "-" ' 9. Sign,          55-55
			Else
				sLine = sLine & "+" ' 9. Sign,          55-55
			End If
			sLine = sLine & PadLeft(CStr(System.Math.Abs(oBatch.MON_BalanceOUT)), 13, " ") ' 10. Saldo          56-68
			If oBatch.MON_BalanceOUTInterest < 0 Then
				sLine = sLine & "-" ' 11. Sign,          69-69
			Else
				sLine = sLine & "+" ' 11. Sign,          69-69
			End If
			sLine = sLine & PadLeft(CStr(System.Math.Abs(oBatch.MON_BalanceOUTInterest)), 13, " ") ' 12. Disponibel saldo 70-82
		End If
		
		
		WriteHB_Bankavstemming_BatchHeader = sLine
		
	End Function
	Function WriteHB_Bankavstemming_TransactionRecord(ByRef oPayment As Payment, ByRef oInvoice As Invoice) As String
		Dim sLine As String
		Dim sPayCode As String
		
		If oPayment.MON_InvoiceCurrency = "NOK" Then
			' assume NOK-account
			sLine = "T-" ' 1. Recordtype 1-2
		Else
			sLine = "VT" ' 1. Recordtype 1-2
		End If
		sLine = sLine & oPayment.I_Account ' 2. Account 3-13
		sLine = sLine & oPayment.DATE_Payment ' 3. Bookdate 14-21
		sLine = sLine & oPayment.DATE_Value ' 4. Valuedate 22-29
		sLine = sLine & PadRight(oPayment.Text_I_Statement, 9, " ") ' 5. Text 30-38
		sLine = sLine & PadRight(oPayment.REF_Bank1, 11, " ") ' 6. Numeric ref 39-49
		sLine = sLine & PadRight(oPayment.REF_Bank2, 11, " ") ' 7. Archiveref 50-60
		
		If oPayment.MON_InvoiceCurrency = "NOK" Then
			' Domestic, NOK account
			If oInvoice.MON_AccountAmount <> 0 Then
				sLine = sLine & IIf(oInvoice.MON_AccountAmount < 0, "-", "+") ' sign + / -
				sLine = sLine & PadLeft(CStr(System.Math.Abs(oInvoice.MON_AccountAmount)), 13, " ") ' 8. Amount 62-74
			Else
				sLine = sLine & IIf(oInvoice.MON_InvoiceAmount < 0, "-", "+") ' sign + / -
				sLine = sLine & PadLeft(CStr(System.Math.Abs(oInvoice.MON_InvoiceAmount)), 13, " ") ' 8. Amount 62-74
			End If
			sLine = sLine & PadRight(oPayment.PayCode, 3, " ") ' 9. Textcode 75-77
			sLine = sLine & PadRight(oInvoice.Freetexts.Item(1).Text, 19, " ") ' 10. text 78-96
		Else
			' currencyaccount
			If oInvoice.MON_LocalAmount <> 0 Then
				sLine = sLine & IIf(oInvoice.MON_LocalAmount < 0, "-", "+") ' sign + / -
				sLine = sLine & PadLeft(CStr(System.Math.Abs(oInvoice.MON_LocalAmount)), 13, " ") ' 8. Amount 62-74
			Else
				sLine = sLine & IIf(oInvoice.MON_InvoiceAmount < 0, "-", "+") ' sign + / -
				sLine = sLine & PadLeft(CStr(System.Math.Abs(oInvoice.MON_InvoiceAmount)), 13, " ") ' 8. Amount 62-74
			End If
			If Not EmptyString((oPayment.MON_AccountCurrency)) Then
				sLine = sLine & PadRight(oPayment.MON_AccountCurrency, 3, " ") ' 9. currencycode 75-77
			Else
				sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ") ' 9. currencycode 75-77
			End If
			sLine = sLine & "+" & PadLeft(CStr(oPayment.MON_LocalExchRate), 9, " ") '10. Currencyrate, last foru decimals, 78-86
			If oInvoice.MON_AccountAmount <> 0 Then
				sLine = sLine & IIf(oInvoice.MON_AccountAmount < 0, "-", "+") ' sign + / -
				sLine = sLine & PadLeft(CStr(System.Math.Abs(oInvoice.MON_AccountAmount)), 13, " ") ' 11. Amount 89-101
			Else
				sLine = sLine & IIf(oInvoice.MON_InvoiceAmount < 0, "-", "+") ' sign + / -
				sLine = sLine & PadLeft(CStr(System.Math.Abs(oInvoice.MON_InvoiceAmount)), 13, " ") ' 11. Amount 89-101
			End If
			sLine = sLine & PadRight(oInvoice.Freetexts.Item(1).Text, 19, " ") ' 10. text 102-120
		End If
		
		WriteHB_Bankavstemming_TransactionRecord = sLine
		
	End Function
End Module
