Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("Profile_NET.Profile")> Public Class Profile

    Private oFileSetups As vbBabel.FileSetups
    Private myBBDB_AccessConnection As System.Data.OleDb.OleDbConnection

    'local variable(s) to hold property value(s)
    Private iCompany_ID As Short
    Private sCompanyName, sCompanyAdr1 As String 'local copy
    Private sCompanyAdr2, sCompanyAdr3 As String
    Private sCompanyZip, sCompanyCity As String
    Private sCompanyNo, sAdditionalNo As String
    Private sDivision As String
    Private sCode As String
    Private bBackUp As Boolean
    Private sBackUpPath As String
    Private iDelDays As Short
    Private eStatus As vbBabel.Profile.CollectionStatus
    ' New 23.10.02 by Kjell
    Private sConUID, sConString, sConPWD As String
    ' New 13.11.02 by JanP
    Private sEmailDisplayName, sEmailSender, sEmailAutoSender As String
    Private sEmailAutoDisplayName, sEmailAutoReceiver As String
    Private sEmailReplyAdress, seMailSMTPHost As String
    Private bEmailSMTP As Boolean
    Private sERPPaymentID As String
    Private bManMatchFromERP As Boolean
    Private iMaxSizeDB As Integer  ' 17.11.2020 changed from Short
    Private bUseArchive As Boolean

    ' New 15.08.02 by JanP
    Private sCustom1Text, sCustom1Value As String
    Private sCustom2Text, sCustom2Value As String
    Private sCustom3Text, sCustom3Value As String
    Private sCustom4Text, sCustom4Value As String
    ' End new 15.08.02
    ' New 27.02.03
    Private sInvoiceDescText, sGeneralNoteText As String
    ' end new 27.02.03
    ' New 13.05.03 by JanP (DnB TBIW)
    Private sEmailCCSupport, sEmailSupport, sAccountsFile As String
    ' added 20.03.06 for testing at customer
    Private bRunTempCode As Boolean
    Private iDebugCode As Short
    Private bCreateBackup As Boolean
    Private bBckBeforeImport As Boolean
    Private bBckBeforeExport As Boolean
    Private bBckAfterExport As Boolean
    Private sExportBackuppath As String
    Private iExportBackupGenerations As Short
    Private bDisableAdminMenus As Boolean
    'Added 07.11.2008
    Private bLockBabelBank As Boolean

    ' added 16.08.2007
    Private sBaseCurrency As String
    ' XokNET 25.06.2014, added 8
    Private sStructuredOCR As String
    Private sStructuredInvoice As String
    Private sStructuredCreditNote As String
    Private iHistoryDelDays As Integer
    Private bTrimFreetext As Boolean
    Private bUseReturnConfirmation As Boolean
    Private bUseReturnRejection As Boolean
    Private bUseReturnPosting As Boolean
    ' added 11.03.2015
    Private bSEPASingle As Boolean
    Private bEndToEndRefFromBabelBank As Boolean
    Private bUnDoDoubleMatched As Boolean
    Private bUnDoAllDoubleMatched As Boolean
    Private sbbLanguage As String = ""  ' added 06.07.2017

    Public Enum CollectionStatus
        NoChange = 0
        SeqNoChange = 1
        ChangeUnder = 2
        Changed = 3
        NewCol = 4
        Deleted = 5
    End Enum

    '********* START PROPERTY SETTINGS ***********************
    Public Property FileSetups() As vbBabel.FileSetups
        Get
            If oFileSetups Is Nothing Then
                oFileSetups = New FileSetups
            End If


            FileSetups = oFileSetups
        End Get
        Set(ByVal Value As vbBabel.FileSetups)
            oFileSetups = Value
        End Set
    End Property
    Public Property Company_ID() As Short
        Get
            Company_ID = iCompany_ID
        End Get
        Set(ByVal Value As Short)
            iCompany_ID = Value
        End Set
    End Property
    Public Property CompanyName() As String
        Get
            CompanyName = sCompanyName
        End Get
        Set(ByVal Value As String)
            sCompanyName = Value
        End Set
    End Property
    Public Property CompanyAdr1() As String
        Get
            CompanyAdr1 = sCompanyAdr1
        End Get
        Set(ByVal Value As String)
            sCompanyAdr1 = Value
        End Set
    End Property
    Public Property CompanyAdr2() As String
        Get
            CompanyAdr2 = sCompanyAdr2
        End Get
        Set(ByVal Value As String)
            sCompanyAdr2 = Value
        End Set
    End Property
    Public Property CompanyAdr3() As String
        Get
            CompanyAdr3 = sCompanyAdr3
        End Get
        Set(ByVal Value As String)
            sCompanyAdr3 = Value
        End Set
    End Property
    Public Property CompanyZip() As String
        Get
            CompanyZip = sCompanyZip
        End Get
        Set(ByVal Value As String)
            sCompanyZip = Value
        End Set
    End Property
    Public Property CompanyCity() As String
        Get
            CompanyCity = sCompanyCity
        End Get
        Set(ByVal Value As String)
            sCompanyCity = Value
        End Set
    End Property
    Public Property CompanyNo() As String
        Get
            CompanyNo = sCompanyNo
        End Get
        Set(ByVal Value As String)
            sCompanyNo = Value
        End Set
    End Property
    Public Property Division() As String
        Get
            Division = sDivision
        End Get
        Set(ByVal Value As String)
            sDivision = Value
        End Set
    End Property
    Public Property AdditionalNo() As String
        Get
            AdditionalNo = sAdditionalNo
        End Get
        Set(ByVal Value As String)
            sAdditionalNo = Value
        End Set
    End Property
    Public Property Backup() As Boolean
        Get
            Backup = bBackUp
        End Get
        Set(ByVal Value As Boolean)
            bBackUp = Value
        End Set
    End Property
    Public Property BackupPath() As String
        Get
            BackupPath = sBackUpPath
        End Get
        Set(ByVal Value As String)
            sBackUpPath = Value
        End Set
    End Property
    Public Property DelDays() As Short
        Get
            DelDays = iDelDays
        End Get
        Set(ByVal Value As Short)
            iDelDays = Value
        End Set
    End Property

    Public Property Status() As vbBabel.Profile.CollectionStatus
        Get
            Status = eStatus
        End Get
        Set(ByVal Value As vbBabel.Profile.CollectionStatus)
            Dim oFilesetup As FileSetup
            Dim oClient As Client
            Dim oaccount As Account

            If CDbl(Value) = 0 Then
                eStatus = CollectionStatus.NoChange
            End If
            If Value > eStatus Then
                eStatus = Value
            End If
            If CDbl(Value) > 3 Then
                For Each oFilesetup In FileSetups
                    oFilesetup.Status = CollectionStatus.NewCol
                    For Each oClient In oFilesetup.Clients
                        oClient.Status = CollectionStatus.NewCol
                        For Each oaccount In oClient.Accounts
                            oaccount.Status = CollectionStatus.NewCol
                        Next oaccount
                    Next oClient
                Next oFilesetup
            End If

        End Set
    End Property
    Public Property Custom1Text() As String
        Get
            Custom1Text = sCustom1Text
        End Get
        Set(ByVal Value As String)
            sCustom1Text = Value
        End Set
    End Property
    Public Property Custom1Value() As String
        Get
            Custom1Value = sCustom1Value
        End Get
        Set(ByVal Value As String)
            sCustom1Value = Value
        End Set
    End Property
    Public Property InvoiceDescText() As String
        Get
            InvoiceDescText = sInvoiceDescText
        End Get
        Set(ByVal Value As String)
            sInvoiceDescText = Value
        End Set
    End Property
    Public Property GeneralNoteText() As String
        Get
            GeneralNoteText = sGeneralNoteText
        End Get
        Set(ByVal Value As String)
            sGeneralNoteText = Value
        End Set
    End Property
    Public Property ConString() As String
        Get
            ConString = sConString
        End Get
        Set(ByVal Value As String)
            sConString = Value
        End Set
    End Property
    Public Property ConUID() As String
        Get
            ConUID = sConUID
        End Get
        Set(ByVal Value As String)
            sConUID = Value
        End Set
    End Property
    Public Property ConPWD() As Object
        Get
            ConPWD = pwDeCrypt(CObj(sConPWD))
        End Get
        Set(ByVal Value As Object)
            sConPWD = pwCrypt(Trim(CStr(Value)))
        End Set
    End Property
    Public Property Custom2Text() As String
        Get
            Custom2Text = sCustom2Text
        End Get
        Set(ByVal Value As String)
            sCustom2Text = Value
        End Set
    End Property
    Public Property Custom2Value() As String
        Get
            Custom2Value = sCustom2Value
        End Get
        Set(ByVal Value As String)
            sCustom2Value = Value
        End Set
    End Property
    Public Property Custom3Text() As String
        Get
            Custom3Text = sCustom3Text
        End Get
        Set(ByVal Value As String)
            sCustom3Text = Value
        End Set
    End Property
    Public Property Custom4Text() As String
        Get
            Custom4Text = sCustom4Text
        End Get
        Set(ByVal Value As String)
            sCustom4Text = Value
        End Set
    End Property
    Public Property Custom3Value() As String
        Get
            Custom3Value = sCustom3Value
        End Get
        Set(ByVal Value As String)
            sCustom3Value = Value
        End Set
    End Property
    Public Property Custom4Value() As String
        Get
            Custom4Value = sCustom4Value
        End Get
        Set(ByVal Value As String)
            sCustom4Value = Value
        End Set
    End Property
    Public Property EmailSender() As String
        Get
            EmailSender = sEmailSender
        End Get
        Set(ByVal Value As String)
            sEmailSender = Value
        End Set
    End Property
    Public Property EmailDisplayName() As String
        Get
            EmailDisplayName = sEmailDisplayName
        End Get
        Set(ByVal Value As String)
            sEmailDisplayName = Value
        End Set
    End Property
    Public Property EmailAutoSender() As String
        Get
            EmailAutoSender = sEmailAutoSender
        End Get
        Set(ByVal Value As String)
            sEmailAutoSender = Value
        End Set
    End Property
    Public Property EmailAutoDisplayName() As String
        Get
            EmailAutoDisplayName = sEmailAutoDisplayName
        End Get
        Set(ByVal Value As String)
            sEmailAutoDisplayName = Value
        End Set
    End Property
    Public Property EmailAutoReceiver() As String
        Get
            EmailAutoReceiver = sEmailAutoReceiver
        End Get
        Set(ByVal Value As String)
            sEmailAutoReceiver = Value
        End Set
    End Property
    Public Property EmailReplyAdress() As String
        Get
            EmailReplyAdress = sEmailReplyAdress
        End Get
        Set(ByVal Value As String)
            sEmailReplyAdress = Value
        End Set
    End Property
    Public Property EmailSMTPHost() As String
        Get
            EmailSMTPHost = seMailSMTPHost
        End Get
        Set(ByVal Value As String)
            seMailSMTPHost = Value
        End Set
    End Property
    Public Property EmailSMTP() As Boolean
        Get
            EmailSMTP = bEmailSMTP
        End Get
        Set(ByVal Value As Boolean)
            bEmailSMTP = Value
        End Set
    End Property
    Public Property EmailSupport() As String
        Get
            EmailSupport = sEmailSupport
        End Get
        Set(ByVal Value As String)
            sEmailSupport = Value
        End Set
    End Property
    Public Property BaseCurrency() As String
        Get
            BaseCurrency = sBaseCurrency
        End Get
        Set(ByVal Value As String)
            sBaseCurrency = Value
        End Set
    End Property
    ' XokNET 25.06.2014 - added next 8
    Public Property StructuredOCR() As String
        Get
            StructuredOCR = sStructuredOCR
        End Get
        Set(ByVal value As String)
            sStructuredOCR = value
        End Set
    End Property
    Public Property StructuredInvoice() As String
        Get
            StructuredInvoice = sStructuredInvoice
        End Get
        Set(ByVal value As String)
            sStructuredInvoice = value
        End Set
    End Property
    Public Property StructuredCreditNote() As String
        Get
            StructuredCreditNote = sStructuredCreditNote
        End Get
        Set(ByVal value As String)
            sStructuredCreditNote = value
        End Set
    End Property
    Public Property HistoryDelDays() As Integer
        Get
            HistoryDelDays = iHistoryDelDays
        End Get
        Set(ByVal value As Integer)
            iHistoryDelDays = value
        End Set
    End Property
    Public Property TrimFreetext() As Boolean
        Get
            TrimFreetext = bTrimFreetext
        End Get
        Set(ByVal value As Boolean)
            bTrimFreetext = value
        End Set
    End Property
    Public Property UseReturnConfirmation() As Boolean
        Get
            UseReturnConfirmation = bUseReturnConfirmation
        End Get
        Set(ByVal value As Boolean)
            bUseReturnConfirmation = value
        End Set
    End Property
    Public Property UseReturnrejection() As Boolean
        Get
            UseReturnrejection = bUseReturnRejection
        End Get
        Set(ByVal value As Boolean)
            bUseReturnRejection = value
        End Set
    End Property
    Public Property UseReturnPosting() As Boolean
        Get
            UseReturnPosting = bUseReturnPosting
        End Get
        Set(ByVal value As Boolean)
            bUseReturnPosting = value
        End Set
    End Property
    Public Property SEPASingle() As Boolean
        Get
            SEPASingle = bSEPASingle
        End Get
        Set(ByVal value As Boolean)
            bSEPASingle = value
        End Set
    End Property
    Public Property EndToEndRefFromBabelBank() As Boolean
        Get
            EndToEndRefFromBabelBank = bEndToEndRefFromBabelBank
        End Get
        Set(ByVal value As Boolean)
            bEndToEndRefFromBabelBank = value
        End Set
    End Property
    Public Property UnDoDoubleMatched() As Boolean
        Get
            UnDoDoubleMatched = bUnDoDoubleMatched
        End Get
        Set(ByVal value As Boolean)
            bUnDoDoubleMatched = value
        End Set
    End Property
    Public Property UnDoAllDoubleMatched() As Boolean
        Get
            UnDoAllDoubleMatched = bUnDoAllDoubleMatched
        End Get
        Set(ByVal value As Boolean)
            bUnDoAllDoubleMatched = value
        End Set
    End Property
    Public Property EmailCCSupport() As String
        Get
            EmailCCSupport = sEmailCCSupport
        End Get
        Set(ByVal Value As String)
            sEmailCCSupport = Value
        End Set
    End Property
    Public Property ERPPaymentID() As String
        Get
            ERPPaymentID = sERPPaymentID
        End Get
        Set(ByVal Value As String)
            sERPPaymentID = Value
        End Set
    End Property
    Public Property ManMatchFromERP() As Boolean
        Get
            ManMatchFromERP = bManMatchFromERP
        End Get
        Set(ByVal Value As Boolean)
            bManMatchFromERP = Value
        End Set
    End Property
    Public Property AccountsFile() As String
        Get
            AccountsFile = sAccountsFile
        End Get
        Set(ByVal Value As String)
            sAccountsFile = Value
        End Set
    End Property
    Public Property RunTempCode() As Boolean
        Get
            RunTempCode = bRunTempCode
        End Get
        Set(ByVal Value As Boolean)
            bRunTempCode = Value
        End Set
    End Property
    Public Property DebugCode() As Short
        Get
            DebugCode = iDebugCode
        End Get
        Set(ByVal Value As Short)
            iDebugCode = Value
        End Set
    End Property
    Public Property MaxSizeDB() As Integer
        ' 17.11.2020 changed from Short to Integer
        Get
            MaxSizeDB = iMaxSizeDB
        End Get
        Set(ByVal Value As Integer)
            iMaxSizeDB = Value
        End Set
    End Property
    Public Property UseArchive() As Boolean
        Get
            UseArchive = bUseArchive
        End Get
        Set(ByVal Value As Boolean)
            bUseArchive = Value
        End Set
    End Property
    Public Property CreateBackup() As Boolean
        Get
            CreateBackup = bCreateBackup
        End Get
        Set(ByVal Value As Boolean)
            bCreateBackup = Value
        End Set
    End Property
    Public Property BckBeforeImport() As Boolean
        Get
            BckBeforeImport = bBckBeforeImport
        End Get
        Set(ByVal Value As Boolean)
            bBckBeforeImport = Value
        End Set
    End Property
    Public Property BckBeforeExport() As Boolean
        Get
            BckBeforeExport = bBckBeforeExport
        End Get
        Set(ByVal Value As Boolean)
            bBckBeforeExport = Value
        End Set
    End Property
    Public Property BckAfterExport() As Boolean
        Get
            BckAfterExport = bBckAfterExport
        End Get
        Set(ByVal Value As Boolean)
            bBckAfterExport = Value
        End Set
    End Property
    Public Property ExportBackuppath() As String
        Get
            ExportBackuppath = sExportBackuppath
        End Get
        Set(ByVal Value As String)
            sExportBackuppath = Value
        End Set
    End Property
    Public Property DisableAdminMenus() As Boolean
        Get
            DisableAdminMenus = bDisableAdminMenus
        End Get
        Set(ByVal Value As Boolean)
            bDisableAdminMenus = Value
        End Set
    End Property
    Public Property LockBabelBank() As Boolean
        Get
            LockBabelBank = bLockBabelBank
        End Get
        Set(ByVal Value As Boolean)
            bLockBabelBank = Value
        End Set
    End Property
    Public Property ExportBackupGenerations() As Short
        Get
            ExportBackupGenerations = iExportBackupGenerations
        End Get
        Set(ByVal Value As Short)
            iExportBackupGenerations = Value
        End Set
    End Property
    Public Property bbLanguage() As String
        Get
            bbLanguage = sbbLanguage
        End Get
        Set(ByVal value As String)
            sbbLanguage = value
        End Set
    End Property

    '********* END PROPERTY SETTINGS ***********************

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        sCompanyName = ""
        sCompanyAdr1 = ""
        sCompanyAdr2 = ""
        sCompanyZip = ""
        sCompanyCity = ""
        sCompanyNo = ""
        sDivision = ""
        sAdditionalNo = ""
        sCode = "zzxinit"
        bBackUp = False
        sBackUpPath = ""
        iDelDays = 30
        eStatus = 0
        ' New 13.11.02 by JanP
        sEmailSender = ""
        sEmailDisplayName = ""
        sEmailAutoSender = ""
        sEmailAutoDisplayName = ""
        sEmailAutoReceiver = ""
        sEmailReplyAdress = ""
        seMailSMTPHost = ""
        bEmailSMTP = False

        ' New 15.08.02 by JanP
        sCustom1Text = ""
        sCustom1Value = ""
        sCustom2Text = ""
        sCustom2Value = ""
        sCustom3Text = ""
        sCustom3Value = ""
        sCustom4Text = ""
        sCustom4Value = ""
        ' End new 15.08.02
        sInvoiceDescText = ""
        sGeneralNoteText = ""
        ' New 25.10.02 JanP
        sConString = ""
        sConUID = ""
        sConPWD = ""
        ' New 13.05.02 by JanP
        sEmailSupport = ""
        sEmailCCSupport = ""
        sAccountsFile = ""

        sERPPaymentID = ""
        sBaseCurrency = ""
        ' XokNET 25.06.2014 - added next 8
        sStructuredOCR = ""
        sStructuredInvoice = ""
        sStructuredCreditNote = ""
        iHistoryDelDays = 50
        bTrimFreetext = False
        bUseReturnConfirmation = False
        bUseReturnRejection = False
        bUseReturnPosting = False
        bSEPASingle = False
        bEndToEndRefFromBabelBank = False
        bUnDoDoubleMatched = False
        bUnDoAllDoubleMatched = False

        bManMatchFromERP = False

        bCreateBackup = False
        bBckBeforeImport = False
        bBckBeforeExport = False
        bBckAfterExport = False
        sExportBackuppath = ""
        iExportBackupGenerations = 1
        bDisableAdminMenus = False
        bLockBabelBank = False

        bRunTempCode = False
        iDebugCode = 0
        iMaxSizeDB = 15
        bUseArchive = False
        sbbLanguage = ""

        ' When vbbabel.dll is called, the code attempts
        ' to find a local satellite DLL that will contain all the
        ' resources.
        'If Not (LoadLocalizedResources("vbbabel")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL")
        'End If

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub

    Private Sub Class_Terminate_Renamed()
        oFileSetups = Nothing
    End Sub
    Public Function CloseConnection()
        If Not myBBDB_AccessConnection Is Nothing Then
            If myBBDB_AccessConnection.State <> ConnectionState.Closed Then
                'Debug.Print("Lukker Connection!")
                myBBDB_AccessConnection.Close()
            End If
            myBBDB_AccessConnection.Dispose()
            myBBDB_AccessConnection = Nothing
        End If

    End Function
    Protected Overrides Sub Finalize()
        Try
            'If Not RunTime() Then
            '    ' TESTING - logg to file whenever we open Company;
            '    Dim sCallstack As String
            '    Dim oFs As Scripting.FileSystemObject
            '    Dim oTempFile As Scripting.TextStream
            '    Dim sLog As String
            '    oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
            '    sLog = "c:\projects.net\CompanyLogg.dat"

            '    sCallstack = Environment.StackTrace
            '    oTempFile = oFs.OpenTextFile(sLog, Scripting.IOMode.ForAppending, True)
            '    ' Write to logfile
            '    oTempFile.Write(Now.ToString & "Company.Finalize" & vbCrLf & sCallstack & vbCrLf)
            '    oTempFile.Close()
            '    oTempFile = Nothing
            '    oFs = Nothing
            'End If

            If Not myBBDB_AccessConnection Is Nothing Then
                If myBBDB_AccessConnection.State <> ConnectionState.Closed Then
                    'Debug.Print("Lukker Connection!")
                    ' TODO B�R VI TA VEKK DENNE; OG SATSE P� AT = Nothing under rydder riktig?
                    'myBBDB_AccessConnection.Close()
                End If
                ' TODO B�R VI TA VEKK DENNE; OG SATSE P� AT = Nothing under rydder riktig?
                'myBBDB_AccessConnection.Dispose()
                myBBDB_AccessConnection = Nothing
            End If

            Class_Terminate_Renamed()
            MyBase.Finalize()
        Catch ex As Exception
            ' continue from here, in a decent manner
            'myBBDB_AccessConnection = Nothing
            ' Do nothing ?
            If Not RunTime() Then
                ' TESTING - logg to file whenever we open Company;
                Dim sCallstack As String
                Dim oFs As Scripting.FileSystemObject
                Dim oTempFile As Scripting.TextStream
                Dim sLog As String
                oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
                sLog = "c:\projects.net\CompanyLogg.dat"

                sCallstack = Environment.StackTrace
                oTempFile = oFs.OpenTextFile(sLog, Scripting.IOMode.ForAppending, True)
                ' Write to logfile
                oTempFile.Write(Now.ToString & " Company.Finalize ERROR" & vbCrLf & sCallstack & vbCrLf & ex.Message & vbCrLf)
                oTempFile.Close()
                oTempFile = Nothing
                oFs = Nothing
            End If
        End Try


    End Sub

    Public Function Load(ByRef iCompanyNo As Short, Optional ByVal bDebug As Boolean = False) As Boolean

        Dim sMyFile As String
        'Dim cnProfile As New ADODB.Connection
        Dim oFilesetup As FileSetup
        Dim bStartLoad As Boolean
        Dim bFileSetup As Boolean
        Dim sMySQL As String
        Dim sSelectPart As String = ""
        Dim myBBDB_AccessFileSetupReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessFileSetupCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim bSQLServer As Boolean = False 'SQQLSERVER
        Dim sTmpConfig As String = ""
        Dim sERPConnectionString As String = ""

        Try

            iCompany_ID = iCompanyNo
            If bDebug Then
                MsgBox("Inne i oProfile.Load", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "DEBUG")
            End If

            oFileSetups = Nothing ' Clear all

            sMyFile = BB_DatabasePath()
            If bDebug Then
                MsgBox("BB_Databasepath = " & sMyFile, MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "DEBUG")
            End If

            'Set cnProfile = New ADODB.Connection
            If sMyFile <> "" Or BB_UseSQLServer() Then '30.11.2016 Added - or BB_UseSQLServer()


                'If Not RunTime() Then
                '    ' TESTING - logg to file whenever we open Company;
                '    Dim sCallstack As String
                '    Dim oFs As Scripting.FileSystemObject
                '    Dim oTempFile As Scripting.TextStream
                '    Dim sLog As String
                '    oFs = CreateObject ("Scripting.FileSystemObject")
                '    sLog = "c:\projects.net\CompanyLogg.dat"

                '    sCallstack = Environment.StackTrace.Substring(0, 500)
                '    oTempFile = oFs.OpenTextFile(sLog, Scripting.IOMode.ForAppending, True)
                '    ' Write to logfile
                '    oTempFile.Write(Now.ToString & "Company.Load" & vbCrLf & sCallstack & vbCrLf)
                '    oTempFile.Close()
                '    oTempFile = Nothing
                '    oFs = Nothing
                'End If
                bSQLServer = BB_UseSQLServer()
                sERPConnectionString = FindBBConnectionString()

                myBBDB_AccessConnection = New System.Data.OleDb.OleDbConnection(sERPConnectionString)
                'OLD - Before SQQLSERVER
                'myBBDB_AccessConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sMyFile)

                If bDebug Then
                    MsgBox("F�r myBBDB_AccessConnection.Open()" & sMyFile, MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "DEBUG")
                End If
                myBBDB_AccessConnection.Open()
                If bDebug Then
                    MsgBox("Etter myBBDB_AccessConnection.Open()" & sMyFile, MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "DEBUG")
                End If
                myBBDB_AccessFileSetupCommand = myBBDB_AccessConnection.CreateCommand()
                myBBDB_AccessFileSetupCommand.CommandType = CommandType.Text
                myBBDB_AccessFileSetupCommand.Connection = myBBDB_AccessConnection
                If bDebug Then
                    MsgBox("F�r StartLoad" & sMyFile, MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "DEBUG")
                End If
                bStartLoad = StartLoad(iCompanyNo, myBBDB_AccessFileSetupCommand, myBBDB_AccessFileSetupReader)

                If bStartLoad Then
                    If bDebug Then
                        MsgBox("bStartLoad=True", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "DEBUG")
                    End If

                    sSelectPart = "SELECT FileSetup_ID, Format_ID, EnumID, F.Bank_ID, ShortName, Description, Silent, Log, LogType, LogFilename, "
                    sSelectPart = sSelectPart & "LogOverwrite, LogShowStartStop, LogMaxLines, LogDetails, FileSetupOut, FileNameIn1, "
                    sSelectPart = sSelectPart & "FileNameIn2, FileNameIn3, BackupIn, Manual, OverWriteIn, WarningIn, FileNameOut1, "
                    sSelectPart = sSelectPart & "FileNameOut2, FileNameOut3, BackupOut, OverWriteOut, WarningOut, RemoveOldOut, "
                    sSelectPart = sSelectPart & "NotSplitOnAccount, SeqnoType, SeqNoTotal, SeqNoDay, Division, CompanyNo, AdditionalNo, "
                    sSelectPart = sSelectPart & "FromAccountingSys, InfoNewClient, KeepBatch, CtrlNegative, CtrlZip, CtrlAccount, "
                    sSelectPart = sSelectPart & "CtrlKID, ChangeDateDue, ChangeDateAll, RejectWrong, RedoWrong, Accumulation, "
                    sSelectPart = sSelectPart & "ClientToScreen, ClientToPrint, ClientToFile, ClientToMail, ReportToPrint1, ReportToPrint2, "
                    sSelectPart = sSelectPart & "ReportToPrint3, ReportToScreen1, ReportToScreen2, ReportToScreen3, Message, Version, "
                    sSelectPart = sSelectPart & "EDIFormat, ReturnMass, PreJCL, PostJCL, TreatSpecial, AutoMatch, MatchOCR, "
                    sSelectPart = sSelectPart & "ShowCorrections, PaymentType, Crypt, SplitAfter450, FilenameOut4, FormatOutID1, "
                    sSelectPart = sSelectPart & "FormatOutID2, FormatOutID3, FormatOutID4, MATCHOutGL, MATCHOutOCR, MATCHOutAutogiro, "
                    sSelectPart = sSelectPart & "MATCHOutRest, DelDays, ReportAllPayments, MergePayments, NotificationMail, NotificationPrint,"
                    sSelectPart = sSelectPart & "ChargeMeDomestic, ChargeMeAbroad, ChargesFromSetup, CodeNB, ExplanationNB, StructText, "
                    sSelectPart = sSelectPart & "StructInvoicePos, AddDays, MappingFileName, StructTextNegative, StructInvoicePosNegative, "
                    sSelectPart = sSelectPart & "SplitOCRandFBO, ExtractInfofromKID, BackupPath, UseSignalFile, FilenameSignalFile, "
                    sSelectPart = sSelectPart & "ValidateFormatAtImport, Sorting, SpecialMarker, B.Enum_ID, CurrencyToUse, Division2, TelepayDebitNO, StandardPurposeCode, SplitFiles, ISO20022 "
                    ' 05.01.2017 Security
                    sSelectPart = sSelectPart & ",secFilenamePublicKey, secHash, secCypher, secEncrypt, secSign, secSecureEnvelope, secFilenameBankPublicKey, secCompressionType "
                    sSelectPart = sSelectPart & ",secPublicKey, secPrivateKey, secKeyCreationDate, secUserID, secPassword, secBANKPublicKey, FileEncoding, secConfidential "
                    '16.05.2022 - FilenameSignalFileError
                    sSelectPart = sSelectPart & ", FilenameSignalFileError "
                    sMySQL = sSelectPart & "FROM FileSetup F, FileSetup2 I, Bank B WHERE F.Company_ID = " & iCompanyNo.ToString & " AND F.Company_ID = I.Company_ID AND F.FileSetup_ID = I.FileSetup_ID2 AND F.Bank_ID = B.Bank_ID ORDER BY FileSetup_ID ASC"

                    If Not myBBDB_AccessFileSetupReader.IsClosed Then
                        myBBDB_AccessFileSetupReader.Close()
                    End If

                    myBBDB_AccessFileSetupCommand.CommandText = sMySQL

                    myBBDB_AccessFileSetupReader = myBBDB_AccessFileSetupCommand.ExecuteReader

                    oFileSetups = New FileSetups
                    If myBBDB_AccessFileSetupReader.HasRows Then
                        Do While myBBDB_AccessFileSetupReader.Read()
                            'Make a FormatUsed-object
                            oFilesetup = oFileSetups.Add(myBBDB_AccessFileSetupReader.GetInt32(0).ToString) 'FileSetup_ID
                            bFileSetup = oFilesetup.Load(iCompanyNo, myBBDB_AccessFileSetupReader, myBBDB_AccessConnection)
                        Loop
                    End If

                End If

                If Not myBBDB_AccessFileSetupReader Is Nothing Then
                    If Not myBBDB_AccessFileSetupReader.IsClosed Then
                        myBBDB_AccessFileSetupReader.Close()
                    End If
                    myBBDB_AccessFileSetupReader = Nothing
                End If
                If Not myBBDB_AccessFileSetupCommand Is Nothing Then
                    myBBDB_AccessFileSetupCommand.Dispose()
                    myBBDB_AccessFileSetupCommand = Nothing
                End If
                If Not myBBDB_AccessConnection Is Nothing Then
                    If myBBDB_AccessConnection.State <> ConnectionState.Closed Then
                        myBBDB_AccessConnection.Close()
                    End If
                    myBBDB_AccessConnection.Dispose()
                    myBBDB_AccessConnection = Nothing
                End If

            End If

        Catch ex As Exception

            If Not EmptyString(sMySQL) Then
                Err.Raise(Err.Number, "LoadProfile", Err.Description & vbCrLf & "SQL: " & sMySQL)
            Else
                Err.Raise(Err.Number, "LoadProfile", Err.Description)
            End If


        End Try


    End Function
    Private Function StartLoad(ByRef iCompanyNo As Short, ByRef myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand, ByRef myBBDB_AccessReader As System.Data.OleDb.OleDbDataReader) As Boolean
        Dim sMySQL As String
        Dim bStartLoad As Boolean
        Dim sSelectPart As String

        bStartLoad = False

        sSelectPart = "SELECT Name, Adr1, Adr2, Adr3, Zip, City, CompanyNo, AdditionalNo, [Backup], BackupPath, code, Custom1Text, "
        sSelectPart = sSelectPart & "Custom1Value, Custom2Text, Custom2Value, Custom3Text, Custom3Value, Custom4Text, Custom4Value, "
        sSelectPart = sSelectPart & "InvoiceDescText, GeneralNoteText, ConString, ConUID, ConPWD, EmailSender, EmailDisplayName, "
        sSelectPart = sSelectPart & "EmailAutoSender, EmailAutoDisplayName, EmailAutoReceiver, EmailReplyAdress, EmailSMTPHost, "
        sSelectPart = sSelectPart & "EmailSMTP, EmailSupport, BaseCurrency, EmailCCSupport, AccountsFile, ERPPaymentID, ArchiveUseArchive, "
        sSelectPart = sSelectPart & "ManMatchFromERP, DelDays, CreateBackup, BckBeforeImport, BckBeforeExport, BckAfterExport, ExportBackuppath, "
        sSelectPart = sSelectPart & "ExportBackupGenerations, DisableAdminMenus, LockBabelBank, RunTempCode, DebugCode, MaxSizeDB,"
        sSelectPart = sSelectPart & "StructuredOCR,StructuredInvoice, StructuredCreditNote, HistoryDelDays, TrimFreetext, UseReturnConfirmation, UseReturnRejection, UseReturnPosting, SEPASingle, EndToEndRefFromBabelBank, "
        sSelectPart = sSelectPart & "UnDoDoubleMatched, UnDoAllDoubleMatched, Division, bbLanguage "
        sMySQL = sSelectPart & " FROM Company where Company_ID = " & iCompanyNo.ToString

        myBBDB_AccessCommand.CommandText = sMySQL

        myBBDB_AccessReader = myBBDB_AccessCommand.ExecuteReader

        If myBBDB_AccessReader.HasRows Then
            Do While myBBDB_AccessReader.Read()
                If Not myBBDB_AccessReader.IsDBNull(0) Then
                    sCompanyName = Trim(myBBDB_AccessReader.GetString(0))
                Else
                    'FIX: Babelerror
                End If
                If Not myBBDB_AccessReader.IsDBNull(1) Then
                    sCompanyAdr1 = Trim(myBBDB_AccessReader.GetString(1))
                End If
                If Not myBBDB_AccessReader.IsDBNull(2) Then
                    sCompanyAdr2 = Trim(myBBDB_AccessReader.GetString(2))
                End If
                If Not myBBDB_AccessReader.IsDBNull(3) Then
                    sCompanyAdr3 = Trim(myBBDB_AccessReader.GetString(3))
                End If
                If Not myBBDB_AccessReader.IsDBNull(4) Then
                    sCompanyZip = myBBDB_AccessReader.GetString(4)
                End If
                If Not myBBDB_AccessReader.IsDBNull(5) Then
                    sCompanyCity = Trim(myBBDB_AccessReader.GetString(5))
                End If
                If Not myBBDB_AccessReader.IsDBNull(6) Then
                    sCompanyNo = myBBDB_AccessReader.GetString(6)
                End If
                If Not myBBDB_AccessReader.IsDBNull(7) Then
                    sAdditionalNo = myBBDB_AccessReader.GetString(7)
                End If
                If Not myBBDB_AccessReader.IsDBNull(8) Then
                    bBackUp = myBBDB_AccessReader.GetBoolean(8)
                End If
                If Not myBBDB_AccessReader.IsDBNull(9) Then
                    sBackUpPath = Trim(myBBDB_AccessReader.GetString(9))
                End If
                If Not myBBDB_AccessReader.IsDBNull(10) Then
                    sCode = myBBDB_AccessReader.GetString(10)
                End If
                ' New 15.08.02 by JanP
                If Not myBBDB_AccessReader.IsDBNull(11) Then
                    sCustom1Text = Trim(myBBDB_AccessReader.GetString(11))
                End If
                If Not myBBDB_AccessReader.IsDBNull(12) Then
                    sCustom1Value = myBBDB_AccessReader.GetString(12)
                End If
                If Not myBBDB_AccessReader.IsDBNull(13) Then
                    sCustom2Text = Trim(myBBDB_AccessReader.GetString(13))
                End If
                If Not myBBDB_AccessReader.IsDBNull(14) Then
                    sCustom2Value = myBBDB_AccessReader.GetString(14)
                End If
                If Not myBBDB_AccessReader.IsDBNull(15) Then
                    sCustom3Text = Trim(myBBDB_AccessReader.GetString(15))
                End If
                If Not myBBDB_AccessReader.IsDBNull(16) Then
                    sCustom3Value = myBBDB_AccessReader.GetString(16)
                End If
                ' end new 15.08.02
                ' new 21.04.06
                If Not myBBDB_AccessReader.IsDBNull(17) Then
                    sCustom4Text = Trim(myBBDB_AccessReader.GetString(17))
                End If
                If Not myBBDB_AccessReader.IsDBNull(18) Then
                    sCustom4Value = myBBDB_AccessReader.GetString(18)
                End If
                ' end new 21.04.06
                ' new 27.02.03
                If Not myBBDB_AccessReader.IsDBNull(19) Then
                    sInvoiceDescText = Trim(myBBDB_AccessReader.GetString(19))
                End If
                If Not myBBDB_AccessReader.IsDBNull(20) Then
                    sGeneralNoteText = Trim(myBBDB_AccessReader.GetString(20))
                End If
                ' end new 27.02.03
                If Not myBBDB_AccessReader.IsDBNull(21) Then
                    sConString = Trim(myBBDB_AccessReader.GetString(21))
                End If
                If Not myBBDB_AccessReader.IsDBNull(22) Then
                    sConUID = Trim(myBBDB_AccessReader.GetString(22))
                End If
                If Not myBBDB_AccessReader.IsDBNull(23) Then
                    sConPWD = Trim(myBBDB_AccessReader.GetString(23))
                End If
                ' new 13.11.02 by JanP
                If Not myBBDB_AccessReader.IsDBNull(24) Then
                    sEmailSender = Trim(myBBDB_AccessReader.GetString(24))
                End If
                If Not myBBDB_AccessReader.IsDBNull(25) Then
                    sEmailDisplayName = Trim(myBBDB_AccessReader.GetString(25))
                End If
                If Not myBBDB_AccessReader.IsDBNull(26) Then
                    sEmailAutoSender = Trim(myBBDB_AccessReader.GetString(26))
                End If
                If Not myBBDB_AccessReader.IsDBNull(27) Then
                    sEmailAutoDisplayName = Trim(myBBDB_AccessReader.GetString(27))
                End If
                If Not myBBDB_AccessReader.IsDBNull(28) Then
                    sEmailAutoReceiver = Trim(myBBDB_AccessReader.GetString(28))
                End If
                If Not myBBDB_AccessReader.IsDBNull(29) Then
                    sEmailReplyAdress = Trim(myBBDB_AccessReader.GetString(29))
                End If
                If Not myBBDB_AccessReader.IsDBNull(30) Then
                    seMailSMTPHost = Trim(myBBDB_AccessReader.GetString(30))
                End If
                If Not myBBDB_AccessReader.IsDBNull(31) Then
                    bEmailSMTP = myBBDB_AccessReader.GetBoolean(31)
                End If
                ' new 13.05.03
                If Not myBBDB_AccessReader.IsDBNull(32) Then
                    sEmailSupport = Trim(myBBDB_AccessReader.GetString(32))
                End If
                If Not myBBDB_AccessReader.IsDBNull(33) Then
                    sBaseCurrency = Trim(myBBDB_AccessReader.GetString(33))
                End If
                If Not myBBDB_AccessReader.IsDBNull(34) Then
                    sEmailCCSupport = Trim(myBBDB_AccessReader.GetString(34))
                End If
                If Not myBBDB_AccessReader.IsDBNull(35) Then
                    sAccountsFile = Trim(myBBDB_AccessReader.GetString(35))
                End If
                If Not myBBDB_AccessReader.IsDBNull(36) Then
                    sERPPaymentID = Trim(myBBDB_AccessReader.GetString(36))
                End If

                '''''If Not IsNull(rsCompany!ArchiveUseArchive) Then
                ' 26.06.2008 JanP
                ' Her l� feilen - ikke test IsNull p� bolske !!!
                If Not myBBDB_AccessReader.IsDBNull(37) Then
                    bUseArchive = myBBDB_AccessReader.GetBoolean(37)
                End If
                ''''End If
                If Not myBBDB_AccessReader.IsDBNull(38) Then
                    bManMatchFromERP = myBBDB_AccessReader.GetBoolean(38)
                End If
                If Not myBBDB_AccessReader.IsDBNull(39) Then 'Added 20.12.2017
                    iDelDays = myBBDB_AccessReader.GetInt32(39)
                End If
                If Not myBBDB_AccessReader.IsDBNull(40) Then
                    bCreateBackup = myBBDB_AccessReader.GetBoolean(40)
                End If
                If Not myBBDB_AccessReader.IsDBNull(41) Then
                    bBckBeforeImport = myBBDB_AccessReader.GetBoolean(41)
                End If
                If Not myBBDB_AccessReader.IsDBNull(42) Then
                    bBckBeforeExport = myBBDB_AccessReader.GetBoolean(42)
                End If
                If Not myBBDB_AccessReader.IsDBNull(43) Then
                    bBckAfterExport = myBBDB_AccessReader.GetBoolean(43)
                End If
                If Not myBBDB_AccessReader.IsDBNull(44) Then
                    sExportBackuppath = Trim(myBBDB_AccessReader.GetString(44))
                End If
                If Not myBBDB_AccessReader.IsDBNull(45) Then
                    iExportBackupGenerations = myBBDB_AccessReader.GetInt32(45)
                End If
                If Not myBBDB_AccessReader.IsDBNull(46) Then
                    bDisableAdminMenus = myBBDB_AccessReader.GetBoolean(46)
                End If
                If Not myBBDB_AccessReader.IsDBNull(47) Then
                    bLockBabelBank = myBBDB_AccessReader.GetBoolean(47)
                End If
                If Not myBBDB_AccessReader.IsDBNull(48) Then
                    bRunTempCode = myBBDB_AccessReader.GetBoolean(48)
                End If
                If Not myBBDB_AccessReader.IsDBNull(49) Then
                    iDebugCode = myBBDB_AccessReader.GetInt32(49)
                End If
                If Not myBBDB_AccessReader.IsDBNull(50) Then '20.12.2017 - Added IsDBNull-test
                    iMaxSizeDB = myBBDB_AccessReader.GetInt32(50)
                End If
                ' XokNET 25.06.2014 added 8
                ' Sjekk indexene p� nye felt !
                If Not myBBDB_AccessReader.IsDBNull(51) Then
                    sStructuredOCR = Trim(myBBDB_AccessReader.GetString(51))
                End If
                If Not myBBDB_AccessReader.IsDBNull(52) Then
                    sStructuredInvoice = Trim(myBBDB_AccessReader.GetString(52))
                End If
                If Not myBBDB_AccessReader.IsDBNull(53) Then
                    sStructuredCreditNote = Trim(myBBDB_AccessReader.GetString(53))
                End If
                If Not myBBDB_AccessReader.IsDBNull(54) Then
                    iHistoryDelDays = Trim(myBBDB_AccessReader.GetInt32(54))
                End If
                If Not myBBDB_AccessReader.IsDBNull(55) Then
                    bTrimFreetext = Trim(myBBDB_AccessReader.GetBoolean(55))
                End If
                If Not myBBDB_AccessReader.IsDBNull(56) Then
                    bUseReturnConfirmation = Trim(myBBDB_AccessReader.GetBoolean(56))
                End If
                If Not myBBDB_AccessReader.IsDBNull(57) Then
                    bUseReturnRejection = Trim(myBBDB_AccessReader.GetBoolean(57))
                End If
                If Not myBBDB_AccessReader.IsDBNull(58) Then
                    bUseReturnPosting = Trim(myBBDB_AccessReader.GetBoolean(58))
                End If
                If Not myBBDB_AccessReader.IsDBNull(59) Then
                    bSEPASingle = Trim(myBBDB_AccessReader.GetBoolean(59))
                End If
                If Not myBBDB_AccessReader.IsDBNull(60) Then
                    bEndToEndRefFromBabelBank = Trim(myBBDB_AccessReader.GetBoolean(60))
                End If
                If Not myBBDB_AccessReader.IsDBNull(61) Then
                    bUnDoDoubleMatched = Trim(myBBDB_AccessReader.GetBoolean(61))
                End If
                If Not myBBDB_AccessReader.IsDBNull(62) Then
                    bUnDoAllDoubleMatched = Trim(myBBDB_AccessReader.GetBoolean(62))
                End If
                If Not myBBDB_AccessReader.IsDBNull(63) Then
                    sDivision = myBBDB_AccessReader.GetString(63).Trim
                End If
                If Not myBBDB_AccessReader.IsDBNull(64) Then
                    sbbLanguage = myBBDB_AccessReader.GetString(64).Trim
                End If
                If Not EmptyString(sbbLanguage) Then
                    ' add language to config-file (tempararily for this run)
                    System.Configuration.ConfigurationManager.AppSettings("Language") = sbbLanguage
                End If

            Loop
            bStartLoad = True
        Else
            bStartLoad = False
        End If

        StartLoad = bStartLoad

    End Function
    Public Function Save(ByRef iCompanyNo As Short) As Boolean

        'KJELL - IKKE BYTT DENNE F�R VI HAR SAMMENLIGNET OG G�TT GJENNOM DENNE

        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim myBBDB_AccessTrans As System.Data.OleDb.OleDbTransaction = Nothing
        Dim myBBDB_AccessReader As System.Data.OleDb.OleDbDataReader = Nothing
        'Dim myBBDB_AccessSelectCommand As System.Data.OleDb.OleDbCommand = Nothing
        '- ' kan vi slette den over? og bruke KUN myBBDB_AccessCommand ?

        Dim myBBDB_AccessSelectConnection As System.Data.OleDb.OleDbConnection = Nothing

        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        Dim oLicense As vbBabel.License
        Dim bNoMoreChanges As Boolean
        Dim bUpdate, bDelete, bNew As Boolean
        Dim bContinue As Boolean
        Dim bStatus As Boolean
        Dim iCheckFilesetups As Short
        Dim sOldCompanyNo As String
        Dim sOldAdditionalNo As String

        Dim sTmpConfig As String
        Dim bSQLServer As Boolean
        Dim sBBConnectionString As String

        On Error GoTo ERRSave

        bNoMoreChanges = False
        bDelete = False
        bUpdate = False
        bNew = False

        Select Case eStatus
            Case CollectionStatus.NoChange
                bNoMoreChanges = True
            Case CollectionStatus.SeqNoChange
                'No action continue to Filesetup
            Case CollectionStatus.ChangeUnder
                'No action continue to Filesetup
            Case CollectionStatus.Changed
                'Have to save to the Company-table
                bUpdate = True
            Case CollectionStatus.NewCol
                bDelete = True
                'bNew = True
                bUpdate = True
            Case CollectionStatus.Deleted
                bDelete = True
                'bNew = True
                bUpdate = True
        End Select

        If bNoMoreChanges Then
            Save = True
            Exit Function
        End If

        bSQLServer = BB_UseSQLServer()
        sBBConnectionString = FindBBConnectionString()

        'Create the connection
        myBBDB_AccessConnection = New System.Data.OleDb.OleDbConnection(sBBConnectionString)
        myBBDB_AccessConnection.Open()
        myBBDB_AccessSelectConnection = New System.Data.OleDb.OleDbConnection(sBBConnectionString)
        myBBDB_AccessSelectConnection.Open()
        myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
        myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
        myBBDB_AccessCommand.CommandType = CommandType.Text
        ' IKKE LAGRE DET UNDER
        myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction
        myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans

        If bDelete Then

            If 1 = 1 Then 'Not RunTime() Then 'SKOLLEBOLLE - M� slette i annen rekkef�lge for ikke � f� konflikt med FK
                sMySQL = "DELETE from ClientFormat WHERE Company_ID = " & iCompanyNo.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE from Client WHERE Company_ID = " & iCompanyNo.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE from FileSetup WHERE Company_ID = " & iCompanyNo.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE from FileSetup2 WHERE Company_ID = " & iCompanyNo.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

            Else
                sMySQL = "DELETE from FileSetup WHERE Company_ID = " & iCompanyNo.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE from FileSetup2 WHERE Company_ID = " & iCompanyNo.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE from Client WHERE Company_ID = " & iCompanyNo.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "DELETE from ClientFormat WHERE Company_ID = " & iCompanyNo.ToString
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

            End If


            For Each oFilesetup In FileSetups
                oFilesetup.Status = 4
                For Each oClient In oFilesetup.Clients
                    oClient.Status = 4
                    For Each oaccount In oClient.Accounts
                        oaccount.Status = 4
                    Next oaccount
                Next oClient
            Next oFilesetup
        End If

        If bNew Then
            sMySQL = "INSERT INTO Company(Company_ID) VALUES(" & iCompanyNo.ToString & " )"
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()
        End If


        If bUpdate Then
            ' New 17.08.2001 jps
            ' If CompanyNo or AdditionalID is changed at Companylevel, then
            ' update also in FileSetup-level
            ' Do so by changing in oFileSetup where CompanyNo or AdditonalID are equal
            ' To do so, we need the old, not saved, values for CompanyNo and AdditonalNo from database
            'Have to check if the Client already exists in the database, because of the
            ' differences between the strusture of the database and the collections
            sMySQL = "SELECT CompanyNo, AdditionalNo FROM Company WHERE Company_ID = " & iCompanyNo.ToString

            'myBBDB_AccessCommand = myBBDB_AccessSelectConnection.CreateCommand()  'myBBDB_AccessSelectCommand 
            'myBBDB_AccessCommand.CommandType = CommandType.Text  'myBBDB_AccessSelectCommand 
            'myBBDB_AccessCommand.CommandType = CommandType.Text  'myBBDB_AccessSelectCommand 
            'myBBDB_AccessCommand.Connection = myBBDB_AccessSelectConnection 'myBBDB_AccessSelectCommand 
            myBBDB_AccessCommand.CommandText = sMySQL  'myBBDB_AccessSelectCommand 

            myBBDB_AccessReader = myBBDB_AccessCommand.ExecuteReader  'myBBDB_AccessSelectCommand 

            If myBBDB_AccessReader.HasRows Then
                Do While myBBDB_AccessReader.Read
                    If Not myBBDB_AccessReader.IsDBNull(0) Then
                        sOldCompanyNo = myBBDB_AccessReader.GetString(0) 'StatusText
                    End If
                    If Not myBBDB_AccessReader.IsDBNull(1) Then
                        sOldAdditionalNo = myBBDB_AccessReader.GetString(1) 'StatusText
                    End If
                    ' Only one record in recordset
                    Exit Do
                Loop
            End If

            myBBDB_AccessReader.Close()


            ' Added 24.03.06
            ' This will be wrong for DnBNOR TBI, where we in many cases want each filesetup
            ' to have its own CompanyNo

            oLicense = New vbBabel.License
            If InStr(oLicense.LicServicesAvailable, "T") = 0 And InStr(oLicense.LicServicesAvailable, "DNBX") = 0 Then

                If sOldCompanyNo <> "" Then
                    ' Check in Profile, oFileSetup if found;
                    For Each oFilesetup In FileSetups
                        If oFilesetup.CompanyNo = sOldCompanyNo Then
                            oFilesetup.CompanyNo = sCompanyNo
                            'sAdditionalNo = oFilesetup.AdditionalNo
                            If Not EmptyString(sAdditionalNo) Then
                                ' update with additionalno from company
                                oFilesetup.AdditionalNo = sAdditionalNo
                            End If
                            oFilesetup.Status = CollectionStatus.Changed
                        End If
                    Next oFilesetup
                End If
            End If 'Not InStr(oLicense.LicServicesAvailable, "T") Then
            oLicense = Nothing
            '---------------------------------------------------

            sMySQL = "UPDATE Company SET Name = '" & Trim(sCompanyName) & "', Adr1= '" & Trim(sCompanyAdr1)
            sMySQL = sMySQL & "', Adr2 = '" & Trim(sCompanyAdr2) & "', Adr3= '" & Trim(sCompanyAdr3)
            sMySQL = sMySQL & "', Zip = '" & sCompanyZip & "', City= '" & Trim(sCompanyCity)
            sMySQL = sMySQL & "', CompanyNo = '" & sCompanyNo & "', AdditionalNo = '" & sAdditionalNo
            If bSQLServer Then
                sMySQL = sMySQL & "', [BackUp] = " & CInt(bBackUp) & ", BackUpPath = '" & sBackUpPath
            Else
                sMySQL = sMySQL & "', BackUp = " & Str(bBackUp) & ", BackUpPath = '" & sBackUpPath
            End If
            sMySQL = sMySQL & "', DelDays = " & Str(iDelDays) '& ", Code = '" & sCode
            sMySQL = sMySQL & ", Custom1Text = '" & Trim(sCustom1Text) & "', Custom1Value = '" & sCustom1Value
            sMySQL = sMySQL & "', Custom2Text = '" & Trim(sCustom2Text) & "', Custom2Value = '" & sCustom2Value
            sMySQL = sMySQL & "', Custom3Text = '" & Trim(sCustom3Text) & "', Custom3Value = '" & sCustom3Value
            sMySQL = sMySQL & "', Custom4Text = '" & Trim(sCustom4Text) & "', Custom4Value = '" & sCustom4Value
            sMySQL = sMySQL & "', InvoiceDescText = '" & Trim(sInvoiceDescText) & "', GeneralNoteText = '" & sGeneralNoteText
            sMySQL = sMySQL & "', ConString = '" & Trim(sConString) & "', ConUID = '" & Trim(sConUID) & "', ConPWD = '" & sConPWD
            sMySQL = sMySQL & "', EmailSender = '" & Trim(sEmailSender) & "', EmailDisplayName = '" & Trim(sEmailDisplayName) & "', EMailAutoSender = '" & Trim(sEmailAutoSender)
            sMySQL = sMySQL & "', EmailAutoDisplayName = '" & Trim(sEmailAutoDisplayName) & "', EmailAutoReceiver = '" & (sEmailAutoReceiver) & "', EMailReplyAdress = '" & Trim(sEmailReplyAdress)
            If bSQLServer Then
                sMySQL = sMySQL & "', EmailSMTPHost = '" & Trim(seMailSMTPHost) & "', EmailSMTP = " & CInt(bEmailSMTP)
            Else
                sMySQL = sMySQL & "', EmailSMTPHost = '" & Trim(seMailSMTPHost) & "', EmailSMTP = " & Trim(CStr(bEmailSMTP))
            End If
            sMySQL = sMySQL & ", EmailSupport = '" & Trim(sEmailSupport)
            sMySQL = sMySQL & "', EmailCCSupport = '" & Trim(sEmailCCSupport) & "', AccountsFile = '" & Trim(sAccountsFile)
            If bSQLServer Then
                sMySQL = sMySQL & "', ManMatchFromERP = " & CInt(bManMatchFromERP)
                sMySQL = sMySQL & ", RunTempCode = " & CInt(bRunTempCode)
            Else
                sMySQL = sMySQL & "', ManMatchFromERP = " & Str(bManMatchFromERP)
                sMySQL = sMySQL & ", RunTempCode = " & Str(bRunTempCode)
            End If
            sMySQL = sMySQL & ", DebugCode = " & Str(iDebugCode)
            sMySQL = sMySQL & ", MaxSizeDB = " & Trim(Str(iMaxSizeDB))
            If bSQLServer Then
                sMySQL = sMySQL & ", ArchiveUseArchive = " & CInt(bUseArchive)
                sMySQL = sMySQL & ", DisableAdminMenus = " & CInt(bDisableAdminMenus)
                sMySQL = sMySQL & ", LockBabelBank = " & CInt(bLockBabelBank)
            Else
                sMySQL = sMySQL & ", ArchiveUseArchive = " & Str(bUseArchive)
                sMySQL = sMySQL & ", DisableAdminMenus = " & Str(bDisableAdminMenus)
                sMySQL = sMySQL & ", LockBabelBank = " & Str(bLockBabelBank)
            End If
            sMySQL = sMySQL & ", BaseCurrency = '" & Trim(sBaseCurrency) & "'"
            ' added 25.06.2014
            sMySQL = sMySQL & ", StructuredOCR = '" & Trim$(sStructuredOCR) & "'"
            sMySQL = sMySQL & ", StructuredInvoice = '" & Trim$(sStructuredInvoice) & "'"
            sMySQL = sMySQL & ", StructuredCreditNote = '" & Trim$(sStructuredCreditNote) & "'"
            sMySQL = sMySQL & ", HistoryDelDays = " & Trim$(Str(iHistoryDelDays))
            If bSQLServer Then
                sMySQL = sMySQL & ", TrimFreetext = " & CInt(bTrimFreetext)
                sMySQL = sMySQL & ", UseReturnConfirmation = " & CInt(bUseReturnConfirmation)
                sMySQL = sMySQL & ", UseReturnRejection = " & CInt(bUseReturnRejection)
                sMySQL = sMySQL & ", UseReturnPosting = " & CInt(bUseReturnPosting)
                sMySQL = sMySQL & ", SEPASingle = " & CInt(bSEPASingle)
                sMySQL = sMySQL & ", EndToEndRefFromBabelBank = " & CInt(bEndToEndRefFromBabelBank)
                sMySQL = sMySQL & ", UnDoDoubleMatched = " & CInt(bUnDoDoubleMatched)
                sMySQL = sMySQL & ", UnDoAllDoubleMatched = " & CInt(bUnDoAllDoubleMatched)
            Else
                sMySQL = sMySQL & ", TrimFreetext = " & Str(bTrimFreetext)
                sMySQL = sMySQL & ", UseReturnConfirmation = " & Str(bUseReturnConfirmation)
                sMySQL = sMySQL & ", UseReturnRejection = " & Str(bUseReturnRejection)
                sMySQL = sMySQL & ", UseReturnPosting = " & Str(bUseReturnPosting)
                sMySQL = sMySQL & ", SEPASingle = " & Str(bSEPASingle)
                sMySQL = sMySQL & ", EndToEndRefFromBabelBank = " & Str(bEndToEndRefFromBabelBank)
                sMySQL = sMySQL & ", UnDoDoubleMatched = " & Str(bUnDoDoubleMatched)
                sMySQL = sMySQL & ", UnDoAllDoubleMatched = " & Str(bUnDoAllDoubleMatched)
            End If
            sMySQL = sMySQL & ", Division = '" & sDivision.Trim & "'"
            ' 21.06.2017 added backup of database
            If bSQLServer Then
                sMySQL = sMySQL & ", CreateBackup = " & CInt(bCreateBackup)
                sMySQL = sMySQL & ", BckBeforeImport = " & CInt(bBckBeforeImport)
                sMySQL = sMySQL & ", BckBeforeExport = " & CInt(bBckBeforeExport)
                sMySQL = sMySQL & ", BckAfterExport = " & CInt(BckAfterExport)
            Else
                sMySQL = sMySQL & ", CreateBackup = " & Str(bCreateBackup)
                sMySQL = sMySQL & ", BckBeforeImport = " & Str(bBckBeforeImport)
                sMySQL = sMySQL & ", BckBeforeExport = " & Str(bBckBeforeExport)
                sMySQL = sMySQL & ", BckAfterExport = " & Str(BckAfterExport)
            End If
            sMySQL = sMySQL & ", ExportBackuppath = '" & Trim(sExportBackuppath) & "'"
            sMySQL = sMySQL & ", ExportBackupGenerations = " & Str(iExportBackupGenerations)
            sMySQL = sMySQL & ", bbLanguage = '" & Trim(sbbLanguage) & "'"  ' added 06.07.2017

            sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo)

            myBBDB_AccessCommand.CommandText = sMySQL

            myBBDB_AccessCommand.ExecuteNonQuery()

        End If

        iCheckFilesetups = CheckFilesetupChanges()

        bContinue = False

        If 1 = 1 Then 'Not RunTime() And 1 = 2 Then 'SKOLLEBOLLE - M� f�rst lagre alle FileSetup for ikke � f� konflikt med FK p� filesetup_ID/Formater 
            For Each oFilesetup In FileSetups

                bContinue = SaveFilesetup(iCompanyNo, oFilesetup, myBBDB_AccessCommand, bSQLServer)
                If bContinue Then

                    'If oFilesetup.Reports.Count > 0 Then
                    ' 15.10.03
                    ' Removed if -- count > 0, because then the last report would never disapear
                    bContinue = False
                    'bContinue = SaveReports(iCompanyNo, oFilesetup.Reports, oFilesetup.FileSetup_ID)
                    ' Changed 08.05.06 due to problems with filesetup_id
                    bContinue = SaveReports(iCompanyNo, (oFilesetup.Reports), CheckFilesetupChanges((oFilesetup.FileSetup_ID)), myBBDB_AccessCommand, bSQLServer)
                    'End If
                End If

            Next oFilesetup

            bContinue = True

            For Each oFilesetup In FileSetups
                If bContinue Then
                    bContinue = False
                    For Each oClient In oFilesetup.Clients
                        ' FIX: ed by JanP 01.11.2001 -
                        ' Er det korrekt � drite i saveing av client, og konto ved slettet klient
                        ' F�r ellers feilmelding ved saveaccount !!!
                        ' Klient blir ikke slettet, men feilmelding forsvinner !
                        If Not oClient.Status = CollectionStatus.Deleted Then
                            bContinue = SaveClient(iCompanyNo, oClient, (oFilesetup.FileSetup_ID), myBBDB_AccessCommand, myBBDB_AccessReader, bSQLServer)
                            If bContinue Then
                                bContinue = False
                                For Each oaccount In oClient.Accounts
                                    bContinue = SaveAccount(iCompanyNo, oaccount, (oFilesetup.FileSetup_ID), (oClient.Index), myBBDB_AccessCommand, myBBDB_AccessReader)   'myBBDB_AccessSelectCommand)
                                Next oaccount
                            End If
                        End If
                    Next oClient
                    bContinue = True
                End If
            Next oFilesetup

        Else
            For Each oFilesetup In FileSetups

                bContinue = SaveFilesetup(iCompanyNo, oFilesetup, myBBDB_AccessCommand, bSQLServer)
                If bContinue Then

                    'If oFilesetup.Reports.Count > 0 Then
                    ' 15.10.03
                    ' Removed if -- count > 0, because then the last report would never disapear
                    bContinue = False
                    'bContinue = SaveReports(iCompanyNo, oFilesetup.Reports, oFilesetup.FileSetup_ID)
                    ' Changed 08.05.06 due to problems with filesetup_id
                    bContinue = SaveReports(iCompanyNo, (oFilesetup.Reports), CheckFilesetupChanges((oFilesetup.FileSetup_ID)), myBBDB_AccessCommand, bSQLServer)
                    'End If
                End If

                If bContinue Then
                    bContinue = False
                    For Each oClient In oFilesetup.Clients
                        ' FIX: ed by JanP 01.11.2001 -
                        ' Er det korrekt � drite i saveing av client, og konto ved slettet klient
                        ' F�r ellers feilmelding ved saveaccount !!!
                        ' Klient blir ikke slettet, men feilmelding forsvinner !
                        If Not oClient.Status = CollectionStatus.Deleted Then
                            bContinue = SaveClient(iCompanyNo, oClient, (oFilesetup.FileSetup_ID), myBBDB_AccessCommand, myBBDB_AccessReader, bSQLServer)
                            If bContinue Then
                                bContinue = False
                                For Each oaccount In oClient.Accounts
                                    bContinue = SaveAccount(iCompanyNo, oaccount, (oFilesetup.FileSetup_ID), (oClient.Index), myBBDB_AccessCommand, myBBDB_AccessReader)   'myBBDB_AccessSelectCommand)
                                Next oaccount
                            End If
                        End If
                    Next oClient
                End If
            Next oFilesetup
        End If

        iCheckFilesetups = CheckFilesetupChanges(-1)

        myBBDB_AccessTrans.Commit()

        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        'If Not myBBDB_AccessSelectCommand Is Nothing Then
        '    myBBDB_AccessSelectCommand.Dispose()
        '    myBBDB_AccessSelectCommand = Nothing
        'End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If
        If Not myBBDB_AccessReader Is Nothing Then
            If Not myBBDB_AccessReader.IsClosed Then
                myBBDB_AccessReader.Close()
            End If
            myBBDB_AccessReader = Nothing
        End If
        If Not myBBDB_AccessSelectConnection Is Nothing Then
            If myBBDB_AccessSelectConnection.State <> ConnectionState.Closed Then
                'Debug.Print("Lukker Connection!")
                'If Not RunTime() Then
                ' TESTING - logg to file whenever we open Company;
                ' ISINGRUD - ER DETTE NOE VI TRENGER ???? 
                'Dim sCallstack As String
                'Dim oFs As Scripting.FileSystemObject
                'Dim oTempFile As Scripting.TextStream
                'Dim sLog As String
                'oFs = CreateObject ("Scripting.FileSystemObject")
                'sLog = "c:\projects.net\CompanyLogg.dat"

                'sCallstack = Environment.StackTrace.Substring(0, 500)
                'oTempFile = oFs.OpenTextFile(sLog, Scripting.IOMode.ForAppending, True)
                ' Write to logfile
                'oTempFile.Write(Now.ToString & "Company.Save, myBBDB_AccessSelectConnection.Close()" & vbCrLf & sCallstack & vbCrLf)
                'oTempFile.Close()
                'oTempFile = Nothing
                'oFs = Nothing
                'End If

                myBBDB_AccessSelectConnection.Close()
            End If
            myBBDB_AccessSelectConnection.Dispose()
            myBBDB_AccessSelectConnection = Nothing
        End If

        Return True

ERRSave:
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        'If Not myBBDB_AccessSelectCommand Is Nothing Then
        '    myBBDB_AccessSelectCommand.Dispose()
        '    myBBDB_AccessSelectCommand = Nothing
        'End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Rollback()
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If
        If Not myBBDB_AccessReader Is Nothing Then
            If Not myBBDB_AccessReader.IsClosed Then
                myBBDB_AccessReader.Close()
            End If
            myBBDB_AccessReader = Nothing
        End If
        If Not myBBDB_AccessSelectConnection Is Nothing Then
            If myBBDB_AccessSelectConnection.State <> ConnectionState.Closed Then
                'Debug.Print("Lukker Connection!")
                myBBDB_AccessSelectConnection.Close()
            End If
            myBBDB_AccessSelectConnection.Dispose()
            myBBDB_AccessSelectConnection = Nothing
        End If

        Err.Raise(Err.Number, Err.Source, Err.Description)

        Return False

    End Function
    Public Function Save_Old(ByRef iCompanyNo As Short) As Boolean

        Dim myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim myBBDB_AccessTrans As System.Data.OleDb.OleDbTransaction = Nothing
        Dim myBBDB_AccessReader As System.Data.OleDb.OleDbDataReader = Nothing
        Dim myBBDB_AccessSelectCommand As System.Data.OleDb.OleDbCommand = Nothing
        Dim myBBDB_AccessSelectConnection As System.Data.OleDb.OleDbConnection = Nothing

        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account
        Dim oLicense As License
        Dim bNoMoreChanges As Boolean
        Dim bUpdate, bDelete, bNew As Boolean
        Dim bContinue As Boolean
        Dim bStatus As Boolean
        Dim iCheckFilesetups As Short
        Dim sOldCompanyNo As String
        Dim sOldAdditionalNo As String

        On Error GoTo ERRSave

        bNoMoreChanges = False
        bDelete = False
        bUpdate = False
        bNew = False

        Select Case eStatus
            Case CollectionStatus.NoChange
                bNoMoreChanges = True
            Case CollectionStatus.SeqNoChange
                'No action continue to Filesetup
            Case CollectionStatus.ChangeUnder
                'No action continue to Filesetup
            Case CollectionStatus.Changed
                'Have to save to the Company-table
                bUpdate = True
            Case CollectionStatus.NewCol
                bDelete = True
                'bNew = True
                bUpdate = True
            Case CollectionStatus.Deleted
                bDelete = True
                'bNew = True
                bUpdate = True
        End Select

        If bNoMoreChanges Then
            Save_Old = True
            Exit Function
        End If

        'Create the connection
        myBBDB_AccessConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BB_DatabasePath())
        myBBDB_AccessConnection.Open()
        myBBDB_AccessSelectConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BB_DatabasePath())
        myBBDB_AccessSelectConnection.Open()
        myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand
        myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
        myBBDB_AccessCommand.CommandType = CommandType.Text
        ' IKKE LAGRE DET UNDER
        myBBDB_AccessTrans = myBBDB_AccessConnection.BeginTransaction
        myBBDB_AccessCommand.Transaction = myBBDB_AccessTrans

        If bDelete Then
            sMySQL = "DELETE * from FileSetup WHERE Company_ID = " & iCompanyNo.ToString
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()

            sMySQL = "DELETE * from FileSetup2 WHERE Company_ID = " & iCompanyNo.ToString
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()

            sMySQL = "DELETE * from Client WHERE Company_ID = " & iCompanyNo.ToString
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()

            sMySQL = "DELETE * from ClientFormat WHERE Company_ID = " & iCompanyNo.ToString
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()

            For Each oFilesetup In FileSetups
                oFilesetup.Status = 4
                For Each oClient In oFilesetup.Clients
                    oClient.Status = 4
                    For Each oaccount In oClient.Accounts
                        oaccount.Status = 4
                    Next oaccount
                Next oClient
            Next oFilesetup
        End If

        If bNew Then
            sMySQL = "INSERT INTO Company(Company_ID) VALUES(" & iCompanyNo.ToString & " )"
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()
        End If

        If bUpdate Then
            ' New 17.08.2001 jps
            ' If CompanyNo or AdditionalID is changed at Companylevel, then
            ' update also in FileSetup-level
            ' Do so by changing in oFileSetup where CompanyNo or AdditonalID are equal
            ' To do so, we need the old, not saved, values for CompanyNo and AdditonalNo from database
            'Have to check if the Client already exists in the database, because of the
            ' differences between the strusture of the database and the collections
            sMySQL = "SELECT CompanyNo, AdditionalNo FROM Company WHERE Company_ID = " & iCompanyNo.ToString

            myBBDB_AccessSelectCommand = myBBDB_AccessSelectConnection.CreateCommand()
            myBBDB_AccessSelectCommand.CommandType = CommandType.Text
            myBBDB_AccessSelectCommand.Connection = myBBDB_AccessSelectConnection
            myBBDB_AccessSelectCommand.CommandText = sMySQL

            myBBDB_AccessReader = myBBDB_AccessSelectCommand.ExecuteReader

            If myBBDB_AccessReader.HasRows Then
                Do While myBBDB_AccessReader.Read
                    If Not myBBDB_AccessReader.IsDBNull(0) Then
                        sOldCompanyNo = myBBDB_AccessReader.GetString(0) 'StatusText
                    End If
                    If Not myBBDB_AccessReader.IsDBNull(1) Then
                        sOldAdditionalNo = myBBDB_AccessReader.GetString(1) 'StatusText
                    End If
                    ' Only one record in recordset
                    Exit Do
                Loop
            End If

            myBBDB_AccessReader.Close()

            ' Added 24.03.06
            ' This will be wrong for DnBNOR TBI, where we in many cases want each filesetup
            ' to have its own CompanyNo
            oLicense = New vbBabel.License
            If InStr(oLicense.LicServicesAvailable, "T") = 0 Then

                If sOldCompanyNo <> "" Then
                    ' Check in Profile, oFileSetup if found;
                    For Each oFilesetup In FileSetups
                        If oFilesetup.CompanyNo = sOldCompanyNo Then
                            oFilesetup.CompanyNo = sCompanyNo
                            oFilesetup.AdditionalNo = sAdditionalNo
                            oFilesetup.Status = CollectionStatus.Changed
                        End If
                    Next oFilesetup
                End If
            End If 'Not InStr(oLicense.LicServicesAvailable, "T") Then
            oLicense = Nothing
            '---------------------------------------------------

            sMySQL = "UPDATE Company SET Name = '" & Trim(sCompanyName) & "', Adr1= '" & Trim(sCompanyAdr1)
            sMySQL = sMySQL & "', Adr2 = '" & Trim(sCompanyAdr2) & "', Adr3= '" & Trim(sCompanyAdr3)
            sMySQL = sMySQL & "', Zip = '" & sCompanyZip & "', City= '" & Trim(sCompanyCity)
            sMySQL = sMySQL & "', CompanyNo = '" & sCompanyNo & "', AdditionalNo = '" & sAdditionalNo
            sMySQL = sMySQL & "', BackUp = " & Str(bBackUp) & ", BackUpPath = '" & sBackUpPath
            sMySQL = sMySQL & "', DelDays = " & Str(iDelDays) '& ", Code = '" & sCode
            sMySQL = sMySQL & ", Custom1Text = '" & Trim(sCustom1Text) & "', Custom1Value = '" & sCustom1Value
            sMySQL = sMySQL & "', Custom2Text = '" & Trim(sCustom2Text) & "', Custom2Value = '" & sCustom2Value
            sMySQL = sMySQL & "', Custom3Text = '" & Trim(sCustom3Text) & "', Custom3Value = '" & sCustom3Value
            sMySQL = sMySQL & "', Custom4Text = '" & Trim(sCustom4Text) & "', Custom4Value = '" & sCustom4Value
            sMySQL = sMySQL & "', InvoiceDescText = '" & Trim(sInvoiceDescText) & "', GeneralNoteText = '" & sGeneralNoteText
            sMySQL = sMySQL & "', ConString = '" & Trim(sConString) & "', ConUID = '" & Trim(sConUID) & "', ConPWD = '" & sConPWD
            sMySQL = sMySQL & "', EmailSender = '" & Trim(sEmailSender) & "', EmailDisplayName = '" & Trim(sEmailDisplayName) & "', EMailAutoSender = '" & Trim(sEmailAutoSender)
            sMySQL = sMySQL & "', EmailAutoDisplayName = '" & Trim(sEmailAutoDisplayName) & "', EmailAutoReceiver = '" & (sEmailAutoReceiver) & "', EMailReplyAdress = '" & Trim(sEmailReplyAdress)
            sMySQL = sMySQL & "', EmailSMTPHost = '" & Trim(seMailSMTPHost) & "', EmailSMTP = " & Trim(CStr(bEmailSMTP))
            sMySQL = sMySQL & ", EmailSupport = '" & Trim(sEmailSupport)
            sMySQL = sMySQL & "', EmailCCSupport = '" & Trim(sEmailCCSupport) & "', AccountsFile = '" & Trim(sAccountsFile)
            sMySQL = sMySQL & "', ManMatchFromERP = " & Str(bManMatchFromERP)
            sMySQL = sMySQL & ", RunTempCode = " & Str(bRunTempCode)
            sMySQL = sMySQL & ", DebugCode = " & Str(iDebugCode)
            sMySQL = sMySQL & ", MaxSizeDB = " & Trim(Str(iMaxSizeDB))
            sMySQL = sMySQL & ", ArchiveUseArchive = " & Str(bUseArchive)
            sMySQL = sMySQL & ", DisableAdminMenus = " & Str(bDisableAdminMenus)
            sMySQL = sMySQL & ", LockBabelBank = " & Str(bLockBabelBank)
            sMySQL = sMySQL & ", BaseCurrency = '" & Trim(sBaseCurrency) & "'"
            sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo)
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()

        End If

        iCheckFilesetups = CheckFilesetupChanges()

        bContinue = False
        For Each oFilesetup In FileSetups
            bContinue = SaveFilesetup(iCompanyNo, oFilesetup, myBBDB_AccessCommand, False)
            If bContinue Then

                'If oFilesetup.Reports.Count > 0 Then
                ' 15.10.03
                ' Removed if -- count > 0, because then the last report would never disapear
                bContinue = False
                'bContinue = SaveReports(iCompanyNo, oFilesetup.Reports, oFilesetup.FileSetup_ID)
                ' Changed 08.05.06 due to problems with filesetup_id
                bContinue = SaveReports(iCompanyNo, (oFilesetup.Reports), CheckFilesetupChanges((oFilesetup.FileSetup_ID)), myBBDB_AccessCommand, False)
                'End If
            End If

            If bContinue Then
                bContinue = False
                For Each oClient In oFilesetup.Clients
                    ' FIX: ed by JanP 01.11.2001 -
                    ' Er det korrekt � drite i saveing av client, og konto ved slettet klient
                    ' F�r ellers feilmelding ved saveaccount !!!
                    ' Klient blir ikke slettet, men feilmelding forsvinner !
                    If Not oClient.Status = CollectionStatus.Deleted Then
                        bContinue = SaveClient_old(iCompanyNo, oClient, (oFilesetup.FileSetup_ID), myBBDB_AccessCommand, myBBDB_AccessSelectCommand)
                        If bContinue Then
                            bContinue = False
                            For Each oaccount In oClient.Accounts
                                bContinue = SaveAccount_old(iCompanyNo, oaccount, (oFilesetup.FileSetup_ID), (oClient.Index), myBBDB_AccessCommand, myBBDB_AccessSelectCommand)
                            Next oaccount
                        End If
                    End If
                Next oClient
            End If
        Next oFilesetup

        iCheckFilesetups = CheckFilesetupChanges(-1)

        myBBDB_AccessTrans.Commit()

        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        If Not myBBDB_AccessSelectCommand Is Nothing Then
            myBBDB_AccessSelectCommand.Dispose()
            myBBDB_AccessSelectCommand = Nothing
        End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If
        If Not myBBDB_AccessReader Is Nothing Then
            If Not myBBDB_AccessReader.IsClosed Then
                myBBDB_AccessReader.Close()
            End If
            myBBDB_AccessReader = Nothing
        End If
        If Not myBBDB_AccessSelectConnection Is Nothing Then
            If myBBDB_AccessSelectConnection.State <> ConnectionState.Closed Then
                'Debug.Print("Lukker Connection!")
                If Not RunTime() Then
                    ' TESTING - logg to file whenever we open Company;
                    Dim sCallstack As String
                    Dim oFs As Scripting.FileSystemObject
                    Dim oTempFile As Scripting.TextStream
                    Dim sLog As String
                    oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
                    sLog = "c:\projects.net\CompanyLogg.dat"

                    sCallstack = Environment.StackTrace.Substring(0, 500)
                    oTempFile = oFs.OpenTextFile(sLog, Scripting.IOMode.ForAppending, True)
                    ' Write to logfile
                    oTempFile.Write(Now.ToString & "Company.Save, myBBDB_AccessSelectConnection.Close()" & vbCrLf & sCallstack & vbCrLf)
                    oTempFile.Close()
                    oTempFile = Nothing
                    oFs = Nothing
                End If

                myBBDB_AccessSelectConnection.Close()
            End If
            myBBDB_AccessSelectConnection.Dispose()
            myBBDB_AccessSelectConnection = Nothing
        End If

        Return True

ERRSave:
        If Not myBBDB_AccessCommand Is Nothing Then
            myBBDB_AccessCommand.Dispose()
            myBBDB_AccessCommand = Nothing
        End If
        If Not myBBDB_AccessSelectCommand Is Nothing Then
            myBBDB_AccessSelectCommand.Dispose()
            myBBDB_AccessSelectCommand = Nothing
        End If
        If Not myBBDB_AccessTrans Is Nothing Then
            myBBDB_AccessTrans.Rollback()
            myBBDB_AccessTrans.Dispose()
            myBBDB_AccessTrans = Nothing
        End If
        If Not myBBDB_AccessReader Is Nothing Then
            If Not myBBDB_AccessReader.IsClosed Then
                myBBDB_AccessReader.Close()
            End If
            myBBDB_AccessReader = Nothing
        End If
        If Not myBBDB_AccessSelectConnection Is Nothing Then
            If myBBDB_AccessSelectConnection.State <> ConnectionState.Closed Then
                'Debug.Print("Lukker Connection!")
                myBBDB_AccessSelectConnection.Close()
            End If
            myBBDB_AccessSelectConnection.Dispose()
            myBBDB_AccessSelectConnection = Nothing
        End If

        Err.Raise(Err.Number, Err.Source, Err.Description)

        Return False

    End Function
    Private Function SaveFilesetup(ByRef iCompanyNo As Short, ByRef oFilesetup As FileSetup, ByRef myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand, ByVal bSQLServer As Boolean) As Boolean
        '08.07.2016 - Added bSQLServer
        Dim sMySQL As String
        Dim bUpdate, bDelete, bNew As Boolean
        Dim bContinue As Boolean

        'This variable is used to reduce all referances to the FileSetupID with its content.
        bContinue = False
        bDelete = False
        bUpdate = False
        bNew = False

        Select Case oFilesetup.Status
            Case CollectionStatus.NoChange
                'No action, next Filesetup
            Case CollectionStatus.SeqNoChange
                sMySQL = "UPDATE FileSetup SET SeqNoTotal = " & oFilesetup.SeqNoTotal
                sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID = "
                sMySQL = sMySQL & Str(oFilesetup.FileSetup_ID)
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()
            Case CollectionStatus.ChangeUnder
                bContinue = True
            Case CollectionStatus.Changed
                bUpdate = True
                bContinue = True
            Case CollectionStatus.NewCol
                bNew = True
                bUpdate = True
                bContinue = True
            Case CollectionStatus.Deleted
                bDelete = True
        End Select


        If bDelete Then
            'FIXED: (Temporarely by JanP 04.10.01, load new profile when saved,
            ' this way the deleted items will disappear
            'FileSetups.Remove (oFileSetup.FileSetup_ID)
        End If

        If bNew Then
            sMySQL = "INSERT INTO FileSetup(Company_ID,FileSetup_ID,Format_ID,Bank_ID) VALUES("
            sMySQL = sMySQL & Str(iCompanyNo) & ", " & Str(CheckFilesetupChanges((oFilesetup.FileSetup_ID)))
            sMySQL = sMySQL & ", " & Str(oFilesetup.Format_ID) & ", " & Str(oFilesetup.Bank_ID) & ")"
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()
            sMySQL = "INSERT INTO FileSetup2(Company_ID,FileSetup_ID2) VALUES("
            sMySQL = sMySQL & Str(iCompanyNo) & ", " & Str(CheckFilesetupChanges((oFilesetup.FileSetup_ID))) & ")"
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()
        End If

        If bUpdate Then
            With oFilesetup

                sMySQL = "UPDATE FileSetup SET Format_ID = " & .Format_ID & ",EnumID = " & .Enum_ID
                sMySQL = sMySQL & ",Bank_ID = " & .Bank_ID & ", ShortName= '" & Trim(.ShortName)
                If bSQLServer Then
                    sMySQL = sMySQL & "', Silent = " & CInt(.Silent)
                    sMySQL = sMySQL & ", [Log] = " & CInt(.Log) & ", LogType = " & Str(.LogType)
                    sMySQL = sMySQL & ", LogFilename = '" & Trim(.LogFilename) & "', LogOverwrite = " & CInt(.LogOverwrite)
                    sMySQL = sMySQL & ", LogShowStartStop = " & CInt(.LogShowStartStop) & ", LogMaxLines = " & Str(.LogMaxLines)
                    sMySQL = sMySQL & ", LogDetails = " & CInt(.LogDetails)
                Else
                    sMySQL = sMySQL & "', Silent = " & Str(.Silent)
                    sMySQL = sMySQL & ", Log = " & Str(.Log) & ", LogType = " & Str(.LogType)
                    sMySQL = sMySQL & ", LogFilename = '" & Trim(.LogFilename) & "', LogOverwrite = " & Str(.LogOverwrite)
                    sMySQL = sMySQL & ", LogShowStartStop = " & Str(.LogShowStartStop) & ", LogMaxLines = " & Str(.LogMaxLines)
                    sMySQL = sMySQL & ", LogDetails = " & Str(.LogDetails)
                End If
                sMySQL = sMySQL & ", Description = '" & Trim(.Description) & "', FileSetupOut= " & CheckFilesetupChanges(.FileSetupOut)
                sMySQL = sMySQL & ", FileNameIn1 = '" & Trim(.FileNameIn1) & "', FileNameIn2= '" & Trim(.FileNameIn2)
                If bSQLServer Then
                    sMySQL = sMySQL & "', FileNameIn3 = '" & Trim(.FileNameIn3) & "', BackupIn= " & CInt(.BackupIn) & ", Manual = " & CInt(.Manual)
                    sMySQL = sMySQL & ", OverWriteIn = " & CInt(.OverWriteIn) & ", WarningIn= " & CInt(.WarningIn)
                Else
                    sMySQL = sMySQL & "', FileNameIn3 = '" & Trim(.FileNameIn3) & "', BackupIn= " & Str(.BackupIn) & ", Manual = " & Str(.Manual)
                    sMySQL = sMySQL & ", OverWriteIn = " & Str(.OverWriteIn) & ", WarningIn= " & Str(.WarningIn)
                End If
                sMySQL = sMySQL & ", FileNameOut1 = '" & Trim(.FileNameOut1) & "', FileNameOut2= '" & Trim(.FileNameOut2)
                If bSQLServer Then
                    sMySQL = sMySQL & "', FileNameOut3 = '" & Trim(.FileNameOut3) & "', BackupOut= " & CInt(.BackupOut)
                    sMySQL = sMySQL & ", OverWriteOut = " & CInt(.OverWriteOut) & ", WarningOut= " & CInt(.WarningOut) & ", RemoveOldOut= " & CInt(.RemoveOldOut)

                    sMySQL = sMySQL & ", NotSplitOnAccount = " & CInt(.NotSplitOnAccount)
                Else
                    sMySQL = sMySQL & "', FileNameOut3 = '" & Trim(.FileNameOut3) & "', BackupOut= " & Str(.BackupOut)
                    sMySQL = sMySQL & ", OverWriteOut = " & Str(.OverWriteOut) & ", WarningOut= " & Str(.WarningOut) & ", RemoveOldOut= " & Str(.RemoveOldOut)

                    sMySQL = sMySQL & ", NotSplitOnAccount = " & Str(.NotSplitOnAccount)
                End If
                sMySQL = sMySQL & ", SeqNoType = " & .SeqnoType & ", SeqNoTotal= " & .SeqNoTotal
                sMySQL = sMySQL & ", SeqNoDay = " & .SeqNoDay & ", Division= '" & .Division
                'Use CompanyNo from FileSetup if filled in, else from profile
                If .CompanyNo <> "" Then
                    sMySQL = sMySQL & "', CompanyNo = '" & .CompanyNo
                Else
                    sMySQL = sMySQL & "', CompanyNo = '" & sCompanyNo
                End If

                If .AdditionalNo <> "" Then
                    sMySQL = sMySQL & "', AdditionalNo = '" & .AdditionalNo & "'"
                Else
                    sMySQL = sMySQL & "', AdditionalNo = '" & sAdditionalNo & "'"
                End If

                sMySQL = sMySQL & " where Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID = "
                sMySQL = sMySQL & Str(CheckFilesetupChanges((oFilesetup.FileSetup_ID)))
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "UPDATE FileSetup SET "
                If bSQLServer Then
                    sMySQL = sMySQL & "FromAccountingSys = " & CInt(.FromAccountingSystem) & ", InfoNewClient= " & CInt(.InfoNewClient)
                    sMySQL = sMySQL & ", KeepBatch = " & CInt(.KeepBatch)
                    sMySQL = sMySQL & ", CtrlNegative = " & CInt(.CtrlNegative) & ", CtrlZip= " & CInt(.CtrlZip)
                    sMySQL = sMySQL & ", CtrlAccount = " & CInt(.CtrlAccount) & ", CtrlKID= " & CInt(.CtrlKID)
                    sMySQL = sMySQL & ", ChangeDateDue = " & CInt(.ChangeDateDue) & ", ChangeDateAll= " & CInt(.ChangeDateAll)
                    sMySQL = sMySQL & ", RejectWrong = " & CInt(.RejectWrong) & ", Accumulation= " & CInt(.Accumulation)
                    sMySQL = sMySQL & ", ClientToScreen = " & CInt(.ClientToScreen) & ", ClientToPrint= " & CInt(.ClientToPrint)
                    sMySQL = sMySQL & ", ClientToFile = " & CInt(.ClientToFile) & ", ClientToMail= " & CInt(.ClientToMail)
                    sMySQL = sMySQL & ", ReportToPrint1 = " & CInt(.ReportToPrint1) & ", ReportToPrint2= " & CInt(.ReportToPrint2)
                    sMySQL = sMySQL & ", ReportToPrint3 = " & CInt(.ReportToPrint3) & ", ReportToScreen1= " & CInt(.ReportToScreen1)
                    sMySQL = sMySQL & ", ReportToScreen2 = " & CInt(.ReportToScreen2) & ", ReportToScreen3= " & CInt(.ReportToScreen3)
                    sMySQL = sMySQL & ", Message = '" & Trim(.Message)
                    sMySQL = sMySQL & "', Version = '" & Trim(.Version) & "', EDIFormat = " & CInt(.EDIFormat)
                    ' 03.10.2008 Replace ' in JCL with @�@ to be able to save in access
                    sMySQL = sMySQL & ", ReturnMass = " & CInt(.ReturnMass) & ", PreJCL = '" & Replace(Trim(.PreJCL), "'", "@$@") & "', PostJCL = '" & Replace(Trim(.PostJCL), "'", "@$@") & "'"
                    sMySQL = sMySQL & ", TreatSpecial = '" & Trim(.TreatSpecial) & "', AutoMatch = " & CInt(.AutoMatch)
                    sMySQL = sMySQL & ", MatchOCR = " & CInt(.MatchOCR) & ", ShowCorrections= " & CInt(.ShowCorrections)
                    sMySQL = sMySQL & ", PaymentType = '" & .PaymentType & "', Crypt = " & CInt(.Crypt) & ", SplitAfter450 = " & CInt(.SplitAfter450)
                Else
                    sMySQL = sMySQL & "FromAccountingSys = " & Str(.FromAccountingSystem) & ", InfoNewClient= " & Str(.InfoNewClient)
                    sMySQL = sMySQL & ", KeepBatch = " & Str(.KeepBatch)
                    sMySQL = sMySQL & ", CtrlNegative = " & Str(.CtrlNegative) & ", CtrlZip= " & Str(.CtrlZip)
                    sMySQL = sMySQL & ", CtrlAccount = " & Str(.CtrlAccount) & ", CtrlKID= " & Str(.CtrlKID)
                    sMySQL = sMySQL & ", ChangeDateDue = " & Str(.ChangeDateDue) & ", ChangeDateAll= " & Str(.ChangeDateAll)
                    sMySQL = sMySQL & ", RejectWrong = " & Str(.RejectWrong) & ", Accumulation= " & Str(.Accumulation)
                    sMySQL = sMySQL & ", ClientToScreen = " & Str(.ClientToScreen) & ", ClientToPrint= " & Str(.ClientToPrint)
                    sMySQL = sMySQL & ", ClientToFile = " & Str(.ClientToFile) & ", ClientToMail= " & Str(.ClientToMail)
                    sMySQL = sMySQL & ", ReportToPrint1 = " & Str(.ReportToPrint1) & ", ReportToPrint2= " & Str(.ReportToPrint2)
                    sMySQL = sMySQL & ", ReportToPrint3 = " & Str(.ReportToPrint3) & ", ReportToScreen1= " & Str(.ReportToScreen1)
                    sMySQL = sMySQL & ", ReportToScreen2 = " & Str(.ReportToScreen2) & ", ReportToScreen3= " & Str(.ReportToScreen3)
                    sMySQL = sMySQL & ", Message = '" & Trim(.Message)
                    sMySQL = sMySQL & "', Version = '" & Trim(.Version) & "', EDIFormat = " & Str(.EDIFormat)
                    ' 03.10.2008 Replace ' in JCL with @�@ to be able to save in access
                    sMySQL = sMySQL & ", ReturnMass = " & Str(.ReturnMass) & ", PreJCL = '" & Replace(Trim(.PreJCL), "'", "@$@") & "', PostJCL = '" & Replace(Trim(.PostJCL), "'", "@$@") & "'"
                    sMySQL = sMySQL & ", TreatSpecial = '" & Trim(.TreatSpecial) & "', AutoMatch = " & Str(.AutoMatch)
                    sMySQL = sMySQL & ", MatchOCR = " & Str(.MatchOCR) & ", ShowCorrections= " & Str(.ShowCorrections)
                    sMySQL = sMySQL & ", PaymentType = '" & .PaymentType & "', Crypt = " & Str(.Crypt) & ", SplitAfter450 = " & Str(.SplitAfter450)
                End If
                sMySQL = sMySQL & ", FilenameOut4 = '" & Trim(.FilenameOut4) & "', FormatOutID1 = " & .FormatOutID1
                sMySQL = sMySQL & ", FormatOutID2 = " & .FormatOutID2 & ", FormatOutID3 = " & .FormatOutID3
                If bSQLServer Then
                    sMySQL = sMySQL & ", FormatOutID4 = " & .FormatOutID4 & ", MATCHOutGL = " & CInt(.MATCHOutGL)
                    sMySQL = sMySQL & ", MATCHOutOCR = " & CInt(.MATCHOutOCR) & ", MATCHOutAutogiro = " & CInt(.MATCHOutAutogiro)
                    sMySQL = sMySQL & ", MATCHOutRest = " & CInt(.MATCHOutRest) & ", DelDays = " & .DelDays
                    sMySQL = sMySQL & ", ReportAllPayments = " & CInt(.ReportAllPayments) & ", MergePayments = " & CInt(.MergePayments)
                    sMySQL = sMySQL & ", NotificationMail = " & CInt(.NotificationMail) & ", NotificationPrint = " & CInt(.NotificationPrint)
                    sMySQL = sMySQL & ", RedoWrong = " & CInt(.RedoWrong)
                Else
                    sMySQL = sMySQL & ", FormatOutID4 = " & .FormatOutID4 & ", MATCHOutGL = " & .MATCHOutGL
                    sMySQL = sMySQL & ", MATCHOutOCR = " & .MATCHOutOCR & ", MATCHOutAutogiro = " & .MATCHOutAutogiro
                    sMySQL = sMySQL & ", MATCHOutRest = " & .MATCHOutRest & ", DelDays = " & .DelDays
                    sMySQL = sMySQL & ", ReportAllPayments = " & .ReportAllPayments & ", MergePayments = " & .MergePayments
                    sMySQL = sMySQL & ", NotificationMail = " & .NotificationMail & ", NotificationPrint = " & .NotificationPrint
                    sMySQL = sMySQL & ", RedoWrong = " & Str(.RedoWrong)
                End If
                sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID = "
                sMySQL = sMySQL & Str(CheckFilesetupChanges((oFilesetup.FileSetup_ID)))
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

                'FilesSetup2
                sMySQL = "UPDATE FileSetup2 SET CodeNB = " & Trim(CStr(.CodeNB)) & ", ExplanationNB = '" & Trim(.ExplanationNB)
                sMySQL = sMySQL & "', StructText = '" & Trim(.StructText) & "', StructInvoicePos = " & Str(.StructInvoicePos)
                sMySQL = sMySQL & ", StructTextNegative = '" & Trim(.StructTextNegative) & "', StructInvoicePosNegative = " & Str(.StructInvoicePosNegative)
                If bSQLServer Then
                    sMySQL = sMySQL & ", SplitOCRandFBO = " & CInt(.SplitOCRandFBO) & ", BackupPath = '" & Trim(.BackupPath) & "'"
                    sMySQL = sMySQL & ", ExtractInfoFromKID = " & CInt(.ExtractInfofromKID) & ", AddDays = " & CInt(.AddDays)
                    sMySQL = sMySQL & ", ChargeMeDomestic = " & CInt(.ChargeMeDomestic) & ", ChargeMeAbroad = " & CInt(.ChargeMeAbroad)
                    sMySQL = sMySQL & ", ChargesFromSetup = " & CInt(.ChargesFromSetup)
                    sMySQL = sMySQL & ", MappingFileName = '" & Trim(.MappingFileName) & "'"
                    sMySQL = sMySQL & ", UseSignalFile = " & CInt(.UseSignalFile) & ", FileNameSignalfile = '" & Trim(.FilenameSignalFile) & "'"
                    sMySQL = sMySQL & ", ValidateFormatAtImport = " & CInt(.ValidateFormatAtImport) & ", Sorting = " & Str(.Sorting)
                Else
                    sMySQL = sMySQL & ", SplitOCRandFBO = " & Str(.SplitOCRandFBO) & ", BackupPath = '" & Trim(.BackupPath) & "'"
                    sMySQL = sMySQL & ", ExtractInfoFromKID = " & Str(.ExtractInfofromKID) & ", AddDays = " & Str(.AddDays)
                    sMySQL = sMySQL & ", ChargeMeDomestic = " & .ChargeMeDomestic & ", ChargeMeAbroad = " & .ChargeMeAbroad
                    sMySQL = sMySQL & ", ChargesFromSetup = " & .ChargesFromSetup
                    sMySQL = sMySQL & ", MappingFileName = '" & Trim(.MappingFileName) & "'"
                    sMySQL = sMySQL & ", UseSignalFile = " & Str(.UseSignalFile) & ", FileNameSignalfile = '" & Trim(.FilenameSignalFile) & "'"
                    sMySQL = sMySQL & ", ValidateFormatAtImport = " & Str(.ValidateFormatAtImport) & ", Sorting = " & Str(.Sorting)
                End If
                ' XNET 13.12.2010 added SpecialMarker
                sMySQL = sMySQL & ", SpecialMarker = '" & Trim(.SpecialMarker) & "' "
                ' XNET 03.07.2012 added CurrencyToUse
                sMySQL = sMySQL & ", CurrencyToUse = '" & Trim(.CurrencyToUse) & "' "
                ' XNET 03.09.2012 added Division2
                sMySQL = sMySQL & ", Division2 = '" & Trim(.Division2) & "' "
                If bSQLServer Then
                    ' XNET 03.09.2012 added TelepayDebitNO
                    sMySQL = sMySQL & ", TelepayDebitNO = " & CInt(.TelepayDebitNO) & " "
                    ' XNET 06.11.2012 added CodeNBCurrencyOnly
                    sMySQL = sMySQL & ", CodeNBCurrencyOnly = " & CInt(.CodeNBCurrencyOnly) & " "
                    ' XokNET 23.06.2014 added next
                    sMySQL = sMySQL & ", StandardPurposeCode = '" & Left$(Trim$(.StandardPurposeCode), 4) & "'"
                    sMySQL = sMySQL & ", SplitFiles = " & CInt(.SplitFiles)
                    sMySQL = sMySQL & ", ISO20022 = " & CInt(.ISO20022)
                Else
                    ' XNET 03.09.2012 added TelepayDebitNO
                    sMySQL = sMySQL & ", TelepayDebitNO = " & Trim(.TelepayDebitNO) & " "
                    ' XNET 06.11.2012 added CodeNBCurrencyOnly
                    sMySQL = sMySQL & ", CodeNBCurrencyOnly = " & Trim(.CodeNBCurrencyOnly) & " "
                    ' XokNET 23.06.2014 added next
                    sMySQL = sMySQL & ", StandardPurposeCode = '" & Left$(Trim$(.StandardPurposeCode), 4) & "'"
                    sMySQL = sMySQL & ", SplitFiles = " & .SplitFiles
                    sMySQL = sMySQL & ", ISO20022 = " & .ISO20022
                End If
                ' 05.01.2017 Funker dette med Boolske likt i SQL server og Access?
                ' 05.01.2017 Security
                sMySQL = sMySQL & ", secFileNamePublicKey = '" & Trim(.secFileNamePublicKey) & "' "
                sMySQL = sMySQL & ", secHash = '" & Trim(.secHash) & "' "
                sMySQL = sMySQL & ", secCypher = '" & Trim(.secCypher) & "' "
                sMySQL = sMySQL & ", secEncrypt = " & CInt(.secEncrypt)
                sMySQL = sMySQL & ", secSign = " & CInt(.secSign)
                sMySQL = sMySQL & ", secSecureEnvelope = " & CInt(.secSecureEnvelope)
                sMySQL = sMySQL & ", secFileNameBankPublicKey = '" & Trim(.secFileNameBankPublicKey) & "' "
                sMySQL = sMySQL & ", secCompressionType = '" & Trim(.secCompression) & "' "
                sMySQL = sMySQL & ", secPublicKey = '" & Trim(.secOurPublicKey) & "' "
                sMySQL = sMySQL & ", secPrivateKey = '" & Trim(.secOurPrivateKey) & "' "
                sMySQL = sMySQL & ", secKeyCreationDate = '" & Trim(.secKeyCreationDate) & "' "
                sMySQL = sMySQL & ", secUserID = '" & Trim(.secUserID) & "' "
                sMySQL = sMySQL & ", secPassword = '" & Trim(.secPassword) & "' "
                sMySQL = sMySQL & ", secBANKPublicKey = '" & Trim(.secBANKPublicKey) & "' "
                sMySQL = sMySQL & ", FileEncoding = " & .FileEncoding & " "
                sMySQL = sMySQL & ", secConfidential = " & CInt(.secConfidential)
                sMySQL = sMySQL & ", FileNameSignalfileError = '" & .FilenameSignalFileError.Trim & "'" '16.05.2022
                sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID2 = "
                sMySQL = sMySQL & Str(CheckFilesetupChanges((oFilesetup.FileSetup_ID)))
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

            End With

            ' Added 08.05.06
            ' Must check if filesetup_id is changed, and if so, update
            ' - BabelFile
            ' - ImportedFiles
            ' - ImportInformation
            If oFilesetup.FileSetup_ID <> CheckFilesetupChanges((oFilesetup.FileSetup_ID)) Then
                sMySQL = "UPDATE BabelFile SET Filesetup_ID = " & CheckFilesetupChanges((oFilesetup.FileSetup_ID))
                sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID = "
                sMySQL = sMySQL & Str(oFilesetup.FileSetup_ID)
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

                sMySQL = "UPDATE ImportedFiles SET Filesetup_ID = " & CheckFilesetupChanges((oFilesetup.FileSetup_ID))
                sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID = "
                sMySQL = sMySQL & Str(oFilesetup.FileSetup_ID)
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

                ' NB In ImportInformation Filesetup_ID is Char !!
                sMySQL = "UPDATE ImportInformation SET Filesetup_ID = '" & CheckFilesetupChanges((oFilesetup.FileSetup_ID))
                sMySQL = sMySQL & "' WHERE Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID = '"
                sMySQL = sMySQL & Trim(Str(oFilesetup.FileSetup_ID)) & "'"
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()

            End If
        End If

        oFilesetup.Status = CollectionStatus.NoChange

        SaveFilesetup = bContinue

    End Function
    'Private Function SaveFilesetup_Old(ByRef iCompanyNo As Short, ByRef oFilesetup As FileSetup, ByRef myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand) As Boolean
    '    Dim sMySQL As String
    '    Dim bUpdate, bDelete, bNew As Boolean
    '    Dim bContinue As Boolean

    '    'This variable is used to reduce all referances to the FileSetupID with its content.
    '    bContinue = False
    '    bDelete = False
    '    bUpdate = False
    '    bNew = False

    '    Select Case oFilesetup.Status
    '        Case CollectionStatus.NoChange
    '            'No action, next Filesetup
    '        Case CollectionStatus.SeqNoChange
    '            sMySQL = "UPDATE FileSetup SET SeqNoTotal = " & oFilesetup.SeqNoTotal
    '            sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID = "
    '            sMySQL = sMySQL & Str(oFilesetup.FileSetup_ID)
    '            myBBDB_AccessCommand.CommandText = sMySQL
    '            myBBDB_AccessCommand.ExecuteNonQuery()
    '        Case CollectionStatus.ChangeUnder
    '            bContinue = True
    '        Case CollectionStatus.Changed
    '            bUpdate = True
    '            bContinue = True
    '        Case CollectionStatus.NewCol
    '            bNew = True
    '            bUpdate = True
    '            bContinue = True
    '        Case CollectionStatus.Deleted
    '            bDelete = True
    '    End Select


    '    If bDelete Then
    '        'FIXED: (Temporarely by JanP 04.10.01, load new profile when saved,
    '        ' this way the deleted items will disappear
    '        'FileSetups.Remove (oFileSetup.FileSetup_ID)
    '    End If

    '    If bNew Then
    '        sMySQL = "INSERT INTO FileSetup(Company_ID,FileSetup_ID,Format_ID,Bank_ID) VALUES("
    '        sMySQL = sMySQL & Str(iCompanyNo) & ", " & Str(CheckFilesetupChanges((oFilesetup.FileSetup_ID)))
    '        sMySQL = sMySQL & ", " & Str(oFilesetup.Format_ID) & ", " & Str(oFilesetup.Bank_ID) & ")"
    '        myBBDB_AccessCommand.CommandText = sMySQL
    '        myBBDB_AccessCommand.ExecuteNonQuery()
    '        sMySQL = "INSERT INTO FileSetup2(Company_ID,FileSetup_ID2) VALUES("
    '        sMySQL = sMySQL & Str(iCompanyNo) & ", " & Str(CheckFilesetupChanges((oFilesetup.FileSetup_ID))) & ")"
    '        myBBDB_AccessCommand.CommandText = sMySQL
    '        myBBDB_AccessCommand.ExecuteNonQuery()
    '    End If

    '    If bUpdate Then
    '        With oFilesetup

    '            sMySQL = "UPDATE FileSetup SET Format_ID = " & .Format_ID & ",EnumID = " & .Enum_ID
    '            sMySQL = sMySQL & ",Bank_ID = " & .Bank_ID & ", ShortName= '" & Trim(.ShortName)
    '            sMySQL = sMySQL & "', Silent = " & Str(.Silent)
    '            sMySQL = sMySQL & ", Log = " & Str(.Log) & ", LogType = " & Str(.LogType)
    '            sMySQL = sMySQL & ", LogFilename = '" & Trim(.LogFilename) & "', LogOverwrite = " & Str(.LogOverwrite)
    '            sMySQL = sMySQL & ", LogShowStartStop = " & Str(.LogShowStartStop) & ", LogMaxLines = " & Str(.LogMaxLines)
    '            sMySQL = sMySQL & ", LogDetails = " & Str(.LogDetails)
    '            sMySQL = sMySQL & ", Description = '" & Trim(.Description) & "', FileSetupOut= " & CheckFilesetupChanges(.FileSetupOut)
    '            sMySQL = sMySQL & ", FileNameIn1 = '" & Trim(.FileNameIn1) & "', FileNameIn2= '" & Trim(.FileNameIn2)
    '            sMySQL = sMySQL & "', FileNameIn3 = '" & Trim(.FileNameIn3) & "', BackupIn= " & Str(.BackupIn) & ", Manual = " & Str(.Manual)
    '            sMySQL = sMySQL & ", OverWriteIn = " & Str(.OverWriteIn) & ", WarningIn= " & Str(.WarningIn)
    '            sMySQL = sMySQL & ", FileNameOut1 = '" & Trim(.FileNameOut1) & "', FileNameOut2= '" & Trim(.FileNameOut2)
    '            sMySQL = sMySQL & "', FileNameOut3 = '" & Trim(.FileNameOut3) & "', BackupOut= " & Str(.BackupOut)
    '            sMySQL = sMySQL & ", OverWriteOut = " & Str(.OverWriteOut) & ", WarningOut= " & Str(.WarningOut) & ", RemoveOldOut= " & Str(.RemoveOldOut)

    '            sMySQL = sMySQL & ", NotSplitOnAccount = " & Str(.NotSplitOnAccount)
    '            sMySQL = sMySQL & ", SeqNoType = " & .SeqnoType & ", SeqNoTotal= " & .SeqNoTotal
    '            sMySQL = sMySQL & ", SeqNoDay = " & .SeqNoDay & ", Division= '" & .Division
    '            'Use CompanyNo from FileSetup if filled in, else from profile
    '            If .CompanyNo <> "" Then
    '                sMySQL = sMySQL & "', CompanyNo = '" & .CompanyNo
    '            Else
    '                sMySQL = sMySQL & "', CompanyNo = '" & sCompanyNo
    '            End If

    '            If .AdditionalNo <> "" Then
    '                sMySQL = sMySQL & "', AdditionalNo = '" & .AdditionalNo & "'"
    '            Else
    '                sMySQL = sMySQL & "', AdditionalNo = '" & sAdditionalNo & "'"
    '            End If

    '            sMySQL = sMySQL & " where Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID = "
    '            sMySQL = sMySQL & Str(CheckFilesetupChanges((oFilesetup.FileSetup_ID)))
    '            myBBDB_AccessCommand.CommandText = sMySQL
    '            myBBDB_AccessCommand.ExecuteNonQuery()

    '            sMySQL = "UPDATE FileSetup SET "
    '            sMySQL = sMySQL & "FromAccountingSys = " & Str(.FromAccountingSystem) & ", InfoNewClient= " & Str(.InfoNewClient)
    '            sMySQL = sMySQL & ", KeepBatch = " & Str(.KeepBatch)
    '            sMySQL = sMySQL & ", CtrlNegative = " & Str(.CtrlNegative) & ", CtrlZip= " & Str(.CtrlZip)
    '            sMySQL = sMySQL & ", CtrlAccount = " & Str(.CtrlAccount) & ", CtrlKID= " & Str(.CtrlKID)
    '            sMySQL = sMySQL & ", ChangeDateDue = " & Str(.ChangeDateDue) & ", ChangeDateAll= " & Str(.ChangeDateAll)
    '            sMySQL = sMySQL & ", RejectWrong = " & Str(.RejectWrong) & ", Accumulation= " & Str(.Accumulation)
    '            sMySQL = sMySQL & ", ClientToScreen = " & Str(.ClientToScreen) & ", ClientToPrint= " & Str(.ClientToPrint)
    '            sMySQL = sMySQL & ", ClientToFile = " & Str(.ClientToFile) & ", ClientToMail= " & Str(.ClientToMail)
    '            sMySQL = sMySQL & ", ReportToPrint1 = " & Str(.ReportToPrint1) & ", ReportToPrint2= " & Str(.ReportToPrint2)
    '            sMySQL = sMySQL & ", ReportToPrint3 = " & Str(.ReportToPrint3) & ", ReportToScreen1= " & Str(.ReportToScreen1)
    '            sMySQL = sMySQL & ", ReportToScreen2 = " & Str(.ReportToScreen2) & ", ReportToScreen3= " & Str(.ReportToScreen3)
    '            sMySQL = sMySQL & ", Message = '" & Trim(.Message)
    '            sMySQL = sMySQL & "', Version = '" & Trim(.Version) & "', EDIFormat = " & Str(.EDIFormat)
    '            ' 03.10.2008 Replace ' in JCL with @�@ to be able to save in access
    '            sMySQL = sMySQL & ", ReturnMass = " & Str(.ReturnMass) & ", PreJCL = '" & Replace(Trim(.PreJCL), "'", "@$@") & "', PostJCL = '" & Replace(Trim(.PostJCL), "'", "@$@") & "'"
    '            sMySQL = sMySQL & ", TreatSpecial = '" & Trim(.TreatSpecial) & "', AutoMatch = " & Str(.AutoMatch)
    '            sMySQL = sMySQL & ", MatchOCR = " & Str(.MatchOCR) & ", ShowCorrections= " & Str(.ShowCorrections)
    '            sMySQL = sMySQL & ", PaymentType = '" & .PaymentType & "', Crypt = " & Str(.Crypt) & ", SplitAfter450 = " & Str(.SplitAfter450)
    '            sMySQL = sMySQL & ", FilenameOut4 = '" & Trim(.FilenameOut4) & "', FormatOutID1 = " & .FormatOutID1
    '            sMySQL = sMySQL & ", FormatOutID2 = " & .FormatOutID2 & ", FormatOutID3 = " & .FormatOutID3
    '            sMySQL = sMySQL & ", FormatOutID4 = " & .FormatOutID4 & ", MATCHOutGL = " & .MATCHOutGL
    '            sMySQL = sMySQL & ", MATCHOutOCR = " & .MATCHOutOCR & ", MATCHOutAutogiro = " & .MATCHOutAutogiro
    '            sMySQL = sMySQL & ", MATCHOutRest = " & .MATCHOutRest & ", DelDays = " & .DelDays
    '            sMySQL = sMySQL & ", ReportAllPayments = " & .ReportAllPayments & ", MergePayments = " & .MergePayments
    '            sMySQL = sMySQL & ", NotificationMail = " & .NotificationMail & ", NotificationPrint = " & .NotificationPrint
    '            sMySQL = sMySQL & ", RedoWrong = " & Str(.RedoWrong)
    '            sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID = "
    '            sMySQL = sMySQL & Str(CheckFilesetupChanges((oFilesetup.FileSetup_ID)))
    '            myBBDB_AccessCommand.CommandText = sMySQL
    '            myBBDB_AccessCommand.ExecuteNonQuery()

    '            'FilesSetup2
    '            sMySQL = "UPDATE FileSetup2 SET CodeNB = " & Trim(CStr(.CodeNB)) & ", ExplanationNB = '" & Trim(.ExplanationNB)
    '            sMySQL = sMySQL & "', StructText = '" & Trim(.StructText) & "', StructInvoicePos = " & Str(.StructInvoicePos)
    '            sMySQL = sMySQL & ", StructTextNegative = '" & Trim(.StructTextNegative) & "', StructInvoicePosNegative = " & Str(.StructInvoicePosNegative)
    '            sMySQL = sMySQL & ", SplitOCRandFBO = " & Str(.SplitOCRandFBO) & ", BackupPath = '" & Trim(.BackupPath) & "'"
    '            sMySQL = sMySQL & ", ExtractInfoFromKID = " & Str(.ExtractInfofromKID) & ", AddDays = " & Str(.AddDays)
    '            sMySQL = sMySQL & ", ChargeMeDomestic = " & .ChargeMeDomestic & ", ChargeMeAbroad = " & .ChargeMeAbroad
    '            sMySQL = sMySQL & ", ChargesFromSetup = " & .ChargesFromSetup
    '            sMySQL = sMySQL & ", MappingFileName = '" & Trim(.MappingFileName) & "'"
    '            sMySQL = sMySQL & ", UseSignalFile = " & Str(.UseSignalFile) & ", FileNameSignalfile = '" & Trim(.FilenameSignalFile) & "'"
    '            sMySQL = sMySQL & ", ValidateFormatAtImport = " & Str(.ValidateFormatAtImport) & ", Sorting = " & Str(.Sorting)
    '            ' XNET 13.12.2010 added SpecialMarker
    '            sMySQL = sMySQL & ", SpecialMarker = '" & Trim$(.SpecialMarker) & "' "
    '            sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID2 = "
    '            sMySQL = sMySQL & Str(CheckFilesetupChanges((oFilesetup.FileSetup_ID)))
    '            myBBDB_AccessCommand.CommandText = sMySQL
    '            myBBDB_AccessCommand.ExecuteNonQuery()

    '        End With

    '        ' Added 08.05.06
    '        ' Must check if filesetup_id is changed, and if so, update
    '        ' - BabelFile
    '        ' - ImportedFiles
    '        ' - ImportInformation
    '        If oFilesetup.FileSetup_ID <> CheckFilesetupChanges((oFilesetup.FileSetup_ID)) Then
    '            sMySQL = "UPDATE BabelFile SET Filesetup_ID = " & CheckFilesetupChanges((oFilesetup.FileSetup_ID))
    '            sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID = "
    '            sMySQL = sMySQL & Str(oFilesetup.FileSetup_ID)
    '            myBBDB_AccessCommand.CommandText = sMySQL
    '            myBBDB_AccessCommand.ExecuteNonQuery()

    '            sMySQL = "UPDATE ImportedFiles SET Filesetup_ID = " & CheckFilesetupChanges((oFilesetup.FileSetup_ID))
    '            sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID = "
    '            sMySQL = sMySQL & Str(oFilesetup.FileSetup_ID)
    '            myBBDB_AccessCommand.CommandText = sMySQL
    '            myBBDB_AccessCommand.ExecuteNonQuery()

    '            ' NB In ImportInformation Filesetup_ID is Char !!
    '            sMySQL = "UPDATE ImportInformation SET Filesetup_ID = '" & CheckFilesetupChanges((oFilesetup.FileSetup_ID))
    '            sMySQL = sMySQL & "' WHERE Company_ID = " & Str(iCompanyNo) & " AND FileSetup_ID = '"
    '            sMySQL = sMySQL & Trim(Str(oFilesetup.FileSetup_ID)) & "'"
    '            myBBDB_AccessCommand.CommandText = sMySQL
    '            myBBDB_AccessCommand.ExecuteNonQuery()

    '        End If
    '    End If

    '    oFilesetup.Status = CollectionStatus.NoChange

    '    SaveFilesetup_Old = bContinue

    'End Function
    Private Function SaveClient(ByRef iCompanyNo As Short, ByRef oClient As Client, ByRef iFileSetup_ID As Short, ByRef myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand, ByVal myBBDB_AccessClientReader As System.Data.OleDb.OleDbDataReader, ByVal bSQLServer As Boolean) As Boolean
        Dim sMySQL As String
        Dim iNoOfRecords As Short
        Dim bNewClient, bDelete, bUpdate, bNewClientFormat As Boolean
        Dim bContinue As Boolean

        bContinue = False
        bDelete = False
        bUpdate = False
        bNewClient = False
        bNewClientFormat = False
        sMySQL = ""

        Select Case oClient.Status
            Case CollectionStatus.NoChange
                'No action, next Client
            Case CollectionStatus.SeqNoChange
                sMySQL = "UPDATE ClientFormat SET SeqNoTotal = " & Trim(Str(oClient.SeqNoTotal))
                sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND ClientFormat_ID = "
                sMySQL = sMySQL & Str(oClient.ClientFormat_ID)
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()
            Case CollectionStatus.ChangeUnder
                bContinue = True
            Case CollectionStatus.Changed
                bUpdate = True
                bContinue = True
            Case CollectionStatus.NewCol
                'Have to check if the Client already exists in the database, because of the
                ' differences between the strusture of the database and the collections
                sMySQL = "SELECT FormatIn, FormatOut FROM ClientFormat WHERE Company_ID = " & Str(iCompanyNo)
                sMySQL = sMySQL & " AND Client_ID = " & oClient.Client_ID
                iNoOfRecords = 0
                'Assume that there doesn't exist a correct record in the ClientFormat-table
                bNewClientFormat = True

                If Not myBBDB_AccessClientReader Is Nothing Then
                    If Not myBBDB_AccessClientReader.IsClosed Then
                        myBBDB_AccessClientReader.Close()
                    End If
                End If

                If myBBDB_AccessCommand Is Nothing Then
                    myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand()
                    myBBDB_AccessCommand.CommandType = CommandType.Text
                    myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
                End If

                myBBDB_AccessCommand.CommandText = sMySQL

                myBBDB_AccessClientReader = myBBDB_AccessCommand.ExecuteReader

                If myBBDB_AccessClientReader.HasRows Then
                    Do While myBBDB_AccessClientReader.Read
                        iNoOfRecords = iNoOfRecords + 1
                        'If the client exists we have to check if the same combination of FormatIn and
                        ' FormatOut ID's exist in the clientformat table. If not we have to add
                        ' a new record in the CF-table but not a new client.
                        If myBBDB_AccessClientReader.GetInt32(0) = CheckFilesetupChanges((oClient.FormatIn)) Then 'FormatIn
                            If myBBDB_AccessClientReader.GetInt32(1) = CheckFilesetupChanges((oClient.FormatOut)) Then 'FormatOut
                                bNewClientFormat = False
                            End If
                        End If
                    Loop
                End If


                ' ADDED 15.06.2012
                myBBDB_AccessClientReader.Close()

                If iNoOfRecords = 0 Then
                    bNewClient = True
                    bNewClientFormat = True
                    bUpdate = True
                    bContinue = True
                Else
                    bUpdate = True
                    bContinue = True
                End If
            Case CollectionStatus.Deleted
                bDelete = True
        End Select


        If bDelete Then
            FileSetups(iFileSetup_ID).Clients.Remove(oClient.Index)
        End If

        If bNewClient Then
            sMySQL = "INSERT INTO Client(Company_ID, Client_ID) VALUES("
            sMySQL = sMySQL & Str(iCompanyNo) & ", " & Str(oClient.Client_ID)
            sMySQL = sMySQL & ")"
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()
        End If
        If bNewClientFormat Then
            If 1 = 1 Then 'Not RunTime() Then 'SKOLLEBOLLE
                sMySQL = "INSERT INTO ClientFormat(Company_ID, ClientFormat_ID, Client_ID) VALUES("
                sMySQL = sMySQL & Str(iCompanyNo) & ", " & Str(oClient.ClientFormat_ID) & ", " & Str(oClient.Client_ID)
                sMySQL = sMySQL & ")"
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()
            Else
                sMySQL = "INSERT INTO ClientFormat(Company_ID, ClientFormat_ID) VALUES("
                sMySQL = sMySQL & Str(iCompanyNo) & ", " & Str(oClient.ClientFormat_ID)
                sMySQL = sMySQL & ")"
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()
            End If
        End If

        If bUpdate Then
            With oClient
                sMySQL = "UPDATE Client SET ClientNo = '" & .ClientNo & "', Name= '" & Replace(.Name, "'", "")
                sMySQL = sMySQL & "', City = '" & Replace(.City, "'", "") & "', CountryCode = '" & .CountryCode
                sMySQL = sMySQL & "', CompNo = '" & .CompNo & "', Division = '" & .Division
                sMySQL = sMySQL & "', Notification1 = " & .Notification1
                sMySQL = sMySQL & ", Notification2 = " & .Notification2 & ", Notification3= " & .Notification3
                sMySQL = sMySQL & ", ImportKID1 = '" & .ImportKID1
                sMySQL = sMySQL & "', ImportKID2 = '" & .ImportKID2 & "', ImportKID3= '" & .ImportKID3
                sMySQL = sMySQL & "', ExportKID = '" & .ExportKID
                sMySQL = sMySQL & "', NoMatchAccount = '" & .NoMatchAccount & "', NoMatchAccountType= " & .NoMatchAccountType
                sMySQL = sMySQL & ", RoundingsAccount = '" & .RoundingsAccount & "', RoundingsAccountType= " & .RoundingsAccountType
                sMySQL = sMySQL & ", ChargesAccount = '" & .ChargesAccount & "', ChargesAccountType= " & .ChargesAccountType
                sMySQL = sMySQL & ", AKontoPattern = '" & .AkontoPattern & "', AKontoPatternType= " & .AkontoPatternType
                sMySQL = sMySQL & ", MatchPattern = '" & .MatchPattern & "', MatchPatternType= " & .MatchPatternType
                sMySQL = sMySQL & ", GLPattern = '" & .GLPattern & "', VoucherNo = '" & .VoucherNo & "', VoucherType = '" & .VoucherType & "',GLPatternType= " & .GLPatternType
                sMySQL = sMySQL & ", VoucherNo2 = '" & .VoucherNo2 & "', VoucherType2 = '" & .VoucherType2
                sMySQL = sMySQL & "',HowToAddVoucherNo = " & .HowToAddVoucherNo
                If bSQLServer Then
                    sMySQL = sMySQL & ",AddVoucherNoBeforeOCR = " & CInt(.AddVoucherNoBeforeOCR)
                    sMySQL = sMySQL & ",AddCounterToVoucherNo = " & CInt(.AddCounterToVoucherNo)
                Else
                    sMySQL = sMySQL & ",AddVoucherNoBeforeOCR = " & .AddVoucherNoBeforeOCR
                    sMySQL = sMySQL & ",AddCounterToVoucherNo = " & .AddCounterToVoucherNo
                End If
                sMySQL = sMySQL & ", DBProfile_ID = " & Trim(Str(.DBProfile_ID))
                sMySQL = sMySQL & ", BB_Year = " & Trim(Str(.BB_Year))
                sMySQL = sMySQL & ", BB_Period = " & Trim(Str(.BB_Period))
                ' added 09.07.2015
                sMySQL = sMySQL & ", HistoryDelDays = " & Trim$(Str(.HistoryDelDays))
                If bSQLServer Then
                    sMySQL = sMySQL & ", SEPASingle = " & CInt(.SEPASingle)
                    sMySQL = sMySQL & ", EndToEndRefFromBabelBank = " & CInt(.EndToEndRefFromBabelBank)
                Else
                    sMySQL = sMySQL & ", SEPASingle = " & Str(.SEPASingle)
                    sMySQL = sMySQL & ", EndToEndRefFromBabelBank = " & Str(.EndToEndRefFromBabelBank)
                End If
                sMySQL = sMySQL & ", MsgID_Variable = '" & Trim$(.MsgID_Variable) & "'"
                sMySQL = sMySQL & ", ValidationLevel = " & Trim$(Str(.ValidationLevel))
                If bSQLServer Then
                    sMySQL = sMySQL & ", SavePaymentData = " & CInt(.SavePaymentData)
                    sMySQL = sMySQL & ", UseStateBankText = " & CInt(.UseStateBankText)
                    sMySQL = sMySQL & ", AdjustForSchemaValidation = " & CInt(.AdjustForSchemaValidation)
                    sMySQL = sMySQL & ", AdjustToBBANNorway = " & CInt(.AdjustToBBANNorway)
                    sMySQL = sMySQL & ", AdjustToBBANSweden = " & CInt(.AdjustToBBANSweden)
                    sMySQL = sMySQL & ", AdjustToBBANDenmark = " & CInt(.AdjustToBBANDenmark)
                    sMySQL = sMySQL & ", UseDifferentInvoiceAndPaymentCurrency = " & CInt(.UseDifferentInvoiceAndPaymentCurrency)
                Else
                    sMySQL = sMySQL & ", SavePaymentData = " & Str(.SavePaymentData)
                    sMySQL = sMySQL & ", UseStateBankText = " & Str(.UseStateBankText)
                    sMySQL = sMySQL & ", AdjustForSchemaValidation = " & Str(.AdjustForSchemaValidation)
                    sMySQL = sMySQL & ", AdjustToBBANNorway = " & Str(.AdjustToBBANNorway)
                    sMySQL = sMySQL & ", AdjustToBBANSweden = " & Str(.AdjustToBBANSweden)
                    sMySQL = sMySQL & ", AdjustToBBANDenmark = " & Str(.AdjustToBBANDenmark)
                    sMySQL = sMySQL & ", UseDifferentInvoiceAndPaymentCurrency = " & Str(.UseDifferentInvoiceAndPaymentCurrency)
                End If
                ' added 20.02.2017
                sMySQL = sMySQL & ", ISOSplitOCR = " & CInt(.ISOSplitOCR)
                sMySQL = sMySQL & ", ISOSplitOCRWithCredits = " & CInt(.ISOSplitOCRWithCredits)
                sMySQL = sMySQL & ", ISOSRedoFreetextToStruct = " & CInt(.ISOSRedoFreetextToStruct)
                ' added 24.04.2017
                sMySQL = sMySQL & ", ISODoNotGroup = " & CInt(.ISODoNotGroup)
                ' added 28.04.2017
                sMySQL = sMySQL & ", ISORemoveBICInSEPA = " & CInt(.ISORemoveBICInSEPA)
                ' added 25.01.2018
                sMySQL = sMySQL & ", ISOUseTransferCurrency = " & CInt(.ISOUseTransferCurrency)
                ' 28.12.2021
                sMySQL = sMySQL & ", SocialSecurityNo = '" & Trim$(.SocialSecurityNo) & "'"
                sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND Client_ID = "
                sMySQL = sMySQL & Str(.Client_ID)
            End With
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()
            With oClient
                If 1 = 1 Then 'Not RunTime() Then 'SKOLLEBOLLE
                    sMySQL = "UPDATE ClientFormat SET FormatIn = " & CheckFilesetupChanges(.FormatIn)
                    If bSQLServer Then
                        sMySQL = sMySQL & ", Outgoing = " & CInt(.Outgoing) & ", OwnRefText = '" & .OwnRefText
                    Else
                        sMySQL = sMySQL & ", Outgoing = " & Str(.Outgoing) & ", OwnRefText = '" & .OwnRefText
                    End If
                    sMySQL = sMySQL & "', FormatOut = " & CheckFilesetupChanges(.FormatOut)
                    'If this client-object is a part of clients for the FileSetup-object that is used for
                    'export, then we have to update the sequence no as well
                    If .FormatOut = iFileSetup_ID Then
                        sMySQL = sMySQL & ", SeqNoTotal = " & Trim(Str(.SeqNoTotal))
                    End If
                    sMySQL = sMySQL & ", KIDStart = " & .KIDStart & ", KIDLength = " & .KIDLength
                    sMySQL = sMySQL & ", KIDValue = '" & .KIDValue
                    sMySQL = sMySQL & "' where Company_ID = " & Str(iCompanyNo) & " AND ClientFormat_ID = "
                    sMySQL = sMySQL & Str(.ClientFormat_ID) & " AND Client_ID = " & .Client_ID.ToString
                Else
                    sMySQL = "UPDATE ClientFormat SET Client_ID = " & .Client_ID & ", FormatIn= " & CheckFilesetupChanges(.FormatIn)
                    If bSQLServer Then
                        sMySQL = sMySQL & ", Outgoing = " & CInt(.Outgoing) & ", OwnRefText = '" & .OwnRefText
                    Else
                        sMySQL = sMySQL & ", Outgoing = " & Str(.Outgoing) & ", OwnRefText = '" & .OwnRefText
                    End If
                    sMySQL = sMySQL & "', FormatOut = " & CheckFilesetupChanges(.FormatOut)
                    'If this client-object is a part of clients for the FileSetup-object that is used for
                    'export, then we have to update the sequence no as well
                    If .FormatOut = iFileSetup_ID Then
                        sMySQL = sMySQL & ", SeqNoTotal = " & Trim(Str(.SeqNoTotal))
                    End If
                    sMySQL = sMySQL & ", KIDStart = " & .KIDStart & ", KIDLength = " & .KIDLength
                    sMySQL = sMySQL & ", KIDValue = '" & .KIDValue
                    sMySQL = sMySQL & "' where Company_ID = " & Str(iCompanyNo) & " AND ClientFormat_ID = "
                    sMySQL = sMySQL & Str(.ClientFormat_ID)
                End If
            End With
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()
        End If

        oClient.Status = CollectionStatus.NoChange

        If Not myBBDB_AccessClientReader Is Nothing Then
            If Not myBBDB_AccessClientReader.IsClosed Then
                myBBDB_AccessClientReader.Close()
            End If
            myBBDB_AccessClientReader = Nothing
        End If

        SaveClient = bContinue

    End Function
    Private Function SaveClient_old(ByRef iCompanyNo As Short, ByRef oClient As Client, ByRef iFileSetup_ID As Short, ByRef myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand, ByRef myBBDB_AccessSelectCommand As System.Data.OleDb.OleDbCommand) As Boolean
        Dim sMySQL As String
        Dim iNoOfRecords As Short
        Dim bNewClient, bDelete, bUpdate, bNewClientFormat As Boolean
        Dim bContinue As Boolean
        Dim myBBDB_AccessClientReader As System.Data.OleDb.OleDbDataReader = Nothing

        bContinue = False
        bDelete = False
        bUpdate = False
        bNewClient = False
        bNewClientFormat = False
        sMySQL = ""

        Select Case oClient.Status
            Case CollectionStatus.NoChange
                'No action, next Client
            Case CollectionStatus.SeqNoChange
                sMySQL = "UPDATE ClientFormat SET SeqNoTotal = " & Trim(Str(oClient.SeqNoTotal))
                sMySQL = sMySQL & " WHERE Company_ID = " & Str(iCompanyNo) & " AND ClientFormat_ID = "
                sMySQL = sMySQL & Str(oClient.ClientFormat_ID)
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()
            Case CollectionStatus.ChangeUnder
                bContinue = True
            Case CollectionStatus.Changed
                bUpdate = True
                bContinue = True
            Case CollectionStatus.NewCol
                'Have to check if the Client already exists in the database, because of the
                ' differences between the strusture of the database and the collections
                sMySQL = "SELECT FormatIn, FormatOut FROM ClientFormat WHERE Company_ID = " & Str(iCompanyNo)
                sMySQL = sMySQL & " AND Client_ID = " & oClient.Client_ID
                iNoOfRecords = 0
                'Assume that there doesn't exist a correct record in the ClientFormat-table
                bNewClientFormat = True

                If Not myBBDB_AccessClientReader Is Nothing Then
                    If Not myBBDB_AccessClientReader.IsClosed Then
                        myBBDB_AccessClientReader.Close()
                    End If
                End If

                If myBBDB_AccessSelectCommand Is Nothing Then
                    myBBDB_AccessSelectCommand = myBBDB_AccessConnection.CreateCommand()
                    myBBDB_AccessSelectCommand.CommandType = CommandType.Text
                    myBBDB_AccessSelectCommand.Connection = myBBDB_AccessConnection
                End If

                myBBDB_AccessSelectCommand.CommandText = sMySQL

                myBBDB_AccessClientReader = myBBDB_AccessSelectCommand.ExecuteReader

                If myBBDB_AccessClientReader.HasRows Then
                    Do While myBBDB_AccessClientReader.Read
                        iNoOfRecords = iNoOfRecords + 1
                        'If the client exists we have to check if the same combination of FormatIn and
                        ' FormatOut ID's exist in the clientformat table. If not we have to add
                        ' a new record in the CF-table but not a new client.
                        If myBBDB_AccessClientReader.GetInt32(0) = CheckFilesetupChanges((oClient.FormatIn)) Then 'FormatIn
                            If myBBDB_AccessClientReader.GetInt32(1) = CheckFilesetupChanges((oClient.FormatOut)) Then 'FormatOut
                                bNewClientFormat = False
                            End If
                        End If
                    Loop
                End If
                If iNoOfRecords = 0 Then
                    bNewClient = True
                    bNewClientFormat = True
                    bUpdate = True
                    bContinue = True
                Else
                    bUpdate = True
                    bContinue = True
                End If
            Case CollectionStatus.Deleted
                bDelete = True
        End Select


        If bDelete Then
            FileSetups(iFileSetup_ID).Clients.Remove(oClient.Index)
        End If

        If bNewClient Then
            sMySQL = "INSERT INTO Client(Company_ID, Client_ID) VALUES("
            sMySQL = sMySQL & Str(iCompanyNo) & ", " & Str(oClient.Client_ID)
            sMySQL = sMySQL & ")"
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()
        End If
        If bNewClientFormat Then
            sMySQL = "INSERT INTO ClientFormat(Company_ID, ClientFormat_ID) VALUES("
            sMySQL = sMySQL & Str(iCompanyNo) & ", " & Str(oClient.ClientFormat_ID)
            sMySQL = sMySQL & ")"
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()
        End If

        If bUpdate Then
            With oClient
                sMySQL = "UPDATE Client SET ClientNo = '" & .ClientNo & "', Name= '" & Replace(.Name, "'", "")
                sMySQL = sMySQL & "', City = '" & Replace(.City, "'", "") & "', CountryCode = '" & .CountryCode
                sMySQL = sMySQL & "', CompNo = '" & .CompNo & "', Division = '" & .Division
                sMySQL = sMySQL & "', Notification1 = " & .Notification1
                sMySQL = sMySQL & ", Notification2 = " & .Notification2 & ", Notification3= " & .Notification3
                sMySQL = sMySQL & ", ImportKID1 = '" & .ImportKID1
                sMySQL = sMySQL & "', ImportKID2 = '" & .ImportKID2 & "', ImportKID3= '" & .ImportKID3
                sMySQL = sMySQL & "', ExportKID = '" & .ExportKID
                sMySQL = sMySQL & "', NoMatchAccount = '" & .NoMatchAccount & "', NoMatchAccountType= " & .NoMatchAccountType
                sMySQL = sMySQL & ", RoundingsAccount = '" & .RoundingsAccount & "', RoundingsAccountType= " & .RoundingsAccountType
                sMySQL = sMySQL & ", ChargesAccount = '" & .ChargesAccount & "', ChargesAccountType= " & .ChargesAccountType
                sMySQL = sMySQL & ", AKontoPattern = '" & .AkontoPattern & "', AKontoPatternType= " & .AkontoPatternType
                sMySQL = sMySQL & ", MatchPattern = '" & .MatchPattern & "', MatchPatternType= " & .MatchPatternType
                sMySQL = sMySQL & ", GLPattern = '" & .GLPattern & "', VoucherNo = '" & .VoucherNo & "', VoucherType = '" & .VoucherType & "',GLPatternType= " & .GLPatternType
                sMySQL = sMySQL & ", VoucherNo2 = '" & .VoucherNo2 & "', VoucherType2 = '" & .VoucherType2
                sMySQL = sMySQL & "',HowToAddVoucherNo = " & .HowToAddVoucherNo
                sMySQL = sMySQL & ",AddVoucherNoBeforeOCR = " & .AddVoucherNoBeforeOCR
                sMySQL = sMySQL & ",AddCounterToVoucherNo = " & .AddCounterToVoucherNo
                sMySQL = sMySQL & ", DBProfile_ID = " & Trim(Str(.DBProfile_ID))
                sMySQL = sMySQL & ", BB_Year = " & Trim(Str(.BB_Year))
                sMySQL = sMySQL & ", BB_Period = " & Trim(Str(.BB_Period))
                sMySQL = sMySQL & " where Company_ID = " & Str(iCompanyNo) & " AND Client_ID = "
                sMySQL = sMySQL & Str(.Client_ID)
            End With
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()
            With oClient
                sMySQL = "UPDATE ClientFormat SET Client_ID = " & .Client_ID & ", FormatIn= " & CheckFilesetupChanges(.FormatIn)
                sMySQL = sMySQL & ", Outgoing = " & Str(.Outgoing) & ", OwnRefText = '" & .OwnRefText
                sMySQL = sMySQL & "', FormatOut = " & CheckFilesetupChanges(.FormatOut)
                'If this client-object is a part of clients for the FileSetup-object that is used for
                'export, then we have to update the sequence no as well
                If .FormatOut = iFileSetup_ID Then
                    sMySQL = sMySQL & ", SeqNoTotal = " & Trim(Str(.SeqNoTotal))
                End If
                sMySQL = sMySQL & ", KIDStart = " & .KIDStart & ", KIDLength = " & .KIDLength
                sMySQL = sMySQL & ", KIDValue = '" & .KIDValue
                sMySQL = sMySQL & "' where Company_ID = " & Str(iCompanyNo) & " AND ClientFormat_ID = "
                sMySQL = sMySQL & Str(.ClientFormat_ID)
            End With
            myBBDB_AccessCommand.CommandText = sMySQL
            myBBDB_AccessCommand.ExecuteNonQuery()
        End If

        oClient.Status = CollectionStatus.NoChange

        If Not myBBDB_AccessClientReader Is Nothing Then
            If Not myBBDB_AccessClientReader.IsClosed Then
                myBBDB_AccessClientReader.Close()
            End If
            myBBDB_AccessClientReader = Nothing
        End If

        SaveClient_old = bContinue

    End Function
    Private Function SaveReports(ByRef iCompanyNo As Short, ByRef oReports As Reports, ByRef iFileSetup_ID As Short, ByRef myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand, ByVal bSQLServer As Boolean) As Boolean
        '08.07.2016 - Added bSQLServer
        Dim sMySQL As String
        Dim iReport_ID As Short
        Dim oReport As Report
        'Dim rs As New ADODB.Recordset
        'Dim cmdUpdate As New ADODB.Command
        sMySQL = ""


        ' Start by deleteing all records for this Filesetup
        ' Makes it easy to add all Reports in oReport for current Filesetup
        sMySQL = "DELETE from Reports WHERE Company_ID = " & Str(iCompanyNo) & " AND Filesetup_ID = " & Str(iFileSetup_ID)
        myBBDB_AccessCommand.CommandText = sMySQL
        myBBDB_AccessCommand.ExecuteNonQuery()

        For Each oReport In oReports
            ' Must now that this filesetup is present in Filesetup-table:
            'sMySQL = "SELECT * FROM Filesetup WHERE Filesetup_ID = " & Str(iFileSetup_ID)
            'rs.Open sMySQL, cnProfile, adOpenForwardOnly, adLockOptimistic
            'If Not rs.eof And Not rs.BOF Then

            iReport_ID = iReport_ID + 1
            ' 21.11.2007 Added LongDate
            With oReport
                '20.01.2011, added 5 fields below
                sMySQL = "INSERT INTO Reports(Company_ID, Filesetup_ID, Report_ID, ReportNO, "
                sMySQL = sMySQL & "Preview, Printer, AskForPrinter, EMail, ExportType, ExportFilename, "
                sMySQL = sMySQL & "BreakLevel, TotalLevel, DetailLevel, Heading, BitmapFilename, PrinterName, "
                sMySQL = sMySQL & "Longdate, ReportName, ReportSQL, IncludeOCR, ReportSort, ReportWhen,"
                sMySQL = sMySQL & "ReportOnDatabase, SaveBeforeReport, SpecialReport, ClientReport, eMailAddresses, ActivateSpecialSQL) VALUES( "

                sMySQL = sMySQL & Str(iCompanyNo) & ", "
                sMySQL = sMySQL & Str(iFileSetup_ID) & ", "
                sMySQL = sMySQL & Str(iReport_ID) & ", "
                sMySQL = sMySQL & .ReportNo & ", "
                If bSQLServer Then
                    sMySQL = sMySQL & CInt(.Preview) & ", "
                    sMySQL = sMySQL & CInt(.Printer) & ", "
                    sMySQL = sMySQL & CInt(.AskForPrinter) & ", "
                    sMySQL = sMySQL & CInt(.EMail) & ", '"
                Else
                    sMySQL = sMySQL & Str(.Preview) & ", "
                    sMySQL = sMySQL & Str(.Printer) & ", "
                    sMySQL = sMySQL & Str(.AskForPrinter) & ", "
                    sMySQL = sMySQL & Str(.EMail) & ", '"
                End If
                sMySQL = sMySQL & .ExportType & "', '"
                sMySQL = sMySQL & .ExportFilename & "', "
                If bSQLServer Then
                    sMySQL = sMySQL & CInt(.BreakLevel) & ", "
                    sMySQL = sMySQL & CInt(.TotalLevel) & ", "
                    sMySQL = sMySQL & CInt(.DetailLevel) & ", '"
                Else
                    sMySQL = sMySQL & Str(.BreakLevel) & ", "
                    sMySQL = sMySQL & Str(.TotalLevel) & ", "
                    sMySQL = sMySQL & Str(.DetailLevel) & ", '"
                End If
                sMySQL = sMySQL & .Heading & "', '"
                sMySQL = sMySQL & .BitmapFilename & "', '"
                sMySQL = sMySQL & Trim(.PrinterName) & "', "
                If bSQLServer Then
                    sMySQL = sMySQL & CInt(.LongDate) & ", '"
                Else
                    sMySQL = sMySQL & Str(.LongDate) & ", '"
                End If
                sMySQL = sMySQL & Trim(.ReportName) & "', '"
                sMySQL = sMySQL & .ReportSQL & "', "
                If bSQLServer Then
                    sMySQL = sMySQL & CInt(.IncludeOCR) & ", "
                    sMySQL = sMySQL & CInt(.ReportSort) & ", "
                    sMySQL = sMySQL & CInt(.ReportWhen) & ", "
                    sMySQL = sMySQL & CInt(.ReportOnDatabase) & ", "
                    sMySQL = sMySQL & CInt(.SaveBeforeReport) & ", "
                    sMySQL = sMySQL & CInt(.SpecialReport) & ", "
                    sMySQL = sMySQL & CInt(.ClientReport) & ", '"
                Else
                    sMySQL = sMySQL & Str(.IncludeOCR) & ", "
                    sMySQL = sMySQL & Str(.ReportSort) & ", "
                    sMySQL = sMySQL & Str(.ReportWhen) & ", "
                    sMySQL = sMySQL & Str(.ReportOnDatabase) & ", "
                    sMySQL = sMySQL & Str(.SaveBeforeReport) & ", "
                    sMySQL = sMySQL & Str(.SpecialReport) & ", "
                    sMySQL = sMySQL & Str(.ClientReport) & ", '"
                End If
                sMySQL = sMySQL & .EMailAdresses & "', "
                If bSQLServer Then
                    sMySQL = sMySQL & CInt(.ActivateSpecialSQL)
                Else
                    sMySQL = sMySQL & Str(.ActivateSpecialSQL)
                End If
                sMySQL = sMySQL & ")"

                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()
            End With

            'End If
            'rs.Close
        Next oReport
        SaveReports = True

    End Function
    Private Function SaveAccount(ByRef iCompanyNo As Short, ByRef oaccount As Account, ByRef iFileSetup_ID As Short, ByRef iClientIndex As Short, ByRef myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand, ByVal myBBDB_AccessAccountReader As System.Data.OleDb.OleDbDataReader) As Boolean
        Dim sMySQL As String
        Dim iNoOfRecords As Short
        Dim iClient_ID As Short
        Dim bUpdate, bDelete, bNew As Boolean
        Dim bContinue As Boolean
        Dim iNextAccountID As Short

        bContinue = False
        bDelete = False
        bUpdate = False
        bNew = False

        ' Added 08.05.06
        'UPGRADE_WARNING: Couldn't resolve default property of object FileSetups(CheckFilesetupChanges(iFileSetup_ID)).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If FileSetups(CheckFilesetupChanges(iFileSetup_ID)).Clients.Count >= iClientIndex Then
            'Used later in the function
            'UPGRADE_WARNING: Couldn't resolve default property of object FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            iClient_ID = FileSetups(CheckFilesetupChanges(iFileSetup_ID)).Clients(iClientIndex).Client_ID

            Select Case oaccount.Status
                Case CollectionStatus.NoChange
                    'No action, next Account
                Case CollectionStatus.SeqNoChange
                    'No action, next account
                Case CollectionStatus.ChangeUnder
                    'No action, next account
                Case CollectionStatus.Changed
                    bUpdate = True
                    bContinue = True
                Case CollectionStatus.NewCol
                    'Have to check if the Account already exists in the database, because of the
                    ' differences between the strusture of the database and the collections
                    sMySQL = "SELECT Account_ID FROM Account WHERE Company_ID = " & Str(iCompanyNo)
                    sMySQL = sMySQL & " AND Client_ID = " & iClient_ID.ToString
                    sMySQL = sMySQL & " AND Account = '" & Trim(oaccount.Account) & "'"
                    'If myBBDB_AccessSelectCommand Is Nothing Then
                    '    myBBDB_AccessSelectCommand = myBBDB_AccessConnection.CreateCommand()
                    '    myBBDB_AccessSelectCommand.CommandType = CommandType.Text
                    '    myBBDB_AccessSelectCommand.Connection = myBBDB_AccessConnection
                    'End If
                    myBBDB_AccessCommand.CommandText = sMySQL
                    If myBBDB_AccessCommand Is Nothing Then
                        myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand()
                        myBBDB_AccessCommand.CommandType = CommandType.Text
                        myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
                    End If
                    myBBDB_AccessCommand.CommandText = sMySQL

                    myBBDB_AccessAccountReader = myBBDB_AccessCommand.ExecuteReader

                    iNoOfRecords = 0
                    If myBBDB_AccessAccountReader.HasRows Then
                        Do While myBBDB_AccessAccountReader.Read
                            iNoOfRecords = iNoOfRecords + 1
                            ' Put the correct Account_ID into the collection!
                            oaccount.Account_ID = myBBDB_AccessAccountReader.GetInt32(0)
                        Loop
                    End If

                    If iNoOfRecords = 0 Then
                        bNew = True
                        bUpdate = True
                        bContinue = True
                    Else
                        bUpdate = True
                        bContinue = True
                    End If
                Case CollectionStatus.Deleted
                    bDelete = True
            End Select

            ' added 15.06.2012
            If Not myBBDB_AccessAccountReader Is Nothing Then
                If Not myBBDB_AccessAccountReader.IsClosed Then
                    myBBDB_AccessAccountReader.Close()
                End If
                myBBDB_AccessAccountReader = Nothing
            End If

            If bDelete Then
                ' To make certain that the index is present;
                'UPGRADE_WARNING: Couldn't resolve default property of object FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If FileSetups(iFileSetup_ID).Clients(iClientIndex).Accounts.Count >= oaccount.Index Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    FileSetups(iFileSetup_ID).Clients(iClientIndex).Accounts.Remove(oaccount.Index)
                End If
            End If

            If bNew Then
                ' Find next Account_ID;

                sMySQL = "SELECT * FROM Account WHERE Company_ID = " & iCompanyNo.ToString
                sMySQL = sMySQL & " AND Client_ID = " & iClient_ID.ToString

                'If myBBDB_AccessSelectCommand Is Nothing Then
                '    myBBDB_AccessSelectCommand = myBBDB_AccessConnection.CreateCommand()
                '    myBBDB_AccessSelectCommand.CommandType = CommandType.Text
                '    myBBDB_AccessSelectCommand.Connection = myBBDB_AccessConnection
                'End If
                'myBBDB_AccessSelectCommand.CommandText = sMySQL

                If myBBDB_AccessCommand Is Nothing Then
                    myBBDB_AccessCommand = myBBDB_AccessConnection.CreateCommand()
                    myBBDB_AccessCommand.CommandType = CommandType.Text
                    myBBDB_AccessCommand.Connection = myBBDB_AccessConnection
                End If
                myBBDB_AccessCommand.CommandText = sMySQL

                myBBDB_AccessAccountReader = myBBDB_AccessCommand.ExecuteReader

                iNoOfRecords = 0
                If myBBDB_AccessAccountReader.HasRows Then
                    Do While myBBDB_AccessAccountReader.Read
                        iNextAccountID = iNextAccountID + 1
                    Loop
                End If

                iNextAccountID = iNextAccountID + 1
                oaccount.Account_ID = iNextAccountID

                ' added 15.06.2012
                If Not myBBDB_AccessAccountReader Is Nothing Then
                    If Not myBBDB_AccessAccountReader.IsClosed Then
                        myBBDB_AccessAccountReader.Close()
                    End If
                    myBBDB_AccessAccountReader = Nothing
                End If

                sMySQL = "INSERT INTO Account(Company_ID,Client_ID,Account_ID) VALUES("
                sMySQL = sMySQL & Str(iCompanyNo) & ", " & Str(iClient_ID)
                sMySQL = sMySQL & ", " & Str(iNextAccountID) & ")"
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()
            End If

            If bUpdate Then
                With oaccount
                    'sMySQL = "UPDATE Account SET Account = '" & .Account
                    ' Changed 03.10.02 JanP
                    sMySQL = "UPDATE Account SET Account = '" & .Account
                    sMySQL = sMySQL & "' ,ContractNo = '" & .ContractNo
                    sMySQL = sMySQL & "' ,ConvertedAccount = '" & .ConvertedAccount
                    sMySQL = sMySQL & "' ,GLAccount = '" & .GLAccount
                    sMySQL = sMySQL & "' ,GLResultsAccount = '" & .GLResultsAccount
                    sMySQL = sMySQL & "' ,SWIFTAddr = '" & .SWIFTAddress
                    sMySQL = sMySQL & "' ,AvtaleGiroID = '" & .AvtaleGiroID
                    sMySQL = sMySQL & "' ,AccountCountryCode = '" & .AccountCountryCode
                    sMySQL = sMySQL & "' ,CurrencyCode = '" & .CurrencyCode
                    ' XNET 17.05.2013 !!! added DebitAccountCountrycode
                    sMySQL = sMySQL & "' ,DebitAccountCountryCode = '" & .DebitAccountCountryCode
                    sMySQL = sMySQL & "' where Company_ID = " & Str(iCompanyNo) & " AND Client_ID = "
                    sMySQL = sMySQL & Str(iClient_ID) & " AND Account_ID = " & .Account_ID
                    myBBDB_AccessCommand.CommandText = sMySQL
                    myBBDB_AccessCommand.ExecuteNonQuery()
                End With
            End If

            oaccount.Status = CollectionStatus.NoChange
        End If

        If Not myBBDB_AccessAccountReader Is Nothing Then
            If Not myBBDB_AccessAccountReader.IsClosed Then
                myBBDB_AccessAccountReader.Close()
            End If
            myBBDB_AccessAccountReader = Nothing
        End If

        SaveAccount = bContinue

    End Function
    Private Function SaveAccount_old(ByRef iCompanyNo As Short, ByRef oaccount As Account, ByRef iFileSetup_ID As Short, ByRef iClientIndex As Short, ByRef myBBDB_AccessCommand As System.Data.OleDb.OleDbCommand, ByRef myBBDB_AccessSelectCommand As System.Data.OleDb.OleDbCommand) As Boolean
        Dim sMySQL As String
        Dim iNoOfRecords As Short
        Dim iClient_ID As Short
        Dim bUpdate, bDelete, bNew As Boolean
        Dim bContinue As Boolean
        Dim iNextAccountID As Short
        Dim myBBDB_AccessAccountReader As System.Data.OleDb.OleDbDataReader = Nothing

        bContinue = False
        bDelete = False
        bUpdate = False
        bNew = False

        ' Added 08.05.06
        'UPGRADE_WARNING: Couldn't resolve default property of object FileSetups(CheckFilesetupChanges(iFileSetup_ID)).Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If FileSetups(CheckFilesetupChanges(iFileSetup_ID)).Clients.Count >= iClientIndex Then
            'Used later in the function
            'UPGRADE_WARNING: Couldn't resolve default property of object FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            iClient_ID = FileSetups(CheckFilesetupChanges(iFileSetup_ID)).Clients(iClientIndex).Client_ID

            Select Case oaccount.Status
                Case CollectionStatus.NoChange
                    'No action, next Account
                Case CollectionStatus.SeqNoChange
                    'No action, next account
                Case CollectionStatus.ChangeUnder
                    'No action, next account
                Case CollectionStatus.Changed
                    bUpdate = True
                    bContinue = True
                Case CollectionStatus.NewCol
                    'Have to check if the Account already exists in the database, because of the
                    ' differences between the strusture of the database and the collections
                    sMySQL = "SELECT Account_ID FROM Account WHERE Company_ID = " & Str(iCompanyNo)
                    sMySQL = sMySQL & " AND Client_ID = " & iClient_ID.ToString
                    sMySQL = sMySQL & " AND Account = '" & Trim(oaccount.Account) & "'"
                    If myBBDB_AccessSelectCommand Is Nothing Then
                        myBBDB_AccessSelectCommand = myBBDB_AccessConnection.CreateCommand()
                        myBBDB_AccessSelectCommand.CommandType = CommandType.Text
                        myBBDB_AccessSelectCommand.Connection = myBBDB_AccessConnection
                    End If
                    myBBDB_AccessSelectCommand.CommandText = sMySQL

                    myBBDB_AccessAccountReader = myBBDB_AccessSelectCommand.ExecuteReader

                    iNoOfRecords = 0
                    If myBBDB_AccessAccountReader.HasRows Then
                        Do While myBBDB_AccessAccountReader.Read
                            iNoOfRecords = iNoOfRecords + 1
                            ' Put the correct Account_ID into the collection!
                            oaccount.Account_ID = myBBDB_AccessAccountReader.GetInt32(0)
                        Loop
                    End If

                    If iNoOfRecords = 0 Then
                        bNew = True
                        bUpdate = True
                        bContinue = True
                    Else
                        bUpdate = True
                        bContinue = True
                    End If
                Case CollectionStatus.Deleted
                    bDelete = True
            End Select


            If bDelete Then
                ' To make certain that the index is present;
                'UPGRADE_WARNING: Couldn't resolve default property of object FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If FileSetups(iFileSetup_ID).Clients(iClientIndex).Accounts.Count >= oaccount.Index Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object FileSetups().Clients. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    FileSetups(iFileSetup_ID).Clients(iClientIndex).Accounts.Remove(oaccount.Index)
                End If
            End If

            If bNew Then
                ' Find next Account_ID;

                sMySQL = "SELECT * FROM Account WHERE Company_ID = " & iCompanyNo.ToString
                sMySQL = sMySQL & " AND Client_ID = " & iClient_ID.ToString

                If myBBDB_AccessSelectCommand Is Nothing Then
                    myBBDB_AccessSelectCommand = myBBDB_AccessConnection.CreateCommand()
                    myBBDB_AccessSelectCommand.CommandType = CommandType.Text
                    myBBDB_AccessSelectCommand.Connection = myBBDB_AccessConnection
                End If
                myBBDB_AccessSelectCommand.CommandText = sMySQL

                myBBDB_AccessAccountReader = myBBDB_AccessSelectCommand.ExecuteReader

                iNoOfRecords = 0
                If myBBDB_AccessAccountReader.HasRows Then
                    Do While myBBDB_AccessAccountReader.Read
                        iNextAccountID = iNextAccountID + 1
                    Loop
                End If

                iNextAccountID = iNextAccountID + 1
                oaccount.Account_ID = iNextAccountID

                sMySQL = "INSERT INTO Account(Company_ID,Client_ID,Account_ID) VALUES("
                sMySQL = sMySQL & Str(iCompanyNo) & ", " & Str(iClient_ID)
                sMySQL = sMySQL & ", " & Str(iNextAccountID) & ")"
                myBBDB_AccessCommand.CommandText = sMySQL
                myBBDB_AccessCommand.ExecuteNonQuery()
            End If

            If bUpdate Then
                With oaccount
                    'sMySQL = "UPDATE Account SET Account = '" & .Account
                    ' Changed 03.10.02 JanP
                    sMySQL = "UPDATE Account SET Account = '" & .Account
                    sMySQL = sMySQL & "' ,ContractNo = '" & .ContractNo
                    sMySQL = sMySQL & "' ,ConvertedAccount = '" & .ConvertedAccount
                    sMySQL = sMySQL & "' ,GLAccount = '" & .GLAccount
                    sMySQL = sMySQL & "' ,GLResultsAccount = '" & .GLResultsAccount
                    sMySQL = sMySQL & "' ,SWIFTAddr = '" & .SWIFTAddress
                    sMySQL = sMySQL & "' ,AvtaleGiroID = '" & .AvtaleGiroID
                    sMySQL = sMySQL & "' ,AccountCountryCode = '" & .AccountCountryCode
                    sMySQL = sMySQL & "' ,CurrencyCode = '" & .CurrencyCode
                    sMySQL = sMySQL & "' where Company_ID = " & Str(iCompanyNo) & " AND Client_ID = "
                    sMySQL = sMySQL & Str(iClient_ID) & " AND Account_ID = " & .Account_ID
                    myBBDB_AccessCommand.CommandText = sMySQL
                    myBBDB_AccessCommand.ExecuteNonQuery()
                End With
            End If

            oaccount.Status = CollectionStatus.NoChange
        End If

        If Not myBBDB_AccessAccountReader Is Nothing Then
            If Not myBBDB_AccessAccountReader.IsClosed Then
                myBBDB_AccessAccountReader.Close()
            End If
            myBBDB_AccessAccountReader = Nothing
        End If

        SaveAccount_old = bContinue

    End Function
    Private Function CheckFilesetupChanges(Optional ByRef iOldFileSetupID As Short = 0) As Short
        'This function is used to create a two-dimensional array consisting of the old FileSetup_ID
        ' ,and the new FileSetup_ID. It's used when changes has been made
        Static aFilesetupIDs(,) As Integer
        'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
        'DOTNETT REDIM Static aFilesetupIDs() As Integer
        Dim iReturnValue As Short
        Dim iNoOfDeleted As Short
        Dim iArrayCounter As Short
        Dim bFilesetupChanged As Boolean
        Dim oFilesetup As FileSetup
        Dim iCounter As Short

        iArrayCounter = 0
        Select Case iOldFileSetupID
            Case -1
                'Kill the array
                ReDim aFilesetupIDs(1, 0)
                iReturnValue = 0
            Case 0
                iNoOfDeleted = 0
                For Each oFilesetup In FileSetups
                    ReDim Preserve aFilesetupIDs(1, iArrayCounter)
                    'UPGRADE_WARNING: Couldn't resolve default property of object aFilesetupIDs(0, iArrayCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    aFilesetupIDs(0, iArrayCounter) = oFilesetup.FileSetup_ID
                    If oFilesetup.Status = CollectionStatus.Deleted Then
                        iNoOfDeleted = iNoOfDeleted + 1
                    End If
                    'UPGRADE_WARNING: Couldn't resolve default property of object aFilesetupIDs(1, iArrayCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    aFilesetupIDs(1, iArrayCounter) = oFilesetup.FileSetup_ID - iNoOfDeleted
                    iArrayCounter = iArrayCounter + 1
                Next oFilesetup
                iReturnValue = 0
            Case Else
                If Not Array_IsEmpty(aFilesetupIDs) Then
                    For iCounter = 0 To UBound(aFilesetupIDs, 2)
                        'UPGRADE_WARNING: Couldn't resolve default property of object aFilesetupIDs(0, iCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If aFilesetupIDs(0, iCounter) = iOldFileSetupID Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object aFilesetupIDs(1, iCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            iReturnValue = aFilesetupIDs(1, iCounter)
                            Exit For
                        End If
                    Next iCounter
                End If
        End Select

        CheckFilesetupChanges = iReturnValue

    End Function
End Class
