﻿' XOKNET 06.01.2013 - Take whole module
' XOKNET 15.08.2013 Again; - take whole module,
Option Explicit On
Module WriteNordeaSE
    Dim nFileSumAmount As Double
    Dim nFileSumAmountFicka As Double
    Dim nFileNoRecords As Double
    Dim iPayNumber As Integer
    Dim sAmountCurrency As String
    Public Function WriteNordea_SE_Plusgirot_Fakturabetalning(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal iFilenameInNo As Integer, ByVal sCompanyNo As String, ByVal sAdditionalNo As String, ByVal sClientNo As String, Optional ByVal oExternalUpdateConnection As vbBabel.DAL = Nothing, Optional ByVal oExternalReadConnection As vbBabel.DAL = Nothing) As Boolean
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i As Double, j As Double, k As Double
        Dim bFirstLine As Boolean
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oClient As Client
        Dim oaccount As Account
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim iCount As Integer
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim bFileStartWritten As Boolean
        Dim sOldAccount As String
        Dim sOld_E_Account As String
        Dim sOldPayType As String
        Dim sOldSupplierNo As String
        Dim sAccountCurrency As String
        Dim bFileFromBank As Boolean

        Dim nPaymentIndex As Double
        Dim bFoundClient As Boolean
        Dim sTxt As String
        Dim sText1 As String
        Dim sText2 As String
        Dim bInternational As Boolean

        Try

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            sOldAccount = "---------------------------"
            sOld_E_Account = "----------------------------"
            sOldSupplierNo = "--------------------------------"
            sOldPayType = ""
            iPayNumber = 0

            For Each oBabel In oBabelFiles
                bExportoBabel = False ' Not needed if no specialchecking is present
                If oBabel.BabelfileContainsPaymentsAccordingToQualifiers(iFilenameInNo, iFormat_ID, bMultiFiles, sClientNo, True, sOwnRef) Then
                    bExportoBabel = True
                End If

                If Not bFileStartWritten Then
                    ' write only once!
                    ' When merging Client-files, skip this one except for first client!
                    ' XOKNET 29.10.2012 - this is not correct. Must have one FileStart-record pr I_Account (pr debet BankgiroNr) !!

                    sLine = WritePlusGirotInledningspost(oBabel, sAdditionalNo, iFilenameInNo, oExternalUpdateConnection, oExternalReadConnection)
                    oFile.WriteLine(sLine)
                    bFileStartWritten = True
                End If

                If bExportoBabel Then
                    'Set if the file was created int the accounting system or not.
                    bFileFromBank = oBabel.FileFromBank
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        ' Added next If, because it would bomb further down for empty batches
                        If oBatch.Payments.Count > 0 Then
                            If oBatch.BatchContainsPaymentsAccordingToQualifiers(iFormat_ID, bMultiFiles, sClientNo, nPaymentIndex, True, sOwnRef) Then
                                bExportoBatch = True
                            End If

                            If bExportoBatch Then
                                For Each oPayment In oBatch.Payments

                                    bExportoPayment = False
                                    'Have to go through the payment-object to see if we have objects thatt shall
                                    ' be exported to this exportfile
                                    If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                        ' XNET 30.01.2014 added next If
                                        If Not oPayment.Exported Then
                                            If Not bMultiFiles Then
                                                bExportoPayment = True
                                            Else
                                                If oPayment.I_Account = sI_Account Then
                                                    If oPayment.REF_Own = sOwnRef Then
                                                        bExportoPayment = True
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If

                                    If bExportoPayment Then
                                        iPayNumber = iPayNumber + 1 ' Mottakernummer

                                        ' must test for change in debitaccount
                                        ' or change between Domestic and Int (added 06.12.2013)
                                        ' ------------------------------------
                                        If oPayment.I_Account <> sOldAccount Or oPayment.PayType <> sOldPayType Then
                                            If oPayment.VB_ProfileInUse Then
                                                For Each oClient In oPayment.VB_Profile.FileSetups(iFormat_ID).Clients
                                                    For Each oaccount In oClient.Accounts
                                                        If oaccount.Account = oPayment.I_Account Or Trim$(oaccount.ConvertedAccount) = oPayment.I_Account Then
                                                            sAccountCurrency = oaccount.CurrencyCode
                                                            bFoundClient = True
                                                            Exit For
                                                        End If
                                                    Next
                                                    If bFoundClient Then
                                                        Exit For
                                                    End If
                                                Next
                                            Else
                                                sAccountCurrency = "SEK"    ' TODO Visma assume SEK-account. What to do if EUR-account ?
                                            End If

                                            If oPayment.PayType = "I" Then
                                                ' ------------------------------
                                                ' International
                                                ' ------------------------------
                                                sLine = WritePlusgirot_Int_Avsendarpost(oBabel, oBatch, oPayment, sCompanyNo, sAdditionalNo, oPayment.I_Account, sAccountCurrency)
                                                oFile.WriteLine(sLine)
                                                sOldAccount = oPayment.I_Account
                                                sOldPayType = "I"
                                            Else
                                                ' ------------------------------
                                                ' Domestic
                                                ' ------------------------------
                                                ' must have new Avsendarpost pr debitaccount
                                                sLine = WritePlusgirotAvsendarpost(oBabel, oBatch, oPayment, sAdditionalNo, oPayment.I_Account, sAccountCurrency) ' Not tested
                                                oFile.WriteLine(sLine)
                                                sOldAccount = oPayment.I_Account
                                                sOldPayType = "D"
                                            End If
                                        End If



                                        For Each oInvoice In oPayment.Invoices
                                            If Not oInvoice.Exported Then

                                                ' Find all freetext;
                                                ' ------------------
                                                sTxt = ""
                                                For Each oFreetext In oInvoice.Freetexts
                                                    sTxt = sTxt & Trim$(oFreetext.Text)
                                                Next oFreetext


                                                If oPayment.PayType = "I" Then
                                                    ' ------------------------------
                                                    ' International
                                                    ' ------------------------------
                                                    bInternational = True

                                                    '*****************************************
                                                    ' TODO Visma
                                                    ' Kan International blandes med Domestic ?
                                                    ' og hvordan ?
                                                    '*****************************************
                                                    sLine = WritePlusgirot_Int_MottagarNamnpost(oPayment, oInvoice)
                                                    oFile.WriteLine(sLine)
                                                    sLine = WritePlusgirot_Int_MottagarAdresspost(oPayment, oInvoice)
                                                    oFile.WriteLine(sLine)

                                                    If oInvoice.MON_InvoiceAmount >= 0 Then
                                                        ' First Beloppspost is for receiving banks BIC
                                                        If Not EmptyString(oPayment.BANK_SWIFTCode) Then
                                                            sLine = WritePlusgirot_Int_BeloppMeddelandepost(oPayment, oInvoice, oPayment.BANK_SWIFTCode, True)
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                        ' If payer will pay charges for receivers bank, set it here:
                                                        If oPayment.MON_ChargeMeAbroad Then
                                                            sLine = WritePlusgirot_Int_BeloppMeddelandepost(oPayment, oInvoice, ":OUR", True)
                                                            oFile.WriteLine(sLine)
                                                        End If
                                                        ' then, add upto three Beloppsrecords with text, each 33 chars, without amount, before the amountrecord itself
                                                        If Len(sTxt) > 33 Then
                                                            For i = 1 To 3
                                                                sLine = WritePlusgirot_Int_BeloppMeddelandepost(oPayment, oInvoice, Left$(sTxt, 33), True)
                                                                oFile.WriteLine(sLine)
                                                                sTxt = Mid$(sTxt, 34)
                                                                If Len(sTxt) < 34 Then
                                                                    Exit For
                                                                End If
                                                            Next i
                                                        End If
                                                        ' set amount as well in last Beloppsrecord
                                                        sLine = WritePlusgirot_Int_BeloppMeddelandepost(oPayment, oInvoice, Left$(sTxt, 33), False)
                                                        oFile.WriteLine(sLine)
                                                    Else
                                                        ' First Beloppspost credit is for receiving banks BIC
                                                        sLine = WritePlusgirot_Int_BeloppKreditMeddelandepost(oPayment, oInvoice, oPayment.BANK_SWIFTCode, True)
                                                        oFile.WriteLine(sLine)

                                                        ' Creditnote
                                                        sLine = WritePlusgirot_Int_BeloppKreditMeddelandepost(oPayment, oInvoice, Left$(sTxt, 33), False)
                                                        oFile.WriteLine(sLine)
                                                    End If

                                                Else
                                                    ' ------------------------------
                                                    ' Domestic
                                                    ' ------------------------------
                                                    bInternational = False

                                                    If oPayment.E_Account <> sOld_E_Account Or oInvoice.SupplierNo <> sOldSupplierNo Then
                                                        ' If not to PlusGirokonto, must have Mottagarpost
                                                        'If oPayment.E_AccountType = SE_PlusGiro Then
                                                        If EmptyString(oPayment.E_Account) Then
                                                            sLine = WritePlusgirotMottagarpostUtbetalning(oPayment, oInvoice) ' Not tested
                                                            oFile.WriteLine(sLine)
                                                        Else
                                                            If oPayment.E_AccountType <> BabelFiles.AccountType.SE_PlusGiro Then
                                                                sLine = WritePlusgirotMottagarpostBankgiroOrKontoisettning(oPayment, oInvoice)
                                                                oFile.WriteLine(sLine)
                                                            End If
                                                        End If
                                                    End If
                                                    sOld_E_Account = oPayment.E_Account
                                                    sOldSupplierNo = oPayment.Invoices(1).SupplierNo


                                                    ' No Meddelande text records for bankkintoisettingar or OCR
                                                    If oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount Or IsOCR(oPayment.PayCode) Then
                                                        sTxt = ""
                                                    Else
                                                        ' If more than 27 chars in freetext, we must use infolines:
                                                        If Len(sTxt) > 27 Then
                                                            iCount = 0
                                                            Do
                                                                iCount = iCount + 1
                                                                If oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro Then
                                                                    ' 35 pos max for each textfield for Bankgiro
                                                                    sText1 = Left$(sTxt, 35)
                                                                    If Len(sTxt) > 35 Then
                                                                        sText2 = Mid$(sTxt, 36, 35)
                                                                    Else
                                                                        sText2 = ""
                                                                    End If
                                                                    If Len(sTxt) > 70 Then
                                                                        sTxt = Mid$(sTxt, 71)
                                                                    Else
                                                                        sTxt = ""
                                                                    End If

                                                                Else
                                                                    ' 40 pos max for each textfield else
                                                                    sText1 = Left$(sTxt, 40)
                                                                    If Len(sTxt) > 40 Then
                                                                        sText2 = Mid$(sTxt, 41, 40)
                                                                    Else
                                                                        sText2 = ""
                                                                    End If
                                                                    If Len(sTxt) > 80 Then
                                                                        sTxt = Mid$(sTxt, 81)
                                                                    Else
                                                                        sTxt = ""
                                                                    End If
                                                                End If

                                                                sLine = WritePlusgirotMeddelande(oPayment, oInvoice, sText1, sText2)
                                                                oFile.WriteLine(sLine)
                                                                ' get out when no more freetext do export
                                                                If Len(sTxt) = 0 Then
                                                                    Exit Do
                                                                End If
                                                                ' or get out when we have reached max no of records
                                                                'Högst 5 meddelandeposter kan föregå en beloppspost. 1 st meddelandepost utgörs av 2 stmeddelanderader.
                                                                'Används factoringpost (posttyp=3) och/eller avsändarreferens och verifikationsnummer
                                                                '(posttyp=5) kan högst 4 meddelandeposter lämnas.
                                                                If Not EmptyString(oInvoice.SupplierNo) Then
                                                                    If iCount = 4 Then
                                                                        Exit Do
                                                                    End If
                                                                Else
                                                                    If iCount = 5 Then
                                                                        Exit Do
                                                                    End If
                                                                End If
                                                            Loop
                                                        End If
                                                    End If


                                                    ' Beloppspost
                                                    If oInvoice.MON_InvoiceAmount >= 0 Then
                                                        sLine = WritePostgirotBeloppspostDebet(oBatch, oPayment, oInvoice, sTxt)
                                                        oFile.WriteLine(sLine)
                                                    Else
                                                        ' only allowed with negatice amounts for payments to Postgirot
                                                        If oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Or oPayment.E_Account = "" Then
                                                            sLine = WritePostgirotBeloppspostKreditPlusgirot(oBatch, oPayment, oInvoice, sTxt)
                                                            oFile.WriteLine(sLine)
                                                        ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro Then
                                                            sLine = WritePostgirotBeloppspostKreditBankgiro(oBatch, oPayment, oInvoice, sTxt)
                                                            oFile.WriteLine(sLine)
                                                        Else
                                                            ' default to bankgiro
                                                            sLine = WritePostgirotBeloppspostKreditBankgiro(oBatch, oPayment, oInvoice, sTxt)
                                                            oFile.WriteLine(sLine)

                                                            ' how do we warn about illegal to have credits other than to Postgirot/Bankgirot?
                                                            'MsgBox "Creditnotes only allowed for payments to Postgirot/Bankgirot"
                                                            'WriteNordea_SE_Plusgirot_Fakturabetalning = False
                                                            'Exit Function

                                                        End If
                                                    End If

                                                End If
                                            End If ' If Not oInvoice.Exported Then
                                        Next oInvoice
                                        oPayment.Exported = True

                                    End If 'If bExportoPayment Then

                                Next ' oPayment
                            End If 'If bExportoBatch Then
                        End If 'If oBatch.Payments.Count > 0 Then

                        If nFileNoRecords > 0 Then
                            If bInternational Then
                                sLine = WritePlusgirot_Int_Summapost(oBatch, sAdditionalNo, sOldAccount, sAccountCurrency)
                            Else
                                sLine = WritePlusgirotSummapost(oBatch, sAdditionalNo, sOldAccount, sAccountCurrency)
                            End If
                            oFile.WriteLine(sLine)
                        End If

                    Next 'oBatch
                End If 'If bExportoBabel Then
            Next 'oBabelfile

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteNordea_SE_Plusgirot_Fakturabetalning" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteNordea_SE_Plusgirot_Fakturabetalning = True

    End Function
    Function WritePlusGirotInledningspost(ByVal oBabelFile As BabelFile, ByVal sAdditionalNo As String, ByVal iFilenameInNo As Integer, Optional ByVal oExternalUpdateConnection As vbBabel.DAL = Nothing, Optional ByVal oExternalReadConnection As vbBabel.DAL = Nothing) As String
        Dim sLine As String
        Dim iProductionNumber As Integer

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileSumAmountFicka = 0
        nFileNoRecords = 0

        sLine = "0"                                         ' 1
        If oBabelFile.Batches.Count > 0 Then
            If EmptyString(sAdditionalNo) Then
                sLine = sLine & PadRight(oBabelFile.Batches(1).I_EnterpriseNo, 5, " ")      '2-6 Kundnummer
            Else
                sLine = sLine & PadRight(sAdditionalNo, 5, " ")    '2-6 Kundnummer
            End If
            sLine = sLine & Format(Date.Today, "yyMMdd")          '7-12 Produksjonsdatum
            ' Produktionsnummer forutsettes her til alltid å være 1, da vi ved flere headere vil ha andre kundnummere
            ' 21.08.2013 For Visma, we have saved the "sequence" for produktionsnummer pr day
            If IsThisVismaIntegrator() Then
                ' find produktionsnummer for Foretaksnr + dato
                iProductionNumber = Visma_RetrieveProductionNumber(oExternalReadConnection, oBabelFile.Batches(1).I_EnterpriseNo, oBabelFile.DATE_Production, oBabelFile.Visma_ProfileID, "BabelBank_" & oBabelFile.Visma_SystemDatabase) ' ProfileNo is in iFilenameInNo
                iProductionNumber = iProductionNumber + 1
                sLine = sLine & Right(Str(iProductionNumber), 1)    '13
                '         --- save last productionno
                'Visma_SaveProductionNumber oExternalConnection, oBabelFile.Batches(1).I_EnterpriseNo, oBabelFile.DATE_Production, iProductionNumber, iFilenameInNo, "BabelBank_" & oBabelFile.Visma_SystemDatabase, oBabelFile.Visma_User
                Visma_SaveProductionNumber(oExternalUpdateConnection, oExternalReadConnection, oBabelFile.Batches(1).I_EnterpriseNo, oBabelFile.DATE_Production, iProductionNumber, oBabelFile.Visma_ProfileID, "BabelBank_" & oBabelFile.Visma_SystemDatabase, oBabelFile.Visma_User)
            Else
                sLine = sLine & "1"                             '13
            End If
        End If
        'End If
        'pad to 100 with blanks
        sLine = PadLine(sLine, 100, " ")

        WritePlusGirotInledningspost = UCase(sLine)

    End Function
    Function WritePlusgirotAvsendarpost(ByVal oBabelFile As BabelFile, ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal sAdditionalNo As String, ByVal sI_Account As String, ByVal sAccountCurrency As String) As String
        Dim sLine As String

        sLine = "2"        ' 1
        If EmptyString(sAdditionalNo) Then
            sLine = sLine & PadRight(oBatch.I_EnterpriseNo, 5, " ")    '2-6 Kundnummer
        Else
            sLine = sLine & PadRight(sAdditionalNo, 5, " ")    '2-6 Kundnummer
        End If
        sLine = sLine & PadLeft(Replace(sI_Account, "-", ""), 10, " ")  '7-16 Avsändarkonto, remove -
        sLine = sLine & Space(2)                        '17-18 Avsändarkode

        If oBabelFile.Batches.Count > 0 Then
            If oBabelFile.VB_ProfileInUse Then
                sLine = sLine & PadRight(oBabelFile.VB_Profile.CompanyName, 27, " ") '19-45 Avsändarbeteckning 1
                sLine = sLine & PadRight(oBabelFile.VB_Profile.CompanyZip & " " & oBabelFile.VB_Profile.CompanyCity, 27, " ") '46-72 Avsändarbeteckning 2
            Else
                ' how can we find the info
                If IsThisVismaIntegrator Then
                    sLine = sLine & PadRight(oBabelFile.Visma_FrmName, 27, " ") '19-45 Avsändarbeteckning 1
                    sLine = sLine & Space(27) '46-72 Avsändarbeteckning 2
                Else
                    sLine = sLine & Space(27) '19-45 Avsändarbeteckning 1
                    sLine = sLine & Space(27) '46-72 Avsändarbeteckning 2
                End If
            End If
            If EmptyString(sAccountCurrency) Then
                sLine = sLine & "SEK"    '73-75 Valutaficka för betalning från - default to SEK
            Else
                sLine = sLine & PadRight(sAccountCurrency, 3, " ")  '73-75 Valutaficka för betalning från - use debitaccounts currency
            End If
            sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ")  '76-78 Beloppsvaluta for belopp
            sLine = sLine & Space(13)    '79-91 Blank
            sLine = sLine & Space(9)    '92-100 Återföringskonto, ikke i bruk foreløpig
        End If

        WritePlusgirotAvsendarpost = UCase(sLine)

    End Function
    Function WritePlusgirot_Int_Avsendarpost(ByVal oBabelFile As BabelFile, ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal sCompanyNo As String, ByVal sAdditionalNo As String, ByVal sI_Account As String, ByVal sAccountCurrency As String) As String
        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1

        sLine = "1"        ' 1
        If EmptyString(sAdditionalNo) Then
            sLine = sLine & PadRight(oBatch.I_EnterpriseNo, 5, " ")    '2-6 Kundnummer
        Else
            sLine = sLine & PadRight(sAdditionalNo, 5, " ")       '2-6 Kundnummer
        End If
        sLine = sLine & PadLeft(Replace(sI_Account, "-", ""), 10, " ")  '7-16 Avsändarkonto
        sLine = sLine & "  "                            '17-18 Avsändarkode

        If oBabelFile.Batches.Count > 0 Then
            If oBabelFile.VB_ProfileInUse Then
                sLine = sLine & PadRight(oBabelFile.VB_Profile.CompanyName, 27, " ") '19-45 Avsändarbeteckning 1
                sLine = sLine & PadRight(oBabelFile.VB_Profile.CompanyAdr1, 27, " ") '46-72 Avsändarbeteckning 2
                sLine = sLine & PadRight(oBabelFile.VB_Profile.CompanyCity, 15, " ")  '73-87 Avsändarbeteckning 3
            Else
                ' how can we find the info
                If IsThisVismaIntegrator Then
                    sLine = sLine & PadRight(oBabelFile.Visma_FrmName, 27, " ") '19-45 Avsändarbeteckning 1
                    sLine = sLine & Space(27) '46-72 Avsändarbeteckning 2
                Else
                    sLine = sLine & Space(27) '19-45 Avsändarbeteckning 1
                    sLine = sLine & Space(27) '46-72 Avsändarbeteckning 2
                End If
                sLine = sLine & Space(15)  '73-87 Avsändarbeteckning 3
            End If
            sLine = sLine & "2"                                                   '88 Utlandskod 2
            sLine = sLine & "2"                                                   '89 Layoutkod 2

            'If sAccountCurrency = "SEK" Or sAccountCurrency = "EUR" Then
            '    If EmptyString(sAccountCurrency) Then
            '        sLine = sLine & "S"    '90 Valutaficka för betalning från - default to SEK
            '    Else
            '        sLine = sLine & Left(sAccountCurrency, 1)  '90 Valutaficka för betalning från - use debitaccounts currency
            '    End If
            'Else
            ' **************************************************************************************************************
            'TODO Visma ' Must be discussed; Can we always set blank for Valutaficka, and use Kalkylbeløp og valutabelop in "5"-record ?
            '***************************************************************************************************************
            ' TODO Visma If EUR as debitaccount, we need "E" in pos 90. How can we know if EUR?
            sLine = sLine & "S"    '90 Valutaficka för betalning från - S for valutakonti
            'End If

            '************ we need Org.no !
            If IsThisVismaIntegrator Then
                sLine = sLine & PadRight(oBabelFile.IDENT_Sender, 10, " ")      '91-100 Org.number
            Else
                If Not EmptyString(sCompanyNo) Then
                    sLine = sLine & PadRight(sCompanyNo, 10, " ")         '91-100 Org.number
                Else
                    sLine = sLine & PadRight(oBatch.I_EnterpriseNo, 10, " ")    '91-100 Org.number
                End If
            End If
        End If

        WritePlusgirot_Int_Avsendarpost = UCase(sLine)

    End Function
    'XNET - WHOLE function
    Function WritePlusgirot_Int_MottagarNamnpost(ByVal oPayment As Payment, ByVal oInvoice As Invoice) As String
        Dim sLine As String
        Dim bCheck As Boolean

        nFileNoRecords = nFileNoRecords + 1

        sLine = "2"                                 '1
        If EmptyString(oPayment.E_Account) Or UCase(oPayment.E_Account) = "CHECK" Or oPayment.E_Account = "00000000019" Then
            bCheck = True
            sLine = sLine & "9"                         '2  0 = kontoisätning, 9 = check
        Else
            bCheck = False
            sLine = sLine & "0"                         '2  0 = kontoisätning, 9 = check
        End If
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadRight(oInvoice.SupplierNo, 15, " ")   ' 3-17 Mottagarident
        Else
            sLine = sLine & PadRight(Str(iPayNumber), 15, " ")   ' 3-17  Løpenr mottager
        End If
        sLine = sLine & Space(5)  '18-22
        If oPayment.Priority Then
            sLine = sLine & "2"                         '23  Mottagarkod 1 = Normal, 2 = Express
        Else
            sLine = sLine & "1"                         '23  Mottagarkod 1 = Normal, 2 = Express
        End If
        If Not bCheck Then
            sLine = sLine & PadRight(oPayment.E_Account, 33, " ")  '24-56   IBAN account
            sLine = sLine & PadRight(oPayment.E_Name, 33, " ")  '57-89  Name if check
        Else
            sLine = sLine & PadRight(oPayment.E_Name, 33, " ")  '24-56  Name if check
            sLine = sLine & PadRight(Trim$(oPayment.E_Adr1), 33, " ")  '57-89
        End If

        sLine = sLine & Space(3)  '90-92 Betalingskod, men angis for hver post isteden
        sLine = sLine & Space(8)                    '93-100
        WritePlusgirot_Int_MottagarNamnpost = UCase(sLine)

    End Function
    Function WritePlusgirot_Int_MottagarAdresspost(ByVal oPayment As Payment, ByVal oInvoice As Invoice) As String
        Dim sLine As String
        Dim bCheck As Boolean

        nFileNoRecords = nFileNoRecords + 1

        sLine = "3"                                 '1
        If EmptyString(oPayment.E_Account) Or UCase(oPayment.E_Account) = "CHECK" Or oPayment.E_Account = "00000000019" Then
            bCheck = True
            sLine = sLine & "9"                         '2  0 = kontoisätning, 9 = check
        Else
            sLine = sLine & "0"                         '2  0 = kontoisätning, 9 = check
        End If
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadRight(oInvoice.SupplierNo, 15, " ")   ' 3-17 Mottagarident
        Else
            sLine = sLine & PadRight(Str(iPayNumber), 15, " ")   ' 3-17  Løpenr mottager
        End If
        If bCheck Then
            sLine = sLine & PadRight(Trim$(oPayment.E_Adr2) & Trim$(oPayment.E_Adr3), 33, " ")   '18-50  Adr if check
        Else
            Select Case oPayment.BANK_CountryCode
                Case "AU"
                    ' Australia - BSB
                    sLine = sLine & PadRight("//AU" & oPayment.BANK_BranchNo, 33, " ")  ' 18-50 Bankcode
                Case "CA"
                    ' Canada - CanadianClearing CC
                    sLine = sLine & PadRight("//CC" & oPayment.BANK_BranchNo, 33, " ")  ' 18-50 Bankcode
                Case "ZA"
                    ' South Africa - Sortcode
                    sLine = sLine & PadRight("//ZA" & oPayment.BANK_BranchNo, 33, " ")  ' 18-50 Bankcode
                Case "US"
                    ' US - Fedwire
                    sLine = sLine & PadRight("//FW" & oPayment.BANK_BranchNo, 33, " ")  ' 18-50 Bankcode
                Case Else
                    ' for other countries, put in bankname (as in Convert-X - documentation says leave blank!)
                    sLine = sLine & PadRight(oPayment.BANK_Name, 33, " ")
            End Select
        End If
        If bCheck Then
            ' if check, give receivers city/zip
            If Not EmptyString(oPayment.E_City) And Not EmptyString(oPayment.E_Zip) Then
                sLine = sLine & PadRight(oPayment.E_City, 22, " ")  '51-72  City
                sLine = sLine & PadRight(oPayment.E_Zip, 10, " ")   '73-82  Zip
            Else
                sLine = sLine & PadRight(oPayment.E_Adr2, 22, " ")  '51-72  City
                sLine = sLine & Space(10)                           '73-82  Blank
            End If
        Else
            ' if kontoisätning, give bankcity
            ' problems knowing which address holds city
            If Not EmptyString(oPayment.BANK_Adr1) Then
                sLine = sLine & PadRight(oPayment.BANK_Adr1, 22, " ")    '51-72  bank city
                sLine = sLine & PadRight(oPayment.BANK_Adr2, 10, " ")   '73-82  Zip
            ElseIf Not EmptyString(oPayment.BANK_Adr2) Then
                sLine = sLine & PadRight(oPayment.BANK_Adr2, 22, " ")    '51-72  bank city
                sLine = sLine & PadRight(oPayment.BANK_Adr3, 10, " ")   '73-82  Zip
            Else
                sLine = sLine & PadRight(oPayment.BANK_Adr3, 22, " ")    '51-72  bank city
                sLine = sLine & Space(10)   '73-82  Zip
            End If

        End If
        sLine = sLine & PadRight(oPayment.BANK_CountryCode, 2, " ")  '83-84  City
        If Not EmptyString(oInvoice.REF_Own) Then
            sLine = sLine & PadRight(oInvoice.REF_Own, 13, " ")  '85-97   Own reference
        Else
            ' if no invoice.ref_own, then use payment.ref_own
            sLine = sLine & PadRight(oPayment.REF_Own, 13, " ")  '85-97   Own reference
        End If
        sLine = sLine & Space(3)                    '98-100
        WritePlusgirot_Int_MottagarAdresspost = UCase(sLine)

    End Function
    Function WritePlusgirot_Int_BeloppMeddelandepost(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sTxt As String, ByVal bNoAmount As Boolean) As String
        Dim sLine As String
        Dim bCheck As Boolean
        ' This record can be used both as amount and notificationrecord, and can be called upto 5 times, 4 without an amount
        nFileNoRecords = nFileNoRecords + 1

        sLine = "5"                                 '1
        If EmptyString(oPayment.E_Account) Or UCase(oPayment.E_Account) = "CHECK" Or oPayment.E_Account = "00000000019" Then
            sLine = sLine & "9"                         '2  0 = kontoisätning, 9 = check
        Else
            sLine = sLine & "0"                         '2  0 = kontoisätning, 9 = check
        End If
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadRight(oInvoice.SupplierNo, 15, " ")   ' 3-17 Mottagarident
        Else
            sLine = sLine & PadRight(Str(iPayNumber), 15, " ")   ' 3-17  Løpenr mottager
        End If
        sLine = sLine & PadRight(sTxt, 33, " ")  '18-50  Adr if check
        If bNoAmount Then
            ' leave rest blank
            sLine = sLine & Space(50)            '51-100
        Else
            ' Add to Batch-totals:
            nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
            nFileSumAmountFicka = nFileSumAmountFicka + oInvoice.MON_LocalAmount

            ' "real" amountrecord
            sLine = sLine & Space(2)             ' 51-52 blank
            '***************************************************************************************************************
            ' vi har satt fickavaluta til blank, kan vi da bruke 53-63 til kalkylbelopp alltid, for å få differ i returfil ?
            '***************************************************************************************************************
            sLine = sLine & PadLeft(Str(oInvoice.MON_LocalAmount), 11, "0")  '53-63 Kalkylbelopp, i øre/cent
            sLine = sLine & Mid$(oPayment.DATE_Payment, 3)  ' 64-69 Bokføringsdag (YYMMDD)
            sLine = sLine & Space(2)             ' 70-71 blank
            ' for JPY, add 2 decimals. JPY normally have no decimals
            If oPayment.MON_InvoiceCurrency = "JPY" Then
                sLine = sLine & PadLeft(Str(oInvoice.MON_InvoiceAmount), 13, "0") & "00"  '72-86 Belopp, i øre/cent
            Else
                sLine = sLine & PadLeft(Str(oInvoice.MON_InvoiceAmount), 15, "0")  '72-86 Belopp, i øre/cent
            End If
            sLine = sLine & oPayment.MON_InvoiceCurrency    '87-89 Currency to be paid

            'Omräkningskod. Anger vilket belopp som är bas för betalningen.
            '1 = Om belopp i annan än fickans valuta anges.
            '2 = Om enbart belopp i fickans valuta anges.
            '***********************************************
            sLine = sLine & "1"                             ' 90 - Always 1 as we do it by using amount from 72-86 ?

            'Samsorteringskod.
            'Betalningar i samma valuta och med samma bokföringsdag till samma
            'mottagare kan sammanföras till en betalning.
            '1 = Ingen samsortering. Varje betalning sänds separat.
            '2 = Samsortering av samtliga betalningssätt. Delbeloppen specificeras.
            'En hopslagen betalning rymmer högst 4 betalningar.
            '3 = Samsortering av samtliga betalningssätt. Delbeloppen specificeras ej.
            'Graden av samsortering beror på längden på de ursprungliga betalningarnas meddelanden

            sLine = sLine & "2"                                      '91 Samsorteringskod
            ' XOKNET 23.09.2013 test for riksbankkod
            If Not EmptyString(oInvoice.STATEBANK_Code) Then
                sLine = sLine & PadRight(oInvoice.STATEBANK_Code, 3, " ") '92-94 Betalningskod (Riksbank)
            Else
                ' Defautl to code 101 Export av varor
                sLine = sLine & "101" '92-94 Betalningskod (Riksbank)
            End If
            sLine = sLine & Space(6)                    '95-100
        End If
        WritePlusgirot_Int_BeloppMeddelandepost = UCase(sLine)

    End Function
    Function WritePlusgirotMottagarpostUtbetalning(ByVal oPayment As Payment, ByVal oInvoice As Invoice) As String
        Dim sLine As String

        sLine = "3"                                 '1
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & "5"                         '2  5 = Mottaker med fast utbetalingsnummer ?
            sLine = sLine & Space(5)                    '3-7
            ' use Mottagarident, in stead of runningnumber, especially when creditnotes involved:
            sLine = sLine & PadLeft(oInvoice.SupplierNo, 10, " ")   ' 8-17 Mottagarident
        Else
            sLine = sLine & "7"                         '2  Mottaker med løpende utbetalingsnummer ?
            sLine = sLine & Space(5)                    '3-7
            sLine = sLine & PadLeft(Str(iPayNumber), 10, " ")   ' 8-17  Løpenr mottager
        End If
        sLine = sLine & PadRight(oPayment.E_Zip, 5, " ")  '18-22
        sLine = sLine & PadRight(oPayment.E_Name, 33, " ")  '23-55
        sLine = sLine & PadRight(oPayment.E_Adr1, 27, " ")  '56-82  Mottaker adr1
        sLine = sLine & PadRight(oPayment.E_City, 13, " ")  '83-95 Sted
        sLine = sLine & Space(5)                    '96-100
        WritePlusgirotMottagarpostUtbetalning = UCase(sLine)

    End Function
    Function WritePlusgirotMottagarpostBankgiroOrKontoisettning(ByVal oPayment As Payment, ByVal oInvoice As Invoice) As String
        Dim sLine As String

        sLine = "3"                                 '1
        sLine = sLine & "4"                         '2
        sLine = sLine & Space(5)                    '3-7
        ' use Mottagarident, in stead of runningnumber, especially when creditnotes involved:
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadLeft(oInvoice.SupplierNo, 10, " ")   ' 8-17 Mottagarident
        Else
            sLine = sLine & PadLeft(Str(iPayNumber), 10, " ")   ' 8-17  Løpenr mottager
        End If
        sLine = sLine & Space(5)                        '18-22 blanka
        sLine = sLine & PadRight(Trim$(oPayment.E_Name), 33, " ")  '23-55
        sLine = sLine & PadLeft(Replace(oPayment.E_Account, "-", ""), 16, " ") '56-71  Mottaker bankgirnummer or bankkontonummer, remove -
        sLine = sLine & Space(29)                    '72-100
        WritePlusgirotMottagarpostBankgiroOrKontoisettning = UCase(sLine)

    End Function
    Function WritePlusgirot_Int_BeloppKreditMeddelandepost(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sTxt As String, ByVal bNoAmount As Boolean) As String
        Dim sLine As String
        Dim bCheck As Boolean

        nFileNoRecords = nFileNoRecords + 1

        sLine = "6"                                 '1
        If EmptyString(oPayment.E_Account) Or UCase(oPayment.E_Account) = "CHECK" Or oPayment.E_Account = "00000000019" Then
            sLine = sLine & "9"                         '2  0 = kontoisätning, 9 = check
        Else
            sLine = sLine & "0"                         '2  0 = kontoisätning, 9 = check
        End If
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadRight(oInvoice.SupplierNo, 15, " ")   ' 3-17 Mottagarident
        Else
            sLine = sLine & PadRight(Str(iPayNumber), 15, " ")   ' 3-17  Løpenr mottager
        End If
        sLine = sLine & PadRight(sTxt, 33, " ")  '18-50  Adr if check
        If bNoAmount Then
            ' leave rest blank
            sLine = sLine & Space(50)            '51-100
        Else
            ' Add to Batch-totals:
            nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
            nFileSumAmountFicka = nFileSumAmountFicka + oInvoice.MON_LocalAmount

            ' "real" amountrecord
            sLine = sLine & Space(2)             ' 51-52 blank
            '***************************************************************************************************************
            ' vi har satt fickavaluta til blank, kan vi da bruke 53-63 til kalkylbelopp alltid, for å få differ i returfil ?
            '***************************************************************************************************************
            sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_LocalAmount)), 11, "0")  '53-63 Kalkylbelopp, i øre/cent
            sLine = sLine & Mid$(oPayment.DATE_Payment, 3)  ' 64-69 Førsta Bokføringsdag (YYMMDD)
            sLine = sLine & Mid$(oPayment.DATE_Value, 3)    ' 70-75 Sista Bokføringsdag (YYMMDD)
            sLine = sLine & Space(2)                        ' 76-77 blank

            ' for JPY, add 2 decimals. JPY normally have no decimals
            If oPayment.MON_InvoiceCurrency = "JPY" Then
                sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 13, "0") & "00"  '78-92 Belopp, i øre/cent
            Else
                sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 15, "0")  '78-92 Belopp, i øre/cent
            End If
            sLine = sLine & oPayment.MON_InvoiceCurrency    '93-95 Currency to be paid

            'Omräkningskod. Anger vilket belopp som är bas för betalningen.
            '1 = Om belopp i annan än fickans valuta anges.
            '2 = Om enbart belopp i fickans valuta anges.
            '***********************************************
            sLine = sLine & "1"                             ' 96 - Always 1 as we do it by using amount from 72-86 ?
            ' XOKNET 23.09.2013 test for riksbankkod
            If Not EmptyString(oInvoice.STATEBANK_Code) Then
                sLine = sLine & PadRight(oInvoice.STATEBANK_Code, 3, " ") '97-99 Betalningskod (Riksbank)
            Else
                ' Default to 101 Export av varor
                sLine = sLine & "101"    '97-99 Betalningskod (Riksbank)
            End If
            sLine = sLine & Space(1)                    '100
        End If
        WritePlusgirot_Int_BeloppKreditMeddelandepost = UCase(sLine)

    End Function
    Function WritePlusgirotMeddelande(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sText1 As String, ByVal sText2 As String) As String
        Dim sLine As String

        sLine = "4"                                 '1
        If oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Then
            sLine = sLine & "3"                         '2
        ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro Then
            sLine = sLine & "4"                         '2
        ElseIf Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & "5"                         '2
        Else
            sLine = sLine & "7"                         '2
        End If

        sLine = sLine & Space(5)                    '3-7
        ' use Mottagarident, in stead of runningnumber, especially when creditnotes involved:
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadLeft(oInvoice.SupplierNo, 10, " ")   ' 8-17 Mottagarident
        Else
            sLine = sLine & PadLeft(Str(iPayNumber), 10, " ")   ' 8-17  Løpenr mottager
        End If

        sLine = sLine & PadRight(sText1, 40, " ")  '18-57
        sLine = sLine & PadRight(sText2, 40, " ")  '58-97
        sLine = sLine & Space(3)                    '98-100
        WritePlusgirotMeddelande = UCase(sLine)

    End Function
    Function WritePostgirotBeloppspostDebet(ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sTxt As String) As String
        Dim sLine As String
        Dim oFreetext As Freetext

        ' Add to Batch-totals:
        nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
        nFileNoRecords = nFileNoRecords + 1

        sLine = "5"                                 '1
        If oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Then
            sLine = sLine & "3"                         '2
            'ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro Then
            ' 16.11.2016 changed to line below (mind 179908 at Visma)
            ' det betyr at ikke "5" og "7" er ibruk
        ElseIf oPayment.E_AccountType <> BabelFiles.AccountType.SE_PlusGiro Then
            sLine = sLine & "4"                         '2
        ElseIf Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & "5"                         '2
        Else
            sLine = sLine & "7"                         '2
        End If
        sLine = sLine & "1" ' Forutsetter samsortering ! kan være at vi må ha en variable her
        '3 Blank = Samsortering önskas av flera utbetalningar (Bet. sätt. 5 och 7)
        'till samma mottagare med gemensamt betalningsdatum. 1 = Samsorte
        'ring önskas inte. Varje betalningsuppdrag redovisas separat.
        ' ConvertX legger ut 1, vi får gjøre det samme TODO Visma
        If EmptyString(oPayment.MON_InvoiceCurrency) Then
            sLine = sLine & "SEK"                               '4-6
            sAmountCurrency = "SEK"
        Else
            sLine = sLine & oPayment.MON_InvoiceCurrency        '4-6
            sAmountCurrency = oPayment.MON_InvoiceCurrency
        End If
        If oPayment.Priority Then
            ' same day execution
            sLine = sLine & "J"                               '7 Yes
        Else
            sLine = sLine & " "                               '7 No
        End If
        ' use Mottagarident, in stead of runningnumber, if known
        If oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Then
            sLine = sLine & PadLeft(Trim(oPayment.E_Account), 10, " ")   ' 8-17 Mottagarident
        Else
            If Not EmptyString(oInvoice.SupplierNo) Then
                sLine = sLine & PadLeft(oInvoice.SupplierNo, 10, " ")   ' 8-17 Mottagarident
            Else
                sLine = sLine & PadLeft(Str(iPayNumber), 10, " ")   ' 8-17  Løpenr mottager
            End If
        End If

        '18-44 meddelanderad
        If IsOutgoingKIDPayment(oPayment.PayCode) And Not EmptyString(oInvoice.Unique_Id) Then
            ' OCR-code
            sLine = sLine & PadRight(oInvoice.Unique_Id, 27, " ")   '18-44
        ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankgiro Then
            sLine = sLine & PadRight(sTxt, 25, " ") & Space(2)   '18-44
        ElseIf oPayment.E_AccountType = BabelFiles.AccountType.SE_Bankaccount Then
            sLine = sLine & PadRight(sTxt, 11, " ") & Space(16)   '18-44
        Else
            sLine = sLine & PadRight(sTxt, 27, " ")               '18-44
        End If
        sLine = sLine & PadLeft(Str(oInvoice.MON_InvoiceAmount), 11, "0")  '45-55 Belopp, i øre/cent
        sLine = sLine & Mid$(oPayment.DATE_Payment, 3, 6) '56-61
        sLine = sLine & PadRight(oInvoice.REF_Own, 30, " ")  '62-91
        sLine = sLine & Space(8)                            '92-99 Verifikationsnummer, not in use so far
        sLine = sLine & Space(1)                            '100

        WritePostgirotBeloppspostDebet = UCase(sLine)

    End Function
    Function WritePostgirotBeloppspostKreditPlusgirot(ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sTxt As String) As String
        Dim sLine As String
        Dim oFreetext As Freetext

        ' Add to Batch-totals:
        nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
        nFileNoRecords = nFileNoRecords + 1

        sLine = "6"                                 '1
        If oPayment.E_AccountType = BabelFiles.AccountType.SE_PlusGiro Then
            sLine = sLine & "3"                         '2
        ElseIf Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & "5"                         '2
        Else
            sLine = sLine & "7"                         '2
        End If
        sLine = sLine & " "                             '3
        If EmptyString(oPayment.MON_InvoiceCurrency) Then
            sLine = sLine & "SEK"                               '4-6
        Else
            sLine = sLine & oPayment.MON_InvoiceCurrency        '4-6
        End If
        sLine = sLine & " "                                '7
        ' use Mottagarident, in stead of runningnumber, if known
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadLeft(oInvoice.SupplierNo, 10, " ")   ' 8-17 Mottagarident
        Else
            sLine = sLine & PadLeft(Str(iPayNumber), 10, " ")   ' 8-17  Løpenr mottager
        End If
        '18-44 meddelanderad
        sLine = sLine & PadRight(sTxt, 27, " ")               '18-44

        sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 11, "0")  '45-55 Belopp, i øre/cent
        sLine = sLine & Mid$(oPayment.DATE_Payment, 3, 6) '56-61    Første bokføringsdato
        sLine = sLine & Mid$(oPayment.DATE_Value, 3, 6)   '62-67    Siste bokføringsdato, samme hvis ikke bevakning - bruker ValDato for bevakningsslutt dato !
        sLine = sLine & PadRight(oInvoice.REF_Own, 30, " ")  '68-97
        sLine = sLine & Space(3)                            '98-100

        WritePostgirotBeloppspostKreditPlusgirot = UCase(sLine)

    End Function
    Function WritePostgirotBeloppspostKreditBankgiro(ByVal oBatch As Batch, ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal sTxt As String) As String
        Dim sLine As String
        Dim oFreetext As Freetext

        ' Add to Batch-totals:
        nFileSumAmount = nFileSumAmount + oInvoice.MON_InvoiceAmount
        nFileNoRecords = nFileNoRecords + 1

        sLine = "6"                                 '1
        sLine = sLine & "4"                         '2
        sLine = sLine & " "                             '3
        If EmptyString(oPayment.MON_InvoiceCurrency) Then
            sLine = sLine & "SEK"                               '4-6
        Else
            sLine = sLine & oPayment.MON_InvoiceCurrency        '4-6
        End If
        sLine = sLine & " "                                '7
        ' use Mottagarident, in stead of runningnumber, if known
        If Not EmptyString(oInvoice.SupplierNo) Then
            sLine = sLine & PadLeft(oInvoice.SupplierNo, 10, " ")   ' 8-17 Mottagarident
        Else
            sLine = sLine & PadLeft(Str(iPayNumber), 10, " ")   ' 8-17  Løpenr mottager
        End If
        '18-44 meddelanderad
        sLine = sLine & PadRight(sTxt, 27, " ")               '18-44

        sLine = sLine & PadLeft(Str(Math.Abs(oInvoice.MON_InvoiceAmount)), 11, "0")  '45-55 Belopp, i øre/cent
        sLine = sLine & Mid$(oPayment.DATE_Payment, 3, 6) '56-61    Første bokføringsdato
        sLine = sLine & Mid$(oPayment.DATE_Value, 3, 6)   '62-67    Siste bokføringsdato, samme hvis ikke bevakning - bruker ValDato for bevakningsslutt dato !
        sLine = sLine & PadRight(oInvoice.REF_Own, 30, " ")  '68-97
        sLine = sLine & Space(3)                            '98-100

        WritePostgirotBeloppspostKreditBankgiro = UCase(sLine)

    End Function
    Function WritePlusgirotSummapost(ByVal oBatch As Batch, ByVal sAdditionalNo As String, ByVal sI_Account As String, ByVal sAccountCurrency As String) As String

        Dim sLine As String

        sLine = "7"        ' 1
        If EmptyString(sAdditionalNo) Then
            sLine = sLine & PadRight(oBatch.I_EnterpriseNo, 5, " ")      '2-6 Kundnummer
        Else
            sLine = sLine & PadRight(sAdditionalNo, 5, " ")    '2-6 Kundnummer
        End If

        sLine = sLine & PadLeft(Replace(sI_Account, "-", ""), 10, " ")  '7-16 Avsändarkonto
        sLine = sLine & "  "                            '17-18 Avsändarkode
        sLine = sLine & PadLeft(Str(Math.Abs(nFileSumAmount)), 13, "0")  '19-31 Belopp, i øre/cent
        sLine = sLine & Space(32)                            '32-63

        If EmptyString(sAccountCurrency) Then
            sLine = sLine & "SEK"    '64-66 Valutaficka för betalning från - default to SEK
        Else
            sLine = sLine & PadRight(sAccountCurrency, 3, " ")  '64-66 Valutaficka för betalning från - use debitaccounts currency
        End If
        sLine = sLine & PadRight(sAmountCurrency, 3, " ")  '67-69 Beloppsvaluta for belopp
        sLine = sLine & Space(31)    '70-100 Återföringskonto, ikke i bruk foreløpig

        WritePlusgirotSummapost = UCase(sLine)
        nFileNoRecords = 0
        nFileSumAmount = 0
        nFileSumAmountFicka = 0

    End Function
    Function WritePlusgirot_Int_Summapost(ByVal oBatch As Batch, ByVal sAdditionalNo As String, ByVal sI_Account As String, ByVal sAccountCurrency As String) As String
        Dim sLine As String

        sLine = "7"        ' 1
        If EmptyString(sAdditionalNo) Then
            sLine = sLine & PadRight(oBatch.I_EnterpriseNo, 5, " ")    '2-6 Kundnummer
        Else
            sLine = sLine & PadRight(sAdditionalNo, 5, " ")       '2-6 Kundnummer
        End If

        sLine = sLine & PadLeft(Replace(sI_Account, "-", ""), 10, " ")  '7-16 Avsändarkonto
        sLine = sLine & "  "                            '17-18 Avsändarkode


        'If sAccountCurrency = "SEK" Or sAccountCurrency = "EUR" Then
        '    If EmptyString(sAccountCurrency) Then
        '        sLine = sLine & "S"    '19 Valutaficka för betalning från - default to SEK
        '    Else
        '        sLine = sLine & Left(sAccountCurrency, 1)  '19 Valutaficka för betalning från - use debitaccounts currency
        '    End If
        'Else
        ' **************************************************************************************************************
        ' Must be discussed; Can we always set S for Valutaficka, and use Kalkylbeløp og valutabelop in "5"-record ?
        '***************************************************************************************************************
        ' TODO Visma If EUR as debitaccount, we need "E" in pos 90. How can we know if EUR?
        sLine = sLine & "S"    '19 Valutaficka för betalning från - not set for valutakonti

        'End If
        sLine = sLine & " "    '20 Blank
        sLine = sLine & PadLeft(LTrim(Str(nFileSumAmountFicka)), 12, "0")         '21-33 Belopp i fickans valuta.
        If nFileSumAmountFicka < 0 Then
            sLine = sLine & "-"                 ' 52 Fortegn for nonsencetotal
        Else
            sLine = sLine & "+"                 ' 52 Fortegn for nonsencetotal
        End If

        sLine = sLine & Space(2)                '34-35 blanke
        sLine = sLine & PadLeft(LTrim(Str(nFileSumAmount)), 16, "0") ' 36-51 Nonsence total
        If nFileSumAmount < 0 Then
            sLine = sLine & "-"                 ' 52 Fortegn for nonsencetotal
        Else
            sLine = sLine & "+"                 ' 52 Fortegn for nonsencetotal
        End If
        sLine = sLine & Space(27)               '53-79 blanke

        'Blank = Betalningarna verkställs om PlusGirot vid summering erhåller samma belopp som angivits i position 21–33.
        '1 = Betalningarna verkställs, om PlusGirot erhåller exakt samma totalbelopp som angivits i position 21–33 och i position 36–52. Vid
        'isstämning skrivs samtliga betalningar ut på rapporten Felaktiga betalingsuppdrag
        sLine = sLine & " "                     '80 Avstemmingskod
        sLine = sLine & PadLeft(LTrim(Str(nFileNoRecords)), 7, "0") ' 81-87 Antall poster
        sLine = sLine & "2"                     '88 Utlandskod
        sLine = sLine & Space(12)               '89-100 blanka

        WritePlusgirot_Int_Summapost = UCase(sLine)

        nFileNoRecords = 0
        'XNET - Next 2
        nFileSumAmount = 0
        nFileSumAmountFicka = 0
    End Function
    'XokNET - 21.08.2013 - Added new function
    Private Function Visma_RetrieveProductionNumber(ByVal oExternalReadConnection As vbBabel.DAL, ByVal sI_EnterpriseNo As String, ByVal sDate As String, ByVal iFilenameInNo As Integer, ByVal sBBDatabase As String) As Integer
		'ProfileNo is in iFilenameInNo
		' find last productionnumber in Vismadatabase for this Enterpriseno and date
		' Stored in database as PROD#00882735532#date

		Dim sMySQL As String
        'Dim oMyDal As vbBabel.DAL
        Dim sErrorString As String
        Dim iSequenceNo As Integer

		'On Error GoTo ERR_ReadSEQ
		sErrorString = "Find sequence from database for " & sI_EnterpriseNo & "/" & sDate
		sMySQL = "SELECT BB_ID,Value FROM " & sBBDatabase & ".dbo.bbParameter WHERE SubString(BB_ID,1,5) = 'PROD#' AND ProfileID =" & Str(iFilenameInNo)
		sErrorString = "Select productionnumber"
		oExternalReadConnection.SQL = sMySQL

		If oExternalReadConnection.Reader_Execute() Then
            If oExternalReadConnection.Reader_HasRows = True Then
                iSequenceNo = 0
                ' find productionnumber for current Enterpriseno and date
                Do While oExternalReadConnection.Reader_ReadRecord
                    If xDelim(oExternalReadConnection.Reader_GetString("BB_Id"), "#", 2) = sI_EnterpriseNo And xDelim(oExternalReadConnection.Reader_GetString("BB_Id"), "#", 3) = sDate Then
                        ' found productionNo for this enterpriseno/date
                        iSequenceNo = oExternalReadConnection.Reader_GetString("Value")
                        Exit Do
                    End If
                Loop
            Else
                iSequenceNo = 0
            End If
        Else
            iSequenceNo = 0
        End If

        'oExternalReadConnection.Close()
        'oExternalReadConnection = Nothing

        Visma_RetrieveProductionNumber = iSequenceNo
        '27.06.2019 - Removed this errorhandling when implementing new way og treating errors
        '        Exit Function

        'ERR_ReadSEQ:
        '        oExternalReadConnection.Close()
        '        oExternalReadConnection = Nothing

        '        Err.Raise(Err.Number, "Visma_RetrieveproductionNumber", sErrorString & vbCrLf & Err.Description)

    End Function
    Private Sub Visma_SaveProductionNumber(ByVal oExternalUpdateConnection As vbBabel.DAL, ByVal oExternalReadConnection As vbBabel.DAL, ByVal sI_EnterpriseNo As String, ByVal sDate As String, ByVal iSequenceNo As Integer, ByVal iFilenameInNo As Integer, ByVal sBBDatabase As String, ByVal sUser As String) 'ProfileNo is in iFilenameInNo
        ' Save productionnumber in Vismadatabase for this Enterpriseno and date
        ' Stored in database as PROD#00882735532#date
        Dim sMySQL As String
        Dim sErrorString As String
        Dim lRecordsAffected As Long
		Dim lMaxParameterID As Long
		Dim oMyDalSelect As DAL

		'On Error GoTo ERR_WriteSEQ

		' VISMAPRO TODO - Må vi bruke to conections her?????
		' 30.03.2022 - use new connection to find highest ParameterId
		'		oMyDalSelect = ConnectToVismaBusinessDB(False, sErrorString, True)
		' Find highest ParameterID
		sMySQL = "SELECT MAX(ParameterId) AS MaxID FROM " & sBBDatabase & ".dbo.bbParameter"
		sErrorString = "Select MaxID"

		oExternalReadConnection.SQL = sMySQL

		If oExternalReadConnection.Reader_Execute() Then
			If oExternalReadConnection.Reader_HasRows = True Then
				oExternalReadConnection.Reader_ReadRecord()
				' Always returns a record;
				lMaxParameterID = Val(oExternalReadConnection.Reader_GetString("MaxID")) + 1
			Else
				lMaxParameterID = 1
            End If
        Else
            lMaxParameterID = 1
        End If

        sErrorString = "UPDATE " & sBBDatabase & ".dbo.bbParameter "
        'XokNET 29.11.2013
        sMySQL = "UPDATE " & sBBDatabase & ".dbo.bbParameter SET Value = '" & LTrim(Str(iSequenceNo)) & "', ChDt = CONVERT(VARCHAR(8), SYSDATETIME(), 112), ChTm = REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', ''), ChUsr = '" & sUser & "' WHERE BB_ID = 'PROD#" & Trim$(sI_EnterpriseNo) & "#" & Trim$(sDate) & "#'"
        oExternalUpdateConnection.SQL = sMySQL
        oExternalUpdateConnection.ExecuteNonQuery()
        If oExternalUpdateConnection.RecordsAffected = 0 Then
			' not found, insert instead

			' 04.04.2022 - but then we need to delete old records, so we do not end up with several old ones
			sErrorString = "DELETE FROM " & sBBDatabase & ".dbo.bbParameter "
			sMySQL = "DELETE FROM " & sBBDatabase & ".dbo.bbParameter WHERE ParameterId < " & lMaxParameterID.ToString
			oExternalUpdateConnection.SQL = sMySQL
			oExternalUpdateConnection.ExecuteNonQuery()

			' XokNET 09.10.2013 added more fields, ChDt, ChTm, ChUsr
			sErrorString = "INSERT INTO " & sBBDatabase & ".dbo.bbParameter "
            sMySQL = "INSERT INTO " & sBBDatabase & ".dbo.bbParameter (ParameterID, BB_ID, LRS_ID, Value, ProfileID, AllowedValues, ParameterType, DataType, ChDt, ChTm, ChUsr) "
            sMySQL = sMySQL & "VALUES (" & lMaxParameterID & ", 'PROD#" & Trim$(sI_EnterpriseNo) & "#" & Trim$(sDate) & "#', 36363,'" & LTrim(Str(iSequenceNo)) & "'," & Str(iFilenameInNo) & ", '', 3, 1"
            sMySQL = sMySQL & ", CONVERT(VARCHAR(8), SYSDATETIME(), 112), REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', ''), '" & sUser & "')"

            oExternalUpdateConnection.SQL = sMySQL
            oExternalUpdateConnection.ExecuteNonQuery()

            If oExternalUpdateConnection.RecordsAffected <> 1 Then
                Err.Raise(Err.Number, "Visma_SaveProductionNumber" & vbCrLf & "SQL : " & sMySQL, sErrorString & vbCrLf & Err.Description)
            End If
        End If

        '27.06.2019 - Removed this errorhandling when implementing new way og treating errors
        '        Exit Sub

        'ERR_WriteSEQ:
        '        Err.Raise(Err.Number, "Visma_SaveProductionNumber", sErrorString & vbCrLf & Err.Description)

    End Sub

End Module
