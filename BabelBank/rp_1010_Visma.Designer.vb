<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_1010_Visma 
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub
    
    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OleDBDataSource1 As DataDynamics.ActiveReports.DataSources.OleDBDataSource = New DataDynamics.ActiveReports.DataSources.OleDBDataSource
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rp_1010_Visma))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtxtClientNo = New DataDynamics.ActiveReports.TextBox
        Me.txtType = New DataDynamics.ActiveReports.TextBox
        Me.txtAccountNo = New DataDynamics.ActiveReports.TextBox
        Me.txtNumber = New DataDynamics.ActiveReports.TextBox
        Me.txtAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtCurrency = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.Line3 = New DataDynamics.ActiveReports.Line
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grTotalHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.lblFilename = New DataDynamics.ActiveReports.Label
        Me.txtFilename = New DataDynamics.ActiveReports.TextBox
        Me.lblHeadClientNo = New DataDynamics.ActiveReports.Label
        Me.lblType = New DataDynamics.ActiveReports.Label
        Me.lblAccount = New DataDynamics.ActiveReports.Label
        Me.lblNumber = New DataDynamics.ActiveReports.Label
        Me.lblCurrency = New DataDynamics.ActiveReports.Label
        Me.lblAmount = New DataDynamics.ActiveReports.Label
        Me.grTotalFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.LineBatchFooter1 = New DataDynamics.ActiveReports.Line
        Me.lblBatchfooterAmount = New DataDynamics.ActiveReports.Label
        Me.lblBatchfooterNoofPayments = New DataDynamics.ActiveReports.Label
        Me.txtBatchfooterNoofPayments = New DataDynamics.ActiveReports.TextBox
        Me.txtBatchfooterTransAmount = New DataDynamics.ActiveReports.TextBox
        Me.TotalSum = New DataDynamics.ActiveReports.Field
        Me.LineBatchFooter2 = New DataDynamics.ActiveReports.Line
        Me.LineBatchFooter3 = New DataDynamics.ActiveReports.Line
        CType(Me.txtxtClientNo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAccountNo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblFilename, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblHeadClientNo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblAccount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBatchfooterTransAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtxtClientNo, Me.txtType, Me.txtAccountNo, Me.txtNumber, Me.txtAmount, Me.txtCurrency})
        Me.Detail.Height = 0.25!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'txtxtClientNo
        '
        Me.txtxtClientNo.CanShrink = True
        Me.txtxtClientNo.DataField = "fldClientNo"
        Me.txtxtClientNo.Height = 0.2!
        Me.txtxtClientNo.Left = 0.0!
        Me.txtxtClientNo.Name = "txtxtClientNo"
        Me.txtxtClientNo.Text = "fldClientNo"
        Me.txtxtClientNo.Top = 0.0!
        Me.txtxtClientNo.Width = 1.0!
        '
        'txtType
        '
        Me.txtType.CanGrow = False
        Me.txtType.DataField = "fldType"
        Me.txtType.Height = 0.2!
        Me.txtType.Left = 1.0!
        Me.txtType.Name = "txtType"
        Me.txtType.Text = "fldType"
        Me.txtType.Top = 0.0!
        Me.txtType.Width = 1.375!
        '
        'txtAccountNo
        '
        Me.txtAccountNo.DataField = "fldAccountNo"
        Me.txtAccountNo.Height = 0.2!
        Me.txtAccountNo.Left = 2.375!
        Me.txtAccountNo.Name = "txtAccountNo"
        Me.txtAccountNo.Text = "fldAccountNo"
        Me.txtAccountNo.Top = 0.0!
        Me.txtAccountNo.Width = 1.115!
        '
        'txtNumber
        '
        Me.txtNumber.DataField = "fldNumber"
        Me.txtNumber.Height = 0.2!
        Me.txtNumber.Left = 3.55!
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.Style = "text-align: center"
        Me.txtNumber.Text = "fldNumber"
        Me.txtNumber.Top = 0.0!
        Me.txtNumber.Width = 1.0!
        '
        'txtAmount
        '
        Me.txtAmount.CanShrink = True
        Me.txtAmount.DataField = "fldAmount"
        Me.txtAmount.Height = 0.2!
        Me.txtAmount.Left = 5.25!
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.OutputFormat = resources.GetString("txtAmount.OutputFormat")
        Me.txtAmount.Style = "text-align: right"
        Me.txtAmount.Text = "fldAmount"
        Me.txtAmount.Top = 0.0!
        Me.txtAmount.Width = 1.188!
        '
        'txtCurrency
        '
        Me.txtCurrency.CanShrink = True
        Me.txtCurrency.DataField = "fldCurrency"
        Me.txtCurrency.Height = 0.2!
        Me.txtCurrency.Left = 4.774!
        Me.txtCurrency.Name = "txtCurrency"
        Me.txtCurrency.OutputFormat = resources.GetString("txtCurrency.OutputFormat")
        Me.txtCurrency.Style = "text-align: left"
        Me.txtCurrency.Text = "Cur"
        Me.txtCurrency.Top = 0.0!
        Me.txtCurrency.Width = 0.4760001!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.Line3, Me.rptInfoPageHeaderDate})
        Me.PageHeader.Height = 0.4583333!
        Me.PageHeader.Name = "PageHeader"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 1.279861!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "font-size: 18pt; text-align: center"
        Me.lblrptHeader.Text = "Batches"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'Line3
        '
        Me.Line3.Height = 0.0!
        Me.Line3.Left = 0.0!
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.39375!
        Me.Line3.Width = 6.299305!
        Me.Line3.X1 = 0.0!
        Me.Line3.X2 = 6.299305!
        Me.Line3.Y1 = 0.39375!
        Me.Line3.Y2 = 0.39375!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right"
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter.Height = 0.2604167!
        Me.PageFooter.Name = "PageFooter"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right"
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right"
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grTotalHeader
        '
        Me.grTotalHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblFilename, Me.txtFilename, Me.lblHeadClientNo, Me.lblType, Me.lblAccount, Me.lblNumber, Me.lblCurrency, Me.lblAmount})
        Me.grTotalHeader.DataField = "BreakField"
        Me.grTotalHeader.Height = 0.73625!
        Me.grTotalHeader.Name = "grTotalHeader"
        Me.grTotalHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'lblFilename
        '
        Me.lblFilename.Height = 0.23!
        Me.lblFilename.HyperLink = Nothing
        Me.lblFilename.Left = 0.0!
        Me.lblFilename.Name = "lblFilename"
        Me.lblFilename.Style = "font-size: 14.25pt"
        Me.lblFilename.Text = "lblFilename"
        Me.lblFilename.Top = 0.062!
        Me.lblFilename.Width = 1.313!
        '
        'txtFilename
        '
        Me.txtFilename.DataField = "fldFilename"
        Me.txtFilename.Height = 0.23!
        Me.txtFilename.Left = 1.374!
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.OutputFormat = resources.GetString("txtFilename.OutputFormat")
        Me.txtFilename.Style = "font-size: 14.25pt"
        Me.txtFilename.Text = "txtFilename"
        Me.txtFilename.Top = 0.062!
        Me.txtFilename.Width = 4.925!
        '
        'lblHeadClientNo
        '
        Me.lblHeadClientNo.Height = 0.19!
        Me.lblHeadClientNo.HyperLink = Nothing
        Me.lblHeadClientNo.Left = 0.0!
        Me.lblHeadClientNo.Name = "lblHeadClientNo"
        Me.lblHeadClientNo.Style = "font-size: 10pt"
        Me.lblHeadClientNo.Text = "lblClient"
        Me.lblHeadClientNo.Top = 0.39!
        Me.lblHeadClientNo.Width = 1.0!
        '
        'lblType
        '
        Me.lblType.Height = 0.19!
        Me.lblType.HyperLink = Nothing
        Me.lblType.Left = 1.0!
        Me.lblType.Name = "lblType"
        Me.lblType.Style = "font-size: 10pt"
        Me.lblType.Text = "Type"
        Me.lblType.Top = 0.39!
        Me.lblType.Width = 1.0!
        '
        'lblAccount
        '
        Me.lblAccount.Height = 0.19!
        Me.lblAccount.HyperLink = Nothing
        Me.lblAccount.Left = 2.375!
        Me.lblAccount.Name = "lblAccount"
        Me.lblAccount.Style = "font-size: 10pt"
        Me.lblAccount.Text = "Account"
        Me.lblAccount.Top = 0.39!
        Me.lblAccount.Width = 1.167!
        '
        'lblNumber
        '
        Me.lblNumber.Height = 0.19!
        Me.lblNumber.HyperLink = Nothing
        Me.lblNumber.Left = 3.55!
        Me.lblNumber.Name = "lblNumber"
        Me.lblNumber.Style = "font-size: 10pt; text-align: left"
        Me.lblNumber.Text = "NoOfPayments"
        Me.lblNumber.Top = 0.39!
        Me.lblNumber.Width = 1.224!
        '
        'lblCurrency
        '
        Me.lblCurrency.Height = 0.19!
        Me.lblCurrency.HyperLink = Nothing
        Me.lblCurrency.Left = 4.774!
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Style = "font-size: 10pt"
        Me.lblCurrency.Text = "Cur"
        Me.lblCurrency.Top = 0.39!
        Me.lblCurrency.Width = 0.4260001!
        '
        'lblAmount
        '
        Me.lblAmount.Height = 0.19!
        Me.lblAmount.HyperLink = Nothing
        Me.lblAmount.Left = 5.25!
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Style = "font-size: 10pt; text-align: center"
        Me.lblAmount.Text = "Amount"
        Me.lblAmount.Top = 0.39!
        Me.lblAmount.Width = 1.139!
        '
        'grTotalFooter
        '
        Me.grTotalFooter.CanShrink = True
        Me.grTotalFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.LineBatchFooter1, Me.lblBatchfooterAmount, Me.lblBatchfooterNoofPayments, Me.txtBatchfooterNoofPayments, Me.txtBatchfooterTransAmount, Me.LineBatchFooter2, Me.LineBatchFooter3})
        Me.grTotalFooter.Height = 0.4166667!
        Me.grTotalFooter.Name = "grTotalFooter"
        '
        'LineBatchFooter1
        '
        Me.LineBatchFooter1.Height = 0.0!
        Me.LineBatchFooter1.Left = 2.4!
        Me.LineBatchFooter1.LineWeight = 1.0!
        Me.LineBatchFooter1.Name = "LineBatchFooter1"
        Me.LineBatchFooter1.Top = 0.34!
        Me.LineBatchFooter1.Width = 4.1!
        Me.LineBatchFooter1.X1 = 2.4!
        Me.LineBatchFooter1.X2 = 6.5!
        Me.LineBatchFooter1.Y1 = 0.34!
        Me.LineBatchFooter1.Y2 = 0.34!
        '
        'lblBatchfooterAmount
        '
        Me.lblBatchfooterAmount.Height = 0.2!
        Me.lblBatchfooterAmount.HyperLink = Nothing
        Me.lblBatchfooterAmount.Left = 4.774!
        Me.lblBatchfooterAmount.Name = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Style = "font-size: 10pt; ddo-char-set: 1"
        Me.lblBatchfooterAmount.Text = "lblBatchfooterAmount"
        Me.lblBatchfooterAmount.Top = 0.145!
        Me.lblBatchfooterAmount.Width = 0.6000001!
        '
        'lblBatchfooterNoofPayments
        '
        Me.lblBatchfooterNoofPayments.Height = 0.2!
        Me.lblBatchfooterNoofPayments.HyperLink = Nothing
        Me.lblBatchfooterNoofPayments.Left = 2.4!
        Me.lblBatchfooterNoofPayments.Name = "lblBatchfooterNoofPayments"
        Me.lblBatchfooterNoofPayments.Style = "font-size: 10pt; ddo-char-set: 1"
        Me.lblBatchfooterNoofPayments.Text = "lblBatchfooterNoofPayments"
        Me.lblBatchfooterNoofPayments.Top = 0.145!
        Me.lblBatchfooterNoofPayments.Width = 1.15!
        '
        'txtBatchfooterNoofPayments
        '
        Me.txtBatchfooterNoofPayments.DataField = "fldNumber"
        Me.txtBatchfooterNoofPayments.DistinctField = "fldNumber"
        Me.txtBatchfooterNoofPayments.Height = 0.2!
        Me.txtBatchfooterNoofPayments.Left = 3.55!
        Me.txtBatchfooterNoofPayments.Name = "txtBatchfooterNoofPayments"
        Me.txtBatchfooterNoofPayments.Style = "font-size: 10pt; text-align: center; ddo-char-set: 1"
        Me.txtBatchfooterNoofPayments.SummaryGroup = "grTotalHeader"
        Me.txtBatchfooterNoofPayments.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtBatchfooterNoofPayments.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtBatchfooterNoofPayments.Text = "txtBatchfooterNoofPayments"
        Me.txtBatchfooterNoofPayments.Top = 0.145!
        Me.txtBatchfooterNoofPayments.Width = 1.0!
        '
        'txtBatchfooterTransAmount
        '
        Me.txtBatchfooterTransAmount.DataField = "fldAmount"
        Me.txtBatchfooterTransAmount.Height = 0.2!
        Me.txtBatchfooterTransAmount.Left = 5.25!
        Me.txtBatchfooterTransAmount.Name = "txtBatchfooterTransAmount"
        Me.txtBatchfooterTransAmount.OutputFormat = resources.GetString("txtBatchfooterTransAmount.OutputFormat")
        Me.txtBatchfooterTransAmount.Style = "font-size: 10pt; text-align: right; ddo-char-set: 1"
        Me.txtBatchfooterTransAmount.SummaryGroup = "grTotalHeader"
        Me.txtBatchfooterTransAmount.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtBatchfooterTransAmount.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtBatchfooterTransAmount.Text = "txtBatchfooterTransAmount"
        Me.txtBatchfooterTransAmount.Top = 0.145!
        Me.txtBatchfooterTransAmount.Width = 1.188!
        '
        'TotalSum
        '
        Me.TotalSum.DefaultValue = "100"
        Me.TotalSum.FieldType = DataDynamics.ActiveReports.FieldTypeEnum.None
        Me.TotalSum.Formula = "3+3"
        Me.TotalSum.Name = "TotalSum"
        Me.TotalSum.Tag = Nothing
        '
        'LineBatchFooter2
        '
        Me.LineBatchFooter2.Height = 0.0!
        Me.LineBatchFooter2.Left = 2.4!
        Me.LineBatchFooter2.LineWeight = 1.0!
        Me.LineBatchFooter2.Name = "LineBatchFooter2"
        Me.LineBatchFooter2.Top = 0.37!
        Me.LineBatchFooter2.Width = 4.1!
        Me.LineBatchFooter2.X1 = 2.4!
        Me.LineBatchFooter2.X2 = 6.5!
        Me.LineBatchFooter2.Y1 = 0.37!
        Me.LineBatchFooter2.Y2 = 0.37!
        '
        'LineBatchFooter3
        '
        Me.LineBatchFooter3.Height = 0.0!
        Me.LineBatchFooter3.Left = 2.4!
        Me.LineBatchFooter3.LineWeight = 1.0!
        Me.LineBatchFooter3.Name = "LineBatchFooter3"
        Me.LineBatchFooter3.Top = 0.05!
        Me.LineBatchFooter3.Width = 4.1!
        Me.LineBatchFooter3.X1 = 2.4!
        Me.LineBatchFooter3.X2 = 6.5!
        Me.LineBatchFooter3.Y1 = 0.05!
        Me.LineBatchFooter3.Y2 = 0.05!
        '
        'rp_1010_Visma
        '
        Me.MasterReport = False
        Me.CalculatedFields.Add(Me.TotalSum)
        OleDBDataSource1.ConnectionString = ""
        OleDBDataSource1.SQL = "Select * from"
        Me.DataSource = OleDBDataSource1
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 6.489583!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.grTotalHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grTotalFooter)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'Times New Roman'; font-style: italic; font-variant: inherit; font-w" & _
                    "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("", "Heading4", "Normal"))
        CType(Me.txtxtClientNo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAccountNo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblFilename, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFilename, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblHeadClientNo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblAccount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchfooterNoofPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBatchfooterTransAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail
    Private WithEvents txtxtClientNo As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtType As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtAccountNo As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtNumber As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtAmount As DataDynamics.ActiveReports.TextBox
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader
    Private WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Private WithEvents Line3 As DataDynamics.ActiveReports.Line
    Private WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter
    Private WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Private WithEvents grTotalHeader As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents lblFilename As DataDynamics.ActiveReports.Label
    Private WithEvents txtFilename As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblHeadClientNo As DataDynamics.ActiveReports.Label
    Private WithEvents grTotalFooter As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents LineBatchFooter1 As DataDynamics.ActiveReports.Line
    Private WithEvents lblBatchfooterAmount As DataDynamics.ActiveReports.Label
    Private WithEvents lblBatchfooterNoofPayments As DataDynamics.ActiveReports.Label
    Private WithEvents txtBatchfooterNoofPayments As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtBatchfooterTransAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents TotalSum As DataDynamics.ActiveReports.Field
    Private WithEvents lblType As DataDynamics.ActiveReports.Label
    Private WithEvents lblAccount As DataDynamics.ActiveReports.Label
    Private WithEvents lblNumber As DataDynamics.ActiveReports.Label
    Private WithEvents lblCurrency As DataDynamics.ActiveReports.Label
    Private WithEvents lblAmount As DataDynamics.ActiveReports.Label
    Private WithEvents txtCurrency As DataDynamics.ActiveReports.TextBox
    Private WithEvents LineBatchFooter2 As DataDynamics.ActiveReports.Line
    Private WithEvents LineBatchFooter3 As DataDynamics.ActiveReports.Line
End Class 
