Option Strict Off
Option Explicit On
Module WriteNacha
	' Nacha US for American ACH-payments
	'-----------------------------------
	
	Dim sLine As String ' en output-linje
	Dim oBatch As Batch
	' Vi m� dimme noen variable for
	' - batchniv�: sum bel�p, antall records, antall transer, f�rste dato, siste dato
	Dim BatchSumAmount As Decimal
	Dim BatchNoRecords As Double
	' - fileniv�: sum bel�p, antall records, antall transer, siste dato
	'             disse p�virkes nedover i niv�ene
	Dim nFileSumAmount, nFileNoRecords As Double
	Dim dFileFirstDate As Date
	Dim iBatchNumber As Short
	Dim nEntryNumber As Double
	Dim nFileHash, nBatchHash As Double
	Dim nLines As Double
    
    Function WriteNachaUS(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sOwnRef As String, ByVal sCompanyNo As String) As Boolean
        ' XNET 05.10.2011 - MAJOR changes - take whole function
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim i As Integer
        Dim sNewOwnref As String
        Dim bFileStartWritten As Boolean
        Dim sEnterpriseNo As String
        ' added 14.04.2010 - need to find CompanyName from oFilesetup
        Dim oFilesetup As FileSetup
        'XOKNET 03.12.2010, added next 2 lines
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim sCompanyName As String
        ' XNET 05.10.2011
        Dim sOldCompanyNo As String
        Dim sOldCompanyName As String
        Dim sOldAccountNo As String
        Dim sText As String
        Dim oFreeText As Freetext
        Dim bBatchWritten As Boolean  ' XNET added 31.01.2012

        BatchSumAmount = 0
        BatchNoRecords = 0
        nFileSumAmount = 0
        nFileNoRecords = 0

        ' lag en outputfil
        Try

            ' added 14.04.2010 - need to find CompanyName from oFilesetup. Descritpion if present
            For Each oFilesetup In oBabelFiles.VB_Profile.FileSetups
                If oFilesetup.FileSetup_ID = iFormat_ID Then
                    sCompanyName = oFilesetup.Description
                    Exit For
                End If
            Next oFilesetup

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabel In oBabelFiles
                ' Setup for DnBNOR TBI have no room for AdditionalID. Put into Company, and can be found there;
                ' 13.08.2007 - Changed from sAdditionalNo to sCompanyNo, to be able to use different CompanyNos in TBI-setup
                'If EmptyString(sCompanyNo) Then
                '    sCompanyNo = oBabel.VB_Profile.AdditionalNo
                'End If
                ' 04.02.2009 Another change on this one;
                ' if imported from DnBNOR Penta, then we find the companyno in the header, and put's it
                ' into oBabelFile.
                ' XNET 05.10.2012 - reset batchflag, so we can write new batch when we have more than one importfile
                bBatchWritten = False
                If Not EmptyString(oBabel.IDENT_Sender) Then
                    sCompanyNo = oBabel.IDENT_Sender
                End If
                'XOKNET 03.12.2010 Slettet over, noe som var det samme som If-en under
                ' new 14.04.2010
                If EmptyString(sCompanyName) Then
                    sCompanyName = oBabel.VB_Profile.CompanyName
                End If

                ' New 04.02.2009
                sEnterpriseNo = oBabel.VB_Profile.CompanyNo

                ' XNET 05.10.2011 - removed much below, and changed
                bExportoBabel = True

                ' XNET 05.10.2011 - several changes underneath - take whole shit
                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If oPayment.Cancel = False And oPayment.Exported = False Then
                                    ' added 07.11.2008 to be able to export to Nacha only ACH-records:
                                    If oPayment.DnBNORTBIPayType = "ACH" Or EmptyString(oPayment.DnBNORTBIPayType) Then
                                        If Not bMultiFiles Then
                                            bExportoBatch = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = vbNullString
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoBatch = True
                                                End If
                                            End If
                                        End If


                                    End If 'If oPayment.DnBNORTBIPayType = "ACH" Or EmptyString(oPayment.DnBNORTBIPayType) Then
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next


                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                ' XNET 05.10.2011 added next section;
                                If oPayment.I_Account <> sOldAccountNo Then
                                    For Each oFilesetup In oBabel.VB_Profile.FileSetups
                                        For Each oClient In oFilesetup.Clients
                                            For Each oaccount In oClient.Accounts
                                                'If oPayment.I_Account = oaccount.Account Then
                                                ' XNET 26.01.2012 added trim
                                                If Trim$(oPayment.I_Account) = oaccount.Account Or _
                                                oPayment.I_Account = Trim(oaccount.ConvertedAccount) Then
                                                    ' XNET 21.10.2011 - her tester vi ogs� p� konvertert kontonummer - det vil ofte
                                                    ' v�re aktuelt i USA
                                                    ' 08.07.2016 Have to keep Enterpriseno and Companyname from Profilesetup for DNV
                                                    If oBabel.Special <> "DNV" Then
                                                        If Not EmptyString(oFilesetup.Division) Then
                                                            sCompanyNo = oFilesetup.Division
                                                            sEnterpriseNo = oFilesetup.Division
                                                        End If

                                                        ' XNET 12.07.2011 - and 26.01.2012
                                                        ' XNET 31.07.12 - From now on, this goes for ALL companies - find enterpriseno from clientsetup !!!
                                                        'If oBabel.Special = "NHST_US" Or oBabel.Special = "NHST_LONN" Then
                                                        ' Special setup for NHST with several clients
                                                        ' Find sCompanyNo from Clientsetup, EnterpriseNo
                                                        ' XNET 31.07.12
                                                        If Not EmptyString(oClient.CompNo) Then
                                                            sCompanyNo = oClient.CompNo
                                                            sEnterpriseNo = oClient.CompNo

                                                            ' 03.05.2016, for Ler�y US Inc
                                                            If oBabel.Special = "NACHA_LEROY" Then
                                                                sCompanyNo = oClient.Division
                                                            End If
                                                        End If
                                                        ' Find sCompanyName from Clientsetup, ClientName
                                                        sCompanyName = oClient.Name.Trim
                                                    End If  'If oBabel.Special <> "DNV" Then
                                                    Exit For  ' XOKNET 05.10.2011
                                                    'End If
                                                End If
                                            Next oaccount
                                        Next oClient
                                    Next oFilesetup
                                    sOldAccountNo = oPayment.I_Account
                                End If
                                ' end XNET 05.10.2011
                                'If oBabel.Special = "USINC" Then
                                '    oPayment.BANK_I_SWIFTCode = "DNBAUS33"
                                '    oPayment.PayType = "D"
                                'End If

                                ' XNET 05.10.2011 -
                                ' added next If, which is moved from above, and If changed
                                'If sCompanyNo <> sOldCompanyNo Or sCompanyName <> sOldCompanyName Then
                                ' XNET 21.10.2013 -
                                ' If we have several batches for same company, we also need to add new batchheader;
                                If bBatchWritten = False Or (sCompanyNo <> sOldCompanyNo Or sCompanyName <> sOldCompanyName) Then

                                    ' Must write batch end record and file controlrecord for previous company
                                    If BatchNoRecords <> 0 Then
                                        sLine = WriteNachaBatchControlRecord(oBatch, sOldCompanyNo)  ' XNET 31.01.2012 use soldCompany
                                        oFile.WriteLine(sLine)
                                        ' XNET 08.02.2012
                                        bBatchWritten = False
                                    End If

                                    ' XNET 21.10.2013 - added next If /EndIf
                                    If (sCompanyNo <> sOldCompanyNo Or sCompanyName <> sOldCompanyName) Then
                                        If nFileNoRecords > 0 Then
                                            sLine = WriteNachaFileControlRecord()
                                            oFile.WriteLine(sLine)
                                        End If


                                        ' Write one header for each company
                                        'sLine = WriteNachaFileHeader(oBabel, sCompanyNo, sCompanyName)
                                        ' 24.01.2012 - can we use EnterpriseNo instead, from setup?
                                        ' XNET 27.01.2012
                                        '''sLine = WriteNachaFileHeader(oBabel, sEnterpriseNo, sCompanyName)

                                        ' XNET 05.10.2011
                                        sOldCompanyNo = sCompanyNo
                                        sOldCompanyName = sCompanyName
                                        '--------------------------------------------------
                                        'oFile.WriteLine (sLine)
                                        bFileStartWritten = False    'True
                                    End If

                                    ' Changed 14.04.2010, to take care of several profiles with different companynumbers:
                                    ' XNET 31.01.2012 - moved down, to avoid writing empty batches
                                    'sLine = WriteNachaBatchHeader(oBatch, sEnterpriseNo, sCompanyNo, sCompanyName)
                                    'oFile.WriteLine (sLine)
                                    bBatchWritten = False
                                    '---------------------------------------------------

                                End If


                                'For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    If oPayment.Cancel = False And oPayment.Exported = False Then
                                        ' added 07.11.2008 to be able to export to Nacha only ACH-records:
                                        ' 05.04.2016 Added: can only export DNB US Domestic payments
                                        If Left(oPayment.BANK_I_SWIFTCode, 6) = "DNBAUS" And oPayment.PayType <> "I" And oPayment.MON_InvoiceCurrency = "USD" Then
                                            If Not bMultiFiles Then
                                                bExportoPayment = True
                                            Else
                                                If oPayment.I_Account = sI_Account Then
                                                    If InStr(oPayment.REF_Own, "&?") Then
                                                        'Set in the part of the OwnRef that BabelBank is using
                                                        sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                    Else
                                                        sNewOwnref = vbNullString
                                                    End If
                                                    If sNewOwnref = sOwnRef Then
                                                        bExportoPayment = True
                                                    End If
                                                End If
                                            End If
                                        End If 'If oPayment.DnBNORTBIPayType = "ACH" Or EmptyString(oPayment.DnBNORTBIPayType) Then
                                    Else
                                        oPayment.Exported = True
                                    End If 'If oPayment.Cancel = False Then
                                End If

                                If bExportoPayment Then

                                    ' XNET 31.01.2012 - moved here from higher up, to avoid writing empty batches and fileheaders
                                    If Not bFileStartWritten Then
                                        sLine = WriteNachaFileHeader(oBabel, sEnterpriseNo, sCompanyName, sCompanyNo)
                                        oFile.WriteLine(sLine)
                                        bFileStartWritten = True
                                    End If
                                    If Not bBatchWritten Then
                                        sLine = WriteNachaBatchHeader(oBatch, sEnterpriseNo, sCompanyNo, sCompanyName)
                                        oFile.WriteLine(sLine)
                                        bBatchWritten = True
                                    End If

                                    i = 0
                                    For Each oInvoice In oPayment.Invoices
                                        ' XNET 24.01.2012
                                        'i = i + 1
                                        sLine = WriteNachaEntryDetailRecord(oPayment, oInvoice, oBatch.Version, sEnterpriseNo)
                                        '-----------------------------------------------------------
                                        oFile.WriteLine(sLine)

                                        ' XNET 24.01.2012 - this will be crappy if we have more than one oFreetext !
                                        'sLine = WriteNachaAddendumRecord(oPayment, oInvoice, i)
                                        '
                                        For Each oFreeText In oInvoice.Freetexts
                                            sText = sText & Trim$(oFreeText.Text)
                                        Next oFreeText
                                        sText = LTrim(sText)
                                        Do
                                            i = i + 1
                                            sLine = WriteNachaAddendumRecord(sText, i)
                                            oFile.WriteLine(sLine)
                                            sText = Mid$(sText, 81)
                                            If Len(sText) = 0 Then
                                                Exit Do
                                            End If
                                        Loop
                                        '-----------------------------------------------------------

                                    Next
                                    oPayment.Exported = True
                                End If

                            Next ' payment

                            'sLine = WriteNachaBatchControlRecord(oBatch, sEnterpriseNo)
                            ' 14.04.2010 changed to reflect different companynumbers

                            ' XNET 31.01.2012 - added next If, to avoid writing empty batches
                            If bBatchWritten Then
                                sLine = WriteNachaBatchControlRecord(oBatch, sOldCompanyNo)   ' XNET 31.01.2012 use soldCompany
                                oFile.WriteLine(sLine)
                                'XNET 21.10.2013
                                ' To be able to write BatchStartrecord again, we need to set this one;
                                bBatchWritten = False
                            End If
                        End If

                    Next 'batch
                End If
            Next 'Babelfile

            If nFileNoRecords > 0 Then
                sLine = WriteNachaFileControlRecord()
                oFile.WriteLine(sLine)
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteNachaUS" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteNachaUS = True

    End Function

    Function WriteNachaFileHeader(ByRef oBabelFile As BabelFile, ByRef sEnterpriseNo As String, ByRef sCompanyName As String, ByVal sCompanyNo As String) As String
        ' 14.04.2010, added parameter sCompanyName
        Dim sLine As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileNoRecords = 0
        dFileFirstDate = System.DateTime.FromOADate(Now.ToOADate + 1000)
        iBatchNumber = 0
        nLines = 1

        sLine = "1" ' 1. Recordtype code        1
        sLine = sLine & "01" ' 2. Priority code          2-3

        ' If Bank = DnBNOR then
        'sLine = sLine & " 026005610"                                 ' 3. Immediate origin       4-13
        ' Changed 13.08.2007, from sAdditionalID to sEnterpriseNo
        sLine = sLine & PadLeft(Left(sEnterpriseNo, 9), 10, " ") ' 3. Immediate destination  4-13

        ' Question: Immeridate origin (EnterpriseNo) - Can we use the same as in TBI ?
        'sLine = sLine & PadLeft(Right$(oBabelFile.VB_Profile.CompanyNo, 10), 10, " ")   '4. Immeridate origin       14-23
        ' changed 13.08.2007 to use CompanyNo as in pos 4-13
        ' changed again 10.05.2016 to Reflect mail from Jane Epperson, DNB NY
        'sLine = sLine & PadLeft(Right(sEnterpriseNo, 10), 10, " ") '4. Immeridate origin       14-23
        sLine = sLine & PadLeft(Right(sCompanyNo, 10), 10, " ") '4. Immeridate origin       14-23

        sLine = sLine & VB6.Format(Date.Today, "YYMMDD") '5. File creation date     24-29
        sLine = sLine & VB6.Format(Now, "hhmm") '6. File creation time     30-33
        ' Question: Day sequence, starting with A. Do we have to keep track of it?
        sLine = sLine & "A" '7. File ID modified       34

        sLine = sLine & "094" '8. Recordsize             35-37
        sLine = sLine & "10" '9. Blocking factor        38-39
        sLine = sLine & "1" '10.Formatcode             40
        ' If Bank = DnBNOR then
        sLine = sLine & PadRight("DNB NOR NY", 23, " ") '11.Immediate destination  41-63

        'sLine = sLine & PadRight(oBabelFile.VB_Profile.CompanyName, 23, " ") '12.Immediate origin name  64-86
        ' 14.04.2010, added parameter sCompanyName
        sLine = sLine & PadRight(sCompanyName, 23, " ") '12.Immediate origin name  64-86
        sLine = sLine & Space(8) '13.Referencecode          87-94

        WriteNachaFileHeader = sLine

    End Function
    Function WriteNachaEntryDetailRecord(ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef sBatchType As String, ByRef sEnterpriseNo As String) As String
        Dim sLine As String
        Dim sPayCode As String

        nLines = nLines + 1
        ' Question: When to use CTX-record ?

        ' Add to Batch-totals:
        'BatchSumAmount = BatchSumAmount + oPayment.MON_InvoiceAmount
        ' 22.08.2016 corrected error for BatchSumAmount
        BatchSumAmount = BatchSumAmount + oInvoice.MON_InvoiceAmount
        BatchNoRecords = BatchNoRecords + 1
        nEntryNumber = nEntryNumber + 1

        sLine = "6"                                                 ' 1. Recordtype code        1
        ' XNET 24.01.2012, added or .. ="32"
        If InStr(oPayment.E_Account, "S") > 0 Or oPayment.E_AccountSuffix = "32" Then
            ' Savingsaccount
            sLine = sLine & "32"                                    ' 2. Transaction code       2-3
        Else
            sLine = sLine & "22"                                    ' 2. Transaction code       2-3
        End If
        sLine = sLine & PadLeft(oPayment.BANK_BranchNo, 9, "0")     ' 3. / 4. Receiving DFI ID + Check Digit 4-12
        sLine = sLine & PadRight(Strip(oPayment.E_Account, "SPC "), 17, " ") ' 5. DFI AccountNo 13-29
        sLine = sLine & PadLeft(oInvoice.MON_InvoiceAmount, 10, "0") ' 6. Amount                30-39
        ' XNET 24.01.2012 changed 7 ID Number
        'sLine = sLine & Space(15)                                   ' 7. ID Number             40-54
        sLine = sLine & PadLeft(oPayment.REF_Own, 15, " ")                   ' 7. ID Number             40-54
        sLine = sLine & PadRight(oPayment.E_Name, 22, " ")           ' 8. Receivers name        55-76
        sLine = sLine & Space(2)                                     ' 9. Discretion data       77-78
        sLine = sLine & "1"                                          ' 10. Addenda Record ID    79
        ' Trace number: First 8 digits is banknumber (stored in Additonal ID), next 7 is runningnumber
        ' XNET 24.01.2012, changed TraceNumber
        If Not EmptyString(oPayment.REF_Bank1) Then
            sLine = sLine & PadRight(oPayment.REF_Bank1, 15, "0")
        Else
            sLine = sLine & PadRight(Left$(sEnterpriseNo, 8) & PadLeft(nEntryNumber, 7, "0"), 15, " ") '11. Trace number          80-94
        End If

        WriteNachaEntryDetailRecord = sLine

    End Function
    Function WriteNachaAddendumRecord(ByVal sText As String, ByVal iSeqNo As Integer) As String 'oPayment As Payment, oInvoice As Invoice, iSeqNo As Integer) As String

        Dim sLine As String
        Dim oFreeText As FreeText

        BatchNoRecords = BatchNoRecords + 1
        nLines = nLines + 1

        'For Each oFreeText In oInvoice.Freetexts
        '    sText = sText & Trim$(oFreeText.Text)
        'Next oFreeText
        'sText = LTrim(sText)

        sLine = "7"                                                 ' 1. Recordtype code        1
        sLine = sLine & "05"                                        ' 2. Addenda type code      2-3
        'sLine = sLine & PadRight(Left$("NTE* " & sText, 80), 80, " ") ' 3. Payment related info   4-83
        ' 14.01.2009 Removed NTE*, Customer can send structured text if they like, otherwise unstructered!
        sLine = sLine & PadRight(Left$(sText, 80), 80, " ") ' 3. Payment related info   4-83
        ' when run with one one entry/one addenda pr invoice, then seq no will always be 1'

        'XokNET 24.01.2012
        'iSeqNo = 1
        ' If more than one addenda pr entry, change this!
        sLine = sLine & PadLeft(Trim(Str(iSeqNo)), 4, "0")          ' 4. Addenda sequenceno     84-87

        ' Entry Detail Sequencenumber is last 7 digits of EntryDetails TraceNumber ( = EntryNumber)
        sLine = sLine & PadLeft(nEntryNumber, 7, "0")             ' 5. Entry Detail Sequencenumber 88-94

        WriteNachaAddendumRecord = sLine

    End Function

    Function WriteNachaFileControlRecord() As String '(oBabelFile As BabelFile) As String
        ' ok jp
        Dim sLine As String
        nLines = nLines + 1

        sLine = "9" ' 1. Recordtype code        1
        sLine = sLine & PadLeft(CStr(iBatchNumber), 6, "0") ' 2. Batchcounter           2-7
        ' Block Count:
        ' Total count of output lines, including the first and last lines, divided by 10,
        ' rounded up to the nearest integer e.g. 99.9 becomes 100); 6 columns, zero-padded on the Left
        sLine = sLine & PadLeft(CStr(Int((nLines / 10) + 0.99)), 6, "0") ' 3. Block Count            8-13
        sLine = sLine & PadLeft(CStr(nFileNoRecords), 8, "0") ' 4. Entry/Addenda count    14-21
        sLine = sLine & PadLeft(CStr(nFileHash), 10, "0") ' 5. Entry hash             22-31
        sLine = sLine & PadLeft(CStr(0), 12, "0") ' 6. Total debit amount     32-43
        sLine = sLine & PadLeft(CStr(nFileSumAmount), 12, "0") ' 7. Total credit amount    44-55
        sLine = sLine & Space(39) ' 8. Reserved               56-94

        nFileNoRecords = 0   ' added 22.06.2022
        WriteNachaFileControlRecord = sLine

    End Function
    Function WriteNachaBatchHeader(ByRef oBatch As Batch, ByRef sEnterpriseNo As String, ByRef sCompanyNo As String, ByRef sCompanyName As String) As String
        ' 14.04.2010, added parameter sCompanyName
        Dim sLine As String
        Dim sDate As String
        Dim oPayment As Payment

        ' Reset Batch-totals:
        BatchSumAmount = 0
        BatchNoRecords = 0
        iBatchNumber = iBatchNumber + 1
        nLines = nLines + 1

        sLine = "5" ' 1. Recordtype code        1
        sLine = sLine & "220" ' Creditbatch                        ' 2. Serviceclass code      2-4
        'sLine = sLine & PadRight(oBatch.VB_Profile.CompanyName, 16, " ") ' 3. Companyname            5-20
        ' 14.04.2010, added parameter sCompanyName
        sLine = sLine & PadRight(sCompanyName, 16, " ") ' 3. Companyname            5-20

        sLine = sLine & PadRight(oBatch.REF_Own, 20, " ") '4. Company descretionary   21-40
        ' Changed several times, latest 29.04.2009
        'sLine = sLine & PadRight(sEnterpriseNo, 10, " ") '5. Company ID           41-50
        ' and changed again 14.04.2010, to take care of several profiles with different companynumbers:
        sLine = sLine & PadRight(sCompanyNo, 10, " ") '5. Company ID           41-50

        ' Test on account-no, CCD og PPD. If P in E_AccountNo, then PPD.
        ' payments must be grouped on CCD and PPD. Done in BB.EXE

        ' In BB.EXE, we have put CCD or PPD into sVersion
        'If oBatch.Version = "PPD" Then
        '    sLine = sLine & "PPD"                                     '6. Standard Entry code    51-53
        'Else
        '    sLine = sLine & "CCD"                                     '6. Standard Entry code    51-53
        'End If
        ' Changed 14.01.2009 when testing Penta
        If oBatch.Version = "PPD" Then
            sLine = sLine & "PPD" '6. Standard Entry code    51-53
        ElseIf oBatch.Version = "CCD" Then
            sLine = sLine & "CCD" '6. Standard Entry code    51-53
        ElseIf oBatch.Version = "CTX" Then
            sLine = sLine & "CTX" '6. Standard Entry code    51-53
        Else
            ' not given in batch, try first payment
            ' XNET 16.02.2012
            'If oBatch.Payments.Count > 0 Then
            If oBatch.Payments.Count > 0 And Len(oBatch.Payments.Item(1).E_AccountSuffix) > 2 Then
                sLine = sLine & Left$(oBatch.Payments.Item(1).E_AccountSuffix, 3)            '6. Standard Entry code    51-53
            Else
                ' default if no info given;
                sLine = sLine & "CCD"                                     '6. Standard Entry code    51-53
            End If
        End If

        If Not EmptyString(oBatch.REF_Bank) Then
            sLine = sLine & PadRight(oBatch.REF_Bank, 10, " ")             '7. Company Entry descr.   54-63
        Else
            sLine = sLine & "SETTLEMENT"                                  '7. Company Entry descr.   54-63
        End If
        sLine = sLine & Space(6) '8. Company Descr.date     64-69

        ' Question: Effect of "Effective Entry Date" ? Use Today ?
        'sLine = sLine &VB6.Format(Date, "YYMMDD")                        '9. Efective Entry Date    70-75
        ' Changed 20070731 - use first payments date here. Re changes made in TreatTBi for Fast about
        ' dates overdue, which are put 0,1 or 2 days forward
        If oBatch.Payments.Count > 0 Then
            ' added 20.11.2008
            ' we may have a mix of both ACH and other payments. Must therefore try to find first ACH-payment
            ' to capture this payments date:
            sDate = ""
            For Each oPayment In oBatch.Payments
                If oPayment.DnBNORTBIPayType = "ACH" Or EmptyString((oPayment.DnBNORTBIPayType)) Then
                    sDate = oPayment.DATE_Payment
                    Exit For
                End If
            Next oPayment
            'If StringToDate(oBatch.Payments(1).DATE_Payment) >= Date Then


            If Not EmptyString(sDate) Then
                ' XNET 03.06.2012 - Nachadata must be at least 2 days forward.
                ' Changed in several lines below
                'If StringToDate(sDate) >= Date Then
                ' XNET 04.04.2013 - ikke test p� to dager fram, bruk det som er satt i profilen, under avansert oppsett !!!
                'If StringToDate(sDate) >= GetBankday(Date.Today, "US", 2) Then
                ' 22.06.2022 - for NHST har vi ikke slikt oppsett. Legg til 2 dg automatisk
                If StringToDate(sDate) >= GetBankday(Date.Today, "US", 2) Then
                    ' XNET 09.11.2012 - Selv om dato er fram i tid, m� vi teste og justere om det er en ikke bankdag!
                    'sLine = sLine & Mid$(sDate, 3)    '9. Efective Entry Date    70-75
                    ' XNET 04.04.2013 - ikke test p� to dager fram, bruk det som er satt i profilen, under avansert oppsett !!!
                    sLine = sLine & Format(GetBankday(StringToDate(sDate), "US", 0), "yyMMdd")    '9. Efective Entry Date    70-75
                Else
                    ' XNET 04.04.2013 - ikke test p� to dager fram, sett neste dag, da vi allerede har justert med det som er i oppsettet
                    ' 22.06.2022 - legg til 2 dager
                    sLine = sLine & Format(GetBankday(Date.Today, "US", 2), "yyMMdd")                      '9. Efective Entry Date    70-75
                End If
            Else
                sLine = sLine & Format(GetBankday(Date.Today, "US", 2), "yyMMdd")                      '9. Efective Entry Date    70-75
            End If
        Else
            sLine = sLine & Format(Date.Today, "yyMMdd") '9. Efective Entry Date    70-75
        End If
        ' Question: How to use  "Settlement date" ? Julian date format Use Today ?
        'sLine = sLine & Trim$(Str(CLng(Date)))                        '10. Settlement date       76-78
        sLine = sLine & Space(3)
        sLine = sLine & "1" '11. Originator status     79
        sLine = sLine & PadLeft(Left(sEnterpriseNo, 8), 8, "0") '12. Originating DFI Id   80-87
        sLine = sLine & PadLeft(CStr(iBatchNumber), 7, "0") '13. Batch number         88-94

        WriteNachaBatchHeader = sLine

    End Function
	
	Function WriteNachaBatchControlRecord(ByRef oBatch As Batch, ByRef sEnterpriseNo As String) As String '(oBatch) As String
		Dim sLine As String
		
		nLines = nLines + 1
		
		' Add to File-totals:
		nFileSumAmount = nFileSumAmount + BatchSumAmount
		nFileNoRecords = nFileNoRecords + BatchNoRecords
		
		
		
		sLine = "8" ' 1. Recordtype code        1
		sLine = sLine & "220" ' Creditbatch                        ' 2. Serviceclass code      2-4
		sLine = sLine & PadLeft(CStr(BatchNoRecords), 6, "0") ' 3. Entry/addenda count    5-10
		' Entry Hash = Sum of individual Bank Identification numbers (from sEnterpriseNo)
		nBatchHash = Val(Left(sEnterpriseNo, 8)) * nEntryNumber
		nFileHash = nFileHash + nBatchHash
		sLine = sLine & PadLeft(CStr(nBatchHash), 10, "0") ' 4. Entry hash             11-20
		sLine = sLine & PadLeft(CStr(0), 12, "0") ' 5. Total debit amount     21-32
		sLine = sLine & PadLeft(CStr(BatchSumAmount), 12, "0") ' 6. Total credit amount    33-44
		' Changed 13.08.2007, from oBatch.VB_Profile.CompanyNo to sEnterpriseNo
		'sLine = sLine & PadRight(oBatch.VB_Profile.CompanyNo, 10, " ") '7. Company ID           45-54
		sLine = sLine & PadRight(sEnterpriseNo, 10, " ") '7. Company ID           45-54
		sLine = sLine & Space(19) ' 8. Mesage authentication  55-73
		sLine = sLine & Space(6) ' 9. Reserved               74-79
		sLine = sLine & PadLeft(Left(sEnterpriseNo, 8), 8, "0") ' 10. Originating DFI Id    80-87
		sLine = sLine & PadLeft(CStr(iBatchNumber), 7, "0") ' 11. Batch number          88-94
		
		WriteNachaBatchControlRecord = sLine
        BatchNoRecords = 0

	End Function
End Module
