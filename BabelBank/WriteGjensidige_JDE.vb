﻿Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports System.Xml
Module WriteGjensidige_JDE
    Function WriteGjensidigeJDE_S12GL(ByRef oBabelFiles As BabelFiles, ByRef bmultifiles As Boolean, ByRef sFilenameOut As Object, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabelFile As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim bExportoBatch, bExportobabelfile, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim iLineNumber As Integer = 0
        Dim sID11 As String = ""
        Dim sID13 As String = ""
        Dim sID6 As String = ""
        Dim sOldCurrency As String = ""
        Dim iTransactionNumber As Integer = 0
        Dim iMaxTransactionNumber As Integer = 0
        Dim sOldClientID As String = ""
        Dim sBatchNumberLastPart As String = ""
        Dim bDomesticCurrency As Boolean = False
        Dim sTmp As String = ""
        Dim aTransactionArray As String()
        Dim sTransactionString As String
        Dim i As Integer
        Dim bFound As Boolean
        Dim bClientFound As Boolean
        Dim iCurrentArrayElement As Long
        Dim sOldTransactionString As String

        Try
            oFs = New Scripting.FileSystemObject
            ' create an outputfile
            bAppendFile = False
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            ' Last part of batchnumber is based on date/time. For long files, this may change during process.
            ' So lets create the last part of the batchnumber here;
            sBatchNumberLastPart = Format(Now, "yyMMddHHmmss")

            For Each oBabelFile In oBabelFiles

                bExportobabelfile = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported And (oPayment.PayType = "GL_HB01" Or oPayment.PayType = "F2100") Then
                            If Not bmultifiles Then
                                bExportobabelfile = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportobabelfile = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportobabelfile Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportobabelfile Then
                        Exit For
                    End If
                Next oBatch

                If bExportobabelfile Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabelFile.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported And (oPayment.PayType = "GL_HB01" Or oPayment.PayType = "F2100") Then
                                If Not bmultifiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported And (oPayment.PayType = "GL_HB01" Or oPayment.PayType = "F2100") Then
                                    If Not bmultifiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    ' first, test if we have a domestic currency (base currency involved):
                                    ' Datatransformation 3
                                    ' ---------------------
                                    'Input record LE01/LE04	Output record SU
                                    'ID 6 pos 029-032	ID 6 pos 161-163	ID 16 pos 147-147	ID 12 pos 087-101	ID 13 pos 102-116	ID 18 pos 151-165	ID 19 pos 166-180
                                    '30, 31 or 50	DKK	D	AMOUNT	AMOUNT	ZEROES	ZEROES
                                    '30, 31 or 50	<> DKK	F	ZEROES	ZEROES	AMOUNT	AMOUNT
                                    '46, 47, 48, 49	SEK	D	AMOUNT	AMOUNT	ZEROES	ZEROES
                                    '46, 47, 48, 49	<> SEK	F	ZEROES	ZEROES	AMOUNT	AMOUNT
                                    'All other company numbers	NOK	D	AMOUNT	AMOUNT	ZEROES	ZEROES
                                    'All other company numbers	<> NOK	F	ZEROES	ZEROES	AMOUNT	AMOUNT
                                    bDomesticCurrency = False
                                    sTmp = RemoveCharacters(oPayment.I_Client, "abcdefghijklmnopqrstuvwxyzæøåABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ,. ")
                                    If sTmp = "21" Or sTmp = "30" Or sTmp = "31" Or sTmp = "50" Then
                                        If oPayment.MON_InvoiceCurrency = "DKK" Then
                                            bDomesticCurrency = True
                                        End If
                                        ' 05.02.2015 Added "34"
                                        ' 01.07.2020 Added "19"
                                    ElseIf sTmp = "19" Or sTmp = "34" Or sTmp = "46" Or sTmp = "47" Or sTmp = "48" Or sTmp = "49" Then
                                        If oPayment.MON_InvoiceCurrency = "SEK" Then
                                            bDomesticCurrency = True
                                        End If
                                    Else
                                        ' all other companies
                                        If oPayment.MON_InvoiceCurrency = "NOK" Then
                                            bDomesticCurrency = True
                                        End If
                                    End If


                                    sLine = ""
                                    '1	UserID	001-010	21HB01	Transformation 1 FAGSYSTEM and FAGTRANS.	Yes – Input ID 1 and ID2
                                    sLine = sLine & PadRight(oPayment.Unique_ERPID & oPayment.StatusText, 10, " ")

                                    '2	Transaction number	011-032	00001 and running if more than one input file	5 digit running number for each different input company (ID9) or each different GL date (ID12) in the output file starting at 0001, next is 00002 etc	No
                                    'sLine = sLine & PadRight("00001", 22, " ")     ' TODO will there be need for a running number here, or always 00001 ?
                                    ' This number must change each time the currency changes
                                    ' Or each time clientID changes (firmanr, ID9)
                                    'If oPayment.MON_InvoiceCurrency <> sOldCurrency Or oPayment.I_Client <> sOldClientID Then

                                    ' 22.04.2013
                                    ' Changed testing of new transactionsnumbers. There can be equal values for the parameters deciding transactionnu,bers placed all around the inputfile. that is; not in sequence
                                    ' We must therefor have an array with these values, and test if they have been exported before.
                                    ' The values to test for are:
                                    ' ID9 - Doc.Key     - oPayment.I_Client
                                    ' ID12 - Date       - oPayment.Date_Payment
                                    ' ID22 - Currency   - oPayment.MON_InvoiceCurrency
                                    ' 30.04.2013 added ID31 - oPayment.Invoices(1).InvoiceNo

                                    ' and 24.04.2013 - redeone again; just add 1 to transactionnumber if new combo of ID9+ID12+ID22

                                    ' check if present in array;
                                    sTransactionString = oPayment.I_Client & ";" & oPayment.DATE_Payment & ";" & oPayment.MON_InvoiceCurrency & ";" & Trim(oPayment.Invoices(1).InvoiceNo)
                                    If sTransactionString <> sOldTransactionString Then
                                        bFound = False
                                        If Not aTransactionArray Is Nothing Then
                                            For i = 0 To aTransactionArray.GetLength(0) - 1
                                                If xDelim(aTransactionArray(i), ";", 1) & ";" & xDelim(aTransactionArray(i), ";", 2) & ";" & xDelim(aTransactionArray(i), ";", 3) & ";" & Trim(xDelim(aTransactionArray(i), ";", 4)) = sTransactionString Then
                                                    iTransactionNumber = Val(xDelim(aTransactionArray(i), ";", 5))
                                                    iLineNumber = Val(xDelim(aTransactionArray(i), ";", 6))
                                                    iCurrentArrayElement = i
                                                    bFound = True
                                                    Exit For
                                                End If
                                            Next
                                        End If

                                        If Not bFound Then
                                            ' add to array
                                            If aTransactionArray Is Nothing Then
                                                aTransactionArray.Resize(aTransactionArray, 1)
                                            Else
                                                aTransactionArray.Resize(aTransactionArray, aTransactionArray.GetLength(0) + 1)
                                            End If


                                            ' add one to unique transactionnumber
                                            'iTransactionNumber = iTransactionNumber + 1
                                            iTransactionNumber = iMaxTransactionNumber + 1
                                            iMaxTransactionNumber = iTransactionNumber

                                            aTransactionArray(aTransactionArray.GetLength(0) - 1) = sTransactionString & ";" & Str(iTransactionNumber) & ";0"
                                            iCurrentArrayElement = aTransactionArray.GetLength(0) - 1

                                            ' Linenumber in ID4 must start on 1 for each transactionnumber
                                            iLineNumber = 0
                                            sOldCurrency = oPayment.MON_InvoiceCurrency
                                            sOldClientID = oPayment.I_Client
                                        Else
                                            bFound = True
                                        End If

                                    End If

                                    sOldTransactionString = sTransactionString
                                    sLine = sLine & PadLeft(iTransactionNumber.ToString, 5, "0") & Space(17)

                                    '3	Document type	033-034	N/A	Not in use	No
                                    sLine = sLine & "  "

                                    '4	Line Number	035-041	0000001 and running	7 digit running number for each record belonging to the same Transaction number	No
                                    iLineNumber = iLineNumber + 1
                                    ' save linenumber in array;
                                    aTransactionArray(iCurrentArrayElement) = sTransactionString & ";" & Str(iTransactionNumber) & ";" & Str(iLineNumber)
                                    sLine = sLine & PadLeft(iLineNumber.ToString, 7, "0")

                                    '5	Successful process	042-042	0	Fixed value	No
                                    sLine = sLine & "0"

                                    '6	Transaction action	043-043	A	Fixed value	No
                                    sLine = sLine & "A"

                                    '7	Transaction type	044-044	J	Fixed value	No
                                    sLine = sLine & "J"

                                    '8	Batch number	045-059	21H and 12digit running number (per file)	
                                    'Start with Fagsystem (Unique_ERPID) + "H". Running number is per output file. (fixed for all records in the file)	No
                                    ' Can we construct this, or do we need a sequence, with no jumps?
                                    ' TODO Check if this is OK ?
                                    sLine = sLine & oPayment.Unique_ERPID & "H" & sBatchNumberLastPart    'Format(Now, "yyMMddHHmmss")

                                    '9	Document Key	060-064		Transformation 2. FIRMANR 	Yes – Input ID 6
                                    ' Remove non digits
                                    ' 05.09.2019 - For F2100 DK - always 50:
                                    If oPayment.PayType = "F2100" And oPayment.MON_InvoiceCurrency = "DKK" Then
                                        ' F2100 DK
                                        sLine = sLine & "00050"
                                    Else
                                        ' as pre 05.09.2019 - for all other formats
                                        sLine = sLine & PadLeft(KeepNumericsOnly(oPayment.I_Client), 5, "0")
                                    End If

                                    '10	Document	065-072	00000000	Fixed value	No
                                    sLine = sLine & "00000000"

                                    '11	Document type	073-074	UY	Fixed value	No
                                    Select Case oPayment.Unique_ERPID
                                        Case "12"
                                            sLine = sLine & "UX"
                                        Case "20", "24", "32"
                                            sLine = sLine & "UI"
                                        Case "21"
                                            sLine = sLine & "UY"
                                        Case "22"
                                            sLine = sLine & "UO"
                                        Case "23"
                                            sLine = sLine & "UJ"
                                        Case "42"   ' added 28.08.2014 (GINS?)
                                            sLine = sLine & "U7"
                                        Case "43"   ' added 28.08.2014 (GINS?)
                                            sLine = sLine & "U8"
                                        Case "70", "72"
                                            sLine = sLine & "UK"
                                        Case "74"
                                            sLine = sLine & "UF"
                                        Case "77"
                                            sLine = sLine & "U6"
                                        Case "84"
                                            sLine = sLine & "UE"
                                        Case "85"
                                            sLine = sLine & "UW"
                                        Case "88"
                                            sLine = sLine & "UH"
                                        Case "97"
                                            sLine = sLine & "UU"
                                        Case Else
                                            sLine = sLine & "  "
                                    End Select


                                    '12	GL date	075-080	1YYddd	Transformation 3 POSTERINGSDATO 
                                    'ddd = day no 1-365	Yes – Input ID 7
                                    sLine = sLine & "1" & Mid(oPayment.DATE_Payment, 3, 2) & PadLeft((DateDiff(DateInterval.Day, StringToDate(Left(oPayment.DATE_Payment, 4) & "0101"), StringToDate(oPayment.DATE_Payment)) + 1).ToString, 3, "0")

                                    '13	Batch type	081-082	G	Fixed value	No
                                    sLine = sLine & "G "

                                    '14	Company	083-087	00000	Fixed value	No
                                    sLine = sLine & "00000"

                                    '15	Account number	088-116		
                                    '--------------------------
                                    ' For F2100, the transformation is already done in the import
                                    ' 28.08.2014 Added "42" og "43"
                                    If oPayment.PayType = "F2100" Or oPayment.Unique_ERPID = "42" Or oPayment.Unique_ERPID = "43" Or oPayment.Unique_ERPID = "84" Then
                                        sLine = sLine & PadRight(oPayment.E_Account, 29, " ")
                                    Else
                                        'Transformation 5	Yes – Input ID 6, 11 and 13
                                        ' Combine ID13 + ID11 from importfile, but truncate ID11 to 5 digits
                                        sID11 = oPayment.E_Account
                                        If Len(Trim(sID11)) = 8 And Right(sID11, 3) = "000" Then
                                            sID11 = Left(sID11, 5)
                                        ElseIf Len(Trim(sID11)) = 8 Then
                                            ' split sid11
                                            sID11 = Left(sID11, 5) & "+" & Right(sID11, 3)
                                        Else
                                            sID11 = Trim(sID11)
                                        End If
                                        sID13 = oPayment.E_AccountPrefix
                                        sID6 = KeepNumericsOnly(oPayment.I_Client)

                                        If EmptyString(sID13) Then
                                            ' ID13 is empty - combine ID11 + numeric part of ID6 (Firmanr)
                                            sLine = sLine & PadRight(sID6 & "+" & sID11, 29, " ")
                                        Else
                                            sLine = sLine & PadRight(sID13 & "+" & sID11, 29, " ")
                                        End If
                                    End If ' If oPayment.PayType = "F2100" Then

                                    '16	Account mode	117-117	2	Fixed value	No
                                    If oPayment.Unique_ERPID = "20" Then
                                        sLine = sLine & "3"
                                    Else
                                        sLine = sLine & "2"
                                    End If


                                    '17	Sub ledger	118-125	N/A	Not in use	No
                                    sLine = sLine & Space(8)

                                    '18	Sub ledger type	126-126	N/A	Not in use	No
                                    sLine = sLine & " "

                                    '19	Ledger type	127-128	AA or blank	Transformation 7	Yes – Input ID 16
                                    'If oPayment.MON_InvoiceCurrency = "NOK" Or EmptyString(oPayment.MON_InvoiceCurrency) Then
                                    If bDomesticCurrency Or EmptyString(oPayment.MON_InvoiceCurrency) Then
                                        sLine = sLine & "AA"
                                    Else
                                        sLine = sLine & "  "
                                    End If

                                    '20	Sign amount (+/-)	129-129	+ or -	Transformation 6 POSTERINGSNØKKEL 	Yes – Input ID 10
                                    If oPayment.MON_InvoiceAmount < 0 Then
                                        sLine = sLine & "-"
                                    Else
                                        sLine = sLine & "+"
                                    End If

                                    '21	Amount	130-144		Transformation 8. BELOP. 	Yes – Input ID 12 and 16
                                    'If oPayment.MON_InvoiceCurrency = "NOK" Or EmptyString(oPayment.MON_InvoiceCurrency) Then
                                    If bDomesticCurrency Or EmptyString(oPayment.MON_InvoiceCurrency) Then
                                        sLine = sLine & PadLeft(System.Math.Abs(oPayment.MON_InvoiceAmount).ToString, 15, "0")
                                    Else
                                        sLine = sLine & StrDup(15, "0")
                                    End If

                                    '22	Currency	145-147		Transformation 7	Yes – Input ID 16
                                    sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ")

                                    '23	Currency cover rate	148-162	000000000000000	Fixed value	No
                                    sLine = sLine & StrDup(15, "0")

                                    '24	Currency mode	163-163	F or blank	Transformation 7	Yes – Input ID 16
                                    'If oPayment.MON_InvoiceCurrency = "NOK" Or EmptyString(oPayment.MON_InvoiceCurrency) Then
                                    If bDomesticCurrency Or EmptyString(oPayment.MON_InvoiceCurrency) Then
                                        sLine = sLine & " "
                                    Else
                                        sLine = sLine & "F"
                                    End If

                                    '25	Sign currency	164-164	+ or -	Transformation 6	Yes – Input ID 10
                                    If oPayment.MON_InvoiceAmount < 0 Then
                                        sLine = sLine & "-"
                                    Else
                                        sLine = sLine & "+"
                                    End If

                                    '26	Amount currency	165-179		Transformation 8. BELOP. 	Yes – Input ID 12 and 16
                                    'If oPayment.MON_InvoiceCurrency = "NOK" Or EmptyString(oPayment.MON_InvoiceCurrency) Then
                                    If bDomesticCurrency Or EmptyString(oPayment.MON_InvoiceCurrency) Then
                                        sLine = sLine & StrDup(15, "0")
                                    Else
                                        sLine = sLine & PadLeft(System.Math.Abs(oPayment.MON_InvoiceAmount).ToString, 15, "0")
                                    End If

                                    '27	Units	180-194	000000000000000	Fixed value	No
                                    sLine = sLine & StrDup(15, "0")

                                    '28	Units of measure	195-196	N/A	Not in use	No
                                    sLine = sLine & "  "

                                    '29	Name	197-226	F2100	Fixed value	No
                                    Select Case oPayment.Unique_ERPID


                                        Case "12"
                                            sLine = sLine & PadRight("SICS/ARP  (nn=59,65)", 30, " ")
                                        Case "20"
                                            sLine = sLine & PadRight("S2000 - betaling", 30, " ")
                                        Case "21"
                                            sLine = sLine & PadRight("F2100", 30, " ")
                                        Case "22"
                                            sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 30, " ")
                                        Case "23"
                                            sLine = sLine & PadRight("S2000 - ukeavregning premie/pr", 30, " ")
                                        Case "24"
                                            sLine = sLine & PadRight("Kommuneforsikring S2000", 30, " ")
                                        Case "32"
                                            sLine = sLine & PadRight("White Label", 30, " ")
                                        Case "42", "43"  ' Added 28.08.2014 GINS?
                                            sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 30, " ")
                                        Case "70"
                                            sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 30, " ")
                                        Case "72"
                                            sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 30, " ")
                                        Case "74"
                                            sLine = sLine & PadRight("Betafond", 30, " ")
                                        Case "77"
                                            sLine = sLine & PadRight("GIR", 30, " ")
                                        Case "84"
                                            sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 30, " ")
                                        Case "85"
                                            sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 30, " ")
                                        Case "88"
                                            sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 30, " ")
                                        Case "97"
                                            sLine = sLine & PadRight("Premie FD", 30, " ")
                                        Case Else
                                            ' added 28.08.2014
                                            sLine = sLine & PadRight(oPayment.NOTI_NotificationIdent, 30, " ")
                                    End Select

                                    '30	Remark	227-256		Transformation 10. TEKST. 	Yes – Input ID 15
                                    sLine = sLine & PadRight(oPayment.REF_EndToEnd, 30, " ")

                                    '31	Reference 1	257-264		Transformation 4	Yes – Input ID 9
                                    sLine = sLine & PadRight(oPayment.Invoices(1).InvoiceNo, 8, " ")

                                    '32	Reference 2	265-272		Transformation 9 TILORDNING	Yes – Input ID 14
                                    sLine = sLine & PadRight(oPayment.REF_Own, 8, " ")
                                    ' 05.03.2018
                                    ' Dette utformat er i bruk for flere importer. For F2100 SE og DK har det blitt endret, lagt til ny
                                    ' info, som ble lagt inn i Ref_Own - og Ref_Own brukes også lenger ned (351 - 362)
                                    ' Det skal det ikke for F2100 SE/DK, derfor blanker vi her
                                    If oBabelFile.ImportFormat = BabelFiles.FileType.GjensidigeF2100_SE_DK Then
                                        oPayment.REF_Own = ""
                                    End If

                                    '33	Transaction originator	273-282	21HB01	Transformation 1 FAGSYSTEM and FAGTRANS. 	Yes – Input ID 1 and ID2
                                    sLine = sLine & PadRight(oPayment.Unique_ERPID & oPayment.StatusText, 10, " ")

                                    '34	User ID	283-292	21HB01	Transformation 1 FAGSYSTEM and FAGTRANS.	Yes – Input ID 1 and ID2
                                    sLine = sLine & PadRight(oPayment.Unique_ERPID & oPayment.StatusText, 10, " ")

                                    '35	Vendor invoice number	293-317	N/A	Not in use	No
                                    sLine = sLine & StrDup(25, " ")

                                    '36	Date invoice	318-323	1YYddd	Transformation 3 POSTERINGSDATO 
                                    'ddd = day no 1-365	Yes – Input ID 7
                                    ' Changed 11.10.2012 after mail from Torgeir. Now use input ID8 Dokumentdato,041-048 when import from HB01. Stored in Date_Value
                                    'If Left(oPayment.DATE_Value, 4) = Year(Today).ToString Then
                                    sLine = sLine & "1" & Mid(oPayment.DATE_Value, 3, 2) & PadLeft((DateDiff(DateInterval.Day, StringToDate(Left(oPayment.DATE_Value, 4) & "0101"), StringToDate(oPayment.DATE_Value)) + 1).ToString, 3, "0")
                                    'Else
                                    ' imported date from last year ?
                                    'sLine = sLine & "1" & (Val(Mid(oPayment.DATE_Value, 3, 2))).ToString & PadLeft((DateDiff(DateInterval.Day, StringToDate(Left(oPayment.DATE_Value, 4) & "0101"), StringToDate(oPayment.DATE_Value)) + 1).ToString, 3, "0")
                                    'End If

                                    '37	Audit trail	324-348	N/A	Not in use	No
                                    sLine = sLine & StrDup(25, " ")

                                    '38	Tag field follows	349-349	J	Fixed value	No
                                    sLine = sLine & "J"

                                    '39	Managerial type 1	350-350	X	Fixed value	No
                                    sLine = sLine & "X"

                                    '40	Managerial code 1	351-362		Transformation 9 TILORDNING	Yes – Input ID 14
                                    sLine = sLine & PadRight(oPayment.REF_Own, 12, " ")

                                    '41	Managerial type 2	363-363	N/A	Not in use	No
                                    sLine = sLine & " "

                                    '42	Managerial code 2	364-375	N/A	Not in use	No
                                    sLine = sLine & StrDup(12, " ")

                                    '43	Managerial type 3	376-376	N/A	Not in use	No
                                    sLine = sLine & " "

                                    '44	Managerial code 3	377-388	N/A	Not in use	No
                                    sLine = sLine & StrDup(12, " ")

                                    '45	Managerial type 4	389-389	N/A	Not in use	No
                                    sLine = sLine & " "

                                    '46	Managerial code 4	390-401	N/A	Not in use	No
                                    sLine = sLine & StrDup(12, " ")

                                    '47	EOR	402-402	/	Fixed value	No
                                    sLine = sLine & "/"

                                    oFile.WriteLine(sLine)

                                    oPayment.Exported = True ' Must set as exported to avoid errormsg.
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabelFile

        Catch ex As Exception

            'KI: Er dette korrekt, før endring til ny feilhåndtering så ble det ikke reist en ny feilmelding
            'Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_S12GL" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

            WriteGjensidigeJDE_S12GL = True

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGjensidigeJDE_S12GL = True
        Exit Function

    End Function
    Function WriteGjensidigeJDE_Basware_SUGL(ByRef oBabelFiles As BabelFiles, ByRef bmultifiles As Boolean, ByVal sFilenameOut As Object, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByVal sAccountSystem As String) As Boolean
        ' For Basware and FileNet export
        ' Exports the SU and GL records
        ' --------------------------------
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabelFile As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim bExportoBatch, bExportobabelfile, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim iLineNumber As Integer = 0
        Dim dTotalAmount As Double = 0
        Dim iTransactionNumber As Integer = 0
        Dim xVoucherNo As String = ""
        Dim iEDILineNumGL As Integer = 0
        Dim iEDILineNumSU As Integer = 0

        Try
            oFs = New Scripting.FileSystemObject
            ' create an outputfile
            bAppendFile = False

            If sAccountSystem = "FileNet" Or sAccountSystem = "P2000" Then
                ' add extension
                sFilenameOut = sFilenameOut & ".S08'"
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForWriting, True, 0)
            Else
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            End If


            ' Create a startrecord;
            ' 20.08.2018 - for BasWare_XML, the last part of StartRecord is changed for U5 to U1 (XML only!)
            If oBabelFiles.Count > 0 Then
                If oBabelFiles(1).ImportFormat = BabelFiles.FileType.GjensidigeJDE_Basware_XML Then
                    sLine = WriteBasware_SUGL_StartRecord("Basware_XML")
                Else
                    sLine = WriteBasware_SUGL_StartRecord(sAccountSystem)
                End If
            Else
                sLine = WriteBasware_SUGL_StartRecord(sAccountSystem)
            End If
            oFile.WriteLine(sLine)

            For Each oBabelFile In oBabelFiles

                bExportobabelfile = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported And (oPayment.PayType = "SU" Or oPayment.PayType = "GL") Then
                            If Not bmultifiles Then
                                bExportobabelfile = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportobabelfile = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportobabelfile Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportobabelfile Then
                        Exit For
                    End If
                Next oBatch

                If bExportobabelfile Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabelFile.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported And (oPayment.PayType = "SU" Or oPayment.PayType = "GL") Then
                                If Not bmultifiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And Not oPayment.Exported And (oPayment.PayType = "SU" Or oPayment.PayType = "GL") Then
                                    If Not bmultifiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    sLine = ""
                                    sLine = WriteGjensidigeJDE_S12_SUGLRecord(oPayment, iLineNumber, dTotalAmount, sAccountSystem, iTransactionNumber, xVoucherNo, iEDILineNumGL, iEDILineNumSU)
                                    oFile.WriteLine(sLine)
                                    xVoucherNo = oPayment.VoucherNo

                                    oPayment.Exported = True ' Must set as exported to avoid errormsg.
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabelFile

            If Not oFile Is Nothing Then
                ' Create an endrecord;
                sLine = WriteBasware_SUGL_EndRecord(iLineNumber, dTotalAmount)
                oFile.WriteLine(sLine)

                oFile.Close()
                oFile = Nothing
            End If

        Catch ex As Exception

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            'KI: Er dette korrekt, før endring til ny feilhåndtering så ble det ikke reist en ny feilmelding
            'Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_Basware_SUGL" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

            WriteGjensidigeJDE_Basware_SUGL = True

        Finally

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGjensidigeJDE_Basware_SUGL = True

    End Function
    Private Function WriteBasware_SUGL_StartRecord(ByVal sAccountSystem As String)
        'Basware_SUGL- startrecord
        ' Start Record

        'Output ID	Field name	Position	Values	Comment	From input file
        '1	FILENUMBER	001-004	0001	Fixed value	No
        '2	RECORDNR	005-010	0000001	Fixed value	No
        '3	RECORDTYPE	011-012	01	Fixed value	No
        '4	ST-EDIUSERID	013-022	LE01	Fixed value	No
        '5	ST-DATEUPDATED	023-030	MMDDYY?	Date?	No
        '6	ST-TIMEUPDATED	031-036	000000	Fixed value	No
        '7	ST-DOCUMENTTYPE	037-038	U5	Fixed value	No

        Dim sLine As String = ""
        Dim dDate As String
        sLine = sLine & "0001"
        sLine = sLine & "000001"
        sLine = sLine & "01"
        sLine = sLine & "LE01" & Space(6)
        'sLine = sLine & Format(Now, "ddMMyy") & "  "
        ' Julian date 1+YY+ddd
        sLine = sLine & PadRight("1" & Date2Julian(Now).ToString, 8, " ")
        sLine = sLine & "000000"
        If sAccountSystem = "FileNet" Then
            sLine = sLine & "UD"
        ElseIf sAccountSystem = "P2000" Then
            sLine = sLine & "UO"
        ElseIf sAccountSystem = "Basware_XML" Then  ' 20.08.2018 - added this If
            sLine = sLine & "U1"
        Else
            sLine = sLine & "U5"
        End If

        WriteBasware_SUGL_StartRecord = sLine
    End Function
    Private Function WriteGjensidigeJDE_S12_SUGLRecord(ByVal oPayment As Payment, ByRef iLineNumber As Integer, ByRef dTotalAmount As Double, ByVal sAccountSystem As String, ByRef iTransactionNumber As Integer, ByVal xVoucherNo As String, ByRef iEDILineNumGL As Integer, ByRef iEDILineNumSU As Integer) As String
        '    Dim iTransactionNumber As Integer = 0
        Dim sBatchNumberLastPart As String = ""
        '   Dim xVoucherNo As String = ""
        'Dim iEDILineNumGL As Integer = 0
        'Dim iEDILineNumSU As Integer = 0
        Dim bDomesticCurrency As Boolean = False
        Dim sTmp As String = ""
        Dim sLine As String


        If oPayment.VoucherNo <> xVoucherNo Then
            ' ID 17 in inputfile, GL Document, has changed.
            ' Then, change Transactionnumber
            iTransactionNumber = iTransactionNumber + 1
            'xVoucherNo = oPayment.VoucherNo
            iEDILineNumGL = 0
            iEDILineNumSU = 0
        End If
        ' first, test if we have a domestic currency (base currency involved):
        ' Datatransformation 3
        ' ---------------------
        'Input record LE01/LE04	Output record SU
        'ID 6 pos 029-032	ID 6 pos 161-163	ID 16 pos 147-147	ID 12 pos 087-101	ID 13 pos 102-116	ID 18 pos 151-165	ID 19 pos 166-180
        '30, 31 or 50	DKK	D	AMOUNT	AMOUNT	ZEROES	ZEROES
        '30, 31 or 50	<> DKK	F	ZEROES	ZEROES	AMOUNT	AMOUNT
        '46, 47, 48, 49	SEK	D	AMOUNT	AMOUNT	ZEROES	ZEROES
        '46, 47, 48, 49	<> SEK	F	ZEROES	ZEROES	AMOUNT	AMOUNT
        'All other company numbers	NOK	D	AMOUNT	AMOUNT	ZEROES	ZEROES
        'All other company numbers	<> NOK	F	ZEROES	ZEROES	AMOUNT	AMOUNT
        bDomesticCurrency = False
        sTmp = RemoveCharacters(oPayment.I_Client, "abcdefghijklmnopqrstuvwxyzæøåABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ,. ")
        ' 10.01.2017 - added company 38, ok
        ' 16.10.2019 - added selskap "97"
        If sTmp = "30" Or sTmp = "31" Or sTmp = "38" Or sTmp = "50" Or sTmp = "97" Then
            ' 20.03.2015 added Trim(
            If oPayment.MON_InvoiceCurrency = "DKK" Or Trim(oPayment.MON_InvoiceCurrency) = "" Then
                oPayment.MON_InvoiceCurrency = "DKK"
                bDomesticCurrency = True
            End If
            ' 05.02.2015 added "34"
            ' ' 16.10.2019 - added selskap "19"
        ElseIf sTmp = "19" Or sTmp = "34" Or sTmp = "46" Or sTmp = "47" Or sTmp = "48" Or sTmp = "49" Then
            If oPayment.MON_InvoiceCurrency = "SEK" Or Trim(oPayment.MON_InvoiceCurrency) = "" Then
                oPayment.MON_InvoiceCurrency = "SEK"
                bDomesticCurrency = True
            End If
        Else
            ' all other companies
            If oPayment.MON_InvoiceCurrency = "NOK" Or Trim(oPayment.MON_InvoiceCurrency) = "" Then
                oPayment.MON_InvoiceCurrency = "NOK"
                bDomesticCurrency = True
            End If
        End If


        ' ---------
        '1	FILENR	001-004	0001	Fixed value	No
        sLine = sLine & "0001"
        '2	RECORDNR	005-010	0000001 and running	Start on 000001 and + 1 for each record	No
        iLineNumber = iLineNumber + 1
        sLine = sLine & PadLeft(iLineNumber.ToString, 6, "0")
        If oPayment.PayType = "SU" Then
            ' SU record
            ' ---------

            '3	RECORDTYPE	011-012	04	Fixed value	No
            sLine = sLine & "04"
            If sAccountSystem = "FileNet" Or sAccountSystem = "P2000" Then
                '4	SU-EDITRANSACTIONN	013-034	00001	Fixed value	No
                sLine = sLine & "00001" & Space(17)
            Else
                '4	SU-EDITRANSACTIONN	013-034	00001and running	Start on 00001 and identical for corresponding 04 and 09 records	No
                sLine = sLine & PadLeft(iTransactionNumber.ToString, 5, "0") & Space(17)
            End If
            '5	SU-EDILINENUM	035-041	0000001 and running within the same transaction (ID 4 above)	The same value for the corresponding 04 and 09 records	No
            iEDILineNumSU = iEDILineNumSU + 1
            sLine = sLine & PadLeft(iEDILineNumSU.ToString, 7, "0")

            If sAccountSystem = "FileNet" Or sAccountSystem = "P2000" Then
                '6	SU-DOCUMENT	042-049	0	Fixed value	No
                'sLine = sLine & Space(7) & "0"
                ' changed 18.09.2015
                sLine = sLine & StrDup(8, "0")
                '7	SU-ADRESSNUM	050-057	00000000	Fixed value	No
                sLine = sLine & StrDup(8, "0")
            Else
                '6	SU-DOCUMENT	042-049		Document ID	Yes – LE01 input ID 17
                sLine = sLine & PadLeft(oPayment.VoucherNo, 8, " ")
                '7	SU-ADRESSNUM	050-057		TRANS ID. Use leading zeros	Yes – LE01 input ID 3
                sLine = sLine & PadLeft(oPayment.E_Account, 8, "0")
            End If

            '8	SU-PAYEE-ADRESSNUM	058-065	00000000	Fixed value	No
            sLine = sLine & "00000000"
            '9	SU-DATEGLANDVOUCHER	066-073	DDMMYYYY	POSTERINGSDATO (Hovedboksdato)	Yes – LE01 input ID 7
            sLine = sLine & Right(oPayment.DATE_Payment, 2) & Mid(oPayment.DATE_Payment, 5, 2) & Left(oPayment.DATE_Payment, 4)
            '10	SU-DATEINVOICE	074-081	DDMMYYYY	DOKUMENTDATO (Fakturadato)	Yes – LE01 input ID 8
            sLine = sLine & Right(oPayment.ERA_Date, 2) & Mid(oPayment.ERA_Date, 5, 2) & Left(oPayment.ERA_Date, 4)
            '11	SU-COMPANY	082-086	000xx	Fill with leading zeros	Yes – LE01 input ID 6
            ' leave numbers only
            sLine = sLine & PadLeft(RemoveCharacters(oPayment.I_Client, "abcdefghijklmnopqrstuvwxyzæøåABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ,. "), 5, "0")
            '12	SU-AMOUNTGROSS	087-101	Fill with leading zeros.	If ID 16 = D	Yes – LE01 input ID 11
            ' first - or blank
            If oPayment.MON_InvoiceAmount < 0 Then
                sLine = sLine & "-"
            Else
                sLine = sLine & " "
            End If
            'If oPayment.MON_InvoiceCurrency = "NOK" Or EmptyString(oPayment.MON_InvoiceCurrency) Then
            If bDomesticCurrency Then  ' added 04.04.2013
                sLine = sLine & PadLeft(System.Math.Abs(oPayment.MON_InvoiceAmount).ToString, 14, "0")
            Else
                sLine = sLine & StrDup(14, "0")
            End If

            '13	SU-AMOUNTTAXABLE	102-116	Fill with leading zeros.	If ID 16 = D	Yes – LE01 input ID 11
            ' first - or blank
            ' 04.04.2013 - always positive amount in this field
            sLine = sLine & " "
            If bDomesticCurrency Then
                sLine = sLine & PadLeft(System.Math.Abs(oPayment.MON_InvoiceAmount).ToString, 14, "0")
            Else
                sLine = sLine & StrDup(14, "0")
            End If

            '14	SU-AMOUNTNONTAX	117-131	0	Fixed? Eller kan det være et beløp her, dvs diff på ID 12 og 13 over	No
            sLine = sLine & " " & StrDup(14, "0")
            '15	SU-AMOUNTTAX	132-146	0	Fixed? Eller kan det være et beløp her, dvs diff på ID 12 og 13 over	No
            sLine = sLine & " " & StrDup(14, "0")
            '16	SU-CURRENCYMODE	147-147	D	Lookup based on OUTPUT ID 11 and ID 17 (Input ID 6 and 15)	Yes
            If bDomesticCurrency Then
                sLine = sLine & "D"
            Else
                sLine = sLine & "F"
            End If

            '17	SU-CURRENCODEFROM	148-150	Currency	Normally NOK	Yes – LE01 input ID 15
            sLine = sLine & PadRight(oPayment.MON_InvoiceCurrency, 3, " ")
            '18	SU-AMOUNTFOREIGNOPEN	151-165	Fill with leading zeros.	If ID 16 = F	Yes – LE01 input ID 11
            ' first - or blank
            If oPayment.MON_InvoiceAmount < 0 Then
                sLine = sLine & "-"
            Else
                sLine = sLine & " "
            End If
            If bDomesticCurrency Then
                sLine = sLine & StrDup(14, "0")
            Else
                sLine = sLine & PadLeft(System.Math.Abs(oPayment.MON_InvoiceAmount).ToString, 14, "0")
            End If

            '19	SU-FOREIGNDISCAVAIL	166-180	Fill with leading zeros.	If ID 16 = F	Yes – LE01 input ID 11
            ' first - or blank
            ' 04.04.2013 - always positive amount in this field
            sLine = sLine & " "
            If bDomesticCurrency Then
                sLine = sLine & StrDup(14, "0")
            Else
                sLine = sLine & PadLeft(System.Math.Abs(oPayment.MON_InvoiceAmount).ToString, 14, "0")
            End If
            '20	SU-FOREIGNDISCTAKEN	181-195	0	Fixed	No
            sLine = sLine & " " & StrDup(14, "0")
            '21	SU-FOREIGNTAXABLE	196-210	0	Fixed	No
            sLine = sLine & " " & StrDup(14, "0")
            '22	SU-FOREIGNNONTAX	211-225	0	Fixed	No
            sLine = sLine & " " & StrDup(14, "0")
            '23	SU-FOREIGNTAX	226-240	0	Fixed	No
            sLine = sLine & " " & StrDup(14, "0")
            '24	SU-TAXRATE	241-250	0	Fixed	No
            sLine = sLine & Space(10)
            '25	SU-TAXEXPLCODE1	251-252	0	Fixed	No
            sLine = sLine & Space(2)
            '26	SU-TAYMENTTERMCODE	253-255	0	Fixed	No
            sLine = sLine & Space(3)
            If sAccountSystem = "FileNet" Or sAccountSystem = "P2000" Then
                '27	SU-DATENETDUE	256-263	0	Fixed	No
                sLine = sLine & StrDup(8, "0")
            Else
                '27	SU-DATENETDUE	256-263		Forfallsdato	Yes – LE01  input ID18
                sLine = sLine & Right(oPayment.DATE_Value, 2) & Mid(oPayment.DATE_Value, 5, 2) & Left(oPayment.DATE_Value, 4)
            End If
            '28	SU-INVOICENUMBER	264-288	Alphanumeric	REFERANSENUMMER	Yes – LE01 input ID 9
            ' assumes only 1 invoice !
            sLine = sLine & PadRight(oPayment.Invoices(1).InvoiceNo, 25, " ")
            If sAccountSystem = "FileNet" Or sAccountSystem = "P2000" Then
                '29	SU-REMARK	289-318	<blank>	Fixed value	No
                'sLine = sLine & Space(30)
                ' 10.06.2013 - If KID, then put into ID29, otherwise blan (message to ID30, as before)
                If EmptyString(oPayment.Invoices(1).Unique_Id) Then
                    ' no kid
                    sLine = sLine & Space(30)
                Else
                    ' have KID, put into ID29
                    sLine = sLine & "*" & PadRight(oPayment.Invoices(1).Unique_Id, 29, " ")
                End If
            Else
                'Melding til leverandør  in pos 290-318	Yes – LE01 input ID 19
                '29	SU-REMARK	289-318	Alphanumeric	Pos 289 shall hold an ‘*’
                If EmptyString(oPayment.REF_EndToEnd) Then
                    ' no * if empty field
                    sLine = sLine & Space(30)
                Else
                    sLine = sLine & "*"
                    sLine = sLine & PadRight(oPayment.REF_EndToEnd, 29, " ")
                End If
            End If
            If sAccountSystem = "FileNet" Or sAccountSystem = "P2000" Then
                '30	SU-MSGTOSUPPLIER	319-348	Alphanumeric	Text reference	Yes – LE01 input ID 14
                sLine = sLine & PadRight(oPayment.REF_EndToEnd, 30, " ")
            Else
                '30	SU-MSGTOSUPPLIER	319-348	<blank>	Fixed value	No
                'sLine = sLine & Space(30)
                ' 18.10.2013 changed to ..
                sLine = sLine & PadRight(oPayment.REF_Bank1, 30, " ")
            End If
            If sAccountSystem = "FileNet" Or sAccountSystem = "P2000" Then
                '31	SU-TAXID	349-368	Numeric	Vendor ID	Yes – LE01 input ID 3
                sLine = sLine & PadRight(oPayment.E_Account, 20, " ")
            Else
                '31	SU-TAXID	349-368	<blank>	Fixed value	No
                sLine = sLine & Space(20)
            End If
            If sAccountSystem = "FileNet" Or sAccountSystem = "P2000" Then
                '32	SU-REFERENCE1	369-393	<blank>	Fixed value
                sLine = sLine & Space(25)
            Else
                '32	SU-REFERENCE1	369-393	Alphanumeric	Ref?	Yes – LE01 input ID 16
                sLine = sLine & PadRight(oPayment.REF_Own, 25, " ")
            End If
            '33	Not In Use	394-402	<blank>	Fixed value	No
            sLine = sLine & Space(9)
            '34	Record End	403-403	/ 	Fixed	No
            sLine = sLine & "/"

        Else

            ' GL record
            ' ---------
            dTotalAmount = dTotalAmount + oPayment.MON_InvoiceAmount

            '3	RECORDTYPE	011-012	09	Fixed value	No
            sLine = sLine & "09"
            If sAccountSystem = "FileNet" Or sAccountSystem = "P2000" Then
                '4	GL-EDITRANSACTIONN	013-034	00001	Fixed value	No
                sLine = sLine & "00001" & Space(17)
            Else
                '4	GL-EDITRANSACTIONN	013-034	00001and running	Start on 00001 and identical for corresponding 04 and 09 records	No
                sLine = sLine & PadLeft(iTransactionNumber.ToString, 5, "0") & Space(17)
            End If
            '5	GL-EDILINENUM	035-041	0000001 and running within the same transaction (ID 4 above)	The same value for the corresponding 04 and 09 records	No
            iEDILineNumGL = iEDILineNumGL + 1
            sLine = sLine & PadLeft(iEDILineNumGL.ToString, 7, "0")
            If sAccountSystem = "FileNet" Or sAccountSystem = "P2000" Then
                '6	GL-DOCUMENT	042-049	0	Fixed value	No
                sLine = sLine & Space(7) & "0"
            Else
                '6	GL-DOCUMENT	042-049		Document ID	Yes – LE02 input ID 17
                sLine = sLine & PadLeft(oPayment.VoucherNo, 8, " ")
            End If
            '7	GL-EDITRANSACTIONTYPE	050-050	J	Fixed value	No
            sLine = sLine & "J"
            '8	GL-COMPANY	051-055	00000	Fixed value	No
            sLine = sLine & "00000"
            '9	GL-ACCTNOIMPUTMODE	056-084	Example: 
            '   10-37220+54741	A ‘+’ has to be inserted between input ID 12 and ID 10	Yes – LE02 input ID 12 and input ID 10
            '   In some cases input ID 6 and input ID 10
            ' This conversion is done in the import of Basware - Data Transformation 1. Account number
            sLine = sLine & PadRight(oPayment.E_Account, 29, " ")
            '10	GL-ACCTMODEGL	085-085	2	Fixed value	No
            sLine = sLine & "2"
            '11	GL-SUBLEDGER	086-093		Not mandatory in the input file	Yes – LE02 input ID 22
            sLine = sLine & PadRight(oPayment.E_AccountSuffix, 8, " ")
            '12	GL-SUBLEDGERTYPE	094-094		Normally A	Yes – LE02 input ID 20
            sLine = sLine & PadRight(oPayment.VoucherType, 1, " ")
            '13	GL-LEDGERTYPE	095-096	AA	Fixed value	No
            sLine = sLine & "AA"
            '14	GL-AMOUNT	097-111		Amount in øre	Yes – LE02 input ID 11
            If oPayment.MON_InvoiceAmount < 0 Then
                sLine = sLine & "-"
            Else
                sLine = sLine & " "
            End If
            sLine = sLine & PadLeft(System.Math.Abs(oPayment.MON_InvoiceAmount).ToString, 14, "0")
            '15	GL-NAMEREMARKEXPL	112-141		REFERANSENR	Yes – LE02 input ID 13. If empty then use  – LE02 input ID 9
            sLine = sLine & PadRight(oPayment.REF_Own, 30, " ")
            '16	GL-MANAGERIALTYPE1	142-142	<blank>	Fixed value	No
            sLine = sLine & " "
            '17	GL-MANAGERIALCODE1	143-154	<blank>	Fixed value	No
            sLine = sLine & Space(12)
            '18	GL-NAMEALPHAEXPL	155-184	<blank>	Fixed value	No
            sLine = sLine & Space(30)
            '19	GL-MSGTOSUPPLIER2	185-214		TEKST	Yes – LE02 input ID 14
            sLine = sLine & PadRight(oPayment.REF_EndToEnd, 30, " ")
            '20 GL-SUBLEDGERTYPE 222-222  - Avvent
            sLine = sLine & " "
            '21 Not in use 223-223
            sLine = sLine & " "
            '22 GL SUBLEDGER 224-230
            sLine = sLine & PadRight(oPayment.e_OrgNo, 7, " ")

            ' 19.03.2018 - endringer etter at vi fikk XML input
            '20	Not In Use	231-402	<blank>	Fixed value	No
            sLine = sLine & Space(179)
            '21	Record End	403-403	/ 	Fixed value	No - 19.03.2018 endret fra felt 21
            sLine = sLine & "/"


        End If
        oPayment.Exported = True

        WriteGjensidigeJDE_S12_SUGLRecord = sLine
    End Function

    Private Function WriteBasware_SUGL_EndRecord(ByVal nTransactionNumber As Long, ByVal dTotalAmount As Double)
        'Basware_SUGL- startrecord
        'End Record

        'Output ID	Field name	Position	Values	Comment	From input file
        '1	FILENR	001-004	0001	Fixed value	No
        '2	RECORDNR	005-010	Numeric	= value of last 04/09 record	No
        '3	RECORDTYPE	011-012	99	Fixed value	No
        '4	ED-TOTALRECORDS	013-018	Should equal Record number + 2	Number of records including Start and End record	No
        '5	ED-TOTALAMOUNT	019-034		Total amount in GL or SU records. (I.e. total/2) First position (pos 19) shall hold the minus sign ‘-‘	No


        Dim sLine As String = ""
        sLine = sLine & "0001"
        sLine = sLine & PadLeft(nTransactionNumber.ToString, 6, "0")
        sLine = sLine & "99"
        nTransactionNumber = nTransactionNumber + 2
        sLine = sLine & PadLeft(nTransactionNumber.ToString, 6, "0")
        ' always negative totalamount
        'sLine = sLine & "-" & PadLeft((dTotalAmount).ToString, 15, "0")
        ' 11.08.2014 NO minussign, and always positive amount
        'sLine = sLine & PadLeft((Math.Abs(dTotalAmount)).ToString, 16, "0")
        ' 15.02.2015 - new changes, add minussign
        ' --------------------------------
        ' and 29.08.2018 - another change.
        ' If positiv amount, then use "-", but if negative amount, then use "0" as first "sign"
        ' thus; new If added below:
        If dTotalAmount < 0 Then
            sLine = sLine & "0" & PadLeft((Math.Abs(dTotalAmount)).ToString, 15, "0")
        Else
            sLine = sLine & "-" & PadLeft((Math.Abs(dTotalAmount)).ToString, 15, "0")
        End If

        WriteBasware_SUGL_EndRecord = sLine
    End Function
    Function WriteGjensidigeJDE_Filenet_Address(ByRef oBabelFiles As BabelFiles, ByRef bMultifiles As Boolean, ByVal sFilenameOut As Object, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        ' For Filenet
        ' Exports the ML01-records here, rest in Basware export
        ' ---------------------------------------------------
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim oBabelFile As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim bExportoBatch, bExportobabelfile, bExportoPayment As Boolean
        Dim bAppendFile As Boolean
        Dim iLineNumber As Integer = 0

        Try
            oFs = New Scripting.FileSystemObject
            ' create an outputfile
            bAppendFile = False
            ' add extension
            sFilenameOut = sFilenameOut & ".S04'"

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForWriting, True, 0)

            For Each oBabelFile In oBabelFiles

                bExportobabelfile = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.PayType = "AB" And Not oPayment.Exported Then
                            If Not bMultifiles Then
                                bExportobabelfile = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportobabelfile = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportobabelfile Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportobabelfile Then
                        Exit For
                    End If
                Next oBatch

                If bExportobabelfile Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabelFile.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.PayType = "AB" And Not oPayment.Exported Then
                                If Not bMultifiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.PayType = "AB" And Not oPayment.Exported Then
                                    If Not bMultifiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    sLine = ""
                                    ' ---------
                                    '1  EDIUSERID	001-010	05ML01	Fixed value for "normal FileNet, 22ML01 for P2000
                                    If oPayment.Unique_ERPID = "22" Then  ' P2000
                                        sLine = sLine & "22ML01" & Space(4)
                                    Else
                                        sLine = sLine & "05ML01" & Space(4)
                                    End If
                                    '2	EDIBATCHNUMBER	011-025	Eksempel fra fil 05ML3001211299	Et unikt nummer pr fil. F.eks basert på dato og klokkeslett
                                    If oPayment.Unique_ERPID = "22" Then  ' P2000
                                        sLine = sLine & PadLeft("22ML" & Format(Now, "yyMMddHHmmss"), 15, " ")
                                    Else
                                        sLine = sLine & PadLeft("05ML" & Format(Now, "yyMMddHHmmss"), 15, " ")
                                    End If
                                    '3	EDITRANSACTIONNUM	026-047	Eksempel fra fil 00001	Et nummer pr bilag (samme for alle i inputfilen?)
                                    sLine = sLine & "00001" & Space(17)
                                    iLineNumber = iLineNumber + 1
                                    '4	EDILINENUM	048-054	0000001 and running	7 digit running number for each record belonging to the same Transaction number (field above)
                                    sLine = sLine & PadLeft(iLineNumber.ToString, 7, "0")
                                    '5	TYPETRANSACTION	055-062	<blank>	Fixed value	No
                                    sLine = sLine & Space(8)
                                    '6	EDIDETAILINEPROCSD	063-067	00000	Fixed value	No
                                    sLine = sLine & "00000"
                                    '7	EDISUCCESSFULLYPROCSD	068-068	0	Fixed value	No
                                    sLine = sLine & "0"
                                    '8	TRANSACTIONACTION	069-070	<blank>	Fixed value	No
                                    sLine = sLine & Space(2)
                                    '9	ADDRESSNUMBER	071-078	0	Fixed value	No
                                    sLine = sLine & Space(7) & "0"
                                    '10  LONGADDRESSNUMBER	079-098	11digits	Bank account number.  Normally 11 digits	Yes – ML01 input ID 14
                                    sLine = sLine & PadRight(oPayment.e_OrgNo, 20, " ")
                                    '11	TAXID	099-118	Alphanumeric	Leverandørnummer	Yes – ML01 input ID 3
                                    sLine = sLine & PadRight(oPayment.E_Account, 20, " ")
                                    '12	NAMEALPHA	119-158	Alphanumeric	Navn 1	Yes – ML01 input ID 8
                                    sLine = sLine & PadRight(oPayment.E_Name, 40, " ")
                                    '13	BUSINESSUNIT	159-170	<blank>	Fixed value	No
                                    sLine = sLine & Space(12)
                                    '14	INDUSTRYCLASSCODE	171-180	<blank>	Fixed value	No
                                    sLine = sLine & Space(10)
                                    '15	SEARCHTYPE	181-183	Q	Fixed value	No
                                    sLine = sLine & "Q  "
                                    If oPayment.E_AccountSuffix <> "" Then
                                        ' 27.11.2015 New field for P2000_NY
                                        '16	ML01	Clearingnummer 184-188
                                        sLine = sLine & PadRight(oPayment.E_AccountSuffix, 5, " ")
                                    Else
                                        '16	AR-AP-NETTINGIND	184-184	<blank>	Fixed value	No
                                        sLine = sLine & " "
                                        '17	ADDRESSTYPE3	185-185	<blank>	Fixed value	No
                                        sLine = sLine & " "
                                        '18	ADDRESSTYPE4	186-186	<blank>	Fixed value	No
                                        sLine = sLine & " "
                                        '19	ADDRESSTYPE5	187-187	<blank>	Fixed value	No
                                        sLine = sLine & " "
                                        '20	ADRESSTYPEPAYABLE	188-188	<blank>	Fixed value	No
                                        sLine = sLine & " "
                                    End If
                                    '21	ADDRESSTYPEPURCHASER	189-189	<blank>	Fixed value	No
                                    ' 27.11.2015 new field for P2000_NY - ML01	Type konto 189
                                    'sLine = sLine & " "
                                    sLine = sLine & PadRight(Left(oPayment.NOTI_NotificationType, 1), 1, " ")
                                    '22	ADDRESSTYPEEMPLOYEE	190-190	<blank>	Fixed value	No
                                    sLine = sLine & " "
                                    '23	ADDRESSNUMBER1	191-198	00000000	Fixed value	No
                                    sLine = sLine & StrDup(8, "0")
                                    '24	ADDRESSNUMBER2	199-206	00000000	Fixed value	No
                                    sLine = sLine & StrDup(8, "0")
                                    '25	ADDRESSNUMBER3	207-214	00000000	Fixed value	No
                                    sLine = sLine & StrDup(8, "0")
                                    '26	ADDRESSNUMBER4	215-222	00000000	Fixed value	No
                                    sLine = sLine & StrDup(8, "0")
                                    '27	ADDRESSNUMBER5	223-230	00000000	Fixed value	No
                                    sLine = sLine & StrDup(8, "0")
                                    '28	FACTOR-SPESPAYEE	231-238	00000000	Fixed value	No
                                    sLine = sLine & StrDup(8, "0")
                                    '29	NAMEMAILING	239-278	<blank>	Fixed value	No
                                    sLine = sLine & Space(40)
                                    '30	NAMEMAILING-SECONDARY	279-318	<blank>	Fixed value	No
                                    sLine = sLine & Space(40)
                                    '31	ADDRESSLINE1	319-358	Alphanumeric	Navn 2	Yes – ML01 input ID 9
                                    If oPayment.Unique_ERPID = "22" Then  ' P2000
                                        If Not EmptyString(oPayment.E_Adr1) Then
                                            sLine = sLine & PadRight(oPayment.E_Adr1, 40, " ")
                                        Else
                                            sLine = sLine & PadRight(oPayment.E_Adr2, 40, " ")
                                        End If
                                    Else
                                        sLine = sLine & PadRight(oPayment.E_Adr1, 40, " ")
                                    End If
                                    '32	ADDRESSLINE2	359-398	Alphanumeric	ADDR1	Yes – ML01 input ID 10
                                    If oPayment.Unique_ERPID = "22" Then  ' P2000
                                        If Not EmptyString(oPayment.E_Adr1) Then
                                            sLine = sLine & PadRight(oPayment.E_Adr2, 40, " ")
                                        Else
                                            sLine = sLine & Space(40)
                                        End If
                                    Else
                                        sLine = sLine & PadRight(oPayment.E_Adr2, 40, " ")
                                    End If
                                    '33	ADDRESSLINE3	399-438	Alphanumeric	ADDR2	Yes – ML01 input ID 11
                                    sLine = sLine & PadRight(oPayment.E_Adr3, 40, " ")
                                    '34	ADDRESSLINE4	439-478	<blank>	Fixed value	No
                                    sLine = sLine & Space(40)
                                    '35	POSTALCODE	479-490	4 digits	POSTNR	Yes – ML01 input ID 12
                                    sLine = sLine & PadRight(oPayment.E_Zip, 12, " ")
                                    '36	CITY	491-515	Alphanumeric	POSTSTED	Yes – ML01 input ID 13
                                    sLine = sLine & PadRight(oPayment.E_City, 25, " ")

                                    '37	COUNTRY	516-518	<blank>	Fixed value	No
                                    sLine = sLine & Space(3)
                                    '38	STATE	519-521	<blank>	Fixed value	No
                                    sLine = sLine & Space(3)
                                    '39	COUNTY	522-546	<blank>	Fixed value	No
                                    sLine = sLine & Space(25)
                                    '40	COMPANY	547-551	<blank>	Fixed value	No
                                    sLine = sLine & Space(5)
                                    '41	TRADINGPARTNERID	552-566	<blank>	Fixed value	No
                                    sLine = sLine & Space(15)
                                    '42	SENDSTATEMENTTOCP	567-567	<blank>	Fixed value	No
                                    sLine = sLine & Space(1)
                                    '43	SENDINVOICETOCP	568-568	<blank>	Fixed value	No
                                    sLine = sLine & Space(1)
                                    '44	CURRNECYCODEFROM	569-571	<blank>	Fixed value	No
                                    sLine = sLine & Space(3)
                                    '45	CURRENCYCODE-AP	572-574	<blank>	Fixed value	No
                                    sLine = sLine & Space(3)
                                    '46	USERID	575-584	05ML01	Fixed value	No
                                    If oPayment.Unique_ERPID = "22" Then  ' P2000
                                        sLine = sLine & "22ML01" & Space(4)
                                    Else
                                        sLine = sLine & "05ML01" & Space(4)
                                    End If
                                    '47	PROGRAMID	585-594	<blank>	Fixed value	No
                                    sLine = sLine & Space(10)
                                    '48	DATEUPDATED	595-600	000000	Fixed value	No
                                    sLine = sLine & StrDup(6, "0")
                                    '49	TIMEUPDATED	601-606	000000	Fixed value	No
                                    sLine = sLine & StrDup(6, "0")

                                    oFile.WriteLine(sLine)

                                    oPayment.Exported = True ' Must set as exported to avoid errormsg.
                                End If

                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabelFile

        Catch ex As Exception

            'KI: Er dette korrekt, før endring til ny feilhåndtering så ble det ikke reist en ny feilmelding
            'Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_Filenet_Address" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

            WriteGjensidigeJDE_Filenet_Address = True

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGjensidigeJDE_Filenet_Address = True

    End Function
    Function WriteGjensidigeJDE_GL02b(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        ' 23.06.2020
        ' Exports a semiclon separated file on GL02B format for Gjensige JDE

        ';;24359;UI;1;;;20200525;17013;;;;;;;;;;;;;;;;;;;;;;;;18005909.00;;;;;;;;;;;;;;;;;;;;;;;;;;;;x
        ';;24359;UI;1;;;20200525;17013;;;;;;;;;;;;;;;;;;;;;;;;-15585567.00;;;;;;;;;;;;;;;;;;;;;;;;;;;;x
        ';;24359;UI;1;;;20200525;17101;;        ;;;;;;;;;;;;;;;;;;ikke oppgittikke opp;;;;-183993.00;;;;;;;;;;;;;;;;;;;;;;;;;;;;x

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sText As String
        Dim sLine As String
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean

        Dim sOldCurrency As String = ""
        Dim iLineNumber As Integer = 0
        Dim iTransactionNumber As Integer = 0
        ' 23.11.2021 - Introduced a new part of OutputID5 - to make "unique enough"
        '              Use 3 digits in the midle derivated from time, to be used for the complete file
        Dim sTimeConstant As String = ""

        Dim iMaxTransactionNumber As Long = 0
        Dim sOldClientID As String = ""
        Dim sBatchNumberLastPart As String = ""
        Dim bDomesticCurrency As Boolean = False
        Dim sTmp As String = ""
        Dim aTransactionArray As String()
        Dim sTransactionString As String
        Dim i As Integer
        Dim bFound As Boolean
        Dim bClientFound As Boolean
        Dim iCurrentArrayElement As Long
        Dim sOldTransactionString As String

        Try

            ' 23.11.2021 - Introduced a new part of OutputID5 - to make "unique enough"
            '              Use 3 digits in the midle derivated from time, to be used for the complete file
            sTimeConstant = Right(Hour(Now), 1) & Right(Minute(Now), 1) & Right(Second(Now), 1)

            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                If iMaxTransactionNumber = 0 Then
                    If oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_F2100_SE_DK_GL02b Then
                        If oBabel.Cargo.Temp = "47" Then
                            iMaxTransactionNumber = 47210000  ' For F2100 SE, always start at 50210001
                        Else
                            iMaxTransactionNumber = 50210000  ' For F2100 DK, always start at 50210001
                        End If
                    End If
                End If
                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                    '02.11.2020 - some imported records are marked for deletion
                                    If oPayment.Exported Then
                                        bExportoPayment = False
                                    End If
                                End If

                                If bExportoPayment Then


                                    For Each oInvoice In oPayment.Invoices

                                        sTransactionString = oPayment.I_Client & ";" & oPayment.DATE_Payment & ";" & oPayment.MON_InvoiceCurrency & ";" & Trim(oPayment.Invoices(1).InvoiceNo)
                                        If sTransactionString <> sOldTransactionString Then
                                            bFound = False
                                            If Not aTransactionArray Is Nothing Then
                                                For i = 0 To aTransactionArray.GetLength(0) - 1
                                                    If xDelim(aTransactionArray(i), ";", 1) & ";" & xDelim(aTransactionArray(i), ";", 2) & ";" & xDelim(aTransactionArray(i), ";", 3) & ";" & Trim(xDelim(aTransactionArray(i), ";", 4)) = sTransactionString Then
                                                        iTransactionNumber = Val(xDelim(aTransactionArray(i), ";", 5))
                                                        iLineNumber = Val(xDelim(aTransactionArray(i), ";", 6))
                                                        iCurrentArrayElement = i
                                                        bFound = True
                                                        Exit For
                                                    End If
                                                Next
                                            End If

                                            If Not bFound Then
                                                ' add to array
                                                If aTransactionArray Is Nothing Then
                                                    aTransactionArray.Resize(aTransactionArray, 1)
                                                Else
                                                    aTransactionArray.Resize(aTransactionArray, aTransactionArray.GetLength(0) + 1)
                                                End If


                                                ' add one to unique transactionnumber
                                                'iTransactionNumber = iTransactionNumber + 1
                                                iTransactionNumber = iMaxTransactionNumber + 1
                                                iMaxTransactionNumber = iTransactionNumber

                                                aTransactionArray(aTransactionArray.GetLength(0) - 1) = sTransactionString & ";" & Str(iTransactionNumber) & ";0"
                                                iCurrentArrayElement = aTransactionArray.GetLength(0) - 1

                                                ' Linenumber in ID4 must start on 1 for each transactionnumber
                                                iLineNumber = 0
                                                sOldCurrency = oPayment.MON_InvoiceCurrency
                                                sOldClientID = oPayment.I_Client
                                            Else
                                                bFound = True
                                            End If

                                        End If
                                        sLine = ";;"   ' 1 and 2 not in use

                                        '3	Entity Code	Company Code	Transformation 1
                                        ' 14.10.2020
                                        ' For F2100 SE and DK; fixed values here
                                        If oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_F2100_SE_DK_GL02b Then
                                            If oPayment.I_Client = "47" Then
                                                sLine = sLine & "24718" & ";"
                                            Else
                                                sLine = sLine & "24721" & ";"
                                            End If
                                        Else
                                            sLine = sLine & oPayment.I_Client & ";"
                                        End If

                                        ' 4 Doc.type
                                        If oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_Aditro Then
                                            ' 22.12.2020 - changed this one to test fagsystem in export
                                            'If oPayment.Unique_ERPID = "26" Then
                                            '    ' Aditro pensjon
                                            '    sLine = sLine & "UR"
                                            'Else
                                            '    sLine = sLine & "UA"
                                            'End If
                                            sLine = sLine & oPayment.ExtraD2
                                        ElseIf oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_IDIT Then
                                            sLine = sLine & "UL"
                                        ElseIf oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_INPAS Then
                                            sLine = sLine & "US"
                                        ElseIf oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_NICE Then
                                            sLine = sLine & "UT"
                                        Else
                                            ' 13.10.2020 - added several doc.types         
                                            Select Case oPayment.Unique_ERPID
                                                Case "12"
                                                    sLine = sLine & "UX"
                                                Case "20", "24", "32"
                                                    sLine = sLine & "UI"
                                                Case "21"
                                                    sLine = sLine & "UY"
                                                Case "22"
                                                    sLine = sLine & "UO"
                                                Case "23"
                                                    sLine = sLine & "UJ"
                                                Case "42"   ' added 28.08.2014 (GINS?)
                                                    sLine = sLine & "U7"
                                                Case "43"   ' added 28.08.2014 (GINS?)
                                                    sLine = sLine & "U8"
                                                Case "70", "72"
                                                    sLine = sLine & "UK"
                                                Case "74"
                                                    sLine = sLine & "UF"
                                                Case "77"
                                                    sLine = sLine & "U6"
                                                Case "84"
                                                    sLine = sLine & "UE"
                                                Case "85"
                                                    sLine = sLine & "UW"
                                                Case "88"
                                                    sLine = sLine & "UH"
                                                Case "97"
                                                    sLine = sLine & "UU"
                                                Case Else
                                                    sLine = sLine & "  "
                                            End Select
                                        End If
                                        sLine = sLine & ";"

                                        '5	Voucher No (Imp)	Temporary voucher number	Running number for each different input company (ID3) or each different GL date (ID8), or each different currency (ID 31) in the output file starting at 1, next is 2 etc (Similar to output format S12 GL, ID 2 Transaction Number)
                                        sOldTransactionString = sTransactionString
                                        ' 23.11.2021 - Introduced a new part of OutputID5 - to make "unique enough"
                                        '              Use 3 digits in the midle derivated from time, to be used for the complete file
                                        ' Gjelder alle sLine = sLine & under
                                        If oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_F2100_SE_DK_GL02b Then
                                            'sLine = sLine & PadLeft(iTransactionNumber.ToString, 8, "0") & ";"
                                            ' 22.12.2021 - changed line below to take last 5 from transactionnumber
                                            sLine = sLine & sTimeConstant & PadLeft(Right(iTransactionNumber.ToString, 5), 5, "0") & ";"
                                        ElseIf oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_Aditro Then
                                            'If oPayment.Unique_ERPID = "26" Then
                                            ' 30.12.2020 Endret test mellom Aditro lønn og Aditro Pensjon
                                            If oPayment.ExtraD2 = "UR" Then
                                                ' Aditro pensjon
                                                'sLine = sLine & "44" & PadLeft(iTransactionNumber.ToString, 5, "0") & ";"
                                                ' 22.12.2021 - changed line below to take last 3 from transactionnumber
                                                sLine = sLine & "44" & sTimeConstant & PadLeft(Right(iTransactionNumber.ToString, 3), 3, "0") & ";"

                                            Else
                                                ' Aditro lønn
                                                'sLine = sLine & "40" & PadLeft(iTransactionNumber.ToString, 5, "0") & ";"
                                                ' 22.12.2021 - changed line below to take last 3 from transactionnumber
                                                sLine = sLine & "40" & sTimeConstant & PadLeft(Right(iTransactionNumber.ToString, 3), 3, "0") & ";"
                                            End If
                                            'sLine = sLine & "4" & PadLeft(iTransactionNumber.ToString, 6, "0") & ";"
                                        ElseIf oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_IDIT Then
                                            'sLine = sLine & "11" & PadLeft(iTransactionNumber.ToString, 5, "0") & ";"
                                            ' 22.12.2021 - changed line below to take last 3 from transactionnumber
                                            sLine = sLine & "11" & sTimeConstant & PadLeft(Right(iTransactionNumber.ToString, 3), 3, "0") & ";"
                                        ElseIf oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_INPAS Or oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_NICE Then
                                            sLine = sLine & "41" & sTimeConstant & PadLeft(Right(iTransactionNumber.ToString, 3), 3, "0") & ";"
                                        Else
                                            '05.10.2020 - Added Fagsystem in front of voucherNo, and padded voucherno
                                            'sLine = sLine & oPayment.Unique_ERPID & PadLeft(iTransactionNumber.ToString, 6, "0") & ";"
                                            ' 22.12.2021 - changed line below to take last 3 from transactionnumber
                                            sLine = sLine & oPayment.Unique_ERPID & sTimeConstant & PadLeft(Right(iTransactionNumber.ToString, 3), 3, "0") & ";"
                                        End If
                                        '6/7 not in use
                                        sLine = sLine & ";;"
                                        '8	Voucher Date (imp)	YYYYMMDD	Transformation 2
                                        sLine = sLine & oPayment.DATE_Payment & ";"
                                        '9	Account		Transformation 5
                                        sLine = sLine & oPayment.E_Account & ";"
                                        '10 Account Ledger (imp)	Reskontro eller Eiendelsnummer	Transformation 6	Yes – Input ID 11	Yes
                                        sLine = sLine & oPayment.E_AccountPrefix & ";"
                                        '11	Posting 1	Kostnadssted
                                        sLine = sLine & oPayment.ExtraD1 & ";"
                                        '12-24 Not un use
                                        sLine = sLine & StrDup(13, ";")
                                        ' 25	XIdentifier (KID)			Yes – Input ID 14	No
                                        sLine = sLine & Trim(oPayment.REF_EndToEnd) & ";"
                                        ' 26-28 Not in use
                                        sLine = sLine & StrDup(3, ";")
                                        '29	Text	Bilagslinjetekst
                                        sLine = sLine & Trim(oPayment.REF_Bank1) & ";"
                                        '30 not in use
                                        sLine = sLine & ";"
                                        '31	Currency		Normally not in use in input file	Yes – Input ID 16	No
                                        sLine = sLine & oPayment.MON_InvoiceCurrency & ";"
                                        If oPayment.PayType = "I" Then
                                            '32	Curr.Amount	Beløp i valuta	If currency <> (blank or company currency) put amount in this field.
                                            'Divide input amount by 100 and use . (point) as decimal separator.
                                            'If ID 10 = K, then the amount should have a minus sign in front -
                                            sText = oPayment.MON_InvoiceAmount.ToString
                                            ' 13.10.2020 - ta høyde for beløp under 1 kr (under 100 i sText)
                                            If Len(sText) < 4 Then
                                                If sText.IndexOf("-") > -1 Then
                                                    ' vi har med et minustegn
                                                    sText = "-" & PadLeft(Mid(sText, 2), 3, "0")
                                                Else
                                                    sText = PadLeft(sText, 3, "0")
                                                End If
                                            End If
                                            '13.07.2021 If 2 last digits = 00, then skip them
                                            If Right(sText, 2) = "00" Then
                                                sLine = sLine & Left(sText, Len(sText) - 2)
                                            Else
                                                sLine = sLine & Left(sText, Len(sText) - 2) & "." & Right(sText, 2)
                                            End If
                                        End If
                                        sLine = sLine & ";"
                                        If oPayment.PayType = "D" Or oPayment.PayType = "F2100" Then
                                            '33	Amount	Beløp i selskapsvaluta	If currency = (blank or company currency) put amount in this field.
                                            'Divide input amount by 100 and use . (point) as decimal separator.
                                            'If ID 10 = K, then the amount should have a minus sign in front -
                                            sText = oPayment.MON_InvoiceAmount.ToString
                                            ' 13.10.2020 - ta høyde for beløp under 1 kr (under 100 i sText)
                                            If Len(sText) < 4 Then
                                                If sText.IndexOf("-") > -1 Then
                                                    ' vi har med et minustegn
                                                    sText = "-" & PadLeft(Mid(sText, 2), 3, "0")
                                                Else
                                                    sText = PadLeft(sText, 3, "0")
                                                End If
                                            End If
                                            '13.07.2021 If 2 last digits = 00, then skip them
                                            If Right(sText, 2) = "00" Then
                                                sLine = sLine & Left(sText, Len(sText) - 2)
                                            Else
                                                sLine = sLine & Left(sText, Len(sText) - 2) & "." & Right(sText, 2)
                                            End If
                                        End If
                                        sLine = sLine & ";"
                                        '34-56 Not un use
                                        sLine = sLine & StrDup(23, ";")
                                        '57	Posting 3	GL-objekt 3 Underkonto		TBD
                                        sLine = sLine & ";"
                                        '58-60 Not in use
                                        sLine = sLine & ";;;"
                                        '61 EOL
                                        sLine = sLine & "x"

                                        oFile.WriteLine(sLine)

                                        oPayment.Exported = True ' Must set as exported to avoid errormsg.
                                    Next oInvoice
                                End If
                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel


            
        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_GL02b" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGjensidigeJDE_GL02b = True

    End Function
    Function WriteGjensidigeJDE_GL02_SYS(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String) As Boolean
        ' 28.07.2020
        ' Exports a semiclon separated file on XLedger GL02-SYS format for Gjensige JDE

        ';;24359;U1;1;;;20200514;27151;;;;;;;;;;;;;;;2046486;;20200514;102248;010224801017801;20200613;;;;;;NOK;;-6992.5;;;;;;;;;;;;;;;;;;;;;;;;;;;;https://gjensidige.p2p.basware.com//api/gjensidige/InvoiceExternal?docId=70luc8veh6eljvuwx7cgwc02p7jc2c3b;;;;x
        ';;24359;U1;1;;;20200514;55381;;10-27530;;;;;;;;;;;;;;;;;test-avst1;;;;;mnd selger;1;;;3496.72;;;;;;;;;;;;;;;;;;;;;;;;;;;;https://gjensidige.p2p.basware.com//api/gjensidige/InvoiceExternal?docId=70luc8veh6eljvuwx7cgwc02p7jc2c3b;;;;x
        ';;24359;U1;1;;;20200514;55381;;10-27550;;;;;;;;;;;;;;;;;test-avst2;;;;;mnd selger;2;;;3495.78;;;;;;;;;;;;;;;;;;;;;;;;;;;;https://gjensidige.p2p.basware.com//api/gjensidige/InvoiceExternal?docId=70luc8veh6eljvuwx7cgwc02p7jc2c3b;;;;x


        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sText As String
        Dim sLine As String
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean

        Dim sOldCurrency As String = ""
        Dim iLineNumber As Integer = 0
        Dim iTransactionNumber As Integer = 0
        Dim iMaxTransactionNumber As Integer = 0
        Dim sOldClientID As String = ""
        Dim sBatchNumberLastPart As String = ""
        Dim bDomesticCurrency As Boolean = False
        Dim sTmp As String = ""
        Dim aTransactionArray As String()
        Dim sTransactionString As String
        Dim i As Integer
        Dim bFound As Boolean
        Dim bClientFound As Boolean
        Dim iCurrentArrayElement As Long
        Dim sOldTransactionString As String
        Dim sCompanyCurrency As String = ""   ' Base currency for company

        Try

            ' create an outputfile

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects to
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If
                                ' 28.10.2020
                                ' Noen records utelukkes, ved at de settes til Exported = True
                                If oPayment.Exported Then
                                    bExportoPayment = False
                                End If

                                If bExportoPayment Then

                                    ' 21.09.2020 do not export AP records for FileNet imports
                                    If oPayment.PayType <> "AP" Then
                                        For Each oInvoice In oPayment.Invoices

                                            sLine = ";;"   ' 1 and 2 not in use

                                            '3	Entity Code	Company Code	1-n	Transformation 1	Yes – Input ID 1	Yes
                                            sLine = sLine & oPayment.I_Client & ";"

                                            If oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_FileNet_XLedger Then
                                                ' LE01 record from FileNet
                                                '4	Voucher Type	Fixed: UD	1-2			No
                                                sLine = sLine & "UD;"
                                            ElseIf oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_P2000_XLedger Then
                                                ' LE01 record from P2000_XLedger
                                                '4	Voucher Type	Fixed: OU	1-2			No
                                                sLine = sLine & "UO;"
                                            Else
                                                '4	Voucher Type	Fixed UI
                                                sLine = sLine & "U1;"
                                            End If
                                            '5	Voucher No (Imp)	Temporary voucher number	Running number for each different input company (ID3) or each different GL date (ID8), or each different currency (ID 31) in the output file starting at 1, next is 2 etc (Similar to output format S12 GL, ID 2 Transaction Number)
                                            sLine = sLine & oPayment.VoucherNo & ";"
                                            '6/7 not in use
                                            sLine = sLine & ";;"
                                            '8	Voucher Date (imp)	YYYYMMDD	Transformation 2
                                            sLine = sLine & oPayment.DATE_Payment & ";"
                                            '9	Account		Transformation 5
                                            sLine = sLine & oPayment.E_Account & ";"
                                            '10	Account Ledger (imp)	Underkonto eller Eiendelsnummer			No
                                            ' 16.12.2020 - For Basware Alusta to GL02SYS
                                            '              Hvis GL record og felt 28 er ifylt, legg inn samme verdi som i felt 9 
                                            If oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_Basware_GL02_XML And oPayment.PayType = "GL" And EmptyString(oPayment.REF_Own) = False Then
                                                sLine = sLine & oPayment.E_Account & ";"
                                            Else
                                                sLine = sLine & ";"
                                            End If

                                            '11	Posting 1	Kostnadssted
                                            sLine = sLine & oPayment.E_AccountPrefix & ";"
                                            '12	Posting 2	Prosjekt			No	
                                            sLine = sLine & oPayment.E_AccountSuffix & ";"
                                            '13-19 Not un use
                                            'sLine = sLine & StrDup(11, ";")
                                            '24.03.2021 - added use of field 20
                                            sLine = sLine & StrDup(7, ";")  ' 13-19
                                            ' Field 20 PayMethod
                                            If oPayment.PayType = "SU" And (oPayment.I_Client = "25116" Or oPayment.I_Client = "24713" Or oPayment.I_Client = "24718" Or oPayment.I_Client = "24721" Or oPayment.I_Client = "25117" Or oPayment.I_Client = "28983") Then
                                                sLine = sLine & "DR3;"
                                            Else
                                                sLine = sLine & ";"
                                            End If
                                            sLine = sLine & StrDup(3, ";")  ' 21-23

                                            If oPayment.PayType = "SU_FileNet" Or oPayment.PayType = "SU_P2000" Or oPayment.PayType = "GL" Then
                                                '24	Subledger No	Leverandør-nummer	1	Transformation 1	LE01	18	Add F in front of the number
                                                sLine = sLine & oInvoice.SupplierNo & ";"
                                            ElseIf oPayment.PayType = "SU" Then
                                                '24	Subledger No	Leverandørnummer	1	<SUPPLIER CODE="xxxxxxx">	Yes – Input ID 10	No
                                                sLine = sLine & oPayment.E_Name & ";"
                                            Else
                                                sLine = sLine & ";"
                                            End If
                                            '25	Subledger Name	Ny leverandør			No	
                                            ' not in use
                                            sLine = sLine & ";"
                                            ' 26	InvoiceDate	Fakturadato	1	<INVOICEDATE> Transformation 2	Yes – Input ID 4	No
                                            If oPayment.PayType = "SU" Or oPayment.PayType = "SU_FileNet" Or oPayment.PayType = "SU_P2000" Then
                                                sLine = sLine & oPayment.ERA_Date & ";"
                                            Else
                                                sLine = sLine & ";"
                                            End If
                                            '27	Invoice No	Fakturanummer	1	<INVOICENUM> Transformation 2	Yes – Input ID 6	No
                                            'If oPayment.PayType = "SU" Or oPayment.PayType = "SU_FileNet" Then
                                            ' changed 15.12.2020 - changed if above, split into two
                                            If oPayment.PayType = "SU" Or oPayment.PayType = "SU_P2000" Then
                                                sLine = sLine & oInvoice.InvoiceNo & ";"
                                            ElseIf oPayment.PayType = "SU_FileNet" Then
                                                sLine = sLine & oPayment.REF_EndToEnd & ";"
                                            Else
                                                sLine = sLine & ";"
                                            End If
                                            '28	XIdentifier (KID)		1 2-n	<REFERENCENUMBER> <AVSTEMMING>	Yes – Input ID 9 Yes – Input ID 18	No
                                            If oPayment.PayType = "SU" Then
                                                sLine = sLine & oPayment.REF_EndToEnd & ";"
                                            ElseIf oPayment.PayType = "SU_FileNet" Or oPayment.PayType = "SU_P2000" Then
                                                sLine = sLine & oInvoice.Unique_Id & ";"
                                            Else
                                                sLine = sLine & oPayment.REF_Own & ";"
                                            End If

                                            '29	Due Date (imp)		1	<DUEDATE> Transformation 2	Yes – Input ID 3	No
                                            If oPayment.PayType = "SU" Or oPayment.PayType = "SU_FileNet" Or oPayment.PayType = "SU_P2000" Then
                                                sLine = sLine & oPayment.DATE_Value & ";"
                                            Else
                                                sLine = sLine & ";"
                                            End If
                                            '30/31/32 not in use
                                            sLine = sLine & ";;;"
                                            '33	Text	Bilagslinjetekst	2-n	<COMMENT>	Yes – Input ID 19	No
                                            If oPayment.PayType = "SU" Or oPayment.PayType = "SU_FileNet" Or oPayment.PayType = "SU_P2000" Then
                                                sLine = sLine & ";"
                                            Else
                                                sLine = sLine & oPayment.REF_EndToEnd & ";"
                                            End If
                                            '34	TaxRule	Mva kode	2-n	<TaxCode>	Yes – Input ID 22	No
                                            sLine = sLine & oPayment.ExtraD1 & ";"
                                            '35	Currency	Valuta	1	<CURRENCY CODE="xxx">	Yes – Input ID 2	No
                                            'If oPayment.PayType = "SU" Then
                                            sLine = sLine & oPayment.MON_InvoiceCurrency & ";"
                                            'Else
                                            'sLine = sLine & ";"
                                            'End If
                                            If oPayment.MON_InvoiceCurrency <> oPayment.MON_AccountCurrency Then
                                                '36	Curr.Amount	Beløp i valuta	If currency <> (blank or company currency) put amount in this field.
                                                'Divide input amount by 100 and use . (point) as decimal separator.
                                                'If ID 10 = K, then the amount should have a minus sign in front -
                                                If oPayment.PayType = "SU" Or oPayment.PayType = "SU_FileNet" Or oPayment.PayType = "SU_P2000" Then
                                                    sText = (oPayment.MON_InvoiceAmount * -1).ToString
                                                Else
                                                    sText = (oPayment.MON_InvoiceAmount).ToString
                                                End If
                                                If sText = "0" Then
                                                    sLine = sLine & "0.00;"
                                                Else
                                                    sLine = sLine & Left(sText, Len(sText) - 2) & "." & Right(sText, 2) & ";"
                                                End If
                                                sLine = sLine & ";"
                                            Else
                                                '37	Amount	Beløp i selskapsvaluta	If currency = (blank or company currency) put amount in this field.
                                                'Divide input amount by 100 and use . (point) as decimal separator.
                                                'If ID 10 = K, then the amount should have a minus sign in front -
                                                If oPayment.PayType = "SU" Or oPayment.PayType = "SU_FileNet" Or oPayment.PayType = "SU_P2000" Then
                                                    sText = (oPayment.MON_InvoiceAmount * -1).ToString
                                                    ' 23.03.2021 - better presentation of amount
                                                    sText = ConvertFromAmountToString(oPayment.MON_InvoiceAmount * -1)
                                                Else
                                                    sText = (oPayment.MON_InvoiceAmount).ToString
                                                    ' 23.03.2021 - better presentation of amount
                                                    sText = ConvertFromAmountToString(oPayment.MON_InvoiceAmount)
                                                End If
                                                
                                                sLine = sLine & ";"
                                                If sText = "0" Then
                                                    sLine = sLine & "0.00;"
                                                Else
                                                    sLine = sLine & Left(sText, Len(sText) - 2) & "." & Right(sText, 2) & ";"
                                                End If
                                            End If
                                            '38-62 Not un use
                                            sLine = sLine & StrDup(25, ";")
                                            '63	Archive No	not in use
                                            sLine = sLine & ";"
                                            '64 Not in use
                                            sLine = sLine & ";"
                                            '65	External Url	Link til Faktura	1-n	<INVOICEEXTERNALURL>	No
                                            sLine = sLine & oPayment.REF_Bank1 & ";"
                                            '66-68 Not in use
                                            sLine = sLine & ";;;"
                                            '69 EOL
                                            sLine = sLine & "x"

                                            oFile.WriteLine(sLine)

                                            oPayment.Exported = True ' Must set as exported to avoid errormsg.
                                        Next oInvoice
                                    End If  'If oPayment.PayType <> "AP then" Then
                                End If  ' bExportoPayment
                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_GL02_SYS" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGjensidigeJDE_GL02_SYS = True

    End Function
    Function WriteGjensidigeJDE_AP02(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef bWrittenBefore As Boolean) As Boolean
        ' 21.09.2020
        ' Exports a semiclon separated file on XLedger AP02 format

        '_ID;Code;Description;Subledger Id;Group;Company No;Company;Company Category;Phone;E-Mail;HTML E-mail;Language;Culture Info;AP Tax Reporting;Credit Limit;Pay Terms;Payment Delays;Pay Method;Pay Doc Code;Pay Doc Text;Payment Charge;Bank (SWIFT);Bank Account (Konto eller IBAN);Pay Currency;Currency;Currency;Separate Payment;Auto Pay;Ext. Account No;Invoice No;Invoice No;Ext. Identifier;Ext. Identifier;Account Name;Account Name;SL Payment Receiver;Ext. Order Ref;Ext. Order Ref;Tax No;Ext. GL;Ext. GL;Sales Contact;Our Ref;Your Reference;Contract;Invoice Header;Invoice Footer;Shipping terms;Shipping Method;Service Type;Account;Account;SL Account;Sl Account;GL Object Value 1;GL Object Value 2;XGL;Tax Rule;Tax Rule;Text;Text;Notes;Date From;Date To;Street Address;Zip Code;City;State;Country;Street Address 2;Zip Code 2;City 2;State 2;Country 2;Payment Notification;Dummy 1;Dummy 2;Dummy 3;Dummy 4;Dummy 5;End of Line
        ';F79733;HOSSEIN KHORSHIDI - Vital Legekontor;;F;;;;;;;;;;;;;DR;;;;;15062822284;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Grønlandsleiret 31;0190;OSLO;;NO;;;;;;;;;;;;x
        ';F95758;GUNNAR TVEITEN - Best Helse AS;;F;;;;;;;;;;;;;DR;;;;;37052384193;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Kongsveien 104;1177;Oslo;;NO;;;;;;;;;;;;x
        ';F999;SEYED OMID HOSSEINI - Seaside Klinikken;;F;;;;;;;;;;;;;DR;;;;;60170516802;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Storgata 38;1440;DRØBAK;;NO;;;;;;;;;;;;x


        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        Dim sText As String
        Dim sLine As String
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean

        Dim sOldCurrency As String = ""
        Dim iLineNumber As Integer = 0
        Dim iTransactionNumber As Integer = 0
        Dim iMaxTransactionNumber As Integer = 0
        Dim sOldClientID As String = ""
        Dim sBatchNumberLastPart As String = ""
        Dim bDomesticCurrency As Boolean = False
        Dim sTmp As String = ""
        Dim aTransactionArray As String()
        Dim sTransactionString As String
        Dim i As Integer
        Dim bFound As Boolean
        Dim bClientFound As Boolean
        Dim iCurrentArrayElement As Long
        Dim sOldTransactionString As String
        Dim sCompanyCurrency As String = ""   ' Base currency for company

        Try

            ' create an outputfile

            oFs = New Scripting.FileSystemObject
            ' This one is exported together with GL02-SYS file (to XLedger). We use only one "line" for setup of a common filename.
            ' Filename in profile can be set up as "GL02-SYS_Filenet.csv"
            ' Remove the "GL02-SYS"-part
            sTmp = System.IO.Path.GetFileName(sFilenameOut)
            sTmp = Replace(sTmp, "GL02-SYS", "")
            ' manipulate filename by adding AP02 in front
            sTmp = "AP02_" & sTmp
            sFilenameOut = Replace(sFilenameOut, System.IO.Path.GetFileName(sFilenameOut), sTmp)
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, Scripting.Tristate.TristateFalse)   'Scripting.Tristate.TristateTrue)

            ' 23.11.2020 - has file been written before  - if so skip header
            If Not bWrittenBefore Then
                ' export a header record
                sLine = "_ID;Code;Description;Subledger Id;Group;Company No;Company;Company Category;Phone;E-Mail;HTML E-mail;Language;Culture Info;AP Tax Reporting;Credit Limit;Pay Terms;Payment Delays;Pay Method;Pay Doc Code;Pay Doc Text;Payment Charge;Bank (SWIFT);Bank Account (Konto eller IBAN);Pay Currency;Currency;Currency;Separate Payment;Auto Pay;Ext. Account No;Invoice No;Invoice No;Ext. Identifier;Ext. Identifier;Account Name;Account Name;SL Payment Receiver;Ext. Order Ref;Ext. Order Ref;Tax No;Ext. GL;Ext. GL;Sales Contact;Our Ref;Your Reference;Contract;Invoice Header;Invoice Footer;Shipping terms;Shipping Method;Service Type;Account;Account;SL Account;Sl Account;GL Object Value 1;GL Object Value 2;XGL;Tax Rule;Tax Rule;Text;Text;Notes;Date From;Date To;Street Address;Zip Code;City;State;Country;Street Address 2;Zip Code 2;City 2;State 2;Country 2;Payment Notification;Dummy 1;Dummy 2;Dummy 3;Dummy 4;Dummy 5;End of Line"
                oFile.WriteLine(sLine)
            End If

            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects that shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            If Not bMultiFiles Then
                                bExportoBabel = True
                            Else
                                If oPayment.I_Account = sI_Account Then
                                    If oPayment.REF_Own = sOwnRef Then
                                        bExportoBabel = True
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch

                If bExportoBabel Then

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.I_Account = sI_Account Then
                                        If oPayment.REF_Own = sOwnRef Then
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment

                        If bExportoBatch Then

                            For Each oPayment In oBatch.Payments

                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects to
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If oPayment.REF_Own = sOwnRef Then
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If

                                If bExportoPayment Then

                                    ' 21.09.2020  export only AP records for FileNet imports
                                    If oPayment.PayType = "AP" Then
                                        For Each oInvoice In oPayment.Invoices

                                            sLine = ";"   ' 1 not in use

                                            '2	Code	Supplier Number	Transformation 1	ML01	16	Add F in front of the number
                                            sLine = sLine & oInvoice.SupplierNo & ";"
                                            '3	Description	Supplier Name	Transformation 2	ML01	ID 8 and ID 9	Concatenate (ID8; “ – “;ID9)
                                            sLine = sLine & oPayment.E_Name & ";"
                                            sLine = sLine & ";"   ' 4 not in use
                                            If oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_FileNet_XLedger Then
                                                '5	Group	Fixed F		No	No
                                                sLine = sLine & "F" & ";"
                                            ElseIf oBabel.ImportFormat = BabelFiles.FileType.GjensidigeJDE_P2000_XLedger Then
                                                '5	Group	Fixed P		No	No
                                                sLine = sLine & "P" & ";"
                                            Else
                                                ' should not happen
                                                sLine = sLine & " " & ";"
                                            End If

                                            '6-17 not in use
                                            sLine = sLine & ";;;;;;;;;;;;"
                                            '18	Pay Method	Fixed DR	
                                            sLine = sLine & "DR" & ";"
                                            '19-22 Not in use
                                            sLine = sLine & ";;;;"
                                            '23	Bank Account			ML01	14
                                            sLine = sLine & oPayment.e_OrgNo & ";"
                                            '24-64 Not in use
                                            sLine = sLine & StrDup(41, ";")
                                            '65	Street Address	Adresse		ML01	10
                                            sLine = sLine & oPayment.E_Adr2 & ";"
                                            '66	Zip Code	Postnummer		ML01	12
                                            sLine = sLine & oPayment.E_Zip & ";"
                                            '67	City	Poststed		ML01	13
                                            sLine = sLine & oPayment.E_City & ";"
                                            '68 not in use
                                            sLine = sLine & ";"
                                            '69	Country	Fixed NO		No	No
                                            '17.08.2021 - addes Sweden, firma 47
                                            If oPayment.I_Client = "47" Or oPayment.I_Zip = "47" Then
                                                sLine = sLine & "SE" & ";"
                                            Else
                                                sLine = sLine & "NO" & ";"
                                            End If
                                            '70	Street Address 2	Adresse 2		ML01	11
                                            sLine = sLine & oPayment.E_Adr3 & ";"
                                            '71-80 Not in use
                                            sLine = sLine & StrDup(10, ";")
                                            '81 EOL
                                            sLine = sLine & "x"

                                            oFile.WriteLine(sLine)

                                            oPayment.Exported = True ' Must set as exported to avoid errormsg.
                                        Next oInvoice
                                    End If  'If oPayment.PayType <> "AP then" Then
                                End If  ' bExportoPayment
                            Next oPayment ' payment
                        End If
                    Next oBatch 'batch
                End If

            Next oBabel

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_AP02" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteGjensidigeJDE_AP02 = True

    End Function

    ' ================================================================================= '
    ' 5 different XML output formats for Gjensidige JDE
    ' ================================================================================= '
    Public Function WriteGjensidigeJDE_UAccounts(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef bVB_MultiFiles As Boolean, ByRef sFILE_Name As String) As Boolean
        ' Felt nr	Kontotekst.txt	UAccounts.xml	
        '1:     Kontonummer - hovedkonto	Text_2		
        '2:     Underkonto                  Text_1
        '3:     Kontotekst                  Text_3
        '4:     Selskapsnummer              Company

        Dim XMLDoc As System.Xml.XmlDocument
        Dim XMLDeclaration As System.Xml.XmlDeclaration
        Dim rootNode As System.Xml.XmlElement
        Dim oBabelFile As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment

        Dim apiNS As String
        Dim xsiNS As String


        Dim oFs As Scripting.FileSystemObject

        Dim sTmp As String
        Dim sMessage As String
        Dim bPaymentsExported As Boolean


        Try

            XMLDoc = New XmlDocument
            XMLDoc.PreserveWhitespace = True
            XMLDeclaration = XMLDoc.CreateXmlDeclaration("1.0", "UTF-8", "yes")
            rootNode = XMLDoc.CreateElement("DocumentElement")

            XMLDoc.InsertBefore(XMLDeclaration, XMLDoc.DocumentElement)
            XMLDoc.AppendChild(rootNode)


            'apiNS = "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02"
            xsiNS = "http://www.w3.org/2001/XMLSchema-instance"

            'rootNode.SetAttribute("xmlns", apiNS)
            rootNode.SetAttribute("xmlns:xsi", xsiNS)
            'rootNode.SetAttribute("schemaLocation", xsiNS, "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02 pain.001.001.02.xsd")

            Using writer As XmlWriter = rootNode.CreateNavigator.AppendChild

                writer.WriteString(vbNewLine)

                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            ' ikke eksporter hvis Text_1 er blank
                            If Not EmptyString(oPayment.E_Account) Then
                                writer.WriteStartElement("Item")
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Text_1")
                                writer.WriteString(oPayment.E_Account)
                                writer.WriteEndElement() 'Text1
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Text_2")
                                writer.WriteString(oPayment.I_Account)
                                writer.WriteEndElement() 'Text2
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Text_3")
                                writer.WriteString(oPayment.Text_I_Statement)
                                writer.WriteEndElement() 'Text3
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Company")
                                writer.WriteString(oPayment.I_Client)
                                writer.WriteEndElement() 'Company
                                writer.WriteString(vbNewLine)

                                writer.WriteEndElement()  'Item
                                writer.WriteString(vbNewLine)

                            End If
                            bPaymentsExported = True
                            oPayment.Exported = True
                        Next
                    Next
                Next

            End Using
            rootNode = Nothing

            XMLDoc.Save(sFILE_Name)

            XMLDoc = Nothing

            If Not bPaymentsExported Then
                oFs = New Scripting.FileSystemObject
                oFs.DeleteFile(sFILE_Name, True)
            Else
                oFs = New Scripting.FileSystemObject
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_UAccounts" & vbCrLf & ex.Message, ex, oPayment, "", sFILE_Name)

        Finally
            If Not rootNode Is Nothing Then
                rootNode = Nothing
            End If
            If Not XMLDoc Is Nothing Then
                XMLDoc = Nothing
            End If
            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        Return True

    End Function
    Public Function WriteGjensidigeJDE_Accounts(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef bVB_MultiFiles As Boolean, ByRef sFILE_Name As String) As Boolean
        ' Felt nr	Kontotekst.txt	Accounts.xml	
        '1:     Kontonummer - hovedkonto	Text_1
        '2:     Underkonto	                TaxCode
        '3:     Kontotekst	                Text_2
        '4:     Selskapsnummer              Company

        Dim XMLDoc As System.Xml.XmlDocument
        Dim XMLDeclaration As System.Xml.XmlDeclaration
        Dim rootNode As System.Xml.XmlElement
        Dim oBabelFile As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim l As Long = 0
        Dim apiNS As String
        Dim xsiNS As String


        Dim oFs As Scripting.FileSystemObject

        Dim sTmp As String
        Dim sMessage As String
        Dim bPaymentsExported As Boolean


        Try

            XMLDoc = New XmlDocument
            XMLDoc.PreserveWhitespace = True
            XMLDeclaration = XMLDoc.CreateXmlDeclaration("1.0", "UTF-8", "yes")
            rootNode = XMLDoc.CreateElement("DocumentElement")

            XMLDoc.InsertBefore(XMLDeclaration, XMLDoc.DocumentElement)
            XMLDoc.AppendChild(rootNode)


            'apiNS = "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02"
            xsiNS = "http://www.w3.org/2001/XMLSchema-instance"

            'rootNode.SetAttribute("xmlns", apiNS)
            rootNode.SetAttribute("xmlns:xsi", xsiNS)
            'rootNode.SetAttribute("schemaLocation", xsiNS, "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02 pain.001.001.02.xsd")

            Using writer As XmlWriter = rootNode.CreateNavigator.AppendChild

                writer.WriteString(vbNewLine)

                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        ' need to "preprocess" each batch
                        ' do not export if blank e_account and NEXT payment has same I_account and I_Client
                        For l = 1 To oBatch.Payments.Count - 1
                            If oBatch.Payments(l).I_Account = oBatch.Payments(l + 1).I_Account And oBatch.Payments(l).I_Client = oBatch.Payments(l + 1).I_Client And oBatch.Payments(l).E_Account = "" Then
                                oBatch.Payments(l).Exported = True
                            End If
                        Next
                        For Each oPayment In oBatch.Payments
                            If oPayment.Exported = False Then

                                writer.WriteStartElement("Item")
                                writer.WriteString(vbNewLine)


                                writer.WriteStartElement("Text_1")
                                writer.WriteString(oPayment.I_Account)
                                writer.WriteEndElement() 'Text1
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Text_2")
                                writer.WriteString(oPayment.Text_I_Statement)
                                writer.WriteEndElement() 'Text2
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Company")
                                writer.WriteString(oPayment.I_Client)
                                writer.WriteEndElement() 'Company
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("TaxCode")
                                writer.WriteString(oPayment.E_Account)
                                writer.WriteEndElement() 'TaxCode
                                writer.WriteString(vbNewLine)

                                writer.WriteEndElement()  'Item
                                writer.WriteString(vbNewLine)

                                bPaymentsExported = True
                                oPayment.Exported = True
                            End If
                        Next
                    Next
                Next

            End Using
            rootNode = Nothing

            XMLDoc.Save(sFILE_Name)

            XMLDoc = Nothing

            If Not bPaymentsExported Then
                oFs = New Scripting.FileSystemObject
                oFs.DeleteFile(sFILE_Name, True)
            Else
                oFs = New Scripting.FileSystemObject
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_Accounts" & vbCrLf & ex.Message, ex, oPayment, "", sFILE_Name)

        Finally
            If Not rootNode Is Nothing Then
                rootNode = Nothing
            End If
            If Not XMLDoc Is Nothing Then
                XMLDoc = Nothing
            End If
            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        Return True

    End Function
    Public Function WriteGjensidigeJDE_Costcenters(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef bVB_MultiFiles As Boolean, ByRef sFILE_Name As String) As Boolean
        ' Felt nr	Kontotekst.txt	Costcenters.xml	
        '1:     Ansvarskode	        Text_1
        '2:     Ansvarssted	        Text_2
        '3:     Selskapsnummer      Company

        Dim XMLDoc As System.Xml.XmlDocument
        Dim XMLDeclaration As System.Xml.XmlDeclaration
        Dim rootNode As System.Xml.XmlElement
        Dim oBabelFile As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment

        Dim apiNS As String
        Dim xsiNS As String


        Dim oFs As Scripting.FileSystemObject

        Dim sTmp As String
        Dim sMessage As String
        Dim bPaymentsExported As Boolean


        Try

            XMLDoc = New XmlDocument
            XMLDoc.PreserveWhitespace = True
            XMLDeclaration = XMLDoc.CreateXmlDeclaration("1.0", "UTF-8", "yes")
            rootNode = XMLDoc.CreateElement("DocumentElement")

            XMLDoc.InsertBefore(XMLDeclaration, XMLDoc.DocumentElement)
            XMLDoc.AppendChild(rootNode)


            'apiNS = "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02"
            xsiNS = "http://www.w3.org/2001/XMLSchema-instance"

            'rootNode.SetAttribute("xmlns", apiNS)
            rootNode.SetAttribute("xmlns:xsi", xsiNS)
            'rootNode.SetAttribute("schemaLocation", xsiNS, "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02 pain.001.001.02.xsd")

            Using writer As XmlWriter = rootNode.CreateNavigator.AppendChild

                writer.WriteString(vbNewLine)

                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments

                            writer.WriteStartElement("Item")
                            writer.WriteString(vbNewLine)

                            writer.WriteStartElement("Text_1")
                            writer.WriteString(oPayment.I_Account)
                            writer.WriteEndElement() 'Text1
                            writer.WriteString(vbNewLine)

                            writer.WriteStartElement("Text_2")
                            writer.WriteString(oPayment.E_Account)
                            writer.WriteEndElement() 'Text2
                            writer.WriteString(vbNewLine)

                            writer.WriteStartElement("Company")
                            writer.WriteString(oPayment.I_Client)
                            writer.WriteEndElement() 'Company
                            writer.WriteString(vbNewLine)

                            writer.WriteEndElement()  'Item
                            writer.WriteString(vbNewLine)

                            bPaymentsExported = True
                            oPayment.Exported = True
                        Next
                    Next
                Next

            End Using
            rootNode = Nothing

            XMLDoc.Save(sFILE_Name)

            XMLDoc = Nothing

            If Not bPaymentsExported Then
                oFs = New Scripting.FileSystemObject
                oFs.DeleteFile(sFILE_Name, True)
            Else
                oFs = New Scripting.FileSystemObject


            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_Costcenters" & vbCrLf & ex.Message, ex, oPayment, "", sFILE_Name)

        Finally
            If Not rootNode Is Nothing Then
                rootNode = Nothing
            End If
            If Not XMLDoc Is Nothing Then
                XMLDoc = Nothing
            End If
            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        Return True

    End Function
    Public Function WriteGjensidigeJDE_Suppliers(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef bVB_MultiFiles As Boolean, ByRef sFILE_Name As String) As Boolean
        ' Felt nr	Kontotekst.txt	Accounts.xml	
        '1:	Leverandørnummer	Code	
        '2: Leverandørnavn      Name
        '3: Bankkontonummer     Bban
        '4:	Leverandørnummer		    Kommer to ganger i dagens fil, se bort i fra denen
        '5:	Landskode	        Company	Her må det gjøres en konvertering. NO=101, SE=102, DK=103
        '6:	Organisasjonsnummer	<SupplierIdentifier>    value	
        '	Ikke i inputfil	Active	Aktiv lev = true, lev som skal slettes = false. I første omgang settes alle til true. Må senere få oppdatert inputfilen med infp om de som skal slettes og settes til false
        '   Ikke i inputfil	key under <SupplierIdentifier>	Fast verdi = PartyID

        Dim XMLDoc As System.Xml.XmlDocument
        Dim XMLDeclaration As System.Xml.XmlDeclaration
        Dim rootNode As System.Xml.XmlElement
        Dim oBabelFile As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim apiNS As String
        Dim xsiNS As String
        Dim dCounter As Double = 0


        Dim oFs As Scripting.FileSystemObject

        Dim sTmp As String
        Dim sMessage As String
        Dim bPaymentsExported As Boolean


        Try

            XMLDoc = New XmlDocument
            XMLDoc.PreserveWhitespace = True
            XMLDeclaration = XMLDoc.CreateXmlDeclaration("1.0", "UTF-8", "yes")
            rootNode = XMLDoc.CreateElement("DocumentElement")

            XMLDoc.InsertBefore(XMLDeclaration, XMLDoc.DocumentElement)
            XMLDoc.AppendChild(rootNode)


            'apiNS = "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02"
            xsiNS = "http://www.w3.org/2001/XMLSchema-instance"

            'rootNode.SetAttribute("xmlns", apiNS)
            rootNode.SetAttribute("xmlns:xsi", xsiNS)
            'rootNode.SetAttribute("schemaLocation", xsiNS, "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02 pain.001.001.02.xsd")

            Using writer As XmlWriter = rootNode.CreateNavigator.AppendChild

                writer.WriteString(vbNewLine)

                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            For Each oInvoice In oPayment.Invoices
                                writer.WriteStartElement("Supplier")
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Company")
                                writer.WriteString(oPayment.E_CountryCode)  '101, 102 103
                                writer.WriteEndElement() 'Company
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Code")
                                writer.WriteString(oPayment.E_Account)
                                writer.WriteEndElement() 'Code
                                writer.WriteString(vbNewLine)

                                ' 29.08.2018 - added next Tag;
                                writer.WriteStartElement("UILanguage")
                                If oPayment.E_CountryCode = "101" Then
                                    writer.WriteString("nb-NO")
                                ElseIf oPayment.E_CountryCode = "102" Then
                                    writer.WriteString("sv-SE")
                                ElseIf oPayment.E_CountryCode = "103" Then
                                    writer.WriteString("da-DK")
                                Else
                                    ' just in case
                                    writer.WriteString("nb-NO")
                                End If
                                writer.WriteEndElement() 'UILanguage
                                writer.WriteString(vbNewLine)

                                dCounter = dCounter + 1
                                writer.WriteStartElement("Name")
                                'writer.WriteString(Gjensidige_ValidateISO20022Characters(oPayment.E_Name))
                                writer.WriteString(oPayment.E_Name)
                                writer.WriteEndElement() 'Name
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Active")
                                'writer.WriteString("True")
                                ' 04.06.2018 - 
                                ' Added >Supplier><Active>
                                ' Input 7 = 0 -> Sett <Active>False
                                ' Input 7 = alle andre verdier -> Sett <Active>True
                                If oPayment.Priority Then
                                    writer.WriteString("True")
                                Else
                                    writer.WriteString("False")
                                End If
                                writer.WriteEndElement() 'Active
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Inherit")
                                writer.WriteString("True")
                                writer.WriteEndElement() 'Inherit
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("BankAccounts")
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("SupplierBankAccount")
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Bban")
                                writer.WriteString(oPayment.I_Account)
                                writer.WriteEndElement() 'Bban
                                writer.WriteString(vbNewLine)

                                writer.WriteEndElement()  '"SupplierBankAccount")
                                writer.WriteString(vbNewLine)

                                writer.WriteEndElement()  '"BankAccounts")
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("SupplierIdentifiers")
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("SupplierIdentifier")
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("key")
                                writer.WriteString("PartyID")
                                writer.WriteEndElement() 'key
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("value")

                                ' 02.09.2020
                                ' Special rules when importformat = XML Suppliers
                                If oBabelFile.ImportFormat = BabelFiles.FileType.GjensidigeJDE_XLedger_Suppliers_XML Then
                                    writer.WriteString(oPayment.e_OrgNo)  ' use the complete org.no.
                                Else
                                    ' as pre 02.09.2020
                                    ' 26.10.2018- For NO, skriv kun de 9 første siffer i OrgNo
                                    If oPayment.E_CountryCode = "101" Then
                                        writer.WriteString(Left(oPayment.e_OrgNo, 9))
                                    Else
                                        writer.WriteString(oPayment.e_OrgNo)
                                    End If
                                End If
                                writer.WriteEndElement() 'value
                                writer.WriteString(vbNewLine)

                                writer.WriteEndElement()  '"SupplierIdentifier")
                                writer.WriteString(vbNewLine)

                                writer.WriteEndElement()  '"SupplierIdentifiers")
                                writer.WriteString(vbNewLine)

                                writer.WriteEndElement()  'Supplier
                                writer.WriteString(vbNewLine)

                                bPaymentsExported = True
                                oPayment.Exported = True
                            Next
                        Next
                    Next
                Next

            End Using
            rootNode = Nothing

            XMLDoc.Save(sFILE_Name)

            XMLDoc = Nothing

            If Not bPaymentsExported Then
                oFs = New Scripting.FileSystemObject
                oFs.DeleteFile(sFILE_Name, True)
            Else
                oFs = New Scripting.FileSystemObject


            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_Suppliers" & vbCrLf & ex.Message, ex, oPayment, "", sFILE_Name)

        Finally
            If Not rootNode Is Nothing Then
                rootNode = Nothing
            End If
            If Not XMLDoc Is Nothing Then
                XMLDoc = Nothing
            End If
            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        Return True

    End Function
    Public Function WriteGjensidigeJDE_UnderRegnskap(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef bVB_MultiFiles As Boolean, ByRef sFILE_Name As String) As Boolean
        ' Felt nr	Kontotekst.txt	UAccounts.xml	
        '1:     Kontonummer - hovedkonto	Text_2		
        '2:     Underkonto                  Text_1
        '3:     Kontotekst                  Text_3
        '4:     Selskapsnummer              Company

        Dim XMLDoc As System.Xml.XmlDocument
        Dim XMLDeclaration As System.Xml.XmlDeclaration
        Dim rootNode As System.Xml.XmlElement
        Dim oBabelFile As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment

        Dim apiNS As String
        Dim xsiNS As String


        Dim oFs As Scripting.FileSystemObject

        Dim sTmp As String
        Dim sMessage As String
        Dim bPaymentsExported As Boolean


        Try

            XMLDoc = New XmlDocument
            XMLDoc.PreserveWhitespace = True
            XMLDeclaration = XMLDoc.CreateXmlDeclaration("1.0", "UTF-8", "yes")
            rootNode = XMLDoc.CreateElement("DocumentElement")

            XMLDoc.InsertBefore(XMLDeclaration, XMLDoc.DocumentElement)
            XMLDoc.AppendChild(rootNode)


            'apiNS = "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02"
            xsiNS = "http://www.w3.org/2001/XMLSchema-instance"

            'rootNode.SetAttribute("xmlns", apiNS)
            rootNode.SetAttribute("xmlns:xsi", xsiNS)
            'rootNode.SetAttribute("schemaLocation", xsiNS, "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02 pain.001.001.02.xsd")

            Using writer As XmlWriter = rootNode.CreateNavigator.AppendChild

                writer.WriteString(vbNewLine)

                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            writer.WriteStartElement("Item")
                            writer.WriteString(vbNewLine)

                            writer.WriteStartElement("Text_1")
                            writer.WriteString(oPayment.I_Account)
                            writer.WriteEndElement() 'Text1
                            writer.WriteString(vbNewLine)

                            writer.WriteStartElement("Text_2")
                            writer.WriteString(oPayment.I_Name)
                            writer.WriteEndElement() 'Text2
                            writer.WriteString(vbNewLine)

                            writer.WriteStartElement("Text_3")
                            writer.WriteString(oPayment.Text_I_Statement)
                            writer.WriteEndElement() 'Text3
                            writer.WriteString(vbNewLine)

                            writer.WriteStartElement("Company")
                            writer.WriteString(oPayment.I_Client)
                            writer.WriteEndElement() 'Company
                            writer.WriteString(vbNewLine)

                            writer.WriteEndElement()  'Item
                            writer.WriteString(vbNewLine)

                            bPaymentsExported = True
                            oPayment.Exported = True
                        Next
                    Next
                Next

            End Using
            rootNode = Nothing

            XMLDoc.Save(sFILE_Name)

            XMLDoc = Nothing

            If Not bPaymentsExported Then
                oFs = New Scripting.FileSystemObject
                oFs.DeleteFile(sFILE_Name, True)
            Else
                oFs = New Scripting.FileSystemObject
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_UnderRegnskap" & vbCrLf & ex.Message, ex, oPayment, "", sFILE_Name)

        Finally
            If Not rootNode Is Nothing Then
                rootNode = Nothing
            End If
            If Not XMLDoc Is Nothing Then
                XMLDoc = Nothing
            End If
            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        Return True

    End Function
    Public Function WriteGjensidigeJDE_Users(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef bVB_MultiFiles As Boolean, ByRef sFILE_Name As String) As Boolean
        ' Input no	Input name	XML	XML Tag	Comment
        ' 1	Aditro_Ident	ITEM	LoginAccount	
        ' 2	Description	ITEM	Surname and FirstName	Input holds Surname,FirstName
        ' 3	EMAIL	ITEM	Email	
        ' 4	FIRMA_ID	ITEM	HomeOrganizationUnitCode	
        ' 5	Basware_ValidFrom	ITEM	ValidFrom	
        ' 6	Basware_SupervisorLoginAccount	ITEM	SupervisorLoginAccount	
        ' 7	Basware_LoginAllowed	ITEM	LoginAllowed	
        ' 8	Basware_LoginType	ITEM	LoginType	
        ' 9	Basware_CollaborationSendType	ITEM	CollaborationSendType	
        ' 10	Basware_MessageSendType	ITEM	MessageSendType	
        ' 11	Aditro_Ansvar_Gyldig_input	ITEM	CostCenterCode	
        ' 12	Aditro_Ident	ITEM	PersonCode	
        ' 13	Aditro_Ident	GROUP	LoginAccount	
        ' 15	Mangler	GROUP	ExternalGroupCode	<FIRMA_ID>_A


        Dim XMLDoc As System.Xml.XmlDocument
        Dim XMLDeclaration As System.Xml.XmlDeclaration
        Dim rootNode As System.Xml.XmlElement
        Dim oBabelFile As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oFreetext As Freetext

        Dim apiNS As String
        Dim xsiNS As String


        Dim oFs As Scripting.FileSystemObject

        Dim sTmp As String
        Dim sMessage As String
        Dim bPaymentsExported As Boolean


        Try

            XMLDoc = New XmlDocument
            XMLDoc.PreserveWhitespace = True
            XMLDeclaration = XMLDoc.CreateXmlDeclaration("1.0", "UTF-8", "yes")
            rootNode = XMLDoc.CreateElement("DocumentElement")

            XMLDoc.InsertBefore(XMLDeclaration, XMLDoc.DocumentElement)
            XMLDoc.AppendChild(rootNode)


            'apiNS = "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02"
            xsiNS = "http://www.w3.org/2001/XMLSchema-instance"

            'rootNode.SetAttribute("xmlns", apiNS)
            rootNode.SetAttribute("xmlns:xsi", xsiNS)
            'rootNode.SetAttribute("schemaLocation", xsiNS, "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02 pain.001.001.02.xsd")

            Using writer As XmlWriter = rootNode.CreateNavigator.AppendChild

                writer.WriteString(vbNewLine)

                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            ' new 15.12.2020 - do not export Cancelled 
                            If Not oPayment.OmitExport Then
                                writer.WriteStartElement("Item")
                                writer.WriteString(vbNewLine)

                                ' Name split in Surname and Firstname, like Sagstad, Jan Hugo Juvik
                                sTmp = oPayment.E_Name
                                If sTmp.IndexOf(",") > -1 Then
                                    sTmp = sTmp.Substring(sTmp.IndexOf(",") + 1).Trim
                                Else
                                    sTmp = ""
                                End If
                                writer.WriteStartElement("FirstName")
                                writer.WriteString(sTmp)
                                writer.WriteEndElement() 'FirstName
                                writer.WriteString(vbNewLine)

                                ' Name split in Surname and Firstname, like Sagstad, Jan Hugo Juvik
                                sTmp = oPayment.E_Name
                                If sTmp.IndexOf(",") > -1 Then
                                    sTmp = sTmp.Substring(0, sTmp.IndexOf(","))
                                End If
                                writer.WriteStartElement("Surname")
                                writer.WriteString(sTmp)
                                writer.WriteEndElement() 'Surname
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("HomeOrganizationUnitCode")
                                writer.WriteString(oPayment.I_Client)
                                writer.WriteEndElement() 'HomeOrganizationUnitCode
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("LoginAccount")
                                writer.WriteString(oPayment.I_Account)
                                writer.WriteEndElement() 'LoginAccount
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Email")
                                writer.WriteString(oPayment.Text_I_Statement)
                                writer.WriteEndElement() 'Email
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("CostCenterCode")
                                writer.WriteString(oPayment.ExtraD4)
                                writer.WriteEndElement() '
                                writer.WriteString(vbNewLine)

                                sTmp = oPayment.DATE_Payment
                                writer.WriteStartElement("ValidFrom")
                                writer.WriteString(sTmp.Substring(0, 4) & "-" & sTmp.Substring(4, 2) & "-" & sTmp.Substring(6, 2))
                                writer.WriteEndElement() 'ValidFrom
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("LoginAllowed")
                                writer.WriteString(oPayment.ExtraD1)
                                writer.WriteEndElement() 'LoginAllowed
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("PersonCode")
                                writer.WriteString(oPayment.I_Account)
                                writer.WriteEndElement() 'PersonCode
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("LoginType")
                                writer.WriteString(oPayment.ExtraD2)
                                writer.WriteEndElement() 'LoginType
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("CollaborationSendType")
                                writer.WriteString(oPayment.ExtraD3)
                                writer.WriteEndElement() 'CollaborationSendType
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("MessageSendType")
                                writer.WriteString(oPayment.NOTI_NotificationAttention)
                                writer.WriteEndElement() 'MessageSendType
                                writer.WriteString(vbNewLine)

                                writer.WriteStartElement("Groups")
                                writer.WriteString(vbNewLine)

                                ' add as many <Group> ExternalGroupCode's as there are freetexts
                                For Each oFreetext In oPayment.Invoices(1).Freetexts
                                    writer.WriteStartElement("Group")
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("LoginAccount")
                                    writer.WriteString(oPayment.I_Account)
                                    writer.WriteEndElement() 'LoginAccount
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("ExternalGroupCode")
                                    writer.WriteString(oFreetext.Text)
                                    writer.WriteEndElement()
                                    writer.WriteString(vbNewLine)

                                    writer.WriteEndElement()  'Groups
                                    writer.WriteString(vbNewLine)
                                Next

                                writer.WriteEndElement()  'Groups
                                writer.WriteString(vbNewLine)

                                writer.WriteEndElement()  'Item
                                writer.WriteString(vbNewLine)

                            End If  ' bOmitExport

                            bPaymentsExported = True
                            oPayment.Exported = True


                        Next
                    Next
                Next

            End Using
            rootNode = Nothing

            XMLDoc.Save(sFILE_Name)

            XMLDoc = Nothing

            If Not bPaymentsExported Then
                oFs = New Scripting.FileSystemObject
                oFs.DeleteFile(sFILE_Name, True)
            Else
                oFs = New Scripting.FileSystemObject
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_User" & vbCrLf & ex.Message, ex, oPayment, "", sFILE_Name)

        Finally
            If Not rootNode Is Nothing Then
                rootNode = Nothing
            End If
            If Not XMLDoc Is Nothing Then
                XMLDoc = Nothing
            End If
            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        Return True

    End Function
    Public Function WriteGjensidigeJDE_AdvancedPermissions(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef bVB_MultiFiles As Boolean, ByRef sFILE_Name As String) As Boolean
        ' File 1	1		LoginAccount
        ' File 1	2		PermissionCode1
        ' File 1	4		Limit
        ' File 1	1-2		ExternalCode
        ' File 2	2	FIRMA_ID	Company
        ' File 2	3	Basware_RowApproveIterationCount	RowApproveIterationCount
        ' File 2	4	Basware_PriorityIndex	PriorityIndex
        ' File 2	5	Basware_Module	Module


        Dim XMLDoc As System.Xml.XmlDocument
        Dim XMLDeclaration As System.Xml.XmlDeclaration
        Dim rootNode As System.Xml.XmlElement
        Dim oBabelFile As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment

        Dim apiNS As String
        Dim xsiNS As String


        Dim oFs As Scripting.FileSystemObject

        Dim sTmp As String
        Dim sMessage As String
        Dim bPaymentsExported As Boolean
        Dim sLastUsedLoginAccount As String = ""
        Dim sLastUsedCompany As String = ""


        Try

            XMLDoc = New XmlDocument
            XMLDoc.PreserveWhitespace = True
            XMLDeclaration = XMLDoc.CreateXmlDeclaration("1.0", "UTF-8", "yes")
            rootNode = XMLDoc.CreateElement("DocumentElement")

            XMLDoc.InsertBefore(XMLDeclaration, XMLDoc.DocumentElement)
            XMLDoc.AppendChild(rootNode)


            'apiNS = "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02"
            xsiNS = "http://www.w3.org/2001/XMLSchema-instance"

            'rootNode.SetAttribute("xmlns", apiNS)
            rootNode.SetAttribute("xmlns:xsi", xsiNS)
            'rootNode.SetAttribute("schemaLocation", xsiNS, "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02 pain.001.001.02.xsd")

            Using writer As XmlWriter = rootNode.CreateNavigator.AppendChild

                writer.WriteString(vbNewLine)

                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If Not oPayment.Exported Then

                                ' 31.10.2018 -
                                ' Each new LoginAccount will first be written,
                                ' then re written with almost same values.
                                ' because of this, a new do/loop has been added
                                Do
                                    writer.WriteStartElement("Item")
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("Company")
                                    writer.WriteString(oPayment.I_Client)
                                    writer.WriteEndElement()
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("ExternalCode")
                                    ' 02.11.2018 - to get a unique ExternalCode, also add PermissionCode2-value
                                    ' 15.11.2018 - also introduced Company as part of the uniqueness
                                    If oPayment.I_Account = sLastUsedLoginAccount And oPayment.I_Client = sLastUsedCompany Then
                                        writer.WriteString(oPayment.I_Account & "-" & oPayment.REF_Own & "-30000-79999")
                                    Else
                                        writer.WriteString(oPayment.I_Account & "-" & oPayment.REF_Own & "-10000-29999")
                                    End If
                                    ' pre 02.11.2018: writer.WriteString(oPayment.I_Account & "-" & oPayment.REF_Own)
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("Limit")
                                    writer.WriteString(oPayment.ExtraD1)
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("LoginAccount")
                                    writer.WriteString(oPayment.I_Account)
                                    writer.WriteEndElement() 'LoginAccount
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("PermissionCode1")
                                    ' 31.10.2018 - for first use of new LoginAccount, leave <PermissionCode> blank
                                    ' If not, write value as previously
                                    If oPayment.I_Account = sLastUsedLoginAccount And oPayment.I_Client = sLastUsedCompany Then
                                        writer.WriteString(oPayment.REF_Own)
                                    End If
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    ' 31.10.2018 - added <PermissionCode2> - with fixed values
                                    ' For new loginaccount, use 10000-29999
                                    ' for same loginaccount, use 30000-79999
                                    writer.WriteStartElement("PermissionCode2")
                                    ' 23.12.2020 - added a 3.rd variant of Advanced permission
                                    If oBabelFile.Special = "JDE_GJENSIDIGE_ADV.PERMISSION3" Then
                                        If oPayment.I_Account = sLastUsedLoginAccount And oPayment.I_Client = sLastUsedCompany Then
                                            writer.WriteString("3000000-7999999")
                                        Else
                                            writer.WriteString("1000000-2999999")
                                        End If
                                    Else
                                        ' standard, old code, as before 23.12.2020
                                        If oPayment.I_Account = sLastUsedLoginAccount And oPayment.I_Client = sLastUsedCompany Then
                                            writer.WriteString("30000-79999")
                                        Else
                                            writer.WriteString("10000-29999")
                                        End If
                                    End If
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("PriorityIndex")
                                    writer.WriteString(oPayment.ExtraD3)
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    sTmp = oPayment.DATE_Payment
                                    writer.WriteStartElement("RowApproveIterationCount")
                                    writer.WriteString(oPayment.ExtraD2)
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("Module")
                                    writer.WriteString(oPayment.E_Name)
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    writer.WriteEndElement()  'Item
                                    writer.WriteString(vbNewLine)

                                    bPaymentsExported = True
                                    oPayment.Exported = True

                                    ' test if new LoginAccount;
                                    ' if so, stay inside loop, and add another record
                                    If oPayment.I_Account = sLastUsedLoginAccount And oPayment.I_Client = sLastUsedCompany Then
                                        Exit Do
                                    End If
                                    ' reset lastUsedLoginAccount
                                    sLastUsedLoginAccount = oPayment.I_Account
                                    sLastUsedCompany = oPayment.I_Client
                                Loop
                                ' reset lastUsedLoginAccount
                                sLastUsedLoginAccount = oPayment.I_Account
                                sLastUsedCompany = oPayment.I_Client
                            End If
                        Next
                    Next
                Next

            End Using
            rootNode = Nothing

            XMLDoc.Save(sFILE_Name)

            XMLDoc = Nothing

            If Not bPaymentsExported Then
                oFs = New Scripting.FileSystemObject
                oFs.DeleteFile(sFILE_Name, True)
            Else
                oFs = New Scripting.FileSystemObject
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_AdvancedPermissionss" & vbCrLf & ex.Message, ex, oPayment, "", sFILE_Name)

        Finally
            If Not rootNode Is Nothing Then
                rootNode = Nothing
            End If
            If Not XMLDoc Is Nothing Then
                XMLDoc = Nothing
            End If
            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        Return True

    End Function
    Public Function WriteGjensidigeJDE_AdvancedPermissions_XLedger(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef bVB_MultiFiles As Boolean, ByRef sFILE_Name As String) As Boolean
        ' 09.12.2020 - added a new AdvancedPermissions format for XLedger

        'Input file	File Name	Input no	Input name	Output no	XML Tag	Comment
        'File 1	UserAdminRules.csv	1	 	4	LoginAccount	 
        'File 1	UserAdminRules.csv	2	Se explanation to PermissionCode2	5	PermissionCode1	First record blank
        ' 	 	 	See explanation to PermissionCode2 below	6	PermissionCode2	First record 1000000-2999999
        'Other records 3000000-7999999
        'File 1	UserAdminRules.csv	4	 	3	Limit	 
        'File 1	UserAdminRules.csv	1-2	 	2	ExternalCode	Concatenate (<Field1>-<Field2>-<PermissionCode2>)
        'File 1	UserAdminRules.csv	5	ansSelskapsnrOW	1	Company	
        'File 2	Basware_AdvancedPermissions.txt	3	Basware_RowApproveIterationCount	8	RowApproveIterationCount	Where File1_Col1 = File2_Col1
        'File 2	Basware_AdvancedPermissions.txt	4	Basware_PriorityIndex	7	PriorityIndex	Where File1_Col1 = File2_Col1
        'File 2	Basware_AdvancedPermissions.txt	5	Basware_Module	9	Module	Where File1_Col1 = File2_Col1


        Dim XMLDoc As System.Xml.XmlDocument
        Dim XMLDeclaration As System.Xml.XmlDeclaration
        Dim rootNode As System.Xml.XmlElement
        Dim oBabelFile As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment

        Dim apiNS As String
        Dim xsiNS As String


        Dim oFs As Scripting.FileSystemObject

        Dim sTmp As String
        Dim sMessage As String
        Dim bPaymentsExported As Boolean
        Dim sLastUsedLoginAccount As String = ""
        Dim sLastUsedCompany As String = ""


        Try

            XMLDoc = New XmlDocument
            XMLDoc.PreserveWhitespace = True
            XMLDeclaration = XMLDoc.CreateXmlDeclaration("1.0", "UTF-8", "yes")
            rootNode = XMLDoc.CreateElement("DocumentElement")

            XMLDoc.InsertBefore(XMLDeclaration, XMLDoc.DocumentElement)
            XMLDoc.AppendChild(rootNode)


            'apiNS = "urn:iso:std:iso:20022:tech:xsd:pain.001.001.02"
            xsiNS = "http://www.w3.org/2001/XMLSchema-instance"

            rootNode.SetAttribute("xmlns:xsi", xsiNS)

            Using writer As XmlWriter = rootNode.CreateNavigator.AppendChild

                writer.WriteString(vbNewLine)

                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If Not oPayment.Exported Then

                                ' 31.10.2018 -
                                ' Each new LoginAccount will first be written,
                                ' then re written with almost same values.
                                ' because of this, a new do/loop has been added
                                Do
                                    writer.WriteStartElement("Item")
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("Company")
                                    writer.WriteString(oPayment.I_Client)
                                    writer.WriteEndElement()
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("ExternalCode")
                                    ' 02.11.2018 - to get a unique ExternalCode, also add PermissionCode2-value
                                    ' 15.11.2018 - also introduced Company as part of the uniqueness
                                    If oPayment.I_Account = sLastUsedLoginAccount And oPayment.I_Client = sLastUsedCompany Then
                                        writer.WriteString(oPayment.I_Account & "-" & oPayment.REF_Own & "-3000000-7999999")
                                    Else
                                        writer.WriteString(oPayment.I_Account & "-" & oPayment.REF_Own & "-1000000-2999999")
                                    End If
                                    ' pre 02.11.2018: writer.WriteString(oPayment.I_Account & "-" & oPayment.REF_Own)
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("Limit")
                                    writer.WriteString(oPayment.ExtraD1)
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("LoginAccount")
                                    writer.WriteString(oPayment.I_Account)
                                    writer.WriteEndElement() 'LoginAccount
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("PermissionCode1")
                                    ' 31.10.2018 - for first use of new LoginAccount, leave <PermissionCode> blank
                                    ' If not, write value as previously
                                    If oPayment.I_Account = sLastUsedLoginAccount And oPayment.I_Client = sLastUsedCompany Then
                                        writer.WriteString(oPayment.REF_Own)
                                    End If
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    ' 09.12.2020 
                                    ' Change from 5 digit to 7 digit number 
                                    writer.WriteStartElement("PermissionCode2")
                                    If oPayment.I_Account = sLastUsedLoginAccount And oPayment.I_Client = sLastUsedCompany Then
                                        writer.WriteString("3000000-7999999")
                                    Else
                                        writer.WriteString("1000000-2999999")
                                    End If
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("PriorityIndex")
                                    writer.WriteString(oPayment.ExtraD3)
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    sTmp = oPayment.DATE_Payment
                                    writer.WriteStartElement("RowApproveIterationCount")
                                    writer.WriteString(oPayment.ExtraD2)
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    writer.WriteStartElement("Module")
                                    writer.WriteString(oPayment.E_Name)
                                    writer.WriteEndElement() '
                                    writer.WriteString(vbNewLine)

                                    writer.WriteEndElement()  'Item
                                    writer.WriteString(vbNewLine)

                                    bPaymentsExported = True
                                    oPayment.Exported = True

                                    ' test if new LoginAccount;
                                    ' if so, stay inside loop, and add another record
                                    If oPayment.I_Account = sLastUsedLoginAccount And oPayment.I_Client = sLastUsedCompany Then
                                        Exit Do
                                    End If
                                    ' reset lastUsedLoginAccount
                                    sLastUsedLoginAccount = oPayment.I_Account
                                    sLastUsedCompany = oPayment.I_Client
                                Loop
                                ' reset lastUsedLoginAccount
                                sLastUsedLoginAccount = oPayment.I_Account
                                sLastUsedCompany = oPayment.I_Client
                            End If
                        Next
                    Next
                Next

            End Using
            rootNode = Nothing

            XMLDoc.Save(sFILE_Name)

            XMLDoc = Nothing

            If Not bPaymentsExported Then
                oFs = New Scripting.FileSystemObject
                oFs.DeleteFile(sFILE_Name, True)
            Else
                oFs = New Scripting.FileSystemObject
            End If

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteGjensidigeJDE_AdvancedPermissionss" & vbCrLf & ex.Message, ex, oPayment, "", sFILE_Name)

        Finally
            If Not rootNode Is Nothing Then
                rootNode = Nothing
            End If
            If Not XMLDoc Is Nothing Then
                XMLDoc = Nothing
            End If
            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        Return True

    End Function
    Private Function Gjensidige_ValidateISO20022Characters(ByVal s As String) As String
        Dim sValidSpecialCharacters As String = ""
        Dim bAllowNorwegianCharacters As Boolean = False
        Dim sIllegalCharacters As String = ""
        Dim bTransformXMLCharacters As Boolean = False

        sValidSpecialCharacters = "|öäÖÄ§|!@#£¤$%€&/{([)]=}?+`\áàèéòóùúÀÁÈÉÒÓÙÚÊÊüÜ~e*;,:._-+:" & Chr(34) & Chr(39)

        Gjensidige_ValidateISO20022Characters = CheckForValidCharacters(s, True, True, bAllowNorwegianCharacters, True, sValidSpecialCharacters, , sIllegalCharacters, bTransformXMLCharacters)

    End Function
End Module
