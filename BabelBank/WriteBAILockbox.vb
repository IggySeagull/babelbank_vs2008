Option Strict Off
Option Explicit On
Module WriteBAILockbox
	' BAI Lockbox for Lockbox, ACH and MT940 American payments (DnBNOR US)
	'---------------------------------------------------------------------
	
	Dim sLine As String ' en output-linje
	Dim oBatch As Batch
	Dim BatchSumAmount As Decimal
	Dim BatchNoRecords As Double
	Dim nFileSumAmount As Double
	Dim dFileFirstDate As Date
	Dim iBatchNumber As Short
	Dim iOldBatchNumber As Short
	Dim nEntryNumber As Double
	Dim nFileEntryNumber As Double
	Dim nFileHash, nBatchHash As Double
	Dim nLines As Double
	Dim nOverflowLines As Double
	Dim sLockboxNo As String ' from ClientSetup, stored in ContractNo (AvtaleID)
	Dim sSpecial As String
    Function WriteBAILockBoxDnBNOR(ByRef oBabelFiles As BabelFiles, ByRef bMultiFiles As Boolean, ByRef sFilenameOut As String, ByRef iFormat_ID As Short, ByRef sI_Account As String, ByRef sOwnRef As String, ByRef sCompanyNo As String, ByRef iFiletype As BabelFiles.FileType) As Boolean
        ' BAILockbox can be exported to three different files;
        ' 1. Original Lockbox payments
        ' 2. Original ACH payments
        ' 3. Original MT940 payments

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreeText As Freetext
        'Dim oFilesetup As FileSetup
        'Dim oClient As Client
        'Dim oaccount As Account
        Dim bExportoBatch, bExportoBabel, bExportoPayment As Boolean
        Dim i As Short
        Dim sNewOwnref As String
        Dim bFileStartWritten As Boolean
        Dim bLastOverflow As Boolean
        Dim bFirst As Boolean
        Dim sFreetext As String
        Dim sOriginalFreetext As String
        Dim sOldLockBoxNO As String
        Dim sOldType As String
        Dim sDate As String
        Dim sOldAccountNo As String
        ''''''''Dim sLockboxNo As String   ' from ClientSetup, stored in ContractNo (AvtaleID)
        Dim bFoundLockboxNo As Boolean

        ' lag en outputfil
        Try

            oFs = New Scripting.FileSystemObject

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            nLines = 0
            iBatchNumber = 0

            For Each oBabel In oBabelFiles
                If EmptyString(sCompanyNo) Then
                    sCompanyNo = oBabel.VB_Profile.CompanyNo
                End If

                sSpecial = UCase(oBabel.Special)


                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects thatt shall
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments

                        If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                            'Don't export payments that have been cancelled, or previously exported
                            If CorrectBAIOutFormat(oPayment, iFiletype) And oPayment.Cancel = False And oPayment.Exported = False Then
                                ' added 28.05.2008, bCorrectOutFormat, to be able to split in several files
                                If oPayment.PayCode = "660" Or oPayment.PayCode = "665" Or (Val(oPayment.PayCode) > 660 And Val(oPayment.PayCode) < 666 And (oPayment.VoucherType = "22" Or oPayment.VoucherType = "32" Or oPayment.VoucherType = "42")) Or ((oPayment.PayCode = "601" Or oPayment.PayCode = "609" Or oPayment.PayCode = "637") And oPayment.MON_InvoiceAmount > 0) Then
                                    If Not bMultiFiles Then
                                        bExportoBabel = True
                                    Else
                                        If oPayment.I_Account = sI_Account Then
                                            If InStr(oPayment.REF_Own, "&?") Then
                                                'Set in the part of the OwnRef that BabelBank is using
                                                sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                            Else
                                                sNewOwnref = ""
                                            End If
                                            If sNewOwnref = sOwnRef Then
                                                bExportoBabel = True
                                            End If
                                        End If
                                    End If
                                Else
                                    oPayment.Exported = True
                                End If
                            End If 'If oPayment.Cancel = False Then
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next oPayment
                    If bExportoBabel Then
                        Exit For
                    End If
                Next oBatch


                If bExportoBabel Then
                    If Not bFileStartWritten Then
                        ' write only once!
                        ' When merging Client-files, skip this one except for first client!
                        sLine = WriteBAILockBoxFileHeader(oBabel, sCompanyNo)
                        oFile.WriteLine((sLine))
                        bFileStartWritten = True
                        ' removed 04.06.2008
                        'sLine = WriteBAILockBoxFileHeader_2(oBabel, sCompanyNo)
                        'oFile.WriteLine (sLine)
                    End If

                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                'Don't export payments that have been cancelled
                                If CorrectBAIOutFormat(oPayment, iFiletype) And oPayment.Cancel = False And oPayment.Exported = False Then
                                    ' added 28.05.2008, bCorrectOutFormat, to be able to split in several files
                                    If oPayment.PayCode = "660" Or oPayment.PayCode = "665" Or (Val(oPayment.PayCode) > 660 And Val(oPayment.PayCode) < 666 And (oPayment.VoucherType = "22" Or oPayment.VoucherType = "32" Or oPayment.VoucherType = "42")) Or ((oPayment.PayCode = "601" Or oPayment.PayCode = "609" Or oPayment.PayCode = "637") And oPayment.MON_InvoiceAmount > 0) Then

                                        If Not bMultiFiles Then
                                            bExportoBatch = True
                                        Else
                                            If oPayment.I_Account = sI_Account Then
                                                If InStr(oPayment.REF_Own, "&?") Then
                                                    'Set in the part of the OwnRef that BabelBank is using
                                                    sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                Else
                                                    sNewOwnref = ""
                                                End If
                                                If sNewOwnref = sOwnRef Then
                                                    bExportoBatch = True
                                                End If
                                            End If
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If
                                End If 'If oPayment.Cancel = False Then
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next oPayment


                        If bExportoBatch Then


                            'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).PayCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If oBatch.Payments(1).PayCode = "660" Then 'Keep original BatchIDs from Lockbox importfile
                                ' XNET 08.07.2013
                                If sSpecial <> "SEACOR" And sSpecial <> "SEACORNEW" Then
                                    ' 15.07.2008 - Seacor will now like the batchnos to run from 001 regardless of paytype
                                    iOldBatchNumber = iBatchNumber
                                    iBatchNumber = Val(oBatch.Batch_ID) ' restored 20.06.2008
                                End If
                                ' XNET 08.07.2013
                                ' Temporary solution for Seacor, while having two lockboxproviders
                                ' Will then use the Accounts-table to "convert" lockboxnumbers, also for Lockboxpayments (previously only for ACH and MT940, as some lines below)
                                ' When the temporary solution with tow providers is past, we can simply change Special in their setup from SEACORNEW to SEACOR
                                If sSpecial = "SEACORNEW" Then
                                    ' Try to find "fake" lockboxno fra accountstable
                                    sLockboxNo = FindFakeLockboxNo(oBatch.Payments(1))
                                Else
                                    ' as pre 08.07.2013
                                    ' Keep orginal lockboxno for 8-record
                                    sLockboxNo = oBatch.Payments(1).VoucherNo
                                End If
                            Else
                                'iOldBatchNumber = iBatchNumber
                                ' ACH or MT940
                                ' Try to find "fake" lockboxno fra accountstable
                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                sLockboxNo = FindFakeLockboxNo(oBatch.Payments(1))
                            End If
                            ' added 2008.03.25 based on Seacor comments
                            ' Write when Lockboxno changes, or when shift between ACH and LBX
                            ' Changed 04.06.2008, Shift when accountno shifts ref Gordon
                            'If Not EmptyString(sOldType) Then
                            'If sOldLockBoxNO <> oBatch.Payments(1).VoucherNo Or sOldType <> oBatch.Payments(1).DnBNORTBIPayType Then
                            'If sOldLockBoxNO <> oBatch.Payments(1).VoucherNo Then
                            'If sOldLockBoxNO <> sLockboxNo Then
                            If sOldLockBoxNO <> sLockboxNo Or sOldAccountNo <> oBatch.Payments(1).I_Account Then
                                If nLines > 1 Then

                                    '------------------------------------------------------
                                    ' XNET 08.07.2013
                                    If oBatch.Payments(1).PayCode = "660" And (sSpecial <> "SEACOR" And sSpecial <> "SEACORNEW") Then 'Keep original BatchIDs from Lockbox importfile
                                        sLine = WriteBAILockBoxAddendumRecord(sDate, sOldLockBoxNO, iOldBatchNumber)
                                    Else
                                        sLine = WriteBAILockBoxAddendumRecord(sDate, sOldLockBoxNO, iBatchNumber)
                                    End If
                                    oFile.WriteLine(sLine)
                                    '------------------------------------------------------

                                    sLine = WriteBAILockBoxFileTrailerRecord(sDate, sOldLockBoxNO)
                                    oFile.WriteLine((sLine))
                                    'sOldLockBoxNO = oBatch.Payments(1).VoucherNo
                                    'sOldLockBoxNO = sLockboxNo
                                End If
                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().DnBNORTBIPayType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                sOldType = oBatch.Payments(1).DnBNORTBIPayType
                                'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().I_Account. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                sOldAccountNo = oBatch.Payments(1).I_Account


                                ' 04.06.2008

                                ' Always a 2 and 5 record paired together
                                sLine = WriteBAILockBoxFileHeader_2(oBabel, sCompanyNo)
                                oFile.WriteLine((sLine))

                                sLine = WriteBAILockBoxBatchHeader(oBatch, sCompanyNo)
                                oFile.WriteLine((sLine))
                                '---------------------------------------------------
                            End If
                            'End If

                            sOldLockBoxNO = sLockboxNo

                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False

                                If EmptyString(sOldType) Then
                                    sOldType = oPayment.DnBNORTBIPayType
                                End If

                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    'Don't export payments that have been cancelled
                                    If CorrectBAIOutFormat(oPayment, iFiletype) And oPayment.Cancel = False And oPayment.Exported = False Then
                                        ' added 28.05.2008, bCorrectOutFormat, to be able to split in several files
                                        If oPayment.PayCode = "660" Or oPayment.PayCode = "665" Or (Val(oPayment.PayCode) > 660 And Val(oPayment.PayCode) < 665 And (oPayment.VoucherType = "22" Or oPayment.VoucherType = "32" Or oPayment.VoucherType = "42")) Then
                                            ' export LBX or (ACH and credits) only
                                            If Not bMultiFiles Then
                                                bExportoPayment = True
                                            Else
                                                If oPayment.I_Account = sI_Account Then
                                                    If InStr(oPayment.REF_Own, "&?") Then
                                                        'Set in the part of the OwnRef that BabelBank is using
                                                        sNewOwnref = Right(oPayment.REF_Own, Len(oPayment.REF_Own) - InStr(oPayment.REF_Own, "&?") - 1)
                                                    Else
                                                        sNewOwnref = ""
                                                    End If
                                                    If sNewOwnref = sOwnRef Then
                                                        bExportoPayment = True
                                                    End If
                                                End If
                                            End If
                                        Else
                                            oPayment.Exported = True
                                        End If
                                    Else
                                        oPayment.Exported = True
                                    End If 'If oPayment.Cancel = False Then
                                End If

                                If bExportoPayment Then

                                    '-----------------------------------------------------------
                                    sLine = WriteBAILockBoxEntryDetailRecord(oBatch, oPayment)
                                    oFile.WriteLine((sLine))
                                    '-----------------------------------------------------------

                                    i = 0
                                    sFreetext = ""

                                    ' Mangler tester for � begrense fritekst
                                    ' Mangler skille p� CTX og CCD for � se hvilken fritekst som skal med
                                    ' Manger Discountamount p� invoice.original

                                    If CDbl(oPayment.PayCode) > 660 And CDbl(oPayment.PayCode) <= 665 Then
                                        ' ------------
                                        ' ACH, or MT940
                                        ' ------------
                                        ' put together the interesting freetextinfo
                                        sOriginalFreetext = ""
                                        For Each oInvoice In oPayment.Invoices
                                            ' Pick RMR, NTE, ADX, REF segmentinfo
                                            ' these are extracted in the import, and put into invoices
                                            ' with .MATCH_Original = False
                                            ' The freetexts that we are interested in are put into
                                            ' freetexts with.Qualifier = 3
                                            'sOriginalFreetext = ""
                                            If Not oInvoice.MATCH_Original Or CDbl(oPayment.PayCode) = 665 Then
                                                bFirst = True
                                                sOriginalFreetext = "" ' One invoice pr RMR
                                                For Each oFreeText In oInvoice.Freetexts
                                                    ' 07.07.2009
                                                    ' use all freetext (from :86: record!
                                                    'If oFreeText.Qualifier = 3 Then
                                                    sOriginalFreetext = sOriginalFreetext & " " & oFreeText.Text
                                                    'End If
                                                Next oFreeText
                                                sOriginalFreetext = Trim(sOriginalFreetext)

                                                ' 19.06.2008
                                                ' SeaCor has asked not export only ONE 4-record pr invoice
                                                ' XNET 08.07.2013
                                                If sSpecial = "SEACOR" Or sSpecial = "SEACORNEW" Then
                                                    If oInvoice.Index = oPayment.Invoices.Count Then
                                                        bLastOverflow = True
                                                    Else
                                                        bLastOverflow = False
                                                    End If
                                                    sLine = WriteBAILockBoxEntryDetailOverflowRecord(oBatch, oPayment, oInvoice, Left(sOriginalFreetext, 33), bLastOverflow, bFirst)
                                                    oFile.WriteLine((sLine))
                                                    bFirst = False
                                                Else
                                                    bLastOverflow = False
                                                    '''' export every invoice where MATCH_Original = False
                                                    '''' If oInvoice.Index = oPayment.Invoices.Count Then
                                                    Do
                                                        If Len(sOriginalFreetext) < 34 Then
                                                            bLastOverflow = True
                                                        End If

                                                        sLine = WriteBAILockBoxEntryDetailOverflowRecord(oBatch, oPayment, oInvoice, Left(sOriginalFreetext, 33), bLastOverflow, bFirst)
                                                        bFirst = False
                                                        oFile.WriteLine((sLine))
                                                        If Len(sOriginalFreetext) < 34 Then
                                                            Exit Do
                                                        End If
                                                        sOriginalFreetext = Mid(sOriginalFreetext, 34)
                                                    Loop
                                                End If
                                                bFirst = False

                                            End If
                                        Next oInvoice


                                    ElseIf oPayment.PayCode = "660" Then
                                        ' -------
                                        ' Lockbox
                                        ' -------
                                        bLastOverflow = False
                                        bFirst = True
                                        For Each oInvoice In oPayment.Invoices
                                            bFirst = True
                                            If oInvoice.Index = oPayment.Invoices.Count Then
                                                bLastOverflow = True
                                            End If
                                            sLine = WriteBAILockBoxEntryDetailOverflowRecord(oBatch, oPayment, oInvoice, "", bLastOverflow, bFirst)
                                            oFile.WriteLine((sLine))
                                            bFirst = False
                                        Next oInvoice

                                    End If


                                    oPayment.Exported = True
                                End If

                            Next oPayment ' payment
                            '------------------------------------------------------
                            ' Removed 04.06.2008
                            'sLine = WriteBAILockBoxAddendumRecord(oBatch)
                            'oFile.WriteLine (sLine)
                            '------------------------------------------------------

                            ' added 2008.03.25 based on Seacor comments
                            ' Write when Lockboxno changes, or when shift between ACH and LBX
                            'If sOldLockBoxNO <> oBatch.Payments(1).VoucherNo Or sOldType <> oBatch.Payments(1).DnBNORTBIPayType Then
                            '    sLine = WriteBAILockBoxFileTrailerRecord(sDate, sLockboxNo)
                            '    oFile.WriteLine (sLine)
                            '    sOldLockBoxNO = oBatch.Payments(1).VoucherNo
                            '    sOldType = oBatch.Payments(1).DnBNORTBIPayType
                            'End If
                        End If
                        sDate = oBatch.DATE_Production
                    Next oBatch 'batch
                End If

            Next oBabel 'Babelfile
            ' Last 7/8-record:
            If nLines > 0 Then

                '------------------------------------------------------
                ' added 04.06.2008
                If iBatchNumber > 0 Then
                    sLine = WriteBAILockBoxAddendumRecord(sDate, sLockboxNo, iBatchNumber)
                Else
                    ' for LBX
                    sLine = WriteBAILockBoxAddendumRecord(sDate, sLockboxNo, iOldBatchNumber)
                End If

                oFile.WriteLine((sLine))
                '------------------------------------------------------

                sLine = WriteBAILockBoxFileTrailerRecord(sDate, sLockboxNo)
                oFile.WriteLine((sLine))

                ' 9 record
                sLine = WriteBAILockBoxFileControlRecord()
                oFile.WriteLine((sLine))
            End If
            '-----------------------------------------

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteBAILockBoxDnBNOR" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteBAILockBoxDnBNOR = True

    End Function
    Function WriteBAILockBoxFileHeader(ByRef oBabelFile As BabelFile, ByRef sCompanyNo As String) As String
        ' This is the DnBNOR NY version of Lockbox format
        Dim sLine As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileEntryNumber = 0
        dFileFirstDate = System.Date.FromOADate(Now.ToOADate + 1000)
        nLines = 1

        sLine = "1" ' 1. Recordtype code        1
        sLine = sLine & "00" ' 2. Priority code          2-3

        sLine = sLine & PadLeft(Left(sCompanyNo, 10), 10, " ") ' 3. Immediate destination  4-13
        sLine = sLine & "DnB NOR NY" ' 4. Immeridate origin       14-23

        sLine = sLine & VB6.Format(Date.Today, "YYMMDD") '5. File creation date     24-29
        sLine = sLine & VB6.Format(Now, "hhmm") '6. File creation time     30-33
        sLine = sLine & Space(47) '7. Not in use

        WriteBAILockBoxFileHeader = sLine

    End Function
    Function WriteBAILockBoxFileHeader_2(ByRef oBabelFile As BabelFile, ByRef sCompanyNo As String) As String
        ' This is the DnBNOR NY version of Lockbox format
        Dim sLine As String
        nLines = nLines + 1

        sLine = "2" ' 1. Recordtype code        1
        sLine = sLine & "  " ' 2. Dest origin, not used  2-3

        sLine = sLine & Space(10) ' 3. Reference Code - Not Used 4-13
        sLine = sLine & "400" ' 4. Service Type Code      14-16
        sLine = sLine & "080" ' 5. Record Size            17-19
        sLine = sLine & "080" ' 6. Block Size             20-22
        sLine = sLine & "2" ' 7. Format                 23
        sLine = sLine & Space(57) ' 8. Not in use

        WriteBAILockBoxFileHeader_2 = sLine

    End Function
    Function WriteBAILockBoxBatchHeader(ByRef oBatch As Batch, ByRef sCompanyNo As String) As String
        Dim sLine As String

        ' Reset Batch-totals:
        BatchSumAmount = 0
        BatchNoRecords = 0

        ' XNET 08.07.2013
        If oBatch.Payments(1).PayCode <> "660" Or sSpecial = "SEACOR" Or sSpecial = "SEACORNEW" Then 'Keep original BatchIDs from Lockbox importfile
            iOldBatchNumber = iBatchNumber
            iBatchNumber = iBatchNumber + 1
        End If

        nEntryNumber = 0 ' Type 6 records
        nLines = nLines + 1

        sLine = "5" ' 1. Recordtype code        1
        'If oBatch.Payments(1).PayCode = "660" Then 'Keep original BatchIDs from Lockbox importfile
        '    sLine = sLine & Right$("000" & oBatch.Batch_ID, 3)            ' 2. Batch Number           2-4
        'Else
        sLine = sLine & Right("000" & LTrim(Str(iBatchNumber)), 3) ' 2. Batch Number           2-4
        'End If

        sLine = sLine & "000" ' 3. Item number, not used   5-7

        ' BabelBank Internal paycodes
        ' Lockbox              660
        ' ACH CTX              661
        ' ACH CCD              662
        ' ACH PPD              663
        sLine = sLine & Right("0000000" & sLockboxNo, 7) ' 4. LockboxNo, from ClientSetup, Accounts, AvtaleID (Contractno)
        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).PayCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If oBatch.Payments(1).PayCode = "660" Or oBatch.Payments(1).PayCode = "665" Then 'Lockbox
            '''sLine = sLine & PadLeft(oBatch.Payments(1).VoucherNo, 7, "0")    ' 4. Lockbox Number   8-14
            'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments().DATE_Payment. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sLine = sLine & Right(oBatch.Payments(1).DATE_Payment, 6) ' 5. Date YYMMDD
        Else
            'sLine = sLine & Space(7)                                         ' 4. LockboxNo, but use CTX, CCD, PPD etc
            ' 2008.04.02 "Fake" lockboxno, from AccountsTable, included for ACH
            ''''sLine = sLine & PadLeft(oBatch.Payments(1).VoucherNo, 7, "0")    ' 4. Lockbox Number   8-14
            sLine = sLine & Right(oBatch.DATE_Production, 6) ' 5. Date YYMMDD
        End If

        sLine = sLine & PadLeft(Left(sCompanyNo, 10), 10, " ") ' 6. Destination name  21-30
        sLine = sLine & "DnB NOR NY" ' 7. Immeridate origin 31-40
        'UPGRADE_WARNING: Couldn't resolve default property of object oBatch.Payments(1).PayCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If oBatch.Payments(1).PayCode <> "660" Then 'Lockbox
            sLine = sLine & Left(oBatch.Version & Space(3), 3) ' 8. SEC-code for non-lockbox
        Else
            sLine = sLine & Space(3) ' 8. Not in use for Lockbox
        End If
        ' added field 9 2008.03.28, replace , "561001100", "") 2008.03.31
        sLine = sLine & PadLeft(Left(Replace(oBatch.Payments(1).I_Account, "561001100", ""), 17), 17, " ") ' 9. DnB Account Number  44-60
        sLine = sLine & Space(20) ' 10. Not in use

        WriteBAILockBoxBatchHeader = sLine

    End Function
    Function WriteBAILockBoxEntryDetailRecord(ByRef oBatch As Batch, ByRef oPayment As Payment) As String
        Dim sLine As String
        Dim sPayCode As String

        nLines = nLines + 1
        nOverflowLines = 0

        ' Add to Batch-totals:
        BatchSumAmount = BatchSumAmount + oPayment.MON_InvoiceAmount
        nFileSumAmount = nFileSumAmount + oPayment.MON_InvoiceAmount
        BatchNoRecords = BatchNoRecords + 1
        nEntryNumber = nEntryNumber + 1
        nFileEntryNumber = nFileEntryNumber + 1

        sLine = "6" ' 1. Recordtype code        1
        sLine = sLine & Right("000" & LTrim(Str(iBatchNumber)), 3) ' 2. Batch Number           2-4

        If oPayment.PayCode = "660" Then 'Keep original BatchIDs from Lockbox importfile
            '''sLine = sLine & Right$("000" & oBatch.Batch_ID, 3)            ' 2. Batch Number           2-4
            ' 16.07.2008 - Do not use original Itemnumbers for Lockbox, in case file is "reorganized"
            '''sLine = sLine & PadLeft(oPayment.ExtraD3, 3, "0")             ' 3. Item number            5-7
            sLine = sLine & PadLeft(CStr(nEntryNumber), 3, "0") ' 3. Item number            5-7
            sLine = sLine & PadLeft(CStr(oPayment.MON_InvoiceAmount), 10, "0") ' 4. Check Amount-2 dec.    8-17
            sLine = sLine & PadLeft(oPayment.REF_Bank2, 9, "0") ' 5. Check R/T              18-26
            sLine = sLine & PadLeft(oPayment.REF_Own, 13, "0") ' 6. Check DDA Acct         27-39
            sLine = sLine & PadLeft(oPayment.REF_Bank1, 9, "0") ' 7. Check number    40-48
            sLine = sLine & PadLeft(oPayment.ExtraD2, 6, "0") ' 8. Check Date - mmddyy   49-54

        Else
            '''sLine = sLine & Right$("000" & LTrim(Str(iBatchNumber)), 3)   ' 2. Batch Number           2-4
            sLine = sLine & PadLeft(CStr(nEntryNumber), 3, "0") ' 3. Item number            5-7
            sLine = sLine & PadLeft(CStr(oPayment.MON_InvoiceAmount), 10, "0") ' 4. Check Amount-2 dec.    8-17
            sLine = sLine & PadLeft(oPayment.BANK_BranchNo, 8, "0") & AddCDV7Digit((oPayment.BANK_BranchNo)) ' 5. Check R/T + checkdigit 18-26
            sLine = sLine & PadLeft(oPayment.ExtraD1, 13, "0") ' 6. Check DDA Acct         27-39

            If CDbl(oPayment.PayCode) = 665 Then
                sLine = sLine & PadLeft(Right(oPayment.REF_Own, 9), 9, "0") ' 7. Check number    40-48
            Else
                'If Not EmptyString(oPayment.REF_Bank1) Then
                ' 20.08.2008 - Must test on oPayment.Text_I_Statement, NOT oPayment.REF_Bank1 (?)
                If Not EmptyString((oPayment.Text_I_Statement)) Then
                    ' Filled in for ACH-payments from a "match" in MT940-payments
                    sLine = sLine & PadLeft(oPayment.Text_I_Statement, 9, "0") ' 7. Check number    40-48
                Else
                    'sLine = sLine & Space(9)                                      ' 7. Check number    40-48
                    ' changed 19.06.2008 to 000000000
                    sLine = sLine & "000000000" ' 7. Check number    40-48
                End If
            End If
            sLine = sLine & Mid(oPayment.DATE_Payment, 5, 2) & Mid(oPayment.DATE_Payment, 7, 2) & Mid(oPayment.DATE_Payment, 3, 2) ' 8. Check Date - mmddyy   49-54
        End If
        sLine = sLine & PadRight(oPayment.E_Name, 26, " ") ' 9. Check Payor Name      55-80

        WriteBAILockBoxEntryDetailRecord = sLine

    End Function
    Function WriteBAILockBoxEntryDetailOverflowRecord(ByRef oBatch As Batch, ByRef oPayment As Payment, ByRef oInvoice As Invoice, ByRef sText As String, ByRef bLastOverflow As Boolean, ByRef bFirst As Boolean) As String
        Dim sLine As String
        Dim sPayCode As String

        nOverflowLines = nOverflowLines + 1
        nLines = nLines + 1

        sLine = "4" ' 1. Recordtype code        1
        sLine = sLine & Right("000" & LTrim(Str(iBatchNumber)), 3) ' 2. Batch Number           2-4

        If oPayment.PayCode = "660" Then 'Keep original BatchIDs from Lockbox importfile
            ''''sLine = sLine & Right$("000" & oBatch.Batch_ID, 3)            ' 2. Batch Number           2-4
            ' 16.07.2008 - Do not use original Itemnumbers for Lockbox, in case file is "reorganized"
            'sLine = sLine & PadLeft(oPayment.ExtraD3, 3, "0")             ' 3. Item number            5-7
            sLine = sLine & PadLeft(CStr(nEntryNumber), 3, "0") ' 3. Item number            5-7
            sLine = sLine & "6" ' 4. Overflow Type          8
            sLine = sLine & PadLeft(LTrim(Str(nOverflowLines)), 2, "0") ' 5. Overflow Seq Nbr       9-10
            If bLastOverflow Then
                sLine = sLine & "9" ' 6. Last Ovfl Flag         11
            Else
                sLine = sLine & "0" ' 6. Last Ovfl Flag         11
            End If

        Else
            '''sLine = sLine & Right$("000" & LTrim(Str(iBatchNumber)), 3)   ' 2. Batch Number           2-4
            sLine = sLine & PadLeft(CStr(nEntryNumber), 3, "0") ' 3. Item number            5-7
            sLine = sLine & "6" ' 4. Overflow Type          8
            sLine = sLine & PadLeft(LTrim(Str(nOverflowLines)), 2, "0") ' 5. Overflow Seq Nbr       9-10
            ' Last overflowline?
            If bLastOverflow Then
                sLine = sLine & "9" ' 6. Last Ovfl Flag         11
            Else
                sLine = sLine & "0" ' 6. Last Ovfl Flag         11
            End If
        End If

        If bFirst Then
            ' Amounts and invoiceno only for first 4-record
            sLine = sLine & PadRight(oInvoice.InvoiceNo, 16, " ") ' 7. Invoice Number         12-27
            'sLine = sLine & PadLeft(oInvoice.MON_InvoiceAmount, 10, "0")      ' 8. Invoice Payment Amount 28-37
            ' 2008.03.21
            ' Should only have Invoice Payment Amount from record 7, no from record 6, so we have changed to MON_TransferredAmount
            If oInvoice.MON_TransferredAmount = 0 Then
                sLine = sLine & New String(" ", 10) ' 8. Invoice Payment Amount 28-37
            Else
                ' XNET 08.07.2013
                If (sSpecial = "SEACOR" Or sSpecial = "SEACORNEW") And EmptyString(oInvoice.InvoiceNo) Then
                    sLine = sLine & New String(" ", 10) ' 8. Invoice Payment Amount 28-37
                Else
                    sLine = sLine & PadLeft(CStr(oInvoice.MON_TransferredAmount), 10, "0") ' 8. Invoice Payment Amount 28-37
                End If
            End If
            'If oInvoice.MON_DiscountAmount = 0 Then
            '    sLine = sLine & String(10, " ")                              ' 8. Invoice Payment Amount 28-37
            'Else
            ' XNET 08.07.2013
            If (sSpecial = "SEACOR" Or sSpecial = "SEACORNEW") And EmptyString(oInvoice.InvoiceNo) Then
                sLine = sLine & New String(" ", 10) ' 8. Invoice Payment Amount 28-37
            Else
                ' XNET 08.07.2013
                If (sSpecial = "SEACOR" Or sSpecial = "SEACORNEW") And EmptyString(oInvoice.InvoiceNo) Then
                    sLine = sLine & New String(" ", 10) ' 8. Invoice Payment Amount 28-37
                Else
                    sLine = sLine & PadLeft(oInvoice.MON_DiscountAmount, 10, "0") ' 9. Invoice Deduction Amount 38-47
                End If
            End If
            'End If
        Else
            sLine = sLine & New String(" ", 16) ' 7. Invoice Number         12-27
            sLine = sLine & New String(" ", 10) ' 8. Invoice Payment Amount 28-37
            sLine = sLine & New String(" ", 10) ' 9. Invoice Deduction Amount 38-47
        End If

        ' 19.06.2008
        ' SeaCor has asked not to have freetext in pos 48-80
        ' XNET 08.07.2013
        If sSpecial = "SEACOR" Or sSpecial = "SEACORNEW" Then
            sLine = sLine & Space(33) '10. Not in use for lockbox  48-80
        Else
            If oPayment.PayCode = "660" Then 'Lockbox
                sLine = sLine & Space(33) '10. Not in use for lockbox  48-80
            ElseIf CDbl(oPayment.PayCode) > 660 And CDbl(oPayment.PayCode) < 665 Then
                ' For ACH, present "structured freetext". This is freetext for invoices with .MATCH_original = False
                sLine = sLine & PadRight(Left(sText, 33), 33, " ")
            ElseIf CDbl(oPayment.PayCode) = 665 Then
                ' MT940
                sLine = sLine & PadRight(Left(sText, 33), 33, " ")

            Else
                sLine = sLine & Space(33) '10. Wait for mt940
            End If
        End If

        WriteBAILockBoxEntryDetailOverflowRecord = sLine

    End Function
    Function WriteBAILockBoxAddendumRecord(ByRef sDate As String, ByRef sLbxNo As String, ByRef iBatchNo As Short) As String
        'Function WriteBAILockBoxAddendumRecord(oBatch As Batch) As String
        Dim sLine As String

        nLines = nLines + 1

        sLine = "7" ' 1. Recordtype code        1

        'If oBatch.Payments(1).PayCode = "660" Then 'Keep original BatchIDs from Lockbox importfile
        '    sLine = sLine & Right$("000" & oBatch.Batch_ID, 3)            ' 2. Batch Number           2-4
        'Else
        sLine = sLine & Right("000" & LTrim(Str(iBatchNo)), 3) ' 2. Batch Number           2-4
        'End If

        sLine = sLine & "000" ' 3. Item number, not used   5-7

        ' BabelBank Internal paycodes
        ' Lockbox              660
        ' ACH CTX              661
        ' ACH CCD              662
        ' ACH PPD              663
        sLine = sLine & Right("0000000" & sLbxNo, 7) ' 4. LockboxNo, from ClientSetup, Accounts, AvtaleID (Contractno)
        'If oBatch.Payments(1).PayCode = "660" Then 'Lockbox
        'If sPayCode = "660" Then ' Lockbox
        ''''    sLine = sLine & PadLeft(oBatch.Payments(1).VoucherNo, 7, "0") ' 4. Lockbox Number   8-14
        'sLine = sLine & Right$(oBatch.Payments(1).DATE_Payment, 6)    ' 5. Date YYMMDD
        sLine = sLine & Right(sDate, 6) ' 5. Date YYMMDD
        'Else
        'sLine = sLine & Space(7)                                      ' 4. LockboxNo, blank for ACH etc
        ' 2008.04.02 "Fake" lockboxno, from AccountsTable, included for ACH
        ''''    sLine = sLine & PadLeft(oBatch.Payments(1).VoucherNo, 7, "0") ' 4. Lockbox Number   8-14
        '    sLine = sLine & Right$(oBatch.DATE_Production, 6) ' 5. Date YYMMDD
        'End If

        sLine = sLine & PadLeft(CStr(Val(CStr(nEntryNumber))), 3, "0") ' 6. No of 6-records    21-23
        sLine = sLine & PadLeft(CStr(BatchSumAmount), 10, "0") ' 7. Batch Total Dollars 24-33
        sLine = sLine & Space(47) ' 8. Not in use,        34-80

        WriteBAILockBoxAddendumRecord = sLine

    End Function
    Function WriteBAILockBoxFileTrailerRecord(ByRef sDate As String, ByRef sLbxNo As String) As String
        Dim sLine As String

        nLines = nLines + 1

        sLine = "8" ' 1. Recordtype code        1
        'sLine = sLine & Right$("000" & LTrim(Str(iBatchNumber)), 3)      ' 2. Batch Number           2-4
        sLine = sLine & "000" ' BatchNo gives no meaning in 8-rec? 2. Batch Number           2-4
        sLine = sLine & "000" ' 3. Item number, not used   5-7

        sLine = sLine & Right("0000000" & sLbxNo, 7) ' 4. LockboxNo, from ClientSetup, Accounts, AvtaleID (Contractno)
        sLine = sLine & Right(sDate, 6) ' 5. Date YYMMDD
        sLine = sLine & PadLeft(CStr(Val(CStr(nFileEntryNumber))), 4, "0") ' 6. No of 6-records    21-24
        sLine = sLine & PadLeft(CStr(nFileSumAmount), 10, "0") ' 7. Batch Total Dollars 24-33

        sLine = sLine & Space(46) ' 8. Not in use,        35-80

        ' added 2008.03.25
        nFileEntryNumber = 0
        nFileSumAmount = 0

        WriteBAILockBoxFileTrailerRecord = sLine

    End Function
    Function WriteBAILockBoxFileControlRecord() As String '(oBabelFile As BabelFile) As String
        ' ok jp
        Dim sLine As String
        nLines = nLines + 1

        sLine = "9" ' 1. Recordtype code        1
        sLine = sLine & PadLeft(CStr(nLines), 6, "0") ' 2. Total Records Sent     2-7
        sLine = sLine & Space(73) ' 3. Not used               8-80

        WriteBAILockBoxFileControlRecord = sLine

    End Function
    Private Function FindFakeLockboxNo(ByRef oPayment As Payment) As String
        ' added 2008.04.04
        ' Find "fake" Lockboxno from clientsetup for ACH and MT940. Stored in ContractNo (avtaleID)
        Dim oFilesetup As FileSetup
        Dim oClient As Client
        Dim oaccount As Account

        Dim sLbx As String
        Dim bFoundLockboxNo As Boolean

        sLbx = ""
        bFoundLockboxNo = False
        For Each oFilesetup In oPayment.VB_Profile.FileSetups
            For Each oClient In oFilesetup.Clients
                For Each oaccount In oClient.Accounts
                    ' this is DnBNOR US specific, and ACH-accounts starts with
                    ' 561001100. Strip 561001100 prefix, to be able to use the same
                    ' account in setup for both ACH and Lockbox
                    ' In the 8-record, fill in this lockboxno for ACH and MT940
                    '--------------------------------
                    ' for LBX, keep original lockboxno
                    ' Has not (2008.03.28) solved several lockboxes pr account
                    ' Must also remove leading zeros
                    If oaccount.Account = Trim(RemoveLeadingCharacters(Replace(oPayment.I_Account, "561001100", ""), "0")) Then
                        sLbx = oaccount.ContractNo
                        If Len(sLbx) > 0 Then
                            bFoundLockboxNo = True
                            Exit For
                        End If

                    ElseIf Not EmptyString((oaccount.ConvertedAccount)) And (Trim(oaccount.ConvertedAccount) = oPayment.I_Account) Then
                        sLbx = oaccount.ContractNo
                        If Len(sLbx) > 0 Then
                            bFoundLockboxNo = True
                            Exit For
                        End If
                    End If

                    If bFoundLockboxNo Then
                        Exit For
                    End If
                Next oaccount
                If bFoundLockboxNo Then
                    Exit For
                End If
            Next oClient

            If bFoundLockboxNo Then
                Exit For
            End If
        Next oFilesetup
        If Not bFoundLockboxNo Then
            sLbx = "0000000"
        End If

        '--------- end find lockboxno -----------------------------------
        FindFakeLockboxNo = sLbx

    End Function
	Private Function CorrectBAIOutFormat(ByRef oPayment As Payment, ByRef iFiletype As BabelFiles.FileType) As Boolean
		Dim bCorrectOutFormat As Boolean
		
		bCorrectOutFormat = False
		' Find out if this payment is of the type that we like to export;
		If CDbl(oPayment.PayCode) > 660 And CDbl(oPayment.PayCode) < 665 And iFiletype = BabelFiles.FileType.BAILockBoxACH Then
			' ACH original
			bCorrectOutFormat = True
		ElseIf CDbl(oPayment.PayCode) = 665 And iFiletype = BabelFiles.FileType.BAILockBoxMT940 Then 
			' MT940 original
			bCorrectOutFormat = True
		ElseIf CDbl(oPayment.PayCode) = 660 And iFiletype = BabelFiles.FileType.BAILockBox Then 
			' Lockbox original
			bCorrectOutFormat = True
		End If
		CorrectBAIOutFormat = bCorrectOutFormat
	End Function
End Module
