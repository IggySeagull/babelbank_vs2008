Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("Mapping_NET.Mapping")> Public Class Mapping
	' Used for importing files with and "easy" fileformat, where we can map
	' the format into an XML-mappingfile
	
	Private bInitialized As Boolean
	Private oXMLMappingObject As New MSXML2.DOMDocument40
	Private iHeaderLines As Short
	Private iFooterLines As Short
	Private sDelimiter As String
	Private sSurroundChar As String
	Private sDateFormat As String
	Private sDateDelim As String
	Private iDateAddDays As Short ' Add f.ex. 2 days for BACS-payments
	Private sCountry As String ' Ex. GB for BACS paymens in UK
	Private sAmountThousandSep As String
	Private sAmountDecimalSep As String
	Private sTypeFormatDomestic As String
	Private sTypeFormatInternational As String
	Private sTypeFormatSalary As String
	Private sTypeFormatMass As String
	Private sLine As String ' Current imported line
	Private sFileType As String
	Private sHeaderLine As String
	Private sFooterLine As String ' Holds content of files footerline
	Private iDayStart, iMonthStart, iYearStart, iYearLength, iMonthLength, iDayLength As Short
	Private nLineNo As Double
	
	'********* START PROPERTY SETTINGS ***********************
	Public ReadOnly Property NoOfHeaderLines() As Short
		Get
			NoOfHeaderLines = iHeaderLines
		End Get
	End Property
	Public ReadOnly Property NoOfFooterLines() As Short
		Get
			NoOfFooterLines = iFooterLines
		End Get
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
	Public Function GetPos(ByRef sVariable As String) As Object
		Dim rElement As MSXML2.IXMLDOMElement
		Dim NodeAttribute As MSXML2.IXMLDOMNode
		
		'UPGRADE_WARNING: Couldn't resolve default property of object GetPos. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetPos = 0
		' Get pos-data from Mappingfile
		NodeAttribute = oXMLMappingObject.selectSingleNode("//*/" & sVariable)
		If Not NodeAttribute Is Nothing Then
			GetPos = Val(NodeAttribute.Attributes.getNamedItem("Pos").nodeValue)
		End If
		'UPGRADE_NOTE: Object NodeAttribute may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		NodeAttribute = Nothing
		
	End Function
	Public Function GetLength(ByRef sVariable As String) As Object
		Dim rElement As MSXML2.IXMLDOMElement
		Dim NodeAttribute As MSXML2.IXMLDOMNode
		
		'UPGRADE_WARNING: Couldn't resolve default property of object GetLength. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetLength = 0
		' Get pos-data from Mappingfile
		NodeAttribute = oXMLMappingObject.selectSingleNode("//*/" & sVariable)
		If Not NodeAttribute Is Nothing Then
			GetLength = Val(NodeAttribute.Attributes.getNamedItem("Length").nodeValue)
		End If
		'UPGRADE_NOTE: Object NodeAttribute may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		NodeAttribute = Nothing
		
	End Function
	Public Function GetFileType() As Object
		Dim rElement As MSXML2.IXMLDOMElement
		Dim NodeAttribute As MSXML2.IXMLDOMNode
		
		NodeAttribute = oXMLMappingObject.selectSingleNode("//*/" & "General")
		If Not NodeAttribute Is Nothing Then
			'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object GetFileType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			GetFileType = NodeAttribute.Attributes.getNamedItem("FileType").nodeValue
		End If
		'UPGRADE_NOTE: Object NodeAttribute may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		NodeAttribute = Nothing
		
	End Function
	Public Function GetDelimiter() As Object
		Dim rElement As MSXML2.IXMLDOMElement
		Dim NodeAttribute As MSXML2.IXMLDOMNode
		
		NodeAttribute = oXMLMappingObject.selectSingleNode("//*/" & "General")
		If Not NodeAttribute Is Nothing Then
			'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object GetDelimiter. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			GetDelimiter = NodeAttribute.Attributes.getNamedItem("Delimiter").nodeValue
		End If
		'UPGRADE_NOTE: Object NodeAttribute may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		NodeAttribute = Nothing
		
	End Function
	Public Function GetSurround() As Object
		Dim rElement As MSXML2.IXMLDOMElement
		Dim NodeAttribute As MSXML2.IXMLDOMNode
		
		NodeAttribute = oXMLMappingObject.selectSingleNode("//*/" & "General")
		If Not NodeAttribute Is Nothing Then
			'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object GetSurround. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			GetSurround = NodeAttribute.Attributes.getNamedItem("Surround").nodeValue
		End If
		'UPGRADE_NOTE: Object NodeAttribute may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		NodeAttribute = Nothing
		
	End Function
	Public Function GetHeaderLines() As Object
		Dim rElement As MSXML2.IXMLDOMElement
		Dim NodeAttribute As MSXML2.IXMLDOMNode
		
		NodeAttribute = oXMLMappingObject.selectSingleNode("//*/" & "General")
		If Not NodeAttribute Is Nothing Then
			GetHeaderLines = Val(NodeAttribute.Attributes.getNamedItem("HeaderLines").nodeValue)
		End If
		'UPGRADE_NOTE: Object NodeAttribute may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		NodeAttribute = Nothing
		
	End Function
	Public Function GetThousandSep() As Object
		Dim rElement As MSXML2.IXMLDOMElement
		Dim NodeAttribute As MSXML2.IXMLDOMNode
		
		'UPGRADE_WARNING: Couldn't resolve default property of object GetThousandSep. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetThousandSep = ""
		NodeAttribute = oXMLMappingObject.selectSingleNode("//*/General")
		If Not NodeAttribute Is Nothing Then
			'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object GetThousandSep. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			GetThousandSep = NodeAttribute.Attributes.getNamedItem("AmountThousandSep").nodeValue
		End If
		'UPGRADE_NOTE: Object NodeAttribute may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		NodeAttribute = Nothing
		
	End Function
	
	Public Function GetDecimalSep() As Object
		Dim rElement As MSXML2.IXMLDOMElement
		Dim NodeAttribute As MSXML2.IXMLDOMNode
		
		'UPGRADE_WARNING: Couldn't resolve default property of object GetDecimalSep. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetDecimalSep = ""
		NodeAttribute = oXMLMappingObject.selectSingleNode("//*/General")
		If Not NodeAttribute Is Nothing Then
			'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object GetDecimalSep. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			GetDecimalSep = NodeAttribute.Attributes.getNamedItem("AmountDecimalSep").nodeValue
		End If
		'UPGRADE_NOTE: Object NodeAttribute may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		NodeAttribute = Nothing
		
	End Function
	Public Function GetDateFormat() As Object
		Dim rElement As MSXML2.IXMLDOMElement
		Dim NodeAttribute As MSXML2.IXMLDOMNode
		
		NodeAttribute = oXMLMappingObject.selectSingleNode("//*/" & "General")
		If Not NodeAttribute Is Nothing Then
			GetDateFormat = Val(NodeAttribute.Attributes.getNamedItem("DateFormat").nodeValue)
		End If
		'UPGRADE_NOTE: Object NodeAttribute may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		NodeAttribute = Nothing
		
	End Function
	
	Public Function GetData(ByRef sLine As String, ByRef sVariable As String) As Object
		
		Dim rElement As MSXML2.IXMLDOMElement
		Dim NodeAttribute As MSXML2.IXMLDOMNode
        Dim iPos As Short
        Dim iPos2 As Integer  ' XokNET 17.08.2011 added iPos2
		Dim iLength As Short
		Dim sType As String
		Dim sValue As String
		Dim sFixed As String
		Dim nDateSeparator As String
		Dim iDatePosAdd As Short
		Dim sTmp As String
		
		'If Not bInitialized Then
		'    ' In case we have forgotton to call InitData
		'    InitializeMappings
		'
		'End If
		
		' Get date from Mappingfile
		NodeAttribute = oXMLMappingObject.selectSingleNode("//*/" & sVariable)
		If Not NodeAttribute Is Nothing Then
            ' XokNET 17.08.2011 - added the posibilty to combine two fields, lime surname and forename
            If InStr(NodeAttribute.attributes.getNamedItem("Pos").nodeValue, "+") > 0 Then
                iPos = Val(Left$(NodeAttribute.attributes.getNamedItem("Pos").nodeValue, InStr(NodeAttribute.attributes.getNamedItem("Pos").nodeValue, "+") - 1))
                iPos2 = Val(Mid$(NodeAttribute.attributes.getNamedItem("Pos").nodeValue, InStr(NodeAttribute.attributes.getNamedItem("Pos").nodeValue, "+") + 1))
            Else
                iPos = Val(NodeAttribute.attributes.getNamedItem("Pos").nodeValue)
                iPos2 = 0
            End If
            iLength = Val(NodeAttribute.attributes.getNamedItem("Length").nodeValue)
			If Not NodeAttribute.Attributes.getNamedItem("Type") Is Nothing Then
                sType = NodeAttribute.attributes.getNamedItem("Type").nodeValue
			End If
			If Not NodeAttribute.Attributes.getNamedItem("Fixed") Is Nothing Then
                sFixed = NodeAttribute.attributes.getNamedItem("Fixed").nodeValue
			End If
			
			
			' Find data
			If sFileType = "Delimited" Or sFileType = "Excel" Then
				If Not EmptyString(sFixed) Then
					' Fixed value for current field, use this value;
					sValue = sFixed
				Else
                    sValue = xDelim(sLine, sDelimiter, iPos, sSurroundChar)
                    ' XokNET 17.08.2011 - added the posibilty to combine two fields, lime surname and forename
                    If iPos2 > 0 Then
                        sValue = sValue & " " & xDelim(sLine, sDelimiter, iPos2, sSurroundChar)
                    End If
                End If
			ElseIf sFileType = "Fixed" Then 
				If Not EmptyString(sFixed) Then
					' Fixed value for current field, use this value;
					sValue = sFixed
				Else
                    ' XokNET 17.03.2011 added next If
                    If iPos > 0 And iLength > 0 Then
                        sValue = Mid$(sLine, iPos, iLength)
                    End If
                End If
			Else
				'error
			End If
			
			' Convert based on different formats
			If sVariable = "Amount" Then
				sValue = ConvertToAmount(sValue, sAmountThousandSep, sAmountDecimalSep)
				GetData = Val(sValue)
			ElseIf sVariable = "PaymentDate" Then 
                ' XokNET 17.03.2011 added next line and next If
                NodeAttribute = oXMLMappingObject.selectSingleNode("//*/General")
                'If Not NodeAttribute.attributes.getNamedItem("DateFormat") Is Nothing Then
                sFixed = NodeAttribute.attributes.getNamedItem("DateFormat").nodeValue
                'End If

                If sFixed = "Today" Then
                    ' XokNET 17.03.2011 changed next line
                    sTmp = DateToString(Date.Today)
                Else
                    ' return date as date
                    ' YEAR
                    If iYearLength = 4 Then
                        sTmp = Mid$(sValue, iYearStart, iYearLength)
                    Else
                        sTmp = "20" & Mid$(sValue, iYearStart, iYearLength)
                    End If
                    ' MONTH
                    sTmp = sTmp & Mid$(sValue, iMonthStart, iMonthLength)
                    ' DAY
                    sTmp = sTmp & Mid$(sValue, iDayStart, iDayLength)
                End If
                sValue = sTmp

                ' For BACS payments we must add days if date not "new" enough
                If iDateAddDays > 0 Then
                    If StringToDate(sValue) < GetBankday(Date.Today, sCountry, iDateAddDays) Then
                        sValue = DateToString(GetBankday(Date.Today, sCountry, iDateAddDays))
                    End If
                End If

                GetData = StringToDate(sValue)

            ElseIf sVariable = "Type" Then
                ' Paymenttype
                If sValue = sTypeFormatDomestic Then
                    sValue = "D" ' Domestic
                ElseIf sValue = sTypeFormatInternational Then
                    sValue = "I" ' International
                ElseIf sValue = sTypeFormatMass Then
                    sValue = "M" ' Mass transrer
                ElseIf sValue = sTypeFormatSalary Then
                    sValue = "S" ' Salary
                Else
                    ' XokNET 17.08.2011 -
                    ' Return what's set in file, and leave it to the mapping importfunction to handle it !
                    'sValue = "D"    ' Domestic

                End If
                GetData = sValue
            Else
                GetData = Trim(sValue)
            End If
		Else
            ' XNokET 05.04.2011 added next If
            If sVariable = "PaymentDate" And sDateFormat = "Today" Then
                sValue = DateToString(Date.Today)
                GetData = StringToDate(sValue)
            Else
                ' XNET 05.04.2011 changed next line
                'sValue = ""
                GetData = ""
            End If

		End If
        NodeAttribute = Nothing
		
	End Function
	Public Function PassHeaderline(ByRef sLine As String) As Object
		If Not EmptyString(sHeaderLine) Then
			' more than one headerline
			sHeaderLine = sHeaderLine & sDelimiter & sLine
		Else
			sHeaderLine = sLine
		End If
		
	End Function
	Public Function PassFooterline(ByRef sLine As String) As Object
		sFooterLine = sLine
	End Function
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		
		bInitialized = False
		iHeaderLines = 0
		iFooterLines = 0
        sLine = "" ' Current imported line
        sDelimiter = ";"
        sSurroundChar = ""
		sDateFormat = "YYMMDD"
		sAmountThousandSep = ""
		sAmountDecimalSep = ","
		sTypeFormatDomestic = ""
		sTypeFormatInternational = ""
		sTypeFormatSalary = ""
		sTypeFormatMass = ""
		sFileType = "Delimited" ' Delimited, Fixed, Excel
		
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	Public Sub Init(ByRef sMappingFilename As String)
		Dim NodeAttribute As MSXML2.IXMLDOMNode
		Dim iDatePosAdd As Short
		
		'''sMappingFilename = App.path & "\BabelMapping.xml"
		' >>>>>>>>>>>>>>>> WAIT - FIND THIS FROM PROFILE !!!!!!!  <<<<<<<<<<<<<<<<<<<<<<<<<<
		nLineNo = 0
		iDateAddDays = 0 ' Add f.ex. 2 days for BACS-payments
		sCountry = "" ' Ex. GB for BACS paymens in UK
		
		
		If Len(sMappingFilename) > 0 And Len(Dir(sMappingFilename)) > 0 Then
			oXMLMappingObject.Load(sMappingFilename)
			NodeAttribute = oXMLMappingObject.selectSingleNode("//*/General")
			
			iHeaderLines = Val(NodeAttribute.Attributes.getNamedItem("Headerlines").nodeValue)
			If Not NodeAttribute.Attributes.getNamedItem("Footerlines") Is Nothing Then
				iFooterLines = Val(NodeAttribute.Attributes.getNamedItem("Footerlines").nodeValue)
			End If
			'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sDelimiter = NodeAttribute.Attributes.getNamedItem("Delimiter").nodeValue
			'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sSurroundChar = NodeAttribute.Attributes.getNamedItem("Surround").nodeValue
			If Not NodeAttribute.Attributes.getNamedItem("DateFormat") Is Nothing Then
				'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sDateFormat = NodeAttribute.Attributes.getNamedItem("DateFormat").nodeValue
			End If
			If EmptyString(sDateFormat) Then
				sDateFormat = "DD/MM/YYYY"
			End If
			If Not NodeAttribute.Attributes.getNamedItem("DateAddDays") Is Nothing Then
				iDateAddDays = Val(NodeAttribute.Attributes.getNamedItem("DateAddDays").nodeValue)
			End If
			'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sAmountThousandSep = NodeAttribute.Attributes.getNamedItem("AmountThousandSep").nodeValue
			'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sAmountDecimalSep = NodeAttribute.Attributes.getNamedItem("AmountDecimalSep").nodeValue
			If EmptyString(sAmountDecimalSep) Then
				sAmountDecimalSep = "."
			End If
			If Not NodeAttribute.Attributes.getNamedItem("TypeFormatDomestic") Is Nothing Then
				'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sTypeFormatDomestic = NodeAttribute.Attributes.getNamedItem("TypeFormatDomestic").nodeValue
			End If
			If Not NodeAttribute.Attributes.getNamedItem("TypeFormatInternational") Is Nothing Then
				'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sTypeFormatInternational = NodeAttribute.Attributes.getNamedItem("TypeFormatInternational").nodeValue
			End If
			If Not NodeAttribute.Attributes.getNamedItem("TypeFormatSalary") Is Nothing Then
				'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sTypeFormatSalary = NodeAttribute.Attributes.getNamedItem("TypeFormatSalary").nodeValue
			End If
			If Not NodeAttribute.Attributes.getNamedItem("TypeFormatMass") Is Nothing Then
				'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sTypeFormatMass = NodeAttribute.Attributes.getNamedItem("TypeFormatMass").nodeValue
			End If
			'UPGRADE_WARNING: Couldn't resolve default property of object NodeAttribute.Attributes.getNamedItem().nodeValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			sFileType = NodeAttribute.Attributes.getNamedItem("FileType").nodeValue
		End If
		
		bInitialized = True
		
		' calculate datepositions based on sDateFormat from mappingfile
		' YYMMDD, YYYYMMDD, YY/MM/DD, YY.MM.DD, YY-MM-DD in mixed sequences
		
		' test for date-delimiter
		iDatePosAdd = 0
		If InStr(sDateFormat, "/") > 0 Then
			sDateDelim = "/"
			iDatePosAdd = 1
		ElseIf InStr(sDateFormat, ".") > 0 Then 
			sDateDelim = "."
			iDatePosAdd = 1
		ElseIf InStr(sDateFormat, "-") > 0 Then 
			sDateDelim = "-"
			iDatePosAdd = 1
		Else
			sDateDelim = ""
			iDatePosAdd = 0
		End If
		
		Select Case Replace(sDateFormat, sDateDelim, "")
			Case "YYYYMMDD"
				iYearStart = 1
				iYearLength = 4
				iMonthStart = 5 + iDatePosAdd
				iMonthLength = 2
				iDayStart = 7 + iDatePosAdd + iDatePosAdd
				iDayLength = 2
			Case "YYYYDDMM"
				iYearStart = 1
				iYearLength = 4
				iMonthStart = 7 + iDatePosAdd + iDatePosAdd
				iMonthLength = 2
				iDayStart = 5 + iDatePosAdd
				iDayLength = 2
			Case "DDMMYYYY"
				iYearStart = 5 + iDatePosAdd + iDatePosAdd
				iYearLength = 4
				iMonthStart = 3 + iDatePosAdd
				iMonthLength = 2
				iDayStart = 1
				iDayLength = 2
			Case "MMDDYYYY"
				iYearStart = 5 + iDatePosAdd + iDatePosAdd
				iYearLength = 4
				iMonthStart = 1
				iMonthLength = 2
				iDayStart = 3 + iDatePosAdd
				iDayLength = 2
			Case "YYMMDD"
				iYearStart = 1
				iYearLength = 2
				iMonthStart = 3 + iDatePosAdd
				iMonthLength = 2
				iDayStart = 5 + iDatePosAdd + iDatePosAdd
				iDayLength = 2
			Case "YYDDMM"
				iYearStart = 1
				iYearLength = 2
				iMonthStart = 5 + iDatePosAdd + iDatePosAdd
				iMonthLength = 2
				iDayStart = 3 + iDatePosAdd
				iDayLength = 2
			Case "DDMMYY"
				iYearStart = 5 + iDatePosAdd + iDatePosAdd
				iYearLength = 2
				iMonthStart = 3 + iDatePosAdd
				iMonthLength = 2
				iDayStart = 1
				iDayLength = 2
			Case "MMDDYY"
				iYearStart = 5 + iDatePosAdd + iDatePosAdd
				iYearLength = 2
				iMonthStart = 1
				iMonthLength = 2
				iDayStart = 3 + iDatePosAdd
				iDayLength = 2
		End Select
		
	End Sub
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Sub Class_Terminate_Renamed()
		' How to close the xmlobject?
		'UPGRADE_NOTE: Object oXMLMappingObject may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		oXMLMappingObject = Nothing
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
End Class
