﻿Imports System
Imports System.Security.Cryptography.X509Certificates
Imports System.Diagnostics

'Namespace CertWrapper
Public Class CertificateWrapper

    Private _cert As X509Certificate2

    Public ReadOnly Property Certificate() As X509Certificate2
        Get
            Return Me._cert
        End Get
    End Property

    Public Sub New()
        MyBase.New()

    End Sub

    Public Sub New(ByVal cert As X509Certificate2)
        MyBase.New()
        Me._cert = cert
    End Sub

    Public Overrides Function ToString() As String
        If (Me._cert Is Nothing) Then
            Return String.Empty
        Else
            Dim sSubject As String = Me._cert.Subject
            If (sSubject.Length > 70) Then
                sSubject = (sSubject.Substring(0, 67) + "...")
            End If

            Return String.Format("{0} [Expires: {1}]", sSubject, Me._cert.GetExpirationDateString)
        End If

    End Function

    Public ReadOnly Property SerialNumber() As String
        Get
            If (Me._cert Is Nothing) Then
                Return "0"
            Else
                Return Me._cert.SerialNumber
            End If

        End Get
    End Property
End Class
Class ChkCert

    Public CtrW As CertificateWrapper

    Public Sub New()
        MyBase.New()
        Me.Certificate()
    End Sub

    Private Sub Certificate()
        ' Open Certificate Store
        Dim xs As X509Store = New X509Store(StoreLocation.LocalMachine)
        xs.Open((OpenFlags.OpenExistingOnly Or OpenFlags.ReadOnly))
        ' Search by Issuer
        Dim xcc As X509Certificate2Collection = CType(xs.Certificates.Find(X509FindType.FindByIssuerName, "XLEDGER.NET", True), X509Certificate2Collection)
        If (xcc.Count = 0) Then
            xs = New X509Store(StoreLocation.CurrentUser)
            xs.Open((OpenFlags.OpenExistingOnly Or OpenFlags.ReadOnly))
            ' Search by Issuer
            xcc = CType(xs.Certificates.Find(X509FindType.FindByIssuerName, "XLEDGER.NET", True), X509Certificate2Collection)
        End If

        xs.Close()
        Try
            Me.CtrW = New CertificateWrapper(xcc(0))
        Catch e As Exception

        End Try

    End Sub
End Class
'End Namespace


