Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("ImportedLine_NET.ImportedLine")> Public Class ImportedLine
	
	Private nIndex As Double
	Dim iQualifier As Short
	Dim sText As String
	
	'Public Key As String - 05.08.2008 Removed this. Probably not in use
	
	'********* START PROPERTY SETTINGS ***********************
	Public Property Index() As Double
		Get
			Index = nIndex
		End Get
		Set(ByVal Value As Double)
			nIndex = Value
		End Set
	End Property
	Public Property Text() As String
		Get
			Text = sText
		End Get
		Set(ByVal Value As String)
			sText = Value
		End Set
	End Property
	Public Property Qualifier() As Short
		Get
			Qualifier = iQualifier
		End Get
		Set(ByVal Value As Short)
			' 1 - Insert lines before rest of collection
			' 9 - Insert lines after rest of collection have been exported
			iQualifier = Value
		End Set
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		iQualifier = 1
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
End Class
