<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_Sub_952_AutoMatch
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(rp_Sub_952_AutoMatch))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.lblType = New DataDynamics.ActiveReports.Label
        Me.txtTypeCount = New DataDynamics.ActiveReports.TextBox
        Me.txtTypePercent = New DataDynamics.ActiveReports.TextBox
        Me.txtTypeAmount = New DataDynamics.ActiveReports.TextBox
        Me.txtTypeProposed = New DataDynamics.ActiveReports.TextBox
        Me.txtTypeFinal = New DataDynamics.ActiveReports.TextBox
        Me.txtTypePercentInv = New DataDynamics.ActiveReports.TextBox
        Me.shapegrAccumulatedHeader = New DataDynamics.ActiveReports.Shape
        CType(Me.lblType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTypeCount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTypePercent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTypeAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTypeProposed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTypeFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTypePercentInv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.CanShrink = True
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.shapegrAccumulatedHeader, Me.lblType, Me.txtTypeCount, Me.txtTypePercent, Me.txtTypeAmount, Me.txtTypeProposed, Me.txtTypeFinal, Me.txtTypePercentInv})
        Me.Detail.Height = 0.188!
        Me.Detail.Name = "Detail"
        '
        'lblType
        '
        Me.lblType.Border.BottomColor = System.Drawing.Color.Black
        Me.lblType.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblType.Border.LeftColor = System.Drawing.Color.Black
        Me.lblType.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblType.Border.RightColor = System.Drawing.Color.Black
        Me.lblType.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblType.Border.TopColor = System.Drawing.Color.Black
        Me.lblType.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblType.Height = 0.15!
        Me.lblType.HyperLink = Nothing
        Me.lblType.Left = 1.1875!
        Me.lblType.Name = "lblType"
        Me.lblType.Style = "ddo-char-set: 0; font-size: 8.25pt; "
        Me.lblType.Text = "lblType"
        Me.lblType.Top = 0.0!
        Me.lblType.Width = 2.5!
        '
        'txtTypeCount
        '
        Me.txtTypeCount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTypeCount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeCount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTypeCount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeCount.Border.RightColor = System.Drawing.Color.Black
        Me.txtTypeCount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeCount.Border.TopColor = System.Drawing.Color.Black
        Me.txtTypeCount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeCount.CanShrink = True
        Me.txtTypeCount.DataField = "TypeCount"
        Me.txtTypeCount.Height = 0.15!
        Me.txtTypeCount.Left = 4.0625!
        Me.txtTypeCount.Name = "txtTypeCount"
        Me.txtTypeCount.OutputFormat = resources.GetString("txtTypeCount.OutputFormat")
        Me.txtTypeCount.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; "
        Me.txtTypeCount.Text = "txtTypeCount"
        Me.txtTypeCount.Top = 0.0!
        Me.txtTypeCount.Width = 1.0!
        '
        'txtTypePercent
        '
        Me.txtTypePercent.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTypePercent.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypePercent.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTypePercent.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypePercent.Border.RightColor = System.Drawing.Color.Black
        Me.txtTypePercent.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypePercent.Border.TopColor = System.Drawing.Color.Black
        Me.txtTypePercent.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypePercent.CanShrink = True
        Me.txtTypePercent.Height = 0.15!
        Me.txtTypePercent.Left = 5.0625!
        Me.txtTypePercent.Name = "txtTypePercent"
        Me.txtTypePercent.OutputFormat = resources.GetString("txtTypePercent.OutputFormat")
        Me.txtTypePercent.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; "
        Me.txtTypePercent.Text = "txtTypePercent"
        Me.txtTypePercent.Top = 0.0!
        Me.txtTypePercent.Width = 0.7!
        '
        'txtTypeAmount
        '
        Me.txtTypeAmount.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTypeAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeAmount.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTypeAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeAmount.Border.RightColor = System.Drawing.Color.Black
        Me.txtTypeAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeAmount.Border.TopColor = System.Drawing.Color.Black
        Me.txtTypeAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeAmount.CanShrink = True
        Me.txtTypeAmount.DataField = "TypeAmount"
        Me.txtTypeAmount.Height = 0.15!
        Me.txtTypeAmount.Left = 5.75!
        Me.txtTypeAmount.Name = "txtTypeAmount"
        Me.txtTypeAmount.OutputFormat = resources.GetString("txtTypeAmount.OutputFormat")
        Me.txtTypeAmount.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; "
        Me.txtTypeAmount.Text = "txtTypeAmount"
        Me.txtTypeAmount.Top = 0.0!
        Me.txtTypeAmount.Width = 1.0!
        '
        'txtTypeProposed
        '
        Me.txtTypeProposed.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTypeProposed.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeProposed.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTypeProposed.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeProposed.Border.RightColor = System.Drawing.Color.Black
        Me.txtTypeProposed.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeProposed.Border.TopColor = System.Drawing.Color.Black
        Me.txtTypeProposed.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeProposed.CanShrink = True
        Me.txtTypeProposed.DataField = "TypeProposed"
        Me.txtTypeProposed.Height = 0.15!
        Me.txtTypeProposed.Left = 7.0625!
        Me.txtTypeProposed.Name = "txtTypeProposed"
        Me.txtTypeProposed.OutputFormat = resources.GetString("txtTypeProposed.OutputFormat")
        Me.txtTypeProposed.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; "
        Me.txtTypeProposed.Text = "txtTypeProposed"
        Me.txtTypeProposed.Top = 0.0!
        Me.txtTypeProposed.Width = 0.7!
        '
        'txtTypeFinal
        '
        Me.txtTypeFinal.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTypeFinal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeFinal.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTypeFinal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeFinal.Border.RightColor = System.Drawing.Color.Black
        Me.txtTypeFinal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeFinal.Border.TopColor = System.Drawing.Color.Black
        Me.txtTypeFinal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypeFinal.CanShrink = True
        Me.txtTypeFinal.DataField = "TypeFinal"
        Me.txtTypeFinal.Height = 0.15!
        Me.txtTypeFinal.Left = 7.75!
        Me.txtTypeFinal.Name = "txtTypeFinal"
        Me.txtTypeFinal.OutputFormat = resources.GetString("txtTypeFinal.OutputFormat")
        Me.txtTypeFinal.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; "
        Me.txtTypeFinal.Text = "txtTypeFinal"
        Me.txtTypeFinal.Top = 0.0!
        Me.txtTypeFinal.Width = 0.7!
        '
        'txtTypePercentInv
        '
        Me.txtTypePercentInv.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTypePercentInv.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypePercentInv.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTypePercentInv.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypePercentInv.Border.RightColor = System.Drawing.Color.Black
        Me.txtTypePercentInv.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypePercentInv.Border.TopColor = System.Drawing.Color.Black
        Me.txtTypePercentInv.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTypePercentInv.CanShrink = True
        Me.txtTypePercentInv.Height = 0.15!
        Me.txtTypePercentInv.Left = 8.4375!
        Me.txtTypePercentInv.Name = "txtTypePercentInv"
        Me.txtTypePercentInv.OutputFormat = resources.GetString("txtTypePercentInv.OutputFormat")
        Me.txtTypePercentInv.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; "
        Me.txtTypePercentInv.Text = "txtTypePercentInv"
        Me.txtTypePercentInv.Top = 0.0!
        Me.txtTypePercentInv.Width = 0.7!
        '
        'shapegrAccumulatedHeader
        '
        Me.shapegrAccumulatedHeader.BackColor = System.Drawing.Color.LightGray
        Me.shapegrAccumulatedHeader.Border.BottomColor = System.Drawing.Color.Black
        Me.shapegrAccumulatedHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulatedHeader.Border.LeftColor = System.Drawing.Color.Black
        Me.shapegrAccumulatedHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulatedHeader.Border.RightColor = System.Drawing.Color.Black
        Me.shapegrAccumulatedHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulatedHeader.Border.TopColor = System.Drawing.Color.Black
        Me.shapegrAccumulatedHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.shapegrAccumulatedHeader.Height = 0.268!
        Me.shapegrAccumulatedHeader.Left = 7.0!
        Me.shapegrAccumulatedHeader.LineStyle = DataDynamics.ActiveReports.LineStyle.Transparent
        Me.shapegrAccumulatedHeader.Name = "shapegrAccumulatedHeader"
        Me.shapegrAccumulatedHeader.RoundingRadius = 9.999999!
        Me.shapegrAccumulatedHeader.Top = -0.07!
        Me.shapegrAccumulatedHeader.Width = 2.5!
        '
        'rp_Sub_952_AutoMatch
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 9.541667!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black; ", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic; ", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ", "Heading3", "Normal"))
        CType(Me.lblType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTypeCount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTypePercent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTypeAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTypeProposed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTypeFinal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTypePercentInv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents lblType As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTypeCount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTypePercent As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTypeAmount As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTypeProposed As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTypeFinal As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTypePercentInv As DataDynamics.ActiveReports.TextBox
    Friend WithEvents shapegrAccumulatedHeader As DataDynamics.ActiveReports.Shape
End Class
