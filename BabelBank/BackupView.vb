Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("BackupView_NET.BackupView")> Public Class BackupView
	' Show files in backup-path
	' User may View, Copy to Original, Send supportmail with file, or cancel
	
	Private sBackUpPath As String
	Private oProfile As vbBabel.Profile
	Private iReturn As Short
	Private sPickedFile As String
    Private eImportFormat As vbBabel.BabelFiles.FileType
    Private aFileArray(,) As Object
    Private iElementNo As String
    Private sMode As String

    '********* START PROPERTY SETTINGS ***********************
	Public WriteOnly Property BackupPath() As String
		Set(ByVal Value As String)
			sBackUpPath = Value
		End Set
    End Property
    Public WriteOnly Property SetMode() As String
        Set(ByVal Value As String)
            sMode = Value
        End Set
    End Property
	Public WriteOnly Property Profile() As vbBabel.Profile
		Set(ByVal Value As vbBabel.Profile)
			oProfile = Value
		End Set
	End Property
	Public ReadOnly Property ReturnValue() As Short
		Get
			ReturnValue = iReturn
		End Get
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
    Public Sub Class_Terminate_Renamed()
    End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
	Public Sub Show()
		' Show spread with backupfiles
		' - Filename (original)
		' - Klient, hvis dette benyttes
		' - Date (for process)
		' - Profilens navn
		' - Type fil
		' - Fil.ext/filformat
		' - Filst�rrelse
		
        Dim frmBackupView As New frmBackupView
        Dim sTempname As String
		Dim oFs As Scripting.FileSystemObject
		Dim iCount As Short
		Dim i As Short
		Dim iLastPos, iStartPos As Short
		Dim sErrString As String
		Dim iFilesetupID As Short
		Dim sFullPathName As String
		Dim sPathName As String
		Dim oClient As Client
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim sTmp As String

		On Error GoTo ShowError
		sErrString = "BackupView"
		
        oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
		' Find files;
		' Filenameexample;
        ' 20030218_1455_03IA1_003_filname.txt
        If sMode = "DATABASE" Then
            sTempname = Dir(sBackUpPath & "\xProfile*.mdb", FileAttribute.Normal) ' Retrieve the first entry.
        Else
            sTempname = Dir(sBackUpPath & "\*.*", FileAttribute.Normal) ' Retrieve the first entry.
        End If
        sErrString = "BackupView, Dir()"
        iCount = 0
        Do While sTempname <> "" ' Start the loop.


            ' show xprofiles
            If sMode = "DATABASE" Then

                If Left(sTempname.ToUpper, 8) = "XPROFILE" Then
                    ReDim Preserve aFileArray(9, iCount)

                    sErrString = "BackupView, Filename"
                    aFileArray(0, iCount) = sTempname
                    ' Fill in clientno
                    aFileArray(1, iCount) = ""
                End If
                ' Date of filename;
                ' YYYYMMDD from start of filename
                sErrString = "BackupView, Date"
                aFileArray(2, iCount) = Format(System.IO.File.GetLastWriteTime(sBackUpPath & "\" & sTempname), "yyyyMMdd-HH:mm:ss")
                ' vurder om klokke skal v�re med, forel�pig ikke
                'aFileArray(2, iCount) = Format(System.IO.File.GetLastWriteTime(sBackUpPath & "\" & sTempname), "yyyyMMdd")

                ' Put filesetupid in element 9, not relevant
                aFileArray(9, iCount) = ""

                ' Type of file;
                aFileArray(4, iCount) = "BB Database"

                ' FilenameNo (1, 2 or 3) - not relevant
                aFileArray(8, iCount) = ""

                ' Filextension/format
                sErrString = "BackupView, Fileformat"
                aFileArray(5, iCount) = ".mdb"

                ' Filesize:
                sErrString = "BackupView, Filesize"
                aFileArray(6, iCount) = Int(FileLen(sBackUpPath & "\" & sTempname) / 1024) + 1
                aFileArray(7, iCount) = sBackUpPath & "\" & sTempname ' Full filename, including path

                sTempname = Dir() ' Get next entry.
                sErrString = "BackupView, Dir()"
                iCount = iCount + 1


            Else
                ' "normal" backup
                ' Must have 4 _ in filename;
                If Len(sTempname) - Len(Replace(sTempname, "_", "")) >= 4 Then
                    ReDim Preserve aFileArray(9, iCount)

                    sErrString = "BackupView, Filename"
                    ' Display filename without leading date and systeminfo;
                    aFileArray(0, iCount) = Mid(sTempname, InStrRev(sTempname, "_") + 1)
                    ' ClientID, if present;
                    If InStr(sTempname, "__") = 0 Then
                        ' If no Clientname, filename has two _ before original filename
                        ' Clientname is held between third _ and fourth _ '
                        sErrString = "BackupView, Client"
                        iStartPos = 0
                        For i = 1 To 3
                            iStartPos = InStr(iStartPos + 1, sTempname, "_") + 1
                        Next i
                        iLastPos = InStr(iStartPos, sTempname, "_") - 1
                        ' Fill in clientno
                        aFileArray(1, iCount) = Mid(sTempname, iStartPos, iLastPos - iStartPos + 1)
                        ' Filename after Clientno;

                        ' Display filename without leading date and systeminfo;
                        'aFileArray(0, iCount) = Mid$(sTempname, InStrRev(sTempname, "_") + 1)
                        aFileArray(0, iCount) = Mid(sTempname, iLastPos + 2)
                    Else
                        aFileArray(1, iCount) = ""
                        ' Filename after __
                        aFileArray(0, iCount) = Mid(sTempname, InStrRev(sTempname, "__") + 2)
                    End If
                    ' Datepart of filename;
                    ' YYYYMMDD from start of filename
                    sErrString = "BackupView, Date"
                    'aFileArray(2, iCount) = DateSerial(Left$(sTempname, 4), Mid$(sTempname, 5, 2), Mid$(sTempname, 7, 2))
                    'aFileArray(2, iCount) = VB6.Format(DateSerial(CInt(Left(sTempname, 4)), CInt(Mid(sTempname, 5, 2)), CInt(Mid(sTempname, 7, 2))), "Short date")
                    aFileArray(2, iCount) = Left$(sTempname, 8)

                    ' Profilename;
                    ' After second _ '
                    iStartPos = 0
                    For i = 1 To 2
                        iStartPos = InStr(iStartPos + 1, sTempname, "_")
                    Next i
                    sErrString = "BackupView, FileSetup"
                    If oProfile.FileSetups.Count >= Val(Mid(sTempname, iStartPos + 1, 2)) Then
                        If Not oProfile.FileSetups(Val(Mid(sTempname, iStartPos + 1, 2))) Is Nothing Then
                            aFileArray(3, iCount) = oProfile.FileSetups(Val(Mid(sTempname, iStartPos + 1, 2))).ShortName
                        End If
                    End If
                    ' Put filesetupid in element 9
                    aFileArray(9, iCount) = Val(Mid(sTempname, iStartPos + 1, 2))

                    ' Type of file;
                    aFileArray(4, iCount) = Mid(sTempname, iStartPos + 3, 2)

                    ' FilenameNo (1, 2 or 3)
                    aFileArray(8, iCount) = Mid(sTempname, iStartPos + 5, 1)

                    ' Filextension/format
                    sErrString = "BackupView, Fileformat"
                    aFileArray(5, iCount) = DetectFileFormat(sBackUpPath & "\" & sTempname, False)

                    ' Filesize:
                    sErrString = "BackupView, Filesize"
                    aFileArray(6, iCount) = Int(FileLen(sBackUpPath & "\" & sTempname) / 1024) + 1
                    aFileArray(7, iCount) = sBackUpPath & "\" & sTempname ' Full filename, including path

                    sTempname = Dir() ' Get next entry.
                    sErrString = "BackupView, Dir()"
                    iCount = iCount + 1
                Else
                    ' Not a BabelBank backupfile!
                    sTempname = Dir() ' Get next entry.
                End If
            End If

        Loop

        If iCount > 0 Then
            '-----------------------------------------------
            ' Fill up grid-browser with Fileinfo
            '-----------------------------------------------


            'TODO - RAPPORT/SPREADSTUFF
            sErrString = "BackupView, grid"

            With frmBackupView.gridFiles
                .ScrollBars = ScrollBars.Vertical
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.None
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                '.AutoSize = True
                .ReadOnly = True
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect


                '.RowsDefaultCellStyle.SelectionForeColor = Color.Black
                '.AutoSize = True
                .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
                .AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells



                '	' Col headings and widths:
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.HeaderText = LRS(60061) 'Filename
                txtColumn.Width = WidthFromSpreadToGrid(15)
                .Columns.Add(txtColumn)

                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.HeaderText = LRS(60107) 'Client"
                txtColumn.Width = WidthFromSpreadToGrid(3)
                .Columns.Add(txtColumn)

                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.HeaderText = LRS(60167) 'Prod.Date"
                txtColumn.Width = WidthFromSpreadToGrid(5)
                txtColumn.ValueType = GetType(System.DateTime)
                txtColumn.DefaultCellStyle.Format = "d"
                .Columns.Add(txtColumn)


                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.HeaderText = LRS(60108) 'Profile"
                txtColumn.Width = WidthFromSpreadToGrid(10)
                .Columns.Add(txtColumn)

                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.HeaderText = LRS(60109) 'Type of file"
                txtColumn.Width = WidthFromSpreadToGrid(6)
                .Columns.Add(txtColumn)

                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.HeaderText = LRS(60110) 'Fileformat
                txtColumn.Width = WidthFromSpreadToGrid(8)
                .Columns.Add(txtColumn)

                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.HeaderText = LRS(60111) 'Size"
                txtColumn.Width = WidthFromSpreadToGrid(6)
                .Columns.Add(txtColumn)

                ' Hidden column, with arrayelementNo, in case we sort
                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                i = 0
                For i = 0 To UBound(aFileArray, 2)

                    .Rows.Add()

                    .Rows(i).Cells(0).Value = aFileArray(0, i) 'Filename
                    .Rows(i).Cells(1).Value = aFileArray(1, i) 'Client"
                    '.Rows(i).Cells(2).Value = aFileArray(2, i) 'Date"
                    If sMode = "DATABASE" Then
                        .Rows(i).Cells(2).Value = StringToDate(Left(aFileArray(2, i), 8))  'Date
                        .Rows(i).Cells(3).Value = Right(aFileArray(2, i), 8)  'Time for database
                    Else
                        .Rows(i).Cells(2).Value = StringToDate(aFileArray(2, i))  'Date
                        .Rows(i).Cells(3).Value = aFileArray(3, i) 'Profile"
                    End If

                    Select Case aFileArray(4, i)
                        Case "IA" ' Imported from accountingsystem
                            sTmp = LRS(60112) ' From Accounting
                        Case "IB" ' Imported from bank
                            sTmp = LRS(60113) ' From Bank
                        Case "OA" ' Exported to accountingsystem from BabelBank
                            sTmp = LRS(60114) ' To Accounting
                        Case "OB" ' Exported to bank from BabelBank
                            sTmp = LRS(60115) ' To Bank
                        Case "BB Database"  'BabelBank database
                            sTmp = "BabelBank database"
                        Case Else
                            'sErrString = "BackupView, Type of File"
                            'Err.Raise(Err.Number, "vbBabel.BackupView", sErrString)
                            sTmp = "Unkonown"
                    End Select
                    .Rows(i).Cells(4).Value = sTmp
                    If sMode = "DATABASE" Then
                        .Rows(i).Cells(5).Value = "Access database"
                    Else
                        .Rows(i).Cells(5).Value = GetEnumFileType(aFileArray(5, i)) 'Fileformat
                    End If
                    If sMode <> "DATABASE" Then
                        If GetEnumFileType(aFileArray(5, i)) = "Ukjent filtype" Then
                            ' put in extension as filetype
                            .Rows(i).Cells(5).Value = oFs.GetExtensionName(sBackUpPath & "\" & aFileArray(0, i)) & "-File"
                        End If
                    End If
                    .Rows(i).Cells(6).Value = Trim(Str(aFileArray(6, i))) & " kB" 'Size"
                    .Rows(i).Cells(7).Value = i.ToString

                Next i

                oFs = Nothing
                .AutoResizeColumnHeadersHeight()
                .Refresh()

            End With

            frmBackupView.lblBackupPath.Text = LRS(60117) & " " & sBackUpPath
            frmBackupView.SetMode(sMode)
            '------------------------------------------------
            frmBackupView.gridFiles.Focus()
            frmBackupView.SendFilesArray((aFileArray))

            If sMode = "DATABASE" Then
                frmBackupView.cmdView.Visible = False  ' deaactivate view-button for database restore
            End If
            ' sort on date, desc
            'frmBackupView.gridFiles.Sort(frmBackupView.gridFiles.Columns(2), System.ComponentModel.ListSortDirection.Descending)
            frmBackupView.ShowDialog()

            '-------------- Find returnvalues from selected file --------

            Do
                iReturn = frmBackupView.ifrmReturn
                Select Case iReturn
                    Case 1  '1 = Copy to original 

                        ' Copy file from backup, to original path, with original name
                        ' NB! This will be wrong when sorting is possible!
                        For Each objrow As DataGridViewRow In frmBackupView.gridFiles.SelectedRows
                            ' Find elementNo in col 7 (will work also if we sort!)
                            iElementNo = Val(objrow.Cells(7).Value)

                            sPickedFile = aFileArray(7, CInt(iElementNo))
                            If sMode = "DATABASE" Then
                                eImportFormat = 0
                                iFilesetupID = 0
                            Else
                                eImportFormat = aFileArray(5, CInt(iElementNo))
                                iFilesetupID = aFileArray(9, CInt(iElementNo))
                                If oProfile.FileSetups.Count >= iFilesetupID Then
                                    If Not oProfile.FileSetups(iFilesetupID) Is Nothing Then
                                        If Left(aFileArray(4, CInt(iElementNo)), 1) = "I" Then
                                            ' Incoming file
                                            If aFileArray(8, CInt(iElementNo)) = 1 Then
                                                sFullPathName = oProfile.FileSetups(iFilesetupID).FileNameIn1
                                            ElseIf aFileArray(8, CInt(iElementNo)) = 2 Then
                                                sFullPathName = oProfile.FileSetups(iFilesetupID).FileNameIn2
                                            Else
                                                sFullPathName = oProfile.FileSetups(iFilesetupID).FileNameIn3
                                            End If
                                        Else
                                            ' Outgoing file
                                            If aFileArray(8, CInt(iElementNo)) = 1 Then
                                                sFullPathName = oProfile.FileSetups(iFilesetupID).FileNameOut1
                                            ElseIf aFileArray(8, CInt(iElementNo)) = 2 Then
                                                sFullPathName = oProfile.FileSetups(iFilesetupID).FileNameOut2
                                            Else
                                                sFullPathName = oProfile.FileSetups(iFilesetupID).FileNameOut3
                                            End If
                                        End If
                                    End If
                                End If

                                ' Clients involved?
                                If aFileArray(1, CInt(iElementNo)) <> "" Then
                                    ' No problem if klientid in filename.
                                    ' If clientid in pathname, must find this
                                    sPathName = Left(sFullPathName, InStrRev(sFullPathName, "\"))
                                    If InStr(sPathName, "�") > 0 Then
                                        ' Swap $ with clientno
                                        ' Lets hope there are as many $ as length of clientno!
                                        sPathName = Replace(sPathName, New String("�", Len(Trim(aFileArray(1, CInt(iElementNo))))), Trim(aFileArray(1, CInt(iElementNo))))
                                        ' Check if all $ are replaced:
                                        If InStr(sPathName, "�") > 0 Then
                                            sErrString = LRS(10018) ' 10018: Preparing copy; Something wrong with clientid
                                            Err.Raise(10018, , sErrString)
                                        End If
                                    ElseIf InStr(sPathName, "�") > 0 Then
                                        ' Clientname as part of path, replace!
                                        ' we know clientno, find clientname from clientpart of filesetup;
                                        For Each oClient In oProfile.FileSetups(iFilesetupID).Clients
                                            If oClient.ClientNo = aFileArray(1, CInt(iElementNo)) Then
                                                sFullPathName = Replace(sFullPathName, "�", oClient.Name)
                                                Exit For
                                            End If
                                        Next oClient
                                    End If
                                Else
                                    ' No clients, find path-part of original filename;
                                    sPathName = Left(sFullPathName, InStrRev(sFullPathName, "\"))
                                End If
                            End If  ' If sMode = "DATABASE"
                            ' for database restore, copy back to databasepath;
                            If sMode = "DATABASE" Then
                                ' Sp�r om du virkelig vil dette !!!
                                'If MsgBox("Er du helt sikker p� at du vil kopiere tilbake database?" & vbCrLf & "Du fjerner da alt som ligger i databasen du n� kj�rer, og erstatter dette med oppsett fra valgt databasekopi.", MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, "BabelBank") = MsgBoxResult.Yes Then
                                If MsgBox(LRS(60216) & vbCrLf & LRS(60217), MsgBoxStyle.YesNo + MsgBoxStyle.Exclamation, "BabelBank") = MsgBoxResult.Yes Then
                                    FileCopy(sPickedFile, BB_DatabasePath())  ' overskrive OK? ja, det virker som om det g�r bra ! ihvertfall i utviklingsmodus
                                    iReturn = 1
                                    Exit Do
                                Else
                                    iReturn = 0
                                End If
                            ElseIf sMode = "EMAIL" Then
                                ' email all files, outside of loop
                            Else
                                ' OK, ready to copy
                                FileCopy(sPickedFile, sPathName & aFileArray(0, CInt(iElementNo)))
                                MsgBox(LRS(60168, sPathName & aFileArray(0, CInt(iElementNo))), MsgBoxStyle.OkOnly)
                            End If
                        Next
                        frmBackupView.ShowDialog() ' added 10.12.2010, to show form again
                    Case 3 ' Cancel
                        ' no action
                        iReturn = 0
                        Exit Do
                    Case 4 ' eMail
                        For Each objrow As DataGridViewRow In frmBackupView.gridFiles.SelectedRows
                            ' Find elementNo in col 7 (will work also if we sort!)
                            iElementNo = Val(objrow.Cells(7).Value)
                            ' put TRUE into selected files
                            aFileArray(0, iElementNo) = "TRUE"
                        Next
                        eMailBackup(aFileArray)
                        iReturn = 1

                End Select
                frmBackupView.ShowDialog()
            Loop
        Else
            ' No files
            iReturn = -1
        End If
        frmBackupView.Close()
        frmBackupView = Nothing

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        Exit Sub

ShowError:
        ' Errorhandler for Showmethod class BackupView
        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        iReturn = False
        Err.Raise(Err.Number, , sErrString & vbCrLf & Err.Description)
		
    End Sub
    Public Sub eMailBackup(Optional ByVal aFilesArray(,) As Object = Nothing)
        '----------------------------------------------
        ' sends an e-mail to Visual Banking
        ' with attachments from backupfolder
        '----------------------------------------------

        'send email

        Dim myMail As New vbBabel.vbMail
        Dim sCompany As String ' Companyname, who needs support ?
        Dim sMsgNote As String
        Dim sErrDescription As String
        Dim iContinue As Integer
        Dim i As Integer
        ' Find info about eMailsetup in database
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sCompanyID As String = ""

        Try
            sCompanyID = "1"  ' TODO - Hardcoded CompanyID
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If
            sMySQL = "SELECT * FROM Company "
            sMySQL = sMySQL & "WHERE Company_ID = " & sCompanyID
            oMyDal.SQL = sMySQL

            If oMyDal.Reader_Execute() Then

                While oMyDal.Reader_ReadRecord
                    sCompany = oMyDal.Reader_GetString("Name")

                    If oMyDal.Reader_GetString("EmailSMTP") Then
                        myMail.EMail_SMTP = True
                    Else
                        myMail.EMail_SMTP = False  ' MAPI
                        iContinue = 1
                    End If
                    sMsgNote = sMsgNote & oMyDal.Reader_GetString("Name") & vbCrLf
                    sMsgNote = sMsgNote & vbCrLf & vbCrLf & "Backupfiles from BabelBank"

                    ' added 04.02.2011 - find systeminfo and add to msg
                    sMsgNote = sMsgNote & vbCrLf & vbCrLf
                    sMsgNote = sMsgNote & "_____________________________________________________________________" & vbCrLf
                    sMsgNote = sMsgNote & "Systeminfo:" & vbCrLf
                    sMsgNote = sMsgNote & "Programfolder: " & Application.StartupPath & vbCrLf
                    sMsgNote = sMsgNote & "Programversion: " & Application.ProductVersion & vbCrLf
                    sMsgNote = sMsgNote & "Database: " & BB_DatabasePath() & vbCrLf
                    sMsgNote = sMsgNote & "License: " & BB_LicensePath() & vbCrLf & vbCrLf

                    sMsgNote = sMsgNote & "Windows version: " & My.Computer.Info.OSFullName & vbCrLf
                    sMsgNote = sMsgNote & "Windows version number: " & My.Computer.Info.OSVersion & vbCrLf
                    sMsgNote = sMsgNote & "Windows platform ID " & My.Computer.Info.OSPlatform & vbCrLf & vbCrLf

                    If iContinue = 1 Then
                        ' Use new class/dll vbSendmail
                        '-----------------------------
                        If oMyDal.Reader_GetString("EmailSMTP") And Len(Trim(oMyDal.Reader_GetString("eMailSMTPHost"))) > 0 Then
                            myMail.EMail_SMTP = True
                            myMail.eMailSMTPHost = oMyDal.Reader_GetString("eMailSMTPHost")
                        End If
                        myMail.eMailSender = oMyDal.Reader_GetString("eMailSender")
                        myMail.eMailSenderDisplayName = oMyDal.Reader_GetString("eMailDisplayName")
                        If Len(oMyDal.Reader_GetString("EMailSupport")) > 0 Then
                            myMail.AddReceivers(Trim(oMyDal.Reader_GetString("EMailSupport")), oMyDal.Reader_GetString("eMailAutoDisplayName"))
                        Else
                            ' Defaults to support@visualbanking.net
                            myMail.AddReceivers("support@visualbanking.net", oMyDal.Reader_GetString("eMailAutoDisplayName"))
                        End If

                        myMail.eMailReplyTo = oMyDal.Reader_GetString("eMailReplyAdress")
                        myMail.Subject = "Backupfiles BabelBank " & sCompany
                        myMail.AddAttachment(BB_DatabasePath() & "_copy")
                        myMail.AddAttachment(BB_LicensePath())

                        If Not Array_IsEmpty(aFilesArray) Then
                            ' Attach files who are imported;
                            For i = 0 To UBound(aFilesArray, 2)
                                If aFilesArray(0, i) = "TRUE" Then
                                    myMail.AddAttachment(aFilesArray(7, i))  ' full path in 7
                                End If
                            Next i
                        End If
                        myMail.Body = sMsgNote
                        myMail.Send()

                        If myMail.Success Then
                            MsgBox(LRS(60224), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                        End If
                        myMail = Nothing
                    End If
                End While
            End If

            Exit Sub

        Catch
            myMail = Nothing
            sErrDescription = Err.Description
            Err.Raise(15011, , sErrDescription)

            Exit Sub
        End Try
    End Sub
End Class
