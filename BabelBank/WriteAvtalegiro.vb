﻿Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module WriteAvtalegiro
    Dim sLine As String ' en output-linje
    Dim i As Double
    Dim oBatch As Batch
    Dim nBatchSumAmount As Double, nBatchNoRecords As Double, nBatchNoTransactions As Double
    Dim dBatchLastDate As Date, dBatchFirstDate As Date
    Dim nFileSumAmount As Double, nFileNoRecords As Double, nFileNoTransactions As Double
    Function WriteAvtalegiroFile(ByVal oBabelFiles As BabelFiles, ByVal bMultiFiles As Boolean, ByVal sFilenameOut As String, ByVal iFormat_ID As Integer, ByVal sI_Account As String, ByVal sAdditionalID As String, ByVal sClientNo As String) As Object
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String ' en output-linje
        Dim i As Double, j As Double
        Dim oBabel As BabelFile
        Dim oBatch As Batch
        Dim oPayment As Payment
        Dim oInvoice As Invoice
        Dim oFreetext As Freetext
        Dim nTransactionNo As Double
        Dim bExportoBabel As Boolean, bExportoBatch As Boolean, bExportoPayment As Boolean
        Dim nExistingLines As Double, sTempFilename As String, iCount As Integer
        Dim bFoundRecord89 As Boolean, oFileWrite As Scripting.TextStream, oFileRead As Scripting.TextStream
        Dim bAppendFile As Boolean, bStartRecordWritten As Boolean
        Dim dProductionDate As Date
        Dim sContractNo As String
        Dim l As Long
        Dim nCol As Long    ' which column in output
        Dim nLine As Long   ' which line in output

        nBatchSumAmount = 0
        nBatchNoRecords = 0
        nBatchNoTransactions = 0
        dBatchLastDate = DateTime.MinValue
        dBatchFirstDate = DateTime.MinValue
        nFileSumAmount = 0
        nFileNoRecords = 0
        nFileNoTransactions = 0

        Try

            bAppendFile = False
            bStartRecordWritten = False
            dProductionDate = DateSerial("1990", "01", "01")
            sContractNo = vbNullString

            'Check if we are appending to an existing file.
            ' If so BB has to delete the last 89-record, and save the counters
            If oFs.FileExists(sFilenameOut) Then
                bAppendFile = True
                bFoundRecord89 = False
                nExistingLines = 0
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                sTempFilename = Left$(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\BBFile.tmp"
                'First count the number of lines in the existing file
                Do While Not oFile.AtEndOfStream = True
                    nExistingLines = nExistingLines + 1
                    oFile.SkipLine()
                Loop
                oFile.Close()
                oFile = Nothing
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForReading, False)
                'Then check if one of the 5 last lines are a 89-line
                ' Store the number of lines until the 89-line
                Do Until oFile.Line = nExistingLines - 1
                    oFile.SkipLine()
                Loop
                For iCount = 1 To 0 Step -1
                    sLine = oFile.ReadLine()
                    If Len(sLine) > 8 Then
                        If Left$(sLine, 8) = "NY000089" Then
                            nExistingLines = nExistingLines - iCount
                            bFoundRecord89 = True
                            Exit For
                        End If
                    End If
                Next iCount
                oFile.Close()
                oFile = Nothing
                If Not bFoundRecord89 Then
                    'FIX: Babelerror Noe feil med fila som ligger der fra før.
                    MsgBox("The existing file is not a BBS-file")
                    WriteAvtalegiroFile = False
                    Exit Function
                    oFs = Nothing
                Else
                    'Copy existing file to a temporary file and delete existing file
                    oFs.CopyFile(sFilenameOut, sTempFilename, True)
                    oFs.DeleteFile(sFilenameOut)
                    'Coopy the content of the temporary file to the new file,
                    ' excluding the 89-record
                    oFileRead = oFs.OpenTextFile(sTempFilename, Scripting.IOMode.ForReading, False, 0)
                    oFileWrite = oFs.CreateTextFile(sFilenameOut, True, 0)
                    Do Until oFileRead.Line > nExistingLines - 1
                        sLine = oFileRead.ReadLine
                        oFileWrite.WriteLine(sLine)
                    Loop
                    'Store the neccesary counters and totalamount
                    sLine = oFileRead.ReadLine
                    nFileNoTransactions = Val(Mid$(sLine, 9, 8))
                    'Subtract 1 because we have deleted the 89-line
                    nFileNoRecords = Val(Mid$(sLine, 17, 8)) - 1
                    nFileSumAmount = Val(Mid(sLine, 25, 17))
                    'Kill'em all
                    oFileRead.Close()
                    oFileRead = Nothing
                    oFileWrite.Close()
                    oFileWrite = Nothing
                    oFs.DeleteFile(sTempFilename)
                End If
            End If

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            For Each oBabel In oBabelFiles

                bExportoBabel = False
                'Have to go through each batch-object to see if we have objects to
                ' be exported to this exportfile
                For Each oBatch In oBabel.Batches
                    For Each oPayment In oBatch.Payments

                        If oPayment.VB_FilenameOut_ID = iFormat_ID And oPayment.Exported = False Then

                            ' Only process payments marked as Avtalegiro (670)!
                            If oPayment.PayCode = "670" Then
                                If Not bMultiFiles Then
                                    bExportoBabel = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBabel = True
                                            End If
                                        Else
                                            bExportoBabel = True
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        If bExportoBabel Then
                            Exit For
                        End If
                    Next
                    If bExportoBabel Then
                        Exit For
                    End If
                Next

                If bExportoBabel Then
                    If Not bAppendFile And Not bStartRecordWritten Then
                        bStartRecordWritten = True
                        sLine = Wr_AvtalegiroFileStart(oBabel, sAdditionalID)
                        oFile.WriteLine(sLine)
                    End If
                    i = 0
                    'Loop through all Batch objs. in BabelFile obj.
                    For Each oBatch In oBabel.Batches
                        bExportoBatch = False
                        'Have to go through the payment-object to see if we have objects thatt shall
                        ' be exported to this exportfile
                        For Each oPayment In oBatch.Payments
                            If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                If Not bMultiFiles Then
                                    bExportoBatch = True
                                Else
                                    If oPayment.VB_ClientNo = sClientNo Then
                                        If Len(oPayment.VB_ClientNo) > 0 Then
                                            If oPayment.VB_ClientNo = sClientNo Then
                                                bExportoBatch = True
                                            End If
                                        Else
                                            bExportoBatch = True
                                        End If
                                    End If
                                End If
                            End If
                            'If true exit the loop, and we have data to export
                            If bExportoBatch Then
                                Exit For
                            End If
                        Next

                        If bExportoBatch Then
                            nTransactionNo = 0
                            i = i + 1

                            sLine = Wr_AvtalegiroBatchStart(oBatch, sContractNo)
                            oFile.WriteLine(sLine)

                            j = 0
                            For Each oPayment In oBatch.Payments
                                bExportoPayment = False
                                'Have to go through the payment-object to see if we have objects thatt shall
                                ' be exported to this exportfile
                                If oPayment.VB_FilenameOut_ID = iFormat_ID Then
                                    If Not bMultiFiles Then
                                        bExportoPayment = True
                                    Else
                                        If oPayment.VB_ClientNo = sClientNo Then
                                            If Len(oPayment.VB_ClientNo) > 0 Then
                                                If oPayment.VB_ClientNo = sClientNo Then
                                                    bExportoPayment = True
                                                End If
                                            Else
                                                bExportoPayment = True
                                            End If
                                        End If
                                    End If
                                End If
                                If bExportoPayment Then
                                    j = j + 1

                                    For Each oInvoice In oPayment.Invoices
                                        ' Add to transactionno
                                        nTransactionNo = nTransactionNo + 1
                                        sLine = Wr_AvtalegiroPayOne(oPayment, oInvoice, nTransactionNo)
                                        oFile.WriteLine(sLine)
                                        sLine = Wr_AvtalegiroPayTwo(oPayment, nTransactionNo)
                                        oFile.WriteLine(sLine)
                                        oPayment.Exported = True

                                        l = 1
                                        nCol = 1 ' which column in output
                                        nLine = 1 ' which line in output
                                        For Each oFreetext In oInvoice.Freetexts
                                            ' gå gjennom alle freetext-er
                                            l = l + 1
                                            sLine = WriteAvtalegiroSpesifikasjon(oPayment, oFreetext, nTransactionNo, nCol, nLine)
                                            oFile.WriteLine(sLine)

                                            If nLine = 21 Then
                                                ' skriv først alt i første kolonne, deretter fra spes. 22 i kolonne 2
                                                nCol = 2
                                                nLine = 1
                                            Else
                                                nLine = nLine + 1
                                            End If

                                            ' max 42 spesifications in one payment
                                            If l > 84 Then
                                                Exit For
                                            End If
                                        Next 'oFreetext

                                    Next 'invoice
                                End If

                            Next ' payment

                            sLine = Wr_AvtalegiroBatchEnd()
                            oFile.WriteLine(sLine)

                            If StringToDate(oBatch.DATE_Production) > dProductionDate Then
                                dProductionDate = StringToDate(oBatch.DATE_Production)
                            End If

                        End If
                    Next 'batch

                End If

            Next


            If nFileNoRecords > 0 Then
                sLine = Wr_AvtalegiroFileEnd(dProductionDate)
                oFile.WriteLine(sLine)
                oFile.Close()
                oFile = Nothing
            Else
                oFile.Close()
                oFile = Nothing
                oFs.DeleteFile(sFilenameOut)
            End If

            oFs = Nothing

        Catch ex As Exception

            Throw New vbBabel.Payment.PaymentException("Function: WriteAvtalegiroFile" & vbCrLf & ex.Message, ex, oPayment, "", sFilenameOut)

        Finally

            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        WriteAvtalegiroFile = True

    End Function
    Function Wr_AvtalegiroFileStart(ByVal oBabelFile As BabelFile, ByVal sAdditionalID As String) As String

        Dim sLine As String

        ' Reset file-totals:
        nFileSumAmount = 0
        nFileNoRecords = 1
        nFileNoTransactions = 0

        sLine = "NY000010"
        If sAdditionalID <> vbNullString Then
            sLine = sLine & PadLeft(sAdditionalID, 8, "0")
        Else
            sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Sender, 1, 8), 8, "0") ' dataavsender
        End If

        If Len(oBabelFile.File_id) = 0 Then
            ' Construct unique Oppdragsnr, 7 digits; mddhMMs
            sLine = sLine & Right$(Str(Month(Now)), 1) & Right$("0" & Trim$(Str(VB.Day(Now))), 2) & Right$(Hour(Now), 1) & Right$("0" & Trim$(Str(Minute(Now))), 2) & Right$(Second(Now), 1)
            ' Must show Forsendelsesnr, used by sender as reference to BBS;
            'MsgBox "Forsendelsesnr. " & Right$(sLine, 7), , "Avtalegiro"
        ElseIf Len(oBabelFile.File_id) < 8 Then
            sLine = sLine & PadLeft(Mid(oBabelFile.File_id, 1, 7), 7, "0") ' forsendelsesnr
        Else
            sLine = sLine & Right$(oBabelFile.File_id, 7)
        End If

        If Len(oBabelFile.IDENT_Receiver) = 0 Then
            sLine = sLine & "00008080"
        Else
            sLine = sLine & PadLeft(Mid(oBabelFile.IDENT_Receiver, 1, 8), 8, "0") ' datamottaker
        End If

        sLine = PadLine(sLine, 80, "0")
        Wr_AvtalegiroFileStart = sLine

    End Function
    Public Function Wr_AvtalegiroPayOne(ByVal oPayment As Payment, ByVal oInvoice As Invoice, ByVal nTransactionNo As Double) As String
        Dim sLine As String

        ' Add to Batch-totals:
        nBatchSumAmount = nBatchSumAmount + oInvoice.MON_InvoiceAmount
        nBatchNoRecords = nBatchNoRecords + 2  'both 30 and 31, payment1 and 2
        nBatchNoTransactions = nBatchNoTransactions + 1


        sLine = "NY21"
        sLine = sLine & "21"   ' med meldingsrecords (kan legge inn test her om det er meldinger
        sLine = sLine & "30"
        sLine = sLine & PadLeft(Str$(nTransactionNo), 7, "0") ' transnr

        'sLine = sLine & Format(StringToDate(oPayment.DATE_Payment), "ddmmyy")  'forfallsdato
        ' XNET 20.11.2012
        sLine = sLine & Right$(oPayment.DATE_Payment, 2) & Mid$(oPayment.DATE_Payment, 5, 2) & Mid$(oPayment.DATE_Payment, 3, 2) 'forfallsdato ddmmyy
        sLine = sLine & New String("0", 11) 'String(11, "0") ' filler
        sLine = sLine & PadLeft(Str(oInvoice.MON_InvoiceAmount), 17, "0") ' Amount
        If Len(Trim$(oPayment.Invoices(1).Unique_Id)) > 0 Then
            sLine = sLine & PadLeft(oPayment.Invoices(1).Unique_Id, 25, " ")
        Else
            sLine = sLine & Space(25)
        End If
        sLine = PadLine(sLine, 80, "0") 'filler

        If StringToDate(oPayment.DATE_Payment) > dBatchLastDate Then
            dBatchLastDate = StringToDate(oPayment.DATE_Payment)
        End If
        If StringToDate(oPayment.DATE_Payment) < dBatchFirstDate Then
            dBatchFirstDate = StringToDate(oPayment.DATE_Payment)
        End If

        Wr_AvtalegiroPayOne = sLine

    End Function
    Public Function Wr_AvtalegiroPayTwo(ByVal oPayment As Payment, ByVal nTransactionNo As Double) As String
        Dim sLine As String

        sLine = "NY21"
        sLine = sLine & "21"   ' med meldingsrecords (kan legge inn test her om det er meldinger
        sLine = sLine & "31"
        sLine = sLine & PadLeft(Str(nTransactionNo), 7, "0") ' transnr

        sLine = sLine & PadLeft(oPayment.E_Name, 10, "0") ' shortname
        sLine = sLine & New String("0", 25) 'String(25, "0") 'filler
        sLine = sLine & PadRight(Mid(oPayment.Text_E_Statement, 1, 25), 25, " ") '

        sLine = PadLine(sLine, 80, "0") 'filler

        Wr_AvtalegiroPayTwo = sLine

    End Function
    Public Function Wr_AvtalegiroFileEnd(ByVal dProductionDate As Date) As String
        Dim sLine As String

        nFileNoRecords = nFileNoRecords + 1 ' also count 89-recs

        sLine = "NY000089"
        sLine = sLine & PadLeft(Str(nFileNoTransactions), 8, "0") ' No of transactions in file (beløpspost1 + 2 = 1)
        sLine = sLine & PadLeft(Str(nFileNoRecords), 8, "0") ' No of records in file (including 20 and 88)
        sLine = sLine & PadLeft(Str(nFileSumAmount), 17, "0") ' Total amount in file

        'If dProductionDate > DateSerial("1999", "12", "01") Then
        If dBatchFirstDate > DateSerial("1999", "12", "01") Then
            'sLine = sLine & Format(dProductionDate, "DDMMYY")
            sLine = sLine & Format(dBatchFirstDate, "ddmmyy") ' First date in batch,DDMMYY
        Else
            sLine = sLine & Format(Now, "ddmmyy") ' BBS date (Productiondate of file)
        End If

        ' pad with 0 to 80
        sLine = PadLine(sLine, 80, "0")

        Wr_AvtalegiroFileEnd = sLine

    End Function
    Function Wr_AvtalegiroBatchStart(ByVal oBatch As Batch, ByVal sContractNo As String) As String
        Dim sLine As String

        ' Reset Batch-totals:
        nBatchSumAmount = 0
        nBatchNoRecords = 1
        nBatchNoTransactions = 0
        dBatchLastDate = System.DateTime.Now.AddDays(-1000)
        dBatchFirstDate = System.DateTime.Now.AddDays(1000)

        sLine = "NY210020"
        sLine = sLine & New String("0", 9) 'String(9, "0")
        If oBatch.Batch_ID = vbNullString Then
            ' Construct unique Oppdragsnr, 7 digits; mddhMMs
            sLine = sLine & Right$(Str(Month(Now)), 1) & Right$("0" & Trim$(Str(VB.Day(Now))), 2) & Right$(Hour(Now), 1) & Right$("0" & Trim$(Str(Minute(Now))), 2) & Right$(Second(Now), 1)
        Else
            sLine = sLine & PadLeft(Mid(oBatch.Batch_ID, 1, 7), 7, "0") ' oppdragsnr
        End If
        sLine = sLine & PadLeft(Mid(oBatch.Payments(1).I_Account, 1, 11), 11, "0") ' oppdragskonto
        sLine = PadLine(sLine, 80, "0") 'filler

        Wr_AvtalegiroBatchStart = sLine

    End Function

    Private Function Wr_AvtalegiroBatchEnd() As String
        Dim sLine As String
        nBatchNoRecords = nBatchNoRecords + 1  'Also count 88-recs

        ' Add to File-totals:
        nFileSumAmount = nFileSumAmount + nBatchSumAmount
        nFileNoRecords = nFileNoRecords + nBatchNoRecords
        nFileNoTransactions = nFileNoTransactions + nBatchNoTransactions

        sLine = "NY210088"
        sLine = sLine & PadLeft(Str(nBatchNoTransactions), 8, "0") ' No of transactions in batch (beløpspost1 + 2 = 1)
        sLine = sLine & PadLeft(Str(nBatchNoRecords), 8, "0") ' No of records in batch (including 20 and 88)

        sLine = sLine & PadLeft(Str(nBatchSumAmount), 17, "0") ' Total amount in batch
        sLine = sLine & Format(dBatchFirstDate, "ddmmyy") ' First date in batch,DDMMYY
        sLine = sLine & Format(dBatchLastDate, "ddmmyy") ' First date in batch, DDMMYY

        sLine = PadLine(sLine, 80, "0")

        Wr_AvtalegiroBatchEnd = sLine

    End Function
    Function WriteAvtalegiroSpesifikasjon(ByVal oPayment As Payment, ByVal oFreetext As Freetext, ByVal iTransactionNo As Double, ByVal nCol As Long, ByVal nLine As Long) As String

        Dim sLine As String
        Dim sPayCode As String
        nBatchNoRecords = nBatchNoRecords + 1

        sLine = "NY21"
        sLine = sLine & "21"
        sLine = sLine & "49"
        sLine = sLine & PadLeft(Str$(iTransactionNo), 7, "0") ' transnr
        sLine = sLine & "4"     ' Betalingsvarsel, alltid 4

        If oFreetext.Row > 0 And oFreetext.Col > 0 Then
            sLine = sLine & PadLeft(Str(oFreetext.Row), 3, "0")
            sLine = sLine & Trim$(Str(oFreetext.Col))
        Else
            sLine = sLine & PadLeft(Str(nLine), 3, "0")
            sLine = sLine & Trim$(Str(nCol))
        End If
        sLine = sLine & Left(oFreetext.Text & New String(" ", 40), 40)  ' ikke bruk padright, da den også trimmer ledende blanke!
        sLine = PadLine(sLine, 80, "0") ' filler

        WriteAvtalegiroSpesifikasjon = sLine

    End Function


End Module
