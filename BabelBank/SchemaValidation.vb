﻿Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Public Class SchemaValidation
    Private sXMLFileName As String = ""
    Private sSchemaFileName As String = ""
    Private sTargetNameSpace As String = ""
    Private bValidationErrorExists As Boolean = False
    Private bValidationWarningExists As Boolean = False
    Private aValidationError As New System.Collections.ArrayList
    Public WriteOnly Property XMLFileName() As String
        Set(ByVal Value As String)
            sXMLFileName = Value
        End Set
    End Property
    Public WriteOnly Property TargetNameSpace() As String
        Set(ByVal Value As String)
            sTargetNameSpace = Value
        End Set
    End Property
    Public WriteOnly Property SchemaFileName() As String
        Set(ByVal Value As String)
            sSchemaFileName = Value
        End Set
    End Property
    Public ReadOnly Property ValidationErrorExists() As String
        Get
            ValidationErrorExists = bValidationErrorExists
        End Get
    End Property
    Public ReadOnly Property ValidationWarningExists() As Boolean
        Get
            ValidationWarningExists = bValidationWarningExists
        End Get
    End Property
    Public Function Validate(ByVal XMLPortalRequest As String) As Boolean
        Try
            Dim Settings As System.Xml.XmlReaderSettings = New System.Xml.XmlReaderSettings()

            Settings.ValidationType = System.Xml.ValidationType.Schema
            ' Create the path for the schema file.
            '''''''Dim schemaFile As String = Path.Combine(Request.PhysicalApplicationPath, "App_Data\BooksList.xsd")
            ' Indicate that elements in the namespace
            ' http://www.Books.com/BooksList should be validated using the schema file.
            Settings.Schemas.Add(sTargetNameSpace, sSchemaFileName)
            ' Open the XML file.
            Dim Fs As System.IO.FileStream = New System.IO.FileStream(sXMLFileName, System.IO.FileMode.Open)

            ' Connect to the method named ValidateHandler.
            AddHandler Settings.ValidationEventHandler, AddressOf ValidationEventHandler

            ' Create the validating reader.
            Dim R As System.Xml.XmlReader = System.Xml.XmlReader.Create(Fs, Settings)

            'Dim objSchemasColl As New System.Xml.Schema.XmlSchemaSet
            'objSchemasColl.Add("xxx", "xxx")
            'objSchemasColl.Add("xxx", "xxxd")
            'Dim xmlDocument As New System.Xml.XmlDocument
            'xmlDocument.LoadXml(XMLPortalRequest)
            'xmlDocument.Schemas.Add(objSchemasColl)
            'xmlDocument.Validate(AddressOf ValidationEventHandler)
            'If validationErrors = "" Then
            '    Return True
            'Else
            '    Return False
            'End If

        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As System.Xml.Schema.ValidationEventArgs)
        Select Case e.Severity
            Case System.Xml.Schema.XmlSeverityType.Error
                bValidationErrorExists = True
                aValidationError.Add("Error: " & e.Message)
            Case System.Xml.Schema.XmlSeverityType.Warning
                bValidationWarningExists = False
                aValidationError.Add("Warning: " & e.Message)
        End Select
        'validationErrors += e.Message & "<br />"
    End Sub


    Private Function xValidate(ByVal Schema As String, ByVal sNameSpace As String, ByVal XMLDoc As String) As ArrayList

        Dim objWorkingXML As New System.Xml.XmlDocument
        Dim objValidateXML As System.Xml.XmlValidatingReader
        Dim objSchemasColl As New System.Xml.Schema.XmlSchemaCollection
        aValidationError.Clear()

        objSchemasColl.Add(sNameSpace, Schema)

        'This loads XML string 
        objValidateXML = New System.Xml.XmlValidatingReader(New System.Xml.XmlTextReader(XMLDoc))

        objValidateXML.Schemas.Add(objSchemasColl)

        'This is how you CATCH the errors (with a handler function)
        AddHandler objValidateXML.ValidationEventHandler, AddressOf ValidationCallBack

        'This is WHERE the validation occurs.. WHEN the XML Document READS throughthe validating reader
        Try
            objWorkingXML.Load(objValidateXML)

        Catch ex As Exception
            aValidationError.Add(ex.Message)
            If Not objValidateXML Is Nothing Then
                objValidateXML.Close()
                objValidateXML = Nothing
            End If
            If Not objWorkingXML Is Nothing Then
                objWorkingXML = Nothing
            End If
            If Not objSchemasColl Is Nothing Then
                objSchemasColl = Nothing
            End If
        Finally
            objValidateXML.Close()
            objValidateXML = Nothing
            objWorkingXML = Nothing
            objSchemasColl = Nothing

        End Try

        Return aValidationError

    End Function

    Private Sub ValidationCallBack(ByVal sender As Object, ByVal e As System.Xml.Schema.ValidationEventArgs)
        Select Case e.Severity
            Case System.Xml.Schema.XmlSeverityType.Error
                bValidationErrorExists = True
                aValidationError.Add("Error: " & e.Message)
            Case System.Xml.Schema.XmlSeverityType.Warning
                bValidationWarningExists = False
                aValidationError.Add("Warning: " & e.Message)
        End Select
        'aValidationError.Add(e.Message)
    End Sub

End Class
