Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_506_Matching_summary
    Dim sClientInfo As String
    Dim iBabelFile_ID As Integer
    Dim iBatch_ID As Integer
    Dim sBreakField As String

    Dim iDetailLevel As Integer
    Dim bShowDATE_Production As Boolean
    Dim bShowClientName As Boolean
    Dim sTypeOfData As String
    Dim sOldTypeOfString As String

    Dim sReportName As String
    Dim sSpecial As String
    Dim bSQLServer As Boolean = False
    Dim iBreakLevel As Integer
    Dim bUseLongDate As Boolean
    Dim sReportNumber As String
    Dim bReportFromDatabase As Boolean
    Dim bEmptyReport As Boolean
    Dim bPrintReport As Boolean
    Dim sPrinterName As String
    Dim bAskForPrinter As Boolean

    Private Sub rp_506_Matching_summary_DataInitialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DataInitialize
        '        Fields.Add("CategoryID")
        '        Fields.Add("CategoryName")
        Fields.Add("BreakField")

    End Sub

    Private Sub rp_506_Matching_summary_NoData(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.NoData
        bEmptyReport = True
        Me.Cancel()
    End Sub

    Private Sub rp_506_Matching_summary_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        iBabelFile_ID = -1
        iBatch_ID = -1
        sBreakField = ""
        sTypeOfData = ""
        sOldTypeOfString = ""

        Select Case iBreakLevel
            'Only 1 possibilities

            Case 0 'NOBREAK
                bShowDATE_Production = True
                bShowClientName = False

            Case Else 'NOBREAK
                bShowDATE_Production = True
                bShowClientName = True

        End Select

        'Long or ShortDate
        If bUseLongDate Then
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:D}"
            Me.txtDATE_Production.OutputFormat = "D"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:D}"
        Else
            Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:d}"
            Me.txtDATE_Production.OutputFormat = "d"
            Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:d}"
        End If

        ' Setup of pagesize, etc
        Me.PageSettings.DefaultPaperSize = True ' 'A4
        Me.PageSettings.DefaultPaperSource = True
        Me.PageSettings.Margins.Bottom = 1.0F
        Me.PageSettings.Margins.Left = 1.0F
        Me.PageSettings.Margins.Right = 0.75F '1.0F
        Me.PageSettings.Margins.Top = 1.0F
        'Me.PrintWidth = 6.5
        ''Me.PageSettings.PaperWidth = 8.5F
        Me.PageSettings.Orientation = PageOrientation.Portrait

        If EmptyString(sReportName) Then
            Me.lblrptHeader.Text = LRS(40116) 'Summary
        Else
            Me.lblrptHeader.Text = sReportName
        End If
        'If Language = Norwegian Then
        Me.rptInfoPageCounterGB.Visible = False
        Me.rptInfoPageCounterNO.Visible = True
        Me.rptInfoPageCounterNO.Left = 4.2
        'Else
        'Me.rptInfoPageCounterGB.Visible = True
        'Me.rptInfoPageCounterNO.Visible = False
        'End If

        'grBatchHeader
        'Me.lblFilename.Text = LRS(40001) '"Filnavn:" 
        Me.lblDate_Production.Text = LRS(40027) '"Prod.dato:" 
        Me.lblOCR.Text = "OCR"
        Me.lblUnmatched.Text = LRS(40029) 'Unposted
        Me.lblMatched.Text = LRS(60088) ' Posteded
        Me.lblTotal.Text = LRS(40117) '"Totalt" 

        'grBabelFileFooter
        Me.lblTotalAmount.Text = LRS(40104) '"Bel�p:" 
        Me.lblTotalNoOfPayments.Text = LRS(40106) '"Total - Antall:" 

    End Sub

    Private Sub rp_506_Matching_summary_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData
        Dim sDATE As String

        If Not eArgs.EOF Then

            Select Case iBreakLevel
                Case 0 'NOBREAK
                    Me.Fields("BreakField").Value = ""

                Case 5 'BREAK_ON_CLIENT 'New
                    Me.Fields("BreakField").Value = Me.Fields("Client").Value

                Case Else
                    Me.Fields("BreakField").Value = ""

            End Select

            'START WORKING HERE

            Me.txtDATE_Production.Value = Now()

            sTypeOfData = Me.Fields("Type").Value

            Select Case sTypeOfData
                Case "DATE"
                    Me.txtType.Text = LRS(60065) & ":" '"Dato:" 
                    If bUseLongDate Then
                        Me.txtHeading.OutputFormat = "D"
                    Else
                        Me.txtHeading.OutputFormat = "d"
                    End If
                    sDATE = Me.Fields("Heading").Value
                    Me.txtHeading.Value = DateSerial(CInt(Left$(sDATE, 4)), CInt(Mid$(sDATE, 5, 2)), CInt(Right$(sDATE, 2)))
                    Me.txtTotalAmount.Value = Me.txtTotalAmount.Value + Me.Fields("TotalAmount").Value
                    Me.txtTotalNoOfPayments.Value = Me.txtTotalNoOfPayments.Value + Me.Fields("NoOfRecords").Value

                Case "ACCOUNT"
                    Me.txtType.Text = LRS(60076) & ":" '"Konto"
                    Me.txtHeading.Text = Me.Fields("Heading").Value

                Case "FILENAME"
                    Me.txtType.Text = LRS(60061) & ":" '"Filnavn:"
                    If Len(Me.Fields("Heading").Value) > 18 Then
                        Me.txtHeading.Text = "..." & Right$(Me.Fields("Heading").Value, 17)
                    Else
                        Me.txtHeading.Text = Me.Fields("Heading").Value
                    End If

            End Select

            If IsDBNull(Me.Fields("SumOCR").Value) Then
                Me.txtOCRAmount.Value = 0
            Else
                Me.txtOCRAmount.Value = Me.Fields("SumOCR").Value
            End If

            If IsDBNull(Me.Fields("SumUnmatched").Value) Then
                Me.txtUnmatchedAmount.Value = 0
            Else
                Me.txtUnmatchedAmount.Value = Me.Fields("SumUnmatched").Value
            End If

            If IsDBNull(Me.Fields("SumMatched").Value) Then
                Me.txtMatchedAmount.Value = 0
            Else
                Me.txtMatchedAmount.Value = Me.Fields("SumMatched").Value
            End If

            sOldTypeOfString = ""

        End If

    End Sub
    Public WriteOnly Property ReportName() As String
        Set(ByVal Value As String)
            sReportName = Value
        End Set
    End Property
    Public WriteOnly Property Special() As String
        Set(ByVal Value As String)
            sSpecial = Value
        End Set
    End Property
    Public WriteOnly Property SQLServer() As Boolean
        Set(ByVal Value As Boolean)
            bSQLServer = Value
        End Set
    End Property
    Public WriteOnly Property BreakLevel() As Integer
        Set(ByVal Value As Integer)
            iBreakLevel = Value
        End Set
    End Property
    Public WriteOnly Property UseLongDate() As Boolean
        Set(ByVal Value As Boolean)
            bUseLongDate = Value
        End Set
    End Property
    Public WriteOnly Property ReportNumber() As String
        Set(ByVal Value As String)
            sReportNumber = Value
        End Set
    End Property
    Public WriteOnly Property ReportFromDatabase() As Boolean
        Set(ByVal Value As Boolean)
            bReportFromDatabase = Value
        End Set
    End Property
    Public ReadOnly Property EmptyReport() As Boolean
        Get
            EmptyReport = bEmptyReport
        End Get
    End Property
    Public WriteOnly Property PrintReport() As Boolean
        Set(ByVal Value As Boolean)
            bPrintReport = Value
        End Set
    End Property
    Public WriteOnly Property PrinterName() As String
        Set(ByVal Value As String)
            sPrinterName = Value
        End Set
    End Property
    Public WriteOnly Property AskForPrinter() As Boolean
        Set(ByVal Value As Boolean)
            bAskForPrinter = Value
        End Set
    End Property
    Private Sub rptPrint_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        If bPrintReport Then
            If Not EmptyString(sPrinterName) And sPrinterName <> "Default" Then
                Me.Document.Printer.PrinterName = sPrinterName
            End If
            Me.Document.Print(bAskForPrinter, False, False)
        End If
    End Sub
End Class
