Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class rp_Sub_952_AutoMatch
    Dim lAutoCount As Long
    Dim lAutoCountInv As Long

    Private Sub rp_Sub_952_AutoMatch_FetchData(ByVal sender As Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles Me.FetchData

    End Sub
    Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format

        Me.lblType.Value = Me.Fields("Type").Value & " - " & Me.Fields("ClientNo").Value
        If lAutoCount > 0 Then
            Me.txtTypePercent.Value = (Me.txtTypeCount.Value / lAutoCount) * 100
        Else
            Me.txtTypePercent.Text = ""
        End If
        If lAutoCountInv > 0 Then
            Me.txtTypePercentInv.Value = ((Me.txtTypeProposed.Value + Me.txtTypeFinal.Value) / lAutoCountInv) * 100
        Else
            Me.txtTypePercentInv.Text = ""
        End If

    End Sub
    Public Sub SetAutoCount(ByVal l As Long)
        lAutoCount = l
    End Sub
    Public Sub SetAutoCountInv(ByVal l As Long)
        lAutoCountInv = l
    End Sub

End Class
