Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("CONTRL_NET.CONTRL")> Public Class CONTRL
	
	'Private bMessageRejected As Boolean
	
	Friend Function CreateCONTRLMessage(ByRef rAppAdmImport As CAppAdmImport, ByRef sFilename As String, ByRef bTranslationOK As Boolean, ByRef lBabelFile_ID As Integer) As String
		Dim sFileString As String
		Dim sNewSegment As String
		Dim lNoofSegments, lNoofMessages As Integer
        Dim oFs As Scripting.FileSystemObject
		Dim oFile As Scripting.TextStream
		Dim sInterchangeReference As String
		Dim sLineUNA As String
		Dim sLineUNB As String
		Dim sLineUNH As String
		Dim sLineUCI As String
		Dim sLineUCM As String
		Dim sLineUNT As String
		Dim sLineUNZ As String
		
		lNoofSegments = 0
		lNoofMessages = 0

        oFs = New Scripting.FileSystemObject

		sNewSegment = CreateUNA((rAppAdmImport.XMLDoc))
		If Left(sNewSegment, 5) = "ERROR: An unexpected error occured duting creation of the UNA-segment." Then
			CreateCONTRLMessage = sNewSegment
			Exit Function
		End If
		sLineUNA = sNewSegment
		
		'sInterchangeReference is created in CreateUNB, but also passed to CreateUNZ
        sInterchangeReference = ""
		sNewSegment = CreateUNB((rAppAdmImport.XMLDoc), "CONTRL", lBabelFile_ID, sInterchangeReference)
		If Left(sNewSegment, 5) = "ERROR: An unexpected error occured duting creation of the UNB-segment." Then
			CreateCONTRLMessage = sNewSegment
			Exit Function
		End If
		sLineUNB = sNewSegment
		
		lNoofMessages = lNoofMessages + 1
		sNewSegment = CreateUNH((rAppAdmImport.XMLDoc), "CONTRL", lNoofMessages)
		lNoofSegments = lNoofSegments + 1
		If Left(sNewSegment, 5) = "ERROR: An unexpected error occured duting creation of the UNH-segment." Then
			CreateCONTRLMessage = sNewSegment
			Exit Function
		End If
		sLineUNH = sNewSegment
		
		sNewSegment = CreateUCI((rAppAdmImport.XMLDoc), bTranslationOK)
		lNoofSegments = lNoofSegments + 1
		If Left(sNewSegment, 5) = "ERROR: An unexpected error occured duting creation of the UCI-segment." Then
			CreateCONTRLMessage = sNewSegment
			Exit Function
		End If
		sLineUCI = sNewSegment
		
		sNewSegment = CreateUCM((rAppAdmImport.XMLDoc), bTranslationOK)
		lNoofSegments = lNoofSegments + 1
		If Left(sNewSegment, 5) = "ERROR: An unexpected error occured duting creation of the UCM-segment." Then
			CreateCONTRLMessage = sNewSegment
			Exit Function
		End If
		sLineUCM = sNewSegment
		
		
		'If the message is checked and found OK, we'll just have to add the trailers.
		' If not we'll have to specify the error.
		'If bMessageRejected Then
		'
		'End If
		
		sNewSegment = CreateUNT((rAppAdmImport.XMLDoc), lNoofMessages, lNoofSegments + 1)
		lNoofSegments = 0
		If Left(sNewSegment, 5) = "ERROR: An unexpected error occured duting creation of the UNT-segment." Then
			CreateCONTRLMessage = sNewSegment
			Exit Function
		End If
		sLineUNT = sNewSegment
		
		sNewSegment = CreateUNZ((rAppAdmImport.XMLDoc), lNoofMessages, sInterchangeReference)
		If Left(sNewSegment, 5) = "ERROR: An unexpected error occured duting creation of the UNZ-segment." Then
			CreateCONTRLMessage = sNewSegment
			Exit Function
		End If
		sLineUNZ = sNewSegment
		
        If CheckFolder(sFilename, False, , LRS(12005), False) < 10 Then
            If oFs.FileExists(sFilename) Then '(App.path & "\CONTRL.dat") Then
                oFs.DeleteFile((sFilename))
            End If
        Else
            CreateCONTRLMessage = "ERROR, Specified folder does not exist, or is write-protected."
            'UPGRADE_NOTE: Object oFs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            Exit Function
        End If
		
		oFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForAppending, True, 0)
		If Not EmptyString(sLineUNA) Then
			oFile.WriteLine((sLineUNA))
		End If
		If Not EmptyString(sLineUNB) Then
			oFile.WriteLine((sLineUNB))
		End If
		If Not EmptyString(sLineUNH) Then
			oFile.WriteLine((sLineUNH))
		End If
		If Not EmptyString(sLineUCI) Then
			oFile.WriteLine((sLineUCI))
		End If
		If Not EmptyString(sLineUCM) Then
			oFile.WriteLine((sLineUCM))
		End If
		If Not EmptyString(sLineUNT) Then
			oFile.WriteLine((sLineUNT))
		End If
		If Not EmptyString(sLineUNZ) Then
			oFile.WriteLine((sLineUNZ))
		End If
		
        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

		CreateCONTRLMessage = "CONTRL-message created sucessfully!"

	End Function
	
	Public Function CreateUCI(ByRef XMLDoc As MSXML2.DOMDocument40, ByRef bTranslationOK As Boolean) As String
		Dim sInterchangeControlRef As String
		Dim sSenderID As String
		Dim sRecipientID As String
		Dim sAction As String
		Dim sSegmentString As String
		Dim i As Short
		
		For i = 0 To 0
			sInterchangeControlRef = GetValue(XMLDoc, "//UNB/TG050/T0020")
			If Len(sInterchangeControlRef) = 0 Then
				sSegmentString = "ERROR, Not able to create a Interchange Control Reference"
				Exit For
			End If
			sSenderID = GetValue(XMLDoc, "//UNB/TG020/T0004")
			'sSenderID = GetValue(XMLDoc, "//UNB/TG030/T0010")
			If Len(sSenderID) = 0 Then
				sSegmentString = "ERROR, Not able to create a sender identification"
				Exit For
			End If
			sRecipientID = GetValue(XMLDoc, "//UNB/TG030/T0010")
			'sRecipientID = GetValue(XMLDoc, "//UNB/TG020/T0004")
			If Len(sRecipientID) = 0 Then
				sSegmentString = "ERROR, Not able to create a recipent identification"
				Exit For
			End If
			If bTranslationOK Then
				sAction = "7"
				'bMessageRejected = False
			Else
				sAction = "4+18"
				'bMessageRejected = True
			End If
			
			sSegmentString = "UCI+" & sInterchangeControlRef & "+" & sSenderID & "+" & sRecipientID
			sSegmentString = sSegmentString & "+" & sAction & "'"
		Next i
		
		CreateUCI = sSegmentString
		
	End Function
	Public Function CreateUCM(ByRef XMLDoc As MSXML2.DOMDocument40, ByRef bTranslationOK As Boolean) As String
		Dim sMessageRefNo As String
		Dim sMessageTypeIdentifier As String
		Dim sMessageTypeVersionNo As String
		Dim sMessageTypeReleaseNo As String
		Dim sControllingAgency As String
		Dim sAssociationAssignedCode As String
		
		Dim sRecipientID As String
		Dim sAction As String
		Dim sSegmentString As String
		Dim i As Short
		
		For i = 0 To 0
			sMessageRefNo = GetValue(XMLDoc, "//UNH/TG010/T0062")
			If Len(sMessageRefNo) = 0 Then
				sSegmentString = "ERROR, Not able to create the Message Reference Number"
				Exit For
			End If
			sMessageTypeIdentifier = GetValue(XMLDoc, "//UNH/TG020/T0065")
			If Len(sMessageTypeIdentifier) = 0 Then
				sSegmentString = "ERROR, Not able to create the Message Type Identifier" 'PAYMUL, CREMUL etc....
				Exit For
			End If
			sMessageTypeVersionNo = GetValue(XMLDoc, "//UNH/TG020/T0052")
			If Len(sMessageTypeVersionNo) = 0 Then
				sSegmentString = "ERROR, Not able to create the Message Type Version Number" 'D
				Exit For
			End If
			sMessageTypeReleaseNo = GetValue(XMLDoc, "//UNH/TG020/T0054")
			If Len(sMessageTypeReleaseNo) = 0 Then
				sSegmentString = "ERROR, Not able to create the Message Type Release Number" '96A
				Exit For
			End If
			sControllingAgency = GetValue(XMLDoc, "//UNH/TG020/T0051")
			If Len(sControllingAgency) = 0 Then
				sSegmentString = "ERROR, Not able to create the Controlling Agency" 'UN
				Exit For
			End If
			sAssociationAssignedCode = GetValue(XMLDoc, "//UNH/TG020/T0057")
			If Len(sAssociationAssignedCode) = 0 Then
				'Not mandatory
				'sSegmentString = "ERROR, Not able to create the Controlling Agency"
				'Exit For
			End If
			If bTranslationOK Then
				sAction = "7"
			Else
				sAction = "4"
			End If
			'Should test if the error is in the UNH or in the UNT segment, then it should be stated here.
			'''    If bTranslationOK Then
			'''        sAction = "7"
			'''        'bMessageRejected = False
			'''    Else
			'''        sAction = "4+18+UNH" 'Should test if the error is in the UNH or in the UNT segment.
			'''        'Error codes
			'''        '12 = Invalid Value
			'''        '13 = Missing
			'''        '16 = Too many constituents
			'''        '18 = Unspecified error
			'''        '28 = References do not match
			'''        '29 = Control count does not match number of instances received
			'''        '39 = Data element too long
			'''        '40 = Data element too short
			'''        'bMessageRejected = True
			'''    End If
			
			sSegmentString = "UCM+" & sMessageRefNo & "+" & sMessageTypeIdentifier & ":" & sMessageTypeVersionNo
			sSegmentString = sSegmentString & ":" & sMessageTypeReleaseNo & ":" & sControllingAgency
			If Not EmptyString(sAssociationAssignedCode) Then
				sSegmentString = sSegmentString & ":" & sAssociationAssignedCode
			End If
			sSegmentString = sSegmentString & "+" & sAction & "'"
		Next i
		
		CreateUCM = sSegmentString
		
	End Function
End Class
