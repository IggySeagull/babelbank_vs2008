<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class rp_012_CONTRL
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim OleDBDataSource1 As DataDynamics.ActiveReports.DataSources.OleDBDataSource = New DataDynamics.ActiveReports.DataSources.OleDBDataSource
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(rp_012_CONTRL))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtStatus = New DataDynamics.ActiveReports.TextBox
        Me.txtIDENT_Sender = New DataDynamics.ActiveReports.TextBox
        Me.lblSender = New DataDynamics.ActiveReports.Label
        Me.lblReceiver = New DataDynamics.ActiveReports.Label
        Me.txtIDENT_Receiver = New DataDynamics.ActiveReports.TextBox
        Me.lblFileID = New DataDynamics.ActiveReports.Label
        Me.txtFile_ID = New DataDynamics.ActiveReports.TextBox
        Me.txtErrorMark = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.lblrptHeader = New DataDynamics.ActiveReports.Label
        Me.Line3 = New DataDynamics.ActiveReports.Line
        Me.rptInfoPageHeaderDate = New DataDynamics.ActiveReports.ReportInfo
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.rptInfoPageFooterDate = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterNO = New DataDynamics.ActiveReports.ReportInfo
        Me.rptInfoPageCounterGB = New DataDynamics.ActiveReports.ReportInfo
        Me.grBabelFileHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.grBabelFileFooter = New DataDynamics.ActiveReports.GroupFooter
        Me.lblNoOfErrors = New DataDynamics.ActiveReports.Label
        Me.LineBabelFileFooter1 = New DataDynamics.ActiveReports.Line
        Me.txtNoOfErrors = New DataDynamics.ActiveReports.TextBox
        Me.TotalSum = New DataDynamics.ActiveReports.Field
        Me.grBatchHeader = New DataDynamics.ActiveReports.GroupHeader
        Me.lblDate_Production = New DataDynamics.ActiveReports.Label
        Me.shapeBatchHeader = New DataDynamics.ActiveReports.Shape
        Me.txtDATE_Production = New DataDynamics.ActiveReports.TextBox
        Me.lblClient = New DataDynamics.ActiveReports.Label
        Me.txtClientName = New DataDynamics.ActiveReports.TextBox
        Me.txtI_Account = New DataDynamics.ActiveReports.TextBox
        Me.lblI_Account = New DataDynamics.ActiveReports.Label
        Me.grBatchFooter = New DataDynamics.ActiveReports.GroupFooter
        CType(Me.txtStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtIDENT_Sender, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblSender, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblReceiver, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtIDENT_Receiver, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblFileID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFile_ID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtErrorMark, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblNoOfErrors, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNoOfErrors, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtStatus, Me.txtIDENT_Sender, Me.lblSender, Me.lblReceiver, Me.txtIDENT_Receiver, Me.lblFileID, Me.txtFile_ID, Me.txtErrorMark})
        Me.Detail.Height = 0.7395833!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'txtStatus
        '
        Me.txtStatus.CanShrink = True
        Me.txtStatus.Height = 0.2!
        Me.txtStatus.Left = 0.0!
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.Style = "font-size: 9.75pt; font-weight: bold"
        Me.txtStatus.Text = "txtStatus"
        Me.txtStatus.Top = 0.25!
        Me.txtStatus.Width = 5.0!
        '
        'txtIDENT_Sender
        '
        Me.txtIDENT_Sender.DataField = "IDENT_Sender"
        Me.txtIDENT_Sender.Height = 0.15!
        Me.txtIDENT_Sender.Left = 0.6!
        Me.txtIDENT_Sender.Name = "txtIDENT_Sender"
        Me.txtIDENT_Sender.Style = "font-size: 8pt; ddo-char-set: 1"
        Me.txtIDENT_Sender.Text = "txtIDENT_Sender"
        Me.txtIDENT_Sender.Top = 0.0!
        Me.txtIDENT_Sender.Width = 1.3!
        '
        'lblSender
        '
        Me.lblSender.Height = 0.15!
        Me.lblSender.HyperLink = Nothing
        Me.lblSender.Left = 0.0!
        Me.lblSender.Name = "lblSender"
        Me.lblSender.Style = "font-size: 8pt; ddo-char-set: 1"
        Me.lblSender.Text = "lblSender"
        Me.lblSender.Top = 0.0!
        Me.lblSender.Width = 0.6!
        '
        'lblReceiver
        '
        Me.lblReceiver.Height = 0.15!
        Me.lblReceiver.HyperLink = Nothing
        Me.lblReceiver.Left = 1.9!
        Me.lblReceiver.Name = "lblReceiver"
        Me.lblReceiver.Style = "font-size: 8pt; ddo-char-set: 1"
        Me.lblReceiver.Text = "lblReceiver"
        Me.lblReceiver.Top = 0.0!
        Me.lblReceiver.Width = 0.6!
        '
        'txtIDENT_Receiver
        '
        Me.txtIDENT_Receiver.DataField = "IDENT_Receiver"
        Me.txtIDENT_Receiver.Height = 0.15!
        Me.txtIDENT_Receiver.Left = 2.5!
        Me.txtIDENT_Receiver.Name = "txtIDENT_Receiver"
        Me.txtIDENT_Receiver.Style = "font-size: 8pt; ddo-char-set: 1"
        Me.txtIDENT_Receiver.Text = "txtIDENT_Receiver"
        Me.txtIDENT_Receiver.Top = 0.0!
        Me.txtIDENT_Receiver.Width = 1.9!
        '
        'lblFileID
        '
        Me.lblFileID.Height = 0.15!
        Me.lblFileID.HyperLink = Nothing
        Me.lblFileID.Left = 4.4!
        Me.lblFileID.Name = "lblFileID"
        Me.lblFileID.Style = "font-size: 8pt; ddo-char-set: 1"
        Me.lblFileID.Text = "lblFileID"
        Me.lblFileID.Top = 0.0!
        Me.lblFileID.Width = 0.6!
        '
        'txtFile_ID
        '
        Me.txtFile_ID.DataField = "File_ID"
        Me.txtFile_ID.Height = 0.15!
        Me.txtFile_ID.Left = 5.0!
        Me.txtFile_ID.Name = "txtFile_ID"
        Me.txtFile_ID.Style = "font-size: 8pt; ddo-char-set: 1"
        Me.txtFile_ID.Text = "txtFile_ID"
        Me.txtFile_ID.Top = 0.0!
        Me.txtFile_ID.Width = 1.3!
        '
        'txtErrorMark
        '
        Me.txtErrorMark.Height = 0.125!
        Me.txtErrorMark.Left = 5.25!
        Me.txtErrorMark.Name = "txtErrorMark"
        Me.txtErrorMark.Style = "font-size: 8pt; ddo-char-set: 1"
        Me.txtErrorMark.Text = "txtErrorMark"
        Me.txtErrorMark.Top = 0.25!
        Me.txtErrorMark.Visible = False
        Me.txtErrorMark.Width = 0.75!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblrptHeader, Me.Line3, Me.rptInfoPageHeaderDate})
        Me.PageHeader.Height = 0.4576389!
        Me.PageHeader.Name = "PageHeader"
        '
        'lblrptHeader
        '
        Me.lblrptHeader.Height = 0.2951389!
        Me.lblrptHeader.HyperLink = Nothing
        Me.lblrptHeader.Left = 1.279861!
        Me.lblrptHeader.Name = "lblrptHeader"
        Me.lblrptHeader.Style = "font-size: 18pt; text-align: center"
        Me.lblrptHeader.Text = "Batches"
        Me.lblrptHeader.Top = 0.0!
        Me.lblrptHeader.Width = 3.641667!
        '
        'Line3
        '
        Me.Line3.Height = 0.0!
        Me.Line3.Left = 0.0!
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.39375!
        Me.Line3.Width = 6.299305!
        Me.Line3.X1 = 0.0!
        Me.Line3.X2 = 6.299305!
        Me.Line3.Y1 = 0.39375!
        Me.Line3.Y2 = 0.39375!
        '
        'rptInfoPageHeaderDate
        '
        Me.rptInfoPageHeaderDate.CanGrow = False
        Me.rptInfoPageHeaderDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageHeaderDate.Height = 0.1875!
        Me.rptInfoPageHeaderDate.Left = 5.3125!
        Me.rptInfoPageHeaderDate.Name = "rptInfoPageHeaderDate"
        Me.rptInfoPageHeaderDate.Style = "text-align: right"
        Me.rptInfoPageHeaderDate.Top = 0.0!
        Me.rptInfoPageHeaderDate.Width = 1.1875!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.rptInfoPageFooterDate, Me.rptInfoPageCounterNO, Me.rptInfoPageCounterGB})
        Me.PageFooter.Height = 0.2604167!
        Me.PageFooter.Name = "PageFooter"
        '
        'rptInfoPageFooterDate
        '
        Me.rptInfoPageFooterDate.CanGrow = False
        Me.rptInfoPageFooterDate.FormatString = "{RunDateTime:}"
        Me.rptInfoPageFooterDate.Height = 0.1875!
        Me.rptInfoPageFooterDate.Left = 0.0!
        Me.rptInfoPageFooterDate.Name = "rptInfoPageFooterDate"
        Me.rptInfoPageFooterDate.Style = ""
        Me.rptInfoPageFooterDate.Top = 0.0!
        Me.rptInfoPageFooterDate.Width = 2.0!
        '
        'rptInfoPageCounterNO
        '
        Me.rptInfoPageCounterNO.FormatString = "Side {PageNumber} av {PageCount}"
        Me.rptInfoPageCounterNO.Height = 0.188!
        Me.rptInfoPageCounterNO.Left = 3.0!
        Me.rptInfoPageCounterNO.Name = "rptInfoPageCounterNO"
        Me.rptInfoPageCounterNO.Style = "text-align: right"
        Me.rptInfoPageCounterNO.Top = 0.0!
        Me.rptInfoPageCounterNO.Width = 2.25!
        '
        'rptInfoPageCounterGB
        '
        Me.rptInfoPageCounterGB.FormatString = "Page {PageNumber} of {PageCount}"
        Me.rptInfoPageCounterGB.Height = 0.188!
        Me.rptInfoPageCounterGB.Left = 4.0!
        Me.rptInfoPageCounterGB.Name = "rptInfoPageCounterGB"
        Me.rptInfoPageCounterGB.Style = "text-align: right"
        Me.rptInfoPageCounterGB.Top = 0.0!
        Me.rptInfoPageCounterGB.Width = 2.25!
        '
        'grBabelFileHeader
        '
        Me.grBabelFileHeader.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.grBabelFileHeader.Height = 0.0625!
        Me.grBabelFileHeader.Name = "grBabelFileHeader"
        Me.grBabelFileHeader.NewPage = DataDynamics.ActiveReports.NewPage.Before
        '
        'grBabelFileFooter
        '
        Me.grBabelFileFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblNoOfErrors, Me.LineBabelFileFooter1, Me.txtNoOfErrors})
        Me.grBabelFileFooter.Height = 0.3541667!
        Me.grBabelFileFooter.Name = "grBabelFileFooter"
        '
        'lblNoOfErrors
        '
        Me.lblNoOfErrors.Height = 0.2!
        Me.lblNoOfErrors.HyperLink = Nothing
        Me.lblNoOfErrors.Left = 4.5!
        Me.lblNoOfErrors.Name = "lblNoOfErrors"
        Me.lblNoOfErrors.Style = "font-size: 10pt; font-weight: bold; ddo-char-set: 1"
        Me.lblNoOfErrors.Text = "lblNoOfErrors"
        Me.lblNoOfErrors.Top = 0.0!
        Me.lblNoOfErrors.Width = 1.3!
        '
        'LineBabelFileFooter1
        '
        Me.LineBabelFileFooter1.Height = 0.0!
        Me.LineBabelFileFooter1.Left = 4.5!
        Me.LineBabelFileFooter1.LineWeight = 1.0!
        Me.LineBabelFileFooter1.Name = "LineBabelFileFooter1"
        Me.LineBabelFileFooter1.Top = 0.2!
        Me.LineBabelFileFooter1.Width = 1.8!
        Me.LineBabelFileFooter1.X1 = 4.5!
        Me.LineBabelFileFooter1.X2 = 6.3!
        Me.LineBabelFileFooter1.Y1 = 0.2!
        Me.LineBabelFileFooter1.Y2 = 0.2!
        '
        'txtNoOfErrors
        '
        Me.txtNoOfErrors.DataField = "txtErrorMark"
        Me.txtNoOfErrors.DistinctField = "NoOfPayments"
        Me.txtNoOfErrors.Height = 0.2!
        Me.txtNoOfErrors.Left = 5.8!
        Me.txtNoOfErrors.Name = "txtNoOfErrors"
        Me.txtNoOfErrors.Style = "font-size: 10pt; font-weight: bold; text-align: right; ddo-char-set: 1"
        Me.txtNoOfErrors.Text = "txtNoOfErrors"
        Me.txtNoOfErrors.Top = 0.0!
        Me.txtNoOfErrors.Width = 0.5!
        '
        'TotalSum
        '
        Me.TotalSum.DefaultValue = "100"
        Me.TotalSum.FieldType = DataDynamics.ActiveReports.FieldTypeEnum.None
        Me.TotalSum.Formula = "3+3"
        Me.TotalSum.Name = "TotalSum"
        Me.TotalSum.Tag = Nothing
        '
        'grBatchHeader
        '
        Me.grBatchHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblDate_Production, Me.shapeBatchHeader, Me.txtDATE_Production, Me.lblClient, Me.txtClientName, Me.txtI_Account, Me.lblI_Account})
        Me.grBatchHeader.Height = 0.635!
        Me.grBatchHeader.Name = "grBatchHeader"
        '
        'lblDate_Production
        '
        Me.lblDate_Production.Height = 0.19!
        Me.lblDate_Production.HyperLink = Nothing
        Me.lblDate_Production.Left = 0.1!
        Me.lblDate_Production.Name = "lblDate_Production"
        Me.lblDate_Production.Style = "font-size: 10pt"
        Me.lblDate_Production.Text = "lblDate_Production"
        Me.lblDate_Production.Top = 0.06!
        Me.lblDate_Production.Width = 1.313!
        '
        'shapeBatchHeader
        '
        Me.shapeBatchHeader.Height = 0.5!
        Me.shapeBatchHeader.Left = 0.0!
        Me.shapeBatchHeader.Name = "shapeBatchHeader"
        Me.shapeBatchHeader.RoundingRadius = 9.999999!
        Me.shapeBatchHeader.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.shapeBatchHeader.Top = 0.0!
        Me.shapeBatchHeader.Width = 6.45!
        '
        'txtDATE_Production
        '
        Me.txtDATE_Production.CanShrink = True
        Me.txtDATE_Production.Height = 0.19!
        Me.txtDATE_Production.Left = 1.5!
        Me.txtDATE_Production.Name = "txtDATE_Production"
        Me.txtDATE_Production.OutputFormat = resources.GetString("txtDATE_Production.OutputFormat")
        Me.txtDATE_Production.Style = "font-size: 10pt"
        Me.txtDATE_Production.Text = "txtDATE_Production"
        Me.txtDATE_Production.Top = 0.0625!
        Me.txtDATE_Production.Width = 1.563!
        '
        'lblClient
        '
        Me.lblClient.Height = 0.19!
        Me.lblClient.HyperLink = Nothing
        Me.lblClient.Left = 3.5!
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Style = "font-size: 10pt"
        Me.lblClient.Text = "lblClient"
        Me.lblClient.Top = 0.0604167!
        Me.lblClient.Width = 1.0!
        '
        'txtClientName
        '
        Me.txtClientName.CanGrow = False
        Me.txtClientName.Height = 0.19!
        Me.txtClientName.Left = 4.5!
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Style = "font-size: 10pt"
        Me.txtClientName.Text = "txtClientName"
        Me.txtClientName.Top = 0.0604167!
        Me.txtClientName.Width = 1.9!
        '
        'txtI_Account
        '
        Me.txtI_Account.DataField = "I_Account"
        Me.txtI_Account.Height = 0.19!
        Me.txtI_Account.Left = 4.5!
        Me.txtI_Account.Name = "txtI_Account"
        Me.txtI_Account.Style = "font-size: 10pt; font-weight: normal"
        Me.txtI_Account.Text = "txtI_Account"
        Me.txtI_Account.Top = 0.2504167!
        Me.txtI_Account.Width = 1.9!
        '
        'lblI_Account
        '
        Me.lblI_Account.Height = 0.19!
        Me.lblI_Account.HyperLink = Nothing
        Me.lblI_Account.Left = 3.5!
        Me.lblI_Account.Name = "lblI_Account"
        Me.lblI_Account.Style = "font-size: 10pt"
        Me.lblI_Account.Text = "lblI_Account"
        Me.lblI_Account.Top = 0.2504167!
        Me.lblI_Account.Width = 1.0!
        '
        'grBatchFooter
        '
        Me.grBatchFooter.CanShrink = True
        Me.grBatchFooter.Height = 0.25!
        Me.grBatchFooter.Name = "grBatchFooter"
        '
        'rp_012_CONTRL
        '
        Me.MasterReport = False
        Me.CalculatedFields.Add(Me.TotalSum)
        OleDBDataSource1.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Projects\BabelBank\Profiles\Profi" & _
            "le - arv report.mdb;Persist Security Info=False"
        ' TODO ACCESS2016 - 27.03.2018 Ikke tilpasset
        OleDBDataSource1.SQL = resources.GetString("OleDBDataSource1.SQL")
        Me.DataSource = OleDBDataSource1
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 6.489583!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.grBabelFileHeader)
        Me.Sections.Add(Me.grBatchHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.grBatchFooter)
        Me.Sections.Add(Me.grBabelFileFooter)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'Times New Roman'; font-style: italic; font-variant: inherit; font-w" & _
                    "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: 'inherit'; font-style: inherit; font-variant: inherit; font-weight: " & _
                    "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("", "Heading4", "Normal"))
        CType(Me.txtStatus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtIDENT_Sender, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblSender, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblReceiver, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtIDENT_Receiver, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblFileID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFile_ID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtErrorMark, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblrptHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageHeaderDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageFooterDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterNO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rptInfoPageCounterGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblNoOfErrors, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNoOfErrors, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDate_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDATE_Production, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblClient, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblI_Account, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DataDynamics.ActiveReports.Detail
    Friend WithEvents txtStatus As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtIDENT_Sender As DataDynamics.ActiveReports.TextBox
    Friend WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader
    Friend WithEvents lblrptHeader As DataDynamics.ActiveReports.Label
    Friend WithEvents Line3 As DataDynamics.ActiveReports.Line
    Friend WithEvents rptInfoPageHeaderDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter
    Friend WithEvents rptInfoPageFooterDate As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterNO As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents rptInfoPageCounterGB As DataDynamics.ActiveReports.ReportInfo
    Friend WithEvents grBabelFileHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents grBabelFileFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents LineBabelFileFooter1 As DataDynamics.ActiveReports.Line
    Friend WithEvents lblSender As DataDynamics.ActiveReports.Label
    Friend WithEvents TotalSum As DataDynamics.ActiveReports.Field
    Friend WithEvents lblReceiver As DataDynamics.ActiveReports.Label
    Friend WithEvents txtIDENT_Receiver As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblNoOfErrors As DataDynamics.ActiveReports.Label
    Friend WithEvents txtNoOfErrors As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblFileID As DataDynamics.ActiveReports.Label
    Friend WithEvents txtFile_ID As DataDynamics.ActiveReports.TextBox
    Friend WithEvents grBatchHeader As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents grBatchFooter As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents lblDate_Production As DataDynamics.ActiveReports.Label
    Friend WithEvents shapeBatchHeader As DataDynamics.ActiveReports.Shape
    Friend WithEvents txtDATE_Production As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblClient As DataDynamics.ActiveReports.Label
    Friend WithEvents txtClientName As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtI_Account As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblI_Account As DataDynamics.ActiveReports.Label
    Friend WithEvents txtErrorMark As DataDynamics.ActiveReports.TextBox
End Class
