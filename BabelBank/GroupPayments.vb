Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("GroupPayments_NET.GroupPayments")> Public Class GroupPayments
	'This class is used to mark payments within a batch-collection, that
	' have some parameters in common
	
	Private oBatch As vbbabel.Batch
	Private oPayment As vbbabel.Payment
	Private bGroupCrossBorderPayments As Boolean
    'XNET - 12.04.2011 - Added next variable
    Private bGroupPriorityPayments As Boolean
    Private bGroupByI_Account As Boolean
	Private bGroupByDate_Payment As Boolean
    Private bGroupByPaycode As Boolean
    Private bGroupByIntraCompany As Boolean  ' added 06.09.2016
    Private bGroupByPayType As Boolean
	Private bGroupByCurrency As Boolean
    Private bGroupBySEPA As Boolean
    Private bGroupByISO20022Check As Boolean
    Private bMovePaymentsDueToToday As Boolean
	Private nMON_InvoiceAmount As Double
	Private nMON_TransferredAmount As Double
    Private bGroupingExists As Boolean
    Private bGroupByFIK As Boolean   ' 03.04.2019
    ' 24.04.2017
    Private bDoNotGroup As Boolean = False
	'Private bAllPaymentsMarkedAsExported As Boolean
	
	'********* START PROPERTY SETTINGS ***********************
	Public WriteOnly Property MovePaymentsDueToToday() As Boolean
		Set(ByVal Value As Boolean)
			bMovePaymentsDueToToday = Value
		End Set
	End Property
	Public WriteOnly Property GroupCrossBorderPayments() As Boolean
		Set(ByVal Value As Boolean)
			bGroupCrossBorderPayments = Value
		End Set
	End Property
    'XNET - 12.04.2011 - Added next property
    Public WriteOnly Property GroupPriorityPayments() As Boolean
        Set(ByVal Value As Boolean)
            bGroupPriorityPayments = Value
        End Set
    End Property
    '24.04.2017 - Added next property
    Public WriteOnly Property DoNotGroup() As Boolean
        Set(ByVal Value As Boolean)
            bDoNotGroup = Value
        End Set
    End Property
    Public ReadOnly Property AllPaymentsMarkedAsExported() As Boolean
        Get
            Dim bReturnValue As Boolean

            'New 02.09.2008
            bReturnValue = True
            For Each oPayment In oBatch.Payments
                If Not oPayment.Exported Then
                    bReturnValue = False
                    Exit For
                End If
            Next oPayment

            AllPaymentsMarkedAsExported = bReturnValue

            'Old code
            'AllPaymentsMarkedAsExported = bAllPaymentsMarkedAsExported

        End Get
    End Property
	Public WriteOnly Property Batch() As vbbabel.Batch
		Set(ByVal Value As vbbabel.Batch)
			oBatch = Value
		End Set
	End Property
	Public ReadOnly Property MON_InvoiceAmount() As Double
		Get
			MON_InvoiceAmount = nMON_InvoiceAmount
		End Get
	End Property
	Public ReadOnly Property MON_TransferredAmount() As Double
		Get
			MON_TransferredAmount = nMON_TransferredAmount
		End Get
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
    Public Function MarkIdenticalPayments(Optional ByVal bChangeImportedDataToDefaults As Boolean = True) As Integer
        'This function sets SpecialMark = True for all payments identical to the first payment
        ' which isn't marked as exported
        Dim bFirstPaymentFound As Boolean
        Dim sI_Account As String
        Dim sDate_Payment As String
        Dim sPayCode As String
        Dim bIntraCompany As Boolean = False  ' added 06.09.2016
        Dim sPayType As String
        Dim sTransferCurrency As String
        Dim sInvoiceCurrency As String
        Dim bMarkPayment As Boolean
        Dim bSEPAPayment As Boolean
        Dim bFIKPayment As Boolean  ' 03.04.2019
        Dim bCheckPayment As Boolean
        ' added 10.05.2010 - not allowed to have more than 9999 SEQ's in one LIN
        Dim lSeqCounter As Integer

        bFirstPaymentFound = False
        sI_Account = ""
        sDate_Payment = ""
        sPayCode = ""
        bIntraCompany = False
        nMON_InvoiceAmount = 0
        nMON_TransferredAmount = 0
        lSeqCounter = 0
        'bAllPaymentsMarkedAsExported = True

        If Not bGroupingExists Then
            For Each oPayment In oBatch.Payments
                If Not oPayment.Exported Then
                    '05.03.2010 - This may be unlogical, normally it should just be the first non-exported payment that
                    '  should be marked with the specialmark. That would work well
                    'However, if You have thousands of payments (f.x. Gjensidige) and You don't want any grouping, the
                    ' code will have to traverse through all payments in the batch a numerous times for each payment to export.
                    oPayment.SpecialMark = True
                    ' added 10.05.2010 - not allowed to have more than 9999 SEQ's in one LIN
                    lSeqCounter = lSeqCounter + 1
                    If lSeqCounter >= 9998 Then
                        Exit For
                    End If

                End If
            Next oPayment
        Else
            For Each oPayment In oBatch.Payments
                If Not oPayment.Exported Then
                    If Not bFirstPaymentFound Then
                        sI_Account = oPayment.I_Account
                        If bChangeImportedDataToDefaults = True Then
                            If oPayment.DATE_Payment < Format(Now, "yyyyMMdd") Then
                                oPayment.DATE_Payment = Format(Now, "yyyyMMdd")
                            End If
                        End If
                        sDate_Payment = oPayment.DATE_Payment
                        sPayCode = oPayment.PayCode
                        bIntraCompany = oPayment.ToOwnAccount
                        sPayType = oPayment.PayType
                        sInvoiceCurrency = oPayment.MON_InvoiceCurrency
                        sTransferCurrency = oPayment.MON_TransferCurrency
                        bSEPAPayment = oPayment.IsSEPA
                        bCheckPayment = IsISO20022Check(oPayment)
                        oPayment.SpecialMark = True
                        nMON_InvoiceAmount = nMON_InvoiceAmount + oPayment.MON_InvoiceAmount
                        nMON_TransferredAmount = nMON_TransferredAmount + oPayment.MON_TransferredAmount
                        bFIKPayment = IsFIK(oPayment.PayCode)
                        bFirstPaymentFound = True

                        If Not bGroupCrossBorderPayments And oPayment.PayType = "I" Then
                            'Don't group crossborder payments
                            'bAllPaymentsMarkedAsExported = False
                            Exit For
                            'XokNET - 12.04.2011 - Added next ElseIf
                        ElseIf Not bGroupPriorityPayments And oPayment.Priority Then
                            'Don't group priority
                            Exit For
                            ' 24.04.2017 added next if
                        ElseIf bDoNotGroup Then  ' denne trigges av oppsett p� klienten, ISODoNotGroup
                            Exit For
                        End If

                    Else
                        If Not bGroupCrossBorderPayments And oPayment.PayType = "I" Then
                            'Don't group crossborder payments
                            'bAllPaymentsMarkedAsExported = False
                            'XokNET 12.04.2011 - Commented next line
                            'Exit For
                            'XokNET - 12.04.2011 - Added next ElseIf
                        ElseIf Not bGroupPriorityPayments And oPayment.Priority Then
                            'Don't group priority
                        Else
                            bMarkPayment = True
                            If bGroupByI_Account Then
                                If sI_Account <> oPayment.I_Account Then
                                    bMarkPayment = False
                                End If
                            End If
                            If bGroupByDate_Payment Then
                                If bChangeImportedDataToDefaults = True Then
                                    If oPayment.DATE_Payment < Format(Now, "yyyyMMdd") Then
                                        oPayment.DATE_Payment = Format(Now, "yyyyMMdd")
                                    End If
                                End If
                                If sDate_Payment <> oPayment.DATE_Payment Then
                                    bMarkPayment = False
                                End If
                            End If
                            If bGroupByPaycode Then
                                If sPayCode <> oPayment.PayCode Then
                                    bMarkPayment = False
                                    'ElseIf sPayType <> oPayment.PayType Then
                                    '    bMarkPayment = False
                                    'End If
                                End If
                            End If
                            ' added next If 06.09.2016
                            If bGroupByIntraCompany Then
                                If bIntraCompany <> oPayment.ToOwnAccount Then
                                    bMarkPayment = False
                                End If
                            End If
                            If bGroupByPayType Then
                                If sPayType <> oPayment.PayType Then
                                    '20.09.2012 Added payType as a separate grouping-parameter. Not done in VB6
                                    bMarkPayment = False
                                End If
                            End If
                            If bGroupByCurrency Then
                                If sInvoiceCurrency <> oPayment.MON_InvoiceCurrency Then
                                    bMarkPayment = False
                                End If
                                If sTransferCurrency <> oPayment.MON_TransferCurrency Then
                                    bMarkPayment = False
                                End If
                            End If
                            If bGroupBySEPA Then
                                If bSEPAPayment <> oPayment.IsSEPA Then
                                    bMarkPayment = False
                                End If
                            End If
                            If bGroupByISO20022Check Then
                                If bCheckPayment <> IsISO20022Check(oPayment) Then
                                    bMarkPayment = False
                                End If
                            End If
                            ' added 03.04.2019 for FIK DK
                            If bGroupByFIK Then
                                If bFIKPayment <> IsFIK(oPayment.PayCode) Then
                                    bMarkPayment = False
                                End If
                            End If
                            If bMarkPayment Then
                                oPayment.SpecialMark = True
                                nMON_InvoiceAmount = nMON_InvoiceAmount + oPayment.MON_InvoiceAmount
                                nMON_TransferredAmount = nMON_TransferredAmount + oPayment.MON_TransferredAmount
                                ' added 10.05.2010 - not allowed to have more than 9999 SEQ's in one LIN
                                lSeqCounter = lSeqCounter + 1
                                If lSeqCounter >= 9998 Then
                                    Exit For
                                End If
                            Else
                                'bAllPaymentsMarkedAsExported = False
                            End If
                        End If ' If Not bGroupCrossBorderPayments And oPayment.PayType = "I" Then
                    End If
                End If
            Next oPayment
        End If

    End Function
	
    Private Function IsISO20022Check(ByVal oPayment As vbBabel.Payment) As Boolean
        Dim bReturnValue As Boolean

        If oPayment.PayType = "I" Then
            If oPayment.Cheque = BabelFiles.ChequeType.noCheque Then
                bReturnValue = False
            Else
                bReturnValue = True
            End If
        Else
            If oPayment.Cheque = BabelFiles.ChequeType.noCheque Then
                bReturnValue = False
            Else
                bReturnValue = True
            End If

            '09.01.2020 - Added this code because all cheque payments aren't marked as a cheque, typical utbetalingsanvisninger in Norway
            If EmptyString(oPayment.E_Account) Or oPayment.PayCode = "160" Or oPayment.E_Account = "00000000019" Then
                '160 = Utbet uten mott.konto
                '"00000000019" is/was used in Telepay to state an utbetalingsanvisning, hopefullt we'll never get a real accountno like this
                '    (We do not know the countrycode the first time we hit this code, so we can not test for Norway)
                bReturnValue = True
                If oPayment.E_Account = "00000000019" Then
                    oPayment.Cheque = BabelFiles.ChequeType.SendToPayer
                    oPayment.E_Account = ""
                End If
            End If

            ''Should be: the code beneath, but I have no idea how to deduct eCountryname here (will be a big change 
            'If eCountryName = BabelFiles.CountryName.Great_Britain Or _
            '    eCountryName = BabelFiles.CountryName.Denmark Or _
            '    eCountryName = BabelFiles.CountryName.Singapore Or _
            '    eCountryName = BabelFiles.CountryName.Australia Or _
            '    eCountryName = BabelFiles.CountryName.USA Then
            '    If oPayment.Cheque = BabelFiles.ChequeType.noCheque Then
            '        bReturnValue = False
            '    Else
            '        bReturnValue = True
            '    End If
            'Else
            '    bReturnValue = False
            'End If
        End If

        Return bReturnValue

    End Function

    Public Function RemoveSpecialMark() As Boolean

        For Each oPayment In oBatch.Payments
            oPayment.SpecialMark = False
        Next oPayment

    End Function


    Public Function SetGroupingParameters(ByRef bI_Account As Boolean, ByRef bDate_Payment As Boolean, ByRef bPayCode As Boolean, ByRef bPayType As Boolean, ByRef bIntraCompany As Boolean, ByRef bCurrency As Boolean, Optional ByRef bSEPA As Boolean = False, Optional ByRef bISO20022Check As Boolean = False, Optional ByRef bFIK As Boolean = False) As String
        '20.09.2012 Added payType as a separate grouping-parameter. Not done in VB6
        ' 06.09.2016 Added IntraCompany as a separate grouping-parameter. Not done in VB6

        bGroupByI_Account = bI_Account
        bGroupByDate_Payment = bDate_Payment
        bGroupByPaycode = bPayCode
        bGroupByIntraCompany = bIntraCompany
        bGroupByPayType = bPayType
        bGroupByCurrency = bCurrency
        bGroupBySEPA = bSEPA
        bGroupByISO20022Check = bISO20022Check
        bGroupByFIK = bFIK

        bGroupingExists = True

        If Not bGroupByI_Account Then
            If Not bGroupByDate_Payment Then
                If Not bGroupByPaycode Then
                    If Not bGroupByIntraCompany Then
                        If Not bGroupByPayType Then
                            If Not bGroupByCurrency Then
                                If Not bGroupBySEPA Then
                                    If Not bGroupByISO20022Check Then
                                        If Not bGroupByFIK Then
                                            bGroupingExists = False
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If

    End Function
    Private Sub Class_Initialize_Renamed()

        bGroupCrossBorderPayments = False
        'XNET - 12.04.2011 - Next line
        bGroupPriorityPayments = False
        bGroupByI_Account = False
        bGroupByDate_Payment = False
        bGroupByPaycode = False
        bGroupByIntraCompany = False
        bGroupByPayType = False
        bGroupByCurrency = False
        bGroupBySEPA = False
        bGroupByISO20022Check = False
        bGroupByFIK = False
        bMovePaymentsDueToToday = False
        nMON_InvoiceAmount = 0
        nMON_TransferredAmount = 0
        'bAllPaymentsMarkedAsExported = False
        bDoNotGroup = False

    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub
End Class
