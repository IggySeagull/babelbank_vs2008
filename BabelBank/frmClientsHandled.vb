Option Strict Off
Option Explicit On
Friend Class frmClientsHandled
	Inherits System.Windows.Forms.Form
	Public bClientsStatus As Boolean
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		bClientsStatus = True
		Me.Hide()
		
	End Sub
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		bClientsStatus = False
		Me.Hide()
		
	End Sub
	
	Private Sub cmdPrint_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPrint.Click
        'Dim sprClientsHandled As Object
        'With sprClientsHandled

        '	'1. Specify orientation
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintOrientation. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintOrientation = SS_PRINTORIENT_PORTRAIT
        '	'2.  Specify the size of the margins in twips using the PrintMarginLeft, PrintMarginRight, PrintMarginTop, and PrintMarginBottom properties.
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintMarginLeft. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintMarginLeft = 800
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintMarginTop. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintMarginTop = 1440 ' 1 inch
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintMarginBottom. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintMarginBottom = 1440 ' 1 inch
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintMarginRight. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintMarginRight = 400
        '	'3.  Specify whether to print the spreadsheet border with the PrintBorder property.
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintBorder. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintBorder = False
        '	'4.  Specify whether to print the spreadsheet column header and row header with the PrintColHeaders and PrintRowHeaders properties.
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintRowHeaders. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintRowHeaders = False
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintColHeaders. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintColHeaders = True
        '	'5.  Specify whether to print the shadow effect within the spreadsheet column header and row header with the PrintShadows property.
        '	'6.  Specify whether to print the spreadsheet grid lines with the PrintGrid property.
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintGrid. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintGrid = False
        '	'8.  Specify the print job name to display in the Print Manager when printing the spreadsheet with the PrintJobName property.
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintJobName. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintJobName = "Babelbank " & LRS(40026) 'rapport"
        '	'9. Headers/Footers
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintHeader. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintHeader = "/c/fb1/fz""12""" & Me.Text

        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintFooter. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintFooter = "/c/p /r Babelbank - Visual Banking AS"
        '	'10. Specify the text to display in an abort dialog box during printing with the PrintAbortMsg property.
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.PrintAbortMsg. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.PrintAbortMsg = LRS(40025) '"Skriver ut Babelbank rapport"
        '	'UPGRADE_WARNING: Couldn't resolve default property of object sprClientsHandled.Action. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        '	.Action = SS_ACTION_PRINT


        'End With
		
	End Sub
	
	Private Sub frmClientsHandled_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		FormvbStyle(Me, "")
	End Sub
End Class