Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ProgId("Database_NET.Database")> Public Class Database
	Private sProvider As String
	Private sFile As String
	Dim cnConnection As New ADODB.Connection
	'********* START PROPERTY SETTINGS ***********************
	Public Property Provider() As String
		Get
			Provider = sProvider
		End Get
		Set(ByVal Value As String)
			sProvider = Value
		End Set
	End Property
	Public Property File() As String
		Get
			File = sFile
		End Get
		Set(ByVal Value As String)
			sFile = Value
		End Set
	End Property
	Public ReadOnly Property Connection() As ADODB.Connection
		Get
			Connection = cnConnection
		End Get
	End Property
	'********* END PROPERTY SETTINGS ***********************
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		' When vbbabel.dll is called, the code attempts
		' to find a local satellite DLL that will contain all the
		' resources.
        'If Not (LoadLocalizedResources("vbbabel")) Then
        ' failed to load satelite language-DLL
        ' FIX: Change messagebox
        'MsgBox("Failed to load language DLL")
        'End If
        sProvider = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="
        sFile = BB_DatabasePath()
        '    If RunTime Then
        '        sFile = App.path + "\profile.mdb"
        '    Else
        '       sFile = "c:\projects\babelbank\profile.mdb"
        '    End If
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Sub Class_Terminate_Renamed()
		If cnConnection.State = ADODB.ObjectStateEnum.adStateOpen Then
			cnConnection.Close()
		End If
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
	Public Function Connect() As Boolean
		Dim sMyConnectionString As String
		
		If Not CanLoad(sFile) Then
			'FIX: Babelerror
			Exit Function
		End If
		sMyConnectionString = sProvider & sFile
		cnConnection.ConnectionString = sMyConnectionString
		
		cnConnection.Open()
		
		'ConnectToProfile = sMyConnectionString
		
		
	End Function
	Public Function SQLCommand(ByRef sMySQL As Object) As Boolean
		Dim cmdUpdate As New ADODB.Command
		'Dim sMyFile As String
		'Dim oDatabase As Database
		
		
		If cnConnection.State = ADODB.ObjectStateEnum.adStateClosed Then
			'ERR: Add errorinformation
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object sMySQL. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		cmdUpdate.CommandText = sMySQL
		cmdUpdate.CommandType = ADODB.CommandTypeEnum.adCmdText
		cmdUpdate.let_ActiveConnection(cnConnection)
		cmdUpdate.Execute()
		
	End Function
	Public Sub Disconnect()
		If cnConnection.State = ADODB.ObjectStateEnum.adStateOpen Then
			cnConnection.Close()
		End If
	End Sub
	
	Public Function ConnectToProfile(ByRef sMyFile As String) As String
		'FIX: This method shall be killed
		Dim sMyConnectionString As String
		
		If Not CanLoad(sMyFile) Then
			'FIX: Babelerror
			Exit Function
		End If
		
		sMyConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sMyFile
		ConnectToProfile = sMyConnectionString
		
	End Function
	Private Function CanLoad(ByRef sMyFile As String) As Boolean
		Dim bCanLoad As Boolean
		Dim oFs As Object
		
		bCanLoad = True
		
		oFs = CreateObject("Scripting.FileSystemObject")
		'UPGRADE_WARNING: Couldn't resolve default property of object oFs.FileExists. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If oFs.FileExists(sMyFile) = False Then
			bCanLoad = False
		End If
		
		CanLoad = bCanLoad
		
		
	End Function
End Class
