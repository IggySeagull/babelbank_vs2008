Option Strict Off
Option Explicit On

Friend Class frmClientImport
	Inherits System.Windows.Forms.Form
	Public bCancelled As Boolean
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		
		bCancelled = True
		Me.Close()
		
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		
		bCancelled = False
		Me.Hide()
		'Unload Me
		
	End Sub
	
	Private Sub frmClientImport_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		FormvbStyle(Me, "", 1)
		FormLRSCaptions(Me)
		bCancelled = False
		
		Me.txtFormatIn.Text = "2"
		Me.txtFormatOut.Text = "1"
		Me.txtObservationAccount.Text = "99999"
		Me.txtPattern.Text = "@2####@"
		
		
	End Sub
End Class
