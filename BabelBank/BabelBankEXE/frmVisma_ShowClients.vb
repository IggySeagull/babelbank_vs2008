Option Strict Off
Option Explicit On
Friend Class frmVisma_ShowClients
    Inherits System.Windows.Forms.Form
    Private iClientID As Short
    Private Declare Function DestroyCaret Lib "user32" () As Integer
    Private sUID As String
    Private sPWD As String
    Dim sInstance As String
    Private sBBDatabase As String
    Private bChanged As Boolean
    Private lActiveRow As Integer
    Private Sub frmVisma_ShowClients_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        ' Nytt kall;
        'VB;/S;F9998;vbsys;system;sa;Visma123
        Dim oVismaCommandLine As New VismaCommandLine

        'sUID = xDelim(Command, ";", 6, Chr(34))
        'sPWD = xDelim(Command, ";", 7, Chr(34))
        'sBBDatabase = "BabelBank_" & xDelim(Command, ";", 4, Chr(34))

        sBBDatabase = oVismaCommandLine.BBDatabase
        sUID = oVismaCommandLine.UID
        sPWD = oVismaCommandLine.PWD
        sInstance = oVismaCommandLine.Instance
        oVismaCommandLine = Nothing

        FormLRSCaptions(Me)
        FormVismaStyle(Me)


        bChanged = False
        SpreadHeaders()
        SpreadFill()
    End Sub
    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        If bChanged Then
            ' Vil du avslutte uten lagring ?
            If MsgBox(LRS(48020), MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Me.Hide()
            End If
        Else
            Me.Hide()
        End If
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        ' delete a client/bankpartner combo
        'If MsgBox("Vil du slette dette klientoppsettet ?", vbQuestion + vbYesNo, "BabelBank") = vbYes Then
        Dim sFirm As String
        Dim sProfileGroup As String
        Dim sAccount As String
        Dim iNextRow As Integer = 0

        On Error GoTo localerror

        With Me.gridClients
            If Not .CurrentRow Is Nothing Then
                ' Firmnumber
                sFirm = .Rows(.CurrentRow.Index).Cells(0).Value
                ' Profilegroupname
                sProfileGroup = .Rows(.CurrentRow.Index).Cells(2).Value
                ' AccountNo
                sAccount = .Rows(.CurrentRow.Index).Cells(3).Value
                'If .CurrentRow.Index < .RowCount - 1 Then
                '    .Rows(.CurrentRow.Index + 1).Selected = True
                'End If
                iNextRow = .CurrentRow.Index
                If iNextRow < .RowCount - 1 Then
                    Do While iNextRow < .RowCount - 1
                        If .Rows(iNextRow + 1).Visible = True Then
                            iNextRow = .CurrentRow.Index + 1
                            Exit Do
                        Else
                            iNextRow = iNextRow + 1
                        End If
                    Loop

                Else
                    Do While iNextRow > 0
                        If .Rows(iNextRow - 1).Visible = True Then
                            iNextRow = .CurrentRow.Index - 1
                            Exit Do
                        Else
                            iNextRow = iNextRow - 1
                        End If
                    Loop

                End If


                'If MsgBox(sFirm & " " & sProfileGroup & " " & sAccount & vbCrLf & LRS(35128), vbQuestion + vbYesNo, "Visma Payment (BabelBank)") = vbYes Then
                If MsgBox(LRS(35183) & " " & sFirm & vbCrLf & LRS(35017) & " " & sProfileGroup & vbCrLf & LRS(35215) & " " & sAccount & vbCrLf & LRS(35128), MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Visma Payment (BabelBank)") = MsgBoxResult.Yes Then
                    .Rows(.CurrentRow.Index).Visible = False
                    bChanged = True
                    If iNextRow > -1 Then
                        If .Rows(iNextRow).Visible = True Then
                            .CurrentCell = .Rows(iNextRow).Cells(2)
                            .Rows(iNextRow).Selected = True
                        End If
                    End If
                End If
            End If
        End With
        Exit Sub

localerror:
        Err.Raise(30001, "frmVismaShowClients.cmdDelete_Click", Err.Description) '
    End Sub
    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        ' run through grid, remove deleted
        Dim oVismaDal As New vbBabel.DAL
        Dim sErrorString As String
        Dim sMySQL As String
        Dim iClientID As Short
        Dim i As Short
        Dim lRecordsAffected As Integer

        On Error GoTo localerror
        sErrorString = "ConnectToVismaBusinessDB"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        'Me.Hide
        oVismaDal.TEST_UseCommitFromOutside = True
        oVismaDal.TEST_BeginTrans()

        With Me.gridClients
            For i = 0 To .RowCount - 1
                If .Rows(i).Visible = False Then
                    iClientID = .Rows(i).Cells(5).Value

                    ' Detelete removed items;
                    ' -----------------------
                    sMySQL = "DELETE FROM " & sBBDatabase & ".dbo.bbClient WHERE ClientID = " & iClientID
                    oVismaDal.SQL = sMySQL
                    oVismaDal.ExecuteNonQuery()
                End If
            Next i
        End With
        oVismaDal.TEST_CommitTrans()
        oVismaDal.Close()
        oVismaDal = Nothing
        bChanged = False
        Me.Hide()

        Exit Sub

localerror:
        oVismaDal.TEST_Rollback()
        Err.Raise(30001, "frmVismaShowClients.cmdOK_Click", sErrorString) '

    End Sub
    Private Sub SpreadHeaders()
        Dim txtColumn As DataGridViewTextBoxColumn

        With Me.gridClients

            .Rows.Clear()  ' empty content of grid

            If .ColumnCount = 0 Then
                .ScrollBars = ScrollBars.Vertical
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.FixedSingle
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowsDefaultCellStyle.SelectionForeColor = Color.Black
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .ReadOnly = False
            End If

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(4)
            txtColumn.HeaderText = Replace(LRS(60149), ":", "") 'FirmNo
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(6)
            txtColumn.HeaderText = Replace(LRS(35001), ":", "") 'BankPartner
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(12)
            txtColumn.HeaderText = Replace(LRS(35017), ":", "") 'Profilgruppe
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(12)
            txtColumn.HeaderText = Replace(LRS(60022), ":", "") 'Kontonummer
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(18)
            txtColumn.HeaderText = Replace(LRS(17505), ":", "") 'Navn
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Visible = False
            .Columns.Add(txtColumn)
        End With

    End Sub
    Private Sub SpreadFill()
        ' Fill up from table bbClients
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sErrorString As String
        Dim sMySQL As String
        Dim myRow As DataGridViewRow

        On Error GoTo VismaFillError

        sErrorString = "ConnectToVismaBusinessDB"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        sMySQL = "SELECT C.*, P.Name AS ProfileName FROM " & sBBDatabase & ".dbo.bbClient C, " & sBBDatabase & ".dbo.bbProfileGroup P WHERE C.ProfileGroupID = P.ProfileGroupID ORDER BY C.FrmNo, C.BPartNo"
        oVismaDal.SQL = sMySQL
        If oVismaDal.Reader_Execute() Then
            If oVismaDal.Reader_HasRows = True Then

                With Me.gridClients
                    ' fill up
                    Do While oVismaDal.Reader_ReadRecord

                        myRow = New DataGridViewRow
                        '.Rows.Add()
                        'myRow = .Rows(.RowCount - 1)
                        myRow.CreateCells(Me.gridClients)

                        ' Firmnumber
                        myRow.Cells(0).Value = VB6.Format(oVismaDal.Reader_GetString("FrmNo"), "####")
                        myRow.Cells(0).ReadOnly = True

                        ' BankpartnerID
                        myRow.Cells(1).Value = oVismaDal.Reader_GetString("BPartNo")
                        myRow.Cells(1).ReadOnly = True

                        ' Profilegroupname
                        myRow.Cells(2).Value = oVismaDal.Reader_GetString("ProfileName")
                        myRow.Cells(2).ReadOnly = True

                        ' AccountNo
                        myRow.Cells(3).Value = oVismaDal.Reader_GetString("AccountNo")
                        myRow.Cells(3).ReadOnly = True

                        ' Firmname
                        myRow.Cells(4).Value = oVismaDal.Reader_GetString("ClientName")
                        myRow.Cells(4).ReadOnly = True

                        myRow.Cells(5).Value = oVismaDal.Reader_GetString("ClientId")
                        myRow.Cells(5).ReadOnly = True

                        .Rows.Add(myRow)
                    Loop
                End With
                Me.gridClients.Focus()
            End If
        End If
        oVismaDal.Close()
        oVismaDal = Nothing

        Exit Sub

VismaFillError:
        oVismaDal.Close()
        oVismaDal = Nothing

    End Sub


End Class