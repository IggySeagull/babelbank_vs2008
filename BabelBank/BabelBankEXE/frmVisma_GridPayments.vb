Option Strict Off
Option Explicit On
Friend Class frmVisma_GridPayments
    Inherits System.Windows.Forms.Form
    Private oBabelFiles As vbBabel.BabelFiles
    Private oBabelFile As vbBabel.BabelFile
    ' 27.05.2014 moved next 3 here
    Private oBatch As vbBabel.Batch
    Private oPayment As vbBabel.Payment
    Private oInvoice As vbBabel.Invoice

    Private iReturnCode As Short
    Private lClientRow As Integer
    Private lRow As Integer
    Private bUpdateSeveralRows As Boolean
    Private bInitialMode As Boolean
    Private bReturnMode As Boolean

    Private Const vblightred As Integer = &H8080FF 'lightsalmon
    Private Const vbLightGray As Integer = &H808080

    ' VISMA_TODO heila driden
    Private Sub frmVisma_GridPayments_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormLRSCaptions(Me)
        FormVismaStyle(Me)

        'If oBabelFiles.Item(1).TypeOfTransaction = vbBabel.BabelFiles.TypeOfTransaction.TransactionType_Outgoing Then
        If bReturnMode Then
            sprClientsPaymentsCreate(vbBabel.BabelFiles.TypeOfTransaction.TransactionType_Incoming)
            Me.cmdExport.Text = LRS("50709")  ' Importer filer
        Else
            sprClientsPaymentsCreate(vbBabel.BabelFiles.TypeOfTransaction.TransactionType_Outgoing)
            Me.cmdExport.Text = LRS("35110")  ' Export
        End If
        sprClientsPaymentsFill()

    End Sub
    Friend Sub ExportMode(ByRef bExp As Boolean)
        If bExp Then
            bReturnMode = False
        Else
            bReturnMode = True
        End If
    End Sub

    Friend WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            If Not IsReference(Value) Then
                RaiseInvalidObject()
            End If

            'Store reference
            oBabelFiles = Value
            If oBabelFiles.Count > 0 Then
                ' find out if we are importing returnfiles, or exporting;
                '        If Val(oBabelFiles(1).StatusCode) > 0 Then
                '            bReturnMode = True
                '        Else
                '            bReturnMode = False
                '        End If
            End If
        End Set
    End Property
    Friend Property BabelFile() As vbBabel.BabelFile
        Get
            BabelFile = oBabelFile
        End Get
        Set(ByVal Value As vbBabel.BabelFile)
            If Not IsReference(Value) Then
                RaiseInvalidObject()
            End If

            'Store reference
            oBabelFile = Value
            ' find out if we are importing returnfiles, or exporting;
            '    If Val(oBabelFile.StatusCode) > 0 Then
            '        bReturnMode = True
            '    Else
            '        bReturnMode = False
            '    End If

        End Set
    End Property
    Private Sub sprClientsPaymentsCreate(ByRef eTransactiontype As vbBabel.BabelFiles.TypeOfTransaction)
        Dim x As Boolean
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim checkBoxColumn As DataGridViewCheckBoxColumn
        Dim imgColumn As DataGridViewImageColumn


        With Me.gridClientPayments

            If .ColumnCount = 0 Then
                '.Height = 100

                .ScrollBars = ScrollBars.Both
                '.SelectionMode = DataGridViewSelectionMode.CellSelect
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.FixedSingle
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowsDefaultCellStyle.ForeColor = Color.Black
                .RowsDefaultCellStyle.SelectionForeColor = Color.Black
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .Font = New Font("Microsoft Sans Serif", 8, FontStyle.Regular)
                .ReadOnly = False
            End If


            '        ' Col 1 is a hidden col, showing if "Client" or "Payment"
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Visible = False
            .Columns.Add(txtColumn)


            '        ' Col 2 show Expand/Collapse icon, on Clientrows only
            '        .Col = 2
            '        .set_ColWidth(-2, 2)
            '        .Text = " "
            imgColumn = New DataGridViewImageColumn
            imgColumn.HeaderText = " "
            imgColumn.Width = WidthFromSpreadToGrid(1)
            imgColumn.Resizable = False
            imgColumn.DefaultCellStyle.NullValue = Nothing
            .Columns.Add(imgColumn)

            '        ' Col 3 is a checkbox, to select rows
            '        .Col = 3
            '        .set_ColWidth(-2, 3)
            '        .Text = " "
            '        If bReturnMode Then
            '            .ColHidden = True
            '        End If
            checkBoxColumn = New DataGridViewCheckBoxColumn
            With checkBoxColumn
                .HeaderText = ""
                .Width = WidthFromSpreadToGrid(2)
                .FlatStyle = FlatStyle.Standard
                .CellTemplate = New DataGridViewCheckBoxCell()
                .CellTemplate.Style.BackColor = Color.Beige
                .TrueValue = True
                .FalseValue = False
            End With
            .Columns.Add(checkBoxColumn)


            '        ' Col 4 is clientNo (F0001, F0002, etc)
            '        .Col = 4
            '        .Text = LRS(60149) ' Client
            '        .set_ColWidth(-2, 5)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60149) ' Client
            txtColumn.Width = WidthFromSpreadToGrid(6)
            .Columns.Add(txtColumn)


            '        ' col 5 fra konto
            '        .Col = 5
            '        .Text = Replace(LRS(35214), ":", "") ' V�r konto
            '        .set_ColWidth(-2, 11)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = Replace(LRS(35214), ":", "") ' V�r konto
            txtColumn.Width = WidthFromSpreadToGrid(8)
            .Columns.Add(txtColumn)


            '        ' col 6 betalingsdato
            '        .Col = 6
            '        .Text = LRS(60162) ' Date
            '        .set_ColWidth(-2, 7)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60162) ' Date
            txtColumn.Width = WidthFromSpreadToGrid(5)
            .Columns.Add(txtColumn)


            '        ' col 7 Name
            '        .Col = 7
            '        .Text = LRS(60201) ' Name
            '        .set_ColWidth(-2, 16)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60201) ' Name
            txtColumn.Width = WidthFromSpreadToGrid(10)
            .Columns.Add(txtColumn)

            '        ' col 8 til konto
            '        .Col = 8
            '        .Text = Replace(LRS(35215), ":", "") ' Konto
            '        .set_ColWidth(-2, 9)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = Replace(LRS(35215), ":", "") ' Konto
            txtColumn.Width = WidthFromSpreadToGrid(6)
            .Columns.Add(txtColumn)

            '        ' col 9 supplierno
            '        .Col = 9
            '        If bReturnMode Then
            '            .Text = LRS(35220) ' Arkivref
            '        Else
            '            .Text = LRS(35006) ' SupplierNo
            '            .set_ColWidth(-2, 6)
            '        End If
            txtColumn = New DataGridViewTextBoxColumn
            If bReturnMode Then
                txtColumn.HeaderText = LRS(35220) ' Arkivref
            Else
                txtColumn.HeaderText = LRS(35006) ' SupplierNo
            End If
            txtColumn.Width = WidthFromSpreadToGrid(6)
            .Columns.Add(txtColumn)


            '        ' col 10 Paymentref.
            '        .Col = 10
            '        .Text = LRS(60202) ' Reference
            '        .set_ColWidth(-2, 10)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60202) ' Reference
            txtColumn.Width = WidthFromSpreadToGrid(7)
            .Columns.Add(txtColumn)

            '        ' col 11 Currency
            '        .Col = 11
            '        .Text = LRS(60087) ' Currency
            '        .set_ColWidth(-2, 5)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60087) ' Currency
            txtColumn.Width = WidthFromSpreadToGrid(3)
            .Columns.Add(txtColumn)

            '        ' col 12 Amount
            '        .Col = 12
            '        .TypeHAlign = SS_CELL_H_ALIGN_RIGHT
            '        .Text = LRS(60086) 'Amount
            '        .set_ColWidth(-2, 10)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60086) 'Amount
            txtColumn.Width = WidthFromSpreadToGrid(6)
            txtColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns.Add(txtColumn)

            '        ' col 13 Status
            '        .Col = 13
            '        .Text = LRS(35008) ' Status
            '        .set_ColWidth(-2, 18)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(35008) ' Status
            txtColumn.Width = WidthFromSpreadToGrid(10)
            .Columns.Add(txtColumn)

            '        ' col 14 Type"
            '        .Col = 14
            '        .Text = LRS(35009) ' type
            '        .set_ColWidth(-2, 8)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(35009) ' type
            txtColumn.Width = WidthFromSpreadToGrid(5)
            .Columns.Add(txtColumn)

            '        ' col 15  PaymntLn/LnNo
            '        .Col = 15
            '        .Text = LRS(35010) ' PaymntLn/LnNo
            '        .set_ColWidth(-2, 5)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(35010) ' PaymntLn/LnNo
            txtColumn.Width = WidthFromSpreadToGrid(3)
            .Columns.Add(txtColumn)

            '        ' col 16  (hidden) E= Error, W = Warning, else blank
            '        .Col = 16
            '        .set_ColWidth(-2, 6)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.ReadOnly = True
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

            '        ' col 17  (hidden) BabelFileindex;BatchIndex;PaymentIndex
            '        .Col = 17
            '        .set_ColWidth(-2, 6)
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.ReadOnly = True
            txtColumn.Visible = False
            .Columns.Add(txtColumn)


        End With

    End Sub

    Private Sub sprClientsPaymentsFill()
        ' Fill up from table cxClients
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oFreetext As vbBabel.Freetext
        Dim sErrorString As String
        Dim sOldClientNo As String
        Dim sClientNo As String
        Dim bClientHasErrors As Boolean
        Dim bClientHasWarnings As Boolean
        Dim sTmp As String
        Dim sErrMsg As String
        Dim oErrorObject As vbBabel.ErrorObject
        Dim sOldCurrency As String
        Dim nTotalPrCurrency As Double
        Dim bLastWasCollapsed As Boolean
        Dim dClientAmount As Double
        Dim eOldTypeOfTransaction As vbBabel.BabelFiles.TypeOfTransaction

        On Error GoTo VismaFillError

        bInitialMode = True
        bUpdateSeveralRows = True
        lClientRow = -1

        bLastWasCollapsed = True ' in initial state detailrows are collapsed
        With Me.gridClientPayments
            .Rows.Clear()

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If Not oPayment.Exported Then ' we may have set exported=True in check for creditnotes
                            ' For Pain.001 and Camt.054, for outgoing payments, Ref_Own is constructed differently;
                            'If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.Pain002 Or _
                            'oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.Pain002_Rejected Or _
                            'oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.Camt054_Outgoing Or _
                            'oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.Camt054 Then
                            '    If oPayment.Invoices.Count > 0 Then
                            '        sClientNo = RemoveLeadingCharacters(oPayment.Invoices(1).REF_Own.Substring(26, 8), "0")
                            '    Else
                            '        sClientNo = RemoveLeadingCharacters(oPayment.REF_Own.Substring(26, 8), "0")
                            '    End If

                            'Else

                            ' 13.03.2017 - can't find clientno inside ref_own in Cremulfiles
                            'If oBabelFile.Visma_TypeOfPayment=vbBabel.BabelFiles.
                            If Not oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.CREMUL Then
                                If oPayment.Invoices.Count > 0 Then
                                    sClientNo = xDelim((oPayment.Invoices(1).REF_Own), ";", 1)
                                Else
                                    sClientNo = xDelim((oPayment.REF_Own), ";", 1)
                                End If
                            Else
                                sClientNo = ""
                            End If
                            'End If
                            If EmptyString(sClientNo) Then
                                sClientNo = oPayment.VB_ClientNo
                            End If

                            If oPayment.MON_InvoiceCurrency <> sOldCurrency Or sClientNo <> sOldClientNo Or oBabelFile.TypeOfTransaction <> eOldTypeOfTransaction Then
                                ' Break on currency, within same client
                                ' -------------------------------------
                                PresentCurrencyTotal(sOldCurrency, nTotalPrCurrency, sOldClientNo)
                                sOldCurrency = oPayment.MON_InvoiceCurrency
                                nTotalPrCurrency = 0
                            End If


                            ' Col 1 is a hidden col, showing if "Client" or "Payment"
                            '.Col = 1
                            If sClientNo <> sOldClientNo Or oBabelFile.TypeOfTransaction <> eOldTypeOfTransaction Then
                                ' Default to mark all
                                If sOldClientNo <> "" Then
                                    sprCheckAll(sOldClientNo, True)
                                End If

                                ' check if we have any errors or warnings for last client;
                                If lClientRow > -1 Then

                                    If bClientHasErrors Then
                                        ' mark clientline as red
                                        .Rows(lClientRow).DefaultCellStyle.BackColor = Color.Red
                                    ElseIf bClientHasWarnings Then
                                        ' mark clientline as light red
                                        .Rows(lClientRow).DefaultCellStyle.BackColor = Color.LightSalmon
                                    End If

                                    ' present totalamount for client;
                                    '									.Row = lClientRow
                                    '									' col 12 Amount
                                    '									.Col = 12
                                    '									.CellType = SS_CELL_TYPE_FLOAT
                                    '									.TypeFloatSeparator = True
                                    '									.Text = VB6.Format(dClientAmount / 100, "##,##0.00")
                                    '									dClientAmount = 0
                                    '									.Lock = True
                                    .Rows(lClientRow).Cells(11).Style.Format = "##,##0.00"
                                    .Rows(lClientRow).Cells(11).Value = dClientAmount
                                    dClientAmount = 0
                                    .Rows(lClientRow).Cells(11).ReadOnly = True

                                End If  'If lClientRow > 0 Then

                                ' add clientrow
                                ' -------------
                                '								.MaxRows = .MaxRows + 1
                                '								.Row = .MaxRows
                                '								.Col = 1
                                '								.Value = "Client;Collapsed"
                                .Rows.Add()
                                .Rows(.Rows.Count - 1).Cells(0).Value = "Client;Collapsed"

                                'sOldClientNo = oPayment.I_Client
                                ' 15.07.2016 - why not set sOldClientNo = ClientNo?
                                sOldClientNo = sClientNo
                                'sOldClientNo = ""
                                'If oPayment.Invoices.Count > 0 Then
                                '    sOldClientNo = xDelim((oPayment.Invoices(1).REF_Own), ";", 1)
                                'Else
                                '    sOldClientNo = xDelim((oPayment.REF_Own), ";", 1)
                                'End If
                                '' If no Ref_Own, like for Incoming payments, use .vb_clientNo
                                'If EmptyString(sOldClientNo) Then
                                '    sOldClientNo = oPayment.VB_ClientNo
                                'End If

                                lClientRow = .Rows.Count - 1
                                bClientHasErrors = False
                                bClientHasWarnings = False

                                .Rows(lClientRow).DefaultCellStyle.BackColor = Color.LightGray
                                .Rows(lClientRow).DefaultCellStyle.Font = New Font(Me.gridClientPayments.Font, FontStyle.Bold)
                                '								.Font = VB6.FontChangeBold(.Font, True)
                                '								.Font = VB6.FontChangeSize(.Font, 9)
                                '								.BlockMode = False

                                ' col 2 - expand/collapse
                                .Rows(lClientRow).Cells(1).Value = Me.imgPlus.Image

                                ' Col 3 is a checkbox, to select rows
                                '								.Col = 3
                                '								.CellType = FPSpread.CellTypeConstants.CellTypeCheckBox
                                '								.TypeCheckCenter = True
                                '								.Value = CStr(0)
                                '								.TypeButtonType = FPSpread.TypeButtonTypeConstants.TypeButtonTypeNormal
                                .Rows(lClientRow).Cells(2).Value = True  'checked

                                If bReturnMode Then
                                    .Rows(lClientRow).Cells(2).ReadOnly = True
                                End If


                                ' Col 4 is clientNo (1, 2, 9999, etc)
                                '								.Col = 4
                                '								If oPayment.Invoices.Count > 0 Then
                                '									.Text = xDelim((oPayment.Invoices(1).REF_Own), ";", 1)
                                '								Else
                                '									.Text = xDelim((oPayment.REF_Own), ";", 1)
                                '								End If
                                '								' If no Ref_Own, like for Incoming payments, use .vb_clientNo
                                '								If EmptyString(.Text) Then
                                '									.Text = oPayment.VB_ClientNo
                                '								End If
                                '								'.ForeColor = vbWhite
                                '								.Lock = True
                                'If oPayment.Invoices.Count > 0 Then
                                '    .Rows(lClientRow).Cells(3).Value = xDelim((oPayment.Invoices(1).REF_Own), ";", 1)
                                'Else
                                '    .Rows(lClientRow).Cells(3).Value = xDelim((oPayment.REF_Own), ";", 1)
                                'End If
                                '' If no Ref_Own, like for Incoming payments, use .vb_clientNo
                                'If EmptyString(.Rows(lClientRow).Cells(3).Value) Then
                                '    .Rows(lClientRow).Cells(3).Value = oPayment.VB_ClientNo
                                'End If
                                .Rows(lClientRow).Cells(3).Value = sClientNo
                                .Rows(lClientRow).Cells(3).ReadOnly = True

                                ' col 5 fra konto
                                .Rows(lClientRow).Cells(4).Value = oBabelFile.Visma_ProfileGroupName ' Profilegroupname
                                .Rows(lClientRow).Cells(4).ReadOnly = True

                                ' col 7 Name
                                '								.Col = 7
                                '								If bReturnMode Then
                                '									' can be more than one firm pr BabelFile, so we have put firmname into oPayment.I_Client
                                '									.Text = oPayment.I_Client
                                '								Else
                                '									.Text = oBabelFile.Visma_FrmName
                                '								End If
                                '								.Lock = True
                                If bReturnMode Then
                                    ' can be more than one firm pr BabelFile, so we have put firmname into oPayment.I_Client
                                    .Rows(lClientRow).Cells(6).Value = oPayment.I_Client
                                Else
                                    .Rows(lClientRow).Cells(6).Value = oBabelFile.Visma_FrmName
                                End If
                                .Rows(lClientRow).Cells(6).ReadOnly = True


                                '								' col 14 PaymentType
                                If bReturnMode Then
                                    '									.Col = 14
                                    '									.Lock = True
                                    '									.Text = GetEnumFileType((oBabelFile.ImportFormat))
                                    .Rows(lClientRow).Cells(13).Value = GetEnumFileType((oBabelFile.ImportFormat))
                                    .Rows(lClientRow).Cells(13).ReadOnly = True
                                End If


                            End If  'If sClientNo <> sOldClientNo Or oBabelFile.TypeOfTransaction <> eOldTypeOfTransaction Then

                            ' add detailline
                            ' --------------

                            ' New 07.06.2013 - must use Invoicelevel, because we may have added creditnotes
                            For Each oInvoice In oPayment.Invoices

                                If Not oInvoice.Exported Then
                                    .Rows.Add()
                                    lRow = .Rows.Count - 1

                                    '									.Col = 1
                                    '									.Value = "Payment"
                                    '									' Initial state is to not show the detaillines, before a client is expanded
                                    '									.RowHidden = True
                                    .Rows(lRow).Cells(0).Value = "Payment"
                                    .Rows(lRow).Visible = False


                                    ' Col 2 show Expand/Collapse icon, on Clientrows only
                                    '									.Col = 2
                                    '									' Mark with same color as client
                                    '									.BackColor = System.Drawing.ColorTranslator.FromOle(&H808080)
                                    '									.Lock = True
                                    .Rows(lRow).Cells(1).ReadOnly = True

                                    ' Col 3 is a checkbox, to select rows
                                    '									.Col = 3
                                    If bReturnMode Then
                                        .Rows(lRow).Cells(2).Value = True
                                    Else
                                        '										.CellType = FPSpread.CellTypeConstants.CellTypeCheckBox
                                        '										.TypeCheckCenter = True
                                        '										.TypeButtonType = FPSpread.TypeButtonTypeConstants.TypeButtonTypeNormal
                                        '										.Value = CStr(0)
                                        .Rows(lRow).Cells(2).Value = False
                                    End If

                                    ' Col 4 is clientNo (1, 2, 9999, etc)
                                    '.Rows(lRow).Cells(3).Value = oPayment.I_Client
                                    '' Find client from OwnRef
                                    'If oPayment.Invoices.Count > 0 Then
                                    '    .Rows(lRow).Cells(3).Value = xDelim((oPayment.Invoices(1).REF_Own), ";", 1)
                                    'Else
                                    '    .Rows(lRow).Cells(3).Value = xDelim((oPayment.REF_Own), ";", 1)
                                    'End If
                                    '' If no Ref_Own, like for Incoming payments, use .vb_clientNo
                                    'If EmptyString(.Rows(lRow).Cells(3).Value) Then
                                    '    .Rows(lRow).Cells(3).Value = oPayment.VB_ClientNo
                                    'End If
                                    .Rows(lRow).Cells(3).Value = sClientNo
                                    .Rows(lRow).Cells(3).ReadOnly = True


                                    ' col 5 fra konto
                                    .Rows(lRow).Cells(4).Value = oPayment.I_Account
                                    .Rows(lRow).Cells(4).ReadOnly = True

                                    ' col 6 betalingsdato
                                    .Rows(lRow).Cells(5).Value = CStr(StringToDate((oPayment.DATE_Payment)))
                                    .Rows(lRow).Cells(5).ReadOnly = True

                                    ' col 7 Name
                                    .Rows(lRow).Cells(6).Value = oPayment.E_Name
                                    .Rows(lRow).Cells(6).ReadOnly = True

                                    ' col 8 til konto
                                    .Rows(lRow).Cells(7).Value = oPayment.E_Account
                                    .Rows(lRow).Cells(7).ReadOnly = True

                                    ' col 9 supplierno/CustomerNo
                                    If bReturnMode Then
                                        ' show archiveref for incomings
                                        If Not EmptyString((oBatch.REF_Bank)) Then
                                            .Rows(lRow).Cells(8).Value = oBatch.REF_Bank
                                            '										ElseIf Not EmptyString((oPayment.REF_Bank1)) Then 
                                            .Rows(lRow).Cells(8).Value = oPayment.REF_Bank1
                                        Else
                                            .Rows(lRow).Cells(8).Value = oPayment.REF_Bank2
                                        End If
                                    Else
                                        .Rows(lRow).Cells(8).Value = oInvoice.CustomerNo
                                    End If
                                    .Rows(lRow).Cells(8).ReadOnly = True

                                    ' col 10 Paymentref.
                                    '									.Col = 10
                                    ' find in invoice
                                    .Rows(lRow).Cells(9).ReadOnly = True
                                    sTmp = ""
                                    If Len(sTmp) > 1 Then
                                        sTmp = sTmp & "/"
                                    End If
                                    If Trim(oInvoice.Unique_Id) <> "" Then
                                        sTmp = sTmp & oInvoice.Unique_Id
                                    ElseIf Trim(oInvoice.InvoiceNo) <> "" Then
                                        sTmp = sTmp & oInvoice.InvoiceNo
                                    Else
                                        ' freetext
                                        For Each oFreetext In oInvoice.Freetexts
                                            ' XOKNET 06.04.2016 to easier split freetext
                                            If Len(sTmp) > 1 Then
                                                sTmp = sTmp & "/"
                                            End If

                                            sTmp = sTmp & oFreetext.Text
                                        Next oFreetext
                                    End If
                                    .Rows(lRow).Cells(9).Value = sTmp

                                    ' col 11 Currency
                                    ' Vi summerer og tester med InvoiceCurrency i gridden, og b�r ogs� presentere InvoiceCurrency
                                    .Rows(lRow).Cells(10).Value = oPayment.MON_InvoiceCurrency
                                    .Rows(lRow).Cells(10).ReadOnly = True


                                    ' col 12 Amount
                                    '									.Text = VB6.Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00")
                                    .Rows(lRow).Cells(11).Style.Format = "##,##0.00"
                                    .Rows(lRow).Cells(11).Value = oInvoice.MON_InvoiceAmount / 100
                                    .Rows(lRow).Cells(11).ReadOnly = True

                                    nTotalPrCurrency = nTotalPrCurrency + (oInvoice.MON_InvoiceAmount / 100)
                                    dClientAmount = dClientAmount + (oInvoice.MON_InvoiceAmount / 100)

                                    ' Color red if error, pink if warning
                                    '									.Col = 1
                                    '									.Col2 = .MaxCols
                                    '									.Row = .MaxRows
                                    '									.row2 = .MaxRows
                                    '									.BlockMode = True
                                    '									sTmp = ""
                                    If HasErrors(oInvoice) Then
                                        '										.BackColor = System.Drawing.Color.Red
                                        .Rows(lRow).DefaultCellStyle.BackColor = Color.Red
                                        bClientHasErrors = True
                                        sTmp = "E"
                                    ElseIf HasWarnings(oInvoice) Then
                                        bClientHasWarnings = True
                                        '.BackColor = System.Drawing.Color.Red 'vblightred
                                        .Rows(lRow).DefaultCellStyle.BackColor = Color.LightSalmon
                                        sTmp = "W"
                                    End If


                                    ' col 13 Status
                                    '									.Col = 13
                                    '									.Lock = True
                                    '									.TypeMaxEditLen = 300
                                    '									' present errormsg
                                    If sTmp = "E" Or sTmp = "W" Then
                                        ' error
                                        sErrMsg = ""
                                        For Each oErrorObject In oInvoice.ErrorObjects
                                            sErrMsg = sErrMsg & oErrorObject.ErrorMessage & "/"
                                        Next oErrorObject
                                        .Rows(lRow).Cells(12).Value = sErrMsg
                                    Else
                                        .Rows(lRow).Cells(12).Value = "OK"
                                    End If
                                    .Rows(lRow).Cells(12).ReadOnly = True

                                    ' col 14 PaymentType
                                    .Rows(lRow).Cells(13).ReadOnly = True
                                    If bReturnMode Then
                                        If oBabelFile.TypeOfTransaction = vbBabel.BabelFiles.TypeOfTransaction.TransactionType_Outgoing Or _
                                        oBabelFile.TypeOfTransaction = vbBabel.BabelFiles.TypeOfTransaction.TransactionType_OutgoingAndIncoming Then
                                            ' Give status instead;
                                            If Val(oPayment.StatusCode) = 0 Then
                                                .Rows(lRow).Cells(13).Value = "OK"
                                            ElseIf Val(oPayment.StatusCode) = 1 And Val(oInvoice.StatusCode) = 1 Then
                                                .Rows(lRow).Cells(13).Value = LRS(35103) ' Mottaksretur
                                            ElseIf Val(oPayment.StatusCode) = 2 And Val(oInvoice.StatusCode) = 2 Then
                                                .Rows(lRow).Cells(13).Value = LRS(35197) ' Avregning
                                            Else
                                                .Rows(lRow).Cells(13).Value = LRS(35229) ' Avvisning
                                            End If
                                        Else
                                            ' Incoming payments
                                            .Rows(lRow).Cells(13).Value = LRS(35257) ' Innbetalinger
                                        End If

                                    Else
                                        .Rows(lRow).Cells(13).Value = oPayment.ExtraD3 ' PaymentTypes from View
                                    End If

                                    ' col 15 PaymntLn/LnNo
                                    .Rows(lRow).Cells(14).Value = xDelim((oInvoice.REF_Own), ";", 3) & "/" & xDelim((oInvoice.REF_Own), ";", 4)
                                    .Rows(lRow).Cells(14).ReadOnly = True

                                    ' Col 16 E= Error W = Warning
                                    .Rows(lRow).Cells(15).Value = sTmp


                                    ' col 17  (hidden) BabelFileindex;BatchIndex;PaymentIndex
                                    .Rows(lRow).Cells(16).Value = LTrim(Str(oBabelFile.Index)) & ";" & LTrim(Str(oBatch.Index)) & ";" & LTrim(Str(oPayment.Index)) & ";" & LTrim(Str(oInvoice.Index))
                                    eOldTypeOfTransaction = oBabelFile.TypeOfTransaction

                                End If  'If Not oInvoice.Exported Then
                            Next oInvoice
                        End If 'If Not oPayment.Exported Then        ' we may have set exported=True in check for creditnotes
                    Next oPayment
                Next oBatch
            Next oBabelFile

            ' For last client, mark with color, and check
            ' Default to mark all
            sprCheckAll(sOldClientNo, True)
            ' present last currencytotal for last client
            PresentCurrencyTotal(sOldCurrency, nTotalPrCurrency, sOldClientNo)
            sOldCurrency = ""
            nTotalPrCurrency = 0

            ' check if we have any errors or warnings for last client;
            ' taken care of in sprChkAll
            If lClientRow > -1 Then

                If bClientHasErrors Then
                    ' mark clientline as red
                    .Rows(lClientRow).DefaultCellStyle.BackColor = Color.Red
                ElseIf bClientHasWarnings Then
                    ' mark clientline as light red
                    .Rows(lClientRow).DefaultCellStyle.BackColor = Color.LightSalmon
                Else
                    ' mark clientline as gray
                    '.Rows(lClientRow).DefaultCellStyle.BackColor = Color.Gray

                End If  'If bClientHasErrors Then

                ' present totalamount for client;
                .Rows(lClientRow).Cells(11).Style.Format = "##,##0.00"
                .Rows(lClientRow).Cells(11).Value = dClientAmount
                .Rows(lClientRow).Cells(11).ReadOnly = True

                dClientAmount = 0

            End If  'If lClientRow > 0 Then
            ' no selected row:
            .Rows(0).Selected = False

            .Focus()

        End With
        bInitialMode = False


VismaFillError:


    End Sub
    Private Sub gridClientPayments_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridClientPayments.RowEnter
        'update statusline with errortext
        With Me.gridClientPayments
            If .Rows(e.RowIndex).Cells(15).Value = "E" Then
                Me.lblStatus.Visible = True
                Me.lblStatus.BackColor = System.Drawing.Color.Red
                Me.lblStatus.Text = .Rows(e.RowIndex).Cells(12).Value.ToString
            ElseIf .Rows(e.RowIndex).Cells(15).Value = "W" Then
                Me.lblStatus.Visible = True
                Me.lblStatus.BackColor = System.Drawing.Color.LightSalmon
                Me.lblStatus.Text = .Rows(e.RowIndex).Cells(12).Value.ToString
            Else
                Me.lblStatus.Visible = False
            End If

        End With

    End Sub
    Private Sub gridClientPayments_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridClientPayments.CellClick
        Dim lCounter As Integer
        Dim sValue As String
        Dim lCurrentRow As Integer

        With Me.gridClientPayments
            If bUpdateSeveralRows = False And bInitialMode = False Then


                If e.RowIndex > -1 And e.ColumnIndex = 2 Then
                    ' clicked a checkbox
                    ' ------------------
                    ' test if this is a checkbox in Clientlevel, or detaillevel
                    '                .eventArgs.Row = eventArgs.row
                    '                .eventArgs.Col = 1
                    sValue = .Rows(e.RowIndex).Cells(0).Value
                    ' is this a Clientsrow ?
                    If xDelim(sValue, ";", 1) = "Client" Then
                        '                    ' Clientsrow - is it previously checked or unchecked ?
                        '                    .eventArgs.Col = 3 ' Checkbox
                        If .Rows(e.RowIndex).Cells(2).Value = False Then
                            ' was unchecked, no clicked, then mark all below
                            '                        .eventArgs.Col = 4
                            sValue = .Rows(e.RowIndex).Cells(3).Value
                            sprCheckAll(sValue, True)
                            bUpdateSeveralRows = True ' to not call buttonclicked several times, looping forever
                            .Rows(e.RowIndex).Cells(2).Value = True
                            bUpdateSeveralRows = False
                        Else
                            ' was checked, now clicked, then unmark all for this client
                            sValue = .Rows(e.RowIndex).Cells(3).Value
                            sprCheckAll(sValue, False)
                            bUpdateSeveralRows = True ' to not call buttonclicked several times, looping forever
                            .Rows(e.RowIndex).Cells(2).Value = False
                            bUpdateSeveralRows = False
                        End If
                    ElseIf xDelim(sValue, ";", 1) = "Payment" Then
                        ' detaillines
                        ' not allowed to check errormarked lines
                        ' check if errormark
                        If .Rows(e.RowIndex).Cells(15).Value = "E" Then
                            'Beep()
                            .Rows(e.RowIndex).Cells(2).Value = False ' Uncheck!
                            bUpdateSeveralRows = False
                        End If
                    End If
                End If

                If e.RowIndex > -1 And e.ColumnIndex = 1 Then
                    ' this is a click in the +/- PictureCell
                    ' Expand or collapse detaillines pr client
                    ' must set active cell to where we Clicked
                    '                .eventArgs.Row = eventArgs.row
                    '                .eventArgs.Col = 1
                    sValue = .Rows(e.RowIndex).Cells(0).Value
                    ' is this a Clientsrow ?
                    If xDelim(sValue, ";", 1) = "Client" Then
                        ' Clientsrow - is it expanded or collapsed?
                        If xDelim(sValue, ";", 2) = "Collapsed" Then
                            ' then expand, that is show each detailrow for this client
                            sValue = .Rows(e.RowIndex).Cells(3).Value
                            sprCollapse(sValue, True)

                            ' change content of cell 1
                            sValue = "Expanded"
                            ' change content of cell
                            .Rows(e.RowIndex).Cells(0).Value = "Client;" & sValue

                            ' set focus back to +/- cell;
                            ' change image to -
                            .Rows(e.RowIndex).Cells(1).Value = Me.imgMinus.Image
                        Else
                            ' collapse
                            sValue = .Rows(e.RowIndex).Cells(3).Value
                            sprCollapse(sValue, False)
                            ' change content of cell 1
                            sValue = "Collapsed"
                            ' change content of cell
                            .Rows(e.RowIndex).Cells(0).Value = "Client;" & sValue


                            ' set focus back to +/- cell;
                            ' change image to +
                            .Rows(e.RowIndex).Cells(1).Value = Me.imgPlus.Image
                        End If


                    Else
                        ' detailsrow, do nothing !
                    End If  'If xDelim(sValue, ";", 1) = "Client" Then
                End If  'e.RowIndex > -1 And e.ColumnIndex = 1 Then
            End If

            End With

    End Sub
    ' Denne koden fanger vi sannsynligvis i Sub gridClientPayments_CellClick - se over ! 
    'Private Sub sprClientsPayments_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxFPSpread._DSpreadEvents_ClickEvent)
    '    Dim lCounter As Integer
    '    Dim sValue As String
    '    Dim lCurrentRow As Integer

    '    If bUpdateSeveralRows = False And bInitialMode = False Then
    '        With sprClientsPayments

    '            If eventArgs.row > 0 And eventArgs.col = 2 Then
    '                ' this is a PictureCell - hopefully click event goes here !
    '                ' Expand or collapse detaillines pr client
    '                ' must set active cell to where we Clicked
    '                .eventArgs.Row = eventArgs.row
    '                .eventArgs.Col = 1
    '                sValue = .Text
    '                ' is this a Clientsrow ?
    '                If xDelim(sValue, ";", 1) = "Client" Then
    '                    ' Clientsrow - is it expanded or collapsed?
    '                    If xDelim(sValue, ";", 2) = "Collapsed" Then
    '                        ' then expand, that is show each detailrow for this client
    '                        .eventArgs.Col = 4
    '                        sValue = .Text
    '                        sprCollapse(sValue, False)

    '                        ' change content of cell 1
    '                        sValue = "Expanded"
    '                        .eventArgs.Col = 1
    '                        .eventArgs.Row = eventArgs.row
    '                        ' change content of cell
    '                        .Text = "Client;" & sValue

    '                        ' set focus back to +/- cell;
    '                        .eventArgs.Row = eventArgs.row
    '                        .eventArgs.Col = eventArgs.col
    '                        .Action = FPSpread.ActionConstants.ActionActiveCell
    '                        ' change image to -
    '                        .TypePictPicture = Me.imgMinus.Image
    '                        .TypePictCenter = True

    '                    Else
    '                        ' collapse
    '                        .eventArgs.Col = 4
    '                        sValue = .Text
    '                        sprCollapse(sValue, True)
    '                        ' change content of cell 1
    '                        sValue = "Collapsed"
    '                        .eventArgs.Col = 1
    '                        .eventArgs.Row = eventArgs.row
    '                        ' change content of cell
    '                        .Text = "Client;" & sValue


    '                        ' set focus back to +/- cell;
    '                        .eventArgs.Row = eventArgs.row
    '                        .eventArgs.Col = eventArgs.col
    '                        .Action = FPSpread.ActionConstants.ActionActiveCell
    '                        ' change image to +
    '                        .TypePictPicture = Me.imgPlus.Image
    '                        .TypePictCenter = True

    '                    End If


    '                Else
    '                    ' detailsrow, do nothing !
    '                End If
    '            End If

    '        End With
    '    End If

    'End Sub
    Private Sub sprCollapse(ByRef sClient As String, ByRef bExpand As Boolean)
        ' show or hide all detaillines for this client
        Dim lRowCounter As Integer
        Dim sValue As String
        Dim lInitialRow As Integer
        Dim lInitialCol As Integer

        bUpdateSeveralRows = True

        With Me.gridClientPayments
            lInitialRow = .CurrentRow.Index
            lInitialCol = .CurrentCell.ColumnIndex


            For lRowCounter = 0 To .Rows.Count - 1
                If lRowCounter > lInitialRow Then
                    If .Rows(lRowCounter).Cells(0).Value = "Payment" Or .Rows(lRowCounter).Cells(0).Value = "Currency" Then
                        sValue = .Rows(lRowCounter).Cells(3).Value  'ClientNo
                        If sValue = sClient Then
                            ' expand (show)/ or hide
                            .Rows(lRowCounter).Visible = bExpand
                        End If
                    Else
                        Exit For ' stop expanding/collapsing when we come to a new clientsrow
                    End If
                End If
            Next
            '    ' reset to where we where
            .Rows(lInitialRow).Cells(lInitialCol).Selected = True

        End With
        bUpdateSeveralRows = False
    End Sub
    Private Sub sprCheckAll(ByRef sClient As String, ByRef bCheck As Boolean)
        ' check or uncheck all for client
        ' if sClient = "", then check, regardless for client
        Dim lRowCounter As Integer
        Dim sValue As String
        Dim sRowType As String
        Dim bAllErrorMarkedForThisClient As Boolean ' must check if all payments are errormarked for client, then uncheck at clientrow as well
        Dim lClientRow As Integer
        Dim sPreviousClient As String
        Dim lInitialRow As Integer
        Dim lInitialCol As Integer

        bUpdateSeveralRows = True

        With Me.gridClientPayments
            lInitialRow = .CurrentRow.Index
            lInitialCol = .CurrentCell.ColumnIndex

            For lRowCounter = 0 To .Rows.Count - 1
                '        .Row = lRowCounter
                '        .Col = 1
                sRowType = .Rows(lRowCounter).Cells(0).Value
                If xDelim(sRowType, ";", 1) = "Client" Then
                    ' check for previous client if all rows are errormarked;
                    If bCheck Then
                        ' new client, set some values
                        bAllErrorMarkedForThisClient = True
                        sPreviousClient = .Rows(lRowCounter).Cells(3).Value
                        lClientRow = lRowCounter
                    End If
                End If
                If sRowType = "Payment" Or sRowType = "Currency" Then
                    sValue = .Rows(lRowCounter).Cells(3).Value 'Clientno
                    If sValue = sClient Or sClient = "" Then
                        ' check or uncheck
                        'If sRowType <> "Currency" Then
                        '                    .Col = 3 ' Checkbox
                        If bCheck Then
                            If sRowType = "Payment" Or sRowType = "Currency" Then
                                ' If errormarked, then there is not possible to Check this payment
                                '                            .Col = 16
                                If .Rows(lRowCounter).Cells(15).Value = "E" Then '- error !
                                    .Rows(lRowCounter).Cells(2).Value = False
                                Else
                                    .Rows(lRowCounter).Cells(2).Value = True
                                    bAllErrorMarkedForThisClient = False
                                End If

                            End If
                        Else
                            .Rows(lRowCounter).Cells(2).Value = False
                        End If
                    End If
                End If
            Next

            ' check for last client if all rows are errormarked;
            If bCheck Then
                If lClientRow > 0 Then
                    ' we had a previous client
                    '            .Row = lClientRow
                    '            .Col = 3
                    If bAllErrorMarkedForThisClient Then
                        ' all payments where errormarked for this client, set Selectedflag = False
                        .Rows(.RowCount - 1).Cells(2).Value = False ' Unselect
                    Else
                        ' one or more payments are OK, check
                        .Rows(.RowCount - 1).Cells(2).Value = True ' Select
                    End If
                End If
            End If
            '    ' reset to where we where
            .Rows(lInitialRow).Cells(lInitialCol).Selected = True
        End With
        bUpdateSeveralRows = False

    End Sub
    Private Function HasErrors(ByRef oInvoice As vbBabel.Invoice) As Boolean
        ' find out if there are one or more errors
        Dim oErrorObject As vbBabel.ErrorObject
        Dim bError As Boolean
        bError = False
        If oInvoice.ErrorObjects.Count = 0 Then
            bError = False
        Else

            For Each oErrorObject In oInvoice.ErrorObjects
                If oErrorObject.ErrorType = "Error" Then
                    bError = True
                    Exit For
                End If
            Next oErrorObject
        End If
        HasErrors = bError
    End Function
    Private Function HasWarnings(ByRef oInvoice As vbBabel.Invoice) As Boolean
        ' find out if there are one or more warnings, but no errors
        Dim oErrorObject As vbBabel.ErrorObject
        Dim bWarning As Boolean
        bWarning = False
        If oInvoice.ErrorObjects.Count = 0 Then
            bWarning = False
        Else
            For Each oErrorObject In oInvoice.ErrorObjects
                If oErrorObject.ErrorType = "Warning" Then
                    bWarning = True
                    Exit For
                End If
            Next oErrorObject
        End If
        HasWarnings = bWarning
    End Function
    Private Sub cmdCheckAll_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)

        sprCheckAll("", True)
    End Sub
    Private Sub cmdUncheckAll_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        sprCheckAll("", False)
    End Sub
    Private Sub cmdExport_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExport.Click
        Dim lRowCounter As Integer
        Dim sREF As String
        Dim nPaymentTotal As Double
        Dim nPaymentTransTotal As Double
        Dim bAllPositive As Boolean
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oErrorObject As vbBabel.ErrorObject
        Dim iCount As Short
        Dim bAllExported As Boolean
        Dim bThereAreErrors As Boolean
        Dim bAllUnMarked As Boolean ' added 15.05.2014

        ' Run through all payments, and set as exported those who are unchecked

        ' Update status-field at InvoiceLevel;
        ' ------------------------------------
        'o   Status-felt invoice;
        '   -   Alt OK (ikke error, ikke warning) and selected: "00"
        '   -   Warning and selected:    "90"
        '   -   Alt OK and deselected: "99"
        '   -   Warning and deselected: "98"
        '   -   Error (og deselected): "97"
        ' ------------------------------------

        bAllUnMarked = True

        If Not bReturnMode Then
            ' for sendfiles only;

            'VISMA_TODO
            With Me.gridClientPayments
                For lRowCounter = 0 To .Rows.Count - 1
                    '		.Row = lRowCounter
                    '		.Col = 1
                    If .Rows(lRowCounter).Cells(0).Value = "Payment" Then
                        sREF = .Rows(lRowCounter).Cells(16).Value
                        oPayment = oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3)))
                        oInvoice = oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).Invoices(Val(xDelim(sREF, ";", 4)))

                        '.Col = 3 ' Checkbox
                        If .Rows(lRowCounter).Cells(2).Value = True Then
                            ' Selected
                            ' --------
                            If oInvoice.ErrorObjects.Count = 0 Then
                                '   -   Alt OK (ikke error, ikke warning) and selected: "00"
                                'oInvoice.StatusCode = "00"
                                ' 10.01.2014 endret
                                oInvoice.Visma_StatusCode = "00"
                            Else
                                '   -   Warning and selected:    "90"
                                'oInvoice.StatusCode = "90"
                                ' 10.01.2014 endret
                                oInvoice.Visma_StatusCode = "90"
                            End If
                            oPayment.Exported = False
                            oInvoice.Exported = False

                            ' added 15.05.2014 to be able to check if all payments are unchecked, no payments to export
                            bAllUnMarked = False
                        Else
                            ' Unselected
                            ' ----------
                            If oInvoice.ErrorObjects.Count = 0 Then
                                '   -   Alt OK and deselected: "99"
                                'oInvoice.StatusCode = "99"
                                ' 10.01.2014 endret
                                oInvoice.Visma_StatusCode = "99"
                            Else
                                ' find out if any errors, run through errorobjects
                                bThereAreErrors = False
                                For Each oErrorObject In oInvoice.ErrorObjects
                                    If oErrorObject.ErrorType = "Error" Then
                                        bThereAreErrors = True
                                        Exit For
                                    End If
                                Next oErrorObject
                                If bThereAreErrors Then
                                    '   -   Error (og deselected): "97"
                                    'oInvoice.StatusCode = "97"
                                    ' 10.01.2014 endret
                                    oInvoice.Visma_StatusCode = "97"
                                Else
                                    '   -   Warnings only and deselected: "98"
                                    'oInvoice.StatusCode = "98"
                                    ' 10.01.2014 endret
                                    oInvoice.Visma_StatusCode = "98"
                                End If
                            End If
                            oInvoice.Exported = True
                        End If

                    End If
                Next lRowCounter
            End With


            ' Do a recheck to see if we have negative payments
            ' if negative payments, must present grid again
            bAllPositive = True
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        nPaymentTotal = 0
                        nPaymentTransTotal = 0 ' added 27.05.2014

                        If Not oPayment.Exported Then
                            For Each oInvoice In oPayment.Invoices
                                If Not oInvoice.Exported Then
                                    nPaymentTotal = nPaymentTotal + oInvoice.MON_InvoiceAmount
                                    nPaymentTransTotal = nPaymentTransTotal + oInvoice.MON_TransferredAmount
                                End If
                            Next oInvoice
                            If nPaymentTotal < 0 Then
                                ' remove previous warnings about negatives
                                For Each oInvoice In oPayment.Invoices
                                    iCount = 0
                                    For Each oErrorObject In oInvoice.ErrorObjects
                                        iCount = iCount + 1
                                        If oErrorObject.PropertyName = "MON_InvoiceAmount" And oErrorObject.ErrorType = "Warning" Then
                                            oInvoice.ErrorObjects.Remove((iCount))
                                        End If
                                    Next oErrorObject
                                Next oInvoice

                                ' 27.05.2014
                                ' Update oPayment amountfields, because we may have deleted some invoices from payment
                                oPayment.MON_InvoiceAmount = nPaymentTotal
                                oPayment.MON_TransferredAmount = nPaymentTransTotal


                                ' XNET 16.12.2014
                                ' For Leverant�rbetalningar (Bankgirot SE) we now allow negative single payments to "bevakning"
                                ' Cargo.Temp holds the exportformat at this stage
                                If oPayment.MATCH_DifferenceExact = True And oBabelFile.ExportFormat = vbBabel.BabelFiles.FileType.Leverantorsbetalningar Then
                                    ' no test for creditnotes
                                Else
                                    bAllPositive = False
                                    ' errormark all invoices in payment
                                    For Each oInvoice In oPayment.Invoices
                                        oErrorObject = oInvoice.ErrorObjects.Add
                                        oErrorObject.PropertyName = "MON_InvoiceAmount" ' - Propertyname  - Which field in collection has error
                                        oErrorObject.PropertyValue = Str(nPaymentTotal) ' - PropertyValue - Value for the errounous property
                                        oErrorObject.ErrorMessage = LRS(35098) ' Betalingen har negativt bel�p
                                        oErrorObject.CollectionLevel = "Payment" ' - CollectionLevel - Level in collection (BabelFile, Batch, Payment, Invoice, Freetext)
                                        oErrorObject.ErrorType = "Warning" ' Warning, eller s� kommer vi aldri videre !
                                    Next oInvoice
                                End If
                            End If
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

            If bAllPositive Then

                ' for those payments with more than one invoice, check if all invoices are "unchecked" (oInvoice.Exported = False)
                ' if so, must then also set oPayment.Exported = True
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.Invoices.Count > 0 Then
                                bAllExported = True
                                For Each oInvoice In oPayment.Invoices
                                    If oInvoice.Exported = False Then
                                        bAllExported = False
                                    End If
                                Next oInvoice
                                If bAllExported Then
                                    ' all invoices for this payment are exported - set oPayment.Exported = True
                                    oPayment.Exported = True
                                End If
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabelFile

                Me.Hide()
                iReturnCode = 1
            Else
                ' otherwise, note that there are credits not covered
                'MsgBox "There are payments with negative amounts, see statuscolumn", vbCritical, "BabelBank"
                MsgBox(LRS(35098), MsgBoxStyle.Critical, "Visma Payment (BabelBank)")
                ' m� fylle opp igjen gridden;
                ' First, remove all Exported-flags;
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            For Each oInvoice In oPayment.Invoices
                                oInvoice.Exported = False
                                ' 10.01.2014 endret
                                'oInvoice.StatusCode = "00"
                                oInvoice.Visma_StatusCode = "00"
                            Next oInvoice
                        Next oPayment
                    Next oBatch
                Next oBabelFile
                sprClientsPaymentsFill()
            End If

            ' added 15.05.2014
            If bAllUnMarked Then
                iReturnCode = -8
            End If
        Else
            ' for returnfiles
            iReturnCode = 1
            Me.Hide()
        End If

    End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        ' End this profile
        Dim sText As String

        If bReturnMode Then
            sText = LRS(35135) '"Vil du avbryte, uten import?"
        Else
            sText = LRS(35116) '"Vil du avbryte, uten eksport?"
        End If
        If MsgBox(sText, MsgBoxStyle.YesNo, "Visma Payment (BabelBank)") = MsgBoxResult.Yes Then
            Me.Hide()
            iReturnCode = 0
        End If

    End Sub
    Private Sub cmdCancelAll_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        ' End all profiles

        'If MsgBox("Vil du avbryte all videre prosessering, uten eksport?", vbYesNo, "BabelBank") = vbYes Then
        If MsgBox(LRS(35117), MsgBoxStyle.YesNo, "Visma Payment (BabelBank)") = MsgBoxResult.Yes Then
            Me.Hide()
            iReturnCode = -9
        End If

    End Sub

    Public Function DialogResult() As Short
        DialogResult = iReturnCode
    End Function
    Private Sub cmdReportError_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        ' VISMA TODO
        Dim oReport As New vbBabel.Report
        Dim oBabelReport As New vbBabel.BabelReport
        Dim oInvoice As vbBabel.Invoice
        Dim oPayment As vbBabel.Payment
        Dim oBatch As vbBabel.Batch
        Dim oErrorObject As vbBabel.ErrorObject
        Dim sStatusCode As String
        Dim sErrorText As String

        Dim sREF As String
        Dim lRowCounter As Integer
        Dim bNothingToShow As Boolean
        bNothingToShow = True
        '------------------------------------------------------------------
        ' Present report with errormarked payments
        ' Run through all payments, and mark the errorvalidated
        '------------------------------------------------------------------
        On Error GoTo Err_Renamed


        If bReturnMode Then
            ' import of returnfiles;
            For Each oBabelFile In oBabelFiles

                ' mark payments with statuscode>02 as errors
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        sStatusCode = "0"
                        If Val(oPayment.StatusCode) > 2 Then
                            sStatusCode = oPayment.StatusCode
                        End If
                        For Each oInvoice In oPayment.Invoices
                            If Val(oInvoice.StatusCode) > 2 Then
                                sStatusCode = oInvoice.StatusCode
                            End If
                            Select Case oBabelFile.ImportFormat
                                Case vbBabel.BabelFiles.FileType.Telepay2, vbBabel.BabelFiles.FileType.TelepayTBIO
                                    sErrorText = TelepayStatusTxts(sStatusCode)
                                Case vbBabel.BabelFiles.FileType.Leverantorsbetalningar
                                    sErrorText = LevBetStatusTxts(sStatusCode)
                                Case Else
                                    sErrorText = sStatusCode
                            End Select

                            ' if error, add to Errorobject
                            If Val(sStatusCode) > 2 Then
                                ' take error from invoicelevel if present, otherwise from payment;
                                oErrorObject = oInvoice.ErrorObjects.Add
                                oErrorObject.PropertyName = "" ' - Propertyname  - Which field in collection has error
                                oErrorObject.PropertyValue = "" ' - PropertyValue - Value for the errounous property
                                oErrorObject.ErrorMessage = sErrorText
                                oErrorObject.CollectionLevel = "Invoice" ' - CollectionLevel - Level in collection (BabelFile, Batch, Payment, Invoice, Freetext)
                                oErrorObject.ErrorType = "Warning" ' Warning, eller s� kommer vi aldri videre !
                            End If

                            If Val(sStatusCode) > 2 Then
                                oPayment.ToSpecialReport = True
                                ' may have more than one invoice pr payment, due to creditnotes;
                                oInvoice.ToSpecialReport = True
                                bNothingToShow = False
                            End If
                        Next oInvoice
                    Next oPayment
                Next oBatch

                'Else
                ' TODO Visma JP - must interpret returncodes for other formats here
                'End If
            Next oBabelFile
        Else
            ' exportfile
            'VISMA_TODO
            'With sprClientsPayments
            '	For lRowCounter = 1 To .MaxRows
            '		.Row = lRowCounter
            '		.Col = 1
            '		If .Text = "Payment" Then
            '			.Row = lRowCounter
            '			.Col = 16 ' "E" if errormarked
            '			If .Value = "E" Then
            '				' Errormarked, set to ToSpecialReport
            '				.Col = 17
            '				.Row = lRowCounter
            '				sREF = .Text
            '				oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).ToSpecialReport = True
            '				' may have more than one invoice pr payment, due to creditnotes;
            '				oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).Invoices.Item(Val(xDelim(sREF, ";", 4))).ToSpecialReport = True
            '				bNothingToShow = False
            '			Else
            '				' OK payment, DO NOT set to ToSpecialReport
            '				.Col = 17
            '				.Row = lRowCounter
            '				sREF = .Text
            '				oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).ToSpecialReport = False
            '				' may have more than one invoice pr payment, due to creditnotes;
            '				oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).Invoices.Item(Val(xDelim(sREF, ";", 4))).ToSpecialReport = False

            '			End If
            '		End If
            '	Next lRowCounter
            'End With
        End If

        If bNothingToShow = False Then
            ' activate report, call report 992
            oReport.ReportNo = CShort("992") ' Specialreport Visma
            oReport.Preview = True
            oReport.Heading = LRS(35013) & " - " & LRS(35206) '"Feilmarkerte betalinger - Kladd"
            oReport.Printer = False
            oReport.AskForPrinter = False
            oReport.EMail = False
            oReport.ExportType = ""
            oReport.ExportFilename = ""
            'Sideskift ved ny konto - [vbsys].[dbo].[BB_ETFInf].SplitReportPerAccount Hvis 1 then True else False [vbsys].[dbo].[BB_ETFInf].BankProc BITVERDI 8 i SQL case when BankProc &32 = 32 THEN True Else False
            'If bSplitReportPerAccount Then
            oReport.BreakLevel = 4
            'Else
            'oReport.BreakLevel = 0
            'End If
            oReport.TotalLevel = 3
            oReport.DetailLevel = 0
            oReport.BitmapFilename = ""
            oReport.PrinterName = ""
            oReport.LongDate = False

            'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            oBabelReport = New vbBabel.BabelReport

            'VISMA_TODO
            'oBabelReport.Report = oReport
            oBabelReport.BabelFiles = oBabelFiles
            oBabelReport.Special = "ErrorReport"
            'oBabelReport.start()


            oReport = Nothing
            oBabelReport = Nothing

            ' Must reset ToSpecialReport;
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.ToSpecialReport = False
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.ToSpecialReport = False
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile
        Else
            ' found no paymentes to report;
            MsgBox(LRS(35207), MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Visma Payment (BabelBank)") ' No payments found
        End If

        Exit Sub

Err_Renamed:
        oReport = Nothing
        oBabelReport = Nothing

    End Sub

    Private Sub PresentCurrencyTotal(ByRef sOldCurrency As String, ByRef nTotalPrCurrency As Double, ByRef sClient As String)
        Dim lRow As Long
        ' create a line with totals for this currency

        If Not sOldCurrency Is Nothing Then
            With gridClientPayments
                .Rows.Add()
                lRow = .RowCount - 1
                .Rows(lRow).ReadOnly = True
                .Rows(lRow).Visible = False  ' Inital state not visible
                .Rows(lRow).DefaultCellStyle.Font = New Font(Me.gridClientPayments.Font, FontStyle.Bold)

                '	' Initial state is to not show the detaillines, before a client is expanded
                '	.RowHidden = True
                '	.Col = 1
                '	.Text = "Currency"
                .Rows(lRow).Cells(0).Value = "Currency"


                '	.Col = 2
                '	' Mark with same color as client
                '	.BackColor = System.Drawing.ColorTranslator.FromOle(&H808080)
                '	.Lock = True

                '	' Col 3 is normally a checkbox, to select rows
                '	' Here, use a normal cell
                '	.Col = 3

                .Rows(lRow).Cells(2).Value = True  'checked

                '	.CellType = SS_CELL_TYPE_STATIC_TEXT
                '	.TypeCheckCenter = True
                '	.Value = " "
                '	.Lock = True
                .Rows(lRow).Cells(10).ReadOnly = True

                '	' Col 4 is clientNo (F0001, F0002, etc)
                '	.Col = 4
                '	.Value = sClient
                '	.Lock = True
                .Rows(lRow).Cells(3).Value = sClient
                .Rows(lRow).Cells(3).ReadOnly = True

                '	' col 11 Currency
                '	.Col = 11
                '	.Font = VB6.FontChangeBold(.Font, True)
                '	.Value = sOldCurrency
                '	.Lock = True
                .Rows(lRow).Cells(10).Value = sOldCurrency
                .Rows(lRow).Cells(10).ReadOnly = True

                '	' col 12 Amount
                '	.Col = 12
                '	.CellType = SS_CELL_TYPE_FLOAT
                '	.TypeFloatSeparator = True
                '	.Font = VB6.FontChangeBold(.Font, True)
                '	.Text = VB6.Format(nTotalPrCurrency / 100, "##,##0.00")
                '	.Lock = True
                .Rows(lRow).Cells(11).Value = VB6.Format(nTotalPrCurrency, "##,##0.00")
                .Rows(lRow).Cells(10).ReadOnly = True


            End With
        End If

    End Sub
    'Private Sub frmVisma_GridPayments_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    '    Dim Cancel As Boolean = eventArgs.Cancel
    '    Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
    '    ' trap user clicking the X in upper right corner
    '    Cancel = True
    '    cmdCancel_Click(cmdCancel, New System.EventArgs())
    '    eventArgs.Cancel = Cancel
    'End Sub

    Private Sub mnuReportTotal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oBabelReport As New vbBabel.BabelReport
        Dim sREF As String
        Dim lRowCounter As Integer
        Dim i As Short
        Dim oBabelFile As vbBabel.BabelFile
        Dim bNothingToShow As Boolean
        Dim dInvoiceAmount As Double
        Dim dTransferredAmount As Double

        bNothingToShow = True
        ' -----------------------------------------------------
        ' Present report with one line pr payment, for all checked payments
        ' Run through all payments, and mark the checked one
        ' -----------------------------------------------------

        On Error GoTo Err_Renamed


        ' show overviewreport
        ' Run through all payments, and set those checked
        ' Grid is now at invoicelevel !!!!

        With Me.gridClientPayments
            For lRowCounter = 0 To .Rows.Count
                '        .Row = lRowCounter
                '        .Col = 1
                '        If .Text = "Payment" Then
                If .Rows(i).Cells(0).Value = "Payment" Then
                    '            .Row = lRowCounter
                    '            .Col = 3 ' Checkbox

                    '            If CDbl(.Value) = 1 Then
                    If .Rows(i).Cells(2).Value = True Then
                        '                ' checked, set to ToSpecialReport
                        '                .Col = 17
                        '                sREF = .Text
                        sREF = .Rows(i).Cells(17).Value

                        oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).ToSpecialReport = True
                        ' may have more than one invoice pr payment, due to creditnotes;
                        oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).Invoices.Item(Val(xDelim(sREF, ";", 4))).ToSpecialReport = True
                        bNothingToShow = False
                    Else
                        '                ' checked, set to ToSpecialReport
                        '                .Col = 17
                        '                sREF = .Text
                        sREF = .Rows(i).Cells(17).Value

                        ' can have more than one invoice pr payment, False mark the ones with .Value = 0
                        ' always true at paymentlevel, to be able to report where we have several invoices, and not all are excluded
                        oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).ToSpecialReport = True 'False
                        ' may have more than one invoice pr payment, due to creditnotes;
                        oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).Invoices.Item(Val(xDelim(sREF, ";", 4))).ToSpecialReport = False
                    End If
                End If
            Next lRowCounter
        End With

        If bNothingToShow = False Then
            ' Total batches, for all payments marked as ToSpecialReport = True
            ' recalculate batches, to reflect marked payments only
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    dInvoiceAmount = 0
                    dTransferredAmount = 0
                    For Each oPayment In oBatch.Payments
                        If oPayment.ToSpecialReport Then
                            dInvoiceAmount = dInvoiceAmount + oPayment.MON_InvoiceAmount
                            dTransferredAmount = dTransferredAmount + oPayment.MON_TransferredAmount
                        End If
                    Next oPayment
                    oBatch.MON_InvoiceAmount = dInvoiceAmount
                    oBatch.MON_TransferredAmount = dTransferredAmount
                Next oBatch
            Next oBabelFile


            oBabelReport = New vbBabel.BabelReport
            oBabelReport.BabelFiles = oBabelFiles

            ' activate report, call report 1002_VismaPayment
            oBabelReport.ReportNumber = 1010 ' totalreport, Visma special
            oBabelReport.Preview = True
            oBabelReport.Heading = LRS(35011) & " - " & LRS(35206) '"Totalrapport - Kladd"
            oBabelReport.ToPrint = False
            oBabelReport.AskForPrinter = False
            oBabelReport.ToEmail = False
            oBabelReport.ExportType = ""
            oBabelReport.ExportFilename = ""
            oBabelReport.BreakLevel = 4 ' BreakOnAccount
            oBabelReport.PrinterName = ""
            oBabelReport.UseLongDate = False

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            oBabelReport.BabelFiles = oBabelFiles
            oBabelReport.Special = "VismaBusiness"
            oBabelReport.RunReport()

            oBabelReport = Nothing

            ' Must reset ToSpecialReport;
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.ToSpecialReport = False
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.ToSpecialReport = False
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile
        Else
            ' found no paymentes to report;
            MsgBox(LRS(35207), MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Visma Payment(BabelBank)") ' No payments found
        End If 'If bNothingToShow = False Then

        Exit Sub

Err_Renamed:
        oBabelReport = Nothing

    End Sub

    Private Sub mnuReportOverview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oBabelReport As New vbBabel.BabelReport
        Dim sREF As String
        Dim lRowCounter As Integer
        Dim i As Short
        Dim oBabelFile As vbBabel.BabelFile
        Dim bNothingToShow As Boolean

        bNothingToShow = True
        ' -----------------------------------------------------
        ' Present report with one line pr payment, for all checked payments
        ' Run through all payments, and mark the checked one
        ' -----------------------------------------------------

        On Error GoTo Err_Renamed


        ' show overviewreport
        ' Run through all payments, and set those checked
        ' Grid is now at invoicelevel !!!!

        With Me.gridClientPayments
            For lRowCounter = 0 To .Rows.Count
                '        .Row = lRowCounter
                '        .Col = 1
                '        If .Text = "Payment" Then
                If .Rows(i).Cells(0).Value = "Payment" Then
                    '            .Row = lRowCounter
                    '            .Col = 3 ' Checkbox

                    '            If CDbl(.Value) = 1 Then
                    If .Rows(i).Cells(2).Value = True Then
                        '                ' checked, set to ToSpecialReport
                        '                .Col = 17
                        '                sREF = .Text
                        sREF = .Rows(i).Cells(17).Value

                        oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).ToSpecialReport = True
                        ' may have more than one invoice pr payment, due to creditnotes;
                        oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).Invoices.Item(Val(xDelim(sREF, ";", 4))).ToSpecialReport = True
                        bNothingToShow = False
                    Else
                        '                ' checked, set to ToSpecialReport
                        '                .Col = 17
                        '                sREF = .Text
                        sREF = .Rows(i).Cells(17).Value

                        ' can have more than one invoice pr payment, False mark the ones with .Value = 0
                        ' always true at paymentlevel, to be able to report where we have several invoices, and not all are excluded
                        oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).ToSpecialReport = True 'False
                        ' may have more than one invoice pr payment, due to creditnotes;
                        oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).Invoices.Item(Val(xDelim(sREF, ";", 4))).ToSpecialReport = False
                    End If
                End If
            Next lRowCounter
        End With

        If bNothingToShow = False Then

            oBabelReport = New vbBabel.BabelReport
            oBabelReport.BabelFiles = oBabelFiles

            ' activate report, call report 1002_VismaPayment
            oBabelReport.ReportNumber = "1002"
            oBabelReport.Preview = True
            oBabelReport.Heading = LRS(35267) & " - " & LRS(35206) '"Oversiktsrapport - Kladd"
            oBabelReport.ToPrint = False
            oBabelReport.AskForPrinter = False
            oBabelReport.ToEmail = False
            oBabelReport.ExportType = ""
            oBabelReport.ExportFilename = ""
            oBabelReport.BreakLevel = 4 ' BreakOnAccount
            oBabelReport.PrinterName = ""
            oBabelReport.UseLongDate = False

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            oBabelReport.BabelFiles = oBabelFiles
            oBabelReport.Special = "VismaBusiness"
            oBabelReport.RunReport()

            oBabelReport = Nothing

            ' Must reset ToSpecialReport;
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.ToSpecialReport = False
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.ToSpecialReport = False
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile
        Else
            ' found no paymentes to report;
            MsgBox(LRS(35207), MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Visma Payment(BabelBank)") ' No payments found
        End If 'If bNothingToShow = False Then

        Exit Sub

Err_Renamed:
        oBabelReport = Nothing


    End Sub

    Private Sub mnuReportDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oBabelReport As New vbBabel.BabelReport
        Dim sREF As String
        Dim lRowCounter As Integer
        Dim i As Short
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim bNothingToShow As Boolean
        bNothingToShow = True
        ' -----------------------------------------------------
        ' Present report with details, for all checked payments
        ' Run through all payments, and mark the checked one
        ' -----------------------------------------------------

        On Error GoTo Err_Renamed

        ' show detailreport
        ' Run through all payments, and set those checked
        ' Grid is now at invoicelevel !!!!

        With Me.gridClientPayments
            For lRowCounter = 0 To .Rows.Count
                '        .Row = lRowCounter
                '        .Col = 1
                '        If .Text = "Payment" Then
                If .Rows(i).Cells(0).Value = "Payment" Then
                    '            .Row = lRowCounter
                    '            .Col = 3 ' Checkbox

                    '            If CDbl(.Value) = 1 Then
                    If .Rows(i).Cells(2).Value = True Then
                        '                ' checked, set to ToSpecialReport
                        '                .Col = 17
                        '                sREF = .Text
                        sREF = .Rows(i).Cells(17).Value

                        oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).ToSpecialReport = True
                        ' may have more than one invoice pr payment, due to creditnotes;
                        oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).Invoices.Item(Val(xDelim(sREF, ";", 4))).ToSpecialReport = True
                        bNothingToShow = False
                    Else
                        '                ' checked, set to ToSpecialReport
                        '                .Col = 17
                        '                sREF = .Text
                        sREF = .Rows(i).Cells(17).Value

                        ' can have more than one invoice pr payment, False mark the ones with .Value = 0
                        ' always true at paymentlevel, to be able to report where we have several invoices, and not all are excluded
                        oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).ToSpecialReport = True 'False
                        ' may have more than one invoice pr payment, due to creditnotes;
                        oBabelFiles.Item(Val(xDelim(sREF, ";", 1))).Batches.Item(Val(xDelim(sREF, ";", 2))).Payments.Item(Val(xDelim(sREF, ";", 3))).Invoices.Item(Val(xDelim(sREF, ";", 4))).ToSpecialReport = False
                    End If
                End If
            Next lRowCounter
        End With


        If bNothingToShow = False Then

            oBabelReport = New vbBabel.BabelReport
            oBabelReport.BabelFiles = oBabelFiles

            ' activate report, call report 1002_VismaPayment
            oBabelReport.ReportNumber = "992"
            oBabelReport.Preview = True
            oBabelReport.Heading = LRS(35012) & " - " & LRS(35206) '"Detaljrapport - Kladd"
            oBabelReport.ToPrint = False
            oBabelReport.AskForPrinter = False
            oBabelReport.ToEmail = False
            oBabelReport.ExportType = ""
            oBabelReport.ExportFilename = ""
            oBabelReport.BreakLevel = 4 ' BreakOnAccount
            oBabelReport.PrinterName = ""
            oBabelReport.UseLongDate = False

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            oBabelReport.BabelFiles = oBabelFiles
            oBabelReport.Special = "VismaBusiness"
            oBabelReport.RunReport()

            oBabelReport = Nothing

            ' Must reset ToSpecialReport;
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.ToSpecialReport = False
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.ToSpecialReport = False
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile
        Else
            ' found no paymentes to report;
            MsgBox(LRS(35207), MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Visma Payment(BabelBank)") ' No payments found
        End If 'If bNothingToShow = False Then

        Exit Sub

Err_Renamed:
        oBabelReport = Nothing

    End Sub

    Private Sub mnuReportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

End Class