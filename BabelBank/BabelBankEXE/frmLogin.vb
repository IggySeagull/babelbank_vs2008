Option Strict Off
Option Explicit On
Friend Class frmLogin
	Inherits System.Windows.Forms.Form
	
	Public bPasswordEntered As Boolean
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		'set the global var to false
		'to denote a failed login
		bPasswordEntered = False
		Me.Hide()
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		'check for correct password
		If EmptyString(txtPWD.Text) Then
			MsgBox(LRS(27003), MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "Login")
			'MsgBox No Password entered, try again!"
			txtPWD.Focus()
			System.Windows.Forms.SendKeys.Send("{Home}+{End}")
		Else
			If EmptyString(txtPWD.Text) Then
				MsgBox(LRS(27004), MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "Login")
				'MsgBox "No User name entered, try again!"
				txtUID.Focus()
				System.Windows.Forms.SendKeys.Send("{Home}+{End}")
			Else
				bPasswordEntered = True
				Me.Hide()
			End If
		End If
	End Sub
	
	'UPGRADE_WARNING: Form event frmLogin.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmLogin_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		If Not EmptyString((txtUID.Text)) Then
			txtPWD.Focus()
		End If
	End Sub
End Class
