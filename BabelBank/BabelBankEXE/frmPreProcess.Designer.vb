﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPreProcess
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPreProcess))
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.lblFunction = New System.Windows.Forms.Label
        Me.lblProgress = New System.Windows.Forms.Label
        Me.lblErrorsFound = New System.Windows.Forms.Label
        Me.lblFilename = New System.Windows.Forms.Label
        Me.cmdMail = New System.Windows.Forms.Button
        Me.cmdStart = New System.Windows.Forms.Button
        Me.listError = New System.Windows.Forms.ListBox
        Me.cmdFileOpen = New System.Windows.Forms.Button
        Me.txtFilename = New System.Windows.Forms.TextBox
        Me.cmdExit = New System.Windows.Forms.Button
        Me.cmdView = New System.Windows.Forms.Button
        Me.txtEMailReceiver = New System.Windows.Forms.TextBox
        Me.lblEMail = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.listFileContent = New System.Windows.Forms.ListBox
        Me.lblAscii = New System.Windows.Forms.Label
        Me.txtAscii = New System.Windows.Forms.TextBox
        Me.lblLine1 = New System.Windows.Forms.Label
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(331, 82)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(404, 14)
        Me.ProgressBar1.TabIndex = 19
        '
        'lblFunction
        '
        Me.lblFunction.AutoSize = True
        Me.lblFunction.Location = New System.Drawing.Point(212, 105)
        Me.lblFunction.Name = "lblFunction"
        Me.lblFunction.Size = New System.Drawing.Size(64, 13)
        Me.lblFunction.TabIndex = 18
        Me.lblFunction.Text = "60332-Task"
        '
        'lblProgress
        '
        Me.lblProgress.AutoSize = True
        Me.lblProgress.Location = New System.Drawing.Point(212, 81)
        Me.lblProgress.MaximumSize = New System.Drawing.Size(2000, 0)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(81, 13)
        Me.lblProgress.TabIndex = 17
        Me.lblProgress.Text = "60344-Progress"
        '
        'lblErrorsFound
        '
        Me.lblErrorsFound.AutoSize = True
        Me.lblErrorsFound.Location = New System.Drawing.Point(212, 129)
        Me.lblErrorsFound.Name = "lblErrorsFound"
        Me.lblErrorsFound.Size = New System.Drawing.Size(103, 13)
        Me.lblErrorsFound.TabIndex = 16
        Me.lblErrorsFound.Text = "60333- Errors Found"
        '
        'lblFilename
        '
        Me.lblFilename.AutoSize = True
        Me.lblFilename.Location = New System.Drawing.Point(212, 57)
        Me.lblFilename.Name = "lblFilename"
        Me.lblFilename.Size = New System.Drawing.Size(82, 13)
        Me.lblFilename.TabIndex = 15
        Me.lblFilename.Text = "60023-Filename"
        '
        'cmdMail
        '
        Me.cmdMail.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdMail.Location = New System.Drawing.Point(519, 580)
        Me.cmdMail.Name = "cmdMail"
        Me.cmdMail.Size = New System.Drawing.Size(91, 23)
        Me.cmdMail.TabIndex = 13
        Me.cmdMail.Text = "55045-eMail"
        '
        'cmdStart
        '
        Me.cmdStart.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdStart.Location = New System.Drawing.Point(617, 580)
        Me.cmdStart.Name = "cmdStart"
        Me.cmdStart.Size = New System.Drawing.Size(91, 23)
        Me.cmdStart.TabIndex = 11
        Me.cmdStart.Text = "55046-Analyze"
        '
        'listError
        '
        Me.listError.FormattingEnabled = True
        Me.listError.HorizontalScrollbar = True
        Me.listError.Location = New System.Drawing.Point(215, 153)
        Me.listError.Name = "listError"
        Me.listError.Size = New System.Drawing.Size(553, 186)
        Me.listError.TabIndex = 21
        '
        'cmdFileOpen
        '
        Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
        Me.cmdFileOpen.Location = New System.Drawing.Point(740, 50)
        Me.cmdFileOpen.Name = "cmdFileOpen"
        Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen.TabIndex = 23
        Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen.UseVisualStyleBackColor = False
        '
        'txtFilename
        '
        Me.txtFilename.AcceptsReturn = True
        Me.txtFilename.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilename.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilename.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilename.Location = New System.Drawing.Point(331, 54)
        Me.txtFilename.MaxLength = 255
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilename.Size = New System.Drawing.Size(404, 20)
        Me.txtFilename.TabIndex = 22
        '
        'cmdExit
        '
        Me.cmdExit.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdExit.Location = New System.Drawing.Point(715, 580)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.Size = New System.Drawing.Size(67, 23)
        Me.cmdExit.TabIndex = 24
        Me.cmdExit.Text = "50110-Exit"
        '
        'cmdView
        '
        Me.cmdView.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdView.Location = New System.Drawing.Point(421, 580)
        Me.cmdView.Name = "cmdView"
        Me.cmdView.Size = New System.Drawing.Size(91, 23)
        Me.cmdView.TabIndex = 25
        Me.cmdView.Text = "55044-Browse"
        '
        'txtEMailReceiver
        '
        Me.txtEMailReceiver.AcceptsReturn = True
        Me.txtEMailReceiver.BackColor = System.Drawing.SystemColors.Window
        Me.txtEMailReceiver.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtEMailReceiver.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtEMailReceiver.Location = New System.Drawing.Point(335, 541)
        Me.txtEMailReceiver.MaxLength = 255
        Me.txtEMailReceiver.Name = "txtEMailReceiver"
        Me.txtEMailReceiver.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtEMailReceiver.Size = New System.Drawing.Size(433, 20)
        Me.txtEMailReceiver.TabIndex = 26
        '
        'lblEMail
        '
        Me.lblEMail.AutoSize = True
        Me.lblEMail.Location = New System.Drawing.Point(212, 544)
        Me.lblEMail.Name = "lblEMail"
        Me.lblEMail.Size = New System.Drawing.Size(65, 13)
        Me.lblEMail.TabIndex = 27
        Me.lblEMail.Text = "55045-eMail"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.BabelBank.My.Resources.Resources.tools
        Me.PictureBox1.Location = New System.Drawing.Point(780, 50)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(40, 40)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 28
        Me.PictureBox1.TabStop = False
        '
        'listFileContent
        '
        Me.listFileContent.FormattingEnabled = True
        Me.listFileContent.HorizontalScrollbar = True
        Me.listFileContent.Location = New System.Drawing.Point(215, 362)
        Me.listFileContent.Name = "listFileContent"
        Me.listFileContent.Size = New System.Drawing.Size(553, 121)
        Me.listFileContent.TabIndex = 29
        '
        'lblAscii
        '
        Me.lblAscii.AutoSize = True
        Me.lblAscii.Location = New System.Drawing.Point(212, 499)
        Me.lblAscii.Name = "lblAscii"
        Me.lblAscii.Size = New System.Drawing.Size(93, 13)
        Me.lblAscii.TabIndex = 31
        Me.lblAscii.Text = "60342-Asciivalues"
        '
        'txtAscii
        '
        Me.txtAscii.AcceptsReturn = True
        Me.txtAscii.BackColor = System.Drawing.SystemColors.Window
        Me.txtAscii.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAscii.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAscii.Location = New System.Drawing.Point(335, 496)
        Me.txtAscii.MaxLength = 255
        Me.txtAscii.Multiline = True
        Me.txtAscii.Name = "txtAscii"
        Me.txtAscii.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAscii.Size = New System.Drawing.Size(433, 40)
        Me.txtAscii.TabIndex = 30
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 572)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(800, 1)
        Me.lblLine1.TabIndex = 82
        Me.lblLine1.Text = "Label1"
        '
        'frmPreProcess
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(832, 608)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.lblAscii)
        Me.Controls.Add(Me.txtAscii)
        Me.Controls.Add(Me.listFileContent)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblEMail)
        Me.Controls.Add(Me.txtEMailReceiver)
        Me.Controls.Add(Me.cmdView)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdFileOpen)
        Me.Controls.Add(Me.txtFilename)
        Me.Controls.Add(Me.listError)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.lblFunction)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.lblErrorsFound)
        Me.Controls.Add(Me.lblFilename)
        Me.Controls.Add(Me.cmdMail)
        Me.Controls.Add(Me.cmdStart)
        Me.Name = "frmPreProcess"
        Me.Text = "50950-Supporttool"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents lblFunction As System.Windows.Forms.Label
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents lblErrorsFound As System.Windows.Forms.Label
    Friend WithEvents lblFilename As System.Windows.Forms.Label
    Friend WithEvents cmdMail As System.Windows.Forms.Button
    Friend WithEvents cmdStart As System.Windows.Forms.Button
    Friend WithEvents listError As System.Windows.Forms.ListBox
    Public WithEvents cmdFileOpen As System.Windows.Forms.Button
    Public WithEvents txtFilename As System.Windows.Forms.TextBox
    Friend WithEvents cmdExit As System.Windows.Forms.Button
    Friend WithEvents cmdView As System.Windows.Forms.Button
    Public WithEvents txtEMailReceiver As System.Windows.Forms.TextBox
    Friend WithEvents lblEMail As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents listFileContent As System.Windows.Forms.ListBox
    Friend WithEvents lblAscii As System.Windows.Forms.Label
    Public WithEvents txtAscii As System.Windows.Forms.TextBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
End Class
