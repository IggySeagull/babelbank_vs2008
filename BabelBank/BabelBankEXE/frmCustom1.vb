Option Strict Off
Option Explicit On
Friend Class frmCustom1
	Inherits System.Windows.Forms.Form
	
	Public bSave As Boolean
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		bSave = False
		Me.Hide()
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		'SaveCustom1
		bSave = True
		Me.Hide()
	End Sub
	
	Private Sub frmCustom1_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "", 400)
	End Sub
End Class
