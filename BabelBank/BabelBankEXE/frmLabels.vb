﻿Friend Class frmLabels

    Inherits System.Windows.Forms.Form

    Private bSave As Boolean
    Private oDal As vbBabel.DAL
    Private sErrorString As String
    Private bChangesDone As Boolean
    Private sCompany_ID As String

    Private Sub frmLabels_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        'Dim ctl As System.Windows.Forms.Control
        Try
            FormvbStyle(Me, "dollar.jpg")
            FormLRSCaptions(Me)

            'For Each ctl In Me.Controls
            '    If TypeName(ctl) = "PictureBox" Then
            '        If ctl.Left = 0 And ctl.Top = 0 Then
            '            ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
            '            Me.lblHeading.Location = New Point(180, 18)
            '            Me.lblHeading.BackColor = Color.Transparent
            '            Me.lblHeading.Parent = ctl
            '        End If
            '    End If
            'Next

            oDal = New vbBabel.DAL
            oDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oDal.ConnectToDB() Then
                Throw New System.Exception(oDal.ErrorMessage)
            End If

        Catch ex As Exception

            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If

            Throw New Exception("Function: frmLabels.Load" & vbCrLf & ex.Message)

        End Try

        gridLables_Construct()
        gridLables_Fill()

        Me.gridLabel.CurrentCell = Me.gridLabel.Rows(0).Cells(1)

        bChangesDone = False
        'bGetTheHellOut = False

    End Sub
    Private Sub gridLables_Construct()
        ' Instantiate gridERP
        ' Set headers etc
        Dim txtColumn As DataGridViewTextBoxColumn
        'Dim oRow As DataGridViewRow
        'Dim sFieldName As String ' Added 11.06.2014

        Try
            With Me.gridLabel
                .ReadOnly = True  ' set gridERP to ReadOnly, no editing here !
                .Rows.Clear()  ' empty previous content
                .Columns.Clear()

                .ScrollBars = ScrollBars.Vertical
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .AllowUserToDeleteRows = False  ' added 22.05.2017
                .RowHeadersVisible = True 'False
                'TODO check ??.MultiSelect = False
                .BorderStyle = BorderStyle.None
                .BackgroundColor = Color.White
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowsDefaultCellStyle.SelectionForeColor = Color.Black
                .RowTemplate.Height = 18
                .CellBorderStyle = DataGridViewCellBorderStyle.None
                '.AutoSize = True
                .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
                .AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells
                .ReadOnly = False
                .RowHeadersVisible = False

                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.HeaderText = "Standardtekst"
                txtColumn.Width = WidthFromSpreadToGrid(12)
                txtColumn.ReadOnly = True
                txtColumn.DisplayIndex = 0
                txtColumn.DefaultCellStyle.BackColor = Color.LightGray
                .Columns.Add(txtColumn)

                txtColumn = New DataGridViewTextBoxColumn
                txtColumn.HeaderText = "Spesialtekst"
                txtColumn.Width = WidthFromSpreadToGrid(12)
                txtColumn.ReadOnly = False
                txtColumn.DisplayIndex = 1
                .Columns.Add(txtColumn)

                'Hidden column to store the LRSId
                txtColumn = New DataGridViewTextBoxColumn
                'txtColumn.HeaderText = "Spesialtekst"
                txtColumn.Width = WidthFromSpreadToGrid(12)
                txtColumn.ReadOnly = True
                txtColumn.DisplayIndex = 2
                txtColumn.Visible = False
                .Columns.Add(txtColumn)

                .RowHeadersDefaultCellStyle.Font = VB6.FontChangeItalic(.Font, False)
                .RowHeadersDefaultCellStyle.Font = VB6.FontChangeBold(.Font, False)

            End With

        Catch
            Debug.WriteLine("Error in gridLabels_Construct" & Err.Description)
            If MsgBox("Error in gridLabels_Construct " & Err.Description, MsgBoxStyle.YesNo) = vbYes Then
                ' Cancel
                End
            End If
        End Try
    End Sub

    Private Sub gridLables_Fill()
        Dim sMySQL As String
        Dim myRow As DataGridViewRow
        Dim sTmp As String
        Dim i As Integer

        sMySQL = "SELECT Label_LRSId, Label FROM Labels WHERE Company_ID = " & sCompany_ID
        oDal.SQL = sMySQL

        oDal.Reader_Execute()
        If oDal.Reader_HasRows Then
            With Me.gridLabel
                Do While oDal.Reader_ReadRecord

                    ' add new line to gridERP
                    .Rows.Add()
                    myRow = .Rows(.RowCount - 1)
                    'lRowNo = .RowCount - 1

                    ' Show all fields from rs
                    For i = 0 To oDal.Reader_FieldCount

                        If i = 0 Then 'Label_LRSId
                            If CDbl(oDal.Reader_GetField(i)) < 10000 Then
                                Select Case oDal.Reader_GetField(i)
                                    Case "15"
                                        sTmp = "MyField"
                                    Case "16"
                                        sTmp = "MyField2"
                                    Case "17"
                                        sTmp = "MyField3"
                                    Case "18"
                                        sTmp = "Dim1"
                                    Case "19"
                                        sTmp = "Dim2"
                                    Case "20"
                                        sTmp = "Dim3"
                                    Case "21"
                                        sTmp = "Dim4"
                                    Case "22"
                                        sTmp = "Dim5"
                                    Case "23"
                                        sTmp = "Dim6"
                                    Case "24"
                                        sTmp = "Dim7"
                                    Case "25"
                                        sTmp = "Dim8"
                                    Case "26"
                                        sTmp = "Dim9"
                                    Case "27"
                                        sTmp = "Dim10"
                                    Case Else
                                        sTmp = "Unknown"
                                End Select
                                myRow.Cells(i).Value = sTmp
                            Else
                                myRow.Cells(i).Value = LRSCommon(CLng(oDal.Reader_GetField(i)))
                            End If
                            'Place LRSId in column 2, used for storing the labels
                            myRow.Cells(i + 2).Value = oDal.Reader_GetField(i)
                        Else
                            myRow.Cells(i).Value = oDal.Reader_GetField(i)
                        End If

                    Next i

                Loop
            End With
        End If

    End Sub
    Private Sub gridLables_Save()
        Dim sMySQL As String
        Dim lCounter As Long

        With Me.gridLabel
            For lCounter = 0 To .RowCount - 1
                ' added 24.03.2017
                If .Rows(lCounter).Cells(1).Value Is Nothing Then
                    .Rows(lCounter).Cells(1).Value = ""
                End If

                sMySQL = "UPDATE Labels SET Label = '" & .Rows(lCounter).Cells(1).Value.ToString & _
                "' WHERE Label_LRSId = " & .Rows(lCounter).Cells(2).Value.ToString

                oDal.SQL = sMySQL
                oDal.ExecuteNonQuery()

                If oDal.RecordsAffected = 1 Then
                    'OK
                Else
                    MsgBox("Lagringen av ledetekster feilet på LRSDId " & .Rows(lCounter).Cells(2).Value.ToString & vbCrLf & _
                           "Lagringen avbrytes.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Feil under lagring.")
                End If


            Next lCounter
        End With

        bChangesDone = False

    End Sub
    Friend Sub SetCompany_ID(ByVal s As String)

        sCompany_ID = s

    End Sub

    Private Sub CmdReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdReset.Click


        If MsgBox(LRS(60424), MsgBoxStyle.YesNo) = vbYes Then

            'Delete from grid
            Me.gridLabel.Rows.Clear()

            'Delete from DB
            oDal.SQL = "DELETE FROM Labels WHERE Company_ID = 1"
            oDal.ExecuteNonQuery()

            UpdateLabels(oDal, "1")

            gridLables_Fill()

            bChangesDone = False

        End If



    End Sub

    Private Sub gridLabel_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridLabel.CellValueChanged

        bChangesDone = True

    End Sub

    Private Sub cmdUse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUse.Click

        gridLables_Save()

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        gridLables_Save()
        Me.Close()

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click

        If bChangesDone Then
            If MsgBox("Ønsker du å avbryte uten å lagre endringer?", MsgBoxStyle.YesNo) = vbYes Then
                Me.Close()
            End If
        Else
            Me.Close()
        End If

    End Sub
End Class