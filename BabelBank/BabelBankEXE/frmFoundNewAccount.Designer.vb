<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmFoundNewAccount
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents lblFileInfoData As System.Windows.Forms.Label
	Public WithEvents lblFileInfo As System.Windows.Forms.Label
	Public WithEvents fraFileInfo As System.Windows.Forms.GroupBox
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents txtClientName As System.Windows.Forms.TextBox
	Public WithEvents txtClientNo As System.Windows.Forms.TextBox
	Public WithEvents lblAccountData As System.Windows.Forms.Label
	Public WithEvents lblClientName As System.Windows.Forms.Label
	Public WithEvents lblClientNo As System.Windows.Forms.Label
	Public WithEvents lblAccount As System.Windows.Forms.Label
	Public WithEvents fraClientInfo As System.Windows.Forms.GroupBox
	Public WithEvents lblHeading As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.fraFileInfo = New System.Windows.Forms.GroupBox
        Me.lblFileInfoData = New System.Windows.Forms.Label
        Me.lblFileInfo = New System.Windows.Forms.Label
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.fraClientInfo = New System.Windows.Forms.GroupBox
        Me.txtClientName = New System.Windows.Forms.TextBox
        Me.txtClientNo = New System.Windows.Forms.TextBox
        Me.lblAccountData = New System.Windows.Forms.Label
        Me.lblClientName = New System.Windows.Forms.Label
        Me.lblClientNo = New System.Windows.Forms.Label
        Me.lblAccount = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.fraFileInfo.SuspendLayout()
        Me.fraClientInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'fraFileInfo
        '
        Me.fraFileInfo.BackColor = System.Drawing.Color.Transparent
        Me.fraFileInfo.Controls.Add(Me.lblFileInfoData)
        Me.fraFileInfo.Controls.Add(Me.lblFileInfo)
        Me.fraFileInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraFileInfo.Location = New System.Drawing.Point(139, 195)
        Me.fraFileInfo.Name = "fraFileInfo"
        Me.fraFileInfo.Padding = New System.Windows.Forms.Padding(0)
        Me.fraFileInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraFileInfo.Size = New System.Drawing.Size(449, 73)
        Me.fraFileInfo.TabIndex = 8
        Me.fraFileInfo.TabStop = False
        Me.fraFileInfo.Text = "fraFileInfo"
        '
        'lblFileInfoData
        '
        Me.lblFileInfoData.BackColor = System.Drawing.Color.Transparent
        Me.lblFileInfoData.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFileInfoData.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFileInfoData.Location = New System.Drawing.Point(104, 13)
        Me.lblFileInfoData.Name = "lblFileInfoData"
        Me.lblFileInfoData.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFileInfoData.Size = New System.Drawing.Size(336, 49)
        Me.lblFileInfoData.TabIndex = 11
        Me.lblFileInfoData.Text = "lblFileInfoData"
        '
        'lblFileInfo
        '
        Me.lblFileInfo.BackColor = System.Drawing.Color.Transparent
        Me.lblFileInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFileInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFileInfo.Location = New System.Drawing.Point(9, 13)
        Me.lblFileInfo.Name = "lblFileInfo"
        Me.lblFileInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFileInfo.Size = New System.Drawing.Size(90, 49)
        Me.lblFileInfo.TabIndex = 3
        Me.lblFileInfo.Text = "lblFileInfo"
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(507, 280)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(80, 25)
        Me.cmdCancel.TabIndex = 5
        Me.cmdCancel.Text = "&Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(419, 280)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(80, 25)
        Me.cmdOK.TabIndex = 1
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'fraClientInfo
        '
        Me.fraClientInfo.BackColor = System.Drawing.Color.Transparent
        Me.fraClientInfo.Controls.Add(Me.txtClientName)
        Me.fraClientInfo.Controls.Add(Me.txtClientNo)
        Me.fraClientInfo.Controls.Add(Me.lblAccountData)
        Me.fraClientInfo.Controls.Add(Me.lblClientName)
        Me.fraClientInfo.Controls.Add(Me.lblClientNo)
        Me.fraClientInfo.Controls.Add(Me.lblAccount)
        Me.fraClientInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraClientInfo.Location = New System.Drawing.Point(139, 96)
        Me.fraClientInfo.Name = "fraClientInfo"
        Me.fraClientInfo.Padding = New System.Windows.Forms.Padding(0)
        Me.fraClientInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraClientInfo.Size = New System.Drawing.Size(449, 92)
        Me.fraClientInfo.TabIndex = 6
        Me.fraClientInfo.TabStop = False
        Me.fraClientInfo.Text = "fraClientInfo"
        '
        'txtClientName
        '
        Me.txtClientName.AcceptsReturn = True
        Me.txtClientName.BackColor = System.Drawing.SystemColors.Window
        Me.txtClientName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtClientName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtClientName.Location = New System.Drawing.Point(143, 44)
        Me.txtClientName.MaxLength = 0
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtClientName.Size = New System.Drawing.Size(217, 20)
        Me.txtClientName.TabIndex = 0
        Me.txtClientName.Text = "txtClientName"
        '
        'txtClientNo
        '
        Me.txtClientNo.AcceptsReturn = True
        Me.txtClientNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtClientNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtClientNo.Enabled = False
        Me.txtClientNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtClientNo.Location = New System.Drawing.Point(143, 24)
        Me.txtClientNo.MaxLength = 0
        Me.txtClientNo.Name = "txtClientNo"
        Me.txtClientNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtClientNo.Size = New System.Drawing.Size(217, 20)
        Me.txtClientNo.TabIndex = 7
        Me.txtClientNo.Text = "txtClientNo"
        Me.txtClientNo.Visible = False
        '
        'lblAccountData
        '
        Me.lblAccountData.BackColor = System.Drawing.Color.Transparent
        Me.lblAccountData.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAccountData.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAccountData.Location = New System.Drawing.Point(143, 68)
        Me.lblAccountData.Name = "lblAccountData"
        Me.lblAccountData.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAccountData.Size = New System.Drawing.Size(286, 17)
        Me.lblAccountData.TabIndex = 12
        Me.lblAccountData.Text = "lblAccountData"
        '
        'lblClientName
        '
        Me.lblClientName.BackColor = System.Drawing.Color.Transparent
        Me.lblClientName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblClientName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblClientName.Location = New System.Drawing.Point(9, 46)
        Me.lblClientName.Name = "lblClientName"
        Me.lblClientName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblClientName.Size = New System.Drawing.Size(134, 17)
        Me.lblClientName.TabIndex = 10
        Me.lblClientName.Text = "lblClientName"
        '
        'lblClientNo
        '
        Me.lblClientNo.BackColor = System.Drawing.Color.Transparent
        Me.lblClientNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblClientNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblClientNo.Location = New System.Drawing.Point(9, 26)
        Me.lblClientNo.Name = "lblClientNo"
        Me.lblClientNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblClientNo.Size = New System.Drawing.Size(134, 19)
        Me.lblClientNo.TabIndex = 9
        Me.lblClientNo.Text = "lblClientNo"
        '
        'lblAccount
        '
        Me.lblAccount.BackColor = System.Drawing.Color.Transparent
        Me.lblAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAccount.Location = New System.Drawing.Point(9, 67)
        Me.lblAccount.Name = "lblAccount"
        Me.lblAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAccount.Size = New System.Drawing.Size(134, 17)
        Me.lblAccount.TabIndex = 4
        Me.lblAccount.Text = "lblAccount"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(147, 58)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(433, 41)
        Me.lblHeading.TabIndex = 2
        Me.lblHeading.Text = "lblHeading"
        Me.lblHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'frmFoundNewAccount
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(599, 311)
        Me.Controls.Add(Me.fraFileInfo)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.fraClientInfo)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmFoundNewAccount"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraFileInfo.ResumeLayout(False)
        Me.fraClientInfo.ResumeLayout(False)
        Me.fraClientInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class
