<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmLicense
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtOneTimeLicenseKey As System.Windows.Forms.TextBox
	Public WithEvents txtSupport As System.Windows.Forms.TextBox
	Public WithEvents txtLimitFormats As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents txtLimitDate As System.Windows.Forms.TextBox
	Public WithEvents txtUsedRecords As System.Windows.Forms.TextBox
	Public WithEvents txtLimitRecords As System.Windows.Forms.TextBox
	Public WithEvents txtNumber As System.Windows.Forms.TextBox
	Public WithEvents lblSupport As System.Windows.Forms.Label
	Public WithEvents lblLimitFormats As System.Windows.Forms.Label
	Public WithEvents lblLimitDate As System.Windows.Forms.Label
	Public WithEvents lblUsedRecords As System.Windows.Forms.Label
	Public WithEvents lblLimitRecords As System.Windows.Forms.Label
	Public WithEvents lblNumber As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLicense))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtOneTimeLicenseKey = New System.Windows.Forms.TextBox
		Me.txtSupport = New System.Windows.Forms.TextBox
		Me.txtLimitFormats = New System.Windows.Forms.TextBox
		Me.cmdOK = New System.Windows.Forms.Button
		Me.txtLimitDate = New System.Windows.Forms.TextBox
		Me.txtUsedRecords = New System.Windows.Forms.TextBox
		Me.txtLimitRecords = New System.Windows.Forms.TextBox
		Me.txtNumber = New System.Windows.Forms.TextBox
		Me.lblSupport = New System.Windows.Forms.Label
		Me.lblLimitFormats = New System.Windows.Forms.Label
		Me.lblLimitDate = New System.Windows.Forms.Label
		Me.lblUsedRecords = New System.Windows.Forms.Label
		Me.lblLimitRecords = New System.Windows.Forms.Label
		Me.lblNumber = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.Text = "Form1"
		Me.ClientSize = New System.Drawing.Size(376, 299)
		Me.Location = New System.Drawing.Point(4, 23)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmLicense"
		Me.txtOneTimeLicenseKey.AutoSize = False
		Me.txtOneTimeLicenseKey.CausesValidation = False
		Me.txtOneTimeLicenseKey.Size = New System.Drawing.Size(33, 19)
		Me.txtOneTimeLicenseKey.Location = New System.Drawing.Point(328, 266)
		Me.txtOneTimeLicenseKey.Maxlength = 4
		Me.txtOneTimeLicenseKey.TabIndex = 13
		Me.txtOneTimeLicenseKey.Visible = False
		Me.txtOneTimeLicenseKey.AcceptsReturn = True
		Me.txtOneTimeLicenseKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtOneTimeLicenseKey.BackColor = System.Drawing.SystemColors.Window
		Me.txtOneTimeLicenseKey.Enabled = True
		Me.txtOneTimeLicenseKey.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtOneTimeLicenseKey.HideSelection = True
		Me.txtOneTimeLicenseKey.ReadOnly = False
		Me.txtOneTimeLicenseKey.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtOneTimeLicenseKey.MultiLine = False
		Me.txtOneTimeLicenseKey.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtOneTimeLicenseKey.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtOneTimeLicenseKey.TabStop = True
		Me.txtOneTimeLicenseKey.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtOneTimeLicenseKey.Name = "txtOneTimeLicenseKey"
		Me.txtSupport.AutoSize = False
		Me.txtSupport.Size = New System.Drawing.Size(113, 19)
		Me.txtSupport.Location = New System.Drawing.Point(248, 226)
		Me.txtSupport.ReadOnly = True
		Me.txtSupport.TabIndex = 12
		Me.txtSupport.AcceptsReturn = True
		Me.txtSupport.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSupport.BackColor = System.Drawing.SystemColors.Window
		Me.txtSupport.CausesValidation = True
		Me.txtSupport.Enabled = True
		Me.txtSupport.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSupport.HideSelection = True
		Me.txtSupport.Maxlength = 0
		Me.txtSupport.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSupport.MultiLine = False
		Me.txtSupport.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSupport.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSupport.TabStop = True
		Me.txtSupport.Visible = True
		Me.txtSupport.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtSupport.Name = "txtSupport"
		Me.txtLimitFormats.AutoSize = False
		Me.txtLimitFormats.Size = New System.Drawing.Size(113, 19)
		Me.txtLimitFormats.Location = New System.Drawing.Point(248, 194)
		Me.txtLimitFormats.ReadOnly = True
		Me.txtLimitFormats.TabIndex = 11
		Me.txtLimitFormats.AcceptsReturn = True
		Me.txtLimitFormats.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLimitFormats.BackColor = System.Drawing.SystemColors.Window
		Me.txtLimitFormats.CausesValidation = True
		Me.txtLimitFormats.Enabled = True
		Me.txtLimitFormats.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLimitFormats.HideSelection = True
		Me.txtLimitFormats.Maxlength = 0
		Me.txtLimitFormats.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLimitFormats.MultiLine = False
		Me.txtLimitFormats.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLimitFormats.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLimitFormats.TabStop = True
		Me.txtLimitFormats.Visible = True
		Me.txtLimitFormats.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLimitFormats.Name = "txtLimitFormats"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "&OK"
		Me.cmdOK.Size = New System.Drawing.Size(81, 25)
		Me.cmdOK.Location = New System.Drawing.Point(148, 266)
		Me.cmdOK.TabIndex = 8
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.txtLimitDate.AutoSize = False
		Me.txtLimitDate.Size = New System.Drawing.Size(113, 19)
		Me.txtLimitDate.Location = New System.Drawing.Point(248, 162)
		Me.txtLimitDate.ReadOnly = True
		Me.txtLimitDate.TabIndex = 7
		Me.txtLimitDate.AcceptsReturn = True
		Me.txtLimitDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLimitDate.BackColor = System.Drawing.SystemColors.Window
		Me.txtLimitDate.CausesValidation = True
		Me.txtLimitDate.Enabled = True
		Me.txtLimitDate.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLimitDate.HideSelection = True
		Me.txtLimitDate.Maxlength = 0
		Me.txtLimitDate.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLimitDate.MultiLine = False
		Me.txtLimitDate.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLimitDate.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLimitDate.TabStop = True
		Me.txtLimitDate.Visible = True
		Me.txtLimitDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLimitDate.Name = "txtLimitDate"
		Me.txtUsedRecords.AutoSize = False
		Me.txtUsedRecords.Size = New System.Drawing.Size(113, 19)
		Me.txtUsedRecords.Location = New System.Drawing.Point(248, 130)
		Me.txtUsedRecords.ReadOnly = True
		Me.txtUsedRecords.TabIndex = 6
		Me.txtUsedRecords.AcceptsReturn = True
		Me.txtUsedRecords.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUsedRecords.BackColor = System.Drawing.SystemColors.Window
		Me.txtUsedRecords.CausesValidation = True
		Me.txtUsedRecords.Enabled = True
		Me.txtUsedRecords.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUsedRecords.HideSelection = True
		Me.txtUsedRecords.Maxlength = 0
		Me.txtUsedRecords.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUsedRecords.MultiLine = False
		Me.txtUsedRecords.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUsedRecords.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUsedRecords.TabStop = True
		Me.txtUsedRecords.Visible = True
		Me.txtUsedRecords.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtUsedRecords.Name = "txtUsedRecords"
		Me.txtLimitRecords.AutoSize = False
		Me.txtLimitRecords.Size = New System.Drawing.Size(113, 19)
		Me.txtLimitRecords.Location = New System.Drawing.Point(248, 98)
		Me.txtLimitRecords.ReadOnly = True
		Me.txtLimitRecords.TabIndex = 5
		Me.txtLimitRecords.AcceptsReturn = True
		Me.txtLimitRecords.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLimitRecords.BackColor = System.Drawing.SystemColors.Window
		Me.txtLimitRecords.CausesValidation = True
		Me.txtLimitRecords.Enabled = True
		Me.txtLimitRecords.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLimitRecords.HideSelection = True
		Me.txtLimitRecords.Maxlength = 0
		Me.txtLimitRecords.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLimitRecords.MultiLine = False
		Me.txtLimitRecords.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLimitRecords.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLimitRecords.TabStop = True
		Me.txtLimitRecords.Visible = True
		Me.txtLimitRecords.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLimitRecords.Name = "txtLimitRecords"
		Me.txtNumber.AutoSize = False
		Me.txtNumber.Size = New System.Drawing.Size(113, 19)
		Me.txtNumber.Location = New System.Drawing.Point(248, 66)
		Me.txtNumber.ReadOnly = True
		Me.txtNumber.TabIndex = 4
		Me.txtNumber.AcceptsReturn = True
		Me.txtNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNumber.BackColor = System.Drawing.SystemColors.Window
		Me.txtNumber.CausesValidation = True
		Me.txtNumber.Enabled = True
		Me.txtNumber.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNumber.HideSelection = True
		Me.txtNumber.Maxlength = 0
		Me.txtNumber.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNumber.MultiLine = False
		Me.txtNumber.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNumber.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNumber.TabStop = True
		Me.txtNumber.Visible = True
		Me.txtNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtNumber.Name = "txtNumber"
		Me.lblSupport.Text = "Supportcontract"
		Me.lblSupport.Size = New System.Drawing.Size(233, 19)
		Me.lblSupport.Location = New System.Drawing.Point(8, 226)
		Me.lblSupport.TabIndex = 10
		Me.lblSupport.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblSupport.BackColor = System.Drawing.SystemColors.Control
		Me.lblSupport.Enabled = True
		Me.lblSupport.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblSupport.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblSupport.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblSupport.UseMnemonic = True
		Me.lblSupport.Visible = True
		Me.lblSupport.AutoSize = False
		Me.lblSupport.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblSupport.Name = "lblSupport"
		Me.lblLimitFormats.Text = "Limit formats"
		Me.lblLimitFormats.Size = New System.Drawing.Size(233, 19)
		Me.lblLimitFormats.Location = New System.Drawing.Point(8, 194)
		Me.lblLimitFormats.TabIndex = 9
		Me.lblLimitFormats.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLimitFormats.BackColor = System.Drawing.SystemColors.Control
		Me.lblLimitFormats.Enabled = True
		Me.lblLimitFormats.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblLimitFormats.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLimitFormats.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLimitFormats.UseMnemonic = True
		Me.lblLimitFormats.Visible = True
		Me.lblLimitFormats.AutoSize = False
		Me.lblLimitFormats.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLimitFormats.Name = "lblLimitFormats"
		Me.lblLimitDate.Text = "LImitDate"
		Me.lblLimitDate.Size = New System.Drawing.Size(233, 19)
		Me.lblLimitDate.Location = New System.Drawing.Point(8, 162)
		Me.lblLimitDate.TabIndex = 3
		Me.lblLimitDate.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLimitDate.BackColor = System.Drawing.SystemColors.Control
		Me.lblLimitDate.Enabled = True
		Me.lblLimitDate.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblLimitDate.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLimitDate.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLimitDate.UseMnemonic = True
		Me.lblLimitDate.Visible = True
		Me.lblLimitDate.AutoSize = False
		Me.lblLimitDate.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLimitDate.Name = "lblLimitDate"
		Me.lblUsedRecords.Text = "Used Records"
		Me.lblUsedRecords.Size = New System.Drawing.Size(233, 19)
		Me.lblUsedRecords.Location = New System.Drawing.Point(8, 130)
		Me.lblUsedRecords.TabIndex = 2
		Me.lblUsedRecords.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblUsedRecords.BackColor = System.Drawing.SystemColors.Control
		Me.lblUsedRecords.Enabled = True
		Me.lblUsedRecords.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblUsedRecords.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblUsedRecords.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblUsedRecords.UseMnemonic = True
		Me.lblUsedRecords.Visible = True
		Me.lblUsedRecords.AutoSize = False
		Me.lblUsedRecords.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblUsedRecords.Name = "lblUsedRecords"
		Me.lblLimitRecords.Text = "LimitRecords"
		Me.lblLimitRecords.Size = New System.Drawing.Size(233, 19)
		Me.lblLimitRecords.Location = New System.Drawing.Point(8, 98)
		Me.lblLimitRecords.TabIndex = 1
		Me.lblLimitRecords.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLimitRecords.BackColor = System.Drawing.SystemColors.Control
		Me.lblLimitRecords.Enabled = True
		Me.lblLimitRecords.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblLimitRecords.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLimitRecords.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLimitRecords.UseMnemonic = True
		Me.lblLimitRecords.Visible = True
		Me.lblLimitRecords.AutoSize = False
		Me.lblLimitRecords.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLimitRecords.Name = "lblLimitRecords"
		Me.lblNumber.Text = "Licensenumber"
		Me.lblNumber.Size = New System.Drawing.Size(233, 19)
		Me.lblNumber.Location = New System.Drawing.Point(8, 66)
		Me.lblNumber.TabIndex = 0
		Me.lblNumber.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblNumber.BackColor = System.Drawing.SystemColors.Control
		Me.lblNumber.Enabled = True
		Me.lblNumber.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblNumber.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblNumber.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblNumber.UseMnemonic = True
		Me.lblNumber.Visible = True
		Me.lblNumber.AutoSize = False
		Me.lblNumber.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblNumber.Name = "lblNumber"
		Me.Controls.Add(txtOneTimeLicenseKey)
		Me.Controls.Add(txtSupport)
		Me.Controls.Add(txtLimitFormats)
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(txtLimitDate)
		Me.Controls.Add(txtUsedRecords)
		Me.Controls.Add(txtLimitRecords)
		Me.Controls.Add(txtNumber)
		Me.Controls.Add(lblSupport)
		Me.Controls.Add(lblLimitFormats)
		Me.Controls.Add(lblLimitDate)
		Me.Controls.Add(lblUsedRecords)
		Me.Controls.Add(lblLimitRecords)
		Me.Controls.Add(lblNumber)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
