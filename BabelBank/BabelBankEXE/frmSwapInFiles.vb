Option Strict Off
Option Explicit On
'Imports VB = Microsoft.VisualBasic

Friend Class frmSwapInFiles
	Inherits System.Windows.Forms.Form
	Private iSwapID As Short
	Public bStatus As Boolean

    ' XokNET 23.12.2014 Added possibility to have more than one Swapsetup
    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iCompany_ID As Short

        iCompany_ID = 1 'TODO - Hardcoded CompanyID

        On Error GoTo ErrorHandling

        If MsgBox(LRS(60427, Me.txtName.Text), vbYesNo + vbQuestion, LRS(60427, "")) = MsgBoxResult.Yes Then
            iCompany_ID = 1
            'iSwapID = 1 ' always 1 as long as we allow only one record
            ' SwapID is saved in txtSwapID
            iSwapID = Val(txtSwapID.Text)

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If
            sMySQL = "DELETE FROM SwapFiles WHERE Company_ID = 1 and SwapFile_ID = " & Str(iSwapID)
            oMyDal.SQL = sMySQL

            If oMyDal.ExecuteNonQuery Then
                If oMyDal.RecordsAffected = 1 Then
                    'OK
                Else
                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                End If
            Else
                Throw New Exception("cmdDelete_Click - " & LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If
            RestoreSwapSetup()
        End If
        Exit Sub

ErrorHandling:

        ErrorHandling(Err, vbNullString, "BabelBank - SwapFiles")

    End Sub
    
    ' New function
    Private Function RestoreSwapSetup() As Boolean
        ' find values in database
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iCompany_ID As Short

        iCompany_ID = 1 'TODO - Hardcoded CompanyID
        ' SwapID is saved in txtSwapID

        ' Fill a recordset with all formats from banktable:
        Me.cmbSelectSwapSetup.Items.Clear()
        Me.cmbSelectSwapSetup.SelectedIndex = -1
        iSwapID = -1

        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If

        sMySQL = "Select * FROM SwapFiles"
        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            If oMyDal.Reader_HasRows Then

                Do While oMyDal.Reader_ReadRecord
                    'Me.cmbSelectSwapSetup.AddItem(rsSwap!Name)
                    'Me.cmbSelectSwapSetup.ItemData(Me.cmbSelectSwapSetup.ListCount - 1) = rsSwap!SwapFile_ID
                    'Me.cmbSelectSwapSetup.Items.Add(oMyDal.Reader_GetString("Name"))
                    'VB6.SetItemData(Me.cmbSelectSwapSetup, Me.cmbSelectSwapSetup.Items.Count - 1, CInt(oMyDal.Reader_GetString("SwapFile_ID")))
                    ' 12.07.2019
                    Me.cmbSelectSwapSetup.Items.Add(New _MyListBoxItem(oMyDal.Reader_GetString("Name"), CInt(oMyDal.Reader_GetString("SwapFile_ID"))))

                    ' save first item
                    If iSwapID = -1 Then
                        iSwapID = CInt(oMyDal.Reader_GetString("SwapFile_ID"))
                    End If
                Loop
                Me.cmbSelectSwapSetup.SelectedIndex = 0

                ' present data for first Swapsetup
                FillSwap()
            Else
                ' disable, use must press New-button
                Me.txtSwapID.Enabled = False
                Me.txtName.Enabled = False
                Me.txtName.Text = ""
                Me.txtFileName.Enabled = False
                Me.txtFileName.Text = ""
                Me.cmbSelectSwapSetup.SelectedIndex = -1
                Me.txtSQLName.Enabled = False
                Me.txtSQLName.Text = ""
            End If
        End If
        oMyDal.Close()
        oMyDal = Nothing


    End Function
    Private Sub FillSwap()
        Dim aFormatsAndProfiles(,) As Object
        Dim iFormatID As Integer
        Dim i As Integer
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iCompany_ID As Short

        iCompany_ID = 1 'TODO - Hardcoded CompanyID
        ' SwapID is saved in txtSwapID

        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If

        ' Fill a recordset with all formats from banktable:
        sMySQL = "Select * FROM SwapFiles WHERE Company_ID = " & str(iCompany_ID) & " AND SwapFile_Id = " & iSwapID
        'rsSwap.Open(sMySQL, cnSwap, adOpenForwardOnly, adLockOptimistic)

        oMyDal.SQL = sMySQL
        If oMyDal.Reader_Execute() Then
            If oMyDal.Reader_HasRows Then

                Do While oMyDal.Reader_ReadRecord
                    Me.txtName.Enabled = True
                    Me.txtFileName.Enabled = True
                    Me.txtFileName.Enabled = True
                    Me.txtSQLName.Enabled = True

                    Me.txtSwapID.Text = Trim(Str(iSwapID))
                    Me.txtName.Text = oMyDal.Reader_GetString("Name")
                    Me.txtFileName.Text = oMyDal.Reader_GetString("FileName")
                    iSwapID = CInt(oMyDal.Reader_GetString("SwapFile_ID"))

                    ' find format
                    iFormatID = CInt(oMyDal.Reader_GetString("FormatID"))
                    Me.chkAllowDiff.Checked = bVal(oMyDal.Reader_GetString("AllowDiff"))
                    Me.chkSwapAuto.Checked = bVal(oMyDal.Reader_GetString("SwapAuto"))
                    Me.chkRemoveAtExport.Checked = bVal(oMyDal.Reader_GetString("RemoveAtExport"))
                    Me.txtDiffAccount.Text = oMyDal.Reader_GetString("DiffAccount")
                    ' select correct type
                    Me.cmbDiffType.SelectedIndex = CInt(oMyDal.Reader_GetString("DiffAccountType")) - 1
                    Me.txtSQLName.Text = oMyDal.Reader_GetString("SQLName")

                    aFormatsAndProfiles = ArrayFormat(False, "", True, False)
                    ' Empty listbox, in case we skip back and forth ..
                    Me.cmbFormat.Items.Clear()
                    ' fill listbox:
                    For i = 0 To UBound(aFormatsAndProfiles, 2)
                        'frmSwapInFiles.cmbFormat.AddItem(aFormatsAndProfiles(0, i) + Space(100) + Str(aFormatsAndProfiles(1, i)))
                        Me.cmbFormat.Items.Add(aFormatsAndProfiles(0, i) + Space(100) + Str(aFormatsAndProfiles(1, i)))
                        If aFormatsAndProfiles(1, i) = iFormatID Then
                            Me.cmbFormat.SelectedIndex = Me.cmbFormat.Items.Count - 1
                        End If
                    Next i
                Loop
            Else
                ' no saved Swaps from before, disable
                Me.txtName.Enabled = False
                Me.txtFileName.Enabled = False
                Me.cmbDiffType.SelectedIndex = -1
                Me.txtFileName.Enabled = False
                Me.txtSQLName.Enabled = False

                Me.lbDiffType.Enabled = False
                Me.lblDiffaccount.Enabled = False
                Me.txtDiffAccount.Enabled = False
                Me.cmbDiffType.Enabled = False

                iSwapID = -1
            End If
        End If

        oMyDal.Close()
        oMyDal = Nothing

    End Sub
    Private Function SaveSwapSetup() As Boolean

        ' Save values to database
        ' Use table SwapFiles
        Dim bUpdate As Boolean = False
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iCompany_ID As Short

        Try

            iCompany_ID = 1 'TODO - Hardcoded CompanyID
            ' SwapID is saved in txtSwapID

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ' SwapID is saved in txtSwapID
            iSwapID = CShort(txtSwapID.Text)


            '19.07.2016 - Changed som code to save.
            ' Fill a recordset with all swaps from Swapfile table:
            sMySQL = "Select * FROM SwapFiles WHERE SwapFile_ID = " & CStr(iSwapID)
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then
                If oMyDal.Reader_HasRows Then
                    bUpdate = True 'The record exists
                Else
                    bUpdate = False ' Create a new record
                End If
            Else
                Throw New Exception("Database error: " & vbCrLf & oMyDal.ErrorMessage & vbCrLf & "SQL: " & sMySQL)
            End If

            If bUpdate Then
                ' at least one record present, update the same as the one we are setting up

                sMySQL = "UPDATE SwapFiles SET "
                sMySQL = sMySQL & "Company_ID = " & Str(iCompany_ID)
                sMySQL = sMySQL & ", SwapFile_ID = " & Str(iSwapID)
                sMySQL = sMySQL & ", Name = '" & Trim$(Me.txtName.Text) & "'"
                sMySQL = sMySQL & ", Filename = '" & Trim$(Me.txtFileName.Text) & "'"
                If Me.cmbFormat.SelectedIndex = -1 Then
                    ' no format selected, possible for Aetat and NAV (old swapsolution)
                    sMySQL = sMySQL & ", FormatID = 0"
                Else
                    sMySQL = sMySQL & ", FormatID = " & Strings.Right(Me.cmbFormat.SelectedItem, 4)
                End If
                sMySQL = sMySQL & ", AllowDiff = " & Str(Me.chkAllowDiff.Checked)
                sMySQL = sMySQL & ", SwapAuto = " & Str(Me.chkSwapAuto.Checked)
                sMySQL = sMySQL & ", RemoveAtExport = " & Str(Me.chkRemoveAtExport.Checked)
                sMySQL = sMySQL & ", DiffAccount = '" & Trim$(Me.txtDiffAccount.Text) & "'"
                sMySQL = sMySQL & ", DiffAccountType = " & Str(Me.cmbDiffType.SelectedIndex + 1)
                sMySQL = sMySQL & ", SQLName = '" & Trim$(Me.txtSQLName.Text) & "'"
                sMySQL = sMySQL & " WHERE Company_ID = 1 and SwapFile_ID = " & Str(iSwapID)
                oMyDal.SQL = sMySQL
                If oMyDal.ExecuteNonQuery Then
                Else
                    Throw New Exception("cmdDelete_Click - " & LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                End If

            Else
                ' No records in SwapFile, or newly added Swap; Insert
                ' First, find swpafile ID
                If iSwapID = -1 Then
                    ' find highest ID
                    sMySQL = "Select MAX(SwapFile_ID) As MaxID FROM SwapFiles"
                    oMyDal.SQL = sMySQL
                    If oMyDal.Reader_Execute() Then
                        If oMyDal.Reader_HasRows Then

                            Do While oMyDal.Reader_ReadRecord
                                iSwapID = CShort(oMyDal.Reader_GetString("MaxID"))
                            Loop
                        End If
                    End If
                End If
                iSwapID = iSwapID + 1
                'If EmptyString(Me.txtName.Text) = False And EmptyString(Me.txtFileName.Text) = False And EmptyString(Me.txtSQLName.Text) = False Then
                ' Ikke krav om at SQL er ifyllt
                If EmptyString(Me.txtName.Text) = False And EmptyString(Me.txtFileName.Text) = False Then
                    sMySQL = "INSERT INTO SwapFiles(Company_ID, SwapFile_ID, Name, Filename, FormatID, AllowDiff, SwapAuto, RemoveAtExport, DiffAccount, DiffAccountType, SQLName) "
                    sMySQL = sMySQL & "VALUES("
                    sMySQL = sMySQL & Str(iCompany_ID) & ","
                    sMySQL = sMySQL & Str(iSwapID) & ","
                    sMySQL = sMySQL & "'" & Trim$(Me.txtName.Text) & "',"
                    sMySQL = sMySQL & "'" & Trim$(Me.txtFileName.Text) & "',"
                    If Me.cmbFormat.SelectedIndex = -1 Then
                        ' no format selected, possible for Aetat and NAV (old swapsolution)
                        sMySQL = sMySQL & "0,"
                    Else
                        sMySQL = sMySQL & Strings.Right(Me.cmbFormat.SelectedItem(), 4) & ","
                    End If
                    sMySQL = sMySQL & Str(Me.chkAllowDiff.Checked) & ","
                    sMySQL = sMySQL & Str(Me.chkSwapAuto.Checked) & ","
                    sMySQL = sMySQL & Str(Me.chkRemoveAtExport.Checked) & ","
                    sMySQL = sMySQL & "'" & Trim$(Me.txtDiffAccount.Text) & "',"
                    sMySQL = sMySQL & Str(Me.cmbDiffType.SelectedIndex + 1) & ","
                    sMySQL = sMySQL & "'" & Trim$(Me.txtSQLName.Text) & "')"
                    oMyDal.SQL = sMySQL
                    If oMyDal.ExecuteNonQuery Then
                    Else
                        Throw New Exception("SaveSwapSetup - " & LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                    End If
                    ' add new item to listbox;
                    'Me.cmbSelectSwapSetup.Items.Add(Me.txtName.Text)
                    'VB6.SetItemData(Me.cmbSelectSwapSetup, Me.cmbSelectSwapSetup.Items.Count - 1, iSwapID)
                    ' 12 07.2019
                    Me.cmbSelectSwapSetup.Items.Add(New _MyListBoxItem(Me.txtName.Text, iSwapID))
                Else
                    iSwapID = -1
                End If

            End If

            txtSwapID.Text = LTrim(Str(iSwapID))

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "SaveSwapSetup")

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

    End Function
    Private Sub chkAllowDiff_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkAllowDiff.CheckStateChanged
        If chkAllowDiff.CheckState = 1 Then
            ' enable diffaccounts
            Me.lbDiffType.Enabled = True
            Me.lblDiffaccount.Enabled = True
            Me.txtDiffAccount.Enabled = True
            Me.cmbDiffType.Enabled = True
        Else
            Me.lbDiffType.Enabled = False
            Me.lblDiffaccount.Enabled = False
            Me.txtDiffAccount.Enabled = False
            Me.cmbDiffType.Enabled = False
        End If
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdCancel.Click
        Me.Hide()
        bStatus = False
    End Sub
    Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
        Dim sPath As String
        Dim sFile As String
        ' Keep filename after Browsing for folder;
        ' Must have \ and .
        If InStr(Me.txtFileName.Text, "\") > 0 And InStr(Me.txtFileName.Text, ".") > 0 Then
            sFile = Mid(Me.txtFileName.Text, InStrRev(Me.txtFileName.Text, "\"))
        ElseIf InStr(Me.txtFileName.Text, ".") > 0 Then
            ' . only, keep all;
            sFile = "\" & Me.txtFileName.Text
        Else
            sFile = ""
        End If

        'sPath = FixPath((Me.txtFileName.Text))
        '31.12.2010
        sPath = BrowseForFilesOrFolders(Me.txtFileName.Text, Me, LRS(60010), True)

        If Len(sPath) > 0 Then
            ' Add previous filename if we have selected path-only in BrowseforFolders (no file selected)
            If InStr(sPath, ".") = 0 Then
                Me.txtFileName.Text = Replace(sPath & sFile, "\\", "\")
            Else
                ' Possibly file selected
                Me.txtFileName.Text = sPath
            End If
        End If
    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdOK.Click

        SaveSwapSetup()

        'Me.Hide
        bStatus = True
        Me.Close()
    End Sub
    Sub frmSwapInFiles_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control

        FormvbStyle(Me, "dollar.jpg", 600)
        FormLRSCaptions(Me)

        'Add accounttype here, must also change in the restore.. function
        'Me.cmbDiffType.Items.Clear()
        'Me.cmbDiffType.Items.Add(LRS(60279)) '"Kundereskontro 1"
        'Me.cmbDiffType.Items.Add(LRS(60280)) '"Hovedbokskonto 2"
        'Me.cmbDiffType.Items.Add(LRS(60281)) '"KID 3"
        'Me.cmbDiffType.Items.Add(LRS(60282)) '"Fra ERP 4"
        'Me.cmbDiffType.Items.Add(LRS(60283)) '"Don't adjust invoice"
        'Me.cmbDiffType.SelectedIndex = -1

        For Each ctl In Me.Controls
            If TypeName(ctl) = "PictureBox" Then
                If ctl.Left = 0 And ctl.Top = 0 Then
                    ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                    Me.lblHeading.Location = New Point(250, 18)
                    Me.lblHeading.BackColor = Color.Transparent
                    Me.lblHeading.Parent = ctl
                End If
            End If
        Next
        RestoreSwapSetup()
    End Sub
    Private Function CheckPath(ByRef sPath As String, ByRef bFilenameIncluded As Boolean, Optional ByRef bMustBeStated As Boolean = True) As Boolean
        Dim oBackup As vbBabel.BabelFileHandling
        Dim bContinue As Boolean
        Dim iResponse As Short
        Dim bReturnValue As Boolean

        bReturnValue = True

        'Check if some text are entered in the control
        If Not EmptyString(sPath) Then
            'Check if a folder and file is stated
            bContinue = False
            If bFilenameIncluded Then
                If InStrRev(sPath, "\") > 0 Then
                    sPath = Strings.Left(sPath, InStrRev(sPath, "\"))
                    bContinue = True
                Else
                    'MsgBox("You must state the full path, not only the filename.", MsgBoxStyle.OKOnly + MsgBoxStyle.Exclamation, "AN ERRONUEUS PATH STATED")
                    MsgBox(LRS(60428), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRS(60429))
                    bContinue = False
                    bReturnValue = False
                End If
            Else
                bContinue = True
            End If

            If bContinue Then
                'Check if the path exists:
                oBackup = New vbBabel.BabelFileHandling

                oBackup.BackupPath = Trim(sPath)
                If Not oBackup.BackupPathExists Then
                    iResponse = MsgBox(oBackup.Message, MsgBoxStyle.YesNo + MsgBoxStyle.Question, LRS(60010))
                    If iResponse = MsgBoxResult.Yes Then
                        If oBackup.CreateFolderOK > 0 Then
                            '03.04.2007 JanP: Her var det tidligere mulighet for feil, pga retur True eller False
                            'Error in creating folder
                            MsgBox(oBackup.Message & vbCrLf & LRS(60011), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRS(60429))
                            bReturnValue = False
                        Else
                            bReturnValue = True
                        End If
                    Else
                        'No backup taken
                        bReturnValue = False
                    End If

                End If

            End If 'If bContinue Then
        Else
            If bMustBeStated Then
                MsgBox(LRS(60428), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRS(60429))
                bReturnValue = False
            Else
                bReturnValue = True
            End If
        End If


    End Function

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        ' save previous in case we have changed anything
        SaveSwapSetup()

    End Sub

    Private Sub cmdNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNew.Click
        Me.cmbSelectSwapSetup.SelectedIndex = -1

        ' add new swap;
        Me.txtSwapID.Enabled = True
        Me.txtName.Enabled = True
        Me.txtFileName.Enabled = True
        Me.txtSQLName.Enabled = True

        Me.lbDiffType.Enabled = False
        Me.lblDiffaccount.Enabled = False
        Me.txtDiffAccount.Enabled = False
        Me.cmbDiffType.Enabled = False


        Me.txtName.Text = ""
        Me.txtFileName.Text = ""
        Me.txtDiffAccount.Text = ""
        Me.txtSQLName.Text = ""
        ' Set swap id to -1 to mark that this one is new
        Me.txtSwapID.Text = "-1"

        Me.txtDiffAccount.Text = ""
        Me.cmbDiffType.SelectedIndex = -1
        Me.chkAllowDiff.Checked = False
        Me.chkSwapAuto.Checked = False
        Me.chkRemoveAtExport.Checked = False
        Me.cmbFormat.SelectedIndex = -1

    End Sub

    Private Sub cmbSelectSwapSetup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSelectSwapSetup.SelectedIndexChanged
        ' show data for selected swap
        '-- test ---
        If Me.cmbSelectSwapSetup.Items.Count > 0 Then
            If Me.cmbSelectSwapSetup.SelectedIndex > -1 Then
                'iSwapID = Me.cmbSelectSwapSetup.ItemData(Me.cmbSelectSwapSetup.ListIndex)
                'iSwapID = VB6.GetItemData(cmbSelectSwapSetup, cmbSelectSwapSetup.SelectedIndex)
                ' 12.07.2019
                iSwapID = cmbSelectSwapSetup.Items(cmbSelectSwapSetup.SelectedIndex).itemdata
            End If
        Else
            iSwapID = -1
        End If
        FillSwap()

    End Sub
End Class
