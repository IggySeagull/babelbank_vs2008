﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLabels
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gridLabel = New System.Windows.Forms.DataGridView
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdUse = New System.Windows.Forms.Button
        Me.CmdReset = New System.Windows.Forms.Button
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        CType(Me.gridLabel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gridLabel
        '
        Me.gridLabel.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridLabel.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.gridLabel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridLabel.DefaultCellStyle = DataGridViewCellStyle2
        Me.gridLabel.Location = New System.Drawing.Point(213, 158)
        Me.gridLabel.Name = "gridLabel"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridLabel.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.gridLabel.Size = New System.Drawing.Size(284, 373)
        Me.gridLabel.TabIndex = 24
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(266, 552)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 26
        Me.cmdCancel.Text = "55006 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(345, 552)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 27
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdUse
        '
        Me.cmdUse.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUse.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdUse.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdUse.Location = New System.Drawing.Point(424, 552)
        Me.cmdUse.Name = "cmdUse"
        Me.cmdUse.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdUse.Size = New System.Drawing.Size(73, 21)
        Me.cmdUse.TabIndex = 28
        Me.cmdUse.Text = "55041 - Use"
        Me.cmdUse.UseVisualStyleBackColor = False
        '
        'CmdReset
        '
        Me.CmdReset.BackColor = System.Drawing.SystemColors.Control
        Me.CmdReset.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdReset.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdReset.Location = New System.Drawing.Point(37, 510)
        Me.CmdReset.Name = "CmdReset"
        Me.CmdReset.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdReset.Size = New System.Drawing.Size(73, 21)
        Me.CmdReset.TabIndex = 29
        Me.CmdReset.Text = "55052 - Reset"
        Me.CmdReset.UseVisualStyleBackColor = False
        '
        'lblDescription
        '
        Me.lblDescription.Location = New System.Drawing.Point(213, 73)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(284, 65)
        Me.lblDescription.TabIndex = 30
        Me.lblDescription.Text = "55051 - Description"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 546)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(490, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmLabels
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(509, 585)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.CmdReset)
        Me.Controls.Add(Me.cmdUse)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.gridLabel)
        Me.Name = "frmLabels"
        Me.Text = "60410 - frmLabels"
        CType(Me.gridLabel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gridLabel As System.Windows.Forms.DataGridView
    Public WithEvents cmdCancel As System.Windows.Forms.Button
    Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents cmdUse As System.Windows.Forms.Button
    Public WithEvents CmdReset As System.Windows.Forms.Button
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
End Class
