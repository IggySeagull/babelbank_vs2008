<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmCoda
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmbDataset As System.Windows.Forms.ComboBox
	Public WithEvents cmbType As System.Windows.Forms.ComboBox
	Public WithEvents cmbCompany As System.Windows.Forms.ComboBox
	Public WithEvents cmbUser As System.Windows.Forms.ComboBox
    Public WithEvents lblDatasett As System.Windows.Forms.Label
	Public WithEvents lblType As System.Windows.Forms.Label
	Public WithEvents lblCompany As System.Windows.Forms.Label
	Public WithEvents lblUser As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmbDataset = New System.Windows.Forms.ComboBox
        Me.cmbType = New System.Windows.Forms.ComboBox
        Me.cmbCompany = New System.Windows.Forms.ComboBox
        Me.cmbUser = New System.Windows.Forms.ComboBox
        Me.lblDatasett = New System.Windows.Forms.Label
        Me.lblType = New System.Windows.Forms.Label
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblUser = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(213, 164)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 9
        Me.cmdCancel.Text = "55006 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(135, 164)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 8
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmbDataset
        '
        Me.cmbDataset.BackColor = System.Drawing.SystemColors.Window
        Me.cmbDataset.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbDataset.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbDataset.Location = New System.Drawing.Point(124, 108)
        Me.cmbDataset.Name = "cmbDataset"
        Me.cmbDataset.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbDataset.Size = New System.Drawing.Size(161, 21)
        Me.cmbDataset.TabIndex = 7
        '
        'cmbType
        '
        Me.cmbType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbType.Location = New System.Drawing.Point(124, 77)
        Me.cmbType.Name = "cmbType"
        Me.cmbType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbType.Size = New System.Drawing.Size(161, 21)
        Me.cmbType.TabIndex = 5
        '
        'cmbCompany
        '
        Me.cmbCompany.BackColor = System.Drawing.SystemColors.Window
        Me.cmbCompany.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbCompany.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbCompany.Location = New System.Drawing.Point(124, 45)
        Me.cmbCompany.Name = "cmbCompany"
        Me.cmbCompany.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbCompany.Size = New System.Drawing.Size(161, 21)
        Me.cmbCompany.TabIndex = 3
        '
        'cmbUser
        '
        Me.cmbUser.BackColor = System.Drawing.SystemColors.Window
        Me.cmbUser.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbUser.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbUser.Location = New System.Drawing.Point(124, 14)
        Me.cmbUser.Name = "cmbUser"
        Me.cmbUser.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbUser.Size = New System.Drawing.Size(161, 21)
        Me.cmbUser.TabIndex = 1
        '
        'lblDatasett
        '
        Me.lblDatasett.BackColor = System.Drawing.SystemColors.Control
        Me.lblDatasett.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDatasett.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDatasett.Location = New System.Drawing.Point(8, 109)
        Me.lblDatasett.Name = "lblDatasett"
        Me.lblDatasett.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDatasett.Size = New System.Drawing.Size(108, 24)
        Me.lblDatasett.TabIndex = 6
        Me.lblDatasett.Text = "Datasett"
        '
        'lblType
        '
        Me.lblType.BackColor = System.Drawing.SystemColors.Control
        Me.lblType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblType.Location = New System.Drawing.Point(8, 78)
        Me.lblType.Name = "lblType"
        Me.lblType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblType.Size = New System.Drawing.Size(108, 24)
        Me.lblType.TabIndex = 4
        Me.lblType.Text = "Type"
        '
        'lblCompany
        '
        Me.lblCompany.BackColor = System.Drawing.SystemColors.Control
        Me.lblCompany.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompany.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompany.Location = New System.Drawing.Point(8, 46)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompany.Size = New System.Drawing.Size(108, 24)
        Me.lblCompany.TabIndex = 2
        Me.lblCompany.Text = "Firmakode"
        '
        'lblUser
        '
        Me.lblUser.BackColor = System.Drawing.SystemColors.Control
        Me.lblUser.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblUser.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblUser.Location = New System.Drawing.Point(8, 13)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblUser.Size = New System.Drawing.Size(108, 24)
        Me.lblUser.TabIndex = 0
        Me.lblUser.Text = "Bruker"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 157)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(280, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmCoda
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(306, 199)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmbDataset)
        Me.Controls.Add(Me.cmbType)
        Me.Controls.Add(Me.cmbCompany)
        Me.Controls.Add(Me.cmbUser)
        Me.Controls.Add(Me.lblDatasett)
        Me.Controls.Add(Me.lblType)
        Me.Controls.Add(Me.lblCompany)
        Me.Controls.Add(Me.lblUser)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmCoda"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Coda / Babelbank"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
