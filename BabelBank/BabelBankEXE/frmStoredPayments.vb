Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic

Imports DGVPrinterHelper
Friend Class frmStoredPayments
	Inherits System.Windows.Forms.Form
	
	Private oSPBabelFiles As vbBabel.BabelFiles
	Private oSPbabel As vbBabel.BabelFile
	Private oSPBatch As vbBabel.Batch
    Private oSPPayment As vbBabel.Payment
	
	Private oSPProfile As vbBabel.Profile
	Private iCompany_ID As Integer
	
	'19.08.2008 - Ved overgang til DOTNETT settes denne til 1
    Private Const DOTNETImageListAdjustment As Short = 1
	
    Private Const colSPStatus As Short = 0
    Private Const colSPAmount As Short = 1
    Private Const colSPCurrency As Short = 2
    Private Const colSPName As Short = 3
    Private Const colSPDebetAccount As Short = 4
    Private Const colSPDate As Short = 5
    Private Const colSPCreditAccount As Short = 6
    Private Const colSPUserID As Short = 7
    Private Const colSPMatchStatus As Short = 8
    Private Const colSPBabelIndex As Short = 9 'Index is the original index, and will be used to retrieve stored data from the DB
    Private Const colSPBatchIndex As Short = 10
    Private Const colSPPaymentIndex As Short = 11
	'Private Const colSPBabelItem = 13 'Item is the item/index in the collections, and will be used to retrieve info from the collections i.e. the Invoice
	'Private Const colSPBatchItem = 14
	'Private Const colSPPaymentItem = 15
    Private Const colSPOpenSoFar As Short = 12
    Private Const colSPExistInERP As Short = 13
    Private Const colSPOmit As Short = 14
    Private Const colSPHidden As Short = 15 'Used together with the filter function.
    Private Const colSPHowMatched = 16  ' added 02.12.2015
	
    'TypeOfFilter
    Private Const filOmit As Short = 6
	Private Const filAll As Short = 5
	Private Const filNotCompletely As Short = 4
	Private Const filMatched As Short = 3
	Private Const filPartly As Short = 2
	Private Const filProposed As Short = 1
	Private Const filNotMatched As Short = 0
	
	Dim bFormLoaded As Boolean
    Dim nActiveBatchIndex As Double
    Dim nActiveBabelIndex As Double
    Dim nActivePaymentIndex As Double
    Dim nTempActiveBatchIndex As Double
    Dim nTempActiveBabelIndex As Double
    Dim nTempActivePaymentIndex As Double
	''Dim lTempActiveBabelItem As Long, lTempActiveBatchItem As Long, lTempActivePaymentItem As Long
    Dim nInitBatchIndex As Double
    Dim nInitBabelIndex As Double
    Dim nInitPaymentIndex As Double
	
    Dim oMyDal As vbBabel.DAL = Nothing
    Dim oMyDalFreetext As vbBabel.DAL = Nothing
    'Dim oConnection As ADODB.Connection
	
	Dim lActiveRow As Integer
	Dim lUnmatchedPaymentsPriorToActivePayment As Integer
	Dim bCancelPressed As Boolean
	Dim bPaymentPicked As Boolean
	Dim sMatchedColumns As String
	Dim bManMatchFromERP As Boolean
	Dim bCheckIfPaymentIsOpenInERP As Boolean
    Dim lSortCol As Integer
    ' XNET 11.10.2013
    Dim sSpecial As String

    ' Labels in grid
    Private sBBRET_CustomerNoLabel As String
    Private sBBRET_InvoiceNoLabel As String
    Private sBBRET_FilterOnLabel As String
    Private sBBRET_FilterOffLabel As String
    Private sBBRET_ClientNoLabel As String
    Private sBBRET_ExchangeRateLabel As String
    Private sBBRET_VendroNoLabel As String
    Private sBBRET_CurrencyLabel As String
    Private sBBRET_GeneralLedgerLabel As String
    Private sBBRET_VoucherNoLabel As String
    Private sBBRET_NameLabel As String
    Private sBBRET_FreetextLabel As String
    Private sBBRET_CurrencyAmountLabel As String
    Private sBBRET_Currency2Label As String
    Private sBBRET_DiscountLabel As String
    Private sBBRET_BackPaymentLabel As String
    Private sBBRET_MyFieldLabel As String
    Private sBBRET_MyField2Label As String
    Private sBBRET_MyField3Label As String
    Private sBBRET_Dim1Label As String
    Private sBBRET_Dim2Label As String
    Private sBBRET_Dim3Label As String
    Private sBBRET_Dim4Label As String
    Private sBBRET_Dim5Label As String
    Private sBBRET_Dim6Label As String
    Private sBBRET_Dim7Label As String
    Private sBBRET_Dim8Label As String
    Private sBBRET_Dim9Label As String
    Private sBBRET_Dim10Label As String


    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click

        '24.10.2005 Kjell has commented the IF-test
        'If TestLocking Then
        bCancelPressed = True
        Me.Close()
        'End If

    End Sub

    Private Sub cmdChoose_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdChoose.Click

        '24.10.2005 Kjell has added the next line
        StoreActiveIndexes(False)
        If TestLocking() Then
            bPaymentPicked = True
            Me.Close()
        End If

    End Sub

    Private Sub cmdPrintThis_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPrintThis.Click

        'Not implemented yet

        ' DOes not work with ImageColumn (col 1)
        ' Se ogs� projectet DGV Print, som kanskje l�ser dette problemet !

        Dim Printer As DGVPrinter = New DGVPrinter
        Printer.Title = LRS(60002)  ' Payments
        Printer.SubTitle = Me.Text
        Printer.SubTitleFormatFlags = _
           StringFormatFlags.LineLimit Or StringFormatFlags.NoClip
        Printer.PageNumbers = True
        Printer.PageNumberInHeader = False
        Printer.PorportionalColumns = True
        Printer.HeaderCellAlignment = StringAlignment.Near
        Printer.Footer = "BabelBank - Visual Banking AS"
        Printer.FooterSpacing = 15
        Printer.RowHeight = DGVPrinter.RowHeightSetting.CellHeight
        Printer.PrintDataGridView(Me.gridStoredPayments)

    End Sub

    Private Sub cmdSearchAmount_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSearchAmount.Click
        FindAmount()
    End Sub

    Private Sub cmdSearchName_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSearchName.Click
        FindName()
    End Sub

    Private Sub frmStoredPayments_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason

        If bPaymentPicked Then
            bCancelPressed = False
        Else
            bCancelPressed = True
        End If

        eventArgs.Cancel = Cancel
    End Sub

    Private Sub imgMatched_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles imgMatched.Click

        optShowMatched_CheckedChanged(optShowMatched, New System.EventArgs())

    End Sub

    Private Sub imgPartly_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles imgPartly.Click

        optShowPartly_CheckedChanged(optShowPartly, New System.EventArgs())

    End Sub

    Private Sub imgProposed_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles imgProposed.Click

        optShowProposed_CheckedChanged(optShowProposed, New System.EventArgs())

    End Sub
    Private Sub imgOmit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles imgOmit.Click

        optShowOmit_CheckedChanged(optShowomit, New System.EventArgs())

    End Sub
    Private Sub gridStoredPayments_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridStoredPayments.CellDoubleClick
        StoreActiveIndexes(False)
        If TestLocking() Then
            bPaymentPicked = True
            Me.Close()
        End If

    End Sub
    'Private Sub gridStoredPayments_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridStoredPayments.CellClick
    '    Dim lCounter As Integer

    '    'lSortCol = Col

    '    'If e.RowIndex = 0 And e.ColumnIndex > 0 Then
    '    ' automatic sorting
    '    'spSortRows(gridStoredPayments, Col)
    '    'If bManMatchFromERP Then
    '    '    lActiveRow = gridStoredPayments.Row
    '    '    For lCounter = 1 To gridStoredPayments.MaxRows
    '    '        gridStoredPayments.Row = lCounter
    '    '        'Check if the characters entered so far fits a name
    '    '        gridStoredPayments.Col = colSPExistInERP
    '    '        If gridStoredPayments.Text = "False" Then
    '    '            gridStoredPayments.RowHidden = True
    '    '        Else
    '    '            gridStoredPayments.Col = colSPHidden
    '    '            If gridStoredPayments.Text = "True" Then
    '    '                gridStoredPayments.RowHidden = True
    '    '            Else
    '    '                gridStoredPayments.RowHidden = False
    '    '            End If
    '    '        End If
    '    '    Next lCounter
    '    'End If
    '    'ShowMatchinfo()
    '    'Else
    '    StoreActiveIndexes(True)
    '    ShowMatchinfo()
    '    'End If

    'End Sub

    'Sub gridStoredPayments_DblClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridStoredPayments.DoubleClick

    '    StoreActiveIndexes(False)
    '    If TestLocking() Then
    '        bPaymentPicked = True
    '        Me.Close()
    '    End If

    'End Sub


    Private Sub frmStoredPayments_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        bPaymentPicked = False
        bFormLoaded = False
        'Localize

        FormvbStyle(Me, "", 400)
        FormLRSCaptions(Me)

        If Len(Me.optShowMatched.Text) = 7 Then 'English
            optShowMatched.Width = VB6.TwipsToPixelsX(950)
            imgMatched.Left = VB6.TwipsToPixelsX(1115)
        Else 'Norwegian
            optShowMatched.Width = VB6.TwipsToPixelsX(1050)
            imgMatched.Left = VB6.TwipsToPixelsX(1215)
        End If
        If Len(Me.optShowPartly.Text) = 14 Then 'English
            optShowPartly.Width = VB6.TwipsToPixelsX(1400)
            imgPartly.Left = VB6.TwipsToPixelsX(4565)
        Else 'Norwegian
            optShowPartly.Width = VB6.TwipsToPixelsX(1500)
            imgPartly.Left = VB6.TwipsToPixelsX(4565)
        End If
        optShowProposed.Width = VB6.TwipsToPixelsX(1700)
        imgProposed.Left = VB6.TwipsToPixelsX(4565)

        optShowOmit.Width = VB6.TwipsToPixelsX(1700)
        imgOmit.Left = VB6.TwipsToPixelsX(4565)

        optShowAll.Checked = True
        lSortCol = -1


        bFormLoaded = True
        bCheckIfPaymentIsOpenInERP = True

    End Sub
    Friend Sub SpreadHeadings()

        'Payment spread - present headings for spread
        Dim txtColumn As DataGridViewTextBoxColumn
        Dim imgColumn As DataGridViewImageColumn

        ' find userspecific labels;
        RetrieveLabels(Nothing, "1", sBBRET_CustomerNoLabel, sBBRET_InvoiceNoLabel, sBBRET_ClientNoLabel, _
                      sBBRET_VendroNoLabel, sBBRET_CurrencyLabel, sBBRET_ExchangeRateLabel, sBBRET_GeneralLedgerLabel, sBBRET_VoucherNoLabel, sBBRET_NameLabel, _
                      sBBRET_FreetextLabel, sBBRET_CurrencyAmountLabel, sBBRET_Currency2Label, sBBRET_DiscountLabel, sBBRET_BackPaymentLabel, sBBRET_MyFieldLabel, _
                      sBBRET_MyField2Label, sBBRET_MyField3Label, sBBRET_Dim1Label, sBBRET_Dim2Label, sBBRET_Dim3Label, sBBRET_Dim4Label, _
                      sBBRET_Dim5Label, sBBRET_Dim6Label, sBBRET_Dim7Label, sBBRET_Dim8Label, sBBRET_Dim9Label, sBBRET_Dim10Label)
        With gridStoredPayments
            .Rows.Clear()  ' empty content of gridMatched

            If .ColumnCount = 0 Then
                '.Height = 100

                .ScrollBars = ScrollBars.Both
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.FixedSingle
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .CellBorderStyle = DataGridViewCellBorderStyle.None
                .ReadOnly = True
                '.AutoSize = True
            End If

            ' Col headings and widths:

            ' 1 colSPStatus
            ' ----------------
            imgColumn = New DataGridViewImageColumn
            imgColumn.HeaderText = " "
            imgColumn.Width = WidthFromSpreadToGrid(2)
            imgColumn.Resizable = True
            .Columns.Add(imgColumn)

            ' 2 colSPAmount
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            txtColumn.ValueType = GetType(Decimal)
            txtColumn.DefaultCellStyle.Format = "##,##0.00"
            txtColumn.HeaderText = LRS(60086) 'Amount
            txtColumn.Width = WidthFromSpreadToGrid(10)
            .Columns.Add(txtColumn)


            ' 3 colSPCurrency
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = sBBRET_CurrencyLabel  'LRSCommon(41005) 'Currency
            txtColumn.Width = WidthFromSpreadToGrid(3)
            'txtColumn.Resizable = False
            .Columns.Add(txtColumn)


            ' 4 colSPName
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = sBBRET_NameLabel  'LRS(60160) 'Payers name
            txtColumn.Width = WidthFromSpreadToGrid(15)
            'txtColumn.Resizable = False
            .Columns.Add(txtColumn)

            ' 5 colSPDebetAccount
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60161) 'From account
            txtColumn.Width = WidthFromSpreadToGrid(10)
            'txtColumn.Resizable = False
            .Columns.Add(txtColumn)

            ' 6 colSPDate
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn

            '09.07.2018 - Added next two lines, two be able to sort column as date.
            txtColumn.ValueType = GetType(System.DateTime)
            txtColumn.DefaultCellStyle.Format = "d"
            txtColumn.HeaderText = LRS(60162) 'Date
            txtColumn.Width = WidthFromSpreadToGrid(6)
            'txtColumn.Resizable = False
            .Columns.Add(txtColumn)

            ' 7 colSPCreditAccount
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(60163) 'To account
            txtColumn.Width = WidthFromSpreadToGrid(10)
            'txtColumn.Resizable = False
            .Columns.Add(txtColumn)

            ' 8 colSPUserID
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            ' 01.07.2020 - Vis KID for Fjordkraftfabrikken (istedenfor UID)
            If sSpecial = "FJORDKRAFT" Then
                txtColumn.HeaderText = "KID/FakturaNr."
            Else
                txtColumn.HeaderText = LRS(60205) 'User ID
            End If
            txtColumn.Width = WidthFromSpreadToGrid(9)
            'txtColumn.Resizable = False
            .Columns.Add(txtColumn)

            ' 9 colSPMatchStatus
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            'txtColumn.Resizable = False
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

            ' 10 colSPBabelIndex
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            'txtColumn.Resizable = False
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

            ' 11 colSPBatchIndex
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            'txtColumn.Resizable = False
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

            ' 12 colSPPaymentIndex
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            'txtColumn.Resizable = False
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

            ' 13 colSPOpenSoFar
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            'txtColumn.Resizable = False
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

            ' 14 colSPExistInErp
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Resizable = False
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

            ' 15 colSPOmit
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            'txtColumn.Resizable = False
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

            ' 16 colSPHidden
            ' ----------------
            txtColumn = New DataGridViewTextBoxColumn
            'txtColumn.Resizable = False
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

            ' added 02.12.2015
            ' 17 colSPHowMatched
            ' ------------------
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRS(62077) 'How matched
            txtColumn.Width = WidthFromSpreadToGrid(20)
            'txtColumn.Resizable = False
            .Columns.Add(txtColumn)


            .Height = Me.Height - 340
            .Width = Me.Width - 100

        End With


        'ImageList1.ImageHeight = 15
        'ImageList1.ImageWidth = 15

        ImageListStatus.Images.Clear()

        'Load pictures for the Status field into the ImageList control.
        ImageListStatus.Images.Add("Matched", Me.imgMatched.Image) '1
        ImageListStatus.Images.Add("Partly", Me.imgPartly.Image) '2
        ImageListStatus.Images.Add("Proposed", Me.imgProposed.Image) '3
        ImageListStatus.Images.Add("Omitted", Me.imgOmit.Image) '4
        ImageListStatus.Images.Add("UnMatched", Me.imgEmptyImage.Image) '5

    End Sub
    Private Sub gridStoredPayments_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gridStoredPayments.KeyDown
        If e.KeyCode = Keys.Enter Then
            ' disable Enter to move to next row
            e.Handled = True
        End If
    End Sub

    Friend Function SpreadPopulate() As Boolean
        ' nBatchIndex shows chosen batchindex
        ' Fill data into payment spread:

        Dim oPayment As vbBabel.Payment
        Dim oFilesetup As vbBabel.FileSetup
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim sMySQL As String
        Dim sErrorString As String
        Dim sTmp As String
        Dim i, j As Double
        Dim lUnMatchedPaymentCounter As Integer
        Dim lCounter As Integer
        Dim lBabelCounter As Integer
        Dim iOldClientID As Short
        Dim sCheckERPSQL As String
        Dim sOldAccountNo As String = ""
        Dim bAccountFound As Boolean
        Dim sI_Account As String
        Dim bContinue As Boolean = False

        'Try ' - 01.12.2020 -Removed errorhandling from this function to get the correct linenumber
        sErrorString = ""
        sTmp = ""

        ' Set timeglass
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        oPayment = New vbBabel.Payment 'To be use to be passed to CheckIfPaymentIsOpenInERP()

        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If

        'Open also a connection used to retrieve freetext
        oMyDalFreetext = New vbBabel.DAL
        oMyDalFreetext.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDalFreetext.ConnectToDB() Then
            Throw New System.Exception("oMyDALFreetext" & oMyDalFreetext.ErrorMessage)
        End If

        'Create a reader
        ' 24.06.2020 - added special SELECT for FJORDKRAFTFABRIKKEN
        If sSpecial = "FJORDKRAFT" Then
            sMySQL = "SELECT P.MON_InvoiceAmount, P.MON_InvoiceCurrency, P.E_Name, P.E_Account, P.DATE_Payment, "
            sMySQL = sMySQL & "P.I_Account, P.UserID, P.MATCH_Matched, P.BabelFile_ID, P.Batch_ID, "
            sMySQL = sMySQL & "P.Payment_ID, P.MATCH_MatchingIDAgainstObservationAccount, P.Unique_ERPID, P.StatusCode, P.HowMatched, I.InvoiceNo, I.Unique_ID "
            sMySQL = sMySQL & "FROM Payment P, Invoice I WHERE I.Match_Original=True AND P.BabelFile_ID=I.BabelFile_ID AND P.Batch_ID=I.Batch_ID AND P.Payment_ID=I.Payment_ID AND I.Invoice_ID = 1 AND P.Company_ID = " & Trim$(Str(iCompany_ID))
            '11.11.2020 - Added AND Invoice_ID = 1 to the SQL. Mot ideal but discussed it with the user.
        Else
            ' XNET 08.10.2013 - lagt til StatusCode
            sMySQL = "SELECT MON_InvoiceAmount, MON_InvoiceCurrency, E_Name, E_Account, DATE_Payment, "
            sMySQL = sMySQL & "I_Account, UserID, MATCH_Matched, BabelFile_ID, Batch_ID, "
            sMySQL = sMySQL & "Payment_ID, MATCH_MatchingIDAgainstObservationAccount, Unique_ERPID, StatusCode, HowMatched "
            sMySQL = sMySQL & "FROM Payment WHERE Company_ID = " & Trim$(Str(iCompany_ID))
        End If
        oMyDal.SQL = sMySQL

        If oMyDal.Reader_Execute() Then
            If oMyDal.Reader_HasRows Then

                With gridStoredPayments
                    .Rows.Clear()
                    lCounter = 0

                    j = 0

                    lBabelCounter = 0

                    Do While oMyDal.Reader_ReadRecord
                        '04.12.2012 - New code, moved from beneath
                        bContinue = False

                        ' XNET 11.10.2013
                        ' Vingcard do not want to see Omitted payments in the form
                        If (sSpecial = "VINGCARD" Or sSpecial = "VINGCARD_UK") And oMyDal.Reader_GetString("StatusCode") = "-1" Then
                            ' do not add a row!
                            bContinue = False
                        ElseIf bManMatchFromERP Then
                            sI_Account = oMyDal.Reader_GetString("I_Account")
                            If sI_Account <> sOldAccountNo Then
                                bAccountFound = False
                                For Each oFilesetup In oSPProfile.FileSetups
                                    For Each oClient In oFilesetup.Clients
                                        For Each oaccount In oClient.Accounts
                                            'XNET - 25.02.2014 - Added Or in the next line
                                            If Trim(sI_Account) = Trim(oaccount.Account) Or Trim(sI_Account) = Trim(oaccount.ConvertedAccount) Then
                                                'If Trim(sI_Account) = Trim(oaccount.Account) Then
                                                bAccountFound = True
                                                sOldAccountNo = sI_Account
                                                Exit For
                                            End If
                                        Next oaccount
                                        If bAccountFound Then Exit For
                                    Next oClient
                                    If bAccountFound Then Exit For
                                Next oFilesetup
                            End If

                            If oClient.Client_ID <> iOldClientID Then
                                iOldClientID = oClient.Client_ID
                                sCheckERPSQL = GetCheckAgainstERPSQL(iCompany_ID, oClient.DBProfile_ID)
                            End If
                            'End If 19.12.2012 - KJELL, er denne End If p� feil plass ?
                            ' Jeg har flyttet den til rett f�r If bContinue


                            If oMyDal.Reader_GetString("Unique_ERPID") = "" Then
                                If CheckIfPaymentIsOpenInERP(oPayment, oClient, sCheckERPSQL, bCheckIfPaymentIsOpenInERP, True, "", oMyDal.Reader_GetString("MON_InvoiceAmount")) Then
                                    bContinue = "True"
                                Else
                                    bContinue = "False"
                                End If
                            Else
                                If CheckIfPaymentIsOpenInERP(oPayment, oClient, sCheckERPSQL, bCheckIfPaymentIsOpenInERP, True, oMyDal.Reader_GetString("Unique_ERPID"), oMyDal.Reader_GetString("MON_InvoiceAmount")) Then
                                    bContinue = "True"
                                Else
                                    bContinue = "False"
                                End If
                            End If
                        Else
                            bContinue = True
                        End If

                        If bContinue Then '04.12.2012 - new IF, see above
                            .Rows.Add()

                            ' Display the picture
                            If CInt(oMyDal.Reader_GetString("MATCH_Matched")) = vbBabel.BabelFiles.MatchStatus.Matched Then
                                .Rows(.RowCount - 1).Cells(colSPStatus).Value = ImageListStatus.Images.Item(1 - DOTNETImageListAdjustment)
                            ElseIf CInt(oMyDal.Reader_GetString("MATCH_Matched")) = vbBabel.BabelFiles.MatchStatus.PartlyMatched Then
                                .Rows(.RowCount - 1).Cells(colSPStatus).Value = ImageListStatus.Images.Item(2 - DOTNETImageListAdjustment)
                            ElseIf CInt(oMyDal.Reader_GetString("MATCH_Matched")) = vbBabel.BabelFiles.MatchStatus.ProposedMatched Then
                                .Rows(.RowCount - 1).Cells(colSPStatus).Value = ImageListStatus.Images.Item(3 - DOTNETImageListAdjustment)
                            ElseIf oMyDal.Reader_GetString("MATCH_Matched") = "-1" Then
                                ' denne sto f�r 17.12.2014 - er dette en feil, skulle det ha st�tt Statuscode = -1 (som den jeg har lagt inn under?)
                                .Rows(.RowCount - 1).Cells(colSPStatus).Value = ImageListStatus.Images.Item(4 - DOTNETImageListAdjustment)
                                ' 17.12.2014 added omitted payments
                            ElseIf oMyDal.Reader_GetString("Statuscode") = "-1" Then
                                .Rows(.RowCount - 1).Cells(colSPStatus).Value = ImageListStatus.Images.Item(4 - DOTNETImageListAdjustment)
                            Else
                                ' set blank image
                                .Rows(.RowCount - 1).Cells(colSPStatus).Value = ImageListStatus.Images.Item(5 - DOTNETImageListAdjustment)
                            End If

                            ImageListStatus.Images.Add("Matched", Me.imgMatched.Image) '1
                            ImageListStatus.Images.Add("Partly", Me.imgPartly.Image) '2
                            ImageListStatus.Images.Add("Proposed", Me.imgProposed.Image) '3
                            ImageListStatus.Images.Add("Omitted", Me.imgOmit.Image) '4
                            ImageListStatus.Images.Add("UnMatched", Me.imgEmptyImage.Image) '5

                            ' Center the picture
                            '.TypePictCenter = True
                            ' Fit the picture
                            '.TypePictStretch = True
                            '.TypePictMaintainScale = True
                            ' Set the cell�s background color
                            ' dark blue, RGB(0, 0, 128)
                            '.BackColor = System.Drawing.ColorTranslator.FromOle(&H8000000F)
                            '''            .CellType = CellTypePicture

                            .Rows(.RowCount - 1).Cells(colSPAmount).Value = oMyDal.Reader_GetString("MON_InvoiceAmount") / 100  'VB6.Format(oMyDal.Reader_GetString("MON_InvoiceAmount") / 100, "##,##0.00")
                            '.CellType = SS_CELL_TYPE_FLOAT
                            '.TypeFloatSeparator = True

                            .Rows(.RowCount - 1).Cells(colSPCurrency).Value = oMyDal.Reader_GetString("MON_InvoiceCurrency")

                            .Rows(.RowCount - 1).Cells(colSPName).Value = oMyDal.Reader_GetString("E_Name")

                            sTmp = oMyDal.Reader_GetString("E_Account")
                            If Len(sTmp) = 11 Then
                                .Rows(.RowCount - 1).Cells(colSPDebetAccount).Value = VB.Left(sTmp, 4) & "." & Mid(sTmp, 5, 2) & "." & VB.Right(sTmp, 5)
                            Else
                                .Rows(.RowCount - 1).Cells(colSPDebetAccount).Value = sTmp
                            End If

                            '.Rows(.RowCount - 1).Cells(colSPDate).Value = VB6.Format(StringToDate(oMyDal.Reader_GetString("DATE_Payment")), "Short Date")
                            '09.07.2016 - New code reagarding sorting
                            .Rows(.RowCount - 1).Cells(colSPDate).Value = StringToDate(oMyDal.Reader_GetString("DATE_Payment"))
                            'Old code
                            'sTmp = StringToDate(oMyDal.Reader_GetString("DATE_Payment"))
                            '.Rows(.RowCount - 1).Cells(colSPDate).Value = sTmp

                            sI_Account = oMyDal.Reader_GetString("I_Account")
                            If Len(sI_Account) = 11 Then
                                .Rows(.RowCount - 1).Cells(colSPCreditAccount).Value = VB.Left(sI_Account, 4) & "." & Mid(sI_Account, 5, 2) & "." & VB.Right(sI_Account, 5)
                            Else
                                .Rows(.RowCount - 1).Cells(colSPCreditAccount).Value = sI_Account
                            End If

                            ' 24.06.2020 - added next for FJORDKRAFTFABRIKKEN
                            If sSpecial = "FJORDKRAFT" Then
                                ' Show KID or Invoice No
                                If Not EmptyString(oMyDal.Reader_GetString("Unique_ID")) Then
                                    .Rows(.RowCount - 1).Cells(colSPUserID).Value = oMyDal.Reader_GetString("Unique_ID")
                                Else
                                    .Rows(.RowCount - 1).Cells(colSPUserID).Value = oMyDal.Reader_GetString("InvoiceNo")
                                End If
                            Else
                                .Rows(.RowCount - 1).Cells(colSPUserID).Value = oMyDal.Reader_GetString("UserID")
                            End If
                            ' added 17.12.2014 for omitted payments;
                            If oMyDal.Reader_GetString("StatusCode") = "-1" Then
                                .Rows(.RowCount - 1).Cells(colSPMatchStatus).Value = filOmit
                            Else
                                .Rows(.RowCount - 1).Cells(colSPMatchStatus).Value = oMyDal.Reader_GetString("MATCH_Matched")
                            End If
                            .Rows(.RowCount - 1).Cells(colSPBabelIndex).Value = oMyDal.Reader_GetString("BabelFile_ID")
                            .Rows(.RowCount - 1).Cells(colSPBatchIndex).Value = oMyDal.Reader_GetString("Batch_ID")
                            .Rows(.RowCount - 1).Cells(colSPPaymentIndex).Value = oMyDal.Reader_GetString("Payment_ID")

                            If CInt(oMyDal.Reader_GetString("MATCH_Matched")) < vbBabel.BabelFiles.MatchStatus.Matched Then
                                lUnMatchedPaymentCounter = lUnMatchedPaymentCounter + 1
                            End If
                            .Rows(.RowCount - 1).Cells(colSPOpenSoFar).Value = Str(lUnMatchedPaymentCounter)

                            If CDbl(oMyDal.Reader_GetString("Payment_ID")) = nActivePaymentIndex Then
                                If CDbl(oMyDal.Reader_GetString("Batch_ID")) = nActiveBatchIndex Then
                                    If CDbl(oMyDal.Reader_GetString("BabelFile_ID")) = nActiveBabelIndex Then
                                        lActiveRow = .RowCount - 1
                                        lUnmatchedPaymentsPriorToActivePayment = lUnMatchedPaymentCounter
                                    End If
                                End If
                            End If


                            If bManMatchFromERP Then
                                .Rows(.RowCount - 1).Cells(colSPExistInERP).Value = "True"
                            End If
                            .Rows(.RowCount - 1).Cells(colSPHidden).Value = "False"

                            ' added 02.12.2015
                            ' 17 colSPHowMatched
                            ' ------------------
                            .Rows(.RowCount - 1).Cells(colSPHowMatched).Value = oMyDal.Reader_GetString("HowMatched")

                        End If

                    Loop

                    ' position to "active" payment
                    If lActiveRow > 0 Then
                        .FirstDisplayedScrollingRowIndex = lActiveRow
                        .Refresh()

                        .CurrentCell = .Rows(lActiveRow).Cells(5)
                        .Rows(lActiveRow).Selected = True
                        .Refresh()
                    End If

                    ' XNET 11.10.2013
                    ' Dette er meningsl�st. Her settes kontonummer for siste betaling, og det blir st�ende uansett hva vi velger.
                    ' Har forel�pig gjort det slik at det blir blankt. Vi f�r diskutere om vi skal ha noe her i det hele tatt
                    'Me.txtLabelStored.Caption = sTmp
                    Me.txtLabelStored.Text = ""

                End With

            End If
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

        lUnMatchedPaymentCounter = 0
        iOldClientID = -1

        ShowMatchinfo()

        Me.Cursor = System.Windows.Forms.Cursors.Default

        'Catch ex As Exception

        '    If Not oMyDal Is Nothing Then
        '        oMyDal.Close()
        '        oMyDal = Nothing
        '    End If

        '    If Not oMyDalFreetext Is Nothing Then
        '        oMyDalFreetext.Close()
        '        oMyDalFreetext = Nothing
        '    End If

        '    Me.Cursor = System.Windows.Forms.Cursors.Default

        '    Throw New Exception("Function: SpreadPopulateNew" & vbCrLf & ex.Message)

        'End Try

    End Function

    Private Function SetFilter(ByRef iTypeOfFilter As Short) As Boolean
        Dim lCounter As Integer
        Dim bActiveRowSet As Boolean
        Dim lTempActiveRow As Integer

        bActiveRowSet = False
        With gridStoredPayments
            'lActiveRow =
            lTempActiveRow = 0
            '.ReDraw = False
            If iTypeOfFilter < filNotCompletely Then
                For lCounter = 0 To .RowCount - 1
                    If Val(.Rows(lCounter).Cells(colSPMatchStatus).Value) = iTypeOfFilter Then
                        If lCounter <= lActiveRow Then
                            bActiveRowSet = True
                            lTempActiveRow = lCounter
                        ElseIf Not bActiveRowSet Then
                            bActiveRowSet = True
                            lTempActiveRow = lCounter
                        End If
                        .Rows(lCounter).Cells(colSPHidden).Value = "False"
                        If .Rows(lCounter).Cells(colSPExistInERP).Value = "False" Then
                            .Rows(lCounter).Visible = False
                        Else
                            .Rows(lCounter).Visible = True
                        End If
                    Else
                        .Rows(lCounter).Visible = False
                        .Rows(lCounter).Cells(colSPHidden).Value = "True"
                    End If
                Next lCounter
            ElseIf iTypeOfFilter = filNotCompletely Then
                For lCounter = 0 To .RowCount - 1
                    If Val(.Rows(lCounter).Cells(colSPMatchStatus).Value) < filMatched Then
                        bActiveRowSet = True
                        lTempActiveRow = lCounter
                        .Rows(lCounter).Cells(colSPHidden).Value = "False"
                        If .Rows(lCounter).Cells(colSPExistInERP).Value = "False" Then
                            .Rows(lCounter).Visible = False
                        Else
                            .Rows(lCounter).Visible = True
                        End If
                    Else
                        .Rows(lCounter).Visible = False
                        .Rows(lCounter).Cells(colSPHidden).Value = "True"
                    End If
                Next lCounter
            ElseIf iTypeOfFilter = filOmit Then
                For lCounter = 0 To .RowCount - 1
                    If Val(.Rows(lCounter).Cells(colSPMatchStatus).Value) = filOmit Then
                        bActiveRowSet = True
                        lTempActiveRow = lCounter
                        .Rows(lCounter).Cells(colSPHidden).Value = "False"
                        If .Rows(lCounter).Cells(colSPExistInERP).Value = "False" Then
                            .Rows(lCounter).Visible = False
                        Else
                            .Rows(lCounter).Visible = True
                        End If
                    Else
                        .Rows(lCounter).Visible = False
                        .Rows(lCounter).Cells(colSPHidden).Value = "True"
                    End If
                Next lCounter

            Else 'TypeOfFilter = 5
                For lCounter = 0 To .RowCount - 1
                    bActiveRowSet = True
                    lTempActiveRow = lCounter

                    .Rows(lCounter).Cells(colSPHidden).Value = "True"
                    If .Rows(lCounter).Cells(colSPExistInERP).Value = "False" Then
                        .Rows(lCounter).Visible = False
                    Else
                        .Rows(lCounter).Visible = True
                    End If
                Next lCounter
            End If

            .Rows.Item(lTempActiveRow).Selected = True

            If lTempActiveRow >= 0 Then

                nTempActiveBabelIndex = CDbl(.Rows(lCounter - 1).Cells(colSPBabelIndex).Value)
                nTempActiveBatchIndex = CDbl(nTempActiveBabelIndex = CDbl(.Rows(lCounter - 1).Cells(colSPBatchIndex).Value))
                nTempActivePaymentIndex = CDbl(nTempActiveBabelIndex = CDbl(.Rows(lCounter - 1).Cells(colSPPaymentIndex).Value))
            Else
                nTempActiveBabelIndex = 0
                nTempActiveBatchIndex = 0
                nTempActivePaymentIndex = 0
            End If

        End With

        ShowMatchinfo()

    End Function
    Public Sub SetBabelFilesInStoredPayments(ByRef NewVal As vbBabel.BabelFiles)
        oSPBabelFiles = NewVal
    End Sub
    Public Sub SetSpecial(ByVal sSp As String)
        ' XNET 11.10.2013
        ' We will need Special to test on;
        sSpecial = sSp
    End Sub
    Public Sub SetProfileInStoredPayments(ByRef NewVal As vbBabel.Profile)
        oSPProfile = NewVal
        iCompany_ID = oSPProfile.Company_ID
    End Sub

    Public Sub SetActiveIndexes(ByRef nBabel As Double, ByRef nBatch As Double, ByRef nPayment As Double)

        nActiveBabelIndex = nBabel
        nActiveBatchIndex = nBatch
        nActivePaymentIndex = nPayment
        nTempActiveBabelIndex = nBabel
        nTempActiveBatchIndex = nBatch
        nTempActivePaymentIndex = nPayment

    End Sub
    Public Sub SetInitialIndexes(ByRef nBabel As Double, ByRef nBatch As Double, ByRef nPayment As Double)

        nActiveBabelIndex = nBabel
        nActiveBatchIndex = nBatch
        nActivePaymentIndex = nPayment
        nTempActiveBabelIndex = nBabel
        nTempActiveBatchIndex = nBatch
        nTempActivePaymentIndex = nPayment
        nInitBabelIndex = nBabel
        nInitBatchIndex = nBatch
        nInitPaymentIndex = nPayment

    End Sub

    Public Sub setMatchedColumns(ByRef sString As String)

        sMatchedColumns = sString

    End Sub
    Public Sub setManMatchFromERP(ByRef b As Boolean)

        bManMatchFromERP = b

    End Sub
    Public Sub setCheckIfPaymentIsOpenInERP(ByRef b As Boolean)

        bCheckIfPaymentIsOpenInERP = b

    End Sub

    Friend Function GetActiveBabelIndex() As Double

        GetActiveBabelIndex = nActiveBabelIndex

    End Function
    Friend Function GetActiveBatchIndex() As Double

        GetActiveBatchIndex = nActiveBatchIndex

    End Function
    Friend Function GetActivePaymentIndex() As Double

        GetActivePaymentIndex = nActivePaymentIndex

    End Function

    Friend Function GetUnMatchedCounter() As Integer

        GetUnMatchedCounter = lUnmatchedPaymentsPriorToActivePayment

    End Function
    Friend Function FormCancelled() As Boolean

        FormCancelled = bCancelPressed

    End Function

    Private Sub frmStoredPayments_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If
        If Not oMyDalFreetext Is Nothing Then
            oMyDalFreetext.Close()
            oMyDalFreetext = Nothing
        End If

        StoreActiveIndexes(False)

    End Sub

    'UPGRADE_WARNING: Event optShowAll.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optShowAll_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optShowAll.CheckedChanged
        If eventSender.Checked Then

            If bFormLoaded Then
                SetFilter(filAll)
                Me.gridStoredPayments.Focus()
            End If

            Me.txtSearchName.Text = ""
            Me.txtSearchAmount.Text = ""

        End If
    End Sub
    Private Sub optShowAll_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optShowAll.Enter
        If optShowAll.Checked = True Then
            If bFormLoaded Then
                SetFilter(filAll)
                Me.gridStoredPayments.Focus()
            End If
        End If

        Me.txtSearchName.Text = ""
        Me.txtSearchAmount.Text = ""

    End Sub

    'UPGRADE_WARNING: Event optShowMatched.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optShowMatched_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optShowMatched.CheckedChanged
        If eventSender.Checked Then
            SetFilter(filMatched)
            Me.gridStoredPayments.Focus()

            ResetSearch()

        End If
    End Sub

    'UPGRADE_WARNING: Event optShowNotCompletely.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optShowNotCompletely_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optShowNotCompletely.CheckedChanged
        If eventSender.Checked Then
            SetFilter(filNotCompletely)
            Me.gridStoredPayments.Focus()

            ResetSearch()

        End If
    End Sub

    'UPGRADE_WARNING: Event optShowNotMatched.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optShowNotMatched_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optShowNotMatched.CheckedChanged
        If eventSender.Checked Then
            SetFilter(filNotMatched)
            Me.gridStoredPayments.Focus()

            ResetSearch()

        End If
    End Sub

    Private Sub optShowPartly_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optShowPartly.CheckedChanged
        If eventSender.Checked Then
            SetFilter(filPartly)
            Me.gridStoredPayments.Focus()

            ResetSearch()

        End If
    End Sub

    Private Sub optShowProposed_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optShowProposed.CheckedChanged
        If eventSender.Checked Then
            SetFilter(filProposed)
            Me.gridStoredPayments.Focus()

            ResetSearch()

        End If
    End Sub
    Private Sub optShowOmit_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optShowOmit.CheckedChanged
        If eventSender.Checked Then
            SetFilter(filOmit)
            Me.gridStoredPayments.Focus()

            ResetSearch()

        End If
    End Sub

    Private Sub StoreActiveIndexes(ByRef bOnlyTempIndexes As Boolean, Optional ByRef bSetActiveRow As Boolean = True, Optional ByRef lNewRow As Long = -1)
        ' 07.10.2010 - added new parameter lNewRow
        Dim lCurrentRow As Long
        With gridStoredPayments
            If Not .CurrentRow Is Nothing Then
                If lNewRow = -1 Then
                    ' no row set from calling procedure
                    lCurrentRow = .CurrentRow.Index
                Else
                    lCurrentRow = lNewRow
                End If

                nTempActiveBabelIndex = CDbl(.Rows.Item(lCurrentRow).Cells(colSPBabelIndex).Value)
                nTempActiveBatchIndex = CDbl(.Rows.Item(lCurrentRow).Cells(colSPBatchIndex).Value)
                nTempActivePaymentIndex = CDbl(.Rows.Item(lCurrentRow).Cells(colSPPaymentIndex).Value)
                '

                If Not bOnlyTempIndexes Then
                    nActiveBabelIndex = CDbl(.Rows.Item(lCurrentRow).Cells(colSPBabelIndex).Value)
                    nActiveBatchIndex = CDbl(.Rows.Item(lCurrentRow).Cells(colSPBatchIndex).Value)
                    nActivePaymentIndex = CDbl(.Rows.Item(lCurrentRow).Cells(colSPPaymentIndex).Value)
                    lUnmatchedPaymentsPriorToActivePayment = Val(.Rows.Item(lCurrentRow).Cells(colSPOpenSoFar).Value)
                End If
            End If
        End With

    End Sub

    Private Sub gridStoredPayments_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gridStoredPayments.KeyUp

        If e.KeyCode = 13 Then
            StoreActiveIndexes(False)
            If TestLocking() Then
                bPaymentPicked = True
                Me.Close()
            End If
        End If

    End Sub

    Private Sub ShowMatchinfo()
        ' After click on Payment,  present spread-info at payment-level
        MatchedSpreadHeadings()
        MatchedSpreadPopulate()

        '' must also move rest of the controls according to the grids;
        'Me.Line1.Y1 = Me.Height - 175
        'Me.Line1.Y2 = Me.Height - 175
        'Me.Frame1.Top = Me.Height - 170

        'Me.cmdSearchAmount.Top = Me.Frame1.Top + 10
        'Me.cmdSearchName.Top = Me.Frame1.Top + 30

        'Me.lblSearchAmount.Top = Me.Frame1.Top + 5
        'Me.LblSearchName.Top = Me.Frame1.Top + 25
        'Me.txtSearchName.Top = Me.Frame1.Top + 25
        'Me.txtSearchAmount.Top = Me.Frame1.Top + 5

        'Me.Line2.Y1 = Me.Height - 50
        'Me.Line2.Y2 = Me.Height - 50

        'Me.cmdCancel.Top = Me.Height - 45
        'Me.cmdCancel. = Me.Width - 100
        'Me.cmdPrintThis.Top = Me.Height - 45
        'Me.cmdChoose.Top = Me.Height - 45


    End Sub
    Private Sub MatchedSpreadHeadings()

        'Payment spread - present headings for spread
        Dim i As Short
        Dim txtColumn As DataGridViewTextBoxColumn

        With gridStoredMatched
            .Rows.Clear()  ' empty content of gridMatched
            .Columns.Clear()
            .Top = Me.Height - 280
            .Height = 100
            .Width = gridStoredPayments.Width
            .Left = gridStoredPayments.Left

            If .ColumnCount = 0 Then
                .ScrollBars = ScrollBars.Both
                .SelectionMode = DataGridViewSelectionMode.CellSelect
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.FixedSingle
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .RowsDefaultCellStyle.SelectionBackColor = Color.White
                .RowsDefaultCellStyle.SelectionForeColor = Color.Black
                .CellBorderStyle = DataGridViewCellBorderStyle.None
                .ReadOnly = True
                '.AutoSize = True

            End If

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = "INVOICESEEK"
            txtColumn.Visible = False
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = sBBRET_CustomerNoLabel  'LRSCommon(41001) 'CustomerNo
            txtColumn.Width = WidthFromSpreadToGrid(8)
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = sBBRET_InvoiceNoLabel  'LRSCommon(41002) 'invoiceno
            txtColumn.Width = WidthFromSpreadToGrid(8)
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRSCommon(41003) 'Debit
            txtColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            txtColumn.ValueType = GetType(Decimal)
            txtColumn.DefaultCellStyle.Format = "##,##0.00"
            txtColumn.Width = WidthFromSpreadToGrid(8)
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRSCommon(41004) 'Credit
            txtColumn.ValueType = GetType(Decimal)
            txtColumn.DefaultCellStyle.Format = "##,##0.00"
            txtColumn.Width = WidthFromSpreadToGrid(8)
            txtColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = sBBRET_CurrencyLabel  'LRSCommon(41005) 'Currency
            txtColumn.Width = WidthFromSpreadToGrid(5)
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRSCommon(41007) 'Ledger Account
            txtColumn.Width = WidthFromSpreadToGrid(6)
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            ' New 10.03.2004
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = LRSCommon(41008) 'Supplier
            txtColumn.Width = WidthFromSpreadToGrid(5)
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = sBBRET_VoucherNoLabel  'LRSCommon(41009) 'VoucherNo
            txtColumn.Width = WidthFromSpreadToGrid(8)
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.HeaderText = sBBRET_FreetextLabel   'LRSCommon(41010) 'Freetext
            txtColumn.Width = WidthFromSpreadToGrid(15)
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn  ' MatchID
            txtColumn.Visible = False
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn  ' aKontoID
            txtColumn.Visible = False
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn  ' MyField
            txtColumn.Visible = False
            txtColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            .Columns.Add(txtColumn)

            For i = 0 To .ColumnCount - 1
                If Mid(sMatchedColumns, i + 1, 1) = "0" Then
                    .Columns.Item(i + 1).Visible = False
                End If
            Next i

        End With

    End Sub
    Private Sub MatchedSpreadPopulate()
        ' nPaymentIndex: Index for chosen payment
        ' Fill data into invoice spread:

        Dim i As Double
        Dim sTempText As String
        Dim sMySQL As String

        With Me.gridStoredMatched

            i = 0

            If nTempActiveBabelIndex >= 0 Then
                'New code 18.07.2007 - retrieve data from the BBDB instead of using collections

                sMySQL = "SELECT P.MON_InvoiceCurrency, P.VoucherNo, P.MATCH_Matched, I.MATCH_Type, I.MATCH_ID, I.MATCH_Final, "
                sMySQL = sMySQL & "I.CustomerNo, I.InvoiceNo, I.MON_InvoiceAmount, I.Invoice_ID "
                sMySQL = sMySQL & "FROM Payment P, Invoice I WHERE P.Company_ID = I.Company_ID AND "
                sMySQL = sMySQL & "P.BabelFile_ID = I.BabelFile_ID AND P.Batch_ID = I.Batch_ID AND P.Payment_ID = I.Payment_ID AND "
                sMySQL = sMySQL & "I.Company_ID = " & Trim(Str(iCompany_ID)) '.NET - 19.05.2010 - Changed from P.Company_ID to I.Company_ID
                sMySQL = sMySQL & " AND I.BabelFile_ID = " & Trim(Str(nTempActiveBabelIndex)) '.NET - 19.05.2010 - As above
                sMySQL = sMySQL & " AND I.Batch_ID = " & Trim(Str(nTempActiveBatchIndex)) '.NET - 19.05.2010 - As above
                sMySQL = sMySQL & " AND I.Payment_ID = " & Trim(Str(nTempActivePaymentIndex)) '.NET - 19.05.2010 - As above

                .Rows.Clear()
                i = 0

                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then

                    Do While oMyDal.Reader_ReadRecord

                        If CInt(oMyDal.Reader_GetString("MATCH_Matched")) < 0.5 Then '0 = Unmatched, don't show any invoiceinfo
                            Exit Do
                        Else
                            If CBool(oMyDal.Reader_GetString("MATCH_Final")) Then
                                i = i + 1
                                .Rows.Add()

                                Select Case CInt(oMyDal.Reader_GetString("MATCH_Type"))

                                    Case vbBabel.BabelFiles.MatchType.MatchedOnGL
                                        '.Col = 7 'Ledger Account
                                        '.Text = rsInvoice.Fields("MATCH_ID").Value
                                        'TODO Column = 6, because in .net we start with 0,and in vb6 with 1 ?????
                                        .Rows.Item(.RowCount - 1).Cells(6).Value = oMyDal.Reader_GetString("MATCH_ID")
                                    Case vbBabel.BabelFiles.MatchType.MatchedOnSupplier
                                        '.Col = 8 ''Supplier
                                        '.Text = rsInvoice.Fields("CustomerNo").Value
                                        .Rows.Item(.RowCount - 1).Cells(7).Value = oMyDal.Reader_GetString("CustomerNo")
                                    Case Else 'Normally matched on invoice (0)
                                        '.Col = 2 'CustomerNo
                                        '.Text = rsInvoice.Fields("CustomerNo").Value
                                        .Rows.Item(.RowCount - 1).Cells(1).Value = oMyDal.Reader_GetString("CustomerNo")
                                End Select

                                '.Col = 3 'InvoiceNo
                                .Rows.Item(.RowCount - 1).Cells(2).Value = oMyDal.Reader_GetString("InvoiceNo")

                                If CDbl(oMyDal.Reader_GetString("MON_InvoiceAmount")) < 0 Then
                                    '.Col = 4 'DebitAmount
                                    '.TypeHAlign = SS_CELL_H_ALIGN_RIGHT
                                    .Rows.Item(.RowCount - 1).Cells(3).Value = CDbl(oMyDal.Reader_GetString("MON_InvoiceAmount")) * -1 / 100  'VB6.Format(CDbl((oMyDal.Reader_GetString("MON_InvoiceAmount")) * -1 / 100), "##,##0.00") ' remove - sign
                                Else
                                    '.Col = 5
                                    '.TypeHAlign = SS_CELL_H_ALIGN_RIGHT
                                    .Rows.Item(.RowCount - 1).Cells(4).Value = CDbl(oMyDal.Reader_GetString("MON_InvoiceAmount")) / 100 ' VB6.Format(CDbl((oMyDal.Reader_GetString("MON_InvoiceAmount")) / 100), "##,##0.00")
                                End If

                                '.Col = 6 'Currency
                                .Rows.Item(.RowCount - 1).Cells(5).Value = oMyDal.Reader_GetString("MON_InvoiceCurrency")

                                '.Col = 9 'VoucherNo
                                .Rows.Item(.RowCount - 1).Cells(8).Value = oMyDal.Reader_GetString("VoucherNo")

                                '.Col = 10 'Freetext
                                sTempText = ""

                                sMySQL = "SELECT XText FROM [Freetext] WHERE "
                                sMySQL = sMySQL & "Company_ID = " & iCompany_ID.ToString
                                sMySQL = sMySQL & " AND BabelFile_ID = " & nTempActiveBabelIndex.ToString
                                sMySQL = sMySQL & " AND Batch_ID = " & nTempActiveBatchIndex.ToString
                                sMySQL = sMySQL & " AND Payment_ID = " & nTempActivePaymentIndex.ToString
                                sMySQL = sMySQL & " AND Invoice_ID = " & oMyDal.Reader_GetString("Invoice_ID")
                                sMySQL = sMySQL & " AND Qualifier = 4"

                                oMyDalFreetext.SQL = sMySQL
                                If oMyDalFreetext.Reader_Execute() Then
                                    Do While oMyDalFreetext.Reader_ReadRecord
                                        sTempText = sTempText & oMyDalFreetext.Reader_GetString("XText")
                                    Loop
                                Else
                                    Throw New Exception(LRSCommon(45002) & oMyDalFreetext.ErrorMessage)
                                End If

                                .Rows.Item(.RowCount - 1).Cells(9).Value = sTempText
                            Else

                            End If
                        End If

                        ' 27.05.2016
                        ' Fill max 100 records into this grid. There will be no practical use of more than 100 in this situation
                        If i > 100 Then
                            Exit Do
                        End If
                    Loop

                Else

                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

                End If

            Else
                'Nothing to show
            End If


        End With

    End Sub

    Private Sub gridStoredPayments_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridStoredPayments.RowEnter
        '    Private Sub gridStoredPayments_RowLeave(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridStoredPayments.RowLeave
        'If EventArgs.NewRow > 0 Then
        If e.RowIndex >= 0 Then
            'gridStoredPayments.Row = EventArgs.NewRow
            StoreActiveIndexes(True, False, e.RowIndex)
            ' fyll opp invoice-grid ...
            '.NET - 19.05.2010 - Commented next line. Should not be necessary, because it will fire the function MatchedSpreadPopulate
            '  which populates the invoicegrid an additional time.
            ShowMatchinfo()
        End If

    End Sub
    Private Sub txtSearchName_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSearchName.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        Dim lCounter As Integer
        Dim lRowPosition As Integer
        Dim iTextLength As Short
        Dim bRowPositionFound As Boolean

        Try
            If Len(txtSearchName.Text) = 0 Then
                For lCounter = 0 To gridStoredPayments.RowCount - 1
                    'Check if the characters entered so far fits a name
                    gridStoredPayments.Rows.Item(lCounter).Visible = True
                    If gridStoredPayments.Rows.Item(lCounter).Cells(colSPHidden).Value = "True" Then
                        gridStoredPayments.Rows.Item(lCounter).Visible = False
                    Else
                        If gridStoredPayments.Rows.Item(lCounter).Cells(colSPExistInERP).Value = "False" Then
                            gridStoredPayments.Rows.Item(lCounter).Visible = False
                        Else
                            gridStoredPayments.Rows.Item(lCounter).Visible = True
                        End If
                    End If
                Next lCounter
                StoreActiveIndexes(True)
                gridStoredPayments.Rows.Item(0).Cells(0).Selected = True
                ShowMatchinfo()
            Else
                If KeyCode = System.Windows.Forms.Keys.Return Or KeyCode = System.Windows.Forms.Keys.Tab Then
                    FindName()
                Else
                    'Check if any text is entered
                    If Len(txtSearchName.Text) > 0 Then
                        'Check if the namecolumn is alreadysorted

                        'Sort the name column
                        lSortCol = colSPName
                        ' TODO Trenger vi � sortere hver gang ????????
                        If Not gridStoredPayments.SortedColumn Is Nothing Then
                            lSortCol = gridStoredPayments.SortedColumn.Index
                            If gridStoredPayments.SortedColumn.Index <> colSPName Then
                                gridStoredPayments.Sort(gridStoredPayments.Columns.Item(colSPName), System.ComponentModel.ListSortDirection.Ascending)
                                gridStoredPayments.Refresh()
                            End If
                        Else
                            gridStoredPayments.Sort(gridStoredPayments.Columns.Item(colSPName), System.ComponentModel.ListSortDirection.Ascending)
                            gridStoredPayments.Refresh()
                        End If

                        iTextLength = Len(txtSearchName.Text)
                        bRowPositionFound = False
                        If Not gridStoredPayments.CurrentRow Is Nothing Then
                            lRowPosition = gridStoredPayments.CurrentRow.Index
                        End If

                        For lCounter = 0 To gridStoredPayments.RowCount - 1
                            If gridStoredPayments.Rows.Item(lCounter).Cells.Item(colSPHidden).Value = "True" Then
                                gridStoredPayments.Rows.Item(lCounter).Visible = False
                            Else
                                If gridStoredPayments.Rows.Item(lCounter).Cells.Item(colSPExistInERP).Value = "False" Then
                                    gridStoredPayments.Rows.Item(lCounter).Visible = False
                                Else
                                    gridStoredPayments.Rows.Item(lCounter).Visible = True
                                    'Check if the characters entered so far fits a name
                                    If UCase(txtSearchName.Text) = UCase(VB.Left(gridStoredPayments.Rows.Item(lCounter).Cells.Item(colSPName).Value, iTextLength)) Then
                                        'Set rowposition to the row found
                                        If Not bRowPositionFound Then
                                            bRowPositionFound = True
                                            lRowPosition = lCounter
                                        End If
                                        ' 07.10.2010 may just as well exit here (?)
                                        Exit For
                                    End If
                                End If
                            End If
                        Next lCounter
                        'Set active row = row position, will be the same if the searched text isn't found
                        ' "unselect" last marked row;

                        'gridStoredPayments.Rows.Item(lRowPosition).Selected = True
                        StoreActiveIndexes(True, True, lRowPosition)
                        'gridStoredPayments.FirstDisplayedScrollingRowIndex = lRowPosition
                        gridStoredPayments.FirstDisplayedScrollingRowIndex = gridStoredPayments.Rows(lRowPosition).Index
                        gridStoredPayments.Refresh()
                        gridStoredPayments.CurrentCell = gridStoredPayments.Rows(lRowPosition).Cells(1)
                        gridStoredPayments.Rows(lRowPosition).Selected = True

                        ShowMatchinfo()
                    End If
                    End If
            End If
        Catch
            MsgBox(Err.Description, MsgBoxStyle.Critical, "frmStoredPayments")
        End Try

    End Sub
    Private Sub FindName()
        ' Find name from userinput in txtSearchName
        Dim bRowPositionFound As Boolean
        Dim lCounter As Integer
        Dim lRowPosition As Integer

        If Len(txtSearchName.Text) > 0 Then
            bRowPositionFound = False
            For lCounter = 0 To gridStoredPayments.RowCount - 1
                'Check if the characters entered so far fits a name
                If InStr(1, UCase(gridStoredPayments.Rows.Item(lCounter).Cells.Item(colSPName).Value), UCase(txtSearchName.Text), CompareMethod.Text) > 0 Then
                    gridStoredPayments.Rows.Item(lCounter).Visible = True
                    If gridStoredPayments.Rows.Item(lCounter).Cells.Item(colSPHidden).Value = "True" Then
                        gridStoredPayments.Rows.Item(lCounter).Visible = False
                    Else
                        If gridStoredPayments.Rows.Item(lCounter).Cells.Item(colSPExistInERP).Value = "False" Then
                            gridStoredPayments.Rows.Item(lCounter).Visible = False
                        Else
                            gridStoredPayments.Rows.Item(lCounter).Visible = True
                        End If
                    End If
                Else
                    gridStoredPayments.Rows.Item(lCounter).Visible = False
                End If
                'Set rowposition to the row found
                If Not bRowPositionFound Then
                    If gridStoredPayments.Rows.Item(lCounter).Visible = True Then
                        lRowPosition = lCounter
                        bRowPositionFound = True
                    End If
                End If

            Next lCounter
            gridStoredPayments.Rows.Item(lRowPosition).Cells(0).Selected = True
            gridStoredPayments.Refresh()
            StoreActiveIndexes(True)
            ShowMatchinfo()
        End If

    End Sub

    Private Sub txtSearchAmount_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSearchAmount.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        If KeyCode = System.Windows.Forms.Keys.Return Or KeyCode = System.Windows.Forms.Keys.Tab Then
            FindAmount()
        End If

    End Sub
    Private Sub FindAmount()
        Dim lCounter As Integer
        Dim lRowPosition As Integer
        Dim sNewText As String
        Dim bRowPositionFound As Boolean

        'Check if any text is entered
        If Len(txtSearchAmount.Text) > 0 Then
            'Check if the amountcolumn is alreadysorted
            If lSortCol <> colSPAmount Then
                'Sort the name column
                lSortCol = colSPAmount
                gridStoredPayments.Sort(gridStoredPayments.Columns.Item(colSPAmount), System.ComponentModel.ListSortDirection.Ascending)
            End If
            sNewText = ConvertToAmount((Me.txtSearchAmount).Text, ",.", ",.")
            bRowPositionFound = False
            lRowPosition = gridStoredPayments.CurrentRow.Index
            'For lCounter = gridStoredPayments.Row To gridStoredPayments.MaxRows
            For lCounter = 0 To gridStoredPayments.RowCount - 1
                If gridStoredPayments.Rows.Item(lCounter).Cells.Item(colSPHidden).Value = "True" Then
                    gridStoredPayments.Rows.Item(lCounter).Visible = False
                Else
                    If gridStoredPayments.Rows.Item(lCounter).Cells.Item(colSPExistInERP).Value = "False" Then
                        gridStoredPayments.Rows.Item(lCounter).Visible = False
                    Else
                        gridStoredPayments.Rows.Item(lCounter).Visible = True
                        'Check if the characters entered so far fits amount
                        'If sNewText = Replace(Replace(Replace(gridStoredPayments.Rows.Item(lCounter).Cells.Item(colSPAmount).Value, ".", ""), ",", ""), Chr(160), "") Then
                        ' 09.01.2017 changed If-test
                        If CDbl(sNewText) = CDbl(gridStoredPayments.Rows.Item(lCounter).Cells.Item(colSPAmount).Value) * 100 Then ', ".", ""), ",", ""), Chr(160), "") Then
                            'Set rowposition to the row found
                            If Not bRowPositionFound Then
                                bRowPositionFound = True
                                lRowPosition = lCounter
                            End If
                            ' added 07.10.2010 - just jump out when found amount (!?)
                            Exit For
                        End If
                    End If
                End If
            Next lCounter
            'Set active row = row position, will be the same if the searched text isn't found
            gridStoredPayments.Rows.Item(lRowPosition).Cells(0).Selected = True
            gridStoredPayments.CurrentCell = gridStoredPayments.Rows.Item(lRowPosition).Cells(0)
            gridStoredPayments.Refresh()
            StoreActiveIndexes(True)
            ShowMatchinfo()
        End If

    End Sub
    Private Sub ResetSearch()

        Me.txtSearchName.Text = ""
        Me.txtSearchAmount.Text = ""
    End Sub
    Private Function TestLocking() As Boolean
        ' Test if selected payment is locked
        Dim bRetValue As Boolean
        ' test for locking, but not if current payment from match_manual
        If Not (nActiveBabelIndex = nInitBabelIndex And nActiveBatchIndex = nInitBatchIndex And nActivePaymentIndex = nInitPaymentIndex) Then

            'If frmMATCH_Manual.IsPaymentLocked(lActiveBabelIndex, lActiveBatchIndex, lActivePaymentIndex) Then
            If IsPaymentLocked(nActiveBabelIndex, nActiveBatchIndex, nActivePaymentIndex, bManMatchFromERP) Then
                MsgBox(LRS(60183), MsgBoxStyle.OkOnly + MsgBoxStyle.Information) '"This payment is in use by another user"
                bRetValue = False
            Else
                bRetValue = True
            End If
        Else
            bRetValue = True
        End If
        TestLocking = bRetValue
    End Function

End Class
