﻿
Imports System
Imports System.Security.Cryptography
Imports System.Security.Cryptography.Xml
Imports System.Xml
Imports DidiSoft.Pgp
Imports System.Security.Cryptography.X509Certificates
Imports System.IO
Module vbSecurity

    Public Function AddPGPSecurity(ByRef aExportInformation(,) As String, ByRef iRun As Short, ByRef oFilesetup As vbBabel.FileSetup) As Boolean
        ' added 05.01.2017
        Dim pgp As New PGPLib()
        Dim sFilenameToCrypt As String = ""
        Dim tmpFilename As String = ""
        Dim sPublicBankKey As String = ""
        Dim sPrivateKey As String = ""
        Dim ks As KeyStore = New KeyStore("local.keystore", "VB00998776878787")  ' random

        ' ------------------------------------------------------------------------------------------------------------
        ' - 19.01.2017
        '   Developed so far for 
        '       - DNB PGP Security, Encryption
        '       - DNB PGP Security, Signing
        '
        ' ------------------------------------------------------------------------------------------------------------
        ' 10.05.2021 - use importfilename for special format NordeaSecureEnvelope
        If aExportInformation(0, iRun) = vbBabel.BabelFiles.FileType.NordeaSecureEnvelope Then
            sFilenameToCrypt = oFilesetup.FileNameIn1
        Else
            sFilenameToCrypt = aExportInformation(3, iRun)
        End If
        ' Does file exist?
        If Dir(sFilenameToCrypt) <> "" Then

            ' Rename file from original to a tempfilename
            ' add .tmp
            tmpFilename = sFilenameToCrypt & ".tmp"
        End If


        If oFilesetup.secEncrypt Then
            '----------------------------------------
            ' CRYPT file
            '----------------------------------------
            If Dir(tmpFilename) <> "" Then
                Kill(tmpFilename) ' Delete in case it exists
            End If
            ' Rename the file;
            Rename(sFilenameToCrypt, tmpFilename)

            ' use public key as key for encryption
            'sPublicKeyFile = oFilesetup.secFileNamePublicKey

            ' import content from banks public key into pgp keystore
            'ks.ImportPublicKey(oFilesetup.secBANKPublicKey)
            ks.ImportPublicKey(oFilesetup.secFileNameBankPublicKey)

            pgp.Cypher = DirectCast([Enum].Parse(GetType(CypherAlgorithm), oFilesetup.secCypher), CypherAlgorithm)
            pgp.Hash = DirectCast([Enum].Parse(GetType(DidiSoft.Pgp.HashAlgorithm), oFilesetup.secHash), DidiSoft.Pgp.HashAlgorithm)
            pgp.Compression = DirectCast([Enum].Parse(GetType(CompressionAlgorithm), oFilesetup.secCompression), CompressionAlgorithm)

            pgp.EncryptFile(tmpFilename, ks, "", sFilenameToCrypt, True, False)
        End If

        If oFilesetup.secSign Then
            '----------------------------------------
            ' SIGN file
            '----------------------------------------

            If Dir(tmpFilename) <> "" Then
                Kill(tmpFilename) ' Delete in case it exists
            End If
            ' Rename the file;
            Rename(sFilenameToCrypt, tmpFilename)

            ' import our private and public key into keystore
            ks.ImportPrivateKey(oFilesetup.secOurPrivateKey)

            pgp.SignFile(tmpFilename, ks, oFilesetup.secUserID, BBDecrypt(oFilesetup.secPassword, 8197), sFilenameToCrypt, False)
        End If

    End Function
    Public Function AddSecureEnvelope(ByRef sFilenameToCrypt As String, ByRef iRun As Short, ByRef oFilesetup As vbBabel.FileSetup, ByVal bSilent As Boolean, ByVal bLog As Boolean, ByVal oBabelLog As vbLog.vbLogging) As Boolean
        ' added 20.01.2017
        'Dim sFilenameToCrypt As String = ""
        Dim tmpFilename As String = ""
        Dim sPublicBankKey As String = ""
        Dim sPrivateKey As String = ""
        Dim SecureEnvelopeDocument As New XmlDocument
        Dim XMLComment As System.Xml.XmlComment
        Dim rootNode As System.Xml.XmlElement
        Dim nodNewNode As XmlElement

        Dim apiNS As String = ""
        Dim xsiNS As String = ""
        Dim sTmp As String = ""

        Dim sProgramVersion As String = ""
        Dim sErrorString As String = ""

        Dim cert As X509Certificate2
        Dim bFoundSecureEnvelopeSertificate As Boolean = False
        Dim byteArray() As Byte


        Try

            sErrorString = "At top for AddSecureEnvelope"

            ' HANDLE Certificate from Nordea
            '-------------------------------
            sErrorString = "Handles Certificate"
            ' The path to the certificate.
            'Dim Certificate As String = oFilesetup.secFileNameBankPublicKey '"C:\Slett\SecureEnvelope\WSNDEA1234.p12"
            ' TESTcertificate from Nordea:
            'SignerID: 5780860238
            'PIN = WSNDEA1234
            'Dim sPassWord = ""    ' "WSNDEA1234" for nordea testcertificate

            'sPassWord = oFilesetup.secPassword

            ' 11.10.2017
            ' If filename given, then import certificate from this file;

            If System.IO.File.Exists(oFilesetup.secFileNamePublicKey) Then
                ' The path to the certificate.
                Dim Certificate As String = oFilesetup.secFileNamePublicKey '"C:\Slett\SecureEnvelope\WSNDEA1234.p12"
                ' if empty filestring, check in "out" part of oFilesetup;
                'Certificate = oFilesetup.secFileNamePublicKey '"C:\Slett\SecureEnvelope\WSNDEA1234.p12"

                ' TESTcertificate from Nordea:
                'SignerID: 5780860238
                'PIN = WSNDEA1234
                Dim sPassWord = ""    ' "WSNDEA1234" for nordea testcertificate
                sPassWord = BBDecrypt(oFilesetup.secPassword, 8197)
                'Load the certificate into an X509Certificate object.
                cert = New X509Certificate2
                cert.Import(Certificate, sPassWord, X509KeyStorageFlags.DefaultKeySet)  ', X509KeyStorageFlags.MachineKeySet Or X509KeyStorageFlags.PersistKeySet Or X509KeyStorageFlags.Exportable)
                bFoundSecureEnvelopeSertificate = True
            Else

                ' Finn Nordea Sertifikat i Windows Certificate Store;
                '------------------------------------------------------
                Dim x509Store As New X509Store(StoreName.My, StoreLocation.CurrentUser)

                ' create and open store for read-only access
                x509Store.Open(OpenFlags.ReadOnly)
                ' search store
                For Each cert In x509Store.Certificates
                    If cert.IssuerName.Name.IndexOf("Nordea Corporate") > -1 Then
                        bFoundSecureEnvelopeSertificate = True
                        Exit For
                    End If
                Next
                x509Store.Close()

                ' then try to find certificate in LocalMachine if not found in CurrentUser
                If bFoundSecureEnvelopeSertificate = False Then
                    x509Store = New X509Store(StoreName.My, StoreLocation.LocalMachine)
                    x509Store.Open(OpenFlags.ReadOnly)
                    For Each cert In x509Store.Certificates
                        If cert.IssuerName.Name.IndexOf("Nordea Corporate") > -1 Then
                            bFoundSecureEnvelopeSertificate = True
                            Exit For
                        End If
                    Next
                    x509Store.Close()
                End If
            End If



            If bFoundSecureEnvelopeSertificate Then

                ' test på sertifikatets utløpsdato;
                ' husk silent!!!
                If DateDiff("d", Date.Today, cert.NotAfter) < 0 Then
                    If Not bSilent Then
                        MsgBox("Sertifikatet er utløpt." & vbCrLf & "Utløpsdato: " & cert.NotAfter)
                    End If
                    If bLog Then
                        oBabelLog.Heading = "BabelBank Warning: " & "Sertifikat"
                        oBabelLog.AddLogEvent("Sertifikatet er utløpt." & vbCrLf & "Utløpsdato: " & cert.NotAfter, System.Diagnostics.TraceEventType.Warning)
                    End If
                ElseIf DateDiff("d", Date.Today, cert.NotAfter) < 30 Then
                    If Not bSilent Then
                        MsgBox("Sertifikatet utløper snart." & vbCrLf & "Utløpsdato: " & cert.NotAfter)
                    End If
                    If bLog Then
                        oBabelLog.Heading = "BabelBank Warning: " & "Sertifikat"
                        oBabelLog.AddLogEvent("Sertifikatet utløper snart." & vbCrLf & "Utløpsdato: " & cert.NotAfter, System.Diagnostics.TraceEventType.Warning)
                    End If
                End If
                sErrorString = "Handles files"
                ' 10.05.2021 sFilenameToCrypt from parameter instead
                ' sFilenameToCrypt = aExportInformation(3, iRun)

                ' Does file exist?
                If Dir(sFilenameToCrypt) <> "" Then
                    ' Rename file from original to a tempfilename
                    ' add .tmp
                    tmpFilename = sFilenameToCrypt & ".tmp"
                End If
                If Dir(tmpFilename) <> "" Then
                    Kill(tmpFilename) ' Delete in case it exists
                End If
                ' Rename the file;
                Rename(sFilenameToCrypt, tmpFilename)

                '' keep a copy for test;
                'If Today.ToString = "30.10.2018 00:00:00" Then
                '    System.IO.File.Copy(tmpFilename, tmpFilename & "tmp")
                'End If

                sErrorString = "Before adding elements to .wrapped filed"

                ' Create a new XML-file, the SecureEnvelope file
                rootNode = SecureEnvelopeDocument.CreateElement("ApplicationRequest")
                'SecureEnvelopeDocument.InsertBefore(XMLDeclaration, SecureEnvelopeDocument.DocumentElement)
                'If RunTime() Then  'DISKUTERNOTRUNTIME Dette blir feil i 2017
                sProgramVersion = Replace(System.Diagnostics.FileVersionInfo.GetVersionInfo(My.Application.Info.DirectoryPath & "\vbbabel.dll").ProductVersion, ".0.", ".")
                'Else
                '    sProgramVersion = Replace(System.Diagnostics.FileVersionInfo.GetVersionInfo("C:\Projects.net\BabelBank\Bin\x86\release\vbbabel.dll").ProductVersion, ".0.", ".")
                'End If

                ' root
                XMLComment = SecureEnvelopeDocument.CreateComment("Created by BabelBank from Visual Banking AS, version " & sProgramVersion)
                ' Midlertidig fjernet pga test i Nordea - :  SecureEnvelopeDocument.AppendChild(XMLComment)
                apiNS = "http://bxd.fi/xmldata/"
                xsiNS = "http://www.w3.org/2001/XMLSchema-instance/"
                rootNode.SetAttribute("xmlns", apiNS)
                rootNode.SetAttribute("xmlns:xsi", xsiNS)

                sErrorString = "Adds CustomerID"
                nodNewNode = SecureEnvelopeDocument.CreateElement("CustomerId")
                nodNewNode.InnerText = oFilesetup.secUserID  ' SenderID = CustomerID in file 'aExportInformation(10, iRun)
                rootNode.AppendChild(nodNewNode)

                sErrorString = "Adds Command"
                nodNewNode = SecureEnvelopeDocument.CreateElement("Command")
                nodNewNode.InnerText = "UploadFile"
                rootNode.AppendChild(nodNewNode)

                sErrorString = "Adds Timestampe"
                sTmp = Format(Now(), "yyyy-MM-dd")
                sTmp = sTmp & "T"
                sTmp = sTmp & Format(Now(), "HH") & ":" & Format(Now(), "mm") & ":" & Format(Now(), "ss")
                nodNewNode = SecureEnvelopeDocument.CreateElement("Timestamp")
                nodNewNode.InnerText = sTmp
                rootNode.AppendChild(nodNewNode)

                ' Status is Mandatory, but how to use it?
                'nodNewNode = SecureEnvelopeDocument.CreateElement("Status")
                'nodNewNode.InnerText = "ALL"
                'rootNode.AppendChild(nodNewNode)

                sErrorString = "Adds Environment"
                nodNewNode = SecureEnvelopeDocument.CreateElement("Environment")
                nodNewNode.InnerText = "PRODUCTION"
                rootNode.AppendChild(nodNewNode)

                sErrorString = "Adds TargetID"
                nodNewNode = SecureEnvelopeDocument.CreateElement("TargetId")
                ' value of serialnumber from certificate

                ' SerialNumber is found in Subject, like    Subject	"SERIALNUMBER=5780860238, CN=Nordea Demo Certificate, C=FI"	String
                sTmp = cert.Subject
                ' take serialnumber part
                sTmp = xDelim(sTmp, ",", 1)
                sTmp = Replace(sTmp, "SERIALNUMBER=", "")
                'sTmp = "1892710226"
                nodNewNode.InnerText = sTmp
                rootNode.AppendChild(nodNewNode)

                If oFilesetup.secCompression = "GZIP" Then
                    ' 18.10.2018 - added GZip comression
                    sErrorString = "Adds Compression node"
                    nodNewNode = SecureEnvelopeDocument.CreateElement("Compression")
                    nodNewNode.InnerText = "true"
                    rootNode.AppendChild(nodNewNode)

                    sErrorString = "Adds compression type"
                    nodNewNode = SecureEnvelopeDocument.CreateElement("CompressionMethod")
                    nodNewNode.InnerText = "GZIP"
                    rootNode.AppendChild(nodNewNode)
                End If

                sErrorString = "Adds SoftwareId"
                nodNewNode = SecureEnvelopeDocument.CreateElement("SoftwareId")
                nodNewNode.InnerText = "BabelBank, version " & Replace(System.Diagnostics.FileVersionInfo.GetVersionInfo(My.Application.Info.DirectoryPath & "\vbbabel.dll").ProductVersion, ".0.", ".")
                rootNode.AppendChild(nodNewNode)

                sErrorString = "Adds FileType"
                nodNewNode = SecureEnvelopeDocument.CreateElement("FileType")
                nodNewNode.InnerText = "NDCAPXMLI"  ' Pain.001
                rootNode.AppendChild(nodNewNode)


                GC.Collect()
                If oFilesetup.secCompression = "GZIP" Then
                    sErrorString = "Adds GZIP"
                    ' 18.10.2018 - starts using gzip
                    ' thus read from file to a bitarray, and compress, before Base64
                    byteArray = System.IO.File.ReadAllBytes(tmpFilename)

                    ' gzip it
                    byteArray = GZIPCompress(byteArray)
                    'GC.Collect()
                    sTmp = Convert.ToBase64String(byteArray)
                    'GC.Collect()
                Else
                    sTmp = Convert.ToBase64String(System.IO.File.ReadAllBytes(tmpFilename))
                End If

                sErrorString = "Adds Content"
                ' 18.10.2018 - before GZip
                'sTmp = Convert.ToBase64String(System.IO.File.ReadAllBytes(tmpFilename))
                ' 18.10.2018  with gzip
                'sTmp = Convert.ToBase64String(byteArray)

                nodNewNode = SecureEnvelopeDocument.CreateElement("Content")
                nodNewNode.InnerText = sTmp
                rootNode.AppendChild(nodNewNode)

                SecureEnvelopeDocument.AppendChild(rootNode)


                rootNode = Nothing

                sErrorString = "Before saving wrapped file"
                '' Save the document, with .wrapped as extension
                ' if file exists then first delete it;
                'If Dir(sFilenameToCrypt & ".wrapped") <> "" Then
                'Kill(sFilenameToCrypt & ".wrapped")
                'End If
                If File.Exists(sFilenameToCrypt & ".wrapped") Then
                    File.Delete(sFilenameToCrypt & ".wrapped")
                End If

                sErrorString = "Saves wrapped file"
                SecureEnvelopeDocument.Save(sFilenameToCrypt & ".wrapped")

                ' --------------------------------------------------------------------
                ' At this stage, we have saved the "wrapped document"
                ' What is needed as the last process is to sign the wrapped document
                ' --------------------------------------------------------------------

                ' KAN VI BRUKE OpenPGP library fra Didisoft til dette?

                sErrorString = "Dims parameters for Securitytag"
                '' Create a new CspParameters object to specify a key container.
                Dim cspParams As New CspParameters()
                cspParams.KeyContainerName = "XML_DSIG_RSA_KEY"
                ' Create a new RSA signing key and save it in the container. 
                Dim rsaPublicKey As New RSACryptoServiceProvider(cspParams)
                rsaPublicKey = cert.PublicKey.Key
                Dim rsaPrivateKey As New RSACryptoServiceProvider(cspParams)
                rsaPrivateKey = cert.PrivateKey
                ' Create a new XML document.
                Dim xmlDoc As New XmlDocument()

                sErrorString = "Loads wrapped file"
                ' Load an XML file into the XmlDocument object.
                xmlDoc.Load(sFilenameToCrypt & ".wrapped")
                xmlDoc.PreserveWhitespace = True
                xmlDoc.Normalize()  '01.11.2018
                sErrorString = "Signs wrapped file with SignXML function"

                ' Sign the XML document. 
                SignXml(xmlDoc, rsaPublicKey, rsaPrivateKey)
                'xmlDoc.Save(sFilenameToCrypt)
                'SignXmlFile(sFilenameToCrypt, sFilenameToCrypt & ".txt", rsaKey)

                'Console.WriteLine("XML file signed.")


                ' THEN, add add KeyInfo tag with X509 certificate

                sErrorString = "Adds KeyInfo"
                Dim childNode As XmlElement
                Dim childNode2 As XmlElement
                Dim childNode3 As XmlElement
                Dim childNode4 As XmlElement

                'xsiNS = "http://bxd.fi/xmldata/"
                'nodNewNode = xmlDoc.CreateElement("KeyInfo", "http://www.w3.org/2000/09/xmldsig#")  ', xsiNS)
                childNode = xmlDoc.CreateElement("KeyInfo", "http://www.w3.org/2000/09/xmldsig#")
                childNode = xmlDoc.LastChild.LastChild.AppendChild(childNode)

                childNode2 = xmlDoc.CreateElement("X509Data", "http://www.w3.org/2000/09/xmldsig#")
                childNode3 = xmlDoc.CreateElement("X509IssuerSerial", "http://www.w3.org/2000/09/xmldsig#")

                childNode4 = xmlDoc.CreateElement("X509IssuerName", "http://www.w3.org/2000/09/xmldsig#")
                childNode4.InnerText = cert.Issuer    '"SERIALNUMBER=516406-0120, CN=Nordea Test Corporate CA 03, O=Nordea Bank AB (publ), C=SE"
                childNode3.AppendChild(childNode4)

                childNode4 = xmlDoc.CreateElement("X509SerialNumber", "http://www.w3.org/2000/09/xmldsig#")
                ' SerialNumber is found in Subject, like    Subject	"SERIALNUMBER=5780860238, CN=Nordea Demo Certificate, C=FI"	String
                ' NO - Leif Hansson Nordea sier nå SerialNumber
                sTmp = cert.SerialNumber
                sTmp = Convert.ToInt32(sTmp, 16).ToString
                'sTmp = cert.Subject
                ' take serialnumber part
                'sTmp = xDelim(sTmp, ",", 1)
                'sTmp = Replace(sTmp, "SERIALNUMBER=", "")
                childNode4.InnerText = sTmp
                childNode3.AppendChild(childNode4)
                childNode2.AppendChild(childNode3)

                childNode3 = xmlDoc.CreateElement("X509Certificate", "http://www.w3.org/2000/09/xmldsig#")
                'childNode3.InnerText = System.Text.Encoding.Unicode.GetString(cert.Export(X509ContentType.Cert))
                childNode3.InnerText = Convert.ToBase64String(cert.Export(X509ContentType.Cert))
                childNode2.AppendChild(childNode3)

                childNode.AppendChild(childNode2)

                ' Save the document.
                sErrorString = "Saves the signed file"
                xmlDoc.Save(sFilenameToCrypt)
                AddSecureEnvelope = True

            Else
                If Not bSilent Then  ' added 14.11.2018
                    MsgBox("Finner ikke noe sertifikat utstedt av Nordea!")
                End If
                If bLog Then  ' 14.11.2018 - added logging
                    oBabelLog.Heading = "BabelBank Warning: " & "Sertifikat"
                    oBabelLog.AddLogEvent("Finner ikke noe sertifikat utstedt av Nordea!", System.Diagnostics.TraceEventType.Warning)
                End If
                AddSecureEnvelope = False
            End If


        Catch e As Exception
            Console.WriteLine(e.Message)
            Err.Raise(Err.Number, "AddSecureEnvelope", sErrorString & vbCrLf & Err.Description)
        End Try

        ' rydd opp i tempfiler;
        ' .xml.tmp file
        'If Dir(tmpFilename) <> "" Then
        'Kill(tmpFilename) ' Delete in case it exists
        'End If
        If File.Exists(tmpFilename) Then
            File.Delete(tmpFilename)
        End If
        ' .wrapped file
        'If Dir(sFilenameToCrypt & ".wrapped") <> "" Then
        'Kill(sFilenameToCrypt & ".wrapped")
        'End If
        If File.Exists(sFilenameToCrypt & ".wrapped") Then
            File.Delete(sFilenameToCrypt & ".wrapped")
        End If

    End Function

    ' Sign an XML file. 
    ' This document cannot be verified unless the verifying 
    ' code has the key with which it was signed.
    ' 01.11.2018 changed from RSA to RSACryptoServiceProvider in line below
    Sub SignXml(ByVal xmlDoc As XmlDocument, ByVal rsaPublicKey As RSACryptoServiceProvider, ByVal rsaPrivateKey As RSACryptoServiceProvider)
        ' Check arguments.
        If xmlDoc Is Nothing Then
            Throw New ArgumentException("xmlDoc")
        End If
        If rsaPublicKey Is Nothing Then
            Throw New ArgumentException("Key")
        End If

        ' Create a SignedXml object.
        Dim signedXml As New SignedXml(xmlDoc)
        ' Add the key to the SignedXml document.
        'signedXml.SigningKey = rsaKey
        Dim KeyInfo = New KeyInfo
        'KeyInfo.addclause(New RSAKeyValue(CType(rsaPrivateKey, RSA)))
        KeyInfo.addclause(New RSAKeyValue(CType(rsaPublicKey, RSACryptoServiceProvider)))

        'signedXml.KeyInfo = KeyInfo
        signedXml.SigningKey = rsaPrivateKey

        ' Create a reference to be signed.
        Dim reference As New Reference()
        reference.Uri = ""
        ' Add an enveloped transformation to the reference.
        GC.Collect()
        Dim env As New XmlDsigEnvelopedSignatureTransform()
        reference.AddTransform(env)
        GC.Collect()
        ' Add the reference to the SignedXml object.
        signedXml.AddReference(reference)
        ' Compute the signature.
        GC.Collect()
        signedXml.ComputeSignature()
        ' Get the XML representation of the signature and save
        ' it to an XmlElement object.
        Dim xmlDigitalSignature As XmlElement = signedXml.GetXml()

        ' Append the element to the XML document.
        xmlDoc.DocumentElement.AppendChild(xmlDoc.ImportNode(xmlDigitalSignature, True))
    End Sub

    Function GZIPCompress(ByVal raw() As Byte) As Byte()
        ' Clean up memory with Using-statements.
        Using memory As System.IO.MemoryStream = New System.IO.MemoryStream()
            ' Create compression stream.
            Using gzip As System.IO.Compression.GZipStream = New System.IO.Compression.GZipStream(memory, System.IO.Compression.CompressionMode.Compress, False)
                ' Write.
                gzip.Write(raw, 0, raw.Length)
            End Using
            ' Return array.
            Return memory.ToArray()
        End Using
    End Function

End Module

