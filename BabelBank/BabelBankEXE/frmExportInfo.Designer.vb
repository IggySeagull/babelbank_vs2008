<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmExportInfo
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdFileOpen As System.Windows.Forms.Button
	Public WithEvents txtFilename As System.Windows.Forms.TextBox
	Public WithEvents lstFormats As System.Windows.Forms.ListBox
	Public WithEvents lblExportformat As System.Windows.Forms.Label
	Public WithEvents lblFilename As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExportInfo))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdFileOpen = New System.Windows.Forms.Button
        Me.txtFilename = New System.Windows.Forms.TextBox
        Me.lstFormats = New System.Windows.Forms.ListBox
        Me.lblExportformat = New System.Windows.Forms.Label
        Me.lblFilename = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(425, 291)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(80, 25)
        Me.cmdCancel.TabIndex = 5
        Me.cmdCancel.Text = "&Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(340, 291)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(80, 25)
        Me.cmdOK.TabIndex = 4
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdFileOpen
        '
        Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
        Me.cmdFileOpen.Location = New System.Drawing.Point(481, 259)
        Me.cmdFileOpen.Name = "cmdFileOpen"
        Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen.TabIndex = 2
        Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen.UseVisualStyleBackColor = False
        '
        'txtFilename
        '
        Me.txtFilename.AcceptsReturn = True
        Me.txtFilename.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilename.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilename.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilename.Location = New System.Drawing.Point(198, 260)
        Me.txtFilename.MaxLength = 0
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilename.Size = New System.Drawing.Size(277, 19)
        Me.txtFilename.TabIndex = 1
        '
        'lstFormats
        '
        Me.lstFormats.BackColor = System.Drawing.SystemColors.Window
        Me.lstFormats.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstFormats.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstFormats.Location = New System.Drawing.Point(198, 78)
        Me.lstFormats.Name = "lstFormats"
        Me.lstFormats.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstFormats.Size = New System.Drawing.Size(277, 173)
        Me.lstFormats.TabIndex = 0
        '
        'lblExportformat
        '
        Me.lblExportformat.BackColor = System.Drawing.SystemColors.Control
        Me.lblExportformat.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExportformat.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExportformat.Location = New System.Drawing.Point(88, 79)
        Me.lblExportformat.Name = "lblExportformat"
        Me.lblExportformat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExportformat.Size = New System.Drawing.Size(105, 17)
        Me.lblExportformat.TabIndex = 6
        Me.lblExportformat.Text = "Eksportformat"
        '
        'lblFilename
        '
        Me.lblFilename.BackColor = System.Drawing.SystemColors.Control
        Me.lblFilename.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilename.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilename.Location = New System.Drawing.Point(90, 263)
        Me.lblFilename.Name = "lblFilename"
        Me.lblFilename.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilename.Size = New System.Drawing.Size(105, 17)
        Me.lblFilename.TabIndex = 3
        Me.lblFilename.Text = "Navn p� eksportfil"
        '
        'frmExportInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(511, 326)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdFileOpen)
        Me.Controls.Add(Me.txtFilename)
        Me.Controls.Add(Me.lstFormats)
        Me.Controls.Add(Me.lblExportformat)
        Me.Controls.Add(Me.lblFilename)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmExportInfo"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Eksport informasjon"
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class
