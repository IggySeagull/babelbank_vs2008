<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmClientImport
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtAllowedDifference As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents txtPattern As System.Windows.Forms.TextBox
	Public WithEvents txtObservationAccount As System.Windows.Forms.TextBox
	Public WithEvents txtFormatOut As System.Windows.Forms.TextBox
	Public WithEvents txtFormatIn As System.Windows.Forms.TextBox
	Public WithEvents lblAllowedDifference As System.Windows.Forms.Label
    Public WithEvents lblPattern As System.Windows.Forms.Label
	Public WithEvents lblObservationAccount As System.Windows.Forms.Label
	Public WithEvents lblFormatOut As System.Windows.Forms.Label
	Public WithEvents lblFormatIn As System.Windows.Forms.Label
	Public WithEvents lblDescription As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtAllowedDifference = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.txtPattern = New System.Windows.Forms.TextBox
        Me.txtObservationAccount = New System.Windows.Forms.TextBox
        Me.txtFormatOut = New System.Windows.Forms.TextBox
        Me.txtFormatIn = New System.Windows.Forms.TextBox
        Me.lblAllowedDifference = New System.Windows.Forms.Label
        Me.lblPattern = New System.Windows.Forms.Label
        Me.lblObservationAccount = New System.Windows.Forms.Label
        Me.lblFormatOut = New System.Windows.Forms.Label
        Me.lblFormatIn = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblReminderNo = New System.Windows.Forms.Label
        Me.txtReminderNo = New System.Windows.Forms.TextBox
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtAllowedDifference
        '
        Me.txtAllowedDifference.AcceptsReturn = True
        Me.txtAllowedDifference.BackColor = System.Drawing.SystemColors.Window
        Me.txtAllowedDifference.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAllowedDifference.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAllowedDifference.Location = New System.Drawing.Point(312, 267)
        Me.txtAllowedDifference.MaxLength = 0
        Me.txtAllowedDifference.Name = "txtAllowedDifference"
        Me.txtAllowedDifference.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAllowedDifference.Size = New System.Drawing.Size(73, 20)
        Me.txtAllowedDifference.TabIndex = 12
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(584, 331)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 10
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(504, 331)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 9
        Me.cmdCancel.Text = "55006 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'txtPattern
        '
        Me.txtPattern.AcceptsReturn = True
        Me.txtPattern.BackColor = System.Drawing.SystemColors.Window
        Me.txtPattern.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPattern.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPattern.Location = New System.Drawing.Point(312, 240)
        Me.txtPattern.MaxLength = 0
        Me.txtPattern.Name = "txtPattern"
        Me.txtPattern.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPattern.Size = New System.Drawing.Size(177, 20)
        Me.txtPattern.TabIndex = 8
        '
        'txtObservationAccount
        '
        Me.txtObservationAccount.AcceptsReturn = True
        Me.txtObservationAccount.BackColor = System.Drawing.SystemColors.Window
        Me.txtObservationAccount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtObservationAccount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtObservationAccount.Location = New System.Drawing.Point(312, 214)
        Me.txtObservationAccount.MaxLength = 0
        Me.txtObservationAccount.Name = "txtObservationAccount"
        Me.txtObservationAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtObservationAccount.Size = New System.Drawing.Size(121, 20)
        Me.txtObservationAccount.TabIndex = 7
        '
        'txtFormatOut
        '
        Me.txtFormatOut.AcceptsReturn = True
        Me.txtFormatOut.BackColor = System.Drawing.SystemColors.Window
        Me.txtFormatOut.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFormatOut.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFormatOut.Location = New System.Drawing.Point(312, 187)
        Me.txtFormatOut.MaxLength = 0
        Me.txtFormatOut.Name = "txtFormatOut"
        Me.txtFormatOut.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFormatOut.Size = New System.Drawing.Size(33, 20)
        Me.txtFormatOut.TabIndex = 6
        Me.txtFormatOut.Text = "1"
        Me.txtFormatOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFormatIn
        '
        Me.txtFormatIn.AcceptsReturn = True
        Me.txtFormatIn.BackColor = System.Drawing.SystemColors.Window
        Me.txtFormatIn.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFormatIn.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFormatIn.Location = New System.Drawing.Point(312, 160)
        Me.txtFormatIn.MaxLength = 0
        Me.txtFormatIn.Name = "txtFormatIn"
        Me.txtFormatIn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFormatIn.Size = New System.Drawing.Size(33, 20)
        Me.txtFormatIn.TabIndex = 5
        Me.txtFormatIn.Text = "2"
        Me.txtFormatIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAllowedDifference
        '
        Me.lblAllowedDifference.BackColor = System.Drawing.SystemColors.Control
        Me.lblAllowedDifference.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAllowedDifference.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAllowedDifference.Location = New System.Drawing.Point(112, 267)
        Me.lblAllowedDifference.Name = "lblAllowedDifference"
        Me.lblAllowedDifference.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAllowedDifference.Size = New System.Drawing.Size(193, 17)
        Me.lblAllowedDifference.TabIndex = 11
        Me.lblAllowedDifference.Text = "60266 - Allowed difference"
        '
        'lblPattern
        '
        Me.lblPattern.BackColor = System.Drawing.SystemColors.Control
        Me.lblPattern.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPattern.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPattern.Location = New System.Drawing.Point(112, 240)
        Me.lblPattern.Name = "lblPattern"
        Me.lblPattern.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPattern.Size = New System.Drawing.Size(193, 17)
        Me.lblPattern.TabIndex = 4
        Me.lblPattern.Text = "60250 - Description of invoicenumber:"
        '
        'lblObservationAccount
        '
        Me.lblObservationAccount.BackColor = System.Drawing.SystemColors.Control
        Me.lblObservationAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblObservationAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblObservationAccount.Location = New System.Drawing.Point(112, 214)
        Me.lblObservationAccount.Name = "lblObservationAccount"
        Me.lblObservationAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblObservationAccount.Size = New System.Drawing.Size(193, 17)
        Me.lblObservationAccount.TabIndex = 3
        Me.lblObservationAccount.Text = "60251 - Observationaccount (AR):"
        '
        'lblFormatOut
        '
        Me.lblFormatOut.BackColor = System.Drawing.SystemColors.Control
        Me.lblFormatOut.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFormatOut.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFormatOut.Location = New System.Drawing.Point(112, 187)
        Me.lblFormatOut.Name = "lblFormatOut"
        Me.lblFormatOut.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFormatOut.Size = New System.Drawing.Size(185, 17)
        Me.lblFormatOut.TabIndex = 2
        Me.lblFormatOut.Text = "60252 - Format out (normally 1):"
        '
        'lblFormatIn
        '
        Me.lblFormatIn.BackColor = System.Drawing.SystemColors.Control
        Me.lblFormatIn.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFormatIn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFormatIn.Location = New System.Drawing.Point(112, 160)
        Me.lblFormatIn.Name = "lblFormatIn"
        Me.lblFormatIn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFormatIn.Size = New System.Drawing.Size(169, 17)
        Me.lblFormatIn.TabIndex = 1
        Me.lblFormatIn.Text = "60253 - Format in (normally 2)"
        '
        'lblDescription
        '
        Me.lblDescription.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescription.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescription.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescription.Location = New System.Drawing.Point(112, 72)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescription.Size = New System.Drawing.Size(489, 65)
        Me.lblDescription.TabIndex = 0
        Me.lblDescription.Text = "60254 - Description of setup"
        '
        'lblReminderNo
        '
        Me.lblReminderNo.BackColor = System.Drawing.SystemColors.Control
        Me.lblReminderNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReminderNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblReminderNo.Location = New System.Drawing.Point(112, 294)
        Me.lblReminderNo.Name = "lblReminderNo"
        Me.lblReminderNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReminderNo.Size = New System.Drawing.Size(193, 17)
        Me.lblReminderNo.TabIndex = 14
        Me.lblReminderNo.Text = "60332 - Reminderno.:"
        '
        'txtReminderNo
        '
        Me.txtReminderNo.AcceptsReturn = True
        Me.txtReminderNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtReminderNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtReminderNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtReminderNo.Location = New System.Drawing.Point(312, 294)
        Me.txtReminderNo.MaxLength = 0
        Me.txtReminderNo.Name = "txtReminderNo"
        Me.txtReminderNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtReminderNo.Size = New System.Drawing.Size(49, 20)
        Me.txtReminderNo.TabIndex = 15
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 322)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(645, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmClientImport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(668, 360)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtReminderNo)
        Me.Controls.Add(Me.lblReminderNo)
        Me.Controls.Add(Me.txtAllowedDifference)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.txtPattern)
        Me.Controls.Add(Me.txtObservationAccount)
        Me.Controls.Add(Me.txtFormatOut)
        Me.Controls.Add(Me.txtFormatIn)
        Me.Controls.Add(Me.lblAllowedDifference)
        Me.Controls.Add(Me.lblPattern)
        Me.Controls.Add(Me.lblObservationAccount)
        Me.Controls.Add(Me.lblFormatOut)
        Me.Controls.Add(Me.lblFormatIn)
        Me.Controls.Add(Me.lblDescription)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmClientImport"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "60255 - Import clients"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents lblReminderNo As System.Windows.Forms.Label
    Public WithEvents txtReminderNo As System.Windows.Forms.TextBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
