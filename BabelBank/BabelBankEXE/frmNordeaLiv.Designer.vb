<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmNordeaLiv
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtResult As System.Windows.Forms.TextBox
	Public WithEvents lstResult As System.Windows.Forms.CheckedListBox
	Public WithEvents chkRemoveFiles As System.Windows.Forms.Button
	Public WithEvents chkOutPayments As System.Windows.Forms.Button
	Public WithEvents cmdRecalculate As System.Windows.Forms.Button
	Public WithEvents cmdClose As System.Windows.Forms.Button
	Public WithEvents txtEndDate As System.Windows.Forms.TextBox
	Public WithEvents txtStartDate As System.Windows.Forms.TextBox
	Public WithEvents cmdBalances As System.Windows.Forms.Button
	Public WithEvents lblExplaination As System.Windows.Forms.Label
    Public WithEvents lblResultUBEndDate As System.Windows.Forms.Label
	Public WithEvents lblUBEndDate As System.Windows.Forms.Label
	Public WithEvents lblResultMovement As System.Windows.Forms.Label
	Public WithEvents lblMovement As System.Windows.Forms.Label
	Public WithEvents lblResultIBStartDate As System.Windows.Forms.Label
	Public WithEvents lblIBStartDate As System.Windows.Forms.Label
	Public WithEvents lblEndDate As System.Windows.Forms.Label
	Public WithEvents lblStartDate As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtResult = New System.Windows.Forms.TextBox
        Me.lstResult = New System.Windows.Forms.CheckedListBox
        Me.chkRemoveFiles = New System.Windows.Forms.Button
        Me.chkOutPayments = New System.Windows.Forms.Button
        Me.cmdRecalculate = New System.Windows.Forms.Button
        Me.cmdClose = New System.Windows.Forms.Button
        Me.txtEndDate = New System.Windows.Forms.TextBox
        Me.txtStartDate = New System.Windows.Forms.TextBox
        Me.cmdBalances = New System.Windows.Forms.Button
        Me.lblExplaination = New System.Windows.Forms.Label
        Me.lblResultUBEndDate = New System.Windows.Forms.Label
        Me.lblUBEndDate = New System.Windows.Forms.Label
        Me.lblResultMovement = New System.Windows.Forms.Label
        Me.lblMovement = New System.Windows.Forms.Label
        Me.lblResultIBStartDate = New System.Windows.Forms.Label
        Me.lblIBStartDate = New System.Windows.Forms.Label
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtResult
        '
        Me.txtResult.AcceptsReturn = True
        Me.txtResult.BackColor = System.Drawing.SystemColors.Window
        Me.txtResult.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtResult.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtResult.Location = New System.Drawing.Point(16, 288)
        Me.txtResult.MaxLength = 0
        Me.txtResult.Multiline = True
        Me.txtResult.Name = "txtResult"
        Me.txtResult.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtResult.Size = New System.Drawing.Size(201, 81)
        Me.txtResult.TabIndex = 17
        Me.txtResult.Text = "txtResult" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'lstResult
        '
        Me.lstResult.BackColor = System.Drawing.SystemColors.Window
        Me.lstResult.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstResult.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstResult.Location = New System.Drawing.Point(8, 192)
        Me.lstResult.Name = "lstResult"
        Me.lstResult.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstResult.Size = New System.Drawing.Size(209, 79)
        Me.lstResult.TabIndex = 15
        '
        'chkRemoveFiles
        '
        Me.chkRemoveFiles.BackColor = System.Drawing.SystemColors.Control
        Me.chkRemoveFiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkRemoveFiles.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRemoveFiles.Location = New System.Drawing.Point(200, 400)
        Me.chkRemoveFiles.Name = "chkRemoveFiles"
        Me.chkRemoveFiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkRemoveFiles.Size = New System.Drawing.Size(81, 25)
        Me.chkRemoveFiles.TabIndex = 4
        Me.chkRemoveFiles.Text = "&Vis filer"
        Me.chkRemoveFiles.UseVisualStyleBackColor = False
        '
        'chkOutPayments
        '
        Me.chkOutPayments.BackColor = System.Drawing.SystemColors.Control
        Me.chkOutPayments.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkOutPayments.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkOutPayments.Location = New System.Drawing.Point(112, 400)
        Me.chkOutPayments.Name = "chkOutPayments"
        Me.chkOutPayments.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkOutPayments.Size = New System.Drawing.Size(81, 25)
        Me.chkOutPayments.TabIndex = 3
        Me.chkOutPayments.Text = "S&jekk utbet"
        Me.chkOutPayments.UseVisualStyleBackColor = False
        '
        'cmdRecalculate
        '
        Me.cmdRecalculate.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRecalculate.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRecalculate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRecalculate.Location = New System.Drawing.Point(8, 400)
        Me.cmdRecalculate.Name = "cmdRecalculate"
        Me.cmdRecalculate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRecalculate.Size = New System.Drawing.Size(97, 25)
        Me.cmdRecalculate.TabIndex = 2
        Me.cmdRecalculate.Text = "&Rekalkuler saldo"
        Me.cmdRecalculate.UseVisualStyleBackColor = False
        '
        'cmdClose
        '
        Me.cmdClose.BackColor = System.Drawing.SystemColors.Control
        Me.cmdClose.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClose.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClose.Location = New System.Drawing.Point(392, 440)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClose.Size = New System.Drawing.Size(81, 25)
        Me.cmdClose.TabIndex = 6
        Me.cmdClose.Text = "&Lukk"
        Me.cmdClose.UseVisualStyleBackColor = False
        '
        'txtEndDate
        '
        Me.txtEndDate.AcceptsReturn = True
        Me.txtEndDate.BackColor = System.Drawing.SystemColors.Window
        Me.txtEndDate.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtEndDate.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtEndDate.Location = New System.Drawing.Point(400, 152)
        Me.txtEndDate.MaxLength = 0
        Me.txtEndDate.Name = "txtEndDate"
        Me.txtEndDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtEndDate.Size = New System.Drawing.Size(75, 19)
        Me.txtEndDate.TabIndex = 1
        '
        'txtStartDate
        '
        Me.txtStartDate.AcceptsReturn = True
        Me.txtStartDate.BackColor = System.Drawing.SystemColors.Window
        Me.txtStartDate.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtStartDate.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtStartDate.Location = New System.Drawing.Point(400, 128)
        Me.txtStartDate.MaxLength = 0
        Me.txtStartDate.Name = "txtStartDate"
        Me.txtStartDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtStartDate.Size = New System.Drawing.Size(75, 19)
        Me.txtStartDate.TabIndex = 0
        '
        'cmdBalances
        '
        Me.cmdBalances.BackColor = System.Drawing.SystemColors.Control
        Me.cmdBalances.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdBalances.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdBalances.Location = New System.Drawing.Point(392, 400)
        Me.cmdBalances.Name = "cmdBalances"
        Me.cmdBalances.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdBalances.Size = New System.Drawing.Size(81, 25)
        Me.cmdBalances.TabIndex = 5
        Me.cmdBalances.Text = "&Saldo"
        Me.cmdBalances.UseVisualStyleBackColor = False
        '
        'lblExplaination
        '
        Me.lblExplaination.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplaination.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplaination.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplaination.Location = New System.Drawing.Point(184, 40)
        Me.lblExplaination.Name = "lblExplaination"
        Me.lblExplaination.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplaination.Size = New System.Drawing.Size(289, 81)
        Me.lblExplaination.TabIndex = 16
        Me.lblExplaination.Text = "Explain"
        '
        'lblResultUBEndDate
        '
        Me.lblResultUBEndDate.BackColor = System.Drawing.SystemColors.Control
        Me.lblResultUBEndDate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblResultUBEndDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblResultUBEndDate.Location = New System.Drawing.Point(360, 248)
        Me.lblResultUBEndDate.Name = "lblResultUBEndDate"
        Me.lblResultUBEndDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblResultUBEndDate.Size = New System.Drawing.Size(118, 17)
        Me.lblResultUBEndDate.TabIndex = 14
        Me.lblResultUBEndDate.Text = "3"
        Me.lblResultUBEndDate.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblUBEndDate
        '
        Me.lblUBEndDate.BackColor = System.Drawing.SystemColors.Control
        Me.lblUBEndDate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblUBEndDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblUBEndDate.Location = New System.Drawing.Point(280, 248)
        Me.lblUBEndDate.Name = "lblUBEndDate"
        Me.lblUBEndDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblUBEndDate.Size = New System.Drawing.Size(78, 17)
        Me.lblUBEndDate.TabIndex = 13
        Me.lblUBEndDate.Text = "UB EndDate"
        '
        'lblResultMovement
        '
        Me.lblResultMovement.BackColor = System.Drawing.SystemColors.Control
        Me.lblResultMovement.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblResultMovement.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblResultMovement.Location = New System.Drawing.Point(360, 224)
        Me.lblResultMovement.Name = "lblResultMovement"
        Me.lblResultMovement.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblResultMovement.Size = New System.Drawing.Size(118, 17)
        Me.lblResultMovement.TabIndex = 12
        Me.lblResultMovement.Text = "2"
        Me.lblResultMovement.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblMovement
        '
        Me.lblMovement.BackColor = System.Drawing.SystemColors.Control
        Me.lblMovement.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMovement.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMovement.Location = New System.Drawing.Point(280, 224)
        Me.lblMovement.Name = "lblMovement"
        Me.lblMovement.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMovement.Size = New System.Drawing.Size(70, 17)
        Me.lblMovement.TabIndex = 11
        Me.lblMovement.Text = "Bevegelse:"
        '
        'lblResultIBStartDate
        '
        Me.lblResultIBStartDate.BackColor = System.Drawing.SystemColors.Control
        Me.lblResultIBStartDate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblResultIBStartDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblResultIBStartDate.Location = New System.Drawing.Point(360, 200)
        Me.lblResultIBStartDate.Name = "lblResultIBStartDate"
        Me.lblResultIBStartDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblResultIBStartDate.Size = New System.Drawing.Size(118, 17)
        Me.lblResultIBStartDate.TabIndex = 10
        Me.lblResultIBStartDate.Text = "1"
        Me.lblResultIBStartDate.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblIBStartDate
        '
        Me.lblIBStartDate.BackColor = System.Drawing.SystemColors.Control
        Me.lblIBStartDate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblIBStartDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblIBStartDate.Location = New System.Drawing.Point(280, 200)
        Me.lblIBStartDate.Name = "lblIBStartDate"
        Me.lblIBStartDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblIBStartDate.Size = New System.Drawing.Size(78, 17)
        Me.lblIBStartDate.TabIndex = 9
        Me.lblIBStartDate.Text = "IB StartDate"
        '
        'lblEndDate
        '
        Me.lblEndDate.BackColor = System.Drawing.SystemColors.Control
        Me.lblEndDate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblEndDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblEndDate.Location = New System.Drawing.Point(280, 152)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblEndDate.Size = New System.Drawing.Size(118, 17)
        Me.lblEndDate.TabIndex = 8
        Me.lblEndDate.Text = "Slutt dato (dd.mm.yyyy):"
        '
        'lblStartDate
        '
        Me.lblStartDate.BackColor = System.Drawing.SystemColors.Control
        Me.lblStartDate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblStartDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblStartDate.Location = New System.Drawing.Point(280, 128)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblStartDate.Size = New System.Drawing.Size(118, 17)
        Me.lblStartDate.TabIndex = 7
        Me.lblStartDate.Text = "Start dato (dd.mm.yyyy):"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(283, 183)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(190, 1)
        Me.lblLine1.TabIndex = 82
        Me.lblLine1.Text = "Label1"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(10, 433)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(465, 1)
        Me.Label1.TabIndex = 83
        Me.Label1.Text = "Label1"
        '
        'frmNordeaLiv
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(486, 475)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtResult)
        Me.Controls.Add(Me.lstResult)
        Me.Controls.Add(Me.chkRemoveFiles)
        Me.Controls.Add(Me.chkOutPayments)
        Me.Controls.Add(Me.cmdRecalculate)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.txtEndDate)
        Me.Controls.Add(Me.txtStartDate)
        Me.Controls.Add(Me.cmdBalances)
        Me.Controls.Add(Me.lblExplaination)
        Me.Controls.Add(Me.lblResultUBEndDate)
        Me.Controls.Add(Me.lblUBEndDate)
        Me.Controls.Add(Me.lblResultMovement)
        Me.Controls.Add(Me.lblMovement)
        Me.Controls.Add(Me.lblResultIBStartDate)
        Me.Controls.Add(Me.lblIBStartDate)
        Me.Controls.Add(Me.lblEndDate)
        Me.Controls.Add(Me.lblStartDate)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmNordeaLiv"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Saldo/Bevegelse"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
#End Region 
End Class
