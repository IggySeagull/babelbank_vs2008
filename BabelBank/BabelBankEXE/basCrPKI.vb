Option Strict Off
Option Explicit On
Module basModExp
	' $Id: basCrPKI.bas $
	
	' This module contains the full list of declaration statements
	' for the CryptoSys (tm) PKI Toolkit library.
	' VB6/VBA version.
	' Last updated:
	'   $Date: 2008-05-22 21:12:00 $
	'   $Revision: 3.2.4 $
	
	'************************* COPYRIGHT NOTICE*************************
	' Copyright (c) 2002-8 DI Management Services Pty Limited.
	' All rights reserved.
	' This code may only be used by licensed users.
	' The latest version of the CryptoSys PKI Toolkit and a licence
	' may be obtained from <www.cryptosys.net>.
	' Refer to licence for conditions of use.
	' This copyright notice must always be left intact.
	'****************** END OF COPYRIGHT NOTICE*************************
	
	
	' CONSTANTS
	Public Const ENCRYPT As Integer = -1
	Public Const DECRYPT As Integer = 0
	Public Const PKI_DIR_ENCRYPT As Integer = -1
	Public Const PKI_DIR_DECRYPT As Integer = 0
	' Maximum number of bytes in hash digest byte array (updated v3.2)
	Public Const PKI_MAX_HASH_BYTES As Integer = 64
	Public Const PKI_SHA1_BYTES As Integer = 20
	Public Const PKI_SHA224_BYTES As Integer = 28
	Public Const PKI_SHA256_BYTES As Integer = 32
	Public Const PKI_SHA384_BYTES As Integer = 48
	Public Const PKI_SHA512_BYTES As Integer = 64
	Public Const PKI_MD5_BYTES As Integer = 16
	Public Const PKI_MD2_BYTES As Integer = 16
	' Maximum number of hex characters in hash digest
	Public Const PKI_MAX_HASH_CHARS As Integer = 2 * PKI_MAX_HASH_BYTES
	Public Const PKI_SHA1_CHARS As Integer = 2 * PKI_SHA1_BYTES
	Public Const PKI_SHA224_CHARS As Integer = 2 * PKI_SHA224_BYTES
	Public Const PKI_SHA256_CHARS As Integer = 2 * PKI_SHA256_BYTES
	Public Const PKI_SHA384_CHARS As Integer = 2 * PKI_SHA384_BYTES
	Public Const PKI_SHA512_CHARS As Integer = 2 * PKI_SHA512_BYTES
	Public Const PKI_MD5_CHARS As Integer = 2 * PKI_MD5_BYTES
	Public Const PKI_MD2_CHARS As Integer = 2 * PKI_MD2_BYTES
	' Synonym retained for backwards compatibility
	Public Const PKI_MAX_HASH_LEN As Integer = PKI_MAX_HASH_CHARS
	' Encryption block sizes in bytes
	Public Const PKI_BLK_TDEA_BYTES As Integer = 8
	Public Const PKI_BLK_AES_BYTES As Integer = 16
	' Key size in bytes
	Public Const PKI_KEYSIZE_TDEA_BYTES As Integer = 24
	Public Const PKI_KEYSIZE_MAX_BYTES As Integer = 32
	' Required size for RNG seed file
	Public Const PKI_RNG_SEED_BYTES As Integer = 64
	
	' CONSTANTS USED IN OPTION FLAGS
	Public Const PKI_DEFAULT As Integer = 0
	' Signature algorithms
	Public Const PKI_SIG_SHA1RSA As Integer = 0 ' default
	Public Const PKI_SIG_MD5RSA As Integer = 1
	Public Const PKI_SIG_MD2RSA As Integer = 2
	Public Const PKI_SIG_SHA256RSA As Integer = 3 ' Added v3.2
	Public Const PKI_SIG_SHA384RSA As Integer = 4 ' Added v3.2
	Public Const PKI_SIG_SHA512RSA As Integer = 5 ' Added v3.2
	Public Const PKI_SIG_SHA224RSA As Integer = 6 ' Added v3.2
	' PKCS#5 Password-based encryption algorithms
	Public Const PKI_PBE_SHA1_3DES As Integer = 0 ' default
	Public Const PKI_PBE_MD5_DES As Integer = 1
	Public Const PKI_PBE_MD2_DES As Integer = 2
	Public Const PKI_PBE_SHA_DES As Integer = 3
	Public Const PKI_PBES2_3DES As Integer = 4 ' (Deprecated as of v3.2)
	Public Const PKI_PBE_PBES2 As Integer = &H1000 ' Added v3.2
	' Message digest hash algorithms
	Public Const PKI_HASH_SHA1 As Integer = 0 ' default
	Public Const PKI_HASH_MD5 As Integer = 1
	Public Const PKI_HASH_MD2 As Integer = 2
	Public Const PKI_HASH_SHA256 As Integer = 3
	Public Const PKI_HASH_SHA384 As Integer = 4
	Public Const PKI_HASH_SHA512 As Integer = 5
	Public Const PKI_HASH_SHA224 As Integer = 6 ' Added v3.2
	Public Const PKI_HASH_MODE_TEXT As Integer = &H10000
	' RSA key generation
	Public Const PKI_KEYGEN_INDICATE As Integer = &H10
	Public Const PKI_KEY_NODELAY As Integer = &H20
	Public Const PKI_KEY_FORMAT_PEM As Integer = &H10000
	Public Const PKI_KEY_FORMAT_SSL As Integer = &H20000
	
	' Return values for RSA_CheckKey
	Public Const PKI_VALID_PUBLICKEY As Integer = 1
	Public Const PKI_VALID_PRIVATEKEY As Integer = 0
	
	Public Const PKI_PFX_NO_PRIVKEY As Integer = &H10
	
	Public Const PKI_XML_RSAKEYVALUE As Integer = &H1
	Public Const PKI_XML_EXCLPRIVATE As Integer = &H10
	Public Const PKI_XML_HEXBINARY As Integer = &H100
	
	Public Const PKI_EME_DEFAULT As Integer = &H0
	Public Const PKI_EME_PKCSV1_5 As Integer = &H0 ' alternate for default
	Public Const PKI_EME_OAEP As Integer = &H10
	Public Const PKI_EMSIG_DEFAULT As Integer = &H20
	Public Const PKI_EMSIG_PKCSV1_5 As Integer = &H20 ' alternate for default
	Public Const PKI_EMSIG_DIGESTONLY As Integer = &H1000
	Public Const PKI_EMSIG_DIGINFO As Integer = &H2000
	Public Const PKI_EMSIG_ISO9796 As Integer = &H100000
	
	Public Const PKI_X509_FORMAT_PEM As Integer = &H10000
	Public Const PKI_X509_FORMAT_BIN As Integer = &H20000
	Public Const PKI_X509_REQ_KLUDGE As Integer = &H100000
	Public Const PKI_X509_LATIN1 As Integer = &H400000
	Public Const PKI_X509_UTF8 As Integer = &H800000
	Public Const PKI_X509_NO_BASIC As Integer = &H2000000
	Public Const PKI_X509_CA_TRUE As Integer = &H4000000
	Public Const PKI_X509_VERSION1 As Integer = &H8000000
	
	Public Const PKI_CMS_FORMAT_BASE64 As Integer = &H10000
	Public Const PKI_CMS_EXCLUDE_CERTS As Integer = &H100
	Public Const PKI_CMS_EXCLUDE_DATA As Integer = &H200
	Public Const PKI_CMS_INCLUDE_ATTRS As Integer = &H800
	Public Const PKI_CMS_ADD_SIGNTIME As Integer = &H1000
	Public Const PKI_CMS_ADD_SMIMECAP As Integer = &H2000
	Public Const PKI_CMS_CERTS_ONLY As Integer = &H400
	Public Const PKI_CMS_NO_OUTER As Integer = &H2000000
	Public Const PKI_CMS_ALT_ALGID As Integer = &H4000000
	
	' CONSTANTS USED IN RSA EXPONENT PARAMETER
	Public Const PKI_RSAEXP_EQ_3 As Integer = 0
	Public Const PKI_RSAEXP_EQ_5 As Integer = 1
	Public Const PKI_RSAEXP_EQ_17 As Integer = 2
	Public Const PKI_RSAEXP_EQ_257 As Integer = 3
	Public Const PKI_RSAEXP_EQ_65537 As Integer = 4
	
	' CONSTANTS USED IN KEY USAGE FLAG
	Public Const PKI_X509_KEYUSAGE_DIGITALSIGNATURE As Integer = &H1
	Public Const PKI_X509_KEYUSAGE_NONREPUDIATION As Integer = &H2
	Public Const PKI_X509_KEYUSAGE_KEYENCIPHERMENT As Integer = &H4
	Public Const PKI_X509_KEYUSAGE_DATAENCIPHERMENT As Integer = &H8
	Public Const PKI_X509_KEYUSAGE_KEYAGREEMENT As Integer = &H10
	Public Const PKI_X509_KEYUSAGE_KEYCERTSIGN As Integer = &H20
	Public Const PKI_X509_KEYUSAGE_CRLSIGN As Integer = &H40
	Public Const PKI_X509_KEYUSAGE_ENCIPHERONLY As Integer = &H80
	Public Const PKI_X509_KEYUSAGE_DECIPHERONLY As Integer = &H100
	
	' SPECIFIC X509 RETURN VALUES
	Public Const PKI_X509_EXPIRED As Integer = -1
	Public Const PKI_X509_VALID_NOW As Integer = 0
	Public Const PKI_X509_VERIFY_SUCCESS As Integer = 0
	Public Const PKI_X509_VERIFY_FAILURE As Integer = -1
	
	' RETURN VALUES FOR CNV_CheckUTF (new in v2.9)
	Public Const PKI_CHRS_NOT_UTF8 As Integer = 0
	Public Const PKI_CHRS_ALL_ASCII As Integer = 1
	Public Const PKI_CHRS_ANSI8 As Integer = 2
	Public Const PKI_CHRS_MULTIBYTE As Integer = 3
	
	' FLAGS AND RETURN VALUES FOR X.509 AND CMS QUERY FUNCTIONS (new in v3.0)
	Public Const PKI_QUERY_GETTYPE As Integer = &H100000
	Public Const PKI_QUERY_NUMBER As Integer = &H1
	Public Const PKI_QUERY_STRING As Integer = &H2
	
	' OPTIONS FOR RNG FUNCTIONS (new in v3.0)
	Public Const PKI_RNG_STRENGTH_112 As Integer = &H0 ' default
	Public Const PKI_RNG_STRENGTH_128 As Integer = &H1
	
	' New options in v3.2...
	' Block cipher algorithm options
	Public Const PKI_BC_TDEA As Integer = &H10 ' (
	Public Const PKI_BC_DESEDE3 As Integer = &H10 ' ( equiv. synonyms for Triple DES
	Public Const PKI_BC_3DES As Integer = &H10 ' (
	Public Const PKI_BC_AES128 As Integer = &H20
	Public Const PKI_BC_AES192 As Integer = &H30
	Public Const PKI_BC_AES256 As Integer = &H40
	' Block cipher mode options
	Public Const PKI_MODE_ECB As Integer = &H0
	Public Const PKI_MODE_CBC As Integer = &H100
	Public Const PKI_MODE_OFB As Integer = &H200
	Public Const PKI_MODE_CFB As Integer = &H300
	Public Const PKI_MODE_CTR As Integer = &H400
	' Key transport algorithms
	Public Const PKI_KT_RSAES_PKCS As Integer = &H0 ' Default
	Public Const PKI_KT_RSA_KEM As Integer = &H2000
	' Key derivation functions
	Public Const PKI_KDF_KDF2 As Integer = &H0 ' Default
	
	' FUNCTION DECLARATIONS
	
	' CRYPTOGRAPHIC MESSAGE SYNTAX (CMS) FUNCTIONS
	Public Declare Function CMS_MakeEnvData Lib "diCrPKI.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strCertList As String, ByVal strSeed As String, ByVal nSeedLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_MakeEnvDataFromString Lib "diCrPKI.dll" (ByVal strFileOut As String, ByVal strDataIn As String, ByVal strCertList As String, ByVal strSeed As String, ByVal nSeedLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_ReadEnvData Lib "diCrPKI.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strCertFile As String, ByVal strPrivateKey As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_ReadEnvDataToString Lib "diCrPKI.dll" (ByVal strDataOut As String, ByVal nDataLen As Integer, ByVal strFileIn As String, ByVal strCertFile As String, ByVal strPrivateKey As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_MakeSigData Lib "diCrPKI.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal strCertList As String, ByVal strPrivateKey As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_MakeSigDataFromString Lib "diCrPKI.dll" (ByVal strFileOut As String, ByVal strDataIn As String, ByVal strCertList As String, ByVal strPrivateKey As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_MakeSigDataFromSigValue Lib "diCrPKI.dll" (ByVal strFileOut As String, ByRef abSigValue As Byte, ByVal nSigLen As Integer, ByRef abData As Byte, ByVal nDataLen As Integer, ByVal strCertList As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_MakeDetachedSig Lib "diCrPKI.dll" (ByVal strFileOut As String, ByVal strHexDigest As String, ByVal strCertList As String, ByVal strPrivateKey As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_ReadSigData Lib "diCrPKI.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_ReadSigDataToString Lib "diCrPKI.dll" (ByVal strDataOut As String, ByVal nDataLen As Integer, ByVal strFileIn As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_GetSigDataDigest Lib "diCrPKI.dll" (ByVal strHexDigest As String, ByVal nHexDigestLen As Integer, ByVal strFileIn As String, ByVal strCertFile As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_VerifySigData Lib "diCrPKI.dll" (ByVal strFileIn As String, ByVal strCertFile As String, ByVal strHexDigest As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_QuerySigData Lib "diCrPKI.dll" (ByVal strDataOut As String, ByVal nDataLen As Integer, ByVal strFileIn As String, ByVal strQuery As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CMS_QueryEnvData Lib "diCrPKI.dll" (ByVal strDataOut As String, ByVal nDataOutLen As Integer, ByVal strFileIn As String, ByVal strQuery As String, ByVal nOptions As Integer) As Integer
	
	' RSA PUBLIC KEY FUNCTIONS
	Public Declare Function RSA_MakeKeys Lib "diCrPKI.dll" (ByVal strPubKeyFile As String, ByVal strPvkKeyFile As String, ByVal nBits As Integer, ByVal nExpFermat As Integer, ByVal nTests As Integer, ByVal nCount As Integer, ByVal strPassword As String, ByVal strSeed As String, ByVal nSeedLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_ReadEncPrivateKey Lib "diCrPKI.dll" (ByVal strPrivateKey As String, ByVal nKeyMaxLen As Integer, ByVal strEpkFileName As String, ByVal strPassword As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_ReadPrivateKeyInfo Lib "diCrPKI.dll" (ByVal strPrivateKey As String, ByVal nKeyMaxLen As Integer, ByVal strPRIFileName As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_ReadPublicKey Lib "diCrPKI.dll" (ByVal strPublicKey As String, ByVal nKeyMaxLen As Integer, ByVal strKeyFileName As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_SavePublicKey Lib "diCrPKI.dll" (ByVal strOutputFile As String, ByVal strPublicKey As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_GetPublicKeyFromCert Lib "diCrPKI.dll" (ByVal strPublicKey As String, ByVal nKeyMaxLen As Integer, ByVal strCertFileName As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_SaveEncPrivateKey Lib "diCrPKI.dll" (ByVal strOutputFile As String, ByVal strPrivateKey As String, ByVal nCount As Integer, ByVal strPassword As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_SavePrivateKeyInfo Lib "diCrPKI.dll" (ByVal strOutputFile As String, ByVal strPrivateKey As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_GetPrivateKeyFromPFX Lib "diCrPKI.dll" (ByVal strOutputFile As String, ByVal strPfxFile As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_ToXMLString Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal nOutputLen As Integer, ByVal strKeyString As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_FromXMLString Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal nOutputLen As Integer, ByVal strXmlString As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_KeyBits Lib "diCrPKI.dll" (ByVal strKey As String) As Integer
	Public Declare Function RSA_KeyBytes Lib "diCrPKI.dll" (ByVal strKey As String) As Integer
	Public Declare Function RSA_CheckKey Lib "diCrPKI.dll" (ByVal strKey As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_KeyHashCode Lib "diCrPKI.dll" (ByVal strKeyString As String) As Integer
	Public Declare Function RSA_KeyMatch Lib "diCrPKI.dll" (ByVal strPrivateKey As String, ByVal strPublicKey As String) As Integer
	Public Declare Function RSA_RawPublic Lib "diCrPKI.dll" (ByRef abData As Byte, ByVal nDataLen As Integer, ByVal strPublicKey As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_RawPrivate Lib "diCrPKI.dll" (ByRef abData As Byte, ByVal nDataLen As Integer, ByVal strPrivateKey As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_EncodeMsg Lib "diCrPKI.dll" (ByRef abOutput As Byte, ByVal nOutputLen As Integer, ByRef abMessage As Byte, ByVal nMsgLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_DecodeMsg Lib "diCrPKI.dll" (ByRef abOutput As Byte, ByVal nOutputLen As Integer, ByRef abInput As Byte, ByVal nInputLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_KemWrap Lib "diCrPKI.dll" (ByRef abOutput As Byte, ByVal nOutBytes As Integer, ByRef abData As Byte, ByVal nDataLen As Integer, ByVal strPublicKey As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RSA_KemUnwrap Lib "diCrPKI.dll" (ByRef abOutput As Byte, ByVal nOutBytes As Integer, ByRef abData As Byte, ByVal nDataLen As Integer, ByVal strPrivateKey As String, ByVal nOptions As Integer) As Integer
	
	' X.509 CERTIFICATE FUNCTIONS
	Public Declare Function X509_MakeCertSelf Lib "diCrPKI.dll" (ByVal strNewCertFile As String, ByVal strEPKFile As String, ByVal nCertNum As Integer, ByVal nYearsValid As Integer, ByVal strDistName As String, ByVal strEmail As String, ByVal KeyUsageFlags As Integer, ByVal strPassword As String, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_MakeCert Lib "diCrPKI.dll" (ByVal strNewCertFile As String, ByVal strIssuerCertFile As String, ByVal strSubjectPubKeyFile As String, ByVal strIssuerPvkInfoFile As String, ByVal nCertNum As Integer, ByVal nYearsValid As Integer, ByVal strDistName As String, ByVal strEmail As String, ByVal KeyUsageFlags As Integer, ByVal strPassword As String, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_CertRequest Lib "diCrPKI.dll" (ByVal strReqFile As String, ByVal strEPKFile As String, ByVal strDistName As String, ByVal strReserved As String, ByVal strPassword As String, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_VerifyCert Lib "diCrPKI.dll" (ByVal strCertToVerify As String, ByVal strIssuerCert As String, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_CertThumb Lib "diCrPKI.dll" (ByVal strCertFile As String, ByVal strHexHash As String, ByVal nHexHashLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_CertIsValidNow Lib "diCrPKI.dll" (ByVal strCertFile As String, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_CertIssuedOn Lib "diCrPKI.dll" (ByVal strCertFile As String, ByVal strOutput As String, ByVal nOutputLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_CertExpiresOn Lib "diCrPKI.dll" (ByVal strCertFile As String, ByVal strOutput As String, ByVal nOutputLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_CertSerialNumber Lib "diCrPKI.dll" (ByVal strCertFile As String, ByVal strOutput As String, ByVal nOutputLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_CertIssuerName Lib "diCrPKI.dll" (ByVal strCertFile As String, ByVal strOutput As String, ByVal nOutputLen As Integer, ByVal strDelim As String, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_CertSubjectName Lib "diCrPKI.dll" (ByVal strCertFile As String, ByVal strOutput As String, ByVal nOutputLen As Integer, ByVal strDelim As String, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_HashIssuerAndSN Lib "diCrPKI.dll" (ByVal strCertFile As String, ByVal strOutput As String, ByVal nOutputLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_GetCertFromP7Chain Lib "diCrPKI.dll" (ByVal strOutputFile As String, ByVal strP7cFile As String, ByVal nIndex As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_GetCertFromPFX Lib "diCrPKI.dll" (ByVal strOutputFile As String, ByVal strPfxFile As String, ByVal strReserved As String, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_KeyUsageFlags Lib "diCrPKI.dll" (ByVal strCertFile As String) As Integer
	Public Declare Function X509_QueryCert Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal nOutChars As Integer, ByVal strFileIn As String, ByVal szQuery As String, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_ReadStringFromFile Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal nOutChars As Integer, ByVal strCertFile As String, ByVal nOptions As Integer) As Integer
	Public Declare Function X509_SaveFileFromString Lib "diCrPKI.dll" (ByVal strNewCertFile As String, ByVal strCertString As String, ByVal nOptions As Integer) As Integer
	
	' PFX (PKCS-12) FUNCTIONS
	Public Declare Function PFX_MakeFile Lib "diCrPKI.dll" (ByVal strOutputFile As String, ByVal strCertFile As String, ByVal strKeyFile As String, ByVal strPassword As String, ByVal strFriendlyName As String, ByVal nOptions As Integer) As Integer
	Public Declare Function PFX_VerifySig Lib "diCrPKI.dll" (ByVal strFileName As String, ByVal strPassword As String, ByVal nOptions As Integer) As Integer
	
	' TRIPLE DATA ENCRYPTION ALGORITHM (TDEA/3DES/TRIPLE DES) BLOCK CIPHER FUNCTIONS
	Public Declare Function TDEA_BytesMode Lib "diCrPKI.dll" (ByRef abOutput As Byte, ByRef abInput As Byte, ByVal nDataLen As Integer, ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abIV As Byte) As Integer
	Public Declare Function TDEA_HexMode Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strIV As String) As Integer
	Public Declare Function TDEA_B64Mode Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal strKey As String, ByVal bEncrypt As Integer, ByVal strMode As String, ByVal strIV As String) As Integer
	Public Declare Function TDEA_File Lib "diCrPKI.dll" (ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByVal bEncrypt As Integer, ByVal strMode As String, ByRef abIV As Byte) As Integer
	
	' GENERIC BLOCK CIPHER FUNCTIONS (new in v3.2)
	Public Declare Function CIPHER_Bytes Lib "diCrPKI.dll" (ByVal fEncrypt As Integer, ByRef abOutput As Byte, ByRef abData As Byte, ByVal nDataLen As Integer, ByRef abKey As Byte, ByRef abIV As Byte, ByVal strAlgAndMode As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CIPHER_Hex Lib "diCrPKI.dll" (ByVal fEncrypt As Integer, ByVal strOutput As String, ByVal nOutChars As Integer, ByVal strData As String, ByVal strKey As String, ByVal strIV As String, ByVal strAlgAndMode As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CIPHER_File Lib "diCrPKI.dll" (ByVal fEncrypt As Integer, ByVal strFileOut As String, ByVal strFileIn As String, ByRef abKey As Byte, ByRef abIV As Byte, ByVal strAlgAndMode As String, ByVal nOptions As Integer) As Integer
	Public Declare Function CIPHER_KeyWrap Lib "diCrPKI.dll" (ByRef abOutput As Byte, ByVal nOutBytes As Integer, ByRef abData As Byte, ByVal nDataLen As Integer, ByRef abKek As Byte, ByVal nKekLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function CIPHER_KeyUnwrap Lib "diCrPKI.dll" (ByRef abOutput As Byte, ByVal nOutBytes As Integer, ByRef abData As Byte, ByVal nDataLen As Integer, ByRef abKek As Byte, ByVal nKekLen As Integer, ByVal nOptions As Integer) As Integer
	
	' MESSAGE DIGEST HASH FUNCTIONS
	Public Declare Function HASH_HexFromBytes Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal nOutChars As Integer, ByRef abMessage As Byte, ByVal nMsgLen As Integer, ByVal nOptions As Integer) As Integer
	' Alternative alias of HASH_HexFromBytes to cope with ANSI strings...
	Public Declare Function HASH_HexFromString Lib "diCrPKI.dll"  Alias "HASH_HexFromBytes"(ByVal strOutput As String, ByVal nOutChars As Integer, ByVal strMessage As String, ByVal nMsgLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function HASH_HexFromFile Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal nOutChars As Integer, ByVal strFileName As String, ByVal nOptions As Integer) As Integer
	Public Declare Function HASH_Bytes Lib "diCrPKI.dll" (ByRef abDigest As Byte, ByVal nDigLen As Integer, ByRef abMessage As Byte, ByVal nMsgLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function HASH_File Lib "diCrPKI.dll" (ByRef abDigest As Byte, ByVal nDigLen As Integer, ByVal strFileName As String, ByVal nOptions As Integer) As Integer
	Public Declare Function HASH_HexFromHex Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal nMaxChars As Integer, ByVal strMsgHex As String, ByVal nOptions As Integer) As Integer
	
	' HMAC FUNCTIONS
	Public Declare Function HMAC_Bytes Lib "diCrPKI.dll" (ByRef abDigest As Byte, ByVal nDigLen As Integer, ByRef abMessage As Byte, ByVal nMsgLen As Integer, ByRef abKey As Byte, ByVal nKeyLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function HMAC_HexFromBytes Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal nOutChars As Integer, ByRef abMessage As Byte, ByVal nMsgLen As Integer, ByRef abKey As Byte, ByVal nKeyLen As Integer, ByVal nOptions As Integer) As Integer
	Public Declare Function HMAC_HexFromHex Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal nOutChars As Integer, ByVal strMsgHex As String, ByVal strKeyHex As String, ByVal nOptions As Integer) As Integer
	
	' ENCODING CONVERSION FUNCTIONS
	' (See cnv* Functions below for VB-friendly versions of these)
	Public Declare Function CNV_HexStrFromBytes Lib "diCrPKI.dll" (ByVal strHex As String, ByVal nHexStrLen As Integer, ByRef abData As Byte, ByVal nDataLen As Integer) As Integer
	Public Declare Function CNV_BytesFromHexStr Lib "diCrPKI.dll" (ByRef abData As Byte, ByVal nDataLen As Integer, ByVal strHex As String) As Integer
	Public Declare Function CNV_HexFilter Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal nStrLen As Integer) As Integer
	Public Declare Function CNV_B64StrFromBytes Lib "diCrPKI.dll" (ByVal strB64 As String, ByVal nB64StrLen As Integer, ByRef abData As Byte, ByVal nDataLen As Integer) As Integer
	Public Declare Function CNV_BytesFromB64Str Lib "diCrPKI.dll" (ByRef abData As Byte, ByVal nDataLen As Integer, ByVal strB64 As String) As Integer
	Public Declare Function CNV_B64Filter Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal strInput As String, ByVal nStrLen As Integer) As Integer
	
	' UTF-8 CONVERSION/CHECK FUNCTIONS
	Public Declare Function CNV_UTF8FromLatin1 Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal nOutChars As Integer, ByVal strInput As String) As Integer
	Public Declare Function CNV_Latin1FromUTF8 Lib "diCrPKI.dll" (ByVal strOutput As String, ByVal nOutChars As Integer, ByVal strInput As String) As Integer
	Public Declare Function CNV_CheckUTF8 Lib "diCrPKI.dll" (ByVal strInput As String) As Integer
	
	' ERROR FUNCTIONS
	Public Declare Function PKI_LastError Lib "diCrPKI.dll" (ByVal strErrMsg As String, ByVal nMaxMsgLen As Integer) As Integer
	' See also pkiGetLastError() below
	Public Declare Function PKI_ErrorCode Lib "diCrPKI.dll" () As Integer
	Public Declare Function PKI_ErrorLookup Lib "diCrPKI.dll" (ByVal strErrMsg As String, ByVal nMaxMsgLen As Integer, ByVal nErrorCode As Integer) As Integer
	Public Declare Function PKI_PowerUpTests Lib "diCrPKI.dll" (ByVal nOptions As Integer) As Integer
	
	' DIAGNOSTIC FUNCTIONS
	' (NB PKI_Version changed in version 2.8 to ignore parameters - just use zeros)
	Public Declare Function PKI_Version Lib "diCrPKI.dll" (ByVal nReserved1 As Integer, ByVal nReserved2 As Integer) As Integer
	Public Declare Function PKI_LicenceType Lib "diCrPKI.dll" (ByVal nReserved As Integer) As Integer
	Public Declare Function PKI_CompileTime Lib "diCrPKI.dll" (ByVal strCompiledOn As String, ByVal nStrLen As Integer) As Integer
	Public Declare Function PKI_ModuleName Lib "diCrPKI.dll" (ByVal strModuleName As String, ByVal nStrLen As Integer, ByVal nOptions As Integer) As Integer
	
	' RNG FUNCTIONS
	Public Declare Function RNG_Bytes Lib "diCrPKI.dll" (ByRef abData As Byte, ByVal nDataLen As Integer, ByVal strSeed As String, ByVal nSeedLen As Integer) As Integer
	' Alternative alias of RNG_Bytes to write to an ANSI string...
	Public Declare Function RNG_String Lib "diCrPKI.dll"  Alias "RNG_Bytes"(ByVal strData As String, ByVal nDataLen As Integer, ByVal strSeed As String, ByVal nSeedLen As Integer) As Integer
	Public Declare Function RNG_Number Lib "diCrPKI.dll" (ByVal nLower As Integer, ByVal nUpper As Integer) As Integer
	Public Declare Function RNG_BytesWithPrompt Lib "diCrPKI.dll" (ByRef abOutput As Byte, ByVal nOutputLen As Integer, ByVal strPrompt As String, ByVal nOptions As Integer) As Integer
	' Alternative alias of RNG_BytesWithPrompt to write to an ANSI string...
	Public Declare Function RNG_StringWithPrompt Lib "diCrPKI.dll"  Alias "RNG_BytesWithPrompt"(ByVal strData As String, ByVal nDataLen As Integer, ByVal strPrompt As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RNG_Initialize Lib "diCrPKI.dll" (ByVal strSeedFile As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RNG_MakeSeedFile Lib "diCrPKI.dll" (ByVal strSeedFile As String, ByVal strPrompt As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RNG_UpdateSeedFile Lib "diCrPKI.dll" (ByVal strSeedFile As String, ByVal nOptions As Integer) As Integer
	Public Declare Function RNG_Test Lib "diCrPKI.dll" (ByVal strFileName As String, ByVal nOptions As Integer) As Integer
	
	' MISC FUNCTIONS
	Public Declare Function PWD_Prompt Lib "diCrPKI.dll" (ByVal strPassword As String, ByVal nPwdLen As Integer, ByVal strCaption As String) As Integer
	Public Declare Function PWD_PromptEx Lib "diCrPKI.dll" (ByVal strPassword As String, ByVal nPwdLen As Integer, ByVal strCaption As String, ByVal strPrompt As String, ByVal nOptions As Integer) As Integer
	Public Declare Function WIPE_File Lib "diCrPKI.dll" (ByVal strFileName As String, ByVal nOptions As Integer) As Integer
	Public Declare Function WIPE_Data Lib "diCrPKI.dll" (ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	' Alternative aliases of WIPE_Data to cope with Byte and String types explicitly...
	Public Declare Function WIPE_Bytes Lib "diCrPKI.dll"  Alias "WIPE_Data"(ByRef abData As Byte, ByVal nBytes As Integer) As Integer
	Public Declare Function WIPE_String Lib "diCrPKI.dll"  Alias "WIPE_Data"(ByVal strData As String, ByVal nStrLen As Integer) As Integer
	
	' *** END OF CRYPTOSYS PKI DECLARATIONS
	
	' SOME USEFUL WRAPPER FUNCTIONS
	' [2006-08-11] Conversion functions updated to handle errors better.
	
	'Public Function cnvHexStrFromBytes(abData() As Byte) As String
	'' Returns hex string encoding of bytes in abData or empty string if error
	'    Dim strHex As String
	'    Dim nHexLen As Long
	'    Dim nDataLen As Long
	'
	'    On Error GoTo CatchEmptyData
	'    nDataLen = UBound(abData) - LBound(abData) + 1
    '    nHexLen = CNV_HexStrFromBytes("", 0, abData(0), nDataLen)
    '    If nHexLen <= 0 Then
    '        Exit Function
    '    End If
    '    strHex = String$(nHexLen, " ")
    '    nHexLen = CNV_HexStrFromBytes(strHex, nHexLen, abData(0), nDataLen)
    '    If nHexLen <= 0 Then
    '        Exit Function
    '    End If
    '    cnvHexStrFromBytes = Left$(strHex, nHexLen)
    '
    'CatchEmptyData:
    '
    'End Function

    'Public Function cnvHexStrFromString(strData As String) As String
    '' Returns hex string encoding of ASCII string or empty string if error
    '    Dim strHex As String
    '    Dim nHexLen As Long
    '    Dim nDataLen As Long
    '    Dim abData() As Byte
    '
    '    If Len(strData) = 0 Then Exit Function
    '    abData = StrConv(strData, vbFromUnicode)
    '    nDataLen = UBound(abData) - LBound(abData) + 1
    '    nHexLen = CNV_HexStrFromBytes("", 0, abData(0), nDataLen)
    '    If nHexLen <= 0 Then
    '        Exit Function
    '    End If
    '    strHex = String$(nHexLen, " ")
    '    nHexLen = CNV_HexStrFromBytes(strHex, nHexLen, abData(0), nDataLen)
    '    If nHexLen <= 0 Then
    '        Exit Function
    '    End If
    '    cnvHexStrFromString = Left$(strHex, nHexLen)
    'End Function

    'Public Function cnvBytesFromHexStr(strHex As String) As Variant
    '' Returns a Variant to an array of bytes decoded from a hex string
    '    Dim abData() As Byte
    '    Dim nDataLen As Long
    '
    '    ' Set default return value that won't cause a run-time error
    '    cnvBytesFromHexStr = StrConv("", vbFromUnicode)
    '    nDataLen = CNV_BytesFromHexStr(0, 0, strHex)
    '    If nDataLen <= 0 Then
    '        Exit Function
    '    End If
    '    ReDim abData(nDataLen - 1)
    '    nDataLen = CNV_BytesFromHexStr(abData(0), nDataLen, strHex)
    '    If nDataLen <= 0 Then
    '        Exit Function
    '    End If
    '    ReDim Preserve abData(nDataLen - 1)
    '    cnvBytesFromHexStr = abData
    'End Function

    'Public Function cnvStringFromHexStr(ByVal strHex As String) As String
    '' Converts string <strHex> in hex format to string of ANSI chars
    '' with value between 0 and 255.
    '' E.g. "6162632E" will be converted to "abc."
    '    Dim abData() As Byte
    '    If Len(strHex) = 0 Then Exit Function
    '    abData = cnvBytesFromHexStr(strHex)
    '    cnvStringFromHexStr = StrConv(abData, vbUnicode)
    'End Function
    '
    'Public Function cnvHexFilter(strHex As String) As String
    '' Returns a string stripped of any invalid hex characters
    '    Dim strFiltered As String
    '    Dim nLen As Long
    '
    '    strFiltered = String(Len(strHex), " ")
    '    nLen = CNV_HexFilter(strFiltered, strHex, Len(strHex))
    '    If nLen > 0 Then
    '        strFiltered = Left$(strFiltered, nLen)
    '    Else
    '        strFiltered = ""
    '    End If
    '    cnvHexFilter = strFiltered
    'End Function

    'Public Function cnvB64StrFromBytes(abData() As Byte) As String
    '' Returns base64 string encoding of bytes in abData or empty string if error
    '    Dim strB64 As String
    '    Dim nB64Len As Long
    '    Dim nDataLen As Long
    '
    '    On Error GoTo CatchEmptyData
    '    nDataLen = UBound(abData) - LBound(abData) + 1
    '    nB64Len = CNV_B64StrFromBytes("", 0, abData(0), nDataLen)
    '    If nB64Len <= 0 Then
    '        Exit Function
    '    End If
    '    strB64 = String$(nB64Len, " ")
    '    nB64Len = CNV_B64StrFromBytes(strB64, nB64Len, abData(0), nDataLen)
    '    If nB64Len <= 0 Then
    '        Exit Function
    '    End If
    '    cnvB64StrFromBytes = Left$(strB64, nB64Len)
    '
    'CatchEmptyData:
    '
    'End Function

    'Public Function cnvB64StrFromString(strData As String) As String
    '' Returns base64 string encoding of ASCII string or empty string if error
    '    Dim strB64 As String
    '    Dim nB64Len As Long
    '    Dim nDataLen As Long
    '    Dim abData() As Byte
    '
    '    If Len(strData) = 0 Then Exit Function
    '    abData = StrConv(strData, vbFromUnicode)
    '    nDataLen = UBound(abData) - LBound(abData) + 1
    '    nB64Len = CNV_B64StrFromBytes("", 0, abData(0), nDataLen)
	'    If nB64Len <= 0 Then
	'        Exit Function
	'    End If
	'    strB64 = String$(nB64Len, " ")
	'    nB64Len = CNV_B64StrFromBytes(strB64, nB64Len, abData(0), nDataLen)
	'    If nB64Len <= 0 Then
	'        Exit Function
	'    End If
	'    cnvB64StrFromString = Left$(strB64, nB64Len)
	'End Function
	'
	'Public Function cnvBytesFromB64Str(strB64 As String) As Variant
	'' Returns a Variant to an array of bytes decoded from a base64 string
	'    Dim abData() As Byte
	'    Dim nDataLen As Long
	'
	'    ' Set default return value that won't cause a run-time error
	'    cnvBytesFromB64Str = StrConv("", vbFromUnicode)
	'    nDataLen = CNV_BytesFromB64Str(0, 0, strB64)
	'    If nDataLen <= 0 Then
	'        Exit Function
	'    End If
	'    ReDim abData(nDataLen - 1)
	'    nDataLen = CNV_BytesFromB64Str(abData(0), nDataLen, strB64)
	'    If nDataLen <= 0 Then
	'        Exit Function
	'    End If
	'    ReDim Preserve abData(nDataLen - 1)
	'    cnvBytesFromB64Str = abData
	'End Function
	'
	'Public Function cnvB64Filter(strB64 As String) As String
	'' Returns a string stripped of any invalid base64 characters
	'    Dim strFiltered As String
	'    Dim nLen As Long
	'
	'    strFiltered = String(Len(strB64), " ")
	'    nLen = CNV_B64Filter(strFiltered, strB64, Len(strB64))
	'    If nLen > 0 Then
	'        strFiltered = Left$(strFiltered, nLen)
	'    Else
	'        strFiltered = ""
	'    End If
	'    cnvB64Filter = strFiltered
	'End Function
	
	Public Function pkiGetLastError() As String
		' Returns the last error message as a string, if any
		Dim sErrMsg As String
		Dim nLen As Integer
		
		nLen = 511
		sErrMsg = New String(" ", nLen)
		nLen = PKI_LastError(sErrMsg, nLen)
		sErrMsg = Left(sErrMsg, nLen)
		pkiGetLastError = sErrMsg
	End Function
	
	Public Function pkiErrorLookup(ByRef nErrCode As Integer) As String
		' Returns the error message for error code nErrCode
		Dim sErrMsg As String
		Dim nLen As Integer
		
		nLen = 127
		sErrMsg = New String(" ", nLen)
		nLen = PKI_ErrorLookup(sErrMsg, nLen, nErrCode)
		sErrMsg = Left(sErrMsg, nLen)
		pkiErrorLookup = sErrMsg
	End Function
	
	Public Function pwdPrompt(Optional ByRef sCaption As String = "") As String
		Dim sPassword As String
		Dim nLen As Integer
		
		nLen = 255
		sPassword = New String(" ", nLen)
		nLen = PWD_Prompt(sPassword, nLen, sCaption)
		If nLen < 0 Then
			Exit Function
		ElseIf nLen > 0 Then 
			pwdPrompt = Left(sPassword, nLen)
		End If
		' Clean up local variable
		Call WIPE_String(sPassword, nLen)
	End Function
	
	Public Function rsaReadPrivateKey(ByRef strEPKFile As String, ByRef strPassword As String) As String
		' Reads the private key from a PKCS-8 EncryptedPrivateKeyInfo file
		' (as created by RSA_MakeKeys)
		' Returns the key as a base64 string or an empty string on error
		Dim nLen As Integer
		Dim lngRet As Integer
		' How long is PrivateKey string?
		nLen = RSA_ReadEncPrivateKey("", 0, strEPKFile, strPassword, 0)
		If nLen <= 0 Then
			Exit Function
		End If
		' Pre-dimension the string to receive data
		rsaReadPrivateKey = New String(" ", nLen)
		' Read in the Private Key
		lngRet = RSA_ReadEncPrivateKey(rsaReadPrivateKey, nLen, strEPKFile, strPassword, 0)
		
	End Function
	
	Public Function rsaReadPublicKey(ByRef strKeyFile As String) As String
		' Reads the public key from a PKCS-1 RSAPublicKey file
		' (as created by RSA_MakeKeys)
		' Returns the key as a base64 string or an empty string on error
		Dim nLen As Integer
		Dim lngRet As Integer
		' How long is key string?
		nLen = RSA_ReadPublicKey("", 0, strKeyFile, 0)
		If nLen <= 0 Then
			Exit Function
		End If
		' Pre-dimension the string to receive data
		rsaReadPublicKey = New String(" ", nLen)
		' Read in the Private Key
		lngRet = RSA_ReadPublicKey(rsaReadPublicKey, nLen, strKeyFile, 0)
		
	End Function
	
	Public Function rsaReadPrivateKeyInfo(ByRef strKeyFile As String) As String
		' Like rsaReadPrivateKey but for an UNencrypted private key info file
		' Returns the key as a base64 string or an empty string on error
		Dim lngKeyLen As Integer
		Dim lngRet As Integer
		Dim strKey As String
		' How long is key string?
		lngKeyLen = RSA_ReadPrivateKeyInfo("", 0, strKeyFile, 0)
		If lngKeyLen <= 0 Then
			Exit Function
		End If
		' Pre-dimension the string to receive data
		strKey = New String(" ", lngKeyLen)
		' Read in the Private Key
		lngRet = RSA_ReadPrivateKeyInfo(strKey, lngKeyLen, strKeyFile, 0)
		rsaReadPrivateKeyInfo = strKey
		
	End Function
	
	Public Function rsaGetPublicKeyFromCert(ByRef strCertFile As String) As String
		' Reads the public key from an X.509 certificate file
		' Returns the key as a base64 string or an empty string on error
		Dim nLen As Integer
		Dim lngRet As Integer
		' How long is key string?
		nLen = RSA_GetPublicKeyFromCert("", 0, strCertFile, 0)
		If nLen <= 0 Then
			Exit Function
		End If
		' Pre-dimension the string to receive data
		rsaGetPublicKeyFromCert = New String(" ", nLen)
		' Read in the Private Key
		lngRet = RSA_GetPublicKeyFromCert(rsaGetPublicKeyFromCert, nLen, strCertFile, 0)
		
	End Function
	
	Public Function wipeString(ByRef strToWipe As String) As String
		' Securely wipes a string and returns a blank string
		' To call: strToWipe = wipeString(strToWipe)
		Call WIPE_String(strToWipe, Len(strToWipe))
		wipeString = ""
	End Function
	
	Public Function wipeBytes(ByRef abToWipe() As Byte) As Object
		Call WIPE_Data(abToWipe(0), UBound(abToWipe) + 1)

        'abToWipe = System.Text.UnicodeEncoding.Unicode.GetBytes(StrConv("", vbFromUnicode))
	End Function
	
	' *** END OF USEFUL WRAPPER FUNCTIONS
	
	Public Function Create_Autack_Signature(ByRef strData As String, Optional ByRef strPrivateKey As String = "", Optional ByRef strXmlPrivate As String = "") As String
		' INPUT:  data as text string; RSA private key in XML format.
		' OUTPUT: RSA/ISO9796-1 signature in hex-encoded form (as the data will be represented in the AUTACK message)
		'         or empty string on error.
		' REMARKS: uses SHA-1 as message digest hash algorithm.
		''''Dim strPrivateKey As String
		Dim klen As Integer
		Dim mlen As Integer
		Dim blen As Integer
		Dim dlen As Integer
		Dim r As Integer
		Dim nKeyBits As Integer
		Dim abData() As Byte
		Dim abDigest() As Byte
		Dim abBlock() As Byte
		Dim strHexSig As String
		
		' [PRELIM: just while we're updating versions] Check PKI version is OK
		If PKI_Version(0, 0) < 324 Then
			MsgBox("Need at least CryptoSys PKI Version 3.2.4", MsgBoxStyle.OKOnly + MsgBoxStyle.Critical)
			Exit Function
		End If
		
		' 04.07.2008
		' JanP added functionality to pass a crypted strPrivate key, instead of a XMLprivate Key
		
		If strPrivateKey = "" Then
			' only necessary if an XML-key is passed!!!
			' 1. Read in RSA private key from XML
			klen = RSA_FromXMLString("", 0, strXmlPrivate, 0)
			Debug.Print("RSA_FromXMLString returns " & klen & " (expecting >0)")
			If klen <= 0 Then Exit Function
			strPrivateKey = New String(" ", klen)
			klen = RSA_FromXMLString(strPrivateKey, Len(strPrivateKey), strXmlPrivate, 0)
			nKeyBits = RSA_KeyBits(strPrivateKey)
			Debug.Print("RSA key length=" & nKeyBits & " bits")
		End If
		
		' 2. Make message digest hash of input data
		Debug.Print("Input data='" & strData & "'")
		' Convert string to an array of bytes
		'UPGRADE_ISSUE: Constant vbFromUnicode was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_TODO: Code was upgraded to use System.Text.UnicodeEncoding.Unicode.GetBytes() which may not have the same behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="93DD716C-10E3-41BE-A4A8-3BA40157905B"'
        'TODO - Hva faen er detta - f�r feil i vbFROMUniCode
        'abData = System.Text.UnicodeEncoding.Unicode.GetBytes(StrConv(strData, vbFromUnicode))
		mlen = UBound(abData) + 1
		' Allocate space for digest byte array
		ReDim abDigest(PKI_SHA1_BYTES - 1)
		' Compute SHA-1 digest using byte array as input
		dlen = HASH_Bytes(abDigest(0), PKI_SHA1_BYTES, abData(0), mlen, PKI_HASH_SHA1)
		Debug.Print("HASH_Bytes returns " & dlen & " (expecting " & PKI_SHA1_BYTES & ")")
       	
		' 3. Encapsulate hash digest in ISO9796-1 encoding
		'  -- we need a block of the same length as the key in bytes
		blen = RSA_KeyBytes(strPrivateKey)
		ReDim abBlock(blen - 1)
		' plus pass the exact key length in bits as part of the option parameter...
		r = RSA_EncodeMsg(abBlock(0), blen, abDigest(0), dlen, PKI_EMSIG_ISO9796 + nKeyBits)
		Debug.Print("RSA_EncodeMsg returns " & r & " (expecting >=0)")
		If (r < 0) Then Exit Function

		' 4. Sign block with RSA private key to create signature
		' -- use special ISO9796/X9.31/P1363 RSA2 method with magic value 0x6
		r = RSA_RawPrivate(abBlock(0), blen, strPrivateKey, &H6)
		Debug.Print("RSA_RawPrivate returns " & r & " (expecting 0)")
		If (r <> 0) Then Exit Function
		
		' 5. Convert to hex encoding

		' 6. Output signature as a hex string
		Debug.Print("Signature=" & vbCrLf & strHexSig)
		Create_Autack_Signature = strHexSig
		
		' Clean up
		Call wipeBytes(abData)
		Call wipeBytes(abDigest)
		Call wipeBytes(abBlock)
		Call wipeString(strPrivateKey)
		
	End Function
End Module
