Option Strict Off
Option Explicit On

Friend Class frmUnlock
	Inherits System.Windows.Forms.Form
	
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Close()
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        frmMATCH_Manual.UnlockPayment(1, 1, 1, True)
		cmdRefresh_Click(cmdRefresh, New System.EventArgs())
		
	End Sub
	
	Private Sub cmdRefresh_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRefresh.Click
        Dim aLocked(,) As Object
		Dim i As Short
		
		' Which items are locked?
		aLocked = frmMATCH_Manual.FindPaymentsLocked(True)
		
		Me.lstUsers.Items.Clear()
		If Not Array_IsEmpty(aLocked) Then
			For i = 0 To UBound(aLocked, 2)
                Me.lstUsers.Items.Add(aLocked(0, i) & " " & aLocked(1, i))
			Next i
		Else
			Me.lstUsers.Items.Add("No payments locked")
		End If
		
	End Sub
	Private Sub frmUnlock_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim aLocked(,) As Object
		Dim i As Short
		Dim sMsgString As String
		
		' Must be done before we show eMail form
		FormvbStyle(Me, "dollar.jpg")
		FormLRSCaptions(Me)
		
		' Warn user that all persons must be logged out of BabelBank
		sMsgString = LRS(60185) '"Denne funksjonen l�ser opp alle elementer i tabellen LockedPayments." & vbCrLf
		sMsgString = sMsgString & LRS(60186) & vbCrLf & vbCrLf ' "Det er meget viktig at du er helt sikker p� at ingen brukere benytter BabelBank." & vbCrLf
		sMsgString = sMsgString & LRS(60187) & vbCrLf ' "F�lgende brukere ligger med oppf�ring i LockedPayments-tabellen:" & vbCrLf
		
		Me.lblCustom.Text = sMsgString
		' Which items are locked?
		aLocked = frmMATCH_Manual.FindPaymentsLocked(True)
		
		If Not Array_IsEmpty(aLocked) Then
			For i = 0 To UBound(aLocked, 2)
                Me.lstUsers.Items.Add(aLocked(0, i) & " " & aLocked(1, i))
			Next i
		Else
			Me.lstUsers.Items.Add(LRS(60188)) '"No payments locked"
		End If
	End Sub
End Class
