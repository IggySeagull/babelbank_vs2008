﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVisma_Start
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVisma_Start))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtStatus = New System.Windows.Forms.TextBox
        Me.prgActionProgress = New System.Windows.Forms.ProgressBar
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuStart = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuReportTotal = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuReportOverview = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuReportDetail = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuReportError = New System.Windows.Forms.ToolStripMenuItem
        Me.lblBBVersion = New System.Windows.Forms.Label
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtStatus
        '
        Me.txtStatus.AcceptsReturn = True
        Me.txtStatus.BackColor = System.Drawing.SystemColors.Window
        Me.txtStatus.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtStatus.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatus.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtStatus.Location = New System.Drawing.Point(15, 93)
        Me.txtStatus.MaxLength = 0
        Me.txtStatus.Multiline = True
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.ReadOnly = True
        Me.txtStatus.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtStatus.Size = New System.Drawing.Size(374, 290)
        Me.txtStatus.TabIndex = 13
        '
        'prgActionProgress
        '
        Me.prgActionProgress.Location = New System.Drawing.Point(15, 54)
        Me.prgActionProgress.Name = "prgActionProgress"
        Me.prgActionProgress.Size = New System.Drawing.Size(374, 20)
        Me.prgActionProgress.TabIndex = 17
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem2})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(410, 24)
        Me.MenuStrip1.TabIndex = 18
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuStart, Me.mnuClose})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(49, 20)
        Me.ToolStripMenuItem1.Text = "50100"
        '
        'mnuStart
        '
        Me.mnuStart.Name = "mnuStart"
        Me.mnuStart.Size = New System.Drawing.Size(104, 22)
        Me.mnuStart.Text = "Start"
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(104, 22)
        Me.mnuClose.Text = "35118"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReportTotal, Me.mnuReportOverview, Me.mnuReportDetail, Me.mnuReportError})
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(49, 20)
        Me.ToolStripMenuItem2.Text = "60380"
        '
        'mnuReportTotal
        '
        Me.mnuReportTotal.Name = "mnuReportTotal"
        Me.mnuReportTotal.Size = New System.Drawing.Size(104, 22)
        Me.mnuReportTotal.Text = "35011"
        '
        'mnuReportOverview
        '
        Me.mnuReportOverview.Name = "mnuReportOverview"
        Me.mnuReportOverview.Size = New System.Drawing.Size(104, 22)
        Me.mnuReportOverview.Text = "35266"
        '
        'mnuReportDetail
        '
        Me.mnuReportDetail.Name = "mnuReportDetail"
        Me.mnuReportDetail.Size = New System.Drawing.Size(104, 22)
        Me.mnuReportDetail.Text = "35012"
        '
        'mnuReportError
        '
        Me.mnuReportError.Name = "mnuReportError"
        Me.mnuReportError.Size = New System.Drawing.Size(104, 22)
        Me.mnuReportError.Text = "35013"
        '
        'lblBBVersion
        '
        Me.lblBBVersion.BackColor = System.Drawing.SystemColors.Control
        Me.lblBBVersion.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBBVersion.ForeColor = System.Drawing.Color.Black
        Me.lblBBVersion.Location = New System.Drawing.Point(312, 31)
        Me.lblBBVersion.Name = "lblBBVersion"
        Me.lblBBVersion.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBBVersion.Size = New System.Drawing.Size(97, 16)
        Me.lblBBVersion.TabIndex = 25
        Me.lblBBVersion.Text = "lblBBVersion"
        '
        'frmVisma_Start
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(410, 406)
        Me.Controls.Add(Me.lblBBVersion)
        Me.Controls.Add(Me.txtStatus)
        Me.Controls.Add(Me.prgActionProgress)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmVisma_Start"
        Me.Text = "Visma Payment (BabelBank)"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents txtStatus As System.Windows.Forms.TextBox
    Public WithEvents prgActionProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportTotal As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportOverview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportDetail As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStart As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents lblBBVersion As System.Windows.Forms.Label
End Class
