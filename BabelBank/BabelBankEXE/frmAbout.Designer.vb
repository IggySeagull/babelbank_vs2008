<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmAbout
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents picVB As System.Windows.Forms.PictureBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents lblAppPath As System.Windows.Forms.Label
	Public WithEvents lblVersion As System.Windows.Forms.Label
	Public WithEvents lblAdress As System.Windows.Forms.Label
    Public WithEvents lblHeading As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.picVB = New System.Windows.Forms.PictureBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblAppPath = New System.Windows.Forms.Label
        Me.lblVersion = New System.Windows.Forms.Label
        Me.lblAdress = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.txtDatabasePath = New System.Windows.Forms.TextBox
        Me.lblDatabasePath = New System.Windows.Forms.Label
        Me.lblLicensePath = New System.Windows.Forms.Label
        Me.txtLicensePath = New System.Windows.Forms.TextBox
        Me.frmTeamViewer = New System.Windows.Forms.Button
        Me.cmdAccess2016 = New System.Windows.Forms.Button
        CType(Me.picVB, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picVB
        '
        Me.picVB.BackColor = System.Drawing.SystemColors.Window
        Me.picVB.Cursor = System.Windows.Forms.Cursors.Default
        Me.picVB.ForeColor = System.Drawing.SystemColors.WindowText
        Me.picVB.Image = Global.BabelBank.My.Resources.Resources.VB_Full_Farge_RGB
        Me.picVB.Location = New System.Drawing.Point(0, 226)
        Me.picVB.Name = "picVB"
        Me.picVB.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.picVB.Size = New System.Drawing.Size(198, 58)
        Me.picVB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picVB.TabIndex = 4
        Me.picVB.TabStop = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(528, 259)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(80, 25)
        Me.cmdOK.TabIndex = 1
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblAppPath
        '
        Me.lblAppPath.BackColor = System.Drawing.SystemColors.Control
        Me.lblAppPath.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAppPath.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAppPath.Location = New System.Drawing.Point(200, 123)
        Me.lblAppPath.Name = "lblAppPath"
        Me.lblAppPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAppPath.Size = New System.Drawing.Size(416, 30)
        Me.lblAppPath.TabIndex = 5
        Me.lblAppPath.Text = "App.path"
        '
        'lblVersion
        '
        Me.lblVersion.BackColor = System.Drawing.SystemColors.Control
        Me.lblVersion.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVersion.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVersion.Location = New System.Drawing.Point(200, 86)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVersion.Size = New System.Drawing.Size(321, 30)
        Me.lblVersion.TabIndex = 3
        Me.lblVersion.Text = "Version 1.00 Build 001"
        '
        'lblAdress
        '
        Me.lblAdress.BackColor = System.Drawing.SystemColors.Control
        Me.lblAdress.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAdress.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAdress.Location = New System.Drawing.Point(200, 241)
        Me.lblAdress.MinimumSize = New System.Drawing.Size(254, 36)
        Me.lblAdress.Name = "lblAdress"
        Me.lblAdress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAdress.Size = New System.Drawing.Size(254, 36)
        Me.lblAdress.TabIndex = 2
        Me.lblAdress.Text = "Visual Banking AS, Fredensborgveien 24D,                                         " & _
            " 0177 Oslo, Norway"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(272, 27)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(105, 33)
        Me.lblHeading.TabIndex = 0
        Me.lblHeading.Text = "BabelBank"
        '
        'txtDatabasePath
        '
        Me.txtDatabasePath.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDatabasePath.Location = New System.Drawing.Point(269, 150)
        Me.txtDatabasePath.Multiline = True
        Me.txtDatabasePath.Name = "txtDatabasePath"
        Me.txtDatabasePath.ReadOnly = True
        Me.txtDatabasePath.Size = New System.Drawing.Size(345, 30)
        Me.txtDatabasePath.TabIndex = 6
        Me.txtDatabasePath.Text = "txtDatabasePath"
        '
        'lblDatabasePath
        '
        Me.lblDatabasePath.BackColor = System.Drawing.SystemColors.Control
        Me.lblDatabasePath.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDatabasePath.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDatabasePath.Location = New System.Drawing.Point(200, 150)
        Me.lblDatabasePath.Name = "lblDatabasePath"
        Me.lblDatabasePath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDatabasePath.Size = New System.Drawing.Size(65, 23)
        Me.lblDatabasePath.TabIndex = 7
        Me.lblDatabasePath.Text = "Database:"
        '
        'lblLicensePath
        '
        Me.lblLicensePath.BackColor = System.Drawing.SystemColors.Control
        Me.lblLicensePath.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLicensePath.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLicensePath.Location = New System.Drawing.Point(200, 182)
        Me.lblLicensePath.Name = "lblLicensePath"
        Me.lblLicensePath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLicensePath.Size = New System.Drawing.Size(65, 23)
        Me.lblLicensePath.TabIndex = 8
        Me.lblLicensePath.Text = "License:"
        '
        'txtLicensePath
        '
        Me.txtLicensePath.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtLicensePath.Location = New System.Drawing.Point(269, 181)
        Me.txtLicensePath.Multiline = True
        Me.txtLicensePath.Name = "txtLicensePath"
        Me.txtLicensePath.ReadOnly = True
        Me.txtLicensePath.Size = New System.Drawing.Size(345, 30)
        Me.txtLicensePath.TabIndex = 9
        Me.txtLicensePath.Text = "txtLicensePath"
        '
        'frmTeamViewer
        '
        Me.frmTeamViewer.Location = New System.Drawing.Point(530, 228)
        Me.frmTeamViewer.Name = "frmTeamViewer"
        Me.frmTeamViewer.Size = New System.Drawing.Size(77, 25)
        Me.frmTeamViewer.TabIndex = 10
        Me.frmTeamViewer.Text = "TeamViewer"
        Me.frmTeamViewer.UseVisualStyleBackColor = True
        '
        'cmdAccess2016
        '
        Me.cmdAccess2016.Location = New System.Drawing.Point(528, 195)
        Me.cmdAccess2016.Name = "cmdAccess2016"
        Me.cmdAccess2016.Size = New System.Drawing.Size(77, 25)
        Me.cmdAccess2016.TabIndex = 11
        Me.cmdAccess2016.Text = "Access2016"
        Me.cmdAccess2016.UseVisualStyleBackColor = True
        '
        'frmAbout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(627, 298)
        Me.Controls.Add(Me.cmdAccess2016)
        Me.Controls.Add(Me.frmTeamViewer)
        Me.Controls.Add(Me.txtLicensePath)
        Me.Controls.Add(Me.lblLicensePath)
        Me.Controls.Add(Me.lblDatabasePath)
        Me.Controls.Add(Me.txtDatabasePath)
        Me.Controls.Add(Me.picVB)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lblAppPath)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.lblAdress)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmAbout"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "About BabelBank"
        CType(Me.picVB, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtDatabasePath As System.Windows.Forms.TextBox
    Public WithEvents lblDatabasePath As System.Windows.Forms.Label
    Public WithEvents lblLicensePath As System.Windows.Forms.Label
    Friend WithEvents txtLicensePath As System.Windows.Forms.TextBox
    Friend WithEvents frmTeamViewer As System.Windows.Forms.Button
    Friend WithEvents cmdAccess2016 As System.Windows.Forms.Button
#End Region 
End Class
