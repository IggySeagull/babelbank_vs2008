<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmStart
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents CompressDB As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Separator As System.Windows.Forms.ToolStripSeparator
	Public WithEvents Exit_Renamed As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents File As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Convert As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents RenameFiles As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Separator65 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents Action As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents View As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents BackupFiles As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents ViewMenu As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuImportFiles As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents AutomaticMatching As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents ManualMatching As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents PrintOmittedPayments As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents ExportPayments As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Seperator71 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents MatchingSQL As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MatchingClients As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MatchingManualSetup As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents ArchiveSetup As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Separator72 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents CleanUpDatabase As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MatchUnlock As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MatchStatistics As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Separator73 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents SANTANDER_Findaccount As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents SI_PeriodicExport As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents NAV_MergeFiles As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Eurosoft_ClientImport As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents NordeaLiv_Balance As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSpecial As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Matching As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents SetupClients As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents SetupNewProfile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents SetupChangeProfile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents SetupDeleteProfile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents SetupCompanyInfo As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents SetupSequence As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents SetupClientSplit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents SetupLicenseCode As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents SetupLicense As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents SetupShortcut As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Divide As System.Windows.Forms.ToolStripSeparator
    Public WithEvents SetupDocumentation As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents Setup As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _mnuDnBNORTBIArray_0 As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuDnBNORTBI As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents HelpContents As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Separator2 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents HelpSupportmail As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents HelpSupportPhone As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Separator3 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents HelpAbout As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuDEBUG As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Help As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
	Public WithEvents cmdNewFIProfile As System.Windows.Forms.Button
	Public WithEvents cmdRetrieveData As System.Windows.Forms.Button
    Public WithEvents cmdClients As System.Windows.Forms.Button
    Public WithEvents lstSendProfiles As System.Windows.Forms.ListBox
    Public WithEvents lstReturnprofiles As System.Windows.Forms.ListBox
    Public WithEvents cmdNewProfile As System.Windows.Forms.Button
    Public WithEvents cmdChangeProfile As System.Windows.Forms.Button
    Public WithEvents cmdDeleteProfile As System.Windows.Forms.Button
    Public WithEvents cmdRun As System.Windows.Forms.Button
    Public dlgFileNameOpen As System.Windows.Forms.OpenFileDialog
    Public dlgFileNameSave As System.Windows.Forms.SaveFileDialog
    Public dlgFileNameFont As System.Windows.Forms.FontDialog
    Public dlgFileNameColor As System.Windows.Forms.ColorDialog
    Public dlgFileNamePrint As System.Windows.Forms.PrintDialog
    Public WithEvents Toolbar_SelectDatabase As System.Windows.Forms.ToolStripButton
    Public WithEvents _Toolbar1_Button2 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents Toolbar_Convert As System.Windows.Forms.ToolStripButton
    Public WithEvents _Toolbar1_Button4 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents Toolbar_ViewDetails As System.Windows.Forms.ToolStripButton
    Public WithEvents Toolbar_Clients As System.Windows.Forms.ToolStripButton
    Public WithEvents _Toolbar1_Button10 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents Toolbar_SupportMail As System.Windows.Forms.ToolStripButton
    Public WithEvents Toolbar_Phone As System.Windows.Forms.ToolStripButton
    Public WithEvents _Toolbar1_Button13 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents Toolbar_Exit As System.Windows.Forms.ToolStripButton
    Public WithEvents Toolbar1 As System.Windows.Forms.ToolStrip
    Public WithEvents imlToolbarIcons As System.Windows.Forms.ImageList
    Public WithEvents lblBabelbank As System.Windows.Forms.Label
    Public WithEvents lblProfilRetur As System.Windows.Forms.Label
    Public WithEvents lblProfilInnsending As System.Windows.Forms.Label
    Public WithEvents mnuDnBNORTBIArray As Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray
    Public WithEvents mnuSetupCustomArray As Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStart))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MenuStrip
        Me.File = New System.Windows.Forms.ToolStripMenuItem
        Me.SelectDatabase = New System.Windows.Forms.ToolStripMenuItem
        Me.CompressDB = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCopyDatabase = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRestoreDatabase = New System.Windows.Forms.ToolStripMenuItem
        Me.Separator = New System.Windows.Forms.ToolStripSeparator
        Me.CreateSQLServerDB = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.Exit_Renamed = New System.Windows.Forms.ToolStripMenuItem
        Me.Action = New System.Windows.Forms.ToolStripMenuItem
        Me.Convert = New System.Windows.Forms.ToolStripMenuItem
        Me.RenameFiles = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDecrypt = New System.Windows.Forms.ToolStripMenuItem
        Me.Separator65 = New System.Windows.Forms.ToolStripSeparator
        Me.RunSQL = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExtractSecureEnvelope = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAnonymizeISO20022 = New System.Windows.Forms.ToolStripMenuItem
        Me.ViewMenu = New System.Windows.Forms.ToolStripMenuItem
        Me.View = New System.Windows.Forms.ToolStripMenuItem
        Me.BackupFiles = New System.Windows.Forms.ToolStripMenuItem
        Me.Matching = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportFiles = New System.Windows.Forms.ToolStripMenuItem
        Me.AutomaticMatching = New System.Windows.Forms.ToolStripMenuItem
        Me.ManualMatching = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintOmittedPayments = New System.Windows.Forms.ToolStripMenuItem
        Me.ExportPayments = New System.Windows.Forms.ToolStripMenuItem
        Me.Seperator71 = New System.Windows.Forms.ToolStripSeparator
        Me.MatchingSQL = New System.Windows.Forms.ToolStripMenuItem
        Me.MatchingClients = New System.Windows.Forms.ToolStripMenuItem
        Me.MatchingManualSetup = New System.Windows.Forms.ToolStripMenuItem
        Me.ArchiveSetup = New System.Windows.Forms.ToolStripMenuItem
        Me.Separator72 = New System.Windows.Forms.ToolStripSeparator
        Me.CleanUpDatabase = New System.Windows.Forms.ToolStripMenuItem
        Me.MatchUnlock = New System.Windows.Forms.ToolStripMenuItem
        Me.MatchStatistics = New System.Windows.Forms.ToolStripMenuItem
        Me.Separator73 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuSpecial = New System.Windows.Forms.ToolStripMenuItem
        Me.SANTANDER_Findaccount = New System.Windows.Forms.ToolStripMenuItem
        Me.SI_PeriodicExport = New System.Windows.Forms.ToolStripMenuItem
        Me.NAV_MergeFiles = New System.Windows.Forms.ToolStripMenuItem
        Me.Eurosoft_ClientImport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuLowellMasterAccount = New System.Windows.Forms.ToolStripMenuItem
        Me.OsloSeafood_ImportOpenItems = New System.Windows.Forms.ToolStripMenuItem
        Me.NordeaLiv_Balance = New System.Windows.Forms.ToolStripMenuItem
        Me.NAV_SplitT14Files = New System.Windows.Forms.ToolStripMenuItem
        Me.SGFinansSafeModeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Setup = New System.Windows.Forms.ToolStripMenuItem
        Me.SetupClients = New System.Windows.Forms.ToolStripMenuItem
        Me.SetupNewProfile = New System.Windows.Forms.ToolStripMenuItem
        Me.SetupChangeProfile = New System.Windows.Forms.ToolStripMenuItem
        Me.SetupDeleteProfile = New System.Windows.Forms.ToolStripMenuItem
        Me.SetupCompanyInfo = New System.Windows.Forms.ToolStripMenuItem
        Me.SetupSequence = New System.Windows.Forms.ToolStripMenuItem
        Me.SetupClientSplit = New System.Windows.Forms.ToolStripMenuItem
        Me.SetupLicenseCode = New System.Windows.Forms.ToolStripMenuItem
        Me.SetupLicense = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.NorskToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EnglishToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SvenskaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DanskToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SetupShortcut = New System.Windows.Forms.ToolStripMenuItem
        Me.Divide = New System.Windows.Forms.ToolStripSeparator
        Me.SetupDocumentation = New System.Windows.Forms.ToolStripMenuItem
        Me._mnuSetupCustomArray_0 = New System.Windows.Forms.ToolStripMenuItem
        Me.Help = New System.Windows.Forms.ToolStripMenuItem
        Me.HelpContents = New System.Windows.Forms.ToolStripMenuItem
        Me.Separator2 = New System.Windows.Forms.ToolStripSeparator
        Me.HelpSupportmail = New System.Windows.Forms.ToolStripMenuItem
        Me.HelpSupportPhone = New System.Windows.Forms.ToolStripMenuItem
        Me.Separator3 = New System.Windows.Forms.ToolStripSeparator
        Me.HelpAbout = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDEBUG = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDnBNORTBI = New System.Windows.Forms.ToolStripMenuItem
        Me._mnuDnBNORTBIArray_0 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSupportTool = New System.Windows.Forms.ToolStripMenuItem
        Me._mnuSupportFIleAnalyzis = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSEBISO20022 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSEBISO20022_MiscSetup = New System.Windows.Forms.ToolStripMenuItem
        Me.cmdNewFIProfile = New System.Windows.Forms.Button
        Me.cmdRetrieveData = New System.Windows.Forms.Button
        Me.cmdClients = New System.Windows.Forms.Button
        Me.lstSendProfiles = New System.Windows.Forms.ListBox
        Me.lstReturnprofiles = New System.Windows.Forms.ListBox
        Me.cmdNewProfile = New System.Windows.Forms.Button
        Me.cmdChangeProfile = New System.Windows.Forms.Button
        Me.cmdDeleteProfile = New System.Windows.Forms.Button
        Me.cmdRun = New System.Windows.Forms.Button
        Me.dlgFileNameOpen = New System.Windows.Forms.OpenFileDialog
        Me.dlgFileNameSave = New System.Windows.Forms.SaveFileDialog
        Me.dlgFileNameFont = New System.Windows.Forms.FontDialog
        Me.dlgFileNameColor = New System.Windows.Forms.ColorDialog
        Me.dlgFileNamePrint = New System.Windows.Forms.PrintDialog
        Me.Toolbar1 = New System.Windows.Forms.ToolStrip
        Me.imlToolbarIcons = New System.Windows.Forms.ImageList(Me.components)
        Me.Toolbar_SelectDatabase = New System.Windows.Forms.ToolStripButton
        Me._Toolbar1_Button2 = New System.Windows.Forms.ToolStripSeparator
        Me.Toolbar_ViewDetails = New System.Windows.Forms.ToolStripButton
        Me.Toolbar_Convert = New System.Windows.Forms.ToolStripButton
        Me._Toolbar1_Button4 = New System.Windows.Forms.ToolStripSeparator
        Me.Toolbar_BackupFiles = New System.Windows.Forms.ToolStripButton
        Me.Toolbar_Clients = New System.Windows.Forms.ToolStripButton
        Me._Toolbar1_Button10 = New System.Windows.Forms.ToolStripSeparator
        Me.Toolbar_SupportMail = New System.Windows.Forms.ToolStripButton
        Me.Toolbar_Phone = New System.Windows.Forms.ToolStripButton
        Me._Toolbar1_Button13 = New System.Windows.Forms.ToolStripSeparator
        Me.Toolbar_Exit = New System.Windows.Forms.ToolStripButton
        Me.Toolbar_SupportTools = New System.Windows.Forms.ToolStripButton
        Me.lblBabelbank = New System.Windows.Forms.Label
        Me.lblProfilRetur = New System.Windows.Forms.Label
        Me.lblProfilInnsending = New System.Windows.Forms.Label
        Me.mnuDnBNORTBIArray = New Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray(Me.components)
        Me.mnuSetupCustomArray = New Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray(Me.components)
        Me.lblPro = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.MainMenu1.SuspendLayout()
        Me.Toolbar1.SuspendLayout()
        CType(Me.mnuDnBNORTBIArray, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mnuSetupCustomArray, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.File, Me.Action, Me.ViewMenu, Me.Matching, Me.Setup, Me.Help, Me.mnuDnBNORTBI, Me.mnuSupportTool, Me.mnuSEBISO20022})
        Me.MainMenu1.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu1.Name = "MainMenu1"
        Me.MainMenu1.Size = New System.Drawing.Size(665, 24)
        Me.MainMenu1.TabIndex = 18
        '
        'File
        '
        Me.File.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SelectDatabase, Me.CompressDB, Me.mnuCopyDatabase, Me.mnuRestoreDatabase, Me.Separator, Me.CreateSQLServerDB, Me.ToolStripSeparator1, Me.Exit_Renamed})
        Me.File.Name = "File"
        Me.File.Size = New System.Drawing.Size(49, 20)
        Me.File.Text = "50100"
        '
        'SelectDatabase
        '
        Me.SelectDatabase.Name = "SelectDatabase"
        Me.SelectDatabase.Size = New System.Drawing.Size(104, 22)
        Me.SelectDatabase.Text = "50102"
        '
        'CompressDB
        '
        Me.CompressDB.Name = "CompressDB"
        Me.CompressDB.Size = New System.Drawing.Size(104, 22)
        Me.CompressDB.Text = "50101"
        '
        'mnuCopyDatabase
        '
        Me.mnuCopyDatabase.Name = "mnuCopyDatabase"
        Me.mnuCopyDatabase.Size = New System.Drawing.Size(104, 22)
        Me.mnuCopyDatabase.Text = "50104"
        '
        'mnuRestoreDatabase
        '
        Me.mnuRestoreDatabase.Name = "mnuRestoreDatabase"
        Me.mnuRestoreDatabase.Size = New System.Drawing.Size(104, 22)
        Me.mnuRestoreDatabase.Text = "50103"
        '
        'Separator
        '
        Me.Separator.Name = "Separator"
        Me.Separator.Size = New System.Drawing.Size(101, 6)
        '
        'CreateSQLServerDB
        '
        Me.CreateSQLServerDB.Name = "CreateSQLServerDB"
        Me.CreateSQLServerDB.Size = New System.Drawing.Size(104, 22)
        Me.CreateSQLServerDB.Text = "50113"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(101, 6)
        '
        'Exit_Renamed
        '
        Me.Exit_Renamed.Name = "Exit_Renamed"
        Me.Exit_Renamed.Size = New System.Drawing.Size(104, 22)
        Me.Exit_Renamed.Text = "50110"
        '
        'Action
        '
        Me.Action.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Convert, Me.RenameFiles, Me.mnuDecrypt, Me.Separator65, Me.RunSQL, Me.mnuExtractSecureEnvelope, Me.mnuAnonymizeISO20022})
        Me.Action.Name = "Action"
        Me.Action.Size = New System.Drawing.Size(49, 20)
        Me.Action.Text = "50400"
        '
        'Convert
        '
        Me.Convert.Name = "Convert"
        Me.Convert.Size = New System.Drawing.Size(152, 22)
        Me.Convert.Text = "50402"
        '
        'RenameFiles
        '
        Me.RenameFiles.Name = "RenameFiles"
        Me.RenameFiles.Size = New System.Drawing.Size(152, 22)
        Me.RenameFiles.Text = "50404"
        '
        'mnuDecrypt
        '
        Me.mnuDecrypt.Name = "mnuDecrypt"
        Me.mnuDecrypt.Size = New System.Drawing.Size(152, 22)
        Me.mnuDecrypt.Text = "50407"
        '
        'Separator65
        '
        Me.Separator65.Name = "Separator65"
        Me.Separator65.Size = New System.Drawing.Size(149, 6)
        '
        'RunSQL
        '
        Me.RunSQL.Name = "RunSQL"
        Me.RunSQL.Size = New System.Drawing.Size(152, 22)
        Me.RunSQL.Text = "50406"
        '
        'mnuExtractSecureEnvelope
        '
        Me.mnuExtractSecureEnvelope.Name = "mnuExtractSecureEnvelope"
        Me.mnuExtractSecureEnvelope.Size = New System.Drawing.Size(152, 22)
        Me.mnuExtractSecureEnvelope.Text = "50408"
        '
        'mnuAnonymizeISO20022
        '
        Me.mnuAnonymizeISO20022.Name = "mnuAnonymizeISO20022"
        Me.mnuAnonymizeISO20022.Size = New System.Drawing.Size(152, 22)
        Me.mnuAnonymizeISO20022.Text = "50409"
        '
        'ViewMenu
        '
        Me.ViewMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.View, Me.BackupFiles})
        Me.ViewMenu.Name = "ViewMenu"
        Me.ViewMenu.Size = New System.Drawing.Size(49, 20)
        Me.ViewMenu.Text = "50500"
        '
        'View
        '
        Me.View.Name = "View"
        Me.View.Size = New System.Drawing.Size(104, 22)
        Me.View.Text = "50501"
        '
        'BackupFiles
        '
        Me.BackupFiles.Name = "BackupFiles"
        Me.BackupFiles.Size = New System.Drawing.Size(104, 22)
        Me.BackupFiles.Text = "50502"
        '
        'Matching
        '
        Me.Matching.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuImportFiles, Me.AutomaticMatching, Me.ManualMatching, Me.PrintOmittedPayments, Me.ExportPayments, Me.Seperator71, Me.MatchingSQL, Me.MatchingClients, Me.MatchingManualSetup, Me.ArchiveSetup, Me.Separator72, Me.CleanUpDatabase, Me.MatchUnlock, Me.MatchStatistics, Me.Separator73, Me.mnuSpecial})
        Me.Matching.Name = "Matching"
        Me.Matching.Size = New System.Drawing.Size(49, 20)
        Me.Matching.Text = "50700"
        '
        'mnuImportFiles
        '
        Me.mnuImportFiles.Name = "mnuImportFiles"
        Me.mnuImportFiles.Size = New System.Drawing.Size(104, 22)
        Me.mnuImportFiles.Text = "50709"
        '
        'AutomaticMatching
        '
        Me.AutomaticMatching.Name = "AutomaticMatching"
        Me.AutomaticMatching.Size = New System.Drawing.Size(104, 22)
        Me.AutomaticMatching.Text = "50708"
        '
        'ManualMatching
        '
        Me.ManualMatching.Name = "ManualMatching"
        Me.ManualMatching.Size = New System.Drawing.Size(104, 22)
        Me.ManualMatching.Text = "50705"
        '
        'PrintOmittedPayments
        '
        Me.PrintOmittedPayments.Name = "PrintOmittedPayments"
        Me.PrintOmittedPayments.Size = New System.Drawing.Size(104, 22)
        Me.PrintOmittedPayments.Text = "50710"
        '
        'ExportPayments
        '
        Me.ExportPayments.Name = "ExportPayments"
        Me.ExportPayments.Size = New System.Drawing.Size(104, 22)
        Me.ExportPayments.Text = "50706"
        '
        'Seperator71
        '
        Me.Seperator71.Name = "Seperator71"
        Me.Seperator71.Size = New System.Drawing.Size(101, 6)
        '
        'MatchingSQL
        '
        Me.MatchingSQL.Name = "MatchingSQL"
        Me.MatchingSQL.Size = New System.Drawing.Size(104, 22)
        Me.MatchingSQL.Text = "50701"
        '
        'MatchingClients
        '
        Me.MatchingClients.Name = "MatchingClients"
        Me.MatchingClients.Size = New System.Drawing.Size(104, 22)
        Me.MatchingClients.Text = "50801"
        '
        'MatchingManualSetup
        '
        Me.MatchingManualSetup.Name = "MatchingManualSetup"
        Me.MatchingManualSetup.Size = New System.Drawing.Size(104, 22)
        Me.MatchingManualSetup.Text = "50703"
        '
        'ArchiveSetup
        '
        Me.ArchiveSetup.Name = "ArchiveSetup"
        Me.ArchiveSetup.Size = New System.Drawing.Size(104, 22)
        Me.ArchiveSetup.Text = "50711"
        '
        'Separator72
        '
        Me.Separator72.Name = "Separator72"
        Me.Separator72.Size = New System.Drawing.Size(101, 6)
        '
        'CleanUpDatabase
        '
        Me.CleanUpDatabase.Name = "CleanUpDatabase"
        Me.CleanUpDatabase.Size = New System.Drawing.Size(104, 22)
        Me.CleanUpDatabase.Text = "50704"
        '
        'MatchUnlock
        '
        Me.MatchUnlock.Name = "MatchUnlock"
        Me.MatchUnlock.Size = New System.Drawing.Size(104, 22)
        Me.MatchUnlock.Text = "50707"
        '
        'MatchStatistics
        '
        Me.MatchStatistics.Name = "MatchStatistics"
        Me.MatchStatistics.Size = New System.Drawing.Size(104, 22)
        Me.MatchStatistics.Text = "50713"
        '
        'Separator73
        '
        Me.Separator73.Name = "Separator73"
        Me.Separator73.Size = New System.Drawing.Size(101, 6)
        '
        'mnuSpecial
        '
        Me.mnuSpecial.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SANTANDER_Findaccount, Me.SI_PeriodicExport, Me.NAV_MergeFiles, Me.Eurosoft_ClientImport, Me.mnuLowellMasterAccount, Me.OsloSeafood_ImportOpenItems, Me.NordeaLiv_Balance, Me.NAV_SplitT14Files, Me.SGFinansSafeModeToolStripMenuItem})
        Me.mnuSpecial.Name = "mnuSpecial"
        Me.mnuSpecial.Size = New System.Drawing.Size(104, 22)
        Me.mnuSpecial.Text = "50712"
        '
        'SANTANDER_Findaccount
        '
        Me.SANTANDER_Findaccount.Name = "SANTANDER_Findaccount"
        Me.SANTANDER_Findaccount.Size = New System.Drawing.Size(271, 22)
        Me.SANTANDER_Findaccount.Text = "Santander: S�k etter kontonummer"
        '
        'SI_PeriodicExport
        '
        Me.SI_PeriodicExport.Name = "SI_PeriodicExport"
        Me.SI_PeriodicExport.Size = New System.Drawing.Size(271, 22)
        Me.SI_PeriodicExport.Text = "SI: Periode eksport"
        '
        'NAV_MergeFiles
        '
        Me.NAV_MergeFiles.Name = "NAV_MergeFiles"
        Me.NAV_MergeFiles.Size = New System.Drawing.Size(271, 22)
        Me.NAV_MergeFiles.Text = "NAV: Sl� sammen filer"
        '
        'Eurosoft_ClientImport
        '
        Me.Eurosoft_ClientImport.Name = "Eurosoft_ClientImport"
        Me.Eurosoft_ClientImport.Size = New System.Drawing.Size(271, 22)
        Me.Eurosoft_ClientImport.Text = "Eurosoft: Import av klientinformasjon"
        '
        'mnuLowellMasterAccount
        '
        Me.mnuLowellMasterAccount.Name = "mnuLowellMasterAccount"
        Me.mnuLowellMasterAccount.Size = New System.Drawing.Size(271, 22)
        Me.mnuLowellMasterAccount.Text = "Lowell Master Accounts table"
        '
        'OsloSeafood_ImportOpenItems
        '
        Me.OsloSeafood_ImportOpenItems.Name = "OsloSeafood_ImportOpenItems"
        Me.OsloSeafood_ImportOpenItems.Size = New System.Drawing.Size(271, 22)
        Me.OsloSeafood_ImportOpenItems.Text = "Oslo Seafood: Importer �pne poster"
        '
        'NordeaLiv_Balance
        '
        Me.NordeaLiv_Balance.Name = "NordeaLiv_Balance"
        Me.NordeaLiv_Balance.Size = New System.Drawing.Size(271, 22)
        Me.NordeaLiv_Balance.Text = "Nordea Liv: Saldoforesp�rsel"
        '
        'NAV_SplitT14Files
        '
        Me.NAV_SplitT14Files.Name = "NAV_SplitT14Files"
        Me.NAV_SplitT14Files.Size = New System.Drawing.Size(271, 22)
        Me.NAV_SplitT14Files.Text = "NAV: Splitt T14 filer"
        '
        'SGFinansSafeModeToolStripMenuItem
        '
        Me.SGFinansSafeModeToolStripMenuItem.Name = "SGFinansSafeModeToolStripMenuItem"
        Me.SGFinansSafeModeToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.SGFinansSafeModeToolStripMenuItem.Text = "Nordea Finans: Test KAR"
        '
        'Setup
        '
        Me.Setup.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SetupClients, Me.SetupNewProfile, Me.SetupChangeProfile, Me.SetupDeleteProfile, Me.SetupCompanyInfo, Me.SetupSequence, Me.SetupClientSplit, Me.SetupLicenseCode, Me.SetupLicense, Me.ToolStripMenuItem1, Me.SetupShortcut, Me.Divide, Me.SetupDocumentation, Me._mnuSetupCustomArray_0})
        Me.Setup.Name = "Setup"
        Me.Setup.Size = New System.Drawing.Size(49, 20)
        Me.Setup.Text = "50800"
        '
        'SetupClients
        '
        Me.SetupClients.Name = "SetupClients"
        Me.SetupClients.Size = New System.Drawing.Size(104, 22)
        Me.SetupClients.Text = "50801"
        '
        'SetupNewProfile
        '
        Me.SetupNewProfile.Name = "SetupNewProfile"
        Me.SetupNewProfile.Size = New System.Drawing.Size(104, 22)
        Me.SetupNewProfile.Text = "50802"
        '
        'SetupChangeProfile
        '
        Me.SetupChangeProfile.Name = "SetupChangeProfile"
        Me.SetupChangeProfile.Size = New System.Drawing.Size(104, 22)
        Me.SetupChangeProfile.Text = "50803"
        '
        'SetupDeleteProfile
        '
        Me.SetupDeleteProfile.Name = "SetupDeleteProfile"
        Me.SetupDeleteProfile.Size = New System.Drawing.Size(104, 22)
        Me.SetupDeleteProfile.Text = "50804"
        '
        'SetupCompanyInfo
        '
        Me.SetupCompanyInfo.Name = "SetupCompanyInfo"
        Me.SetupCompanyInfo.Size = New System.Drawing.Size(104, 22)
        Me.SetupCompanyInfo.Text = "50805"
        '
        'SetupSequence
        '
        Me.SetupSequence.Name = "SetupSequence"
        Me.SetupSequence.Size = New System.Drawing.Size(104, 22)
        Me.SetupSequence.Text = "50806"
        '
        'SetupClientSplit
        '
        Me.SetupClientSplit.Name = "SetupClientSplit"
        Me.SetupClientSplit.Size = New System.Drawing.Size(104, 22)
        Me.SetupClientSplit.Text = "50807"
        '
        'SetupLicenseCode
        '
        Me.SetupLicenseCode.Name = "SetupLicenseCode"
        Me.SetupLicenseCode.Size = New System.Drawing.Size(104, 22)
        Me.SetupLicenseCode.Text = "50813"
        '
        'SetupLicense
        '
        Me.SetupLicense.Name = "SetupLicense"
        Me.SetupLicense.Size = New System.Drawing.Size(104, 22)
        Me.SetupLicense.Text = "50808"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NorskToolStripMenuItem, Me.EnglishToolStripMenuItem, Me.SvenskaToolStripMenuItem, Me.DanskToolStripMenuItem})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(104, 22)
        Me.ToolStripMenuItem1.Text = "35120"
        '
        'NorskToolStripMenuItem
        '
        Me.NorskToolStripMenuItem.Name = "NorskToolStripMenuItem"
        Me.NorskToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.NorskToolStripMenuItem.Text = "Norsk"
        '
        'EnglishToolStripMenuItem
        '
        Me.EnglishToolStripMenuItem.Name = "EnglishToolStripMenuItem"
        Me.EnglishToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.EnglishToolStripMenuItem.Text = "English"
        '
        'SvenskaToolStripMenuItem
        '
        Me.SvenskaToolStripMenuItem.Name = "SvenskaToolStripMenuItem"
        Me.SvenskaToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.SvenskaToolStripMenuItem.Text = "Svenska"
        '
        'DanskToolStripMenuItem
        '
        Me.DanskToolStripMenuItem.Name = "DanskToolStripMenuItem"
        Me.DanskToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.DanskToolStripMenuItem.Text = "Dansk"
        '
        'SetupShortcut
        '
        Me.SetupShortcut.Name = "SetupShortcut"
        Me.SetupShortcut.Size = New System.Drawing.Size(104, 22)
        Me.SetupShortcut.Text = "50811"
        '
        'Divide
        '
        Me.Divide.Name = "Divide"
        Me.Divide.Size = New System.Drawing.Size(101, 6)
        '
        'SetupDocumentation
        '
        Me.SetupDocumentation.Name = "SetupDocumentation"
        Me.SetupDocumentation.Size = New System.Drawing.Size(104, 22)
        Me.SetupDocumentation.Text = "50814"
        '
        '_mnuSetupCustomArray_0
        '
        Me._mnuSetupCustomArray_0.Name = "_mnuSetupCustomArray_0"
        Me._mnuSetupCustomArray_0.Size = New System.Drawing.Size(104, 22)
        Me._mnuSetupCustomArray_0.Visible = False
        '
        'Help
        '
        Me.Help.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpContents, Me.Separator2, Me.HelpSupportmail, Me.HelpSupportPhone, Me.Separator3, Me.HelpAbout, Me.mnuDEBUG})
        Me.Help.Name = "Help"
        Me.Help.Size = New System.Drawing.Size(49, 20)
        Me.Help.Text = "50900"
        '
        'HelpContents
        '
        Me.HelpContents.Name = "HelpContents"
        Me.HelpContents.Size = New System.Drawing.Size(104, 22)
        Me.HelpContents.Text = "50901"
        '
        'Separator2
        '
        Me.Separator2.Name = "Separator2"
        Me.Separator2.Size = New System.Drawing.Size(101, 6)
        '
        'HelpSupportmail
        '
        Me.HelpSupportmail.Name = "HelpSupportmail"
        Me.HelpSupportmail.Size = New System.Drawing.Size(104, 22)
        Me.HelpSupportmail.Text = "50903"
        '
        'HelpSupportPhone
        '
        Me.HelpSupportPhone.Name = "HelpSupportPhone"
        Me.HelpSupportPhone.Size = New System.Drawing.Size(104, 22)
        Me.HelpSupportPhone.Text = "50904"
        '
        'Separator3
        '
        Me.Separator3.Name = "Separator3"
        Me.Separator3.Size = New System.Drawing.Size(101, 6)
        '
        'HelpAbout
        '
        Me.HelpAbout.Name = "HelpAbout"
        Me.HelpAbout.Size = New System.Drawing.Size(104, 22)
        Me.HelpAbout.Text = "50905"
        '
        'mnuDEBUG
        '
        Me.mnuDEBUG.Name = "mnuDEBUG"
        Me.mnuDEBUG.Size = New System.Drawing.Size(104, 22)
        Me.mnuDEBUG.Text = "50906"
        '
        'mnuDnBNORTBI
        '
        Me.mnuDnBNORTBI.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._mnuDnBNORTBIArray_0})
        Me.mnuDnBNORTBI.Name = "mnuDnBNORTBI"
        Me.mnuDnBNORTBI.Size = New System.Drawing.Size(82, 20)
        Me.mnuDnBNORTBI.Text = "DNB FI-Kort"
        Me.mnuDnBNORTBI.Visible = False
        '
        '_mnuDnBNORTBIArray_0
        '
        Me.mnuDnBNORTBIArray.SetIndex(Me._mnuDnBNORTBIArray_0, CType(0, Short))
        Me._mnuDnBNORTBIArray_0.Name = "_mnuDnBNORTBIArray_0"
        Me._mnuDnBNORTBIArray_0.Size = New System.Drawing.Size(155, 22)
        Me._mnuDnBNORTBIArray_0.Text = "(DnBFIProjects)"
        '
        'mnuSupportTool
        '
        Me.mnuSupportTool.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._mnuSupportFIleAnalyzis})
        Me.mnuSupportTool.Name = "mnuSupportTool"
        Me.mnuSupportTool.Size = New System.Drawing.Size(118, 20)
        Me.mnuSupportTool.Text = "50950-SupportTool"
        Me.mnuSupportTool.Visible = False
        '
        '_mnuSupportFIleAnalyzis
        '
        Me._mnuSupportFIleAnalyzis.Name = "_mnuSupportFIleAnalyzis"
        Me._mnuSupportFIleAnalyzis.Size = New System.Drawing.Size(168, 22)
        Me._mnuSupportFIleAnalyzis.Text = "50951-Fileanalyzis"
        Me._mnuSupportFIleAnalyzis.Visible = False
        '
        'mnuSEBISO20022
        '
        Me.mnuSEBISO20022.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSEBISO20022_MiscSetup})
        Me.mnuSEBISO20022.Name = "mnuSEBISO20022"
        Me.mnuSEBISO20022.Size = New System.Drawing.Size(89, 20)
        Me.mnuSEBISO20022.Text = "SEB ISO20022"
        Me.mnuSEBISO20022.Visible = False
        '
        'mnuSEBISO20022_MiscSetup
        '
        Me.mnuSEBISO20022_MiscSetup.Name = "mnuSEBISO20022_MiscSetup"
        Me.mnuSEBISO20022_MiscSetup.Size = New System.Drawing.Size(172, 22)
        Me.mnuSEBISO20022_MiscSetup.Text = "51202 -Misc. setup"
        Me.mnuSEBISO20022_MiscSetup.Visible = False
        '
        'cmdNewFIProfile
        '
        Me.cmdNewFIProfile.BackColor = System.Drawing.SystemColors.Control
        Me.cmdNewFIProfile.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdNewFIProfile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNewFIProfile.Location = New System.Drawing.Point(155, 354)
        Me.cmdNewFIProfile.Name = "cmdNewFIProfile"
        Me.cmdNewFIProfile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdNewFIProfile.Size = New System.Drawing.Size(87, 25)
        Me.cmdNewFIProfile.TabIndex = 13
        Me.cmdNewFIProfile.Text = "55025-FIProfil"
        Me.cmdNewFIProfile.UseVisualStyleBackColor = False
        Me.cmdNewFIProfile.Visible = False
        '
        'cmdRetrieveData
        '
        Me.cmdRetrieveData.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRetrieveData.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRetrieveData.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRetrieveData.Location = New System.Drawing.Point(38, 62)
        Me.cmdRetrieveData.Name = "cmdRetrieveData"
        Me.cmdRetrieveData.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRetrieveData.Size = New System.Drawing.Size(95, 29)
        Me.cmdRetrieveData.TabIndex = 12
        Me.cmdRetrieveData.Text = "RetrieveData"
        Me.cmdRetrieveData.UseVisualStyleBackColor = False
        Me.cmdRetrieveData.Visible = False
        '
        'cmdClients
        '
        Me.cmdClients.BackColor = System.Drawing.SystemColors.Control
        Me.cmdClients.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClients.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClients.Location = New System.Drawing.Point(494, 354)
        Me.cmdClients.Name = "cmdClients"
        Me.cmdClients.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClients.Size = New System.Drawing.Size(80, 25)
        Me.cmdClients.TabIndex = 11
        Me.cmdClients.Text = "55004"
        Me.cmdClients.UseVisualStyleBackColor = False
        '
        'lstSendProfiles
        '
        Me.lstSendProfiles.BackColor = System.Drawing.SystemColors.Window
        Me.lstSendProfiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstSendProfiles.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstSendProfiles.Location = New System.Drawing.Point(258, 114)
        Me.lstSendProfiles.Name = "lstSendProfiles"
        Me.lstSendProfiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstSendProfiles.Size = New System.Drawing.Size(195, 225)
        Me.lstSendProfiles.TabIndex = 10
        '
        'lstReturnprofiles
        '
        Me.lstReturnprofiles.BackColor = System.Drawing.SystemColors.Window
        Me.lstReturnprofiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstReturnprofiles.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstReturnprofiles.Location = New System.Drawing.Point(460, 114)
        Me.lstReturnprofiles.Name = "lstReturnprofiles"
        Me.lstReturnprofiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstReturnprofiles.Size = New System.Drawing.Size(195, 225)
        Me.lstReturnprofiles.TabIndex = 9
        '
        'cmdNewProfile
        '
        Me.cmdNewProfile.BackColor = System.Drawing.SystemColors.Control
        Me.cmdNewProfile.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdNewProfile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNewProfile.Location = New System.Drawing.Point(243, 354)
        Me.cmdNewProfile.Name = "cmdNewProfile"
        Me.cmdNewProfile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdNewProfile.Size = New System.Drawing.Size(80, 25)
        Me.cmdNewProfile.TabIndex = 8
        Me.cmdNewProfile.Text = "55001"
        Me.cmdNewProfile.UseVisualStyleBackColor = False
        '
        'cmdChangeProfile
        '
        Me.cmdChangeProfile.BackColor = System.Drawing.SystemColors.Control
        Me.cmdChangeProfile.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdChangeProfile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdChangeProfile.Location = New System.Drawing.Point(324, 354)
        Me.cmdChangeProfile.Name = "cmdChangeProfile"
        Me.cmdChangeProfile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdChangeProfile.Size = New System.Drawing.Size(88, 25)
        Me.cmdChangeProfile.TabIndex = 7
        Me.cmdChangeProfile.Text = "55002"
        Me.cmdChangeProfile.UseVisualStyleBackColor = False
        '
        'cmdDeleteProfile
        '
        Me.cmdDeleteProfile.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDeleteProfile.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDeleteProfile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDeleteProfile.Location = New System.Drawing.Point(413, 354)
        Me.cmdDeleteProfile.Name = "cmdDeleteProfile"
        Me.cmdDeleteProfile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDeleteProfile.Size = New System.Drawing.Size(80, 25)
        Me.cmdDeleteProfile.TabIndex = 6
        Me.cmdDeleteProfile.Text = "55003"
        Me.cmdDeleteProfile.UseVisualStyleBackColor = False
        '
        'cmdRun
        '
        Me.cmdRun.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRun.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRun.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRun.Location = New System.Drawing.Point(575, 354)
        Me.cmdRun.Name = "cmdRun"
        Me.cmdRun.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRun.Size = New System.Drawing.Size(80, 25)
        Me.cmdRun.TabIndex = 5
        Me.cmdRun.Text = "55005"
        Me.cmdRun.UseVisualStyleBackColor = False
        '
        'Toolbar1
        '
        Me.Toolbar1.ImageList = Me.imlToolbarIcons
        Me.Toolbar1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.Toolbar1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Toolbar_SelectDatabase, Me._Toolbar1_Button2, Me.Toolbar_ViewDetails, Me.Toolbar_Convert, Me._Toolbar1_Button4, Me.Toolbar_BackupFiles, Me.Toolbar_Clients, Me._Toolbar1_Button10, Me.Toolbar_SupportMail, Me.Toolbar_Phone, Me._Toolbar1_Button13, Me.Toolbar_Exit, Me.Toolbar_SupportTools})
        Me.Toolbar1.Location = New System.Drawing.Point(0, 24)
        Me.Toolbar1.Name = "Toolbar1"
        Me.Toolbar1.Size = New System.Drawing.Size(665, 28)
        Me.Toolbar1.TabIndex = 1
        '
        'imlToolbarIcons
        '
        Me.imlToolbarIcons.ImageStream = CType(resources.GetObject("imlToolbarIcons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imlToolbarIcons.TransparentColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.imlToolbarIcons.Images.SetKeyName(0, "Open")
        Me.imlToolbarIcons.Images.SetKeyName(1, "Macro")
        Me.imlToolbarIcons.Images.SetKeyName(2, "Print")
        Me.imlToolbarIcons.Images.SetKeyName(3, "View Details")
        Me.imlToolbarIcons.Images.SetKeyName(4, "Doorexi1")
        Me.imlToolbarIcons.Images.SetKeyName(5, "Print1")
        Me.imlToolbarIcons.Images.SetKeyName(6, "Printdet")
        Me.imlToolbarIcons.Images.SetKeyName(7, "Printspl")
        Me.imlToolbarIcons.Images.SetKeyName(8, "Print2")
        Me.imlToolbarIcons.Images.SetKeyName(9, "Printdet1")
        Me.imlToolbarIcons.Images.SetKeyName(10, "Printspl1")
        Me.imlToolbarIcons.Images.SetKeyName(11, "Phone")
        Me.imlToolbarIcons.Images.SetKeyName(12, "Convert")
        Me.imlToolbarIcons.Images.SetKeyName(13, "Split")
        Me.imlToolbarIcons.Images.SetKeyName(14, "Merge")
        Me.imlToolbarIcons.Images.SetKeyName(15, "Help")
        Me.imlToolbarIcons.Images.SetKeyName(16, "Split2")
        Me.imlToolbarIcons.Images.SetKeyName(17, "Merge2")
        Me.imlToolbarIcons.Images.SetKeyName(18, "Clipmail")
        Me.imlToolbarIcons.Images.SetKeyName(19, "Mapif1l")
        Me.imlToolbarIcons.Images.SetKeyName(20, "CONVERT")
        Me.imlToolbarIcons.Images.SetKeyName(21, "PRINT")
        Me.imlToolbarIcons.Images.SetKeyName(22, "PRINTDET")
        Me.imlToolbarIcons.Images.SetKeyName(23, "PRINTSPL")
        Me.imlToolbarIcons.Images.SetKeyName(24, "MAPIF1L")
        Me.imlToolbarIcons.Images.SetKeyName(25, "PHONE")
        Me.imlToolbarIcons.Images.SetKeyName(26, "DOOREXI1")
        Me.imlToolbarIcons.Images.SetKeyName(27, "OPEN")
        Me.imlToolbarIcons.Images.SetKeyName(28, "PREVIEW")
        Me.imlToolbarIcons.Images.SetKeyName(29, "HELP")
        '
        'Toolbar_SelectDatabase
        '
        Me.Toolbar_SelectDatabase.AutoSize = False
        Me.Toolbar_SelectDatabase.Image = CType(resources.GetObject("Toolbar_SelectDatabase.Image"), System.Drawing.Image)
        Me.Toolbar_SelectDatabase.ImageTransparentColor = System.Drawing.Color.Silver
        Me.Toolbar_SelectDatabase.Name = "Toolbar_SelectDatabase"
        Me.Toolbar_SelectDatabase.Size = New System.Drawing.Size(25, 25)
        Me.Toolbar_SelectDatabase.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Toolbar_SelectDatabase.ToolTipText = "50102"
        '
        '_Toolbar1_Button2
        '
        Me._Toolbar1_Button2.AutoSize = False
        Me._Toolbar1_Button2.Name = "_Toolbar1_Button2"
        Me._Toolbar1_Button2.Size = New System.Drawing.Size(16, 16)
        '
        'Toolbar_ViewDetails
        '
        Me.Toolbar_ViewDetails.AutoSize = False
        Me.Toolbar_ViewDetails.Image = CType(resources.GetObject("Toolbar_ViewDetails.Image"), System.Drawing.Image)
        Me.Toolbar_ViewDetails.ImageTransparentColor = System.Drawing.Color.Silver
        Me.Toolbar_ViewDetails.Name = "Toolbar_ViewDetails"
        Me.Toolbar_ViewDetails.Size = New System.Drawing.Size(25, 25)
        Me.Toolbar_ViewDetails.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Toolbar_ViewDetails.ToolTipText = "62009"
        '
        'Toolbar_Convert
        '
        Me.Toolbar_Convert.AutoSize = False
        Me.Toolbar_Convert.Image = CType(resources.GetObject("Toolbar_Convert.Image"), System.Drawing.Image)
        Me.Toolbar_Convert.Name = "Toolbar_Convert"
        Me.Toolbar_Convert.Size = New System.Drawing.Size(25, 25)
        Me.Toolbar_Convert.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Toolbar_Convert.ToolTipText = "62004"
        '
        '_Toolbar1_Button4
        '
        Me._Toolbar1_Button4.AutoSize = False
        Me._Toolbar1_Button4.Name = "_Toolbar1_Button4"
        Me._Toolbar1_Button4.Size = New System.Drawing.Size(22, 22)
        '
        'Toolbar_BackupFiles
        '
        Me.Toolbar_BackupFiles.AutoSize = False
        Me.Toolbar_BackupFiles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Toolbar_BackupFiles.Image = CType(resources.GetObject("Toolbar_BackupFiles.Image"), System.Drawing.Image)
        Me.Toolbar_BackupFiles.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Toolbar_BackupFiles.Name = "Toolbar_BackupFiles"
        Me.Toolbar_BackupFiles.Size = New System.Drawing.Size(24, 25)
        Me.Toolbar_BackupFiles.ToolTipText = "50502"
        '
        'Toolbar_Clients
        '
        Me.Toolbar_Clients.AutoSize = False
        Me.Toolbar_Clients.Image = CType(resources.GetObject("Toolbar_Clients.Image"), System.Drawing.Image)
        Me.Toolbar_Clients.Name = "Toolbar_Clients"
        Me.Toolbar_Clients.Size = New System.Drawing.Size(25, 25)
        Me.Toolbar_Clients.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Toolbar_Clients.ToolTipText = "55004"
        '
        '_Toolbar1_Button10
        '
        Me._Toolbar1_Button10.AutoSize = False
        Me._Toolbar1_Button10.Name = "_Toolbar1_Button10"
        Me._Toolbar1_Button10.Size = New System.Drawing.Size(24, 22)
        '
        'Toolbar_SupportMail
        '
        Me.Toolbar_SupportMail.AutoSize = False
        Me.Toolbar_SupportMail.Image = CType(resources.GetObject("Toolbar_SupportMail.Image"), System.Drawing.Image)
        Me.Toolbar_SupportMail.ImageTransparentColor = System.Drawing.Color.Black
        Me.Toolbar_SupportMail.Name = "Toolbar_SupportMail"
        Me.Toolbar_SupportMail.Size = New System.Drawing.Size(25, 25)
        Me.Toolbar_SupportMail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Toolbar_SupportMail.ToolTipText = "62010"
        '
        'Toolbar_Phone
        '
        Me.Toolbar_Phone.AutoSize = False
        Me.Toolbar_Phone.Image = CType(resources.GetObject("Toolbar_Phone.Image"), System.Drawing.Image)
        Me.Toolbar_Phone.Name = "Toolbar_Phone"
        Me.Toolbar_Phone.Size = New System.Drawing.Size(25, 25)
        Me.Toolbar_Phone.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Toolbar_Phone.ToolTipText = "62011"
        '
        '_Toolbar1_Button13
        '
        Me._Toolbar1_Button13.AutoSize = False
        Me._Toolbar1_Button13.Name = "_Toolbar1_Button13"
        Me._Toolbar1_Button13.Size = New System.Drawing.Size(24, 22)
        '
        'Toolbar_Exit
        '
        Me.Toolbar_Exit.AutoSize = False
        Me.Toolbar_Exit.Image = CType(resources.GetObject("Toolbar_Exit.Image"), System.Drawing.Image)
        Me.Toolbar_Exit.Name = "Toolbar_Exit"
        Me.Toolbar_Exit.Size = New System.Drawing.Size(25, 25)
        Me.Toolbar_Exit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Toolbar_Exit.ToolTipText = "62002"
        '
        'Toolbar_SupportTools
        '
        Me.Toolbar_SupportTools.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Toolbar_SupportTools.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Toolbar_SupportTools.Name = "Toolbar_SupportTools"
        Me.Toolbar_SupportTools.Size = New System.Drawing.Size(23, 25)
        Me.Toolbar_SupportTools.Text = "ToolStripButton1"
        Me.Toolbar_SupportTools.ToolTipText = "50950"
        Me.Toolbar_SupportTools.Visible = False
        '
        'lblBabelbank
        '
        Me.lblBabelbank.BackColor = System.Drawing.SystemColors.Control
        Me.lblBabelbank.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBabelbank.Font = New System.Drawing.Font("Arial Narrow", 26.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBabelbank.ForeColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(21, Byte), Integer))
        Me.lblBabelbank.Location = New System.Drawing.Point(17, 234)
        Me.lblBabelbank.Name = "lblBabelbank"
        Me.lblBabelbank.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBabelbank.Size = New System.Drawing.Size(240, 45)
        Me.lblBabelbank.TabIndex = 0
        Me.lblBabelbank.Text = "BABELBANK"
        '
        'lblProfilRetur
        '
        Me.lblProfilRetur.BackColor = System.Drawing.SystemColors.Control
        Me.lblProfilRetur.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProfilRetur.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProfilRetur.Location = New System.Drawing.Point(460, 95)
        Me.lblProfilRetur.Name = "lblProfilRetur"
        Me.lblProfilRetur.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProfilRetur.Size = New System.Drawing.Size(200, 20)
        Me.lblProfilRetur.TabIndex = 4
        Me.lblProfilRetur.Text = "60002"
        '
        'lblProfilInnsending
        '
        Me.lblProfilInnsending.BackColor = System.Drawing.SystemColors.Control
        Me.lblProfilInnsending.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProfilInnsending.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProfilInnsending.Location = New System.Drawing.Point(257, 95)
        Me.lblProfilInnsending.Name = "lblProfilInnsending"
        Me.lblProfilInnsending.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProfilInnsending.Size = New System.Drawing.Size(195, 20)
        Me.lblProfilInnsending.TabIndex = 3
        Me.lblProfilInnsending.Text = "60001"
        '
        'mnuSetupCustomArray
        '
        '
        'lblPro
        '
        Me.lblPro.BackColor = System.Drawing.SystemColors.Control
        Me.lblPro.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPro.Font = New System.Drawing.Font("Arial Narrow", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPro.ForeColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(21, Byte), Integer))
        Me.lblPro.Location = New System.Drawing.Point(202, 220)
        Me.lblPro.Name = "lblPro"
        Me.lblPro.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPro.Size = New System.Drawing.Size(47, 20)
        Me.lblPro.TabIndex = 19
        Me.lblPro.Text = "Pro"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(19, 287)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(218, 67)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 20
        Me.PictureBox1.TabStop = False
        '
        'frmStart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(665, 394)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblPro)
        Me.Controls.Add(Me.cmdNewFIProfile)
        Me.Controls.Add(Me.cmdRetrieveData)
        Me.Controls.Add(Me.cmdClients)
        Me.Controls.Add(Me.lstSendProfiles)
        Me.Controls.Add(Me.lstReturnprofiles)
        Me.Controls.Add(Me.cmdNewProfile)
        Me.Controls.Add(Me.cmdChangeProfile)
        Me.Controls.Add(Me.cmdDeleteProfile)
        Me.Controls.Add(Me.cmdRun)
        Me.Controls.Add(Me.Toolbar1)
        Me.Controls.Add(Me.lblBabelbank)
        Me.Controls.Add(Me.lblProfilRetur)
        Me.Controls.Add(Me.lblProfilInnsending)
        Me.Controls.Add(Me.MainMenu1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(11, 37)
        Me.Name = "frmStart"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "BabelBank"
        Me.MainMenu1.ResumeLayout(False)
        Me.MainMenu1.PerformLayout()
        Me.Toolbar1.ResumeLayout(False)
        Me.Toolbar1.PerformLayout()
        CType(Me.mnuDnBNORTBIArray, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mnuSetupCustomArray, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents _mnuSetupCustomArray_0 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SelectDatabase As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents Toolbar_BackupFiles As System.Windows.Forms.ToolStripButton
    Public WithEvents mnuSupportTool As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _mnuSupportFIleAnalyzis As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Toolbar_SupportTools As System.Windows.Forms.ToolStripButton
    Public WithEvents lblPro As System.Windows.Forms.Label
    Friend WithEvents NAV_SplitT14Files As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSEBISO20022 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSEBISO20022_MiscSetup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRestoreDatabase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCopyDatabase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CreateSQLServerDB As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents RunSQL As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NorskToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EnglishToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SvenskaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DanskToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents SGFinansSafeModeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDecrypt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OsloSeafood_ImportOpenItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLowellMasterAccount As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExtractSecureEnvelope As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAnonymizeISO20022 As System.Windows.Forms.ToolStripMenuItem
#End Region
End Class