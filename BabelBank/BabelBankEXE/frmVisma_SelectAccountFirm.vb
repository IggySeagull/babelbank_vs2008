Option Strict Off
Option Explicit On

Friend Class frmVisma_SelectAccountFirm
	Inherits System.Windows.Forms.Form
	' XNET 21.06.2013 added frmVisma_SelectImportProfiles
	Private Declare Function DestroyCaret Lib "user32" () As Integer
	Private sFirmNo As String
	Private sFirmName As String
	Private sUID As String
	Private sPWD As String
	Private sVBSys As String
	Private sUser As String
	Private sInstance As String
	Private sAccountNo As String
	Private sProfileGroupID As String
	Private Const vbLightGray As Integer = &H808080
	Private Const vbGray As Integer = &HC0C0C0
	Private Sub frmVisma_SelectAccountFirm_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		'VB;/S;F9998;vbsys;system;sa;Visma123;
		'sUID = xDelim(Command, ";", 6, Chr(34))
		'sPWD = xDelim(Command, ";", 7, Chr(34))
		'sVBSys = xDelim(Command, ";", 4, Chr(34))
		'sUser = xDelim(Command, ";", 5, Chr(34))
		
		Dim oVismaCommandLine As New VismaCommandLine
		
		sUID = oVismaCommandLine.UID
		sPWD = oVismaCommandLine.PWD
		sVBSys = oVismaCommandLine.SystemDatabase
		sUser = oVismaCommandLine.User
		sInstance = oVismaCommandLine.Instance
        oVismaCommandLine = Nothing
		
		FormLRSCaptions(Me)
		FormVismaStyle(Me)


		SpreadHeaders()
		SpreadFill()
		
    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Dim i As Short

        Me.Hide()

        ' set sFirmNo to selected Firm from spread
        With Me.gridFirms
            sFirmName = .Rows(.CurrentRow.Index).Cells(0).Value
            sFirmNo = .Rows(.CurrentRow.Index).Cells(1).Value
        End With

        Me.DialogResult = Windows.Forms.DialogResult.OK
        ' Save connection to database
        If Not SaveClient() Then
            sFirmNo = "-1"
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        End If

    End Sub
    Private Function SaveClient() As Boolean
        Dim oVismaDal As New vbBabel.DAL
        Dim sErrorString As String
        Dim sMySQL As String
        Dim sBBDatabase As String
        Dim i As Short
        Dim iMaxClientID As Short
        Dim lRecordsAffected As Integer

        On Error GoTo Err_Renamed

        sBBDatabase = "BabelBank_" & sVBSys

        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        If Me.gridFirms.RowCount > -1 Then
            ' First, find out if there are any rows in bbClient;
            sMySQL = "SELECT *  FROM " & sBBDatabase & ".dbo.bbClient"
            '			rsVisma = CreateObject ("ADODB.Recordset")
            '			rsVisma.Open(sMySQL, oVismaCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)
            oVismaDal.SQL = sMySQL
            If oVismaDal.Reader_Execute() Then
                If oVismaDal.Reader_HasRows = False Then
                    ' finds no name, use databasename instead
                    iMaxClientID = 1
                Else
                    ' OK, there are lines, find highest ClientID
                    sMySQL = "SELECT Max(ClientID) AS MaxID FROM " & sBBDatabase & ".dbo.bbClient"
                    oVismaDal.SQL = sMySQL
                    If oVismaDal.Reader_Execute() Then
						If oVismaDal.Reader_HasRows = True Then
							Do While oVismaDal.Reader_ReadRecord
								iMaxClientID = CShort(oVismaDal.Reader_GetString("MaxID")) + 1
								Exit Do
							Loop
						Else
							iMaxClientID = 1
                        End If
                    End If
                End If
            End If

            ' Then, save this collection to bbClient-table;
            ' there is an autoincrement on ClientID, so no need to find the highest

            sMySQL = "INSERT INTO " & sBBDatabase & ".dbo.bbClient (ClientId, FrmNo, BPartNo, ClientName, AccountNo, ProfileGroupID, ChDt, ChTm, ChUsr) "
            sMySQL = sMySQL & " VALUES (" & Trim(Str(iMaxClientID)) & ", "
            sMySQL = sMySQL & "'" & sFirmNo & "', "
            ' 13.03.2017 Hvorfor alltid 0 p� bankpartner
            sMySQL = sMySQL & "0,"
            sMySQL = sMySQL & "'" & sFirmName & "', " ' Name
            sMySQL = sMySQL & "'" & Replace(Replace(Replace(sAccountNo, " ", ""), ".", ""), "-", "") & "', " ' AccountNo
            sMySQL = sMySQL & sProfileGroupID ' ProfileGroupID
            sMySQL = sMySQL & ", CONVERT(VARCHAR(8), SYSDATETIME(), 112), REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', ''), '" & sUser & "')"

            '			oVismaCon.BeginTrans()
            '			cmdUpdate = New ADODB.Command
            '			cmdUpdate.CommandText = sMySQL
            '			cmdUpdate.CommandType = ADODB.CommandTypeEnum.adCmdText
            '			cmdUpdate.let_ActiveConnection(oVismaCon)
            '			cmdUpdate.Execute(lRecordsAffected)
            oVismaDal.TEST_UseCommitFromOutside = True
            oVismaDal.TEST_BeginTrans()
            oVismaDal.SQL = sMySQL
            oVismaDal.ExecuteNonQuery()
            If oVismaDal.RecordsAffected <> 1 Then
                Err.Raise(35006, "Visma_SetAccountFirm, SaveClient", "There should be exactly one record affected when running this SQL: " & vbCrLf & sMySQL & vbCrLf & vbCrLf & "Visma Payment (BabelBank) found " & Trim(Str(lRecordsAffected)) & " records.")
            End If
            oVismaDal.TEST_CommitTrans()
            SaveClient = True
        Else
            ' user has cancelled
            SaveClient = False
        End If

        ' Clean up
        frmVisma_SelectProfile = Nothing
        oVismaDal.Close()
        oVismaDal = Nothing

        Exit Function

Err_Renamed:

        frmVisma_SelectProfile = Nothing

        If Not oVismaDal Is Nothing Then
            oVismaDal.Close()
            oVismaDal = Nothing
        End If

        If Not EmptyString(sMySQL) Then
            Err.Raise(Err.Number, "Visma_SetAccountFirm, SaveClient", Err.Description & vbCrLf & "SQL: " & sMySQL)
        Else
            Err.Raise(Err.Number, "Visma_SetAccountFirm, SaveClient", Err.Description)
        End If


    End Function
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        'LRS(35135)  '"Vil du avbryte, uten import?"
        If MsgBox(LRS(35135), MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Me.Hide()
            sFirmNo = "-1"
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        End If
    End Sub
	Private Sub SpreadHeaders()
        Dim txtColumn As DataGridViewTextBoxColumn

        With Me.gridFirms
            If .ColumnCount = 0 Then
                '.Height = 100

                .ScrollBars = ScrollBars.Both
                .SelectionMode = DataGridViewSelectionMode.CellSelect
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.FixedSingle
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .ReadOnly = False
            End If

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(12)
            txtColumn.HeaderText = LRS(60346)  ' Firmanavn
            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(10)
            txtColumn.HeaderText = LRS(35183)  '"Firmanummer
            .Columns.Add(txtColumn)

        End With

    End Sub
	Private Sub SpreadFill()
        ' Fill up from table array
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sErrorString As String
        Dim sMySQL As String
        Dim i As Short
        Dim myRow As DataGridViewRow


        On Error GoTo VismaFillError

        sErrorString = "ConnectToVismaBusinessDB"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        sErrorString = "Before SELECT"
        ' Find all companies from vbsys.
        sMySQL = "SELECT * FROM " & sVBSys & ".dbo.Frm ORDER BY FrmNo"
        oVismaDal.SQL = sMySQL

        If oVismaDal.Reader_Execute() Then
            If oVismaDal.Reader_HasRows = True Then

                sErrorString = "Fill spread"
                With Me.gridFirms
                    .Rows.Clear()
                    '			Do While Not rsVisma.EOF
                    Do While oVismaDal.Reader_ReadRecord

                        ' fill up
                        myRow = New DataGridViewRow
                        myRow.CreateCells(Me.gridFirms)

                        ' Companyname
                        myRow.Cells(0).Value = oVismaDal.Reader_GetString("Nm")
                        myRow.Cells(0).ReadOnly = True

                        ' Companynumber
                        myRow.Cells(1).Value = oVismaDal.Reader_GetString("FrmNo")
                        myRow.Cells(1).ReadOnly = True
                        .Rows.Add(myRow)
                    Loop
                End With

            End If  'If oVismaDal.Reader_HasRows = True Then

        End If  '        If oVismaDal.Reader_Execute() Then

        oVismaDal.Close()
        oVismaDal = Nothing

        Exit Sub
VismaFillError:
        oVismaDal.Close()
        oVismaDal = Nothing

        Err.Raise(30008, "frmVisma_SelectAccountFirmSpreadFill", sErrorString)


    End Sub
    'UPGRADE_NOTE: DialogResult was upgraded to DialogResult_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Function DialogResult_Renamed() As String
		DialogResult_Renamed = sFirmNo
	End Function
	Public Sub SetAccountNo(ByRef sAC As String)
		sAccountNo = sAC
		Me.txtAccountNumber.Text = sAccountNo
	End Sub
	Public Sub SetProfileGroupID(ByRef sPGID As String)
		sProfileGroupID = sPGID
	End Sub
    'Private Sub frmVisma_SelectAccountFirm_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    '    Dim Cancel As Boolean = eventArgs.Cancel
    '    Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
    '    ' trap user clicking the X in upper right corner
    '    Cancel = True
    '    cmdCancel_Click(cmdCancel, New System.EventArgs())
    '    eventArgs.Cancel = Cancel
    'End Sub
    Private Sub gridFirms_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridFirms.CellClick
        With Me.gridFirms
            If e.RowIndex > -1 Then
                sFirmNo = .Rows(e.RowIndex).Cells(1).Value
            End If
        End With
    End Sub

End Class