Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmDocumentation
	Inherits System.Windows.Forms.Form
	
	Public bContinue As Boolean
	Private Sub cmdContinue_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdContinue.Click
		
		' Remove all tekst starting with < and ending with >
		If VB.Left(Me.txtStartHeader.Text, 1) = "<" And VB.Right(Me.txtStartHeader.Text, 1) = ">" Then
            Me.txtStartHeader.Text = ""
        End If
        If VB.Left(Me.txtStartBody.Text, 1) = "<" And VB.Right(Me.txtStartBody.Text, 1) = ">" Then
            Me.txtStartBody.Text = ""
        End If

        If VB.Left(Me.txtProfilesHeader.Text, 1) = "<" And VB.Right(Me.txtProfilesHeader.Text, 1) = ">" Then
            Me.txtProfilesHeader.Text = ""
        End If
        If VB.Left(Me.txtProfilesBody.Text, 1) = "<" And VB.Right(Me.txtProfilesBody.Text, 1) = ">" Then
            Me.txtProfilesBody.Text = ""
        End If

        If VB.Left(Me.txtClientsHeader.Text, 1) = "<" And VB.Right(Me.txtClientsHeader.Text, 1) = ">" Then
            Me.txtClientsHeader.Text = ""
        End If
        If VB.Left(Me.txtClientsBody.Text, 1) = "<" And VB.Right(Me.txtClientsBody.Text, 1) = ">" Then
            Me.txtClientsBody.Text = ""
        End If

        If VB.Left(Me.txtMatchingHeader.Text, 1) = "<" And VB.Right(Me.txtMatchingHeader.Text, 1) = ">" Then
            Me.txtMatchingHeader.Text = ""
        End If
        If VB.Left(Me.txtMatchingBody.Text, 1) = "<" And VB.Right(Me.txtMatchingBody.Text, 1) = ">" Then
            Me.txtMatchingBody.Text = ""
        End If

        If VB.Left(Me.txtEndHeader.Text, 1) = "<" And VB.Right(Me.txtEndHeader.Text, 1) = ">" Then
            Me.txtEndHeader.Text = ""
        End If
        If VB.Left(Me.txtEndBody.Text, 1) = "<" And VB.Right(Me.txtEndBody.Text, 1) = ">" Then
            Me.txtEndBody.Text = ""
        End If
		
		bContinue = True
		Me.Hide()
		
	End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		bContinue = False
		Me.Hide()
		
	End Sub
	
	
	Sub frmDocumentation_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		FormSelectAllTextboxs(Me)
	End Sub
End Class
