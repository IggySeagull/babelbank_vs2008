Option Strict Off
Option Explicit On

Friend Class frmPasswordStatistics
	Inherits System.Windows.Forms.Form
	Public bCancelled As Boolean
	Private sPWOld As String
	Private sPWNew As String
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		
		bCancelled = True
		' restore old pw
		sPWNew = sPWOld
		Me.Hide()
		
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		' test against old pw;
		If Trim(Me.txtPWOld.Text) <> sPWOld Then
			MsgBox(LRS(27005), MsgBoxStyle.Critical, LRS(62054))
		ElseIf Trim(Me.txtPWNew.Text) <> Trim(Me.txtPWConfirm.Text) Then 
			MsgBox(LRS(27006), MsgBoxStyle.Critical, LRS(62055))
		Else
			bCancelled = False
			sPWNew = Trim(Me.txtPWNew.Text)
			Me.Hide()
		End If
		
	End Sub
	
	Private Sub frmPasswordStatistics_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
        FormvbStyle(Me, "dollar.jpg", , 60)
		FormLRSCaptions(Me)
		bCancelled = False
		
	End Sub
	Public Function PWOld(ByRef sPW As String) As Object
		sPWOld = pwDeCrypt(sPW)
	End Function
	Public Function PWNew() As String
		PWNew = pwCrypt(sPWNew)
	End Function
End Class
