<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDebug
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdDatabase As System.Windows.Forms.Button
	Public WithEvents Picture1 As System.Windows.Forms.PictureBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdSystemInfo As System.Windows.Forms.Button
	Public WithEvents cmdDLL As System.Windows.Forms.Button
    Public WithEvents lblInfo As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDebug))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdDatabase = New System.Windows.Forms.Button
        Me.Picture1 = New System.Windows.Forms.PictureBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdSystemInfo = New System.Windows.Forms.Button
        Me.cmdDLL = New System.Windows.Forms.Button
        Me.lblInfo = New System.Windows.Forms.Label
        Me.txtInfoString = New System.Windows.Forms.TextBox
        Me.lblLine1 = New System.Windows.Forms.Label
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdDatabase
        '
        Me.cmdDatabase.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDatabase.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDatabase.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDatabase.Location = New System.Drawing.Point(369, 305)
        Me.cmdDatabase.Name = "cmdDatabase"
        Me.cmdDatabase.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDatabase.Size = New System.Drawing.Size(81, 25)
        Me.cmdDatabase.TabIndex = 1
        Me.cmdDatabase.Text = "&Databaseinfo"
        Me.cmdDatabase.UseVisualStyleBackColor = False
        '
        'Picture1
        '
        Me.Picture1.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Picture1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Picture1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Picture1.Image = CType(resources.GetObject("Picture1.Image"), System.Drawing.Image)
        Me.Picture1.Location = New System.Drawing.Point(467, 58)
        Me.Picture1.Name = "Picture1"
        Me.Picture1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Picture1.Size = New System.Drawing.Size(35, 34)
        Me.Picture1.TabIndex = 5
        Me.Picture1.TabStop = False
        Me.Picture1.Visible = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(540, 351)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdSystemInfo
        '
        Me.cmdSystemInfo.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSystemInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSystemInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSystemInfo.Location = New System.Drawing.Point(541, 305)
        Me.cmdSystemInfo.Name = "cmdSystemInfo"
        Me.cmdSystemInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSystemInfo.Size = New System.Drawing.Size(81, 25)
        Me.cmdSystemInfo.TabIndex = 4
        Me.cmdSystemInfo.Text = "SystemInfo"
        Me.cmdSystemInfo.UseVisualStyleBackColor = False
        '
        'cmdDLL
        '
        Me.cmdDLL.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDLL.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDLL.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDLL.Location = New System.Drawing.Point(455, 305)
        Me.cmdDLL.Name = "cmdDLL"
        Me.cmdDLL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDLL.Size = New System.Drawing.Size(81, 25)
        Me.cmdDLL.TabIndex = 2
        Me.cmdDLL.Text = "DLL's"
        Me.cmdDLL.UseVisualStyleBackColor = False
        '
        'lblInfo
        '
        Me.lblInfo.BackColor = System.Drawing.SystemColors.Control
        Me.lblInfo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblInfo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblInfo.Location = New System.Drawing.Point(188, 60)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblInfo.Size = New System.Drawing.Size(262, 35)
        Me.lblInfo.TabIndex = 3
        Me.lblInfo.Text = "To obtain debug-information, please press one of the three buttons below."
        '
        'txtInfoString
        '
        Me.txtInfoString.Location = New System.Drawing.Point(191, 99)
        Me.txtInfoString.Multiline = True
        Me.txtInfoString.Name = "txtInfoString"
        Me.txtInfoString.ReadOnly = True
        Me.txtInfoString.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInfoString.Size = New System.Drawing.Size(430, 200)
        Me.txtInfoString.TabIndex = 8
        Me.txtInfoString.TabStop = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(26, 345)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(600, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmDebug
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(653, 381)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtInfoString)
        Me.Controls.Add(Me.cmdDatabase)
        Me.Controls.Add(Me.Picture1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdSystemInfo)
        Me.Controls.Add(Me.cmdDLL)
        Me.Controls.Add(Me.lblInfo)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDebug"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Debug info"
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtInfoString As System.Windows.Forms.TextBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
