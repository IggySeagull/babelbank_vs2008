﻿Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic

Friend Class frmVisma_Start
    Inherits System.Windows.Forms.Form


    'Private Declare Function DestroyCaret Lib "user32" () As Integer
    Private oBabelFiles As vbBabel.BabelFiles
    'Public iStartPointImport As Short

    '' to be able to show modal form in taskbar:
    'Private Const vbLightGray As Integer = &H808080
    'Private Const WS_EX_APPWINDOW As Integer = &H40000
    'Private Const GWL_EXSTYLE As Integer = (-20)
    'Private Const SW_HIDE As Integer = 0
    'Private Const SW_SHOW As Integer = 5
    'Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Integer, ByVal nIndex As Integer) As Integer
    'Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Integer, ByVal nIndex As Integer, ByVal dwNewLong As Integer) As Integer
    'Private Declare Function ShowWindow Lib "user32" (ByVal hwnd As Integer, ByVal nCmdShow As Integer) As Integer

    'Private m_bActivated As Boolean
    'Private Sub frmVisma_Start_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
    '    If Not m_bActivated Then
    '        m_bActivated = True
    '        Call SetWindowLong(Handle.ToInt32, GWL_EXSTYLE, GetWindowLong(Handle.ToInt32, GWL_EXSTYLE) Or WS_EX_APPWINDOW)
    '        Call ShowWindow(Handle.ToInt32, SW_HIDE)
    '        Call ShowWindow(Handle.ToInt32, SW_SHOW)
    '    End If
    'End Sub
    Private Sub frmVisma_Start_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        ' WAIT
        'Dim windowPosition As Long
        ' Always on top
        'windowPosition = SetWindowPos(frmVisma_Start.hwnd, -1, 0, 0, 0, 0, 1)

        ' XNET 05.06.2015 (Vismaendring)
        'Show
        Me.Show()

        FormLRSCaptions(Me)
        FormVismaStyle(Me)
        Me.prgActionProgress.ForeColor = Color.Blue
        'We will also need to present BabelBank versionno
        'Me.lblBBVersion.BackColor = System.Drawing.ColorTranslator.FromOle(&H808080)
        Me.lblBBVersion.Text = LRS(60045) & " " & Replace(System.Diagnostics.FileVersionInfo.GetVersionInfo(My.Application.Info.DirectoryPath & "\BabelBank.exe").ProductVersion, ".0.", ".")
        StartImportExport()
    End Sub
    Public Function StartImportExport() As Object
        Dim aSelectedFilenames() As String

        ReDim Preserve aSelectedFilenames(0)
        'VISMA_TODO
        Visma_ImportExport(aSelectedFilenames, Me, oBabelFiles)

    End Function

    'Private Sub Timer1_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Timer1.Tick
    '    Dim frmVisma_start As frmVisma_Start
    '    'Timer1.Interval = 0
    '    Timer1.Enabled = False

    '    ' XNET 05.06.2015 (Vismaendring)
    '    ' added 05.06.2015 to not start ImportExport for visma when called with S; /Setup
    '    If IsThisVismaIntegrator() Then
    '        If VB.Left(VB.Command(), 5) = "VB;/S" Then
    '            ' no call to ImportExport
    '            'frmVisma_Setup.Show
    '        Else
    '            'StartImportExport()
    '            'Me.Show()
    '            '    Me.Refresh()
    '        End If
    '    Else
    '        ' vanlig Babelbank
    '        'StartImportExport()
    '    End If

    'End Sub
    'Private Sub frmVisma_Start_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    '    Dim Cancel As Boolean = eventArgs.Cancel
    '    Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
    '    ' trap user clicking the X in upper right corner
    '    Cancel = True
    '    Me.Hide()
    '    End
    'End Sub
    Friend WriteOnly Property BabelFiles() As vbBabel.BabelFiles
        Set(ByVal Value As vbBabel.BabelFiles)
            If Not IsReference(Value) Then
                RaiseInvalidObject()
            End If

            'Store reference
            oBabelFiles = Value
        End Set
    End Property
    Private Sub txtStatus_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtStatus.TextChanged
        txtStatus.SelectionStart = Len(txtStatus.Text)
        txtStatus.SelectionLength = 0
    End Sub
    'Public Sub Form_ReInitialize()
    '    Dim FormStyles As Integer
    '    FormStyles = GetWindowLong(Me.Handle.ToInt32, GWL_EXSTYLE)
    '    FormStyles = FormStyles Or WS_EX_APPWINDOW
    '    Call SetWindowLong(Me.Handle.ToInt32, GWL_EXSTYLE, FormStyles)
    'End Sub

    Private Sub mnuClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Hide()
        End
    End Sub

    Private Sub mnuReportTotal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportTotal.Click

        Dim oBabelReport As New vbBabel.BabelReport
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim sREF As String
        Dim lRowCounter As Integer
        Dim dInvoiceAmount As Double
        Dim dTransferredAmount As Double
        Dim bFoundAny As Boolean
        Dim bAllRejected As Boolean
        Dim oMyDal As New vbBabel.DAL
        Dim oVismaCommandLine As New VismaCommandLine
        Dim sInstance As String
        Dim sUID As String
        Dim sPWD As String
        Dim sErrorString As String
        Dim sSystemDatabase As String
        Dim iReturnValue As Short
        ' -----------------------------------------------------
        ' Present report with totals only
        ' -----------------------------------------------------

        On Error GoTo Err_Renamed
        bFoundAny = False

        sUID = oVismaCommandLine.UID
        sPWD = oVismaCommandLine.PWD
        sSystemDatabase = oVismaCommandLine.SystemDatabase
        sInstance = oVismaCommandLine.Instance

        oMyDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        '--
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        ' fill up ToSpecialReport, otherwise we will have nothing in reports
        If Not oBabelFiles Is Nothing Then
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' report only on exported payments !
                        If oPayment.Exported Then
                            ' do not include errormarked payments;
                            If Val(oPayment.StatusCode) > 2 Then
                                oPayment.ToSpecialReport = False
                            Else
                                oPayment.ToSpecialReport = True
                                bFoundAny = True
                            End If
                            bAllRejected = True
                            For Each oInvoice In oPayment.Invoices
                                ' 10.01.2014 Endret fra If Val(oInvoice.StatusCode) > 2 Then
                                ' 27.03.2014 - added and Not "90" (Warning)
                                If oInvoice.MATCH_Original Then '11.02.2015 - Added next IF to correspond with the matching
                                    If Val(oInvoice.Visma_StatusCode) > 2 And Val(oInvoice.Visma_StatusCode) <> 90 Then
                                        oInvoice.ToSpecialReport = False
                                    Else
                                        oInvoice.ToSpecialReport = True
                                        bAllRejected = False
                                    End If
                                Else
                                    oInvoice.ToSpecialReport = False
                                End If
                            Next oInvoice
                            If bAllRejected Then
                                oPayment.ToSpecialReport = False
                            End If
                        Else
                            oPayment.ToSpecialReport = False
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile
        End If

        If bFoundAny Then
            ' activate report, call report 001
            oBabelReport.ReportNumber = CShort("1010") ' totalreport, Visma special
            oBabelReport.Preview = True
            oBabelReport.Heading = LRS(35011)  '"Totalrapport
            oBabelReport.ToPrint = False
            oBabelReport.AskForPrinter = False
            oBabelReport.ToEmail = False
            oBabelReport.ExportType = ""
            oBabelReport.ExportFilename = ""
            'oBabelReport.TotalLevel = 3
            'oBabelReport.DetailLevel = 0
            'oBabelReport.BitmapFilename = ""
            oBabelReport.PrinterName = ""
            oBabelReport.UseLongDate = False

            oBabelReport.BabelFiles = oBabelFiles
            oBabelReport.OwnerForm = frmStart 'oOwnerForm ' 09.09.05 - Needs Owner form to use when Form.Show
            oBabelReport.Special = "VismaBusiness"
            oBabelReport.ReportOnSelectedItems = True
            oBabelReport.VISMA_ConnectionString = oMyDal.Connectionstring
            oBabelReport.VISMA_BBDB = oVismaCommandLine.BBDatabase
            oBabelReport.BreakLevel = vbBabel.BabelFiles.BreakLevel.BreakOnAccount '4 BreakOnAccount
            oBabelReport.RunReport()

            ' reset ToSpecialReport
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.ToSpecialReport = True
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.ToSpecialReport = False
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile
        Else
            ' found no paymentes to report;
            MsgBox(LRS(35207), MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Visma Payment (BabelBank)") ' No payments found
        End If

        If Not oVismaCommandLine Is Nothing Then
            oVismaCommandLine = Nothing
        End If

        Exit Sub

Err_Renamed:

    End Sub

    Private Sub mnuStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuStart.Click
        StartImportExport()
    End Sub

    Private Sub mnuReportOverview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportOverview.Click
        Dim oBabelReport As New vbBabel.BabelReport
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim sREF As String
        Dim lRowCounter As Integer
        Dim bFoundAny As Boolean
        Dim bAllRejected As Boolean
        Dim oMyDal As New vbBabel.DAL
        Dim sInstance As String
        Dim oVismaCommandLine As New VismaCommandLine
        Dim sUID As String
        Dim sPWD As String
        Dim sErrorString As String
        Dim sSystemDatabase As String
        Dim iReturnValue As Short

        On Error GoTo Err_Renamed

        sUID = oVismaCommandLine.UID
        sPWD = oVismaCommandLine.PWD
        sSystemDatabase = oVismaCommandLine.SystemDatabase
        sInstance = oVismaCommandLine.Instance

        oMyDal = ConnectToVismaBusinessDB(False, sErrorString, True)


        ' -----------------------------------------------------
        ' Present report with one line pr payment, for all checked payments
        ' Run through all payments, and mark the checked one
        ' -----------------------------------------------------

        bFoundAny = False

        ' activate report, call report 1002_VismaPayment
        oBabelReport.ReportNumber = CShort("1002")
        oBabelReport.Preview = True
        oBabelReport.Heading = LRS(35267) 'Betalingsoversikt
        oBabelReport.ToPrint = False
        oBabelReport.AskForPrinter = False
        oBabelReport.ToEmail = False
        oBabelReport.ExportType = ""
        oBabelReport.ExportFilename = ""
        'Sideskift ved ny konto - [vbsys].[dbo].[BB_ETFInf].SplitReportPerAccount Hvis 1 then True else False [vbsys].[dbo].[BB_ETFInf].BankProc BITVERDI 8 i SQL case when BankProc &32 = 32 THEN True Else False
        'If bSplitReportPerAccount Then
        oBabelReport.BreakLevel = 4 ' BreakOnAccount
        'Else
        'oReport.BreakLevel = 0
        'End If
        'oBabelReport.TotalLevel = 3
        'oBabelReport.DetailLevel = 0
        'oBabelReport.BitmapFilename = ""
        oBabelReport.PrinterName = ""
        oBabelReport.UseLongDate = False
        '--
        oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.Before_Export  '????

        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

        oBabelReport.BabelFiles = oBabelFiles
        oBabelReport.Special = "VismaBusiness"
        'oBabelReport.RunReport()


        'System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        'oBabelReport = New vbBabel.BabelReport
        'oBabelReport.Report = oReport

        ' fill up ToSpecialReport, otherwise we will have nothing in reports
        If Not oBabelFiles Is Nothing Then
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' report only on exported payments !
                        'If oPayment.Exported Then
                        ' do not include errormarked payments;
                        If Val(oPayment.StatusCode) > 2 Then
                            oPayment.ToSpecialReport = False
                        Else
                            oPayment.ToSpecialReport = True
                            bFoundAny = True
                        End If
                        bAllRejected = True
                        For Each oInvoice In oPayment.Invoices
                            ' 10.01.2014 Endret fra If Val(oInvoice.StatusCode) > 2 Then
                            ' 27.03.2014 - added and Not "90" (Warning)
                            If oInvoice.MATCH_Original Then '11.02.2015 - Added next IF to correspond with the matching
                                If Val(oInvoice.Visma_StatusCode) > 2 And Val(oInvoice.Visma_StatusCode) <> 90 Then

                                    oInvoice.ToSpecialReport = False
                                Else
                                    oInvoice.ToSpecialReport = True
                                    bAllRejected = False
                                End If
                            Else
                                oInvoice.ToSpecialReport = False
                            End If
                        Next oInvoice
                        If bAllRejected Then
                            oPayment.ToSpecialReport = False
                        End If
                        'Else
                        '    oPayment.ToSpecialReport = False
                        'End If
                    Next oPayment
                Next oBatch
            Next oBabelFile
        End If
        If bFoundAny Then
            oBabelReport.OwnerForm = frmStart 'oOwnerForm ' 09.09.05 - Needs Owner form to use when Form.Show
            oBabelReport.BabelFiles = oBabelFiles
            oBabelReport.Special = "VismaBusiness"

            oBabelReport.VISMA_ConnectionString = oMyDal.Connectionstring
            oBabelReport.VISMA_BBDB = oVismaCommandLine.BBDatabase
            oBabelReport.BreakLevel = vbBabel.BabelFiles.BreakLevel.BreakOnAccount '4 BreakOnAccount
            oBabelReport.RunReport()


            ' reset ToSpecialReport
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.ToSpecialReport = True
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.ToSpecialReport = False
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Else
            ' found no paymentes to report;
            MsgBox(LRS(35207), MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Visma Payment (BabelBank)") ' No payments found
        End If
        oBabelReport = Nothing
        oMyDal.Close()
        oMyDal = Nothing

        If Not oVismaCommandLine Is Nothing Then
            oVismaCommandLine = Nothing
        End If

        Exit Sub

Err_Renamed:
        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

    End Sub

    Private Sub mnuReportDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportDetail.Click

        Dim oBabelReport As New vbBabel.BabelReport
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim sREF As String
        Dim lRowCounter As Integer
        Dim bFoundAny As Boolean
        Dim oMyDal As New vbBabel.DAL
        ' -----------------------------------------------------
        ' Present report with details, for all checked payments
        ' Run through all payments, and mark the checked one
        ' -----------------------------------------------------

        '---------------------------------------------------------------------------------------
        ' TEMP 02.12.2014 - lagt inn for testlogging;
        '---------------------------------------------------------------------------------------
        Dim sLogPath As String
        Dim oBabelLog As vbLog.vbLogging
        Dim bLog As Boolean
        'VISMA_TODO
        'Dim oVismaCon As ADODB.Connection
        Dim sInstance As String
        Dim oVismaCommandLine As New VismaCommandLine
        Dim sUID As String
        Dim sPWD As String
        Dim sErrorString As String
        Dim sSystemDatabase As String
        Dim iReturnValue As Short

        On Error GoTo Err_Renamed

        sUID = oVismaCommandLine.UID
        sPWD = oVismaCommandLine.PWD
        sSystemDatabase = oVismaCommandLine.SystemDatabase
        sInstance = oVismaCommandLine.Instance

        oMYDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        sLogPath = Visma_RetrieveProgramDataBabelBank(oMyDal, sSystemDatabase) & "\Log.txt"

        iReturnValue = CheckFolder(sLogPath, vb_Utils.ErrorMessageType.Use_ERR_Raise, True, "Logfile", True, True)
        If iReturnValue > 10 Then
            bLog = False
            Exit Sub
        Else
            bLog = True
            oBabelLog = New vbLog.vbLogging
            oBabelLog.LogPath = sLogPath
            oBabelLog.LogDetail = 8 'Everything
            oBabelLog.LogType = 2 'To file
            oBabelLog.AutoDeleteLines = 5000
            oBabelLog.LogOverWrite = False
            oBabelLog.StartLog()
            oBabelLog.AddLogEvent("======" & "Visma Payment (BabelBank) Top of cmdReportDetail_Click " & New String("=", 58), System.Diagnostics.TraceEventType.Information)
            oBabelLog.Heading = "Visma Payment (BabelBank) info: "
        End If


        bFoundAny = False

        ' show detailreport

        ' activate report, call report 992
        oBabelReport.ReportNumber = CShort("992") ' rp_992_VismaDetail
        oBabelReport.Preview = True
        oBabelReport.Heading = LRS(35267) 'Betalingsoversikt
        oBabelReport.ToPrint = False
        oBabelReport.AskForPrinter = False
        oBabelReport.ToEmail = False
        oBabelReport.ExportType = ""
        oBabelReport.ExportFilename = ""
        'Sideskift ved ny konto - [vbsys].[dbo].[BB_ETFInf].SplitReportPerAccount Hvis 1 then True else False [vbsys].[dbo].[BB_ETFInf].BankProc BITVERDI 8 i SQL case when BankProc &32 = 32 THEN True Else False
        'If bSplitReportPerAccount Then
        oBabelReport.BreakLevel = 4 ' BreakOnAccount
        'Else
        'oReport.BreakLevel = 0
        'End If
        'oBabelReport.TotalLevel = 3
        'oBabelReport.DetailLevel = 0
        'oBabelReport.BitmapFilename = ""
        oBabelReport.PrinterName = ""
        oBabelReport.UseLongDate = False
        '--
        oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.Before_Export  '????

        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

        ' fill up ToSpecialReport, otherwise we will have nothing in reports
        If Not oBabelFiles Is Nothing Then
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' report only on exported payments !
                        If oPayment.Exported Then
                            ' do not include errormarked payments;
                            If Val(oPayment.StatusCode) > 2 Then
                                oPayment.ToSpecialReport = False
                            Else
                                oPayment.ToSpecialReport = True
                                bFoundAny = True
                            End If
                            For Each oInvoice In oPayment.Invoices
                                ' 10.01.2014 Endret fra If Val(oInvoice.StatusCode) > 2 Then
                                ' 27.03.2014 - added and Not "90" (Warning)
                                If oInvoice.MATCH_Original Then '11.02.2015 - Added next IF to correspond with the matching
                                    If Val(oInvoice.Visma_StatusCode) > 2 And Val(oInvoice.Visma_StatusCode) <> 90 Then
                                        oInvoice.ToSpecialReport = False
                                    Else
                                        oInvoice.ToSpecialReport = True
                                    End If
                                Else
                                    oInvoice.ToSpecialReport = False
                                End If
                            Next oInvoice
                        Else
                            oPayment.ToSpecialReport = False
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile
        End If
        If bFoundAny Then
            oBabelReport.OwnerForm = frmStart 'oOwnerForm ' 09.09.05 - Needs Owner form to use when Form.Show
            'oBabelLog.AddLogEvent "Before oBabelReport.Start ", vbLogEventTypeInformation
            oBabelReport.LoggingActivated = bLog
            oBabelReport.BabelLog = oBabelLog

            oBabelReport.BabelFiles = oBabelFiles
            oBabelReport.Special = "VismaBusiness"
            oBabelReport.VISMA_ConnectionString = oMyDal.Connectionstring
            oBabelReport.VISMA_BBDB = oVismaCommandLine.BBDatabase
            oBabelReport.BreakLevel = vbBabel.BabelFiles.BreakLevel.BreakOnAccount '4 BreakOnAccount
            oBabelReport.RunReport()

            ' reset ToSpecialReport
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.ToSpecialReport = True
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.ToSpecialReport = False
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Else
            ' found no paymentes to report;
            MsgBox(LRS(35207), MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Visma Payment (BabelBank)") ' No payments found
        End If
        oBabelReport = Nothing

        oBabelLog.AddLogEvent("End of cmdReportDetail_Click ", System.Diagnostics.TraceEventType.Information)
        oBabelLog = Nothing

        oMyDal.Close()
        oMyDal = Nothing

        If Not oVismaCommandLine Is Nothing Then
            oVismaCommandLine = Nothing
        End If

        Exit Sub

Err_Renamed:
        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        oBabelLog.AddLogEvent("In errorhandler of cmdReportDetail_Click ", System.Diagnostics.TraceEventType.Information)
        oBabelLog = Nothing

    End Sub
    Private Sub cmdReportError_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuReportError.Click
        Dim oBabelReport As New vbBabel.BabelReport
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim sREF As String
        Dim lRowCounter As Integer
        Dim oErrorObject As vbBabel.ErrorObject
        Dim bHasErrors As Boolean
        Dim bFoundAny As Boolean
        Dim sInstance As String
        Dim oVismaCommandLine As New VismaCommandLine
        Dim sUID As String
        Dim sPWD As String
        Dim sErrorString As String
        Dim sSystemDatabase As String
        Dim oMyDal As New vbBabel.DAL
        '------------------------------------------------------------------
        ' Present report with errormarked payments
        ' Run through all payments, and mark the errorvalidated
        '------------------------------------------------------------------
        On Error GoTo Err_Renamed
        bFoundAny = False

        sUID = oVismaCommandLine.UID
        sPWD = oVismaCommandLine.PWD
        sSystemDatabase = oVismaCommandLine.SystemDatabase
        sInstance = oVismaCommandLine.Instance

        oMyDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        If Not oBabelFiles Is Nothing Then
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' report only on exported payments !
                        If oPayment.Exported Then
                            bHasErrors = False
                            For Each oInvoice In oPayment.Invoices
                                For Each oErrorObject In oInvoice.ErrorObjects
                                    If oErrorObject.ErrorType = "Error" Then
                                        bFoundAny = True
                                        oPayment.ToSpecialReport = True
                                        oInvoice.ToSpecialReport = True
                                    End If

                                Next oErrorObject
                            Next oInvoice
                        Else
                            oPayment.ToSpecialReport = True
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile
        End If

        If bFoundAny Then
            ' activate report, call report 992
            oBabelReport.ReportNumber = CShort("992") ' rp_992_VismaDetail
            oBabelReport.Preview = True
            oBabelReport.Heading = LRS(35013) '"Feilmarkerte betalinger"
            oBabelReport.ToPrint = False
            oBabelReport.AskForPrinter = False
            oBabelReport.ToEmail = False
            oBabelReport.ExportType = ""
            oBabelReport.ExportFilename = ""
            'Sideskift ved ny konto - [vbsys].[dbo].[BB_ETFInf].SplitReportPerAccount Hvis 1 then True else False [vbsys].[dbo].[BB_ETFInf].BankProc BITVERDI 8 i SQL case when BankProc &32 = 32 THEN True Else False
            'If bSplitReportPerAccount Then
            'oBabelReport.BreakLevel = 4 ' BreakOnAccount
            'Else
            'oReport.BreakLevel = 0
            'End If
            'oBabelReport.TotalLevel = 3
            'oBabelReport.DetailLevel = 0
            'oBabelReport.BitmapFilename = ""
            oBabelReport.PrinterName = ""
            oBabelReport.UseLongDate = False
            '--
            oBabelReport.VISMA_ConnectionString = oMyDal.Connectionstring
            oBabelReport.VISMA_BBDB = oVismaCommandLine.BBDatabase
            oBabelReport.BreakLevel = vbBabel.BabelFiles.BreakLevel.BreakOnAccount '4 BreakOnAccount
            oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.Before_Export  '????


            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            oBabelReport = New vbBabel.BabelReport
            oBabelReport.BabelFiles = oBabelFiles
            oBabelReport.OwnerForm = frmStart 'oOwnerForm ' 09.09.05 - Needs Owner form to use when Form.Show
            oBabelReport.Special = "ErrorReport"

            ' reset ToSpecialReport
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.ToSpecialReport = False
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.ToSpecialReport = False
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile
        Else
            ' found no paymentes to report;
            MsgBox(LRS(35207), MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Visma Payment (BabelBank)") ' No payments found
        End If

        oBabelReport = Nothing
        oMyDal.Close()
        oMyDal = Nothing

        If Not oVismaCommandLine Is Nothing Then
            oVismaCommandLine = Nothing
        End If

        Exit Sub

Err_Renamed:
        oBabelReport = Nothing

    End Sub

End Class