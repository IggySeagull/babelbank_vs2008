Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmAlpharmaTreasury
	Inherits System.Windows.Forms.Form
	Private aIndexes(,) AS Long
	'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
	'DOTNETT REDIM Private aIndexes() As Long  ' Holds babelfile, batch, payment indexes.
	Private oBabelFiles As vbBabel.BabelFiles
	Private oBabelFile As vbBabel.BabelFile
	Private oBatch As vbBabel.Batch
	Private oPayment As vbBabel.Payment
	Private oFreetext As vbBabel.Freetext
	Private nBabelFileIndex As Integer
	Private nBatchIndex As Integer
	Private nPaymentIndex As Integer
	Private bLoaded As Boolean
	
	Private Sub cmdContinue_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdContinue.Click
		' save last comment
		lstPayments_SelectedIndexChanged(lstPayments, New System.EventArgs())
		Me.Hide()
	End Sub
	Private Sub frmAlpharmaTreasury_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		' Fill Listbox and aIndexes
		Dim aIndexes(0, 0) As Object
		bLoaded = False
		Me.lstPayments.Items.Clear()
		For	Each oBabelFile In oBabelFiles
			For	Each oBatch In oBabelFile.Batches
				For	Each oPayment In oBatch.Payments
                    Me.lstPayments.Items.Add(PadRight(Trim(oPayment.I_Account), 20, " ") & " " & PadRight(Trim(oPayment.E_Account), 25, " ") & " " & PadRight(Trim(oPayment.E_Name), 20, " ") & " " & oPayment.MON_InvoiceCurrency & " " & PadLeft(VB6.Format(oPayment.MON_InvoiceAmount / 100, "##,###,#0.00"), 15, " "))
					' add to array with indexes
					If Not UBound(aIndexes, 1) = 0 Then
						ReDim Preserve aIndexes(2, UBound(aIndexes, 2) + 1)
					Else
						ReDim aIndexes(2, 0)
					End If
					'UPGRADE_WARNING: Couldn't resolve default property of object aIndexes(0, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					aIndexes(0, UBound(aIndexes, 2)) = oBabelFile.Index
					'UPGRADE_WARNING: Couldn't resolve default property of object aIndexes(1, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					aIndexes(1, UBound(aIndexes, 2)) = oBatch.Index
					'UPGRADE_WARNING: Couldn't resolve default property of object aIndexes(2, UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					aIndexes(2, UBound(aIndexes, 2)) = oPayment.Index
				Next oPayment
			Next oBatch
		Next oBabelFile
		Me.lstPayments.SelectedIndex = 0
		bLoaded = True
	End Sub
	
	'UPGRADE_WARNING: Event lstPayments.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub lstPayments_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstPayments.SelectedIndexChanged
		Dim aIndexes As Object
		' Change text in collections for current payment
		' Assumes only one invoice pr payment
		Dim i As Short
		Dim sText As String
		sText = Trim(txtText.Text)
		
		If nBabelFileIndex > 0 Then
			' remove previous freetexts:
			For i = 1 To oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex).Invoices(1).Freetexts.Count
				oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex).Invoices(1).Freetexts.Remove((i))
			Next i
			oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex).Invoices(1).Freetexts.Add()
			oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex).Invoices(1).Freetexts(1).Text = VB.Left(sText, 35)
			If Len(sText) > 35 Then
				oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex).Invoices(1).Freetexts.Add()
				oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex).Invoices(1).Freetexts(2).Text = Mid(sText, 36, 35)
			End If
			If Len(sText) > 70 Then
				oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex).Invoices(1).Freetexts.Add()
				oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex).Invoices(1).Freetexts(3).Text = Mid(sText, 71, 35)
			End If
			If Len(sText) > 105 Then
				oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex).Invoices(1).Freetexts.Add()
				oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex).Invoices(1).Freetexts(4).Text = Mid(sText, 106, 35)
			End If
		End If
		' Show text for selected payment
		'UPGRADE_WARNING: Couldn't resolve default property of object aIndexes(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		nBabelFileIndex = aIndexes(0, lstPayments.SelectedIndex)
		'UPGRADE_WARNING: Couldn't resolve default property of object aIndexes(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		nBatchIndex = aIndexes(1, lstPayments.SelectedIndex)
		'UPGRADE_WARNING: Couldn't resolve default property of object aIndexes(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		nPaymentIndex = aIndexes(2, lstPayments.SelectedIndex)
		
		txtText.Text = ""
		For	Each oFreetext In oBabelFiles(nBabelFileIndex).Batches(nBatchIndex).Payments(nPaymentIndex).Invoices(1).Freetexts
			txtText.Text = txtText.Text & oFreetext.Text
		Next oFreetext
		
		Me.txtText.SelectionStart = Len(Trim(Me.txtText.Text)) + 1
		If bLoaded Then
			'Me.txtText.SetFocus
		End If
	End Sub
	Friend Function SetBabelFiles(ByRef oImportObj As vbBabel.BabelFiles) As Boolean
		
        oBabelFiles = New vbBabel.BabelFiles
		oBabelFiles = oImportObj
		
	End Function
End Class
