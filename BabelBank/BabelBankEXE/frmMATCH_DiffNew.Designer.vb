<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMATCH_DiffNew
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents lstToAccount As System.Windows.Forms.ListBox
	Public WithEvents lstQuickPostings As System.Windows.Forms.ListBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents lblToAccount As System.Windows.Forms.Label
    Public WithEvents lblDiff As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lstToAccount = New System.Windows.Forms.ListBox
        Me.lstQuickPostings = New System.Windows.Forms.ListBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.lblToAccount = New System.Windows.Forms.Label
        Me.lblDiff = New System.Windows.Forms.Label
        Me.lblLine2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lstToAccount
        '
        Me.lstToAccount.BackColor = System.Drawing.SystemColors.Window
        Me.lstToAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstToAccount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstToAccount.Location = New System.Drawing.Point(225, 136)
        Me.lstToAccount.Name = "lstToAccount"
        Me.lstToAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstToAccount.Size = New System.Drawing.Size(305, 17)
        Me.lstToAccount.TabIndex = 1
        '
        'lstQuickPostings
        '
        Me.lstQuickPostings.BackColor = System.Drawing.SystemColors.Window
        Me.lstQuickPostings.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstQuickPostings.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstQuickPostings.Location = New System.Drawing.Point(225, 72)
        Me.lstQuickPostings.Name = "lstQuickPostings"
        Me.lstQuickPostings.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstQuickPostings.Size = New System.Drawing.Size(305, 17)
        Me.lstQuickPostings.TabIndex = 0
        '
        'cmdOK
        '
        Me.cmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(418, 176)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(496, 176)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "55006 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'lblToAccount
        '
        Me.lblToAccount.BackColor = System.Drawing.SystemColors.Control
        Me.lblToAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblToAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblToAccount.Location = New System.Drawing.Point(223, 113)
        Me.lblToAccount.Name = "lblToAccount"
        Me.lblToAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblToAccount.Size = New System.Drawing.Size(360, 16)
        Me.lblToAccount.TabIndex = 3
        Me.lblToAccount.Text = "60127 - Difference to account"
        '
        'lblDiff
        '
        Me.lblDiff.BackColor = System.Drawing.SystemColors.Control
        Me.lblDiff.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDiff.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDiff.Location = New System.Drawing.Point(222, 41)
        Me.lblDiff.Name = "lblDiff"
        Me.lblDiff.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDiff.Size = New System.Drawing.Size(291, 17)
        Me.lblDiff.TabIndex = 2
        Me.lblDiff.Text = "60129 - Restamount to match"
        '
        'lblLine2
        '
        Me.lblLine2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLine2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine2.Location = New System.Drawing.Point(200, 169)
        Me.lblLine2.Name = "lblLine2"
        Me.lblLine2.Size = New System.Drawing.Size(370, 1)
        Me.lblLine2.TabIndex = 82
        Me.lblLine2.Text = "Label1"
        '
        'frmMATCH_DiffNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(585, 209)
        Me.Controls.Add(Me.lblLine2)
        Me.Controls.Add(Me.lstToAccount)
        Me.Controls.Add(Me.lstQuickPostings)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.lblToAccount)
        Me.Controls.Add(Me.lblDiff)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmMATCH_DiffNew"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "60124 - Behandling av differanse"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine2 As System.Windows.Forms.Label
#End Region 
End Class
