<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmStoredPayments
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdSearchAmount As System.Windows.Forms.Button
	Public WithEvents txtSearchAmount As System.Windows.Forms.TextBox
	Public WithEvents cmdSearchName As System.Windows.Forms.Button
	Public WithEvents txtSearchName As System.Windows.Forms.TextBox
	Public WithEvents ImageListStatus As System.Windows.Forms.ImageList
	Public WithEvents cmdChoose As System.Windows.Forms.Button
	Public WithEvents cmdPrintThis As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents optShowNotMatched As System.Windows.Forms.RadioButton
	Public WithEvents optShowPartly As System.Windows.Forms.RadioButton
	Public WithEvents optShowProposed As System.Windows.Forms.RadioButton
	Public WithEvents optShowNotCompletely As System.Windows.Forms.RadioButton
	Public WithEvents optShowAll As System.Windows.Forms.RadioButton
	Public WithEvents optShowMatched As System.Windows.Forms.RadioButton
    Public WithEvents imgMatched As System.Windows.Forms.PictureBox
	Public WithEvents imgProposed As System.Windows.Forms.PictureBox
	Public WithEvents imgPartly As System.Windows.Forms.PictureBox
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents lblSearchAmount As System.Windows.Forms.Label
	Public WithEvents LblSearchName As System.Windows.Forms.Label
    Public WithEvents txtLabelStored As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStoredPayments))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdSearchAmount = New System.Windows.Forms.Button
        Me.txtSearchAmount = New System.Windows.Forms.TextBox
        Me.cmdSearchName = New System.Windows.Forms.Button
        Me.txtSearchName = New System.Windows.Forms.TextBox
        Me.ImageListStatus = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdChoose = New System.Windows.Forms.Button
        Me.cmdPrintThis = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.Frame1 = New System.Windows.Forms.GroupBox
        Me.imgOmit = New System.Windows.Forms.PictureBox
        Me.optShowOmit = New System.Windows.Forms.RadioButton
        Me.imgEmptyImage = New System.Windows.Forms.PictureBox
        Me.optShowNotMatched = New System.Windows.Forms.RadioButton
        Me.optShowPartly = New System.Windows.Forms.RadioButton
        Me.optShowProposed = New System.Windows.Forms.RadioButton
        Me.optShowNotCompletely = New System.Windows.Forms.RadioButton
        Me.optShowAll = New System.Windows.Forms.RadioButton
        Me.optShowMatched = New System.Windows.Forms.RadioButton
        Me.imgMatched = New System.Windows.Forms.PictureBox
        Me.imgProposed = New System.Windows.Forms.PictureBox
        Me.imgPartly = New System.Windows.Forms.PictureBox
        Me.lblSearchAmount = New System.Windows.Forms.Label
        Me.LblSearchName = New System.Windows.Forms.Label
        Me.txtLabelStored = New System.Windows.Forms.Label
        Me.gridStoredPayments = New System.Windows.Forms.DataGridView
        Me.gridStoredMatched = New System.Windows.Forms.DataGridView
        Me.lblLine2 = New System.Windows.Forms.Label
        Me.Frame1.SuspendLayout()
        CType(Me.imgOmit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgEmptyImage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgProposed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgPartly, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridStoredPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridStoredMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdSearchAmount
        '
        Me.cmdSearchAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSearchAmount.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSearchAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSearchAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSearchAmount.Location = New System.Drawing.Point(655, 516)
        Me.cmdSearchAmount.Name = "cmdSearchAmount"
        Me.cmdSearchAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSearchAmount.Size = New System.Drawing.Size(55, 17)
        Me.cmdSearchAmount.TabIndex = 16
        Me.cmdSearchAmount.Text = "60180 "
        Me.cmdSearchAmount.UseVisualStyleBackColor = False
        '
        'txtSearchAmount
        '
        Me.txtSearchAmount.AcceptsReturn = True
        Me.txtSearchAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchAmount.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchAmount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSearchAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSearchAmount.Location = New System.Drawing.Point(532, 516)
        Me.txtSearchAmount.MaxLength = 0
        Me.txtSearchAmount.Name = "txtSearchAmount"
        Me.txtSearchAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSearchAmount.Size = New System.Drawing.Size(121, 20)
        Me.txtSearchAmount.TabIndex = 15
        '
        'cmdSearchName
        '
        Me.cmdSearchName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSearchName.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSearchName.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSearchName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSearchName.Location = New System.Drawing.Point(655, 492)
        Me.cmdSearchName.Name = "cmdSearchName"
        Me.cmdSearchName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSearchName.Size = New System.Drawing.Size(55, 17)
        Me.cmdSearchName.TabIndex = 13
        Me.cmdSearchName.Text = "60180 "
        Me.cmdSearchName.UseVisualStyleBackColor = False
        '
        'txtSearchName
        '
        Me.txtSearchName.AcceptsReturn = True
        Me.txtSearchName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchName.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSearchName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSearchName.Location = New System.Drawing.Point(532, 491)
        Me.txtSearchName.MaxLength = 0
        Me.txtSearchName.Name = "txtSearchName"
        Me.txtSearchName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSearchName.Size = New System.Drawing.Size(121, 20)
        Me.txtSearchName.TabIndex = 12
        '
        'ImageListStatus
        '
        Me.ImageListStatus.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageListStatus.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListStatus.TransparentColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        '
        'cmdChoose
        '
        Me.cmdChoose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdChoose.BackColor = System.Drawing.SystemColors.Control
        Me.cmdChoose.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdChoose.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdChoose.Location = New System.Drawing.Point(423, 572)
        Me.cmdChoose.Name = "cmdChoose"
        Me.cmdChoose.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdChoose.Size = New System.Drawing.Size(89, 25)
        Me.cmdChoose.TabIndex = 0
        Me.cmdChoose.Text = "55018 - Choose"
        Me.cmdChoose.UseVisualStyleBackColor = False
        '
        'cmdPrintThis
        '
        Me.cmdPrintThis.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdPrintThis.BackColor = System.Drawing.SystemColors.Control
        Me.cmdPrintThis.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdPrintThis.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPrintThis.Location = New System.Drawing.Point(522, 572)
        Me.cmdPrintThis.Name = "cmdPrintThis"
        Me.cmdPrintThis.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdPrintThis.Size = New System.Drawing.Size(89, 25)
        Me.cmdPrintThis.TabIndex = 1
        Me.cmdPrintThis.Text = "55017 - Print"
        Me.cmdPrintThis.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(620, 572)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(89, 25)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "55006 - Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'Frame1
        '
        Me.Frame1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Frame1.BackColor = System.Drawing.Color.White
        Me.Frame1.Controls.Add(Me.imgOmit)
        Me.Frame1.Controls.Add(Me.optShowOmit)
        Me.Frame1.Controls.Add(Me.imgEmptyImage)
        Me.Frame1.Controls.Add(Me.optShowNotMatched)
        Me.Frame1.Controls.Add(Me.optShowPartly)
        Me.Frame1.Controls.Add(Me.optShowProposed)
        Me.Frame1.Controls.Add(Me.optShowNotCompletely)
        Me.Frame1.Controls.Add(Me.optShowAll)
        Me.Frame1.Controls.Add(Me.optShowMatched)
        Me.Frame1.Controls.Add(Me.imgMatched)
        Me.Frame1.Controls.Add(Me.imgProposed)
        Me.Frame1.Controls.Add(Me.imgPartly)
        Me.Frame1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Frame1.Location = New System.Drawing.Point(16, 476)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.Padding = New System.Windows.Forms.Padding(0)
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(367, 81)
        Me.Frame1.TabIndex = 3
        Me.Frame1.TabStop = False
        '
        'imgOmit
        '
        Me.imgOmit.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgOmit.Enabled = False
        Me.imgOmit.Image = CType(resources.GetObject("imgOmit.Image"), System.Drawing.Image)
        Me.imgOmit.Location = New System.Drawing.Point(342, 57)
        Me.imgOmit.Name = "imgOmit"
        Me.imgOmit.Size = New System.Drawing.Size(15, 15)
        Me.imgOmit.TabIndex = 74
        Me.imgOmit.TabStop = False
        '
        'optShowOmit
        '
        Me.optShowOmit.BackColor = System.Drawing.Color.White
        Me.optShowOmit.Cursor = System.Windows.Forms.Cursors.Default
        Me.optShowOmit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optShowOmit.Location = New System.Drawing.Point(184, 55)
        Me.optShowOmit.Name = "optShowOmit"
        Me.optShowOmit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optShowOmit.Size = New System.Drawing.Size(145, 17)
        Me.optShowOmit.TabIndex = 15
        Me.optShowOmit.Text = "61008 - Removed payments"
        Me.optShowOmit.UseVisualStyleBackColor = False
        '
        'imgEmptyImage
        '
        Me.imgEmptyImage.BackColor = System.Drawing.Color.Transparent
        Me.imgEmptyImage.Image = CType(resources.GetObject("imgEmptyImage.Image"), System.Drawing.Image)
        Me.imgEmptyImage.Location = New System.Drawing.Point(161, 18)
        Me.imgEmptyImage.Name = "imgEmptyImage"
        Me.imgEmptyImage.Size = New System.Drawing.Size(16, 15)
        Me.imgEmptyImage.TabIndex = 14
        Me.imgEmptyImage.TabStop = False
        '
        'optShowNotMatched
        '
        Me.optShowNotMatched.BackColor = System.Drawing.SystemColors.Control
        Me.optShowNotMatched.Cursor = System.Windows.Forms.Cursors.Default
        Me.optShowNotMatched.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optShowNotMatched.Location = New System.Drawing.Point(184, 78)
        Me.optShowNotMatched.Name = "optShowNotMatched"
        Me.optShowNotMatched.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optShowNotMatched.Size = New System.Drawing.Size(169, 17)
        Me.optShowNotMatched.TabIndex = 9
        Me.optShowNotMatched.Text = "60168 - Not matched"
        Me.optShowNotMatched.UseVisualStyleBackColor = False
        '
        'optShowPartly
        '
        Me.optShowPartly.BackColor = System.Drawing.Color.White
        Me.optShowPartly.Cursor = System.Windows.Forms.Cursors.Default
        Me.optShowPartly.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optShowPartly.Location = New System.Drawing.Point(184, 35)
        Me.optShowPartly.Name = "optShowPartly"
        Me.optShowPartly.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optShowPartly.Size = New System.Drawing.Size(145, 17)
        Me.optShowPartly.TabIndex = 8
        Me.optShowPartly.Text = "60167 - Partly matched"
        Me.optShowPartly.UseVisualStyleBackColor = False
        '
        'optShowProposed
        '
        Me.optShowProposed.BackColor = System.Drawing.Color.White
        Me.optShowProposed.Cursor = System.Windows.Forms.Cursors.Default
        Me.optShowProposed.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optShowProposed.Location = New System.Drawing.Point(184, 14)
        Me.optShowProposed.Name = "optShowProposed"
        Me.optShowProposed.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optShowProposed.Size = New System.Drawing.Size(153, 17)
        Me.optShowProposed.TabIndex = 7
        Me.optShowProposed.Text = "60166 - Proposed matched"
        Me.optShowProposed.UseVisualStyleBackColor = False
        '
        'optShowNotCompletely
        '
        Me.optShowNotCompletely.BackColor = System.Drawing.Color.White
        Me.optShowNotCompletely.Cursor = System.Windows.Forms.Cursors.Default
        Me.optShowNotCompletely.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optShowNotCompletely.Location = New System.Drawing.Point(8, 35)
        Me.optShowNotCompletely.Name = "optShowNotCompletely"
        Me.optShowNotCompletely.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optShowNotCompletely.Size = New System.Drawing.Size(169, 17)
        Me.optShowNotCompletely.TabIndex = 5
        Me.optShowNotCompletely.Text = "60164 - Not completely match"
        Me.optShowNotCompletely.UseVisualStyleBackColor = False
        '
        'optShowAll
        '
        Me.optShowAll.BackColor = System.Drawing.Color.White
        Me.optShowAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.optShowAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optShowAll.Location = New System.Drawing.Point(8, 15)
        Me.optShowAll.Name = "optShowAll"
        Me.optShowAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optShowAll.Size = New System.Drawing.Size(169, 17)
        Me.optShowAll.TabIndex = 4
        Me.optShowAll.Text = "60159 - All"
        Me.optShowAll.UseVisualStyleBackColor = False
        '
        'optShowMatched
        '
        Me.optShowMatched.BackColor = System.Drawing.Color.White
        Me.optShowMatched.Cursor = System.Windows.Forms.Cursors.Default
        Me.optShowMatched.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optShowMatched.Location = New System.Drawing.Point(8, 55)
        Me.optShowMatched.Name = "optShowMatched"
        Me.optShowMatched.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optShowMatched.Size = New System.Drawing.Size(121, 17)
        Me.optShowMatched.TabIndex = 6
        Me.optShowMatched.Text = "60165 - Matched"
        Me.optShowMatched.UseVisualStyleBackColor = False
        '
        'imgMatched
        '
        Me.imgMatched.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgMatched.Image = CType(resources.GetObject("imgMatched.Image"), System.Drawing.Image)
        Me.imgMatched.Location = New System.Drawing.Point(144, 54)
        Me.imgMatched.Name = "imgMatched"
        Me.imgMatched.Size = New System.Drawing.Size(16, 16)
        Me.imgMatched.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgMatched.TabIndex = 11
        Me.imgMatched.TabStop = False
        '
        'imgProposed
        '
        Me.imgProposed.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgProposed.Image = CType(resources.GetObject("imgProposed.Image"), System.Drawing.Image)
        Me.imgProposed.Location = New System.Drawing.Point(342, 14)
        Me.imgProposed.Name = "imgProposed"
        Me.imgProposed.Size = New System.Drawing.Size(16, 16)
        Me.imgProposed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgProposed.TabIndex = 12
        Me.imgProposed.TabStop = False
        '
        'imgPartly
        '
        Me.imgPartly.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgPartly.Image = CType(resources.GetObject("imgPartly.Image"), System.Drawing.Image)
        Me.imgPartly.Location = New System.Drawing.Point(342, 35)
        Me.imgPartly.Name = "imgPartly"
        Me.imgPartly.Size = New System.Drawing.Size(17, 16)
        Me.imgPartly.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgPartly.TabIndex = 13
        Me.imgPartly.TabStop = False
        '
        'lblSearchAmount
        '
        Me.lblSearchAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSearchAmount.BackColor = System.Drawing.SystemColors.Control
        Me.lblSearchAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSearchAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSearchAmount.Location = New System.Drawing.Point(392, 516)
        Me.lblSearchAmount.Name = "lblSearchAmount"
        Me.lblSearchAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSearchAmount.Size = New System.Drawing.Size(105, 17)
        Me.lblSearchAmount.TabIndex = 14
        Me.lblSearchAmount.Text = "60179 Searchamount"
        '
        'LblSearchName
        '
        Me.LblSearchName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblSearchName.BackColor = System.Drawing.SystemColors.Control
        Me.LblSearchName.Cursor = System.Windows.Forms.Cursors.Default
        Me.LblSearchName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LblSearchName.Location = New System.Drawing.Point(392, 494)
        Me.LblSearchName.Name = "LblSearchName"
        Me.LblSearchName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LblSearchName.Size = New System.Drawing.Size(105, 17)
        Me.LblSearchName.TabIndex = 11
        Me.LblSearchName.Text = "60178 Searchname"
        '
        'txtLabelStored
        '
        Me.txtLabelStored.BackColor = System.Drawing.SystemColors.Control
        Me.txtLabelStored.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtLabelStored.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLabelStored.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtLabelStored.Location = New System.Drawing.Point(24, 38)
        Me.txtLabelStored.Name = "txtLabelStored"
        Me.txtLabelStored.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLabelStored.Size = New System.Drawing.Size(665, 15)
        Me.txtLabelStored.TabIndex = 10
        Me.txtLabelStored.Text = "Label1"
        '
        'gridStoredPayments
        '
        Me.gridStoredPayments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridStoredPayments.Location = New System.Drawing.Point(27, 57)
        Me.gridStoredPayments.Name = "gridStoredPayments"
        Me.gridStoredPayments.Size = New System.Drawing.Size(641, 150)
        Me.gridStoredPayments.TabIndex = 0
        '
        'gridStoredMatched
        '
        Me.gridStoredMatched.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridStoredMatched.Location = New System.Drawing.Point(29, 253)
        Me.gridStoredMatched.Name = "gridStoredMatched"
        Me.gridStoredMatched.Size = New System.Drawing.Size(639, 87)
        Me.gridStoredMatched.TabIndex = 19
        '
        'lblLine2
        '
        Me.lblLine2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLine2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine2.Location = New System.Drawing.Point(10, 565)
        Me.lblLine2.Name = "lblLine2"
        Me.lblLine2.Size = New System.Drawing.Size(705, 1)
        Me.lblLine2.TabIndex = 88
        Me.lblLine2.Text = "Label1"
        '
        'frmStoredPayments
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(725, 615)
        Me.Controls.Add(Me.lblLine2)
        Me.Controls.Add(Me.gridStoredMatched)
        Me.Controls.Add(Me.gridStoredPayments)
        Me.Controls.Add(Me.cmdSearchAmount)
        Me.Controls.Add(Me.txtSearchAmount)
        Me.Controls.Add(Me.cmdSearchName)
        Me.Controls.Add(Me.txtSearchName)
        Me.Controls.Add(Me.cmdChoose)
        Me.Controls.Add(Me.cmdPrintThis)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.Frame1)
        Me.Controls.Add(Me.lblSearchAmount)
        Me.Controls.Add(Me.LblSearchName)
        Me.Controls.Add(Me.txtLabelStored)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmStoredPayments"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Form1"
        Me.Frame1.ResumeLayout(False)
        CType(Me.imgOmit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgEmptyImage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgProposed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgPartly, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridStoredPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridStoredMatched, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gridStoredPayments As System.Windows.Forms.DataGridView
    Friend WithEvents gridStoredMatched As System.Windows.Forms.DataGridView
    Friend WithEvents imgEmptyImage As System.Windows.Forms.PictureBox
    Public WithEvents optShowOmit As System.Windows.Forms.RadioButton
    Public WithEvents imgOmit As System.Windows.Forms.PictureBox
    Friend WithEvents lblLine2 As System.Windows.Forms.Label
#End Region 
End Class