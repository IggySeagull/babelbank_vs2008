<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmArchiveSetup
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents chkActivate As System.Windows.Forms.CheckBox
	Public WithEvents CmdTest As System.Windows.Forms.Button
	Public WithEvents cmdUse As System.Windows.Forms.Button
	Public WithEvents chkIncludeOCR As System.Windows.Forms.CheckBox
	Public WithEvents cmbDatabaseType As System.Windows.Forms.ComboBox
	Public WithEvents txtConnstring As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents lblTestConnection As System.Windows.Forms.Label
	Public WithEvents lblDatabaseType As System.Windows.Forms.Label
	Public WithEvents lblConnString As System.Windows.Forms.Label
    Public WithEvents lblExplaination As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkActivate = New System.Windows.Forms.CheckBox
        Me.CmdTest = New System.Windows.Forms.Button
        Me.cmdUse = New System.Windows.Forms.Button
        Me.chkIncludeOCR = New System.Windows.Forms.CheckBox
        Me.cmbDatabaseType = New System.Windows.Forms.ComboBox
        Me.txtConnstring = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.lblTestConnection = New System.Windows.Forms.Label
        Me.lblDatabaseType = New System.Windows.Forms.Label
        Me.lblConnString = New System.Windows.Forms.Label
        Me.lblExplaination = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.txtUserID = New System.Windows.Forms.TextBox
        Me.lblPassword = New System.Windows.Forms.Label
        Me.lblUserID = New System.Windows.Forms.Label
        Me.cmbProvider = New System.Windows.Forms.ComboBox
        Me.lblProvider = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'chkActivate
        '
        Me.chkActivate.BackColor = System.Drawing.SystemColors.Control
        Me.chkActivate.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkActivate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkActivate.Location = New System.Drawing.Point(192, 136)
        Me.chkActivate.Name = "chkActivate"
        Me.chkActivate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkActivate.Size = New System.Drawing.Size(209, 17)
        Me.chkActivate.TabIndex = 1
        Me.chkActivate.Text = "60235 - Activate archiving"
        Me.chkActivate.UseVisualStyleBackColor = False
        '
        'CmdTest
        '
        Me.CmdTest.BackColor = System.Drawing.SystemColors.Control
        Me.CmdTest.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdTest.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdTest.Location = New System.Drawing.Point(669, 288)
        Me.CmdTest.Name = "CmdTest"
        Me.CmdTest.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdTest.Size = New System.Drawing.Size(73, 21)
        Me.CmdTest.TabIndex = 3
        Me.CmdTest.Text = "&Test"
        Me.CmdTest.UseVisualStyleBackColor = False
        '
        'cmdUse
        '
        Me.cmdUse.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUse.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdUse.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdUse.Location = New System.Drawing.Point(669, 508)
        Me.cmdUse.Name = "cmdUse"
        Me.cmdUse.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdUse.Size = New System.Drawing.Size(73, 21)
        Me.cmdUse.TabIndex = 10
        Me.cmdUse.Text = "60234 -Use"
        Me.cmdUse.UseVisualStyleBackColor = False
        '
        'chkIncludeOCR
        '
        Me.chkIncludeOCR.BackColor = System.Drawing.SystemColors.Control
        Me.chkIncludeOCR.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkIncludeOCR.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkIncludeOCR.Location = New System.Drawing.Point(192, 365)
        Me.chkIncludeOCR.Name = "chkIncludeOCR"
        Me.chkIncludeOCR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkIncludeOCR.Size = New System.Drawing.Size(209, 17)
        Me.chkIncludeOCR.TabIndex = 5
        Me.chkIncludeOCR.Text = "60233 - Include OCR"
        Me.chkIncludeOCR.UseVisualStyleBackColor = False
        '
        'cmbDatabaseType
        '
        Me.cmbDatabaseType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbDatabaseType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbDatabaseType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbDatabaseType.Location = New System.Drawing.Point(336, 248)
        Me.cmbDatabaseType.Name = "cmbDatabaseType"
        Me.cmbDatabaseType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbDatabaseType.Size = New System.Drawing.Size(409, 21)
        Me.cmbDatabaseType.TabIndex = 4
        '
        'txtConnstring
        '
        Me.txtConnstring.AcceptsReturn = True
        Me.txtConnstring.BackColor = System.Drawing.SystemColors.Window
        Me.txtConnstring.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtConnstring.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtConnstring.Location = New System.Drawing.Point(336, 168)
        Me.txtConnstring.MaxLength = 0
        Me.txtConnstring.Multiline = True
        Me.txtConnstring.Name = "txtConnstring"
        Me.txtConnstring.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtConnstring.Size = New System.Drawing.Size(409, 73)
        Me.txtConnstring.TabIndex = 2
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(513, 508)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 6
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(591, 508)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 7
        Me.cmdCancel.Text = "55006 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'lblTestConnection
        '
        Me.lblTestConnection.BackColor = System.Drawing.SystemColors.Control
        Me.lblTestConnection.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTestConnection.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblTestConnection.Location = New System.Drawing.Point(192, 395)
        Me.lblTestConnection.Name = "lblTestConnection"
        Me.lblTestConnection.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTestConnection.Size = New System.Drawing.Size(553, 89)
        Me.lblTestConnection.TabIndex = 11
        '
        'lblDatabaseType
        '
        Me.lblDatabaseType.BackColor = System.Drawing.SystemColors.Control
        Me.lblDatabaseType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDatabaseType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDatabaseType.Location = New System.Drawing.Point(192, 248)
        Me.lblDatabaseType.Name = "lblDatabaseType"
        Me.lblDatabaseType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDatabaseType.Size = New System.Drawing.Size(137, 17)
        Me.lblDatabaseType.TabIndex = 9
        Me.lblDatabaseType.Text = "60232 - Databasetype"
        '
        'lblConnString
        '
        Me.lblConnString.BackColor = System.Drawing.SystemColors.Control
        Me.lblConnString.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblConnString.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblConnString.Location = New System.Drawing.Point(192, 168)
        Me.lblConnString.Name = "lblConnString"
        Me.lblConnString.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblConnString.Size = New System.Drawing.Size(129, 17)
        Me.lblConnString.TabIndex = 8
        Me.lblConnString.Text = "25004 - Conn.string"
        '
        'lblExplaination
        '
        Me.lblExplaination.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplaination.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplaination.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplaination.Location = New System.Drawing.Point(200, 64)
        Me.lblExplaination.Name = "lblExplaination"
        Me.lblExplaination.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplaination.Size = New System.Drawing.Size(544, 57)
        Me.lblExplaination.TabIndex = 0
        Me.lblExplaination.Text = "60231 - Explaination"
        '
        'txtPassword
        '
        Me.txtPassword.AcceptsReturn = True
        Me.txtPassword.BackColor = System.Drawing.SystemColors.Window
        Me.txtPassword.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPassword.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtPassword.Location = New System.Drawing.Point(336, 336)
        Me.txtPassword.MaxLength = 50
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPassword.Size = New System.Drawing.Size(185, 20)
        Me.txtPassword.TabIndex = 13
        '
        'txtUserID
        '
        Me.txtUserID.AcceptsReturn = True
        Me.txtUserID.BackColor = System.Drawing.SystemColors.Window
        Me.txtUserID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtUserID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtUserID.Location = New System.Drawing.Point(336, 307)
        Me.txtUserID.MaxLength = 30
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtUserID.Size = New System.Drawing.Size(185, 20)
        Me.txtUserID.TabIndex = 14
        '
        'lblPassword
        '
        Me.lblPassword.BackColor = System.Drawing.Color.Transparent
        Me.lblPassword.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPassword.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPassword.Location = New System.Drawing.Point(192, 336)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPassword.Size = New System.Drawing.Size(129, 17)
        Me.lblPassword.TabIndex = 16
        Me.lblPassword.Text = "25007 - Password:"
        '
        'lblUserID
        '
        Me.lblUserID.BackColor = System.Drawing.Color.Transparent
        Me.lblUserID.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblUserID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblUserID.Location = New System.Drawing.Point(192, 307)
        Me.lblUserID.Name = "lblUserID"
        Me.lblUserID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblUserID.Size = New System.Drawing.Size(121, 17)
        Me.lblUserID.TabIndex = 15
        Me.lblUserID.Text = "25006 - User ID:"
        '
        'cmbProvider
        '
        Me.cmbProvider.BackColor = System.Drawing.SystemColors.Window
        Me.cmbProvider.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbProvider.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbProvider.Location = New System.Drawing.Point(336, 278)
        Me.cmbProvider.Name = "cmbProvider"
        Me.cmbProvider.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbProvider.Size = New System.Drawing.Size(249, 21)
        Me.cmbProvider.TabIndex = 19
        '
        'lblProvider
        '
        Me.lblProvider.BackColor = System.Drawing.Color.Transparent
        Me.lblProvider.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProvider.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProvider.Location = New System.Drawing.Point(192, 278)
        Me.lblProvider.Name = "lblProvider"
        Me.lblProvider.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProvider.Size = New System.Drawing.Size(137, 17)
        Me.lblProvider.TabIndex = 18
        Me.lblProvider.Text = "Provider"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 500)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(740, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmArchiveSetup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(760, 538)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmbProvider)
        Me.Controls.Add(Me.lblProvider)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtUserID)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.lblUserID)
        Me.Controls.Add(Me.chkActivate)
        Me.Controls.Add(Me.CmdTest)
        Me.Controls.Add(Me.cmdUse)
        Me.Controls.Add(Me.chkIncludeOCR)
        Me.Controls.Add(Me.cmbDatabaseType)
        Me.Controls.Add(Me.txtConnstring)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.lblTestConnection)
        Me.Controls.Add(Me.lblDatabaseType)
        Me.Controls.Add(Me.lblConnString)
        Me.Controls.Add(Me.lblExplaination)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmArchiveSetup"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "50711 - Archive setup"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents txtPassword As System.Windows.Forms.TextBox
    Public WithEvents txtUserID As System.Windows.Forms.TextBox
    Public WithEvents lblPassword As System.Windows.Forms.Label
    Public WithEvents lblUserID As System.Windows.Forms.Label
    Public WithEvents cmbProvider As System.Windows.Forms.ComboBox
    Public WithEvents lblProvider As System.Windows.Forms.Label
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
