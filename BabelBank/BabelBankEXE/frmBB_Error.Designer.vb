﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBB_Error
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdEMail = New System.Windows.Forms.Button
        Me.lblSource = New System.Windows.Forms.Label
        Me.lblErrorNo = New System.Windows.Forms.Label
        Me.lblErrorMsg = New System.Windows.Forms.Label
        Me.picError = New System.Windows.Forms.PictureBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblSourceContent = New System.Windows.Forms.Label
        Me.lblErrorNoContent = New System.Windows.Forms.Label
        Me.cmdSaveCallstack = New System.Windows.Forms.Button
        Me.lblCallstack = New System.Windows.Forms.Label
        Me.txtErrorMsgContent = New System.Windows.Forms.TextBox
        CType(Me.picError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdEMail
        '
        Me.cmdEMail.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdEMail.Location = New System.Drawing.Point(453, 281)
        Me.cmdEMail.Name = "cmdEMail"
        Me.cmdEMail.Size = New System.Drawing.Size(110, 23)
        Me.cmdEMail.TabIndex = 2
        Me.cmdEMail.Text = "50903-Supportmail"
        '
        'lblSource
        '
        Me.lblSource.AutoSize = True
        Me.lblSource.Location = New System.Drawing.Point(126, 76)
        Me.lblSource.Name = "lblSource"
        Me.lblSource.Size = New System.Drawing.Size(74, 13)
        Me.lblSource.TabIndex = 4
        Me.lblSource.Text = "15010-Source"
        '
        'lblErrorNo
        '
        Me.lblErrorNo.AutoSize = True
        Me.lblErrorNo.Location = New System.Drawing.Point(126, 100)
        Me.lblErrorNo.Name = "lblErrorNo"
        Me.lblErrorNo.Size = New System.Drawing.Size(76, 13)
        Me.lblErrorNo.TabIndex = 6
        Me.lblErrorNo.Text = "15003-ErrorNo"
        '
        'lblErrorMsg
        '
        Me.lblErrorMsg.AutoSize = True
        Me.lblErrorMsg.Location = New System.Drawing.Point(126, 128)
        Me.lblErrorMsg.Name = "lblErrorMsg"
        Me.lblErrorMsg.Size = New System.Drawing.Size(82, 13)
        Me.lblErrorMsg.TabIndex = 8
        Me.lblErrorMsg.Text = "15030-ErrorMsg"
        '
        'picError
        '
        Me.picError.BackColor = System.Drawing.Color.Transparent
        Me.picError.Image = Global.BabelBank.My.Resources.Resources.application_exit
        Me.picError.Location = New System.Drawing.Point(29, 76)
        Me.picError.Name = "picError"
        Me.picError.Size = New System.Drawing.Size(54, 53)
        Me.picError.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picError.TabIndex = 12
        Me.picError.TabStop = False
        '
        'cmdOK
        '
        Me.cmdOK.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdOK.Location = New System.Drawing.Point(564, 281)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(67, 23)
        Me.cmdOK.TabIndex = 13
        Me.cmdOK.Text = "&OK"
        '
        'lblHeading
        '
        Me.lblHeading.AutoSize = True
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.Location = New System.Drawing.Point(238, 29)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.Size = New System.Drawing.Size(235, 16)
        Me.lblHeading.TabIndex = 15
        Me.lblHeading.Text = "15031-Feilmelding fra Babelbank"
        '
        'lblSourceContent
        '
        Me.lblSourceContent.AutoSize = True
        Me.lblSourceContent.Location = New System.Drawing.Point(230, 76)
        Me.lblSourceContent.Name = "lblSourceContent"
        Me.lblSourceContent.Size = New System.Drawing.Size(41, 13)
        Me.lblSourceContent.TabIndex = 18
        Me.lblSourceContent.Text = "Source"
        '
        'lblErrorNoContent
        '
        Me.lblErrorNoContent.AutoSize = True
        Me.lblErrorNoContent.Location = New System.Drawing.Point(230, 100)
        Me.lblErrorNoContent.Name = "lblErrorNoContent"
        Me.lblErrorNoContent.Size = New System.Drawing.Size(43, 13)
        Me.lblErrorNoContent.TabIndex = 19
        Me.lblErrorNoContent.Text = "ErrorNo"
        '
        'cmdSaveCallstack
        '
        Me.cmdSaveCallstack.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdSaveCallstack.Location = New System.Drawing.Point(342, 281)
        Me.cmdSaveCallstack.Name = "cmdSaveCallstack"
        Me.cmdSaveCallstack.Size = New System.Drawing.Size(110, 23)
        Me.cmdSaveCallstack.TabIndex = 21
        Me.cmdSaveCallstack.Text = "55050-Save callstack"
        '
        'lblCallstack
        '
        Me.lblCallstack.AutoSize = True
        Me.lblCallstack.Location = New System.Drawing.Point(126, 259)
        Me.lblCallstack.Name = "lblCallstack"
        Me.lblCallstack.Size = New System.Drawing.Size(50, 13)
        Me.lblCallstack.TabIndex = 22
        Me.lblCallstack.Text = "Callstack"
        '
        'txtErrorMsgContent
        '
        Me.txtErrorMsgContent.Location = New System.Drawing.Point(233, 129)
        Me.txtErrorMsgContent.Multiline = True
        Me.txtErrorMsgContent.Name = "txtErrorMsgContent"
        Me.txtErrorMsgContent.ReadOnly = True
        Me.txtErrorMsgContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtErrorMsgContent.Size = New System.Drawing.Size(397, 143)
        Me.txtErrorMsgContent.TabIndex = 23
        '
        'frmBB_Error
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(657, 316)
        Me.Controls.Add(Me.txtErrorMsgContent)
        Me.Controls.Add(Me.lblCallstack)
        Me.Controls.Add(Me.cmdSaveCallstack)
        Me.Controls.Add(Me.lblErrorNoContent)
        Me.Controls.Add(Me.lblSourceContent)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.picError)
        Me.Controls.Add(Me.lblErrorMsg)
        Me.Controls.Add(Me.lblErrorNo)
        Me.Controls.Add(Me.lblSource)
        Me.Controls.Add(Me.cmdEMail)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBB_Error"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "15031-Feilmelding fra BabelBank"
        Me.TopMost = True
        CType(Me.picError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdEMail As System.Windows.Forms.Button
    Friend WithEvents lblSource As System.Windows.Forms.Label
    Friend WithEvents lblErrorNo As System.Windows.Forms.Label
    Friend WithEvents lblErrorMsg As System.Windows.Forms.Label
    Friend WithEvents picError As System.Windows.Forms.PictureBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents lblHeading As System.Windows.Forms.Label
    Friend WithEvents lblSourceContent As System.Windows.Forms.Label
    Friend WithEvents lblErrorNoContent As System.Windows.Forms.Label
    Friend WithEvents cmdSaveCallstack As System.Windows.Forms.Button
    Friend WithEvents lblCallstack As System.Windows.Forms.Label
    Friend WithEvents txtErrorMsgContent As System.Windows.Forms.TextBox

End Class
