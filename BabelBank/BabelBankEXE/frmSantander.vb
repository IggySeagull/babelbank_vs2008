Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmSantander
	Inherits System.Windows.Forms.Form
	Private sCompanyID As String
    Private oDAL As vbBabel.DAL
    Private bMsgBoxShown As Boolean
	
	Friend Function SetCompanyID(ByRef iCompanyID As Object) As Boolean
		
		'UPGRADE_WARNING: Couldn't resolve default property of object iCompanyID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		sCompanyID = Trim(Str(iCompanyID))
		
	End Function
	
	Private Sub cmdClose_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdClose.Click
		
		Me.Hide()
		
	End Sub
	
	Private Sub cmdSearch_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSearch.Click
		
		RunSQL()
		
    End Sub

    Private Sub frmSantander_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If Not oDAL Is Nothing Then
            oDAL.Close()
            oDAL = Nothing
        End If
    End Sub
	
	Private Sub frmSantander_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        Try
            FormvbStyle(Me, "")

            oDAL = New vbBabel.DAL
            oDAL.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
            If Not oDAL.ConnectToDB() Then
                Throw New System.Exception(oDAL.ErrorMessage)
            End If

        Catch ex As Exception

            If Not oDAL Is Nothing Then
                oDAL.Close()
                oDAL = Nothing
            End If

            Throw New Exception("Function: frmOmitAccounts.Load" & vbCrLf & ex.Message)

        End Try

        bMsgBoxShown = False
		
	End Sub
	
	Private Sub RunSQL()
        Dim bSearchCriteriaFound As Boolean
		Dim sString As String
		Dim sMySQL As String
		Dim lCounter As Integer
		
		Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
		
		bMsgBoxShown = False
		
		For lCounter = Me.lstResult.Items.Count - 1 To 0 Step -1
			Me.lstResult.Items.RemoveAt(lCounter)
		Next lCounter
		
		bSearchCriteriaFound = False
        sMySQL = ""
        sMySQL = "SELECT A.KontoNr AS BBRET_AccountNo, A.SistBruktDt AS SistBrukt, AntallGanger AS Antall FROM Kontrakt K "
        sMySQL = sMySQL & "LEFT OUTER JOIN RegistreringsNr R ON K.La_Ar = R.La_Ar AND K.La_LanNr = R.La_LanNr, "
        sMySQL = sMySQL & "Part P LEFT OUTER JOIN KontoNr A ON P.PartNr = A.PartNr "
        sMySQL = sMySQL & "WHERE K.Lantaker = P.PartNr AND "
        If Me.optVILE.Checked = True Then
            sMySQL = sMySQL & "K.Portefolje = '0'"
        Else
            sMySQL = sMySQL & "K.Portefolje = '2'"
        End If

        If Not EmptyString((Me.txtSaksnummer).Text) Then
            sMySQL = sMySQL & " AND K.LA_Ar = SUBSTRING('" & Trim(Me.txtSaksnummer.Text) & "', 1, 2) AND K.LA_Lannr = SUBSTRING(REPLACE(REPLACE('" & Trim(Me.txtSaksnummer.Text) & "','-',''), ' ', ''), 3, 5)"
            bSearchCriteriaFound = True
        End If

        If Not EmptyString((Me.txtRegnr).Text) Then
            sMySQL = sMySQL & " AND R.RegistreringsNr = REPLACE(REPLACE('" & Trim(Me.txtRegnr.Text) & "', ' ', ''), '-', '')"
            bSearchCriteriaFound = True
        End If

        If Not EmptyString((Me.txtOrgPersnr).Text) Then
            If Not bSearchCriteriaFound Then
                sMySQL = Replace(sMySQL, "SELECT ", "SELECT DISTINCT ")
            End If
            sMySQL = sMySQL & " AND (P.ForetaksNr = '" & Trim(Me.txtOrgPersnr.Text) & "' Or P.FodselsNr = '" & Trim(Me.txtOrgPersnr.Text) & "')"
            bSearchCriteriaFound = True
        End If

        If bSearchCriteriaFound Then
            sMySQL = sMySQL & " ORDER BY A.SistBruktDt DESC"

            oDAL.SQL = sMySQL
            If oDAL.Reader_Execute() Then
                If oDAL.Reader_HasRows Then
                    Do While oDAL.Reader_ReadRecord
                        sString = ""
                        sString = VB.Left(oDAL.Reader_GetString("BBRET_AccountNo"), 4) & "." & Mid(oDAL.Reader_GetString("BBRET_AccountNo"), 5, 2) & "." & Mid(oDAL.Reader_GetString("BBRET_AccountNo"), 7) & "         "
                        sString = sString & oDAL.Reader_GetString("SistBrukt") & "     "
                        sString = sString & oDAL.Reader_GetString("Antall")
                        Me.lstResult.Items.Add(sString)
                    Loop
                Else
                    MsgBox("Ingen kontonummer funnet for angitte s�kekriterie(r)", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Ingen poster funnet")
                    bMsgBoxShown = True
                End If
            Else
                Throw New Exception(LRSCommon(45002) & oDAL.ErrorMessage)
            End If
        Else
            MsgBox("Ingen s�kekriterier er angitt", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Feil i s�k")
            bMsgBoxShown = True
        End If

        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    'UPGRADE_WARNING: Event lstResult.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub lstResult_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstResult.SelectedIndexChanged

        My.Computer.Clipboard.SetText(Replace(Replace(VB.Left(Me.lstResult.Text, 16), " ", ""), ".", ""))

    End Sub
	
	Private Sub txtSaksnummer_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSaksnummer.Enter
		Me.txtSaksnummer.SelectionStart = 0
		Me.txtSaksnummer.SelectionLength = Len(Me.txtSaksnummer.Text)
		
		bMsgBoxShown = False
	End Sub
	
	Private Sub txtSaksnummer_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSaksnummer.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		
		If KeyCode = System.Windows.Forms.Keys.Return Then
			If bMsgBoxShown Then
				'Don't run the SQL again
				bMsgBoxShown = False
				Me.txtSaksnummer.SelectionStart = 0
				Me.txtSaksnummer.SelectionLength = Len(Me.txtSaksnummer.Text)
			Else
				RunSQL()
			End If
		End If
		
	End Sub
	Private Sub txtRegnr_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtRegnr.Enter
		Me.txtRegnr.SelectionStart = 0
		Me.txtRegnr.SelectionLength = Len(Me.txtRegnr.Text)
		
		bMsgBoxShown = False
	End Sub
	
	Private Sub txtRegnr_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtRegnr.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		
		If KeyCode = System.Windows.Forms.Keys.Return Then
			If bMsgBoxShown Then
				'Don't run the SQL again
				bMsgBoxShown = False
				Me.txtRegnr.SelectionStart = 0
				Me.txtRegnr.SelectionLength = Len(Me.txtRegnr.Text)
			Else
				RunSQL()
			End If
		End If
		
	End Sub
	Private Sub txtOrgPersnr_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOrgPersnr.Enter
		Me.txtOrgPersnr.SelectionStart = 0
		Me.txtOrgPersnr.SelectionLength = Len(Me.txtOrgPersnr.Text)
		
		bMsgBoxShown = False
	End Sub
	
	Private Sub txtOrgPersnr_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtOrgPersnr.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		
		If KeyCode = System.Windows.Forms.Keys.Return Then
			If bMsgBoxShown Then
				'Don't run the SQL again
				bMsgBoxShown = False
				Me.txtOrgPersnr.SelectionStart = 0
				Me.txtOrgPersnr.SelectionLength = Len(Me.txtOrgPersnr.Text)
			Else
				RunSQL()
			End If
		End If
		
	End Sub
End Class
