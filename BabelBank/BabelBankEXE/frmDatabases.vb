Option Strict Off
Option Explicit On
Friend Class frmDatabases
	Inherits System.Windows.Forms.Form
	
	Private Sub frmDatabases_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		'FormvbStyle Me, "dollar.jpg"
		FormLRSCaptions(Me)
		Me.cmdCancel.Text = LRSCommon(50001) ' &Cancel
	End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Hide()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Hide()
    End Sub
End Class
