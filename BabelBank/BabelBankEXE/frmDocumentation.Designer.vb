<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDocumentation
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdContinue As System.Windows.Forms.Button
	Public WithEvents txtEndHeader As System.Windows.Forms.TextBox
	Public WithEvents txtEndBody As System.Windows.Forms.TextBox
	Public WithEvents txtMatchingHeader As System.Windows.Forms.TextBox
	Public WithEvents txtMatchingBody As System.Windows.Forms.TextBox
	Public WithEvents txtClientsHeader As System.Windows.Forms.TextBox
	Public WithEvents txtClientsBody As System.Windows.Forms.TextBox
	Public WithEvents txtProfilesHeader As System.Windows.Forms.TextBox
	Public WithEvents txtProfilesBody As System.Windows.Forms.TextBox
	Public WithEvents txtStartHeader As System.Windows.Forms.TextBox
	Public WithEvents txtStartBody As System.Windows.Forms.TextBox
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDocumentation))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.cmdCancel = New System.Windows.Forms.Button
		Me.cmdContinue = New System.Windows.Forms.Button
		Me.txtEndHeader = New System.Windows.Forms.TextBox
		Me.txtEndBody = New System.Windows.Forms.TextBox
		Me.txtMatchingHeader = New System.Windows.Forms.TextBox
		Me.txtMatchingBody = New System.Windows.Forms.TextBox
		Me.txtClientsHeader = New System.Windows.Forms.TextBox
		Me.txtClientsBody = New System.Windows.Forms.TextBox
		Me.txtProfilesHeader = New System.Windows.Forms.TextBox
		Me.txtProfilesBody = New System.Windows.Forms.TextBox
		Me.txtStartHeader = New System.Windows.Forms.TextBox
		Me.txtStartBody = New System.Windows.Forms.TextBox
		Me.Label5 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.Text = "BabelBank documentation"
		Me.ClientSize = New System.Drawing.Size(642, 639)
		Me.Location = New System.Drawing.Point(4, 30)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmDocumentation"
		Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdCancel.Text = "&Avbryt"
		Me.cmdCancel.Size = New System.Drawing.Size(113, 26)
		Me.cmdCancel.Location = New System.Drawing.Point(376, 606)
		Me.cmdCancel.TabIndex = 16
		Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.cmdCancel.CausesValidation = True
		Me.cmdCancel.Enabled = True
		Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdCancel.TabStop = True
		Me.cmdCancel.Name = "cmdCancel"
		Me.cmdContinue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdContinue.Text = "&Lag dokumentasjon"
		Me.cmdContinue.Size = New System.Drawing.Size(113, 26)
		Me.cmdContinue.Location = New System.Drawing.Point(494, 605)
		Me.cmdContinue.TabIndex = 10
		Me.cmdContinue.BackColor = System.Drawing.SystemColors.Control
		Me.cmdContinue.CausesValidation = True
		Me.cmdContinue.Enabled = True
		Me.cmdContinue.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdContinue.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdContinue.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdContinue.TabStop = True
		Me.cmdContinue.Name = "cmdContinue"
		Me.txtEndHeader.AutoSize = False
		Me.txtEndHeader.Size = New System.Drawing.Size(395, 23)
		Me.txtEndHeader.Location = New System.Drawing.Point(213, 484)
		Me.txtEndHeader.TabIndex = 8
		Me.txtEndHeader.Text = "<Overskriftsluttekst>"
		Me.txtEndHeader.AcceptsReturn = True
		Me.txtEndHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEndHeader.BackColor = System.Drawing.SystemColors.Window
		Me.txtEndHeader.CausesValidation = True
		Me.txtEndHeader.Enabled = True
		Me.txtEndHeader.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEndHeader.HideSelection = True
		Me.txtEndHeader.ReadOnly = False
		Me.txtEndHeader.Maxlength = 0
		Me.txtEndHeader.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEndHeader.MultiLine = False
		Me.txtEndHeader.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEndHeader.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEndHeader.TabStop = True
		Me.txtEndHeader.Visible = True
		Me.txtEndHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtEndHeader.Name = "txtEndHeader"
		Me.txtEndBody.AutoSize = False
		Me.txtEndBody.Size = New System.Drawing.Size(580, 86)
		Me.txtEndBody.Location = New System.Drawing.Point(28, 512)
		Me.txtEndBody.MultiLine = True
		Me.txtEndBody.TabIndex = 9
		Me.txtEndBody.Text = "<Fyll inn tekst som skal vises nederst i dokumentasjonen>"
		Me.txtEndBody.AcceptsReturn = True
		Me.txtEndBody.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEndBody.BackColor = System.Drawing.SystemColors.Window
		Me.txtEndBody.CausesValidation = True
		Me.txtEndBody.Enabled = True
		Me.txtEndBody.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEndBody.HideSelection = True
		Me.txtEndBody.ReadOnly = False
		Me.txtEndBody.Maxlength = 0
		Me.txtEndBody.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEndBody.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEndBody.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEndBody.TabStop = True
		Me.txtEndBody.Visible = True
		Me.txtEndBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtEndBody.Name = "txtEndBody"
		Me.txtMatchingHeader.AutoSize = False
		Me.txtMatchingHeader.Size = New System.Drawing.Size(395, 23)
		Me.txtMatchingHeader.Location = New System.Drawing.Point(211, 365)
		Me.txtMatchingHeader.TabIndex = 6
		Me.txtMatchingHeader.Text = "<Overskrift avstemmingstekst>"
		Me.txtMatchingHeader.AcceptsReturn = True
		Me.txtMatchingHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMatchingHeader.BackColor = System.Drawing.SystemColors.Window
		Me.txtMatchingHeader.CausesValidation = True
		Me.txtMatchingHeader.Enabled = True
		Me.txtMatchingHeader.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMatchingHeader.HideSelection = True
		Me.txtMatchingHeader.ReadOnly = False
		Me.txtMatchingHeader.Maxlength = 0
		Me.txtMatchingHeader.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMatchingHeader.MultiLine = False
		Me.txtMatchingHeader.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMatchingHeader.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMatchingHeader.TabStop = True
		Me.txtMatchingHeader.Visible = True
		Me.txtMatchingHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtMatchingHeader.Name = "txtMatchingHeader"
		Me.txtMatchingBody.AutoSize = False
		Me.txtMatchingBody.Size = New System.Drawing.Size(580, 82)
		Me.txtMatchingBody.Location = New System.Drawing.Point(28, 394)
		Me.txtMatchingBody.MultiLine = True
		Me.txtMatchingBody.TabIndex = 7
		Me.txtMatchingBody.Text = "<Fyll inn tekst som skal vises f�r avstemmingsregler>"
		Me.txtMatchingBody.AcceptsReturn = True
		Me.txtMatchingBody.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMatchingBody.BackColor = System.Drawing.SystemColors.Window
		Me.txtMatchingBody.CausesValidation = True
		Me.txtMatchingBody.Enabled = True
		Me.txtMatchingBody.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMatchingBody.HideSelection = True
		Me.txtMatchingBody.ReadOnly = False
		Me.txtMatchingBody.Maxlength = 0
		Me.txtMatchingBody.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMatchingBody.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMatchingBody.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMatchingBody.TabStop = True
		Me.txtMatchingBody.Visible = True
		Me.txtMatchingBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtMatchingBody.Name = "txtMatchingBody"
		Me.txtClientsHeader.AutoSize = False
		Me.txtClientsHeader.Size = New System.Drawing.Size(395, 23)
		Me.txtClientsHeader.Location = New System.Drawing.Point(211, 248)
		Me.txtClientsHeader.TabIndex = 4
		Me.txtClientsHeader.Text = "<Overskrift klienttekst>"
		Me.txtClientsHeader.AcceptsReturn = True
		Me.txtClientsHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtClientsHeader.BackColor = System.Drawing.SystemColors.Window
		Me.txtClientsHeader.CausesValidation = True
		Me.txtClientsHeader.Enabled = True
		Me.txtClientsHeader.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtClientsHeader.HideSelection = True
		Me.txtClientsHeader.ReadOnly = False
		Me.txtClientsHeader.Maxlength = 0
		Me.txtClientsHeader.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtClientsHeader.MultiLine = False
		Me.txtClientsHeader.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtClientsHeader.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtClientsHeader.TabStop = True
		Me.txtClientsHeader.Visible = True
		Me.txtClientsHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtClientsHeader.Name = "txtClientsHeader"
		Me.txtClientsBody.AutoSize = False
		Me.txtClientsBody.Size = New System.Drawing.Size(580, 83)
		Me.txtClientsBody.Location = New System.Drawing.Point(28, 277)
		Me.txtClientsBody.MultiLine = True
		Me.txtClientsBody.TabIndex = 5
		Me.txtClientsBody.Text = "<Fyll inn tekst som skal vises f�r klienter>"
		Me.txtClientsBody.AcceptsReturn = True
		Me.txtClientsBody.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtClientsBody.BackColor = System.Drawing.SystemColors.Window
		Me.txtClientsBody.CausesValidation = True
		Me.txtClientsBody.Enabled = True
		Me.txtClientsBody.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtClientsBody.HideSelection = True
		Me.txtClientsBody.ReadOnly = False
		Me.txtClientsBody.Maxlength = 0
		Me.txtClientsBody.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtClientsBody.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtClientsBody.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtClientsBody.TabStop = True
		Me.txtClientsBody.Visible = True
		Me.txtClientsBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtClientsBody.Name = "txtClientsBody"
		Me.txtProfilesHeader.AutoSize = False
		Me.txtProfilesHeader.Size = New System.Drawing.Size(395, 23)
		Me.txtProfilesHeader.Location = New System.Drawing.Point(211, 122)
		Me.txtProfilesHeader.TabIndex = 2
		Me.txtProfilesHeader.Text = "<Overskrift profiltekst>"
		Me.txtProfilesHeader.AcceptsReturn = True
		Me.txtProfilesHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtProfilesHeader.BackColor = System.Drawing.SystemColors.Window
		Me.txtProfilesHeader.CausesValidation = True
		Me.txtProfilesHeader.Enabled = True
		Me.txtProfilesHeader.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtProfilesHeader.HideSelection = True
		Me.txtProfilesHeader.ReadOnly = False
		Me.txtProfilesHeader.Maxlength = 0
		Me.txtProfilesHeader.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtProfilesHeader.MultiLine = False
		Me.txtProfilesHeader.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtProfilesHeader.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtProfilesHeader.TabStop = True
		Me.txtProfilesHeader.Visible = True
		Me.txtProfilesHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtProfilesHeader.Name = "txtProfilesHeader"
		Me.txtProfilesBody.AutoSize = False
		Me.txtProfilesBody.Size = New System.Drawing.Size(580, 91)
		Me.txtProfilesBody.Location = New System.Drawing.Point(28, 151)
		Me.txtProfilesBody.MultiLine = True
		Me.txtProfilesBody.TabIndex = 3
		Me.txtProfilesBody.Text = "<Fyll inn tekst som skal vises f�r profiler>"
		Me.txtProfilesBody.AcceptsReturn = True
		Me.txtProfilesBody.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtProfilesBody.BackColor = System.Drawing.SystemColors.Window
		Me.txtProfilesBody.CausesValidation = True
		Me.txtProfilesBody.Enabled = True
		Me.txtProfilesBody.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtProfilesBody.HideSelection = True
		Me.txtProfilesBody.ReadOnly = False
		Me.txtProfilesBody.Maxlength = 0
		Me.txtProfilesBody.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtProfilesBody.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtProfilesBody.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtProfilesBody.TabStop = True
		Me.txtProfilesBody.Visible = True
		Me.txtProfilesBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtProfilesBody.Name = "txtProfilesBody"
		Me.txtStartHeader.AutoSize = False
		Me.txtStartHeader.Size = New System.Drawing.Size(395, 23)
		Me.txtStartHeader.Location = New System.Drawing.Point(211, 3)
		Me.txtStartHeader.TabIndex = 0
		Me.txtStartHeader.Text = "<Overskrift starttekst>"
		Me.txtStartHeader.AcceptsReturn = True
		Me.txtStartHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtStartHeader.BackColor = System.Drawing.SystemColors.Window
		Me.txtStartHeader.CausesValidation = True
		Me.txtStartHeader.Enabled = True
		Me.txtStartHeader.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtStartHeader.HideSelection = True
		Me.txtStartHeader.ReadOnly = False
		Me.txtStartHeader.Maxlength = 0
		Me.txtStartHeader.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtStartHeader.MultiLine = False
		Me.txtStartHeader.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtStartHeader.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtStartHeader.TabStop = True
		Me.txtStartHeader.Visible = True
		Me.txtStartHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtStartHeader.Name = "txtStartHeader"
		Me.txtStartBody.AutoSize = False
		Me.txtStartBody.Size = New System.Drawing.Size(580, 84)
		Me.txtStartBody.Location = New System.Drawing.Point(28, 32)
		Me.txtStartBody.MultiLine = True
		Me.txtStartBody.TabIndex = 1
		Me.txtStartBody.Text = "<Fyll inn tekst som skal vises �verst i dokumentasjonen>"
		Me.txtStartBody.AcceptsReturn = True
		Me.txtStartBody.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtStartBody.BackColor = System.Drawing.SystemColors.Window
		Me.txtStartBody.CausesValidation = True
		Me.txtStartBody.Enabled = True
		Me.txtStartBody.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtStartBody.HideSelection = True
		Me.txtStartBody.ReadOnly = False
		Me.txtStartBody.Maxlength = 0
		Me.txtStartBody.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtStartBody.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtStartBody.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtStartBody.TabStop = True
		Me.txtStartBody.Visible = True
		Me.txtStartBody.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtStartBody.Name = "txtStartBody"
		Me.Label5.Text = "Avslutning Overskrift/tekst"
		Me.Label5.Size = New System.Drawing.Size(177, 19)
		Me.Label5.Location = New System.Drawing.Point(30, 488)
		Me.Label5.TabIndex = 15
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.Label4.Text = "Avstemming Overskrift/tekst"
		Me.Label4.Size = New System.Drawing.Size(177, 19)
		Me.Label4.Location = New System.Drawing.Point(30, 370)
		Me.Label4.TabIndex = 14
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label3.Text = "KlientOverskrift/tekst"
		Me.Label3.Size = New System.Drawing.Size(177, 19)
		Me.Label3.Location = New System.Drawing.Point(30, 253)
		Me.Label3.TabIndex = 13
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.Text = "Profil Overskrift/tekst"
		Me.Label2.Size = New System.Drawing.Size(177, 19)
		Me.Label2.Location = New System.Drawing.Point(30, 127)
		Me.Label2.TabIndex = 12
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Innledende Overskrift/tekst"
		Me.Label1.Size = New System.Drawing.Size(177, 19)
		Me.Label1.Location = New System.Drawing.Point(30, 8)
		Me.Label1.TabIndex = 11
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Controls.Add(cmdCancel)
		Me.Controls.Add(cmdContinue)
		Me.Controls.Add(txtEndHeader)
		Me.Controls.Add(txtEndBody)
		Me.Controls.Add(txtMatchingHeader)
		Me.Controls.Add(txtMatchingBody)
		Me.Controls.Add(txtClientsHeader)
		Me.Controls.Add(txtClientsBody)
		Me.Controls.Add(txtProfilesHeader)
		Me.Controls.Add(txtProfilesBody)
		Me.Controls.Add(txtStartHeader)
		Me.Controls.Add(txtStartBody)
		Me.Controls.Add(Label5)
		Me.Controls.Add(Label4)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
