Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmVisma_Parameters
    Inherits System.Windows.Forms.Form
    Private sUID As String
    Private sPWD As String
    Private sBBDatabase As String
    Private sUser As String
    Private sInstance As String
    Private iProfileID As Short
    Private bChanged As Boolean
	Private sFormatID As vbBabel.BabelFiles.FileType
	Private sOldFormatID As vbBabel.BabelFiles.FileType   ' added 06.10.2021
	Dim bInit As Boolean = True

    Private Sub frmVisma_Parameters_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        ' Interpret Commandline parameters, in call from Visma
        ' Nytt kall
        'VB;/S;F9998;vbsys;system;sa;Visma123
        Dim oVismaCommandLine As New VismaCommandLine

        sBBDatabase = oVismaCommandLine.BBDatabase
        sUID = oVismaCommandLine.UID
        sPWD = oVismaCommandLine.PWD
        sUser = oVismaCommandLine.User
        sInstance = oVismaCommandLine.Instance
        oVismaCommandLine = Nothing

        FormLRSCaptions(Me)
        FormVismaStyle(Me)

        bChanged = False
        Me.cmdSave.Enabled = False

        sprParametersCreate()
        sprParametersFill()
        bInit = False

    End Sub
    Private Sub sprParametersCreate()
        Dim x As Boolean
        Dim txtColumn As DataGridViewTextBoxColumn


        With Me.gridParameters

            'Visma_GridCreate(spr)
            .Rows.Clear()  ' empty content of grid

            If .ColumnCount = 0 Then
                '.Height = 100

                .ScrollBars = ScrollBars.Both
                .SelectionMode = DataGridViewSelectionMode.CellSelect
                .EditMode = DataGridViewEditMode.EditOnEnter

                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.FixedSingle
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowsDefaultCellStyle.SelectionForeColor = Color.Black
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .ReadOnly = False
            End If


            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(15)
            txtColumn.HeaderText = Replace(LRS(35015), ":", "") 'Parameter

            .Columns.Add(txtColumn)

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(14)
            txtColumn.HeaderText = Replace(LRS(35016), ":", "") 'Verdi
            .Columns.Add(txtColumn)

            ' ParameterID from database
            ' not visible
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

            ' LRSId from database
            ' not visible
            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Visible = False
            .Columns.Add(txtColumn)

        End With

    End Sub
    Private Sub sprParametersFill()    'ByRef spr As AxFPSpread.AxvaSpread)
        ' Fill grids from table bbProfile
        ' -------------------------------
        Dim oVismaDal As vbBabel.DAL = Nothing
		Dim sErrorString As String = ""
		Dim sMySQL As String
        Dim i As Short
        Dim ComboCell As DataGridViewComboBoxCell
		Dim bExit As Boolean = False  ' 06.10.2021

		On Error GoTo VismaFillError
		sErrorString = "ConnectToVismaBusinessDB"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        With Me.gridParameters
            ' sjekk p� uncomitted 
            ' Must run this because we need to Select against uncomitted data
            oVismaDal.SQL = "set transaction isolation level read uncommitted"
			If oVismaDal.ExecuteNonQuery Then
				If oVismaDal.RecordsAffected <> -1 Then
					' TODO VISMAPRO - lag feilmelding
					sErrorString = "Error at SQL set transaction isolation level read uncommitted - Recordsaffected <> -1"
					Throw New Exception(vbCrLf & LRS(35307, " 'uncomitted'") & vbCrLf & "sprParametersFill")
				End If
			Else
				sErrorString = "Error at SQL set transaction isolation level read uncommitted, ExecuteNonQuery = False"
				Throw New Exception(LRSCommon(45002) & vbCrLf & oVismaDal.ErrorMessage)
            End If

            ' clear previous grid content
            .Rows.Clear()
            sMySQL = "SELECT *, P.FormatId FROM " & sBBDatabase & ".dbo.bbParameter PM, " & sBBDatabase & ".dbo.bbProfile P WHERE PM.ProfileID = " & iProfileID & " AND P.ProfileId=PM.ProfileId"

			oVismaDal.SQL = sMySQL
			sErrorString = "SQL SELECT *, P.FormatId FROM " & sBBDatabase & ".dbo.bbParameter PM, " & sBBDatabase & ".dbo.bbProfile P WHERE PM.ProfileID = " & iProfileID & " AND P.ProfileId=PM.ProfileId"
			If oVismaDal.Reader_Execute() Then
				If oVismaDal.Reader_HasRows = True Then

					' 06.06.2014 First, check if we have changed to Format. If changed, then do not use the previous settings;
					' 06.10.2021
					' Her har vi ikke kj�rt noen oVismaDal.Reader_ReadRecord, og det kan f�re til feil ved SQLOLEDB.
					' Vi kutter ut If-en under, og kj�rer heller en test etter Do While oVismaDal.Reader_ReadRecord
					'If oVismaDal.Reader_GetString("FormatID").Trim <> sFormatID Then
					'                    ' do not bring up old parameters
					'Else
					' fill up new content
					'                    rsVisma.MoveFirst()
					'                    Do While Not rsVisma.EOF
					'                        '.Row = .Row + 1
					'                        .MaxRows = .MaxRows + 1
					'                        .Row = .MaxRows
					'                        .Col = 1
					sErrorString = "Fill content into grid"
					Do While oVismaDal.Reader_ReadRecord
						Dim myRow As DataGridViewRow

						.Rows.Add()
						myRow = .Rows(.RowCount - 1)

						If VB.Left(oVismaDal.Reader_GetString("BB_ID").Trim, 4) <> "SEQ#" And VB.Left(oVismaDal.Reader_GetString("BB_ID").Trim, 5) <> "PROD#" Then
							' Text to user;
							myRow.Cells(0).Value = LRS(oVismaDal.Reader_GetString("LRS_ID"))
							myRow.Cells(0).ReadOnly = True
						Else
							' SEQ#123456789# or PROD#45679876#20130831
							myRow.Cells(0).Value = oVismaDal.Reader_GetString("BB_ID")
							myRow.Cells(0).ReadOnly = False
						End If


						' VISMA TODO - har ikke lagt opp til ComboBox med Ja/Nei - b�r nok gj�res
						' For Pain.001 har vi oppsett av bank, som m� gj�res i combocell
						If sFormatID = vbBabel.BabelFiles.FileType.Pain001 And oVismaDal.Reader_GetString("LRS_ID") = "35018" Then ' 35018 = Bank
							ComboCell = New DataGridViewComboBoxCell
							ComboCell.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
							ComboCell.Items.Add("Danske Bank")
							ComboCell.Items.Add("BITS Standard")
							ComboCell.Items.Add("DNB")
							ComboCell.Items.Add("Handelsbanken")
							ComboCell.Items.Add("Nordea")
							ComboCell.Items.Add("S|E|B")
							ComboCell.Items.Add("SwedBank")
							ComboCell.Value = oVismaDal.Reader_GetString("Value")
							'ComboCell.ReadOnly = True
							.Rows(.Rows.Count - 1).Cells(1) = ComboCell

						Else
							myRow.Cells(1).Value = oVismaDal.Reader_GetString("Value")
						End If

						' ParameterID, hidden
						myRow.Cells(2).Value = oVismaDal.Reader_GetString("ParameterID")

						' LRSId, hidden
						myRow.Cells(3).Value = oVismaDal.Reader_GetString("LRS_ID")

						' 06.10.2021 - added next If - see comments some lines above
						If oVismaDal.Reader_GetString("FormatID").Trim <> sOldFormatID And sOldFormatID <> vbBabel.BabelFiles.FileType.Unknown Then
							'                    ' do not bring up old parameters
							bExit = True
						End If

					Loop

					If bExit Then
						' must delete previous parameters for this profile if we have changed format
						'.Rows.Clear()  ' empty content of grid - feil
						' 12.10.2021 - m� sette hver rad som Visible = False. Da sletter vi i Save
						'--- test dette ---
						For i = 0 To .Rows.Count - 1
							.Rows(i).Visible = False
						Next
					End If
					'End If  'If oVismaDal.Reader_GetString("FormatID").Trim <> sFormatID Then
				Else
					bExit = True  ' 18.10.2021 - Else and this line added, to be able to run commit below
				End If  'If oVismaDal.Reader_HasRows = True Then
            End If  'If oVismaDal.Reader_Execute() Then

			'            '"select" first row;
			If .RowCount > 0 Then
				If .Rows(0).Cells(0).Visible Then
					.CurrentCell = .Rows(0).Cells(0)
				End If
			End If
        End With

		''' 06.10.2021 - added next If, otherwise bombs when we have changed format.
		''' 18.10.2021 - below always bombs with SQLClient and SQLOLEDB(?)
		''If Not bExit Then
		''	oVismaDal.SQL = "set transaction isolation level read committed"
		''	If oVismaDal.ExecuteNonQuery Then
		''		If oVismaDal.RecordsAffected <> -1 Then
		''			' TODO VISMAPRO - lag feilmelding
		''			sErrorString = LRS(35307, "'comitted'")
		''			Throw New Exception(vbCrLf & LRS(35307, "'comitted'") & vbCrLf & "sprParametersFill")
		''		End If
		''	Else
		''		Throw New Exception(LRSCommon(45002) & vbCrLf & oVismaDal.ErrorMessage)
		''	End If
		''End If

		oVismaDal.Close()
        oVismaDal = Nothing

        Exit Sub

VismaFillError:
        oVismaDal.Close()
		oVismaDal = Nothing
		Err.Raise(30008, "sprParametersFill", sErrorString)

	End Sub
    Private Sub gridParameters_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridParameters.CellEndEdit
        ' special test for sequenceno;
        With Me.gridParameters
            If e.RowIndex > 0 And e.ColumnIndex = 0 Then
                ' the only thing allowed to edit is SEQ#123456789#Division or PROD#987654321#20130831
                If sFormatID = vbBabel.BabelFiles.FileType.Telepay2 Then
                    If VB.Left(.Rows(e.RowIndex).Cells(0).Value, 4) <> "SEQ#" Then
                        'MsgBox "Sekvensnummerangivelse settes slik: 'SEQ#nnnnnnnn#XXXXXXXX'  hvor nnnnnnnn er foretaksnr., XXXXXXXX er divisjon"
                        MsgBox(LRS(35181))
                    End If

                ElseIf sFormatID = vbBabel.BabelFiles.FileType.Nordea_SE_Plusgirot_FakturabetalningService Then
                    If VB.Left(.Rows(e.RowIndex).Cells(0).Value, 5) <> "PROD#" Then
                        'MsgBox "Produksjonsnummer settes slik: 'PROD#nnnnnnnn#YYMMDD'  hvor nnnnnnnn er foretaksnr., YYMMDD er dato."
                        MsgBox(LRS(35181))
                    End If

                End If

            End If
        End With

    End Sub
	Public Sub SetFormatID(ByRef sId As vbBabel.BabelFiles.FileType)
		sFormatID = sId
	End Sub
	Public Sub SetOldFormatID(ByRef sId As vbBabel.BabelFiles.FileType)
		sOldFormatID = sId
	End Sub
	Public Sub SetProfileID(ByRef iPI As Short)
		iProfileID = iPI
	End Sub

	'Private Sub frmVisma_Parameters_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
	'    Dim Cancel As Boolean = eventArgs.Cancel
	'    Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
	'    ' trap user clicking the X in upper right corner
	'    Cancel = True
	'    cmdClose_Click(cmdClose, New System.EventArgs())
	'    eventArgs.Cancel = Cancel
	'End Sub

	Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        Dim ComboCell As DataGridViewComboBoxCell

        With Me.gridParameters
			'	'05.02.2015 Added "Or sFormatID = filetype.Leverantorsbetalningar Or sFormatID = filetype.SEB_Sisu"
			'If sFormatID = vbBabel.BabelFiles.FileType.Telepay2 Or sFormatID = vbBabel.BabelFiles.FileType.Nordea_SE_Plusgirot_FakturabetalningService Or sFormatID = vbBabel.BabelFiles.FileType.Pain001 Or sFormatID = vbBabel.BabelFiles.FileType.NordeaCorporateFilePayments Or sFormatID = vbBabel.BabelFiles.FileType.Leverantorsbetalningar Or sFormatID = vbBabel.BabelFiles.FileType.SEB_Sisu Then
			' add sequencesetup
			' 13.10.2021 changed Ifs below
			'If sFormatID = vbBabel.BabelFiles.FileType.Telepay2 And .Rows.Count = 0 Then
			If sFormatID = vbBabel.BabelFiles.FileType.Telepay2 Then
				.Rows.Add()
				.Rows(.Rows.Count - 1).Cells(0).Value = "SEQ#foretaksnr#divisjon#"
				.Rows(.Rows.Count - 1).Cells(1).Value = "0"
				.Rows(.Rows.Count - 1).Cells(3).Value = "35179"

				'ElseIf sFormatID = vbBabel.BabelFiles.FileType.Nordea_SE_Plusgirot_FakturabetalningService And .Rows.Count = 0 Then
			ElseIf sFormatID = vbBabel.BabelFiles.FileType.Nordea_SE_Plusgirot_FakturabetalningService Then
				.Rows.Add()
				.Rows(.Rows.Count - 1).Cells(0).Value = "PROD#"
				.Rows(.Rows.Count - 1).Cells(1).Value = "0"
				.Rows(.Rows.Count - 1).Cells(3).Value = "35179"



			ElseIf sFormatID = vbBabel.BabelFiles.FileType.Pain001 And .Rows.Count < 3 Then
				' 11.02.2014 added parameters for Pain.001
				.Rows.Add()
				' Payers name
				.Rows(.Rows.Count - 1).Cells(0).Value = LRS(60160)
				.Rows(.Rows.Count - 1).Cells(0).ReadOnly = True
				.Rows(.Rows.Count - 1).Cells(1).Value = ""
				.Rows(.Rows.Count - 1).Cells(3).Value = "60160"

				.Rows.Add()
				' Org no
				.Rows(.Rows.Count - 1).Cells(0).Value = LRS(35019)
				.Rows(.Rows.Count - 1).Cells(0).ReadOnly = True
				.Rows(.Rows.Count - 1).Cells(1).Value = ""
				.Rows(.Rows.Count - 1).Cells(3).Value = "35019"

				.Rows.Add()
				' Division
				.Rows(.Rows.Count - 1).Cells(0).Value = LRS(35020)
				.Rows(.Rows.Count - 1).Cells(0).ReadOnly = True
				.Rows(.Rows.Count - 1).Cells(1).Value = ""
				.Rows(.Rows.Count - 1).Cells(3).Value = "35020"

				' 28.04.2017
				' BatchBooking
				.Rows.Add()
				.Rows(.Rows.Count - 1).Cells(0).Value = LRS(35319)  ' BatchBooking Ja/Nei)
				.Rows(.Rows.Count - 1).Cells(0).ReadOnly = True
				.Rows(.Rows.Count - 1).Cells(1).Value = ""
				.Rows(.Rows.Count - 1).Cells(3).Value = "35319"


				.Rows.Add()
				' Bank
				.Rows(.Rows.Count - 1).Cells(0).Value = LRS(35018)
				.Rows(.Rows.Count - 1).Cells(0).ReadOnly = True
				ComboCell = New DataGridViewComboBoxCell
				ComboCell.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
				ComboCell.Items.Add("Danske Bank")
				ComboCell.Items.Add("BITS Standard")
				ComboCell.Items.Add("DNB")
				ComboCell.Items.Add("Handelsbanken")
				ComboCell.Items.Add("Nordea")
				ComboCell.Items.Add("S|E|B")
				ComboCell.Items.Add("SwedBank")
				ComboCell.Value = "Danske Bank"
				'ComboCell.ReadOnly = True
				.Rows(.Rows.Count - 1).Cells(1) = ComboCell
				.Rows(.Rows.Count - 1).Cells(3).Value = "35018"

			ElseIf sFormatID = vbBabel.BabelFiles.FileType.Pain001 And .Rows.Count < 5 Then
				' 09.05.2017 added BtchBooking, also when we had parameters for Pain.001 from older versions
				' BatchBooking
				.Rows.Add()
				.Rows(.Rows.Count - 1).Cells(0).Value = LRS(35319)  ' BatchBooking Ja/Nei)
				.Rows(.Rows.Count - 1).Cells(1).Value = ""
				.Rows(.Rows.Count - 1).Cells(3).Value = "35319"

				'ElseIf (sFormatID = vbBabel.BabelFiles.FileType.Pain002 Or sFormatID = vbBabel.BabelFiles.FileType.Camt054 Or sFormatID = vbBabel.BabelFiles.FileType.Camt054_Outgoing Or sFormatID = vbBabel.BabelFiles.FileType.Camt054_Incoming) And .Rows.Count < 1 Then
			ElseIf (sFormatID = vbBabel.BabelFiles.FileType.Pain002 Or sFormatID = vbBabel.BabelFiles.FileType.Camt054 Or sFormatID = vbBabel.BabelFiles.FileType.Camt054_Outgoing Or sFormatID = vbBabel.BabelFiles.FileType.Camt054_Incoming) Then
				.Rows.Add()
				' Bank
				.Rows(.Rows.Count - 1).Cells(0).Value = LRS(35018)
				.Rows(.Rows.Count - 1).Cells(0).ReadOnly = True
				ComboCell = New DataGridViewComboBoxCell
				ComboCell.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
				ComboCell.Items.Add("Danske Bank")
				ComboCell.Items.Add("BITS Standard")
				ComboCell.Items.Add("DNB")
				ComboCell.Items.Add("Handelsbanken")
				ComboCell.Items.Add("Nordea")
				ComboCell.Items.Add("S|E|B")
				ComboCell.Items.Add("SwedBank")
				ComboCell.Value = "Danske Bank"
				'ComboCell.ReadOnly = True
				.Rows(.Rows.Count - 1).Cells(1) = ComboCell
				.Rows(.Rows.Count - 1).Cells(3).Value = "35018"


				'ElseIf (sFormatID = vbBabel.BabelFiles.FileType.NordeaCorporateFilePayments Or sFormatID = vbBabel.BabelFiles.FileType.Leverantorsbetalningar Or sFormatID = vbBabel.BabelFiles.FileType.SEB_Sisu) And .Rows.Count = 0 Then
			ElseIf (sFormatID = vbBabel.BabelFiles.FileType.NordeaCorporateFilePayments Or sFormatID = vbBabel.BabelFiles.FileType.Leverantorsbetalningar Or sFormatID = vbBabel.BabelFiles.FileType.SEB_Sisu) Then
				' UseRefOwn (returnfiles or not)
				.Rows.Add()   ' added this line 10.02.2017
				.Rows(.Rows.Count - 1).Cells(0).Value = "UseRefOwn"
				.Rows(.Rows.Count - 1).Cells(0).ReadOnly = True
				.Rows(.Rows.Count - 1).Cells(1).Value = "Ja"
                .Rows(.Rows.Count - 1).Cells(3).Value = "62095"

            End If

			If .RowCount > 0 Then
				' position to bottom
				If .Rows(.Rows.Count - 1).Cells(0).Visible Then
					.Rows(.Rows.Count - 1).Cells(0).Selected = True
				End If
			End If
			'End If

			bChanged = True
            Me.cmdSave.Enabled = True

        End With

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        ' Delete row from gridParameters
        Dim sText As String
        Dim iRow As Integer

        On Error GoTo localerror
        If Me.gridParameters.RowCount > 0 Then
            If Not Me.gridParameters.CurrentCell Is Nothing Then
                sText = Me.gridParameters.CurrentCell.Value
                If MsgBox(LRS(35180, sText), MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Visma Payment (BabelBank)") = MsgBoxResult.Yes Then '

                    bChanged = True
                    Me.cmdSave.Enabled = True


                    With Me.gridParameters
                        '.Rows.Remove(.CurrentRow)
                        iRow = .CurrentRow.Index
                        .Rows(iRow).Visible = False
                        ' TODO VISMA
                        ' Will we need to position to next/prev row?
                        Do While iRow < .RowCount - 1
                            If .Rows(iRow + 1).Visible = True Then
                                .CurrentCell = .Rows(iRow + 1).Cells(0)
                                sText = "set"
                                Exit Do
                            End If
                        Loop
                        If sText <> "set" Then
                            If .Rows(0).Visible = True Then
                                .CurrentCell = .Rows(0).Cells(1)
                            End If

                        End If
                    End With
                    bChanged = True
                End If
            End If
        End If
        Exit Sub

localerror:
        Err.Raise(30003, "frmVisma_Parameters.cmdDelete_Click", "")


    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        If bChanged Then
            ' Vil du avslutte uten lagring ?
            If MsgBox(LRS(48020), MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Me.Hide()
            End If
        Else
            Me.Hide()
        End If

    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim iNextID As Short
        Dim i As Short
        Dim sErrorString As String
        Dim sMySQL As String
        Dim lRecordsAffected As Integer
        Dim sValue As String
        Dim sParameterID As String
        Dim sLRSId As String
        Dim sText As String

        On Error GoTo localerror
        '        ' update parameters
        sErrorString = "ConnectToVismaBusinessDB"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        sMySQL = "SELECT MAX(ParameterId) AS MaxID FROM " & sBBDatabase & ".dbo.bbParameter"
        oVismaDal.SQL = sMySQL
        If oVismaDal.Reader_Execute() Then
            If oVismaDal.Reader_HasRows = True Then
				Do While oVismaDal.Reader_ReadRecord
					If Not IsDBNull(oVismaDal.Reader_GetString("MaxID")) Then
						iNextID = Val(oVismaDal.Reader_GetString("MaxID")) + 1
					Else
						iNextID = 1
					End If
					Exit Do
				Loop
			End If
        End If
        oVismaDal.Close()

        sErrorString = "ConnectToVismaBusinessDB_2"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)
        oVismaDal.TEST_UseCommitFromOutside = True
        oVismaDal.TEST_BeginTrans()

        ' new parameter, possible for sequenceno's and production nos only
        ' and others for Pain.001
        ' Find next ParameterID

        With Me.gridParameters
            '  g� gjennom linje for linje, og oppdater Values for hver ParameterID
            For i = 0 To .RowCount - 1

                sText = .Rows(i).Cells(0).Value

                '                .Col = 2
                '                ' do not update hidden rows
                If .Rows(i).Cells(1).Visible Then
                    sValue = .Rows(i).Cells(1).Value
                    sParameterID = .Rows(i).Cells(2).Value
                    sLRSId = .Rows(i).Cells(3).Value


                    If Len(sParameterID) > 0 Then
                        ' existing parameter
                        If VB.Left(.Rows(i).Cells(0).Value, 4) = "SEQ#" Then
                            ' must update both BB_Id and Value
                            sMySQL = "UPDATE " & sBBDatabase & ".dbo.bbParameter SET BB_Id = '" & sText & "', Value = '" & sValue & "', ChUsr = '" & sUser & "', ChDt = CONVERT(VARCHAR(8), SYSDATETIME(), 112), ChTm = REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', '') WHERE ParameterID = " & sParameterID
                        ElseIf VB.Left(.Rows(i).Cells(0).Value, 5) = "PROD#" Then
                            ' must update both BB_Id and Value
                            sMySQL = "UPDATE " & sBBDatabase & ".dbo.bbParameter SET BB_Id = '" & sText & "', Value = '" & sValue & "', ChUsr = '" & sUser & "', ChDt = CONVERT(VARCHAR(8), SYSDATETIME(), 112), ChTm = REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', '') WHERE ParameterID = " & sParameterID
                        Else
                            ' all other, except sequence/prod
                            sMySQL = "UPDATE " & sBBDatabase & ".dbo.bbParameter SET Value = '" & sValue & "', ChUsr = '" & sUser & "', ChDt = CONVERT(VARCHAR(8), SYSDATETIME(), 112), ChTm = REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', '') WHERE ParameterID = " & sParameterID
                        End If
                    Else

                        If VB.Left(sText, 4) = "SEQ#" Or VB.Left(sText, 5) = "PROD#" Then
                            ' find SEQ#123456789#DIVISJON
                            '                            .Col = 1
                            sMySQL = "INSERT INTO " & sBBDatabase & ".dbo.bbParameter (ParameterID, BB_ID, LRS_ID, ProfileID, Value, AllowedValues, ParameterType, DataType, ChDt, ChTm, ChUsr) VALUES ("
                            'sMySQL = sMySQL & iNextID & ", '" & .Text & "', " & sLRSId & "," & Str(iProfileID) & ", '" & sValue & "', '', 3, 1"
                            sMySQL = sMySQL & iNextID & ", '" & .Rows(i).Cells(0).Value & "', " & sLRSId & "," & Str(iProfileID) & ", '" & sValue & "', '', 3, 1"
                            sMySQL = sMySQL & ", CONVERT(VARCHAR(8), SYSDATETIME(), 112), REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', ''), '" & sUser & "')"
                        Else
                            '                            .Col = 1
                            sMySQL = "INSERT INTO " & sBBDatabase & ".dbo.bbParameter (ParameterID, BB_ID, LRS_ID, ProfileID, Value, AllowedValues, ParameterType, DataType, ChDt, ChTm, ChUsr) VALUES ("
                            'sMySQL = sMySQL & iNextID & ", '" & .Text & "', " & sLRSId & "," & Str(iProfileID) & ", '" & sValue & "', '', 3, 1"
                            sMySQL = sMySQL & iNextID & ", '" & .Rows(i).Cells(0).Value & "', " & sLRSId & "," & Str(iProfileID) & ", '" & sValue & "', '', 3, 1"
                            sMySQL = sMySQL & ", CONVERT(VARCHAR(8), SYSDATETIME(), 112), REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', ''), '" & sUser & "')"
                        End If  'If VB.Left(sText, 4) = "SEQ#" Or VB.Left(sText, 5) = "PROD#" Then

                    End If  'If Len(sParameterID) > 0 Then 
                    oVismaDal.SQL = sMySQL
                    If oVismaDal.ExecuteNonQuery Then
                        If oVismaDal.RecordsAffected <> 1 Then
                            ' TODO VISMA LRS
                            Throw New Exception("frmVisma_Parameters.cmdSave" & vbCrLf & LRS("Error updating database", sMySQL, oVismaDal.RecordsAffected.ToString))
                            ' VIsma TODO - er det riktig � kj�re rollback her?
                            'oVismaDal.TEST_Rollback()
                        Else
                            'oVismaDal.TEST_CommitTrans()
                        End If
                    Else
                        Throw New Exception(LRSCommon(45002) & vbCrLf & oVismaDal.ErrorMessage)
                    End If

                Else
                    ' deleted by user
                    ' delete in database
                    sValue = .Rows(i).Cells(2).Value
                    If Len(sValue) > 0 Then
                        sMySQL = "DELETE FROM " & sBBDatabase & ".dbo.bbParameter WHERE ParameterID = " & sValue
                        'oVismaDal.TEST_UseCommitFromOutside = True
                        'oVismaDal.TEST_BeginTrans()
                        oVismaDal.SQL = sMySQL
                        If oVismaDal.ExecuteNonQuery Then
                            If oVismaDal.RecordsAffected <> 1 Then
                                ' TODO VISMA LRS
                                Throw New Exception("frmVisma_Parameters.cmdSave" & vbCrLf & LRS("Error updating database", sMySQL, oVismaDal.RecordsAffected.ToString))
                                ' VIsma TODO - er det riktig � kj�re rollback her?
                                'oVismaDal.TEST_Rollback()
                            Else
                                'oVismaDal.TEST_CommitTrans()
                            End If
                        Else
                            Throw New Exception(LRSCommon(45002) & vbCrLf & oVismaDal.ErrorMessage)
                        End If

                    End If  'If Len(sValue) > 0 Then
                End If  'If Not .Rows(i).Cells(1).ReadOnly Then
                iNextID = iNextID + 1
            Next i
        End With
        oVismaDal.TEST_CommitTrans()
        oVismaDal.TEST_UseCommitFromOutside = False
        oVismaDal.Close()
        oVismaDal = Nothing

        bChanged = False
        Me.cmdSave.Enabled = False

        Exit Sub

localerror:
        ' TODO VISMA
        ' KJELL - b�r vi ikke ha en property i DAL ala If oVismaDal.TEST_UseCommitFromOutside = True then
        oVismaDal.TEST_Rollback()
        oVismaDal.Close()
        oVismaDal = Nothing

        Err.Raise(30007, "cmdSave_Click", sErrorString)

    End Sub

    Private Sub gridParameters_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles gridParameters.CellBeginEdit
        If Not bInit Then
            bChanged = True
            Me.cmdSave.Enabled = True
        End If
    End Sub
End Class