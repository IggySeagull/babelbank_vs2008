﻿Option Strict Off
Option Explicit On

'Imports System.Windows.Forms

Friend Class frmVisma_ShowProfileGroups
    Inherits System.Windows.Forms.Form
    Private iClientID As Short
    Private Declare Function DestroyCaret Lib "user32" () As Integer
    Private sUID As String = ""
    Private sPWD As String = ""
    Dim sInstance As String = ""
    Private sBBDatabase As String = ""
    Dim sUser As String = ""
    Private bChanged As Boolean = False
    Private lActiveRow As Integer = 0

    Private Sub frmVisma_ShowProfileGroups_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        ' Nytt kall;
        'VB;/S;F9998;vbsys;system;sa;Visma123
        Dim oVismaCommandLine As New VismaCommandLine

        sBBDatabase = oVismaCommandLine.BBDatabase
        sUser = oVismaCommandLine.User
        sUID = oVismaCommandLine.UID
        sPWD = oVismaCommandLine.PWD
        sInstance = oVismaCommandLine.Instance
        oVismaCommandLine = Nothing

        FormLRSCaptions(Me)
        FormVismaStyle(Me)


        bChanged = False
        SpreadHeaders()
        SpreadFill()
    End Sub
    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        If bChanged Then
            ' Vil du avslutte uten lagring ?
            If MsgBox(LRS(48020), MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Me.Hide()
            End If
        Else
            Me.Hide()
        End If
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        ' delete a Profilegroup
        Dim sFirm As String
        Dim sProfileGroup As String
        Dim sProfileGroupID As String
        Dim sAccount As String
        Dim iNextRow As Integer = 0
        Dim oVismaDal As New vbBabel.DAL
        Dim sErrorString As String
        Dim sMySQL As String

        On Error GoTo localerror

        With Me.gridProfileGroups
            ' Profilegroupname
            sProfileGroup = .Rows(.CurrentRow.Index).Cells(0).Value
            If EmptyString(.Rows(.CurrentRow.Index).Cells(1).Value) Then
                sProfileGroupID = -1  ' Newly added, not saved profilegroup
            Else
                sProfileGroupID = .Rows(.CurrentRow.Index).Cells(1).Value.ToString
            End If

            If .CurrentRow.Index < .RowCount - 1 Then
                iNextRow = .CurrentRow.Index + 1
            Else
                iNextRow = .CurrentRow.Index - 1
            End If

            If sProfileGroupID <> -1 Then
                ' Check if there are profiles in use for this Profilegroup
                sErrorString = "ConnectToVismaBusinessDB"
                oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)
                sMySQL = "SELECT * FROM " & sBBDatabase & ".dbo.bbProfile WHERE ProfileGroupID = " & sProfileGroupID
                oVismaDal.SQL = sMySQL
                sErrorString = "Check if profiles are connected to this profilegroup"

                If oVismaDal.Reader_Execute() Then
                    If oVismaDal.Reader_HasRows = True Then
                        ' there are profiles connected to this profilegroup  user must first delete them
                        MsgBox(LRS(35122), vbInformation, "Visma Payment (BabelBank)")
                    Else
                        If MsgBox(LRS(35123) & " " & sProfileGroup, MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Visma Payment (BabelBank)") = MsgBoxResult.Yes Then
                            .Rows(.CurrentRow.Index).Visible = False
                            bChanged = True
                            If iNextRow > -1 And .Rows(iNextRow).Visible Then
                                .CurrentCell = .Rows(iNextRow).Cells(0)
                                .Rows(iNextRow).Selected = True
                            Else
                                If .RowCount > 0 Then
                                    .CurrentCell = .Rows(0).Cells(0)
                                End If
                            End If
                        End If
                    End If
                End If
            Else
                .Rows(.CurrentRow.Index).Visible = False
                bChanged = True
                If iNextRow > -1 And .Rows(iNextRow).Visible Then
                    .CurrentCell = .Rows(iNextRow).Cells(0)
                    .Rows(iNextRow).Selected = True
                Else
                    If .RowCount > 0 Then
                        .CurrentCell = .Rows(0).Cells(0)
                    End If
                End If

            End If

        End With

        oVismaDal.Close()
        oVismaDal = Nothing

        Exit Sub

localerror:
        Err.Raise(30001, "frmVismaShowProfielGroups.cmdDelete_Click", sErrorString & vbCrLf & Err.Description) '
        oVismaDal.Close()
        oVismaDal = Nothing

    End Sub
    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        ' run through grid, remove deleted
        Dim oVismaDal As New vbBabel.DAL
        Dim sErrorString As String
        Dim sMySQL As String
        Dim iProfileGroupID As Short = 0
        Dim iNextProfileGroupID As Short = 1
        Dim i As Short
        Dim lRecordsAffected As Integer

        On Error GoTo localerror
        sErrorString = "ConnectToVismaBusinessDB"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        ' first; find highest ProfileGroupID
        sMySQL = "SELECT MAX(ProfileGroupId) AS MaxID FROM " & sBBDatabase & ".dbo.bbProfileGroup"
        oVismaDal.SQL = sMySQL
        sErrorString = "Find  highest ProfileGroupID"

        If oVismaDal.Reader_Execute() Then
            If oVismaDal.Reader_HasRows Then
                oVismaDal.Reader_ReadRecord()
                iNextProfileGroupID = Val(oVismaDal.Reader_GetString("MaxID")) + 1
            End If
        End If
        oVismaDal.Close()
        oVismaDal = Nothing


        ' reconnect to database to Insert/Delete
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)
        'Me.Hide
        oVismaDal.TEST_UseCommitFromOutside = True
        oVismaDal.TEST_BeginTrans()

        With Me.gridProfileGroups
            For i = 0 To .RowCount - 1
                If .Rows(i).Visible = False Then
                    iProfileGroupID = .Rows(i).Cells(1).Value

                    ' Detelete removed items;
                    ' -----------------------
                    ' Must first delete from Clientstable for this profilegroup
                    sMySQL = "DELETE FROM " & sBBDatabase & ".dbo.bbClient WHERE ProfileGroupID = " & iProfileGroupID.ToString
                    oVismaDal.SQL = sMySQL
                    oVismaDal.ExecuteNonQuery()

                    sMySQL = "DELETE FROM " & sBBDatabase & ".dbo.bbProfileGroup WHERE ProfileGroupID = " & iProfileGroupID.ToString
                    oVismaDal.SQL = sMySQL
                    oVismaDal.ExecuteNonQuery()

                Else
                    ' check if we have any ProfileGroup_ID or not
                    If EmptyString(.Rows(i).Cells(1).Value) Then
                        ' new profilegroup added
                        ' check if name is filled in
                        If Not EmptyString(.Rows(i).Cells(0).Value) Then
                            ' save this profilegroup
                            sMySQL = "INSERT INTO " & sBBDatabase & ".dbo.bbProfileGroup (ProfileGroupID, Name, ChDt, ChTm, ChUsr) VALUES (" & iNextProfileGroupID & ", '" & .Rows(i).Cells(0).Value
                            sMySQL = sMySQL & "', CONVERT(VARCHAR(8), SYSDATETIME(), 112), REPLACE(CONVERT(VARCHAR(5), SYSDATETIME(), 114), ':', ''), '" & sUser & "')"
                            oVismaDal.SQL = sMySQL
                            oVismaDal.ExecuteNonQuery()

                            iNextProfileGroupID = iNextProfileGroupID + 1
                        End If
                    End If

                End If
            Next i
        End With
        oVismaDal.TEST_CommitTrans()
        oVismaDal.Close()
        oVismaDal = Nothing
        bChanged = False
        Me.Hide()

        Exit Sub

localerror:
        oVismaDal.TEST_Rollback()
        Err.Raise(30001, "frmVismaShowProfileGroups.cmdOK_Click", sErrorString) '

    End Sub
    Private Sub SpreadHeaders()
        Dim txtColumn As DataGridViewTextBoxColumn

        With Me.gridProfileGroups

            .Rows.Clear()  ' empty content of grid

            If .ColumnCount = 0 Then
                .ScrollBars = ScrollBars.Vertical
                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                ' not possible to add rows manually!
                .AllowUserToAddRows = False
                .RowHeadersVisible = False
                '.MultiSelect = False
                .BorderStyle = BorderStyle.FixedSingle
                .RowTemplate.Height = 18
                .BackgroundColor = Color.White
                .RowsDefaultCellStyle.SelectionBackColor = Color.LightBlue
                .RowsDefaultCellStyle.SelectionForeColor = Color.Black
                .CellBorderStyle = DataGridViewCellBorderStyle.Single
                .ReadOnly = False
            End If

            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Width = WidthFromSpreadToGrid(28)
            txtColumn.HeaderText = Replace(LRS(35017), ":", "") 'Profilgruppe
            .Columns.Add(txtColumn)


            txtColumn = New DataGridViewTextBoxColumn
            txtColumn.Visible = False
            .Columns.Add(txtColumn)
        End With

    End Sub
    Private Sub SpreadFill()
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sErrorString As String
        Dim sMySQL As String
        Dim myRow As DataGridViewRow

        On Error GoTo VismaFillError

        sErrorString = "ConnectToVismaBusinessDB"
        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)

        sMySQL = "SELECT * FROM " & sBBDatabase & ".dbo.bbProfileGroup ORDER BY Name"
        oVismaDal.SQL = sMySQL
        If oVismaDal.Reader_Execute() Then
            If oVismaDal.Reader_HasRows = True Then

                With Me.gridProfileGroups
                    ' fill up
                    Do While oVismaDal.Reader_ReadRecord

                        myRow = New DataGridViewRow
                        myRow.CreateCells(Me.gridProfileGroups)

                        ' Profilegroupname
                        myRow.Cells(0).Value = oVismaDal.Reader_GetString("Name")
                        myRow.Cells(0).ReadOnly = True

                        ' ProfileGroupID
                        myRow.Cells(1).Value = oVismaDal.Reader_GetString("ProfileGroupId")
                        myRow.Cells(1).ReadOnly = True

                        .Rows.Add(myRow)
                    Loop
                End With
                Me.gridProfileGroups.Focus()
            End If
        End If
        oVismaDal.Close()
        oVismaDal = Nothing

        Exit Sub

VismaFillError:
        oVismaDal.Close()
        oVismaDal = Nothing

    End Sub
    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        ' add new row;
        Me.gridProfileGroups.Rows.Add()
        'Me.gridProfileGroups.Rows(Me.gridProfileGroups.Rows.Count - 1).Selected = True
        ' position to new row
        Me.gridProfileGroups.CurrentCell = Me.gridProfileGroups.Rows(Me.gridProfileGroups.Rows.Count - 1).Cells(0)
        Me.gridProfileGroups.Focus()
        Me.gridProfileGroups.BeginEdit(True)
        'Me.gridProfileGroups.FirstDisplayedScrollingRowIndex = Me.gridProfileGroups.CurrentRow.Index
        'Me.gridProfileGroups.PerformLayout()
    End Sub
End Class
