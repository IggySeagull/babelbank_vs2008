<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMATCH_ManualSetup
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdOmitAccounts As System.Windows.Forms.Button
	Public WithEvents chkCommitTrans As System.Windows.Forms.CheckBox
	Public WithEvents cmdPWStatistics As System.Windows.Forms.Button
	Public WithEvents chkStatistics As System.Windows.Forms.CheckBox
	Public WithEvents cmdFileSwap As System.Windows.Forms.Button
	Public WithEvents chkExportOnlyMacthed As System.Windows.Forms.CheckBox
	Public WithEvents chkUseReceivedAmount As System.Windows.Forms.CheckBox
    Public WithEvents chkDeductPreviousMatched As System.Windows.Forms.CheckBox
	Public WithEvents lstColumnsEnterSequence As System.Windows.Forms.CheckedListBox
	Public WithEvents chkActiveOmitPayments As System.Windows.Forms.CheckBox
	Public WithEvents txtERPID As System.Windows.Forms.TextBox
	Public WithEvents chkMatchARItem As System.Windows.Forms.CheckBox
	Public WithEvents lstMatchedColumns As System.Windows.Forms.CheckedListBox
	Public WithEvents chkAskWhenRest As System.Windows.Forms.CheckBox
	Public WithEvents chkJumpToNext As System.Windows.Forms.CheckBox
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents lblColumnsEnterSequence As System.Windows.Forms.Label
	Public WithEvents lblERPID As System.Windows.Forms.Label
	Public WithEvents lblColumns As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdOmitAccounts = New System.Windows.Forms.Button
        Me.chkCommitTrans = New System.Windows.Forms.CheckBox
        Me.cmdPWStatistics = New System.Windows.Forms.Button
        Me.chkStatistics = New System.Windows.Forms.CheckBox
        Me.cmdFileSwap = New System.Windows.Forms.Button
        Me.chkExportOnlyMacthed = New System.Windows.Forms.CheckBox
        Me.chkUseReceivedAmount = New System.Windows.Forms.CheckBox
        Me.chkDeductPreviousMatched = New System.Windows.Forms.CheckBox
        Me.lstColumnsEnterSequence = New System.Windows.Forms.CheckedListBox
        Me.chkActiveOmitPayments = New System.Windows.Forms.CheckBox
        Me.txtERPID = New System.Windows.Forms.TextBox
        Me.chkMatchARItem = New System.Windows.Forms.CheckBox
        Me.lstMatchedColumns = New System.Windows.Forms.CheckedListBox
        Me.chkAskWhenRest = New System.Windows.Forms.CheckBox
        Me.chkJumpToNext = New System.Windows.Forms.CheckBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblColumnsEnterSequence = New System.Windows.Forms.Label
        Me.lblERPID = New System.Windows.Forms.Label
        Me.lblColumns = New System.Windows.Forms.Label
        Me.chkValidateMatching = New System.Windows.Forms.CheckBox
        Me.chkReportDeviations = New System.Windows.Forms.CheckBox
        Me.cmdChangeLabels = New System.Windows.Forms.Button
        Me.cmdRightClick = New System.Windows.Forms.Button
        Me.chkDoubleMatchingUndoAll = New System.Windows.Forms.CheckBox
        Me.ChkDoubleMatching = New System.Windows.Forms.CheckBox
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.txtHistoryDays = New System.Windows.Forms.TextBox
        Me.lblHistoryDays = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdOmitAccounts
        '
        Me.cmdOmitAccounts.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOmitAccounts.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOmitAccounts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOmitAccounts.Location = New System.Drawing.Point(648, 475)
        Me.cmdOmitAccounts.Name = "cmdOmitAccounts"
        Me.cmdOmitAccounts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOmitAccounts.Size = New System.Drawing.Size(105, 21)
        Me.cmdOmitAccounts.TabIndex = 99
        Me.cmdOmitAccounts.TabStop = False
        Me.cmdOmitAccounts.Text = "55037 - Omit accounts"
        Me.cmdOmitAccounts.UseVisualStyleBackColor = False
        '
        'chkCommitTrans
        '
        Me.chkCommitTrans.BackColor = System.Drawing.SystemColors.Control
        Me.chkCommitTrans.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkCommitTrans.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkCommitTrans.Location = New System.Drawing.Point(200, 297)
        Me.chkCommitTrans.Name = "chkCommitTrans"
        Me.chkCommitTrans.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkCommitTrans.Size = New System.Drawing.Size(513, 16)
        Me.chkCommitTrans.TabIndex = 17
        Me.chkCommitTrans.Text = "62057 - Commit trans after each change in the tables"
        Me.chkCommitTrans.UseVisualStyleBackColor = False
        '
        'cmdPWStatistics
        '
        Me.cmdPWStatistics.BackColor = System.Drawing.SystemColors.Control
        Me.cmdPWStatistics.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdPWStatistics.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPWStatistics.Location = New System.Drawing.Point(432, 320)
        Me.cmdPWStatistics.Name = "cmdPWStatistics"
        Me.cmdPWStatistics.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdPWStatistics.Size = New System.Drawing.Size(105, 21)
        Me.cmdPWStatistics.TabIndex = 26
        Me.cmdPWStatistics.Text = "55036 - PW Statistics"
        Me.cmdPWStatistics.UseVisualStyleBackColor = False
        '
        'chkStatistics
        '
        Me.chkStatistics.BackColor = System.Drawing.SystemColors.Control
        Me.chkStatistics.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkStatistics.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkStatistics.Location = New System.Drawing.Point(200, 322)
        Me.chkStatistics.Name = "chkStatistics"
        Me.chkStatistics.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkStatistics.Size = New System.Drawing.Size(534, 16)
        Me.chkStatistics.TabIndex = 18
        Me.chkStatistics.Text = "62053 -Activate statistics"
        Me.chkStatistics.UseVisualStyleBackColor = False
        '
        'cmdFileSwap
        '
        Me.cmdFileSwap.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileSwap.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileSwap.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileSwap.Location = New System.Drawing.Point(648, 502)
        Me.cmdFileSwap.Name = "cmdFileSwap"
        Me.cmdFileSwap.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileSwap.Size = New System.Drawing.Size(105, 21)
        Me.cmdFileSwap.TabIndex = 99
        Me.cmdFileSwap.TabStop = False
        Me.cmdFileSwap.Text = "55035 - Swap filecontent"
        Me.cmdFileSwap.UseVisualStyleBackColor = False
        '
        'chkExportOnlyMacthed
        '
        Me.chkExportOnlyMacthed.BackColor = System.Drawing.SystemColors.Control
        Me.chkExportOnlyMacthed.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkExportOnlyMacthed.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkExportOnlyMacthed.Location = New System.Drawing.Point(226, 86)
        Me.chkExportOnlyMacthed.Name = "chkExportOnlyMacthed"
        Me.chkExportOnlyMacthed.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkExportOnlyMacthed.Size = New System.Drawing.Size(469, 16)
        Me.chkExportOnlyMacthed.TabIndex = 2
        Me.chkExportOnlyMacthed.Text = "60274 - Export only fully posted payments"
        Me.chkExportOnlyMacthed.UseVisualStyleBackColor = False
        '
        'chkUseReceivedAmount
        '
        Me.chkUseReceivedAmount.BackColor = System.Drawing.SystemColors.Control
        Me.chkUseReceivedAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUseReceivedAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUseReceivedAmount.Location = New System.Drawing.Point(200, 258)
        Me.chkUseReceivedAmount.Name = "chkUseReceivedAmount"
        Me.chkUseReceivedAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUseReceivedAmount.Size = New System.Drawing.Size(550, 28)
        Me.chkUseReceivedAmount.TabIndex = 10
        Me.chkUseReceivedAmount.Text = "62033 - Use received amount ......"
        Me.chkUseReceivedAmount.UseVisualStyleBackColor = False
        '
        'chkDeductPreviousMatched
        '
        Me.chkDeductPreviousMatched.AutoSize = True
        Me.chkDeductPreviousMatched.BackColor = System.Drawing.SystemColors.Control
        Me.chkDeductPreviousMatched.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkDeductPreviousMatched.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkDeductPreviousMatched.Location = New System.Drawing.Point(200, 238)
        Me.chkDeductPreviousMatched.Name = "chkDeductPreviousMatched"
        Me.chkDeductPreviousMatched.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkDeductPreviousMatched.Size = New System.Drawing.Size(386, 17)
        Me.chkDeductPreviousMatched.TabIndex = 9
        Me.chkDeductPreviousMatched.Text = "62030 - Deduct amount for previously match items in same run of BabelBank"
        Me.chkDeductPreviousMatched.UseVisualStyleBackColor = False
        '
        'lstColumnsEnterSequence
        '
        Me.lstColumnsEnterSequence.BackColor = System.Drawing.SystemColors.Window
        Me.lstColumnsEnterSequence.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstColumnsEnterSequence.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstColumnsEnterSequence.Location = New System.Drawing.Point(432, 373)
        Me.lstColumnsEnterSequence.Name = "lstColumnsEnterSequence"
        Me.lstColumnsEnterSequence.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstColumnsEnterSequence.Size = New System.Drawing.Size(204, 154)
        Me.lstColumnsEnterSequence.TabIndex = 20
        '
        'chkActiveOmitPayments
        '
        Me.chkActiveOmitPayments.BackColor = System.Drawing.SystemColors.Control
        Me.chkActiveOmitPayments.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkActiveOmitPayments.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkActiveOmitPayments.Location = New System.Drawing.Point(200, 218)
        Me.chkActiveOmitPayments.Name = "chkActiveOmitPayments"
        Me.chkActiveOmitPayments.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkActiveOmitPayments.Size = New System.Drawing.Size(534, 16)
        Me.chkActiveOmitPayments.TabIndex = 8
        Me.chkActiveOmitPayments.Text = "60204 - Activate possibility to omit payments"
        Me.chkActiveOmitPayments.UseVisualStyleBackColor = False
        '
        'txtERPID
        '
        Me.txtERPID.AcceptsReturn = True
        Me.txtERPID.BackColor = System.Drawing.SystemColors.Window
        Me.txtERPID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtERPID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtERPID.Location = New System.Drawing.Point(608, 63)
        Me.txtERPID.MaxLength = 0
        Me.txtERPID.Name = "txtERPID"
        Me.txtERPID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtERPID.Size = New System.Drawing.Size(81, 20)
        Me.txtERPID.TabIndex = 21
        '
        'chkMatchARItem
        '
        Me.chkMatchARItem.BackColor = System.Drawing.SystemColors.Control
        Me.chkMatchARItem.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkMatchARItem.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkMatchARItem.Location = New System.Drawing.Point(200, 64)
        Me.chkMatchARItem.Name = "chkMatchARItem"
        Me.chkMatchARItem.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkMatchARItem.Size = New System.Drawing.Size(234, 16)
        Me.chkMatchARItem.TabIndex = 1
        Me.chkMatchARItem.Text = "60181 - Match open items from AReceiveble"
        Me.chkMatchARItem.UseVisualStyleBackColor = False
        '
        'lstMatchedColumns
        '
        Me.lstMatchedColumns.BackColor = System.Drawing.SystemColors.Window
        Me.lstMatchedColumns.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstMatchedColumns.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstMatchedColumns.Location = New System.Drawing.Point(200, 373)
        Me.lstMatchedColumns.Name = "lstMatchedColumns"
        Me.lstMatchedColumns.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstMatchedColumns.Size = New System.Drawing.Size(204, 154)
        Me.lstMatchedColumns.TabIndex = 19
        '
        'chkAskWhenRest
        '
        Me.chkAskWhenRest.BackColor = System.Drawing.SystemColors.Control
        Me.chkAskWhenRest.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkAskWhenRest.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAskWhenRest.Location = New System.Drawing.Point(480, 16)
        Me.chkAskWhenRest.Name = "chkAskWhenRest"
        Me.chkAskWhenRest.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAskWhenRest.Size = New System.Drawing.Size(517, 32)
        Me.chkAskWhenRest.TabIndex = 3
        Me.chkAskWhenRest.Text = "IKKE I BRUK! 60121 - Still kontrollsp�rsm�l n�r det er diff ved manuell avstemmin" & _
            "g"
        Me.chkAskWhenRest.UseVisualStyleBackColor = False
        Me.chkAskWhenRest.Visible = False
        '
        'chkJumpToNext
        '
        Me.chkJumpToNext.BackColor = System.Drawing.SystemColors.Control
        Me.chkJumpToNext.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkJumpToNext.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkJumpToNext.Location = New System.Drawing.Point(200, 153)
        Me.chkJumpToNext.Name = "chkJumpToNext"
        Me.chkJumpToNext.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkJumpToNext.Size = New System.Drawing.Size(534, 16)
        Me.chkJumpToNext.TabIndex = 5
        Me.chkJumpToNext.Text = "60120 - Hopp automatisk til neste post n�r differanse er null"
        Me.chkJumpToNext.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(682, 558)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 99
        Me.cmdCancel.TabStop = False
        Me.cmdCancel.Text = "55006 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(604, 558)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblColumnsEnterSequence
        '
        Me.lblColumnsEnterSequence.BackColor = System.Drawing.SystemColors.Control
        Me.lblColumnsEnterSequence.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblColumnsEnterSequence.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblColumnsEnterSequence.Location = New System.Drawing.Point(432, 349)
        Me.lblColumnsEnterSequence.Name = "lblColumnsEnterSequence"
        Me.lblColumnsEnterSequence.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblColumnsEnterSequence.Size = New System.Drawing.Size(216, 19)
        Me.lblColumnsEnterSequence.TabIndex = 10
        Me.lblColumnsEnterSequence.Text = "60206 - Velg kolonner for avstemte poster"
        '
        'lblERPID
        '
        Me.lblERPID.BackColor = System.Drawing.SystemColors.Control
        Me.lblERPID.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblERPID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblERPID.Location = New System.Drawing.Point(472, 66)
        Me.lblERPID.Name = "lblERPID"
        Me.lblERPID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblERPID.Size = New System.Drawing.Size(127, 16)
        Me.lblERPID.TabIndex = 1
        Me.lblERPID.Text = "60182 - Last used ERP-ID"
        '
        'lblColumns
        '
        Me.lblColumns.BackColor = System.Drawing.SystemColors.Control
        Me.lblColumns.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblColumns.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblColumns.Location = New System.Drawing.Point(201, 349)
        Me.lblColumns.Name = "lblColumns"
        Me.lblColumns.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblColumns.Size = New System.Drawing.Size(216, 19)
        Me.lblColumns.TabIndex = 7
        Me.lblColumns.Text = "60123 - Velg kolonner for avstemte poster"
        '
        'chkValidateMatching
        '
        Me.chkValidateMatching.BackColor = System.Drawing.SystemColors.Control
        Me.chkValidateMatching.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkValidateMatching.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkValidateMatching.Location = New System.Drawing.Point(200, 108)
        Me.chkValidateMatching.Name = "chkValidateMatching"
        Me.chkValidateMatching.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkValidateMatching.Size = New System.Drawing.Size(495, 16)
        Me.chkValidateMatching.TabIndex = 3
        Me.chkValidateMatching.Text = "60402 - Validate postings before export (recomended)"
        Me.chkValidateMatching.UseVisualStyleBackColor = False
        '
        'chkReportDeviations
        '
        Me.chkReportDeviations.BackColor = System.Drawing.SystemColors.Control
        Me.chkReportDeviations.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkReportDeviations.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkReportDeviations.Location = New System.Drawing.Point(226, 130)
        Me.chkReportDeviations.Name = "chkReportDeviations"
        Me.chkReportDeviations.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkReportDeviations.Size = New System.Drawing.Size(469, 16)
        Me.chkReportDeviations.TabIndex = 4
        Me.chkReportDeviations.Text = "60403 - Report deviations (as opposed to automatic unposting of the payment)"
        Me.chkReportDeviations.UseVisualStyleBackColor = False
        '
        'cmdChangeLabels
        '
        Me.cmdChangeLabels.BackColor = System.Drawing.SystemColors.Control
        Me.cmdChangeLabels.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdChangeLabels.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdChangeLabels.Location = New System.Drawing.Point(648, 448)
        Me.cmdChangeLabels.Name = "cmdChangeLabels"
        Me.cmdChangeLabels.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdChangeLabels.Size = New System.Drawing.Size(105, 21)
        Me.cmdChangeLabels.TabIndex = 99
        Me.cmdChangeLabels.TabStop = False
        Me.cmdChangeLabels.Text = "55048 - Prompts"
        Me.cmdChangeLabels.UseVisualStyleBackColor = False
        '
        'cmdRightClick
        '
        Me.cmdRightClick.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRightClick.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRightClick.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRightClick.Location = New System.Drawing.Point(648, 421)
        Me.cmdRightClick.Name = "cmdRightClick"
        Me.cmdRightClick.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRightClick.Size = New System.Drawing.Size(105, 21)
        Me.cmdRightClick.TabIndex = 99
        Me.cmdRightClick.TabStop = False
        Me.cmdRightClick.Text = "55049 - Rightclick"
        Me.cmdRightClick.UseVisualStyleBackColor = False
        '
        'chkDoubleMatchingUndoAll
        '
        Me.chkDoubleMatchingUndoAll.BackColor = System.Drawing.SystemColors.Control
        Me.chkDoubleMatchingUndoAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkDoubleMatchingUndoAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkDoubleMatchingUndoAll.Location = New System.Drawing.Point(226, 195)
        Me.chkDoubleMatchingUndoAll.Name = "chkDoubleMatchingUndoAll"
        Me.chkDoubleMatchingUndoAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkDoubleMatchingUndoAll.Size = New System.Drawing.Size(469, 16)
        Me.chkDoubleMatchingUndoAll.TabIndex = 7
        Me.chkDoubleMatchingUndoAll.Text = "62079 - Undo all matchings, and add a comment."
        Me.chkDoubleMatchingUndoAll.UseVisualStyleBackColor = False
        '
        'ChkDoubleMatching
        '
        Me.ChkDoubleMatching.BackColor = System.Drawing.SystemColors.Control
        Me.ChkDoubleMatching.Cursor = System.Windows.Forms.Cursors.Default
        Me.ChkDoubleMatching.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ChkDoubleMatching.Location = New System.Drawing.Point(200, 174)
        Me.ChkDoubleMatching.Name = "ChkDoubleMatching"
        Me.ChkDoubleMatching.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ChkDoubleMatching.Size = New System.Drawing.Size(384, 16)
        Me.ChkDoubleMatching.TabIndex = 6
        Me.ChkDoubleMatching.Text = "62078 - Don't allow doublematching"
        Me.ChkDoubleMatching.UseVisualStyleBackColor = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 547)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(750, 1)
        Me.lblLine1.TabIndex = 82
        Me.lblLine1.Text = "Label1"
        '
        'txtHistoryDays
        '
        Me.txtHistoryDays.AcceptsReturn = True
        Me.txtHistoryDays.BackColor = System.Drawing.SystemColors.Window
        Me.txtHistoryDays.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHistoryDays.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtHistoryDays.Location = New System.Drawing.Point(608, 86)
        Me.txtHistoryDays.MaxLength = 4
        Me.txtHistoryDays.Name = "txtHistoryDays"
        Me.txtHistoryDays.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHistoryDays.Size = New System.Drawing.Size(81, 20)
        Me.txtHistoryDays.TabIndex = 22
        Me.txtHistoryDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblHistoryDays
        '
        Me.lblHistoryDays.BackColor = System.Drawing.SystemColors.Control
        Me.lblHistoryDays.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHistoryDays.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHistoryDays.Location = New System.Drawing.Point(472, 87)
        Me.lblHistoryDays.Name = "lblHistoryDays"
        Me.lblHistoryDays.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHistoryDays.Size = New System.Drawing.Size(127, 16)
        Me.lblHistoryDays.TabIndex = 83
        Me.lblHistoryDays.Text = "60433-Days saerchhistory"
        '
        'frmMATCH_ManualSetup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(766, 582)
        Me.Controls.Add(Me.txtHistoryDays)
        Me.Controls.Add(Me.lblHistoryDays)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.chkDoubleMatchingUndoAll)
        Me.Controls.Add(Me.ChkDoubleMatching)
        Me.Controls.Add(Me.cmdRightClick)
        Me.Controls.Add(Me.cmdChangeLabels)
        Me.Controls.Add(Me.chkReportDeviations)
        Me.Controls.Add(Me.chkValidateMatching)
        Me.Controls.Add(Me.cmdOmitAccounts)
        Me.Controls.Add(Me.chkCommitTrans)
        Me.Controls.Add(Me.cmdPWStatistics)
        Me.Controls.Add(Me.chkStatistics)
        Me.Controls.Add(Me.cmdFileSwap)
        Me.Controls.Add(Me.chkExportOnlyMacthed)
        Me.Controls.Add(Me.chkUseReceivedAmount)
        Me.Controls.Add(Me.chkDeductPreviousMatched)
        Me.Controls.Add(Me.lstColumnsEnterSequence)
        Me.Controls.Add(Me.chkActiveOmitPayments)
        Me.Controls.Add(Me.txtERPID)
        Me.Controls.Add(Me.chkMatchARItem)
        Me.Controls.Add(Me.lstMatchedColumns)
        Me.Controls.Add(Me.chkAskWhenRest)
        Me.Controls.Add(Me.chkJumpToNext)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lblColumnsEnterSequence)
        Me.Controls.Add(Me.lblERPID)
        Me.Controls.Add(Me.lblColumns)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmMATCH_ManualSetup"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "60122- Oppsett manuell avstemming"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents chkValidateMatching As System.Windows.Forms.CheckBox
    Public WithEvents chkReportDeviations As System.Windows.Forms.CheckBox
    Public WithEvents cmdChangeLabels As System.Windows.Forms.Button
    Public WithEvents cmdRightClick As System.Windows.Forms.Button
    Public WithEvents chkDoubleMatchingUndoAll As System.Windows.Forms.CheckBox
    Public WithEvents ChkDoubleMatching As System.Windows.Forms.CheckBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents txtHistoryDays As System.Windows.Forms.TextBox
    Public WithEvents lblHistoryDays As System.Windows.Forms.Label
#End Region 
End Class
