<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSwapInFiles
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmbDiffType As System.Windows.Forms.ComboBox
	Public WithEvents chkSwapAuto As System.Windows.Forms.CheckBox
	Public WithEvents cmbFormat As System.Windows.Forms.ComboBox
	Public WithEvents txtName As System.Windows.Forms.TextBox
	Public WithEvents txtFileName As System.Windows.Forms.TextBox
	Public WithEvents txtDiffAccount As System.Windows.Forms.TextBox
	Public WithEvents txtSQLName As System.Windows.Forms.TextBox
	Public WithEvents chkAllowDiff As System.Windows.Forms.CheckBox
	Public WithEvents cmdFileOpen As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents CmdOK As System.Windows.Forms.Button
	Public WithEvents lbDiffType As System.Windows.Forms.Label
	Public WithEvents lblSQLName As System.Windows.Forms.Label
	Public WithEvents lblDiffaccount As System.Windows.Forms.Label
	Public WithEvents lblInFormat As System.Windows.Forms.Label
	Public WithEvents lblFileName As System.Windows.Forms.Label
	Public WithEvents lblName As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSwapInFiles))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmbDiffType = New System.Windows.Forms.ComboBox
        Me.chkSwapAuto = New System.Windows.Forms.CheckBox
        Me.cmbFormat = New System.Windows.Forms.ComboBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtFileName = New System.Windows.Forms.TextBox
        Me.txtDiffAccount = New System.Windows.Forms.TextBox
        Me.txtSQLName = New System.Windows.Forms.TextBox
        Me.chkAllowDiff = New System.Windows.Forms.CheckBox
        Me.cmdFileOpen = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.CmdOK = New System.Windows.Forms.Button
        Me.lbDiffType = New System.Windows.Forms.Label
        Me.lblSQLName = New System.Windows.Forms.Label
        Me.lblDiffaccount = New System.Windows.Forms.Label
        Me.lblInFormat = New System.Windows.Forms.Label
        Me.lblFileName = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.cmbSelectSwapSetup = New System.Windows.Forms.ComboBox
        Me.cmdSave = New System.Windows.Forms.Button
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.cmdNew = New System.Windows.Forms.Button
        Me.txtSwapID = New System.Windows.Forms.TextBox
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.chkRemoveAtExport = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'cmbDiffType
        '
        Me.cmbDiffType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbDiffType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbDiffType.Enabled = False
        Me.cmbDiffType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbDiffType.Location = New System.Drawing.Point(354, 271)
        Me.cmbDiffType.Name = "cmbDiffType"
        Me.cmbDiffType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbDiffType.Size = New System.Drawing.Size(220, 21)
        Me.cmbDiffType.TabIndex = 16
        Me.cmbDiffType.Text = "cmbDiffType"
        '
        'chkSwapAuto
        '
        Me.chkSwapAuto.BackColor = System.Drawing.SystemColors.Control
        Me.chkSwapAuto.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkSwapAuto.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkSwapAuto.Location = New System.Drawing.Point(315, 342)
        Me.chkSwapAuto.Name = "chkSwapAuto"
        Me.chkSwapAuto.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkSwapAuto.Size = New System.Drawing.Size(130, 17)
        Me.chkSwapAuto.TabIndex = 6
        Me.chkSwapAuto.Text = "62041-Bytt ut automatisk"
        Me.chkSwapAuto.UseVisualStyleBackColor = False
        '
        'cmbFormat
        '
        Me.cmbFormat.BackColor = System.Drawing.SystemColors.Window
        Me.cmbFormat.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFormat.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbFormat.Location = New System.Drawing.Point(354, 204)
        Me.cmbFormat.Name = "cmbFormat"
        Me.cmbFormat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbFormat.Size = New System.Drawing.Size(220, 21)
        Me.cmbFormat.TabIndex = 2
        '
        'txtName
        '
        Me.txtName.AcceptsReturn = True
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtName.Location = New System.Drawing.Point(354, 140)
        Me.txtName.MaxLength = 35
        Me.txtName.Name = "txtName"
        Me.txtName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtName.Size = New System.Drawing.Size(220, 20)
        Me.txtName.TabIndex = 0
        '
        'txtFileName
        '
        Me.txtFileName.AcceptsReturn = True
        Me.txtFileName.BackColor = System.Drawing.SystemColors.Window
        Me.txtFileName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFileName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFileName.Location = New System.Drawing.Point(354, 172)
        Me.txtFileName.MaxLength = 150
        Me.txtFileName.Name = "txtFileName"
        Me.txtFileName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFileName.Size = New System.Drawing.Size(220, 20)
        Me.txtFileName.TabIndex = 1
        '
        'txtDiffAccount
        '
        Me.txtDiffAccount.AcceptsReturn = True
        Me.txtDiffAccount.BackColor = System.Drawing.SystemColors.Window
        Me.txtDiffAccount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDiffAccount.Enabled = False
        Me.txtDiffAccount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDiffAccount.Location = New System.Drawing.Point(354, 238)
        Me.txtDiffAccount.MaxLength = 255
        Me.txtDiffAccount.Name = "txtDiffAccount"
        Me.txtDiffAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDiffAccount.Size = New System.Drawing.Size(220, 20)
        Me.txtDiffAccount.TabIndex = 3
        '
        'txtSQLName
        '
        Me.txtSQLName.AcceptsReturn = True
        Me.txtSQLName.BackColor = System.Drawing.SystemColors.Window
        Me.txtSQLName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSQLName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSQLName.Location = New System.Drawing.Point(354, 307)
        Me.txtSQLName.MaxLength = 49
        Me.txtSQLName.Name = "txtSQLName"
        Me.txtSQLName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSQLName.Size = New System.Drawing.Size(220, 20)
        Me.txtSQLName.TabIndex = 4
        '
        'chkAllowDiff
        '
        Me.chkAllowDiff.BackColor = System.Drawing.SystemColors.Control
        Me.chkAllowDiff.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkAllowDiff.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAllowDiff.Location = New System.Drawing.Point(199, 342)
        Me.chkAllowDiff.Name = "chkAllowDiff"
        Me.chkAllowDiff.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAllowDiff.Size = New System.Drawing.Size(130, 17)
        Me.chkAllowDiff.TabIndex = 5
        Me.chkAllowDiff.Text = "62040-Tillat avvik"
        Me.chkAllowDiff.UseVisualStyleBackColor = False
        '
        'cmdFileOpen
        '
        Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
        Me.cmdFileOpen.Location = New System.Drawing.Point(582, 169)
        Me.cmdFileOpen.Name = "cmdFileOpen"
        Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen.TabIndex = 12
        Me.cmdFileOpen.TabStop = False
        Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(454, 384)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 7
        Me.CmdCancel.Text = "55006-&Cancel"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'CmdOK
        '
        Me.CmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.CmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdOK.Location = New System.Drawing.Point(532, 383)
        Me.CmdOK.Name = "CmdOK"
        Me.CmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdOK.Size = New System.Drawing.Size(73, 21)
        Me.CmdOK.TabIndex = 8
        Me.CmdOK.Text = "&OK"
        Me.CmdOK.UseVisualStyleBackColor = False
        '
        'lbDiffType
        '
        Me.lbDiffType.BackColor = System.Drawing.SystemColors.Control
        Me.lbDiffType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lbDiffType.Enabled = False
        Me.lbDiffType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbDiffType.Location = New System.Drawing.Point(200, 271)
        Me.lbDiffType.Name = "lbDiffType"
        Me.lbDiffType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbDiffType.Size = New System.Drawing.Size(154, 22)
        Me.lbDiffType.TabIndex = 17
        Me.lbDiffType.Text = "60284-AccountType"
        '
        'lblSQLName
        '
        Me.lblSQLName.BackColor = System.Drawing.SystemColors.Control
        Me.lblSQLName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSQLName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSQLName.Location = New System.Drawing.Point(199, 309)
        Me.lblSQLName.Name = "lblSQLName"
        Me.lblSQLName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSQLName.Size = New System.Drawing.Size(148, 27)
        Me.lblSQLName.TabIndex = 15
        Me.lblSQLName.Text = "62039Navn p� SQL som skal kj�res"
        '
        'lblDiffaccount
        '
        Me.lblDiffaccount.BackColor = System.Drawing.SystemColors.Control
        Me.lblDiffaccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDiffaccount.Enabled = False
        Me.lblDiffaccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDiffaccount.Location = New System.Drawing.Point(199, 237)
        Me.lblDiffaccount.Name = "lblDiffaccount"
        Me.lblDiffaccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDiffaccount.Size = New System.Drawing.Size(150, 27)
        Me.lblDiffaccount.TabIndex = 14
        Me.lblDiffaccount.Text = "62038-Avvikskonto for differanse"
        '
        'lblInFormat
        '
        Me.lblInFormat.BackColor = System.Drawing.SystemColors.Control
        Me.lblInFormat.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblInFormat.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblInFormat.Location = New System.Drawing.Point(200, 203)
        Me.lblInFormat.Name = "lblInFormat"
        Me.lblInFormat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblInFormat.Size = New System.Drawing.Size(145, 27)
        Me.lblInFormat.TabIndex = 13
        Me.lblInFormat.Text = "62037-Velg format for fil med nytt innhold"
        '
        'lblFileName
        '
        Me.lblFileName.BackColor = System.Drawing.SystemColors.Control
        Me.lblFileName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFileName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFileName.Location = New System.Drawing.Point(199, 172)
        Me.lblFileName.Name = "lblFileName"
        Me.lblFileName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFileName.Size = New System.Drawing.Size(145, 27)
        Me.lblFileName.TabIndex = 11
        Me.lblFileName.Text = "62036-Filnavn/sti for fil med nytt innhold"
        '
        'lblName
        '
        Me.lblName.BackColor = System.Drawing.SystemColors.Control
        Me.lblName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblName.Location = New System.Drawing.Point(199, 143)
        Me.lblName.Name = "lblName"
        Me.lblName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblName.Size = New System.Drawing.Size(144, 27)
        Me.lblName.TabIndex = 10
        Me.lblName.Text = "62035-Navn p� utbyttingsregel"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(200, 17)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(393, 17)
        Me.lblHeading.TabIndex = 9
        Me.lblHeading.Text = "62034 - Utbytting av info i filer"
        '
        'cmbSelectSwapSetup
        '
        Me.cmbSelectSwapSetup.FormattingEnabled = True
        Me.cmbSelectSwapSetup.Location = New System.Drawing.Point(354, 103)
        Me.cmbSelectSwapSetup.Name = "cmbSelectSwapSetup"
        Me.cmbSelectSwapSetup.Size = New System.Drawing.Size(218, 21)
        Me.cmbSelectSwapSetup.TabIndex = 19
        '
        'cmdSave
        '
        Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Location = New System.Drawing.Point(354, 383)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSave.Size = New System.Drawing.Size(73, 21)
        Me.cmdSave.TabIndex = 20
        Me.cmdSave.Text = "55034-&Save"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'cmdDelete
        '
        Me.cmdDelete.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDelete.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDelete.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDelete.Location = New System.Drawing.Point(275, 383)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDelete.Size = New System.Drawing.Size(73, 21)
        Me.cmdDelete.TabIndex = 21
        Me.cmdDelete.Text = "55043-Delete"
        Me.cmdDelete.UseVisualStyleBackColor = False
        '
        'cmdNew
        '
        Me.cmdNew.BackColor = System.Drawing.SystemColors.Control
        Me.cmdNew.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdNew.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNew.Location = New System.Drawing.Point(196, 383)
        Me.cmdNew.Name = "cmdNew"
        Me.cmdNew.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdNew.Size = New System.Drawing.Size(73, 21)
        Me.cmdNew.TabIndex = 22
        Me.cmdNew.Text = "55047-New"
        Me.cmdNew.UseVisualStyleBackColor = False
        '
        'txtSwapID
        '
        Me.txtSwapID.AcceptsReturn = True
        Me.txtSwapID.BackColor = System.Drawing.SystemColors.Window
        Me.txtSwapID.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSwapID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSwapID.Location = New System.Drawing.Point(12, 339)
        Me.txtSwapID.MaxLength = 49
        Me.txtSwapID.Name = "txtSwapID"
        Me.txtSwapID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSwapID.Size = New System.Drawing.Size(43, 20)
        Me.txtSwapID.TabIndex = 23
        Me.txtSwapID.Visible = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 375)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(605, 1)
        Me.lblLine1.TabIndex = 88
        Me.lblLine1.Text = "Label1"
        '
        'chkRemoveAtExport
        '
        Me.chkRemoveAtExport.BackColor = System.Drawing.SystemColors.Control
        Me.chkRemoveAtExport.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkRemoveAtExport.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRemoveAtExport.Location = New System.Drawing.Point(445, 342)
        Me.chkRemoveAtExport.Name = "chkRemoveAtExport"
        Me.chkRemoveAtExport.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkRemoveAtExport.Size = New System.Drawing.Size(170, 17)
        Me.chkRemoveAtExport.TabIndex = 89
        Me.chkRemoveAtExport.Text = "61018 - Remove file at export"
        Me.chkRemoveAtExport.UseVisualStyleBackColor = False
        '
        'frmSwapInFiles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(622, 417)
        Me.Controls.Add(Me.chkRemoveAtExport)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtSwapID)
        Me.Controls.Add(Me.cmdNew)
        Me.Controls.Add(Me.cmdDelete)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.cmbSelectSwapSetup)
        Me.Controls.Add(Me.cmbDiffType)
        Me.Controls.Add(Me.chkSwapAuto)
        Me.Controls.Add(Me.cmbFormat)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.txtFileName)
        Me.Controls.Add(Me.txtDiffAccount)
        Me.Controls.Add(Me.txtSQLName)
        Me.Controls.Add(Me.chkAllowDiff)
        Me.Controls.Add(Me.cmdFileOpen)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdOK)
        Me.Controls.Add(Me.lbDiffType)
        Me.Controls.Add(Me.lblSQLName)
        Me.Controls.Add(Me.lblDiffaccount)
        Me.Controls.Add(Me.lblInFormat)
        Me.Controls.Add(Me.lblFileName)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSwapInFiles"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "62034 - Utbytting av info i filer"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbSelectSwapSetup As System.Windows.Forms.ComboBox
    Public WithEvents cmdSave As System.Windows.Forms.Button
    Public WithEvents cmdDelete As System.Windows.Forms.Button
    Public WithEvents cmdNew As System.Windows.Forms.Button
    Public WithEvents txtSwapID As System.Windows.Forms.TextBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Public WithEvents chkRemoveAtExport As System.Windows.Forms.CheckBox
#End Region 
End Class
