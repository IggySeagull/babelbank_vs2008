﻿Public Class frmDNBTBI_NewAccount
    Public iDialogResult As Integer
    Private Sub cmdCancel_Click()
        Me.Hide()
        iDialogResult = -1
    End Sub
    Private Sub Form_Load()
        FormvbStyle(Me, "", 600)
        FormLRSCaptions(Me)
    End Sub
    Private Sub OKButton_Click()
        Me.Hide()
        iDialogResult = Me.lstClients.SelectedIndex
    End Sub
    Private Sub cmdOK_Click()
        Me.Hide()
        iDialogResult = Me.lstClients.SelectedIndex
    End Sub
    Public Sub LoadClients(ByVal aClients(,) As String)
        ' receives a list of clients, and fill listbox
        'aClients has Client_ID, ClientNo, ClientName
        Dim i As Integer
        Me.lstClients.Items.Clear()
        For i = 0 To UBound(aClients, 2)
            Me.lstClients.Items.Add(aClients(1, i) & " " & aClients(2, i))
        Next i

    End Sub
    Public Sub LoadAccount(ByVal sAccountNo As String)
        Me.lblAccountNo.Text = "AccountNo" & ":" & sAccountNo
    End Sub

End Class