<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmVisma_LogView
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtLogFile As System.Windows.Forms.TextBox
    Public WithEvents lblLogfile As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVisma_LogView))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtLogFile = New System.Windows.Forms.TextBox
        Me.lblLogfile = New System.Windows.Forms.Label
        Me.txtLog = New System.Windows.Forms.TextBox
        Me.cmdClose = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtLogFile
        '
        Me.txtLogFile.AcceptsReturn = True
        Me.txtLogFile.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.txtLogFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLogFile.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLogFile.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLogFile.Location = New System.Drawing.Point(142, 21)
        Me.txtLogFile.MaxLength = 0
        Me.txtLogFile.Name = "txtLogFile"
        Me.txtLogFile.ReadOnly = True
        Me.txtLogFile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLogFile.Size = New System.Drawing.Size(312, 20)
        Me.txtLogFile.TabIndex = 4
        Me.txtLogFile.TabStop = False
        '
        'lblLogfile
        '
        Me.lblLogfile.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.lblLogfile.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLogfile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLogfile.Location = New System.Drawing.Point(16, 21)
        Me.lblLogfile.Name = "lblLogfile"
        Me.lblLogfile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLogfile.Size = New System.Drawing.Size(121, 13)
        Me.lblLogfile.TabIndex = 5
        Me.lblLogfile.Text = "60364-Logging til fil"
        '
        'txtLog
        '
        Me.txtLog.AcceptsReturn = True
        Me.txtLog.BackColor = System.Drawing.SystemColors.Control
        Me.txtLog.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLog.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLog.Location = New System.Drawing.Point(19, 57)
        Me.txtLog.MaxLength = 0
        Me.txtLog.Multiline = True
        Me.txtLog.Name = "txtLog"
        Me.txtLog.ReadOnly = True
        Me.txtLog.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtLog.Size = New System.Drawing.Size(435, 270)
        Me.txtLog.TabIndex = 7
        Me.txtLog.Text = "Log .."
        '
        'cmdClose
        '
        Me.cmdClose.BackColor = System.Drawing.SystemColors.Control
        Me.cmdClose.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClose.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdClose.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClose.Location = New System.Drawing.Point(354, 334)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClose.Size = New System.Drawing.Size(100, 24)
        Me.cmdClose.TabIndex = 37
        Me.cmdClose.Text = "35118-Lukk"
        Me.cmdClose.UseVisualStyleBackColor = False
        '
        'frmVisma_LogView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(472, 348)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.txtLog)
        Me.Controls.Add(Me.txtLogFile)
        Me.Controls.Add(Me.lblLogfile)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(8, 30)
        Me.Name = "frmVisma_LogView"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Visma Payment (BabelBank)"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents txtLog As System.Windows.Forms.TextBox
    Public WithEvents cmdClose As System.Windows.Forms.Button
#End Region 
End Class