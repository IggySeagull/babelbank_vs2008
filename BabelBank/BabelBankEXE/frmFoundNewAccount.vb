Option Strict Off
Option Explicit On
Friend Class frmFoundNewAccount
	Inherits System.Windows.Forms.Form
	Public bCancel As Boolean
	Dim oClients As vbBabel.Clients
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Dim response As Object
		
		'UPGRADE_WARNING: Couldn't resolve default property of object response. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		response = MsgBox(LRS(60050) & vbCrLf & LRS(60051), MsgBoxStyle.YesNo + MsgBoxStyle.Question + MsgBoxStyle.DefaultButton2, "Avbryt BabelBank")
		'"�nsker du � avbryte programmet" og "De p�begynte oppgaver vil ikke fullf�res."
		If response = MsgBoxResult.Yes Then
			bCancel = True
			Me.Hide()
		Else
			If txtClientNo.Enabled = True Then
				txtClientNo.Focus()
			Else
				CmdCancel.Focus()
			End If
		End If
		
		
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		Dim bCheck As Boolean
		
		If txtClientNo.Visible = True Then
            If txtClientNo.Text = "" Then
                MsgBox(LRS(60052), MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, LRS(60053))
                '"Klientnummer m� fylles ut!" og "Feil klientnummer"
                txtClientNo.Focus()
                bCheck = False
            Else
                bCheck = True
            End If
        Else
            bCheck = True
        End If

        If bCheck Then
            If txtClientName.Visible = True Then
                If txtClientName.Text = "" Then
                    MsgBox(LRS(60052), MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, LRS(60053))
                    '"Klientnavn m� fylles ut!" og "Feil klientnavn"
                    txtClientName.Focus()
                    bCheck = False
                Else
                    bCheck = True
                End If
            Else
                bCheck = True
            End If
        End If
		
		If bCheck Then
			Me.Hide()
		End If
		
	End Sub
	Private Sub frmFoundNewAccount_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		bCancel = False
		FormvbStyle(Me, "")
		
	End Sub
	
	Public Function ExistingClients(ByRef oC As Object) As Boolean
		
		oClients = oC
		
		ExistingClients = True
		
	End Function
	
	
	
	Private Sub txtClientNo_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtClientNo.Leave
		Dim oClient As Object
		
		For	Each oClient In oClients
			'UPGRADE_WARNING: Couldn't resolve default property of object oClient.ClientNo. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If txtClientNo.Text = oClient.ClientNo Then
				'UPGRADE_WARNING: Couldn't resolve default property of object oClient.Name. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				txtClientName.Text = oClient.Name
				Exit For
			End If
		Next oClient
		
	End Sub
End Class
