<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmChangeFilePaths
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdFilepathToOpen As System.Windows.Forms.Button
	Public WithEvents cmdFilepathFromOpen As System.Windows.Forms.Button
	Public WithEvents cmdSave As System.Windows.Forms.Button
	Public WithEvents cmdChangeFromAndTo As System.Windows.Forms.Button
	Public WithEvents cmdRun As System.Windows.Forms.Button
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents txtFilepathTo As System.Windows.Forms.TextBox
	Public WithEvents txtFilepathFrom As System.Windows.Forms.TextBox
    Public WithEvents lblFilepathToOpen As System.Windows.Forms.Label
	Public WithEvents lblFilepathFromOpen As System.Windows.Forms.Label
	Public WithEvents lblExplaination As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmChangeFilePaths))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdFilepathToOpen = New System.Windows.Forms.Button
        Me.cmdFilepathFromOpen = New System.Windows.Forms.Button
        Me.cmdSave = New System.Windows.Forms.Button
        Me.cmdChangeFromAndTo = New System.Windows.Forms.Button
        Me.cmdRun = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.txtFilepathTo = New System.Windows.Forms.TextBox
        Me.txtFilepathFrom = New System.Windows.Forms.TextBox
        Me.lblFilepathToOpen = New System.Windows.Forms.Label
        Me.lblFilepathFromOpen = New System.Windows.Forms.Label
        Me.lblExplaination = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdFilepathToOpen
        '
        Me.cmdFilepathToOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFilepathToOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFilepathToOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFilepathToOpen.Image = CType(resources.GetObject("cmdFilepathToOpen.Image"), System.Drawing.Image)
        Me.cmdFilepathToOpen.Location = New System.Drawing.Point(688, 224)
        Me.cmdFilepathToOpen.Name = "cmdFilepathToOpen"
        Me.cmdFilepathToOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFilepathToOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFilepathToOpen.TabIndex = 10
        Me.cmdFilepathToOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFilepathToOpen.UseVisualStyleBackColor = False
        '
        'cmdFilepathFromOpen
        '
        Me.cmdFilepathFromOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFilepathFromOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFilepathFromOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFilepathFromOpen.Image = CType(resources.GetObject("cmdFilepathFromOpen.Image"), System.Drawing.Image)
        Me.cmdFilepathFromOpen.Location = New System.Drawing.Point(688, 192)
        Me.cmdFilepathFromOpen.Name = "cmdFilepathFromOpen"
        Me.cmdFilepathFromOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFilepathFromOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFilepathFromOpen.TabIndex = 9
        Me.cmdFilepathFromOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFilepathFromOpen.UseVisualStyleBackColor = False
        '
        'cmdSave
        '
        Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Location = New System.Drawing.Point(472, 296)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSave.Size = New System.Drawing.Size(73, 21)
        Me.cmdSave.TabIndex = 8
        Me.cmdSave.Text = "55034 &Save"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'cmdChangeFromAndTo
        '
        Me.cmdChangeFromAndTo.BackColor = System.Drawing.SystemColors.Control
        Me.cmdChangeFromAndTo.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdChangeFromAndTo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdChangeFromAndTo.Location = New System.Drawing.Point(576, 256)
        Me.cmdChangeFromAndTo.Name = "cmdChangeFromAndTo"
        Me.cmdChangeFromAndTo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdChangeFromAndTo.Size = New System.Drawing.Size(129, 21)
        Me.cmdChangeFromAndTo.TabIndex = 7
        Me.cmdChangeFromAndTo.Text = "60271 original/new"
        Me.cmdChangeFromAndTo.UseVisualStyleBackColor = False
        '
        'cmdRun
        '
        Me.cmdRun.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRun.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRun.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRun.Location = New System.Drawing.Point(552, 296)
        Me.cmdRun.Name = "cmdRun"
        Me.cmdRun.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRun.Size = New System.Drawing.Size(73, 21)
        Me.cmdRun.TabIndex = 6
        Me.cmdRun.Text = "55033 &Run"
        Me.cmdRun.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(632, 296)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 5
        Me.CmdCancel.Text = "55006 &Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'txtFilepathTo
        '
        Me.txtFilepathTo.AcceptsReturn = True
        Me.txtFilepathTo.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilepathTo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilepathTo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilepathTo.Location = New System.Drawing.Point(288, 224)
        Me.txtFilepathTo.MaxLength = 255
        Me.txtFilepathTo.Name = "txtFilepathTo"
        Me.txtFilepathTo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilepathTo.Size = New System.Drawing.Size(385, 19)
        Me.txtFilepathTo.TabIndex = 4
        '
        'txtFilepathFrom
        '
        Me.txtFilepathFrom.AcceptsReturn = True
        Me.txtFilepathFrom.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilepathFrom.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFilepathFrom.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilepathFrom.Location = New System.Drawing.Point(288, 192)
        Me.txtFilepathFrom.MaxLength = 255
        Me.txtFilepathFrom.Name = "txtFilepathFrom"
        Me.txtFilepathFrom.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFilepathFrom.Size = New System.Drawing.Size(385, 19)
        Me.txtFilepathFrom.TabIndex = 2
        '
        'lblFilepathToOpen
        '
        Me.lblFilepathToOpen.BackColor = System.Drawing.SystemColors.Control
        Me.lblFilepathToOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilepathToOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilepathToOpen.Location = New System.Drawing.Point(200, 224)
        Me.lblFilepathToOpen.Name = "lblFilepathToOpen"
        Me.lblFilepathToOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilepathToOpen.Size = New System.Drawing.Size(87, 19)
        Me.lblFilepathToOpen.TabIndex = 3
        Me.lblFilepathToOpen.Text = "60270 New path:"
        '
        'lblFilepathFromOpen
        '
        Me.lblFilepathFromOpen.BackColor = System.Drawing.SystemColors.Control
        Me.lblFilepathFromOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFilepathFromOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFilepathFromOpen.Location = New System.Drawing.Point(200, 192)
        Me.lblFilepathFromOpen.Name = "lblFilepathFromOpen"
        Me.lblFilepathFromOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFilepathFromOpen.Size = New System.Drawing.Size(87, 19)
        Me.lblFilepathFromOpen.TabIndex = 1
        Me.lblFilepathFromOpen.Text = "60269 Orig path:"
        '
        'lblExplaination
        '
        Me.lblExplaination.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplaination.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplaination.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplaination.Location = New System.Drawing.Point(200, 96)
        Me.lblExplaination.Name = "lblExplaination"
        Me.lblExplaination.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplaination.Size = New System.Drawing.Size(505, 65)
        Me.lblExplaination.TabIndex = 0
        Me.lblExplaination.Text = "60268 - Explain the form"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 290)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(700, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmChangeFilePaths
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(717, 329)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdFilepathToOpen)
        Me.Controls.Add(Me.cmdFilepathFromOpen)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.cmdChangeFromAndTo)
        Me.Controls.Add(Me.cmdRun)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.txtFilepathTo)
        Me.Controls.Add(Me.txtFilepathFrom)
        Me.Controls.Add(Me.lblFilepathToOpen)
        Me.Controls.Add(Me.lblFilepathFromOpen)
        Me.Controls.Add(Me.lblExplaination)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmChangeFilePaths"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "60267 - Change all filepaths in BabelBank"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
