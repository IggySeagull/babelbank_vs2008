Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic

Friend Class frmNordeaLiv
    '**************************************************************************
    'NOT CONVERTED TO VB.NET !!!!!!!!!!!!!!!
    '**************************************************************************
    Inherits System.Windows.Forms.Form
    '    Private sCompanyID As String
    '    Private conArchiveDB As ADODB.Connection
    '    Private bConnectionOpen As Boolean
    '    Private bMarkedPaymentsExists As Boolean


    '    Private Sub chkOutPayments_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkOutPayments.Click
    '        'Check if there are any holes in the l�penr. If so report them.
    '        Dim rsOutPayments As ADODB.Recordset
    '        Dim sMySQL As String
    '        Dim sIBDate As String
    '        Dim sUBDate As String
    '        Dim sMonth As String
    '        Dim lCounter, lCounter2 As Integer
    '        Dim sLastID As String
    '        Dim aMissingIDsArray() As String
    '        Dim iMissingIDsCounter As Short
    '        Dim aDoubleIDsArray(,) As String
    '        'UPGRADE_ISSUE: The preceding line couldn't be parsed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="82EBB1AE-1FCB-4FEF-9E6C-8736A316F8A7"'
    '        'DOTNETT REDIM Dim aDoubleIDsArray() As String
    '        Dim iDoubleIDsCounter As Short
    '        Dim sErrorString As String

    '        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

    '        Me.txtResult.Text = ""

    '        ConnectToArchive()

    '        rsOutPayments = CreateObject ("ADODB.Recordset")

    '        sMonth = Mid(Me.txtStartDate.Text, InStr(Me.txtStartDate.Text, ".") + 1, 2)
    '        If VB.Right(sMonth, 1) = "." Then
    '            ' only one digit for month
    '            sMonth = "0" & VB.Left(sMonth, 1)
    '        End If
    '        sIBDate = DateToString(System.DateTime.FromOADate(StringToDate(VB.Right(Me.txtStartDate.Text, 4) & sMonth & VB.Left(Me.txtStartDate.Text, InStr(Me.txtStartDate.Text, ".") - 1)).ToOADate - 1))

    '        sMonth = Mid(Me.txtEndDate.Text, InStr(Me.txtEndDate.Text, ".") + 1, 2)
    '        If VB.Right(sMonth, 1) = "." Then
    '            ' only one digit for month
    '            sMonth = "0" & VB.Left(sMonth, 1)
    '        End If
    '        sUBDate = VB.Right(Me.txtEndDate.Text, 4) & sMonth & VB.Left(Me.txtEndDate.Text, InStr(Me.txtEndDate.Text, ".") - 1)

    '        If sIBDate > sUBDate Then
    '            'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '            'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '            Me.Cursor = System.Windows.Forms.Cursors.Default
    '            MsgBox("Startdato ved kontrollen kan ikke v�re senere enn angitt sluttdato.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Feil i dato!")
    '            Exit Sub
    '        End If

    '        sMySQL = "SELECT P.REF_Bank1 AS REF, P.E_Name AS PName, P.DATE_Payment AS Bookdate, P.MON_InvoiceAmount as PAmount FROM BankavstKoll.Payment P, BankavstKoll.BabelFile B WHERE P.Company_ID = B.Company_ID AND P.babelFile_ID = B.BabelFile_ID AND P.DATE_Payment >= '" & sIBDate & "' AND P.DATE_Payment <= '" & sUBDate & "' AND P.Company_ID = " & sCompanyID & " AND B.Importformat = " & Trim(Str(vbBabel.BabelFiles.FileType.KLINK_ExcelUtbetaling)) & " ORDER BY P.REF_Bank1 ASC"

    '        rsOutPayments.Open(sMySQL, conArchiveDB, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

    '        If rsOutPayments.RecordCount < 1 Then
    '            'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '            'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '            Me.Cursor = System.Windows.Forms.Cursors.Default
    '            MsgBox("BabelBank finner ingen utbetalinger fra K-Link i det angitte datointervallet.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Ingen poster!")
    '            Exit Sub
    '        Else
    '            iMissingIDsCounter = -1
    '            iDoubleIDsCounter = -1
    '            ReDim aDoubleIDsArray(3, 0)
    '            '0 - Keeps the ID
    '            '1 - Keeps the name
    '            '2 - Keeps the amount
    '            '3 - Keeps the date
    '            ReDim Preserve aMissingIDsArray(0)
    '            sLastID = rsOutPayments.Fields("REF").Value
    '            rsOutPayments.MoveNext()
    '            For lCounter = 2 To rsOutPayments.RecordCount
    '                'Check if we have a double occurance of an ID
    '                If sLastID = rsOutPayments.Fields("REF").Value Then
    '                    iDoubleIDsCounter = iDoubleIDsCounter + 1
    '                    If iDoubleIDsCounter > 0 Then
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(0, iDoubleIDsCounter - 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        If sLastID = aDoubleIDsArray(0, iDoubleIDsCounter - 1) Then
    '                            'OK the ID is already in the array
    '                            iDoubleIDsCounter = iDoubleIDsCounter - 1
    '                        Else
    '                            'Store the ID
    '                            ReDim Preserve aDoubleIDsArray(3, iDoubleIDsCounter)
    '                            'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(0, iDoubleIDsCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                            aDoubleIDsArray(0, iDoubleIDsCounter) = sLastID
    '                            If Not IsDBNull(rsOutPayments.Fields("PName").Value) Then
    '                                'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(1, iDoubleIDsCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                aDoubleIDsArray(1, iDoubleIDsCounter) = Trim(rsOutPayments.Fields("PName").Value)
    '                            End If
    '                            If Not IsDBNull(rsOutPayments.Fields("Bookdate").Value) Then
    '                                'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(2, iDoubleIDsCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                aDoubleIDsArray(2, iDoubleIDsCounter) = Trim(rsOutPayments.Fields("Bookdate").Value)
    '                            End If
    '                            If Not IsDBNull(rsOutPayments.Fields("PAmount").Value) Then
    '                                'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(3, iDoubleIDsCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                aDoubleIDsArray(3, iDoubleIDsCounter) = ConvertFromAmountToString(rsOutPayments.Fields("PAmount").Value, ".", ",")
    '                            End If
    '                        End If
    '                    Else
    '                        'No IDs stored
    '                        'Store the ID
    '                        ReDim Preserve aDoubleIDsArray(3, iDoubleIDsCounter)
    '                        'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(0, iDoubleIDsCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        aDoubleIDsArray(0, iDoubleIDsCounter) = sLastID
    '                        If Not IsDBNull(rsOutPayments.Fields("PName").Value) Then
    '                            'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(1, iDoubleIDsCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                            aDoubleIDsArray(1, iDoubleIDsCounter) = Trim(rsOutPayments.Fields("PName").Value)
    '                        End If
    '                        If Not IsDBNull(rsOutPayments.Fields("Bookdate").Value) Then
    '                            'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(2, iDoubleIDsCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                            aDoubleIDsArray(2, iDoubleIDsCounter) = Trim(rsOutPayments.Fields("Bookdate").Value)
    '                        End If
    '                        If Not IsDBNull(rsOutPayments.Fields("PAmount").Value) Then
    '                            'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(3, iDoubleIDsCounter). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                            aDoubleIDsArray(3, iDoubleIDsCounter) = ConvertFromAmountToString(rsOutPayments.Fields("PAmount").Value, ".", ",")
    '                        End If
    '                    End If
    '                    'Check if the ID is 1 higher than the previous one
    '                ElseIf Not IsEqualAmount(Val(sLastID) + 1, Val(rsOutPayments.Fields("REF").Value)) Then
    '                    'It may be more than one missing. Add them all
    '                    For lCounter2 = Val(sLastID) + 1 To Val(rsOutPayments.Fields("REF").Value)
    '                        If lCounter2 < Val(rsOutPayments.Fields("REF").Value) Then
    '                            iMissingIDsCounter = iMissingIDsCounter + 1
    '                            ReDim Preserve aMissingIDsArray(iMissingIDsCounter)
    '                            aMissingIDsArray(iMissingIDsCounter) = Trim(Str(lCounter2))
    '                        End If
    '                    Next lCounter2
    '                End If

    '                sLastID = rsOutPayments.Fields("REF").Value

    '                rsOutPayments.MoveNext()
    '            Next lCounter
    '        End If

    '        sErrorString = ""

    '        'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '        If Not EmptyString(aDoubleIDsArray(0, 0)) Then
    '            sErrorString = sErrorString & "F�lgende poster ligger dobbelt:"
    '            For lCounter = 0 To UBound(aDoubleIDsArray, 2)
    '                'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                sErrorString = sErrorString & vbCrLf & "ID: " & aDoubleIDsArray(0, lCounter)
    '                'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                sErrorString = sErrorString & " - Dato: " & aDoubleIDsArray(2, lCounter)
    '                'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                sErrorString = sErrorString & " - Bel�p: " & PadLeft(aDoubleIDsArray(3, lCounter), 17, " ")
    '                'UPGRADE_WARNING: Couldn't resolve default property of object aDoubleIDsArray(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                sErrorString = sErrorString & " - Navn: " & aDoubleIDsArray(1, lCounter)
    '            Next lCounter
    '        End If

    '        If Not EmptyString(aMissingIDsArray(0)) Then
    '            If Not EmptyString(sErrorString) Then
    '                sErrorString = sErrorString & vbCrLf & vbCrLf
    '            End If
    '            sErrorString = sErrorString & "F�lgende poster mangler:"
    '            For lCounter = 0 To UBound(aMissingIDsArray, 1)
    '                sErrorString = sErrorString & vbCrLf & "ID: " & aMissingIDsArray(lCounter)
    '            Next lCounter
    '        End If

    '        ShowHideControls(False, False, True)

    '        If rsOutPayments.State = ADODB.ObjectStateEnum.adStateOpen Then
    '            rsOutPayments.Close()
    '        End If
    '        If Not rsOutPayments Is Nothing Then
    '            'UPGRADE_NOTE: Object rsOutPayments may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            rsOutPayments = Nothing
    '        End If

    '        Me.txtResult.Text = sErrorString

    '        'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '        'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '        Me.Cursor = System.Windows.Forms.Cursors.Default

    '    End Sub

    '    Private Sub chkRemoveFiles_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkRemoveFiles.Click
    '        'This control gives the user a possibility to remove files from the archive DB
    '        Dim rsImportedFiles As ADODB.Recordset
    '        Dim sMySQL As String
    '        Dim sIBDate As String
    '        Dim sUBDate As String
    '        Dim sMonth As String
    '        Dim lCounter As Integer
    '        Dim sBabelFileID As String
    '        Dim sImportFormat As String
    '        Dim sDateProduction As String
    '        Dim sMON_InvoiceAmount As String
    '        Dim sFilename As String
    '        Dim sResultString As String
    '        Dim iResponse As Short
    '        Dim cmdDelete As ADODB.Command
    '        Dim lTotalRecordsAffected As Integer
    '        Dim lRecordsAffected As Integer
    '        Dim bRenameButton As Boolean

    '        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

    '        Me.lstResult.Items.Clear()

    '        ConnectToArchive()

    '        bRenameButton = False

    '        If bMarkedPaymentsExists Then
    '            'At least 1 file is selected. Remove the selcted files
    '            iResponse = MsgBox("�nsker du � slette valgte filer?" & vbCrLf & vbCrLf, MsgBoxStyle.YesNo + MsgBoxStyle.Question + MsgBoxStyle.DefaultButton1, "SLETT FILER")
    '            If iResponse = MsgBoxResult.Yes Then
    '                'Continue

    '                cmdDelete = CreateObject ("ADODB.Command")

    '                'Traverse through the listbox to see which imported files are selected.

    '                lTotalRecordsAffected = 0
    '                For lCounter = 0 To Me.lstResult.Items.Count - 1
    '                    If Me.lstResult.GetItemChecked(lCounter) Then

    '                        lRecordsAffected = 0

    '                        Me.lstResult.SelectedIndex = lCounter

    '                        sMySQL = "DELETE FROM BankavstKoll.BabelFile WHERE Company_ID = " & sCompanyID & "AND BabelFile_ID = " & Val(xDelim((Me.lstResult.Text), Chr(9), 1))

    '                        cmdDelete.CommandText = sMySQL
    '                        cmdDelete.CommandType = ADODB.CommandTypeEnum.adCmdText
    '                        cmdDelete.let_ActiveConnection(conArchiveDB)
    '                        cmdDelete.Execute(lRecordsAffected)

    '                        lTotalRecordsAffected = lTotalRecordsAffected + 1

    '                    End If
    '                Next lCounter

    '                bRenameButton = True

    '                MsgBox(Trim(Str(lTotalRecordsAffected)) & " filer ble slettet." & vbCrLf & vbCrLf & "Nye saldi beregnes.", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Vellykket sletting!")

    '                cmdRecalculate_Click(cmdRecalculate, New System.EventArgs())

    '            Else
    '                'OK, nothing to do
    '            End If

    '        End If

    '        'The user has searched for files. Show the result
    '        Me.lstResult.Items.Clear()

    '        rsImportedFiles = CreateObject ("ADODB.Recordset")

    '        sMonth = Mid(Me.txtStartDate.Text, InStr(Me.txtStartDate.Text, ".") + 1, 2)
    '        If VB.Right(sMonth, 1) = "." Then
    '            ' only one digit for month
    '            sMonth = "0" & VB.Left(sMonth, 1)
    '        End If
    '        sIBDate = DateToString(System.DateTime.FromOADate(StringToDate(VB.Right(Me.txtStartDate.Text, 4) & sMonth & VB.Left(Me.txtStartDate.Text, InStr(Me.txtStartDate.Text, ".") - 1)).ToOADate - 1))

    '        sMonth = Mid(Me.txtEndDate.Text, InStr(Me.txtEndDate.Text, ".") + 1, 2)
    '        If VB.Right(sMonth, 1) = "." Then
    '            ' only one digit for month
    '            sMonth = "0" & VB.Left(sMonth, 1)
    '        End If
    '        sUBDate = VB.Right(Me.txtEndDate.Text, 4) & sMonth & VB.Left(Me.txtEndDate.Text, InStr(Me.txtEndDate.Text, ".") - 1)

    '        If sIBDate > sUBDate Then
    '            'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '            'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '            Me.Cursor = System.Windows.Forms.Cursors.Default
    '            MsgBox("Startdato ved kontrollen kan ikke v�re senere enn angitt sluttdato.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Feil i dato!")
    '            Exit Sub
    '        End If

    '        sMySQL = "SELECT B.BabelFile_ID AS BBID, B.Importformat AS FormatIn, B.DATE_Production AS PDate, B.MON_InvoiceAmount AS BAmount, B.FileNameIn AS Filename FROM BankavstKoll.BabelFile B WHERE B.DATE_Production >= '" & sIBDate & "' AND B.DATE_Production <= '" & sUBDate & "' AND B.Company_ID = " & sCompanyID & " ORDER BY B.BabelFile_ID DESC"

    '        rsImportedFiles.Open(sMySQL, conArchiveDB, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

    '        If rsImportedFiles.RecordCount > 0 Then
    '            ShowHideControls(True, False, False)
    '        Else
    '            MsgBox("Ingen filer er importert i det angitte tidsrommet.", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Ingen poster!")
    '        End If

    '        For lCounter = 1 To rsImportedFiles.RecordCount
    '            sResultString = ""
    '            sBabelFileID = PadLeft(Str(rsImportedFiles.Fields("BBID").Value), 5, "0")
    '            If Not IsDBNull(rsImportedFiles.Fields("FormatIn").Value) Then
    '                sImportFormat = VB.Left(GetEnumFileType(rsImportedFiles.Fields("FormatIn")), 6)
    '                If sImportFormat = "K-LINK" Then
    '                    sImportFormat = sImportFormat & "    "
    '                End If
    '            Else
    '                sImportFormat = "Ukjent"
    '            End If
    '            If Not IsDBNull(rsImportedFiles.Fields("PDate").Value) Then
    '                sDateProduction = rsImportedFiles.Fields("PDate").Value
    '            Else
    '                sDateProduction = "Ukjent  "
    '            End If
    '            If Not IsDBNull(rsImportedFiles.Fields("BAmount").Value) Then
    '                sMON_InvoiceAmount = PadLeft(ConvertFromAmountToString(rsImportedFiles.Fields("BAmount").Value, ".", ","), 13, " ")
    '            Else
    '                sMON_InvoiceAmount = Space(13)
    '            End If
    '            If Not IsDBNull(rsImportedFiles.Fields("FileName").Value) Then
    '                sFilename = "..." & VB.Right(rsImportedFiles.Fields("FileName").Value, 25)
    '            Else
    '                sFilename = "No filename found."
    '            End If

    '            sResultString = sBabelFileID & Chr(9)
    '            sResultString = sResultString & sImportFormat & Chr(9)
    '            sResultString = sResultString & sDateProduction & Chr(9)
    '            sResultString = sResultString & sMON_InvoiceAmount & Chr(9)
    '            sResultString = sResultString & sFilename

    '            Me.lstResult.Items.Add(sResultString)

    '            rsImportedFiles.MoveNext()

    '        Next lCounter

    '        If bRenameButton Then
    '            Me.chkRemoveFiles.Text = "&Vis filer"
    '        End If

    '        'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '        'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '        Me.Cursor = System.Windows.Forms.Cursors.Default

    '    End Sub

    '    Private Sub cmdClose_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdClose.Click

    '        Me.Close()

    '    End Sub

    '    Private Sub cmdBalances_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdBalances.Click

    '        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

    '        CalculateTheMovement()

    '        'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '        'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '        Me.Cursor = System.Windows.Forms.Cursors.Default

    '    End Sub

    '    Private Sub cmdRecalculate_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRecalculate.Click
    '        Dim rsRecalculate As ADODB.Recordset
    '        Dim sStartDate As String
    '        Dim oArchive As vbArchive.Archive
    '        Dim sMySQL As String
    '        Dim aArchiveArray() As String
    '        Dim sErrorString As String

    '        On Error GoTo ERR_Recalculate

    '        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

    '        ConnectToArchive()

    '        'Retrive the date that is set in the initial balance
    '        rsRecalculate = CreateObject ("ADODB.Recordset")

    '        sMySQL = "SELECT Date FROM BankavstKoll.IBSaldo WHERE Company_ID = " & sCompanyID

    '        rsRecalculate.Open(sMySQL, conArchiveDB, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

    '        If rsRecalculate.RecordCount = 1 Then
    '            If Not IsDBNull(rsRecalculate.Fields("Date").Value) Then
    '                sStartDate = DateToString(System.DateTime.FromOADate(StringToDate(Trim(rsRecalculate.Fields("Date").Value)).ToOADate + 1))
    '            Else
    '                MsgBox("BabelBank finner ingen dato angitt for inng�ende saldo. Dato er lagret som NULL.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Feil i saldo!")
    '            End If
    '        Else
    '            MsgBox("BabelBank finner ingen inng�ende saldo.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Feil i saldo!")
    '        End If

    '        rsRecalculate.Close()

    '        'UPGRADE_NOTE: Object rsRecalculate may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        rsRecalculate = Nothing

    '        aArchiveArray = GetArchiveInfo(Val(sCompanyID))

    '        oArchive = New vbArchive.Archive
    '        oArchive.InitiateAndConnect(aArchiveArray(0), Val(aArchiveArray(1)))
    '        oArchive.TableOwner = "BankAvstKoll" & "."
    '        oArchive.Special = "NORDEA LIV" 'Should not be necassary, because this is only used to calculate the balances

    '        If oArchive.NordeaLivUpdateBalance(sStartDate, sCompanyID, sErrorString, True) Then
    '            MsgBox("Vellykket oppdatering av saldo.", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Vellykket kj�ring!")
    '        Else
    '            MsgBox("Feil i oppdatering av saldo." & vbCrLf & vbCrLf & "Feil: " & vbCrLf & sErrorString, MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Mislykket kj�ring!")
    '        End If

    '        'UPGRADE_NOTE: Object oArchive may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        oArchive = Nothing

    '        On Error GoTo 0
    '        'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '        'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '        Me.Cursor = System.Windows.Forms.Cursors.Default

    '        Exit Sub

    'ERR_Recalculate:

    '        'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '        'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '        Me.Cursor = System.Windows.Forms.Cursors.Default

    '        MsgBox("Feil i oppdatering av saldo." & Err.Description, MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "Mislykket kj�ring!")

    '        Err.Clear()

    '    End Sub

    '    Private Sub frmNordeaLiv_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

    '        FormvbStyle(Me, "dollar.jpg", 500)
    '        FormLRSCaptions(Me)

    '        ShowHideControls(False, False, False)

    '        'Me.lblResultIBStartDate.Caption = ""
    '        'Me.lblResultMovement.Caption = ""
    '        'Me.lblResultUBEndDate.Caption = ""
    '        'Me.lblResultIBStartDate.Visible = False
    '        'Me.lblResultIBStartDate.Enabled = False
    '        'Me.lblResultMovement.Visible = False
    '        'Me.lblResultMovement.Enabled = False
    '        'Me.lblResultUBEndDate.Enabled = False
    '        'Me.lblResultUBEndDate.Visible = False
    '        'Me.lblUBEndDate.Visible = False
    '        'Me.lblUBEndDate.Enabled = False
    '        'Me.lblMovement.Visible = False
    '        'Me.lblMovement.Enabled = False
    '        'Me.lblIBStartDate.Visible = False
    '        'Me.lblIBStartDate.Enabled = False
    '        'Me.lstResult.Visible = False
    '        'Me.lstResult.Enabled = False
    '        Me.txtStartDate.Text = PadLeft(Trim(Str(VB.Day(Now))), 2, "0") & "." & PadLeft(Trim(Str(Month(Now))), 2, "0") & "." & Trim(Str(Year(Now)))
    '        Me.txtEndDate.Text = PadLeft(Trim(Str(VB.Day(Now))), 2, "0") & "." & PadLeft(Trim(Str(Month(Now))), 2, "0") & "." & Trim(Str(Year(Now)))
    '        Me.txtStartDate.SelectionStart = 0
    '        Me.txtStartDate.SelectionLength = Len(Trim(Me.txtStartDate.Text))
    '        Me.lblExplaination.Text = "Angi �nsket dato intervall, og start �nsket funksjon." & vbCrLf & "Rekalkul�r saldo: Beregner nye saldi med utangspunkt i IB som er angitt i databasen." & vbCrLf & "Sjekk utbet.: Sjekker om importerte utbetalinger er i sekvens." & vbCrLf & "Vis filer: Viser filer i angitt tidsrom." & vbCrLf & "Saldo: Viser IB, UB og bevegelse i angitt tidsrom."


    '        bConnectionOpen = False
    '        bMarkedPaymentsExists = False


    '    End Sub

    '    Private Sub CalculateTheMovement()
    '        Dim sMySQL As String
    '        Dim rsBalances As ADODB.Recordset
    '        Dim sMonth As String
    '        Dim nIB As Double
    '        Dim sIBDate As String
    '        Dim nUB As Double
    '        Dim sUBDate As String

    '        ConnectToArchive()

    '        rsBalances = CreateObject ("ADODB.Recordset")

    '        sMonth = Mid(Me.txtStartDate.Text, InStr(Me.txtStartDate.Text, ".") + 1, 2)
    '        If VB.Right(sMonth, 1) = "." Then
    '            ' only one digit for month
    '            sMonth = "0" & VB.Left(sMonth, 1)
    '        End If
    '        sIBDate = DateToString(System.DateTime.FromOADate(StringToDate(VB.Right(Me.txtStartDate.Text, 4) & sMonth & VB.Left(Me.txtStartDate.Text, InStr(Me.txtStartDate.Text, ".") - 1)).ToOADate - 1))

    '        sMonth = Mid(Me.txtEndDate.Text, InStr(Me.txtEndDate.Text, ".") + 1, 2)
    '        If VB.Right(sMonth, 1) = "." Then
    '            ' only one digit for month
    '            sMonth = "0" & VB.Left(sMonth, 1)
    '        End If
    '        sUBDate = VB.Right(Me.txtEndDate.Text, 4) & sMonth & VB.Left(Me.txtEndDate.Text, InStr(Me.txtEndDate.Text, ".") - 1)

    '        If sIBDate > sUBDate Then
    '            'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '            'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '            Me.Cursor = System.Windows.Forms.Cursors.Default
    '            MsgBox("Dato for inng�ende saldo kan ikke v�re senere enn angitt sluttdato.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Feil i dato!")
    '            Exit Sub
    '        End If

    '        sMySQL = "SELECT UB FROM BankavstKoll.Saldo WHERE Date = '" & sIBDate & "' AND Company_ID = " & sCompanyID

    '        rsBalances.Open(sMySQL, conArchiveDB, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

    '        If rsBalances.RecordCount = 1 Then
    '            If Not IsDBNull(rsBalances.Fields("UB").Value) Then
    '                nIB = rsBalances.Fields("UB").Value
    '            Else
    '                'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '                'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '                Me.Cursor = System.Windows.Forms.Cursors.Default
    '                MsgBox("BabelBank finner ingen IB for angitt startdato. UB er lagret som NULL.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Feil i saldo!")
    '                Me.txtStartDate.Focus()
    '                Me.txtStartDate.SelectionStart = 0
    '                Me.txtStartDate.SelectionLength = Len(Trim(Me.txtStartDate.Text))
    '                Exit Sub
    '            End If
    '        Else
    '            'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '            'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '            Me.Cursor = System.Windows.Forms.Cursors.Default
    '            MsgBox("BabelBank finner ingen IB for angitt startdato.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Feil i saldo!")
    '            Me.txtStartDate.Focus()
    '            Me.txtStartDate.SelectionStart = 0
    '            Me.txtStartDate.SelectionLength = Len(Trim(Me.txtStartDate.Text))
    '            Exit Sub
    '        End If

    '        rsBalances.Close()

    '        sMySQL = "SELECT UB FROM BankavstKoll.Saldo WHERE Date = '" & sUBDate & "' AND Company_ID = " & sCompanyID

    '        rsBalances.Open(sMySQL, conArchiveDB, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)

    '        If rsBalances.RecordCount = 1 Then
    '            If Not IsDBNull(rsBalances.Fields("UB").Value) Then
    '                nUB = rsBalances.Fields("UB").Value
    '            Else
    '                'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '                'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '                Me.Cursor = System.Windows.Forms.Cursors.Default
    '                MsgBox("BabelBank finner ingen UB for angitt sluttdato. UB er lagret som NULL.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Feil i saldo!")
    '                Me.txtEndDate.Focus()
    '                Me.txtEndDate.SelectionStart = 0
    '                Me.txtEndDate.SelectionLength = Len(Trim(Me.txtStartDate.Text))
    '                Exit Sub
    '            End If
    '        Else
    '            'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3B44E51-B5F1-4FD7-AA29-CAD31B71F487"'
    '            'UPGRADE_ISSUE: Form property frmNordeaLiv.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
    '            Me.Cursor = System.Windows.Forms.Cursors.Default
    '            MsgBox("BabelBank finner ingen UB for angitt sluttdato.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Feil i saldo!")
    '            Me.txtEndDate.Focus()
    '            Me.txtEndDate.SelectionStart = 0
    '            Me.txtEndDate.SelectionLength = Len(Trim(Me.txtStartDate.Text))
    '            Exit Sub
    '        End If

    '        rsBalances.Close()

    '        'UPGRADE_NOTE: Object rsBalances may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '        rsBalances = Nothing

    '        ShowHideControls(False, True, False)

    '        Me.lblIBStartDate.Text = Replace(Me.lblIBStartDate.Text, "Startdate", Me.txtStartDate.Text, , , CompareMethod.Text)
    '        Me.lblUBEndDate.Text = Replace(Me.lblUBEndDate.Text, "Enddate", Me.txtEndDate.Text, , , CompareMethod.Text)
    '        Me.lblResultIBStartDate.Text = ConvertFromAmountToString(nIB, ".", ",")
    '        Me.lblResultMovement.Text = ConvertFromAmountToString(nIB - nUB, ".", ",")
    '        Me.lblResultUBEndDate.Text = ConvertFromAmountToString(nUB, ".", ",")

    '    End Sub
    '    Friend Function SetCompanyID(ByRef iCompanyID As Object) As Boolean

    '        'UPGRADE_WARNING: Couldn't resolve default property of object iCompanyID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '        sCompanyID = Trim(Str(iCompanyID))

    '    End Function

    '    Private Sub frmNordeaLiv_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    '        Dim Cancel As Boolean = eventArgs.Cancel
    '        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason

    '        If Not conArchiveDB Is Nothing Then
    '            If conArchiveDB.State = ADODB.ObjectStateEnum.adStateOpen Then
    '                conArchiveDB.Close()
    '            End If
    '            'UPGRADE_NOTE: Object conArchiveDB may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            conArchiveDB = Nothing
    '        End If

    '        eventArgs.Cancel = Cancel
    '    End Sub

    '    'UPGRADE_WARNING: Event lstResult.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    '    Private Sub lstResult_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstResult.SelectedIndexChanged
    '        Dim lCounter As Integer

    '        bMarkedPaymentsExists = False
    '        If Me.lstResult.GetItemChecked(Me.lstResult.SelectedIndex) Then
    '            bMarkedPaymentsExists = True
    '        Else
    '            For lCounter = 0 To Me.lstResult.Items.Count - 1
    '                If Me.lstResult.GetItemChecked(lCounter) Then
    '                    bMarkedPaymentsExists = True
    '                    Exit For
    '                End If
    '            Next lCounter
    '        End If

    '        If bMarkedPaymentsExists Then
    '            Me.chkRemoveFiles.Text = "&Fjern filer"
    '        Else
    '            Me.chkRemoveFiles.Text = "&Vis filer"
    '        End If

    '    End Sub

    '    Private Sub txtEndDate_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEndDate.Leave

    '        If Not ValidateDate((Me.txtEndDate.Text), "dd.mm.yyyy", True) Then
    '            Me.txtEndDate.Focus()
    '            Me.txtEndDate.SelectionStart = 0
    '            Me.txtEndDate.SelectionLength = Len(Trim(Me.txtStartDate.Text))
    '        Else
    '            If Not ValidateDate((Me.txtStartDate.Text), "dd.mm.yyyy", True) Then
    '                Me.txtStartDate.Focus()
    '                Me.txtStartDate.SelectionStart = 0
    '                Me.txtStartDate.SelectionLength = Len(Trim(Me.txtStartDate.Text))
    '                '    Else
    '                '        CalculateTheMovement
    '            End If
    '        End If

    '    End Sub
    '    Private Sub txtStartDate_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtStartDate.Leave

    '        If Not ValidateDate((Me.txtStartDate.Text), "dd.mm.yyyy", True) Then
    '            Me.txtStartDate.Focus()
    '            Me.txtStartDate.SelectionStart = 0
    '            Me.txtStartDate.SelectionLength = Len(Trim(Me.txtStartDate.Text))
    '        End If

    '    End Sub
    '    Private Sub ConnectToArchive()
    '        Dim aArchiveArray() As String


    '        If Not bConnectionOpen Then
    '            aArchiveArray = GetArchiveInfo(Val(sCompanyID))

    '            'Connect to Archive-database
    '            conArchiveDB = CreateObject ("ADODB.Connection")
    '            conArchiveDB.Open(aArchiveArray(0))
    '            bConnectionOpen = True
    '        End If

    '    End Sub

    '    Private Sub ShowHideControls(ByRef bShowListResult As Boolean, ByRef bShowBalances As Boolean, ByRef bShowLabelResult As Boolean)

    '        If bShowListResult = True Then
    '            Me.lstResult.Width = VB6.TwipsToPixelsX(7020)
    '            Me.lstResult.Height = VB6.TwipsToPixelsY(2985)
    '            Me.lstResult.Left = VB6.TwipsToPixelsX(120)
    '            Me.lstResult.Top = VB6.TwipsToPixelsY(2880)
    '        End If
    '        If bShowLabelResult Then
    '            Me.txtResult.Width = VB6.TwipsToPixelsX(7020)
    '            Me.txtResult.Height = VB6.TwipsToPixelsY(2985)
    '            Me.txtResult.Left = VB6.TwipsToPixelsX(120)
    '            Me.txtResult.Top = VB6.TwipsToPixelsY(2880)
    '        End If


    '        Me.lstResult.Visible = bShowListResult
    '        Me.lstResult.Enabled = bShowListResult
    '        Me.lblIBStartDate.Visible = bShowBalances
    '        Me.lblIBStartDate.Enabled = bShowBalances
    '        Me.lblMovement.Visible = bShowBalances
    '        Me.lblMovement.Enabled = bShowBalances
    '        Me.lblUBEndDate.Visible = bShowBalances
    '        Me.lblUBEndDate.Enabled = bShowBalances
    '        Me.lblResultIBStartDate.Visible = bShowBalances
    '        Me.lblResultIBStartDate.Enabled = bShowBalances
    '        Me.lblResultMovement.Visible = bShowBalances
    '        Me.lblResultMovement.Enabled = bShowBalances
    '        Me.lblResultUBEndDate.Visible = bShowBalances
    '        Me.lblResultUBEndDate.Enabled = bShowBalances
    '        Me.txtResult.Visible = bShowLabelResult
    '        Me.txtResult.Enabled = bShowLabelResult

    '    End Sub
End Class
