Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic

Friend Class frmChangeFilePaths
	Inherits System.Windows.Forms.Form
    Dim oMyBBDal As vbBabel.DAL = Nothing
    Dim sMySQL, sCompanyID As String
	Dim sCompany_ID As String
	
	Friend Function SetCompanyID(ByRef s As String) As Boolean
		
		sCompanyID = s
		
	End Function
	
	
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		
		Me.Hide()
		
	End Sub
	
	Private Sub cmdChangeFromAndTo_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdChangeFromAndTo.Click
		Dim sTemp As String
		
		sTemp = Me.txtFilepathFrom.Text
		Me.txtFilepathFrom.Text = Me.txtFilepathTo.Text
		Me.txtFilepathTo.Text = sTemp
		
	End Sub
	
	Private Sub cmdRun_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRun.Click
        ' XNET 17.02.2011 - moved all Dims to new Public Sub ChangeAllFoldersInBabelBank
        Dim iResponse As Integer

        iResponse = MsgBox(LRS(60273), vbYesNo + vbQuestion + vbDefaultButton2, LRS(60272))
        'Do You want to change all filepaths in BabelBank?

        If Not iResponse = vbYes Then
            'Do as you are told. Nothing!
        Else
            ' XNET 17.02.2011 - moved all text below to new Public Sub ChangeAllFoldersInBabelBank
            ChangeAllFoldersInBabelBank(Me.txtFilepathFrom.Text, Me.txtFilepathTo.Text)

        End If

	End Sub
	
	Private Sub cmdSave_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSave.Click

        Try

            If oMyBBDal Is Nothing Then
                oMyBBDal = New vbBabel.DAL
                oMyBBDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyBBDal.ConnectToDB() Then
                    Throw New System.Exception(oMyBBDal.ErrorMessage)
                End If
            End If

            sMySQL = "Update Company SET OriginalPath = '" & Trim(Me.txtFilepathFrom.Text) & "', NewPath = '" & Trim(Me.txtFilepathTo.Text)
            sMySQL = sMySQL & "' WHERE Company_ID = " & Trim(sCompanyID)

            oMyBBDal.SQL = sMySQL

            If oMyBBDal.ExecuteNonQuery Then

                If oMyBBDal.RecordsAffected = 1 Then
                    'OK
                Else
                    MsgBox(LRS(15020), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
                    'MsgBox "An unexpected error occured. No records was saved."
                End If

            Else

                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyBBDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyBBDal Is Nothing Then
                oMyBBDal.Close()
                oMyBBDal = Nothing
            End If

            Throw New Exception("Function: Save_Click" & vbCrLf & ex.Message)

        End Try

    End Sub

	Private Sub frmChangeFilePaths_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		sCompany_ID = "1"
		
		FormLRSCaptions(Me)
		FormvbStyle(Me, "dollar.jpg", 1000)
		
		'Me.lblExplaination.Caption = "Dette er et valg for � endre alle filstier i BabelBank." & vbCrLf & _
		'"Angi opprinnelig katalog og hva denne skal endres til, f�r du klikker p� kj�r." & vbCrLf & _
		'"Hvis man f.eks. skal endre driver p� alle filstier fra C: til G:, s� angir man C:\ i feltet 'Orginal filsti' og G:\ i feltet 'Ny filsti'."
		
		'You may use this screen to change all the filepaths in BabelBank.  & vbCrLf &
		'Enter the original path/part of a path, and the new path/part of a path. Then click on run.  & vbCrLf &
		'If You for instance want to change the drive on all paths from C:\ to G:\, enter C:\ in the field 'Original path' and G:\ in the field 'New path'.
		
		
	End Sub
	
	Private Sub frmChangeFilePaths_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		
        If Not oMyBBDal Is Nothing Then
            oMyBBDal.Close()
            oMyBBDal = Nothing

            eventArgs.Cancel = Cancel
        End If
	End Sub
End Class
