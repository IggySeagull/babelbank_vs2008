Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic

Friend Class frmStart
    Inherits System.Windows.Forms.Form
    Dim aFilesArray(,) As String
    Dim bFilesPicked As Boolean 'has the user picked a file yet?
    Dim oProfile As vbBabel.Profile 'Object 'vbbabel.Profile    ' Object 'vbbabel.Profile
    Dim oFilesetup As vbBabel.FileSetup 'Object
    Dim oClients As vbBabel.Clients 'Object
    Dim aFilenameData() As String 'Array to store information about selected Shortname
    Dim oBabelFiles As vbBabel.BabelFiles 'vbbabel.BabelFiles 'As Object
    Dim oBabel As vbBabel.BabelFile 'As vbbabel.BabelFile
    Dim oExportBabelFiles As vbBabel.BabelFiles

    Dim bCut As Boolean
    Dim iSelectedProfile As Short ' Which profile is selected in listbox ?
    Dim sSelectedShortname As String
    Dim bCalledFromCommandline As Boolean
    Dim bLockBabelBank As Boolean
    Dim bDisableBabelBank As Boolean '�dded 14.12.2020 for SG Fsnans
    Dim mnuRightClick As New ContextMenuStrip()
    Dim sProfileName As String = ""
    Private listitem As _MyListBoxItem

    WithEvents mnulstSendProfiles_SupportMail As New ToolStripMenuItem()

    Public Function CalledFromCommandline() As Boolean
        ' If called with /Manuell from commandline
        bCalledFromCommandline = True
        LoadProfile()
        frmMATCH_Manual.CalledFromCommandline()
        ManualMatching_Click(ManualMatching, New System.EventArgs())
    End Function

    Public Sub ArchiveSetup_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ArchiveSetup.Click
        Dim aArray() As String

        'aArray = GetArchiveInfo((oProfile.Company_ID))

        'frmArchiveSetup.txtConnstring.Text = aArray(0)

        '-1 = No DatabaseType choosen
        ' 1 = Microsoft Access
        ' 2 = Oracle ver. ??
        ' 3 = SQL Server
        ' 4 = Oracle for Odin
        'If changed, change also SaveArchiveInfo in BB_Utils.
        'frmArchiveSetup.cmbDatabaseType.Items.Add(("Microsoft Access"))
        'frmArchiveSetup.cmbDatabaseType.Items.Add(("Oracle ver. ??"))
        'frmArchiveSetup.cmbDatabaseType.Items.Add(("SQL Server"))
        'frmArchiveSetup.cmbDatabaseType.Items.Add(("Oracle for Odin"))
        'frmArchiveSetup.cmbDatabaseType.SelectedIndex = Val(aArray(1))
        'If aArray(2) = "True" Then
        '    frmArchiveSetup.chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.Checked
        'Else
        '    frmArchiveSetup.chkIncludeOCR.CheckState = System.Windows.Forms.CheckState.UnChecked
        'End If
        'frmArchiveSetup.SetActivateArchiving(CBool(aArray(3)))
        'If CBool(aArray(3)) = True Then
        '    frmArchiveSetup.chkActivate.CheckState = System.Windows.Forms.CheckState.Checked
        'Else
        '    frmArchiveSetup.chkActivate.CheckState = System.Windows.Forms.CheckState.UnChecked
        '    frmArchiveSetup.txtConnstring.Enabled = False
        '    frmArchiveSetup.cmbDatabaseType.Enabled = False
        '    frmArchiveSetup.chkIncludeOCR.Enabled = False
        'End If
        frmArchiveSetup.SetCompany_ID((oProfile.Company_ID))

        VB6.ShowForm(frmArchiveSetup, 1, Me)

        oProfile.UseArchive = CBool(frmArchiveSetup.ActivateArchiving)

        frmArchiveSetup.Close()
    End Sub

    Public Sub CompressDB_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CompressDB.Click
        Dim oFs As Scripting.FileSystemObject
        Dim BBdbJetEngine As JRO.JetEngine
        Dim sSrcDatabasePath As String
        Dim sDest1DatabasePath, sDest2DatabasePath As String
        Dim lOldSize As Integer
        Dim iResponse As Short

        'To be sure:
        On Error GoTo ERR_CompressDB_Click

        If BB_UseSQLServer() Then

            MsgBox(LRS(60431), MsgBoxStyle.Information, LRS(50101))

        Else

            iResponse = MsgBox(LRS(60224), MsgBoxStyle.Question + MsgBoxStyle.YesNo, LRS(50101))
            '"Make sure all users are logged out of BabelBank.
            '"Do You want to continue?
            '
            If iResponse = MsgBoxResult.Yes Then
                'Continue
            Else
                Exit Sub
            End If

            'Check if someone else is using BB

            'Find the source- and destinationpath
            sSrcDatabasePath = BB_DatabasePath()

            ' added 08.06.2018
            If BB_UseAccess2016() Then
                ' ACCESS2016
                ' ----------
                sDest1DatabasePath = VB.Left(sSrcDatabasePath, Len(sSrcDatabasePath) - 4) & "_repairing.accdb"
                sDest2DatabasePath = VB.Left(sSrcDatabasePath, Len(sSrcDatabasePath) - 4) & "_repaired.accdb"

                oFs = New Scripting.FileSystemObject

                'Delete old files used to compress DB if they exist
                If oFs.FileExists(sDest1DatabasePath) Then
                    Kill(sDest1DatabasePath)
                End If
                If oFs.FileExists(sDest2DatabasePath) Then
                    Kill(sDest2DatabasePath)
                End If
                If oFs.FileExists(VB.Left(sSrcDatabasePath, Len(sSrcDatabasePath) - 6) & ".laccdb") Or oFs.FileExists(VB.Left(sSrcDatabasePath, Len(sSrcDatabasePath) - 6) & ".mdb") Then
                    MsgBox(LRS(60220), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60221))
                    'There are still other users using BabelBank - All users must be logged out of BabelBank, before the database may be compressed.
                Else
                    'First, make a xCopy of the DB
                    If MakeACopyOfBBDB(Nothing, False, True) Then

                        If oFs.FileExists(VB.Left(sSrcDatabasePath, Len(sSrcDatabasePath) - 7) & ".laccdb") Then
                            MsgBox(LRS(60220), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60221))
                            Exit Sub
                        End If
                        'OK, Compress and repair the DB

                        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                        'Rename Profile.accdb to Profile_repairing.accdb
                        oFs.MoveFile(sSrcDatabasePath, sDest1DatabasePath)

                        lOldSize = FileLen(sDest1DatabasePath)

                        'Compress the database
                        BBdbJetEngine = New JRO.JetEngine
                        'On Error Resume Next
                        BBdbJetEngine.CompactDatabase("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & sDest1DatabasePath & ";Jet OLEDB:Engine Type=5", "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & sDest2DatabasePath & ";Jet OLEDB:Engine Type=5")
                        BBdbJetEngine = Nothing

                        'First, rename Profile_repaired.mdb to Profile.Mdb
                        oFs.MoveFile(sDest2DatabasePath, sSrcDatabasePath)
                        Kill(sDest1DatabasePath)

                        Me.Cursor = System.Windows.Forms.Cursors.Default

                        MsgBox(LRS(60225, Trim(Str(lOldSize)), Trim(Str(FileLen(sSrcDatabasePath)))), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, LRS(50101))

                    Else
                        MsgBox(LRS(60222, Err.Description), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60221))
                        'Error during backup of BabelBank's database
                    End If
                End If

            Else
                ' ACCESS2003
                ' ----------
                sDest1DatabasePath = VB.Left(sSrcDatabasePath, Len(sSrcDatabasePath) - 4) & "_repairing.mdb"
                sDest2DatabasePath = VB.Left(sSrcDatabasePath, Len(sSrcDatabasePath) - 4) & "_repaired.mdb"

                oFs = New Scripting.FileSystemObject

                'Delete old files used to compress DB if they exist
                If oFs.FileExists(sDest1DatabasePath) Then
                    Kill(sDest1DatabasePath)
                End If
                If oFs.FileExists(sDest2DatabasePath) Then
                    Kill(sDest2DatabasePath)
                End If

                If oFs.FileExists(VB.Left(sSrcDatabasePath, Len(sSrcDatabasePath) - 4) & ".ldb") Then
                    MsgBox(LRS(60220), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60221))
                    'There are still other users using BabelBank - All users must be logged out of BabelBank, before the database may be compressed.
                Else
                    'First, make a xCopy of the DB
                    If MakeACopyOfBBDB(Nothing, False, True) Then

                        If oFs.FileExists(VB.Left(sSrcDatabasePath, Len(sSrcDatabasePath) - 4) & ".ldb") Then
                            MsgBox(LRS(60220), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60221))
                            Exit Sub
                        End If
                        'OK, Compress and repair the DB

                        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                        'Rename Profile.Mdb to Profile_repairing.mdb
                        oFs.MoveFile(sSrcDatabasePath, sDest1DatabasePath)

                        lOldSize = FileLen(sDest1DatabasePath)

                        'Compress the database
                        BBdbJetEngine = New JRO.JetEngine
                        'On Error Resume Next
                        BBdbJetEngine.CompactDatabase("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sDest1DatabasePath, "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sDest2DatabasePath)
                        ' TODO ACCESS2016 JPS 27.03.2018 - Ikke tilpasset SQL Server og Access 2016
                        'On Error GoTo 0

                        BBdbJetEngine = Nothing

                        'First, rename Profile_repaired.mdb to Profile.Mdb
                        oFs.MoveFile(sDest2DatabasePath, sSrcDatabasePath)
                        Kill(sDest1DatabasePath)

                        Me.Cursor = System.Windows.Forms.Cursors.Default

                        MsgBox(LRS(60225, Trim(Str(lOldSize)), Trim(Str(FileLen(sSrcDatabasePath)))), MsgBoxStyle.OkOnly + MsgBoxStyle.Information, LRS(50101))

                    Else
                        MsgBox(LRS(60222, Err.Description), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, LRS(60221))

                        'Error during backup of BabelBank's database

                    End If
                End If

            End If   'If bThisIsAccess2016 Then

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If
        End If


        On Error GoTo 0
        Exit Sub

ERR_CompressDB_Click:
        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        Me.Cursor = System.Windows.Forms.Cursors.Default

        Err.Raise(Err.Number, "CompressDB_Click", Err.Description)

    End Sub

    Public Sub Eurosoft_ClientImport_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Eurosoft_ClientImport.Click
        Dim sFormatIn As String
        Dim sFormatOut As String
        Dim sObservationAccount As String
        Dim sPattern As String
        Dim sReminderNo As String 'XNET - 23.02.2012
        Dim nAllowedDifference As Double
        Dim sErrorString As String

        frmClientImport.ShowDialog()

        If Not frmClientImport.bCancelled Then
            sFormatIn = frmClientImport.txtFormatIn.Text
            sFormatOut = frmClientImport.txtFormatOut.Text
            sObservationAccount = frmClientImport.txtObservationAccount.Text
            sPattern = frmClientImport.txtPattern.Text
            'KOTOKO - Neste linje
            sReminderNo = frmClientImport.txtReminderNo.Text 'XNET - 23.02.2012
            If Not EmptyString((frmClientImport.txtAllowedDifference.Text)) Then
                If vbIsNumeric((frmClientImport.txtAllowedDifference.Text), "0123456789") Then
                    nAllowedDifference = System.Math.Abs(Val(frmClientImport.txtAllowedDifference.Text) * 100)
                Else
                    nAllowedDifference = 0
                End If
            Else
                nAllowedDifference = 0
            End If

            frmClientImport = Nothing

            sErrorString = ""
            'XNET - 23.02.2012 - Changed next line
            If ImportClientInfoFromEurosoft(sFormatIn, sFormatOut, sObservationAccount, sPattern, nAllowedDifference, sReminderNo, sErrorString) Then
                'If ImportClientInfoFromEurosoft(sFormatIn, sFormatOut, sObservationAccount, sPattern, nAllowedDifference, sErrorString) Then
                MsgBox("Oppdatering vellykket!", MsgBoxStyle.OkOnly, "Oppdatering av klientinformasjon")
            Else
                MsgBox("En uventet feil oppstod ved oppdatering av klientinformasjon." & vbCrLf & "Feil: " & vbCrLf & sErrorString, MsgBoxStyle.OkOnly, "Oppdatering av klientinformasjon")
            End If

        Else
            frmClientImport = Nothing
        End If

    End Sub

    Public Sub mnuImportFiles_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuImportFiles.Click
        Dim aSelectedFilenames() As String
        Dim oTempFilesetup As vbBabel.FileSetup
        Dim sTempSpecial As String
        Dim bExitFor As Boolean

        sTempSpecial = ""

        bExitFor = False
        For Each oFilesetup In oProfile.FileSetups
            For Each oTempFilesetup In oProfile.FileSetups
                If oFilesetup.FileSetupOut = oTempFilesetup.Filesetup_ID Then
                    'find matching profiles
                    'If oTempFilesetup.FormatOutID1 > 0 Then
                    sTempSpecial = oTempFilesetup.TreatSpecial
                    bExitFor = True
                    Exit For
                    'End If
                End If
            Next oTempFilesetup
            If bExitFor Then
                Exit For
            End If
        Next oFilesetup


        If oProfile.ManMatchFromERP And Not sTempSpecial = "NFR" Then
            MsgBox(LRS(16008), MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, LRS(16001))
            'Du kan ikke benytte dette valget n�r det foreg�r en validering mot ERP-systemet.
            ' Run profile
        Else
            For Each oFilesetup In oProfile.FileSetups
                For Each oTempFilesetup In oProfile.FileSetups
                    If oFilesetup.FileSetupOut = oTempFilesetup.Filesetup_ID Then
                        'find matching profiles
                        If oTempFilesetup.AutoMatch Then '.FormatOutID1 > 0 Then
                            If Array_IsEmpty(aSelectedFilenames) Then
                                ReDim aSelectedFilenames(0)
                            Else
                                ReDim Preserve aSelectedFilenames(UBound(aSelectedFilenames) + 1)
                            End If
                            aSelectedFilenames(UBound(aSelectedFilenames)) = oFilesetup.ShortName
                        End If
                    End If
                Next oTempFilesetup
            Next oFilesetup
            ImportExport(aSelectedFilenames, oProfile, UBound(aSelectedFilenames) + 1, True, True, , True, True, True, Me)
        End If

    End Sub
    Public Sub mnuDEBUG_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuDEBUG.Click
        ' Present DEBUG-info in a form
        VB6.ShowForm(frmDebug, 1, Me)
    End Sub

    Public Sub AutomaticMatching_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles AutomaticMatching.Click
        Dim bx As Boolean
        Dim oLocalBabelFile As vbBabel.BabelFile
        Dim oLocalBatch As vbBabel.Batch
        Dim oLocalPayment As vbBabel.Payment
        Dim oLocalClient As vbBabel.Client
        Dim oLocalAccount As vbBabel.Account
        Dim iOldFileSetupID As Short
        Dim bFirstFileSetupIDFound As Boolean
        Dim bProceed As Boolean
        Dim iAutoMatchStatus As Short
        Dim sErrorString As String
        Dim aLocked(,) As Object
        Dim sSpecial As String
        'XNET - 13.10.2010
        Dim aArchiveArray() As String
        Dim bExport As Boolean
        Dim oArchive As vbArchive.Archive
        'End XNET
        Dim bItemsRemoved As Boolean = False
        Dim sOldAccountNo As String = ""
        Dim iOldClientID As Integer
        Dim bAccountFound As Boolean
        Dim sCheckERPSQL As String
        Dim lPaymentCount As Long
        Dim lBatchCount As Long
        Dim lBabelCount As Long

        'First, test if any payments are locked
        aLocked = frmMATCH_Manual.FindPaymentsLocked(True)

        If Not Array_IsEmpty(aLocked) Then
            MsgBox(LRS(17512) & vbCrLf & vbCrLf & LRS(17513, Replace(LRS(50707), "&", ""), Replace(LRS(50700), "&", "")), MsgBoxStyle.Information + MsgBoxStyle.OKOnly, LRS(19505))
            'MsgBox "Can't process the requested action, because at least one payment is locked by another user." _
            '& vbCrLf & vbCrLf & "To unlock the payments choose 'Unlock Payments' on the 'Matching'-menu.", vbInformation + vbOKOnly, "CAN'T START EXPORT."
        Else
            'OK, no payments are locked
            oExportBabelFiles = New vbBabel.BabelFiles
            oExportBabelFiles.VB_Profile = oProfile
            iOldFileSetupID = -1
            bFirstFileSetupIDFound = False
            bProceed = True
            If oExportBabelFiles.MATCH_RetrieveData(True, 1, False, False) Then
                'If oExportBabelFiles.MATCH_ReindexCollection Then
                If oExportBabelFiles.Count > 0 Then
                    For Each oLocalBabelFile In oExportBabelFiles
                        If Not oLocalBabelFile.VB_FileSetupID = iOldFileSetupID Then
                            If iOldFileSetupID <> -1 Then
                                If bFirstFileSetupIDFound Then
                                    MsgBox(LRS(19506), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(19505))
                                    MsgBox("The payments to be matched originates from two different profiles. Contact Your dealer.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(17508))
                                    bProceed = False
                                    Exit For
                                Else
                                    iOldFileSetupID = oLocalBabelFile.VB_FileSetupID
                                    bFirstFileSetupIDFound = True
                                End If
                            Else
                                iOldFileSetupID = oLocalBabelFile.VB_FileSetupID
                                bFirstFileSetupIDFound = True
                            End If
                        End If
                    Next oLocalBabelFile
                Else
                    'There are no payments to match!
                    'FIXFIXFIX
                    MsgBox(LRS(19507), MsgBoxStyle.Information + MsgBoxStyle.OkOnly, LRS(19505))
                    bProceed = False
                End If
                If bProceed Then
                    oExportBabelFiles.VB_Profile = oProfile
                    bx = oExportBabelFiles.SetProfileInUndelyingCollections

                    bItemsRemoved = False

                    'Erase matching info on some payments
                    For Each oLocalBabelFile In oExportBabelFiles
                        'XNET - 13.10.2010
                        sSpecial = oLocalBabelFile.Special

                        For Each oLocalBatch In oLocalBabelFile.Batches

                            For Each oLocalPayment In oLocalBatch.Payments

                                If oLocalPayment.ExtraD1 <> "RTV" Then 'Case Lindorff


                                    '******** New code to check if payment is open in ERP
                                    '04.12.2012 - New code, moved from beneath

                                    If oProfile.ManMatchFromERP Then

                                        If oLocalPayment.I_Account <> sOldAccountNo Then
                                            bAccountFound = False
                                            For Each oFilesetup In oProfile.FileSetups
                                                For Each oLocalClient In oFilesetup.Clients
                                                    For Each oLocalAccount In oLocalClient.Accounts
                                                        If oLocalPayment.I_Account = Trim(oLocalAccount.Account) Then
                                                            bAccountFound = True
                                                            sOldAccountNo = oLocalPayment.I_Account
                                                            Exit For
                                                        End If
                                                    Next oLocalAccount
                                                    If bAccountFound Then Exit For
                                                Next oLocalClient
                                                If bAccountFound Then Exit For
                                            Next oFilesetup
                                        End If

                                        If oLocalClient.Client_ID <> iOldClientID Then
                                            iOldClientID = oLocalClient.Client_ID
                                            sCheckERPSQL = GetCheckAgainstERPSQL(oProfile.Company_ID, oLocalClient.DBProfile_ID)
                                        End If

                                        'Old code
                                        'If Not EmptyString(sCheckERPSQL) > 0 Then 'Assume that we shall check if the payment is open if the SQL to do this is stated
                                        '15.12.2017 - The code above is stranged. It entered the code below even if the sCheckERPSQL was empty
                                        If Not EmptyString(sCheckERPSQL) Then 'Assume that we shall check if the payment is open if the SQL to do this is stated
                                            If oLocalPayment.Unique_ERPID = "" Then
                                                If CheckIfPaymentIsOpenInERP(oLocalPayment, oLocalClient, sCheckERPSQL, True, True, "", oLocalPayment.MON_InvoiceAmount) Then
                                                    'OK
                                                Else
                                                    bItemsRemoved = True
                                                    oLocalPayment.ToSpecialReport = True
                                                End If
                                            Else
                                                If CheckIfPaymentIsOpenInERP(oLocalPayment, oLocalClient, sCheckERPSQL, True, True, oLocalPayment.Unique_ERPID, oLocalPayment.MON_InvoiceAmount) Then
                                                    'OK
                                                Else
                                                    bItemsRemoved = True
                                                    oLocalPayment.ToSpecialReport = True
                                                End If
                                            End If
                                        Else
                                            'Don't remove
                                        End If
                                    Else
                                        'Don't remove
                                    End If

                                    If oLocalPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.PartlyMatched Then
                                        oLocalPayment.MATCH_Undo()
                                    ElseIf oLocalPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.ProposedMatched Then
                                        oLocalPayment.MATCH_Undo()
                                    End If
                                    If oLocalPayment.MATCH_Matched <> vbBabel.BabelFiles.MatchStatus.Matched Then
                                        oLocalPayment.StatusSplittedFreetext = 0
                                    End If
                                End If
                            Next oLocalPayment
                        Next oLocalBatch
                    Next oLocalBabelFile

                    '******** New code to check if payment is open in ERP
                    If bItemsRemoved Then
                        ' then delete marked payments, clean up empty batches and recalculate
                        For lBabelCount = oExportBabelFiles.Count To 1 Step -1
                            bItemsRemoved = False
                            oLocalBabelFile = oExportBabelFiles.Item(lBabelCount)
                            '17.03.2009 - added next IF because of Korforbundet which populates oBabelfiles later on
                            If Not oLocalBabelFile.Batches Is Nothing Then
                                For lBatchCount = oLocalBabelFile.Batches.Count To 1 Step -1
                                    oLocalBatch = oLocalBabelFile.Batches.Item(lBatchCount)
                                    ' Delete marked payments
                                    For lPaymentCount = oLocalBatch.Payments.Count To 1 Step -1
                                        If oLocalBatch.Payments.Item(lPaymentCount).ToSpecialReport = True Then
                                            oLocalBatch.Payments.Remove(lPaymentCount)
                                            bItemsRemoved = True
                                        End If
                                    Next lPaymentCount
                                    ' Must check if any payments left in this batch
                                    If oLocalBatch.Payments.Count = 0 Then
                                        oLocalBabelFile.Batches.Remove((lBatchCount))
                                        'bBatchRemoved = True
                                    End If
                                Next lBatchCount
                                ' Must check if any batches left in this BabelFile
                                If oLocalBabelFile.Batches.Count = 0 Then
                                    oExportBabelFiles.Remove((lBabelCount))
                                    'bBabelFileRemoved = True
                                Else
                                    If bItemsRemoved Then
                                        'We should also add a method to recalculate indexes.
                                        '  Set index to equal vtIndex in BabelfileS.
                                        oLocalBabelFile.CalculateTotals(False, False, False)
                                    End If
                                End If
                            End If 'If Not oBabelFile.Batches Is Nothing Then
                        Next lBabelCount
                    End If
                    '******** End new code to check if payment is open in ERP

                    'The following parameters are sent to automatching
                    'oLocalBabelfiles which contains all payments stored in the BBDB
                    'iOldFileSetupID, which is the FileSetupID used, if more than 1 an error is raised.
                    'iFilenameInNo is set to 1. It should never be used from this function
                    '   because it's just used when a new accountnumber is found
                    'TreatSpecial is set to "". It's not used in the automatching-function
                    'bMtchOCR is set to true, because if OCR-payments are stored in BBDB they shall obviously be matched
                    'iProgAutoMatch is set to 100, to get a correct progressbar
                    'CompanyID is set, and the ErrorString is passed to store error messages.
                    ProgressStart(False)
                    ProgressText((LRS(50708)))
                    ProgressPos((0))
                    ShowProgressBar2()
                    ShowProgressBar3()
                    ProgressText2((LRS(60066)))
                    ProgressText3((LRS(60067)))
                    Pause((1))

                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                    'XNET - 13.10.2010 - For Odin, Must run automatch even though the open items aren't stored in the ERP-system
                    'Last parameter is set to False
                    If sSpecial = "ODIN" Then
                        oExportBabelFiles.MATCH_MatchOCR = True
                        iAutoMatchStatus = AutoMatching(oExportBabelFiles, iOldFileSetupID, 1, True, 100, oProfile.Company_ID, sErrorString, "", False)
                    ElseIf sSpecial = "VINGCARD" Or sSpecial = "VINGCARD_UK" Then 'XNET 24.10.2012 - Added this ElseIf
                        'As with Odin
                        iAutoMatchStatus = AutoMatching(oExportBabelFiles, iOldFileSetupID, 1, True, 100, oProfile.Company_ID, sErrorString, "", False)
                    Else
                        'Old code
                        iAutoMatchStatus = AutoMatching(oExportBabelFiles, iOldFileSetupID, 1, True, 100, oProfile.Company_ID, sErrorString, "", True)
                    End If

                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    ProgressStop()
                    If iAutoMatchStatus = -1 Then
                        MsgBox(sErrorString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(19501))
                    Else

                        'XNET - 13.10.2010
                        If oProfile.UseArchive Then
                            aArchiveArray = GetArchiveInfo(oProfile.Company_ID)
                            oArchive = New vbArchive.Archive ' 23.05.2017 CreateObject ("vbArchive.Archive")
                            oArchive.InitiateAndConnect(aArchiveArray(0), Val(aArchiveArray(1)))
                            bExport = oArchive.CopyDataToArchiveDB(oExportBabelFiles, False, False, False)
                        End If
                        'End XNET

                        If sSpecial = "CONECTO_OP" Then
                            oExportBabelFiles.MATCH_MatchOCR = True
                        End If

                        'Store the matched payments
                        bx = oExportBabelFiles.MATCH_SaveData(True, True, False)

                        'XNET - 13.10.2010
                        If oProfile.UseArchive Then
                            If bx Then
                                oArchive.CommitCopiedDataToArchiveDB()
                            Else
                                oArchive.RollbackCopiedDataToArchiveDB()
                            End If
                            oArchive = Nothing
                        End If
                        'End XNET

                        '                For Each oLocalBabelFile In oExportBabelFiles
                        '                    For Each oLocalBatch In oLocalBabelFile.Batches
                        '                        For Each oLocalPayment In oLocalBatch.Payments
                        '                            If oLocalPayment.MATCH_Matched > MatchStatus.NotMatched Then
                        '                                oExportBabelFiles.MATCH_aveData
                        '                            End If
                        '                        Next oLocalPayment
                        '                    Next oLocalBatch
                        '                Next oLocalBabelFile
                    End If
                End If
                'Else
                '    MsgBox oExportBabelFiles.MATCH_ErrorText, vbCritical + vbOKOnly, LRS(17508)
                'End If
            Else
                MsgBox(oExportBabelFiles.MATCH_ErrorText, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(19501))
            End If
        End If 'If Not Array_IsEmpty(aLocked) Then

    End Sub

    Public Sub ExportPayments_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ExportPayments.Click
        Dim bImportExportOK As Boolean
        Dim aSelectedFilenames() As String
        Dim oLocalBabelFile As vbBabel.BabelFile
        Dim iOldFileSetupID As Short
        Dim bFirstFileSetupIDFound As Boolean
        Dim bProceed As Boolean
        Dim oFilesetup As vbBabel.FileSetup
        Dim aLocked(,) As Object
        Dim bExportAllPayments As Boolean
        Dim mResponse As Microsoft.VisualBasic.MsgBoxResult

        'First, test if any payments are locked
        aLocked = frmMATCH_Manual.FindPaymentsLocked(True)

        If Not Array_IsEmpty(aLocked) Then
            MsgBox(LRS(17512) & vbCrLf & vbCrLf & LRS(17513, Replace(LRS(50707), "&", ""), Replace(LRS(50700), "&", "")), MsgBoxStyle.Information + MsgBoxStyle.OKOnly, LRS(17514))
            'MsgBox "Can't process the requested action, because at least one payment is locked by another user." _
            '& vbCrLf & vbCrLf & "To unlock the payments choose 'Unlock Payments' on the 'Matching'-menu.", vbInformation + vbOKOnly, "CAN'T START EXPORT."
        Else
            'OK, no payments are locked

            oExportBabelFiles = New vbBabel.BabelFiles
            oExportBabelFiles.VB_Profile = oProfile
            iOldFileSetupID = -1
            bFirstFileSetupIDFound = False
            bProceed = True

            '**********************************************************************************
            'AUTOKJ�RING SG 25.03.2008
            'Added new code for SG Finans, test against setup

            '20.02.2017 - Added new code for SG Finans
            'Sometimes they will export only matched payments, sometimes all payments.
            '04.03.2021 - Added "NORDEAFINANCE_APTIC"
            bExportAllPayments = False
            If oProfile.CompanyName.Trim.ToUpper = "SG FINANS AS" Or oProfile.CompanyName.Trim.ToUpper = "SG FINANS EQ" Or oProfile.CompanyName.Trim.ToUpper = "NORDEA FINANCE APTIC" Then
                mResponse = MsgBox("�nsker du � eksportere kun avstemte betalinger?", vbYesNo + vbQuestion, "Eksport")
                If mResponse = vbYes Then
                    'OK, the code will export only matched
                Else
                    mResponse = MsgBox("Alle innbetalinger vil bli eksportert!" & vbCrLf & vbCrLf & "�nsker du � fortsette med eksport?", vbYesNo + vbQuestion, "Eksport")
                    If mResponse = vbYes Then
                        bExportAllPayments = True
                    Else
                        Exit Sub
                    End If
                End If
            End If

            ' 20.12.2021 - EuroFinans will have two runs, because there are two different file formats for Matched and Unmatched
            '               The run for matched payments, below, and further down one for unmatched payments
            If oProfile.CompanyName.Trim.ToUpper = "EUROFINANS" Then
                bExportAllPayments = False
            End If

            If Not ExportOnlyMatchedPayments((oProfile.Company_ID)) Or bExportAllPayments Then
                If oExportBabelFiles.MATCH_RetrieveData(False, 1, False, False) Then 'TODO: Hardcoded CompanyNo
                    bProceed = True
                Else
                    MsgBox(oExportBabelFiles.MATCH_ErrorText, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(17508))
                    bProceed = False
                End If
            Else
                If oExportBabelFiles.MATCH_RetrieveData(False, 1, True, False) Then 'TODO: Hardcoded CompanyNo
                    bProceed = True
                Else
                    MsgBox(oExportBabelFiles.MATCH_ErrorText, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(17508))
                    bProceed = False
                End If
            End If


            If bProceed Then
                'End new code
                '***********************************************************************************

                'Old code (next line and the else)
                'If oExportBabelFiles.MATCH_RetrieveData(False, 1, True, False) Then

                '07.07.2010 - NBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBN!!!!!!!!!!!!!!!!!!!!!!!!
                'The next if is changed to 1=1, the reindexing is removed
                'This may be catastrofic for the customer using this choice.
                'The problem arises with the use of oBabelFile.Index in combination with oBabelFile.Item
                'In combination with the archivedatabase we need the correct .Index to update the Archive DB correct
                'If we have 1 BabelFile with Index 20, and want to refer to it using .Item this will happen
                'oBabelFile.Index = 20
                'You would want to refer to oBabelFile.Item(1)
                'However if you store the index in a variable (that's what we normally do)
                'iSomething = oBabelFile.Index (hence iSomething = 20)
                'and then refer to it through .Item;
                'oBabelFile.Item(iSomething)
                'Then You ask for oBabelFile.Item(20), Which does not exist and will raise an error!
                If 1 = 1 Then ' oExportBabelFiles.MATCH_ReindexCollection Then
                    If oExportBabelFiles.Count > 0 Then
                        For Each oLocalBabelFile In oExportBabelFiles
                            If Not oLocalBabelFile.VB_FileSetupID = iOldFileSetupID Then
                                If iOldFileSetupID <> -1 Then
                                    If bFirstFileSetupIDFound Then
                                        MsgBox(LRS(17515), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(17514))
                                        'MsgBox "The payments to be exported originates from two different profiles. Contact Your dealer.", vbCritical + vbOKOnly, LRS(17508)
                                        bProceed = False
                                        Exit For
                                    Else
                                        iOldFileSetupID = oLocalBabelFile.VB_FileSetupID
                                        bFirstFileSetupIDFound = True
                                    End If
                                Else
                                    iOldFileSetupID = oLocalBabelFile.VB_FileSetupID
                                    bFirstFileSetupIDFound = True
                                End If
                            End If
                        Next oLocalBabelFile
                    Else
                        If oProfile.CompanyName.Trim.ToUpper = "EUROFINANS" Then
                            ' No warning for EuroFinans, because there may be siutations where we have no matched payments to export
                            bProceed = False
                        Else
                            'There are no matched payments to export!
                            MsgBox(LRS(17511), MsgBoxStyle.Information + MsgBoxStyle.OkOnly, LRS(17508))
                            bProceed = False
                        End If
                    End If
                    If bProceed Then
                        For Each oFilesetup In oProfile.FileSetups
                            If oFilesetup.FileSetup_ID = iOldFileSetupID Then
                                ReDim Preserve aSelectedFilenames(0)
                                aSelectedFilenames(0) = oFilesetup.ShortName
                            End If
                        Next oFilesetup
                        'AUTOKJ�RING SG 25.03.2008
                        'To be able to distinguish between the customers who want to export all payments, and
                        ' the one who wants to export only the matched payments.
                        'bExportOnlyMacthedPayments =
                        bImportExportOK = ImportExport(aSelectedFilenames, oProfile, 1, False, True, , , , , Me, , True, True)
                    End If
                Else
                    MsgBox(oExportBabelFiles.MATCH_ErrorText, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(17508))
                End If
                'Old code see note 25.03.2008
                'Else
                '    MsgBox oExportBabelFiles.MATCH_ErrorText, vbCritical + vbOKOnly, LRS(17508)
            End If


            ' Second run for EuroFinans, Unmatched payments
            If oProfile.CompanyName.Trim.ToUpper = "EUROFINANS" Then
                bProceed = True
                'If Not ExportOnlyMatchedPayments((oProfile.Company_ID)) Or bExportAllPayments Then
                '    If oExportBabelFiles.MATCH_RetrieveData(False, 1, False, False) Then 'TODO: Hardcoded CompanyNo
                '        bProceed = True
                '    Else
                '        MsgBox(oExportBabelFiles.MATCH_ErrorText, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(17508))
                '        bProceed = False
                '    End If
                'Else
                If oExportBabelFiles.MATCH_RetrieveData(False, 1, False, False) Then 'All payments, not only matched
                    bProceed = True
                Else
                    MsgBox(oExportBabelFiles.MATCH_ErrorText, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(17508))
                    bProceed = False
                End If
                'End If


                If bProceed Then
                    'End new code
                    '***********************************************************************************

                    If oExportBabelFiles.Count > 0 Then
                        For Each oLocalBabelFile In oExportBabelFiles
                            If Not oLocalBabelFile.VB_FileSetupID = iOldFileSetupID Then
                                If iOldFileSetupID <> -1 Then
                                    If bFirstFileSetupIDFound Then
                                        MsgBox(LRS(17515), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(17514))
                                        'MsgBox "The payments to be exported originates from two different profiles. Contact Your dealer.", vbCritical + vbOKOnly, LRS(17508)
                                        bProceed = False
                                        Exit For
                                    Else
                                        iOldFileSetupID = oLocalBabelFile.VB_FileSetupID
                                        bFirstFileSetupIDFound = True
                                    End If
                                Else
                                    iOldFileSetupID = oLocalBabelFile.VB_FileSetupID
                                    bFirstFileSetupIDFound = True
                                End If
                            End If
                            ' to signal to prepare export that we want second file exported (unmatched file):
                            oLocalBabelFile.VB_FilenameInNo = 2
                        Next oLocalBabelFile
                    Else
                        'There are no matched payments to export!
                        MsgBox(LRS(17511), MsgBoxStyle.Information + MsgBoxStyle.OkOnly, LRS(17508))
                        bProceed = False
                    End If
                    If bProceed Then
                        For Each oFilesetup In oProfile.FileSetups
                            If oFilesetup.FileSetup_ID = iOldFileSetupID Then
                                ReDim Preserve aSelectedFilenames(0)
                                aSelectedFilenames(0) = oFilesetup.ShortName
                            End If
                        Next oFilesetup
                        bImportExportOK = ImportExport(aSelectedFilenames, oProfile, 1, False, True, , , , , Me, , True, False)
                    End If
                End If
            End If

        End If 'If Not Array_IsEmpty(aLocked) Then

    End Sub

    Public Function GetExportObject() As vbBabel.BabelFiles

        GetExportObject = oExportBabelFiles

    End Function

    Private Sub frmStart_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        ' Exit Babelbank

        SetEnvironment("BabelBase", "")

        If Not oProfile Is Nothing Then
            oProfile = Nothing
        End If
        If Not oClients Is Nothing Then
            oClients = Nothing
        End If
        If Not oFilesetup Is Nothing Then
            oFilesetup = Nothing
        End If
        If Not oBabel Is Nothing Then
            oBabel = Nothing
        End If
        If Not oBabelFiles Is Nothing Then
            oBabelFiles = Nothing
        End If
        If Not oExportBabelFiles Is Nothing Then
            oExportBabelFiles = Nothing
        End If

        ' added 09.05.2019
        'If eventArgs.CloseReason = CloseReason.UserClosing Then
        eventArgs.Cancel = False
        ' b�r dette med? Me.Dispose(True) 
        Me.Dispose()  ' added 10.05.2019
        'End If

        Application.Exit()  ' added 10.05.2019 - bedre enn End ??
        'End ' Terminate program. Cleans up memory

        'eventArgs.Cancel = Cancel


    End Sub
    Public Sub BackupFiles_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles BackupFiles.Click
        ' Show files from backup-katalog
        ' user may copy file back to original name/katalog
        Dim oBackupView As vbBabel.BackupView
        If oProfile.Backup Then
            oBackupView = New vbBabel.BackupView
            oBackupView.BackupPath = oProfile.BackupPath
            oBackupView.Profile = oProfile


            oBackupView.Show()
            If oBackupView.ReturnValue = -1 Then
                ' No relevant files found in backuppath
                MsgBox(LRS(60071), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
            End If

        Else
            ' Backup inactive
            MsgBox(LRS(60069), MsgBoxStyle.OKOnly + MsgBoxStyle.Information) ' Backup not active. Activate in "Setup/Company"
        End If

    End Sub


    Public Sub CleanUpDatabase_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CleanUpDatabase.Click

        ResetDBValues((oProfile.Company_ID))

    End Sub

    'Private Sub cmdManMatch_Click()
    '
    ''frmManMatch.Show 1
    'frmMATCH_Manual.SetBabelFiles oBabelFiles
    'frmMATCH_Manual.Show vbModeless, frmStart
    '
    '
    'End Sub
    Private Sub cmdNewFIProfile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdNewFIProfile.Click
        '------------------------------------------------------------
        ' Special for TBI WEB FI-Profile
        ' This is a returnprofile
        ' Button visible only for TBI-WEB Customers
        '------------------------------------------------------------
        Dim oBabelSetup As BabelSetup.vbBabelSetup
        Dim bSetup As Boolean

        oBabelSetup = New BabelSetup.vbBabelSetup

        oBabelSetup.Profile = oProfile
        oBabelSetup.Filesetup_ID = -1

        bSetup = oBabelSetup.Setup()

        If bSetup = True Then
            oProfile = oBabelSetup.Profile

            oProfile.Save(1)
            ' Reread profile, to make sure all values are set correctly
            oProfile.Load(1)

            FillListboxes()

        Else
            ' Run new load to make sure no errors are carried on
            oProfile.Load(1)
        End If

    End Sub

    Private Sub cmdNewProfile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdNewProfile.Click
        '------------------------------------------------------------
        ' Call Setup with oProfile, and no selected profile
        ' Returns oProfile, filled with collections for new profile
        '------------------------------------------------------------
        Dim oBabelSetup As Object
        Dim bSetup As Boolean
        Dim oLicense As vbBabel.License
        Dim sServices As String

        oBabelSetup = New BabelSetup.vbBabelSetup

        'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Profile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        oBabelSetup.Profile = oProfile
        'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Filesetup_ID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        oBabelSetup.Filesetup_ID = -1

        ' Check if we will be using normal setup, or DnB TBIW-setup:
        oLicense = New vbBabel.License
        ' 16.02.05 - Versjon 1.02.82
        ' Endring pga problemer med at lisensfil slettes
        ' Hent ut fra License, og lukk med en gang:
        'If InStr(oLicense.LicServicesAvailable, "T") Then
        ' Ny
        'UPGRADE_WARNING: Couldn't resolve default property of object oLicense.LicServicesAvailable. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sServices = oLicense.LicServicesAvailable
        'UPGRADE_NOTE: Object oLicense may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oLicense = Nothing
        If InStr(sServices, "T") > 0 Or InStr(sServices, "DNBX") > 0 Then
            ' Special setup for DnB TBIWeb
            'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Start_DnB_TBIW_SendProfile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bSetup = oBabelSetup.Start_DnB_TBIW_SendProfile()
        Else
            ' Normal setup
            'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Setup. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bSetup = oBabelSetup.Setup()
        End If
        'Set oLicense = Nothing

        If bSetup = True Then
            'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Profile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oProfile = oBabelSetup.Profile

            oProfile.Save(1)
            ' Reread profile, to make sure all values are set correctly
            oProfile.Load(1)

            FillListboxes()

        Else
            ' Run new load to make sure no errors are carried on
            oProfile.Load(1)
        End If

    End Sub
    Private Sub NewProfile()
        '------------------------------------------------------------
        ' Called from menu, CreateNewProfile
        ' Call Setup with oProfile, and no selected profile
        ' Returns oProfile, filled with collections for new profile
        '------------------------------------------------------------
        Dim oBabelSetup As Object
        Dim bSetup As Boolean

        Dim oLicense As vbBabel.License
        Dim sServices As String
        oBabelSetup = New BabelSetup.vbBabelSetup

        'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Profile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        oBabelSetup.Profile = oProfile
        'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Filesetup_ID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        oBabelSetup.Filesetup_ID = -1


        ' Check if we will be using normal setup, or DnB TBIW-setup:
        oLicense = New vbBabel.License
        ' 16.02.05 - Versjon 1.02.82
        ' Endring pga problemer med at lisensfil slettes
        ' Hent ut fra License, og lukk med en gang:
        'If InStr(oLicense.LicServicesAvailable, "T") Then
        ' Ny
        'UPGRADE_WARNING: Couldn't resolve default property of object oLicense.LicServicesAvailable. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sServices = oLicense.LicServicesAvailable
        'UPGRADE_NOTE: Object oLicense may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oLicense = Nothing
        If InStr(sServices, "T") > 0 Or InStr(sServices, "DNBX") > 0 Then
            ' Special setup for DnB TBIWeb
            'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Start_DnB_TBIW_SendProfile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bSetup = oBabelSetup.Start_DnB_TBIW_SendProfile()
        Else
            ' Normal setup
            'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Setup. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bSetup = oBabelSetup.Setup()
        End If


        If bSetup = True Then
            'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Profile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oProfile = oBabelSetup.Profile

            oProfile.Save(1)
            ' Reread profile, to make sure all values are set correctly
            oProfile.Load(1)

            FillListboxes()

        Else
            ' Run new load to make sure no errors are carried on
            oProfile.Load(1)
        End If

    End Sub

    Private Sub cmdDeleteProfile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDeleteProfile.Click
        '--------------------------------------------------------------
        ' Mark selected profile as deleted. Run Save to update database
        '--------------------------------------------------------------

        Dim Style As MsgBoxStyle
        Style = MsgBoxStyle.YesNo + MsgBoxStyle.Critical + MsgBoxStyle.DefaultButton2
        ' A profile must have been selected:
        If iSelectedProfile = 0 Then
            MsgBox(LRS(60042), MsgBoxStyle.OKOnly + MsgBoxStyle.Information) '("Du m� f�rst velge en profil!")
        Else
            ' Ask if we realy want to delete:
            If MsgBox(LRS(60043), Style, LRS(60044) & " " & sSelectedShortname) = MsgBoxResult.Yes Then
                oProfile.FileSetups(iSelectedProfile).Status = vbBabel.Profile.CollectionStatus.Deleted
                oProfile.Status = vbBabel.Profile.CollectionStatus.Deleted 'ChangeUnder
                'FIX: Must delete connected profiles as well
                If oProfile.FileSetups(iSelectedProfile).FileSetupOut <> -1 Then
                    ' if one, mark directly
                    oProfile.FileSetups((oProfile.FileSetups(iSelectedProfile).FileSetupOut)).Status = vbBabel.Profile.CollectionStatus.Deleted
                Else
                    ' FIX: if several, loop through
                End If
                oProfile.Save(1)
                'FIXED Temporarly by JanP 04.10.01, see also vbBabel
                oProfile.Load(1) 'load to make sure deleted profiles are gone
                FillListboxes()
                iSelectedProfile = 0
            End If

        End If

    End Sub
    Private Sub cmdChangeProfile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdChangeProfile.Click
        '------------------------------------------------------------
        ' Call Setup with oProfile, with one selected profile
        ' Returns oProfile, with changes made for selected profile
        '------------------------------------------------------------
        Dim oBabelSetup As BabelSetup.vbBabelSetup 'object
        Dim bSetup As Boolean
        Dim oLicense As vbBabel.License
        Dim oFilesetup As vbBabel.FileSetup
        Dim bSendProfile As Boolean
        Dim sServices As String


		' A profile must have been selected:
		If iSelectedProfile = 0 Then
            MsgBox(LRS(60042), MsgBoxStyle.OKOnly + MsgBoxStyle.Information) '("Du m� f�rst velge en profil!")
        Else
            oBabelSetup = New BabelSetup.vbBabelSetup

            oBabelSetup.Profile = oProfile
            oBabelSetup.Filesetup_ID = iSelectedProfile



            ' Check if we will be using normal setup, or DnB TBIW-setup:
            oLicense = New vbBabel.License
			' 16.02.05 - Versjon 1.02.82
			' Endring pga problemer med at lisensfil slettes
			' Hent ut fra License, og lukk med en gang:
			'If InStr(oLicense.LicServicesAvailable, "T") Then
			' Ny
			sServices = oLicense.LicServicesAvailable
			oLicense = Nothing
			If InStr(sServices, "T") > 0 Or InStr(sServices, "DNBX") > 0 Then
                ' Sendprofiles or Returnprofiles only:
                For Each oFilesetup In oProfile.FileSetups
                    If oFilesetup.FileSetup_ID = iSelectedProfile Then
                        ' If sendprofile, check if TBIW-Setup;
                        If oFilesetup.FromAccountingSystem Then
                            bSendProfile = True ' Normal, goes for all but FI-Browser profile
                        Else
                            bSendProfile = False ' FI-Browser profile
                        End If

                        If bSendProfile Then
                            ' Special setup for DnB TBIWeb
                            bSetup = oBabelSetup.Start_DnB_TBIW_SendProfile()
                        Else
                            ' Use ordinary Setup for FI-project
                            bSetup = oBabelSetup.Setup()
                        End If
                    End If
                Next oFilesetup

            Else
                ' Normal setup
                bSetup = oBabelSetup.Setup()
            End If
            'Set oLicense = Nothing

            If bSetup = True Then
                oProfile = oBabelSetup.Profile

                oProfile.Save(1)
                ' Reread profile, to make sure all values are set correctly
                oProfile.Load(1)

                FillListboxes()
            Else
                ' Run new load to make sure no errors are carried on
                oProfile.Load(1)
            End If
        End If

    End Sub
    Private Sub ChangeProfile()
        '------------------------------------------------------------
        ' Called from Menu
        ' Call Setup with oProfile, with one selected profile
        ' Returns oProfile, with changes made for selected profile
        '------------------------------------------------------------
        Dim oBabelSetup As Object
        Dim bSetup As Boolean

        ' A profile must have been selected:
        If iSelectedProfile = 0 Then
            MsgBox(LRS(60042), MsgBoxStyle.OKOnly + MsgBoxStyle.Information) '("Du m� f�rst velge en profil!")
        Else
            oBabelSetup = New BabelSetup.vbBabelSetup

            'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Profile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oBabelSetup.Profile = oProfile
            'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Filesetup_ID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oBabelSetup.Filesetup_ID = iSelectedProfile

            ' Normal setup
            'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Setup. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bSetup = oBabelSetup.Setup()

            If bSetup = True Then
                'UPGRADE_WARNING: Couldn't resolve default property of object oBabelSetup.Profile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oProfile = oBabelSetup.Profile

                oProfile.Save(1)
                ' Reread profile, to make sure all values are set correctly
                oProfile.Load(1)

                FillListboxes()
            Else
                ' Run new load to make sure no errors are carried on
                oProfile.Load(1)
            End If
        End If

    End Sub

    Private Sub cmdRun_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRun.Click
        ' Run profile
        Dim aSelectedFilenames() As String
        Dim bImportExportOK As Boolean

        If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20201019" Then
            KARTest(oProfile, 2) '' test oppkall mot Nets KAR-register
        End If

        If sSelectedShortname <> "" Then

            ' 23.01.2017
            ' disable Start-button, so the profile not runs twice if user clicks twice
            Me.cmdRun.Enabled = False

            ReDim Preserve aSelectedFilenames(0)
            aSelectedFilenames(0) = sSelectedShortname
            ' XNET 12.03.2013 added next if
            '************************************VISMA******************************************************
            If bVismaIntegrator Then
                'KOTOKO - Neste linje
                'bImportExportOK = Visma_ImportExport(aSelectedFilenames, Me)
            Else
                bImportExportOK = ImportExport(aSelectedFilenames, oProfile, 1, True, True, , , , , Me)
            End If
            'bImportExportOK = ImportExport(aSelectedFilenames, oProfile, 1, True, True, , , , , Me)
        Else
            MsgBox(LRS(60042), MsgBoxStyle.OkOnly + MsgBoxStyle.Information) '("Du m� f�rst velge en profil!")
        End If

    End Sub
    Private Sub cmdClients_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdClients.Click
        ' 11.08.06
        ' Changed to use new ClientSetup
        MatchingClients_Click(MatchingClients, New System.EventArgs())

        '------------------------------------------------------------
        ' Call Setup with oProfile, and no selected profile
        ' Returns oProfile, filled with collections after Clients might
        ' have been changed by user are changed
        '------------------------------------------------------------
        'Dim oClientSetup As Object
        'Dim bClientSetup As Boolean
        '
        'frmStart.MousePointer = vbHourglass
        '
        'Set oClientSetup = CreateObject ("Babelsetup.ClientSetup")
        '
        'Set oClientSetup.Profile = oProfile
        '
        'bClientSetup = oClientSetup.Start()
        '
        'If bClientSetup = True Then
        '    Set oProfile = oClientSetup.Profile
        '
        '    oProfile.Save (1)
        '    ' Reread profile, to make sure all values are set correctly
        '    oProfile.Load (1)
        '
        'Else
        '    ' Run new load to make sure no errors are carried on
        '    oProfile.Load (1)
        'End If
        '
        'frmStart.MousePointer = vbDefault
        '
    End Sub
    '-----------------------------------------------------------------
    'Sub cmdSave_Click()  'oBabelFiles As vbBabel.BabelFiles)
    '' Test lagring av collections
    'Dim oConnection As vbBabel.Database
    'Dim cnX As New ADODB.Connection
    'Dim rs As ADODB.Recordset
    '
    'Dim cat As New ADOX.Catalog
    '
    '
    'Dim bError As Boolean
    'Dim sErrDescription As String
    'Dim sMySQL As String, bx As Boolean
    '
    ''------------------------------
    '' Test:
    'Dim bCreateTables As Boolean
    'Dim bOverWrite As Boolean
    '
    'bCreateTables = False
    'bOverWrite = True
    ''--- end test ---
    'On Error GoTo ERRHAndler
    '
    '
    'bError = False
    'Set oConnection = New vbbabel.Database")   'New ADODB.Connection
    ''oconnection.Open sConnString '"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.path & "\Forint.mdb"  ';Data Source=" & sMyFile
    ''oConnection.Connect
    '
    ''bOverWrite = False
    '
    'If bOverWrite Then
    '    ' drop existing tables, create new!
    '    bCreateTables = True
    '
    '    cnX.ConnectionString = oConnection.ConnectToProfile(BB_DatabasePath)
    '    cnX.Open
    '
    '    Set rs = cnX.OpenSchema(adSchemaTables)
    '
    '    rs.Find "Table_Name = " & Chr(39) & "BabelFile" & Chr(39)
    '
    '    If rs.EOF Then
    '     ' Table Not Found
    '    Else
    '    ' Table Exists
    '        bx = oConnection.Connect
    '        sMySQL = "DROP TABLE Freetext"
    '        oConnection.SQLCommand (sMySQL)
    '        sMySQL = "DROP TABLE Invoice"
    '        oConnection.SQLCommand (sMySQL)
    '        sMySQL = "DROP TABLE Payment"
    '        oConnection.SQLCommand (sMySQL)
    '        sMySQL = "DROP TABLE Batch"
    '        oConnection.SQLCommand (sMySQL)
    '        sMySQL = "DROP TABLE BabelFile"
    '        oConnection.SQLCommand (sMySQL)
    '        Set oConnection = Nothing
    '        Set oConnection = New vbbabel.Database")   'New ADODB.Connection
    '    End If
    '    cnX.Close
    '    Set cnX = Nothing
    'End If
    '
    '
    'If bCreateTables Then
    '    bx = oConnection.Connect
    '
    '    '-------------------
    '    ' BABELFILE
    '    '-------------------
    '    sErrDescription = "Table BabelFile "
    '    'sMySQL = "CREATE TABLE Batch (BabelFile_ID LONG, Batch_ID LONG, PRIMARY KEY(Company_ID, BabelFile_ID, Batch_ID))"
    '    'sMySQL = "CREATE TABLE BabelFile (Company_ID LONG, BabelFile_ID LONG AUTOINCREMENT PRIMARY KEY )"
    '    sMySQL = "CREATE TABLE BabelFile (Company_ID LONG, BabelFile_ID LONG, PRIMARY KEY(Company_ID, BabelFile_ID))"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD FilenameIn TEXT(100) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD File_ID TEXT(10) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD Ident_Sender TEXT(40) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD Ident_Receiver TEXT(40) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD MON_TransferredAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD MON_InvoiceAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD AmountSetTransferred LOGICAL NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD AmountSetInvoice LOGICAL NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD No_Of_Transactions LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD No_Of_Records LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD DATE_Production Text(8) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD ImportFormat LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD ProfileInUse Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD RejectsExists Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD REPORT_Print BYTE NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD REPORT_Screen BYTE NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD FilenameInNo INTEGER NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD FileFromBank Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD Version Text(7) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD EDI_Format Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD EDI_MessageNo Text(10) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD EDIMapping Text(15) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD FileSetup_ID Integer NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD ERR_UseErrorObject Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD ERR_Number Long NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD ERR_Message Text(100) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD XIndex LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD XLanguage LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD ERPSystem Text(30) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD Bank LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD EDI_CreateCONTRLMessage Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD EDI_FilenameCONTRLMessage Text(100) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE BabelFile ADD StatusCode Text(5) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    'Add foreign key
    '    sMySQL = "ALTER TABLE BabelFile ADD CONSTRAINT FK_ColCompany FOREIGN KEY (Company_ID) REFERENCES Company(Company_ID)"
    '    oConnection.SQLCommand (sMySQL)
    '
    '    '-------------------
    '    ' BATCH
    '    '-------------------
    '    sErrDescription = "Table Batch "
    '    sMySQL = "CREATE TABLE Batch (Company_ID LONG, BabelFile_ID LONG, Batch_ID LONG, PRIMARY KEY(Company_ID, BabelFile_ID, Batch_ID))"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD XIndex LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD SequenceNoStart LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD SequenceNoEnd LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD StatusCode Text(5) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD Version Text(7) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD Operator Text(30) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD FormatType Text(30) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD I_EnterpriseNo Text(20) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD I_Branch Text(20) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD DATE_Production Text(8) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD REF_Own Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD REF_Bank Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD MON_TransferredAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD MON_InvoiceAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD AmountSetTransferred Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD AmountSetInvoice Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD No_Of_Transactions LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD No_Of_Records LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD ProfileInUse Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Batch ADD ImportFormat Integer NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    ' Trenger ikke denne? - oCargo As Cargo
    '
    '    'Add foreign key
    '    sMySQL = "ALTER TABLE Batch ADD CONSTRAINT FK_BabelFile FOREIGN KEY (Company_ID, BabelFile_ID) REFERENCES BabelFile(Company_ID, BabelFile_ID)"
    '    oConnection.SQLCommand (sMySQL)
    '
    '    '-------------------
    '    ' PAYMENT
    '    '-------------------
    '    sErrDescription = "Table Payment "
    '    sMySQL = "CREATE TABLE Payment (Company_ID LONG, BabelFile_ID LONG, Batch_ID LONG, Payment_ID LONG, PRIMARY KEY(Company_ID, BabelFile_ID, Batch_ID, Payment_ID))"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD XIndex LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD StatusCode Text(5) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD Cancel Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD DATE_Payment Text(8) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_TransferredAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD AmountSetInvoice Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD AmountSetTransferred Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_TransferCurrency Text(3) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD REF_Own Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD REF_Bank1 Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD REF_Bank2 Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD FormatType Text(30) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD E_Account Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD E_AccountPrefix Text(20) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD E_AccountSuffix Text(20) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD E_Name Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD E_Adr1 Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD E_Adr2 Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD E_Adr3 Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD E_Zip Text(8) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD E_City Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD E_CountryCode Text(2) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD DATE_Value Text(8) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD Priority Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD I_Client Text(20) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD Text_E_Statement Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD Text_I_Statement Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD Cheque BYTE NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ToOwnAccount Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD PayCode Text(3) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD PayType Text(1) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD I_Account Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD I_Name Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD I_Adr1 Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD I_Adr2 Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD I_Adr3 Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD I_Zip Text(8) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD I_City Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD I_CountryCode Text(2) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD Structured Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD VB_ClientNo Text(8) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ExtraD1 Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ExtraD2 Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ExtraD3 Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ExtraD4 Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ExtraD5 Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_LocalAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_LocalCurrency Text(3) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_AccountAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_AccountCurrency Text(3) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_InvoiceAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_InvoiceCurrency Text(3) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_EuroAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_AccountExchRate LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_LocalExchRate LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_EuroExchRate LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_ChargesAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_ChargesCurrency Text(3) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_ChargeMeDomestic Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MON_ChargeMeAbroad Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ERA_ExchRateAgreed LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ERA_DealMadeWith Text(15) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD FRW_ForwardContractRate LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD FRW_ForwardContractNo Text(15) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD NOTI_NotificationMessageToBank Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD NOTI_NotificationType Text(15) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD NOTI_NotificationParty INTEGER NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD NOTI_NotificationAttention Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD NOTI_NotificationIdent Text(15) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD BANK_SWIFTCode Text(15) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD BANK_BranchNo Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD BANK_Name Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD BANK_Adr1 Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD BANK_Adr2 Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD BANK_Adr3 Text(35) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD BANK_CountryCode Text(2) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD BANK_SWIFTCodeCorrBank Text(15) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ExtraI1 Text(5) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ExtraI2 Text(5) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ExtraI3 Text(5) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ProfileInUse Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD ImportFormat INTEGER NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD FilenameOut_ID INTEGER NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD InvoiceDescription Text(30) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD NoOfLeadingZeros INTEGER NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD InvoiceCounter LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD InvoiceCustomerNo Text(30) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD StatusSplittedFreetext INTEGER NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD Exported Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD MATCH_Matched Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Payment ADD Unique_PaymentID LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '
    '    'Add foreign key
    '    sMySQL = "ALTER TABLE Payment ADD CONSTRAINT FK_Batch FOREIGN KEY (Company_ID, BabelFile_ID, Batch_ID) REFERENCES Batch(Company_ID, BabelFile_ID, Batch_ID)"
    '    oConnection.SQLCommand (sMySQL)
    '
    '    ' Not needed?
    '    'aPossibleInvoiceNos() TEXT(  )
    '    'ImportLineBuffer Text()
    '
    '
    '
    '    '-------------------
    '    ' INVOICE
    '    '-------------------
    '    sErrDescription = "Table Invoice "
    '    sMySQL = "CREATE TABLE Invoice (Company_ID LONG, BabelFile_ID LONG, Batch_ID LONG, Payment_ID LONG, Invoice_ID LONG, PRIMARY KEY(Company_ID, BabelFile_ID, Batch_ID, Payment_ID, Invoice_ID))"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD XIndex LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD StatusCode Text(5) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD Cancel Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD MON_InvoiceAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD MON_TransferredAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD MON_AccountAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD MON_LocalAmount LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD AmountSetTransferred Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD AmountSetInvoice Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD REF_Own Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD REF_Bank Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD InvoiceNo Text(30) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD CustomerNo Text(30) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD Unique_Id Text(30) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD STATEBANK_Code Text(5) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD STATEBANK_Text Text(100) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD STATEBANK_DATE Text(8) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD Extra1 Text(50) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD MATCH_Original Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD MATCH_Final Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD MATCH_Matched Logical NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Invoice ADD CargoFTX Text(100) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    'NOT needed - ImportLineBuffer As String
    '    ' set relations!
    '    'Add foreign key
    '    sMySQL = "ALTER TABLE Invoice ADD CONSTRAINT FK_Payment FOREIGN KEY (Company_ID, BabelFile_ID, Batch_ID, Payment_ID) REFERENCES Payment(Company_ID, BabelFile_ID, Batch_ID, Payment_ID)"
    '    oConnection.SQLCommand (sMySQL)
    '
    '
    '    '-------------------
    '    ' FREETEXT
    '    '-------------------
    '    sErrDescription = "Table Freetext "
    '    sMySQL = "CREATE TABLE Freetext (Company_ID LONG, BabelFile_ID LONG, Batch_ID LONG, Payment_ID LONG, Invoice_ID LONG, Freetext_ID LONG, PRIMARY KEY(Company_ID, BabelFile_ID, Batch_ID, Payment_ID, Invoice_ID, Freetext_ID))"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Freetext ADD XIndex LONG NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Freetext ADD Qualifier Integer NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    sMySQL = "ALTER TABLE Freetext ADD XText Text(70) NULL"
    '    oConnection.SQLCommand (sMySQL)
    '    'Add foreign key
    '    sMySQL = "ALTER TABLE Freetext ADD CONSTRAINT FK_Invoice FOREIGN KEY (Company_ID, BabelFile_ID, Batch_ID, Payment_ID, Invoice_ID) REFERENCES Invoice(Company_ID, BabelFile_ID, Batch_ID, Payment_ID, Invoice_ID)"
    '    oConnection.SQLCommand (sMySQL)
    '
    '
    '    'Allow zero-length strings - The tables must be created before
    '    '  the cat-object is created.
    '    cat.ActiveConnection = _
    ''    "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BB_DatabasePath & ";"
    '
    '    '-------------------
    '    ' BABELFILE
    '    '-------------------
    '    cat.Tables("BabelFile").Columns("FilenameIn").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("BabelFile").Columns("File_ID").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("BabelFile").Columns("Ident_Sender").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("BabelFile").Columns("Ident_Receiver").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("BabelFile").Columns("DATE_Production").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("BabelFile").Columns("Version").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("BabelFile").Columns("EDI_MessageNo").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("BabelFile").Columns("EDIMapping").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("BabelFile").Columns("ERR_Message").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("BabelFile").Columns("ERPSystem").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("BabelFile").Columns("EDI_FilenameCONTRLMessage").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("BabelFile").Columns("StatusCode").Properties("Jet OLEDB:Allow Zero Length") = True
    '    '-------------------
    '    ' BATCH
    '    '-------------------
    '    cat.Tables("Batch").Columns("StatusCode").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Batch").Columns("Version").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Batch").Columns("Operator").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Batch").Columns("FormatType").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Batch").Columns("I_EnterpriseNo").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Batch").Columns("I_Branch").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Batch").Columns("DATE_Production").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Batch").Columns("REF_Own").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Batch").Columns("REF_Bank").Properties("Jet OLEDB:Allow Zero Length") = True
    '    '-------------------
    '    ' PAYMENT
    '    '-------------------
    '    cat.Tables("Payment").Columns("StatusCode").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("DATE_Payment").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("MON_TransferCurrency").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("REF_Own").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("REF_Bank1").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("REF_Bank2").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("FormatType").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("E_Account").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("E_AccountPrefix").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("E_AccountSuffix").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("E_Name").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("E_Adr1").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("E_Adr2").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("E_Adr3").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("E_Zip").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("E_City").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("E_CountryCode").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("DATE_Value").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("I_Client").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("Text_E_Statement").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("Text_I_Statement").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("PayCode").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("I_Account").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("I_Name").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("I_Adr1").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("I_Adr2").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("I_Adr3").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("I_Zip").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("I_City").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("I_CountryCode").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("VB_ClientNo").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("ExtraD1").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("ExtraD2").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("ExtraD3").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("ExtraD4").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("ExtraD5").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("MON_LocalCurrency").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("MON_AccountCurrency").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("MON_InvoiceCurrency").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("MON_ChargesCurrency").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("ERA_DealMadeWith").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("FRW_ForwardContractNo").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("NOTI_NotificationMessageToBank").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("NOTI_NotificationType").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("NOTI_NotificationAttention").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("NOTI_NotificationIdent").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("BANK_SWIFTCode").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("BANK_BranchNo").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("BANK_Name").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("BANK_Adr1").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("BANK_Adr2").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("BANK_Adr3").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("BANK_CountryCode").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("BANK_SWIFTCodeCorrBank").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("ExtraI1").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("ExtraI2").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("ExtraI3").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("InvoiceDescription").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Payment").Columns("InvoiceCustomerNo").Properties("Jet OLEDB:Allow Zero Length") = True
    '    '-------------------
    '    ' INVOICE
    '    '-------------------
    '    cat.Tables("Invoice").Columns("StatusCode").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Invoice").Columns("REF_Own").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Invoice").Columns("REF_Bank").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Invoice").Columns("InvoiceNo").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Invoice").Columns("CustomerNo").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Invoice").Columns("Unique_Id").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Invoice").Columns("STATEBANK_Code").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Invoice").Columns("STATEBANK_Text").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Invoice").Columns("STATEBANK_DATE").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Invoice").Columns("Extra1").Properties("Jet OLEDB:Allow Zero Length") = True
    '    cat.Tables("Invoice").Columns("CargoFTX").Properties("Jet OLEDB:Allow Zero Length") = True
    '    '-------------------
    '    ' FREETEXT
    '    '-------------------
    '    'Allow zero-length strings
    '    cat.Tables("Freetext").Columns("XText").Properties("Jet OLEDB:Allow Zero Length") = True
    '
    '
    '    '---------------------
    '    ' All tables created!
    '    '---------------------
    'End If 'bCreateTables
    '
    ''---------------------------------
    '' Import a file
    ''---------------------------------
    '
    'Set cnX = Nothing
    'Set oConnection = Nothing
    '
    'Dim oBabelFilesDB As vbBabel.BabelFiles
    'Dim oBabelDB As vbBabel.BabelFile
    'Dim iImport As Integer
    '
    'Set oBabelFilesDB = New vbbabel.babelfiles")
    'Set oBabelFilesDB.VB_Profile = oProfile
    '
    'Set oBabelDB = oBabelFilesDB.Add(1)
    'Set oBabelDB.VB_Profile = oProfile
    'oBabelDB.ImportFormat = FileType.CREMUL
    'oBabelDB.FilenameIn = "C:\DBSave\CREMUL.DAT"
    ''oBabelDB.REPORT_Print = aImportInformation(3, iArrayCounter)
    ''oBabelDB.REPORT_Screen = aImportInformation(4, iArrayCounter)
    ''oBabelDB.VB_FilenameInNo = aImportInformation(5, iArrayCounter)
    ''oBabelDB.Version = aImportInformation(7, iArrayCounter)
    ''oBabelDB.Bank = aImportInformation(9, iArrayCounter)
    '
    'iImport = oBabelDB.Import()
    '
    ''bx = WriteCollections(oBabelFilesDB, "C:/Slett/Inn.dat")
    '
    'bx = oBabelFilesDB.SaveData(False)
    '
    'Set oConnection = Nothing
    '
    'Exit Sub
    '
    'ERRHAndler:
    '
    'If Not oConnection Is Nothing Then
    '    Set oConnection = Nothing
    'End If
    '
    'sErrDescription = "Error saving collections!" & vbCrLf & sErrDescription
    'sErrDescription = sErrDescription & vbCrLf & vbCrLf & vbCrLf & Err.Description
    '
    'Err.Raise 55555, "BabelBank.exe", sErrDescription   'FIX: Define errno
    '
    '
    'End Sub

    'Private Sub cmdTestCorrect_Click()
    '' Test Correct.cls
    'Dim oCorrect As vbBabel.Correct
    'Dim oBabelFiles As BabelFiles
    'Dim lContinue As Boolean
    'Dim aErrors() As Variant
    '
    '' import a file first;
    'lContinue = True
    '' Which files to import
    'lContinue = PickAFile
    '
    'If lContinue Then
    '    Set oBabelFiles = ImportBabel(aFilesArray(), oProfile, False, False, Nothing)
    '
    '    ' call class correct
    '    Set oCorrect = New vbbabel.correct")
    '    Set oCorrect.BabelFiles = oBabelFiles
    '    oCorrect.FileName = "c:\slett\testfil.txt"
    '    ReDim aErrors(5, 3)
    '    aErrors(0, 0) = "11100"
    '    aErrors(1, 0) = "E_Zip"
    '    aErrors(2, 0) = "Meld inn fire sifre for postnr"
    '    aErrors(3, 0) = "0"
    '    aErrors(4, 0) = "1"
    '
    '    aErrors(0, 1) = "11100"
    '    aErrors(1, 1) = "I_Account"
    '    aErrors(2, 1) = "CDV feil i kontonummer"
    '    aErrors(3, 1) = "1"
    '    aErrors(4, 1) = "1"
    '
    '    aErrors(0, 2) = "11100"
    '    aErrors(1, 2) = "E_Account"
    '    aErrors(2, 2) = "CDV feil i kontonummer mottaker"
    '    aErrors(3, 2) = "1"
    '    aErrors(4, 2) = "1"
    '
    '    aErrors(0, 3) = "11100"
    '    aErrors(1, 3) = "BANK_CountryCode"
    '    aErrors(2, 3) = "Finner ikke landkode"
    '    aErrors(3, 3) = "1"
    '    aErrors(4, 3) = "0"
    '
    '    oCorrect.InfoArray = aErrors
    '    oCorrect.Show
    '
    'End If
    '
    '
    'End Sub

    Public Sub ManualMatching_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ManualMatching.Click
        Dim bx As Boolean
        'XNET - 02.11.2010 - New variable
        Dim bMatchOCR As Boolean

        'If bCalledFromOutside Then
        'LoadProfile
        'End If

        'The function raise the message on screen
        bx = BBdbTooLarge(True)

        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

        ' 06.09.2010 moved next line here
        bx = frmMATCH_Manual.SetUseArchive((oProfile.UseArchive))
        bx = frmMATCH_Manual.setManMatchFromERP((oProfile.ManMatchFromERP))
        Dim oTempFilesetup As vbBabel.FileSetup

        '12.01.2016 - This code seems to work OK, but is a bit strange
        'We run through all filesetups and then find the coordinating filesSetupe from the value in FileSetupOut
        'Then we continue to the next Filesetup and repeat the process. The values retrieved are from the last Filesetup,
        ' and that seems to work fine in all cases we have. 
        For Each oFilesetup In oProfile.FileSetups
            For Each oTempFilesetup In oProfile.FileSetups
                If oFilesetup.FileSetupOut = oTempFilesetup.FileSetup_ID Then
                    'find matching profiles
                    If oTempFilesetup.AutoMatch Then '.FormatOutID1 > 0 Then
                        '28.06.2007 - Labels are not set if the function SetSpecial ain't called
                        If Not EmptyString(oTempFilesetup.TreatSpecial) Then
                            bx = frmMATCH_Manual.SetSpecial(oTempFilesetup.TreatSpecial)
                        ElseIf Not EmptyString((oFilesetup.TreatSpecial)) Then
                            'New 25.03.2008 - for SG
                            bx = frmMATCH_Manual.SetSpecial((oFilesetup.TreatSpecial))
                        End If
                        'XNET - 02.11.2010 - Next 2 lines - To be able to match OCR-payments in manual matching when starting from the menu.
                        bx = frmMATCH_Manual.SetAlsoMatchOCR(oFilesetup.MatchOCR)
                        bMatchOCR = oFilesetup.MatchOCR
                    End If
                End If
            Next oTempFilesetup
        Next oFilesetup


        frmMATCH_Manual.SetSaveBabelFiles(False)
        '03.09.2010 - Added next IF
        ' 06.09.2010 - moved up, to be set before load !!
        'bx = frmMATCH_Manual.SetUseArchive(oProfile.UseArchive)

        If oBabelFiles Is Nothing Then
            oBabelFiles = New vbBabel.BabelFiles
            oBabelFiles.VB_Profile = oProfile
            'XNET - 02.11.2010 - Next line - To be able to match OCR-payments in manual matching when starting from the menu.
            oBabelFiles.MATCH_MatchOCR = bMatchOCR
            frmMATCH_Manual.SetBabelFiles(oBabelFiles)
        End If

        'BabelFix, retrieve value from DB_Profile
        'frmMATCH_Manual.SetFilesetup_ID sFileSetupIn_ID

        If frmMATCH_Manual.startmatching Then
            VB6.ShowForm(frmMATCH_Manual, 1, Me)
            'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            frmMATCH_Manual.Close()
            'UPGRADE_NOTE: Object frmMATCH_Manual may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            frmMATCH_Manual = Nothing
        Else
            frmMATCH_Manual.ExitManual(False)
            'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            frmMATCH_Manual.Close()
            'UPGRADE_NOTE: Object frmMATCH_Manual may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            frmMATCH_Manual = Nothing
            'iBabelBankCancel = frmMATCH_Manual.Returnvalue
        End If

        ''        Set frmMATCH_Manual = Nothing
        ''
        ''        'iBabelBankCancel = MATCH_ManualStart(oBabelFiles)
        ''        If bStoredPaymentsExists Then
        ''            'No oBabelFiles exist
        ''            frmMATCH_Manual.SetSaveBabelFiles False
        ''            If oBabelFiles Is Nothing Then
        ''                Set oBabelFiles = New vbbabel.babelfiles")
        ''                Set oBabelFiles.VB_Profile = oProfile
        ''                frmMATCH_Manual.SetBabelFiles oBabelFiles
        ''            End If
        ''        Else
        ''            frmMATCH_Manual.SetBabelFiles oBabelFiles
        ''            frmMATCH_Manual.SetSaveBabelFiles True
        ''            oBabelFiles.MATCH_MatchOCR = False 'CBool(aImportInformation(11, i))
        ''        End If
        ''        'BabelFix, retrieve value from DB_Profile
        ''        frmMATCH_Manual.SetFilesetup_ID sFileSetupIn_ID
        ''
        ''        If frmMATCH_Manual.StartMatching Then
        ''            frmMATCH_Manual.Show 1, frmStart
        ''            Unload frmMATCH_Manual
        ''            ' Check returnvalues ...
        ''            iBabelBankCancel = frmMATCH_Manual.Returnvalue
        ''        Else
        ''            frmMATCH_Manual.ExitManual False
        ''            iBabelBankCancel = frmMATCH_Manual.Returnvalue
        ''        End If
        ''
        ''        Set frmMATCH_Manual = Nothing
        ''

        If Not oBabelFiles Is Nothing Then
            'UPGRADE_NOTE: Object oBabelFiles may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oBabelFiles = Nothing
        End If

    End Sub

    Public Sub MatchUnlock_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MatchUnlock.Click
        ' Unclock items from LockedPayments
        frmUnlock.ShowDialog()
    End Sub
    Public Sub MatchStatistics_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MatchStatistics.Click
        ' 23.12.2009: Added menuchoice for printing statistics for matching
        ' run report rp_952_statistics
        BB_Statistics()

    End Sub

    Public Sub NAV_MergeFiles_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles NAV_MergeFiles.Click
        Dim aFArray() As String
        Dim aFilesArray(,) As String
        Dim i As Short
        Dim bFilesPicked As Boolean
        Dim oBabelFiles As vbBabel.BabelFiles
        Dim oBabelFile As vbBabel.BabelFile
        Dim oRTVBatch As vbBabel.Batch
        Dim oRTVPayment As vbBabel.Payment
        Dim oRTVInvoice As vbBabel.Invoice
        Dim oRTVFreeText As vbBabel.Freetext
        Dim iImport As Short
        Dim oFs As Scripting.FileSystemObject
        Dim oRTVFile As Scripting.TextStream
        Dim bHeaderWritten As Boolean
        Dim lNoOfTransactions As Integer
        Dim sLine As String
        Dim sKommunenr As String
        Dim iAmountPos As Short
        Dim dlg As OpenFileDialog

        bFilesPicked = False

        ' return filename(s) within an array
        If Array_IsEmpty(aFilesArray) Then
            aFArray = Filenamepicker(Me.dlgFileNameOpen, True)
            'aFArray = FilenamePicker((Me.dlgFileName))
        Else
            aFArray = Filenamepicker(Me.dlgFileNameOpen, True, aFilesArray(1, 0))
            'aFArray = FilenamePicker((Me.dlgFileName), aFilesArray(1, 0))
        End If
        If Not Array_IsEmpty(aFArray) Then
            bFilesPicked = True
            ' Put into aFilesArray, two dimensional,
            ' each element: 0 : Importformat, (enmum as string)
            '               1 : Filename
            '               2 : empty here
            '               3 : Report to print, enum fixed 0 - NoPrint
            '               4 : Report to screen, fixed True
            '               5 : Not in use, set to 0 (FileNameInNumber)
            '               6 : ClientNo (not in use here)
            '               7 : Version
            '               8 : EDIFormat (true/false)

            oBabelFiles = New vbBabel.BabelFiles


            For i = 0 To UBound(aFArray)

                oBabelFile = oBabelFiles.Add(CStr(1))
                oBabelFile.ImportFormat = CShort("1036") 'RTV
                oBabelFile.FilenameIn = aFArray(i) 'sImportFilename
                oBabelFile.REPORT_Print = False
                oBabelFile.REPORT_Screen = False
                oBabelFile.VB_FilenameInNo = 1
                oBabelFile.Special = "LINDORFF-RTV"
                iImport = oBabelFile.Import()




                ''        ReDim Preserve aFilesArray(12, i)
                ''        ' Check fileformat:
                ''        aFilesArray(0, i) = FileType.RTV2 'Trim$(Str(DetectFileFormat(aFArray(i))))
                ''        aFilesArray(1, i) = aFArray(i)
                ''        aFilesArray(2, i) = ""
                ''        aFilesArray(3, i) = PrintType.NoPrint
                ''        aFilesArray(4, i) = PrintType.PrintBatches
                ''        aFilesArray(5, i) = 0
                ''        aFilesArray(6, i) = ""         'DetectFormatInOut(aFilesArray(0, i))
                ''        aFilesArray(7, i) = ""
                ''        'Changed 21.04.2006 by Kjell
                ''        ' Element 8 is used in BabelImport to set the Special-property, therefore populate with nada.
                ''        aFilesArray(8, i) = "" 'DetectEDI(aFilesArray(0, i))
                ''        aFilesArray(9, i) = 0
                ''        aFilesArray(10, i) = ""
                ''        aFilesArray(11, i) = ""
                ''        aFilesArray(12, i) = ""
                ''        ' Fill in version for EDI-formats;
                ''        If aFilesArray(0, i) > 500 And aFilesArray(0, i) < 1000 Then
                ''            eFormat = aFilesArray(0, i)
                ''            aFilesArray(7, i) = DetectVersion(eFormat, aFArray(i))
                ''        End If
                ''        If aFilesArray(0, i) = "0" Then 'Filetype.unknown then
                ''            bFilesPicked = False
                ''            Exit For
                ''        Else
                ''            bFilesPicked = True
                ''        End If
            Next i
        Else
            bFilesPicked = False
        End If

        If Not bFilesPicked Then
            MsgBox("No files picked", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
        Else
            'Set oBabelFiles = ImportBabel(aFilesArray(), oProfile, False, False, Nothing)

            oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")

            bHeaderWritten = False
            sKommunenr = ""
            lNoOfTransactions = 0
            For Each oBabelFile In oBabelFiles
                For Each oRTVBatch In oBabelFile.Batches
                    'bFileOpened = False
                    For Each oRTVPayment In oRTVBatch.Payments

                        If Not bHeaderWritten Then
                            oRTVFile = oFs.OpenTextFile("C:\CONCAT\RTVSamlet" & oRTVPayment.E_Account & oRTVPayment.DATE_Payment & ".TXT", Scripting.IOMode.ForAppending, True)
                            'Write headerrecord
                            '101018801  280205   HALDEN TRYGDEKONTOR
                            sLine = "1" 'recordtype
                            sLine = sLine & PadRight(oRTVPayment.E_Account, 4, " ") 'Kommunenr.
                            sLine = sLine & "8801" & "  " 'ID Lindorf
                            sLine = sLine & VB.Right(oRTVPayment.DATE_Payment, 2) & Mid(oRTVPayment.DATE_Payment, 5, 2) & Mid(oRTVPayment.DATE_Payment, 3, 2) & "   " 'Dato, "DDMMYY")
                            sLine = sLine & oRTVPayment.E_Name
                            sLine = PadRight(sLine, 79, " ")
                            sKommunenr = oRTVPayment.E_Account
                            oRTVFile.WriteLine(sLine)
                            bHeaderWritten = True
                        End If
                        'Write transaction
                        '20101880101073239911AAA MARTIN                    000100000000000
                        For Each oRTVInvoice In oRTVPayment.Invoices
                            sLine = "2" 'recordtype
                            sLine = sLine & PadRight(oRTVPayment.E_Account, 4, " ") 'Kommunenr.
                            sLine = sLine & "8801" 'ID Lindorf
                            For Each oRTVFreeText In oRTVInvoice.Freetexts
                                iAmountPos = InStrRev(oRTVFreeText.Text, ":")
                                sLine = sLine & VB.Left(oRTVFreeText.Text, 11) 'F�dselsnr.
                                If oRTVPayment.ImportFormat = vbBabel.BabelFiles.FileType.RTV2 Then
                                    'SISMO - No name specified
                                    sLine = sLine & Space(30) 'Name
                                Else
                                    'Lindorff
                                    sLine = sLine & PadRight(Mid(oRTVFreeText.Text, 13, iAmountPos - 13), 30, " ") 'Name
                                End If
                                Exit For 'Only 1 freetext
                            Next oRTVFreeText
                            sLine = sLine & PadLeft(CStr(oRTVInvoice.MON_InvoiceAmount), 9, "0") & "000000" 'Amount
                            sLine = PadRight(sLine, 79, " ")
                            oRTVFile.WriteLine(sLine)
                            lNoOfTransactions = lNoOfTransactions + 1
                        Next oRTVInvoice
                    Next oRTVPayment
                Next oRTVBatch
            Next oBabelFile
            'Write footerrecord
            '801018801           0000056
            sLine = "8" 'recordtype
            sLine = sLine & PadRight(sKommunenr, 4, " ") 'Kommunenr.
            sLine = sLine & "8801" 'ID Lindorf
            sLine = sLine & Space(11)
            sLine = sLine & PadLeft(Trim(Str(lNoOfTransactions)), 7, "0")
            sLine = PadRight(sLine, 79, " ")
            oRTVFile.WriteLine(sLine)
            'Write fileend
            '999998801           0007068
            sLine = "999998801" & Space(11)
            sLine = sLine & PadLeft(Trim(Str(lNoOfTransactions + 3)), 7, "0") 'Include header and footer and this line
            sLine = PadRight(sLine, 79, " ")
            oRTVFile.WriteLine(sLine)
            oRTVFile.Close()

            '    Set oBabelExport = New vbbabel.babelexport")
            '    Set oBabelExport.BabelFiles = oBabelFiles
            '    oBabelExport.VB_Format_ID = 1 'aExportInformation(0, iArrayCounter)
            '    oBabelExport.ExportFormat = FileType.RTV 'Val(aExportInformation(1, iArrayCounter))
            '    oBabelExport.VB_MultiFiles = False 'aExportInformation(2, iArrayCounter)
            '    oBabelExport.FILE_Name = "C:\CONCAT\RTVSamlet.TXT"  'Left$(aExportInformation(3, iArrayCounter), InStr(1, aExportInformation(3, iArrayCounter), ".", vbTextCompare) - 1) & "_" & Trim$(Str(iFileCounter)) & Right(aExportInformation(3, iArrayCounter), 4)
            '    oBabelExport.VB_I_Account = "" 'aExportInformation(4, iArrayCounter)
            '    oBabelExport.VB_REF_Own = "" 'aExportInformation(5, iArrayCounter)
            '    oBabelExport.FILE_OverWrite = True 'aExportInformation(6, iArrayCounter)
            '    oBabelExport.FILE_WarningIfExists = False 'aExportInformation(7, iArrayCounter)
            '    oBabelExport.VB_FilenameInNo = 1 'aExportInformation(8, iArrayCounter)
            '    oBabelExport.VB_ClientNo = "" 'aExportInformation(9, iArrayCounter)
            '    oBabelExport.VB_CompanyNo = "" 'aExportInformation(10, iArrayCounter)
            '    oBabelExport.VB_AdditionalID = "" 'aExportInformation(11, iArrayCounter)
            '    oBabelExport.VB_AccountIDAdd = "" 'aExportInformation(12, iArrayCounter)
            '    oBabelExport.VB_Branch = "" 'aExportInformation(13, iArrayCounter)
            '    oBabelExport.Version = "" 'aExportInformation(14, iArrayCounter)
            '    'New 15.11.2006
            '    oBabelExport.Special = "" 'sSpecial
            '    'oBabelExport.FilenameImported = aExportInformation(16, iArrayCounter)
            '    'oBabelExport.Bank = aExportInformation(18, iArrayCounter)
            '    'oBabelExport.VB_PostAgainstObservationAccount = bPostAgainstObservationAccount
            '
            '    oBabelExport.Export

        End If

        If Not oRTVFile Is Nothing Then
            oRTVFile.Close()
            oRTVFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

    End Sub

    Private Sub NAV_SplitT14Files_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NAV_SplitT14Files.Click
        'XNET - 08.02.2012 - Whole FUNCTION
        '************************************
        'M� OGS� LEGGE INN NYTT MENYVALG FOR DENNE!!!!!!!!!!!!!!!!!!!
        '************************************
        Dim aFArray() As String
        Dim bFilesPicked As Boolean
        Dim oBabelFiles As vbBabel.BabelFiles
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBabelExport As vbBabel.BabelExport
        Dim oRTVBatch As vbBabel.Batch
        Dim oRTVPayment As vbBabel.Payment
        Dim oRTVInvoice As vbBabel.Invoice
        Dim oRTVFreeText As vbBabel.Freetext
        Dim oFs As Scripting.FileSystemObject
        Dim oRTVFile As Scripting.TextStream
        Dim i As Integer
        Dim iImport As Integer
        Dim sLine As String
        Dim bImportOK As Boolean
        Dim bErrorExist As Boolean
        Dim sErrorString As String
        Dim iResponse As Integer
        Dim bContinue As Boolean
        Dim bNewFile As Boolean
        Dim sFilePath As String
        'XNET - 14.02.2012 - Added 3 new variables
        Dim sFilename As String
        Dim iFilenameCounter As Integer
        Dim oBackup As vbBabel.BabelFileHandling

        Dim iAmountPos As Integer
        Dim lNoOfTransactions As Long
        Dim sKommunenr As String

        bErrorExist = False
        sErrorString = ""

        ' return filename(s) within an array
        If Array_IsEmpty(aFilesArray) Then
            aFArray = Filenamepicker(Me.dlgFileNameOpen, True)
        Else
            ' give filename last used as parameter
            aFArray = Filenamepicker(Me.dlgFileNameOpen, True, aFilesArray(1, 0))
        End If
        If Not Array_IsEmpty(aFArray) Then
            bFilesPicked = True
            ' Put into aFilesArray, two dimensional,
            ' each element: 0 : Importformat, (enmum as string)
            '               1 : Filename
            '               2 : empty here
            '               3 : Report to print, enum fixed 0 - NoPrint
            '               4 : Report to screen, fixed True
            '               5 : Not in use, set to 0 (FileNameInNumber)
            '               6 : ClientNo (not in use here)
            '               7 : Version
            '               8 : EDIFormat (true/false)

            oBabelFiles = New vbBabel.BabelFiles  '23.05.2017 CreateObject ("vbbabel.babelfiles")

            For i = 0 To UBound(aFArray)

                oBabelFile = oBabelFiles.Add(1)
                oBabelFile.ImportFormat = "1036" 'RTV
                oBabelFile.FilenameIn = aFArray(i) 'sImportFilename
                oBabelFile.REPORT_Print = False
                oBabelFile.REPORT_Screen = False
                oBabelFile.VB_FilenameInNo = 1
                On Error GoTo ErrImport
                bImportOK = True
                iImport = oBabelFile.Import()
                On Error GoTo 0
                If Not bImportOK Then
                    oBabelFiles.Remove(oBabelFile.Index)
                    bErrorExist = True
                    If EmptyString(sErrorString) Then
                        sErrorString = "F�lgende fil(er) ble ikke importert: " & vbCrLf & aFArray(i)
                    Else
                        sErrorString = sErrorString & vbCrLf & aFArray(i)
                    End If
                End If

            Next i
        Else
            bFilesPicked = False
        End If

        If Not bFilesPicked Then
            MsgBox("No files picked", vbOKOnly + vbInformation)
        Else
            bContinue = True
            If Not EmptyString(sErrorString) Then
                iResponse = MsgBox(sErrorString & vbCrLf & vbCrLf & "�nsker du � fortsette med splitting av filer?", vbYesNo + vbQuestion, "Fortsette?")
                If iResponse = vbYes Then
                    bContinue = True
                Else
                    bContinue = False
                End If

            End If

            If bContinue Then
                sFilePath = Trim(oProfile.Custom1Value) & "\"
                'oFs = CreateObject ("Scripting.Filesystemobject")
                oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")

                For Each oBabelFile In oBabelFiles
                    For Each oRTVBatch In oBabelFile.Batches
                        For Each oRTVPayment In oRTVBatch.Payments
                            bNewFile = True
                            For Each oRTVInvoice In oRTVPayment.Invoices
                                If bNewFile Then
                                    'XNET - 14.02.2012 - Added new code
                                    sFilename = ""

                                    For iFilenameCounter = 1 To 1000
                                        sFilename = sFilePath & Trim(Str(oRTVPayment.MON_InvoiceAmount)) & "-" & oBabelFile.DATE_Production & "-" & oRTVPayment.I_Account & "-" & oRTVPayment.I_Name & "_" & Trim(Str(iFilenameCounter)) & ".csv"
                                        If Not oFs.FileExists(sFilename) Then
                                            Exit For
                                        End If
                                    Next iFilenameCounter

                                    If iFilenameCounter > 998 Then
                                        Err.Raise(2983, "NAV_SplitT14Files", "BabelBank is not able to create a unique filename because there are to many identical files.")
                                    End If

                                    oRTVFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForAppending, True)

                                    'XNET - End new/changed code

                                    'Write headerrecord
                                    'H;20120202;AKTIV KAPITALADMINISTRASJ;20120202;00942824378; ;60030550741;80000429451;20120101;20120131;
                                    sLine = "H" & ";" 'recordtype
                                    sLine = sLine & oBabelFile.DATE_Production & ";"
                                    sLine = sLine & oRTVPayment.E_Name & ";"
                                    sLine = sLine & oBabelFile.DATE_Production & ";"
                                    sLine = sLine & oBabelFile.IDENT_Receiver & ";"
                                    sLine = sLine & " ;"
                                    sLine = sLine & oRTVPayment.I_Account & ";"
                                    sLine = sLine & oBabelFile.EDI_MessageNo & ";"
                                    sLine = sLine & oBabelFile.DATE_Production & ";"
                                    sLine = sLine & oBabelFile.DATE_Production & ";"
                                    oRTVFile.WriteLine(sLine)
                                    bNewFile = False
                                End If
                                'E;Q313480005;4819;NAV                      ;9218852;993111031;01105537251;LIEN INGER JOHANNE;20120101;20120131;330000;KU;
                                sLine = "E" & ";" 'recordtype
                                sLine = sLine & oRTVPayment.REF_Bank1 & ";"
                                sLine = sLine & oRTVPayment.I_Adr1 & ";"
                                sLine = sLine & oRTVPayment.I_Name & ";"
                                sLine = sLine & oRTVInvoice.Unique_Id & ";"
                                sLine = sLine & oRTVPayment.I_Adr3 & ";"
                                sLine = sLine & oRTVInvoice.REF_Bank & ";" '7
                                sLine = sLine & oRTVInvoice.REF_Own & ";"
                                sLine = sLine & " ;"
                                sLine = sLine & " ;"
                                sLine = sLine & Trim(Str(oRTVInvoice.MON_InvoiceAmount)) & ";"
                                sLine = sLine & "KU" & ";"
                                oRTVFile.WriteLine(sLine)
                            Next oRTVInvoice
                            'Write End Batch
                            'D;6278900;
                            sLine = "D" & ";" 'recordtype
                            sLine = sLine & Trim(Str(oRTVPayment.MON_InvoiceAmount)) & ";"
                            oRTVFile.WriteLine(sLine)
                            'A;6278900;
                            sLine = "A" & ";" 'recordtype
                            sLine = sLine & Trim(Str(oRTVPayment.MON_InvoiceAmount)) & ";"
                            oRTVFile.WriteLine(sLine)
                            'Write End File
                            'T;7208900;
                            sLine = "T" & ";" 'recordtype
                            sLine = sLine & Trim(Str(oRTVPayment.MON_InvoiceAmount)) & ";"
                            oRTVFile.WriteLine(sLine)
                            oRTVFile.Close()
                            oRTVFile = Nothing
                        Next oRTVPayment
                    Next oRTVBatch
                Next oBabelFile

                'XNET - 12.02.2012
                'Move imported files to backup - also the ones not imported!

                For i = 0 To UBound(aFArray)
                    oBackup = New vbBabel.BabelFileHandling  '23.05.2017 CreateObject ("vbbabel.BabelFileHandling")
                    oBackup.BackupPath = oProfile.BackupPath
                    oBackup.SourceFile = aFArray(i)
                    oBackup.DeleteOriginalFile = True
                    oBackup.FilesetupID = 8
                    oBackup.InOut = "I"
                    oBackup.FilenameNo = 8 'FilenameOut1 or 2 or 3
                    oBackup.Client = vbNullString
                    oBackup.TheFilenameHasACounter = False
                    oBackup.BankAccounting = "B"

                    bImportOK = oBackup.CreateBackup()
                    oBackup = Nothing

                Next i

            End If
        End If

        MsgBox("Vellykket splitting!", vbOKOnly)

        Exit Sub

ErrImport:
        bImportOK = False
        Resume Next


    End Sub

    Public Sub NordeaLiv_Balance_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles NordeaLiv_Balance.Click

        'TODO: THIS FUNCTION IS NOT CONVERTED IN VB.NET

        MsgBox("The function NordeaLiv_Balance_Click is not yet converted to vb.net" & vbCrLf & vbCrLf & "Please contact Visual Banking AS", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Function not available")

        'frmNordeaLiv.SetCompanyID((oProfile.Company_ID))

        'frmNordeaLiv.ShowDialog()

        ''UPGRADE_NOTE: Object frmNordeaLiv may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        'frmNordeaLiv = Nothing

    End Sub

    Public Sub PrintOmittedPayments_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles PrintOmittedPayments.Click
        ' Print omitted transactions
        Dim lReturnValue As Integer
        Dim bx As Boolean
        Dim sMySQL As String
        Dim oOmittedBabelFiles As vbBabel.BabelFiles
        Dim oOmittedBabelFile As vbBabel.BabelFile
        Dim oOmittedBatch As vbBabel.Batch
        Dim oOmittedPayment As vbBabel.Payment
        Dim oOmittedBabelReport As vbBabel.BabelReport

        Dim oOmittedFileSetup As vbBabel.FileSetup
        Dim oOmittedReport As vbBabel.Report
        Dim oOmittedClient As vbBabel.Client

        Dim oMyDal As vbBabel.DAL = Nothing

        Dim lReportCount As Integer
        Dim bContinue As Boolean
        Dim aTmp() As String
        Dim aeMails() As String
        Dim i As Short

        oMyDal = New vbBabel.DAL
        oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oMyDal.ConnectToDB() Then
            Throw New System.Exception(oMyDal.ErrorMessage)
        End If

        sMySQL = "UPDATE Company SET OmittedReportInProgress = True WHERE Company_ID = " & Trim(Str(oProfile.Company_ID))

        oMyDal.SQL = sMySQL

        If oMyDal.ExecuteNonQuery Then

            lReturnValue = oMyDal.RecordsAffected

        Else

            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

        End If

        'Old code
        'lReturnValue = UpdateProfileDB(sMySQL, "PrintOmittedPayments")

        'Retrieve the omitted payments from the Profile.mdb
        oOmittedBabelFiles = New vbBabel.BabelFiles
        bx = oOmittedBabelFiles.MATCH_RetrieveOmittedData((oProfile.Company_ID))

        'Validate if some of the payments are in use by a user.
        For Each oOmittedBabelFile In oOmittedBabelFiles
            For Each oOmittedBatch In oOmittedBabelFile.Batches
                For Each oOmittedPayment In oOmittedBatch.Payments
                    If IsPaymentLockedByAnotherUser(oOmittedBabelFile.Index, oOmittedBatch.Index, oOmittedPayment.Index, True) Then
                        MsgBox("One of the payments that are marked as 'Omit' are in use by another user." & vbCrLf & vbCrLf & "Payers name: " & oOmittedPayment.E_Name & vbCrLf & "Amount:      " & ConvertFromAmountToString(oOmittedPayment.MON_InvoiceAmount, ".", ","), MsgBoxStyle.OKOnly + MsgBoxStyle.Critical)
                        Exit Sub
                    End If
                Next oOmittedPayment
            Next oOmittedBatch
        Next oOmittedBabelFile

        bContinue = False

        If bx Then
            If oOmittedBabelFiles.Count > 0 Then

                For Each oOmittedFileSetup In oProfile.FileSetups
                    If oOmittedFileSetup.Filesetup_ID = oOmittedBabelFiles.Item(1).VB_FileSetupID Then
                        bContinue = True
                        Exit For
                    End If
                Next oOmittedFileSetup

                If bContinue Then
                    bContinue = False
                    For lReportCount = oOmittedFileSetup.Reports.Count To 1 Step -1
                        'Set oreport = oFilesetup.Reports.Item(lreportCount)
                        If oOmittedFileSetup.Reports.Item(lReportCount).ReportNo = CDbl("503") Then
                            oOmittedReport = oOmittedFileSetup.Reports.Item(lReportCount)
                            oOmittedReport.Heading = "SLETTEDE BETALINGER"
                            bContinue = True
                            Exit For
                        End If
                    Next lReportCount
                Else
                    MsgBox("Can't find correct setup-file to look for reports.", MsgBoxStyle.Critical + MsgBoxStyle.OKOnly)
                    Exit Sub
                End If

                If bContinue Then
                    oOmittedBabelReport = New vbBabel.BabelReport
                    oOmittedBabelReport.BabelFiles = oOmittedBabelFiles
                    'TODO: BABELREPORT 
                    '    oOmittedBabelReport.Report = oOmittedReport

                    '    ReDim aeMails(0)
                    '    For Each oOmittedClient In oOmittedFileSetup.Clients
                    '        ' Set eMailadresses in case eMail reports;
                    '        aTmp = dbFillEmails(oOmittedClient.ClientNo, oOmittedClient.Client_ID) ' upto 3 emails pr client
                    '        i = 0
                    '        If Not Array_IsEmpty(aTmp) Then
                    '            Do
                    '                If i <= UBound(aTmp) Then
                    '                    If i > 0 Then
                    '                        ReDim Preserve aeMails(UBound(aeMails) + 1)
                    '                    End If
                    '                    'UPGRADE_WARNING: Couldn't resolve default property of object aeMails(UBound()). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '                    aeMails(UBound(aeMails)) = aTmp(i)
                    '                Else
                    '                    Exit Do
                    '                End If
                    '                i = i + 1
                    '            Loop
                    '        End If
                    '    Next oOmittedClient

                    '    oOmittedBabelReport.EMailAddresses = VB6.CopyArray(aeMails)

                    '    If Not oOmittedBabelReport.Start Then
                    '        bx = False
                    '    Else
                    '        bx = True
                    '    End If
                Else
                    MsgBox("Can't find any reports to run.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
                    Exit Sub
                End If
            Else
                MsgBox("No payments are marked as omitted", MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
            End If
        Else
            MsgBox(oOmittedBabelFiles.MATCH_ErrorText, MsgBoxStyle.OKOnly + MsgBoxStyle.Critical)
        End If

        bx = oOmittedBabelFiles.MATCH_DeleteOmittedData((oProfile.Company_ID))

        sMySQL = "UPDATE Company SET OmittedReportInProgress = False WHERE Company_ID = " & oProfile.Company_ID.ToString
        oMyDal.SQL = sMySQL

        If oMyDal.ExecuteNonQuery Then
            lReturnValue = oMyDal.RecordsAffected
        Else
            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
        End If

        'Old code
        'lReturnValue = UpdateProfileDB(sMySQL, "PrintOmittedPayments")

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

    End Sub

    Public Sub RenameFiles_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles RenameFiles.Click
        'This is a function where you may change all paths in the Profile.mdb
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sCompany_ID As String

        Try
            sCompany_ID = "1" 'TODO - Hardcoded CompanyID
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            'Retrieve information about stored conversions
            sMySQL = "SELECT OriginalPath, NewPath FROM Company WHERE Company_ID = " & sCompany_ID
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Do While oMyDal.Reader_ReadRecord

                    frmChangeFilePaths.txtFilepathFrom.Text = oMyDal.Reader_GetString("OriginalPath")
                    frmChangeFilePaths.txtFilepathTo.Text = oMyDal.Reader_GetString("NewPath")

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "RenameFile_Click")

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        frmChangeFilePaths.SetCompanyID(sCompany_ID)

        frmChangeFilePaths.ShowDialog()

    End Sub

    Public Sub SANTANDER_Findaccount_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SANTANDER_Findaccount.Click

        frmSantander.SetCompanyID((oProfile.Company_ID))

        frmSantander.ShowDialog()

        'UPGRADE_NOTE: Object frmSantander may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        frmSantander = Nothing

    End Sub
    Public Sub SetupClientSplit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SetupClientSplit.Click
        '------------------------------------------------------------
        ' Call Setup with oProfile, and no selected profile
        ' Used to give start/length/value for OCR splitting where
        ' accountno is not used
        '------------------------------------------------------------
        Dim oClientSplit As BabelSetup.ClientSplit
        Dim bClientSplit As Boolean

        oClientSplit = New BabelSetup.ClientSplit

        'UPGRADE_WARNING: Couldn't resolve default property of object oClientSplit.Profile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        oClientSplit.Profile = oProfile

        'UPGRADE_WARNING: Couldn't resolve default property of object oClientSplit.Start. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        bClientSplit = oClientSplit.Start()

        If bClientSplit = True Then
            'UPGRADE_WARNING: Couldn't resolve default property of object oClientSplit.Profile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oProfile = oClientSplit.Profile

            oProfile.Save(1)
            ' Reread profile, to make sure all values are set correctly
            oProfile.Load(1)

        Else
            ' Run new load to make sure no errors are carried on
            oProfile.Load(1)
        End If

    End Sub
    Public Sub MatchingClients_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MatchingClients.Click
        'cmdClients_Click

        '------------------------------------------------------------
        ' "New" Clientsetup - with parameters also for matching
        '------------------------------------------------------------
        Dim oClientSetup As BabelSetup.ClientSetup
        Dim bClientSetup As Boolean

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        oClientSetup = New BabelSetup.ClientSetup
        oClientSetup.Profile = oProfile
        bClientSetup = oClientSetup.StartNew()

        'The bClientSetup is of no use, because it alwayss returns false, but thats OK!

        If bClientSetup = True Then
            '    Set oProfile = oClientSetup.Profile'

            'oProfile.Save (1)
            ' Reread profile, to make sure all values are set correctly
            'oProfile.Load (1)

        Else
            ' Run new load to make sure no errors are carried on
            oProfile.Load(1)
        End If

        Me.Cursor = System.Windows.Forms.Cursors.Default
        'End If

    End Sub

    Public Sub MatchingManualSetup_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MatchingManualSetup.Click

        VB6.ShowForm(frmMATCH_ManualSetup, 1, Me)


        If Not frmMATCH_ManualSetup.WasFormCanceled Then
            oProfile.ManMatchFromERP = CBool(frmMATCH_ManualSetup.chkMatchARItem.CheckState)
            oProfile.ERPPaymentID = frmMATCH_ManualSetup.txtERPID.Text
        End If

        frmMATCH_ManualSetup.Close()
    End Sub
    'Private Sub MatchingManualeMail_Click()
    'frmMATCH_ManualeMail.Show 1
    'End Sub

    Public Sub SetupCompanyInfo_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SetupCompanyInfo.Click

        '------------------------------------------------------------
        ' Call Setup with oProfile, and no selected profile
        ' Change companyinfo
        '------------------------------------------------------------
        Dim oCompanySetup As BabelSetup.Company
        Dim bCompanySetup As Boolean

        oCompanySetup = New BabelSetup.Company

        oCompanySetup.Profile = oProfile

        bCompanySetup = oCompanySetup.Start()

        If bCompanySetup = True Then
            '    Set oProfile = oCompanySetup.Profile

            oProfile.Save(1)
            ' Reread profile, to make sure all values are set correctly

            oProfile.Load(1)

        Else
            ' Run new load to make sure no errors are carried on
            oProfile.Load(1)
        End If

    End Sub

    Public Sub SetupDocumentation_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SetupDocumentation.Click

        BBDocumentation()

    End Sub
    Private Sub SetupCustomValue1_Click()

        BBDocumentation()
        'Hva er dette?

    End Sub

    Public Sub SetupShortcut_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SetupShortcut.Click
        '------------------------------------------------------------
        ' Call Setup to create shortcut for current profile
        '------------------------------------------------------------
        If iSelectedProfile = 0 Then
            MsgBox(LRS(60042), MsgBoxStyle.OKOnly + MsgBoxStyle.Critical) '("Du m� f�rst velge en profil!")
        Else
            If CreateShortcut(sSelectedShortname, "BabelBank") > 0 Then
                MsgBox(LRS(62024, sSelectedShortname), MsgBoxStyle.OKOnly + MsgBoxStyle.Information, "BabelBank") ' Snarvei laget
            Else
                MsgBox(LRS(62025, sSelectedShortname), MsgBoxStyle.OKOnly + MsgBoxStyle.Information, "BabelBank") ' Feil ved opprettelse av snarvei
            End If
        End If

    End Sub


    Public Sub HelpAbout_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles HelpAbout.Click


        'If RunTime() Then  'DISKUTERNOTRUNTIME Dette blir feil i 2017
        frmAbout.lblVersion.Text = "Programversion " & _
        Replace(System.Diagnostics.FileVersionInfo.GetVersionInfo(My.Application.Info.DirectoryPath & "\vbbabel.dll").ProductVersion, ".0.", ".") & vbNewLine & "DatabaseVersion " & GetVersion()
        '(Redo from like 2.2.0.32 to 2.2.32)
        '??? System.Reflection.Assembly.GetExecutingAssembly.GetName.Version.Major
        'Else
        '    frmAbout.lblVersion.Text = "Programversion " & _
        '    Replace(System.Diagnostics.FileVersionInfo.GetVersionInfo("C:\Projects.net\BabelBank\Bin\x86\release\vbbabel.dll").ProductVersion, ".0.", ".") & vbNewLine & "DatabaseVersion " & GetVersion()
        'End If

        'Load frmAbout  removed 05.08.2008
        frmAbout.Text = LRS(60041) & " " & "BabelBank"
        frmAbout.lblAppPath.Text = "Program:       " & My.Application.Info.DirectoryPath
        frmAbout.lblDatabasePath.Text = "Database:"
        If BB_UseSQLServer() Then
            frmAbout.txtDatabasePath.Text = FindBBConnectionString()
        Else
            frmAbout.txtDatabasePath.Text = BB_DatabasePath()
        End If
        frmAbout.txtLicensePath.Text = BB_LicensePath()
        frmAbout.Show()

    End Sub

    Public Sub SetupLicense_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SetupLicense.Click
        Dim oLicense As vbBabel.License
        Dim iFacitKey As Short

        oLicense = New vbBabel.License
        frmLicense.Text = LRS(62013) & " " + oLicense.LicName
        frmLicense.lblNumber.Text = LRS(62014)
        frmLicense.lblLimitRecords.Text = LRS(62015)
        frmLicense.lblUsedRecords.Text = LRS(62016)
        frmLicense.lblLimitDate.Text = LRS(62017)
        frmLicense.lblLimitFormats.Text = LRS(62021)
        frmLicense.lblSupport.Text = LRS(62022)

        frmLicense.txtNumber.Text = oLicense.LicNumber
        frmLicense.txtLimitRecords.Text = oLicense.LicLimitRecords
        frmLicense.txtUsedRecords.Text = oLicense.LicUsedRecords
        frmLicense.txtLimitDate.Text = oLicense.LicLimitDate
        frmLicense.txtLimitFormats.Text = oLicense.LicLimitFormats
        frmLicense.txtSupport.Text = oLicense.LicSupport

        frmLicense.ShowDialog()

        ' Check for OneTimeLicense;
        'New 02.08.02
        'Key to unlock license on expirydate for one time necessity use:
        'Algorithm: vbs adress (99) + abs(dd - mm*2) + yy*3
        'Example: august 2 2002: 99 + abs(2 - 8*2) + 2*3 = 119
        iFacitKey = 99 + System.Math.Abs(VB.Day(Now) * 2 - Month(Now)) + (Year(Now) * 3) - 6000
        If iFacitKey = Val(frmLicense.txtOneTimeLicenseKey.Text) Then
            oLicense.LicOneTimeUnlock()
            MsgBox("Lisens l�st opp", MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
        End If
        frmLicense.Close()
        'UPGRADE_NOTE: Object oLicense may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oLicense = Nothing

    End Sub
    Public Sub SetupLicenseCode_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SetupLicenseCode.Click
        Dim aFilesArray(,) As String
        ' key in licensecode
        ' A 28-digit code holds info about
        ' - limitnumber of records
        ' - limitdate
        ' - no of formats
        ' - support or not
        ' - services available
        Dim oLicense As vbBabel.License
        Dim sCode As String

        On Error GoTo ErrorHandling

        oLicense = New vbBabel.License
        frmLicenseCode.Text = LRS(62013) & " " + oLicense.LicName
        frmLicenseCode.lblNumber.Text = LRS(62014)
        frmLicenseCode.lblLicenseName.Text = LRS(62013)
        frmLicenseCode.lblLicenseCode.Text = LRS(50813)
        frmLicenseCode.cmdCancel.Text = LRS(55006) '"&Avbryt"

        frmLicenseCode.txtNumber.Text = oLicense.LicNumber
        frmLicenseCode.txtName.Text = oLicense.LicName

        If oLicense.LicNumber <> "31000" Then
            ' user may not change licensenumber if present in licensefile
            frmLicenseCode.txtNumber.Enabled = False
        Else
            frmLicenseCode.txtNumber.SelectionStart = 0
            frmLicenseCode.txtNumber.SelectionLength = 5
            'frmLicenseCode.txtNumber.SelText = oLicense.LicNumber
        End If

        If VB.Left(UCase(oLicense.LicName), 4) <> "MIDL" And Len(Trim(oLicense.LicName)) > 0 Then
            ' user may not change licensename if present in licensefile
            frmLicenseCode.txtName.Enabled = False
        End If



        Do While True
            '    Load frmLicenseCode
            '   frmLicenseCode.txtLicenseCode1.SetFocus
            VB6.ShowForm(frmLicenseCode, 1, Me)
            If frmLicenseCode.bRetValue Then
                ' Check licensekey
                sCode = frmLicenseCode.txtLicenseCode1.Text & frmLicenseCode.txtLicenseCode2.Text & frmLicenseCode.txtLicenseCode3.Text & frmLicenseCode.txtLicenseCode4.Text
                If oLicense.LicCode(sCode) Then
                    oLicense.LicNumber = frmLicenseCode.txtNumber.Text
                    oLicense.LicName = frmLicenseCode.txtName.Text
                    Exit Do
                Else
                    MsgBox(LRS(15014), MsgBoxStyle.OKOnly + MsgBoxStyle.Critical) '"Error in licensecode!"
                End If
            Else
                Exit Do ' User cancelled
            End If
        Loop

        frmLicenseCode.Close()

        'UPGRADE_NOTE: Object oLicense may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oLicense = Nothing
        Exit Sub

ErrorHandling:
        ErrorHandling(Err, "", "BabelBank")
        ' Show messagebox, with possiblity to send Files to Support@visualbanking.net
        'UPGRADE_WARNING: Couldn't resolve default property of object aFilesArray. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        SupportMail(aFilesArray, Err.Description)

    End Sub

    Public Sub HelpSupportPhone_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles HelpSupportPhone.Click
        ' show a form with contact info
        'Load frmContactInfo  Removed 05.08.2008
        frmContactInfo.Text = LRS(60038)
        frmContactInfo.lblText.Text = LRS(60039) & " " & "Visual Banking AS"
        frmContactInfo.lblText2.Text = LRS(60040)

        frmContactInfo.Show()

    End Sub
    Public Sub SetupChangeProfile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SetupChangeProfile.Click
        'ChangeProfile
        ' Changed 06.10.05. Then we need only one sub
        cmdChangeProfile_Click(cmdChangeProfile, New System.EventArgs())
    End Sub


    Public Sub SetupClients_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SetupClients.Click
        'cmdClients_Click
        ' Changed 06.09.06 to use new client-setup
        MatchingClients_Click(MatchingClients, New System.EventArgs())
    End Sub

    Public Sub SetupDeleteProfile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SetupDeleteProfile.Click
        cmdDeleteProfile_Click(cmdDeleteProfile, New System.EventArgs())
    End Sub

    Public Sub SetupNewProfile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SetupNewProfile.Click
        NewProfile()
    End Sub
    Public Sub SetupSequence_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SetupSequence.Click
        '------------------------------------------------------------
        ' Change sequenceNo
        '------------------------------------------------------------
        Dim oSequenceSetup As Object
        Dim bSequenceSetup As Boolean

        oSequenceSetup = New BabelSetup.Sequence

        'UPGRADE_WARNING: Couldn't resolve default property of object oSequenceSetup.Profile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        oSequenceSetup.Profile = oProfile

        'UPGRADE_WARNING: Couldn't resolve default property of object oSequenceSetup.Start. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        bSequenceSetup = oSequenceSetup.Start()

        If bSequenceSetup = True Then
            'UPGRADE_WARNING: Couldn't resolve default property of object oSequenceSetup.Profile. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oProfile = oSequenceSetup.Profile

            oProfile.Save(1)
            ' Reread profile, to make sure all values are set correctly
            oProfile.Load(1)

        Else
            ' Run new load to make sure no errors are carried on
            oProfile.Load(1)
        End If


    End Sub

    Public Sub SI_PeriodicExport_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SI_PeriodicExport.Click

        Dim bImportExportOK As Boolean
        Dim aSelectedFilenames() As String
        Dim oLocalBabelFile As vbBabel.BabelFile
        Dim iOldFileSetupID As Short
        Dim bFirstFileSetupIDFound As Boolean
        Dim bProceed As Boolean
        Dim oFilesetup As vbBabel.FileSetup
        Dim aLocked(,) As Object
        Dim dLastDate As Date
        Dim sLastDate As String
        Dim sMonth, sYear, sDay As String
        Dim bValidDateStated, bOutOfLoop As Boolean
        Dim sResponse As String

        'First, test if any payments are locked
        aLocked = frmMATCH_Manual.FindPaymentsLocked(True)

        If Not Array_IsEmpty(aLocked) Then
            MsgBox(LRS(17512) & vbCrLf & vbCrLf & LRS(17513, Replace(LRS(50707), "&", ""), Replace(LRS(50700), "&", "")), MsgBoxStyle.Information + MsgBoxStyle.OkOnly, LRS(17514))
            'MsgBox "Can't process the requested action, because at least one payment is locked by another user." _
            '& vbCrLf & vbCrLf & "To unlock the payments choose 'Unlock Payments' on the 'Matching'-menu.", vbInformation + vbOKOnly, "CAN'T START EXPORT."
        Else
            'OK, no payments are locked

            'Ask for date restriction
            bOutOfLoop = False
            bValidDateStated = False
            ' Ask for value date:
            Do While bOutOfLoop = False
                bOutOfLoop = True
                dLastDate = DateSerial(Year(Now), Month(Now), 1 - 1)

                sResponse = InputBox("Legg inn siste bokf�ringsdato som skal eksporteres (dd.mm.yyyy)", "Bokf�ringsdato:", PadLeft(Trim(Str(VB.Day(Now))), 2, "0") & "." & PadLeft(Trim(Str(Month(Now))), 2, "0") & "." & Trim(Str(Year(Now))))
                If sResponse = "" Then
                    ' user pressed Cancel
                    bOutOfLoop = False
                    Exit Do
                End If

                If Len(sResponse) - Len(Replace(sResponse, ".", "")) = 2 Then
                    ' OK, two . in datestring:
                    ' Test date given;
                    sDay = VB.Left(sResponse, InStr(sResponse, ".") - 1)
                    sMonth = Mid(sResponse, InStr(sResponse, ".") + 1, 2)
                    If VB.Right(sMonth, 1) = "." Then
                        ' only one digit for month
                        sMonth = VB.Left(sMonth, 1)
                    End If
                    sYear = VB.Right(sResponse, 4)
                    If Val(sYear) < 2001 Or Val(sYear) > 2099 Then
                        MsgBox("Feil i �rstall - Pr�v igjen!", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
                        bOutOfLoop = False
                    End If
                    If Val(sMonth) < 1 Or Val(sMonth) > 12 Then
                        MsgBox("Feil i m�ned - Pr�v igjen!", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
                        bOutOfLoop = False
                    End If
                    If Val(sDay) < 1 Or Val(sDay) > 31 Then
                        MsgBox("Feil i dag - Pr�v igjen!", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
                        bOutOfLoop = False
                    End If
                    If Val(sMonth) = 4 Or Val(sMonth) = 6 Or Val(sMonth) = 9 Or Val(sMonth) = 11 Then
                        If Val(sDay) > 30 Then
                            MsgBox("Feil i dag - Pr�v igjen!", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
                            bOutOfLoop = False
                        End If
                    End If
                    If Val(sMonth) = 2 Then
                        If Val(sDay) > 29 Then
                            MsgBox("Feil i dag - Pr�v igjen!", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
                            bOutOfLoop = False
                        End If
                    End If
                Else
                    MsgBox("Feil i dato!" & vbCrLf & "Skal ha formen dd.mm.yyyy" & vbCrLf & "Husk punktum som skilletegn!", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical)
                    bOutOfLoop = False
                End If

            Loop

            If sResponse = "" Then
                bValidDateStated = False
            Else
                sLastDate = sYear & sMonth & sDay
                bValidDateStated = True
            End If

            If bValidDateStated Then
                oExportBabelFiles = New vbBabel.BabelFiles
                oExportBabelFiles.VB_Profile = oProfile
                iOldFileSetupID = -1
                bFirstFileSetupIDFound = False
                bProceed = True
                If Not EmptyString(sLastDate) Then
                    'Set last valid valuedate to export in the BabelFiles object
                    oExportBabelFiles.MATCH_LastValidBookDate = sLastDate
                    'oExportBabelFiles.MATCH_RemoveAll - just for test, run in ImportExport
                End If
                If oExportBabelFiles.MATCH_RetrieveData(False, 1, False, False) Then

                    'Check which Filesetup ID that are used
                    If oExportBabelFiles.Count > 0 Then
                        For Each oLocalBabelFile In oExportBabelFiles
                            If Not oLocalBabelFile.VB_FileSetupID = iOldFileSetupID Then
                                If iOldFileSetupID <> -1 Then
                                    If bFirstFileSetupIDFound Then
                                        MsgBox(LRS(17515), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(17514))
                                        'MsgBox "The payments to be exported originates from two different profiles. Contact Your dealer.", vbCritical + vbOKOnly, LRS(17508)
                                        bProceed = False
                                        Exit For
                                    Else
                                        iOldFileSetupID = oLocalBabelFile.VB_FileSetupID
                                        bFirstFileSetupIDFound = True
                                    End If
                                Else
                                    iOldFileSetupID = oLocalBabelFile.VB_FileSetupID
                                    bFirstFileSetupIDFound = True
                                End If
                            End If
                        Next oLocalBabelFile
                    Else
                        'There are no matched payments to export!
                        MsgBox("Det finnes ingen poster � eksportere.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, LRS(17508))
                        bProceed = False
                    End If


                    'Remove all transactions with oPayment.DATE_Value >
                    If Not TreatFilter(oExportBabelFiles, iOldFileSetupID, False, "DATE_BOOK>", sLastDate) And bProceed Then
                        MsgBox("En uventet feil oppstod under fjerning av poster som ikke skal ekporteres." & vbCrLf & vbCrLf & "Rutinen avbrytes, ingen fil ble eksportert.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Feil under uthenting av data!")
                    Else
                        'Removed next IF 13.12.2006. Can't reindex the collection because of the Archive DB
                        'If oExportBabelFiles.MATCH_ReindexCollection Then

                        'New IF 13.12.2006
                        If oExportBabelFiles.Count > 0 Then

                            ''                    If oExportBabelFiles.Count > 0 Then
                            ''                        For Each oLocalBabelFile In oExportBabelFiles
                            ''                            If Not oLocalBabelFile.VB_FileSetupID = iOldFileSetupID Then
                            ''                                If iOldFileSetupID <> -1 Then
                            ''                                    If bFirstFileSetupIDFound Then
                            ''                                        MsgBox LRS(17515), vbCritical + vbOKOnly, LRS(17514)
                            ''                                        'MsgBox "The payments to be exported originates from two different profiles. Contact Your dealer.", vbCritical + vbOKOnly, LRS(17508)
                            ''                                        bProceed = False
                            ''                                        Exit For
                            ''                                    Else
                            ''                                        iOldFileSetupID = oLocalBabelFile.VB_FileSetupID
                            ''                                        bFirstFileSetupIDFound = True
                            ''                                    End If
                            ''                                Else
                            ''                                    iOldFileSetupID = oLocalBabelFile.VB_FileSetupID
                            ''                                    bFirstFileSetupIDFound = True
                            ''                                End If
                            ''                            End If
                            ''                        Next
                            ''                    Else
                            ''                        'There are no matched payments to export!
                            ''                        MsgBox "Det finnes ingen poster � eksportere.", vbInformation + vbOKOnly, LRS(17508)
                            ''                        bProceed = False
                            ''                    End If
                            If bProceed Then
                                For Each oFilesetup In oProfile.FileSetups
                                    If oFilesetup.FileSetup_ID = iOldFileSetupID Then
                                        ReDim Preserve aSelectedFilenames(0)
                                        aSelectedFilenames(0) = oFilesetup.ShortName
                                    End If
                                Next oFilesetup
                                bImportExportOK = ImportExport(aSelectedFilenames, oProfile, 1, False, True, , , , , Me, "SISMOMND")
                            End If
                            'Else
                            '    MsgBox oExportBabelFiles.MATCH_ErrorText, vbCritical + vbOKOnly, LRS(17508)
                            'End If
                        Else
                            'There are no matched payments to export!
                            MsgBox("Det finnes ingen poster � eksportere.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, LRS(17508))
                            bProceed = False
                        End If
                    End If
                Else
                    MsgBox("En uventet feil oppstod under uthenting av data." & vbCrLf & oExportBabelFiles.MATCH_ErrorText, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, LRS(17508))
                End If
            End If 'If bValidDateStated Then

        End If 'If Not Array_IsEmpty(aLocked) Then

    End Sub
    '  Private Sub Toolbar_BackupFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Toolbar_BackupFiles.Click
    ' TODO REMOVE
    ' for some reason, this one has it's own Click method
    '     BackupFiles_Click(BackupFiles, New System.EventArgs())
    'End Sub
    Private Sub Toolbar1_ButtonClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Toolbar_SelectDatabase.Click, _Toolbar1_Button2.Click, Toolbar_Convert.Click, _Toolbar1_Button4.Click, Toolbar_ViewDetails.Click, Toolbar_Clients.Click, _Toolbar1_Button10.Click, Toolbar_SupportMail.Click, Toolbar_Phone.Click, _Toolbar1_Button13.Click, Toolbar_Exit.Click, Toolbar_BackupFiles.Click, Toolbar_SupportTools.Click
        Dim Button As System.Windows.Forms.ToolStripItem = CType(eventSender, System.Windows.Forms.ToolStripItem)
        Dim bx As Boolean
        Dim dlg As OpenFileDialog

        On Error Resume Next
        Select Case Button.Name
            Case "Toolbar_SelectDatabase"
                Me.SelectDatabase_Click(SelectDatabase, New System.EventArgs())

            Case "Toolbar_ViewDetails"
                Me.View_Click(View, New System.EventArgs())

            Case "Toolbar_Convert"
                Convert_Click(Convert, New System.EventArgs())

            Case "Toolbar_BrowseBackupfiles"
                BackupFiles_Click(BackupFiles, New System.EventArgs())

            Case "Toolbar_BackupFiles"
                BackupFiles_Click(BackupFiles, New System.EventArgs())

            Case "Toolbar_Clients"
                cmdClients_Click(ClientSize, New System.EventArgs())

            Case "Toolbar_SupportMail"
                HelpSupportmail_Click(HelpSupportmail, New System.EventArgs())

            Case "Toolbar_Phone"
                HelpSupportPhone_Click(HelpSupportPhone, New System.EventArgs())

            Case "Toolbar_Exit"
                Me.Close()

            Case "Toolbar_SupportTools"
                _mnuSupportFIleAnalyzis_Click(Nothing, New System.EventArgs())

        End Select
    End Sub

    Public Sub Convert_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Convert.Click
        'Dim aFilesArray(,) As String
        ' ------------------------------------
        ' Choose a file
        ' Import
        ' Ask for output-format and name
        ' Export
        ' ------------------------------------
        'Dim cInFormat As String
        Dim lContinue As Boolean
        Dim oFormats As BabelSetup.Formats
        Dim aFormats(,) As String
        Dim i As Short
        Dim sExportFilename As String
        Dim iExportFormat As Short
        Dim eExportFormat As vbBabel.BabelFiles.FileType
        Dim oBabelExport As vbBabel.BabelExport
        Dim bPaymentOut As Boolean
        Dim oLocalprofile As vbBabel.Profile
        On Error GoTo ErrorHandling

        lContinue = PickAFile()

        If lContinue Then

            oFormats = New BabelSetup.Formats ' Class in Setup which returns possible formats
            bPaymentOut = DetectFormatInOut(aFilesArray(0, i))
            aFormats = oFormats.ReturnFormats(bPaymentOut) 'true if outgoing payments
            ' Ask for exportfilename and exportfiletype
            frmExportInfo.Text = LRS(60034) ' "Eksportinformasjon"
            frmExportInfo.lblExportformat.Text = LRS(60036) '"Eksportformat"
            frmExportInfo.lblFilename.Text = LRS(60035) '"Navn p� eksportfil
            frmExportInfo.cmdCancel.Text = LRS(55006) '"&Avbryt"
            ' fill listbox:
            For i = 0 To UBound(aFormats, 2)
                frmExportInfo.lstFormats.Items.Add((aFormats(0, i)))
            Next i
            frmExportInfo.ShowDialog()


            If frmExportInfo.Status = True Then
                sExportFilename = frmExportInfo.txtFilename.Text
                iExportFormat = aFormats(1, frmExportInfo.lstFormats.SelectedIndex)
                eExportFormat = aFormats(14, frmExportInfo.lstFormats.SelectedIndex) 'enum Filetype
            Else
                lContinue = False
            End If
            frmExportInfo.Close()
            'FIX: This check has been changed, make a new one.
            'lContinue = CheckFilename(sExportFilename, True, True)

        Else
            lContinue = False
        End If

        If lContinue Then

            ProgressStart()
            ProgressText((LRS(60003)))
            ProgressPos((5))

            oLocalprofile = New vbBabel.Profile
            ' Import file
            'UPGRADE_WARNING: Couldn't resolve default property of object aFilesArray(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            oBabelFiles = ImportBabel(aFilesArray, oLocalprofile, False, False, Nothing)

            ProgressText((LRS(60004)))
            ProgressPos((50))
            Pause((0.5))


            ' Export file:
            ProgressText((LRS(60030))) '"Eksporterer filer")
            ProgressPos((60))

            oBabelExport = New vbBabel.BabelExport
            oBabelExport.BabelFiles = oBabelFiles

            oBabelExport.VB_Format_ID = 0 ' Tricks vbbabel to export all payments
            oBabelExport.ExportFormat = eExportFormat
            oBabelExport.VB_MultiFiles = False
            oBabelExport.FILE_Name = sExportFilename
            oBabelExport.VB_Version = GetVersion()

            oBabelExport.Export()
            oBabelExport = Nothing
            oBabelFiles = Nothing

            ProgressText((LRS(60030))) '"Eksporterer filer")
            ProgressPos((100))
            Pause((0.5))
            ProgressText((LRS(60056))) '"BabelBankkj�ring vellykket
            Pause((1))

        End If
        ProgressStop()
        Exit Sub

ErrorHandling:
        ProgressStop()
        ErrorHandling(Err, "", "BabelBank")
        'UPGRADE_WARNING: Couldn't resolve default property of object aFilesArray. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        SupportMail(aFilesArray, Err.Description)

    End Sub
    Public Sub mnuDecrypt_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuDecrypt.Click
        ' ------------------------------------
        ' Choose a file
        ' Import
        ' Decrypt
        ' Export - with original name, prefixed by Decrypted, in original folder.
        ' SO FAR - 30.12.2020 - for SecureEnvelope files only
        ' ------------------------------------
        'Dim cInFormat As String
        Dim bContinue As Boolean
        Dim oFormats As BabelSetup.Formats
        Dim aFormats(,) As String
        Dim i As Short
        Dim sExportFilename As String
        Dim iExportFormat As Short
        Dim eExportFormat As vbBabel.BabelFiles.FileType
        Dim oBabelExport As vbBabel.BabelExport
        Dim bPaymentOut As Boolean
        Dim oLocalprofile As vbBabel.Profile
        Dim sTmp As String = ""
        Dim sFolder As String = ""

        On Error GoTo ErrorHandling

        bContinue = PickAFile()
        eExportFormat = aFilesArray(0, 0)

        If Not (eExportFormat = vbBabel.BabelFiles.FileType.Pain001 Or eExportFormat = vbBabel.BabelFiles.FileType.Pain002 Or eExportFormat = vbBabel.BabelFiles.FileType.Pain002_Rejected Or eExportFormat = vbBabel.BabelFiles.FileType.Camt054 Or eExportFormat = vbBabel.BabelFiles.FileType.Camt054_Outgoing) Then
            bContinue = False
        End If

        If bContinue Then
            sTmp = aFilesArray(1, 0)
            sFolder = System.IO.Path.GetDirectoryName(sTmp)
            sTmp = System.IO.Path.GetFileName(sTmp)
            sExportFilename = sFolder & "\" & "Decrypted_" & sTmp

        Else
            bContinue = False
        End If

        If bContinue Then
            ' import as raw string

            ExportDecryptedFile(aFilesArray(1, 0), sExportFilename)


        End If
        Exit Sub

ErrorHandling:
        ProgressStop()
        ErrorHandling(Err, "", "BabelBank")
        SupportMail(aFilesArray, Err.Description)

    End Sub
    'Private Sub ExportDecryptedFile(ByVal sFilenameIn As String, ByVal sFilenameOut As String)
    '    Dim oFs As Scripting.FileSystemObject   ' moved here 05.12.2012
    '    Dim oFile As Scripting.TextStream 'FileoProfile object
    '    Dim stmp As String = ""
    '    Dim sTmp2 As String = ""
    '    Dim bSecureEnvelopeInUse As Boolean = False
    '    Dim bt64 As Byte()
    '    Dim bGZIP As Boolean = False
    '    Dim iPos As Long
    '    Dim iPos2 As Long
    '    Dim bPain001 As Boolean = False

    '    oFs = New Scripting.FileSystemObject
    '    oFile = oFs.OpenTextFile(sFilenameIn, Scripting.IOMode.ForReading)
    '    sTmp = ""
    '    sTmp = oFile.Read(500000)  ' Is 500000 encough???
    '    sTmp2 = sTmp.Substring(0, 500)  ' to check compression
    '    ' close file
    '    oFile.Close()
    '    oFile = Nothing
    '    ' ------------------------------------------------------------------

    '    iPos = InStr(stmp, "NDCAPXMLI", CompareMethod.Text)   ' Pain.001 with SecureEnvelope 
    '    If iPos > 0 Then
    '        bPain001 = True
    '    Else
    '        iPos = InStr(stmp, "NDCAPXMLO", CompareMethod.Text)   ' Pain.002 with SecureEnvelope 
    '        If iPos <= 1 Then
    '            iPos = InStr(stmp, "NDCAPXML", CompareMethod.Text)
    '            ' Also test for NDCAPXMLD54O
    '            If iPos <= 1 Then
    '                If iPos < 1 Then
    '                    iPos = InStr(stmp, "NDCAPXMLD54O", CompareMethod.Text)
    '                End If
    '            End If
    '        End If
    '    End If

    '    iPos2 = InStr(stmp, "<Content>", CompareMethod.Text)
    '    If iPos > 0 And iPos2 > 0 Then
    '        ' this file is with SecureEnvelope
    '        bSecureEnvelopeInUse = True
    '        ' find the actual filecontent
    '        iPos = iPos2 + 9
    '        ' find the end of content
    '        iPos2 = InStr(stmp, "</Content>", CompareMethod.Text) - 1
    '        ' find base64 string;
    '        'sTmp = Mid(sTmp, iPos, iPos2 - iPos)
    '        stmp = Mid(stmp, iPos, iPos2 - iPos + 1)
    '        bt64 = System.Convert.FromBase64String(stmp)
    '    End If

    '    ' --------------------------------------------------
    '    If bSecureEnvelopeInUse Then
    '        iPos = InStr(sTmp2, "<Compression>", CompareMethod.Text)   ' Pain.001 with SecureEnvelope
    '        If iPos > 0 Then
    '            iPos = iPos + 13
    '            iPos2 = iPos + 5
    '            stmp = sTmp2.Substring(iPos - 1, iPos2 - iPos - 1).ToUpper
    '        Else
    '            ' Pain.002/Camt.054
    '            iPos = InStr(sTmp2, "<Compressed>", CompareMethod.Text) ' Pain.002 or Camt.054
    '            iPos = iPos + 12
    '            iPos2 = iPos + 5
    '            stmp = sTmp2.Substring(iPos - 1, iPos2 - iPos - 1).ToUpper
    '        End If

    '        If stmp = "TRUE" Then
    '            ' Compressed with GZIP. We take the assumption that if compression = true, then it's GZIP
    '            bGZIP = True
    '        Else
    '            bGZIP = False
    '        End If
    '        If bGZIP Then
    '            bt64 = GZIPDeCompress(bt64)
    '            ' unzip via string
    '            'bt64 = System.Text.Encoding.Unicode.GetBytes(GZ_UnZip(System.Text.Encoding.Unicode.GetString(bt64)))
    '        End If
    '        ' read from base64 string
    '        stmp = System.Text.Encoding.ASCII.GetString(bt64)
    '        If bPain001 Then
    '            ' for Pain.001 files we sometimes have UTF-8 charset, and we then have 3 characters in front which creates problems
    '            If VB.Left(stmp, 1) <> "<" Then
    '                stmp = Mid(stmp, 4)
    '            End If
    '        End If



    '    Else
    '        ' bare eksporter det vi har, som ikke er kryptert

    '    End If

    '    ' Export to file;
    '    oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForWriting, True)
    '    oFile.Write(stmp)
    '    oFile.Close()
    '    oFile = Nothing
    '    oFs = Nothing
    'End Sub
    Public Sub Exit_Renamed_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Exit_Renamed.Click
        ' Exit Babelbank
        Me.Close()
    End Sub
    Public Sub mnulstSendProfiles_SupportMail_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnulstSendProfiles_SupportMail.Click

        Dim aFilesArray(,) As String
        Dim sFilePath As String = ""
        'Dim sFilename As String = ""
        'Dim sDirName As String = ""
        Dim i As Integer = 0
        Dim iStart As Integer = 0
        Dim iLen As Integer = 0
        Dim files() As String


        ' Find files from this profile;
        ' Find inpart of Filesetup;
        For Each oFilesetup In oProfile.FileSetups
            If oFilesetup.ShortName = sProfileName Then
                Exit For
            End If
        Next


        ' Importfiles;
        ' ------------
        For i = 1 To 3
            If i = 1 Then
                ' FileNameIn1
                sFilePath = oFilesetup.FileNameIn1
            ElseIf i = 2 Then
                ' FileNameIn2
                sFilePath = oFilesetup.FileNameIn2
            ElseIf i = 3 Then
                ' FileNameIn3
                sFilePath = oFilesetup.FileNameIn3
            End If

            If sFilePath <> "" Then
                ' if using % % for date/time, then remove this 
                If sFilePath.IndexOf("%") > 0 Then
                    iStart = sFilePath.IndexOf("%", 0)
                    Dim iStop As Integer = sFilePath.IndexOf("%", iStart + 1)
                    sFilePath = sFilePath.Substring(0, iStart - 1) & "*" & sFilePath.Substring(iStop + 1)
                End If

                'finnes filbanen?
                If Dir(sFilePath) <> "" Then

                    files = IO.Directory.GetFiles(System.IO.Path.GetDirectoryName(sFilePath), System.IO.Path.GetFileName(sFilePath))
                    For Each file As String In files
                        ' Do work, example
                        'Dim text As String = IO.File.ReadAllText(file)
                        Console.WriteLine(file)
                        If aFilesArray Is Nothing Then
                            ReDim aFilesArray(12, 0)
                        Else
                            ReDim Preserve aFilesArray(12, aFilesArray.GetUpperBound(1) + 1)
                        End If

                        aFilesArray(0, aFilesArray.GetUpperBound(1)) = ""
                        aFilesArray(1, aFilesArray.GetUpperBound(1)) = file
                    Next
                End If
            End If
        Next i

        ' change to filesetup out
        oFilesetup = oProfile.FileSetups(oFilesetup.FileSetupOut)
        ' Exportfiles;
        ' ------------
        For i = 1 To 3
            If i = 1 Then
                ' FileNameout1
                sFilePath = oFilesetup.FileNameOut1
            ElseIf i = 2 Then
                ' FileNameOut2
                sFilePath = oFilesetup.FileNameOut2
            ElseIf i = 3 Then
                ' FileNameOut3
                sFilePath = oFilesetup.FileNameOut3
            End If

            If sFilePath <> "" Then
                ' if using % % for date/time, then remove this 
                If sFilePath.IndexOf("%") > 0 Then
                    iStart = sFilePath.IndexOf("%", 0)
                    Dim iStop As Integer = sFilePath.IndexOf("%", iStart + 1)
                    sFilePath = sFilePath.Substring(0, iStart - 1) & "*" & sFilePath.Substring(iStop + 1)
                End If

                'finnes filbanen?
                If Dir(sFilePath) <> "" Then
                    files = IO.Directory.GetFiles(System.IO.Path.GetDirectoryName(sFilePath), System.IO.Path.GetFileName(sFilePath))
                    For Each file As String In files
                        ' Do work, example
                        'Dim text As String = IO.File.ReadAllText(file)
                        Console.WriteLine(file)
                        If aFilesArray Is Nothing Then
                            ReDim aFilesArray(12, 0)
                        Else
                            ReDim Preserve aFilesArray(12, aFilesArray.GetUpperBound(1) + 1)
                        End If

                        aFilesArray(0, aFilesArray.GetUpperBound(1)) = ""
                        aFilesArray(1, aFilesArray.GetUpperBound(1)) = file
                    Next
                End If
            End If
        Next i


        SendSupportMail(aFilesArray, LRS(62100) & " " & sProfileName)   ' "Support for profil")

    End Sub


    Sub frmStart_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        'Dim oLicense As New vbBabel.License
        Dim ctl As System.Windows.Forms.Control
        Dim item As New ToolStripMenuItem

        'Me.lblLicensedTo.Text = LRS(62013) & ": " & oLicense.LicName
        'Me.Text = "BabelBank " & LRS(62013) & ": " & oLicense.LicName
        'oLicense = Nothing

        Me.lstSendProfiles.ContextMenuStrip = mnuRightClick
        mnuRightClick.Items.Clear()
        item = mnuRightClick.Items.Add(LRS(62010))    '"Send supportmail")
        AddHandler item.Click, AddressOf mnulstSendProfiles_SupportMail_Click 'Name of the event (search for this function)

        Show()

        If bDEBUG Then
            MsgBox("I frmStart, Form_Load, top", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "DEBUG")
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        LoadProfile()  ' 06.07.2017 - moved here from below

        If bDEBUG Then
            MsgBox("I frmStart, Form_Load, F�R FormLRSCaptions", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "DEBUG")
        End If

        ' Translate Strings (menu, labels, etc)
        FormLRSCaptions(Me)

        If bDEBUG Then
            MsgBox("I frmStart, Form_Load, etter FormLRSCaptions", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "DEBUG")
        End If

        ' Make vb.net look:
        FormvbStyle(Me, "dollar.jpg", 1050)
        If bDEBUG Then
            MsgBox("I frmStart, Form_Load, etter Formvbstyle", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "DEBUG")
        End If
        'Set oProfile = New vbbabel.profile")
        'FIX: Hardcoded companyno. = 1
        'oProfile.Load(1)

        'oProfile.Save (1)

        'Dim b As Boolean
        'b = FillListboxer(oProfilerrr)
        'FillListboxes

        '    '**************************************************
        '    ' 11.11.2016 - IMPORTANT CHANGE -
        '    ' Timer1 is removed !!!!!
        '    ' Instead, call LoadProfile from frmStartLoad
        '    '**************************************************
        'LoadProfile()  ' 06.07.2017 Commented out
        'oProfile = Nothing

    End Sub
    'Private Sub Timer1_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Timer1.Tick
    '    '**************************************************
    '    ' 11.11.2016 - IMPORTANT CHANGE -
    '    ' Timer1 is removed !!!!!
    '    ' Instead, call LoadProfile from frmStartLoad
    '    '**************************************************
    '    Timer1.Enabled = False
    '    LoadProfile()

    'End Sub
    Public WithEvents mnuSetupCustom1 As New System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuSetupCustom2 As New System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuSetupCustom3 As New System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuSetupCustom4 As New System.Windows.Forms.ToolStripMenuItem
    Private Sub LoadProfile()
        Dim oLicense As vbBabel.License

        On Error GoTo ERRLoadProfile

        'XNET - 04.06.2012 - CRITICAL CHANGE - When we start Manuel matching directly with a parameter this was set twice

        ' Added the if to test if the Profile is already ran
        If oProfile Is Nothing Then
            'FIX: Hardcoded companyno. = 1
            oProfile = New vbBabel.Profile
            oProfile.Load(1) 'KOTOKO - HER M� NOE GJ�RES Profile er noe n�r den b�r loades her.

            '''' test BackupDatabase oProfile.BackupPath

            ' New 03.06.03
            ' Special for DnB TBI WEB
            ' Check if file with Accountnumbers and SWIFT-codes is present;
            ' CheckDnBTBIW_AccountsFile oProfile


            FillListboxes()
            ' New 16.08.02 JanP
            ' Check for any Cutomized menuchoices in Setup-menu
            If Len(Trim(oProfile.Custom1Text)) > 0 Then
                'mnuSetupCustomArray(0).Visible = True ' Separator
                Me.Setup.DropDownItems.Add("-")

                ' Add menutitem into CustomArray
                mnuSetupCustom1.Name = "mnuSetupCustom1"
                mnuSetupCustom1.Text = oProfile.Custom1Text
                Me.Setup.DropDownItems.Add(mnuSetupCustom1)
            End If

            If Len(Trim(oProfile.Custom2Text)) > 0 Then
                ' Add menutitem into CustomArray
                mnuSetupCustom2.Name = "mnuSetupCustom2"
                mnuSetupCustom2.Text = oProfile.Custom2Text
                Me.Setup.DropDownItems.Add(mnuSetupCustom2)

                '    mnuSetupCustomArray.Load(2)
                '    mnuSetupCustomArray(2).Visible = True
                '    mnuSetupCustomArray(2).Text = oProfile.Custom2Text
            End If
            If Len(Trim(oProfile.Custom3Text)) > 0 Then
                mnuSetupCustom3.Name = "mnuSetupCustom3"
                mnuSetupCustom3.Text = oProfile.Custom3Text
                Me.Setup.DropDownItems.Add(mnuSetupCustom3)

                '    mnuSetupCustomArray.Load(3)
                '    mnuSetupCustomArray(3).Visible = True
                '    mnuSetupCustomArray(3).Text = oProfile.Custom3Text
            End If
            If Len(Trim(oProfile.Custom4Text)) > 0 Then
                mnuSetupCustom4.Name = "mnuSetupCustom4"
                mnuSetupCustom4.Text = oProfile.Custom4Text
                Me.Setup.DropDownItems.Add(mnuSetupCustom4)

                '    mnuSetupCustomArray.Load(4)
                '    mnuSetupCustomArray(4).Visible = True
                '    mnuSetupCustomArray(4).Text = oProfile.Custom4Text
            End If

            If Not oProfile.ManMatchFromERP Then
                Me.ManualMatching.Enabled = False
                'Me.ExportPayments.Enabled = False
                Me.AutomaticMatching.Enabled = False
            End If

            Me.PrintOmittedPayments.Enabled = False


            ' Display a DnBTBI-specific menu if TBI-license
            oLicense = New vbBabel.License
            If InStr(oLicense.LicServicesAvailable, "T") Or InStr(oLicense.LicServicesAvailable, "DNBX") Then

                ' Show button FI-Profile
                Me.cmdNewFIProfile.Visible = True

                ' Add special DnBNOR TBI-menu:
                'Load mnuDnBNORTBIArray(0)
                mnuDnBNORTBI.Visible = True

                '    Load mnuDnBNORTBIArray(0)
                mnuDnBNORTBIArray(0).Visible = True
                mnuDnBNORTBIArray(0).Text = LRS(51101) '"Show incoming advices"

                mnuDnBNORTBIArray.Load(1)
                mnuDnBNORTBIArray(1).Visible = True
                mnuDnBNORTBIArray(1).Text = LRS(51102) '"Import BEC-files"

            ElseIf InStr(oLicense.LicServicesAvailable, "SEB") Then

                ' Add special SEB ISO20022-menu
                Me.mnuSEBISO20022.Visible = True
                Me.mnuSEBISO20022_MiscSetup.Visible = True

            Else
                ' Do not show button FI-Profile
                Me.cmdNewFIProfile.Visible = False
            End If

            If (InStr(oLicense.LicServicesAvailable, "S") And Not InStr(oLicense.LicServicesAvailable, "SEB")) Or (Not RunTime()) Then
                ' "S" = Supportool for paymentfiles (in licensefile)
                ' Add special supporttoolsmenu
                Me.mnuSupportTool.Visible = True
                Me._mnuSupportFIleAnalyzis.Visible = True
                ' show icon in toolbar
                Me.Toolbar_SupportTools.Visible = True

            End If


            '14.12.2020
            If Not oProfile Is Nothing Then
                If oProfile.Custom1Text.Trim.ToUpper = "SAFE MODE" Then
                    If oProfile.Custom1Value.Trim.ToUpper = "AKTIV" Or oProfile.Custom1Value.Trim.ToUpper = "ACTIVE" Then
                        bLockBabelBank = False
                        bDisableBabelBank = False
                        'Me.chkDisableAdminMenus.Text = "Deaktiv�r AML"
                        'Me.chkLockBabelBank.Text = "Deaktiv�r Rett betaler"
                    Else
                        bDisableBabelBank = oProfile.DisableAdminMenus
                    End If
                Else
                    bDisableBabelBank = oProfile.DisableAdminMenus
                End If
            End If

            ' Special menus, test on CompanyName in Companytable
            'Select Case UCase(oProfile.CompanyName)
            'Case "LINDORFF ASA"
            ' Changed 03.08.2007 at Haglebu. Now one can check chkDisableAdminMenus in Companysetup (Firmaopplysninger)

            'New 07.11.2008, first IF and removed the LINDORFF ASA part, changed 13.12.2008
            If oProfile.LockBabelBank And bLockBabelBank Then
                ' Disable menuitems
                Me.lstReturnprofiles.Enabled = False
                Me.lstSendProfiles.Enabled = False
                Me.RenameFiles.Enabled = False
                Me.mnuImportFiles.Enabled = False
                Me.AutomaticMatching.Enabled = False
                Me.ManualMatching.Enabled = False
                Me.PrintOmittedPayments.Enabled = False
                Me.ExportPayments.Enabled = False
                Me.MatchingSQL.Enabled = False
                Me.MatchingClients.Enabled = False
                Me.MatchingManualSetup.Enabled = False
                Me.ArchiveSetup.Enabled = False
                Me.CleanUpDatabase.Enabled = False
                Me.MatchUnlock.Enabled = False
                Me.mnuSpecial.Enabled = False
                Me.SetupClients.Enabled = False
                Me.SetupNewProfile.Enabled = False
                Me.SetupChangeProfile.Enabled = False
                Me.SetupDeleteProfile.Enabled = False
                Me.SetupCompanyInfo.Enabled = False 'Added 19.12.2008
                Me.SetupSequence.Enabled = False
                Me.SetupClientSplit.Enabled = False
                Me.SetupDocumentation.Enabled = False

                ' Disable command-buttons
                ' Needs this one Me.cmdChangeProfile.Enabled = False
                Me.cmdClients.Enabled = False
                Me.cmdDeleteProfile.Enabled = False
                Me.cmdNewProfile.Enabled = False
                Me.cmdRun.Enabled = False
                Me.cmdChangeProfile.Enabled = False

                MsgBox(LRS(15021), MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, LRS(15022))
                'MsgBox "BabelBank vedlikeholdes, og du kan ikke benyttes BabelBank til � postere innbetalinger." & vbCrLf & "Kontakt din driftsansvarlige hvis du har sp�rsm�l.", vbOKOnly + vbExclamation, "BabelBank er l�st"

                '14.12.2020 - Added bDisableBabelBank because of SG Finans
            ElseIf oProfile.DisableAdminMenus And bDisableBabelBank Then  'Or UCase(oProfile.CompanyName) = "LINDORFF ASA" Then
                ' Disable menuitems
                mnuImportFiles.Enabled = False
                AutomaticMatching.Enabled = False
                PrintOmittedPayments.Enabled = False
                'New 06.10.2008
                If InStr(1, UCase(oProfile.CompanyName), "SG FINANS", CompareMethod.Text) > 0 Then
                    ExportPayments.Enabled = True
                ElseIf InStr(1, UCase(oProfile.CompanyName), "NORGES FORSKNINGSR�D", CompareMethod.Text) > 0 Then
                    Me.mnuImportFiles.Enabled = True
                    ExportPayments.Enabled = True
                    Me.ManualMatching.Enabled = True
                Else
                    ExportPayments.Enabled = False
                End If
                CleanUpDatabase.Enabled = False

                ' Disable command-buttons
                ' Needs this one Me.cmdChangeProfile.Enabled = False
                ' 16.11.2018 - disables ChangeProfile again - after discussions KI and JPS at Lindorff
                Me.cmdChangeProfile.Enabled = False
                Me.cmdClients.Enabled = False
                Me.cmdDeleteProfile.Enabled = False
                Me.cmdNewProfile.Enabled = False
                Me.cmdRun.Enabled = False
                'End Select
            End If

            'New 28.04.2010 - May be disabled for .NET
            If InStr(1, UCase(oProfile.CompanyName), "SANTANDER", CompareMethod.Text) > 0 Then
                Me.SetupDocumentation.Enabled = True
            End If

            Me.Cursor = System.Windows.Forms.Cursors.Default
            ' New 16.02.05
            'UPGRADE_NOTE: Object oLicense may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oLicense = Nothing

            ' 13.03.2012
            '###################
            ' Siden vi har en oProfile.Load i starten av denne sub, m� vi vel lukke her ???
            '----

            'oProfile.CloseConnection()
            'oProfile = Nothing
            '##########################
        End If

        Exit Sub

ERRLoadProfile:
        If Not oProfile Is Nothing Then
            oProfile.CloseConnection()
            oProfile = Nothing
        End If
        If Not oLicense Is Nothing Then
            oLicense = Nothing
        End If

        Err.Raise(Err.Number, "LoadProfile", Err.Description)

    End Sub

    Sub FillListboxes()
        Dim iCountFilenames As Short
        'Dim bSlett As Boolean

        lstSendProfiles.Items.Clear()
        lstReturnprofiles.Items.Clear()

        'We don't need the array aFilenameData

        iCountFilenames = 0
        For Each oFilesetup In oProfile.FileSetups
            ReDim Preserve aFilenameData(iCountFilenames)
            If oFilesetup.FromAccountingSystem = True And oFilesetup.FileSetup_ID <> oFilesetup.FileSetupOut Then
                ' Don't show outpart of profiles which has only returnattributes (no sendprofile)
                If Not (oFilesetup.FileNameIn1 = "" And oFilesetup.FileNameIn2 = "" And oFilesetup.FileNameIn3 = "") Then
                    'lstSendProfiles.Items.Add(New VB6.ListBoxItem(oFilesetup.ShortName, oFilesetup.FileSetup_ID))
                    ' 12.07.2019
                    lstSendProfiles.Items.Add(New _MyListBoxItem(oFilesetup.ShortName, oFilesetup.FileSetup_ID))
                    aFilenameData(iCountFilenames) = oFilesetup.ShortName
                End If
            Else
                ' Don't show outpart of profiles which has only sendattributes (no returnprofile)
                If Not (oFilesetup.FileNameIn1 = "" And oFilesetup.FileNameIn2 = "" And oFilesetup.FileNameIn3 = "") Then
                    'lstReturnprofiles.Items.Add(New VB6.ListBoxItem(oFilesetup.ShortName, oFilesetup.FileSetup_ID))
                    lstReturnprofiles.Items.Add(New _MyListBoxItem(oFilesetup.ShortName, oFilesetup.FileSetup_ID))
                    aFilenameData(iCountFilenames) = oFilesetup.ShortName
                End If
            End If
            iCountFilenames = iCountFilenames + 1
        Next oFilesetup

        lstSendProfiles.Refresh()
        lstReturnprofiles.Refresh()


        'Uncomment the following code for test of the function
        ' FindFiles(). The test should be run on Kjell's PC
        'Dim slettarray() As String
        ''
        '    slettarray() = FindFiles("C:\Test\Klient ���.dat")
        '    slettarray() = FindFiles("C:\Test\Kli.�")
        '    slettarray() = FindFiles("C:\Kli����\Alle.dat")
        '    slettarray() = FindFiles("\\jan-petter\janpetter\test\kli���.dat")
        '    slettarray() = FindFiles("C:\���\Dette er klient")
        '    slettarray() = FindFiles("C:\Test\Kli.*")
        '' FIX: The following will return all files according to C:\Test\Kli*.*
        '    slettarray() = FindFiles("C:\Test\Kli*.���")


        'bSlett = True

    End Sub

    Private Sub lstSendProfiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSendProfiles.Click
        ' 23.01.2017
        ' enable Start-button, was disabled when user clicked it last time
        If Me.cmdRun.Enabled = False Then
            ' 16.11.2018 - do NOT turn it on if "Sl� av Admin-valg" er merket
            If oProfile.DisableAdminMenus = False Then
                Me.cmdRun.Enabled = True
            End If
        End If

    End Sub

    'UPGRADE_WARNING: Event lstSendProfiles.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub lstSendProfiles_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstSendProfiles.SelectedIndexChanged
        ' A click in listbox selects an item, and also deselects
        ' items in the other listbox
        Dim i As Short
        If bCut Then
            ' called from Click method in the other listbox - do nothing!
            Exit Sub
        End If

        bCut = True
        If lstSendProfiles.SelectedIndex > -1 Then
            ' Which item is selected?
            'iSelectedProfile = VB6.GetItemData(lstSendProfiles, lstSendProfiles.SelectedIndex)
            'sSelectedShortname = VB6.GetItemString(lstSendProfiles, lstSendProfiles.SelectedIndex)
            ' 12.07.2019
            iSelectedProfile = lstSendProfiles.Items(lstSendProfiles.SelectedIndex).itemdata
            sSelectedShortname = lstSendProfiles.Items(lstSendProfiles.SelectedIndex).itemstring
        End If


        ' Deselect in lstReturnProfiles
        For i = 0 To lstReturnprofiles.Items.Count - 1
            lstReturnprofiles.SetSelected(i, False)
        Next i

        bCut = False

    End Sub
    Private Sub lstSendProfiles_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lstSendProfiles.MouseDown
        ' Check rightclick - for supportmail
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' select with rightclick;
            If e.Button = MouseButtons.Right Then
                Dim n As Integer = Me.lstSendProfiles.IndexFromPoint(e.X, e.Y)
                If n <> Me.lstSendProfiles.NoMatches Then
                    Me.lstSendProfiles.SelectedIndex = n
                End If
            End If
            sProfileName = Me.lstSendProfiles.Text
            mnuRightClick.Show(MousePosition)
        End If
    End Sub
    Private Sub lstReturnprofiles_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lstReturnprofiles.MouseDown
        ' Check rightclick - for supportmail
        If e.Button = Windows.Forms.MouseButtons.Right Then
            ' select with rightclick;
            If e.Button = MouseButtons.Right Then
                Dim n As Integer = Me.lstReturnprofiles.IndexFromPoint(e.X, e.Y)
                If n <> Me.lstReturnprofiles.NoMatches Then
                    Me.lstReturnprofiles.SelectedIndex = n
                End If
            End If
            sProfileName = Me.lstReturnprofiles.Text
            mnuRightClick.Show(MousePosition)
        End If
    End Sub
    Private Sub lstReturnprofiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstReturnprofiles.Click
        ' 23.01.2017
        ' enable Start-button, was disabled when user clicked it last time
        If Me.cmdRun.Enabled = False Then
            ' 16.11.2018 - do NOT turn it on if "Sl� av Admin-valg" er merket
            If oProfile.DisableAdminMenus = False Then
                Me.cmdRun.Enabled = True
            End If
        End If
    End Sub
    'UPGRADE_WARNING: Event lstReturnProfiles.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub lstReturnProfiles_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstReturnprofiles.SelectedIndexChanged
        ' A click in listbox selects an item, and also deselects
        ' items in the other listbox
        Dim i As Short
        If bCut Then
            ' called from Click method in the other listbox - do nothing!
            Exit Sub
        End If

        bCut = True
        If lstReturnprofiles.SelectedIndex > -1 Then
            ' Which item is selected?
            'iSelectedProfile = VB6.GetItemData(lstReturnprofiles, lstReturnprofiles.SelectedIndex)
            'sSelectedShortname = VB6.GetItemString(lstReturnprofiles, lstReturnprofiles.SelectedIndex)
            ' 12.07.2019
            iSelectedProfile = lstReturnprofiles.Items(lstReturnprofiles.SelectedIndex).itemdata
            sSelectedShortname = lstReturnprofiles.Items(lstReturnprofiles.SelectedIndex).itemstring
        End If

        ' Deselect in lstSendProfiles
        For i = 0 To lstSendProfiles.Items.Count - 1
            lstSendProfiles.SetSelected(i, False)
        Next i

        bCut = False

    End Sub

    Private Sub lstSendProfiles_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstSendProfiles.DoubleClick
        Dim iCounter, iSelectedFilename As Short
        Dim aSelectedFilenames() As String
        'Dim iInFormat As Integer, sFilenameIn As String
        Dim bImportExportOK As Boolean
        iCounter = 0

        ' 16.11.2018 - do NOT run profile if "Sl� av Admin-valg" er merket
        If oProfile.DisableAdminMenus = False Then
            'Find the selected Shortname
            For iSelectedFilename = 0 To lstSendProfiles.Items.Count - 1
                If lstSendProfiles.GetSelected(iSelectedFilename) Then
                    ReDim Preserve aSelectedFilenames(iCounter)
                    'aSelectedFilenames(iCounter) = VB6.GetItemString(lstSendProfiles, lstSendProfiles.SelectedIndex)
                    ' 12.07.2019
                    aSelectedFilenames(iCounter) = lstSendProfiles.Items(lstSendProfiles.SelectedIndex).itemstring
                    'aSelectedFilenames(iCounter) = aFilenameData(iSelectedFilename)
                    iCounter = iCounter + 1
                    'Exit For
                End If
            Next iSelectedFilename

            bImportExportOK = ImportExport(aSelectedFilenames, oProfile, 1, True, True, , , , , Me)

        End If
     



    End Sub
    Private Sub lstReturnprofiles_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstReturnprofiles.DoubleClick

        Dim iCounter, iSelectedFilename As Short
        Dim aSelectedFilenames() As String
        Dim bImportExportOK As Boolean

        iCounter = 0
        ' 16.11.2018 - do NOT run profile if "Sl� av Admin-valg" er merket
        If oProfile.DisableAdminMenus = False Then

            'Find the selected Shortname
            For iSelectedFilename = 0 To lstReturnprofiles.Items.Count - 1
                If lstReturnprofiles.GetSelected(iSelectedFilename) Then
                    ReDim Preserve aSelectedFilenames(iCounter)
                    'aSelectedFilenames(iCounter) = VB6.GetItemString(lstReturnprofiles, lstReturnprofiles.SelectedIndex)
                    ' 12.07.2019
                    aSelectedFilenames(iCounter) = lstReturnprofiles.Items(lstReturnprofiles.SelectedIndex).itemstring
                    iCounter = iCounter + 1
                End If
            Next iSelectedFilename

            bImportExportOK = ImportExport(aSelectedFilenames, oProfile, 1, True, True, , , , , Me)
        End If
    End Sub

    Public Sub SupportMail(ByRef aF(,) As String, Optional ByVal sMsg As String = "")
        'If MsgBox("Vil du sende supportmail til Visual Banking AS, vedlagt de filer du behander ?", vbCritical + vbYesNo + vbQuestion, "Supportmail?") = vbYes Then
        If MsgBox(LRS(60070), MsgBoxStyle.YesNo + MsgBoxStyle.Question, LRS(60040)) = MsgBoxResult.Yes Then
            'HelpSupportmail_Click(HelpSupportmail, New System.EventArgs(), aF, sMsg)
            SendSupportMail(aFilesArray, sMsg)
        End If
    End Sub
    Public Sub HelpSupportmail_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs, Optional ByVal aFilesArray(,) As String = Nothing, Optional ByVal sMsg As String = "") Handles HelpSupportmail.Click

        If Not RunTime() Then  'DISKUTERNOTRUNTIME
            Dim frmSupportMail_New As frmSupportMail_New
            frmSupportMail_New = New frmSupportMail_New
            frmSupportMail_New.lblComments.Text = LRS(61001) ' Please describe the BabelBank problem
            frmSupportMail_New.lblName.Text = LRS(61002)   ' Name
            frmSupportMail_New.lblPhone.Text = LRS(61003)  ' Phone
            frmSupportMail_New.lblSendProfiles.Text = LRS(60001)  ' Send profiles
            frmSupportMail_New.lblPhone.Text = LRS(60002)  ' Receive profiles

            frmSupportMail_New.ShowDialog()

        Else
            SendSupportMail(aFilesArray, sMsg)
        End If

    End Sub
    Public Sub View_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles View.Click
        ' View on screen, batch level (with datagridview)
        Dim oBabelFiles As vbBabel.BabelFiles
        Dim lContinue As Boolean = False
        Dim oView As vbBabel.bbviewer

        Try

            'Dim oBabel As vbBabel.BabelFile
            'Dim oBatch As vbBabel.Batch
            'Dim nOCR As Double = 0
            'Dim nOther As Double = 0

            lContinue = PickAFile()

            If lContinue Then
                ProgressStart()
                ProgressText((LRS(62032)))
                ProgressPos((5))

                oBabelFiles = ImportBabel(aFilesArray, oProfile, False, False, Nothing)

                ProgressText((LRS(60004)))
                ProgressPos((50))
                Pause((0.5))

                'Remove this after 01.08.2020 - remove the function as well
                'If Not RunTime() Then
                '    If TreatSGFinansTemp(oBabelFiles) Then

                '    End If
                'End If

                ProgressText(LRS(60005))
                ProgressPos((60))

                ' Start spread
                ProgressPos((90))
                oView = New vbBabel.bbViewer
                oView.BabelFiles = oBabelFiles
                oView.Show()

                ProgressStop()
            End If

        Catch ex As vbBabel.Payment.PaymentException ' - Testing Try ... Catch
            ProgressStop()

            ExPaymentErrorHandling(ex, "BabelBank", , , , aFilesArray)
            '21.08.2018 - Moved the question regarding sending of supportmail to within the errorhandling in frmBB_Error.
            'SupportMail(aFilesArray, ex.Message)

        Catch ex As Exception

            ProgressStop()

            ErrorHandling(Err, Err.Description, "BabelBank")

            SupportMail(aFilesArray, Err.Description)


        End Try

    End Sub

    Public Function PickAFile() As Boolean
        Dim aFArray() As String
        Dim i As Short
        Dim eFormat As vbBabel.BabelFiles.FileType

        ' return filename(s) within an array
        If Array_IsEmpty(aFilesArray) Then
            aFArray = Filenamepicker(Me.dlgFileNameOpen, True)
            'aFArray = FilenamePicker((Me.dlgFileName))
        Else
            ' give filename last used as parameter
            aFArray = Filenamepicker(Me.dlgFileNameOpen, True, , aFilesArray(1, 0))
            'aFArray = FilenamePicker((Me.dlgFileName), aFilesArray(1, 0))
        End If
        If Not Array_IsEmpty(aFArray) Then
            bFilesPicked = True
            ' Put into aFilesArray, two dimensional,
            ' each element: 0 : Importformat, (enmum as string)
            '               1 : Filename
            '               2 : empty here
            '               3 : Report to print, enum fixed 0 - NoPrint
            '               4 : Report to screen, fixed True
            '               5 : Not in use, set to 0 (FileNameInNumber)
            '               6 : ClientNo (not in use here)
            '               7 : Version
            '               8 : EDIFormat (true/false)



            For i = 0 To UBound(aFArray)
                ReDim Preserve aFilesArray(12, i)
                ' Check fileformat:
                aFilesArray(0, i) = Trim(Str(DetectFileFormat(aFArray(i))))
                aFilesArray(1, i) = aFArray(i)
                aFilesArray(2, i) = ""
                aFilesArray(3, i) = vbBabel.BabelFiles.PrintType.NoPrint
                aFilesArray(4, i) = vbBabel.BabelFiles.PrintType.PrintBatches
                aFilesArray(5, i) = 0
                aFilesArray(6, i) = "" 'DetectFormatInOut(aFilesArray(0, i))
                aFilesArray(7, i) = ""
                'Changed 21.04.2006 by Kjell
                ' Element 8 is used in BabelImport to set the Special-property, therefore populate with nada.
                aFilesArray(8, i) = "" 'DetectEDI(aFilesArray(0, i))
                aFilesArray(9, i) = 0
                aFilesArray(10, i) = ""
                aFilesArray(11, i) = ""
                aFilesArray(12, i) = ""
                ' Fill in version for EDI-formats;
                If aFilesArray(0, i) > 500 And aFilesArray(0, i) < 1000 Then
                    eFormat = aFilesArray(0, i)
                    aFilesArray(7, i) = DetectVersion(eFormat, aFArray(i))
                End If
                If aFilesArray(0, i) = "0" Then 'Filetype.unknown then
                    PickAFile = False
                    Exit For
                Else
                    PickAFile = True
                End If
            Next i
        Else
            PickAFile = False
        End If

    End Function
    
    Public Sub mnuSetupCustom1_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuSetupCustom1.Click
        ' 23.08.06 Special for VB use - documentation of babelbank

        ' added 16.10.2009, for test Statistics
        If oProfile.Custom1Text = "Statistikk" Then
            ' run report rp_952_statistics
            BB_Statistics()

        Else
            ' as pre 16.10.2009

            frmCustom1.lblCustom.Text = oProfile.Custom1Text
            frmCustom1.txtCustom.Text = Trim(oProfile.Custom1Value)
            frmCustom1.Text = Trim(oProfile.Custom1Text)
            frmCustom1.txtCustom.SelectionStart = 0
            frmCustom1.txtCustom.SelectionLength = Len(Trim(oProfile.Custom1Value))
            frmCustom1.cmdCancel.Text = LRS(55006) '"&Avbryt"

            frmCustom1.ShowDialog()
            If frmCustom1.bSave Then
                ' save values from frmCustom1 into oProfile
                oProfile.Custom1Value = frmCustom1.txtCustom.Text
                oProfile.Status = 3 'Changed
                oProfile.Save(1)
            End If
            'End If
        End If
    End Sub
    Public Sub mnuSetupCustom2_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuSetupCustom2.Click
        frmCustom2.lblCustom.Text = oProfile.Custom2Text
        frmCustom2.txtCustom.Text = Trim(oProfile.Custom2Value)
        frmCustom2.Text = Trim(oProfile.Custom2Text)
        frmCustom2.txtCustom.SelectionStart = 0
        frmCustom2.txtCustom.SelectionLength = Len(Trim(oProfile.Custom2Value))
        frmCustom2.cmdCancel.Text = LRS(55006) '"&Avbryt"
        frmCustom2.ShowDialog()
        If frmCustom2.bSave Then
            ' save values from frmCustom2 into oProfile
            oProfile.Custom2Value = frmCustom2.txtCustom.Text
            oProfile.Status = 3 'Changed
            oProfile.Save(1)
        End If

    End Sub
    Public Sub mnuSetupCustom3_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuSetupCustom3.Click
        frmCustom3.lblCustom.Text = oProfile.Custom3Text
        frmCustom3.txtCustom.Text = Trim(oProfile.Custom3Value)
        frmCustom3.Text = Trim(oProfile.Custom3Text)
        frmCustom3.txtCustom.SelectionStart = 0
        frmCustom3.txtCustom.SelectionLength = Len(Trim(oProfile.Custom3Value))
        frmCustom3.cmdCancel.Text = LRS(55006) '"&Avbryt"

        frmCustom3.ShowDialog()
        If frmCustom3.bSave Then
            ' save values from frmCustom3 into oProfile
            oProfile.Custom3Value = frmCustom3.txtCustom.Text
            oProfile.Status = 3 'Changed
            oProfile.Save(1)
        End If

    End Sub
    Public Sub mnuSetupCustom4_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuSetupCustom4.Click
        frmCustom4.lblCustom.Text = oProfile.Custom4Text
        frmCustom4.txtCustom.Text = Trim(oProfile.Custom4Value)
        frmCustom4.Text = Trim(oProfile.Custom4Text)
        frmCustom4.txtCustom.SelectionStart = 0
        frmCustom4.txtCustom.SelectionLength = Len(Trim(oProfile.Custom4Value))
        frmCustom4.cmdCancel.Text = LRS(55006) '"&Avbryt"

        frmCustom4.ShowDialog()
        If frmCustom4.bSave Then
            ' save values from frmCustom3 into oProfile
            oProfile.Custom4Value = frmCustom4.txtCustom.Text
            oProfile.Status = 3 'Changed
            oProfile.Save(1)
        End If


    End Sub

    Public Sub mnuSetupCustomArray_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuSetupCustomArray.Click
        Dim Index As Short = mnuSetupCustomArray.GetIndex(eventSender)
        Select Case Index
            Case 1 ' Custom1
                If oProfile.Custom1Text = "Statistikk" Then
                    ' run report rp_952_statistics
                    BB_Statistics()

                Else
                    ' as pre 16.10.2009
                    frmCustom1.lblCustom.Text = oProfile.Custom1Text
                    frmCustom1.txtCustom.Text = Trim(oProfile.Custom1Value)
                    frmCustom1.Text = Trim(oProfile.Custom1Text)
                    frmCustom1.txtCustom.SelectionStart = 0
                    frmCustom1.txtCustom.SelectionLength = Len(Trim(oProfile.Custom1Value))
                    frmCustom1.cmdCancel.Text = LRS(55006) '"&Avbryt"

                    frmCustom1.ShowDialog()
                    If frmCustom1.bSave Then
                        ' save values from frmCustom1 into oProfile
                        oProfile.Custom1Value = frmCustom1.txtCustom.Text
                        oProfile.Status = 3 'Changed
                        oProfile.Save(1)
                    End If
                    'End If
                End If

            Case 2 ' Custom2
                frmCustom2.lblCustom.Text = oProfile.Custom2Text
                frmCustom2.txtCustom.Text = Trim(oProfile.Custom2Value)
                frmCustom2.Text = Trim(oProfile.Custom2Text)
                frmCustom2.txtCustom.SelectionStart = 0
                frmCustom2.txtCustom.SelectionLength = Len(Trim(oProfile.Custom2Value))
                frmCustom2.cmdCancel.Text = LRS(55006) '"&Avbryt"
                frmCustom2.ShowDialog()
                If frmCustom2.bSave Then
                    ' save values from frmCustom2 into oProfile
                    oProfile.Custom2Value = frmCustom2.txtCustom.Text
                    oProfile.Status = 3 'Changed
                    oProfile.Save(1)
                End If

            Case 3 ' Custom3
                frmCustom3.lblCustom.Text = oProfile.Custom3Text
                frmCustom3.txtCustom.Text = Trim(oProfile.Custom3Value)
                frmCustom3.Text = Trim(oProfile.Custom3Text)
                frmCustom3.txtCustom.SelectionStart = 0
                frmCustom3.txtCustom.SelectionLength = Len(Trim(oProfile.Custom3Value))
                frmCustom3.cmdCancel.Text = LRS(55006) '"&Avbryt"

                frmCustom3.ShowDialog()
                If frmCustom3.bSave Then
                    ' save values from frmCustom3 into oProfile
                    oProfile.Custom3Value = frmCustom3.txtCustom.Text
                    oProfile.Status = 3 'Changed
                    oProfile.Save(1)
                End If

            Case 4 ' Custom4
                frmCustom4.lblCustom.Text = oProfile.Custom4Text
                frmCustom4.txtCustom.Text = Trim(oProfile.Custom4Value)
                frmCustom4.Text = Trim(oProfile.Custom4Text)
                frmCustom4.txtCustom.SelectionStart = 0
                frmCustom4.txtCustom.SelectionLength = Len(Trim(oProfile.Custom4Value))
                frmCustom4.cmdCancel.Text = LRS(55006) '"&Avbryt"

                frmCustom4.ShowDialog()
                If frmCustom4.bSave Then
                    ' save values from frmCustom3 into oProfile
                    oProfile.Custom4Value = frmCustom4.txtCustom.Text
                    oProfile.Status = 3 'Changed
                    oProfile.Save(1)
                End If


        End Select

    End Sub

    Public Sub MatchingSQL_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MatchingSQL.Click
        Dim oQuerySetup As BabelSetup.Query
        Dim bQuery As Boolean

        oQuerySetup = New BabelSetup.Query

        oQuerySetup.Profile = oProfile

        bQuery = oQuerySetup.Start

        If bQuery = True Then
            '    Set oProfile = oCompanySetup.Profile

            oProfile.Save(1)
            ' Reread profile, to make sure all values are set correctly
            oProfile.Load(1)

        Else
            ' Run new load to make sure no errors are carried on
            oProfile.Load(1)
        End If

    End Sub
    Private Sub BBDocumentation()
        Dim oBabelReport As vbBabel.BabelReport
        Dim oReport As vbBabel.Report
        Dim oFilesetup As vbBabel.FileSetup
        Dim oBabelFile As vbBabel.BabelFile
        Dim oClient As vbBabel.Client
        Dim oaccount As vbBabel.Account
        Dim aDoc(,) As Object
        Dim aFilesetupsHandled() As Short
        Dim lCounter As Short
        Dim iCurrentFilesetup As Short
        Dim oLicense As vbBabel.License
        Dim sServices As String
        Dim aClientsHandled() As Short
        Dim sAccount As String
        Dim lAccountType As Short
        Dim sAccountType As String = ""
        Dim aQuickPostingAccounts(,) As String
        Dim aAdjustments(,) As String
        Dim i As Short
        Dim bMatchingProfile As Boolean
        Dim sText As String
        Dim aQueryArray(,,) As String

        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sMyFile As String
        Dim sCompanyID As String
        Dim bAutoBookingExists As Boolean
        Dim aAutoBook(,) As String
        Dim aPattern() As String
        Dim aNotifications() As String 'added 01.09.2009
        Dim bRecordExists As Boolean = False
        ' XNET 03.11.2011
        Dim bThisIsTBI As Boolean


        Try
            oLicense = New vbBabel.License
            sServices = oLicense.LicServicesAvailable
            oLicense = Nothing
            ' XNET 03.11.2011 added next If
            If InStr(sServices, "T") > 0 Then
                bThisIsTBI = True
            Else
                bThisIsTBI = False
            End If

            ' do not open for customer
            If RunTime() Then
                If Not oProfile Is Nothing Then
                    If InStr(1, oProfile.CompanyName, "SANTANDER", vbTextCompare) > 0 Then
                        'OK continue
                    Else
                        ' XNET 03.11.2011 - Open for TBI users
                        If Not bThisIsTBI Then
                            Exit Sub
                        End If
                    End If
                Else
                    ' XNET 03.11.2011 - Open for TBI users
                    If Not bThisIsTBI Then
                        Exit Sub
                    End If
                End If
            End If

            ' For TBI, do not use table BB_Documentation, or frmDocumentation
            If Not bThisIsTBI Then

                sCompanyID = "1" 'TODO - Hardcoded CompanyID

                oMyDal = New vbBabel.DAL
                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDal.ConnectToDB() Then
                    Throw New System.Exception(oMyDal.ErrorMessage)
                End If

                ' Fill a recordset with records from BB_Documentation
                sMySQL = "SELECT * FROM BB_Documentation WHERE Company_ID=" & sCompanyID
                oMyDal.SQL = sMySQL
                If oMyDal.Reader_Execute() Then
                    Do While oMyDal.Reader_ReadRecord
                        bRecordExists = True
                        ' Fill up form:
                        If Not EmptyString(oMyDal.Reader_GetString("StartHeader")) Then
                            frmDocumentation.txtStartHeader.Text = oMyDal.Reader_GetString("StartHeader")
                        End If
                        If Not EmptyString(oMyDal.Reader_GetString("StartBody")) Then
                            frmDocumentation.txtStartBody.Text = oMyDal.Reader_GetString("StartBody")
                        End If
                        If Not EmptyString(oMyDal.Reader_GetString("ProfilesHeader")) Then
                            frmDocumentation.txtProfilesHeader.Text = oMyDal.Reader_GetString("ProfilesHeader")
                        End If
                        If Not EmptyString(oMyDal.Reader_GetString("ProfilesBody")) Then
                            frmDocumentation.txtProfilesBody.Text = oMyDal.Reader_GetString("ProfilesBody")
                        End If
                        If Not EmptyString(oMyDal.Reader_GetString("ClientsHeader")) Then
                            frmDocumentation.txtClientsHeader.Text = oMyDal.Reader_GetString("ClientsHeader")
                        End If
                        If Not EmptyString(oMyDal.Reader_GetString("ClientsBody")) Then
                            frmDocumentation.txtClientsBody.Text = oMyDal.Reader_GetString("ClientsBody")
                        End If
                        If Not EmptyString(oMyDal.Reader_GetString("MatchingHeader")) Then
                            frmDocumentation.txtMatchingHeader.Text = oMyDal.Reader_GetString("MatchingHeader")
                        End If
                        If Not EmptyString(oMyDal.Reader_GetString("MatchingBody")) Then
                            frmDocumentation.txtMatchingBody.Text = oMyDal.Reader_GetString("MatchingBody")
                        End If
                        If Not EmptyString(oMyDal.Reader_GetString("EndHeader")) Then
                            frmDocumentation.txtEndHeader.Text = oMyDal.Reader_GetString("EndHeader")
                        End If
                        If Not EmptyString(oMyDal.Reader_GetString("EndBody")) Then
                            frmDocumentation.txtEndBody.Text = oMyDal.Reader_GetString("EndBody")
                        End If
                        Exit Do
                    Loop
                Else
                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                End If

                frmDocumentation.ShowDialog()
            Else
                frmDocumentation.txtStartHeader.Text = "" 'rsDoc!StartHeader
                frmDocumentation.txtStartBody.Text = "" 'rsDoc!StartBody
                frmDocumentation.txtProfilesHeader.Text = "" 'rsDoc!ProfilesHeader
                frmDocumentation.txtProfilesBody.Text = "" 'rsDoc!ProfilesBody
                frmDocumentation.txtClientsHeader.Text = "" 'rsDoc!ClientsHeader
                frmDocumentation.txtClientsBody.Text = "" 'rsDoc!ClientsBody
                frmDocumentation.txtMatchingHeader.Text = "" 'rsDoc!MatchingHeader
                frmDocumentation.txtMatchingBody.Text = "" 'rsDoc!MatchingBody
                frmDocumentation.txtEndHeader.Text = "" 'rsDoc!EndHeader
                frmDocumentation.txtEndBody.Text = ""  'rsDoc!EndBody
            End If

            If Not frmDocumentation.bContinue Then
                'Nothing to do
            Else
                ' XNET 03.11.2011
                If Not bThisIsTBI Then

                    ' Save text from form into table BB_Documentation
                    If bRecordExists Then
                        sMySQL = "UPDATE BB_Documentation SET StartHeader = '" & frmDocumentation.txtStartHeader.Text
                        sMySQL = sMySQL & "', StartBody = '" & frmDocumentation.txtStartBody.Text
                        sMySQL = sMySQL & "', ProfilesHeader = '" & frmDocumentation.txtProfilesHeader.Text
                        sMySQL = sMySQL & "', ProfilesBody = '" & frmDocumentation.txtProfilesBody.Text
                        sMySQL = sMySQL & "', ClientsHeader = '" & frmDocumentation.txtClientsHeader.Text
                        sMySQL = sMySQL & "', ClientsBody = '" & frmDocumentation.txtClientsBody.Text
                        sMySQL = sMySQL & "', MatchingHeader = '" & frmDocumentation.txtMatchingHeader.Text
                        sMySQL = sMySQL & "', MatchingBody = '" & frmDocumentation.txtMatchingBody.Text
                        sMySQL = sMySQL & "', EndHeader = '" & frmDocumentation.txtEndHeader.Text
                        sMySQL = sMySQL & "', EndBody = '" & frmDocumentation.txtEndBody.Text
                        sMySQL = sMySQL & "' WHERE Company_ID=" & sCompanyID
                    Else
                        sMySQL = "INSERT INTO BB_Documentation (Company_ID, Doc_ID, StartHeader, StartBody, ProfilesHeader, ProfilesBody, "
                        sMySQL = sMySQL & "ClientsHeader, ClientsBody, MatchingHeader, MatchingBody, EndHeader, EndBody) "
                        sMySQL = sMySQL & "VALUES(" & sCompanyID & ", 1, '" & frmDocumentation.txtStartHeader.Text & "', '" & frmDocumentation.txtStartBody.Text
                        sMySQL = sMySQL & "', '" & frmDocumentation.txtProfilesHeader.Text & "', '" & frmDocumentation.txtProfilesBody.Text
                        sMySQL = sMySQL & "', '" & frmDocumentation.txtClientsHeader.Text & "', '" & frmDocumentation.txtClientsBody.Text
                        sMySQL = sMySQL & "', '" & frmDocumentation.txtMatchingHeader.Text & "', '" & frmDocumentation.txtMatchingBody.Text
                        sMySQL = sMySQL & "', '" & frmDocumentation.txtEndHeader.Text & "', '" & frmDocumentation.txtEndBody.Text & "')"
                    End If

                    oMyDal.SQL = sMySQL

                    If oMyDal.ExecuteNonQuery Then
                        If oMyDal.RecordsAffected = 1 Then
                            'OK
                        Else
                            Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                        End If
                    Else
                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                    End If
                End If

                ReDim aDoc(2, 0)
                ReDim aFilesetupsHandled(0)
                ReDim aClientsHandled(0)

                ' XNET 03.11.2011
                If bThisIsTBI Then
                    frmDocumentation.txtStartHeader.Text = "BabelBank setup for DnB"
                    frmDocumentation.txtStartBody.Text = LRS(60393) '
                    '"This document is an overview of the settings in BabelBank for DnB."
                End If

                If Not EmptyString(frmDocumentation.txtStartHeader.Text) Then
                    ' Startheader from form
                    aDoc(0, lCounter) = frmDocumentation.txtStartHeader.Text
                    aDoc(1, lCounter) = "&M"  ' F�rste heading blir rapportens hovedoverskrift!
                End If

                ' XNET 03.11.2011 added .text under
                If Not EmptyString(frmDocumentation.txtStartBody.Text) Then
                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(2, lCounter)
                    ' Start body from form
                    aDoc(0, lCounter) = ""
                    aDoc(1, lCounter) = Replace(frmDocumentation.txtStartBody.Text, "crlf", vbCrLf, , , vbTextCompare)
                End If

                ' Companyinfo
                '------------
                lCounter = lCounter + 1
                ReDim Preserve aDoc(2, lCounter)
                aDoc(0, lCounter) = LRS(60347)   '"Firmaopplysninger"  'lrs(
                aDoc(1, lCounter) = "&M"
                ' Detail section of report
                lCounter = lCounter + 1
                ReDim Preserve aDoc(2, lCounter)
                ' CompanyName
                aDoc(0, lCounter) = LRS(60346)   '"Firmanavn"  'lrs(
                aDoc(1, lCounter) = oProfile.CompanyName
                If Not EmptyString(oProfile.CompanyNo) Then
                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(2, lCounter)
                    aDoc(0, lCounter) = LRS(60345)  '"Foretaksnummer"  'lrs(
                    aDoc(1, lCounter) = oProfile.CompanyNo
                End If
                lCounter = lCounter + 1
                ReDim Preserve aDoc(2, lCounter)
                aDoc(0, lCounter) = LRS(60348)  '"Backupkatalog"  'lrs(
                aDoc(1, lCounter) = oProfile.BackupPath
                lCounter = lCounter + 1
                ReDim Preserve aDoc(2, lCounter)
                aDoc(0, lCounter) = LRS(60349)  '"Backup, antall dager"  'lrs(
                aDoc(1, lCounter) = Str(Int(oProfile.DelDays))
                If Not EmptyString(oProfile.EmailAutoSender) Then
                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(2, lCounter)
                    aDoc(0, lCounter) = LRS(60350)  '"EMail avsender"  'lrs(
                    aDoc(1, lCounter) = oProfile.EmailAutoSender
                End If

                '---------------------------
                ' Profiles
                '---------------------------

                ' New Main section show Profiles
                lCounter = lCounter + 1
                ReDim Preserve aDoc(2, lCounter)
                aDoc(0, lCounter) = LRS(60351)  '"Profiloppsett"  'lrs(
                aDoc(1, lCounter) = "&M"

                ' Heading and text before profile
                If Not bThisIsTBI Then
                    If Not EmptyString(frmDocumentation.txtProfilesHeader.Text) Then
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        ' Profilesheader from form
                        aDoc(0, lCounter) = frmDocumentation.txtProfilesHeader.Text
                        aDoc(1, lCounter) = "&S"
                    End If
                End If
                If Not EmptyString(frmDocumentation.txtProfilesBody.Text) Then
                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(2, lCounter)
                    ' Profile body from form
                    aDoc(0, lCounter) = ""
                    aDoc(1, lCounter) = Replace(frmDocumentation.txtProfilesBody.Text, "crlf", vbCrLf, , , vbTextCompare)
                End If


                ' Run throug all profiles
                For Each oFilesetup In oProfile.FileSetups
                    If Not FoundInFilesetupArray(oProfile.FileSetups, aFilesetupsHandled, oFilesetup.FileSetup_ID) Then
                        bMatchingProfile = False
                        iCurrentFilesetup = oFilesetup.FileSetup_ID
                        ' Keep track of which filesetups are handled. We have to jump between them, because
                        ' they are splitted in two records
                        ReDim Preserve aFilesetupsHandled(UBound(aFilesetupsHandled) + 1)
                        aFilesetupsHandled(UBound(aFilesetupsHandled)) = oFilesetup.FileSetup_ID

                        ' Find info from each Profile, and fill into array
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)

                        ' SubHeader, Profilename
                        aDoc(0, lCounter) = oFilesetup.ShortName
                        aDoc(1, lCounter) = "&S"

                        ' Detail section of report
                        If Not EmptyString(oFilesetup.Description) Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = LRS(60352)  '"Beskrivelse"  'lrs(
                            aDoc(1, lCounter) = oFilesetup.Description
                        End If

                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        ' Profiltype (Send, Retur, Avstemming, DnBNOR TBI Send/Retur)

                        aDoc(0, lCounter) = LRS(60353)  '"Profiltype"  'lrs(
                        'If oFilesetup.FormatOutID1 > 0 Or oProfile.FileSetups(oFilesetup.FileSetupOut).FormatOutID1 > 0 Then
                        If oFilesetup.AutoMatch Then
                            aDoc(1, lCounter) = LRS(60354)  '"Avstemmingsprofil"  'lrs(
                            bMatchingProfile = True
                        ElseIf oFilesetup.FromAccountingSystem = True Then
                            If InStr(sServices, "T") > 0 Then
                                ' XNET 22.12.2011 changed from DnBNOR to DNB
                                aDoc(1, lCounter) = "DnB TBI - " & LRS(60355)  '"Sendeprofil"
                            Else
                                aDoc(1, lCounter) = LRS(60355)  '"Sendeprofil"  'lrs(
                            End If
                        Else
                            If InStr(sServices, "T") > 0 Then
                                aDoc(1, lCounter) = "DnB -" & LRS(60356)  '"Returprofil" 'lrs(
                            Else
                                aDoc(1, lCounter) = LRS(60356)  '"Returprofil"  'lrs(
                            End If
                        End If

                        If Not EmptyString(oFilesetup.TreatSpecial) Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            ' Special
                            aDoc(0, lCounter) = LRS(60357)  '"Spesialkode"  'lrs(
                            aDoc(1, lCounter) = oFilesetup.TreatSpecial
                        End If

                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        ' Format in
                        aDoc(0, lCounter) = LRS(60358)  '"Importformat"  'lrs(
                        aDoc(1, lCounter) = GetEnumFileType(oFilesetup.Enum_ID)

                        ' XNET 04.11.2011 - added mappingfile if present
                        If oFilesetup.Enum_ID = vbBabel.BabelFiles.FileType.BabelBank_Mapped_Out Then
                            If Not EmptyString(oFilesetup.MappingFileName) Then
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = LRS(60359)  '"Mappingfil"  'lrs(
                                aDoc(1, lCounter) = oFilesetup.MappingFileName
                            End If
                        End If
                        ' FilenameIn
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = LRS(60360)  '"Importfil"  'lrs(
                        aDoc(1, lCounter) = oFilesetup.FileNameIn1
                        If oFilesetup.BackupIn Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            ' Backup
                            aDoc(0, lCounter) = "Backup"  'lrs(
                            aDoc(1, lCounter) = LRS(60361)   '"Det tas backup av importfiler"  'lrs(
                        End If

                        If Not EmptyString(oFilesetup.CompanyNo) Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = LRS(60345)   '"Foretaksnummer"  'lrs(
                            aDoc(1, lCounter) = oFilesetup.CompanyNo
                        End If
                        If Not EmptyString(oFilesetup.AdditionalNo) Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = LRS(60362)  '"KundenehtsID"  'lrs(
                            aDoc(1, lCounter) = oFilesetup.AdditionalNo
                        End If
                        If oFilesetup.Log > 0 Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = LRS(60363)  '"Logging"  'lrs(
                            If oFilesetup.LogType = 1 Then
                                aDoc(1, lCounter) = "Logging til Windows Eventlog" 'lrs(
                            ElseIf oFilesetup.LogType = 2 Then
                                aDoc(1, lCounter) = LRS(60364)  '"Logging til fil " & oFilesetup.LogFilename  'lrs(
                            Else
                                aDoc(1, lCounter) = LRS(60365)  '"Logging til email"  'lrs(
                            End If
                        End If
                        If oFilesetup.Silent Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = LRS(60366)  '"Visning"  'lrs(
                            aDoc(1, lCounter) = LRS(60367)  '"Profil kj�res uten visning til skjerm (silent)" 'lrs(
                        End If

                        ' Find outpart of filesetup;
                        '---------------------------
                        oFilesetup = oProfile.FileSetups(oFilesetup.FileSetupOut)
                        ' Keep track of which filesetups are handled. We have to jump between them, because
                        ' they are splitted in two records
                        ReDim Preserve aFilesetupsHandled(UBound(aFilesetupsHandled) + 1)
                        aFilesetupsHandled(UBound(aFilesetupsHandled)) = oFilesetup.FileSetup_ID

                        ' XNET 04.11.2011
                        If Not EmptyString(oFilesetup.Division) Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = LRS(60368)  '"Divisjon"  'lrs(
                            aDoc(1, lCounter) = oFilesetup.Division
                        End If

                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        ' Format in
                        aDoc(0, lCounter) = LRS(60369)  '"Eksportformat"  'lrs(
                        aDoc(1, lCounter) = GetEnumFileType(oFilesetup.Enum_ID)

                        If Not EmptyString(oFilesetup.FileNameOut1) Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            ' Filenameout
                            aDoc(0, lCounter) = LRS(60370)  '"Eksportfil" & " 1"  'lrs(
                            aDoc(1, lCounter) = oFilesetup.FileNameOut1
                        End If
                        If Not EmptyString(oFilesetup.FileNameOut2) Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            ' Filenameout
                            aDoc(0, lCounter) = LRS(60370) '"Eksportfil" & " 2"
                            aDoc(1, lCounter) = oFilesetup.FileNameOut2
                        End If
                        If Not EmptyString(oFilesetup.FileNameOut3) Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            ' Filenameout
                            aDoc(0, lCounter) = LRS(60370)  '"Eksportfil" & " 3"
                            aDoc(1, lCounter) = oFilesetup.FileNameOut3
                        End If


                        If oFilesetup.BackupOut Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            ' Backup
                            aDoc(0, lCounter) = LRS(60371)  '"Backup"  'lrs(
                            aDoc(1, lCounter) = LRS(60372)  '"Det tas backup av eksportfiler"  'lrs(
                        End If

                        ' XNET 04.11.2011
                        ' Special items for DnBNOR only;
                        ' ------------------------------
                        If bThisIsTBI Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)

                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = "Debacclist "
                            aDoc(1, lCounter) = oProfile.AccountsFile

                            ' Find "in"part of filesetup;
                            '---------------------------
                            oFilesetup = oProfile.FileSetups(oFilesetup.FileSetupOut)

                            If Not EmptyString(oFilesetup.PaymentType) Then
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = LRS(60373)  '"Valgt betalingstype "
                                aDoc(1, lCounter) = oFilesetup.PaymentType
                            End If

                            If oFilesetup.AddDays > 0 Then
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = LRS(60374)  '"Antall dager som legges til"
                                aDoc(1, lCounter) = oFilesetup.AddDays
                            End If

                            If oFilesetup.MergePayments > 0 Then
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = LRS(60375)  '"Sl� sammen betalinger / Merge payments "
                                Select Case oFilesetup.MergePayments
                                    Case 1
                                        aDoc(1, lCounter) = LRS(60376) '"Merge payments, but keep all info to receiver"
                                    Case 2
                                        aDoc(1, lCounter) = LRS(60377) '"Merge payments, regardless of informationloss"
                                    Case 3
                                        aDoc(1, lCounter) = LRS(60378) '"Merge payments, leave NO information"
                                    Case 4
                                        aDoc(1, lCounter) = LRS(60379) '"Merge payments, ONLY if creditnotes are involved"
                                End Select
                            End If
                        End If

                        ' Reports (for "outpart")
                        '------------------------
                        If oFilesetup.Reports.Count > 0 Then
                            lCounter = lCounter + 2 ' Blank linje for rapporter
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = ""
                            aDoc(1, lCounter) = "&B&G" & LRS(60380)  '"Rapporter" 'lrs(
                            ' Find all reports for this filesetupid
                            For Each oReport In oFilesetup.Reports
                                lCounter = lCounter + 2
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = VB.Right("000" & LTrim(Str(oReport.ReportNo)), 3) & " " & ReportName(oReport.ReportNo) & oReport.Heading
                                If Not EmptyString(oReport.ExportType) Then
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = LRS(60381) & ": " & oReport.ExportType   '"Eksporttype: "
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = LRS(60382) & ": " & oReport.ExportFilename   '"Filnavn: " &
                                End If

                            Next oReport
                        End If
                        ' Reports (for "inpart")
                        '------------------------
                        oFilesetup = oProfile.FileSetups(oFilesetup.FileSetupOut)
                        If oFilesetup.Reports.Count > 0 Then
                            lCounter = lCounter + 2 ' Blank linje for rapporter
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = ""
                            aDoc(1, lCounter) = "&B&G" & LRS(60380)  '"Rapporter" 'lrs(
                            ' Find all reports for this filesetupid
                            For Each oReport In oFilesetup.Reports
                                lCounter = lCounter + 2
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = VB.Right("000" & LTrim(Str(oReport.ReportNo)), 3) & " " & ReportName(oReport.ReportNo) & oReport.Heading
                                If Not EmptyString(oReport.ExportType) Then
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = LRS(60381) & ": " & oReport.ExportType   '"Eksporttype: "
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = LRS(60382) & ": " & oReport.ExportFilename  '"Filnavn: " &
                                End If

                            Next oReport
                        End If

                    End If
                Next oFilesetup

                '---------------------------
                ' Clients
                '---------------------------
                ' New Main section show Clients
                lCounter = lCounter + 1
                ReDim Preserve aDoc(2, lCounter)
                aDoc(0, lCounter) = LRS(60383)  '"Klienter"  'lrs(
                aDoc(1, lCounter) = "&M"

                ' XNET 04.11.2011
                If Not bThisIsTBI Then
                    ' Heading and tekst before Clients
                    If Not EmptyString(frmDocumentation.txtClientsHeader.Text) Then
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        ' Clientsheader from form
                        aDoc(0, lCounter) = frmDocumentation.txtClientsHeader.Text
                        aDoc(1, lCounter) = "&S"
                    End If
                End If

                If Not EmptyString(frmDocumentation.txtClientsBody.Text) Then
                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(2, lCounter)
                    ' Profile body from form
                    aDoc(0, lCounter) = ""
                    aDoc(1, lCounter) = Replace(frmDocumentation.txtClientsBody.Text, "crlf", vbCrLf, , , vbTextCompare)
                End If

                For Each oFilesetup In oProfile.FileSetups
                    For Each oClient In oFilesetup.Clients
                        If Not FoundInClientArray(aClientsHandled, oClient.Client_ID) Then
                            ' Not Handled
                            ReDim Preserve aClientsHandled(UBound(aClientsHandled) + 1)
                            aClientsHandled(UBound(aClientsHandled)) = oClient.Client_ID

                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = LRS(60384) & " " & oClient.ClientNo & " " & oClient.Name  'lrs("Klient "
                            aDoc(1, lCounter) = "&S"

                            'added 01.09.2009 to also show Notifications
                            aNotifications = dbFillEmails(oClient.ClientNo, oClient.Client_ID)
                            For i = 0 To UBound(aNotifications)
                                If Not EmptyString(aNotifications(i)) Then
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = PadRight("e-mail", 25, " ") & Chr(9) & aNotifications(i)
                                End If
                            Next i

                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            ' Backup
                            aDoc(0, lCounter) = ""
                            aDoc(1, lCounter) = "&B&G" & LRS(60385)  '"Konti" 'lrs(


                            For Each oaccount In oClient.Accounts
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = "&B" & oaccount.Account
                                If Not EmptyString(oaccount.ContractNo) Then
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = LRS(60386) & ": " & oaccount.ContractNo  'lrs(AvtaleID
                                End If
                                If Not EmptyString(oaccount.GLAccount) Then
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = LRS(60387) & ": " & oaccount.GLAccount 'lrs(Hovedbok
                                End If
                                If Not EmptyString(oaccount.ConvertedAccount) Then
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = LRS(60388) & ": " & oaccount.ConvertedAccount 'lrs(Konvertert konto
                                End If
                                If Not EmptyString(oaccount.SWIFTAddress) Then
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = "SWIFT" & ": " & oaccount.SWIFTAddress
                                End If
                                If Not EmptyString(oaccount.AvtaleGiroID) Then
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = LRS(60389) & ": " & oaccount.AvtaleGiroID  'lrs(Avtalegiro ID
                                End If
                                ' XNET 04.11.2011 added next
                                If Not EmptyString(oaccount.CurrencyCode) Then
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = LRS(60390) & ": " & oaccount.CurrencyCode  'lrs(Valuta
                                End If
                                ' XNET 04.11.2011 added next
                                If Not EmptyString(oaccount.DebitAccountCountryCode) Then
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = LRS(60391) & ": " & oaccount.DebitAccountCountryCode 'lrs(Landkode
                                End If

                                ' Blank linje mellom hver konto
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = ""
                            Next oaccount


                            ' Avstemmingsparametre;
                            '----------------------
                            If bMatchingProfile Then
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = "&B&GKlientoppsett for avstemming" 'lrs(

                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = ""

                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = "&B&GAvvikskonti" 'lrs(

                                ' Diffaccounts:
                                '--------------
                                ' NoMatchAccount
                                sAccount = GetNoMatchAccount(1, (oClient.Client_ID))
                                If Not EmptyString(sAccount) Then
                                    lAccountType = GetNoMatchAccountType(1, (oClient.Client_ID))
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = "&B" & PadRight("Observasjonskonto", 30, " ") & Chr(9) & sAccount 'lrs(
                                    If lAccountType = 1 Then
                                        sAccountType = "Kundereskontro" 'lrs(
                                    ElseIf lAccountType = 2 Then
                                        sAccountType = "Hovedbokskonto" 'lrs(
                                    ElseIf lAccountType = 3 Then
                                        sAccountType = "KID" 'lrs(
                                    ElseIf lAccountType = 4 Then
                                        sAccountType = "Fra ERP" 'lrs(
                                    ElseIf lAccountType = 6 Then
                                        sAccountType = "Leverand�rreskontro" 'lrs(
                                    End If
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = PadRight("Observasjonskontotype", 30, " ") & Chr(9) & sAccountType

                                    ' Blank linje etter Nomatch account
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = ""

                                End If

                                ' DiscountAccount
                                sAccount = GetDiscountAccount(1, (oClient.Client_ID))
                                If Not EmptyString(sAccount) Then
                                    lAccountType = GetDiscountAccountType(1, (oClient.Client_ID))
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = "&B" & PadRight("Avstemming, rabattkonto", 30, " ") & Chr(9) & sAccount 'lrs(
                                    If lAccountType = 1 Then
                                        sAccountType = "Kundereskontro" 'lrs(
                                    ElseIf lAccountType = 2 Then
                                        sAccountType = "Hovedbokskonto" 'lrs(
                                    ElseIf lAccountType = 3 Then
                                        sAccountType = "KID" 'lrs(
                                    ElseIf lAccountType = 3 Then
                                        sAccountType = "Fra ERP" 'lrs(
                                    End If
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = PadRight("Rabattkontotype", 30, " ") & Chr(9) & Chr(9) & sAccountType

                                    ' Blank linje etter dicountaccount
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = ""
                                End If

                                'aGetQuickPostingAccounts consists of:
                                '0 = Name
                                '1 = Pattern
                                '2 = Type
                                '3 = Text
                                '4 = Function
                                '5 = VATCode
                                aQuickPostingAccounts = GetQuickPostingAccounts(1, (oClient.Client_ID), False)
                                For i = 0 To UBound(aQuickPostingAccounts, 2)
                                    If aQuickPostingAccounts(4, i) <> "NOMATCH" And aQuickPostingAccounts(4, i) <> "CASHDISCOUNT" Then
                                        If Not EmptyString(aQuickPostingAccounts(0, i)) Then
                                            lCounter = lCounter + 1
                                            ReDim Preserve aDoc(2, lCounter)
                                            aDoc(0, lCounter) = ""
                                            aDoc(1, lCounter) = "&B" & PadRight("Avvikskonto", 20, " ") & Chr(9) & aQuickPostingAccounts(0, i) 'lrs(
                                        End If
                                        If Not EmptyString(aQuickPostingAccounts(1, i)) Then
                                            lCounter = lCounter + 1
                                            ReDim Preserve aDoc(2, lCounter)
                                            aDoc(0, lCounter) = ""
                                            aDoc(1, lCounter) = PadRight("Avviksm�nster", 20, " ") & Chr(9) & aQuickPostingAccounts(1, i) 'lrs(
                                        End If
                                        If Not EmptyString(aQuickPostingAccounts(2, i)) Then
                                            lCounter = lCounter + 1
                                            ReDim Preserve aDoc(2, lCounter)
                                            aDoc(0, lCounter) = ""
                                            aDoc(1, lCounter) = PadRight("Avvikskontotype", 20, " ") & Chr(9) & aQuickPostingAccounts(2, i) 'lrs(
                                        End If
                                        If Not EmptyString(aQuickPostingAccounts(3, i)) Then
                                            lCounter = lCounter + 1
                                            ReDim Preserve aDoc(2, lCounter)
                                            aDoc(0, lCounter) = ""
                                            aDoc(1, lCounter) = PadRight("Avvikskontotekst", 20, " ") & Chr(9) & aQuickPostingAccounts(3, i) 'lrs(
                                        End If
                                        If Not EmptyString(aQuickPostingAccounts(4, i)) Then
                                            lCounter = lCounter + 1
                                            ReDim Preserve aDoc(2, lCounter)
                                            aDoc(0, lCounter) = ""
                                            aDoc(1, lCounter) = PadRight("Avviksfunksjon", 20, " ") & Chr(9) & aQuickPostingAccounts(4, i) 'lrs(
                                        End If
                                        If Not EmptyString(aQuickPostingAccounts(5, i)) Then
                                            lCounter = lCounter + 1
                                            ReDim Preserve aDoc(2, lCounter)
                                            aDoc(0, lCounter) = ""
                                            aDoc(1, lCounter) = PadRight("Avviksmomskode", 20, " ") & Chr(9) & aQuickPostingAccounts(5, i) 'lrs(
                                        End If
                                        ' Blank linje mellom hver avvikskonto
                                        lCounter = lCounter + 1
                                        ReDim Preserve aDoc(2, lCounter)
                                        aDoc(0, lCounter) = ""
                                        aDoc(1, lCounter) = ""
                                    End If
                                Next i

                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = "&B&GAvviksbel�p" 'lrs(

                                ' Avviksbel�p
                                'Element 0 - Amount
                                'Element 1 - Currency
                                'Element 2 - Exact or Up to
                                'Element 3 - AccountNo
                                'Element 4 - AccountType
                                aAdjustments = GetAdjustments(1, CStr(oClient.Client_ID))
                                'New 12.10.2008
                                If Not Array_IsEmpty(aAdjustments) Then
                                    For i = 0 To UBound(aAdjustments, 2)
                                        If Not EmptyString(aAdjustments(0, i)) Then
                                            lCounter = lCounter + 1
                                            ReDim Preserve aDoc(2, lCounter)
                                            aDoc(0, lCounter) = ""
                                            aDoc(1, lCounter) = "&B" & PadRight("Avviksbel�p", 20, " ") & Chr(9) & VB6.Format(CDbl(aAdjustments(0, i)) / 100, "##,##0.00") 'lrs(
                                        End If
                                        If Not EmptyString(aAdjustments(1, i)) Then
                                            lCounter = lCounter + 1
                                            ReDim Preserve aDoc(2, lCounter)
                                            aDoc(0, lCounter) = ""
                                            aDoc(1, lCounter) = PadRight("Avviksvaluta", 20, " ") & Chr(9) & aAdjustments(1, i) 'lrs(
                                        End If
                                        If Not EmptyString(aAdjustments(2, i)) Then
                                            lCounter = lCounter + 1
                                            ReDim Preserve aDoc(2, lCounter)
                                            aDoc(0, lCounter) = ""
                                            aDoc(1, lCounter) = PadRight("Avvik - Eksakt", 20, " ") & Chr(9) & aAdjustments(2, i) 'lrs(
                                        End If
                                        If Not EmptyString(aAdjustments(3, i)) Then
                                            lCounter = lCounter + 1
                                            ReDim Preserve aDoc(2, lCounter)
                                            aDoc(0, lCounter) = ""
                                            aDoc(1, lCounter) = PadRight("Avvikskonto", 20, " ") & Chr(9) & aAdjustments(3, i) 'lrs(
                                        End If
                                        If Not EmptyString(aAdjustments(4, i)) Then
                                            lCounter = lCounter + 1
                                            ReDim Preserve aDoc(2, lCounter)
                                            aDoc(0, lCounter) = ""
                                            aDoc(1, lCounter) = PadRight("Avvikskontotype", 20, " ") & Chr(9) & aAdjustments(4, i) 'lrs(
                                        End If
                                        ' Blank linje mellom hvert avviksbel�p
                                        lCounter = lCounter + 1
                                        ReDim Preserve aDoc(2, lCounter)
                                        aDoc(0, lCounter) = ""
                                        aDoc(1, lCounter) = ""
                                    Next i
                                End If
                            End If ' bMatchingprofile


                            ' Autobooking
                            '------------
                            'Second dimension is the data -
                            'Element 0 - ClientNo
                            'Element 1 - SelectOnField
                            'Element 2 - ValueInField
                            'Element 3 - ToAccount
                            'Element 4 - Booktext
                            'Element 5 - Type
                            'Element 6 - ProposeMatch
                            'Element 7 - Name
                            bAutoBookingExists = False
                            'aAutoBook = GetAutobookingArray(Val(sCompanyID), (oClient.ClientNo), bAutoBookingExists)
                            '25.07.2011 - KI - Calling another function to populate the array
                            'The new function may return more than 1 criteria per autobooking
                            'This was the omly time in BB that we called this function.
                            'NBNBNBNBNBNB! The further use of this function is NOT TESTED!!!!!!!
                            aAutoBook = GetAutobookingsArray(Val(sCompanyID), (oClient.ClientNo), bAutoBookingExists)
                            If bAutoBookingExists Then
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = ""
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = "&B&GAutobokf�ringer" 'lrs(

                                For i = 0 To UBound(aAutoBook, 2)
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = aAutoBook(1, i) & " = " & aAutoBook(2, i) & " til konto " & aAutoBook(3, i)
                                Next i
                            End If

                            ' M�nster
                            '--------
                            aPattern = GetPatternPrClient(Val(sCompanyID), (oClient.Client_ID))
                            'New 12.11.2008
                            If Not Array_IsEmpty(aPattern) Then
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = ""
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = "&B&GAvstemmingsm�nster" 'lrs(
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = ""
                                aDoc(1, lCounter) = "BabelBank benytter m�nster for gjenkjenning av elementer i fritekst fra betaler."

                                For i = 0 To UBound(aPattern)
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = aPattern(i)
                                Next i
                            End If

                            If Array_IsEmpty(aClientsHandled) Then
                                lCounter = lCounter + 1
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = "Ingen klienter ibruk" 'lrs(
                                aDoc(1, lCounter) = ""
                            End If
                        End If 'not found i aClientsHandled

                    Next oClient
                Next oFilesetup
                '---------



                '-------------------------
                ' Matching
                '-------------------------
                ' XNET 04.11.2011
                If Not bThisIsTBI Then

                    ' Heading and text before matchingrules
                    If Not EmptyString((frmDocumentation.txtMatchingHeader).Text) Then
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        ' Matchingheader from form
                        aDoc(0, lCounter) = frmDocumentation.txtMatchingHeader.Text
                        aDoc(1, lCounter) = "&S"
                    End If

                    If Not EmptyString((frmDocumentation.txtMatchingBody).Text) Then
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        ' Profile body from form
                        aDoc(0, lCounter) = ""
                        aDoc(1, lCounter) = Replace(frmDocumentation.txtMatchingBody.Text, "crlf", vbCrLf, , , CompareMethod.Text)
                    End If

                    'Build an array consisting of the ConnectionStrings and Query agianst the ERP-system.
                    aQueryArray = GetERPDBInfo(1, 1) ' Automatching

                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(2, lCounter)
                    aDoc(0, lCounter) = "Avstemmingsregler" 'lrs(
                    aDoc(1, lCounter) = "&M"

                    'New 12.11.2008
                    If Not Array_IsEmpty(aQueryArray) Then
                        'New 07.07.2008 - Standard explaination of the matching in BabelBank
                        If UBound(aQueryArray, 3) > 0 Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = "Forklaring til SQL sp�rringer"
                            aDoc(1, lCounter) = Replace("I SQL-ene som benyttes mot databasen m� enkelte felter ha spesialnavn for at BabelBank skal kunne tolke feltene riktig.crlfAlle verdier som begynner med BB_ er variabler som BabelBank bytter ut med en verdi fra innbetalingen.crlfF.eks. vil BB_Amount byttes ut med navn p� innbetalingen.crlfDisse vil du normalt finne i WHERE-delen av sp�rringen.crlfcrlfAlle verdier som begyner med BBRET_ er returverdier fra sp�rringen. Fordi BabelBank kan beyttes mot mange forskjellige systemer, m� f.eks. bel�p oversettes til en standard kolonnenavn i recordsettet som returneres. Dette gj�res ved at kolonnenavnet settes til BBRET_Amount.crlfcrlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BB_InvoiceIdentifier - Benyttes sammen med M�nster, f.eks. fakturanummer. Kan ogs� benyttes for validering av KID. Info om KID meldes inn i tabellen KID-description.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BB_Amount - BB_Amount er innbetalt bel�p i �rer, f.eks. kr. 10,- vil bli 1000.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BB_ClientNo - Denne variablen hentes fra klientoppsettet (utledes fra mottakers kontonummer) i BabelBank, og kan benyttes i forbindelse med l�sninger hvor det finnes flere klienter/selskaper i samme database.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BB_Currency - Innbetalt bel�p. Standard ISO-koder.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BB_AccountNo - Betalers kontonummer.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BB_Name - Betaler navn.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BB_CustomerNo - Betalers kundenummer, kan f.eks. hentes fra KID, eventuelt fra en annen sp�rring.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BB_MyDim - Ingen standard bruk.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BB_UniqueID - KIDcrlfcrlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BBRET_Amount - (Rest)bel�p p� faktura. M� returneres i �rer, f.eks. kr. 10,- = 1000.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BBRET_CustomerNo - Kundenummer.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BBRET_Currency - Standard ISO-koder. Hvis ikke dette feltet hentes, settes valuta til NOK. Dette feltet lagres ikke, men m� v�re lik kontoens valuta for at posten skal avstemmes.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BBRET_InvoiceNo - Fakturanummer.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BBRET_ClientNo - Klientnummer.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BBRET_Freetex - Fritekst.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BBRET_Name - Reskontronavn.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BBRET_MatchID - Normalt det som legges ut som unik ID mot �konomisystem (f.eks. KID).crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                            aDoc(1, lCounter) = aDoc(1, lCounter) & Replace("BBRET_MyField - Kan benyttes hvis det er et ekstra felt som trengs i forbindelse med oppdatering av �konomisystemet.crlf", "crlf", vbCrLf, , , CompareMethod.Text)
                        End If

                        ' Connection string
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "P�loggingsstreng" 'lrs(
                        aDoc(1, lCounter) = "&S"

                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        ' Profile body from form
                        aDoc(0, lCounter) = "P�loggingsstreng" 'lrs(
                        aDoc(1, lCounter) = aQueryArray(0, 0, 0)

                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "Automatisk avstemming" 'lrs(
                        aDoc(1, lCounter) = "&S"

                        For i = 1 To UBound(aQueryArray, 3)
                            ' blank linje between each rule
                            lCounter = lCounter + 2
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = aQueryArray(0, 4, i)
                            aDoc(1, lCounter) = aQueryArray(0, 0, i)

                            ' Find a description for each rule:
                            sMySQL = "SELECT SQLDescription FROM ERPQuery WHERE Company_ID=" & sCompanyID
                            sMySQL = sMySQL & " AND DBProfile_ID = " & aQueryArray(0, 3, 0)
                            sMySQL = sMySQL & " AND ERPQuery_ID = " & aQueryArray(0, 5, i)

                            oMyDal.SQL = sMySQL
                            If oMyDal.Reader_Execute() Then

                                Do While oMyDal.Reader_ReadRecord

                                    lCounter = lCounter + 2
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = "Beskrivelse:" 'lrs(
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = RetrieveIllegalCharactersInString(oMyDal.Reader_GetString("SQLDescription"))

                                    Exit Do
                                Loop

                            Else

                                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

                            End If

                        Next i

                        aQueryArray = GetERPDBInfo(1, 2) ' manual matching
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "Manuell avstemming" 'lrs(
                        aDoc(1, lCounter) = "&S"

                        lCounter = lCounter + 2
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "" 'lrs(
                        aDoc(1, lCounter) = "&B&GHurtigtaster i Manuell avstemming"

                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "" 'lrs(
                        aDoc(1, lCounter) = "Funksjonstasetene F1 til F6 benyttes p� kikkertene."
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "" 'lrs(
                        aDoc(1, lCounter) = "Ctrl+A for Avstemming"
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "" 'lrs(
                        aDoc(1, lCounter) = "Ctrl+O for Oppheving"
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "" 'lrs(
                        aDoc(1, lCounter) = "Ctrl+L for � slette markert Linje i avstemmingsforslaget "
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "" 'lrs(
                        aDoc(1, lCounter) = "Ctrl+B for � bla til f�rste post"
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "" 'lrs(
                        aDoc(1, lCounter) = "Ctrl+F for � bla til Forrige post"
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "" 'lrs(
                        aDoc(1, lCounter) = "Ctrl+N for � bla til Neste post "
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "" 'lrs(
                        aDoc(1, lCounter) = "Ctrl+S for � bla til Siste post "
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "" 'lrs(
                        aDoc(1, lCounter) = "Ctrl+P for � vise alle Posteringer"

                        For i = 1 To UBound(aQueryArray, 3)
                            If aQueryArray(0, 4, i) <> "Not in use" Then
                                ' blank linje between each rule
                                lCounter = lCounter + 2
                                ReDim Preserve aDoc(2, lCounter)
                                aDoc(0, lCounter) = aQueryArray(0, 4, i)
                                aDoc(1, lCounter) = aQueryArray(0, 0, i)

                                ' Find a description for each rule:
                                sMySQL = "SELECT SQLDescription FROM ERPQuery WHERE Company_ID=" & sCompanyID
                                sMySQL = sMySQL & " AND DBProfile_ID = " & aQueryArray(0, 3, 0)
                                sMySQL = sMySQL & " AND ERPQuery_ID = " & aQueryArray(0, 5, i)

                                oMyDal.SQL = sMySQL
                                If oMyDal.Reader_Execute() Then

                                    Do While oMyDal.Reader_ReadRecord

                                        lCounter = lCounter + 2
                                        ReDim Preserve aDoc(2, lCounter)
                                        aDoc(0, lCounter) = ""
                                        aDoc(1, lCounter) = "Beskrivelse:" 'lrs(
                                        lCounter = lCounter + 1
                                        ReDim Preserve aDoc(2, lCounter)
                                        aDoc(0, lCounter) = ""
                                        aDoc(1, lCounter) = oMyDal.Reader_GetString("SQLDescription")

                                    Loop
                                Else
                                    Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                                End If
                            End If
                        Next i

                        aQueryArray = GetERPDBInfo(1, 3) ' Aftermatching
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "Etterbehandling" 'lrs(
                        aDoc(1, lCounter) = "&S"

                        For i = 1 To UBound(aQueryArray, 3)
                            lCounter = lCounter + 2
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = aQueryArray(0, 4, i)
                            aDoc(1, lCounter) = aQueryArray(0, 0, i)

                            ' Find a description for each rule:
                            sMySQL = "SELECT SQLDescription FROM ERPQuery WHERE Company_ID=" & sCompanyID
                            sMySQL = sMySQL & " AND DBProfile_ID = " & aQueryArray(0, 3, 0)
                            sMySQL = sMySQL & " AND ERPQuery_ID = " & aQueryArray(0, 5, i)

                            oMyDal.SQL = sMySQL
                            If oMyDal.Reader_Execute() Then
                                Do While oMyDal.Reader_ReadRecord

                                    lCounter = lCounter + 2
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = "Beskrivelse:" 'lrs(
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = oMyDal.Reader_GetString("SQLDescription")

                                    Exit Do

                                Loop
                            Else
                                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                            End If
                        Next i

                        '    ' Heading and text before end
                        '    If Not EmptyString(frmDocumentation.txtEndHeader) Then
                        '        lCounter = lCounter + 1
                        '        ReDim Preserve aDoc(2, lCounter)
                        '        ' Endheader from form
                        '        aDoc(0, lCounter) = frmDocumentation.txtEndHeader.Text
                        '        aDoc(1, lCounter) = "&S"
                        '    End If
                        '    If Not EmptyString(frmDocumentation.txtEndBody) Then
                        '        lCounter = lCounter + 1
                        '        ReDim Preserve aDoc(2, lCounter)
                        '        ' Profile body from form
                        '        aDoc(0, lCounter) = ""
                        '        aDoc(1, lCounter) = Replace(frmDocumentation.txtEndBody, "crlf", vbCrLf, , , vbTextCompare)
                        '    End If

                        '**************************************************************************
                        'New 20.12.2007
                        aQueryArray = GetERPDBInfo(1, 4) ' Validation
                        lCounter = lCounter + 1
                        ReDim Preserve aDoc(2, lCounter)
                        aDoc(0, lCounter) = "Validering" 'lrs(
                        aDoc(1, lCounter) = "&S"

                        For i = 1 To UBound(aQueryArray, 3)
                            lCounter = lCounter + 2
                            ReDim Preserve aDoc(2, lCounter)
                            aDoc(0, lCounter) = aQueryArray(0, 4, i)
                            aDoc(1, lCounter) = aQueryArray(0, 0, i)

                            ' Find a description for each rule:
                            sMySQL = "SELECT SQLDescription FROM ERPQuery WHERE Company_ID=" & sCompanyID
                            sMySQL = sMySQL & " AND DBProfile_ID = " & aQueryArray(0, 3, 0)
                            sMySQL = sMySQL & " AND ERPQuery_ID = " & aQueryArray(0, 5, i)

                            oMyDal.SQL = sMySQL
                            If oMyDal.Reader_Execute() Then
                                Do While oMyDal.Reader_ReadRecord

                                    lCounter = lCounter + 2
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = "Beskrivelse:" 'lrs(
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(2, lCounter)
                                    aDoc(0, lCounter) = ""
                                    aDoc(1, lCounter) = oMyDal.Reader_GetString("SQLDescription")
                                    Exit Do

                                Loop
                            Else
                                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                            End If

                        Next i

                        ' Heading and text before end
                        If Not EmptyString((frmDocumentation.txtEndHeader).Text) Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            ' Endheader from form
                            aDoc(0, lCounter) = frmDocumentation.txtEndHeader.Text
                            aDoc(1, lCounter) = "&S"
                        End If
                        If Not EmptyString((frmDocumentation.txtEndBody).Text) Then
                            lCounter = lCounter + 1
                            ReDim Preserve aDoc(2, lCounter)
                            ' Profile body from form
                            aDoc(0, lCounter) = ""
                            aDoc(1, lCounter) = Replace(frmDocumentation.txtEndBody.Text, "crlf", vbCrLf, , , CompareMethod.Text)
                        End If
                        'End new code
                        '**************************************************************************
                    End If
                    If Not oMyDal Is Nothing Then
                        oMyDal.Close()
                        oMyDal = Nothing
                    End If

                End If

                ' Send to report 9001
                oBabelReport = New vbBabel.BabelReport
                oReport = New vbBabel.Report
                oReport.ReportNo = CShort("9001")
                oReport.Heading = LRS(60392)   '"BabelBank dokumentasjon"  'lrs(
                oReport.DetailLevel = 1
                'TODO: BABELREPORT
                'oBabelReport.ReportArray = VB6.CopyArray(aDoc)
                'oBabelReport.Report = oReport
                'oBabelReport.Start()

                oBabelReport = Nothing
                oReport = Nothing
            End If 'If Not frmDocumentation.bContinue Then

        Catch ex As Exception

            If Not oBabelReport Is Nothing Then
                oBabelReport = Nothing
            End If
            If Not oReport Is Nothing Then
                oReport = Nothing
            End If

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            MsgBox("En feil oppstod under produksjon av dokumentasjon:" & vbCrLf & vbCrLf & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "UVENTET FEIL")

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        If Not oBabelReport Is Nothing Then
            oBabelReport = Nothing
        End If
        If Not oReport Is Nothing Then
            oReport = Nothing
        End If

        frmDocumentation.Close()

    End Sub
    Private Sub BB_Statistics()
        Dim oBabelReport As vbBabel.BabelReport
        Dim oReport As vbBabel.Report

        ' run report rp_952_statistics

        ' Open database
        ' -------------
        'Set oDatabase = New vbBabel.Database
        'sMyFile = BB_DatabasePath
        'cnStat.ConnectionString = oDatabase.ConnectToProfile(sMyFile)
        'cnStat.Open
        '
        '' Ask for dateinterval
        '' --------------------
        '
        '' Select from table Statistics within dateinterval
        '' -------------------------------------------------
        'Set rsStat = New ADODB.Recordset
        'sMySQL = "SELECT * FROM Statistics WHERE Company_ID=" & sCompanyID & "Date_Run >= " & dFromDate & " AND Date_Run <= " & dToDate
        '
        'rsStat.Open sMySQL, cnStat, adOpenForwardOnly, adLockOptimistic
        '
        'If BB_RecordCount(rsStat, True) = 1 Then

        ' Fill array
        ' ----------

        ' Run report
        ' ----------
        oBabelReport = New vbBabel.BabelReport
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

        oBabelReport.ReportNumber = 952
        oBabelReport.Heading = "Statistikk posteringer" 'lrs(
        oBabelReport.Preview = True
        oBabelReport.ToPrint = False
        oBabelReport.AskForPrinter = False
        oBabelReport.ToEmail = False
        oBabelReport.ExportType = ""
        oBabelReport.ExportFilename = ""
        oBabelReport.ReportOnDatabase = True
        oBabelReport.SavebeforeReport = False
        'oBabelReport.BreakLevel = 4 ' BreakOnAccount
        oBabelReport.PrinterName = ""
        oBabelReport.UseLongDate = False

        'oBabelReport.BabelFiles = oBabelFiles
        oBabelReport.Special = ""
        oBabelReport.RunReport()
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        oBabelReport = Nothing


        'oReport = New vbBabel.Report
        'oReport.ReportNo = CShort("952")
        'oReport.Heading = "Statistikk posteringer" 'lrs(
        'oReport.DetailLevel = 1
        'oReport.Printer = False
        ''oBabelReport.ReportArray = aStat
        ''TODO: BABELREPORT
        ''oBabelReport.Report = oReport
        ''oBabelReport.Start()
        ''UPGRADE_NOTE: Object oBabelReport may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        'oBabelReport = Nothing
        ''UPGRADE_NOTE: Object oReport may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        'oReport = Nothing
        'End If

    End Sub
    Private Function FoundInFilesetupArray(ByRef oFilesetups As vbBabel.FileSetups, ByRef aFilesetupsHandled() As Short, ByRef iId As Short) As Boolean
        Dim i As Short
        ' Check if found in FilesetupArray (used in BBDocumentation
        For i = 0 To UBound(aFilesetupsHandled)
            If aFilesetupsHandled(i) = iId Then
                ' m� her teste om det kan v�re en returprofil som henger p� en sendeprofil -
                ' test med Filenameout1,2,3
                FoundInFilesetupArray = True
                Exit For
            Else
                ' Must have importfilename
                If EmptyString((oFilesetups(iId).FileNameIn1)) And EmptyString((oFilesetups(iId).FileNameIn2)) And EmptyString((oFilesetups(iId).FileNameIn3)) Then
                    FoundInFilesetupArray = True
                    Exit For
                End If
            End If
        Next i

    End Function
    Private Function FoundInClientArray(ByRef aClientsHandled() As Short, ByRef iId As Short) As Boolean
        Dim i As Short
        ' Check if found in ClientsArray (used in BBDocumentation)
        For i = 0 To UBound(aClientsHandled)
            If aClientsHandled(i) = iId Then
                FoundInClientArray = True
                Exit For
            End If
        Next i

    End Function


    Private Function WriteCollections(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef sFilename As String) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oBabel As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oFreeText As vbBabel.Freetext

        On Error GoTo errWriteCollections

        oFs = New Scripting.FileSystemObject

        oFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForWriting, True, 0)

        For Each oBabel In oBabelFiles
            With oBabel
                oFile.WriteLine((.FilenameIn))
                oFile.WriteLine((.File_id))
                oFile.WriteLine((.IDENT_Sender))
                oFile.WriteLine((.IDENT_Receiver))
                oFile.WriteLine((Str(.MON_TransferredAmount)))
                oFile.WriteLine((Str(.MON_InvoiceAmount)))
                'oFile.WriteLine (.AmountSetTransferred)
                'oFile.WriteLine (.AmountSetInvoice)
                oFile.WriteLine((Str(.No_Of_Transactions)))
                'oFile.WriteLine (Str(.No_Of_Records))
                oFile.WriteLine((.DATE_Production))
                oFile.WriteLine((Str(.ImportFormat)))
                oFile.WriteLine(CStr(.RejectsExists))
                'oFile.WriteLine (Str(.FilenameInNo))
                oFile.WriteLine(CStr(.FileFromBank))
                oFile.WriteLine((.Version))
                'oFile.WriteLine (.EDI_Format)
                oFile.WriteLine((.EDI_MessageNo))
                'oFile.WriteLine (Str(.Filesetup_ID))
                oFile.WriteLine((Str(.Index)))
                'oFile.WriteLine (Str(.Language))
                'oFile.WriteLine (.ERPSystem)
                oFile.WriteLine((Str(.BankByCode)))
                oFile.WriteLine((.StatusCode))
            End With
            For Each oBatch In oBabel.Batches
                With oBatch
                    oFile.WriteLine((Str(.SequenceNoStart)))
                    oFile.WriteLine((Str(.SequenceNoEnd)))
                    oFile.WriteLine((.StatusCode))
                    oFile.WriteLine((.Version))
                    oFile.WriteLine((.OperatorID))
                    oFile.WriteLine((.FormatType))
                    oFile.WriteLine((.I_EnterpriseNo))
                    oFile.WriteLine((.I_Branch))
                    oFile.WriteLine((.DATE_Production))
                    oFile.WriteLine((.REF_Own))
                    oFile.WriteLine((.REF_Bank))
                    oFile.WriteLine((Str(.MON_TransferredAmount)))
                    oFile.WriteLine((Str(.MON_InvoiceAmount)))
                    'oFile.WriteLine (.AmountSetTransferred)
                    'oFile.WriteLine (.AmountSetInvoice)
                    'oFile.WriteLine (Str(.No_Of_Transactions))
                    'oFile.WriteLine (Str(.No_Of_Records))
                    oFile.WriteLine((Str(.ImportFormat)))
                End With
                For Each oPayment In oBatch.Payments
                    With oPayment
                        oFile.WriteLine((.StatusCode))
                        oFile.WriteLine(CStr(.Cancel))
                        oFile.WriteLine((.DATE_Payment))
                        oFile.WriteLine((Str(.MON_TransferredAmount)))
                        'oFile.WriteLine (.AmountSetInvoice)
                        'oFile.WriteLine (.AmountSetTransferred)
                        oFile.WriteLine((.MON_TransferCurrency))
                        oFile.WriteLine((.REF_Own))
                        oFile.WriteLine((.REF_Bank1))
                        oFile.WriteLine((.REF_Bank2))
                        'oFile.WriteLine (.FormatType)
                        oFile.WriteLine((.E_Account))
                        oFile.WriteLine((.E_AccountPrefix))
                        oFile.WriteLine((.E_AccountSuffix))
                        oFile.WriteLine((.E_Name))
                        oFile.WriteLine((.E_Adr1))
                        oFile.WriteLine((.E_Adr2))
                        oFile.WriteLine((.E_Adr3))
                        oFile.WriteLine((.E_Zip))
                        oFile.WriteLine((.E_City))
                        oFile.WriteLine((.E_CountryCode))
                        oFile.WriteLine((.DATE_Value))
                        oFile.WriteLine(CStr(.Priority))
                        oFile.WriteLine((.I_Client))
                        oFile.WriteLine((.Text_E_Statement))
                        oFile.WriteLine((.Text_I_Statement))
                        oFile.WriteLine((Str(.Cheque)))
                        oFile.WriteLine(CStr(.ToOwnAccount))
                        oFile.WriteLine((.PayCode))
                        oFile.WriteLine((.PayType))
                        oFile.WriteLine((.I_Account))
                        oFile.WriteLine((.I_Name))
                        oFile.WriteLine((.I_Adr1))
                        oFile.WriteLine((.I_Adr2))
                        oFile.WriteLine((.I_Adr3))
                        oFile.WriteLine((.I_Zip))
                        oFile.WriteLine((.I_City))
                        oFile.WriteLine((.I_CountryCode))
                        oFile.WriteLine(CStr(.Structured))
                        oFile.WriteLine((.VB_ClientNo))
                        'oFile.WriteLine (.ExtraD1)
                        'oFile.WriteLine (.ExtraD2)
                        'oFile.WriteLine (.ExtraD3)
                        'oFile.WriteLine (.ExtraD4)
                        'oFile.WriteLine (.ExtraD5)
                        oFile.WriteLine((Str(.MON_LocalAmount)))
                        oFile.WriteLine((.MON_LocalCurrency))
                        oFile.WriteLine((Str(.MON_AccountAmount)))
                        oFile.WriteLine((.MON_AccountCurrency))
                        oFile.WriteLine((Str(.MON_InvoiceAmount)))
                        oFile.WriteLine((.MON_InvoiceCurrency))
                        oFile.WriteLine((Str(.MON_EuroAmount)))
                        oFile.WriteLine((Str(.MON_AccountExchRate)))
                        oFile.WriteLine((Str(.MON_LocalExchRate)))
                        oFile.WriteLine((Str(.MON_EuroExchRate)))
                        oFile.WriteLine((Str(.MON_ChargesAmount)))
                        oFile.WriteLine((.MON_ChargesCurrency))
                        oFile.WriteLine(CStr(.MON_ChargeMeDomestic))
                        oFile.WriteLine(CStr(.MON_ChargeMeAbroad))
                        oFile.WriteLine((Str(.ERA_ExchRateAgreed)))
                        oFile.WriteLine((.ERA_DealMadeWith))
                        oFile.WriteLine((Str(.FRW_ForwardContractRate)))
                        oFile.WriteLine((.FRW_ForwardContractNo))
                        oFile.WriteLine((.NOTI_NotificationMessageToBank))
                        oFile.WriteLine((.NOTI_NotificationType))
                        oFile.WriteLine((Str(.NOTI_NotificationParty)))
                        oFile.WriteLine((.NOTI_NotificationAttention))
                        oFile.WriteLine((.NOTI_NotificationIdent))
                        oFile.WriteLine((.BANK_SWIFTCode))
                        oFile.WriteLine((.BANK_BranchNo))
                        oFile.WriteLine((.BANK_Name))
                        oFile.WriteLine((.BANK_Adr1))
                        oFile.WriteLine((.BANK_Adr2))
                        oFile.WriteLine((.BANK_Adr3))
                        oFile.WriteLine((.BANK_CountryCode))
                        oFile.WriteLine((.BANK_SWIFTCodeCorrBank))
                        'oFile.WriteLine (.ExtraI1)
                        'oFile.WriteLine (.ExtraI2)
                        'oFile.WriteLine (.ExtraI3)
                        oFile.WriteLine((Str(.ImportFormat)))
                        'oFile.WriteLine (Str(.FilenameOut_ID))
                        'oFile.WriteLine (Str(.StatusSplittedFreetext))
                        oFile.WriteLine(CStr(.Exported))
                    End With
                    For Each oInvoice In oPayment.Invoices
                        With oInvoice
                            oFile.WriteLine((.StatusCode))
                            oFile.WriteLine(CStr(.Cancel))
                            oFile.WriteLine((Str(.MON_InvoiceAmount)))
                            oFile.WriteLine((Str(.MON_TransferredAmount)))
                            oFile.WriteLine((Str(.MON_AccountAmount)))
                            oFile.WriteLine((Str(.MON_LocalAmount)))
                            'oFile.WriteLine (.AmountSetTransferred)
                            'oFile.WriteLine (.AmountSetInvoice)
                            oFile.WriteLine((.REF_Own))
                            oFile.WriteLine((.REF_Bank))
                            oFile.WriteLine((.InvoiceNo))
                            oFile.WriteLine((.CustomerNo))
                            oFile.WriteLine((.Unique_Id))
                            oFile.WriteLine((.STATEBANK_Code))
                            oFile.WriteLine((.STATEBANK_Text))
                            oFile.WriteLine((.STATEBANK_DATE))
                            'oFile.WriteLine (.Extra1)
                            oFile.WriteLine(CStr(.MATCH_Original))
                            oFile.WriteLine(CStr(.MATCH_Final))
                            oFile.WriteLine(CStr(.MATCH_Matched))
                        End With
                        For Each oFreeText In oInvoice.Freetexts
                            oFile.WriteLine((Str(oFreeText.Qualifier)))
                            oFile.WriteLine((oFreeText.Text))
                        Next oFreeText
                    Next oInvoice
                Next oPayment
            Next oBatch
        Next oBabel

        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        Exit Function

errWriteCollections:
        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not oFs Is Nothing Then
            oFs = Nothing
        End If

        Err.Raise(12345, "Test", Err.Description)

    End Function
    Friend Function SetLockBabelBank(ByRef b As Boolean) As Boolean
        'Added 13.12.2008

        bLockBabelBank = b

    End Function
    Friend Function CreateShortcut(ByRef sName As String, ByRef sApplication As String) As Integer
        Dim shortcutName As String
        Dim creationDir As String
        Dim targetFullpath As String
        Dim workingDir As String

        CreateShortcut = 1  ' true

        shortcutName = sApplication & " " & sName
        creationDir = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
        targetFullpath = Application.ExecutablePath
        workingDir = Application.StartupPath


        Try
            If Not IO.Directory.Exists(creationDir) Then
                Dim retVal As DialogResult = MsgBox(creationDir & " does not exist. Do you wish to create it?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo)
                If retVal = DialogResult.Yes Then
                    IO.Directory.CreateDirectory(creationDir)
                End If
            End If

            Dim shortCut As IWshRuntimeLibrary.IWshShortcut
            Dim sObj As Object
            sObj = CreateObject("WScript.Shell")
            shortCut = CType(sObj.CreateShortcut(creationDir & "\" & shortcutName & ".lnk"), IWshRuntimeLibrary.IWshShortcut)
            shortCut.TargetPath = targetFullpath
            shortCut.Arguments = sName
            shortCut.WindowStyle = 1
            shortCut.Description = shortcutName
            shortCut.WorkingDirectory = workingDir
            'shortCut.IconLocation = iconFile & ", " & iconNumber
            shortCut.Save()
        Catch ex As System.Exception
            'Return False
            CreateShortcut = -1
        End Try

    End Function
    Private Sub SelectDatabase_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles SelectDatabase.Click
        ' Select another BabelBank database for setups involving more than one database
        ' Then we are using BabelBank.ini
        Dim sDatabaseName As String = ""

        sDatabaseName = BB_DatabasePathInitiate(bDEBUG)

        oProfile = New vbBabel.Profile
        oProfile.Load(1)

        ' 23.01.2017 - added databasename to frmStart.caption
        Dim oLicense As New vbBabel.License
        Me.Text = "BabelBank " & LRS(62013) & ": " & oLicense.LicName & " " & "Database: " & sDatabaseName
        oLicense = Nothing
        FillListboxes()

    End Sub
    Private Sub _mnuSupportFIleAnalyzis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _mnuSupportFIleAnalyzis.Click
        ' File analyzis of paymentfiles
        Dim myPreprocessfiles As New PreProcessFiles
        ' TESTING AV XLedger API
        'XLedgerTest()
        myPreprocessfiles.Analyze()

    End Sub
    Private Sub XLedgerTest()

        ' '' TESTING AV XLedger API
        'Dim mySoapClient As xLedgerAuthentication.AuthenticationSoapClient


        'Dim XLedgerAutentisering As New XLedgerAutentisering.AuthenticationSoapClient



        '        Dim mySoapClient As XLedgerAutentisering.
        'Dim sKey As String = ""
        'Dim sUID As String = "jan-petter@visualbanking.net"
        'Dim sPWD As String = "skeid2002"
        'Dim sApp As String = "XLEDGER"
        'Dim iEntity As Integer = 4953
        ''Dim myImport As XLedgerImport.ImportSoapClient
        ''Dim myExport As XLedgerExport.ExportSoapClient
        'Dim myExport As xLedgerExport.ExportSoapClient



        'Dim sXML As String = ""
        ''Dim myEndPointBinding As System.ServiceModel.BasicHttpBinding

        ''mySoapClient = New xLedgerAuthentication.AuthenticationSoapClient
        ''sKey = mySoapClient.LogonKey(sUID, sPWD, sApp)

        ''myImport = New XLedgerImport.ImportSoapClient()
        'myExport = New xLedgerExport.ExportSoapClient
        'Dim myEndPointBinding As New System.ServiceModel.BasicHttpBinding  '  .BasicHttpBinding
        'myEndPointBinding.Security.Transport.ClientCredentialType = ServiceModel.HttpClientCredentialType.None
        'myExport.Endpoint.Binding = myEndPointBinding

        'sXML = myExport.GetDebtCollectionItemDetailsData(sUID, sKey, sApp, iEntity, False, "", "", xLedgerExport.DataOption.Open)
        ' sxml=BabelWCFExport.GetInvoices(String0, Optional String1, Optional string2, .......)


        '=======================================================================================================================================

        ' Testing av ActiveBrands NAV2017 web services

        ' Test av ActiveBrands WEB Services

        'Dim BB_GetInvoiceItems_Service As New ActiveBrandsWCF.BB_GetInvoiceItems_Service
        'Dim BBFilter As New ActiveBrandsWCF.BB_GetInvoiceItems_Filter
        'BBFilter.Field = ActiveBrandsWCF.BB_GetInvoiceItems_Fields.Document_No
        'BBFilter.Criteria = ""
        'Dim bbFilters() As ActiveBrandsWCF.BB_GetInvoiceItems_Filter = New ActiveBrandsWCF.BB_GetInvoiceItems_Filter(0) {BBFilter}
        'Dim oReturn() As ActiveBrandsWCF.BB_GetInvoiceItems
        'Dim lCounter As Long = 0

        'BB_GetInvoiceItems_Service.Credentials = New Net.NetworkCredential("VisualBank", "Barnemat17", "skigutane")

        ''oReturn = BB_GetInvoiceItems_Service.ReadMultiple({}, vbNull, 10)
        'oReturn = BB_GetInvoiceItems_Service.ReadMultiple(bbFilters, Nothing, 0)

        'Dim sCustomer_No As String = ""
        'Dim sInvoice_No As String = ""
        'For lCounter = 0 To oReturn.GetUpperBound(0) - 1
        '    sCustomer_No = oReturn(lCounter).Customer_No
        '    sInvoice_No = oReturn(lCounter).Document_No
        'Next

    End Sub
    
    ''Private Sub mnuSEBISO20022Array_Click(ByVal Index As Integer)
    ''    Private Sub mnuSEBISO20022_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSEBISO20022.Click
    'Public Sub mnuSEBISO20022Array_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuSetupCustomArray.Click
    '    Dim Index As Short  'mnuSEBISO20022Array.GetIndex(eventSender)

    '    Dim i As Integer
    '    i = 1
    '    Select Case Index
    '        Case 0  ' "Setup structured payments

    '            ' fill info from CompanyTable into form
    '            frmSEBISO20022_SetupStructured.txtOCR.Text = oProfile.StructuredOCR
    '            frmSEBISO20022_SetupStructured.txtInvoice.Text = oProfile.StructuredInvoice
    '            frmSEBISO20022_SetupStructured.txtCreditNote.Text = oProfile.StructuredCreditNote
    '            frmSEBISO20022_SetupStructured.ShowDialog(Me)
    '            If frmSEBISO20022_SetupStructured.Save Then
    '                oProfile.StructuredOCR = frmSEBISO20022_SetupStructured.txtOCR.Text
    '                oProfile.StructuredInvoice = frmSEBISO20022_SetupStructured.txtInvoice.Text
    '                oProfile.StructuredCreditNote = frmSEBISO20022_SetupStructured.txtCreditNote.Text
    '            End If
    '        Case 1  ' "Misc. setup
    '            ' fill info from CompanyTable into form
    '            frmSEBISO20022_SetupMisc.txtHistoryDeleteDays.Text = oProfile.HistoryDelDays
    '            frmSEBISO20022_SetupMisc.chkTrimFreeText.Checked = oProfile.TrimFreetext
    '            frmSEBISO20022_SetupMisc.chkUseReturnConfirmation.Checked = oProfile.UseReturnConfirmation
    '            frmSEBISO20022_SetupMisc.chkUseReturnRejection.Checked = oProfile.UseReturnrejection
    '            frmSEBISO20022_SetupMisc.chkUseReturnPosting.Checked = oProfile.UseReturnPosting
    '            frmSEBISO20022_SetupMisc.ShowDialog(Me)
    '            If frmSEBISO20022_SetupMisc.Save Then
    '                oProfile.HistoryDelDays = frmSEBISO20022_SetupMisc.txtHistoryDeleteDays.Text
    '                oProfile.TrimFreetext = frmSEBISO20022_SetupMisc.chkTrimFreeText.Checked
    '                oProfile.UseReturnConfirmation = frmSEBISO20022_SetupMisc.chkUseReturnConfirmation.Checked
    '                oProfile.UseReturnrejection = frmSEBISO20022_SetupMisc.chkUseReturnRejection.Checked
    '                oProfile.UseReturnPosting = frmSEBISO20022_SetupMisc.chkUseReturnPosting.Checked
    '                oProfile.Status = vbBabel.Profile.CollectionStatus.Changed
    '                oProfile.Save(1)
    '                oProfile.Load(1)
    '            End If
    '    End Select

    'End Sub
    Private Sub mnuExtractSecureEnvelope_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExtractSecureEnvelope.Click
        ' Unpack a file with secureenvelope to a "clean" file (Pain.002, Camt.054)
        Dim dlg As OpenFileDialog
        Dim aFArray As String()
        Dim i As Integer
        Dim sOutFileName As String = ""
        Dim sTmp As String = ""
        Dim sTmp2 As String = ""
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim bt64 As Byte()
        Dim bGZIP As Boolean = False
        Dim iPos As Integer = 0
        Dim iPos2 As Integer = 0
        Dim bSuccess As Boolean = True
        Dim UTFEncodingWithoutBOM As System.Text.Encoding = New System.Text.UTF8Encoding(False)

        Try


            ' filedialog to select file
            ' return filename(s) within an array
            aFArray = Filenamepicker(Me.dlgFileNameOpen, True) ', aFilesArray(1, 0))
            If Not Array_IsEmpty(aFArray) Then
                For i = 0 To UBound(aFArray)
                    ' read file content
                    oFile = oFs.OpenTextFile(aFArray(i), Scripting.IOMode.ForReading)
                    sTmp = ""
                    sTmp = oFile.ReadAll
                    sTmp2 = sTmp

                    ' close file
                    oFile.Close()
                    oFile = Nothing
                    ' ------------------------------------------------------------------
                    ' find tag <Content>
                    ' keep content from start of packed text to end
                    If InStr(sTmp, "<Content>", CompareMethod.Text) > 0 Then
                        sTmp = Mid(sTmp, InStr(sTmp, "<Content>", CompareMethod.Text) + 9)
                        ' remove everything after packed content
                        sTmp = Mid(sTmp, 1, InStr(sTmp, "</Content>", CompareMethod.Text) - 1)

                        '
                        ' unpack
                        ' content is base64
                        bt64 = System.Convert.FromBase64String(sTmp)
                        '
                        ' check for GZIP
                        iPos = InStr(sTmp2, "GZIP", CompareMethod.Text)
                        If iPos > 0 Then
                            ' Compressed with GZIP
                            bGZIP = True
                        
                            If bGZIP Then
                                ' unzip base64 string;
                                bt64 = GZIPDeCompress(bt64)
                            End If
                        End If


                        ' convert to text
                        '09.03.2022 - Changed for Nordea Finance EQ
                        'Using ASCII created problems with ���, using UTF8 solved the issue
                        sTmp = System.Text.Encoding.UTF8.GetString(bt64)
                        'Old code
                        'sTmp = System.Text.Encoding.ASCII.GetString(bt64)

                        ' we may have problems with charset (ANSI/UTF8)
                        ' remove signs before first <, Ansi file should start with <?xml version=
                        If Mid(sTmp, 1, 1) <> "<" Then
                            sTmp = Mid(sTmp, InStr(sTmp, "<"))
                        End If
                        ' export to same folder
                        ' modify filename to have "unpacked" before .extension
                        If InStr(aFArray(i), ".") > 0 Then
                            sOutFileName = Mid(aFArray(i), 1, InStrRev(aFArray(i), ".") - 1) & "_Unpacked" & Mid(aFArray(i), InStrRev(aFArray(i), "."))
                        Else
                            sOutFileName = aFArray(i) & "_Unpacked"
                        End If

                        Using sw As New System.IO.StreamWriter(sOutFileName, False, UTFEncodingWithoutBOM)
                            'sw.Write(XMLDoc.OuterXml)
                            sw.Write(sTmp)
                            sw.Close()
                        End Using

                        'oFile = oFs.OpenTextFile(sOutFileName, Scripting.IOMode.ForWriting, True)
                        'oFile.Write(sTmp)

                        ' close file
                        'oFile.Close()
                        If Not oFile Is Nothing Then
                            oFile = Nothing
                        End If

                    Else
                        ' did not find <Content>
                        ' warning <<<<<<<<<<------------
                        ' Can't find <Content> tag in %1%. Is this a SecureEnvelope file?
                        MsgBox(LRS(62101, aFArray(i)), MsgBoxStyle.Exclamation)
                        bSuccess = False

                    End If

                Next i
                If bSuccess Then
                    ' Success
                    MsgBox(LRS(62102, System.IO.Path.GetDirectoryName(aFArray(0))), MsgBoxStyle.Information)
                End If

            End If

        Finally
            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If
            If Not oFs Is Nothing Then
                oFs = Nothing
            End If
            '
        End Try
    End Sub
    Private Sub mnuISO20022_SetupStructuredPayments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' fill info from CompanyTable into form
        frmSEBISO20022_SetupStructured.txtOCR.Text = oProfile.StructuredOCR
        frmSEBISO20022_SetupStructured.txtInvoice.Text = oProfile.StructuredInvoice
        frmSEBISO20022_SetupStructured.txtCreditNote.Text = oProfile.StructuredCreditNote
        frmSEBISO20022_SetupStructured.ShowDialog(Me)
        If frmSEBISO20022_SetupStructured.Save Then
            oProfile.StructuredOCR = frmSEBISO20022_SetupStructured.txtOCR.Text
            oProfile.StructuredInvoice = frmSEBISO20022_SetupStructured.txtInvoice.Text
            oProfile.StructuredCreditNote = frmSEBISO20022_SetupStructured.txtCreditNote.Text
            oProfile.Status = vbBabel.Profile.CollectionStatus.Changed
            oProfile.Save(1)
            oProfile.Load(1)
        End If

    End Sub

    Private Sub mnuSEBISO20022_MiscSetup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSEBISO20022_MiscSetup.Click
        ' fill info from CompanyTable into form
        frmSEBISO20022_SetupMisc.txtHistoryDeleteDays.Text = oProfile.HistoryDelDays
        frmSEBISO20022_SetupMisc.chkTrimFreeText.Checked = oProfile.TrimFreetext
        frmSEBISO20022_SetupMisc.chkUseReturnConfirmation.Checked = oProfile.UseReturnConfirmation
        frmSEBISO20022_SetupMisc.chkUseReturnRejection.Checked = oProfile.UseReturnrejection
        frmSEBISO20022_SetupMisc.chkUseReturnPosting.Checked = oProfile.UseReturnPosting
        frmSEBISO20022_SetupMisc.ChkSEPASingle.Checked = oProfile.SEPASingle
        frmSEBISO20022_SetupMisc.chkEndToEndRefFromBabelBank.checked = oProfile.EndToEndRefFromBabelBank
        frmSEBISO20022_SetupMisc.ShowDialog(Me)
        If frmSEBISO20022_SetupMisc.Save Then
            oProfile.HistoryDelDays = frmSEBISO20022_SetupMisc.txtHistoryDeleteDays.Text
            oProfile.TrimFreetext = frmSEBISO20022_SetupMisc.chkTrimFreeText.Checked
            oProfile.UseReturnConfirmation = frmSEBISO20022_SetupMisc.chkUseReturnConfirmation.Checked
            oProfile.UseReturnrejection = frmSEBISO20022_SetupMisc.chkUseReturnRejection.Checked
            oProfile.UseReturnPosting = frmSEBISO20022_SetupMisc.chkUseReturnPosting.Checked
            oProfile.SEPASingle = frmSEBISO20022_SetupMisc.ChkSEPASingle.Checked
            oProfile.EndToEndRefFromBabelBank = frmSEBISO20022_SetupMisc.chkEndToEndRefFromBabelBank.Checked
            oProfile.Status = vbBabel.Profile.CollectionStatus.Changed
            oProfile.Save(1)
            oProfile.Load(1)
        End If

    End Sub

    Private Sub mnuCopyDatabase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuCopyDatabase.Click
        Dim sPath As String = ""

        sPath = BrowseForFilesOrFolders("", Me, "Database", False)

        ' spath ends with end of string - remove
        ' Removed 18.10.05, do not end with space
        'spath = Left$(RTrim(spath), Len(RTrim(spath)) - 1)

        ' If browseforfilesorfolders returns empty path, then keep the old one;
        If Len(sPath) > 0 Then
            If MakeACopyOfBBDB(Nothing, False, vbBabel.BabelFiles.BackupType.Unknown, True, sPath, 10, False) Then
                MsgBox(LRS(62096), MsgBoxStyle.Information, "Backup)")
            Else
                'Errormessage already shown
            End If

        End If

    End Sub
    Private Sub mnuRestoreDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRestoreDatabase.Click
        ' restore xProfile database from backupfolder
        Dim sFolder As String
        Dim sFilename As String
        Dim bVersionCheck As Boolean

        Dim oBackupView As vbBabel.BackupView
        If oProfile.Backup Then
            oBackupView = New vbBabel.BackupView
            'oBackupView.BackupPath = oProfile.BackupPath
            oBackupView.BackupPath = oProfile.ExportBackuppath
            oBackupView.Profile = oProfile
            oBackupView.SetMode = "DATABASE"  ' signal that this is about show and restore Babelbanks database 


            oBackupView.Show()
            If oBackupView.ReturnValue = False Then
                ' No relevant files found in backuppath
                MsgBox(LRS(62075), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
            Else

                ' bruk den nye basen;
                oProfile = New vbBabel.Profile
                oProfile.Load(1)
                FillListboxes()

                ' vellykket tilbakekopiering - Ny database lastet!
                MsgBox(LRS(62074), MsgBoxStyle.Exclamation, "BabelBank")


            End If

        Else
            ' Backup inactive
            MsgBox(LRS(60069), MsgBoxStyle.OkOnly + MsgBoxStyle.Information) ' Backup not active. Activate in "Setup/Company"
        End If

    End Sub

 
    Private Sub CreateSQLServerDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CreateSQLServerDB.Click
        Dim sResponse As String
        Dim nYear As Double
        Dim nMonth As Double
        Dim nDay As Double
        Dim nCode As Double

        sResponse = InputBox(LRS(48033), , "XXXXXX")
        If sResponse.Length = 0 Then
            ' cancel pressed
            ' cut rest
        Else
            nYear = CDbl(Format(Now(), "yyyy"))
            nMonth = CDbl(Format(Now(), "MM"))
            nDay = CDbl(Format(Now(), "dd"))

            nCode = Math.Truncate(nYear / nDay * nMonth * 3.14)

            If Math.Truncate(CDbl(sResponse) - nCode) < 2 Then

                If CreateSQLServerDatabase() Then
                    MsgBox("Ny database ble vellykket opprettet!", MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
                Else

                End If
            End If
        End If

    End Sub

    Private Sub RunSQL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RunSQL.Click

        frmSQL.ShowDialog()

    End Sub
    Private Sub BB_LanguageSelect(ByRef sLanguage As String)
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sString As String
        Dim sCode As String
        Dim sLanguageAbr As String = "no"

        Select Case sLanguage
            Case "Norsk"
                sString = "Norsk spr�k valgt."
                sCode = "414"
                sLanguageAbr = "nn-NO"
            Case "English"
                sString = "English language selected."
                sCode = "809"
                sLanguageAbr = "en"
            Case "Dansk"
                sString = "Dansk spr�g valgt."
                sCode = "406"
                sLanguageAbr = "da"
            Case "Svenska"
                sString = "Svensk spr�k vald."
                sCode = "41d"
                sLanguageAbr = "se"
        End Select

        MsgBox(sString & vbCrLf & "Please restart to update language! /" & vbCrLf & "Vennligst lukk og start igjen for � oppdatere spr�k !", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Visma Payment (BabelBank)") 'TODO LRS(Me)

        ' update in configfile;
        ' this is temporary!
        System.Configuration.ConfigurationManager.AppSettings("Language") = sLanguageAbr
        ' update language to companytable;
        oProfile.bbLanguage = sLanguageAbr
        oProfile.Status = vbBabel.Profile.CollectionStatus.Changed
        oProfile.Save(1)


    End Sub
    Private Sub NorskToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NorskToolStripMenuItem.Click
        BB_LanguageSelect("Norsk")
    End Sub

    Private Sub EnglishToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnglishToolStripMenuItem.Click
        BB_LanguageSelect("English")
    End Sub

    Private Sub SvenskaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SvenskaToolStripMenuItem.Click
        BB_LanguageSelect("Svenska")
    End Sub

    Private Sub DanskToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DanskToolStripMenuItem.Click
        BB_LanguageSelect("Dansk")
    End Sub

    Private Sub SGFinansSafeModeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SGFinansSafeModeToolStripMenuItem.Click

        Dim sReturnCode As String
        Dim sReturnComment As String

        Try

            If verifyAccountOwner("", "", "987654321", "55550512345", sReturnCode, sReturnComment, True) Then
                MsgBox(sReturnCode & " - " & sReturnComment, MsgBoxStyle.OkOnly, "OK - KAR Check - OK")
            Else
                MsgBox(sReturnCode & " - " & sReturnComment, MsgBoxStyle.OkOnly, "OK - KAR Check - OK")
            End If

        Catch ex As Exception
            MsgBox("ERROR;" & vbCrLf & ex.Message, MsgBoxStyle.OkOnly, "FEIL - KAR Check - FEIL")

        Finally

        End Try

    End Sub
    Public Sub OsloSeafood_Import(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles OsloSeafood_ImportOpenItems.Click
        ' Oslo Seafood (Coast datter) har PowerOffice
        ' Vi kan ikke sp�rre mot PowerOffice, men isteden tar de ut en Excelfil med alle �pne poster fra Reskontro.
        ' Vi legger inn data inn i Access databasen PowerOfficeSkygge, som vi sp�r mot ved avstemming

        Dim bRetValue As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sProvider As String
        Dim sUID As String
        Dim sPWD As String
        Dim l As Long = 0
        Dim sImportLineBuffer As String = ""
        Dim sFileNameIn As String = ""

        Dim sCustomerNo As String = ""
        Dim scustomerName As String = ""
        Dim sInvoiceNo As String = ""
        Dim sDueDate As String = ""
        Dim sCurrency As String = ""
        Dim nAmount As Double = 0
        'Dim appExcel As Object
        'Dim oXLWBook As Object 'Microsoft.Office.Interop.Excel.Workbook
        'Dim oXLWSheet As Object  'Microsoft.Office.Interop.Excel.Worksheet

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream

        Dim aQueryArray(,,) As String


        Try
            sFileNameIn = Trim(oProfile.Custom1Value)

            ' open database
            oMyDal = New vbBabel.DAL
            aQueryArray = GetERPDBInfo(1, 1)
            sProvider = aQueryArray(0, 0, 0) ' Holds p�loggingsstreng, set in Profile


            oMyDal = New vbBabel.DAL
            oMyDal.Provider = aQueryArray(0, 9, 0)
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
            oMyDal.Connectionstring = sProvider
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ' start med � slette alle gamle poster
            sMySQL = "DELETE FROM ApnePoster"
            oMyDal.SQL = sMySQL
            oMyDal.ExecuteNonQuery()


            ' 18.03.2021 - Byttet fra Excel til .CSV file
            ' open excelfile
            'appExcel = New Microsoft.Office.Interop.Excel.Application
            'appExcel.DisplayAlerts = False

            'oXLWBook = appExcel.Workbooks.Open(getlongpath(sFileNameIn), , True)
            'oXLWSheet = oXLWBook.Worksheets(1)

            '' Reset rowno in  ReadLineFromExcel
            'ReadLineFromExcelMini(oXLWSheet, 0, -1)

            ' 18.03.2021 - Open .csv file
            oFs = New Scripting.FileSystemObject  '22.05.2017 CreateObject ("Scripting.FileSystemObject")
            oFile = oFs.OpenTextFile(sFileNameIn, Scripting.IOMode.ForReading, False)
            ' read ONE header line
            sImportLineBuffer = oFile.ReadLine

            l = 0

            If l < 50 Then
                Do

                    'sImportLineBuffer = ReadLineFromExcelMini(oXLWSheet, 20)
                    sImportLineBuffer = oFile.ReadLine

                    ' import from rows with open invoices
                    sCustomerNo = xDelim(sImportLineBuffer, ";", 1, Chr(34))
                    scustomerName = xDelim(sImportLineBuffer, ";", 2, Chr(34))
                    sInvoiceNo = xDelim(sImportLineBuffer, ";", 4, Chr(34))
                    ' 18.05.2021 - endret dato fra 14 til 15
                    If Len(xDelim(sImportLineBuffer, ";", 15, Chr(34)).Trim) > 8 Then
                        sDueDate = xDelim(sImportLineBuffer, ";", 15, Chr(34))   ' 10.01.2021
                        sDueDate = StringToDate(Mid(sDueDate, 7, 4) & Mid(sDueDate, 4, 2) & Mid(sDueDate, 1, 2))  ' String field
                        'sDueDate = Mid(sDueDate, 7, 4) & "-" & Mid(sDueDate, 4, 2) & "-" & Mid(sDueDate, 1, 2) & " 12:00:00"
                    End If
                    sCurrency = xDelim(sImportLineBuffer, ";", 19, Chr(34)) ' Q  18.05.2021, changed from 17 to 19, from S to Q
                    ' 25.03.2021 - bruk kolonne T Saldo, istedenfor kolonne R
                    ' 18.05.2021 - endret fra kolonne T til V (20 til 22)
                    nAmount = CDbl(xDelim(sImportLineBuffer, ";", 22, Chr(34))) * 100

                    If Len(sCustomerNo) > 4 And Len(sInvoiceNo) > 4 Then
                        sMySQL = "INSERT INTO ApnePoster ( ClientNo, CustomerNo, CustomerName, InvoiceNo, DueDate, ISO, Amount ) VALUES ( 1, '" & sCustomerNo & "', '" & scustomerName & "', '" & sInvoiceNo & "', '" & sDueDate & "','" & sCurrency & "'," & nAmount & ")"
                        oMyDal.SQL = sMySQL
                        oMyDal.ExecuteNonQuery()
                    End If
                    'End If

                    If oFile.AtEndOfStream Then
                        Exit Do
                    End If
                Loop
            End If

        Catch ex As Exception

  
            Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch

        Finally

            'oXLWBook.close()
            'oXLWBook = Nothing
            'appExcel = Nothing

            oFile.Close()
            oFile = Nothing
            oFs = Nothing

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

        End Try

        MsgBox("Vellykket oppdatering av skyggedatabase.", MsgBoxStyle.Exclamation)

    End Sub
    Public Sub LowellMasterAccounts(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuLowellMasterAccount.Click
        ' Lowell OY
        ' Import accounts list into BabelBanks Accounts table

        Dim bRetValue As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sProvider As String
        Dim sUID As String
        Dim sPWD As String
        Dim l As Long = 0
        Dim sImportLineBuffer As String = ""
        Dim sFileNameIn As String = ""

        Dim sIBAN As String = ""
        Dim sBBAN As String = ""
        Dim sCurrency As String = ""
        Dim sBIC As String = ""
        Dim sCountryCode As String = ""

        'Dim appExcel As Object
        'Dim oXLWBook As Object 'Microsoft.Office.Interop.Excel.Workbook
        'Dim oXLWSheet As Object  'Microsoft.Office.Interop.Excel.Worksheet
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sTmp As String = ""


        Dim aQueryArray(,,) As String


        Try
            sFileNameIn = Trim(oProfile.Custom1Value)
            If System.IO.File.Exists(sFileNameIn) Then

                ' open database
                oMyDal = New vbBabel.DAL
                oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                If Not oMyDal.ConnectToDB() Then
                    Throw New System.Exception(oMyDal.ErrorMessage)
                End If

                ' start med � slette alle gamle poster i Accounts
                sMySQL = "DELETE FROM Account"
                oMyDal.SQL = sMySQL
                oMyDal.ExecuteNonQuery()

                ' Import from .csv (01.03.2021)

                '' open excelfile
                'appExcel = New Microsoft.Office.Interop.Excel.Application
                'appExcel.DisplayAlerts = False

                'oXLWBook = appExcel.Workbooks.Open(getlongpath(sFileNameIn), , True)
                'oXLWSheet = oXLWBook.Worksheets(1)

                '' Reset rowno in  ReadLineFromExcel
                'ReadLineFromExcelMini(oXLWSheet, 0, -1)

                oFs = New Scripting.FileSystemObject
                oFile = oFs.OpenTextFile(sFileNameIn, Scripting.IOMode.ForReading, False, )
                ' 2 header lines
                'sImportLineBuffer = ReadLineFromExcelMini(oXLWSheet, 6)
                'sImportLineBuffer = ReadLineFromExcelMini(oXLWSheet, 6)
                sImportLineBuffer = oFile.ReadLine
                sImportLineBuffer = oFile.ReadLine

                l = 1
                Do

                    'sImportLineBuffer = ReadLineFromExcelMini(oXLWSheet, 20)
                    sImportLineBuffer = oFile.ReadLine

                    If xDelim(sImportLineBuffer, ";", 1) = vbNullString And xDelim(sImportLineBuffer, ";", 2) = vbNullString Then
                        ' jump out on first empty line
                        Exit Do
                    End If
                    l = l + 1

                    ' import from rows with open invoices
                    sIBAN = xDelim(sImportLineBuffer, ";", 1)
                    ' 24.03.2021
                    ' There can be "shitty" characters in the IBAN account info. Remove it.
                    sTmp = Mid(sIBAN, 3) ' take the part after FI
                    ' keep digist only
                    sTmp = KeepNumericsOnly(sTmp)
                    sIBAN = Mid(sIBAN, 1, 2) & sTmp


                    'sBBAN = xDelim(sImportLineBuffer, ";", 2)
                    ' changed 24.03.2021 due to the above char problems
                    sBBAN = sIBAN

                    ' er IBAN i b�de A og B, gj�r om til BBAN
                    If Len(sIBAN) > 6 And Mid(sIBAN, 1, 2) = "FI" Then
                        sBBAN = Mid(sIBAN, 5)
                    End If
                    sBIC = xDelim(sImportLineBuffer, ";", 3)
                    sCurrency = xDelim(sImportLineBuffer, ";", 4)
                    sCountryCode = xDelim(sImportLineBuffer, ";", 5)

                    ' 25.02.2021 - do not set IBAN in ConvertedAccount before Lowell tells us to do so
                    sMySQL = "INSERT INTO Account ( Company_ID, Client_ID, Account_ID, Account, SWIFTAddr, CurrencyCode ) VALUES (1, 1, " & l.ToString & ", '" & sBBAN & "', '" & sBIC & "', '" & sCurrency & "'" & ")"
                    oMyDal.SQL = sMySQL
                    oMyDal.ExecuteNonQuery()

                    If oFile.AtEndOfStream Then
                        Exit Do
                    End If
                Loop
            Else
                MsgBox("File " & sFileNameIn & " not found!", MsgBoxStyle.Exclamation)
            End If


        Catch ex As Exception


            Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch

        Finally

            If System.IO.File.Exists(sFileNameIn) Then

                MsgBox("Accounts list imported successfully", MsgBoxStyle.Exclamation)
                oFile.Close()
                oFile = Nothing
                oFs = Nothing

                'oXLWBook.close()
                'oXLWBook = Nothing
                'appExcel = Nothing

                If Not oMyDal Is Nothing Then
                    oMyDal.Close()
                    oMyDal = Nothing
                End If
            End If
        End Try

    End Sub

    Public Function ReadLineFromExcelMini(ByRef oExcelSheet As Microsoft.Office.Interop.Excel.Worksheet, ByRef lNoOfCols As Integer, Optional ByRef lNoOfRows As Integer = 99999999) As String
        ' Return a semicolonseparated line of cells from an excel worksheet
        Static lRowNo As Integer
        Dim i As Integer
        Dim sLine As String
        Dim sTmp As String

        ' Must reset lRowNo for each new file
        If lNoOfRows = -1 Then
            lRowNo = 0
        Else
            lRowNo = lRowNo + 1
            If lRowNo > lNoOfRows Then
                sLine = ""
            Else
                For i = 1 To lNoOfCols
                    sTmp = Replace(oExcelSheet.Cells(lRowNo, i).Value, ";", ",", , , vbTextCompare)
                    sLine = sLine & ";" & sTmp
                Next i
                ' Check if all cells are empty
                If Len(Replace(sLine, ";", "")) = 0 Then
                    sLine = ""
                Else
                    ' Remove first ;
                    sLine = Mid(sLine, 2)
                End If
            End If
        End If
        ReadLineFromExcelMini = sLine
    End Function

    Private Sub mnuAnonymizeISO20022_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAnonymizeISO20022.Click
        Dim dlg As OpenFileDialog
        Dim aFArray As String()
        Dim UTFEncodingWithoutBOM As System.Text.Encoding = New System.Text.UTF8Encoding(False)
        Dim sOutFileName As String = ""
        Dim XMLDoc As System.Xml.XmlDocument
        Dim XMLSetupDoc As New System.Xml.XmlDocument
        Dim nodeSDTags As System.Xml.XmlNodeList
        Dim iSDTagsCounter As Integer
        Dim nodeSDTag As System.Xml.XmlNode
        Dim nodeSDExceptions As System.Xml.XmlNodeList
        Dim iSDTagsExceptionsCounter As Integer
        Dim nodeSDException As System.Xml.XmlNode
        Dim nodeList As System.Xml.XmlNodeList
        Dim i As Integer
        Dim node As System.Xml.XmlNode
        Dim bSuccess As Boolean
        Dim eFormat As vbBabel.BabelFiles.FileType
        Dim oFs As New Scripting.FileSystemObject
        Dim sXPath As String
        Dim sDefaultValue As String

        Dim nsMgr As System.Xml.XmlNamespaceManager
        Dim dicSpecificValues As New System.Collections.Generic.Dictionary(Of String, String)
        Dim sOriginalValue As String
        Dim sNewValue As String


        Try

            'FJORDKRAFT UTBETALING - fjernng av KID
            'If 1 = 1 Then
            '    Dim sImportLineBuffer As String
            '    Dim oFileRead As Scripting.TextStream
            '    Dim oFileWrite As Scripting.TextStream
            '    Dim iPos As Integer

            '    aFArray = Filenamepicker(Me.dlgFileNameOpen, True) ', aFilesArray(1, 0))

            '    oFs = New Scripting.FileSystemObject
            '    oFileRead = oFs.OpenTextFile(aFArray(0), Scripting.IOMode.ForReading, True, 0)
            '    oFileWrite = oFs.OpenTextFile(aFArray(0) & "_uten KID", Scripting.IOMode.ForWriting, True, 0)

            '    Do While oFileRead.AtEndOfStream = False
            '        sImportLineBuffer = oFileRead.ReadLine()
            '        iPos = 0
            '        iPos = sImportLineBuffer.LastIndexOf(";")
            '        If iPos > 0 Then
            '            sImportLineBuffer = sImportLineBuffer.Substring(0, iPos + 1)
            '        Else
            '            sImportLineBuffer = sImportLineBuffer
            '        End If
            '        oFileWrite.WriteLine(sImportLineBuffer)
            '    Loop

            '    oFileRead.Close()
            '    oFileRead = Nothing
            '    oFileWrite.Close()
            '    oFileWrite = Nothing
            '    oFs = Nothing

            'End If

            If RunTime() Then
                If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\AnonymizeISO20022.xml") Then
                    Throw New Exception("mnuAnonymizeISO20022 - Kan ikke finne filen " & My.Application.Info.DirectoryPath & "\Mapping\AnonymizeISO20022.xml, " & vbCrLf & "som benyttes til anonymisering av ISO20022-filer.")
                Else
                    XMLSetupDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml")

                End If
            Else
                If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\AnonymizeISO20022.xml") Then
                    Throw New Exception("mnuAnonymizeISO20022 - Kan ikke finne filen " & My.Application.Info.DirectoryPath & "\Mapping\AnonymizeISO20022.xml" & vbCrLf & "som benyttes til anonymisering av ISO20022-filer.")
                Else
                    XMLSetupDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\AnonymizeISO20022.xml")
                End If
            End If

            aFArray = Filenamepicker(Me.dlgFileNameOpen, True)
            If Not Array_IsEmpty(aFArray) Then
                For i = 0 To UBound(aFArray)

                    eFormat = DetectFileFormat(aFArray(i), True, "")

                    XMLDoc = New System.Xml.XmlDocument
                    XMLDoc.Load(aFArray(i))

                    nsMgr = New System.Xml.XmlNamespaceManager(XMLDoc.NameTable)
                    nsMgr.AddNamespace("ISO", XMLDoc.DocumentElement.NamespaceURI)

                    Select Case eFormat

                        Case vbBabel.BabelFiles.FileType.Camt053, vbBabel.BabelFiles.FileType.Camt053_Incoming, vbBabel.BabelFiles.FileType.Camt053_Outgoing
                            nodeSDTags = Nothing
                            nodeSDTags = XMLSetupDoc.SelectNodes("/Document/Camt.053/Tag[@Active='True']")

                        Case vbBabel.BabelFiles.FileType.Camt054, vbBabel.BabelFiles.FileType.Camt054_Incoming, vbBabel.BabelFiles.FileType.Camt054_Outgoing
                            nodeSDTags = Nothing
                            nodeSDTags = XMLSetupDoc.SelectNodes("/Document/Camt.054/Tag[@Active='True']")

                        Case vbBabel.BabelFiles.FileType.Pain001
                            nodeSDTags = Nothing
                            nodeSDTags = XMLSetupDoc.SelectNodes("/Document/Pain.001/Tag[@Active='True']")


                        Case Else


                    End Select

                    For Each nodeSDTag In nodeSDTags
                        node = nodeSDTag.SelectSingleNode("Value")
                        sXPath = node.InnerText
                        node = nodeSDTag.SelectSingleNode("Default")
                        sDefaultValue = node.InnerText

                        nodeSDExceptions = nodeSDTag.SelectNodes("Match")
                        dicSpecificValues.Clear()
                        For Each nodeSDException In nodeSDExceptions
                            sOriginalValue = ""
                            sNewValue = ""
                            sOriginalValue = GetInnerText(nodeSDException, "Original", nsMgr)
                            sNewValue = GetInnerText(nodeSDException, "New", nsMgr)
                            If Not EmptyString(sOriginalValue) And Not EmptyString(sNewValue) Then
                                If Not dicSpecificValues.ContainsKey(sOriginalValue) Then
                                    dicSpecificValues.Add(sOriginalValue, sNewValue)
                                End If
                            End If
                        Next nodeSDException

                        nodeList = Nothing
                        nodeList = XMLDoc.SelectNodes(sXPath, nsMgr)
                        For Each node In nodeList
                            If dicSpecificValues.ContainsKey(node.InnerText) Then
                                node.InnerText = dicSpecificValues(node.InnerText)
                            Else
                                node.InnerText = sDefaultValue
                            End If

                        Next node

                    Next nodeSDTag


                    If InStr(aFArray(i), ".") > 0 Then
                        sOutFileName = Mid(aFArray(i), 1, InStrRev(aFArray(i), ".") - 1) & "_Anonymized" & Mid(aFArray(i), InStrRev(aFArray(i), "."))
                    Else
                        sOutFileName = aFArray(i) & "_Anonymized"
                    End If

                    Using sw As New System.IO.StreamWriter(sOutFileName, False, UTFEncodingWithoutBOM)
                        'sw.Write(XMLDoc.OuterXml)
                        sw.Write(XMLDoc.OuterXml)
                        sw.Close()
                    End Using


                    ' close file
                    'oFile.Close()
                    'If Not oFile Is Nothing Then
                    'oFile = Nothing
                    'End If

                    '    Else
                    '' did not find <Content>
                    '' warning <<<<<<<<<<------------
                    '' Can't find <Content> tag in %1%. Is this a SecureEnvelope file?
                    'MsgBox(LRS(62101, aFArray(i)), MsgBoxStyle.Exclamation)
                    bSuccess = True

                Next i
            End If

            If bSuccess Then
                ' Success
                MsgBox(LRS(62103, System.IO.Path.GetDirectoryName(aFArray(0))), MsgBoxStyle.Information)
            End If

        Catch ex As Exception


        Finally
            If Not XMLDoc Is Nothing Then
                XMLDoc = Nothing
            End If
            If Not nsMgr Is Nothing Then
                nsMgr = Nothing
            End If

        End Try

    End Sub
End Class
