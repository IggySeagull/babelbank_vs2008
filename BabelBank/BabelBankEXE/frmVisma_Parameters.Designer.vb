<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmVisma_Parameters
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents mnuDanish As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuEnglish As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuNorwegian As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFinnish As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSwedish As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuLanguagePopUp As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVisma_Parameters))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MenuStrip
        Me.mnuLanguagePopUp = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDanish = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEnglish = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNorwegian = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFinnish = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSwedish = New System.Windows.Forms.ToolStripMenuItem
        Me.gridParameters = New System.Windows.Forms.DataGridView
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.cmdClose = New System.Windows.Forms.Button
        Me.cmdSave = New System.Windows.Forms.Button
        Me.MainMenu1.SuspendLayout()
        CType(Me.gridParameters, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLanguagePopUp})
        Me.MainMenu1.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu1.Name = "MainMenu1"
        Me.MainMenu1.Size = New System.Drawing.Size(507, 24)
        Me.MainMenu1.TabIndex = 9
        '
        'mnuLanguagePopUp
        '
        Me.mnuLanguagePopUp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDanish, Me.mnuEnglish, Me.mnuNorwegian, Me.mnuFinnish, Me.mnuSwedish})
        Me.mnuLanguagePopUp.Name = "mnuLanguagePopUp"
        Me.mnuLanguagePopUp.Size = New System.Drawing.Size(12, 20)
        '
        'mnuDanish
        '
        Me.mnuDanish.Name = "mnuDanish"
        Me.mnuDanish.Size = New System.Drawing.Size(143, 22)
        Me.mnuDanish.Text = "&Dansk"
        '
        'mnuEnglish
        '
        Me.mnuEnglish.Name = "mnuEnglish"
        Me.mnuEnglish.Size = New System.Drawing.Size(143, 22)
        Me.mnuEnglish.Text = "&English"
        '
        'mnuNorwegian
        '
        Me.mnuNorwegian.Name = "mnuNorwegian"
        Me.mnuNorwegian.Size = New System.Drawing.Size(143, 22)
        Me.mnuNorwegian.Text = "&Norsk"
        '
        'mnuFinnish
        '
        Me.mnuFinnish.Name = "mnuFinnish"
        Me.mnuFinnish.Size = New System.Drawing.Size(143, 22)
        Me.mnuFinnish.Text = "&Suomalainen"
        '
        'mnuSwedish
        '
        Me.mnuSwedish.Name = "mnuSwedish"
        Me.mnuSwedish.Size = New System.Drawing.Size(143, 22)
        Me.mnuSwedish.Text = "S&venska"
        '
        'gridParameters
        '
        Me.gridParameters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridParameters.Location = New System.Drawing.Point(32, 81)
        Me.gridParameters.Name = "gridParameters"
        Me.gridParameters.Size = New System.Drawing.Size(454, 270)
        Me.gridParameters.TabIndex = 10
        '
        'cmdAdd
        '
        Me.cmdAdd.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAdd.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAdd.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAdd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAdd.Location = New System.Drawing.Point(77, 368)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAdd.Size = New System.Drawing.Size(100, 24)
        Me.cmdAdd.TabIndex = 39
        Me.cmdAdd.Text = "55042 - Legg til"
        Me.cmdAdd.UseVisualStyleBackColor = False
        '
        'cmdDelete
        '
        Me.cmdDelete.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDelete.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDelete.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDelete.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDelete.Location = New System.Drawing.Point(180, 368)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDelete.Size = New System.Drawing.Size(100, 24)
        Me.cmdDelete.TabIndex = 37
        Me.cmdDelete.Text = "55043-Slett"
        Me.cmdDelete.UseVisualStyleBackColor = False
        '
        'cmdClose
        '
        Me.cmdClose.BackColor = System.Drawing.SystemColors.Control
        Me.cmdClose.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClose.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdClose.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClose.Location = New System.Drawing.Point(386, 368)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClose.Size = New System.Drawing.Size(100, 24)
        Me.cmdClose.TabIndex = 35
        Me.cmdClose.Text = "35118-Lukk"
        Me.cmdClose.UseVisualStyleBackColor = False
        '
        'cmdSave
        '
        Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Location = New System.Drawing.Point(283, 368)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSave.Size = New System.Drawing.Size(100, 24)
        Me.cmdSave.TabIndex = 34
        Me.cmdSave.Text = "55034-Lagre"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'frmVisma_Parameters
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(507, 409)
        Me.Controls.Add(Me.cmdAdd)
        Me.Controls.Add(Me.cmdDelete)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.gridParameters)
        Me.Controls.Add(Me.MainMenu1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(10, 32)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmVisma_Parameters"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "35015-Parameter"
        Me.MainMenu1.ResumeLayout(False)
        Me.MainMenu1.PerformLayout()
        CType(Me.gridParameters, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gridParameters As System.Windows.Forms.DataGridView
    Public WithEvents cmdAdd As System.Windows.Forms.Button
    Public WithEvents cmdDelete As System.Windows.Forms.Button
    Public WithEvents cmdClose As System.Windows.Forms.Button
    Public WithEvents cmdSave As System.Windows.Forms.Button
#End Region 
End Class