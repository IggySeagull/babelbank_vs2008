﻿Imports System.Threading
Module lrs_BBExe
    Public Function LRS(ByVal StringIndex As Long, Optional ByVal sString1 As String = vbNullString, Optional ByVal sString2 As String = vbNullString, Optional ByVal sString3 As String = vbNullString) As String
        Dim sReturnString As String
        Dim sLanguage As String
        Dim sNorwegian As String = ""
        Dim sEnglish As String = ""
        Dim sSwedish As String = ""
        Dim sDanish As String = ""
        Dim bUseNorwegian As Boolean = False
        Dim bUseDanish As Boolean = False
        Dim bUseSwedish As Boolean = False
        Dim sTmp As String
        Dim oFS As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Static sLanguageFromFile As String = ""

        sReturnString = ""
        'sLanguage = Thread.CurrentThread.CurrentCulture.ToString

        sTmp = System.Configuration.ConfigurationManager.AppSettings("Language")

        If sTmp = "" Then
            sLanguage = Thread.CurrentThread.CurrentCulture.ToString
        Else
            sLanguage = sTmp
        End If
        ' Check if we have "Language.txt" file with Language code set in file.
        If sLanguageFromFile = "" Then
            ' check languagefile
            oFS = New Scripting.FileSystemObject
            If oFS.FileExists(My.Application.Info.DirectoryPath & "\language.txt") Then
                ' file found, import
                oFile = oFS.OpenTextFile(My.Application.Info.DirectoryPath & "\language.txt", Scripting.IOMode.ForReading)
                sLanguageFromFile = oFile.ReadAll
                oFile.Close()
                oFile = Nothing
                oFS = Nothing
            Else
                sLanguageFromFile = "no file"
            End If
        End If
        If sLanguageFromFile <> "no file" Then
            If sLanguageFromFile = "406" Then
                sLanguage = "da"
            ElseIf sLanguageFromFile = "41d" Then
                sLanguage = "se"
            ElseIf sLanguageFromFile = "414" Then
                sLanguage = "nn-NO"
            ElseIf sLanguageFromFile = "809" Then
                sLanguage = "en"
            End If
        End If


        If sLanguage = "nb-NO" Or sLanguage = "nn-NO" Then
            bUseNorwegian = True
        ElseIf sLanguage = "da" Then
            bUseDanish = True
        ElseIf sLanguage = "se" Then
            bUseSwedish = True
        Else
            bUseNorwegian = False
        End If

        Select Case StringIndex
            Case 15000
                sNorwegian = "--------(Error Messages) --------"
                sEnglish = "--------(Error Messages) --------"
                sSwedish = "--------(Error Messages) --------"
                sDanish = "--------(Error Messages) --------"
            Case 15001
                sNorwegian = "BABELBANK AVBRYTES!"
                sEnglish = "BABELBANK TERMINATES!"
                sSwedish = "BABELBANK AVBRYTS!"
                sDanish = "BABELBANK AFBRYDES!"
            Case 15002
                sNorwegian = "Ingen filer å importere."
                sEnglish = "No files to import."
                sSwedish = "Inga filer att importera."
                sDanish = "Ingen filer at importere."
            Case 15003
                sNorwegian = "Feil #"
                sEnglish = "Error #"
                sSwedish = "Fel #"
                sDanish = "Fejl #"
            Case 15004
                sNorwegian = "Vennligst kontakt din forhandler"
                sEnglish = "Contact Your dealer"
                sSwedish = "Kontakta din återförsäljare"
                sDanish = "Kontakt din forhandler"
            Case 15005
                sNorwegian = "Ingen profiler samsvarer med oppstartsparameteren"
                sEnglish = "No Profile fits the short-name given as a parameter:"
                sSwedish = "Inga profiler motsvarar startparametern"
                sDanish = "Der er ingen profiler, der svarer til opstartsparameteren"
            Case 15006
                sNorwegian = "BabelBank avsluttes"
                sEnglish = "BabelBank terminates"
                sSwedish = "BabelBank avslutas"
                sDanish = "BabelBank afsluttes"
            Case 15007
                sNorwegian = "Finner ikke database (Profile.mdb)"
                sEnglish = "Can not find database (Profile.mdb)"
                sSwedish = "Kan inte hitta databas (Profile.mdb)"
                sDanish = "Finder ikke database (Profile.mdb)"
            Case 15008
                sNorwegian = "Lisensfeil"
                sEnglish = "Licenseerror"
                sSwedish = "Licensfel"
                sDanish = "Licensfejl"
            Case 15009
                sNorwegian = "Programfeil:"
                sEnglish = "Programerror:"
                sSwedish = "Programfel:"
                sDanish = "Programfejl:"
            Case 15010
                sNorwegian = "Kilde:"
                sEnglish = "Source:"
                sSwedish = "Källa:"
                sDanish = "Kilde:"
            Case 15011
                sNorwegian = "Feil i sending av supportmail!"
                sEnglish = "Error in sending supportmail!"
                sSwedish = "Fel i utskick av supportpost!"
                sDanish = "Fejl i afsendelse af supportmail!"
            Case 15012
                sNorwegian = "Finner ikke BabelBanks database"
                sEnglish = "Cannot find BabelBank's database!"
                sSwedish = "Kan inte hitta BabelBanks databas"
                sDanish = "Kan ikke finde BabelBanks database"
            Case 15013
                sNorwegian = "Finner ikke BabelBanks lisensfil"
                sEnglish = "Cannot find BabelBank's licensefile!"
                sSwedish = "Kan inte hitta BabelBanks licensfil"
                sDanish = "Kan ikke finde BabelBanks licensfil"
            Case 15014
                sNorwegian = "Feil i registrert lisenskode"
                sEnglish = "Error in keyed in licensecode"
                sSwedish = "Fel i registrerad licenskod"
                sDanish = "Fejl i registreret licenskode"
            Case 15015
                sNorwegian = "15015: Kan ikke laste inn språkfilen."
                sEnglish = "15015: Failed to load the language-file."
                sSwedish = "15015: Kan inte läsa in språkfilen."
                sDanish = "15015: Kan ikke indlæse sprogfilen."
            Case 15016
                sNorwegian = "(UpDayTotals) Feil 'type' angitt. Dette vil gi feil i rapport for dagtotaler"
                sEnglish = "(UpDayTotals) Wrong 'type' given. Will cause error in report DayTotals"
                sSwedish = "(UpDayTotals) Fel 'typ' har angetts. Detta genererar fel i rapport om dagssummor"
                sDanish = "(UpDayTotals) Forkert 'type' angivet. Dette vil give fejl i rapport for dagtotaler"
            Case 15017
                sNorwegian = "(UpDayTotals) Feil datoformat (YYYMMDD) angitt. Dette vil gi feil i rapport for dagtotaler"
                sEnglish = "(UpDayTotals) Wrong dateformat (YYYYMMDD). Will cause error in report DayTotals."
                sSwedish = "(UpDayTotals) Fel datumformat (ÅÅÅMMDD) anges. Detta genererar fel i rapport om dagssummor"
                sDanish = "(UpDayTotals) Forkert datoformat (YYYMMDD) angivet. Dette vil give fejl i rapport for dagtotaler"
            Case 15018
                sNorwegian = "15018: Finner ikke avtale for klient %1 for valutakode %2"
                sEnglish = "15018: Can not find agreement for client %1 for currency %2"
                sSwedish = "15018: Hittar inte avtal för klient %1 om valutakod %2"
                sDanish = "15018: Kan ikke finde aftale for klient %1 for valutakode %2"
            Case 15019
                sNorwegian = "15019: Uspesifisert feil for klient %1 for valutakode %2"
                sEnglish = "15019: Unspecified error for client %1 for currency %2"
                sSwedish = "15019: Ospecificerat fel för klient %1 om valutakod %2"
                sDanish = "15019: Uspecificeret fejl for klient %1 for valutakode %2"
            Case 15020
                sNorwegian = "15020: En uventet feil oppstod. Ingen endringer ble lagret."
                sEnglish = "15020: An unexpected error occured. No records was saved."
                sSwedish = "15020: Ett oväntat fel inträffade. Inga ändringar sparas."
                sDanish = "15020: Der opstod en uventet fejl. Ingen ændringer gemt."
            Case 15021
                sNorwegian = "BabelBank blir vedlikeholdt, og du kan ikke benytte programmet til å postere innbetalinger." & vbLf & "Kontakt din driftsansvarlige hvis du har spørsmål."
                sEnglish = "BabelBank is being maintained, and is locked for posting payments." & vbLf & "Please contact the IT department."
                sSwedish = "Underhåll av BabelBank pågår och du kan inte använda programmet för att bokföra inbetalningar." & vbLf & "Kontakta driftsansvarig vid frågor.	"
                sDanish = "BabelBank bliver vedligeholdt, og du kan ikke benytte programmet til at postere indbetalinger." & vbLf & "Kontakt din driftsansvarlige hvis du har spørgsmål.	"
            Case 15022
                sNorwegian = "BabelBank er låst"
                sEnglish = "BabelBank is locked"
                sSwedish = "BabelBank är låst"
                sDanish = "BabelBank er låst"
            Case 15023
                sNorwegian = "15023: KRITISK FEIL: BabelBank kunne ikke oppdatere LastUsedBabelFileID."
                sEnglish = "15023: CRITICAL ERROR: BabelBank was not able to update LastUsedBabelFileID."
                sSwedish = "15023: KRITISKT FEL: BabelBank kunde inte uppdatera LastUsedBabelFileID."
                sDanish = "15023: KRITISK FEIL: BabelBank kunne ikke opdatere LastUsedBabelFileID."
            Case 15024
                sNorwegian = "Du må først sette filstier for både original og ny mappe i menyvalget 'Handling' / 'Endre filstier'."
                sEnglish = "Please state both an original path and a new path." & vbCrLf & "You find this in the menu: 'Action' / 'Change of filepath'."
                sSwedish = "Du måste först ställa in sökvägar för både den ursprungliga och ny mapp i menyvalet" 'Åtgärd' / 'Förändring sökvägar'."
                sDanish = "Du skal først indstille filstier for både den oprindelige og den nye mappe i menuen 'Action' / 'Ændring af filsti'."
            Case 15025
                sNorwegian = "Manglende sti."
                sEnglish = "Missing path."
                sSwedish = "Saknad väg."
                sDanish = "Manglende sti."
            Case 15026
                sNorwegian = "Vellykket endring av filstier." & vbCrLf & "%1 filstier ble endret." & vbCrLf & vbCrLf & "NB! Vennligst start BabelBank på nytt for å aktivere endringene."
                sEnglish = "The changes in filepaths were successful." & vbCrLf & "%1 paths were changed." & vbCrLf & vbCrLf & "NB! Please restart BabelBank to make the changes effective."
                sSwedish = "Lyckad ändring av filsökvägar." & vbCrLf & "% 1 sökvägar har ändrats." & vbCrLf & vbCrLf & "NB Börja BabelBank igen för att tillämpa ändringarna."
                sDanish = "Vellykket ændring af fil stier." & vbCrLf & "% 1 fil stier blev ændret." & vbCrLf & vbCrLf & "NB venligst starte BabelBank igen for at anvende ændringerne."
            Case 15027
                sNorwegian = "FILSTIER ENDRET"
                sEnglish = "PATHS CHANGED"
                sSwedish = "SÖKVÄGAR ÄNDRAD"
                sDanish = "FILSTIER ÆNDRET"
            Case 15028
                sNorwegian = "BabelBank kunne ikke endre filstier i %1-tabellen."
                sEnglish = "BabelBank failed to change paths in the %1-table."
                sSwedish = "BabelBank misslyckades med att ändra filsökvägar i %1-tabell."
                sDanish = "BabelBank undladt at ændre filstier i %1-tabel."
            Case 15029
                sNorwegian = "KUNNE IKKE UTFØRE ENDRING"
                sEnglish = "ERROR DURING EXCHANGE OF FILEPATHS"
                sSwedish = "KUNDE INTE UTFÖRA FÖRÄNDRING"
                sDanish = "KUNNE IKKE UDFOERE AENDRING"
            Case 15030
                sNorwegian = "Feilmelding:"
                sEnglish = "Errormessage:"
                sSwedish = "Felmeddelande:"
                sDanish = "Fejlmeddelelse:"

            Case 15031
                sNorwegian = "Feilmelding fra BabelBank"
                sEnglish = "Errormessage from BabelBank"
                sSwedish = "Felmeddelande från BabelBank"
                sDanish = "Fejlmeddelelse fra BabelBank"
            Case 15032
                sNorwegian = "Feil i filsti for logging"
                sEnglish = "Error in filepath for loging"
                sSwedish = "Fel i sökvägen för loggning"
                sDanish = "Fejl i filstien til logning"
            Case 15033
                sNorwegian = "Feil i filsti for signalfil"
                sEnglish = "Error in filepath for signalfile"
                sSwedish = "Feil i sökvägen för signalfil"
                sDanish = "Feil i filsti af signalfil"
            Case 15034
                sNorwegian = "Feil i filsti for eksportfil"
                sEnglish = "Error in filepath for exportfile"
                sSwedish = "Feil i sökvägen for exportfil"
                sDanish = "Feil i filsti for eksport-fil"
            Case 15035
                sNorwegian = "Feil i filsti for utbytting av fil"
                sEnglish = "Error in filepath for fileswapping"
                sSwedish = "Feil i sökvägen for byta för fil"
                sDanish = "Feil i filsti for switching af fil"
            Case 15036
                sNorwegian = "BabelBank fant Company%1, %2, men klarer ikke å finne påloggingsstrengen til databasen." & vbCrLf & "Vennligst legg inn påloggingsstrengen som BB_CONNECTIONSTRING%1 i filen BabelBank.exe.config"
                sEnglish = "BabelBank has found Company%1, %2, but are not able to find the connectionstring to the database." & vbCrLf & "Please add the connectionstring as BB_CONNECTIONSTRING%1 in the file BabelBank.exe.config"
                sSwedish = "BabelBank hittade Company%1, %2, men kan inte hitta inloggningssträngen til databasen." & vbCrLf & "Vänligen legg inn inloggingssträngen som BB_CONNECTIONSTRING%1 i filen BabelBank.exe.config"
                sDanish = "BabelBank fandt Company%1, %2, men kan ikke finde påloggingsstrengen til databasen." & vbCrLf & "Venligst legg inn påloggingsstrengen som BB_CONNECTIONSTRING%1 i filen BabelBank.exe.config"
            Case 15037
                sNorwegian = "BabelBank fant Company%1, %2, men klarer ikke å finne lisensfilen." & vbCrLf & "Vennligst angi hvor lisensfilen ligger som LICENSEPATH%1 i filen BabelBank.exe.config"
                sEnglish = "BabelBank has found Company%1, %2, but are not able to find the licensefile." & vbCrLf & "Please state where the licensefile is stored as LICENSEPATH%1 in the file BabelBank.exe.config"
                sSwedish = "BabelBank hittade Company%1, %2, men kan inte hitta licensfilen." & vbCrLf & "Vänligen ange licensefilen placering som BB_CONNECTIONSTRING%1 i filen BabelBank.exe.config"
                sDanish = "BabelBank fandt Company%1, %2, men kan ikke finde licensefilen." & vbCrLf & "Venligst angiv hvor licensefilen ligger som BB_CONNECTIONSTRING%1 i filen BabelBank.exe.config"
            Case 15038
                sNorwegian = "Legg inn SQL som skal kjøres, og velg " & vbCrLf & "om det er en spørring (SELECT) eller ikke."
                sEnglish = "Enter the SQL to run, and choose" & vbCrLf & "if it is a query (SELECT) or not."
                sSwedish = "Lägg inn den SQL som skall utföras, " & vbCrLf & "och om det är en fråga (SELECT) eller inte."
                sDanish = "Indtast din SQL at køre, og om " & vbCrLf & "det er en forespørgsel (SELECT) eller ej."
            Case 15039
                sNorwegian = "%1 post(er) ble oppdatert."
                sEnglish = "%1 record(s) were updated."
                sSwedish = "%1 post(er) blev uppdaterat."
                sDanish = "%1 post(er) blev opdateret."
            Case 15040
                sNorwegian = "Ingen poster ble oppdatert."
                sEnglish = "No records were updated."
                sSwedish = "Inga poster har uppdaterats."
                sDanish = "Ingen poster blev opdateret."
            Case 15041
                sNorwegian = "15041: BabelBank kunne ikke oppdatere tabellen over importerte filer."
                sEnglish = "15041: BabelBank was not able to update the table for imported files."
                sSwedish = "15041: BabelBank kunde inte uppdatera tabellen över importerade filer."
                sDanish = "15041: BabelBank kunne ikke opdatere tabellen over importerte filer."
            Case 15042
                sNorwegian = "Feil under pålogging til webClient, påloggingsstreng:" & vbCrLf & "%1"
                sEnglish = "Error connecting to webClient using string:" & vbCrLf & "%1"
                sSwedish = "Fel vid inloggning på webClient, inloggningssträng:" & vbCrLf & "%1"
                sDanish = "Fejl ved login på webClient, pålogningsstreng:" & vbCrLf & "%1"
            Case 15043
                sNorwegian = "Feil under oppdatering via RestAPI, data:" & vbCrLf & "%1"
                sEnglish = "Error during update using RestAPIconnecting, data:" & vbCrLf & "%1"
                sSwedish = "Fel vid uppdatering via RestAPI, data:" & vbCrLf & "%1"
                sDanish = "Fejl ved opdatering via RestAPI, data:" & vbCrLf & "%1"
            Case 15044
                sNorwegian = "Feil under oppdatering"
                sEnglish = "Error in execute"
                sSwedish = "Fel vid uppdatering"
                sDanish = "Fejl ved opdatering"

            Case 15500
                sNorwegian = "------- (Error Prepare Import) ------"
                sEnglish = "------- (Error Prepare Import) ------"
                sSwedish = "------- (Error Prepare Import) ------"
                sDanish = "------- (Error Prepare Import) ------"
            Case 15501
                sNorwegian = "FEIL I FORBEREDELSE AV IMPORT"
                sEnglish = "ERROR IN PREPARATION OF THE IMPORT"
                sSwedish = "FEL I FÖRBEREDELSE FÖR IMPORT"
                sDanish = "FEJL I FORBEREDELSE AF IMPORT"
            Case 15502
                sNorwegian = "Du må velge en profil før du trykker start."
                sEnglish = "Please select a profile, before pressing <Run>"
                sSwedish = "Du måste välja en profil innan du trycker på start."
                sDanish = "Du skal vælge en profil før du trykker start."
            Case 15503
                sNorwegian = "Velg profil"
                sEnglish = "Select a profile"
                sSwedish = "Välj profil"
                sDanish = "Vælg profil"
            Case 15504
                sNorwegian = "Det ble ikke funnet noen filer å importere."
                sEnglish = "No files to import."
                sSwedish = "Det fanns inga filer att importera."
                sDanish = "Det blev ikke fundet nogen filer at importere."
            Case 15505
                sNorwegian = "Profil:"
                sEnglish = "Profile:"
                sSwedish = "Profil:"
                sDanish = "Profil:"
            Case 15506
                sNorwegian = "Filnavn:"
                sEnglish = "Filename:"
                sSwedish = "Filnamn:"
                sDanish = "Filnavn:"
            Case 15507
                sNorwegian = "Kontonummer %1 på filen %2"
                sEnglish = "The accountnumber %1 in the file %2"
                sSwedish = "Kontonummer %1 i filen %2"
                sDanish = "Kontonummer %1 på filen %2"
            Case 15508
                sNorwegian = "finnes allerede registrert på klient: %1 %2."
                sEnglish = "does already exist on client: %1 %2."
                sSwedish = "är redan registrerat för klient: %1 %2."
                sDanish = "er allerede registreret på klient: %1 %2."
            Case 15509
                sNorwegian = "Klienten som importeres er: %1 %2."
                sEnglish = "The client which is imported is: %1 %2."
                sSwedish = "Klient som importeras: %1 %2."
                sDanish = "Klienten som importeres er: %1 %2."
            Case 15510
                sNorwegian = "Kontonummeret %1 for ny klient %2 er allerede ibruk for annen klient."
                sEnglish = "15510: The accountno %1 for new client %2 is already set for another client"
                sSwedish = "Kontonumret %1 för ny klient %2 används redan för en annan klient."
                sDanish = "Kontonummeret %1 for ny klient %2 er allerede i brug for anden klient."
            Case 15511
                sNorwegian = "15511: Det finnes allerede betalinger i BabelBank sin database." & vbCrLf & "" & vbCrLf & "Foregående importerte betalinger er sannsynligvis ikke eksportert ennå."
                sEnglish = "15511: Stored payments exists in BabelBank's database." & vbCrLf & "" & vbCrLf & "The previoussly imported paymentfiles are probably not exported yet."
                sSwedish = "15511: Det finns redan betalningar i BabelBanks databas." & vbCrLf & "" & vbCrLf & "Föregående importerade betalningar har sannolikt inte exporterats ännu.	"
                sDanish = "15511: Der findes allerede betalinger i BabelBanks database." & vbCrLf & "" & vbCrLf & "Foregående importerede betalinger er sandsynligvis ikke eksporteret endnu.	"
            Case 15512
                sNorwegian = "BabelBank vil forsøke å tilbakestille databasen til slik den var før import."
                sEnglish = "BabelBank will try to restore the database from before the import was done."
                sSwedish = "Babel Bank kommer att försöka återställa databasen till hur det var före införseln."
                sDanish = "Babel Bank vil forsøge at nulstille databasen til, hvordan det var før import."
            Case 15513
                sNorwegian = "Tilbakestilling"
                sEnglish = "Restoring"
                sSwedish = "Återställning"
                sDanish = "Nulstilling"
            Case 15514
                sNorwegian = "Vellykket tilbakestilling av databasen!" & vbCrLf & "Gå inn på manuelt posteringsvindu og sjekk at alt er OK" _
                                   & vbCrLf & "Slett eventuelle filer produsert av BabelBank, og legg eventuelt tilbake importfiler." _
                                   & vbCrLf & "Deretter kan filene leses inn på nytt."
                sEnglish = "The database was successfully restored!" & vbCrLf & "Please enter manual matching and check that everything is OK" _
                                   & vbCrLf & "Manually delete files produced by BabelBank, and copy the importfile(s) back to the importfolder if necessary." _
                                   & vbCrLf & "You may then try to do a new import."
                sSwedish = "Framgångsrikt återställning av databasen!" & vbCrLf & "Gå in på manuellt bokföringsfönster och kontrollera att allt är OK" _
                                                & vbCrLf & "Ta bort eventuella filer producerat av BabelBank och lägga eventuellt tillbaka importfiler ." _
                                                & vbCrLf & "Därefter kan filene läses igen."
                sDanish = "Succesfuld nulstiling af databasen!" & vbCrLf & "Gå ind på manuelt posteringsvindu og kontroller at alt er OK" _
                                                & vbCrLf & "Slet eventuelle filer produceret af BabelBank og tilføje eventuelt tilbage import filer." _
                                                & vbCrLf & "Dernæst kan filene læses igen."
            Case 15515
                sNorwegian = "BabelBank klarer ikke å tilbakestille databasen korrekt."
                sEnglish = "BabelBank failed to restore the database correctly."
                sSwedish = "BabelBank misslyckas med att återställa databasen korrekt."
                sDanish = "BabelBank undlader at nulstille databasen korrekt."
            Case 15516
                sNorwegian = "Tilbakestilling feilet!"
                sEnglish = "Restoring failed!"
                sSwedish = "Återställning misslyckades!"
                sDanish = "Nulstilling fejlet!"
            Case 15517
                sNorwegian = "Kontrollsummen stemmer ikke."
                sEnglish = "The controlsum doesn't match."
                sNorwegian = "Kontrollsumman stämmer inte."
                sNorwegian = "Checksummen passer ikke."
                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 15520
            Case 15600
                sNorwegian = "--- Feil i Manuell avstemming ---"
                sEnglish = "------- (Error Manual Matching) -------"
                sSwedish = "--- Feil i Manuell avstemming ---"
                sDanish = "--- Feil i Manuell avstemming ---"
            Case 15601
                sNorwegian = "Feil ved kjøring av søk. Feilkode fra database:"
                sEnglish = "An unexpected error occured. The database reported the following error:"
                sSwedish = "Fel vid körning av fråga. Felkod från databasen:"
                sDanish = "Fejl ved kørsel af søgning. Fejlkode fra database:"
            Case 15602
                sNorwegian = "En post er allerde låst av din bruker-ID" & vbLf & "" & vbLf & "Ønsker du å oppheve låsing av denne posten:" & vbLf & "Navn: %1" & vbLf & "Beløp: %2"
                sEnglish = "A payment is already locked by your User-ID" & vbLf & "" & vbLf & "Do You want to remove the lockingof this payment:" & vbLf & "Name: %1" & vbLf & "Amount: %2"
                sSwedish = "En post är redan låst av ditt användar-ID" & vbLf & "" & vbLf & "Vill du ångra låsningen av denna post:" & vbLf & "Namn: %1" & vbLf & "Belopp: %2	"
                sDanish = "En post er allerede låst af din bruger-ID" & vbLf & "" & vbLf & "Ønsker du at ophæve låsning af denne post:" & vbLf & "Navn: %1" & vbLf & "Beløb: %2	"
            Case 15603
                sNorwegian = "LÅST POST"
                sEnglish = "LOCKED PAYMENT"
                sSwedish = "LÅST POST"
                sDanish = "LÅST POST"
            Case 15604
                sNorwegian = "BabelBank fant ingen poster å avstemme manuelt på sist importerte fil." & vbLf & "" & vbLf & "Ønsker du å hente fram eldre poster?"
                sEnglish = "BabelBank can't find any payments to match manually on the last imported file." & vbLf & "" & vbLf & "Do You want to retrieve older payments?"
                sSwedish = "BabelBank hittade inga poster att avstämma manuellt på den senast importerade filen." & vbLf & "" & vbLf & "Vill du hämta äldre poster?	"
                sDanish = "BabelBank fandt ingen poster at afstemme manuelt på sidst importerede fil." & vbLf & "" & vbLf & "Vil du hente ældre poster frem?	"
            Case 15605
                sNorwegian = "INGEN POSTER FUNNET!"
                sEnglish = "NO PAYMENTS TO MATCH!"
                sSwedish = "INGA POSTER HITTADES!"
                sDanish = "INGEN POSTER FUNDET!"
            Case 15606
                sNorwegian = "Integrasjonen mot økonomisystemet tillater ikke føring av negative poster. Innbetalingen må omposteres."
                sEnglish = "The integration aginst the ERP-system doesn't allow negative postings. Please repost the payment."
                sSwedish = "Integreringen med ekonomisystemet tillåter inte redovisning av negativa poster. Betalningen måste bokas om."
                sDanish = "Integrationen mod økonomisystemet tillader føring af negative poster. Indbetalingen skal omposteres."
            Case 15607
                sNorwegian = "FEIL I POSTERING!"
                sEnglish = "ERROR IN THE ALLOCATION!"
                sSwedish = "FEL I BOKFÖRING!"
                sDanish = "FEJL I POSTERING!"
            Case 15608
                sNorwegian = "BabelBank kan ikke lage en tilbakebetaling fordi mottakers kontonummer ikke er gyldig."
                sEnglish = "BabelBank can't create a repayment because receivers accountnumber isn't valid."
                sSwedish = "XX ANVÄNDES I .NET"
                sDanish = "XX BENYTTET i .NET"
            Case 15700
                sNorwegian = "--- Feil i Automatisk avstemming ---"
                sEnglish = "------- (Error AutoMatch) -------"
                sSwedish = "--- Feil i Automatisk avstämming ---"
                sDanish = "--- Feil i Automatisk afstemming ---"
            Case 15701
                sNorwegian = "Konto %1 ble ikke funnet i BabelBank sin database." & vbLf & "" & vbLf & "For å opprette kontoen må du gå inn på valget 'Avstemming' i menyen." & vbLf & "Gå deretter inn på underpunktet 'Klienter' fra menyen."
                sEnglish = "The account %1 doesn't exist in BabelBank's database." & vbLf & "" & vbLf & "To add the account choose 'Clients' from the Matching-menu."
                sSwedish = "Konto %1 hittades inte i BabelBanks databas." & vbLf & "" & vbLf & "För att upprätta konton måste du gå in på alternativet 'Avstämning' i menyn." & vbLf & "Gå därefter in på ''Klienter' via menyn.	"
                sDanish = "Konto %1 blev ikke fundet i BabelBanks database." & vbLf & "" & vbLf & "For at oprette kontoen skal du gå ind på valget 'Afstemning' i menuen." & vbLf & "Gå derefter ind på underpunktet 'Klienter' fra menuen.	"
            Case 15702
                sNorwegian = "UKJENT KONTO"
                sEnglish = "UNKNOWN ACCOUNT"
                sSwedish = "OKÄNT KONTO"
                sDanish = "UKENDT KONTO"
            Case 15703
                sNorwegian = "BabelBank kjenner ikke formatet på filen." & vbLf & "" & vbLf & "Filnavn: %1"
                sEnglish = "BabelBank is unable to detect the format of the file." & vbLf & "" & vbLf & "Filename: %1"
                sSwedish = "BabelBank kan inte identifiera formatet på filen." & vbLf & "" & vbLf & "Filnamn: %1	"
                sDanish = "BabelBank kender ikke formatet på filen." & vbLf & "" & vbLf & "Filnavn: %1	"
            Case 15704
                sNorwegian = "15704: Det er ikke angitt noen observasjonskonto." & vbLf & "" & vbLf & "Legg inn en observasjonskonto i klientoppsettet."
                sEnglish = "15704: No observationaccount is stated." & vbLf & "" & vbLf & "Enter an observationaccount in Client-setup."
                sSwedish = "15704: Det finns inget angivet observationskonto." & vbLf & "" & vbLf & "Ange ett observationskonto i klientinställningarna.	"
                sDanish = "15704: Der er ikke angivet nogen observationskonto." & vbLf & "" & vbLf & "Tilføj en observationskonto i klientopsætningen.	"
            Case 15705
                sNorwegian = "15705: Det er angitt mer enn en observasjonskonto." & vbLf & "" & vbLf & "Slett ugyldige observasjonskonti i klientoppsettet."
                sEnglish = "15705: More than one observationaccount is stated." & vbLf & "" & vbLf & "Delete the invalid observationaccount(s) in Client-setup."
                sSwedish = "15705: Det finns mer än ett angivet observationskonto." & vbLf & "" & vbLf & "Ta bort ogiltiga observationskonton i klientinställningarna.	"
                sDanish = "15705: De er angivet mere end en observationskonto." & vbLf & "" & vbLf & "Slet ugyldige observationskonti i klientopsætningen.	"
            Case 15706
                sNorwegian = "15706: BabelBank kan ikke starte automatisk avstemming!" & vbLf & "" & vbLf & "Enten har du ikke rettigheter til å starte dette valget, alternativt er det ikke angitt en SQL for å sjekke at innbetalingene ligger i ERP-systemet."
                sEnglish = "15706: BabelBank is not able to start automatic matching!" & vbLf & "" & vbLf & "Either You don't have access to do this, or no SQL is stated to check if the payments exist in the ERP-system."
                sSwedish = "15706: BabelBank kan inte starta automatisk avstämning!" & vbLf & "" & vbLf & "Antingen har du inte rättigheter till att starta detta alternativ, eller så har det inte angetts någon SQL för att kontrollera att inbetalningarna ligger i ERP-systemet.	"
                sDanish = "15706: BabelBank kan ikke starte automatisk afstemning!" & vbLf & "" & vbLf & "Enten har du ikke rettigheder til at starte dette valg, alternativt er der ikke angivet en SQL for at kontrollere at indbetalingerne ligger i ERP-systemet.	"
            Case 15707
                sNorwegian = "Opphevet grunnet dobbelpostering"
                sEnglish = "Undone because of doublematching"
                sSwedish = "Ångrat på grund av dubbelbokning"
                sDanish = "Ophævet pga. dobbeltpostering"
            Case 15708
                sNorwegian = "15708: BabelBank finner ikke bankkonto %1 i Invoicia for å utlede hvilken klient dette er." & vbCrLf & vbCrLf & "Vennligst kontakt support."
                sEnglish = "15708: BabelBank isn't able to find bankaccount %1 in Invoicia in order to find the correct client." & vbCrLf & vbCrLf & "Please contact support."
                sSwedish = "15708: BabelBank hittar inte bankkonto %1 i Invoicia for å utlede hvilken klient dette er." & vbCrLf & vbCrLf & "Vennligst kontakt support."
                sDanish = "15708: BabelBank finner ikke bankkonto %1 i Invoicia for å utlede hvilken klient dette er." & vbCrLf & vbCrLf & "Vennligst kontakt support."
            Case 15709
                sNorwegian = "Opphevet grunnet differanse, beløp: %1"
                sEnglish = "Undone because of difference, amount: %1"
                sSwedish = "Ångrat på grund av skillnad, belopp: %1"
                sDanish = "Ophævet pga. forskel i beløb: %1"

                '--------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 15720

            Case 16000
                sNorwegian = "------- (Error Import) -------"
                sEnglish = "------- (Error Import) -------"
                sSwedish = "------- (Error Import) -------"
                sDanish = "------- (Error Import) -------"
            Case 16001
                sNorwegian = "FEIL UNDER IMPORT"
                sEnglish = "IMPORT FAILED"
                sSwedish = "FEL UNDER IMPORT"
                sDanish = "FEJL UNDER IMPORT"
            Case 16002
                sNorwegian = "Filen %1 inneholder ingen betalinger!"
                sEnglish = "The file % has no payments!"
                sSwedish = "Filen %1 innehåller inga betalningar!"
                sDanish = "Filen %1 indeholder ingen betalinger!"
            Case 16003
                sNorwegian = "FEIL VED FILTRERING"
                sEnglish = "FILTER FAILED!"
                sSwedish = "FEL VID FILTRERING"
                sDanish = "FEJL VED FILTRERING"
            Case 16004
                sNorwegian = "16004: Feil under import av kontonummer." & vbLf & "En <TAB> må være satt mellom kontonummeret og SWIFT-adressen." & vbLf & "Importfil: %1"
                sEnglish = "16004: Error during import of accountnumbers." & vbLf & "A <TAB> must exist between the accountnumber and the SWIFT-adress." & vbLf & "Importfile: %1"
                sSwedish = "16004: Fel vid import av kontonummer." & vbLf & "En <TAB> måste finnas mellan kontonummeret och SWIFT-adressen." & vbLf & "Importfil: %1	"
                sDanish = "16004: Fejl under import af kontonummer." & vbLf & "Der må være sat en <TAB> mellem kontonummeret og SWIFT-adressen." & vbLf & "Importfil: %1	"
            Case 16005
                sNorwegian = "En fil med identisk meldingsnummer, %1, ble importert %2." & vbLf & "Importfilnavn: %3" & vbLf & "Importfilens produksjonsdato: %4" & vbLf & "Totalt beløp på importfil: %5"
                sEnglish = "A file with an identical messagenumber %1 was imported %2." & vbLf & "Importfilename: %3" & vbLf & "Creation date of importfile: %4" & vbLf & "Total amount on importfile: %5"
                sSwedish = "En fil med samma meddelandenummer, %1, importerades till %2." & vbLf & "Importfil: %3" & vbLf & "Importfilens produksjonsdato: %4" & vbLf & "Totalt beløp på importfil: %5	"
                sDanish = "En fil med identisk meddelelsesnummer, %1, blev importeret %2." & vbLf & "Importfilnavn: %3" & vbLf & "Importfilens produktionsdato: %4" & vbLf & "Totalt beløb på importfil: %5	"
            Case 16006
                sNorwegian = "Ønsker du å fortsette med importen?"
                sEnglish = "Do You want to continue the import?"
                sSwedish = "Vill du fortsätta med importen?"
                sDanish = "Vil du fortsætte med importen?"
            Case 16007
                sNorwegian = "FILEN ER TIDLIGERE IMPORTERT!"
                sEnglish = "FILE PREVIOUSLY IMPORTED!"
                sSwedish = "FIL HAR IMPORTERATS TIDIGARE!"
                sDanish = "FILEN ER TIDLIGERE IMPORTERET!"
            Case 16008
                sNorwegian = "Du kan ikke benytte dette valget når det foregår en validering mot ERP-systemet." & vbLf & "" & vbLf & "Benytt profilen du ønsker for å importere."
                sEnglish = "You can't import files because when there is a validation against the ERP-system." & vbLf & "" & vbLf & "Run a profile to import files."
                sSwedish = "Du kan inte använda det här alternativet under pågående validering mot ERP-systemet." & vbLf & "" & vbLf & "Använd önskad profil för att importera.	"
                sDanish = "Du kan ikke benytte dette valg når der foregår en validering mod ERP-systemet." & vbLf & "" & vbLf & "Benyt profilen du ønsker til at importere.	"
            Case 16009
                sNorwegian = "Feil ved behandling av strukturert informasjon"
                sEnglish = "Error when trying to structure information"
                sSwedish = "Fel vid behandling av strukturerad information"
                sDanish = "Fejl ved behandling af struktureret information"
            Case 16010
                sNorwegian = "16010: En uventet feil oppstod under oppdatering av %1-tabellen."
                sEnglish = "16010: A problem occured when updating the %1-tabel"
                sSwedish = "Användaren har valt att importera filen."
                sDanish = "Bruger valgte at importere filen."
            Case 16011
                sNorwegian = "Pain.002-filen er ikke sendt på grunnlag av en Pain.001 fil." & vbCrLf & "Filnavn: %1"
                sEnglish = "The Pain.002-file doesn't originate from a Pain.001-file." & vbCrLf & "Filename: %1"
                sSwedish = "Användaren har valt att INTE importera filen."
                sDanish = "Bruger valgte IKKE at importere filen."
            Case 16012
                sNorwegian = "Feil under strukturering av S|E|B betalinger"
                sEnglish = "Error structuring S|E|B-payments"
                sSwedish = "Fel strukturering av S | E | B betalningar"
                sDanish = "Fejl strukturering af S | E | B betalinger"
            Case 16013
                sNorwegian = "Feil under strukturering av ISO 20022 filer"
                sEnglish = "Error structuring ISO 20022 files"
                sSwedish = "Fel strukturering av ISO 20022 filer"
                sDanish = "Fejl strukturering af ISO 20022 filer"
            Case 16014
                sNorwegian = "Feil i TreatSpecial rutiner"
                sEnglish = "Error in TreatSpecial routines"
                sSwedish = "Fel in TreatSpecial rutiner"
                sDanish = "Fejl i TreatSpecial rutiner"





                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 16020

            Case 16500
                sNorwegian = "------- (Error Spread) ------"
                sEnglish = "------- (Error Spread) ------"
                sSwedish = "------- (Error Spread) ------"
                sDanish = "------- (Error Spread) ------"
            Case 16501
                sNorwegian = "FEIL I RAPPORTMODUL"
                sEnglish = "ERROR IN REPORTMODULE"
                sSwedish = "FEL I RAPPORTMODUL"
                sDanish = "FEJL I RAPPORTMODUL"
            Case 17000
                sNorwegian = "------- (Error Prepare Export) ------"
                sEnglish = "------- (Error Prepare Export) ------"
                sSwedish = "------- (Error Prepare Export) ------"
                sDanish = "------- (Error Prepare Export) ------"
            Case 17001
                sNorwegian = "FEIL I FORBEREDELSE AV EKSPORT"
                sEnglish = "ERROR IN PREPARATION OF THE EXPORT"
                sSwedish = "FEL I FÖRBEREDELSE FÖR EXPORT"
                sDanish = "FEJL I FORBEREDELSE AF EKSPORT"
            Case 17002
                sNorwegian = "Katalogen %1 er skrivebeskyttet."
                sEnglish = "The folder %1 is read-only."
                sSwedish = "Katalogen %1 är skrivskyddad."
                sDanish = "Kataloget %1 er skrivebeskyttet."
            Case 17003
                sNorwegian = "Legg inn en annen katalog under oppsett, eller endre egenskapene til katalogen."
                sEnglish = "Change the folder in setup, or change the attributes of the folder."
                sSwedish = "Ange en annan katalog under inställningen eller ändra egenskaper för katalogen."
                sDanish = "Tilføj et andet katalog under opsætning, eller rediger katalogets egenskaber."
            Case 17004
                sNorwegian = "Katalogen %1 finnes ikke."
                sEnglish = "Folder %1 doesn't exist."
                sSwedish = "Katalogen %1 finns inte."
                sDanish = "Kataloget %1 findes ikke."
            Case 17005
                sNorwegian = "Opprett katalogen eller endre katalogen under oppsett."
                sEnglish = "Create the folder or change the folder in setup."
                sSwedish = "Ställ in katalogen eller ändra katalogen under inställningen."
                sDanish = "Opret kataloget eller rediger kataloget under opsætning."
            Case 17006
                sNorwegian = "Feil ved sletting av tidligere eksporterte filer"
                sEnglish = "Error trying to delete previously exported file"
                sSwedish = "Fel vid borttagning av tidigare exporterade filer"
                sDanish = "Fejl ved sletning af tidligere eksporterede filer"
            Case 17007
                sNorwegian = "Du må velge eksportformat"
                sEnglish = "Please select an exportformat!"
                sSwedish = "Du måste välja exportformat"
                sDanish = "Du skal vælge eksportformat"
            Case 17008
                sNorwegian = "17008: Ukjent betalingstype oppdaget. Betalingstype: %1 " & vbLf & "BabelBank kan ikke lage en korrekt TBI-fil."
                sEnglish = "17008: Unknown paytype detected. Paytype: %1 " & vbLf & "BabelBank is unable to create a correct TBI-file."
                sSwedish = "17008: Okänd betalningstyp upptäcktes. Betalningstyp: %1" & vbLf & "BabelBank kan inte skapa en korrekt TBI-fil.	"
                sDanish = "17008: Ukendt betalingstype opdaget. Betalingstype: %1" & vbLf & "BabelBank kan ikke oprette en korrekt TBI-fil.	"
            Case 17009
                sNorwegian = "17009: Det er avvik mellom antall siffer i klientnummeret," & vbLf & "og antall $-tegn (Klientnummer) i filnavnet angitt i profiloppsettet." & vbLf & "Klientnummer: %1, Klientnummerets lengde: %2" & vbLf & "Filnavn: %3, Klientnummerets lengde: %4"
                sEnglish = "17009: Differense between the number of digits in ClientNo," & vbLf & "and the number of $-signs (Clientnumber) in the Filename stated in setup." & vbLf & "ClientNo stated: %1, Length of clientnumber: %2" & vbLf & "Filename stated: %3, length of clientnumber: %4"
                sSwedish = "17009: Det finns en avvikelse mellan antalet siffror i klientnumret" & vbLf & "och antalet §-tecken (Klientnummer) i filinamnet som angetts i profilinställningen." & vbLf & "Klientnummer: %1, Klientnumrets längd: %2" & vbLf & "Filnamn: %3, Klientnumrets längd: %4	"
                sDanish = "17009: Der er en afvigelse mellem antal cifre i klientnummeret," & vbLf & "og antal §-tegn (Klientnummer) i filnavnet angivet i profilopsætningen." & vbLf & "Klientnummer: %1, Klientnummerets længde: %2" & vbLf & "Filnavn: %3, Klientnummerets længde: %4	"
            Case 17010
                sNorwegian = "17010: Eksportformatet TBIW må kjøres ved hjelp av BabelBank." & vbLf & "Du kan ikke benytte vbBabel.dll til å lage dette formatet."
                sEnglish = "17010: The export-format TBIW needs to be run through BabelBank." & vbLf & "You can't use the vbBabel.dll to create this format."
                sSwedish = "17010: Exportformat TBIW måste köras med hjälp BabelBank." & vbLf & "Du kan inte använda vbBabel.dll til för att skapa detta format.	"
                sDanish = "17010: Eksportformatet TBIW skal køres vha. BabelBank." & vbLf & "Du kan ikke benytte vbBabel.dll til at oprette dette format.	"
            Case 17011
                sNorwegian = "17011: SWIFT-adressen for debetkontoen %1 er ikke gyldig." & vbLf & "Landkoden er ikke implementert. Landkode: %2"
                sEnglish = "17011: The SWIFT-address for the debit bankaccount %1 is not valid." & vbLf & "The landcode is not implemented. Landcode: %2"
                sSwedish = "17011: SWIFT-adressen till debetkontot %1 är ogiltig." & vbLf & "Landsnumret har inte implementerats. Landsnummer: %2	"
                sDanish = "17011: SWIFT-adressen for debetkontoen %1 er ikke gyldig." & vbLf & "Landekoden er ikke implementeret. Landekode: %2	"
            Case 17012
                sNorwegian = "17012: BabelBank er ikke i stand til å utlede korrekt betalingstype."
                sEnglish = "17012: BabelBank is unable to deduct the correct payment type."
                sSwedish = "17012 BabelBank inte kan härleda rätt betalningssätt."
                sDanish = "17012: BabelBank er ikke i stand til at udlede korrekt betalingstype."
            Case 17013
                sNorwegian = "17013: BabelBank er ikke i stand til å utlede betalers landkode."
                sEnglish = "17013: BabelBank is unable to deduct the payers country code."
                sSwedish = "17013 BabelBank inte kan härleda betalarens landsnummer."
                sDanish = "17013: BabelBank er ikke i stand til at udlede betalers landekode."
            Case 17014
                sNorwegian = "17014: Feil i sammenslåing av betalinger"
                sEnglish = "17014: Error when merging payments"
                sSwedish = "17014: Fel i sammanslagning av betalningar"
                sDanish = "17014: Fejl i sammenlægning af betalinger"
            Case 17015
                sNorwegian = "17015: Feil ved sortering av betalinger"
                sEnglish = "17015: Error when sorting payments"
                '----------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 17020

                sSwedish = "17015: Fel vid sortering av betalningar"
                sDanish = "17015: Fejl ved sortering af betalinger"
            Case 17500
                sNorwegian = "------- (Error Export) -------"
                sEnglish = "------- (Error Export) -------"
                sSwedish = "------- (Error Export) -------"
                sDanish = "------- (Error Export) -------"
            Case 17501
                sNorwegian = "FEIL UNDER EKSPORT"
                sEnglish = "EXPORT FAILED"
                sSwedish = "FEL UNDER  EXPORT"
                sDanish = "FEJL UNDER EKSPORT"
            Case 17502
                sNorwegian = "Følgende post ble ikke eksportert fra Babelbank:"
                sEnglish = "Following record was not exported from Babelbank:"
                sSwedish = "Följande post exporterades inte från BabelBank:"
                sDanish = "Følgende post blev ikke eksporteret fra Babelbank:"
            Case 17503
                sNorwegian = "Filnavn:"
                sEnglish = "Filename:"
                sSwedish = "Filnamn:"
                sDanish = "Filnavn:"
            Case 17504
                sNorwegian = "Dato:"
                sEnglish = "Date:"
                sSwedish = "Datum:"
                sDanish = "Dato:"
            Case 17505
                sNorwegian = "Navn:"
                sEnglish = "Name:"
                sSwedish = "Namn:"
                sDanish = "Navn:"
            Case 17506
                sNorwegian = "Beløp:"
                sEnglish = "Amount:"
                sSwedish = "Belopp:"
                sDanish = "Beløb:"
            Case 17507
                sNorwegian = "Ønsker du å vise flere poster?"
                sEnglish = "Show more records?"
                sSwedish = "Vill du se fler poster?"
                sDanish = "Ønsker du at vise flere poster?"
            Case 17508
                sNorwegian = "Feil under eksport"
                sEnglish = "Export error"
                sSwedish = "Fel vid export"
                sDanish = "Fejl under eksport"
            Case 17509
                sNorwegian = "Post"
                sEnglish = "Record"
                sSwedish = "Bokför"
                sDanish = "Post"
            Case 17510
                sNorwegian = "Feil under kryptering av fil"
                sEnglish = "Error when crypting file"
                sSwedish = "Fel under kryptering av fil"
                sDanish = "Fejl under kryptering af fil"
            Case 17511
                sNorwegian = "Det finnes ingen innbetalinger å eksportere!"
                sEnglish = "There are no payments to export!"
                sSwedish = "Det finns inga betalningar att exportera!"
                sDanish = "Der er ingen indbetalinger at eksportere!"
            Case 17512
                sNorwegian = "BabelBank kan ikke utføre ønsket handling, fordi minst en betaling er låst av en annen bruker."
                sEnglish = "Can't process the requested action, because at least one payment is locked by another user."
                sSwedish = "BabelBank kan inte utföra önskad åtgärd eftersom minst en betalning är låst av en annan användare."
                sDanish = "BabelBank kan ikke udføre ønsket handling, fordi mindst en betaling er låst af en anden bruger."
            Case 17513
                sNorwegian = "For å låse opp betalingen velg '%1' på menyen '%2'."
                sEnglish = "To unlock the payments choose '%1' on the menu '%2'."
                sSwedish = "För att låsa upp din betalning väljer du ''%1'' på menyn ''%2''."
                sDanish = "For at låse betalingen op, vælg '%1' i menuen '%2'."
            Case 17514
                sNorwegian = "KAN IKKE STARTE EKSPORT"
                sEnglish = "CAN'T START EXPORT."
                sSwedish = "KAN INTE STARTA EXPORT"
                sDanish = "KAN IKKE STARTE EKSPORT"
            Case 17515
                sNorwegian = "Betalingene som skal eksporteres stammer fra to forskjellige profiler. Kontakt din forhandler."
                sEnglish = "The payments to be exported originates from two different profiles. Contact Your dealer."
                sSwedish = "Betalningarna som ska exporteras kommer från två olika profiler. Kontakta din återförsäljare."
                sDanish = "Betalingerne som skal eksporteres stammer fra to forskellige profiler. Kontakt din forhandler."
            Case 17516
                sNorwegian = "Filen %1 er allerede kryptert."
                sEnglish = "The file %1 has already been crypted."
                sSwedish = "Filen %1 är redan krypterad."
                sDanish = "Filen %1 er allerede krypteret."
            Case 17517
                sNorwegian = "Feil under behandling av AUTACK"
                sEnglish = "Error during AUTACK-treatment"
                sSwedish = "Fel under behandling av AUTACK"
                sDanish = "Fejl under behandling af AUTACK"
            Case 17518
                sNorwegian = "BabelBank kan ikke slette betalingene i databasen fordi ikke-posterte innbetalinger lagres i databasen." & vbLf & "Ønsker du allikevel å slette betalinger så kontakt din forhandler."
                sEnglish = "BabelBank can not delete payments from the database, because non-posted payments are stored in BabelBank." & vbLf & "Please contact your dealer if you still want to delete the payments."
                sSwedish = "BabelBank kan inte ta bort dina betalningar i databasen eftersom det finns ännu inte bokförda inbetalningar lagrade i databasen." & vbLf & "Om du ändå vill ta bort betalningar så kontakta din återförsäljare.	"
                sDanish = "BabelBank kan ikke slette betalingerne i databasen fordi ikke-posterede indbetalinger gemmes i databasen." & vbLf & "Ønsker du alligevel at slette betalinger så kontakt din forhandler.	"
            Case 17519
                sNorwegian = "KAN IKKE SLETTE BETALINGER."
                sEnglish = "CAN'T DELETE RECORDS."
                sSwedish = "KAN INTE TA BORT BETALNINGAR."
                sDanish = "KAN IKKE SLETTE BETALINGER."
            Case 17520
                sNorwegian = "Feil under behandling av PGP/SecureEnvelope sikkerhet"
                sEnglish = "Error during PGP Security treatment"
                sSwedish = "Fel under behandling av PGP sikkerhet"
                sDanish = "Fejl under behandling af PGP sikkerhet"

            Case 18000
                sNorwegian = "------- (Error File Cleanup) -------"
                sEnglish = "------- (Error File Cleanup) -------"
                sSwedish = "------- (Error File Cleanup) -------"
                sDanish = "------- (Error File Cleanup) -------"
            Case 18001
                sNorwegian = "FEIL I FILHÅNDTERING"
                sEnglish = "ERROR IN THE FILEHANDLING"
                sSwedish = "FEL I FILHANTERING"
                sDanish = "FEJL I FILHÅNDTERING"
            Case 18500
                sNorwegian = "----- (Error ShowClients) -----"
                sEnglish = "----- (Error ShowClients) -----"
                sSwedish = "----- (Error ShowClients) -----"
                sDanish = "----- (Error ShowClients) -----"
            Case 18501
                sNorwegian = "FEIL I VISNING AV KLIENTER"
                sEnglish = "ERROR IN CLIENTSLISTING"
                sSwedish = "FEL I VISNING AV KLIENTER"
                sDanish = "FEJL I VISNING AF KLIENTER"
            Case 19000
                sNorwegian = "----- (Error in ShowRejected) -----"
                sEnglish = "----- (Error in ShowRejected) -----"
                sSwedish = "----- (Error in ShowRejected) -----"
                sDanish = "----- (Error in ShowRejected) -----"
            Case 19001
                sNorwegian = "FEIL I VISNING AV AVVISTE POSTER"
                sEnglish = "ERROR IN REPORT REJECTED PAYMENTS"
                sSwedish = "FEL I VISNING AV AVVISADE POSTER"
                sDanish = "FEJL I VISNING AF AFVISTE POSTER"
            Case 19002
                sNorwegian = "19002: Det finnes avviste poster i filen."
                sEnglish = "There are rejected payments in the file."
                sSwedish = "19002: Det finns avvisade poster i filen."
                sDanish = "19002: Der er afviste poster i filen."
            Case 19500
                sNorwegian = "----- (Error in Automatic Matching) -----"
                sEnglish = "----- (Error in Automatic Matching) -----"
                sSwedish = "----- (Error in Automatic Matching) -----"
                sDanish = "----- (Error in Automatic Matching) -----"
            Case 19501
                sNorwegian = "FEIL UNDER AUTOMATISK AVSTEMMING"
                sEnglish = "ERROR DURING AUTOMATIC MATCHING"
                sSwedish = "FEL UNDER AUTOMATISK AVSTÄMNING"
                sDanish = "FEJL UNDER AUTOMATISK AFSTEMNING"
            Case 19502
                sNorwegian = "19502: Feil i beskrivelse av KID. Ugyldig karakter er brukt. " & vbLf & "KID-beskrivelse: %1" & vbLf & "" & vbLf & "Endringer gjøres under Oppsett."
                sEnglish = "19502: Error in KID-description. Invalid character used . " & vbLf & "KID-description: %1" & vbLf & "" & vbLf & "Change the description under Setup."
                sSwedish = "19502: Fel i beskrivningen av OCR. Ogiltigt tecken används." & vbLf & "OCR-beskrivning: %1" & vbLf & "" & vbLf & "Ändringar görs i Inställningar.	"
                sDanish = "19502: Fejl i beskrivelse af KID. Ugyldig karakter er brugt." & vbLf & "KID-beskrivelse: %1" & vbLf & "" & vbLf & "Ændringer foretages under Opsætning.	"
            Case 19503
                sNorwegian = "19503: Du forsøker å avstemme innbetalinger på klient %1," & vbLf & "men det er ikke satt opp noen pålogging mot ERP-systemet."
                sEnglish = "19503: You try to match payments on client %1," & vbLf & "but no ERP-connection is specified for the client."
                sSwedish = "19503: Du försöker avstämma inbetalningar på klient %1," & vbLf & "men det finns ingen inloggning till ERP-systemet.	"
                sDanish = "19503: Du forsøger at afstemme indbetalinger på klient %1," & vbLf & "men det er ikke opsat nogen pålogning mod ERP-systemet.	"
            Case 19504
                sNorwegian = "19504: Ugyldig %1 angitt."
                sEnglish = "19504: Invalid %1 specified."
                sSwedish = "19504: Ogiltigt %1 har angetts."
                sDanish = "19504: Ugyldig %1 angivet."
            Case 19505
                sNorwegian = "KAN IKKE STARTE AUTOMATISK AVSTEMMING"
                sEnglish = "CAN'T START AUTOMATIC MATCHING"
                sSwedish = "KAN INTE STARTA AUTOMATISKT AVSTÄMNING"
                sDanish = "KAN IKKE STARTE AUTOMATISK AFSTEMNING"
            Case 19506
                sNorwegian = "Betalingene som skal avstemmes stammer fra to forskjellige profiler. Kontakt din forhandler."
                sEnglish = "The payments to be matched originates from two different profiles. Contact Your dealer."
                sSwedish = "De betalningar som ska avstämmas kommer från två olika profiler. Kontakta din återförsäljare."
                sDanish = "Betalingerne som skal afstemmes stammer fra to forskellige profiler. Kontakt din forhandler."
            Case 19507
                sNorwegian = "Det er ingen betalinger å avstemme!"
                sEnglish = "There are no payments to match!"
                sSwedish = "Det finns inga betalningar att avstämma!"
                sDanish = "Det er ingen betalinger at afstemme!"
            Case 19508
                sNorwegian = "Kan ikke finne SQLen som skal hente ut hvilke klienter som er koblet mot denne bankkontoen for klient %1"
                sEnglish = "Can't find the SQL to retrieve which clients are connected to this bankaccount for client %1"
                sSwedish = "Kan inte hitta SQLen som skall att ta reda på vilka kunder är kopplade till bankkontot  för klient% 1"
                sDanish = "Kan ikke finde SQLen som skal bringe ud, hvilke kunder er knyttet til denne bankkonto til klient% 1"
            Case 19700
                sNorwegian = "FEIL UNDER MANUELL AVSTEMMING"
                sEnglish = "ERROR DURING MANUAL MATCHING"
                sSwedish = "FEL UNDER MANUELL AVSTÄMNING"
                sDanish = "FEJL UNDER MANUEL AFSTEMNING"
            Case 19701
                sNorwegian = "Feil i drop/create table."
                sEnglish = "Error in drop/create table"
                sSwedish = "Fel under drop/create table."
                sDanish = "Fejl i drop/create table."
            Case 19702
                sNorwegian = "Ingen klient funnet! Er kontoen %1meldt inn under oppsett?"
                sEnglish = "Client not found! Is the account %1 specified in setup?"
                sSwedish = "Ingen klient hittades! Är kontot %1 registrerat i inställningarna?"
                sDanish = "Ingen klient fundet! Er kontoen %1meldt på under opsætning?"
            Case 19703
                sNorwegian = "19703: En hovedbokskonto er oppgitt, men det er ikke beskrevet noe 'mønster' for dette i klient-oppsettet." & vbLf & "" & vbLf & "Kan økonomisystemet postere på hovedbokskonti fra BabelBank?" & vbLf & "Hvis Ja, gå in på klient-oppsettet og opprett et møsnter for denne klienten." & vbLf & "Klient: %1 %2"
                sEnglish = "19703: An GL-account is stated, but no pattern is described in client-setup." & vbLf & "" & vbLf & "Can the ERP-system post on GL-accounts from BabelBank?" & vbLf & "If yes, describe the pattern for GL-accounts in Client-setup." & vbLf & "Client: %1 %2"
                sSwedish = "19703: Ett huvudbokskonto är inställt, men det finns ingen beskrivning av ''mönster'' för detta i klientinställningen." & vbLf & "" & vbLf & "Kan ekonomisystemet bokföra på huvudbokskonton från BabelBank?" & vbLf & "Om Ja, gå in på klientinställningarna och skapa ett mönster för denna klient." & vbLf & "Klient: %1 %2	"
                sDanish = "19703: En hovedbokskonto er angivet, men der er ikke beskrevet noget 'mønster' for denne i klient-opsætningen." & vbLf & "" & vbLf & "Kan økonomisystemet postere på hovedbokskonti fra BabelBank?" & vbLf & "Hvis Ja, gå ind på klient-opsætningen og opsæt et mønster for denne klient." & vbLf & "Klient: %1 %2	"
            Case 19704
                sNorwegian = "19704: Kan ikke påføre nødvendig avstemmingsinformasjon på innbetalingen. Det er ikke angitt noe posteringsinformasjon."
                sEnglish = "19704: Can't add necessary matching information on the payment, no matching information stated."
                sSwedish = "19704: Det går inte att tillämpa den nödvändiga synkroniseringsinformationen för inbetalningen. Det finns ingen bokföringsinformation."
                sDanish = "19704: Kan ikke påføre nødvendige afstemningsinformationer på indbetalingen. Der er ikke angivet nogen posteringsinformation."
            Case 19705
                sNorwegian = "19705: Du kan ikke angi både en hovedbokskonto og et fakturanummer på betalingen. Vennligst fjern en av dem."
                sEnglish = "19705: You can't enter both an GL-Account and an invoicenumber on the payment. Please erase either information."
                sSwedish = "19705: Du kan inte ange både ett huvudbokskonto och fakturanummer på betalningen. Ta bort något av dem."
                sDanish = "19705: Du kan ikke angive både en hovedbogskonto og et fakturanummer på betalingen. Fjern en af dem."
            Case 19706
                sNorwegian = "19706: Betalingen kan ikke avstemmes ved kun å oppgi et fakturanummer."
                sEnglish = "19706: The payment can't be matched by just adding an invoicenumber."
                sSwedish = "19706: Betalningen kan inte avstämmas genom att helt enkelt ange ett fakturanummer."
                sDanish = "19706: Betalingen kan ikke afstemmes ved kun at angive et fakturanummer."
            Case 19707
                sNorwegian = "19707: Denne betalingen kan ikke avstemmes fordi det mangler en unik avstemmings-ID fra ERP-systemet på minst en av fakturaene."
                sEnglish = "19707: This payment can't be matched, because You lack an unique matching-ID from the ERP-system on at least one of the invoices."
                sSwedish = "19707: Betalningen kan inte avstämmas eftersom den saknar ett unikt avstämnings-ID från ERP-systemet i minst en av fakturorna."
                sDanish = "19707: Denne betaling kan ikke afstemmes fordi der mangler et unikt afstemnings-ID fra ERP-systemet på mindst en af fakturaerne."
            Case 19708
                sNorwegian = "19708: Valideringen av avstemmingen rapporterte følgende feil:"
                sEnglish = "19708: The validation of the matching reported the following error(s):"
                sSwedish = "19708: Validering av avstämningen rapporterade följande fel:"
                sDanish = "19708: Valideringen af afstemningen rapporterede følgende fejl:"
            Case 19709
                sNorwegian = "Det oppgitte fakturanummeret er for langt i henhold til oppgitt mønster under klientoppsettet."
                sEnglish = "The invoicenumber stated is too long according to the pattern stated in the clientsetup."
                sSwedish = "Det angivna fakturanumret är för långt enligt det angivna mönstret under klientinställningen."
                sDanish = "Det angivne fakturanummer er for langt i henhold til det angivne mønster under klientopsætningen."
            Case 19710
                sNorwegian = "Det oppgitte kundenummeret er for langt i henhold til oppgitt mønster under klientoppsettet."
                sEnglish = "The customernumber stated is too long according to the pattern stated in the clientsetup."
                sSwedish = "Den angivna kundnumret är för långt enligt det angivna mönstret under klientinställningen."
                sDanish = "Det angivne kundenummer er for langt i henhold til det angivne  mønster under klientopsætningen."
            Case 19711
                sNorwegian = "Det er ikke angitt fakturanummer."
                sEnglish = "No invoicenumber is stated."
                sSwedish = "Det finns inga fakturanummer."
                sDanish = "Der er ikke angivet fakturanummer."
            Case 19712
                sNorwegian = "Det er ikke angitt kundenummer."
                sEnglish = "No customernumber is stated."
                sSwedish = "Det finns inget angivet kundnummer."
                sDanish = "Der er ikke angivet kundenummer."
            Case 19713
                sNorwegian = "Valutaen %1 ble ikke funnet i Maritech."
                sEnglish = "The currency %1 was not found in Maritech."
                sSwedish = "Valutan %1 hittades inte i Maritech."
                sDanish = "Valutaen %1 blev ikke fundet i Maritech."
            Case 19714
                sNorwegian = "Valutaen %1 er oppgitt mer enn en gang i Maritech."
                sEnglish = "The currency %1 is stated more than once in Maritech."
                sSwedish = "Valutan %1 har angetts mer än en gång i Maritech."
                sDanish = "Valutaen %1 er angivet mere end en gang i Maritech."
            Case 19715
                sNorwegian = "Feil ved utskrift av skjermbilde manuell. Sjekk om skriveren %1 er klar!"
                sEnglish = "Error printing screencontent. Check if printer %1 is ready!"
                sSwedish = "Fel vid manuell utskrift av skärmbilden. Kontrollera om skrivaren %1 är klar!"
                sDanish = "Fejl ved manuel udskrivning af skærmbillede. Kontroller om printeren %1 er klar!"
            Case 19716
                sNorwegian = "Ønsker du at faktura %1 skal overbetales?" & vbLf & "" & vbLf & "Den valgte åpne posten har et beløp som er lavere enn restbeløpet på innbetalingen."
                sEnglish = "Do you really want to overpay invoice %1?" & vbLf & "" & vbLf & "The selected open amount, is less than the rest amount on the payment."
                sSwedish = "Den valda öppna posten är ett belopp som är lägre än restbeloppet i inbetalningen." & vbLf & "" & vbLf & "Om du vill delbetala denna faktura" & vbLf & "väljer du betalning och ändrar sedan beloppet.	"
                sDanish = "Den valgte åbne post har et beløb, der er lavere en restbeløbet på indbetalingen." & vbLf & "" & vbLf & "Hvis du ønsker at delbetale denne faktura," & vbLf & "vælg betalingen og ændr derefter beløbet.	"
            Case 19717
                sNorwegian = "KAN IKKE UTFØRE VALGT HANDLING!"
                sEnglish = "UNABLE TO COMPLETE SELECTED ACTION!"
                sSwedish = "KAN INTE UTFÖRA DEN VALDA ÅTGÄRDEN!"
                sDanish = "KAN IKKE UDFØRE VALGT HANDLING!"
            Case 19718
                sNorwegian = "Beløpet som er lagt inn er mindre enn det opprinnelige beløpet på fakturaen (%1)." & vbLf & "Dette vil bli en delbetaling." & vbLf & "" & vbLf & "Ønsker du å fortsette?"
                sEnglish = "The entered amount is less than the original amount (%1) on the invoice." & vbLf & "This will be a part payment." & vbLf & "" & vbLf & "Do You want to continue?"
                sSwedish = "Det angivna beloppet är mindre än det ursprungliga fakturabeloppet (%1)." & vbLf & "Detta blir en delbetalning." & vbLf & "" & vbLf & "Vill du fortsätta?	"
                sDanish = "Beløbet, der er lagt ind er mindre end det oprindelige beløb på fakturaen (%1)." & vbLf & "Dette vil blive en delbetaling." & vbLf & "" & vbLf & "Vil du fortsætte?	"
            Case 19719
                sNorwegian = "Beløpet som er lagt inn er høyere enn det opprinnelige beløpet på fakturaen (%1)." & vbLf & "" & vbLf & "Ønsker du å fortsette?"
                sEnglish = "The entered amount is larger than the original amount (%1) on the invoice." & vbLf & "" & vbLf & "Do You want to continue?"
                sSwedish = "Det angivna beloppet är högre än det ursprungliga fakturabeloppet (%1)." & vbLf & "" & vbLf & "Vill du fortsätta?	"
                sDanish = "Beløbet, der er lagt ind er højere end det oprindelige beløb på fakturaen (%1)." & vbLf & "" & vbLf & "Vil du fortsætte?	"
            Case 19720
                sNorwegian = "BELØPSAVVIK"
                sEnglish = "AMOUNT DEVIATION"
                sSwedish = "BELOPPSAVVIKELSE"
                sDanish = "BELØBSAFVIGELSE"
            Case 19721
                sNorwegian = "Du kan ikke endre beløpet på en kreditnota."
                sEnglish = "You can't change the amount of a credit note."
                sSwedish = "Du kan inte ändra belopp i en kreditnota."
                sDanish = "Du kan ikke ændre beløbet på en kreditnota."
            Case 19722
                sNorwegian = "19722: Det er ikke lagt inn noen SQL for å verifisere at innbetalinger i BabelBank" & vbLf & "også ligger på observasjonkontoen i økonomisystemet."
                sEnglish = "19722: No SQL stated to verify that the payments in BabelBank" & vbLf & "exists in the observation account in the ERP-system."
                sSwedish = "19722: Det finns ingen angiven SQL för att verifiera att inbetalningar i BabelBank" & vbLf & "även ligger på observationskonton i ekonomisystemet.	"
                sDanish = "19722: Der er ikke lagt nogen SQL ind for at verificere at indbetalinger i BabelBank" & vbLf & "også ligger på observationskontoen i økonomisystemet.	"
            Case 19723
                sNorwegian = "Mangler en SQL-spørring!"
                sEnglish = "No SQL stated!"
                sSwedish = "Saknas en SQL-sats!"
                sDanish = "Mangler en SQL-forespørgsel!"
            Case 19724
                sNorwegian = "Det er lagt inn en SQL for validering av en kolonne som ennå ikke er tilpasset dette." & vbLf & "Gjelder kolonne nummer %1."
                sEnglish = "You have entered a SQL to validate a column that is not yet possible to validate." & vbLf & "Column number %1."
                sSwedish = "En SQL har angetts för validering av en kolumn som ännu inte är anpassad för detta." & vbLf & "Det gäller kolumn nummer %1.	"
                sDanish = "Der er lagt en SQL ind for validering af en kolonne som endnu ikke er tilpasset dette." & vbLf & "Gælder kolonne nummer %1.	"
            Case 19725
                sNorwegian = "Ingen validerings-ID funnet for: %1"
                sEnglish = "No validation-ID found for the entered expression: %1"
                sSwedish = "Inget validerings-ID hittades: %1"
                sDanish = "Ingen validerings-ID fundet for: %1"
            Case 19726
                sNorwegian = "Det er lagt inn en SQL for validering." & vbLf & "" & vbLf & "Denne SQL-en skal vise resultatet i kolonnen 'Navn'." & vbLf & "I Babelbank sitt oppsett står det at denne kolonnen ikke skal vises."
                sEnglish = "A validation SQL is present." & vbLf & "" & vbLf & "This SQL will show the result in the column called 'Name'." & vbLf & "This column is not marked for viewing in the BabelBank setup."
                sSwedish = "En SQL har angetts för validering." & vbLf & "" & vbLf & "Denna SQL ska visa resultatet i kolumn 'Namn'." & vbLf & "I Babelbanks inställningar står det att denna kolumn inte ska visas.	"
                sDanish = "Der er lagt en SQL ind for validering." & vbLf & "" & vbLf & "Denne SQL skal vise resultatet i kolonnen 'Navn'." & vbLf & "I Babelbanks opsætning står der, at denne kolonne ikke skal vises.	"
            Case 19727
                sNorwegian = "Det er feil i det oppgitte beløpet (%1)."
                sEnglish = "The amount stated (%1) is not valid."
                sSwedish = "Det finns fel i det angivna beloppet (%1)."
                sDanish = "Der er fejl i det angivne beløb (%1)."
            Case 19728
                sNorwegian = "Det er feil i oppgitt valutakode (%1)."
                sEnglish = "The currency stated (%1) is not valid."
                sSwedish = "Det finns fel i den angivna valutakoden (%1)."
                sDanish = "Der er fejl i den angivne valutakode (%1)."
            Case 19729
                sNorwegian = "Det er feil i det oppgitte gebyret (%1)."
                sEnglish = "The charges stated (%1) is not valid."
                sSwedish = "Det finns fel i den angivna avgiften (%1)."
                sDanish = "Der er fejl i det angivne gebyr (%1)."
            Case 19730
                sNorwegian = "Endringene vil ikke bli lagret."
                sEnglish = "The changes will not be stored."
                sSwedish = "Ändringarna sparas inte."
                sDanish = "Ændringerne vil ikke blive gemt."
            Case 19731
                sNorwegian = "Feil i inntastet informasjon."
                sEnglish = "Error in entered data"
                sSwedish = "Fel i den angivna informationen."
                sDanish = "Fejl i indtastet information."
            Case 19732
                sNorwegian = "%3  BabelBank finner ikke postert faktura i Eurosoft," & vbLf & "og kan derfor ikke legge på nødvendig posteringsinformasjon." & vbLf & "Vennligst start opp manuell postering og opphev eller poster innbetaling på nytt." & vbLf & "Betaler navn: %1" & vbLf & "Beløp: %2"
                sEnglish = "%3   BabelBank was not able to locate the invoice in Eurosoft," & vbLf & "thus not able add necessary information." & vbLf & "Please reenter manual matching and rematch or undo the payment." & vbLf & "Payers name: %1" & vbLf & "Amount: %2"
                sSwedish = "%3  BabelBank hittar inte bokförd faktura i Eurosoft" & vbLf & "och kan därför inte lägga till nödvändig bokföringsinformation." & vbLf & "Starta manuell bokföring och ångra eller bokför inbetalningen på nytt." & vbLf & "Betalarens namn: %1" & vbLf & "Belopp: %2" & vbLf & "Bet.konto: %4	"
                sDanish = "%3  BabelBank finder ikke posteret faktura i Eurosoft," & vbLf & "og kan derfor ikke tilføje nødvendig posteringsinformation." & vbLf & "Start manuel postering op og ophæv eller poster indbetaling på ny." & vbLf & "Betaler navn: %1" & vbLf & "Beløb: %2" & vbLf & "Bet.konto: %4	"
            Case 19733
                sNorwegian = "Beløpet som er lagt inn er ikke gyldig"
                sEnglish = "The amount entered is not valid."
                sSwedish = "Beloppet som angetts är inte giltigt"
                sDanish = "Beløbet som er lagt ind er ikke gyldigt"
            Case 19734
                sNorwegian = "Feil i beløp!"
                sEnglish = "Error in amount!"
                sSwedish = "Fel i belopp!"
                sDanish = "Fejl i beløb!"
            Case 19735
                sNorwegian = "Feil KID:"
                sEnglish = "Wrong KID:"
                sSwedish = "Fel OCR:"
                sDanish = "Forkert KID:"
            Case 19736
                sNorwegian = "Melding fra BabelBank: "
                sEnglish = "Message from BabelBank: "
                sSwedish = "Meddelande från BabelBank:"
                sDanish = "Meddelelse fra BabelBank:"
            Case 19737
                sNorwegian = "FAKTURA VIL BLI OVERBETALT!"
                sEnglish = "THE INVOICE WILL BE OVERPAID!"
                sSwedish = "FAKTURA KOMMER ATT VARA ÖVER BETALT!"
                sDanish = "FAKTURA VIL BLIVE OVER BETALT!"
            Case 19738
                sNorwegian = "Tilbakebetales"
                sEnglish = "Repayment"
                sSwedish = "Återbetalning"
                sDanish = "Tilbagebetaling"

                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 19750

            Case 19900
                sNorwegian = "FEIL UNDER ETTERBEHANDLING AV AVSTEMMING"
                sEnglish = "ERROR IN POSTMATCHING"
                sSwedish = "FEL UNDER EFTERBEHANDLING AV AVSTÄMNING"
                sDanish = "FEJL UNDER EFTERBEHANDLING AF AFSTEMNING"
            Case 19901
                sNorwegian = "19901: Ingen avstemmings-ID er angitt på en avstemt post" & vbLf & "Betalers navn: %1" & vbLf & "Totalbeløp: %3" & vbLf & "Dato: %4" & vbLf & "Fakturabeløp: %2"
                sEnglish = "19901: No match_ID stated on a matched transaction" & vbLf & "Payers name: %1" & vbLf & "Total amount: %3" & vbLf & "Date: %4" & vbLf & "Invoice amount: %2"
                sSwedish = "19901: Inget avstämnings-ID har angetts för en avstämd post" & vbLf & "Betalarens namn: %1" & vbLf & "Totalbelopp: %3" & vbLf & "Datum: %4" & vbLf & "Fakturabelopp: %2	"
                sDanish = "19901: Der er ikke angivet nogen afstemnings-ID på en afstemt post" & vbLf & "Betalers navn: %1" & vbLf & "Totalbeløb: %3" & vbLf & "Dato: %4" & vbLf & "Fakturabeløb: %2	"
            Case 19902
                sNorwegian = "19902: Sist brukte bilagsnummer er ikke angitt på klient %1." & vbLf & "Oppgi dette i klientoppsettet."
                sEnglish = "19902: No voucher number is stated on the client: %1." & vbLf & "Please enter this in client setup."
                sSwedish = "19902: Senast använda verifikationsnummer har inte angetts på klient %1." & vbLf & "Ange detta i klientinställningarna.	"
                sDanish = "19902: Sidst brugte bilagsnummer er ikke angivet på klient %1." & vbLf & "Angiv dette i klientopsætningen.	"
            Case 19903
                sNorwegian = "19903: Babelbank kan ikke hente sist brukte bilagsnummer fra ERP-systemet på klient %1." & vbLf & "Vennlgst kontroller SQL-setningen."
                sEnglish = "19903: No voucher number is retrieved from the ERP on the client: %1." & vbLf & "Please check the SQL."
                sSwedish = "19903: BabelBank kan inte hämta senast använda verifikationsnummer från ERP-system till klienten %1." & vbLf & "Kontrollera SQL-satsen.	"
                sDanish = "19903: Babelbank kan ikke hente sidst brugte bilagsnummer fra ERP-systemet på klient %1." & vbLf & "Kontroller SQL-indstillingen.	"
            Case 19904
                sNorwegian = "19904: Ukorret bruk av bilagsnummer." & vbLf & "I klient oppsettet er det satt at BabelBank skal hente bilagsnummeret" & vbLf & "fra ERP-systemet, men det er ikke laget en SQL for å gjøre dette. Klient: %1."
                sEnglish = "19904: Inconsistent use of vouchernumber." & vbLf & "Under client setup it is marked for retrieving a voucher number" & vbLf & "from the ERP-system. But no SQL are stated to do this for client %1."
                sSwedish = "19904: Felaktig användning av verifikationsnummer." & vbLf & "I klientinställningarna är det angivet att BabelBank ska hämta verifikationsnumret" & vbLf & "från ERP-systemet, men det har inte skapats någon SQL för detta. Klient: %1.	"
                sDanish = "19904: Ukorrekt brug af bilagsnummer." & vbLf & "I klientopsætningen er det indstillet, at BabelBank skal hente bilagsnummeret" & vbLf & "fra ERP-systemet, men der er ikke lavet en SQL til at gøre dette. Klient: %1.	"
            Case 19905
                sNorwegian = "19905: BabelBank kan ikke hente ut sist brukte bilagsnummer fra ERP-systemet. Klient: %1"
                sEnglish = "19905: BabelBank can't retrieve the last used voucher number fromt the ERP-system." & vbLf & "Client: %1"
                sSwedish = "19905: BabelBank kan inte hämta senast använda verifikationsnummer från ERP-systemet. Klient: %1"
                sDanish = "19905: BabelBank kan ikke hente det sidste anvendte bilagsnummer fra ERP-systemet. Klient: %1"
            Case 19906
                sNorwegian = "19906: Mangler valutakode for OriginalAmount. Sett opp valutakode for kontoen, eller som basisvaluta under Firmaopplysninger."
                sEnglish = "Currencycode for Originally paid amount is missing. Please specify currencycode for current account, or in Companyinfo as basecurrency."
                sSwedish = "19906: Saknar vautakod för OriginalAmount. Ställ in valutakod för kontot eller som basvaluta under Företagsinformation."
                sDanish = "19906: Mangler valutakode for OriginalAmount. Opret valutakode for kontoen, eller som basisvaluta under Firmaoplysninger."
            Case 20000
                sNorwegian = "----- (Error in export of KID-transactions) -----"
                sEnglish = "----- (Error during export of KID transactions) -----"
                sSwedish = "----- (Error in export of KID-transactions) -----"
                sDanish = "----- (Error in export of KID-transactions) -----"
            Case 20001
                sNorwegian = "FEIL UNDER EKSPORT AV KID-TRANSAKSJONER"
                sEnglish = "ERROR DURING EXPORT OF KID-TRANSACTIONS"
                sSwedish = "FEL UNDER  EXPORT AV OCR-TRANSAKTIONER"
                sDanish = "FEJL UNDER EKSPORT AF KID-TRANSAKTIONER"
            Case 22501
                sNorwegian = "FEIL UNDER AUTOMATPOSTERING"
                sEnglish = "ERROR DURING AUTOBOOKING"
                sSwedish = "FEL UNDER AUTOBOKFÖRING"
                sDanish = "FEJL UNDER AUTOMATPOSTERING"
            Case 25000
                sNorwegian = "------ RESERVED FOR UTILS-STRINGS ----"
                sEnglish = "------ RESERVED FOR UTILS-STRINGS ----"
                '25001 - 25004 - moved to LRS_VB
                sSwedish = "------ RESERVED FOR UTILS-STRINGS ----"
                sDanish = "------ RESERVED FOR UTILS-STRINGS ----"
                'Case 25001
                '    sNorwegian = "25001: Kunne ikke koble til ekstern database."
                '    sEnglish = "25001: Couldn't connect to external database."
                sSwedish = "Det gick inte att ansluta till extern databas."
                sDanish = "Kunne ikke forbinde til ekstern database."
                'Case 25002
                '    sNorwegian = "Påloggingen til databasen rapporterte følgende feil:"
                '    sEnglish = "The connection reported the following error:"
                sSwedish = "Inloggningen på databasen genererade följande fel:"
                sDanish = "Pålogningen til databasen rapporterede følgende fejl:"
                'Case 25003
                '    sNorwegian = "Feil under oppbygning av påloggingsstreng."
                '    sEnglish = "Error during the building of the connectionstring."
                sSwedish = "Fel under konstruktion av inloggningssträng."
                sDanish = "Fejl under opbygning af pålogningsstreng."
            Case 25004
                sNorwegian = "Påloggingsstreng:"
                sEnglish = "Connectionstring:"
                sSwedish = "Inloggningssträng:"
                sDanish = "Pålogningsstreng:"
            Case 25005
                sNorwegian = "For å teste/endre påloggingsstreng, gå inn på Firmaopplysninger under Oppsett i BabelBank."
                sEnglish = "To test/change the connectionstring, start BabelBank and select Company under Setup in the menu."
                sSwedish = "Testa/ändra inloggningssträngen i företagsinformationen under Vyer i BabelBank."
                sDanish = "For at teste/ændre pålogningsstreng, gå ind på Firmaoplysninger under Opsætning i BabelBank."
            Case 25006
                sNorwegian = "Bruker ID:"
                sEnglish = "User ID:"
                sSwedish = "Användar-ID:"
                sDanish = "Bruger-ID:"
            Case 25007
                sNorwegian = "Passord:"
                sEnglish = "Password:"
                sSwedish = "Lösenord:"
                sDanish = "Kodeord:"
            Case 26000
                sNorwegian = "--- Error FI-Browser"
                sEnglish = "--- Error FI-Browser ---"
                sSwedish = "--- Error FI-Browser"
                sDanish = "--- Error FI-Browser"
            Case 26001
                sNorwegian = "Feil i bokføringsdato - Format: ÅÅÅÅMMDD"
                sEnglish = "Error in Book.Date - Format YYYYMMDD"
                sSwedish = "Fel i bokföringsdatum- Format: ÅÅÅÅMMDD"
                sDanish = "Fejl i bogføringsdato - Format: ÅÅÅÅMMDD"
            Case 26002
                sNorwegian = "Feil i innbetalingsdato - Format: ÅÅÅÅMMDD"
                sEnglish = "Error in Pay Date - Format YYYYMMDD"
                sSwedish = "Fel i inbetalningsdatum- Format: ÅÅÅÅMMDD"
                sDanish = "Fejl i indbetalingsdato - Format: ÅÅÅÅMMDD"
            Case 27000
                sNorwegian = "----- frmLogin/VBERPWorks ----"
                sEnglish = "----- frmLogin/VBERPWorks ----"
                sSwedish = "----- frmLogin/VBERPWorks ----"
                sDanish = "----- frmLogin/VBERPWorks ----"
            Case 27001
                sNorwegian = "Du har valgt å avbryte BabelBank."
                sEnglish = "You have chosen to cancel BabelBank."
                sSwedish = "Du har valt att avbryta BabelBank."
                sDanish = "Du har valgt at afbryde BabelBank."
            Case 27002
                sNorwegian = "For mange forsøk på å logge på ERP-system."
                sEnglish = "Too many atempts to log on to the ERP-system."
                sSwedish = "För många försök att logga in i ERP-systemet."
                sDanish = "For mange forsøg på at logge på ERP-system."
            Case 27003
                sNorwegian = "Det er ikke angitt noe passord. Prøv igjen!"
                sEnglish = "No Password entered, try again!"
                sSwedish = "Det finns inget angivet lösenord. Försök igen!"
                sDanish = "Der er ikke angivet noget kodeord. Prøv igen!"
            Case 27004
                sNorwegian = "Det er ikke angitt noen bruker-ID. Prøv igjen!"
                sEnglish = "No User name entered, try again!"
                sSwedish = "Det finns inget angivet användar-ID. Försök igen!"
                sDanish = "Det er ikke angivet noget bruger-ID. Prøv igen!"
            Case 27005
                sNorwegian = "Feil i gammelt passord"
                sSwedish = "Fel i gammalt lösenord"
                sDanish = "Fejl i gammelt kodeord"
            Case 27006
                sNorwegian = "Nytt passord ikke likt i de to feltene"
                sSwedish = "Nytt lösenord matchar inte i de två fälten"
                sDanish = "Nyt kodeord ikke det samme i de to felter"
            Case 30000
                sNorwegian = "----- (Vb_Utils messages) ----"
                sEnglish = "----- (Vb_Utils messages) ----"
                sSwedish = "----- (Vb_Utils messages) ----"
                sDanish = "----- (Vb_Utils messages) ----"
            Case 30001
                sNorwegian = "Kan ikke opprette katalogen %1." & vbLf & "" & vbLf & "Feil spesifikajon av katalogen."
                sEnglish = "Can't create the folder %1." & vbLf & "" & vbLf & "Wrong specification of folder."
                sSwedish = "Kan inte skapa katalogen %1." & vbLf & "" & vbLf & "Fel specifikation av katalogen.	"
                sDanish = "Kan ikke oprette kataloget %1." & vbLf & "" & vbLf & "Forkert specifikation for kataloget.	"
            Case 30002
                sNorwegian = "Katalogen %1 er skrivebeskyttet."
                sEnglish = "The folder %1 is read-only."
                sSwedish = "Katalogen %1 är skrivskyddad."
                sDanish = "Kataloget %1 er skrivebeskyttet."
            Case 30003
                sNorwegian = "Legg inn en annen katalog, eller endre rettighetene."
                sEnglish = "Change the folder, or change the attributes of the folder."
                sSwedish = "Amge en annan katalog eller annan behörighet."
                sDanish = "Tilføj et andet katalog eller rediger rettighederne."
            Case 30004
                sNorwegian = "Katalogen %1 finnes ikke."
                sEnglish = "Folder %1 does not exist."
                sSwedish = "Katalogen %1 finns inte."
                sDanish = "Kataloget %1 findes ikke."
            Case 30005
                sNorwegian = "Ønsker du å opprette katalogen?"
                sEnglish = "Do You want to create the folder?"
                sSwedish = "Vill du skapa katalogen?"
                sDanish = "Vil du oprette kataloget?"
            Case 30006
                sNorwegian = "Opprett katalogen, eller endre stien under oppsett."
                sEnglish = "Create the folder or change the path in setup."
                sSwedish = "Skapa katalogen eller ändra sökväg i inställningen."
                sDanish = "Opret kataloget, eller rediger stien under opsætning."
            Case 30007
                sNorwegian = "Stasjon %1 finnes ikke."
                sEnglish = "The drive %1 does not exist."
                sSwedish = "Station %1 finns inte."
                sDanish = "Drev %1 findes ikke."
            Case 30008
                sNorwegian = "Filen %1 er skrivebeskyttet. " & vbLf & "Kan ikke slette filen."
                sEnglish = "The file %1 is read-only. " & vbLf & "Can't delete the file."
                sSwedish = "Filen %1 är skrivskyddad." & vbLf & "Kan inte ta bort filen.	"
                sDanish = "Filen %1 er skrivebeskyttet." & vbLf & "Kan ikke slette filen.	"
            Case 30009
                sNorwegian = "Filen %1 kan ikke slettes."
                sEnglish = "The file %1 can't be deleted."
                sSwedish = "Filen %1 kan inte tas bort."
                sDanish = "Filen %1 kan ikke slettes."
            Case 35001
                sNorwegian = "Bankpartner:"
                sEnglish = "Bankpartner:"
                sSwedish = "Bankpartner:"
                sDanish = "Bankpartner:"
            Case 35002
                sNorwegian = "Profiler:"
                sEnglish = "Profiles:"
                sSwedish = "Profiler:"
                sDanish = "Profiler:"
            Case 35003
                sNorwegian = "Velg forbindelse"
                sEnglish = "Choose collection"
                sSwedish = "Välj anslutning"
                sDanish = "Vælg forbindelse"
            Case 35004
                sNorwegian = "Hjem"
                sEnglish = "Home"
                sSwedish = "Start"
                sDanish = "Hjem"
            Case 35005
                sNorwegian = "Oppsett"
                sEnglish = "Setup"
                sSwedish = "Vyer"
                sDanish = "Opsætning"
            Case 35006
                sNorwegian = "LeverandørNr."
                sEnglish = "SupplierNo."
                sSwedish = "Leverantörsnr"
                sDanish = "LeverandørNr."
            Case 35008
                sNorwegian = "Status"
                sEnglish = "Status"
                sSwedish = "Status"
                sDanish = "Status"
            Case 35009
                sNorwegian = "Type"
                sEnglish = "Type"
                sSwedish = "Typ"
                sDanish = "Type"
            Case 35010
                sNorwegian = "BetLnNo/LnNo"
                sEnglish = "PaymntLn/LnNo"
                sSwedish = "BetLnNo/LnNo"
                sDanish = "BetLnNo/LnNo"
            Case 35011
                sNorwegian = "Sumrapport"
                sEnglish = "Totalreport"
                sSwedish = "Summarapport"
                sDanish = "Sumrapport"
            Case 35012
                sNorwegian = "Detaljrapport"
                sEnglish = "Detailreport"
                sSwedish = "Detaljrapport"
                sDanish = "Detaljerapport"
            Case 35013
                sNorwegian = "Feilrapport"
                sEnglish = "Errorreport"
                sSwedish = "Felrapport"
                sDanish = "Fejlrapport"
            Case 35014
                sNorwegian = "Ingen profilgrupper funnet, vennligst lag en profil !"
                sEnglish = "No profilgroups found, please create a profile !"
                sSwedish = "Inga profilgrupper hittades, skapa en profil !"
                sDanish = "Ingen profilgrupper fundet, lav en profil !"
            Case 35015
                sNorwegian = "Parametre"
                sEnglish = "Parameters"
                sSwedish = "Parametre"
                sDanish = "Parametre"
            Case 35016
                sNorwegian = "Verdi"
                sEnglish = "Value"
                sSwedish = "Värde"
                sDanish = "Værdi"
            Case 35017
                sNorwegian = "Profilgruppe"
                sEnglish = "Profilegroup"
                sSwedish = "Profilgrupp"
                sDanish = "Profilgruppe"
            Case 35018
                sNorwegian = "Bank"
                sEnglish = "Bank"
                sSwedish = "Bank"
                sDanish = "Bank"
            Case 35019
                sNorwegian = "Kundenr./Org.nr."
                sEnglish = "Bank customer number"
                sSwedish = "Kundenr./Org.nr."
                sDanish = "Kundenr./Org.nr."
            Case 35020
                sNorwegian = "Divisjon"
                sEnglish = "Division"
                sSwedish = "Division"
                sDanish = "Division"
            Case 35021
                sNorwegian = "Operatørnummer"
                sEnglish = "Operator number"
                sSwedish = "Operatörsnummer"
                sDanish = "Operatørnummer"
            Case 35022
                sNorwegian = "Passord"
                sEnglish = "Password"
                sSwedish = "Lösenord"
                sDanish = "Kodeord"
            Case 35023
                sNorwegian = "Dagsekvens"
                sEnglish = "Day Sequence"
                sSwedish = "Dagssekvens"
                sDanish = "Dagsekvens"
            Case 35024
                sNorwegian = "Bunting"
                sEnglish = "Bundling"
                sSwedish = "Buntning"
                sDanish = "Bundtning"
            Case 35025
                sNorwegian = "Separat provisjonspost"
                sEnglish = "Separate Commission"
                sSwedish = "Separat provisionspost"
                sDanish = "Separat provisionspost"
            Case 35026
                sNorwegian = "Sekvens per organisasjonsnummer"
                sEnglish = "Separate Control Sequence"
                sSwedish = "Sekvens per organisationsnummer"
                sDanish = "Sekvens pr. organisationsnummer"
            Case 35027
                sNorwegian = "Ekstra buntfelt"
                sEnglish = "Extra Bundle Field"
                sSwedish = "Extra buntfält"
                sDanish = "Ekstra bundtfelt"
            Case 35028
                sNorwegian = "Swiftkoder som skal ha blank sjekkode"
                sEnglish = "Swiftaddresses With Blank Cheque Code"
                sSwedish = "Swiftkoder ska ha tom kontrollkod"
                sDanish = "Swiftkoder som skal have blank checkkode"
            Case 35029
                sNorwegian = "Varslingstekst for blank sjekkode"
                sEnglish = "Notification When Blank Chequecode"
                sSwedish = "Aviseringstext för tom kontrollkod"
                sDanish = "Advarselstekst for blank checkkode"
            Case 35030
                sNorwegian = "Fast tekst for lønnstransaksjoner"
                sEnglish = "Fixed Text For Salary Payments"
                sSwedish = "Fast text för lönetransaktioner"
                sDanish = "Fast tekst for lønstransaktioner"
            Case 35031
                sNorwegian = "Skriv Telepay 1.0"
                sEnglish = "Write telepay 1.0"
                sSwedish = "Skriv Telepay 1.0"
                sDanish = "Skriv Telepay 1.0"
            Case 35032
                sNorwegian = "Konverter Masseoverføring uten kontonummer til sjekk"
                sEnglish = "Convert Masstransfer Without Account Number to Cheque"
                sSwedish = "Konvertera massöverföring utan kontonummer för kontroll"
                sDanish = "Konverter Masseoverførsel uden kontonummer til check"
            Case 35033
                sNorwegian = "Konverter poster uten melding til Masseoverførsel"
                sEnglish = "Convert Payment Without Notification to MassTransfer"
                sSwedish = "Konvertera poster utan meddelade till massöverföring"
                sDanish = "Konverter poster uden meddelelse til Masseoverførsel"
            Case 35034
                sNorwegian = "Fast tekst for overførsler uten melding"
                sEnglish = "Fixed Text When Transfer Without Notification"
                sSwedish = "Fast text för överföring utan meddelade"
                sDanish = "Fast tekst for overførsler uden meddelelse"
            Case 35035
                sNorwegian = "Skriv banknavn selv om swift"
                sEnglish = "Write Name Of Bank Even If Swift"
                sSwedish = "Skriv banknamn själv för swift"
                sDanish = "Skriv banknavn selv om swift"
            Case 35036
                sNorwegian = "FTP ProduksjonsID"
                sEnglish = "FTP Production ID"
                sSwedish = "FTP-produktionsID"
                sDanish = "FTP ProduktionsID"
            Case 35037
                sNorwegian = "Validering"
                sEnglish = "Validate"
                sSwedish = "Validering"
                sDanish = "Validering"
            Case 35038
                sNorwegian = "Informasjonstekst"
                sEnglish = "Information Text"
                sSwedish = "Informationstext"
                sDanish = "Informationstekst"
            Case 35039
                sNorwegian = "Datoinformasjon"
                sEnglish = "Information Date"
                sSwedish = "Datuminformation"
                sDanish = "Datoinformation"
            Case 35040
                sNorwegian = "Kreditnotaovervåking,dager"
                sEnglish = "CreditMemoDays"
                sSwedish = "Kreditnoteövervakning, dagar"
                sDanish = "Kreditnotaovervågning,dage"
            Case 35041
                sNorwegian = "Konto valutakode"
                sEnglish = "Account Currency Code"
                sSwedish = "Konto valutakod"
                sDanish = "Konto valutakode"
            Case 35042
                sNorwegian = "Tillat negativ verdi"
                sEnglish = "Allow Negative Value"
                sSwedish = "Tillåt negativt värde"
                sDanish = "Tilladt negativ værdi"
            Case 35043
                sNorwegian = "Tillat utenlandsk betaling"
                sEnglish = "Allow Foreign Payment"
                sSwedish = "Tillåt utländsk betalning"
                sDanish = "Tilladt udenlandsk betaling"
            Case 35044
                sNorwegian = "SWIFT kode for sjekker"
                sEnglish = "SWIFT Check for Cheques"
                sSwedish = "SWIFT-kod för kontroller"
                sDanish = "SWIFT kode for check"
            Case 35045
                sNorwegian = "Swift kode for Ikke sjekk"
                sEnglish = "SWIFT check For None Cheque"
                sSwedish = "Swiftkod för ej kontroll"
                sDanish = "Swift kode for Ikke check"
            Case 35046
                sNorwegian = "Gyldig valuta for SWIFT sjekk"
                sEnglish = "Valid currency for SWIFT check"
                sSwedish = "Giltig valuta för SWIFT-kontroll"
                sDanish = "Gyldig valuta for SWIFT check"
            Case 35047
                sNorwegian = "Konverter IBAN til BBAN"
                sEnglish = "Convert IBAN to BBAN"
                sSwedish = "Konvertera IBAN till BBAN"
                sDanish = "Konverter IBAN til BBAN"
            Case 35048
                sNorwegian = "Valider lengde på betalers kontonummer"
                sEnglish = "Validate length of payer's account number"
                sSwedish = "Validera längd betalarens kontonummer"
                sDanish = "Valider længde på betalers kontonummer"
            Case 35049
                sNorwegian = "Felles transaksjoner"
                sEnglish = "Joint transactions"
                sSwedish = "Vanliga transaktioner"
                sDanish = "Fælles transaktioner"
            Case 35050
                sNorwegian = "Skilletegn for felles transaksjoner"
                sEnglish = "Punctuation when joint transactions"
                sSwedish = "Avgränsare för vanliga transaktioner"
                sDanish = "Skilletegn for fælles transaktioner"
            Case 35051
                sNorwegian = "Kundeenhet Id"
                sEnglish = "CustomerNo"
                sSwedish = "Kundenhets-id"
                sDanish = "Kundeenhed Id"
            Case 35052
                sNorwegian = "Avtale Id"
                sEnglish = "AgreementNo"
                sSwedish = "Avtals-id"
                sDanish = "Aftale Id"
            Case 35053
                sNorwegian = "Forsendelsesnummer"
                sEnglish = "ShipmentNo"
                sSwedish = "Försändelsenummer"
                sDanish = "Forsendelsesnummer"
            Case 35054
                sNorwegian = "Oppdragsnummer"
                sEnglish = "ComissionNo"
                sSwedish = "Uppdragsnummer"
                sDanish = "Opgavenummer"
            Case 35055
                sNorwegian = "Alltid skriv melding dersom ikke KID"
                sEnglish = "Always Write Message If No CID"
                sSwedish = "Skriver alltid meddelande om ej OCR"
                sDanish = "Skriv altid meddelelse hvis ikke KID"
            Case 35056
                sNorwegian = "Skriv AvtaleGiro"
                sEnglish = "Write Direct Debit"
                sSwedish = "Skriv avtalsgiro"
                sDanish = "Skriv AftaleGiro"
            Case 35057
                sNorwegian = "Varslingstype AvtaleGiro"
                sEnglish = "Notification type Direct Debit"
                sSwedish = "Aviseringstyp avtalsgiro"
                sDanish = "Varslingstype AftaleGiro"
            Case 35058
                sNorwegian = "Skriv AutoGiro"
                sEnglish = "Write AutoGiro"
                sSwedish = "Skriv autogiro"
                sDanish = "Skriv AutoGiro"
            Case 35059
                sNorwegian = "AutoGiro uten fullmakt"
                sEnglish = "AutoGiro with no authority"
                sSwedish = "Autogiro utan fullmakt"
                sDanish = "AutoGiro uden fuldmagt"
            Case 35060
                sNorwegian = "Les oppdragsfil og skriv samme fil (AutoGiro)"
                sEnglish = "Read and write same File AutoGiro"
                sSwedish = "Läs uppdragsfil och skriv samma fil (autogiro)"
                sDanish = "Læs opgavefil og skriv samme fil (AutoGiro)"
            Case 35061
                sNorwegian = "Tekst i fremmedreferanse"
                sEnglish = "Foreign Ref Txt"
                sSwedish = "Text med utländsk referens"
                sDanish = "Tekst i fremmedreference"
            Case 35062
                sNorwegian = "Overstyre fast tekst"
                sEnglish = "Overwrite Foreign Ref Txt"
                sSwedish = "Åsidosätt fast text"
                sDanish = "Overstyre fast tekst"
            Case 35063
                sNorwegian = "Meldingstekst"
                sEnglish = "Message Txt"
                sSwedish = "Meddelandetext"
                sDanish = "Meddelelsestekst"
            Case 35064
                sNorwegian = "Moduluskode for betalers referansenummer (Autogiro fullmaktsoppdrag)"
                sEnglish = "MOD Code"
                sSwedish = "Modulkod för betalarens referensnummer (autogiro med fullmakt)"
                sDanish = "Moduluskode for betalers referencenummer (Autogiro fuldmagtsopgave)"
            Case 35065
                sNorwegian = "Type KID bytte fil"
                sEnglish = "Type Of CID Change"
                sSwedish = "Typ OCR-utväxlingsfil"
                sDanish = "Type KID skifte fil"
            Case 35066
                sNorwegian = "Ny oppdragskonto"
                sEnglish = "New commission Account"
                sSwedish = "Nytt uppdragskonto"
                sDanish = "Ny opgavekonto"
            Case 35067
                sNorwegian = "Gammel oppdragskonto"
                sEnglish = "Old Commission Account"
                sSwedish = "Gammalt uppdragskonto"
                sDanish = "Gammel opgavekonto"
            Case 35068
                sNorwegian = "Skriv Direkte Remittering"
                sEnglish = "Write Dir.Rem"
                sSwedish = "Skriv direktremittering"
                sDanish = "Skriv Direkte Remittering"
            Case 35069
                sNorwegian = "Tegn for indikering av bunting ved direkte remittering"
                sEnglish = "Bundle Char."
                sSwedish = "Tecken som tyder på buntning genom direkt överföring"
                sDanish = "Tegn for indikering af bundtning ved direkte remittering"
            Case 35070
                sNorwegian = "Bruk oppdragsdato i oppdragsbunting"
                sEnglish = "Use Date"
                sSwedish = "Använd uppdragsdatum vid buntning"
                sDanish = "Brug opgavedato i opgavebundtning"
            Case 35071
                sNorwegian = "Dataavsender"
                sEnglish = "Data Sender"
                sSwedish = "Dataavsändare"
                sDanish = "Dataafsender"
            Case 35072
                sNorwegian = "Kontaktperson"
                sEnglish = "Contact"
                sSwedish = "Kontaktperson"
                sDanish = "Kontaktperson"
            Case 35073
                sNorwegian = "Telefonnummer"
                sEnglish = "Phone"
                sSwedish = "Telefonnummer"
                sDanish = "Telefonnummer"
            Case 35074
                sNorwegian = "Inkluder egenreferanse i buntebegrep"
                sEnglish = "Use Own Ref When Bundling"
                sSwedish = "Inkludera egen referens i buntbegrepp"
                sDanish = "Inkluder egenreference i bundtebegreb"
            Case 35075
                sNorwegian = "Skriv underspesifikasjon"
                sEnglish = "Write Bundle Spesification"
                sSwedish = "Skriv underspecifikation"
                sDanish = "Skriv underspecifikation"
            Case 35076
                sNorwegian = "Meldingstype kontooverførsel"
                sEnglish = "Message Type"
                sSwedish = "Meddelandetyp kontoöverföring"
                sDanish = "Meddelelsestype kontooverførsel"
            Case 35077
                sNorwegian = "Meldingstype innbetalingskort"
                sEnglish = "Message Type FIK"
                sSwedish = "Meddelandetyp inbetalningskort"
                sDanish = "Meddelelsestype indbetalingskort"
            Case 35078
                sNorwegian = "Melding til mottaker ved sjekk"
                sEnglish = "Message Type Cheque"
                sSwedish = "Meddelande till mottagare vid check"
                sDanish = "Meddelelse til modtager ved check"
            Case 35079
                sNorwegian = "Alternativ avsender"
                sEnglish = "Use Alternate Senders Name"
                sSwedish = "Alternativ avsändare"
                sDanish = "Alternativ afsender"
            Case 35080
                sNorwegian = "Brev til avsender"
                sEnglish = "Note To Sender"
                sSwedish = "Brev till avsändaren"
                sDanish = "Brev til afsender"
            Case 35081
                sNorwegian = "Sjekkmottaker"
                sEnglish = "Cheque Recipient"
                sSwedish = "Checkmottagare"
                sDanish = "Checkmodtager"
            Case 35082
                sNorwegian = "Avdeling - Registreringsnummer"
                sEnglish = "Cheque Delivery Place"
                sSwedish = "Avdelning - Registreringsnummer"
                sDanish = "Afdeling - Registreringsnummer"
            Case 35083
                sNorwegian = "Sjekkbeløp inklusive omkostninger"
                sEnglish = "Cheque Expenses Inkl"
                sSwedish = "Kontrollera belopp inklusive kostnader"
                sDanish = "Checkbeløb inklusive omkostninger"
            Case 35084
                sNorwegian = "Krysse sjekk"
                sEnglish = "Cheque Crossing"
                sSwedish = "Märk check"
                sDanish = "Afstemt check"
            Case 35085
                sNorwegian = "Egenreferanse"
                sEnglish = "OwnRef"
                sSwedish = "Egen referens"
                sDanish = "Egenreference"
            Case 35086
                sNorwegian = "FIK Spesial"
                sEnglish = "FIKSpecial"
                sSwedish = "FIK Special"
                sDanish = "Gironummer Special"
            Case 35087
                sNorwegian = "Postnr, poststed og landkode i følge SHB"
                sEnglish = "SHBSpecial"
                sSwedish = "Postnummer, ort och landsnummer enligt SHB"
                sDanish = "Postnr, by og landekode ifølge SHB"
            Case 35088
                sNorwegian = "Sortering"
                sEnglish = "Sorting"
                sSwedish = "Sortering"
                sDanish = "Sortering"
            Case 35089
                sNorwegian = "Bunting per dato"
                sEnglish = "DateBundling"
                sSwedish = "Buntning efter datum"
                sDanish = "Bundtning pr. dato"
            Case 35090
                sNorwegian = "Skriv referanse for å lese in avregningsretur"
                sEnglish = "WriteOwnref2"
                sSwedish = "Skriv referens för inläsning i avräkningsretur"
                sDanish = "Skriv reference for at indlæse afregningsretur"
            Case 35091
                sNorwegian = "SISU/BankgirotFilter"
                sEnglish = "Filter SISU BankGirot"
                sSwedish = "SISU/BankgirotFilter"
                sDanish = "SISU/BankgiroFilter"
            Case 35096
                sNorwegian = "Parameter %1 ikke funnet"
                sEnglish = "Parameter %1 not found"
                sSwedish = "Parametern %1 hittades inte"
                sDanish = "Parameter %1 ikke fundet"
            Case 35097
                sNorwegian = "Betalingen har negativt beløp"
                sEnglish = "The payment has a negative amount"
                sSwedish = "Betalningen har ett negativt belopp"
                sDanish = "Betalingen har negativt beløb"
            Case 35098
                sNorwegian = "Det finnes beløp med negativ verdi, se statuskolonnen"
                sEnglish = "There are payments with negative value, see statuscolumn"
                sSwedish = "Det finns mängd negativt värde, se kolumnen Status"
                sDanish = "Der findes beløb med negativ værdi, se statuskolonnen"
            Case 35099
                sNorwegian = "Filnavnet %1 er ikke et gyldig filnavn"
                sEnglish = "The filename %1 is not a valid filename"
                sSwedish = "Filnamnet %1 är inte ett giltigt filnamn"
                sDanish = "Filnavnet %1 er ikke et gyldigt filnavn"
            Case 35100
                sNorwegian = "Finner ikke loggfilen %1"
                sEnglish = "Can't find the logfile %1"
                sSwedish = "Hittar inte loggfilen %1"
                sDanish = "Kan ikke finde logfilen %1"
            Case 35101
                sNorwegian = "Denne profilgruppe inneholder ikke en profil av type"
                sEnglish = "This ProfileGroup doesn't contain a Profile of the type"
                sSwedish = "Denna profilgrupp innehåller inte en profil av typ"
                sDanish = "Denne profilgruppe indeholder ikke en profil af type"
            Case 35102
                sNorwegian = "Det finnes to forskjellige divisjoner på samme bankpartner"
                sEnglish = "Two different divisions on same bankpartner"
                sSwedish = "Det finns två olika avdelningar hos samma bankpartner"
                sDanish = "Der findes to forskellige divisioner på samme bankpartner"
            Case 35103
                sNorwegian = "Mottaksretur"
                sEnglish = "Confirmationfile"
                sSwedish = "Mottagningskvitto"
                sDanish = "Modtagelsesretur"
            Case 35104
                sNorwegian = "Det skal være eksakt null eller en post valgt når denne SQL-en er kjørt."
                sEnglish = "There have to be zero or one record selected when this SQL is processed"
                sSwedish = "Det bör vara exakt noll eller en vald post när följande SQL körs."
                sDanish = "Det skal være eksakt nul eller en post valgt når denne SQL-en er kørt."
            Case 35105
                sNorwegian = "BabelBank fant"
                sEnglish = "BabelBank found"
                sSwedish = "BabelBank hittade"
                sDanish = "BabelBank fandt"
            Case 35106
                sNorwegian = "posteringer."
                sEnglish = "records."
                sSwedish = "poster."
                sDanish = "posteringer."
            Case 35107
                sNorwegian = "Vil du slette profilen"
                sEnglish = "Delete the profile"
                sSwedish = "Vill du ta bort profilen"
                sDanish = "Vil du slette profilen"
            Case 35108
                sNorwegian = "Merk alle"
                sEnglish = "Select all"
                sSwedish = "Markera alla"
                sDanish = "Marker alle"
            Case 35109
                sNorwegian = "Avmerk alle"
                sEnglish = "Deselect all"
                sSwedish = "Avmarkera alla"
                sDanish = "Afmarker alle"
            Case 35110
                sNorwegian = "Eksporter"
                sEnglish = "Export"
                sSwedish = "Exportera"
                sDanish = "Eksporter"
            Case 35111
                sNorwegian = "Avbryt resterende"
                sEnglish = "Cancel rest"
                sSwedish = "Avbryt återst."
                sDanish = "Afbryd resterende"
            Case 35112
                sNorwegian = "&Sumrapport"
                sEnglish = "Totalreport"
                sSwedish = "&Summarapport"
                sDanish = "&Sumrapport"
            Case 35113
                sNorwegian = "&Detaljrapport"
                sEnglish = "Detailreport"
                sSwedish = "&Detaljrapport"
                sDanish = "&Detaljerapport"
            Case 35114
                sNorwegian = "&Feilrapport"
                sEnglish = "Errorreport"
                sSwedish = "&Felrapport"
                sDanish = "&Fejlrapport"
            Case 35115
                sNorwegian = "Handlinger"
                sEnglish = "Actions"
                sSwedish = "Åtgärder"
                sDanish = "Handlinger"
            Case 35116
                sNorwegian = "Vil du avbryte, uten å eksportere?"
                sEnglish = "Cancel process, without export?"
                sSwedish = "Vill du avbryta utan att exportera?"
                sDanish = "Vil du afbryde uden at eksportere?"
            Case 35117
                sNorwegian = "Vil du avbryte all videre prosessering, uten eksport?"
                sEnglish = "Canvel all futher processing, with export?"
                sSwedish = "Vill du avbryta all vidare behandling, utan export?"
                sDanish = "Vil du afbryde alle yderligere processer, uden eksport?"
            Case 35118
                sNorwegian = "L&ukk"
                sEnglish = "&Close"
                sSwedish = "Stä&ng"
                sDanish = "L&uk"
            Case 35119
                sNorwegian = "&Endre"
                sEnglish = "C&hange"
                sSwedish = "&Ändra"
                sDanish = "&Ændr"
            Case 35120
                sNorwegian = "&Språk"
                sEnglish = "&Language"
                sSwedish = "&Språk"
                sDanish = "&Sprog"
            Case 35121
                sNorwegian = "L&ogg"
                sEnglish = "L&og"
                sSwedish = "L&ogg"
                sDanish = "L&og"
            Case 35122
                sNorwegian = "Det er koblet profiler til denne profilgruppen. Slett disse først!"
                sEnglish = "There ar profiles conected to this profilegroup. Please delete those first."
                sSwedish = "Det finns profiler som är kopplade till den här profilgruppen. Ta bort dessa först!"
                sDanish = "Der er koblet profiler til denne profilgruppe. Slet disse først!"
            Case 35123
                sNorwegian = "Slett profilgruppe"
                sEnglish = "Delete profilegroup"
                sSwedish = "Ta bort profilgrupp"
                sDanish = "Slet profilgruppe"
            Case 35124
                sNorwegian = "Le&gg til eksport"
                sEnglish = "&Add export"
                sSwedish = "Lä&gg till export"
                sDanish = "Ti&lføj til eksport"
            Case 35125
                sNorwegian = "&Slett eksport"
                sEnglish = "&Delete export"
                sSwedish = "&Ta bort export"
                sDanish = "&Slet eksport"
            Case 35126
                sNorwegian = "&Ny profilgruppe"
                sEnglish = "&Add profilegroup"
                sSwedish = "&Nyt profilgrupp"
                sDanish = "&Ny profilgruppe"
            Case 35127
                sNorwegian = "&Slett profilgr."
                sEnglish = "&Del. profilegroup"
                sSwedish = "&Ta bort profilgr."
                sDanish = "&Slet profilgr."
            Case 35128
                sNorwegian = "Vil du slette dette klientoppsettet ?"
                sEnglish = "Delete this clientsetup?"
                sSwedish = "Vill du ta bort denna klientinställning ?"
                sDanish = "Vil du slette denne klientopsætning?"
            Case 35129
                sNorwegian = "&Endre eksport"
                sEnglish = "&Change export"
                sSwedish = "&Ändra export"
                sDanish = "&Ændr eksport"
            Case 35130
                sNorwegian = "Le&gg til import"
                sEnglish = "&Add import"
                sSwedish = "Lä&gg till import"
                sDanish = "Ti&lføj til import"
            Case 35131
                sNorwegian = "&Slett import"
                sEnglish = "&Delete import"
                sSwedish = "&Ta bort import"
                sDanish = "&Slet import"
            Case 35132
                sNorwegian = "&Endre import"
                sEnglish = "&Change import"
                sSwedish = "&Ändra import"
                sDanish = "&Ændr import"
            Case 35133
                sNorwegian = "Avbryt &denne"
                sEnglish = "Cancel &this"
                sSwedish = "Avbryt &denna"
                sDanish = "Afbryd &denne"
            Case 35134
                sNorwegian = "&Importer"
                sEnglish = "&Import"
                sSwedish = "&Importera"
                sDanish = "&Importer"
            Case 35135
                sNorwegian = "Vil du avbryte, uten å importere ?"
                sEnglish = "Cancel process, without import ?"
                sSwedish = "Vill du avbryta utan att importera ?"
                sDanish = "Vil du afbryde, uden at importere ?"
            Case 35136
                sNorwegian = "Leser betalinger for klient"
                sEnglish = "Retrieves payments for client"
                sSwedish = "Läser betalningar för kunden"
                sDanish = "Læser betalinger for klient"
            Case 35137
                sNorwegian = "Datauthenting ferdig, klargjør grid."
                sEnglish = "Retrieving of data completed, starting grid"
                sSwedish = "Läsning färdigt, förbered rutnät."
                sDanish = "Datalæsning færdig, klargør grid."
            Case 35138
                sNorwegian = "Avbrutt av bruker, videre behandling avbrytes"
                sEnglish = "Cancelled by user, further processing stopped"
                sSwedish = "Användaren avbröt, ytterligare behandling avbryts"
                sDanish = "Afbrudt af bruger, videre behandling afbrydes"
            Case 35139
                sNorwegian = "Avsluttet Grid, fortsetter behandling"
                sEnglish = "Grid closed, process continues"
                sSwedish = "Avslutat rutnät, fortsätter behandlingen"
                sDanish = "Afsluttet Grid, fortsætter behandling"
            Case 35140
                sNorwegian = "Kjørt funksjonene 'Visma_CheckIfAllPaymentlinesAreUnselected', starter eksport til fil:"
                sEnglish = "The function 'Visma:CheckIfAllPaymentlinesAreUnselected' is run, starting export to file:"
                sSwedish = "Körde funktionerna 'Visma_CheckIfAllPaymentlinesAreUnselected', börjar exportera till fil:"
                sDanish = "Kørt funktionerne 'Visma_CheckIfAllPaymentlinesAreUnselected', starter eksport til fil:"
            Case 35141
                sNorwegian = "Feil under eksport til fil:"
                sEnglish = "Error during export to file:"
                sSwedish = "Fel vid export till fil:"
                sDanish = "Fejl under eksport til fil:"
            Case 35142
                sNorwegian = "Vellykket oppdatering av status i Visma."
                sEnglish = "Succesfull export of status to the Visma database."
                sSwedish = "Felfri uppdatering av status i Visma."
                sDanish = "Vellykket opdatering af status i Visma."
            Case 35143
                sNorwegian = "Eksport ferdig."
                sEnglish = "Export completed."
                sSwedish = "Export klar."
                sDanish = "Eksport færdig."
            Case 35144
                sNorwegian = "Avbrutt av bruker."
                sEnglish = "Cancelled by user."
                sSwedish = "Avbröts av användaren."
                sDanish = "Afbrudt af bruger."
            Case 35145
                sNorwegian = "Starter behandling av import."
                sEnglish = "Starts processing of importfiles."
                sSwedish = "Startar behandling av importen."
                sDanish = "Starter behandling af import."
            Case 35146
                sNorwegian = "Det ble ikke funnet noe oppsett for import i denne profilgruppen."
                sEnglish = "No setup for import was found in this profilegroup."
                sSwedish = "Det gick inte att hitta någon inställning för import i denna profilgrupp."
                sDanish = "Der blev ikke fundet nogen opsætning for import i denne profilgruppe."
            Case 35147
                sNorwegian = "Sjekker om det finnes filer å importere."
                sEnglish = "Checking if there are files to import"
                sSwedish = "Kontrollerar om det finns filer att importera."
                sDanish = "Kontrollerer om der er filer at importere."
            Case 35148
                sNorwegian = "Det ble ikke funnet noe filer å importere."
                sEnglish = "There were not found any files to import"
                sSwedish = "Det finns inte någon fil att importera."
                sDanish = "Der blev ikke fundet nogen filer at importere."
            Case 35149
                sNorwegian = "Starter import av filen:"
                sEnglish = "Starting import of file:"
                sSwedish = "Startar import av filen:"
                sDanish = "Starter import af filen:"
            Case 35150
                sNorwegian = "Feil under import av filen:"
                sEnglish = "Error while importing the file:"
                sSwedish = "Fel under import av filen:"
                sDanish = "Fejl under import af filen:"
            Case 35151
                sNorwegian = "PROGRAM AVBRYTES!"
                sEnglish = "PROGRAM INTERRUPTED!"
                sSwedish = "PROGRAM AVBRYTS!"
                sDanish = "PROGRAM AFBRYDES!"
            Case 35152
                sNorwegian = "Filen: %1 inneholder ingen transaksjoner."
                sEnglish = "The file %! has no transactions."
                sSwedish = "Filen: %1 innehåller inga transaktioner."
                sDanish = "Filen: %1 indeholder ingen transaktioner."
            Case 35153
                sNorwegian = "Vellykket import av filen:"
                sEnglish = "Succesfull import of the file:"
                sSwedish = "Felfri import av filen:"
                sDanish = "Vellykket import af filen:"
            Case 35155
                sNorwegian = "Prosessen ble avbrutt av bruker."
                sEnglish = "The process was cancelled by the user."
                sSwedish = "Processen avbröts av användaren."
                sDanish = "Processen blev afbrudt af bruger."
            Case 35156
                sNorwegian = "Det ble ikke funnet noen betalinger i importfilene."
                sEnglish = "No payments where found in the importfiles."
                sSwedish = "Det fanns inga betalningar i importfilerna."
                sDanish = "Der blev ikke fundet nogen betalinger i importfilerne."
            Case 35157
                sNorwegian = "Starter oppdatering av mottaksretur, filnavn:"
                sEnglish = "Starts update of confirmationfile, filename:"
                sSwedish = "Startar uppdatering av mottagningsretur, filnamn:"
                sDanish = "Starter opdatering af modtagsretur, filnavn:"
            Case 35158
                sNorwegian = "Vellykket behandling av mottaksretur, filnavn:"
                sEnglish = "Succesfull processing of confimationfile, filename:"
                sSwedish = "Felfri behandling av mottagningsretur, filnamn:"
                sDanish = "Vellykket behandling af modtagsretur, filnavn:"
            Case 35159
                sNorwegian = "Uspesifisert feil i behandling av mottaksretur, filnavn:"
                sEnglish = "Unspecified error in processing of confirmationfile, filename:"
                sSwedish = "Ospecificerat fel vid behandling av mottagningsretur, filnamn:"
                sDanish = "Uspecificeret fejl i behandling af modtagsretur, filnavn:"
            Case 35160
                sNorwegian = "Starter oppdatering av avregningsretur, filnavn:"
                sEnglish = "Starts update of settlementfile, filename:"
                sSwedish = "Starta uppdatering av avräkningsretur, filnamn:"
                sDanish = "Starter opdatering af afregningsretur, filnavn:"
            Case 35161
                sNorwegian = "Vellykket behandling av avregningsretur, filnavn:"
                sEnglish = "Successful update of settlementfile, filename:"
                sSwedish = "Felfri behandling av avräkningsretur, filnamn:"
                sDanish = "Vellykket behandling af afregningsretur, filnavn:"
            Case 35162
                sNorwegian = "Uspesifisert feil i behandling av avregningsretur, filnavn:"
                sEnglish = "Unspecified error in processing of settlementfile, filename:"
                sSwedish = "Ospecificerat fel vid behandling av avräkningsretur, filnamn:"
                sDanish = "Uspecificeret fejl i behandling af afregningsretur, filnavn:"
            Case 35163
                sNorwegian = "Starter oppdatering av innbetalingsfil, filnavn:"
                sEnglish = "Starts update of incoming paymentsfile, filename:"
                sSwedish = "Starta uppdatering av inbetalningsfil, filnamn:"
                sDanish = "Starter opdatering af indbetalingsfil, filnavn:"
            Case 35164
                sNorwegian = "Vellykket behandling av innbetalingsfil, filnavn:"
                sEnglish = "Starts update of incoming paymentsfile, filename:"
                sSwedish = "Felfri behandling av inbetalningsfil, filnamn:"
                sDanish = "Vellykket behandling af indebetalingsfil, filnavn:"
            Case 35165
                sNorwegian = "Uspesifisert feil i behandling av innbetalingsfil, filnavn:"
                sEnglish = "Unspecified error in processing of incoming paymentsfile, filename:"
                sSwedish = "Ospecificerat fel vid behandling inbetalningsfil, filnamn:"
                sDanish = "Uspecificeret fejl i behandling af indbetalingsfil, filnavn:"
            Case 35166
                sNorwegian = "Starter oppdatering av avvisningsfil, filnavn:"
                sEnglish = "Starts update of rejectionfile, filename:"
                sSwedish = "Starta uppdatering av avvisningsfil, filnamn:"
                sDanish = "Starter opdatering af afvisningsfil, filnavn:"
            Case 35167
                sNorwegian = "Vellykket behandling av avvisningsfil, filnavn:"
                sEnglish = "Starts update of rejectionfile, filename:"
                sSwedish = "Felfri behandling av avvisningsfil, filnamn:"
                sDanish = "Vellykket behandling af afvisningsfil, filnavn:"
            Case 35168
                sNorwegian = "Uspesifisert feil i behandling av avvisningsfil, filnavn:"
                sEnglish = "Unspecified error in processing of rejectionfile, filename:"
                sSwedish = "Ospecificerat fel vid behandling av avvisningsfil, filnamn:"
                sDanish = "Uspecificeret fejl i behandling af afvisningsfil, filnavn:"
            Case 35169
                sNorwegian = "Vellykket kjøring av BabelBank."
                sEnglish = "Successful processing in BabelBank."
                sSwedish = "Felfritt genomförande av BabelBank."
                sDanish = "Vellykket kørsel af BabelBank."
            Case 35170
                sNorwegian = "Mislykket kjøring av BabelBank."
                sEnglish = "Unsuccessful processing in BabelBank."
                sSwedish = "Misslyckad exekvering BabelBank."
                sDanish = "Mislykket kørsel af BabelBank."
            Case 35171
                sNorwegian = "Starter konvertering av Convert-X databasen"
                sEnglish = "Starts converting the Convert-X database"
                sSwedish = "Börjar konvertera Convert-X-databasen"
                sDanish = "Starter konvertering af Convert-X databasen"
            Case 35172
                sNorwegian = "Convert-X database er vellykket konvertert til BabelBanks database"
                sEnglish = "The Convert-X database is succesfully converted to the BabelBank database."
                sSwedish = "Convert-X-databasen konverterades felfritt till BabelBanks databas"
                sDanish = "Convert-X database er konverteret vellykket til BabelBanks database"
            Case 35173
                sNorwegian = "Convert-X databasen ble IKKE vellykket konvertert!"
                sEnglish = "The Convert-X database was NOT succesfully converted."
                sSwedish = "Convert-X-databasen konverterades inte felfritt!"
                sDanish = "Convert-X databasen blev IKKE konverteret vellykket!"
            Case 35174
                sNorwegian = "Leser oppsett fra database."
                sEnglish = "Reads setup from the database"
                sSwedish = "Läser inställningar från databasen."
                sDanish = "Læser opsætning fra database."
            Case 35175
                sNorwegian = "Har lest fra databasen selskaper som skal remitteres"
                sEnglish = "Has found in the database which client to remit"
                sSwedish = "Har läst från databasen vilka företag som ska remitteras"
                sDanish = "Har læst fra databasen, hvilke selskaber som skal remitteres"
            Case 35176
                sNorwegian = "Bruker har avbrutt prosessen."
                sEnglish = "User has cancelled the process"
                sSwedish = "Användaren har avbrutit processen."
                sDanish = "Bruger har afbrudt processen."
            Case 35177
                sNorwegian = "Eksporterer"
                sEnglish = "Exports"
                sSwedish = "Exporterar"
                sDanish = "Eksporterer"
            Case 35178
                sNorwegian = "Ingen betalinger behandlet"
                sEnglish = "No payments handled"
                sSwedish = "Inga betalningar har behandlats"
                sDanish = "Ingen betalinger behandlet"
            Case 35179
                sNorwegian = "Sekvensnummer"
                sEnglish = "Sequencenumber"
                sSwedish = "Sekvensnummer"
                sDanish = "Sekvensnummer"
            Case 35180
                sNorwegian = "Vil du slette parameteren %1 ?"
                sEnglish = "Delete the parameter %1 ?"
                sSwedish = "Vill du ta bort parametern %1 ?"
                sDanish = "Vil du slette parameteren %1 ?"
            Case 35181
                sNorwegian = "Sekvensnummerangivelse settes slik: 'SEQ#nnnnnnnn#XXXXXXXX'  hvor nnnnnnnn er foretaksnr., XXXXXXXX er divisjon"
                sEnglish = "The setup for sequencenumbers are: 'SEQ#nnnnnnnn#XXXXXXXX'  where nnnnnnnn is companyno., XXXXXXXX is division."
                sSwedish = "Sekvensnummer anges enligt följande: 'SEQ#nnnnnnnn#XXXXXXXX' där nnnnnnnn är föetagsnr och XXXXXXXX divisionen"
                sDanish = "Sekvensnummerangivelse angives sådan: 'SEQ#nnnnnnnn#XXXXXXXX'  hvor nnnnnnnn er virksomhedsnr., XXXXXXXX er division"
            Case 35182
                sNorwegian = "Oppgrader views"
                sEnglish = "Upgrade views"
                sSwedish = "Uppgradera vyer"
                sDanish = "Opgrader views"
            Case 35183
                sNorwegian = "Firmanummer"
                sEnglish = "Companynumber"
                sSwedish = "Företagsnummer"
                sDanish = "Firmanummer"
            Case 35184
                sNorwegian = "Lagre profiler ?"
                sEnglish = "Save profile ?"
                sSwedish = "Spara profiler ?"
                sDanish = "Gemme profiler ?"
            Case 35185
                sNorwegian = "Vellykket oppgradering av BabelBank views"
                sEnglish = "Succesfully updated the BabelBank views"
                sSwedish = "Felfri uppgradering av BabelBank-vyer"
                sDanish = "Vellykket opgradering af BabelBank views"
            Case 35186
                sNorwegian = "Forbereder uttrekk fra Visma Business databasen"
                sEnglish = "Preparing retrieval of data from the Visma Business database"
                sSwedish = "Förbereder hämtning av data fån Visma Business databasen"
                sDanish = "Forbereder uttrekk fra Visma Business databasen"
            Case 35187
                sNorwegian = "Ingen betalinger funnet."
                sEnglish = "No payments found."
                sSwedish = "Inga utbetalningar hittades."
                sDanish = "Ingen betalinger fundet."
            Case 35188
                sNorwegian = "BabelBank-eksport AVBRYTES"
                sEnglish = "BabelBank export stops."
                sSwedish = "BabelBank-export AVBRYTS"
                sDanish = "BabelBank-eksport AFBRYDES"
            Case 35189
                sNorwegian = "Det ble funnet en ny, ukjent, konto ved import av returfiler."
                sEnglish = "A new and unknown account is found when importing file."
                sSwedish = "Ett nytt, okänt konto hittades vid import av returfiler."
                sDanish = "Der blev fundet en ny, ukendt, konto ved import af returfiler."
            Case 35190
                sNorwegian = "Det finnes allerede et oppsett for BabelBank."
                sEnglish = "A setup for BabelBank already exists."
                sSwedish = "Det finns redan en inställning för BabelBank."
                sDanish = "Der findes allerede en opsætning for BabelBank."
            Case 35191
                sNorwegian = "Fortsetter du kjøringen vil dette bli overskrevet med data fra oppsettet i ConvertX."
                sEnglish = "If you continue, this setup will be overwritten by the settings from ConvertX."
                sSwedish = "Fortsätts körningen så skrivs data över med data från inställningen i ConvertX."
                sDanish = "Fortsætter du kørslen vil dette blive overskrevet med data fra opsætningen i ConvertX."
            Case 35192
                sNorwegian = "Overskriv BabelBanks oppsett?"
                sEnglish = "Overwrite the BabelBank setup?"
                sSwedish = "Skriva över BabelBank-inställning?"
                sDanish = "Skal BabelBanks opsætning overskrives?"
            Case 35193
                sNorwegian = "BabelBank greier ikke å finne den riktige scriptfilen for å oppdatere viewene."
                sEnglish = "BabelBank is unable to find the correct scriptfile to update the views."
                sSwedish = "BabelBank hittar inte rätt skriptfil för att uppdatera vyerna."
                sDanish = "BabelBank kan ikke finde den rigtige scriptfil til at opdatere viewene."
            Case 35194
                sNorwegian = "Scriptfilen skal være plassert i %1."
                sEnglish = "The scritpfile should be stored in %1."
                sSwedish = "Skriptfil bör finnas i %1."
                sDanish = "Scriptfilen skal være placeret i %1."
            Case 35195
                sNorwegian = "Eksport-/importformatet %1 er ennå ikke implementert i BabelBank."
                sEnglish = "The export-/Importformat %1 is not yet implemented in BabelBank."
                sSwedish = "Export-/importformatet %1 är ännu inte implementerat i BabelBank."
                sDanish = "Eksport-/importformatet %1 er endnu ikke implementeret i BabelBank."
            Case 35196
                sNorwegian = "Dette oppsettet vil ikke konverteres til BabelBank."
                sEnglish = "This export/import will not be converted to BabelBank."
                sSwedish = "Denna inställning kommer inte att konverteras till BabelBank."
                sDanish = "Denne opsætning kan ikke konverteres til BabelBank."
            Case 35197
                sNorwegian = "Avregning"
                sEnglish = "Settlement"
                sSwedish = "Avräkning"
                sDanish = "Afregning"
            Case 35198
                sNorwegian = "Faktura ikke funnet!"
                sEnglish = "Invoice not found!"
                sSwedish = "Faktura kan ej hittas!"
                sDanish = "Faktura ikke fundet!"
            Case 35199
                sNorwegian = "Avvik"
                sEnglish = "Deviation"
                sSwedish = "Avvikelser"
                sDanish = "Afvig"
            Case 35200
                sNorwegian = "BabelBank finner ikke den riktige databasen i Visma."
                sEnglish = "BabelBank is unable to find the appropriate ConvertX-database in Visma."
                sSwedish = "BabelBank kan inte hitta rätt databas i Visma."
                sDanish = "BabelBank finder ikke den rigtige database i Visma."
            Case 35201
                sNorwegian = "Kan ikke finne ConvertX databasen"
                sEnglish = "Can't find the ConvertX-database!"
                sSwedish = "Kan inte hitta ConvertX-databas"
                sDanish = "Kan ikke finde ConvertX databasen"
            Case 35202
                sNorwegian = "Fant ingen formater i formattabellen."
                sEnglish = "Found no formats in the formattable."
                sSwedish = "Inga format i formattabellen."
                sDanish = "Fandt ingen formater i formattabellen."
            Case 35203
                sNorwegian = "Mangler Feilføringskonto eller Fast konto for innbetalinger som ikke avstemmes, konto %1."
                sEnglish = "Missing Posting errors account or Fixed account for non-reconciled payment received for account %1."
                sSwedish = "Saknar Felföringskonto eller Fast konto för betalningar som inte är avstämda, konto %1."
                sDanish = "Mangler Fejlføringskonto eller Fast konto for indbetalinger som ikke afstemmes, konto %1."
            Case 35204
                sNorwegian = "Hovedbokskonto mangler i Bedriftsopplysninger, 'Feilføringskonto' eller Factoringselskap, 'Fast konto for innbetalinger som ikke avstemmes'."
                sEnglish = "General ledger account is missing in Firm Data, 'Posting errors account' or Factoring firm, 'Fixed account for non-reconciled payment received'."
                sSwedish = "Huvudbokskonto saknas i företagsinformationen, felföringskonto  eller factoringföretag, fast konto för inbetalningar som inte är avstämda."
                sDanish = "Hovedbokskonto mangler i Bedriftsopplysninger, 'Fejlføringskonto' eller Factoringselskap, 'Fast konto for indbetalinger som ikke afstemmes'."
            Case 35205
                sNorwegian = "BabelBank fant %1 records."
                sEnglish = "BabelBank found %1 records."
                sSwedish = "BabelBank hittade %1 poster."
                sDanish = "BabelBank fandt %1 poster."
            Case 35206
                sNorwegian = "Foreløpig"
                sEnglish = "Draft"
                sSwedish = "Preliminär"
                sDanish = "Foreløbig"
            Case 35207
                sNorwegian = "Ingen betalinger funnet"
                sEnglish = "No payments found"
                sSwedish = "Inga utbetalningar funna"
                sDanish = "Ingen betalinger fundet"
            Case 35208
                sNorwegian = "Det ble ikke funnet noe remitteringoppsett for brukeren: %1."
                sEnglish = "No remittancesetup found for user: %1"
                sSwedish = "Det gick inte att hitta någon remitteringsinställning för användare: %1."
                sDanish = "Der blev ikke fundet nogen remitteringopsætning for brugeren: %1."
            Case 35209
                sNorwegian = "Denne SQL-en skal returnere eksakt en post, SQL: %1"
                sEnglish = "There should be exactly one record retrieved when running this SQL: %1"
                sSwedish = "Denna SQL ska returnera exakt en post, SQL: %1"
                sDanish = "Denne SQL skal returnere eksakt en post, SQL: %1"
            Case 35210
                sNorwegian = "Denne SQL-en skal sette inn eksakt en rad, SQL: %1"
                sEnglish = "There should be exactly one record inserted when running this SQL: %1"
                sSwedish = "Denna SQL ska infoga exakt en rad, SQL: %1"
                sDanish = "Denne SQL skal indsætte eksakt en linje, SQL: %1"
            Case 35211
                sNorwegian = "Denne SQL-en skal oppdatere eksakt en rad, SQL: %1"
                sEnglish = "There should be exactly one record updated when running this SQL: %1"
                sSwedish = "Denna SQL ska uppdatera exakt en rad, SQL: %1"
                sDanish = "Denne SQL skal opdatere eksakt en linje, SQL: %1"
            Case 35212
                sNorwegian = "Rapporter lagres i"
                sEnglish = "Save reports into"
                sSwedish = "Rapporter lagras i"
                sDanish = "Rapporter gemmes i"
            Case 35213
                sNorwegian = "Rapportoppsett"
                sEnglish = "Reportsetup"
                sSwedish = "Rapportvy"
                sDanish = "Rapportopsætn."
            Case 35214
                sNorwegian = "Vår konto"
                sEnglish = "Our account"
                sSwedish = "Vårt konto"
                sDanish = "Vores konto"
            Case 35215
                sNorwegian = "Konto"
                sEnglish = "Account"
                sSwedish = "Konto"
                sDanish = "Konto"
            Case 35216
                sNorwegian = "Knytt konto til et firma"
                sEnglish = "Attach a company for this account"
                sSwedish = "Koppla konto till ett företag"
                sDanish = "Knyt konto til et firma"
            Case 35217
                sNorwegian = "Avviste betalinger"
                sEnglish = "Rejected payments"
                sSwedish = "Avvisade betalningar"
                sDanish = "Afviste betalinger"
            Case 35218
                sNorwegian = "Manuelle innbetalinger"
                sEnglish = "Incoming manual payments"
                sSwedish = "Manuella inbetalningar"
                sDanish = "Manuelle indbetalinger"
            Case 35219
                sNorwegian = "Velg profilgruppe"
                sEnglish = "Select profilegroup"
                sSwedish = "Välj profilgrupp"
                sDanish = "Vælg profilgruppe"
            Case 35220
                sNorwegian = "Arkivref"
                sEnglish = "Archiveref"
                sSwedish = "Arkivref"
                sDanish = "Arkivref"
            Case 35221
                sNorwegian = "Kan ikke opprette rapportkatalog %1"
                sEnglish = "Unable to create reportpath %1"
                sSwedish = "Kan inte skapa rapportkatalog %1"
                sDanish = "Kan ikke oprette rapportkatalog %1"
            Case 35222
                sNorwegian = "Dette profilgruppenavn finnes allerede."
                sEnglish = "This profilegroupname is already in use"
                sSwedish = "Detta profilgruppsnamn finns redan."
                sDanish = "Dette profilgruppenavn findes allerede."
            Case 35223
                sNorwegian = "Du må velge et format for filen %1"
                sEnglish = "Please select a format for file %1"
                sSwedish = "Du måste välja ett format för filen %1"
                sDanish = "Du skal vælge et format for filen %1"
            Case 35224
                sNorwegian = "Bekreft at du ønsker å konvertere Convert-X databasen til BabelBank database"
                sEnglish = "Please confirm convertion of the Convert-X database to BabelBank database"
                sSwedish = "Bekräfta att du vill konvertera Convert-X-databasen till BabelBank-databas"
                sDanish = "Bekræft at du ønsker at konvertere Convert-X databasen til BabelBank database"
            Case 35225
                sNorwegian = "Bekreft at du ønsker å konvertere til nye BabelBank view"
                sEnglish = "Please confirm that you will create new BabelBank views"
                sSwedish = "Bekräfta att du vill konvertera till ny BabelBank-vy"
                sDanish = "Bekræft at du ønsker at konvertere til nye BabelBank view"
            Case 35226
                sNorwegian = "BabelBnk views ble vellykket oppdatert."
                sEnglish = "BabelBank views succesfully created."
                sSwedish = "BabelBank-vyerna har uppdaterats."
                sDanish = "BabelBnk views blev opdateret vellykket."
            Case 35227
                sNorwegian = "Feil under opprettelse av BabelBanks views!"
                sEnglish = "Error when creating BabelBank views."
                sSwedish = "Fel vid generering av BabelBank-vyer!"
                sDanish = "Fejl under opretholdelse af BabelBanks views!"
            Case 35228
                sNorwegian = "Produksjonsnummer settes slik: 'PROD#nnnnnnnn#YYMMDD'  hvor nnnnnnnn er foretaksnr., YYMMDD er dato"
                sEnglish = "Productionnumber is set like this: 'PROD#nnnnnnnn#YYMMDD'  where nnnnnnnn is enterprisenumber, YYMMDD is date."
                sSwedish = "Produktionsnummer ställs in så här: ''PROD#nnnnnnnn#ÅÅMMDD'' där nnnnnnnn är företagsnamn och ÅÅMMDD är datum"
                sDanish = "Produktionsnummer indstilles sådan: 'PROD#nnnnnnnn#YYMMDD'  hvor nnnnnnnn er virksomhedsnr., YYMMDD er dato"
            Case 35229
                sNorwegian = "Avvisning"
                sEnglish = "Rejection"
                sSwedish = "Avvisade"
                sDanish = "Afvisning"
            Case 35230
                sNorwegian = "Noe feil med rapportstien %1"
                sEnglish = "Something wrong in the reportpath %1"
                sSwedish = "Fel i rapportsökvägen %1"
                sDanish = "Nogle fejl med rapportstien %1"
            Case 35231
                sNorwegian = "(35231) Kan ikke fastslå firma. Mer enn en oppføring er funnet."
                sEnglish = "(35231) Not able to deduct correct firm. More than one entry found"
                sSwedish = "(35231) Det går inte att etablera företaget. Mer än en post hittades."
                sDanish = "(35231) Kan ikke fastslå firma. Mere end en registering??? er fundet."
            Case 35232
                sNorwegian = "(35232) Det er angitt i %1.dbo.Frm at firma med database %2 finnes."
                sEnglish = "(35232) It is stated in %1.dbo.Frm that company with the database %1 exists."
                sSwedish = "(35232) Anges i %1.dbo.Frm att företag med databasen %2 finns."
                sDanish = "(35232) Det er angivet i %1.dbo.Frm at firma med database %2 findes."
            Case 35233
                sNorwegian = "Imidlertid så finnes ikke denne databasen."
                sEnglish = "However, this database does could not be found."
                sSwedish = "Men denna databas finns inte."
                sDanish = "Imidlertid findes denne database ikke."
            Case 35234
                sNorwegian = "Firmaet blir ikke konvertert, men BabelBank fortsetter med konverteringen av andre firmaer."
                sEnglish = "This database will not be converted, but BabelBank will continue to convert other companies."
                sSwedish = "Företaget har inte konverterats, men BabelBank fortsätter med konverteringen av andra företag."
                sDanish = "Firmaet bliver ikke konverteret, men BabelBank fortsætter med konverteringen af andre firmaer."
            Case 35235
                sNorwegian = "(35235) BabelBank greier ikke å finne korrekt scriptfil for oppdatering av viewene."
                sEnglish = "(35235) BabelBank is not able to find the correct scriptfile to update the views."
                sSwedish = "(35235) BabelBank inte hittar rätt skriptfil för uppdatering av vyerna."
                sDanish = "(35235) BabelBank kan ikke finde korrekt scriptfil for opdatering af viewene."
            Case 35236
                sNorwegian = "Filen skal finnes i"
                sEnglish = "The file should be stored in"
                sSwedish = "Filen ska finnas i"
                sDanish = "Filen skal findes i"
            Case 35237
                sNorwegian = "Ukjent format"
                sEnglish = "Unknown format"
                sSwedish = "Okänt format"
                sDanish = "Ukendt format"
            Case 35238
                sNorwegian = "(35238) Fant ingen poster i BB_UserAccView for systembruker"
                sEnglish = "(35238) Found no records in BB_UserAccView for systemuser"
                sSwedish = "(35238) Inga poster i BB_UserAccView för systemanvändare"
                sDanish = "(35238) Fandt ingen poster i BB_UserAccView for systembruger"
            Case 35239
                sNorwegian = "Hvis det er feil systembruker så kan dette endres i Visma under EBF systemnøkkel i EBF opplysninger på rad nummer 2 (Nr=2)."
                sEnglish = "If this is not the correct systemuser, this can be corrected in Visma Business under EBF systemkey data on row 2 (No=2)."
                sSwedish = "Om det är fel systemanvändare kan detta ändras i Visma under  EBF-systemnyckeln i EBF-informationen på rad nummer 2 (Nr=2)."
                sDanish = "Hvis der er fejl i systembruger så kan dette ændres i Visma under EBF systemnøgle i EBF oplysninger på linje nummer 2 (Nr=2)."
            Case 35240
                sNorwegian = "Konto"
                sEnglish = "Account"
                sSwedish = "Konto"
                sDanish = "Konto"
            Case 35241
                sNorwegian = "(35241) Posten ble ikke oppdatert når denne SQL ble kjørt:"
                sEnglish = "(35241) The record was not updated when running this SQL:"
                sSwedish = "(35241) Posten uppdaterades inte när SQL kördes:"
                sDanish = "(35241) Posten blev ikke opdateret da denne SQL blev kørt:"
            Case 35242
                sNorwegian = "BabelBank oppdaterte %1 poster."
                sEnglish = "BabelBank update %1 records."
                sSwedish = "BabelBank uppdaterade %1 poster."
                sDanish = "BabelBank opdaterede %1 poster."
            Case 35243
                sNorwegian = "(35243) Bankpartner %1 for selskap %2  er ikke satt opp til å behandle returfiler fra bank."
                sEnglish = "(35243) Bankpartner %1 for company %2 is not set up to handle returnfiles from bank."
                sSwedish = "35243) Bankpartner %1 för företag %2 är inte inställd för att bearbeta returfiler från banken."
                sDanish = "(35243) Bankpartner %1 for selskab %2  er ikke sat op til at behandle returfiler fra bank."
            Case 35244
                sNorwegian = "Dette kan endres i oppsettet for EBF-behandling på bankpartneren."
                sEnglish = "This may be changed in the setup of EBF-processing on the bankpartner."
                sSwedish = "Detta kan ändras i inställningen för EBF-behandling av bankpartnern."
                sDanish = "Dette kan ændres i opsætningen for EBF-behandling på bankpartneren."
            Case 35245
                sNorwegian = "Merk av 'Generer bunt/bilagslinjer for avregning fra EBF-system'."
                sEnglish = "Tick of 'Create batch/paymentlines for settlement from EBF-system'."
                sSwedish = "Välj 'Generer bunt/bilagslinjer för avrekning från EBF-system'."
                sDanish = "Marker 'Generer bunt/bilagslinjer for avregning fra EBF-system'."
            Case 35246
                sNorwegian = "(35246) Bankkonto funnet på feil bankpartner:"
                sEnglish = "(35246) Bankaccount found on wrong bankpartner:"
                sSwedish = "(35246) Bankkonto hittades med fel bankpartner:"
                sDanish = "(35246) Bankkonto fundet på forkert bankpartner:"
            Case 35247
                sNorwegian = "Betalingen ble laget med bankpartner:"
                sEnglish = "The payment was created by using bankpartner:"
                sSwedish = "Betalningen skapades med bankpartner:"
                sDanish = "Betalingen blev lavet med bankpartner:"
            Case 35248
                sNorwegian = "(35248) Valutakoden på betalingen, %1 avviker fra valutakoden for kontoen %2."
                sEnglish = "(35248) The currency on the payment, %1 deviates from the currency of the Account, %2."
                sSwedish = "(35248) Valutakoden för betalningen, är %1, avviker från valutakoden för kontot %2."
                sDanish = "(35248) Valutakoden på betalingen, %1 afviger fra valutakoden for kontoen %2."
            Case 35249
                sNorwegian = "(35249) Betalers konto ble ikke funnet i Visma Business."
                sEnglish = "(35249) Payers bankaccount not found in Visma Business."
                sSwedish = "(35249) Betalarens konto hittades inte i Visma Business."
                sDanish = "(35249) Betalers konto blev ikke fundet i Visma Business."
            Case 35250
                sNorwegian = "Bankkontoen som er satt på betalingen:"
                sEnglish = "Bankaccount stated on the payment:"
                sSwedish = "Bankkonton som är inställda för betalningen:"
                sDanish = "Bankkontoen som er sat på betalingen:"
            Case 35251
                sNorwegian = "(35251) BabelBank greier ikke å finne en oppføring i factoring eller firmaoppsettet for konto %1"
                sEnglish = "(35251) BabelBank is not able to find an entry in the factoring or firm setup for the account %1"
                sSwedish = "(35251) BabelBank kan inte hitta en post i factoring- eller företagsinställning för konto %1"
                sDanish = "(35251) BabelBank kan ikke finde en opfølgning i factoring eller firmaopsætningen for konto %1"
            Case 35252
                sNorwegian = "Det burde finnes minst en record etter å kjørt denne SQL:"
                sEnglish = "There should be at least one record retrieved when running this SQL:"
                sSwedish = "Det bör finnas minst en post efter att ha kört denna SQL:"
                sDanish = "Der burde findes mindst en post efter at have kørt denne SQL:"
            Case 35253
                sNorwegian = "BabelBank fant"
                sEnglish = "BabelBank found"
                sSwedish = "BabelBank hittade"
                sDanish = "BabelBank fandt"
            Case 35254
                sNorwegian = "For mye"
                sEnglish = "Too much"
                sSwedish = "För mycket"
                sDanish = "For meget"
            Case 35255
                sNorwegian = "For lite"
                sEnglish = "Too little"
                sSwedish = "För lite"
                sDanish = "For lidt"
            Case 35256
                sNorwegian = "Det må være registrert minst en profilgruppe før du kan legge til en Import eller Eksport !"
                sEnglish = "At least one profilegroup must be present before you can add an Import or Export !"
                sSwedish = "Det måste finnas minst en angiven profilgrupp innan du kan lägga till en import eller export !"
                sDanish = "Der må være registreret mindst en profilgruppe før du kan tilføje en Import eller Eksport !"
            Case 35257
                sNorwegian = "Innbetalinger"
                sEnglish = "Incoming"
                sSwedish = "Inbetalningar"
                sDanish = "Indbetalinger"
            Case 35258
                sNorwegian = "Det er ikke angitt noe filnavn for eksport"
                sEnglish = "No filenavn given for exportfile!"
                sSwedish = "Inget filnamn har angetts för export"
                sDanish = "Der er ikke angivet noget filnavn for eksport"
            Case 35259
                sNorwegian = "Fant ingen firmaer å remittere for."
                sEnglish = "Found companies to remit."
                sSwedish = "Hittade inga företag för remittering."
                sDanish = "Fandt ingen firmaer at remittere for."
            Case 35260
                sNorwegian = "Ingen betalinger å eksportere."
                sEnglish = "No payments to export."
                sSwedish = "Inga betalningar att exportera."
                sDanish = "Ingen betalinger at eksportere."
            Case 35261
                sNorwegian = "Funnet informasjon om den aktive betalingen som skal remitteres."
                sEnglish = "Retrieved info about the active payment to remit."
                sSwedish = "Hittade information om den aktuella betalningen som ska remitteras."
                sDanish = "Fundet information om den aktive betaling som skal remitteres."
            Case 35262
                sNorwegian = "Funnet informasjon om betalinger som skal remitteres pr firma."
                sEnglish = "Retrived info about the payments to be retrieved per company."
                sSwedish = "Hittade information om betalningar som ska remitteras per företag."
                sDanish = "Fundet information om betalinger som skal remitteres pr. firma."
            Case 35263
                sNorwegian = "BabelBank prosessen er startet med følgende parametre:"
                sEnglish = "The BabelBank process is started with the following parameterstring:"
                sSwedish = "BabelBank-processen startades med följande parametrar:"
                sDanish = "BabelBank processen er startet med følgende parametre:"
            Case 35264
                sNorwegian = "Bruker %1 har ikke rettigheter til å oppdatere database %2, i henhold til oppsettet i viewet %3"
                sEnglish = "User %1 has not access to update database %2, according to the setup in %3"
                sSwedish = "Användaren %1 har inte behörighet att uppdatera databasen %2 i enlighet med inställningarna i vyn %3"
                sDanish = "Bruger %1 har ikke rettigheder til at opdatere database %2, i henhold til opsætningen i viewet %3"
            Case 35265
                sNorwegian = "Kontoen %1 finnes på flere selskaper i tabellen bbClient i databasen %2."
                sEnglish = "The account %1 is found in more than one company in the table bbClient in database %2."
                sSwedish = "Kontot %1 finns i flera företag i tabellen bbClient i databasen %2."
                sDanish = "Kontoen %1 findes på flere selskaber i tabellen bbClient i databasen %2."
            Case 35266
                sNorwegian = "&Oversiktsrapport"
                sEnglish = "&Overviewreport"
                sSwedish = "&Översiktsrapport"
                sDanish = "&Oversigtsrapport"
            Case 35267
                sNorwegian = "Betalingsoversikt"
                sEnglish = "Payment overview"
                sSwedish = "Betalningsöversikt"
                sDanish = "Betalingsoversigt"
            Case 35268
                sNorwegian = "Avvik:"
                sEnglish = "Deviation:"
                sSwedish = "Avvikelser:"
                sDanish = "Afvigelse:"
            Case 35269
                sNorwegian = "Bunting:"
                sEnglish = "BankVoucher:"
                sSwedish = "Buntning:"
                sDanish = "Bundtning:"
            Case 35270
                sNorwegian = "Bank HB:"
                sEnglish = "GL Bank:"
                sSwedish = "Bank-HB:"
                sDanish = "Bank HB:"
            Case 35271
                sNorwegian = "Feilkonto:"
                sEnglish = "Error account:"
                sSwedish = "Felkonto:"
                sDanish = "Fejlkonto:"
            Case 35272
                sNorwegian = "Enkelte parametere i KID oppsettet må være identiske per konto. For firma %1, konto %2, er de ikke identiske."
                sEnglish = "Some parameters in the CID Set up must be identical per account. For company %1 and bankaccount %2 they are not identical."
                sSwedish = "Vissa parametrar i OCR-inställningen ska vara identiska per konto. För företag %1, konto %2, är de inte identiska."
                sDanish = "Enkelte parametre i KID opsætningen skal være identiske pr. konto. For firma %1, konto %2, er de ikke identiske."
            Case 35273
                sNorwegian = "Konverterer ConvertX-database til BabelBank database. Framdrift %1 prosent."
                sEnglish = "Converts the ConvertX database to BabelBank database. Progress %1 percent."
                sSwedish = "Konverterar ConvertX-databas till BabelBank-databas. Förlopp %1 procent."
                sDanish = "Konverterer ConvertX-database til BabelBank database. Fremskridt %1 procent."
            Case 35274
                sNorwegian = "Ingen betalinger å eksportere fordi firmaet ikke finnes."
                sEnglish = "No payments to export because company doesn't exist."
                sSwedish = "Inga betalningar att exportera eftersom företaget inte finns."
                sDanish = "Ingen betalinger at eksportere fordi firmaet ikke findes."
            Case 35275
                sNorwegian = "35275 - Kan ikke finne firmanummer i filen - Kan filen være laget i annet system, eller være feilformatert?"
                sEnglish = "35275 - Can't find the companynumber in the file - Could this file have been created in another program, or is it corrupted?"
                sSwedish = "35275 - Kan inte hitta företagsnumret i filen - filen kan vara skapad i ett annat system, eller felaktigt formaterad?"
                sDanish = "35275 - Kan ikke finde firmanummer i filen - Kan filen være lavet i andet system, eller være fejlformateret?"
            Case 35276
                sNorwegian = "35276 - Det er en feil i formattabellen i BabelBank."
                sEnglish = "35276 - There is an error in the formattable of BabelBank."
                sSwedish = "35276 - Det finns ett fel i formattabellen i BabelBank."
                sDanish = "35276 - Der er en fejl i formattabellen i BabelBank."
            Case 35277
                sNorwegian = "Formattype for formatnummer: %1 er ikke angitt."
                sEnglish = "The type of format for formatno: %1 is not set."
                sSwedish = "Formattyp för formatnummer: %1 har inte angetts."
                sDanish = "Formattype for formatnummer: %1 er ikke angivet."
            Case 35278
                sNorwegian = "35278 - Importformatet med  ID %1  er ennå ikke implementert i BabelBank. Filnavn  %2"
                sEnglish = "35278 - The importformat with ID %1  is not yet implemented in BabelBank. Filename  %2"
                sSwedish = "35278 - Importformat med ID %1 är ännu inte implementerat i BabelBank. Filnamn %2"
                sDanish = "35278 - Importformatet med  ID %1  er endnu ikke implementeret i BabelBank. Filnavn  %2"
            Case 35279
                sNorwegian = "Starter VismaLicenseUtility."
                sEnglish = "Starts VismaLicenseUtility."
                sSwedish = "Startar VismaLicenseUtility"
                sDanish = "Starter VismaLicensUtility"
            Case 35280
                sNorwegian = "Avsluttet uttrekk av informasjon fra VismaLicenseUtility."
                sEnglish = "Completed informationcollection from VismaLicenseUtility."
                sSwedish = "Avslutat uttag av information från VismaLicenseUtility"
                sDanish = "Afsluttet udtræk af informationer fra Visma LicensUtility."
            Case 35281
                sNorwegian = "Feil i Visma lisenshåndtering. Feilmelding:"
                sEnglish = "Error in Visma Licensehandling. Error:"
                sSwedish = "Fel i Vismas licenshantering. Felmeddelande:"
                sDanish = "Fejl i Visma licenshåndtering. Fejlmelding:"
            Case 35282
                sNorwegian = "Du har ikke lisens til å kjøre Visma Payment (BabelBank)."
                sEnglish = "You're not licensed to run Visma Payment (BabelBank)"
                sSwedish = "Du har inte licens för Visma Payment"
                sDanish = "Du har ikke licens til at køre Visma Payment (BabelBank)."
            Case 35283
                sNorwegian = "Du har lisens for kjøring av OCR til kun en konto."
                sEnglish = "You're licensed to import OCR for one single account."
                sSwedish = "Du har endast licens för OCR till ett konto."
                sDanish = "Du har kun licens til at køre med OCR til en konto."
            Case 35284
                sNorwegian = "Databasever.:"
                sEnglish = "Databasever.:"
                sSwedish = "Databasever.:"
                sDanish = "Databasever.:"
            Case 35285
                sNorwegian = "Starter oppdatering av views for BabelBank i database (engangsjobb)."
                sEnglish = "Starts updating the BabelBank views."
                sSwedish = "Startar uppdatering av vyer för Visma Payment i databasen. (Detta jobb görs en gång.)"
                sDanish = "Starter opdatering af views til BabalBank i databasen (engangopgave)."
            Case 35286
                sNorwegian = "0ppdatering av views for BabelBank ferdig!"
                sEnglish = "The update of BabelBankviews has completed succesfully."
                sSwedish = "Uppdatering av vyer för Visma Payment är klar."
                sDanish = "Opdatering af views til BabelBank er færdig!"
            Case 35287
                sNorwegian = "Det er ikke valgt importfiler i oppsettet."
                sEnglish = "No import specified in the setup"
                sSwedish = "Importfiler är inte valt i uppsättet."
                sDanish = "Mindst to konti bliver opdateret i Visma Business, men du har kun licens til at opdatere en."
            Case 35288
                sNorwegian = "Minst to konti blir oppdatert i Visma Business, men du har lisens til kun å oppdatere en."
                sEnglish = "At least two accounts are being updated in Visma, but you have license to update only one account."
                sSwedish = "Minst två konton blir uppdaterade i Visma Business, men du har endast licens att uppdatera ett konto."
                sDanish = "Minst to konti blir oppdatert i Visma Business, men du har lisens til kun å oppdatere en."
            Case 35289
                sNorwegian = "Sist brukt:"
                sEnglish = "Last used:"
                sSwedish = "Senast använt:"
                sDanish = "Sidst brugt:"
            Case 35290
                sNorwegian = "Elektronisk betaling fra Leverantörsbetalningar,"
                sEnglish = "Electronic payment from Leverantörsbetalningar,"
                sSwedish = "Elektronisk betalning från Leverantörsbetalningar,"
                sDanish = "Elektronisk betaling fra leverandørbetalinger,"
            Case 35291
                sNorwegian = "Du har kommet til det siste lovlige bilagsnummeret i serien %1. Neste nummer er %2."
                sEnglish = "You have reached the last number of the vouchenumbers in the series %1. Next number is %2."
                sSwedish = "Du har hittat det sista tillåtna bilagsnummer i serien %1. Nesta nummer er %2."
                sDanish = "Du har kommet til det siste tilladte bilagsnummer i serien %1. Neste nummer er %2."
            Case 35292
                sNorwegian = "Det er ikke angitt noen øreavrundingskonto ved postering av innbetalinger."
                sEnglish = "No deviationaccount stated for posting of roundings for icoming payments."
                sSwedish = "Det er inte angivit någon øreavrundingskonto vid postering av innbetalningar."
                sDanish = "Det er ikke angivet noen øreavrundingskonto ved postering av indbetalinger."
            Case 35293
                sNorwegian = "SWIFT"
                sEnglish = "SWIFT"
                sSwedish = "SWIFT"
                sDanish = "SWIFT"
            Case 35294
                sNorwegian = "Det finnes ingen beskrivelse i KID-oppsettet for innbetalinger i VismaBusiness."
                sEnglish = "There is no description in the KID-setup for incoming payments in VismaBusiness."
                sSwedish = "Det finnes Ingen beskrivning av KID-Inställning for innbetalningar i VismaBusiness"
                sDanish = "Ingen beskrivelse i FIK/KID-layout for innbetalinger i VismaBusiness."
            Case 35295
                sNorwegian = "Lengde på betalt KID er:"
                sEnglish = "The length of the paid KID is:"
                sSwedish = "Längd på betald KID er:"
                sDanish = "Længde på betalt FIK/KID er:"
            Case 35296
                sNorwegian = "Finner ikke PKId = 2 i"
                sEnglish = "PKId = 2 is not found in"
                sSwedish = "Hittar inte PKId = 2 i"
                sDanish = "Fandt ikke PKId = 2 i"
            Case 35297
                sNorwegian = "Utbetalingen på firma %1, betaling %2, betalingslinje %3 på bankpartner %4 finnes ikke i Visma Business. Avslutt?"
                sEnglish = "The payment for company %1, payment %2, paymentline %3 on bankpartner %4 is not found in Visma Business. Exit?"
                sSwedish = "Utbetalningen för firma %1, betalning %2, betalningslinje %3 på bankpartner %4 fins inte i Visma Business. Avsluta?"
                sDanish = "Udbetalingen på firma %1, betaling %2, betalingslinje %3 på bankpartner %4 finnes ikke i Visma Business. Avslutt?"
            Case 35298
                sNorwegian = "Kan ikke oppdatere status på betalingen på firma %1, betaling %2, betalingslinje %3 på bankpartner %4 fordi den har feil status, status %5."
                sEnglish = "Unable to update status on the payment for company %1, payment %2, paymentline %3 on bankpartner %4 because it has wrong status, status %5."
                sSwedish = "Kan inte oppdatera status på betalningen för firma %1, betalning %2, betalningslinje %3 på bankpartner %4 fördi den har fel status, status %5."
                sDanish = "Kan ikke oppdatere status på betalingen på firma %1, betaling %2, betalingslinje %3 på bankpartner %4 fordi den har feil status, status %5."
            Case 35299
                sNorwegian = "Finner ikke betalingen"
                sEnglish = "Can't find the payment"
                sSwedish = "Hittar inte betalningen"
                sDanish = "Finder ikke betalingen"
            Case 35300
                sNorwegian = "Finner ikke firmaet %1 i databasen"
                sEnglish = "Unable to find company %1 in the database"
                sSwedish = "Kan inte hitta företag %1 i databasen"
                sDanish = "Kan ikke find firma %1 i databasen"
            Case 35301
                sNorwegian = "Egenreferanse %1 er ikke på korrekt format"
                sEnglish = "The own reference %1 is not on correct format"
                sSwedish = "Egenreferensen %1 är inte på rätt format"
                sDanish = "Egenreferanse %1 er ikke på korrekt format"
            Case 35302
                sNorwegian = "Avvist av BabelBank"
                sEnglish = "Rejected by BabelBank"
                sSwedish = "Avvisad av BabelBank"
                sDanish = "Afvist av BabelBank"
            Case 35303
                sNorwegian = "Ingen betalinger å eksportere - Avslutter"
                sEnglish = "No payments to export - processing stopped."
                sSwedish = "Inga betalningar till export - Slutar"
                sDanish = "Ingen betalinger til eksport - Slutter"
            Case 35304
                sNorwegian = "Det er ikke angitt noen konto for hovedbok bank, for bankkonto %1."
                sEnglish = "No bankledger account is stated for bankaccount %1."
                sSwedish = "Det är inte angett någon konto huvudbok för bankkonto %1."
                sDanish = "Det er ikke specificeret nogen konto hovedbog for bankkonto %1"
            Case 35305
                sNorwegian = "Kreditnota"
                sEnglish = "Credit note"
                sSwedish = "Kreditnota"
                sDanish = "Kreditnota"
            Case 35306
                sNorwegian = "En feil oppstod ved uthenting av posteringsdata."
                sEnglish = "An error occurred while retrieving posting data."
                sSwedish = "Ett fel inträffade vid hämtning av data."
                sDanish = "Der opstod en fejl under hentning af data."
            Case 35307
                sNorwegian = "35307: En feil oppstod når kobling settes til å lese %1 poster."
                sEnglish = "35307: An error occurred when setting the connection to read %1 posts."
                sSwedish = "35307: Ett fel inträffade när koblingen ställs inn til att läsa %1 poster."
                sDanish = "35307:Der opstod en fejl når forbindelsen indstilles til at læse %1 poster."
            Case 35308
                sNorwegian = "35308: Finner ingen betalingslinjer i Visma Business med NtfTxt referanse %1 "
                sEnglish = "35308: Can't find paymentlines in Visma Business with NtfTxt referanse %1 "
                sSwedish = "35308: Finner inga betalingslinjer i Visma Business med NtfTxt referens %1 "
                sDanish = "35308: Finner ingen betalingslinjer i Visma Business med NtfTxt referanse %1 "
            Case 35309
                sNorwegian = "35309: Feil beløpstotal i  betalingslinjer i Visma Business med NtfTxt referanse %1." & vbCrLf & "Beløp fra fil: %2" & vbCrLf & "Beløp fra Visma: %3"
                sEnglish = "35309: Wrong amounttotal for paymentlines in Visma Business with NtfTxt referanse %1 " & vbCrLf & "Amount in file: %2" & vbCrLf & "Amount in Visma: %3"
                sSwedish = "35309: Fel i beloppstotal i betalningslinjer i Visma Business med NtfTxt referens %1 " & vbCrLf & "Belopp från fil: %2" & vbCrLf & "Belopp från Visma: %3"
                sDanish = "35309: Feil i beløpstotal i betalingslinjer i Visma Business med NtfTxt referanse %1 " & vbCrLf & "Beløp fra fil: %2" & vbCrLf & "Beløp fra Visma: %3"
            Case 35310
                sNorwegian = "35310: Finner ikke Swiftkode for belastningskonto %1 i tabellen BankCon"
                sEnglish = "35310: Can't find Swiftcode for debitaccount %1 in the table BankCon"
                sSwedish = "35310: Hittar inte Swiftkod för debetkonto %1 i tabellen BankCon"
                sDanish = "35310: Finner ikke Swiftkode for debetkonto %1 i tabellen BankCon"
            Case 35311
                sNorwegian = "Ingen gyldige betalinger."
                sEnglish = "No valid payments."
                sSwedish = "Inga giltiga betalningar."
                sDanish = "Ingen gyldige betalinger."
            Case 35312
                sNorwegian = "Ingen betalinger på denne filen er generert av Visma Payment (BabelBank)."
                sEnglish = "No payments in this file is created by using Visma Payment (BabelBank)."
                sSwedish = "Inga betalningar på den här filen har genererast av Visma Payment (BabelBank)."
                sDanish = "Ingen betalinger på denne fil er genereret af Visma Payment (BabelBank)."
            Case 35313
                sNorwegian = "Oppsett på bankpartnere i Visma avviker."
                sEnglish = "The setup of bankpartners in Visma differs."
                sSwedish = "Inställning av bankpartnere i Visma skiljer sig."
                sDanish = "Opsæt af bankpartnere i Visma afviger."
            Case 35314
                sNorwegian = "Bank er satt opp forskjellig, henholdsvis " & vbCrLf & "%1 og %2"
                sEnglish = "Bank is set up differently, respectively " & vbCrLf & "%1 og %2"
                sSwedish = "Bank är inrättat olika, respektive " & vbCrLf & "%1 og %2"
                sDanish = "Bank er sat op forskelligt, henholdsvis " & vbCrLf & "%1 og %2"
            Case 35315
                sNorwegian = "Kundenummer bank er satt opp forskjellig, henholdsvis " & vbCrLf & "%1 og %2"
                sEnglish = "Customer number Bank is set up differently, respectively " & vbCrLf & "%1 og %2"
                sSwedish = "Kundnummer bank är inrättat olika, respektive " & vbCrLf & "%1 og %2"
                sDanish = "Kundenummer bank er sat op forskelligt, henholdsvis " & vbCrLf & "%1 og %2"
            Case 35316
                sNorwegian = "Divisjon er satt opp forskjellig, henholdsvis " & vbCrLf & "%1 og %2"
                sEnglish = "Division is set up differently, respectively " & vbCrLf & "%1 og %2"
                sSwedish = "Division är inrättat olika, respektive " & vbCrLf & "%1 og %2"
                sDanish = "Division er sat op forskelligt, henholdsvis " & vbCrLf & "%1 og %2"
            Case 35317
                sNorwegian = "Operatør er satt opp forskjellig, henholdsvis " & vbCrLf & "%1 og %2"
                sEnglish = "Operator is set up differently, respectively " & vbCrLf & "%1 og %2"
                sSwedish = "Operatör är inrättat olika, respektive " & vbCrLf & "%1 og %2"
                sDanish = "Operatør er sat op forskelligt, henholdsvis " & vbCrLf & "%1 og %2"
            Case 35318
                sNorwegian = "Det er ikke angitt hvilken bank som benyttes." & vbCrLf & "Dette må angis enten på bankpartner eller som parameter i oppsettet i Visma Payment (BabelBank)."
                sEnglish = "There is not stated any information about which bank the file will be sent to." & vbCrLf & "This mist be stated in either the bankpartner or in the setup of parameters in Visma Payment (BabelBank)."
                sSwedish = "Det är inte angett vilken bank som används." & vbCrLf & "Detta måste specificeras antingen i bankpartner eller som en parameter i inställningen av Visma Betalning (BabelBank)"
                sDanish = "Det er ikke specificeret, hvilken bank som anvendes." & vbCrLf & "Dette skal angives enten i bank partner eller som en parameter i opsætningen af Visma Payment (BabelBank)."
            Case 35319
                sNorwegian = "BatchBooking (Ja/Nei)"
                sEnglish = "BatchBooking (Yes/No)"
                sSwedish = "BatchBooking (Ja/Nei)"
                sDanish = "BatchBooking (Ja/Nei)"
			Case 35320
				sNorwegian = "SQL pålogging"
				sEnglish = "SQL login"
				sSwedish = "SQL login"
				sDanish = "SQL pålogging"

			Case 48000
                sNorwegian = "-------- (Database) --------"
                sEnglish = "--------- Database ---------"
                sSwedish = "-------- (Database) --------"
                sDanish = "-------- (Database) --------"
            Case 48001
                sNorwegian = "En-gangs format"
                sEnglish = "One-time format"
                sSwedish = "Engångsformat"
                sDanish = "En-gangs format"
            Case 48002
                sNorwegian = "SLETTING AV INNBETALINGER"
                sEnglish = "DELETE PAYMENTS"
                sSwedish = "BORTTAGNING AV INBETALNINGAR"
                sDanish = "SLETNING AF INDBETALINGER"
            Case 48003
                sNorwegian = "Ønsker du å slette lagrede betalinger i databasen?"
                sEnglish = "Do You want to delete stored payments from the database?"
                sSwedish = "Vill du ta bort lagrade betalningar i databasen?"
                sDanish = "Ønsker du at slette gemte betalinger i databasen?"
            Case 48004
                sNorwegian = "VELLYKKET SLETTING!"
                sEnglish = "DELETION SUCCEEDED!"
                sSwedish = "BORTTAGNING UTFÖRDES!"
                sDanish = "VELLYKKET SLETNING!"
            Case 48005
                sNorwegian = "Vellykket sletting i database!"
                sEnglish = "Deletion from database succeeded"
                sSwedish = "Felfri borttagning i databasen!"
                sDanish = "Vellykket sletning i database!"
            Case 48006
                sNorwegian = "MISYKKET SLETTING!"
                sEnglish = "DELETION FAILED!"
                sSwedish = "MISSLYCKAD BORTTAGNING!"
                sDanish = "MISLYKKET SLETNING!"
            Case 48007
                sNorwegian = "Misykket sletting i database!"
                sEnglish = "Deletion from database failed"
                sSwedish = "Misslyckad borttagning i databasen!"
                sDanish = "Mislykket sletning i database!"
            Case 48008
                sNorwegian = "Antall linjer i Import-informasjon: "
                sEnglish = "Nuber of lines in Import-information: "
                sSwedish = "Antal rader i importinformation:"
                sDanish = "Antal linjer i Import-information:"
            Case 48009
                sNorwegian = "Antall linjer slettet: "
                sEnglish = "Number of lines deleted: "
                sSwedish = "Antalet borttagna rader:"
                sDanish = "Antal linjer slettet:"
            Case 48010
                sNorwegian = "Antall linjer i Babelfile: "
                sEnglish = "Number of lines in Babelfile: "
                sSwedish = "Antal rader i Babelfile:"
                sDanish = "Antal linjer i Babelfile:"
            Case 48011
                sNorwegian = "Antall linjer i Filesetup: "
                sEnglish = "Number of lines in Filesetup: "
                sSwedish = "Antal rader i Filesetup:"
                sDanish = "Antal linjer i Filesetup:"
            Case 48012
                sNorwegian = "Antall linjer nullstillt: "
                sEnglish = "Number of lines reset: "
                sSwedish = "Antal rader som nollställts:"
                sDanish = "Antal linjer nulstillet:"
            Case 48013
                sNorwegian = "En uventet feil oppstod."
                sEnglish = "An unexcpected error occured."
                sSwedish = "Ett oväntat fel inträffade."
                sDanish = "Der opstod en uventet fejl."
            Case 48014
                sNorwegian = "Feil under uthenting av data"
                sEnglish = "Error retrieving data."
                sSwedish = "Fel vid hämtning av data"
                sDanish = "Fejl under hentning af data"
            Case 48015
                sNorwegian = "Endringene ble ikke lagret."
                sEnglish = "The changes were not saved."
                sSwedish = "Ändringarna sparades inte."
                sDanish = "Ændringerne blev ikke gemt."
            Case 48016
                sNorwegian = "Feil under lagring av data"
                sEnglish = "Error during saving data"
                sSwedish = "Fel under lagring av data"
                sDanish = "Fejl under lagring af data"
            Case 48017
                sNorwegian = "Databasetype må oppgis for å benytte arkiveringsfunksjonen."
                sEnglish = "A databasetype must be provided to use the archive function."
                sSwedish = "Databastyp måste anges för att använda arkiveringsfunktionen."
                sDanish = "Databasetype skal angives for at benytte arkiveringsfunktionen."
            Case 48018
                sNorwegian = "En påloggingsstreng må oppgis for å benytte arkiveringsfunksjonen."
                sEnglish = "A connectionstring must be provided to use the archive function"
                sSwedish = "En inloggningssträng krävs för att använda arkiveringsfunktionen."
                sDanish = "Der skal angives en pålogningsstreng for at benytte arkiveringsfunktionen."
            Case 48019
                sNorwegian = "Påloggingen til databasen rapporterte følgende feil:"
                sEnglish = "The connection reported the following error:"
                sSwedish = "Inloggningen på databasen genererade följande fel:"
                sDanish = "Pålogningen til databasen rapporterede følgende fejl:"
            Case 48020
                sNorwegian = "Vil du avslutte uten lagring ?"
                sEnglish = "Do You want to exit without saving?"
                sSwedish = "Vill du avsluta utan att spara ?"
                sDanish = "Vil du afslutte uden at gemme ?"
            Case 48021
                sNorwegian = "Du må legge inn et kontonummer i tekstboksen til venstre."
                sEnglish = "You must enter an accountnumber in the textbox to the left."
                sSwedish = "Du måste ange ett kontonummer i textrutan till vänster."
                sDanish = "Du skal indtaste et kontonummer i tekstboksen til venstre."
            Case 48022
                sNorwegian = "Kontonummer mangler"
                sEnglish = "Account missing"
                sSwedish = "Kontonummer saknas"
                sDanish = "Kontonummer mangler"
            Case 48023
                sNorwegian = "Kontonummeret er allerede angitt for å utelates fra automatisk postering."
                sEnglish = "The account is already stated to be omitted from automatic matching."
                sSwedish = "Kontonumret är redan inställt med undantag från automatisk bokföring."
                sDanish = "Kontonummeret er allerede angivet til at udelades fra automatisk postering."
            Case 48024
                sNorwegian = "Kontonummer finnes"
                sEnglish = "Account exists"
                sSwedish = "Kontonummer finns"
                sDanish = "Kontonummer findes"
            Case 48025
                sNorwegian = "Kontonummeret er allerede angitt for å utelates fra oppdatering av ERP-system."
                sEnglish = "The account is already stated to be omitted from aftermatching."
                sSwedish = "Kontonumret är redan inställt med undantag från uppdatering av ERP-systemet."
                sDanish = "Kontonummeret er allerede angivet til at udelades fra opdatering af ERP-system."
            Case 48026
                sNorwegian = "Du må velge hvilke(n) modul som kontoen skal utelates fra."
                sEnglish = "You have to choose in which module of BabelBank to omit the account."
                sSwedish = "Du måste välja modul som kontot ska uteslutas från."
                sDanish = "Du skal vælge hvilke(t) modul(er) kontoen skal udelades fra."
            Case 48027
                sNorwegian = "Velg modul"
                sEnglish = "Pick a module"

                '------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 48040

                sSwedish = "Välj modul"
                sDanish = "Vælg modul"
            Case 48028
                sNorwegian = "Databasen, %1, finnes allerede." & vbCrLf & "Kan ikke opprette den nye databasen."
                sEnglish = "The database, %1, already exists." & vbCrLf & "Can't create the new database."
                sSwedish = "Databasen, %1, finns allerede." & vbCrLf & "Kan inte skapa den nya databasen."
                sDanish = "Databasen, %1, eksistere allerede." & vbCrLf & "Kan ikke oprette den nye database."
            Case 48029
                sNorwegian = "Kan ikke opprette databasen!"
                sEnglish = "Can't create the database!"
                sSwedish = "Kan inte skapa databasen!"
                sDanish = "Kan ikke oprette database!"
            Case 48030
                sNorwegian = "Legg inn navn på ny database:"
                sEnglish = "Enter the name of the new database:"
                sSwedish = "Skriv in namnet för den nya databasen:"
                sDanish = "Indtast navnet på den nye database:"
            Case 48031
                sNorwegian = "Databasenavn"
                sEnglish = "Name of database"
                sSwedish = "Databasenamn"
                sDanish = "Databasenavn"
            Case 48032
                sNorwegian = "Vennligst legg inn påloggingsstreng mot SQL Server:"
                sEnglish = "Please enter the connectionstring against SQL Server:"
                sSwedish = "Vänligen ange inloggningssträng mot SQL Server:"
                sDanish = "Venligst indtast pålogningsstreng mod SQL Server:"
            Case 48033
                sNorwegian = "Vennligst legg inn koden du har mottatt fra Visual Banking AS"
                sEnglish = "Please enter the code you have received from Visual Banking AS"
                sSwedish = "Vänligen ange koden du har mottagit från Visual Banking AS"
                sDanish = "Vennligst indtast koden du har modtaget fra Visual Banking AS"
            Case 48034
                sNorwegian = "Ny database ble vellykket opprettet!"
                sEnglish = "Ny database ble vellykket opprettet!"
                sSwedish = "Ny database ble vellykket opprettet!"
                sDanish = "Ny database ble vellykket opprettet!"
            Case 48035
                sNorwegian = "Finner ikke filen %1"
                sEnglish = "Can't find the file %1"
                sSwedish = "Hittar inte filen %1"
                sDanish = "Kan ikke finde filen %1"
            Case 48036
                sNorwegian = "FORRIGE BabelBank kjøring avsluttet med en feilmelding." & Environment.NewLine & "Feilkode: %1"
                sEnglish = "An error was raised LAST TIME BabelBank was run." & Environment.NewLine & "Errorcode: %1"
                sSwedish = "FÖREGÅENDE BabelBank körning slutade med ett felmeddelande." & Environment.NewLine & "Felkod: %1"
                sDanish = "TIDLIGERE BabelBank kørsel sluttede med en fejlmeddelelse." & Environment.NewLine & "Feilkode: %1"
            Case 48037
                sNorwegian = "Dersom du mener alt er OK etter forrige kjøring, trykker du 'Ja' på denne meldingen, og fortsetter som vanlig."
                sEnglish = "If you think everything is OK after the last run, press 'Yes' on this message and continue as usual."
                sSwedish = "Om du tror att allt är OK efter den sista körningen, tryck på 'Ja' på det här meddelandet och fortsätt som vanligt."
                sDanish = "Hvis du tror at alt er OK efter sidste runde, skal du trykke på 'Ja' på denne meddelelse og fortsætte som normalt."
            Case 48038
                sNorwegian = "Dersom du eksporterer OCR-poster i en egen fil:" & Environment.NewLine & "  - En slik fil er sannsynligvis allerede produsert. Sjekk dette før du fortsetter!"
                sEnglish = "If you export OCR records in a specific file:" & Environment.NewLine & "  - Such a file is probably already produced. Check this before continuing!"
                sSwedish = "Om du exporterar OCR-poster i din egen fil:" & Environment.NewLine & "  - En sådan fil har förmodligen redan producerats. Kontrollera detta innan du fortsätter!"
                sDanish = "Hvis du eksporterer OCR-poster i din egen fil:" & Environment.NewLine & "  - En sådan fil er sandsynligvis allerede produceret. Tjek dette før du fortsætter!"
            Case 48039
                sNorwegian = "Er du i tvil, ta kontakt med Visual Banking (support@VisualBanking.net)."
                sEnglish = "If in doubt, contact Visual Banking (support@VisualBanking.net)."
                sSwedish = "Om du är osäker, kontakta Visual Banking (support@VisualBanking.net)."
                sDanish = "Hvis du er i tvivl, kontakt Visual Banking (support@VisualBanking.net)."
            Case 48040
                sNorwegian = "Lagrede betalinger finnes i BabelBanks database"
                sEnglish = "Stored payments exists in BabelBank's database"
                sSwedish = "Sparade betalningar finns i BabelBanks database"
                sDanish = "Gemte betalinger findes i BabelBanks database"

            Case 48041
                sNorwegian = "En uventet feil har oppstått." & Environment.NewLine & "Verdiene i BabelBanks database er ulogiske."
                sEnglish = "An unexpected error has occured." & Environment.NewLine & "The flags set in BabelBank's database are unlogical."
                sSwedish = "Ett oväntat fel har inträffat." & Environment.NewLine & "Värdena i BabelBanks databas är ologiska."
                sDanish = "Der er opstået en uventet fejl." & Environment.NewLine & "Værdierne i BabelBanks database er ulogiske."
            Case 48042
                sNorwegian = "Feilkode: %1."
                sEnglish = "Errorcode %1."
                sSwedish = "Felkod: %1."
                sDanish = "Feilkode: %1."
            Case 48043
                sNorwegian = "En uventet feil har oppstått." & Environment.NewLine & "Kan ikke finne importprofilen for avstemming."
                sEnglish = "An unexpected error has occured." & Environment.NewLine & "Can't find the importprofile for matching."
                sSwedish = "Ett oväntat fel har inträffat." & Environment.NewLine & "Det gick inte att hitta importprofilen för avstämning."
                sDanish = "Der er opstået en uventet fejl." & Environment.NewLine & "Kan ikke finde importprofilen for afstemning."
            Case 48044
                sNorwegian = "Du vil komme til 'Manuel avstemming' og kan eksportere nye filer."
                sEnglish = "You will enter 'Manual Matching' and can export new files."
                sSwedish = "Du kommer inn i 'Manuell avstäming' och kan exportera nya filer."
                sDanish = "Du vil komme til 'Manuel afstemming' og kan eksportere nye filer."



            Case 50000
                sNorwegian = "--------(Menusystem)------"
                sEnglish = "--------(Menusystem)------"
                sSwedish = "--------(Menusystem)------"
                sDanish = "--------(Menusystem)------"
            Case 50100
                sNorwegian = "&Fil"
                sEnglish = "&File"
                sSwedish = "&Fil"
                sDanish = "&Fil"
            Case 50101
                sNorwegian = "&Komprimer database"
                sEnglish = "&Compress database"
                sSwedish = "&Komprimera databas"
                sDanish = "&Komprimer database"

            Case 50102
                sNorwegian = "Velg database"
                sEnglish = "Select database"
                sSwedish = "Välj databas"
                sDanish = "Vælg database"
            Case 50103
                sNorwegian = "Legg tilbake lagret database"
                sEnglish = "Restore saved database"
                sSwedish = "Lägga tillbaka sparad databas"
                sDanish = "Tilføj tilbage lagret database"
            Case 50104
                sNorwegian = "Kopier database"
                sEnglish = "Copy database"
                sSwedish = "Kopiera databas"
                sDanish = "Kopier database"
            Case 50110
                sNorwegian = "&Avslutt"
                sEnglish = "&Exit"
                sSwedish = "&Avsluta"
                sDanish = "&Afslut"
            Case 50111
                sNorwegian = "Skriv ut skjermbildet"
                sEnglish = "Print screen"
                sSwedish = "Skriv ut skärmbild"
                sDanish = "Udskriv skærmbilledet"
            Case 50112
                sNorwegian = "Send ePost"
                sEnglish = "Send eMail"
                sSwedish = "Skicka ePost"
                sDanish = "Send e-mail"
            Case 50113
                sNorwegian = "Lag ny SQLServer DB"
                sEnglish = "Create new SQLServer DB"
                sSwedish = "Skapa ny SQLServer DB"
                sDanish = "Lav ny SQLServer DB"
            Case 50114
                sNorwegian = "Andre"
                sEnglish = "Others"
                sSwedish = "Andra"
                sDanish = "Andre"
            Case 50115
                sNorwegian = "Lukk"
                sEnglish = "Close"
                sSwedish = "Stänga"
                sDanish = "Luk"
            Case 50407
                sNorwegian = "Dekryptér fil"
                sEnglish = "Decrypt file"
                sSwedish = "Dekryptera fil"
                sDanish = "Dekrypter fil"

            Case 50400
                sNorwegian = "&Handling"
                sEnglish = "&Action"
                sSwedish = "&Hantering"
                sDanish = "&Handling"
            Case 50401
                sNorwegian = "&Splitt"
                sEnglish = "&Split"
                sSwedish = "&Delning"
                sDanish = "&Opdel"
            Case 50402
                sNorwegian = "&Konverter"
                sEnglish = "&Convert"
                sSwedish = "&Konvertera"
                sDanish = "&Konverter"
            Case 50403
                sNorwegian = "S&lå sammen"
                sEnglish = "&Merge"
                sSwedish = "S&lå ihop"
                sDanish = "S&lå sammen"
            Case 50404
                sNorwegian = "Endre alle filstier"
                sEnglish = "Change all paths"
                sSwedish = "Ändra alla sökvägar"
                sDanish = "Ændr alle filstier"
            Case 50405
                sNorwegian = "Konverter til Telepay +"
                sEnglish = "Convert to Telepay +"
                sSwedish = "Konvertera till Telepay +"
                sDanish = "Konverter til Telepay +"
            Case 50406
                sNorwegian = "Kjør SQL"
                sEnglish = "Run SQL"
                sSwedish = "Kör SQL"
                sDanish = "Køre SQL"
            Case 50407
                sNorwegian = "Dekryptér fil"
                sEnglish = "Decrypt file"
                sSwedish = "Dekryptera fil"
                sDanish = "Dekrypter fil"
            Case 50408
                sNorwegian = "Pakk ut SecureEnvelopefil"
                sEnglish = "Unpack SecureEnvelope file"
                sSwedish = "Packa upp SecureEnvelope fil"
                sDanish = "Udpakke SecureEnvelope fil"
            Case 50409
                sNorwegian = "Anonymiser ISO20022 filer"
                sEnglish = "Anonymize ISO20022 files"
                sSwedish = "Anonymisera ISO20022 filer"
                sDanish = "Anonymiser ISO20022 filer"
            Case 50410
                sNorwegian = "&Avstem"
                sEnglish = "&Match"
                sSwedish = "&Avstäm"
                sDanish = "&Afstem"
            Case 50411
                sNorwegian = "&Opphev betaling"
                sEnglish = "&Undo payment"
                sSwedish = "&Ångra betalning"
                sDanish = "&Ophæv betaling"
            Case 50413
                sNorwegian = "&Slett linje"
                sEnglish = "&Delete line"
                sSwedish = "&Ta bort rad"
                sDanish = "&Slet linje"
            Case 50414
                sNorwegian = "Alle poster"
                sEnglish = "All payments"
                sSwedish = "Alla poster"
                sDanish = "Alle poster"
            Case 50415
                sNorwegian = "Koordinere uavstemte poster"
                sEnglish = "Coordinate open payments"
                sSwedish = "Koordinera oavstämda poster"
                sDanish = "Koordinere uafstemte poster"
            Case 50416
                sNorwegian = "&Endre avstemmingsbeløp"
                sEnglish = "C&hange matching amount"
                sSwedish = "&Ändra avstämningsbelopp"
                sDanish = "&Ændr afstemningsbeløb"
            Case 50500
                sNorwegian = "&Vis"
                sEnglish = "&View"
                sSwedish = "&Visa"
                sDanish = "&Vis"
            Case 50501
                sNorwegian = "&Vis betalinger på skjerm"
                sEnglish = "&View payments"
                sSwedish = "&Visa betalningar på skärmen"
                sDanish = "&Vis betalinger på skærm"
            Case 50502
                sNorwegian = "Backupfiler"
                sEnglish = "Backupfiles"
                sSwedish = "Säkerhetskopieringsfiler"
                sDanish = "Backupfiler"
            Case 50510
                sNorwegian = "&Første"
                sEnglish = "&First"
                sSwedish = "&Första"
                sDanish = "&Første"
            Case 50511
                sNorwegian = "F&orrige"
                sEnglish = "&Previous"
                sSwedish = "F&öregående"
                sDanish = "F&orrige"
            Case 50512
                sNorwegian = "&Neste"
                sEnglish = "&Next"
                sSwedish = "&Nästa"
                sDanish = "&Næste"
            Case 50513
                sNorwegian = "&Siste"
                sEnglish = "&Last"
                sSwedish = "&Sista"
                sDanish = "&Sidste"
            Case 50514
                sNorwegian = "&Fakturamodus"
                sEnglish = "Invoicelevel"
                sSwedish = "&Fakturaläge"
                sDanish = "&Fakturamodus"
            Case 50515
                sNorwegian = "&Betalingsmodus"
                sEnglish = "Paymentlevel"
                sSwedish = "&Betalningsläge"
                sDanish = "&Betalingsmodus"
            Case 50516
                sNorwegian = "- Alle"
                sEnglish = "- All"
                sSwedish = "- Alla"
                sDanish = "- Alle"
            Case 50517
                sNorwegian = "- Mine"
                sEnglish = "- Mine"
                sSwedish = "- Mina"
                sDanish = "- Mine"
            Case 50518
                sNorwegian = "- Anonyme"
                sEnglish = "- Anonymus"
                sSwedish = "- Anonymt"
                sDanish = "- Anonyme"
            Case 50519
                sNorwegian = "---   Filter   ---"
                sEnglish = "--- Filter ---"
                sSwedish = "---   Filter   ---"
                sDanish = "---   Filter   ---"
            Case 50520
                sNorwegian = "Sist avstemte"
                sEnglish = "Last matched"
                sSwedish = "Senast avstämda"
                sDanish = "Sidst afstemte"
            Case 50521
                sNorwegian = "- Spesialkriterium"
                sEnglish = "- Special criterium"
                sSwedish = "- Specialkriterium"
                sDanish = "- Specialkriterium"
            Case 50522
                sNorwegian = "Klientgruppe"
                sEnglish = "Client Group"
                sSwedish = "Klientgrupp"
                sDanish = "Klientgruppe"
            Case 50523
                sNorwegian = "BabelBank kan maksimalt vise 15 klientgrupper."
                sEnglish = "BabelBank may only show a maximum of 15 clientgroups."
                sSwedish = "BabelBank kan visa högst 15 klientgrupper."
                sDanish = "BabelBank kan vise højst 15 klientgrupper."
            Case 50524
                sNorwegian = "Notat"
                sEnglish = "Note"
                sSwedish = "Anteckning"
                sDanish = "Notat"
            Case 50525
                sNorwegian = "- Forslagsavstemte"
                sEnglish = "- Proposed matched"
                sSwedish = "- Förslagsavstämda"
                sDanish = "- Foreslået afstemte"

            Case 50600
                sNorwegian = "&Utskrift"
                sEnglish = "&Print"
                sSwedish = "&Utskrift"
                sDanish = "&Udskrift"
            Case 50601
                sNorwegian = "Skriv &filinnhold, batchnivå"
                sEnglish = "Print file at &batchlevel"
                sSwedish = "Skriv &filinnehåll, batchnivå"
                sDanish = "Skriv &filindhold, batchniveau"
            Case 50602
                sNorwegian = "Skriv fillinhold på &betalingsnivå"
                sEnglish = "Print file at &paymentlevel"
                sSwedish = "Skriv filinnehåll på &betalningsnivå"
                sDanish = "Skriv filindhold på &betalingsniveau"
            Case 50603
                sNorwegian = "Skriv filinnhold på fakturanivå"
                sEnglish = "Print file at &invoicelevel"
                sSwedish = "Skriv filinnehåll på fakturanivå"
                sDanish = "Skriv filindhold på fakturaniveau"
            Case 50604
                sNorwegian = "Skriv &klientliste"
                sEnglish = "Print &clientlist"
                sSwedish = "Skriv &klientlista"
                sDanish = "Skriv &klientliste"
            Case 50605
                sNorwegian = "Skriv &splittede klienter"
                sEnglish = "Print &splitted clients"
                sSwedish = "Skriv &delade klienter"
                sDanish = "Skriv &opdelte klienter"
            Case 50606
                sNorwegian = "Skriv &avviste poster"
                sEnglish = "Print &rejected payments"
                sSwedish = "Skriv &avvisade poster"
                sDanish = "Skriv &afviste poster"
            Case 50607
                sNorwegian = "Skjermbildet"
                sEnglish = "Screen"
                sSwedish = "Skärmbilden"
                sDanish = "Skærmbillede"
            Case 50608
                sNorwegian = "Sum pr. dag"
                sEnglish = "Totals per day"
                sSwedish = "Sum per dag"
                sDanish = "Sum pr. dag"
            Case 50609
                sNorwegian = "Detaljer pr. dag"
                sEnglish = "Details per day"
                sSwedish = "Detaljer per dag"
                sDanish = "Detaljer pr. dag"
            Case 50610
                sNorwegian = "Alle poster"
                sEnglish = "All items"
                sSwedish = "Alla poster"
                sDanish = "Alle poster"
            Case 50611
                sNorwegian = "Slettede betalinger"
                sEnglish = "Deleted payments"
                sSwedish = "Borttagna betalningar"
                sDanish = "Slettede betalinger"
            Case 50700
                sNorwegian = "&Avstemming"
                sEnglish = "&Matching"
                sSwedish = "&Avstämning"
                sDanish = "&Afstemning"
            Case 50701
                sNorwegian = "Avstemmings&regler"
                sEnglish = "Matching&rules"
                sSwedish = "Avstämnings&regler"
                sDanish = "Afstemnings&regler"
            Case 50702
                sNorwegian = "Avstemmings&parametere"
                sEnglish = "Matching&parameters"
                sSwedish = "Avstämnings&parametrar"
                sDanish = "Afstemnings&parametre"
            Case 50703
                sNorwegian = "Oppsett avstemming"
                sEnglish = "Setup matching"
                sSwedish = "Ställ in avstämning"
                sDanish = "Opsæt afstemning"
            Case 50704
                sNorwegian = "Slett betalinger fra database"
                sEnglish = "Delete payments from the database"
                sSwedish = "Ta bort betalningar från databasen"
                sDanish = "Slet betalinger fra database"
            Case 50705
                sNorwegian = "Manuell avstemming"
                sEnglish = "Manual matching"
                sSwedish = "Manuell avstämning"
                sDanish = "Manuel afstemning"
            Case 50706
                sNorwegian = "Eksporter avstemte poster"
                sEnglish = "Export matched payments"
                sSwedish = "Exportera avstämda poster"
                sDanish = "Eksporter afstemte poster"
            Case 50707
                sNorwegian = "Lås opp poster i Manuell avstemming"
                sEnglish = "Unlock payments in Match Manual"
                sSwedish = "Lås upp poster i manuell avstämning"
                sDanish = "Lås poster op i Manuel afstemning"
            Case 50708
                sNorwegian = "Automatisk avstemming"
                sEnglish = "Automatic matching"
                sSwedish = "Automatisk avstämning"
                sDanish = "Automatisk afstemning"
            Case 50709
                sNorwegian = "Importer filer"
                sEnglish = "Import files"
                sSwedish = "Importera filer"
                sDanish = "Importer filer"
            Case 50710
                sNorwegian = "Skriv ut og slett fjernede poster"
                sEnglish = "Print and delete removed payments"
                sSwedish = "Skriv ut och radera borttagna poster"
                sDanish = "Udskriv og slet fjernede poster"
            Case 50711
                sNorwegian = "Oppsett arkiv"
                sEnglish = "Setup archive"
                sSwedish = "Ställ in arkiv"
                sDanish = "Opsæt arkiv"
            Case 50712
                sNorwegian = "Kundespesifikk meny"
                sEnglish = "Customer specific menu"
                sSwedish = "Kundspecifik meny"
                sDanish = "Kundespecifik menu"
            Case 50713
                sNorwegian = "Statistikk"
                sEnglish = "Statistics"
                sSwedish = "Statistik"
                sDanish = "Statistik"
            Case 50800
                sNorwegian = "&Oppsett"
                sEnglish = "&Setup"
                sSwedish = "&Inställning"
                sDanish = "&Opsæt"
            Case 50801
                sNorwegian = "Klienter"
                sEnglish = "Clients"
                sSwedish = "&Klienter"
                sDanish = "&Klienter"
            Case 50802
                sNorwegian = "&Ny profil"
                sEnglish = "&New profile"
                sSwedish = "&Ny profil"
                sDanish = "&Ny profil"
            Case 50803
                sNorwegian = "&Endre profil"
                sEnglish = "&Change profile"
                sSwedish = "&Ändra profil"
                sDanish = "&Ændr profil"
            Case 50804
                sNorwegian = "&Slette profil"
                sEnglish = "&Delete profile"
                sSwedish = "&Ta bort profil"
                sDanish = "&Slet profil"
            Case 50805
                sNorwegian = "&Firmaopplysninger"
                sEnglish = "C&ompany"
                sSwedish = "&Företagsinformation"
                sDanish = "&Firmaoplysninger"
            Case 50806
                sNorwegian = "Se&kvensnumre"
                sEnglish = "&Sequencenumbers"
                sSwedish = "Se&kvensnummer"
                sDanish = "Se&kvensnumre"
            Case 50807
                sNorwegian = "&OCR Parametre for splitting"
                sEnglish = "&OCR Parametre for splitting"
                sSwedish = "&OCR-parametrar för delning"
                sDanish = "&OCR Parametre for opdeling"
            Case 50808
                sNorwegian = "Vis lisensopplysninger"
                sEnglish = "View licensedata"
                sSwedish = "Visa licensinformation"
                sDanish = "Vis licensoplysninger"
            Case 50809
                sNorwegian = "Aktiver ''Åpne i &BabelBank'"
                sEnglish = "Activate 'Open with BabelBank'"
                sSwedish = "Aktivera ''Öppna i &BabelBank''"
                sDanish = "Aktiver ''Åbn i &BabelBank'"
            Case 50810
                sNorwegian = "Fjern 'Åpne i &BabelBank'"
                sEnglish = "Deactivate 'Open with BabelBank"
                sSwedish = "Ta bort ''Öppna i &BabelBank''"
                sDanish = "Fjern 'Åbn i &BabelBank'"
            Case 50811
                sNorwegian = "Lag snarvei for merket profil"
                sEnglish = "Create shortcut for selected profile"
                sSwedish = "Skapa genväg till markerad profil"
                sDanish = "Opret genvej for mærket profil"
            Case 50812
                sNorwegian = "Oppsett avstemming"
                sEnglish = "Setup matching"
                sSwedish = "Ställ in avstämning"
                sDanish = "Opsæt afstemning"
            Case 50813
                sNorwegian = "Registrer lisenskode"
                sEnglish = "Licensecode"
                sSwedish = "Registrera licenskod"
                sDanish = "Registrer licenskode"
            Case 50814
                sNorwegian = "Dokumentasjon"
                sEnglish = "Documentation"
                sSwedish = "Dokumentation"
                sDanish = "Dokumentation"
            Case 50900
                sNorwegian = "&Hjelp"
                sEnglish = "&Help"
                sSwedish = "&Hjälp"
                sDanish = "&Hjælp"
            Case 50901
                sNorwegian = "&Innhold"
                sEnglish = "&Contents"
                sSwedish = "&Innehåll"
                sDanish = "&Indhold"
            Case 50902
                sNorwegian = "&Søk"
                sEnglish = "&Search"
                sSwedish = "&Sök"
                sDanish = "&Søg"
            Case 50903
                sNorwegian = "Support &mail"
                sEnglish = "Support &mail"
                sSwedish = "Support&post"
                sDanish = "Support &mail"
            Case 50904
                sNorwegian = "Support &telefon"
                sEnglish = "Support &phone"
                sSwedish = "Support&telefon"
                sDanish = "Support &telefon"
            Case 50905
                sNorwegian = "&Om BabelBank"
                sEnglish = "&About BabelBank"
                sSwedish = "&Om BabelBank"
                sDanish = "&Om BabelBank"
            Case 50906
                sNorwegian = "Debug informasjon"
                sEnglish = "Debug information"
                sSwedish = "Felsökningsinformation"
                sDanish = "Debug information"
            Case 50950
                sNorwegian = "Supportverktøy"
                sEnglish = "Supporttool"
                sSwedish = "Supportverktyg "
                sDanish = "Supportværktøjer"
            Case 50951
                sNorwegian = "Filanalyse"
                sEnglish = "File analyzis"
                sSwedish = "Filanalyse"
                sDanish = "Filanalyse"

            Case 51001
                sNorwegian = "&Kryss post"
                sEnglish = "&Select payment"
                sSwedish = "&Markera post"
                sDanish = "&Afstem post"
            Case 51002
                sNorwegian = "&Delbetaling, %1 mot faktura %2"
                sEnglish = "&Part payment, %1 on invoice %2"
                sSwedish = "&Delbetalnng, %1 mot faktura %2"
                sDanish = "&Delbetaling, %1 med faktura %2"
            Case 51003
                sNorwegian = "&Legg til post (RTV)"
                sEnglish = "&Add payment (RTV)"
                sSwedish = "&Lägg till post (RTV)"
                sDanish = "&Tilføj post (RTV)"
            Case 51004
                sNorwegian = "&Akonto, %1 mot kunde %2"
                sEnglish = "&On account, %1 against customer %2"
                sSwedish = "&Akonto, %1 mot kund %2"
                sDanish = "&Aconto, %1 med kunde %2"
            Case 51005
                sNorwegian = "&Benytt restbeløp %1"
                sEnglish = "&Use restamount, %1"
                sSwedish = "&Använd restbelopp %1"
                sDanish = "&Benyt restbeløb %1"
            Case 51006
                sNorwegian = "B&enytt rabattbeløp %1 på faktura %2"
                sEnglish = "Us&e discountamount, %1 on invoice %2"
                sSwedish = "A&nvänd rabattbelopp %1 på faktura %2"
                sDanish = "B&enyt rabatbeløb %1 på faktura %2"
            Case 51007
                sNorwegian = "Postér en inkassosak med renter, faktura %1"
                sEnglish = "Post an external collection with interest, invoice %1"
                sSwedish = "Bokför inkassoärende med ränta, faktura %1"
                sDanish = "Postér en inkassosag med renter, faktura %1"
            Case 51008
                sNorwegian = "Postér en inkassosak med renter og mva, faktura %1"
                sEnglish = "Post an external collection with interest and VAT, invoice %1"
                sSwedish = "Bokför ett inkassoärende med ränta och moms, faktura %1"
                sDanish = "Postér en inkassosag med renter og moms, faktura %1"
            Case 51009
                sNorwegian = "A&konto, %1 mot leverandør %2"
                sEnglish = "O&n account, %1 against vendor %2"
                sSwedish = "A&konto, %1 mot leverantör %2"
                sDanish = "A&conto, %1 mod leverandør %2"
            Case 51101
                sNorwegian = "Vis innkommende FI-betalinger"
                sEnglish = "Show incoming FI-Cards"
                sSwedish = "Visa inkommande FI-betalningar"
                sDanish = "Vis indkommende FI-betalinger"
            Case 51102
                sNorwegian = "Import DnB BEC-files"
                sEnglish = "Import DnB BEC-files"
                sSwedish = "Import DNB BEC-files"
                sDanish = "Import DNB BEC-files"
            Case 51103
                sNorwegian = "Eksporter FIK-betalinger til fil"
                sEnglish = "Export FI-Cards to file"

                ' SEB ISO20022
                sSwedish = "Exportera FIK-betalningar till fil"
                sDanish = "Eksporter FIK-betalinger til fil"
            Case 51201
                sNorwegian = "&Oppsett strukturerte betalinger"
                sEnglish = "&Setup structured payments"
                sSwedish = "Innstälning strukturerade betalningar"
                sDanish = "Opsæt strukturerede betalinger"
            Case 51202
                sNorwegian = "&Diverse oppsett"
                sEnglish = "&Misc. setup"

                sSwedish = "Diverse innstälningar"
                sDanish = "Diverse opsæt"
            Case 51400
                sNorwegian = "&Søk"
                sEnglish = "&Find"
                sSwedish = "&Sök"
                sDanish = "&Søg"
            Case 51800
                sNorwegian = "Oppsett manuell"
                sEnglish = "Manual setup"


                sSwedish = "Ställ in manuell"
                sDanish = "Opsæt manuelt"
            Case 55000
                sNorwegian = "----- (Buttons) ------"
                sEnglish = "----- (Buttons) ------"
                sSwedish = "----- (Buttons) ------"
                sDanish = "----- (Buttons) ------"
            Case 55001
                sNorwegian = "&Ny profil"
                sEnglish = "&New profile"
                sSwedish = "&Ny profil"
                sDanish = "&Ny profil"
            Case 55002
                sNorwegian = "&Endre profil"
                sEnglish = "&Change profile"
                sSwedish = "&Ändra profil"
                sDanish = "&Ændr profil"
            Case 55003
                sNorwegian = "S&lette profil"
                sEnglish = "&Delete profile"
                sSwedish = "T&a bort profil"
                sDanish = "S&let profil"
            Case 55004
                sNorwegian = "Klienter"
                sEnglish = "Clients"
                sSwedish = "&Klienter"
                sDanish = "&Klienter"
            Case 55005
                sNorwegian = "&Start"
                sEnglish = "&Run"
                sSwedish = "&Start"
                sDanish = "&Start"
            Case 55006
                sNorwegian = "&Avbryt"
                sEnglish = "&Cancel"
                sSwedish = "&Avbryt"
                sDanish = "&Afbryd"
            Case 55007
                sNorwegian = "&Overskriv"
                sEnglish = "&Overwrite"
                sSwedish = "&Skriv över"
                sDanish = "&Overskriv"
            Case 55008
                sNorwegian = "&Legg til"
                sEnglish = "&Append"
                sSwedish = "&Lägg till"
                sDanish = "&Tilføj"
            Case 55009
                sNorwegian = "&Neste>"
                sEnglish = "&Next>"
                sSwedish = "&Nästa >"
                sDanish = "&Næste>"
            Case 55010
                sNorwegian = "<&Forrige"
                sEnglish = "<&Previous"
                sSwedish = "< &Föregående"
                sDanish = "<&Forrige"
            Case 55011
                sNorwegian = "F&ørste"
                sEnglish = "&First"
                sSwedish = "F&örsta"
                sDanish = "F&ørste"
            Case 55012
                sNorwegian = "&Siste"
                sEnglish = "&Last"
                sSwedish = "&Sista"
                sDanish = "&Sidste"
            Case 55013
                sNorwegian = "&Avslutt"
                sEnglish = "&End"
                sSwedish = "&Avsluta"
                sDanish = "&Afslut"
            Case 55014
                sNorwegian = "Find &beløp"
                sEnglish = "Find &Amount"
                sSwedish = "Sök &belopp"
                sDanish = "Find &beløb"
            Case 55015
                sNorwegian = "Find &konto"
                sEnglish = "Find A&ccount"
                sSwedish = "Sök &konto"
                sDanish = "Find &konto"
            Case 55016
                sNorwegian = "&Avstem"
                sEnglish = "&Match"
                sSwedish = "&Avstäm"
                sDanish = "&Afstem"
            Case 55017
                sNorwegian = "&Skriv ut"
                sEnglish = "&Print"
                sSwedish = "&Skriv ut"
                sDanish = "&Udskriv"
            Case 55018
                sNorwegian = "&Velg"
                sEnglish = "&Choose"
                sSwedish = "&Välj"
                sDanish = "&Vælg"
            Case 55019
                sNorwegian = "&Send"
                sEnglish = "&Send"
                sSwedish = "&Skicka"
                sDanish = "&Send"
            Case 55020
                sNorwegian = "&Oppdater"
                sEnglish = "&Refresh"
                sSwedish = "&Uppdatera"
                sDanish = "&Opdater"
            Case 55021
                sNorwegian = "Lås opp"
                sEnglish = "Unlock"
                sSwedish = "Lås upp"
                sDanish = "Lås op"
            Case 55022
                sNorwegian = "&Eksporter"
                sEnglish = "&Export"
                sSwedish = "&Exportera"
                sDanish = "&Eksporter"
            Case 55023
                sNorwegian = "&Kriterier"
                sEnglish = "&Criterias"
                sSwedish = "&Kriterier"
                sDanish = "&Kriterier"
            Case 55024
                sNorwegian = "Kreditor&No"
                sEnglish = "&CreditorNo"
                sSwedish = "Kreditor&nr"
                sDanish = "Kreditor&Nr"
            Case 55025
                sNorwegian = "Øvrige profiler"
                sEnglish = "Other profiles"
                sSwedish = "Andra profiler"
                sDanish = "Øvrige profiler"
            Case 55026
                sNorwegian = "&Fjern kriterier"
                sEnglish = "&Remove criteria"
                sSwedish = "&Ta bort kriterier"
                sDanish = "&Fjern kriterier"
            Case 55027
                sNorwegian = "&Oppsett"
                sEnglish = "&Setup"
                sSwedish = "&Inställning"
                sDanish = "&Opsæt"
            Case 55028
                sNorwegian = "Outlook adressebok"
                sEnglish = "Outlook addressbook"
                sSwedish = "Outlook-adressbok"
                sDanish = "Outlook adressebog"
            Case 55029
                sNorwegian = "Filter er på"
                sEnglish = "Filter is on"
                sSwedish = "Filtret är på"
                sDanish = "Filter er på"
            Case 55030
                sNorwegian = "Filter er av"
                sEnglish = "Filter is off"
                sSwedish = "Filtret är av"
                sDanish = "Filter er af"
            Case 55031
                sNorwegian = "Vis alle poster"
                sEnglish = "Show all items"
                sSwedish = "Visa alla poster"
                sDanish = "Vis alle poster"
            Case 55032
                sNorwegian = "Vis åpne poster"
                sEnglish = "Show open items"
                sSwedish = "Visa öppna poster"
                sDanish = "Vis åbne poster"
            Case 55033
                sNorwegian = "&Kjør"
                sEnglish = "&Run"
                sSwedish = "&Kör"
                sDanish = "&Kør"
            Case 55034
                sNorwegian = "&Lagre"
                sEnglish = "&Save"
                sSwedish = "&Spara"
                sDanish = "&Gem"
            Case 55035
                sNorwegian = "Bytte filinnhold"
                sEnglish = "Swap filecontent"
                sSwedish = "Byta filinnehåll"
                sDanish = "Skift filindhold"
            Case 55036
                sNorwegian = "Passord statistikk"
                sEnglish = "Password statistics"
                sSwedish = "Lösenordsstatistik"
                sDanish = "Kodeord statistik"
            Case 55037
                sNorwegian = "Utelat konti"
                sEnglish = "Omit accounts"
                sSwedish = "Uteslut konton"
                sDanish = "Udeladte konti"
            Case 55038
                sNorwegian = "I dette skjermbildet kan du angi betalers kontonummere som ikke skal oppdateres i økonomisystemet, eller som ikke skal forsøkes posteres automatisk. " & vbLf & "" & vbLf & "Legg inn et kontonummer og angi hvilke moduler som ikke skal kjøres."
                sEnglish = "In this screen You may enter payers accountnumbers that should not be updated in the ERP-system, or should not be matched in the automatic matching. " & vbLf & "" & vbLf & "Enter an accountnuber and select in which module(s) it should be overlooked."
                sSwedish = "I det här fönstret kan du ange betalares kontonummer som inte ska uppdateras i ekonomisystemet, eller som inte bör bokföras automatiskt." & vbLf & "" & vbLf & "Ange ett kontonummer och ange vilka moduler som inte ska köras.	"
                sDanish = "I dette skærmbillede kan du angive betalers kontonumre som ikke skal opdateres i økonomisystemet, eller som ikke skal forsøges posteret automatisk." & vbLf & "" & vbLf & "Indtast et kontonummer og angiv hvilke moduler, der ikke skal køres.	"
            Case 55039
                sNorwegian = "Etterbehandling"
                sEnglish = "After matching"
                sSwedish = "Efterbehandling"
                sDanish = "Efterbehandling"
            Case 55040
                sNorwegian = "Kontonummer:"
                sEnglish = "Account number:"
                sSwedish = "Kontonummer:"
                sDanish = "Kontonummer:"
            Case 55041
                sNorwegian = "&Bruk"
                sEnglish = "&Use"
                sSwedish = "&Använd"
                sDanish = "&Brug"
            Case 55042
                sNorwegian = "&Legg til"
                sEnglish = "&Add"
                sSwedish = "&Lägg till"
                sDanish = "&Tilføj"
            Case 55043
                sNorwegian = "&Slett"
                sEnglish = "&Delete"
                sSwedish = "&Ta bort"
                sDanish = "&Slet"
            Case 55044
                sNorwegian = "&Vis"
                sEnglish = "&Browse"
                sSwedish = "&Bläddra"
                sDanish = "&Browse"
            Case 55045
                sNorwegian = "ePost"
                sEnglish = "eMail"
                sSwedish = "ePost"
                sDanish = "eMail"
            Case 55046
                sNorwegian = "&Analyser"
                sEnglish = "&Analyze"
                sSwedish = "&Analysera"
                sDanish = "&Analysere"
            Case 55047
                sNorwegian = "&Ny"
                sEnglish = "&New"
                sSwedish = "&Ny"
                sDanish = "&Ny"
            Case 55048
                sNorwegian = "Ledetekst"
                sEnglish = "Labels"
                sSwedish = "Hjälptext "
                sDanish = "Ledetekst"
            Case 55049
                sNorwegian = "Høyreklikk"
                sEnglish = "Rightclick"
                sSwedish = "Höger klick"
                sDanish = "Højreklik"

                '-------------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 55050
            Case 55050
                sNorwegian = "&Lagre detaljer"
                sEnglish = "&Save callstack"
                sSwedish = "&Spara uppgifter"
                sDanish = "&Gem detaljer"
            Case 55051
                sNorwegian = "I dette vinduet kan du endre ledetekster på" & vbCrLf & "enkelte parametere i BabelBank." & vbCrLf & _
                "Det vil påvirke skjermbildet for manuell avstemming, " & vbCrLf & "rapporter og enkelte oppsett vinduer."
                sEnglish = "In this window you may change the labels for some" & vbCrLf & "parameters in BabelBank." & vbCrLf & _
                "This will affect the screen for manual matching, " & vbCrLf & "reports and some windows for setup."
                sSwedish = "I detta fönster kan du ändra ledetexter på" & vbCrLf & "vissa parametrar i BabelBank." & vbCrLf & _
                            "Det kommer att påverka skärmbilden för manuell avstämning, " & vbCrLf & "rapporter och vissa inställningsfönster."
                sDanish = "I dette vindue kan du ændre ledetekster på" & vbCrLf & "visse parametre i BabelBank." & vbCrLf & _
                            "Det vil påvirke skærmbilledet for manuel afstemning, " & vbCrLf & "rapporter og nogle setup vinduer."
            Case 55052
                sNorwegian = "Nullstill"
                sEnglish = "Reset"
                sSwedish = "Återställ"
                sDanish = "Nulstill"

            Case 60000
                sNorwegian = "-----(Display on screen ...)-----"
                sEnglish = "-----(Display on screen ...)-----"
                sSwedish = "-----(Display on screen ...)-----"
                sDanish = "-----(Display on screen ...)-----"
            Case 60001
                sNorwegian = "Profiler for innsendingsfiler"
                sEnglish = "Profiles sendfiles"
                sSwedish = "Profiler för insändningsfiler"
                sDanish = "Profiler for indsendelsesfiler"
            Case 60002
                sNorwegian = "Profiler for returfiler"
                sEnglish = "Profiles returnfiles"
                sSwedish = "Profiler för returfiler"
                sDanish = "Profiler for returfiler"
            Case 60003
                sNorwegian = "Importerer filer"
                sEnglish = "Importing files"
                sSwedish = "Importera filer"
                sDanish = "Importerer filer"
            Case 60004
                sNorwegian = "Import OK"
                sEnglish = "Import OK"
                sSwedish = "Import OK"
                sDanish = "Import OK"
            Case 60005
                sNorwegian = "Fyller opp visningstabell"
                sEnglish = "Filling spread"
                sSwedish = "Fyller i visningstabell"
                sDanish = "Fylder visningstabel op"
            Case 60006
                sNorwegian = "Fullført"
                sEnglish = "Completed"
                sSwedish = "Slutfört"
                sDanish = "Fuldført"
            Case 60007
                sNorwegian = "Velg et filformat"
                sEnglish = "Choose a file format"
                sSwedish = "Välj ett filformat"
                sDanish = "Vælg et filformat"
            Case 60008
                sNorwegian = "Filer TIL bank"
                sEnglish = "Files TO bank"
                sSwedish = "Filer TILL bank"
                sDanish = "Filer TIL bank"
            Case 60009
                sNorwegian = "Filer FRA bank"
                sEnglish = "Files FROM bank"
                sSwedish = "Filer FRÅN bank"
                sDanish = "Filer FRA bank"
            Case 60010
                sNorwegian = "Backupkatalog finnes ikke"
                sEnglish = "Folder does not exist"
                sSwedish = "Säkerhetskopieringskatalog finns inte"
                sDanish = "Backupkatalog findes ikke"
            Case 60011
                sNorwegian = "BabelBank avbrytes."
                sEnglish = "BabelBank terminates!"
                sSwedish = "BabelBank avbryts."
                sDanish = "BabelBank afbrydes."
            Case 60012
                sNorwegian = "Kan ikke opprette katalog"
                sEnglish = "Can't create folder"
                sSwedish = "Kan inte skapa katalog"
                sDanish = "Kan ikke oprette katalog"
            Case 60013
                sNorwegian = "Back-up katalog ikke oppgitt." & vbLf & "Legg inn backup-katalog under Oppsett -> Firmaopplysninger." & vbLf & "" & vbLf & "Program avbrytes!"
                sEnglish = "Backup-folder is not stated." & vbLf & "Enter the backup-folder under Setup -> Company." & vbLf & "" & vbLf & "BabelBank terminates!"
                sSwedish = "Katalog för säkerhetskopiering har inte angetts." & vbLf & "Ange katalog för säkerhetskopiering under Inställning -> Företagsinformation." & vbLf & "" & vbLf & "Programmet avbryts!	"
                sDanish = "Back-up katalog ikke angivet." & vbLf & "Tilføj backup-katalog under Opsætning -> Firmaoplysninger." & vbLf & "" & vbLf & "Program afbrydes!	"
            Case 60016
                sNorwegian = "Filen finnes fra før"
                sEnglish = "The file already exists"
                sSwedish = "Filen finns redan"
                sDanish = "Filen findes fra før"
            Case 60017
                sNorwegian = "Velg katalog eller fil"
                sEnglish = "Choose directory or file"
                sSwedish = "Välj katalog eller fil"
                sDanish = "Vælg mappe eller fil"
            Case 60018
                sNorwegian = "Ny klient funnet"
                sEnglish = "New Client found"
                sSwedish = "Ny klient hittades"
                sDanish = "Ny klient fundet"
            Case 60019
                sNorwegian = "Under import fra"
                sEnglish = "During import with"
                sSwedish = "Under import från"
                sDanish = "Under import fra"
            Case 60020
                sNorwegian = "ble det funnet en ny klient."
                sEnglish = "a new client was found."
                sSwedish = "hittades en ny klient."
                sDanish = "blev der fundet en ny klient."
            Case 60021
                sNorwegian = "Klientnummer:"
                sEnglish = "Clientnumber:"
                sSwedish = "Klientnummer:"
                sDanish = "Klientnummer:"
            Case 60022
                sNorwegian = "Kontonummer:"
                sEnglish = "Accountnumber:"
                sSwedish = "Kontonummer:"
                sDanish = "Kontonummer:"
            Case 60023
                sNorwegian = "Filnavn import:"
                sEnglish = "Imported filename:"
                sSwedish = "Filnamn import:"
                sDanish = "Filnavn import:"
            Case 60024
                sNorwegian = "Eksportformat:"
                sEnglish = "Exportformat:"
                sSwedish = "Exportformat:"
                sDanish = "Eksportformat:"
            Case 60025
                sNorwegian = "Filnavn eksport:"
                sEnglish = "Exportfilename:"
                sSwedish = "Filnamn export:"
                sDanish = "Filnavn eksport:"
            Case 60026
                sNorwegian = "Klientnavn:"
                sEnglish = "Clientname:"
                sSwedish = "Klientnamn:"
                sDanish = "Klientnavn:"
            Case 60027
                sNorwegian = "Importerer fil(er)"
                sEnglish = "Importing file(s)"
                sSwedish = "Importerar fil(er)"
                sDanish = "Importerer fil(er)"
            Case 60028
                sNorwegian = "Forbereder rapport"
                sEnglish = "Preparing reports"
                sSwedish = "Förbereder rapport"
                sDanish = "Forbereder rapport"
            Case 60029
                sNorwegian = "Forbereder eksport"
                sEnglish = "Preparing export"
                sSwedish = "Förbereder export"
                sDanish = "Forbereder eksport"
            Case 60030
                sNorwegian = "Eksporterer fil(er)"
                sEnglish = "Exporting file(s)"
                sSwedish = "Exporterar fil(er)"
                sDanish = "Eksporterer fil(er)"
            Case 60031
                sNorwegian = "Kontrollerer fil(er)"
                sEnglish = "Controlling file(s)"
                sSwedish = "Kontrollerar fil(er)"
                sDanish = "Kontrollerer fil(er)"
            Case 60032
                sNorwegian = "Kjører backup"
                sEnglish = "Running backup"
                sSwedish = "Kör säkerhetskopiering"
                sDanish = "Kører backup"
            Case 60033
                sNorwegian = "Lager ønskede rapporter"
                sEnglish = "Creating selected reports"
                sSwedish = "Skapar önskade rapporter"
                sDanish = "Gem ønskede rapporter"
            Case 60034
                sNorwegian = "Eksportinformasjon"
                sEnglish = "Exportinformation"
                sSwedish = "Exportinformation"
                sDanish = "Eksportinformation"
            Case 60035
                sNorwegian = "Navn på eksportfil"
                sEnglish = "Exportfilename"
                sSwedish = "Namn på exportfil"
                sDanish = "Navn på eksportfil"
            Case 60036
                sNorwegian = "Eksportformat"
                sEnglish = "Exportformat"
                sSwedish = "Exportformat"
                sDanish = "Eksportformat"
            Case 60037
                sNorwegian = "Vennligst skriv inn eksportfilnavn!"
                sEnglish = "Please fill in exportfilename!"
                sSwedish = "Ange exportfilnamnet!"
                sDanish = "Indtast eksportfilnavn!"
            Case 60038
                sNorwegian = "Support"
                sEnglish = "Support"
                sSwedish = "Stöd"
                sDanish = "Support"
            Case 60039
                sNorwegian = "Supporttelefon"
                sEnglish = "Supportphone"
                sSwedish = "Telefonsupport"
                sDanish = "Supporttelefon"
            Case 60040
                sNorwegian = "Supportmail:"
                sEnglish = "Supportmail:"
                sSwedish = "Supportpost:"
                sDanish = "Supportmail:"
            Case 60041
                sNorwegian = "Om"
                sEnglish = "About"
                sSwedish = "Om"
                sDanish = "Om"
            Case 60042
                sNorwegian = "Vennligst velg profil"
                sEnglish = "Please select a profile"
                sSwedish = "Välj profil"
                sDanish = "Vælg profil"
            Case 60043
                sNorwegian = "Vil du virkelig slette den valgte profil"
                sEnglish = "Do You want to delete selected profile"
                sSwedish = "Vill du vill ta bort den valda profilen"
                sDanish = "Vil du virkelig slette den valgte profil"
            Case 60044
                sNorwegian = "Slette profil"
                sEnglish = "Delete profile"
                sSwedish = "Ta bort profil"
                sDanish = "Slet profil"
            Case 60045
                sNorwegian = "Versjon"
                sEnglish = "Version"
                sSwedish = "Version"
                sDanish = "Version"
            Case 60046
                sNorwegian = "sendeprofil"
                sEnglish = "sendprofile"
                sSwedish = "sändningsprofil"
                sDanish = "sendeprofil"
            Case 60047
                sNorwegian = "returprofil"
                sEnglish = "returnprofile"
                sSwedish = "returprofil"
                sDanish = "returprofil"
            Case 60048
                sNorwegian = "Klientinformasjon"
                sEnglish = "Client information"
                sSwedish = "Klientinformation"
                sDanish = "Klientinformation"
            Case 60049
                sNorwegian = "Filinformasjon"
                sEnglish = "File information"
                sSwedish = "Filinformation"
                sDanish = "Filinformation"
            Case 60050
                sNorwegian = "Ønsker du å avbryte BabelBank?"
                sEnglish = "Cancel BabelBank?"
                sSwedish = "Vill du avbryta BabelBank?"
                sDanish = "Vil du afbryde BabelBank?"
            Case 60051
                sNorwegian = "De påbegynte oppgaver vil ikke fullføres."
                sEnglish = "The tasks started, will not be completed."
                sSwedish = "De påbörjade uppgifterna kommer inte att slutföras."
                sDanish = "De påbegyndte opgaver vil ikke fuldføres."
            Case 60052
                sNorwegian = "Klientnummer må fylles ut!"
                sEnglish = "Please fill in the clientnumber."
                sSwedish = "Klientnummer måste anges!"
                sDanish = "Klientnummer skal udfyldes!"
            Case 60053
                sNorwegian = "Feil klientnummer"
                sEnglish = "Incorrect clientnumber"
                sSwedish = "Fel i klientnumret"
                sDanish = "Forkert klientnummer"
            Case 60054
                sNorwegian = "Du må opprette en eller flere profiler i Babelbank."
                sEnglish = "You have to create a profile in Babelbank"
                sSwedish = "Du måste skapa en eller flera profiler i Babelbank."
                sDanish = "Du skal oprette en eller flere profiler i Babelbank."
            Case 60055
                sNorwegian = "Ingen profil opprettet"
                sEnglish = "No profiles in the database"
                sSwedish = "Ingen profil har skapats"
                sDanish = "Ingen profil oprettet"
            Case 60056
                sNorwegian = "BabelBank kjøring vellykket! Avslutter."
                sEnglish = "BabelBank completed successfully!"
                sSwedish = "Felfri BabelBank-körning! Avslutas."
                sDanish = "BabelBank kørsel vellykket! Afslutter."
            Case 60057
                sNorwegian = "NB! BabelBank kjøring mislykket"
                sEnglish = "NB! BabelBank failed to complete!"
                sSwedish = "OBS! BabelBank-körningen misslyckades"
                sDanish = "NB! BabelBank kørsel mislykkedes"
            Case 60058
                sNorwegian = "Ukjent filtype"
                sEnglish = "Unknown filetype"
                sSwedish = "Okänd filtyp"
                sDanish = "Ukendt filtype"
            Case 60059
                sNorwegian = "Funnet ny konto for eksisterende klient: %1"
                sEnglish = "Found new account for existing client: %1"
                sSwedish = "Hittade nytt konto hos befintlig klient"
                sDanish = "Fundet ny konto for eksisterende klient"
            Case 60060
                sNorwegian = "ble det funnet en ny konto."
                sEnglish = "a new account was found."
                sSwedish = "hittades ett nytt konto."
                sDanish = "blev der fundet en ny konto."
            Case 60061
                sNorwegian = "Klientnummer %1 brukes i en annen profil!"
                sEnglish = "Clientnumber %1 is in use by another profile!"
                sSwedish = "Klientnummer %1 används i en annan profil!"
                sDanish = "Klientnummer %1 bruges i en anden profil!"
            Case 60062
                sNorwegian = "Avslutt, gå inn i' Klienter', og knytt klient %1 til profilen %2!" & vbLf & "" & vbLf & "Importfil: %3"
                sEnglish = "Cancel operation, click on 'Clients', and connect client %1 to profile %2!" & vbLf & "" & vbLf & "Importfile: %3"
                sSwedish = "Avsluta, gå till 'Klienter' och koppla klient  %1 till profilen %2!" & vbLf & "" & vbLf & "Importfil: %3	"
                sDanish = "Afslut, gå ind i' Klienter', og knyt klient %1 til profilen %2!" & vbLf & "" & vbLf & "Importfil: %3	"
            Case 60063
                sNorwegian = "Nye data legges til i eksisterende fil."
                sEnglish = "New data is added to the existing file."
                sSwedish = "Nya data läggs till i den befintliga filen."
                sDanish = "Nye data føjes til eksisterende fil."
            Case 60064
                sNorwegian = "Eksisterende fil overskrives."
                sEnglish = "Existing file will be overwritten."
                sSwedish = "Befintlig fil skrivs över."
                sDanish = "Eksisterende fil overskrives."
            Case 60065
                sNorwegian = "BabelBank-kjøring avbrytes. Eksisternde fil beholdes."
                sEnglish = "BabelBank terminates. Exsiting file will not be changed."
                sSwedish = "BabelBank-körning avbryts. Befintlig fil behålls."
                sDanish = "BabelBank-kørsel afbrydes. Eksisterende fil beholdes."
            Case 60066
                sNorwegian = "Framdrift automatisk avstemming:"
                sEnglish = "Progress automatic matching:"
                sSwedish = "Förlopp i automatisk avstämning:"
                sDanish = "Fremskridt automatisk afstemning:"
            Case 60067
                sNorwegian = "Avstemmingsprosent:"
                sEnglish = "Percentage matched:"
                sSwedish = "Avstämningsprocent:"
                sDanish = "Afstemningsprocent:"
            Case 60068
                sNorwegian = "Framdrift totalt:"
                sEnglish = "Total progress:"
                sSwedish = "Förlopp totalt:"
                sDanish = "Fremskridt totalt:"
            Case 60069
                sNorwegian = "Backup ikke aktivert. Aktiveres i 'Oppsett/Firmaopplysninger'"
                sEnglish = "Backup not activated. Activate in  'Setup/Company'"
                sSwedish = "Säkerhetskopiering är inte aktiverat. Aktiveras i 'Vyer/Företagsinformation'"
                sDanish = "Backup ikke aktiveret. Aktiveres i 'Opsætning/Firmaoplysninger'"
            Case 60070
                sNorwegian = "Vil du sende supportmail til Visual Banking AS, vedlagt de filer som behandles?"
                sEnglish = "Send supportmail to Visual Banking, with problemfiles attached ?"
                sSwedish = "Vill du skicka supportpost till Visual Bank AS, med bifogning av de filer som behandlas?"
                sDanish = "Vil du sende supportmail til Visual Banking AS, vedlagt de filer som behandles?"
            Case 60071
                sNorwegian = "Finner ingen relevante filer i  backupkatalogen"
                sEnglish = "No relevant files found in backuppath"
                sSwedish = "Hittar inte relevanta filer i säkerhetskopieringskatalogen"
                sDanish = "Finder ingen relevante filer i  backupkataloget"
            Case 60072
                sNorwegian = "Manuell Avstemming"
                sEnglish = "Manual matching"
                sSwedish = "Manuell avstämning"
                sDanish = "Manuel Afstemning"
            Case 60073
                sNorwegian = "MELDING OM KREDITERING"
                sEnglish = "STATEMENT INCOMING PAYMENT"
                sSwedish = "MEDDELANDE TILL KREDITERING"
                sDanish = "MEDDELELSE OM KREDITERING"
            Case 60074
                sNorwegian = "BELØPET GJELDER:"
                sEnglish = "PAYMENT CONCERNS:"
                sSwedish = "BELOPPET GÄLLER:"
                sDanish = "BELØBET GÆLDER:"
            Case 60075
                sNorwegian = "MOTTAKERS NAVN, ADRESSE, POSTNR, STED"
                sEnglish = "RECEIVERS NAME, ADDRESS, ZIPCODE, CITY"
                sSwedish = "MOTTAGARENS NAMN, ADRESS, POSTNR, ORT"
                sDanish = "MODTAGERS NAVN, ADRESSE, POSTNR, BY"
            Case 60076
                sNorwegian = "BETALERS NAVN, ADRESSE, POSTNR, STED"
                sEnglish = "PAYERS NAME, ADDRESS, ZIPCODE, CITY"
                sSwedish = "BETALARENS NAMN, ADRESS, POSTNR, ORT"
                sDanish = "BETALERS NAVN, ADRESSE, POSTNR, BY"
            Case 60077
                sNorwegian = "DATO:"
                sEnglish = "DATE:"
                sSwedish = "DATUM:"
                sDanish = "DATO:"
            Case 60078
                sNorwegian = "TIL KONTO:"
                sEnglish = "TO ACCOUNT:"
                sSwedish = "TILL KONTO:"
                sDanish = "TIL KONTO:"
            Case 60079
                sNorwegian = "BELØP:"
                sEnglish = "AMOUNT:"
                sSwedish = "BELOPP:"
                sDanish = "BELØB:"
            Case 60080
                sNorwegian = "REFERANSE 1:"
                sEnglish = "REFERENCE 1:"
                sSwedish = "REFERENS 1:"
                sDanish = "REFERENCE 1:"
            Case 60081
                sNorwegian = "REFERANSE 2:"
                sEnglish = "REFERENCE 2:"
                sSwedish = "REFERENS 2:"
                sDanish = "REFERENCE 2:"
            Case 60082
                sNorwegian = "REFERANSE 3:"
                sEnglish = "REFERENCE 3:"
                sSwedish = "REFERENS 3:"
                sDanish = "REFERENCE 3:"
            Case 60083
                sNorwegian = "VALUTA:"
                sEnglish = "CURRENCY:"
                sSwedish = "VALUTA:"
                sDanish = "VALUTA:"
            Case 60084
                sNorwegian = "Kundenr."
                sEnglish = "Customerno."
                sSwedish = "Kundnr."
                sDanish = "Kundenr."
            Case 60085
                sNorwegian = "Fakturanr."
                sEnglish = "Invoiceno."
                sSwedish = "Fakturanr."
                sDanish = "Fakturanr."
            Case 60086
                sNorwegian = "Beløp"
                sEnglish = "Amount"
                sSwedish = "Belopp"
                sDanish = "Beløb"
            Case 60087
                sNorwegian = "Valuta"
                sEnglish = "Currency"
                sSwedish = "Valuta"
                sDanish = "Valuta"
            Case 60088
                sNorwegian = "Resultat av søk"
                sEnglish = "Searchresult"
                sSwedish = "Sökresultat"
                sDanish = "Resultat af søgning"
            Case 60089
                sNorwegian = "Velg"
                sEnglish = "Select"
                sSwedish = "Välj"
                sDanish = "Vælg"
            Case 60090
                sNorwegian = "FRA KONTO:"
                sEnglish = "FROM ACCOUNT:"
                sSwedish = "FRÅN KONTO:"
                sDanish = "FRA KONTO:"
            Case 60091
                sNorwegian = "Avstemmingsforslag"
                sEnglish = "Preliminary matching"
                sSwedish = "Avstämningsförslag"
                sDanish = "Afstemningsforslag"
            Case 60092
                sNorwegian = "Valgt beløp"
                sEnglish = "Selected amount"
                sSwedish = "Valt belopp"
                sDanish = "Valgt beløb"
            Case 60093
                sNorwegian = "Restbeløp"
                sEnglish = "Restamount"
                sSwedish = "Restbelopp"
                sDanish = "Restbeløb"
            Case 60094
                sNorwegian = "Posten er avstemt"
                sEnglish = "Payment is matched"
                sSwedish = "Posten är avstämd"
                sDanish = "Posten er afstemt"
            Case 60095
                sNorwegian = "Fakturanivå"
                sEnglish = "Invoicemode"
                sSwedish = "Fakturanivå"
                sDanish = "Fakturaniveau"
            Case 60096
                sNorwegian = "Se GiroMail / Nettbank"
                sEnglish = "See GiroMail / Nettbank"
                sSwedish = "Se GiroMail/Internetbank"
                sDanish = "Se GiroMail / Netbank"
            Case 60097
                sNorwegian = "H.Bok"
                sEnglish = "GL Account"
                sSwedish = "H.bok"
                sDanish = "H.Bog"
            Case 60098
                sNorwegian = "Bilagsnr."
                sEnglish = "Voucherno."
                sSwedish = "Verifikationsnr."
                sDanish = "Bilagsnr."
            Case 60099
                sNorwegian = "Fritekst"
                sEnglish = "Freetext"
                sSwedish = "Fritext"
                sDanish = "Fritekst"
            Case 60100
                sNorwegian = "Debet"
                sEnglish = "Debit"
                sSwedish = "Debet"
                sDanish = "Debet"
            Case 60101
                sNorwegian = "Kredit"
                sEnglish = "Credit"
                sSwedish = "Kredit"
                sDanish = "Kredit"
            Case 60102
                sNorwegian = "Dette er siste faktura"
                sEnglish = "This is the last invoice"
                sSwedish = "Detta är den sista fakturan"
                sDanish = "Dette er sidste faktura"
            Case 60103
                sNorwegian = "Dette er første faktura"
                sEnglish = "This is the first invoice"
                sSwedish = "Detta är den första fakturan"
                sDanish = "Dette er første faktura"
            Case 60104
                sNorwegian = "Kan ikke koble til ekstern database. Årsak:"
                sEnglish = "Unable to connect to external database. Cause:"
                sSwedish = "Kan inte ansluta till extern databas. Orsak:"
                sDanish = "Kan ikke koble til ekstern database. Årsag:"
            Case 60105
                sNorwegian = "Konto/Kundenummer for søket er ikke angitt"
                sEnglish = "Accountnumber for search not given"
                sSwedish = "Konto-/Kundnummer för sökningen anges inte"
                sDanish = "Konto/Kundenummer for søgning er ikke angivet"
            Case 60106
                sNorwegian = "Begrep for søket er ikke angitt"
                sEnglish = "Searchcriteria is not stated"
                sSwedish = "Begrepp för sökningen anges inte"
                sDanish = "Begreb for søgning er ikke angivet"
            Case 60107
                sNorwegian = "Du må stå i 'Payment-modus' for å avstemme"
                sEnglish = "You must be in Paymentmode to match"
                sSwedish = "Du måste vara i 'betalningsläge' för att kunna avstämma"
                sDanish = "Du skal stå i 'Payment-modus' for at afstemme"
            Case 60108
                sNorwegian = "Del av bankkontosum med beløp"
                sEnglish = "Part of bankstatement with amount"
                sSwedish = "Del av bankkontosumma med belopp"
                sDanish = "Del af bankkontosum med beløb"
            Case 60109
                sNorwegian = "Del av"
                sEnglish = "Part of"
                sSwedish = "Del av"
                sDanish = "Del af"
            Case 60110
                sNorwegian = "Kan ikke slette denne linjen"
                sEnglish = "Unable to delete this line"
                sSwedish = "Kan inte ta bort den här raden"
                sDanish = "Kan ikke slette denne linje"
            Case 60111
                sNorwegian = "Frisøk"
                sEnglish = "Freeseach"
                sSwedish = "Frisökning"
                sDanish = "Frisøg"
            Case 60112
                sNorwegian = "Kundesøk"
                sEnglish = "Customersearch"
                sSwedish = "Kundsökning"
                sDanish = "Kundesøgning"
            Case 60113
                sNorwegian = "Fakturasøk"
                sEnglish = "Invoicesearch"
                sSwedish = "Fakturasökning"
                sDanish = "Fakturasøgning"
            Case 60114
                sNorwegian = "Konto for øreavvik"
                sEnglish = "Smalldiff. account"
                sSwedish = "Konto för öresutjämning"
                sDanish = "Konto for øreafvigelse"
            Case 60115
                sNorwegian = "Navn på 'fakturasøk'"
                sEnglish = "Name 'Invoicesearch'"
                sSwedish = "Namn ''fakturasökning''"
                sDanish = "Navn på 'fakturasøgning'"
            Case 60116
                sNorwegian = "Navn på 'kundesøk'"
                sEnglish = "Name 'Customersearch'"
                sSwedish = "Namn på ''kundsökning''"
                sDanish = "Navn på 'kundesøgning'"
            Case 60117
                sNorwegian = "Navn på frisøk1"
                sEnglish = "Name freesearch1"
                sSwedish = "Namn på frisökning1"
                sDanish = "Navn på frisøgning 1"
            Case 60118
                sNorwegian = "Navn på frisøk2"
                sEnglish = "Name freesearch2"
                sSwedish = "Namn på frisökning2"
                sDanish = "Navn på frisøgning2"
            Case 60119
                sNorwegian = "Navn på frisøk3"
                sEnglish = "Name freesearch3"
                sSwedish = "Namn på frisökning3"
                sDanish = "Navn på frisøgning3"
            Case 60120
                sNorwegian = "Hopp automatisk til neste post når differanse er null"
                sEnglish = "Jump to next payment when difference equals 0"
                sSwedish = "Gå till nästa post automatiskt när skillnaden är noll"
                sDanish = "Spring automatisk til næste post når differencen er nul"
            Case 60121
                sNorwegian = "Still kontrollspørsmål når det er differanse ved avstemming."
                sEnglish = "Ask a question if difference when matching"
                sSwedish = "Ställ kontrollfrågor vid varians i avstämningen."
                sDanish = "Stil kontrolspørgsmål når der er difference ved afstemning."
            Case 60122
                sNorwegian = "Oppsett for manuell avstemming"
                sEnglish = "Setup Manual Matching"
                sSwedish = "Inställning för manuell avstämning"
                sDanish = "Opsætning for manuel afstemning"
            Case 60123
                sNorwegian = "Velg kolonner for avstemte poster"
                sEnglish = "Choose columns for matched payments"
                sSwedish = "Välj kolumner för avstämda poster"
                sDanish = "Vælg kolonner for afstemte poster"
            Case 60124
                sNorwegian = "Behandling av avstemmingsdifferanser"
                sEnglish = "Posting of restamount"
                sSwedish = "Behandling av avstämningsvarianser"
                sDanish = "Behandling af afstemningsdifferencer"
            Case 60125
                sNorwegian = "Hopp til registreringsbilde"
                sEnglish = "Jump to input form"
                sSwedish = "Hoppa till registreringsbild"
                sDanish = "Spring til registreringsbilledet"
            Case 60126
                sNorwegian = "Føre restbeløp til øreavvikskonto"
                sEnglish = "Post difference to discrepancyaccount"
                sSwedish = "Bokför restbelopp på öresutjämningskonto"
                sDanish = "Føre restbeløb til øreafvigelseskonto"
            Case 60127
                sNorwegian = "Føre restbeløp akonto"
                sEnglish = "Post difference on account"
                sSwedish = "Bokför restbelopp på akonto"
                sDanish = "Føre restbeløb aconto"
            Case 60128
                sNorwegian = "Føre restbeløp til feilkonto/feilkunde"
                sEnglish = "Post difference to observation account"
                sSwedish = "Bokför restbelopp på felkonto/fel kund"
                sDanish = "Føre restbeløb til fejlkonto/fejlkunde"
            Case 60129
                sNorwegian = "Restbeløp til avstemming"
                sEnglish = "Difference to post"
                sSwedish = "Restbelopp för avstämning"
                sDanish = "Restbeløb til afstemning"
            Case 60130
                sNorwegian = "Velg kundekonto"
                sEnglish = "Choose customer"
                sSwedish = "Välj kundkonto"
                sDanish = "Vælg kundekonto"
            Case 60131
                sNorwegian = "Oppgi feilkonto"
                sEnglish = "Enter observationaccount"
                sSwedish = "Ange felkonto"
                sDanish = "Angiv fejlkonto"
            Case 60132
                sNorwegian = "Hvilken faktura skal endres?"
                sEnglish = "Which invoice is to be edited?"
                sSwedish = "Vilken faktura ska ändras?"
                sDanish = "Hvilken faktura skal ændres?"
            Case 60133
                sNorwegian = "Øredifferanse"
                sEnglish = "Discrepancy"
                sSwedish = "Öresutjämning"
                sDanish = "Øredifference"
            Case 60134
                sNorwegian = "Første betaling"
                sEnglish = "First payment"
                sSwedish = "Första betalningen"
                sDanish = "Første betaling"
            Case 60135
                sNorwegian = "Forrige betaling"
                sEnglish = "Previous payment"
                sSwedish = "Föregående betalning"
                sDanish = "Forrige betaling"
            Case 60136
                sNorwegian = "Neste betaling"
                sEnglish = "Next payment"
                sSwedish = "Nästa betalning"
                sDanish = "Næste betaling"
            Case 60137
                sNorwegian = "Siste betaling"
                sEnglish = "Last payment"
                sSwedish = "Sista betalning"
                sDanish = "Sidste betaling"
            Case 60138
                sNorwegian = "Avstem"
                sEnglish = "Match"
                sSwedish = "Avstäm"
                sDanish = "Afstem"
            Case 60139
                sNorwegian = "Opphev avstemming"
                sEnglish = "Undo matching"
                sSwedish = "Ångra avstämning"
                sDanish = "Ophæv afstemning"
            Case 60140
                sNorwegian = "Bytt mellom betalings- og fakturamodus"
                sEnglish = "Change between payment- and invoicelevel"
                sSwedish = "Växla mellan betalnings och fakturaläge"
                sDanish = "Skift mellem betalings- og fakturamodus"
            Case 60141
                sNorwegian = "Slett linje i forslagsavstemming"
                sEnglish = "Delete line in the matching proposal"
                sSwedish = "Ta bort rad i avstämningsförslag"
                sDanish = "Slet linje i foreslået afstemning"
            Case 60142
                sNorwegian = "Søk etter beløp"
                sEnglish = "Search for amount"
                sSwedish = "Sök efter belopp"
                sDanish = "Søg efter beløb"
            Case 60143
                sNorwegian = "aKonto"
                sEnglish = "On account"
                sSwedish = "aKonto"
                sDanish = "aconto"
            Case 60144
                sNorwegian = "Til feilkonto"
                sEnglish = "To observation account"
                sSwedish = "Till felkonto"
                sDanish = "Til fejlkonto"
            Case 60145
                sNorwegian = "Ønsker du å fortsette med rapporter og eksport av data?"
                sEnglish = "Do You want to continue to create reports and to export data?"
                sSwedish = "Vill du fortsätta med rapporter och export av data?"
                sDanish = "Ønsker du at fortsætte med rapporter og eksport af data?"
            Case 60146
                sNorwegian = "Svarer du Nei, lagres dataene for senere bruk."
                sEnglish = "Choose No, if You want to store the data, and continue later."
                sSwedish = "Om du svarar Nej, lagras data för senare bruk."
                sDanish = "Svarer du Nej, gemmes dataene til senere brug."
            Case 60147
                sNorwegian = "Avslutt manuell avstemming"
                sEnglish = "End manual matching"
                sSwedish = "Avsluta manuell avstämning"
                sDanish = "Afslut manuel afstemning"
            Case 60148
                sNorwegian = "Alle betalinger ble automatisk avstemt." & vbLf & "Ingen betalinger å avstemme manuelt"
                sEnglish = "All payments was automatically matched." & vbLf & "No payments to match manually"
                sSwedish = "Alla betalningar avstämdes automatiskt." & vbLf & "Inga betalningar finns att avstämma manuellt	"
                sDanish = "Alle betalinger blev afstemt automatisk." & vbLf & "Ingen betalinger at afstemme manuelt	"
            Case 60149
                sNorwegian = "Klient"
                sEnglish = "Client"
                sSwedish = "Klient"
                sDanish = "Klient"
            Case 60150
                sNorwegian = "Ingen poster funnet!"
                sEnglish = "No items found!"
                sSwedish = "Inga poster hittades!"
                sDanish = "Ingen poster fundet!"
            Case 60151
                sNorwegian = "Kan ikke avstemme innbetalingen!"
                sEnglish = "Can't match the payment!"
                sSwedish = "Kan inte avstämma inbetalningen!"
                sDanish = "Kan ikke afstemme indbetalingen!"
            Case 60152
                sNorwegian = "Kundenummeret som er angitt stemmer ikke overens med avstemmingsmønter for Akonto-posteringer." & vbLf & "Oppgitt kundenummer: %1" & vbLf & "Avstemmingsmønster: %2"
                sEnglish = "The customernumber doesn't match the pattern for direct posting." & vbLf & "Stated customernumber: %1" & vbLf & "Pattern for direct posting: %2"
                sSwedish = "Kundnummer som angetts matchar inte avstämningsmönster för på akonto-bokföring." & vbLf & "Angivet kundnummer: %1" & vbLf & "Avstämningsmönster: %2	"
                sDanish = "Kundenummeret som er angivet stemmer ikke overens med afstemningsmønster for aconto-posteringer." & vbLf & "Angivet kundenummer: %1" & vbLf & "Afstemningsmønster: %2	"
            Case 60153
                sNorwegian = "Føre restbeløp til gebyrkonto"
                sEnglish = "Post difference to account for charges"
                sSwedish = "Bokför restbelopp på avgiftskonto"
                sDanish = "Før restbeløb til gebyrkonto"
            Case 60154
                sNorwegian = "Gebyr"
                sEnglish = "Charges"
                sSwedish = "Avgift"
                sDanish = "Gebyr"
            Case 60155
                sNorwegian = "Denne fakturaen er betalt mer enn en gang!"
                sEnglish = "This invoice has been paid more than once!"
                sSwedish = "Denna faktura är betald mer än en gång!"
                sDanish = "Denne faktura er betalt mere end en gang!"
            Case 60156
                sNorwegian = "En %1fil har blitt produsert. Filnavn:"
                sEnglish = "An %1file has been produced. Filename:"
                sSwedish = "En %1fil har genererats. Filnamn:"
                sDanish = "Der er blevet produceret en %1fil. Filnavn:"
            Case 60157
                sNorwegian = "Framdriftskode: %1 -"
                sEnglish = "Progresscode: %1 -"
                sSwedish = "Förloppskod: %1 -"
                sDanish = "Fremskridtskode: %1 -"
            Case 60158
                sNorwegian = "Levnr."
                sEnglish = "Supplierno."
                sSwedish = "Levnr."
                sDanish = "Levnr."
            Case 60159
                sNorwegian = "Alle"
                sEnglish = "All"
                sSwedish = "Alla"
                sDanish = "Alle"
            Case 60160
                sNorwegian = "Betalers navn"
                sEnglish = "Payers name"
                sSwedish = "Betalarens namn"
                sDanish = "Betalers navn"
            Case 60161
                sNorwegian = "Fra konto"
                sEnglish = "From account"
                sSwedish = "Från konto"
                sDanish = "Fra konto"
            Case 60162
                sNorwegian = "Dato"
                sEnglish = "Date"
                sSwedish = "Datum"
                sDanish = "Dato"
            Case 60163
                sNorwegian = "Til konto"
                sEnglish = "To account"
                sSwedish = "Till konto"
                sDanish = "Til konto"
            Case 60164
                sNorwegian = "Ikke fullstendig avstemte"
                sEnglish = "Not completely match"
                sSwedish = "Inte helt avstämt"
                sDanish = "Ikke fuldstændig afstemte"
            Case 60165
                sNorwegian = "Avstemte"
                sEnglish = "Matched"
                sSwedish = "Avstämt"
                sDanish = "Afstemte"
            Case 60166
                sNorwegian = "Forslagsavstemte"
                sEnglish = "Proposed matched"
                sSwedish = "Avstämningsförslag"
                sDanish = "Foreslået afstemning"
            Case 60167
                sNorwegian = "Delvis avstemte"
                sEnglish = "Partly matched"
                sSwedish = "Delvis avstämt"
                sDanish = "Delvis afstemte"
            Case 60168
                sNorwegian = "Ikke avstemte"
                sEnglish = "Not matched"
                sSwedish = "Inte avstämt"
                sDanish = "Ikke afstemte"
            Case 60169
                sNorwegian = "Betalingen er krysset ut"
                sEnglish = "The payment is matched"
                sSwedish = "Betalningen är överstruken"
                sDanish = "Betalingen er afstemt"
            Case 60170
                sNorwegian = "En feil oppstod under innleggelse av avstemmingsinformasjon."
                sEnglish = "An error occured during population of the matched information."
                sSwedish = "Ett fel inträffade under infogning av avstämningsinformation."
                sDanish = "Der opstod en fejl under tilføjelse af afstemningsinformation."
            Case 60171
                sNorwegian = "Kan ikke oppheve innbetalingen."
                sEnglish = "Can't undo the matching."
                sSwedish = "Kan inte ångra inbetalningen."
                sDanish = "Kan ikke ophæve indbetalingen."
            Case 60172
                sNorwegian = "Forfallsdato"
                sEnglish = "Duedate"
                sSwedish = "Förfallodatum"
                sDanish = "Forfaldsdato"
            Case 60173
                sNorwegian = "Ingen skrivere installert"
                sEnglish = "No printers installed!"
                sSwedish = "Ingen skrivare finns installerad"
                sDanish = "Ingen printer installeret"
            Case 60174
                sNorwegian = "Send ePost fra Manuell Avstemming"
                sEnglish = "Send mail from Manual Matching"
                sSwedish = "Skicka ePost från manuell avstämning"
                sDanish = "Send e-mail fra Manuel Afstemning"
            Case 60175
                sNorwegian = "ePostens emne"
                sEnglish = "Mail subject"
                sSwedish = "ePostens ämne"
                sDanish = "e-mailens emne"
            Case 60176
                sNorwegian = "Skjermbilde som vedlegg"
                sEnglish = "Copy of screen as attachment"
                sSwedish = "Skärmbild som bilaga"
                sDanish = "Skærmbillede som bilag"
            Case 60177
                sNorwegian = "Send til (ePost adresse)"
                sEnglish = "Send to (eMail address)"
                sSwedish = "Skicka till (e-postadress)"
                sDanish = "Send til (e-mailadresse)"
            Case 60178
                sNorwegian = "Søk på navn"
                sEnglish = "Search for name"
                sSwedish = "Sök efter namn"
                sDanish = "Søg på navn"
            Case 60179
                sNorwegian = "Søk på beløp"
                sEnglish = "Search for amount"
                sSwedish = "Sök efter belopp"
                sDanish = "Søg på beløb"
            Case 60180
                sNorwegian = "Søk"
                sEnglish = "Search"
                sSwedish = "Sök"
                sDanish = "Søg"
            Case 60181
                sNorwegian = "Eksporter kun posterte innbetalinger"
                sEnglish = "Export matched payments only"
                sSwedish = "Exportera endast bokförda betalningar"
                sDanish = "Eksporter kun posterede indbetalinger"
            Case 60182
                sNorwegian = "Sist brukt ERP-ID:"
                sEnglish = "Last used ERP-ID"
                sSwedish = "Senast använt ERP-ID:"
                sDanish = "Sidst brugte ERP-ID:"
            Case 60183
                sNorwegian = "Denne betaling er under behandling av annen bruker"
                sEnglish = "This record is in use by another user"
                sSwedish = "Denna betalning behandlas av en annan användare"
                sDanish = "Denne betaling er under behandling af anden bruger"
            Case 60184
                sNorwegian = "Lås opp stengte poster"
                sEnglish = "Force unlock"
                sSwedish = "Lås upp stängda poster"
                sDanish = "Lås låste poster op"
            Case 60185
                sNorwegian = "Denne funksjonen låser opp alle elementer i tabellen LockedPayments."
                sEnglish = "WARNING: This function unlocks all items in the databasetable LockedPayments."
                sSwedish = "Den här funktionen låser upp alla element i tabellen LockedPayments."
                sDanish = "Denne funktion låser alle elementer i tabellen LockedPayments op."
            Case 60186
                sNorwegian = "Det er meget viktig at du er helt sikker på at ingen brukere benytter BabelBank."
                sEnglish = "It is important that you know that all users have logged off BabelBank, before proceeding."
                sSwedish = "Det är mycket viktigt att du är helt säker på att inga användare använder BabelBank."
                sDanish = "Det er meget vigtigt, at du er helt sikker på at ingen brugere benytter BabelBank."
            Case 60187
                sNorwegian = "Følgende brukere ligger med oppføring i LockedPayments-tabellen:"
                sEnglish = "The following users have items locked in Manual Matching:"
                sSwedish = "Följande användare infogar poster i LockedPayments-tabellen:"
                sDanish = "Følgende brugere ligger med opfølgning i LockedPayments-tabellen:"
            Case 60188
                sNorwegian = "Ingen betalinger er låst."
                sEnglish = "No payments locked"
                sSwedish = "Inga betalningar är låsta."
                sDanish = "Ingen betalinger er låst."
            Case 60189
                sNorwegian = "Bokføringsdato"
                sEnglish = "Book date"
                sSwedish = "Bokföringsdatum"
                sDanish = "Bogføringsdato"
            Case 60190
                sNorwegian = "Innbet.dato"
                sEnglish = "Payment date"
                sSwedish = "Inbet.datum"
                sDanish = "Indbet.dato"
            Case 60191
                sNorwegian = "DebitorIdent"
                sEnglish = "Debitor Ident"
                sSwedish = "DebitorIdent"
                sDanish = "DebitorIdent"
            Case 60192
                sNorwegian = "Eksportert"
                sEnglish = "Exported"
                sSwedish = "Exporterat"
                sDanish = "Eksporteret"
            Case 60193
                sNorwegian = "Val.kurs"
                sEnglish = "Exch.rate"
                sSwedish = "Val.kurs"
                sDanish = "Val.kurs"
            Case 60194
                sNorwegian = "Bokføringsdato (DDMMÅÅ)"
                sEnglish = "Book.date (DDMMYY)"
                sSwedish = "Bokföringsdatum (DDMMÅÅ)"
                sDanish = "Bogføringsdato (DDMMÅÅ)"
            Case 60195
                sNorwegian = "Innbet.dato (DDMMÅÅ)"
                sEnglish = "Paid date (DDMMYY)"
                sSwedish = "Inbet.datum (DDMMÅÅ)"
                sDanish = "Indbet.dato (DDMMÅÅ)"
            Case 60196
                sNorwegian = "Beløp fra /til"
                sEnglish = "Amount from/to"
                sSwedish = "Belopp från/till"
                sDanish = "Beløb fra /til"
            Case 60197
                sNorwegian = "Kreditornummer"
                sEnglish = "CreditorNumber"
                sSwedish = "Kreditornummer"
                sDanish = "Kreditornummer"
            Case 60198
                sNorwegian = "Kun ikke-eksporterte"
                sEnglish = "Non-exported only"
                sSwedish = "Endast ej exporterade"
                sDanish = "Kun ikke-eksporterede"
            Case 60199
                sNorwegian = "Kriterier for visning av FI-kort"
                sEnglish = "Criterias for FI-browser"
                sSwedish = "Kriterier för visning av FI-kort"
                sDanish = "Kriterier for visning af FI-kort"
            Case 60200
                sNorwegian = "Sist oppdatert"
                sEnglish = "Last updated"
                sSwedish = "Senast uppdaterat"
                sDanish = "Sidst opdateret"
            Case 60201
                sNorwegian = "Navn"
                sEnglish = "Name"
                sSwedish = "Namn"
                sDanish = "Navn"
            Case 60202
                sNorwegian = "Referanse"
                sEnglish = "Reference"
                sSwedish = "Referens"
                sDanish = "Reference"
            Case 60203
                sNorwegian = "Fjern innbetaling"
                sEnglish = "Omit payment"
                sSwedish = "Ta bort inbetalning"
                sDanish = "Fjern indbetaling"
            Case 60204
                sNorwegian = "Aktivér valg for å fjerne betalinger"
                sEnglish = "Activate possibility to omit payments"
                sSwedish = "Aktivera alternativet för att ta bort betalningar"
                sDanish = "Aktivér valg for at fjerne betalinger"
            Case 60205
                sNorwegian = "Bruker ID"
                sEnglish = "User ID"
                sSwedish = "Användar-ID"
                sDanish = "Bruger ID"
            Case 60206
                sNorwegian = "Velg kolonner som skal kunne registreres"
                sEnglish = "Columns to enter data into"
                sSwedish = "Välj kolumner som ska kunna registreras"
                sDanish = "Vælg kolonner som skal kunne registreres"
            Case 60207
                sNorwegian = "Du kan ikke legge til denne kolonnen for registrering, fordi den ikke er markert for visning!"
                sEnglish = "You can't add this column for editing, because it is not marked as visible!"
                sSwedish = "Du kan inte lägga till den här kolumnen för registrering eftersom den inte är markerad för visning!"
                sDanish = "Du kan ikke tilføje denne kolonne for registrering, fordi den ikke er markeret for visning!"
            Case 60208
                sNorwegian = "ULOGISK VALG"
                sEnglish = "UNLOGICAL CHOICE"
                sSwedish = "OLOGISKT VAL"
                sDanish = "ULOGISK VALG"
            Case 60209
                sNorwegian = "Innbetalingen kan ikke utelukkes fordi den er en del av en samlesum på kontoutdraget, med beløp %1."
                sEnglish = "The payment can't be omitted, because it is a part of a lumpsum on the bank statement, on amount %1."
                sSwedish = "Inbetalningen kan inte uteslutas eftersom den ingår i en samlad summa på kontoutdraget."
                sDanish = "Indbetalingen kan ikke udelukkes fordi den er en del af en samlesum på kontoudskriften."
            Case 60210
                sNorwegian = "ULOVLIG VALG"
                sEnglish = "ILLEGAL CHOICE"
                sSwedish = "OTILLÅTET VAL"
                sDanish = "ULOVLIGT VALG"
            Case 60211
                sNorwegian = "Forf.dato"
                sEnglish = "DueDate"
                sSwedish = "Förf.datum"
                sDanish = "Forf.dato"
            Case 60212
                sNorwegian = "Manuelt avstemt av: %1"
                sEnglish = "Manually matched by: %1"
                sSwedish = "Manuellt avstämt av: %1"
                sDanish = "Manuelt afstemt af: %1"
            Case 60213
                sNorwegian = "Ønsker du virkelig å oppheve denne posteringen?" & vbLf & "" & vbLf & "For å fjerne posteringsinformasjon, trykk på opphev postering en gang til."
                sEnglish = "Do You really want to cancel this matching?" & vbLf & "" & vbLf & "To remove matching information, please press undo matching once more."
                sSwedish = "Vill du ångra bokföringen?" & vbLf & "" & vbLf & "För att ta bort bokföringsinformationen klickar du på ångra bokföring en gång till.	"
                sDanish = "Vil du virkelig ophæve denne postering?" & vbLf & "" & vbLf & "For at fjerne posteringsinformation, skal du trykke på ophæv postering en gang til.	"
            Case 60214
                sNorwegian = "OPPHEVE AVSTEMMING"
                sEnglish = "UNDO MATCHING"
                sSwedish = "ÅNGRA AVSTÄMNING"
                sDanish = "OPHÆVE AFSTEMNING"
            Case 60215
                sNorwegian = "OPPRINNELIG BELØP:"
                sEnglish = "ORIGINAL AMOUNT:"
                sSwedish = "URSPRUNGLIGT BELOPP:"
                sDanish = "OPRINDELIGT BELØB:"
            Case 60216
                sNorwegian = "Opphevet av: %1"
                sEnglish = "Nullified by: %1"
                sSwedish = "Ångrat av: %1"
                sDanish = "Ophævet af: %1"
            Case 60217
                sNorwegian = "Kjører automatisk utkryssing"
                sEnglish = "Running automatic matching"
                sSwedish = "Kör automatisk matching"
                sDanish = "Kører automatisk afstemning"
            Case 60218
                sNorwegian = "Størrelsen på BabelBank sin database overstiger maksimumstørrelsen som er angitt" & vbLf & "For å komprimere databasen:" & vbLf & "1) Påse at alle andre brukere er avlogget BabelBank." & vbLf & "2) Velg 'Fil' -> 'Komprimer database' fra menyen i BabelBank" & vbLf & "3) Bekreft oppstart av prosessen ved å svare 'Ja''" & vbLf & "" & vbLf & "Et alternativ er å øke maksimum størrelsen på databasen." & vbLf & "For å øke maksimum størrelsen:" & vbLf & "1) Velg 'Oppsett' -> 'Firmaopplysninger' fra menyen i BabelBank" & vbLf & "2) I feltet '%1' angis ønsket størrelse" & vbLf & "3) Ved å klikke på 'OK'-knappen lagres endringen"
                sEnglish = "BabelBank's database exceeds the maximum size that are stated under setup" & vbLf & "To compress the database:" & vbLf & "1) Make sure that all other users are logged out of BabelBank." & vbLf & "2) Choose 'File' -> 'Compress database' from the menu in BabelBank" & vbLf & "3) Confirm to start the process by pressing 'Yes'" & vbLf & "" & vbLf & "An alternative is to increase the maximum size of the database" & vbLf & "To increase the maximum size" & vbLf & "1) Choose 'Setup' -> 'Company' from the menu in BabelBank" & vbLf & "2) In the field '%1' set the desired size" & vbLf & "3) Save by clicking the 'OK'-button"
                sSwedish = "Storleken på BabelBanks databas överskrider den maximala storlek som har angetts" & vbLf & "För att komprimera databasen:" & vbLf & "1) Se till att alla andra användare har loggat ut ur BabelBank." & vbLf & "2) Välj 'Fil'-> 'Komprimera databas' på menyn i BabelBank" & vbLf & "3) Bekräfta starten av processen genom att svara 'Ja'" & vbLf & "" & vbLf & "Ett alternativ är att öka maxstorleken på databasen." & vbLf & "För att öka maxstorleken:" & vbLf & "1) Väl 'Inställningar' -> 'Företagsinformation' på menyn i i BabelBank" & vbLf & "2) I fältet '%1' anges önskad storlek" & vbLf & "3) Spara ändringen genom att klicka på 'OK'	"
                sDanish = "Størrelsen på BabelBanks database overstiger den maksimumstørrelse, der er angivet" & vbLf & "For at komprimere databasen:" & vbLf & "1) Sørg for at alle brugere er logget af BabelBank." & vbLf & "2) Vælg 'Fil' -> 'Komprimer database' fra menuen i BabelBank" & vbLf & "3) Bekræft opstart af processen ved at svare 'Ja''" & vbLf & "" & vbLf & "Et alternativ er at øge maksimumstørrelsen på databasen." & vbLf & "For at øge maksimumstørrelsen:" & vbLf & "1) Vælg 'Opsætning' -> 'Firmaoplysninger' fra menuen i BabelBank" & vbLf & "2) I feltet '%1' angives ønsket størrelse" & vbLf & "3) Ved at klikke på 'OK'-knappen gemmes ændringen	"
            Case 60219
                sNorwegian = "MAKSIMUM STØRRELSE PÅ DATABASE OVERSKREDET"
                sEnglish = "MAXIMUM SIZE FOR THE DATABASE EXCEEDED"
                sSwedish = "MAXIMAL STORLEK PÅ DATABAS ÖVERSKRIDEN"
                sDanish = "MAKSIMUMSTØRRELSE PÅ DATABASE OVERSKREDET"
            Case 60220
                sNorwegian = "BabelBank er i bruk av andre." & vbLf & "" & vbLf & "Alle andre brukere må logge seg av BabelBank før databasen kan komprimeres."
                sEnglish = "There are still other users using BabelBank" & vbLf & "" & vbLf & "All users must be logged out of BabelBank, before the database may be compressed."
                sSwedish = "BabelBank används av andra." & vbLf & "" & vbLf & "Alla andra användare måste logga ut ur BabelBank innan databasen kan komprimeras.	"
                sDanish = "BabelBank bruges af andre." & vbLf & "" & vbLf & "Alle andre brugere skal logge af BabelBank før databasen kan komprimeres.	"
            Case 60221
                sNorwegian = "KAN IKKE FORTSETTE"
                sEnglish = "NOT ABLE TO CONTINUE"
                sSwedish = "KAN INTE FORTSÄTTA"
                sDanish = "KAN IKKE FORTSÆTTE"
            Case 60222
                sNorwegian = "Feil under sikkerhetskopiering av BabelBank sin database:" & vbLf & "%1"
                sEnglish = "Error during backup of BabelBank's database:" & vbLf & "%1"
                sSwedish = "Fel vid säkerhetskopiering av BabelBanks databas:" & vbLf & "%1	"
                sDanish = "Fejl under sikkerhedskopiering af BabelBanks database:" & vbLf & "%1	"
            Case 60223
                sNorwegian = "60223: BabelBank kan ikke hente ønskede data fra database," & vbLf & "fordi en annen bruker komprimerer databasen."
                sEnglish = "60223: BabelBank can't retrieve data from the database," & vbLf & "because an other user is compressing BabelBank's database."
                sSwedish = "60223: BabelBank kan inte hämta data från databasen" & vbLf & "eftersom en annan användare håller på att komprimera databasen.	"
                sDanish = "60223: BabelBank kan ikke hente ønskede data fra database," & vbLf & "fordi en anden bruger komprimerer databasen.	"
            Case 60224
                sNorwegian = "Påse at alle andre brukere er avlogget BabelBank." & vbLf & "" & vbLf & "Ønsker du å fortsette?"
                sEnglish = "Make sure all users are logged out of BabelBank." & vbLf & "" & vbLf & "Do You want to continue?"
                sSwedish = "Se till att alla andra användare är utloggade ur BabelBank." & vbLf & "" & vbLf & "Vill du fortsätta?	"
                sDanish = "Sørg for at alle andre brugere er logget af BabelBank." & vbLf & "" & vbLf & "Ønsker du at fortsætte?	"
            Case 60225
                sNorwegian = "Vellykket komprimering!" & vbLf & "" & vbLf & "Gammel størrelse: %1" & vbLf & "Ny størrelse: %2"
                sEnglish = "Compress suceeded!" & vbLf & "" & vbLf & "Old size: %1" & vbLf & "New size: %2"
                sSwedish = "Felfri komprimering!" & vbLf & "" & vbLf & "Tidigare storlek: %1" & vbLf & "Ny storlek: %2	"
                sDanish = "Vellykket komprimering!" & vbLf & "" & vbLf & "Gammel størrelse: %1" & vbLf & "Ny størrelse: %2	"
            Case 60226
                sNorwegian = "Ingen betalinger uten unik ID (KID) ble importert." & vbLf & "Det blir ikke laget noen fil for denne type transaksjoner."
                sEnglish = "No payments without unique-ID (KID) was imported." & vbLf & "No file with theese sort of payments will be created."
                sSwedish = "Inga betalningar utan unikt ID (OCR) importerades." & vbLf & "Det skapades ingen fil för denna typ av transaktioner.	"
                sDanish = "Ingen betalinger uden unik ID (KID) blev importeret." & vbLf & "Der bliver ikke oprettet nogen fil for denne type transaktioner.	"
            Case 60227
                sNorwegian = "DIFFERANSE"
                sEnglish = "DIFFERENCE"
                sSwedish = "DIFFERENS"
                sDanish = "DIFFERENCE"
            Case 60228
                sNorwegian = "Totalsum på avstemte poster avviker fra innbetalt beløp."
                sEnglish = "The total amount posted differs from the amount on the payment."
                sSwedish = "Total mängd avstämda poster avviker från inbetalt belopp."
                sDanish = "Totalsum på afstemte poster afviger fra indbetalt beløb."
            Case 60229
                sNorwegian = "Hvis du ikke avstemmer innbetalingen senere, vil hele innbetalingen posteres som uavstemt ved eksport."
                sEnglish = "The payment will be posted as unmatched, if You don't complete the posting of this payment later."
                sSwedish = "Om du inte avstämmer inbetalningen senare bokförs hela betalningen som oavstämd vid exporten."
                sDanish = "Hvis du ikke afstemmer indbetalingen senere, vil hele indbetalingen blive posteret som uafstemt ved eksport."
            Case 60230
                sNorwegian = "Ønsker du allikevel å lagre innbetalingen?"
                sEnglish = "Do You still want to save?"
                sSwedish = "Vill du ändå vill spara inbetalningen?"
                sDanish = "Vil du alligevel gemme indbetalingen?"
            Case 60231
                sNorwegian = "Angi påloggingsstreng for pålogging til arkivdatabase." & vbLf & "I tillegg må databasetype angis."
                sEnglish = "Enter the connectionstring for the archive database." & vbLf & "In addition the type of database must be entered."
                sSwedish = "Ange inloggningssträng för inloggning till arkivdatabasen." & vbLf & "Även databstyp måste anges.	"
                sDanish = "Angiv pålogningsstreng for pålogning til arkivdatabase." & vbLf & "Derudover skal databasetype angives.	"
            Case 60232
                sNorwegian = "Datbasetype:"
                sEnglish = "Type of database:"
                sSwedish = "Datbastyp:"
                sDanish = "Databasetype:"
            Case 60233
                sNorwegian = "Innkludér OCR-innbetalinger"
                sEnglish = "Include OCR payments"
                sSwedish = "Inkludera vid OCR-inbetalningar"
                sDanish = "Inkludér OCR-indbetalinger"
            Case 60234
                sNorwegian = "&Bruk"
                sEnglish = "&Use"
                sSwedish = "&Använd"
                sDanish = "&Brug"
            Case 60235
                sNorwegian = "Aktivér arkivfunksjon"
                sEnglish = "Activate function for archiving"
                sSwedish = "Aktivera arkivfunktion"
                sDanish = "Aktivér arkivfunktion"
            Case 60236
                sNorwegian = "Ta kopi av database"
                sEnglish = "Make a copy of the database"
                sSwedish = "Ta kopia av databasen"
                sDanish = "Tag kopi af database"
            Case 60237
                sNorwegian = "Område for backup"
                sEnglish = "Path for backup"
                sSwedish = "Område för säkerhetskopiering"
                sDanish = "Område for backup"
            Case 60238
                sNorwegian = "Antall generasjoner av backup"
                sEnglish = "Number of backup generations"
                sSwedish = "Antall generationer av säkerhetskopian"
                sDanish = "Antal generationer af backup"
            Case 60239
                sNorwegian = "Bilagsnr.:"
                sEnglish = "VoucherNo:"
                sSwedish = "Verifikationsnr:"
                sDanish = "Bilagsnr.:"
            Case 60240
                sNorwegian = "Før import"
                sEnglish = "Before import"
                sSwedish = "Före import"
                sDanish = "Før import"
            Case 60241
                sNorwegian = "Før eksport"
                sEnglish = "Before export"
                sSwedish = "Före export"
                sDanish = "Før eksport"
            Case 60242
                sNorwegian = "Etter eksport"
                sEnglish = "After export"
                sSwedish = "Efter export"
                sDanish = "Efter eksport"
            Case 60243
                sNorwegian = "Navn"
                sEnglish = "Name"
                sSwedish = "Namn"
                sDanish = "Navn"
            Case 60244
                sNorwegian = "Det ble ikke funne noen beskrivelse av KID for konto %1." & vbLf & "" & vbLf & "For å beskrive KID må du gå inn på 'Klientoppsettet' i BabelBank." & vbLf & "Velg deretter kontoen ved å dobbeltklikke, og velg deretter 'KID-oppsett'."
                sEnglish = "No description of KID found for account %1." & vbLf & "" & vbLf & "To describe the KID, please enter the 'Client setup' in BabelBank." & vbLf & "Then choose the account by doubleclicking on the account, and select 'KID setup'."
                sSwedish = "Det gick inte att hitta någon beskrivning av OCR för konto %1." & vbLf & "" & vbLf & "För att beskriva OCR måste du gå in i klientinställningarna i BabelBank." & vbLf & "Välj därefter konton genom att dubbeklicka, och därefter OCR-inställningar.	"
                sDanish = "Der blev ikke fundet nogen beskrivelse af KID for konto %1." & vbLf & "" & vbLf & "For at beskrive KID skal du gå ind på 'Klientopsætning' i BabelBank." & vbLf & "Vælg derefter kontoen ved at dobbeltklikke, og vælg derefter 'KID-opsætning'.	"
            Case 60245
                sNorwegian = "Verdien av iBabelBankCancel etter funksjon"
                sEnglish = "The value of iBabelBankCancel after function"
                sSwedish = "Värdet av iBabelBankCancel efter funktion"
                sDanish = "Værdi af iBabelBankCancel efter funktion"
            Case 60246
                sNorwegian = "Velg format"
                sEnglish = "Select format"
                sSwedish = "Välj format"
                sDanish = "Vælg format"
            Case 60247
                sNorwegian = "Babelbank kan ikke fortsette, fordi det ikke er valgt noe format."
                sEnglish = "Babelbank can't continue, because no format is selected"
                sSwedish = "BabelBank kan inte fortsätta eftersom inget format har valts."
                sDanish = "Babelbank kan ikke fortsætte, fordi der ikke er valgt noget format."
            Case 60248
                sNorwegian = "Ikke valgt format"
                sEnglish = "No format selected"
                sSwedish = "Inget valt format"
                sDanish = "Ikke valgt format"
            Case 60249
                sNorwegian = "Velg format på filen som skal importeres"
                sEnglish = "Select format on the file to import"
                sSwedish = "Välj format för den fil som ska importeras"
                sDanish = "Vælg format på filen som skal importeres"
            Case 60250
                sNorwegian = "Beskrivelse av fakturanummer:"
                sEnglish = "Description of invoicenumber:"
                sSwedish = "Beskrivning av fakturanummer:"
                sDanish = "Beskrivelse af fakturanummer:"
            Case 60251
                sNorwegian = "Observasjonskonto (reskontronr.):"
                sEnglish = "Observationaccount (AR):"
                sSwedish = "Observationskonto (reskontranr):"
                sDanish = "Observationskonto (samlekontonr.):"
            Case 60252
                sNorwegian = "Utfomat (normalt 1):"
                sEnglish = "Format out (normally 1):"
                sSwedish = "Utfomat (normalt 1):"
                sDanish = "Udformat (normalt 1):"
            Case 60253
                sNorwegian = "Innfomat (normalt 2):"
                sEnglish = "Format in (normally 2):"
                sSwedish = "Informat (normalt två):"
                sDanish = "Indformat (normalt 2):"
            Case 60254
                sNorwegian = "Legg inn faste parametere på nye klienter som skal importeres." & vbLf & "Hvis du har kun en profil, behøver du ikke endre standardverdiene på inn-/utformat." & vbLf & "" & vbLf & "Hvis du er i tvil kontakt din forhandler."
                sEnglish = "Enter fixed parameters for the new clients to be omported." & vbLf & "If You have only one profile, no changes in the standard values for format in/out are needed." & vbLf & "" & vbLf & "If in doubt, please contact Your dealer."
                sSwedish = "Ange fasta parametrar för nya kunder som ska importeras." & vbLf & "Om du bara har en profil behöver du inte ändra standardvärdena på in-/utformat." & vbLf & "" & vbLf & "Om du har funderingar, kontakta din återförsäljare.	"
                sDanish = "Indtast faste parametre på nye klienter som skal importeres." & vbLf & "Hvis du kun har en profil, behøver du ikke ændre standardværdierne på ind-/udformat." & vbLf & "" & vbLf & "Hvis du er i tvivl kontakt din forhandler.	"
            Case 60255
                sNorwegian = "Import av klientinformasjon"
                sEnglish = "Import clientinformation"
                sSwedish = "Import av klientinformation"
                sDanish = "Import af klientinformation"
            Case 60256
                sNorwegian = "Åpner manuell avstemming"
                sEnglish = "Opens manual matching"
                sSwedish = "Öppnar manuell avstämning"
                sDanish = "Åbner manuel afstemning"
            Case 60257
                sNorwegian = "KID:"
                sEnglish = "KID:"
                sSwedish = "OCR:"
                sDanish = "KID:"
            Case 60258
                sNorwegian = "Du kan i dette bildet legge inn informasjon om mottatt beløp og bankgebyr." & vbLf & "Hvis angitt valuta avviker fra kontoens valuta, vil mottatt beløp benyttes ved postering." & vbLf & "Gebyr må alltid angis i kontoens valuta."
                sEnglish = "You may change the information about received amount and bankcharges in this screen." & vbLf & "If the currency You state differs from the accounts currency, the amount stated will be used for matching." & vbLf & "Note that the charges always must be stated in the accounts currency."
                sSwedish = "Du kan ange information om mottaget belopp och bankavgifter i denna bild." & vbLf & "Om angiven valuta avviker från kontots valuta kommer mottaget belopp att användas vid bokföring." & vbLf & "Avgifter måste alltid anges i kontots valuta.	"
                sDanish = "Du kan i dette billede tilføje information om modtaget beløb og bankgebyr." & vbLf & "Hvis angivet valuta afviger fra kontoens valuta, vil det modtagne beløb benyttes ved postering." & vbLf & "Gebyr skal altid angives i kontoens valuta.	"
            Case 60259
                sNorwegian = "Betalt beløp er justert av BabelBank"
                sEnglish = "Original amount adjusted by BabelBank"
                sSwedish = "Betalt belopp justeras av BabelBank"
                sDanish = "Betalt beløb er justeret af BabelBank"
            Case 60260
                sNorwegian = "Beløp før justering"
                sEnglish = "Amount before adjustment"
                sSwedish = "Belopp före justering"
                sDanish = "Beløb før justering"
            Case 60261
                sNorwegian = "Betalingsvaluta justert av BabelBank"
                sEnglish = "Original currency adjusted by BabelBank"
                sSwedish = "Betalningsvaluta justerad av BabelBank"
                sDanish = "Betalingsvaluta justeret af BabelBank"
            Case 60262
                sNorwegian = "Valutakode før justering"
                sEnglish = "Currency before adjustment"
                sSwedish = "Valutakod före justering"
                sDanish = "Valutakode før justering"
            Case 60263
                sNorwegian = "Bruk opprinnelig betalt beløp"
                sEnglish = "Use originally paid amount"
                sSwedish = "Använd det ursprungliga betalda beloppet"
                sDanish = "Brug oprindeligt betalt beløb"
            Case 60264
                sNorwegian = "Gebyrbeløp justert av BabelBank, fra"
                sEnglish = "Chargesamount adjusted by BabelBank, from"
                sSwedish = "Avgiftsbelopp justeras med BabelBank, från"
                sDanish = "Gebyrbeløb justeret af BabelBank, fra"
            Case 60265
                sNorwegian = "Gebyrvaluta justert av BabelBank fra"
                sEnglish = "Chargescurrency adjusted by BabelBank, from"
                sSwedish = "Avgiftrvaluta justeras av BabelBank från"
                sDanish = "Gebyrvaluta justeret af BabelBank fra"
            Case 60266
                sNorwegian = "Tillatt beløpsavvik"
                sEnglish = "Allowed difference"
                sSwedish = "Tillåten avvikelse i belopp"
                sDanish = "Tilladt beløbsafvigelse"
            Case 60267
                sNorwegian = "Endre alle filstier i BabelBank"
                sEnglish = "Change all filepaths in BabelBank"
                sSwedish = "Ändra alla sökvägar i BabelBank"
                sDanish = "Ændre alle filstier i BabelBank"
            Case 60268
                sNorwegian = "Dette er et valg for å endre alle filstier i BabelBank." & vbLf & "Angi opprinnelig katalog og hva denne skal endres til, før du klikker på kjør." & vbLf & "" & vbLf & "Hvis man f.eks. skal endre driver på alle filstier fra C: til G:, så angir man C:\ i feltet 'Orginal filsti' og G:\ i feltet 'Ny filsti'."
                sEnglish = "You may use this screen to change all the filepaths in BabelBank." & vbLf & "Enter the original path/part of a path, and the new path/part of a path. Then click on run." & vbLf & "" & vbLf & "If You for instance want to change the drive on all paths from C:\ to G:\, enter C:\ in the field 'Original path' and G:\ in the field 'New path'."
                sSwedish = "Detta är ett alternativ för att ändra alla sökvägar i BabelBank." & vbLf & "Ange källkatalog och vad denna ska ändras till innan du klickar på kör." & vbLf & "" & vbLf & "Om man till exempel ska ändra alla sökvägar från C: till G:, så anger man C:\ i fältet 'Originalsökväg' och G:\ i fältet 'Ny sökväg'.	"
                sDanish = "Dette er et valg for at ændre alle filstier i BabelBank." & vbLf & "Angiv oprindeligt katalog og hvad dette skal ændres til, før du klikker på kør." & vbLf & "" & vbLf & "Hvis man f.eks. skal ændre driver på alle filstier fra C: til G:, så angiver man C:\ i feltet 'Orginal filsti' og G:\ i feltet 'Ny filsti'.	"
            Case 60269
                sNorwegian = "Orginal filsti:"
                sEnglish = "Original path:"
                sSwedish = "Ursprunglig sökväg:"
                sDanish = "Original filsti:"
            Case 60270
                sNorwegian = "Ny filsti:"
                sEnglish = "New path:"
                sSwedish = "Ny sökväg:"
                sDanish = "Ny filsti:"
            Case 60271
                sNorwegian = "Bytt orginal/ny"
                sEnglish = "Change original/new"
                sSwedish = "Ersätt original/ny"
                sDanish = "Skift original/ny"
            Case 60272
                sNorwegian = "Endre filstier"
                sEnglish = "Change of filepath"
                sSwedish = "Ändra sökvägar"
                sDanish = "Ændr filstier"
            Case 60273
                sNorwegian = "Ønsker du å bytte alle filstier i BabelBank?"
                sEnglish = "Do You want to change all the filepaths in BabelBank?"
                sSwedish = "Vill du ersätta alla sökvägar i BabelBank?"
                sDanish = "Vil du udskifte alle filstier i BabelBank?"
            Case 60274
                sNorwegian = "Eksportér kun fullt posterte betalinger"
                sEnglish = "Export only fully posted payments"
                sSwedish = "Export endast fullständigt bokförda betalningar"
                sDanish = "Eksportér kun fuldt posterede betalinger"
            Case 60275
                sNorwegian = "Ønsker du å fjerne all posteringsinformasjon?"
                sEnglish = "Do You want to remove all matching information?"
                sSwedish = "Vill du ta bort någon bokföringsinformation?"
                sDanish = "Ønsker du at fjerne alle posteringsinformationer?"
            Case 60276
                sNorwegian = "Sist posterte"
                sEnglish = "Last posted"
                sSwedish = "Senast bokförda"
                sDanish = "Sidst posterede"
            Case 60277
                sNorwegian = "BabelBank kan ikke utføre ønsket handling, fordi betalingen er låst av en annen bruker."
                sEnglish = "Can't process the requested action, because the payment is locked by another user."
                sSwedish = "BabelBank kan inte utföra önskad åtgärd, eftersom betalningen är låst av en annan användare."
                sDanish = "BabelBank kan ikke udføre ønsket handling, fordi betalingen er låst af en anden bruger."
            Case 60278
                sNorwegian = "BabelBank kan ikke utføre ønsket handling, fordi du ennå ikke har postert noen innbetaling."
                sEnglish = "Can't process the requested action, because You have not yet posted any payments"
                sSwedish = "BabelBank kan inte utföra den önskade åtgärden eftersom du ännu inte har bokfört någon betalning."
                sDanish = "BabelBank kan ikke udføre ønsket handling, fordi du endnu ikke har posteret nogen indbetaling."
            Case 60279
                sNorwegian = "Kundereskontro"
                sEnglish = "Sales ledger"
                sSwedish = "Kundreskontra"
                sDanish = "Kundesamlekonto"
            Case 60280
                sNorwegian = "Hovedbokskonto"
                sEnglish = "GL Account"
                sSwedish = "Huvudbokskonto"
                sDanish = "Hovedbogskonto"
            Case 60281
                sNorwegian = "KID"
                sEnglish = "KID"
                sSwedish = "KID"
                sDanish = "KID"
            Case 60282
                sNorwegian = "Fra ERP"
                sEnglish = "From ERP"
                sSwedish = "Från ERP"
                sDanish = "Fra ERP"
            Case 60283
                sNorwegian = "Ikke juster"
                sEnglish = "Don't adjust invoice"
                sSwedish = "Ojusterade"
                sDanish = "Juster ikke"
            Case 60284
                sNorwegian = "Kontotype"
                sEnglish = "Accounttype"
                sSwedish = "Kontotyp"
                sDanish = "Kontotype"
            Case 60285
                sNorwegian = "BabelBank finner ikke ønsket SQL for å postere de importerte detaljene."
                sEnglish = "BabelBank is not able to find the desired SQL to match the imported details."
                sSwedish = "BabelBank kan inte hitta den nödvändiga SQL:n för att bokföra importerade detaljer."
                sDanish = "BabelBank kan ikke finde ønsket SQL for at postere de importerede detaljer."
            Case 60286
                sNorwegian = "Navn på spørring:"
                sEnglish = "Name of SQL:"
                sSwedish = "Namn på fråga:"
                sDanish = "Navn på forespørgsel:"
            Case 60287
                sNorwegian = "Utbyttingen stoppes."
                sEnglish = "The exchange will be aborted."
                sSwedish = "Ersättning stoppades."
                sDanish = "Udskiftningen stoppes."
            Case 60288
                sNorwegian = "MANGLENDE SQL"
                sEnglish = "MISSING SQL"
                sSwedish = "SAKNAD SQL"
                sDanish = "MANGLENDE SQL"
            Case 60289
                sNorwegian = "Beløp på importerte poster avviker fra beløp på innbetaling."
                sEnglish = "The total amount of the imported specification, deviates from the paid amount."
                sSwedish = "Belopp i importerade poster avviker från belopp i inbetalning."
                sDanish = "Beløb på importerede poster afviger fra beløb på indbetaling."
            Case 60290
                sNorwegian = "Importert beløp:"
                sEnglish = "Imported amount:"
                sSwedish = "Importerat belopp:"
                sDanish = "Importeret beløb:"
            Case 60291
                sNorwegian = "Innbetalt beløp:"
                sEnglish = "Paid amount:"
                sSwedish = "Inbetalt belopp:"
                sDanish = "Indbetalt beløb:"
            Case 60292
                sNorwegian = "Differanse vil bli postert på konto:"
                sEnglish = "The deviation will be posted on the account:"
                sSwedish = "Variansen bokförs på konto:"
                sDanish = "Differencen bliver posteret på konto:"
            Case 60293
                sNorwegian = "Ønsker du å fortsette?"
                sEnglish = "Do You want to continue?"
                sSwedish = "Vill du fortsätta?"
                sDanish = "Vil du fortsætte?"
            Case 60294
                sNorwegian = "Kunne ikke lagre data:"
                sEnglish = "Couldn't save data:"
                sSwedish = "Kunde inte spara data:"
                sDanish = "Kunne ikke gemme data:"
            Case 60295
                sNorwegian = "Ønsker du å konvertere innsendingsformatet til bank til Telepay + ?"
                sEnglish = "Do you like to convert sendformat to bank to Telepay + ?"
                sSwedish = "Vill konvertera insändningsformat för bank till Telepay + ?"
                sDanish = "Vil du konvertere indsendelsesformatet til bank til Telepay + ?"
            Case 60296
                sNorwegian = "Ingen formathenvisninger ble endret."
                sEnglish = "No formats were changed !"
                sSwedish = "Inga formatreferenser ändrades."
                sDanish = "Der blev ikke ændret formathenvisninger."
            Case 60297
                sNorwegian = "MISLYKKET ENDRING!"
                sEnglish = "UNSUCCESSFULL CHANGE!"
                sSwedish = "MISSLYCKAD ÄNDRING!"
                sDanish = "MISLYKKET ÆNDRING!"
            Case 60298
                sNorwegian = "Totalt ble"
                sEnglish = "A total of"
                sSwedish = "Totalt har"
                sDanish = "Totalt blev"
            Case 60299
                sNorwegian = "formathenvisninger endret."
                sEnglish = "formats were changed."
                sSwedish = "formatreferenser ändrats."
                sDanish = "formathenvisninger ændret."
            Case 60300
                sNorwegian = "VELLYKKET ENDRING!"
                sEnglish = "SUCCESSFULL CHANGE!"
                sSwedish = "FELFRI ÄNDRING!"
                sDanish = "VELLYKKET ÆNDRING!"
            Case 60301
                sNorwegian = "Antall poster eksportert: %1"
                sEnglish = "Number of transactions exported: %1"
                sSwedish = "Antal poster som exporterats: %1"
                sDanish = "Antal poster eksporteret: %1"
            Case 60302
                sNorwegian = "Totalsum på eksporterte poster: %1"
                sEnglish = "Total amount exported: %1"
                sSwedish = "Totalsumma i exporterade poster: %1"
                sDanish = "Totalsum på eksporterede poster: %1"
            Case 60303
                sNorwegian = "Eksporterer fil på format %1"
                sEnglish = "Exporting file on format %1"
                sSwedish = "Exporterar fil i format %1"
                sDanish = "Eksporterer fil til format %1"
            Case 60304
                sNorwegian = "Alle poster"
                sEnglish = "All items"
                sSwedish = "Alla poster"
                sDanish = "Alle poster"
            Case 60305
                sNorwegian = "Betalingsdato"
                sEnglish = "Date of payment"
                sSwedish = "Betalningsdatum"
                sDanish = "Betalingsdato"
            Case 60307
                sNorwegian = "Total antall uavstemte:"
                sEnglish = "Total no of unmatched:"
                sSwedish = "Totalt antal avstämda:"
                sDanish = "Totalt antal afstemte:"
            Case 60308
                sNorwegian = "Ulovlig bruk av karakterer, asciiverdi = "
                sEnglish = "Illegal character, asciivalue = "
                sSwedish = "Ulovlig bruk av karakterer, asciiverdi = "
                sDanish = "Olaglig användning av tecken, ascii värde" = ""
            Case 60309
                sNorwegian = ". Reell posisjon "
                sEnglish = ". Real position "
                sSwedish = ". Reell läge "
                sDanish = ". Reell position "
            Case 60310
                sNorwegian = ". Posisjon i editor "
                sEnglish = ". Position in editor "
                sSwedish = ". Position i editor "
                sDanish = ". Position i editor "
            Case 60311
                sNorwegian = "Finner ikke CRLF(*) (ascii 13+ ascii 10), men finner CR(*) (ascii(13), men i feil posisjon. Forventet posisjon "
                sEnglish = "Found no CRLF(*) (ascii 13+ ascii 10), but found CR(*) (ascii(13), but in wrong position. Expected position "
                sSwedish = "Kan inte hitta CRLF (*) (ascii 13+ ascii 10), men finner CR (*) (ascii (13), men i fel läge. Förväntad läge "
                sDanish = "Kan ikke finde CRLF (*) (ascii 13+ ascii 10), men finder CR (*) (ASCII (13), men i forkert position. Forventet position "
            Case 60312
                sNorwegian = "Funnet i posisjon "
                sEnglish = "Found in position "
                sSwedish = "Hittade i läge "
                sDanish = "Fundet i position "
            Case 60313
                sNorwegian = "Finner ikke CRLF(*) (ascii 13+ ascii 10), men finner CR(*) (ascii(13). Fant i posisjon "
                sEnglish = "Found no CRLF(*) (ascii 13+ ascii 10), but found CR(*) (ascii(13). Found in position "
                sSwedish = "Kan inte hitta CRLF (*) (ascii 13+ ascii 10), men finner CR(*) (ascii (13). Fann i läge "
                sDanish = "Kan ikke finde CRLF (*) (ascii 13+ ascii 10), men finder CR(*) (ascii (13). Fundet i position "
            Case 60314
                sNorwegian = "Finner ikke CRLF(*) (ascii 13+ ascii 10), men finner LF(*) (ascii(10), men i feil posisjon. Forventet posisjon "
                sEnglish = "Found no CRLF(*) (ascii 13+ ascii 10), but found LF(*) (ascii(10), but in wrong position. Expected position "
                sSwedish = "Kan inte hitta CRLF (*) (ascii 13+ ascii 10), men finner LF(*) (ascii (10), men i fel läge. Förväntad läge "
                sDanish = "Kan ikke finde CRLF (*) (ascii 13+ ascii 10), men finder LF(*) (ascii (10), men i forkert position. Forventet position "
            Case 60315
                sNorwegian = "Finner ikke CRLF(*) (ascii 13+ ascii 10), men finner LF(*) (ascii(10). Fant i posisjon "
                sEnglish = "Found no CRLF(*) (ascii 13+ ascii 10), but found LF(*) (ascii(10). Found in position "
                sSwedish = "Kan inte hitta CRLF (*) (ascii 13+ ascii 10), men finner LF(*) (ascii (10). Fann i läge "
                sDanish = "Kan ikke finde CRLF (*) (ascii 13+ ascii 10), men finder LF(*) (ascii (10). Fundet i position "
            Case 60316
                sNorwegian = "Finner ikke CR(*) (ascii 13) og ikke LF(*) (ascii 10) in file. Stopper gjennomgang."
                sEnglish = "Found no CR(*) (ascii 13) and no LF(*) (ascii 10) in file. Stops processing."
                sSwedish = "Kan inte hitta CR (*) (ascii 13) och inte LF (*) (ascii 10) i filen. Stannar översyn."
                sDanish = "Kan ikke finde CR (*) (ascii 13) og ikke LF (*) (ascii 10) i filen. Stopper gennemgang."
            Case 60317
                sNorwegian = "Finner CRLF(*), men i feil posisjon. Forventet posisjon "
                sEnglish = "Found CRLF(*), but in wrong position. Expected position "
                sSwedish = "Hittar CRLF(*), men i fel läge. Förväntad läge "
                sDanish = "Finder CRLF(*), men i forkert position. Forventet position "
            Case 60318
                sNorwegian = "Finner CRLF(*) (ascii 13+ ascii 10), men fant en annen CR(*) (ascii 13) i samme linje."
                sEnglish = "Found CRLF(*) (ascii 13+ ascii 10), but found another CR(*) (ascii 13) inside the line."
                sSwedish = "Hittar CRLF(*) (ascii 13+ ascii 10), men fann en annan CR(*) (ascii 13) i samma linje."
                sDanish = "Finder CRLF(*) (ascii 13+ ascii 10), men fundet en anden CR(*) (ascii 13) i samme linje."
            Case 60319
                sNorwegian = "Finner CRLF(*) (ascii 13+ ascii 10), men fant en annen CR(*) (ascii 13) i samme linje."
                sEnglish = "Found CRLF(*) (ascii 13+ ascii 10), but found another LF(*) (ascii 13) inside the line."
                sSwedish = "Hittar CRLF(*) (ascii 13+ ascii 10), men fann en annan CR(*) (ascii 13) i samma linje."
                sDanish = "Finder CRLF(*) (ascii 13+ ascii 10), men fandt en anden CR(*) (ascii 13) i samme linje."
            Case 60320
                sNorwegian = "Feil linjelengde - Linjelengde = "
                sEnglish = "Wrong line length - Linelength = "
                sSwedish = "Fel linjelängd - Linjelängd = "
                sDanish = "Fejl linjelængde - Linjelængde = "
            Case 60321
                sNorwegian = "Forventet "
                sEnglish = "Expected "
                sSwedish = "Förväntad "
                sDanish = "Forventet "
            Case 60322
                sNorwegian = "Ikke komplett fil/record."
                sEnglish = "Incomplete file/record."
                sSwedish = "Ikke komplett fil/record."
                sDanish = "Ikke komplett fil/record."
            Case 60323
                sNorwegian = "Mangler BETFOR"
                sEnglish = "Missing BETFOR "
                sSwedish = "Saknar BETFOR"
                sDanish = "Mangler BETFOR"
            Case 60324
                sNorwegian = "Feil BETFOR - Fant "
                sEnglish = "Incorrect BETFOR - Found "
                sSwedish = "Fel BETFOR - Fann "
                sDanish = "Fejl BETFOR - Fandt "
            Case 60325
                sNorwegian = "Ulogisk kombinasjon av records. Husk å adskille innlands- og utlandsrecords med en ny batch (BETFOR00/99). Forrige record "
                sEnglish = "Illegal combination of records. Remember to separate domestic and international records by new batch (BETFOR00/99). Previous record "
                sSwedish = "Ulogisk kombinasjon av records. Husk å adskille innlands- og utlandsrecords med en ny batch (BETFOR00/99). Forrige record "
                sDanish = "Ulogisk kombinasjon av records. Husk å adskille innlands- og utlandsrecords med en ny batch (BETFOR00/99). Forrige record "
            Case 60326
                sNorwegian = "Denne record: "
                sEnglish = "This record: "
                sSwedish = "Denna post: "
                sDanish = "Denne record: "
            Case 60327
                sNorwegian = "Ulogisk kombinasjon av records. Forrige record "
                sEnglish = "Illegal combination of records. Previous record "
                sSwedish = "Ologiskt kombination av records. Forrige record "
                sDanish = "Ulogisk kombinasjon af records. Forrige record "
            Case 60328
                sNorwegian = "Feil header."
                sEnglish = "Wrong header."
                sSwedish = "Fel header."
                sDanish = "Fejl header."
            Case 60329
                sNorwegian = "Sekvensfeil, denne sekvens "
                sEnglish = "Sequenceerror, this sequence "
                sSwedish = "Sekvensfel, denne sekvens "
                sDanish = "Sekvensfejl, denne sekvens "
            Case 60330
                sNorwegian = "- Forrige sekvens "
                sEnglish = " - Previous sequence "
                sSwedish = "- Forrige sekvens "
                sDanish = "- Forrige sekvens "
            Case 60331
                sNorwegian = "Feil avslutning av filen"
                sEnglish = "Incorrect completion of file"
                sSwedish = "Fel avslutning av filen"
                sDanish = "Fejl afslutningen af filen"
            Case 60332
                sNorwegian = "Purrenummer: "
                sEnglish = "Reminderno: "

                '----------------------------------------------
                ' NB Leave room for msg in BabelBank for VB6
                ' Start next on 60330
                sSwedish = "Påminnelsenummer:"
                sDanish = "Rykkernummer:"
            Case 60333
                sNorwegian = "Antall feil:"
                sEnglish = "No of errors:"
                sSwedish = "Antal fel:"
                sDanish = "Antal fejl:"
            Case 60334
                sNorwegian = "Det ble funnet en ukjent konto blant betalingene." & vbCrLf & "Vennligst legg denne inn under klientoppsettet i BabelBank" & vbCrLf & "Konto: %1"
                sEnglish = "An unknown account was found among the payments." & vbCrLf & "Add the new account in the clientsetup in BabelBank" & vbCrLf & "Account: %1"
                sSwedish = "Det befanns ett okänt konto bland betalningar." & vbCrLf & "Tilføj vänligen dette i klientsetup i BabelBank" & vbCrLf & "Konto: %1"
                sDanish = "Det blev fundet en ukendt konto blandt betalinger." & vbCrLf & "Tilføj venligst dette i klientopsætning i BabelBank" & vbCrLf & "Konto: %1"
            Case 60335
                sNorwegian = "Det ble funnet en ukjent klient blant betalingene." & vbCrLf & "Vennligst legg denne inn under klientoppsettet i BabelBank" & vbCrLf & "Klient: %1"
                sEnglish = "An unknown client was found among the payments." & vbCrLf & "Add the new account in the clientsetup in BabelBank" & vbCrLf & "Client: %1"
                sSwedish = "Det befanns ett okänt klient bland betalningar." & vbCrLf & "Tilføj vänligen dette i klientsetup i BabelBank" & vbCrLf & "Klient: %1"
                sDanish = "Det blev fundet en ukendt klient blandt betalinger." & vbCrLf & "Tilføj venligst dette i klientopsætning i BabelBank" & vbCrLf & "Klient: %1"
            Case 60336
                sNorwegian = "Ukjent konto"
                sEnglish = "Unknown account"
                sSwedish = "Okänt konto"
                sDanish = "Ukendt konto"
                'Case 60335
                '    sNorwegian = "Sjekker CR og LF"
                '    sEnglish = "Checking CR and LF"
                'Case 60336
                '    sNorwegian = "Sjekker linjelengde"
                '    sEnglish = "Checking linelength"
            Case 60337
                sNorwegian = "Sjekker karaktersett"
                sEnglish = "Checking characterset"
                sSwedish = "Checkar teckenuppsättning"
                sDanish = "Checks karaktersæt"
            Case 60338
                sNorwegian = "Analyse fullført."
                sEnglish = "Analysis completed."
                sSwedish = "Analys slutförts ."
                sDanish = "Analyse gennemført."
            Case 60339
                sNorwegian = "Feilen som ble oppdaget gjør det ikke mulig å fortsette analyse av filen."
                sEnglish = "The error found makes it impossible to continue the fileanalysis."
                sSwedish = "Felet som blev upptäcktes gör det omöjligt att fortsätta analysen av filen."
                sDanish = "Fejlen som blev opdaget gør det umuligt at fortsætte analyse af filen."
            Case 60340
                sNorwegian = "Maksimalt antall feil er nådd"
                sEnglish = "Maximum number of errors reached"
                sSwedish = "Det maksimale antal fejl er nået"
                sDanish = "Maksimalt antall feil er nådd"
            Case 60341
                sNorwegian = "Sjekker om Telepayfil/records er komplette"
                sEnglish = "Checking Telepay complete record/file"
                sSwedish = "Kontrollerar om Telepayfil/records är fullständiga"
                sDanish = "Kontrollerer om Telepayfil/records er fuldstændige"
            Case 60342
                sNorwegian = "Asciiverdier"
                sEnglish = "Asciivalues"
                sSwedish = "Asciivärden"
                sDanish = "Asciiværdier"
            Case 60343
                sNorwegian = "Velg database"
                sEnglish = "Select database"
                sSwedish = "Välj databas"
                sDanish = "Vælg database"
            Case 60344
                sNorwegian = "Framdrift"
                sEnglish = "Progress"
                sSwedish = "Förloppsindikator "
                sDanish = "Statuslinje"
            Case 60345
                sNorwegian = "Foretaksnummer"
                sEnglish = "Company number"
                sSwedish = "Företagsnummer"
                sDanish = "Virksomhedsnummer"
            Case 60346
                sNorwegian = "Firmanavn"
                sEnglish = "Company name"
                sSwedish = "Företagsnamn"
                sDanish = "Firmanavn"
            Case 60347
                sNorwegian = "Firmaopplysninger"
                sEnglish = "Company setup"
                sSwedish = "Företagsinformation"
                sDanish = "Firmaoplysninger"
            Case 60348
                sNorwegian = "Backupkatalog"
                sEnglish = "Backup folder"
                sSwedish = "Säkerhetskopieringskatalog"
                sDanish = "Backupkatalog"
            Case 60349
                sNorwegian = "Backup, antall dager	"
                sEnglish = "Backup, no of days	"
                sSwedish = "Säkerhetskopiering, antal dagar"
                sDanish = "Backup, antal dage"
            Case 60350
                sNorwegian = "EMail avsender"
                sEnglish = "eMail sender"
                sSwedish = "EMail-avsändare"
                sDanish = "E-mailafsender"
            Case 60351
                sNorwegian = "Profiloppsett"
                sEnglish = "Profile setup"
                sSwedish = "Profilinställning"
                sDanish = "Profilopsætning"
            Case 60352
                sNorwegian = "Beskrivelse"
                sEnglish = "Description"
                sSwedish = "Beskrivning"
                sDanish = "Beskrivelse"
            Case 60353
                sNorwegian = "Profiltype"
                sEnglish = "Profile type"
                sSwedish = "Profiltyp"
                sDanish = "Profiltype"
            Case 60354
                sNorwegian = "Avstemmingsprofil"
                sEnglish = "Matchingprofile"
                sSwedish = "Avstämningsprofil"
                sDanish = "Afstemningsprofil"
            Case 60355
                sNorwegian = "Sendeprofil"
                sEnglish = "Sendprofile"
                sSwedish = "Sändningsprofil"
                sDanish = "Sendeprofil"
            Case 60356
                sNorwegian = "Returprofil"
                sEnglish = "Returnprofile"
                sSwedish = "Returprofil"
                sDanish = "Returprofil"
            Case 60357
                sNorwegian = "Spesialkode"
                sEnglish = "Special code"
                sSwedish = "Specialkod"
                sDanish = "Specialkode"
            Case 60358
                sNorwegian = "Importformat"
                sEnglish = "Import format"
                sSwedish = "Importformat"
                sDanish = "Importformat"
            Case 60359
                sNorwegian = "Mappingfil"
                sEnglish = "Mapping file"
                sSwedish = "Mappningsfil"
                sDanish = "Mappingfil"
            Case 60360
                sNorwegian = "Importfil"
                sEnglish = "Import file"
                sSwedish = "Importfil"
                sDanish = "Importfil"
            Case 60361
                sNorwegian = "Det tas backup av importfiler	"
                sEnglish = "Backup of importfiles taken	"
                sSwedish = "Importfiler säkerhetskopieras"
                sDanish = "Der tages backup af importfiler"
            Case 60362
                sNorwegian = "KundenhetsID"
                sEnglish = "Kundeenhets ID"
                sSwedish = "KundenhetsID"
                sDanish = "KundeenhedsID"
            Case 60363
                sNorwegian = "Logging til Windows Eventlog	"
                sEnglish = "Logging to Windows eventlog	"
                sSwedish = "Loggning till Windows Eventlog"
                sDanish = "Logging til Windows Eventlog"
            Case 60364
                sNorwegian = "Logging til fil"
                sEnglish = "Logging to file	"
                sSwedish = "Loggning till fil"
                sDanish = "Logging til fil"
            Case 60365
                sNorwegian = "Logging til email	"
                sEnglish = "Logging to eMail"
                sSwedish = "Loggning till e-post"
                sDanish = "Logging til e-mail"
            Case 60366
                sNorwegian = "Visning"
                sEnglish = "Display"
                sSwedish = "Visning"
                sDanish = "Visning"
            Case 60367
                sNorwegian = "Profil kjøres uten visning til skjerm (silent)	"
                sEnglish = "Profile runs without messaging to screen (silent)"
                sSwedish = "Profil körs utan visning på skärmen (tyst)"
                sDanish = "Profil køres uden visning til skærm (silent)"
            Case 60368
                sNorwegian = "Divisjon"
                sEnglish = "Division"
                sSwedish = "Division"
                sDanish = "Division"
            Case 60369
                sNorwegian = "Eksportformat"
                sEnglish = "Export format"
                sSwedish = "Exportformat"
                sDanish = "Eksportformat"
            Case 60370
                sNorwegian = "Eksportfil"
                sEnglish = "Export file"
                sSwedish = "Exportfil"
                sDanish = "Eksportfil"
            Case 60371
                sNorwegian = "Backup"
                sEnglish = "Backup"
                sSwedish = "Säkerhetskopia"
                sDanish = "Backup"
            Case 60372
                sNorwegian = "Det tas backup av eksportfiler	"
                sEnglish = "Backup of exportfiles taken	"
                sSwedish = "Exportfiler säkerhetskopieras"
                sDanish = "Der tages backup af eksportfiler"
            Case 60373
                sNorwegian = "Valgt betalingstype"
                sEnglish = "Selected payment type"
                sSwedish = "Vald betalningstyp"
                sDanish = "Valgt betalingstype"
            Case 60374
                sNorwegian = "Antall dager som legges til	"
                sEnglish = "No of days to add to payment date"
                sSwedish = "Antal dagar som läggs till"
                sDanish = "Antal dage der tilføjes"
            Case 60375
                sNorwegian = "Slå sammen betalinger	"
                sEnglish = "Merge payments"
                sSwedish = "Slå ihop betalningar"
                sDanish = "Slå betalinger sammen"
            Case 60376
                sNorwegian = "Slå sammen betalinger, men behold all informasjon til mottaker	"
                sEnglish = "Merge payments, but keep info to receiver	"
                sSwedish = "Slå ihop betalningar, men behåll all information till mottagaren"
                sDanish = "Slå betalinger sammen, men behold alle informationer til modtager"
            Case 60377
                sNorwegian = "Slå sammen betalinger, uavhengig om informasjon går tapt	"
                sEnglish = "Merge payments, regardless of informationloss	"
                sSwedish = "Slå ihop betalningar oavsett om information går förlorad"
                sDanish = "Slå betalinger sammen, uafhængig om information går tabt"
            Case 60378
                sNorwegian = "Slå sammen betalinger, og fjern all informasjon til mottaker	"
                sEnglish = "Merge payments, keep no info to receiver	"
                sSwedish = "Slå ihop betalningar och ta bort all information till mottagaren"
                sDanish = "Slå betalinger sammen, og fjern alle informationer til modtager"
            Case 60379
                sNorwegian = "Slå sammen betalinger, kun dersom det er med kreditonater	"
                sEnglish = "Merge payments only where creditnotes are involved	"
                sSwedish = "Slå ihop betalningar endast för kreditnotor"
                sDanish = "Slå kun betalinger sammen, hvis det er med kreditnotaer"
            Case 60380
                sNorwegian = "Rapporter"
                sEnglish = "Reports"
                sSwedish = "Rapporter"
                sDanish = "Rapporter"
            Case 60381
                sNorwegian = "Eksporttype"
                sEnglish = "Export type"
                sSwedish = "Exporttyp"
                sDanish = "Eksporttype"
            Case 60382
                sNorwegian = "Filnavn"
                sEnglish = "File name"
                sSwedish = "Filnamn"
                sDanish = "Filnavn"
            Case 60383
                sNorwegian = "Klienter"
                sEnglish = "Clients"
                sSwedish = "Klienter"
                sDanish = "Klienter"
            Case 60384
                sNorwegian = "Klient"
                sEnglish = "Client"
                sSwedish = "Klient"
                sDanish = "Klient"
            Case 60385
                sNorwegian = "Konti"
                sEnglish = "Account"
                sSwedish = "konton"
                sDanish = "Konti"
            Case 60386
                sNorwegian = "AvtaleID"
                sEnglish = "Agreement ID"
                sSwedish = "AvtalsID"
                sDanish = "AftaleID"
            Case 60387
                sNorwegian = "Hovedbok"
                sEnglish = "General Ledger"
                sSwedish = "Huvudbok"
                sDanish = "Hovedbog"
            Case 60388
                sNorwegian = "Konvertert(konto)"
                sEnglish = "Converted account"
                sSwedish = "Konverterat konto"
                sDanish = "Konverteret konto"
            Case 60389
                sNorwegian = "Avtalegiro(ID)"
                sEnglish = "Avtalegiro ID"
                sSwedish = "Avtalsgiro-ID"
                sDanish = "Aftalegiro ID"
            Case 60390
                sNorwegian = "Valuta"
                sEnglish = "Currency"
                sSwedish = "Valuta"
                sDanish = "Valuta"
            Case 60391
                sNorwegian = "Landkode"
                sEnglish = "Country code"
                sSwedish = "Landsnummer"
                sDanish = "Landekode"
            Case 60392
                sNorwegian = "BabelBank(dokumentasjon)"
                sEnglish = "BabelBank documentation"
                sSwedish = "BabelBank-dokumentation"
                sDanish = "BabelBank dokumentation"
            Case 60393
                sNorwegian = "Dette dokumentet viser en oversikt over oppsettet i BabelBank for DnB."
                sEnglish = "This document is an overview of the settings in BabelBank for DnB."
                sSwedish = "Detta dokument ger en översikt över inställningen i BabelBank för DNB."
                sDanish = "Dette dokumentet viser en oversigt over opsætningen i BabelBank for DNB."
            Case 60394
                sNorwegian = "Rapport for filen "
                sEnglish = "Report for file "
                sSwedish = "Rapport för fil "
                sDanish = "Rapport for fil "
            Case 60395
                sNorwegian = "Gyldig tegnsett ser sjekket mot standarden ISO 8859-1."
                sEnglish = "Charactersett controlled against ISO8859-1."
                sSwedish = "Giltig teckenuppsättning  kontrolleras mot standarden ISO 8859-1."
                sDanish = "Gyldig tegnsæt kontrolleres mod standard ISO 8859-1."
            Case 60396
                sNorwegian = "* (CR = CarriageReturn - Retur) "
                sEnglish = "* (CR = CarriageReturn ) "
                sSwedish = "* (CR = CarriageReturn - Retur) "
                sDanish = "* (CR = CarriageReturn - Retur) "
            Case 60397
                sNorwegian = "* (LF = LineFeed - Linjeskift) "
                sEnglish = "* (LF = LineFeed) "
                sSwedish = "* (LF = LineFeed - Radbrytning) "
                sDanish = "* (LF = LineFeed - Radmatning) "
            Case 60398
                sNorwegian = "* (CRLF = CarriageReturn/LineFeed - Retur/Linjeskift) "
                sEnglish = "* (CRLF = CarriageReturn/LineFeed) "
                sSwedish = "* (CRLF = CarriageReturn/LineFeed - Retur/Radbrytning) "
                sDanish = "* (CRLF = CarriageReturn/LineFeed - Retur/Radmatning) "
            Case 60399
                sNorwegian = "Ingen feil ble funnet i filen."
                sEnglish = "No errors are found in the file."
                sSwedish = "Inga fel hittades i filen."
                sDanish = "Ingen fejl blev fundet i filen."
            Case 60400
                sNorwegian = "Følgende feil ble funnet i filen "
                sEnglish = "These files are found in the file: "
                sSwedish = "Följande fel hittades i filen."
                sDanish = "Følgende fejl blev fundet i filen."
            Case 60401
                sNorwegian = "Analyse av betalingsfiler"
                sEnglish = "Paymentfile analysis"
                sSwedish = "Analys av betalningsfiler"
                sDanish = "Analyse af betalingsfiler"
            Case 60402
                sNorwegian = "Validér posteringer før eksport (anbefales)"
                sEnglish = "Validate postings before export (recomended)"
                sSwedish = "Validera poster före export (rekommenderas)"
                sDanish = "Validér posteringer før eksport (anbefales)"
            Case 60403
                sNorwegian = "Rapporter avvik (alternativ er automatisk oppheving av betaling)"
                sEnglish = "Report deviations (as opposed to automatic unposting of the payment)"

                ' 60404 overføres fra vb.6

                sSwedish = "Rapportera avvikelser (alternativt autoångra betalning)"
                sDanish = "Rapporter afvigelse (alternativ er automatisk ophævelse af betaling)"
            Case 60405
                sNorwegian = "Funksjon"
                sEnglish = "Task"
                sSwedish = "Funksjon"
                sDanish = "Funksjon"

                ' moved here 12.08.2014, and changed number
            Case 60406
                sNorwegian = "Sjekker CR og LF"
                sEnglish = "Checking CR and LF"
                sSwedish = "Checkar CR og LF"
                sDanish = "Kontroller CR og LF"
            Case 60407
                sNorwegian = "Sjekker linjelengde"
                sEnglish = "Checking linelength"
                sSwedish = "Checkar linjelängd "
                sDanish = "Kontroller linjelængde"
            Case 60408
                sNorwegian = "Finner ikke %1 i BabelBank sin database."
                sEnglish = "%1 not found in the BabelBank database."
                sSwedish = "Hittar inte %1 i BabelBank sin databas."
                sDanish = "Finder ikke % 1 i BabelBank sin database."
            Case 60409
                sNorwegian = "Sumbeløpet på batchnivå avviker fra summen av alle betalinger."
                sEnglish = "The total of invoiceamount on payments, differs from invoiceamount on batch"
                sSwedish = "Beloppsumma på batchnivå avviker från summan av alla betalningar."
                sDanish = "Sumbeløbet på batchnivå afviger fra summen af alle betalinger."
            Case 60410
                sNorwegian = "Oppsett ledetekster"
                sEnglish = "Labels"
                sSwedish = "Oppset ledetexter"
                sDanish = "Opsæt ledetekster"
            Case 60411
                sNorwegian = "Tekst for menyvalg"
                sEnglish = "Label for menuitem"
                sSwedish = "Text för menyval"
                sDanish = "Tekst til menuvalg"
            Case 60412
                sNorwegian = "Spør etter verdi"
                sEnglish = "Ask for value"
                sSwedish = "Fråga efter värde"
                sDanish = "Spørg om værdi"
            Case 60413
                sNorwegian = "Standardverdi"
                sEnglish = "Defaultvalue"
                sSwedish = "Standardvärde"
                sDanish = "Standardværdi"
            Case 60414
                sNorwegian = "Tilbakebet."
                sEnglish = "Refund"
                sSwedish = "Återbetalning."
                sDanish = "Tilbagebetalt."
            Case 60415
                sNorwegian = "Feltnavn"
                sEnglish = "Fieldname"
                sSwedish = "Fältnamn"
                sDanish = "Feltnavn"
            Case 60416
                sNorwegian = "Verdi"
                sEnglish = "Value"
                sSwedish = "Värde"
                sDanish = "Værdi"
            Case 60417
                sNorwegian = "Kilde"
                sEnglish = "Source"
                sSwedish = "Källa"
                sDanish = "Kilde"
            Case 60418
                sNorwegian = "Posteringsnavn"
                sEnglish = "Postingname"
                sSwedish = "Posteringsnamn"
                sDanish = "Posteringsnavn"
            Case 60419
                sNorwegian = "Fast verdi"
                sEnglish = "Fixed value"
                sSwedish = "Fast värde"
                sDanish = "Fast værdi"
            Case 60420
                sNorwegian = "Fra regnskap"
                sEnglish = "From receivables"
                sSwedish = "Från redovisnings"
                sDanish = "Fra regnskab"
            Case 60421
                sNorwegian = "Fra betaling"
                sEnglish = "From payment"
                sSwedish = "Från betalning"
                sDanish = "Fra betaling"
            Case 60422
                sNorwegian = "Fra SQL"
                sEnglish = "From SQL"
                sSwedish = "Från SQL"
                sDanish = "Fra SQL"
            Case 60423
                sNorwegian = "Fra spesiell fast verdi"
                sEnglish = "From special fixed value"
                sSwedish = "Från speciell fast värde"
                sDanish = "Fra særlig fast værdi"
            Case 60424
                sNorwegian = "Ønsker du virkelig å sette alle ledetekster tilbake til standardverdier?" & vbCrLf & vbCrLf & "Du kan ikke angre på dette valget."
                sEnglish = "Do you really want to reset all labels to standard value?" & vbCrLf & vbCrLf & "You will not be able to regret the choice."
                sSwedish = "Vill du verkligen återställa alle ledetexter tilbake til standardverdier?" & vbCrLf & vbCrLf & "Du kan inte ångra detta val."
                sDanish = "Vil du virkelig ønsker at sætte alle ledetekster til deres standardværdier?" & vbCrLf & vbCrLf & "Du kan ikke fortryde dette valg."
            Case 60425
                sNorwegian = "Ønsker du å lagre høyreklikk-oppsettet"
                sEnglish = "Save RightClick-setup?"
                sSwedish = "Vill du spara högerklicka-installation"
                sDanish = "Vil du spare højre-opsætning"
            Case 60426
                sNorwegian = "Posteringslinje"
                sEnglish = "Postingline"
                sSwedish = "Posteringslinje"
                sDanish = "Posteringslinje"
            Case 60427
                sNorwegian = "Slett linje %1"
                sEnglish = "Delete line %1"
                sSwedish = "Radera linje %1"
                sDanish = "Slet linje %1"
            Case 60428
                sNorwegian = "Du må angi navn inklusive filmappe, ikke kun filnavn"
                sEnglish = "You must state the full path, not only the filename."
                sSwedish = "Du måste ange namn, inklusive filmapp, inte bara filnamn"
                sDanish = "Du skal angive navnet herunder mappe, ikke bare filnavne"
            Case 60429
                sNorwegian = "FEIL I ANGIVELSE AV FILSTI"
                sEnglish = "AN ERRONUEUS PATH STATED"
                sSwedish = "FEL I ANGIVANDE AV SÖKVÄGAR"
                sDanish = "FEJL I ANGIVELSE AF FILSTI"
            Case 60430
                sNorwegian = "Innbetalinger er IKKE avstemt."
                sEnglish = "The payment is NOT matched."
                sSwedish = "Inbetalningar er inte avstämd."
                sDanish = "Innbetalinger er IKKE avstemt."
            Case 60431
                sNorwegian = "Du benytter SQL Server. Databasen kan derfor ikke komprimeres."
                sEnglish = "You use SQL Server as database. This type of database may not be compressed."
                sSwedish = "Du använder SQL Server. Databasen kan därför inte komprimeras."
                sDanish = "Du bruger SQL Server. Databasen kan derfor ikke komprimeres."
            Case 60432
                sNorwegian = "Faktura"
                sEnglish = "Invoice"
                sSwedish = "Faktura"
                sDanish = "Faktura"
            Case 60433
                sNorwegian = "Dager søkehistorikk"
                sEnglish = "Days searchhistory"
                sSwedish = "Dagar sökhistorik"
                sDanish = "Dager søgehistorikk"
            Case 60434
                sNorwegian = "Telepay splitt Innland/Utland"
                sEnglish = "Telepay split Internat./Domestic"
                sSwedish = "Telepay split Domestic/Internat."
                sDanish = "Telepay split Indenlandsk/Internasj."
            Case 60435
                sNorwegian = "Telepay splitt mottak/avvisning"
                sEnglish = "Telepay split confirm/rejection"
                sSwedish = "Telepay split mottak/avvis"
                sDanish = "Telepay split modtag/afviste"
            Case 60436
                sNorwegian = "Telepay mottak/avvis og Inn-/Utland"
                sEnglish = "Telepay confirm/reject and Inter/Dom"
                sSwedish = "Telepay mottak/avvis och Dom/Int"
                sDanish = "Telepay modtag/afvis og Dom/Int"
            Case 60437
                sNorwegian = "Databasen er allerede konvertert til Access 2016"
                sEnglish = "The database has already been converted to Access 2016"
                sSwedish = "Databasen har redan konverterats till Access 2016"
                sDanish = "Databasen er allerede konverteret til Access 2016"
            Case 60438
                sNorwegian = "Er du sikker på at du vil oppgradere BabelBanks database til Access 2016?"
                sEnglish = "Are you sure you want to upgrade BabelBank's database to Access 2016?"
                sSwedish = "Är du säker på att du vill uppgradera BabelBanks databas till Access 2016?"
                sDanish = "Er du sikker på, at du vil opgradere BabelBanks database til Access 2016?"
            Case 60439
                sNorwegian = "Databasefilen finnes allerede!"
                sEnglish = "The database file already exists!"
                sSwedish = "Databasfilen finns redan!"
                sDanish = "Databasefilen findes allerede!"
            Case 60440
                sNorwegian = "Vellykket konvertering av databasen."
                sEnglish = "Successful conversion of database."
                sSwedish = "Lyckad konvertering av databasen."
                sDanish = "Vellykket konvertering af databasen."
            Case 60441
                sNorwegian = "For å oppgradere databasen, må du ha skrivetilgang til BabelBanks programområde,"
                sEnglish = "To upgrade the database, you must have write access to the Babel Bank program area,"
                sSwedish = "För att uppgradera databasen måste du ha skrivåtkomst till programområdet Babel Bank,"
                sDanish = "For at opgradere databasen skal du have skriveadgang til Babel Bank-programområdet,"
            Case 60442
                sNorwegian = "Har du ikke dette, kontakt din IT support, eller Visual Banking."
                sEnglish = "If you do not have write access, contact your IT support, or Visual Banking."
                sSwedish = "Har du inte skrivåtkomst, kontakta din IT-support eller Visual Banking."
                sDanish = "Har du ikke skriveadgang, skal du kontakte din IT-support eller Visual Banking."
            Case 60443
                sNorwegian = "Feil ved konvertering av Access databasen"
                sEnglish = "Error when converting the Access database!"
                sSwedish = "Fel vid konvertering av Access-databasen"
                sDanish = "Fejl ved konvertering af Access-databasen"
            Case 60444
                sNorwegian = "Kontakt Visual Banking!"
                sEnglish = "Contact Visual Banking!"
                sSwedish = "Kontakta Visual Banking!"
                sDanish = "Kontakt Visual Banking!"
            Case 60445
                sNorwegian = "Det er ikke lov å føre negative beløp som aKonto."
                sEnglish = "Negative amount is not allowed on On Account postings."
                sSwedish = "Negativa belopp som aKonto är inte tillåten."
                sDanish = "Det er ikke tilladt å postere negative beløb som aKonto."
			Case 60446
				sNorwegian = "Svaret på det endelige spørsmålet om liv, universet og alt er 42."
				sEnglish = "The answer to the ultimate question of life, the universe and everything is 42."
				sSwedish = "Svaret på den ultimata frågan om livet, universum och allt är 42."
				sDanish = "Svaret på det ultimative spørgsmål om liv, universet og alt er 42."

			Case 61000
                sNorwegian = "-----(E-mail texts)-----"
                sEnglish = "-----(E-mail texts)-----"
                sSwedish = "-----(E-mail texts)-----"
                sDanish = "-----(E-mail texts)-----"
            Case 61001
                sNorwegian = "Vennligst beskriv Babelbankproblemet:"
                sEnglish = "Please describe the Babelbankproblem:"
                sSwedish = "Beskriv BabelBank-problemet:"
                sDanish = "Beskriv Babelbankproblemet:"
            Case 61002
                sNorwegian = "Kontaktperson:"
                sEnglish = "Contactperson:"
                sSwedish = "Kontaktperson:"
                sDanish = "Kontaktperson:"
            Case 61003
                sNorwegian = "Telefonnummer:"
                sEnglish = "PhoneNo.:"
                sSwedish = "Telefonnummer:"
                sDanish = "Telefonnummer:"
            Case 61004
                sNorwegian = "Melding om åpen post i BabelBank"
                sEnglish = "Notification from BabelBank about unmatched payment"
                sSwedish = "Meddelande om öppen post i BabelBank"
                sDanish = "Meddelelse om åben post i BabelBank"
            Case 61005
                sNorwegian = "Antall dager som poster lagres"
                sEnglish = "No of days to keep records"
                sSwedish = "Antal dagar som poster lagras"
                sDanish = "Antal dage poster gemmes"
            Case 61006
                sNorwegian = "Foreslått datointervall, ant. dager tilbake"
                sEnglish = "Dateinterval, no of days from now"
                sSwedish = "Föreslaget datumintervall, ant. dagar tillbaka"
                sDanish = "Foreslået datointerval, ant. dage tilbage"
            Case 61007
                sNorwegian = "finnes fra før. Vil du legge den inn på ny?"
                sEnglish = "is present. Will you add it anyhow?"
                sSwedish = "finns redan. Vill du ange detta igen?"
                sDanish = "findes fra før. Vil du tilføje den på ny?"
            Case 61008
                sNorwegian = "Fjernede"
                sEnglish = "Removed"
                sSwedish = "Avlägsnades"
                sDanish = "Fjernede"
            Case 61009
                sNorwegian = "ULOVLIG"
                sEnglish = "ILLEGAL"
                sSwedish = "FÖRBJUDEN"
                sDanish = "ULOVLIGT"
            Case 61010
                sNorwegian = "Du kan ikke postere mot en post av denne typen."
                sEnglish = "You are not allowed to match against an item of this type."
                sSwedish = "Du kan ikke postere mot en post av denne typen."
                sDanish = "Du kan ikke skrive til en post af denne art."
            Case 61011
                sNorwegian = "Feil i føring"
                sEnglish = "Error in posting"
                sSwedish = "Fel i föring"
                sDanish = "Fejl i føring"
            Case 61012
                sNorwegian = "Du har endret i avstemmingsforslaget."
                sEnglish = "You've changed the reconciliation proposal"
                sSwedish = "Du har ändrat avstämingsförslaget"
                sDanish = "Du har ændret afstemmingsforslaget"
            Case 61013
                sNorwegian = "Hvis du ønsker å ta vare på endringen, må avstemmingsforslaget lagres eller avstemmes."
                sEnglish = "If you want to take care of the amendment, the reconciliationproposal must be saved or reconciled."
                sSwedish = "Hvis du ønsker å ta vare på ändringen, må avstemmingsforslaget lagras eller avstämmes."
                sDanish = "Om du ønsker å ta vare på ændringen, må afstemmingsforslaget gemmes eller avstemmes."
            Case 61014
                sNorwegian = "Ønsker du å gå tilbake til avstemmingsforslag?"
                sEnglish = "Want to return to the reconciliation proposal?"
                sSwedish = "Vill du återvända till avstämmingsförslaget?"
                sDanish = "Ønsker du at vende tilbage til afstemmingsforslaget?"
            Case 61015
                sNorwegian = "ENDRING I AVSTEMMINGSFORSLAG!"
                sEnglish = "CHANGE I MATCHINGPROPOSAL"
                sSwedish = "FÖRÄNDRING I AVSTÄMMINGSFÖRSLAG"
                sDanish = "ENDRING I AFSTÆMMINGSFORSLAG"
            Case 61016
                sNorwegian = "Lagre"
                sEnglish = "Save"
                sSwedish = "Spara"
                sDanish = "Gæm"
            Case 61017
                sNorwegian = "Avslutt manuell"
                sEnglish = "Exit manual"
                sSwedish = "Afslut manuell"
                sDanish = "Avslut manuell"
            Case 61018
                sNorwegian = "Fjern fil ved eksport"
                sEnglish = "Remove file at export"
                sSwedish = "Ta bort fil ved eksport"
                sDanish = "Fjern fil ved eksport"
			Case 61019
				sNorwegian = "SQL passord"
				sEnglish = "SQL password"
				sSwedish = "SQL lösenord"
				sDanish = "SQL passord"
			Case 61020
				sNorwegian = "Passord for SQL bruker i Visma database"
				sEnglish = "Password for SQL user in Visma database"
				sSwedish = "Lösenord for SQL bruker i Visma database"
				sDanish = "Passord for SQL bruker i Visma database"
			Case 61021
				sNorwegian = "SQL brukerID i Visma database"
				sEnglish = "SQL user ID in Visma database"
				sSwedish = "SQL-användar-ID i Visma-databasen"
				sDanish = "SQL-bruger-id i Visma-databasen"
			Case 61022
				sNorwegian = "BrukerID/Passord er lagret"
				sEnglish = "UserID/ Password saved"
				sSwedish = "Användar-ID/Lösenord er lagrad"
				sDanish = "Bruger-id/Passord er lagret"
			Case 61023
				sNorwegian = "Windows autentisering"
				sEnglish = "Windows authentication"
				sSwedish = "Windows-autentisering"
				sDanish = "Windows-godkendelse"
			Case 61024
				sNorwegian = "Windows autentisering lagret"
				sEnglish = "Windows authentication saved"
				sSwedish = "Windows-autentisering sparad"
				sDanish = "Windows-godkendelse lagret"
			Case 61025
				sNorwegian = "Bekreft passord"
				sEnglish = "Confirm password"
				sSwedish = "Bekräfta lösenord"
				sDanish = "Bekraft passord"
			Case 61026
				sNorwegian = "Passordene samsvarer ikke"
				sEnglish = "Passwords does not match"
				sSwedish = "Lösenord matchar inte"
				sDanish = "Passord stemmer ikke overens"
            Case 61027
                sNorwegian = "Ikke avstemte"
                sEnglish = "Not matched"
                sSwedish = "Ej avstemda"
                sDanish = "Ikke avstemte"

			Case 62000
                sNorwegian = "----(Tooltips)----"
                sEnglish = "----(Tooltips)----"
                sSwedish = "----(Tooltips)----"
                sDanish = "----(Tooltips)----"
            Case 62001
                sNorwegian = "Åpne fil"
                sEnglish = "Open file"
                sSwedish = "Öppna fil"
                sDanish = "Åbn fil"
            Case 62002
                sNorwegian = "Avslutt Babelbank"
                sEnglish = "Exit Babelbank"
                sSwedish = "Avsluta BabelBank"
                sDanish = "Afslut Babelbank"
            Case 62003
                sNorwegian = "Splitte fil(er)"
                sEnglish = "Split file(s)"
                sSwedish = "Dela fil(er)"
                sDanish = "Opdel fil(er)"
            Case 62004
                sNorwegian = "Konverter fil(er)"
                sEnglish = "Convert file(s)"
                sSwedish = "Konvertera fil(er)"
                sDanish = "Konverter fil(er)"
            Case 62005
                sNorwegian = "Slå sammen fil(er)"
                sEnglish = "Merge file(s)"
                sSwedish = "Slå ihop fil(er)"
                sDanish = "Slå fil(er) sammen"
            Case 62006
                sNorwegian = "Skriv ut filinnhold på batchnivå"
                sEnglish = "Print filecontent at batchlevel"
                sSwedish = "Skriv ut filinnehåll på batchnivå"
                sDanish = "Udskriv filindhold på batchniveau"
            Case 62007
                sNorwegian = "Skriv ut filinnhold på betalingsnivå"
                sEnglish = "Print filecontent at paymentlevel"
                sSwedish = "Skriv ut filinnehåll på betalningsnivå"
                sDanish = "Udskriv filindhold på betalingsniveau"
            Case 62008
                sNorwegian = "Skriv ut filinnhold på detaljnivå"
                sEnglish = "Print details"
                sSwedish = "Skriv ut filinnehåll på detaljnivå"
                sDanish = "Udskriv filindhold på detaljeniveau"
            Case 62009
                sNorwegian = "Vis filinnhold på skjerm"
                sEnglish = "View filecontent on screen"
                sSwedish = "Visa filinnehåll på skärmen"
                sDanish = "Vis filindhold på skærm"
            Case 62010
                sNorwegian = "Send en supportmail til Visual Banking"
                sEnglish = "Send a supportmail to Visual Banking"
                sSwedish = "Skicka e-post om support till Visual Banking"
                sDanish = "Send en supportmail til Visual Banking"
            Case 62011
                sNorwegian = "Informasjon om support"
                sEnglish = "Supportinformation"
                sSwedish = "Information om support"
                sDanish = "Information om support"
            Case 62012
                sNorwegian = "Hjelp"
                sEnglish = "Help"
                sSwedish = "Hjälp"
                sDanish = "Hjælp"
            Case 62013
                sNorwegian = "Lisensiert til"
                sEnglish = "Licensed to"
                sSwedish = "Licensierad till"
                sDanish = "Licenseret til"
            Case 62014
                sNorwegian = "Lisensnummer"
                sEnglish = "Licensenumber"
                sSwedish = "Licensnummer"
                sDanish = "Licensnummer"
            Case 62015
                sNorwegian = "Grense antall betalinger"
                sEnglish = "Licensed number of payments"
                sSwedish = "Begränsa antal betalningar"
                sDanish = "Grænse antal betalinger"
            Case 62016
                sNorwegian = "Benyttet antall betalinger (dette år)"
                sEnglish = "Used number of payments (this year)"
                sSwedish = "Använt antal betalningar (i år)"
                sDanish = "Benyttet antal betalinger (dette år)"
            Case 62017
                sNorwegian = "Kan benyttes til dato"
                sEnglish = "Expirydate"
                sSwedish = "Kan användas till datum"
                sDanish = "Kan benyttes til dato"
            Case 62018
                sNorwegian = "Åpne i &BabelBank"
                sEnglish = "Open with &BabelBank"
                sSwedish = "Öppna &BabelBank"
                sDanish = "Åbn i &BabelBank"
            Case 62019
                sNorwegian = "Høyreklikk med 'Åpne i &BabelBank' aktivert"
                sEnglish = "Rightclicking 'Open with BabelBank' activated"
                sSwedish = "Högerklick av ''Öppna i & BabelBank'' aktiverat"
                sDanish = "Højreklik med 'Åbn i &BabelBank' aktiveret"
            Case 62020
                sNorwegian = "Høyreklikk med 'Åpne i &BabelBank' DEaktivert"
                sEnglish = "Rightclicking 'Open with BabelBank' DEactivated"
                sSwedish = "Högerklick av ''Öppna i & BabelBank'' inaktiverat"
                sDanish = "Højreklik med 'Åbn i &BabelBank' deaktiveret"
            Case 62021
                sNorwegian = "Antall formater"
                sEnglish = "Licensed number of formats"
                sSwedish = "Antal format"
                sDanish = "Antal formater"
            Case 62022
                sNorwegian = "Supportavtale"
                sEnglish = "Supportcontract"
                sSwedish = "Supportavtal"
                sDanish = "Supportaftale"
            Case 62023
                sNorwegian = "Lisens låst opp!"
                sEnglish = "License unlocked!"
                sSwedish = "Licensen upplåst!"
                sDanish = "Licens låst op!"
            Case 62024
                sNorwegian = "Snarvei for %1 opprettet"
                sEnglish = "Shortcut for %1 created"
                sSwedish = "Genväg till %1 har skapats"
                sDanish = "Genvej for %1 oprettet"
            Case 62025
                sNorwegian = "Feil under opprettelse av snarvei for %1"
                sEnglish = "Creating shortcut for %1 failed"
                sSwedish = "Fel vid skapande av genväg till %1"
                sDanish = "Fejl under oprettelse af genvej for %1"
            Case 62026
                sNorwegian = "Sending av mail fullført!"
                sEnglish = "Sending of supportmail completed!"
                sSwedish = "E-postutskicket har slutförts!"
                sDanish = "Afsendelse af mail fuldført!"
            Case 62027
                sNorwegian = "Skriv ut skjermbildet"
                sEnglish = "Print screen"
                sSwedish = "Skriv ut skärmbild"
                sDanish = "Udskriv skærmbilledet"
            Case 62028
                sNorwegian = "Merk som min post"
                sEnglish = "Mark as mine"
                sSwedish = "Markera som min post"
                sDanish = "Marker som min post"
            Case 62029
                sNorwegian = "Denne kolonnen kan ikke markeres for visning!"
                sEnglish = "You are not allowed to mark this column for displaying!"
                sSwedish = "Denna kolumn kan inte markeras för visning!"
                sDanish = "Denne kolonne kan ikke markeres for visning!"
            Case 62030
                sNorwegian = "Trekk fra beløp for avstemte i samme kjøring av BabelBank"
                sEnglish = "Deduct amount for previously match items in same run of BabelBank"
                sSwedish = "Dra av belopp för avstämda i samma körning av BabelBank"
                sDanish = "Træk beløb fra for afstemte i samme kørsel af BabelBank"
            Case 62031
                sNorwegian = "Du må angi en katalog for hvor sikkerhetskopien skal ligge!"
                sEnglish = "You have to state a folder for the backup!"
                sSwedish = "Du måste ange en katalog där säkerhetskopian ska lagras!"
                sDanish = "Du skal angive et katalog for hvor sikkerhedskopien skal ligge!"
            Case 62032
                sNorwegian = "Åpner fil(er)"
                sEnglish = "Opens file for browsing"
                sSwedish = "Öppnar fil(er)"
                sDanish = "Åbner fil(er)"
            Case 62033
                sNorwegian = "Benytt opprinnelig beløp for betalinger der orignal valuta avviker fra kontoens valuta (avtal bruk med leverandør)"
                sEnglish = "Use original amount for payments where original amount's currency differs from account's currency (use of this must be agreed with vendor)."
                sSwedish = "Använd ursprungligt betalningsbelop där originalvaluta avviker från kontovaluta (avtal bruk med leverantör)"
                sDanish = "Benyt oprindeligt beløb til betalinger, hvor original valuta afviger fra kontoens valuta (aftal bruk med leverandør)"
            Case 62034
                sNorwegian = "Utbytting av info i filer"
                sEnglish = "Swap info in files"
                sSwedish = "Utväxling av information i filer"
                sDanish = "Udskiftning af info i filer"
            Case 62035
                sNorwegian = "Navn på utbyttingsregel"
                sEnglish = "Name for swapsetup"
                sSwedish = "Namn på utväxlingsregel"
                sDanish = "Navn på udskiftningsregel"
            Case 62036
                sNorwegian = "Filnavn/sti for fil med nytt innhold"
                sEnglish = "Filename/path for file with new content"
                sSwedish = "Filnamn/sökväg till fil med nytt innehåll"
                sDanish = "Filnavn/sti for fil med nyt indhold"
            Case 62037
                sNorwegian = "Format for fil med nytt innhold"
                sEnglish = "Format for file with new content"
                sSwedish = "Format på fil med nytt innehåll"
                sDanish = "Format for fil med nyt indhold"
            Case 62038
                sNorwegian = "Avvikskonto for differanse"
                sEnglish = "Account for discrepancy"
                sSwedish = "Avvikelsekonto för varians"
                sDanish = "Afvigelseskonto for difference"
            Case 62039
                sNorwegian = "Navn på SQL som skal kjøres"
                sEnglish = "Name for SQL to run"
                sSwedish = "Namn på SQL som ska köras"
                sDanish = "Navn på SQL som skal køres"
            Case 62040
                sNorwegian = "Tillat avvik"
                sEnglish = "Allow difference"
                sSwedish = "Tillåt avvikelse"
                sDanish = "Tilladt afvigelse"
            Case 62041
                sNorwegian = "Automatisk utbytting"
                sEnglish = "Automatic swapping"
                sSwedish = "Automatisk utväxling"
                sDanish = "Automatisk udskiftning"
            Case 62042
                sNorwegian = "/NavnPåProfil - Start en bestemt profil fra kommandolinje eller snarvei"
                sEnglish = "/Profilname - Start a specified profile from command or shortcut"
                sSwedish = "/NamnPåProfil - Starta en specifik profil från kommandoraden eller genväg"
                sDanish = "/NavnPåProfil - Start en bestemt profil fra kommandolinje eller genvej"
            Case 62043
                sNorwegian = "Åpne en angitt database, basert på innhold i filen BabelBank.ini"
                sEnglish = "Open a specified database based on settings in BabelBank.ini"
                sSwedish = "Öppna en angiven databas, baserad på innehållet i filen BabelBank.ini"
                sDanish = "Åbn en angivet database, baseret på indhold i filen BabelBank.ini"
            Case 62044
                sNorwegian = "Tillat at det kan åpnes flere instanser av BabelBank på samme maskin/server"
                sEnglish = "Allow more than one instance of BabelBank on a comouter or server"
                sSwedish = "Tillåt öppning av flera instanser av BabelBank på samma maskin/server"
                sDanish = "Tilladt, at der kan åbnes flere instanser af BabelBank på samme maskine/server"
            Case 62045
                sNorwegian = "Åpner BabelBank i 'debug'-modus"
                sEnglish = "Opens BabelBank in 'debug'-mode"
                sSwedish = "Öppnar BabelBank i 'debug'-läge (felsökning)"
                sDanish = "Åbner BabelBank i 'debug'-modus"
            Case 62046
                sNorwegian = "Kjør alle profiler  i BabelBank"
                sEnglish = "Run all profiles in BabelBank"
                sSwedish = "Kör alla profiler i BabelBank"
                sDanish = "Kør alle profiler  i BabelBank"
            Case 62047
                sNorwegian = "Kjør alle sende profiler i BabelBank"
                sEnglish = "Run all sendprofiles in BabelBank"
                sSwedish = "Kör alla sändningsprofiler i BabelBank"
                sDanish = "Kør alle sendeprofiler i BabelBank"
            Case 62048
                sNorwegian = "Kjør alle returprofiler i BabelBank"
                sEnglish = "Run all returnprofiles in BabelBank"
                sSwedish = "Kör alla returprofiler i BabelBank"
                sDanish = "Kør alle returprofiler i BabelBank"
            Case 62049
                sNorwegian = "Gå direkte til bilde for Manuell avstemming"
                sEnglish = "Enter Manual matching directly"
                sSwedish = "Gå direkt till bilden för manuell avstämning"
                sDanish = "Gå direkte til billedet for Manuel afstemning"
            Case 62050
                sNorwegian = "Overstyr 'lås-parameter' satt i BabelBanks oppsett"
                sEnglish = "Override the 'menulock' set in BabelBank setup"
                sSwedish = "Åsidosätt 'låsparametern' i BabelBanks inställningar"
                sDanish = "Overstyr 'lås-parameter' indstillet i BabelBanks opsætning"
            Case 62051
                sNorwegian = "Viser denne hjelpeteksten for parametre"
                sEnglish = "Shows this parameter helptext"
                sSwedish = "Visa denna hjälptext för parametrar"
                sDanish = "Viser denne hjælpetekst for parametre"
            Case 62052
                sNorwegian = "Hjelp for BabelBanks parametre"
                sEnglish = "Help for BabelBank parameters"
                sSwedish = "Hjälp med BabelBanks parametrar"
                sDanish = "Hjælp for BabelBanks parametre"
            Case 62053
                sNorwegian = "Aktiver statistikk"
                sEnglish = "Activate statistics"
                sSwedish = "Aktivera statistik"
                sDanish = "Aktiver statistik"
            Case 62054
                sNorwegian = "Gammelt passord statistikk"
                sEnglish = "Old password statistics"
                sSwedish = "Statistik för gammalt lösenord"
                sDanish = "Gammelt kodeord statistik"
            Case 62055
                sNorwegian = "Nytt passord statistikk"
                sEnglish = "New password statistics"
                sSwedish = "Statistik för nytt lösenord"
                sDanish = "Nyt kodeord statistik"
            Case 62056
                sNorwegian = "Bekreft passord statistikk"
                sEnglish = "Confirm password statistics"
                sSwedish = "Bekräfta lösenords statistik"
                sDanish = "Bekræft kodeord statistik"
            Case 62057
                sNorwegian = "Benytt 'Commit trans' ved endringer i tabellene"
                sEnglish = "Commit trans after each change in the tables"
                sSwedish = "Använd ''Commit trans'' vid ändringar i tabellerna"
                sDanish = "Benyt 'Commit trans' ved ændringer i tabellerne"
            Case 62058
                sNorwegian = "Betalers navn, adresse, postnr, sted"
                sEnglish = "Payers name, address, zip, city"
                sSwedish = "Betalarens namn, adress, postnummer, ort:"
                sDanish = "Betalers navn, adresse, postnr, by:"
            Case 62059
                sNorwegian = "(Se vedlegg for flere detaljer)"
                sEnglish = "(See attachment for more details)"
                sSwedish = "(Se bilaga för mer information)"
                sDanish = "(Se bilag for flere detaljer)"
            Case 62060
                sNorwegian = "Du har ikke lisens til å kjøre BabelBank Pro." & vbCrLf & "Vennligst kontakt Visual Banking, tlf 2318 3600, eller Support@VisualBanking.net"
                sEnglish = "You are not licensed to run BabelBank Pro." & vbCrLf & "Please contact Visual Banking, phone 2318 3600, or Support@VisualBanking.net"
                sSwedish = "Du är inte licens att köra BabelBank Pro." & vbCrLf & "Vänligen kontakt Visual Banking, tlf 2318 3600, eller Support@VisualBanking.net"
                sDanish = "Du har ikke licens til at køre BabelBank Pro." & vbCrLf & "Venligst kontakt Visual Banking, tlf 2318 3600, eller Support@VisualBanking.net"
                '----------------------------------------------
                ' NB Leave room for errormsg in BabelBank for VB6
                ' Start next on 62070
                sSwedish = "Denna e-post har skapats med BabelBank Inbetalning från Visual Bank AS"
                sDanish = "Denne mail er lavet med BabelBank Indbetaling fra Visual Banking AS"
            Case 62070
                sNorwegian = "Fjern filter"
                sEnglish = "Remove filter"
                sSwedish = "Ta bort filtret"
                sDanish = "Fjern filter"
            Case 62071
                sNorwegian = "Legg inn beløp som skal benyttes."
                sEnglish = "Enter amount."
                sSwedish = "Lägg till belopp som skall användas."
                sDanish = "Indlæg beløb der skal anvendes."
            Case 62072
                sNorwegian = "Legg inn kontonummer til mottaker."
                sEnglish = "Remove filter"
                sSwedish = "Lägg till kontonumret  för mottagaren."
                sDanish = "Angiv kontonummer på modtageren."
            Case 62073
                sNorwegian = "Legg inn melding til mottaker."
                sEnglish = "Remove filter"
                sSwedish = "Lägg till meddelande för mottagaren."
                sDanish = "Tilføj en besked til modtageren."
            Case 62074
                sNorwegian = "Vellykket tilbakekopiering av database." & vbCrLf & "Ny database er lastet inn. Trykk OK"
                sEnglish = "Succesfull restore of database." & vbCrLf & "New database is loaded. Press OK"
                sSwedish = "Vellykket tillbakakopiering av databas." & vbCrLf & "Ny databas är inläst. Tryck OK"
                sDanish = "Vellykket tilbagekopiering af database." & vbCrLf & "Ny database er indlæst. Tryk OK"
            Case 62075
                sNorwegian = "Ingen tilbakekopiering av database utført!"
                sEnglish = "No restore of database!"
                sSwedish = "Ingen tillbakakopiering av databas utförs!"
                sDanish = "Ingen tilbagekopiering af database udført!"
            Case 62076
                sNorwegian = "Denne e-Posten er laget med BabelBank Innbetaling fra Visual Banking AS"
                sEnglish = "This e-Mail is produced from BabelBank Incomings from Visual Banking AS"
                sSwedish = "E-Posten genererad av BabelBank Innbetaling från Visual Banking AS"
                sDanish = "E-Posten generert af BabelBank Innbetaling fra Visual Banking AS"
            Case 62077
                sNorwegian = "Hvordan avstemt"
                sEnglish = "How matched"
                sSwedish = "Hur avstämd"
                sDanish = "Hvordan afstemt"
            Case 62078
                sNorwegian = "Ikke tillat dobbeltkryssing av en faktura under automatisk avstemming."
                sEnglish = "Don't allow doublematching of an invoice in automatic matching."
                sSwedish = "Inte tillåtet dubbelstrykning av en faktura enligt automatisk avstämning."
                sDanish = "Ikke tilladt dobbeltafstemning af en faktura i henhold til automatisk afstemning."
            Case 62079
                sNorwegian = "Opphev alle krysninger, og legg på kommentar."
                sEnglish = "Undo all matchings, and add a comment."
                sSwedish = "Rensa alla korsningar och lägga kommentaren."
                sDanish = "Ryd alle krydsningar og tilføje kommentaren."

            Case 62082
                sNorwegian = "Eksportfil"
                sEnglish = "Exportfile"
                sSwedish = "Exportfil"
                sDanish = "Eksportfil"
            Case 62083
                sNorwegian = "Det tas backup av eksportfiler"
                sEnglish = "Backup will be created for exportfiles"
                sSwedish = "Exportfiler säkerhetskopieras"
                sDanish = "Der tages backup af eksportfiler"
            Case 62084
                sNorwegian = "Klienter"
                sEnglish = "Clients"
                sSwedish = "Klienter"
                sDanish = "Klienter"
            Case 62085
                sNorwegian = "Konti"
                sEnglish = "Accounts"
                sSwedish = "konton"
                sDanish = "Konti"
            Case 62086
                sNorwegian = "AvtaleID"
                sEnglish = "AvtaleID"
                sSwedish = "AvtalsID"
                sDanish = "AftaleID"
            Case 62087
                sNorwegian = "Hovedbok"
                sEnglish = "General ledger"
                sSwedish = "Huvudbok"
                sDanish = "Hovedbog"
            Case 62088
                sNorwegian = "Konvertert konto"
                sEnglish = "Converted account"
                sSwedish = "Konverterat konto"
                sDanish = "Konverteret konto"
            Case 62089
                sNorwegian = "Avtalegiro ID"
                sEnglish = "Avtalegiro ID"
                sSwedish = "Avtalsgiro-ID"
                sDanish = "Aftalegiro ID"
            Case 62090
                sNorwegian = "Valuta"
                sEnglish = "Currency"
                sSwedish = "Valuta"
                sDanish = "Valuta"
            Case 62091
                sNorwegian = "Landkode"
                sEnglish = "Countrycode"
                sSwedish = "Landsnummer"
                sDanish = "Landekode"
            Case 62092
                sNorwegian = "BabelBank dokumentasjon"
                sEnglish = "BabelBank documentation"
                sSwedish = "BabelBank-dokumentation"
                sDanish = "BabelBank dokumentation"
            Case 62094
                sNorwegian = "Eksport-/importformatet med ID %1 er ennå ikke implementert i BabelBank."
                sEnglish = "The export/importformat with ID %1 is not yet implemented in BabelBank."
                sSwedish = "Export-/importformatet med ID %1 är ännu inte implementerat i BabelBank."
                sDanish = "Eksport-/importformatet med ID %1 er endnu ikke implementeret i BabelBank."
            Case 62095
                sNorwegian = "Bruk returfiler"
                sEnglish = "Use returnfiles"
                sSwedish = "Använd återrapporteringsfiler"
                sDanish = "Benyt returfiler"
            Case 62096
                sNorwegian = "Vellykket backup av databasen!"
                sEnglish = "Succesfull backup of the database!"
                sSwedish = "Vellykket backup av databas!"
                sDanish = "Vellykket backup af database!"
            Case 62097
                sNorwegian = "Henter ut lagrede betalingsdata på paymentnivå."
                sEnglish = "Retrieving stored data at paymentlevel."
                sSwedish = "Hämtning av lagrade betalningsdata på paymentlevel."
                sDanish = "Hentning af lagrede betalinsdata på paymentlevel."
            Case 62098
                sNorwegian = "Henter ut lagrede betalingsdata på batchnivå."
                sEnglish = "Retrieving stored data at batchlevel."
                sSwedish = "Hämtning av lagrade betalningsdata på batchlevel."
                sDanish = "Hentning af lagrede betalinsdata på batchlevel."
            Case 62097
                sNorwegian = "Henter ut lagrede betalingsdata på babelfilenivå."
                sEnglish = "Retrieving stored data at babelfilelevel."
                sSwedish = "Hämtning av lagrade betalningsdata på babelfilelevel."
                sDanish = "Hentning af lagrede betalinsdata på babelfilelevel."
            Case 62098
                sNorwegian = "Feil i kontrollsum ved splitting av KID-betalinger. Betalingsbeløp %1"
                sEnglish = "Error in checksum when splitting KID payments. Paymentamount %1"
                sSwedish = "Fel i kontrollsumma ved uppdelning KID betalningar. Betalningsbelopp %1"
                sDanish = "Fejl i kontrolsum ved opdeling av KID betalinger. Betalingsbekløp %1"
            Case 62099
                sNorwegian = "Henter ut lagrede betalingsdata på BabelFilenivå"
                sEnglish = "Retrieving stored paymentdata on BabelFilelevel"
                sSwedish = "Hämtar lagrade betalningsuppgifter på Babelfilenivå"
                sDanish = "Finner lagrede betalingsdata på BabelFilenivå"
            Case 62100
                sNorwegian = "Support for profil"
                sEnglish = "Support for profile"
                sSwedish = "Support för profil"
                sDanish = "Support for profil"
            Case 62101
                sNorwegian = "Finner ikke <Content>-taggen i filen " & vbCrLf & "%1." & vbCrLf & "Dette er kanskje ikke en SecureEnvelope fil?"
                sEnglish = "Can't find the <Content>-tag in the file " & vbCrLf & "%1." & vbCrLf & "This is probably not a SecureEnvelope file?"
                sSwedish = "Kan inte hitta taggen <Content> i filen " & vbCrLf & "%1." & vbCrLf & "Det här är kanskje inte en SecureEnvelope fil?"
                sDanish = "Kan ikke finde <Content> tag i filen " & vbCrLf & "%1." & vbCrLf & "Det er kanskje ikke en SecureEnvelope fil?"
            Case 62102
                sNorwegian = "Vellykket utpakking av fil(er). " & vbCrLf & vbCrLf & "Filene er lagret i mappen %1."
                sEnglish = "Successful extraction of file (s)." & vbCrLf & vbCrLf & "The files are stored in the folder% 1."
                sSwedish = "Lyckad uppackning av fil(er)." & vbCrLf & vbCrLf & "Filerna er lagrad i mappen% 1."
                sDanish = "Succesfuld udtrækning af fil(er)." & vbCrLf & vbCrLf & "Filerne er gemt i mappen% 1."
            Case 62103
                sNorwegian = "Vellykket anonymisering av fil(er). " & vbCrLf & vbCrLf & "Filene er lagret i mappen %1."
                sEnglish = "Successful anonymization of file (s)." & vbCrLf & vbCrLf & "The files are stored in the folder% 1."
                sSwedish = "Lyckad anonymisering av fil(er)." & vbCrLf & vbCrLf & "Filerna er lagrad i mappen% 1."
                sDanish = "Succesfuld anonymisering af fil(er)." & vbCrLf & vbCrLf & "Filerne er gemt i mappen% 1."
        End Select


        If bUseDanish Then
            sReturnString = sDanish
        ElseIf bUseSwedish Then
            sReturnString = sSwedish
        ElseIf bUseNorwegian Then
            sReturnString = sNorwegian
        Else
            sReturnString = sEnglish
        End If
        ' If no hit, then use English;
        If EmptyString(sReturnString) Then
            sReturnString = sEnglish
        End If
        ' If still not found
        If EmptyString(sReturnString) Then
            sReturnString = StringIndex & " languagestring is missing."
        End If

        sReturnString = Replace(sReturnString, "%1", Trim$(sString1))
        sReturnString = Replace(sReturnString, "%2", Trim$(sString2))
        sReturnString = Replace(sReturnString, "%3", Trim$(sString3))

        LRS = sReturnString

    End Function

End Module

