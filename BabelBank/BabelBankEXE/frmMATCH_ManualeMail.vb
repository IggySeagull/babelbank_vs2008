Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic

Friend Class frmMATCH_ManualeMail
	Inherits System.Windows.Forms.Form
	
	Private Const SRCCOPY As Integer = &HCC0020
	Private Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Integer, ByVal x As Integer, ByVal y As Integer, ByVal nWidth As Integer, ByVal nHeight As Integer, ByVal hSrcDC As Integer, ByVal xSrc As Integer, ByVal ySrc As Integer, ByVal dwRop As Integer) As Integer
	
    Dim oProfile As vbBabel.Profile
    Dim sClientname As String = ""
    Dim sAttachment As String = "" 'Added 19.04.2022 to use a unique filename for the attachment
    Dim frmMatch_Manual As frmMATCH_Manual
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Close()
    End Sub
    Public Sub Passfrm(ByVal frm As frmMATCH_Manual)
        ' send frmMatch_Manual to frmMatch_Manualemal
        frmMatch_Manual = frm
    End Sub
	
	Private Sub frmMATCH_ManualeMail_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim bDummy1 As Boolean = False
        Dim sDummy6 As String = ""
        Dim sDummy2 As String = ""
        Dim sDummy99 As String = ""
        Dim sDummy3 As String = ""
        Dim bDummy4 As Boolean = False
        Dim bDummy9 As Boolean = False
        Dim bDummy7 As Boolean = False
        Dim bDummy5 As Boolean = False
        Dim bDummy8 As Boolean = False
        Dim bDummy10 As Boolean = False
        Dim seMailSubject As String = ""
        Dim seMailAddress As String = ""
        Dim beMailAttachment As Boolean = False

        ' Must be done before we show eMail form
        FormvbStyle(Me, "dollar.jpg")
        FormLRSCaptions(Me)

        ' Return saved setupvalues from database:
        RestoreManualSetup(bDummy1, sDummy2, sDummy6, bDummy5, bDummy7, bDummy8, sDummy3, bDummy4, bDummy9, bDummy10, sDummy99, seMailSubject, seMailAddress, beMailAttachment)

        Me.txtSubject.Text = seMailSubject
        Me.txteMail.Text = seMailAddress
        Me.chkAttachment.CheckState = bVal(beMailAttachment)

        'If oProfile.EmailSMTP Then
        'Me.cmdOutlookAddressBook.Visible = False
        'End If

        ' Find possible eMailAddresses from Notification and fill Me.lsteMails
        RestoreNotifications()

        FormSelectAllTextboxs(Me) ' Set selection for all textboxes
    End Sub
	Private Sub RestoreNotifications()
		' Connect to database
		' Find emailAddresses from Notifications
		' Fill lsteMails
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sCompany As String
		
        Try
            sCompany = "1" 'TODO - Hardcoded CompanyID
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ' Fill the reader with all mailaddresses
            sMySQL = "Select eMail_Adress FROM Notification WHERE Company_ID = " & sCompany
            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then

                Me.lsteMails.Items.Clear()

                Do While oMyDal.Reader_ReadRecord

                    Me.lsteMails.Items.Add(oMyDal.Reader_GetString("eMail_Adress"))

                Loop

            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

            If Me.lsteMails.Items.Count = 0 Then
                Me.lsteMails.Visible = False
            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: RestoreNotifications" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

    End Sub
	'Private Sub cmdOutlookAddressBook_Click()
	'' Find eMail addresses from Outlook
	'' Only when set as MAPI-mail
	'Dim olApp As Outlook.Application
	'Dim Contacts As Outlook.MAPIFolder
	'Dim Contact As Outlook.ContactItem
	'Dim i As Integer
	'
	'
	'If oProfile.EmailSMTP = "False" Then
	'    ' New 05.04.06
	'    ' Find eMail-adresses from Outlook;
    '    Me.txteMail.Text = ""
    '
    '    Set olApp = New Outlook.Application
    '    Set Contacts = olApp.GetNamespace("MAPI").GetDefaultFolder(olFolderContacts)
    '    For i = 1 To Contacts.Items.Count
    '      If TypeOf Contacts.Items.Item(i) Is Outlook.ContactItem Then
    '        Set Contact = Contacts.Items.Item(i)
    '        If Not EmptyString(Contact.Email1Address) Then
    '            Me.lsteMails.AddItem Contact.Email1Address
    '        End If
    '      End If
    '    Next
    'End If
    '
    'If Me.lsteMails.ListCount > 0 Then
    '    Me.lsteMails.Visible = True
    'End If
    '
    'Set olApp = Nothing
    'Set Contacts = Nothing
    'Set Contact = Nothing
    '
    'End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Dim seMailAddress As String


        ' Check if mailaddresses are selected in listbox;
        'For i = 0 To lsteMails.ListCount - 1
        '    If lsteMails.Selected(i) Then
        '        seMailAddress = seMailAddress & ";" & lsteMails.List(i)
        '    End If
        'Next i
        ' remove first ;
        'If Len(seMailAddress) > 2 Then
        '    seMailAddress = Mid$(seMailAddress, 2)
        '    SaveManualSetup Me.txtSubject.Text, Left$(seMailAddress, 150), CBool(Me.chkAttachment.Value)
        'Else
        '    ' no addresses from lstemails, try txteMail
        seMailAddress = Me.txteMail.Text
        ' take away special chars
        Me.txtSubject.Text = Replace(Replace(Replace(Me.txtSubject.Text, Chr(9), ""), Chr(13), ""), Chr(10), "")

        SaveManualSetupMail((Me.txtSubject.Text), VB.Left(Me.txteMail.Text, 150), CBool(Me.chkAttachment.CheckState))
        'End If

        SendMatchMail((Me.txtSubject).Text, seMailAddress)
        Me.Close()
    End Sub
    Public Sub SaveManualSetupMail(ByRef seMailSubject As String, ByRef seMailAddress As String, ByRef beMailAttachment As Boolean)
        ' Save values to database
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True

        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL = "UPDATE MATCH_Manual SET eMailSubject = "
            sMySQL = sMySQL & "'" & seMailSubject & "'"
            sMySQL = sMySQL & " WHERE Company_ID = 1"
            oMyDal.SQL = sMySQL
            If oMyDal.ExecuteNonQuery Then

            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If

            sMySQL = "UPDATE MATCH_Manual SET eMailAddress = "
            sMySQL = sMySQL & "'" & seMailAddress & "'"
            sMySQL = sMySQL & " WHERE Company_ID = 1"
            oMyDal.SQL = sMySQL
            If oMyDal.ExecuteNonQuery Then

            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If

            sMySQL = "UPDATE MATCH_Manual SET eMailAttachment = "
            sMySQL = sMySQL & "" & beMailAttachment & ""
            sMySQL = sMySQL & " WHERE Company_ID = 1"
            oMyDal.SQL = sMySQL
            If oMyDal.ExecuteNonQuery Then

            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: frmMatch_ManualeMail:SaveManualSetupMail" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

    End Sub
    Private Sub SendMatchMail(ByRef sSubject As String, ByRef seMailAddress As String)

        Dim myMail As vbBabel.vbMail
        Dim sMsgNote As String = ""
        Dim sText As String = ""
        Dim sErrDescription As String = ""
        Dim iContinue As Integer = 0
        Dim sTmp As String = ""

        myMail = New vbBabel.vbMail
        ' Compose message

        ' add client to Subject;
        sSubject = sSubject & " " & LRS(60026) & sClientname

        sMsgNote = ""
        'New 28.11.2008
        If frmMATCH_Manual.lblVoucherNo.Visible = True Then
            sMsgNote = sMsgNote & frmMATCH_Manual.lblVoucherNo.Text & vbCrLf & vbCrLf
        End If
        'sMsgNote = sMsgNote & "Betalers navn, adresse, postnr, sted:" & vbCrLf
        sMsgNote = sMsgNote & LRS(62058) & vbCrLf
        sMsgNote = sMsgNote & "----------------------------------------" & vbCrLf
        sMsgNote = sMsgNote & frmMATCH_Manual.lblPayerName.Text & vbCrLf
        sMsgNote = sMsgNote & frmMATCH_Manual.lblPayersAdr1.Text & vbCrLf
        sMsgNote = sMsgNote & frmMATCH_Manual.lblPayersAdr2.Text & vbCrLf
        sMsgNote = sMsgNote & frmMATCH_Manual.lblPayersAdr3.Text & vbCrLf
        sMsgNote = sMsgNote & "----------------------------------------" & vbCrLf
        sMsgNote = sMsgNote & vbCrLf
        sMsgNote = sMsgNote & LRS(60162) & ": " & frmMATCH_Manual.lblDate.Text & vbCrLf   ' Dato
        sMsgNote = sMsgNote & LRS(60163) & ": " & frmMATCH_Manual.lblToAccount.Text & vbCrLf   ' Til konto
        sMsgNote = sMsgNote & LRS(60086) & ": " & frmMATCH_Manual.lblAmount.Text & vbCrLf   'Bel�p
        sMsgNote = sMsgNote & LRS(60093) & ": " & Format(frmMATCH_Manual.RestAmount, "## ##0.00") & vbCrLf  'RestBel�p
        sMsgNote = sMsgNote & LRS(60390) & ": " & frmMATCH_Manual.lblCurrency.Text & vbCrLf  'Valuta
        sMsgNote = sMsgNote & vbCrLf
        ' Freetext
        'sMsgNote = sMsgNote & "Bel�pet gjelder" & vbCrLf
        sMsgNote = sMsgNote & LRS(60074) & vbCrLf
        sMsgNote = sMsgNote & "----------------------------------------" & vbCrLf
        sText = frmMATCH_Manual.txtNotification.Text
        Do While Len(sText) > 0
            sMsgNote = sMsgNote & VB.Left(sText, 40)
            sText = Mid(sText, 41)
        Loop
        If Me.chkAttachment.CheckState = 1 Then
            'sMsgNote = sMsgNote & vbCrLf & "(Se vedlegg for flere detaljer)" & vbCrLf
            sMsgNote = sMsgNote & vbCrLf & LRS(62059) & vbCrLf
        End If
        sMsgNote = sMsgNote & vbCrLf
        'sMsgNote = sMsgNote & "Denne mailen er laget med BabelBank Innbetaling fra Visual Banking AS" & vbCrLf
        sMsgNote = sMsgNote & LRS(62076) & vbCrLf

        If oProfile.EmailSMTP Then
            myMail.EMail_SMTP = True
            On Error GoTo errSMTP
        Else
            myMail.EMail_SMTP = False  ' MAPI
            On Error GoTo errMAPI
        End If

        ' Use new class/dll vbSendmail
        '-----------------------------
        On Error GoTo errSMTP
        If oProfile.EmailSMTP And Len(Trim(oProfile.EmailSMTPHost)) > 0 Then
            myMail.EMail_SMTP = True
            myMail.eMailSMTPHost = oProfile.EmailSMTPHost
        End If
        myMail.eMailSender = oProfile.EmailSender
        myMail.eMailSenderDisplayName = oProfile.EmailDisplayName
        myMail.AddReceivers(Trim(seMailAddress))

        myMail.Subject = sSubject
        'myMail.AddAttachment(Application.StartupPath & "\tmp.jpg")
        ' 08.09.2015 - if we have no accessrights to the folder where BabelBank.exe is installed,
        ' we will not be able to create a tmpfile in this folder
        ' Use profilepath instead!

        ' added next IF 21.10.2015
        If Me.chkAttachment.CheckState = 1 Then


            'OLD CODE PRIOR TO 19.04.2022
            ''29.09.2017 - Added next IF
            'If BB_UseSQLServer() Then

            '    'MsgBox("Skal hente ut temp path i ny2222 form.")

            '    sTmp = System.IO.Path.GetTempPath()
            'Else
            '    sTmp = BB_DatabasePath()
            '    ' remove name of database
            '    sTmp = VB.Left(sTmp, InStrRev(sTmp, "\") - 1)
            '    'myMail.AddAttachment(Application.StartupPath & "\tmp.jpg")
            '    ' 14.10.2015 - missed something 08.09.2015, need to use sTmp in the below line !!!!!
            'End If
            'myMail.AddAttachment(sTmp & "\tmp.jpg")

            'Added 19.04.2022 to use a unique filename for the attachment
            myMail.AddAttachment(sAttachment)
        End If

        myMail.Body = sMsgNote
        myMail.Send()
        If myMail.Success Then
            MsgBox(LRS(62026), MsgBoxStyle.OkOnly + MsgBoxStyle.Information)
        End If
        myMail = Nothing



        Exit Sub

errSMTP:
        myMail = Nothing
        sErrDescription = Err.Description
        Err.Raise(15011, , sErrDescription)

        Exit Sub

errMAPI:
        ' Test errors:
        'Cancel of Mail-session gives Error.!
        If Err.Number <> 32001 Then ' User cancelled process
            sErrDescription = Err.Description
            Err.Raise(15011, , sErrDescription)
        End If

    End Sub
    Public Sub SetProfile(ByRef oP As vbBabel.Profile)
        oProfile = oP
    End Sub
    Public Sub setClientName(ByVal sName As String)
        sClientname = sName
    End Sub
    Public Sub setAttachment(ByVal sTmp As String)
        'Added 19.04.2022 to use a unique filename for the attachment
        sAttachment = sTmp
    End Sub
    'UPGRADE_WARNING: Event lsteMails.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub lsteMails_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lsteMails.SelectedIndexChanged
        ' If selected, move eMail to txteMail
        If Me.lsteMails.GetItemChecked(Me.lsteMails.SelectedIndex) Then
            'Me.txteMail.Text = Me.txteMail.Text & ";" & VB6.GetItemString(Me.lsteMails, Me.lsteMails.SelectedIndex)
            ' 16.07.2019
            Me.txteMail.Text = Me.txteMail.Text & ";" & Me.lsteMails.Text
        Else
            ' Remove from txt
            'Me.txteMail.Text = Replace(Me.txteMail.Text, VB6.GetItemString(Me.lsteMails, Me.lsteMails.SelectedIndex), "")
            ' 16.07.2019
            Me.txteMail.Text = Replace(Me.txteMail.Text, Me.lsteMails.Text, "")
            ' remove ; or ;;
            Me.txteMail.Text = Replace(Me.txteMail.Text, ";;", ";")
        End If
        ' Remove possible leading or trailing ;
        If VB.Left(Me.txteMail.Text, 1) = ";" Then
            Me.txteMail.Text = Mid(Me.txteMail.Text, 2)
        End If
        If VB.Right(Me.txteMail.Text, 1) = ";" Then
            Me.txteMail.Text = VB.Left(Me.txteMail.Text, Len(Me.txteMail.Text) - 1)
        End If


    End Sub

End Class
