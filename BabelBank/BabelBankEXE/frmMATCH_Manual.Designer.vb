<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMATCH_Manual
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents mnuSendeMail As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents File As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MnuMatch As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuUndoLast As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuDeleteLine As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSep504 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuCoordinateORandBBDB As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuChangeReceivedAmount As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Action As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFirst As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuPrevious As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuNext As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuLast As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuGoToLastMatched As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuNullifyMatching As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSep505 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuNothing As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFilterAll As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFilterMine As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFilterAnonymous As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSep505_2 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuInvoiceMode As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuPaymentMode As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents ViewMenu As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFindAmount As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFindCustomer As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFindInvoice As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSep51400 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuSeekFree1 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSeekFree2 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSeekFree3 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSeekFree4 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSeekFree5 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSeekFree6 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSeekFree7 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents FindMenu As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuPrintThisItem As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSep50600 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents Print_Renamed As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSetupManual As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents Setup As System.Windows.Forms.ToolStripMenuItem
    'Public WithEvents mnuAddRTVPost As System.Windows.Forms.ToolStripMenuItem
    'Public WithEvents mnuDiscountAmountsprERP As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuGuleSider As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuTelefonkatalogen As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuRosaIndex As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuBrReg As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuRightClickName As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
	Public WithEvents chkSpecialMark As System.Windows.Forms.CheckBox
    Public WithEvents txtLBLSGCustomerNo As System.Windows.Forms.TextBox
	Public WithEvents txtSGCustomerNo As System.Windows.Forms.TextBox
	Public WithEvents chkSGRegion As System.Windows.Forms.CheckBox
    Public WithEvents PictureeMail As System.Windows.Forms.PictureBox
    Public WithEvents lblSeekHeading As System.Windows.Forms.Label
	Public WithEvents frameSeekResult As System.Windows.Forms.Panel
	Public WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Public WithEvents ImageList1 As System.Windows.Forms.ImageList
	Public WithEvents cmdNRXNameAddress As System.Windows.Forms.Button
	Public WithEvents cmdImportFile As System.Windows.Forms.Button
	Public WithEvents txtNotification As System.Windows.Forms.RichTextBox
	Public WithEvents lblNRXType As System.Windows.Forms.Label
	Public WithEvents lblPayerName As System.Windows.Forms.Label
	Public WithEvents lblPayersAdr1 As System.Windows.Forms.Label
	Public WithEvents lblPayersAdr2 As System.Windows.Forms.Label
	Public WithEvents lblPayersAdr3 As System.Windows.Forms.Label
	Public WithEvents framePayer As System.Windows.Forms.GroupBox
	Public WithEvents cmbSISMOSetCode As System.Windows.Forms.ComboBox
	Public WithEvents lblReceiverName As System.Windows.Forms.Label
	Public WithEvents lblReceiverAdr1 As System.Windows.Forms.Label
	Public WithEvents lblReceiverAdr2 As System.Windows.Forms.Label
	Public WithEvents lblReceiverAdr3 As System.Windows.Forms.Label
	Public WithEvents frameReceiver As System.Windows.Forms.GroupBox
	Public WithEvents imgMinus As System.Windows.Forms.PictureBox
	Public WithEvents imgPlus As System.Windows.Forms.PictureBox
	Public WithEvents lblKID As System.Windows.Forms.Label
	Public WithEvents lblHeadKID As System.Windows.Forms.Label
	Public WithEvents lblVoucherNo As System.Windows.Forms.Label
	Public WithEvents lblOriginalAmount As System.Windows.Forms.Label
	Public WithEvents lblHeadOriginalAmount As System.Windows.Forms.Label
	Public WithEvents imgOmitCoveringPayment As System.Windows.Forms.PictureBox
	Public WithEvents lblFromAccount As System.Windows.Forms.Label
	Public WithEvents lblHeadFromAccount As System.Windows.Forms.Label
    Public WithEvents lblHeadCurrency As System.Windows.Forms.Label
	Public WithEvents lblRef3 As System.Windows.Forms.Label
    Public WithEvents lblHeadAmount As System.Windows.Forms.Label
	Public WithEvents lblHeadRef1 As System.Windows.Forms.Label
	Public WithEvents lblHeadRef2 As System.Windows.Forms.Label
	Public WithEvents lblHeadRef3 As System.Windows.Forms.Label
	Public WithEvents lblHeadToAccount As System.Windows.Forms.Label
	Public WithEvents lblHeadDate As System.Windows.Forms.Label
	Public WithEvents lblNotifyGiroMail As System.Windows.Forms.Label
	Public WithEvents lblNoOfInvoices As System.Windows.Forms.Label
	Public WithEvents lblIncomingHeading As System.Windows.Forms.Label
	Public WithEvents lblNotification As System.Windows.Forms.Label
	Public WithEvents ImgMatched As System.Windows.Forms.PictureBox
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents frameMATCH_Incoming As System.Windows.Forms.Panel
    Public WithEvents lblHowmatched As System.Windows.Forms.Label
	Public WithEvents lblSelectedAmount As System.Windows.Forms.Label
	Public WithEvents lblMatchedHeading As System.Windows.Forms.Label
    Public WithEvents frame_Matched As System.Windows.Forms.Panel
    Public WithEvents ToolbarMatchManual As System.Windows.Forms.ToolStrip
    Public WithEvents imgGoToLastMatched As System.Windows.Forms.PictureBox
    Public WithEvents imgFind7 As System.Windows.Forms.PictureBox
    Public WithEvents imgFind6 As System.Windows.Forms.PictureBox
    Public WithEvents imgFind5 As System.Windows.Forms.PictureBox
    Public WithEvents imgFind4 As System.Windows.Forms.PictureBox
    Public WithEvents ImgContinue As System.Windows.Forms.PictureBox
    Public WithEvents imgSave As System.Windows.Forms.PictureBox
    Public WithEvents imgLockUser As System.Windows.Forms.PictureBox
    Public WithEvents imgOmit As System.Windows.Forms.PictureBox
    Public WithEvents imgeMail As System.Windows.Forms.PictureBox
    Public WithEvents imgPrintForm As System.Windows.Forms.PictureBox
    Public WithEvents imgClose As System.Windows.Forms.PictureBox
    Public WithEvents imgFind3 As System.Windows.Forms.PictureBox
    Public WithEvents imgFind2 As System.Windows.Forms.PictureBox
    Public WithEvents imgFind1 As System.Windows.Forms.PictureBox
    Public WithEvents imgFindInvoice As System.Windows.Forms.PictureBox
    Public WithEvents imgFindCustomer As System.Windows.Forms.PictureBox
    Public WithEvents imgFindAmount As System.Windows.Forms.PictureBox
    Public WithEvents imgFront As System.Windows.Forms.PictureBox
    Public WithEvents imgDelete As System.Windows.Forms.PictureBox
    Public WithEvents imgUndo As System.Windows.Forms.PictureBox
    Public WithEvents imgTick As System.Windows.Forms.PictureBox
    Public WithEvents imgLast As System.Windows.Forms.PictureBox
    Public WithEvents imgNext As System.Windows.Forms.PictureBox
    Public WithEvents imgPrevious As System.Windows.Forms.PictureBox
    Public WithEvents imgFirst As System.Windows.Forms.PictureBox
    Public WithEvents lblSeek As System.Windows.Forms.Label
    Public WithEvents lblProgress As System.Windows.Forms.Label
    Public WithEvents lblError As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMATCH_Manual))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MenuStrip
        Me.File = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSendeMail = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem
        Me.Action = New System.Windows.Forms.ToolStripMenuItem
        Me.MnuMatch = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUndoLast = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDeleteLine = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSep504 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuCoordinateORandBBDB = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuChangeReceivedAmount = New System.Windows.Forms.ToolStripMenuItem
        Me.ViewMenu = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFirst = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrevious = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNext = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuLast = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGoToLastMatched = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNullifyMatching = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSep505 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuNothing = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFilterAll = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFilterMine = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFilterAnonymous = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFilterSpecial = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFilterProposed = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSep505_2 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuInvoiceMode = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPaymentMode = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNote = New System.Windows.Forms.ToolStripMenuItem
        Me.FindMenu = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFindAmount = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFindCustomer = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFindInvoice = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSep51400 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuSeekFree1 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSeekFree2 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSeekFree3 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSeekFree4 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSeekFree5 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSeekFree6 = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSeekFree7 = New System.Windows.Forms.ToolStripMenuItem
        Me.Print_Renamed = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrintThisItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSep50600 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuPrintUnmatched = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrintOmitted = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrintSpecial = New System.Windows.Forms.ToolStripMenuItem
        Me.Setup = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSetupManual = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRightClickName = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGuleSider = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuTelefonkatalogen = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRosaIndex = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBrReg = New System.Windows.Forms.ToolStripMenuItem
        Me.chkSpecialMark = New System.Windows.Forms.CheckBox
        Me.txtLBLSGCustomerNo = New System.Windows.Forms.TextBox
        Me.txtSGCustomerNo = New System.Windows.Forms.TextBox
        Me.chkSGRegion = New System.Windows.Forms.CheckBox
        Me.PictureeMail = New System.Windows.Forms.PictureBox
        Me.frameSeekResult = New System.Windows.Forms.Panel
        Me.gridERP = New System.Windows.Forms.DataGridView
        Me.lblSeekHeading = New System.Windows.Forms.Label
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.frameMATCH_Incoming = New System.Windows.Forms.Panel
        Me.Frame2 = New System.Windows.Forms.GroupBox
        Me.txtNotificationForPrint = New System.Windows.Forms.TextBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.lblRef2 = New System.Windows.Forms.Label
        Me.lblRef1 = New System.Windows.Forms.Label
        Me.lblAmount = New System.Windows.Forms.Label
        Me.lblToAccount = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblHeadDate = New System.Windows.Forms.Label
        Me.cmdNRXNameAddress = New System.Windows.Forms.Button
        Me.cmdImportFile = New System.Windows.Forms.Button
        Me.txtNotification = New System.Windows.Forms.RichTextBox
        Me.framePayer = New System.Windows.Forms.GroupBox
        Me.lblPayersCC = New System.Windows.Forms.Label
        Me.lblNRXType = New System.Windows.Forms.Label
        Me.lblPayerName = New System.Windows.Forms.Label
        Me.lblPayersAdr1 = New System.Windows.Forms.Label
        Me.lblPayersAdr2 = New System.Windows.Forms.Label
        Me.lblPayersAdr3 = New System.Windows.Forms.Label
        Me.frameReceiver = New System.Windows.Forms.GroupBox
        Me.cmbSISMOSetCode = New System.Windows.Forms.ComboBox
        Me.lblReceiverName = New System.Windows.Forms.Label
        Me.lblReceiverAdr1 = New System.Windows.Forms.Label
        Me.lblReceiverAdr2 = New System.Windows.Forms.Label
        Me.lblReceiverAdr3 = New System.Windows.Forms.Label
        Me.imgMinus = New System.Windows.Forms.PictureBox
        Me.imgPlus = New System.Windows.Forms.PictureBox
        Me.lblKID = New System.Windows.Forms.Label
        Me.lblHeadKID = New System.Windows.Forms.Label
        Me.lblVoucherNo = New System.Windows.Forms.Label
        Me.lblOriginalAmount = New System.Windows.Forms.Label
        Me.lblHeadOriginalAmount = New System.Windows.Forms.Label
        Me.imgOmitCoveringPayment = New System.Windows.Forms.PictureBox
        Me.lblFromAccount = New System.Windows.Forms.Label
        Me.lblHeadFromAccount = New System.Windows.Forms.Label
        Me.lblHeadCurrency = New System.Windows.Forms.Label
        Me.lblRef3 = New System.Windows.Forms.Label
        Me.lblHeadAmount = New System.Windows.Forms.Label
        Me.lblHeadRef1 = New System.Windows.Forms.Label
        Me.lblHeadRef2 = New System.Windows.Forms.Label
        Me.lblHeadRef3 = New System.Windows.Forms.Label
        Me.lblHeadToAccount = New System.Windows.Forms.Label
        Me.lblNotifyGiroMail = New System.Windows.Forms.Label
        Me.lblNoOfInvoices = New System.Windows.Forms.Label
        Me.lblIncomingHeading = New System.Windows.Forms.Label
        Me.lblNotification = New System.Windows.Forms.Label
        Me.ImgMatched = New System.Windows.Forms.PictureBox
        Me.frame_Matched = New System.Windows.Forms.Panel
        Me.lblHowmatched = New System.Windows.Forms.Label
        Me.lblSelectedAmount = New System.Windows.Forms.Label
        Me.lblMatchedHeading = New System.Windows.Forms.Label
        Me.ToolbarMatchManual = New System.Windows.Forms.ToolStrip
        Me.imgGoToLastMatched = New System.Windows.Forms.PictureBox
        Me.imgFind7 = New System.Windows.Forms.PictureBox
        Me.imgFind6 = New System.Windows.Forms.PictureBox
        Me.imgFind5 = New System.Windows.Forms.PictureBox
        Me.imgFind4 = New System.Windows.Forms.PictureBox
        Me.ImgContinue = New System.Windows.Forms.PictureBox
        Me.imgSave = New System.Windows.Forms.PictureBox
        Me.imgLockUser = New System.Windows.Forms.PictureBox
        Me.imgOmit = New System.Windows.Forms.PictureBox
        Me.imgeMail = New System.Windows.Forms.PictureBox
        Me.imgPrintForm = New System.Windows.Forms.PictureBox
        Me.imgClose = New System.Windows.Forms.PictureBox
        Me.imgFind3 = New System.Windows.Forms.PictureBox
        Me.imgFind2 = New System.Windows.Forms.PictureBox
        Me.imgFind1 = New System.Windows.Forms.PictureBox
        Me.imgFindInvoice = New System.Windows.Forms.PictureBox
        Me.imgFindCustomer = New System.Windows.Forms.PictureBox
        Me.imgFindAmount = New System.Windows.Forms.PictureBox
        Me.imgFront = New System.Windows.Forms.PictureBox
        Me.imgDelete = New System.Windows.Forms.PictureBox
        Me.imgUndo = New System.Windows.Forms.PictureBox
        Me.imgTick = New System.Windows.Forms.PictureBox
        Me.imgLast = New System.Windows.Forms.PictureBox
        Me.imgNext = New System.Windows.Forms.PictureBox
        Me.imgPrevious = New System.Windows.Forms.PictureBox
        Me.imgFirst = New System.Windows.Forms.PictureBox
        Me.lblSeek = New System.Windows.Forms.Label
        Me.lblProgress = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.cmdDemo = New System.Windows.Forms.Button
        Me.txtDemoText = New System.Windows.Forms.TextBox
        Me.PictureDemoExit = New System.Windows.Forms.PictureBox
        Me.PictureFilterOn = New System.Windows.Forms.PictureBox
        Me.PictureFilterOff = New System.Windows.Forms.PictureBox
        Me.txtNote = New System.Windows.Forms.RichTextBox
        Me.imgYellowPin = New System.Windows.Forms.PictureBox
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.imgRed = New System.Windows.Forms.PictureBox
        Me.imgGreen = New System.Windows.Forms.PictureBox
        Me.imgBlue = New System.Windows.Forms.PictureBox
        Me.imgYellow = New System.Windows.Forms.PictureBox
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.chkERPAll = New System.Windows.Forms.CheckBox
        Me.MainMenu1.SuspendLayout()
        CType(Me.PictureeMail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.frameSeekResult.SuspendLayout()
        CType(Me.gridERP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.frameMATCH_Incoming.SuspendLayout()
        Me.Frame2.SuspendLayout()
        Me.framePayer.SuspendLayout()
        Me.frameReceiver.SuspendLayout()
        CType(Me.imgMinus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgPlus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgOmitCoveringPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImgMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.frame_Matched.SuspendLayout()
        CType(Me.imgGoToLastMatched, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFind7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFind6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFind5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFind4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImgContinue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgSave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgLockUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgOmit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgeMail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgPrintForm, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFind3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFind2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFind1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFindInvoice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFindCustomer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFindAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFront, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgDelete, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgUndo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgLast, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgNext, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgPrevious, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFirst, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureDemoExit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureFilterOn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureFilterOff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgYellowPin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgRed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgGreen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgBlue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgYellow, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.File, Me.Action, Me.ViewMenu, Me.FindMenu, Me.Print_Renamed, Me.Setup, Me.mnuRightClickName})
        Me.MainMenu1.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu1.Name = "MainMenu1"
        Me.MainMenu1.Size = New System.Drawing.Size(888, 24)
        Me.MainMenu1.TabIndex = 92
        '
        'File
        '
        Me.File.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSendeMail, Me.mnuExit})
        Me.File.Name = "File"
        Me.File.Size = New System.Drawing.Size(49, 20)
        Me.File.Text = "50100"
        '
        'mnuSendeMail
        '
        Me.mnuSendeMail.Name = "mnuSendeMail"
        Me.mnuSendeMail.Size = New System.Drawing.Size(104, 22)
        Me.mnuSendeMail.Text = "50112"
        '
        'mnuExit
        '
        Me.mnuExit.Name = "mnuExit"
        Me.mnuExit.Size = New System.Drawing.Size(104, 22)
        Me.mnuExit.Text = "50110"
        '
        'Action
        '
        Me.Action.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MnuMatch, Me.mnuUndoLast, Me.mnuDeleteLine, Me.mnuSep504, Me.mnuCoordinateORandBBDB, Me.mnuChangeReceivedAmount})
        Me.Action.Name = "Action"
        Me.Action.Size = New System.Drawing.Size(49, 20)
        Me.Action.Text = "50400"
        '
        'MnuMatch
        '
        Me.MnuMatch.Name = "MnuMatch"
        Me.MnuMatch.ShortcutKeys = System.Windows.Forms.Keys.F12
        Me.MnuMatch.Size = New System.Drawing.Size(147, 22)
        Me.MnuMatch.Text = "50410"
        '
        'mnuUndoLast
        '
        Me.mnuUndoLast.Name = "mnuUndoLast"
        Me.mnuUndoLast.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.mnuUndoLast.Size = New System.Drawing.Size(147, 22)
        Me.mnuUndoLast.Text = "50411"
        '
        'mnuDeleteLine
        '
        Me.mnuDeleteLine.Name = "mnuDeleteLine"
        Me.mnuDeleteLine.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.L), System.Windows.Forms.Keys)
        Me.mnuDeleteLine.Size = New System.Drawing.Size(147, 22)
        Me.mnuDeleteLine.Text = "50413"
        '
        'mnuSep504
        '
        Me.mnuSep504.Name = "mnuSep504"
        Me.mnuSep504.Size = New System.Drawing.Size(144, 6)
        '
        'mnuCoordinateORandBBDB
        '
        Me.mnuCoordinateORandBBDB.Enabled = False
        Me.mnuCoordinateORandBBDB.Name = "mnuCoordinateORandBBDB"
        Me.mnuCoordinateORandBBDB.Size = New System.Drawing.Size(147, 22)
        Me.mnuCoordinateORandBBDB.Text = "50415"
        '
        'mnuChangeReceivedAmount
        '
        Me.mnuChangeReceivedAmount.Enabled = False
        Me.mnuChangeReceivedAmount.Name = "mnuChangeReceivedAmount"
        Me.mnuChangeReceivedAmount.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.mnuChangeReceivedAmount.Size = New System.Drawing.Size(147, 22)
        Me.mnuChangeReceivedAmount.Text = "50416"
        '
        'ViewMenu
        '
        Me.ViewMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFirst, Me.mnuPrevious, Me.mnuNext, Me.mnuLast, Me.mnuGoToLastMatched, Me.mnuNullifyMatching, Me.mnuSep505, Me.mnuNothing, Me.mnuFilterAll, Me.mnuFilterMine, Me.mnuFilterAnonymous, Me.mnuFilterSpecial, Me.mnuFilterProposed, Me.mnuSep505_2, Me.mnuInvoiceMode, Me.mnuPaymentMode, Me.mnuNote})
        Me.ViewMenu.Name = "ViewMenu"
        Me.ViewMenu.Size = New System.Drawing.Size(49, 20)
        Me.ViewMenu.Text = "50500"
        '
        'mnuFirst
        '
        Me.mnuFirst.Name = "mnuFirst"
        Me.mnuFirst.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.mnuFirst.Size = New System.Drawing.Size(179, 22)
        Me.mnuFirst.Text = "50510"
        '
        'mnuPrevious
        '
        Me.mnuPrevious.Name = "mnuPrevious"
        Me.mnuPrevious.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.mnuPrevious.Size = New System.Drawing.Size(179, 22)
        Me.mnuPrevious.Text = "50511"
        '
        'mnuNext
        '
        Me.mnuNext.Name = "mnuNext"
        Me.mnuNext.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.mnuNext.Size = New System.Drawing.Size(179, 22)
        Me.mnuNext.Text = "50512"
        '
        'mnuLast
        '
        Me.mnuLast.Name = "mnuLast"
        Me.mnuLast.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me.mnuLast.Size = New System.Drawing.Size(179, 22)
        Me.mnuLast.Text = "50513"
        '
        'mnuGoToLastMatched
        '
        Me.mnuGoToLastMatched.Name = "mnuGoToLastMatched"
        Me.mnuGoToLastMatched.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuGoToLastMatched.Size = New System.Drawing.Size(179, 22)
        Me.mnuGoToLastMatched.Text = "50520"
        '
        'mnuNullifyMatching
        '
        Me.mnuNullifyMatching.Name = "mnuNullifyMatching"
        Me.mnuNullifyMatching.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.mnuNullifyMatching.Size = New System.Drawing.Size(179, 22)
        Me.mnuNullifyMatching.Text = "50414"
        '
        'mnuSep505
        '
        Me.mnuSep505.Name = "mnuSep505"
        Me.mnuSep505.Size = New System.Drawing.Size(176, 6)
        '
        'mnuNothing
        '
        Me.mnuNothing.Name = "mnuNothing"
        Me.mnuNothing.Size = New System.Drawing.Size(179, 22)
        Me.mnuNothing.Text = "50519"
        '
        'mnuFilterAll
        '
        Me.mnuFilterAll.Name = "mnuFilterAll"
        Me.mnuFilterAll.Size = New System.Drawing.Size(179, 22)
        Me.mnuFilterAll.Text = "50516"
        '
        'mnuFilterMine
        '
        Me.mnuFilterMine.Name = "mnuFilterMine"
        Me.mnuFilterMine.Size = New System.Drawing.Size(179, 22)
        Me.mnuFilterMine.Text = "50517"
        '
        'mnuFilterAnonymous
        '
        Me.mnuFilterAnonymous.Checked = True
        Me.mnuFilterAnonymous.CheckState = System.Windows.Forms.CheckState.Checked
        Me.mnuFilterAnonymous.Name = "mnuFilterAnonymous"
        Me.mnuFilterAnonymous.Size = New System.Drawing.Size(179, 22)
        Me.mnuFilterAnonymous.Text = "50518"
        '
        'mnuFilterSpecial
        '
        Me.mnuFilterSpecial.Name = "mnuFilterSpecial"
        Me.mnuFilterSpecial.Size = New System.Drawing.Size(179, 22)
        Me.mnuFilterSpecial.Text = "50521"
        '
        'mnuFilterProposed
        '
        Me.mnuFilterProposed.Name = "mnuFilterProposed"
        Me.mnuFilterProposed.Size = New System.Drawing.Size(179, 22)
        Me.mnuFilterProposed.Text = "50525"
        '
        'mnuSep505_2
        '
        Me.mnuSep505_2.Name = "mnuSep505_2"
        Me.mnuSep505_2.Size = New System.Drawing.Size(176, 6)
        '
        'mnuInvoiceMode
        '
        Me.mnuInvoiceMode.Name = "mnuInvoiceMode"
        Me.mnuInvoiceMode.Size = New System.Drawing.Size(179, 22)
        Me.mnuInvoiceMode.Text = "50514"
        '
        'mnuPaymentMode
        '
        Me.mnuPaymentMode.Name = "mnuPaymentMode"
        Me.mnuPaymentMode.Size = New System.Drawing.Size(179, 22)
        Me.mnuPaymentMode.Text = "50515"
        '
        'mnuNote
        '
        Me.mnuNote.Name = "mnuNote"
        Me.mnuNote.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
                    Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.mnuNote.Size = New System.Drawing.Size(179, 22)
        Me.mnuNote.Text = "50524"
        '
        'FindMenu
        '
        Me.FindMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFindAmount, Me.mnuFindCustomer, Me.mnuFindInvoice, Me.mnuSep51400, Me.mnuSeekFree1, Me.mnuSeekFree2, Me.mnuSeekFree3, Me.mnuSeekFree4, Me.mnuSeekFree5, Me.mnuSeekFree6, Me.mnuSeekFree7})
        Me.FindMenu.Name = "FindMenu"
        Me.FindMenu.Size = New System.Drawing.Size(49, 20)
        Me.FindMenu.Text = "51400"
        '
        'mnuFindAmount
        '
        Me.mnuFindAmount.Name = "mnuFindAmount"
        Me.mnuFindAmount.ShortcutKeys = System.Windows.Forms.Keys.F1
        Me.mnuFindAmount.Size = New System.Drawing.Size(171, 22)
        Me.mnuFindAmount.Text = "Find Amount"
        '
        'mnuFindCustomer
        '
        Me.mnuFindCustomer.Name = "mnuFindCustomer"
        Me.mnuFindCustomer.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.mnuFindCustomer.Size = New System.Drawing.Size(171, 22)
        Me.mnuFindCustomer.Text = "Find Customer"
        '
        'mnuFindInvoice
        '
        Me.mnuFindInvoice.Name = "mnuFindInvoice"
        Me.mnuFindInvoice.ShortcutKeys = System.Windows.Forms.Keys.F3
        Me.mnuFindInvoice.Size = New System.Drawing.Size(171, 22)
        Me.mnuFindInvoice.Text = "Find Invoice"
        '
        'mnuSep51400
        '
        Me.mnuSep51400.Name = "mnuSep51400"
        Me.mnuSep51400.Size = New System.Drawing.Size(168, 6)
        '
        'mnuSeekFree1
        '
        Me.mnuSeekFree1.Name = "mnuSeekFree1"
        Me.mnuSeekFree1.ShortcutKeys = System.Windows.Forms.Keys.F4
        Me.mnuSeekFree1.Size = New System.Drawing.Size(171, 22)
        Me.mnuSeekFree1.Text = "FindFree1"
        '
        'mnuSeekFree2
        '
        Me.mnuSeekFree2.Name = "mnuSeekFree2"
        Me.mnuSeekFree2.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.mnuSeekFree2.Size = New System.Drawing.Size(171, 22)
        Me.mnuSeekFree2.Text = "FindFree2"
        '
        'mnuSeekFree3
        '
        Me.mnuSeekFree3.Name = "mnuSeekFree3"
        Me.mnuSeekFree3.ShortcutKeys = System.Windows.Forms.Keys.F6
        Me.mnuSeekFree3.Size = New System.Drawing.Size(171, 22)
        Me.mnuSeekFree3.Text = "FindFree3"
        '
        'mnuSeekFree4
        '
        Me.mnuSeekFree4.Name = "mnuSeekFree4"
        Me.mnuSeekFree4.ShortcutKeys = System.Windows.Forms.Keys.F7
        Me.mnuSeekFree4.Size = New System.Drawing.Size(171, 22)
        Me.mnuSeekFree4.Text = "FindFree4"
        '
        'mnuSeekFree5
        '
        Me.mnuSeekFree5.Name = "mnuSeekFree5"
        Me.mnuSeekFree5.ShortcutKeys = System.Windows.Forms.Keys.F8
        Me.mnuSeekFree5.Size = New System.Drawing.Size(171, 22)
        Me.mnuSeekFree5.Text = "FindFree5"
        '
        'mnuSeekFree6
        '
        Me.mnuSeekFree6.Name = "mnuSeekFree6"
        Me.mnuSeekFree6.ShortcutKeys = System.Windows.Forms.Keys.F9
        Me.mnuSeekFree6.Size = New System.Drawing.Size(171, 22)
        Me.mnuSeekFree6.Text = "FindFree6"
        '
        'mnuSeekFree7
        '
        Me.mnuSeekFree7.Name = "mnuSeekFree7"
        Me.mnuSeekFree7.ShortcutKeys = System.Windows.Forms.Keys.F10
        Me.mnuSeekFree7.Size = New System.Drawing.Size(171, 22)
        Me.mnuSeekFree7.Text = "FindFree7"
        '
        'Print_Renamed
        '
        Me.Print_Renamed.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrintThisItem, Me.mnuSep50600, Me.mnuPrintUnmatched, Me.mnuPrintOmitted, Me.mnuPrintSpecial})
        Me.Print_Renamed.Name = "Print_Renamed"
        Me.Print_Renamed.Size = New System.Drawing.Size(49, 20)
        Me.Print_Renamed.Text = "50600"
        '
        'mnuPrintThisItem
        '
        Me.mnuPrintThisItem.Name = "mnuPrintThisItem"
        Me.mnuPrintThisItem.Size = New System.Drawing.Size(181, 22)
        Me.mnuPrintThisItem.Text = "50607"
        '
        'mnuSep50600
        '
        Me.mnuSep50600.Name = "mnuSep50600"
        Me.mnuSep50600.Size = New System.Drawing.Size(178, 6)
        '
        'mnuPrintUnmatched
        '
        Me.mnuPrintUnmatched.Name = "mnuPrintUnmatched"
        Me.mnuPrintUnmatched.Size = New System.Drawing.Size(181, 22)
        Me.mnuPrintUnmatched.Text = "50610"
        '
        'mnuPrintOmitted
        '
        Me.mnuPrintOmitted.Name = "mnuPrintOmitted"
        Me.mnuPrintOmitted.Size = New System.Drawing.Size(181, 22)
        Me.mnuPrintOmitted.Text = "50611"
        '
        'mnuPrintSpecial
        '
        Me.mnuPrintSpecial.Name = "mnuPrintSpecial"
        Me.mnuPrintSpecial.Size = New System.Drawing.Size(181, 22)
        Me.mnuPrintSpecial.Text = "ToolStripMenuItem1"
        Me.mnuPrintSpecial.Visible = False
        '
        'Setup
        '
        Me.Setup.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetupManual})
        Me.Setup.Name = "Setup"
        Me.Setup.Size = New System.Drawing.Size(49, 20)
        Me.Setup.Text = "50800"
        '
        'mnuSetupManual
        '
        Me.mnuSetupManual.Name = "mnuSetupManual"
        Me.mnuSetupManual.Size = New System.Drawing.Size(104, 22)
        Me.mnuSetupManual.Text = "51800"
        '
        'mnuRightClickName
        '
        Me.mnuRightClickName.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuGuleSider, Me.mnuTelefonkatalogen, Me.mnuRosaIndex, Me.mnuBrReg})
        Me.mnuRightClickName.Name = "mnuRightClickName"
        Me.mnuRightClickName.Size = New System.Drawing.Size(105, 20)
        Me.mnuRightClickName.Text = "RightClickName"
        Me.mnuRightClickName.Visible = False
        '
        'mnuGuleSider
        '
        Me.mnuGuleSider.Name = "mnuGuleSider"
        Me.mnuGuleSider.Size = New System.Drawing.Size(164, 22)
        Me.mnuGuleSider.Text = "GuleSider"
        '
        'mnuTelefonkatalogen
        '
        Me.mnuTelefonkatalogen.Name = "mnuTelefonkatalogen"
        Me.mnuTelefonkatalogen.Size = New System.Drawing.Size(164, 22)
        Me.mnuTelefonkatalogen.Text = "Telefonkatalogen"
        '
        'mnuRosaIndex
        '
        Me.mnuRosaIndex.Name = "mnuRosaIndex"
        Me.mnuRosaIndex.Size = New System.Drawing.Size(164, 22)
        Me.mnuRosaIndex.Text = "RosaIndex"
        '
        'mnuBrReg
        '
        Me.mnuBrReg.Name = "mnuBrReg"
        Me.mnuBrReg.Size = New System.Drawing.Size(164, 22)
        Me.mnuBrReg.Text = "Br�nn�ysund"
        '
        'chkSpecialMark
        '
        Me.chkSpecialMark.BackColor = System.Drawing.SystemColors.Window
        Me.chkSpecialMark.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkSpecialMark.Enabled = False
        Me.chkSpecialMark.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkSpecialMark.Location = New System.Drawing.Point(674, 445)
        Me.chkSpecialMark.Name = "chkSpecialMark"
        Me.chkSpecialMark.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkSpecialMark.Size = New System.Drawing.Size(155, 18)
        Me.chkSpecialMark.TabIndex = 64
        Me.chkSpecialMark.Text = "SpecialMark"
        Me.chkSpecialMark.UseVisualStyleBackColor = False
        Me.chkSpecialMark.Visible = False
        '
        'txtLBLSGCustomerNo
        '
        Me.txtLBLSGCustomerNo.AcceptsReturn = True
        Me.txtLBLSGCustomerNo.BackColor = System.Drawing.SystemColors.Control
        Me.txtLBLSGCustomerNo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtLBLSGCustomerNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLBLSGCustomerNo.Enabled = False
        Me.txtLBLSGCustomerNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtLBLSGCustomerNo.Location = New System.Drawing.Point(680, 360)
        Me.txtLBLSGCustomerNo.MaxLength = 0
        Me.txtLBLSGCustomerNo.Name = "txtLBLSGCustomerNo"
        Me.txtLBLSGCustomerNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtLBLSGCustomerNo.Size = New System.Drawing.Size(64, 13)
        Me.txtLBLSGCustomerNo.TabIndex = 61
        Me.txtLBLSGCustomerNo.TabStop = False
        Me.txtLBLSGCustomerNo.Text = "Filter klientnr.:"
        Me.txtLBLSGCustomerNo.Visible = False
        '
        'txtSGCustomerNo
        '
        Me.txtSGCustomerNo.AcceptsReturn = True
        Me.txtSGCustomerNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtSGCustomerNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSGCustomerNo.Enabled = False
        Me.txtSGCustomerNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSGCustomerNo.Location = New System.Drawing.Point(682, 408)
        Me.txtSGCustomerNo.MaxLength = 0
        Me.txtSGCustomerNo.Name = "txtSGCustomerNo"
        Me.txtSGCustomerNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSGCustomerNo.Size = New System.Drawing.Size(105, 20)
        Me.txtSGCustomerNo.TabIndex = 60
        Me.txtSGCustomerNo.Visible = False
        '
        'chkSGRegion
        '
        Me.chkSGRegion.BackColor = System.Drawing.SystemColors.Control
        Me.chkSGRegion.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkSGRegion.Enabled = False
        Me.chkSGRegion.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkSGRegion.Location = New System.Drawing.Point(688, 384)
        Me.chkSGRegion.Name = "chkSGRegion"
        Me.chkSGRegion.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkSGRegion.Size = New System.Drawing.Size(78, 17)
        Me.chkSGRegion.TabIndex = 59
        Me.chkSGRegion.Text = "Alle regioner"
        Me.chkSGRegion.UseVisualStyleBackColor = False
        Me.chkSGRegion.Visible = False
        '
        'PictureeMail
        '
        Me.PictureeMail.BackColor = System.Drawing.SystemColors.Control
        Me.PictureeMail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureeMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureeMail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PictureeMail.Location = New System.Drawing.Point(612, 255)
        Me.PictureeMail.Name = "PictureeMail"
        Me.PictureeMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PictureeMail.Size = New System.Drawing.Size(32, 29)
        Me.PictureeMail.TabIndex = 48
        Me.PictureeMail.TabStop = False
        '
        'frameSeekResult
        '
        Me.frameSeekResult.BackColor = System.Drawing.SystemColors.Window
        Me.frameSeekResult.Controls.Add(Me.gridERP)
        Me.frameSeekResult.Controls.Add(Me.lblSeekHeading)
        Me.frameSeekResult.Cursor = System.Windows.Forms.Cursors.Default
        Me.frameSeekResult.ForeColor = System.Drawing.SystemColors.WindowText
        Me.frameSeekResult.Location = New System.Drawing.Point(5, 406)
        Me.frameSeekResult.Name = "frameSeekResult"
        Me.frameSeekResult.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.frameSeekResult.Size = New System.Drawing.Size(303, 121)
        Me.frameSeekResult.TabIndex = 21
        Me.frameSeekResult.Visible = False
        '
        'gridERP
        '
        Me.gridERP.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridERP.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.gridERP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridERP.DefaultCellStyle = DataGridViewCellStyle2
        Me.gridERP.Location = New System.Drawing.Point(16, 17)
        Me.gridERP.Name = "gridERP"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gridERP.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.gridERP.Size = New System.Drawing.Size(284, 104)
        Me.gridERP.TabIndex = 23
        '
        'lblSeekHeading
        '
        Me.lblSeekHeading.BackColor = System.Drawing.SystemColors.Window
        Me.lblSeekHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSeekHeading.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeekHeading.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblSeekHeading.Location = New System.Drawing.Point(10, 0)
        Me.lblSeekHeading.Name = "lblSeekHeading"
        Me.lblSeekHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSeekHeading.Size = New System.Drawing.Size(240, 26)
        Me.lblSeekHeading.TabIndex = 22
        Me.lblSeekHeading.Text = "60088 - Svar fra ERP-system"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(240, 553)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(277, 20)
        Me.ProgressBar1.TabIndex = 44
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        '
        'frameMATCH_Incoming
        '
        Me.frameMATCH_Incoming.BackColor = System.Drawing.SystemColors.Window
        Me.frameMATCH_Incoming.Controls.Add(Me.Frame2)
        Me.frameMATCH_Incoming.Cursor = System.Windows.Forms.Cursors.Default
        Me.frameMATCH_Incoming.ForeColor = System.Drawing.SystemColors.WindowText
        Me.frameMATCH_Incoming.Location = New System.Drawing.Point(6, 74)
        Me.frameMATCH_Incoming.Name = "frameMATCH_Incoming"
        Me.frameMATCH_Incoming.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.frameMATCH_Incoming.Size = New System.Drawing.Size(554, 330)
        Me.frameMATCH_Incoming.TabIndex = 7
        '
        'Frame2
        '
        Me.Frame2.BackColor = System.Drawing.SystemColors.Window
        Me.Frame2.Controls.Add(Me.txtNotificationForPrint)
        Me.Frame2.Controls.Add(Me.lblCurrency)
        Me.Frame2.Controls.Add(Me.lblRef2)
        Me.Frame2.Controls.Add(Me.lblRef1)
        Me.Frame2.Controls.Add(Me.lblAmount)
        Me.Frame2.Controls.Add(Me.lblToAccount)
        Me.Frame2.Controls.Add(Me.lblDate)
        Me.Frame2.Controls.Add(Me.lblHeadDate)
        Me.Frame2.Controls.Add(Me.cmdNRXNameAddress)
        Me.Frame2.Controls.Add(Me.cmdImportFile)
        Me.Frame2.Controls.Add(Me.txtNotification)
        Me.Frame2.Controls.Add(Me.framePayer)
        Me.Frame2.Controls.Add(Me.frameReceiver)
        Me.Frame2.Controls.Add(Me.imgMinus)
        Me.Frame2.Controls.Add(Me.imgPlus)
        Me.Frame2.Controls.Add(Me.lblKID)
        Me.Frame2.Controls.Add(Me.lblHeadKID)
        Me.Frame2.Controls.Add(Me.lblVoucherNo)
        Me.Frame2.Controls.Add(Me.lblOriginalAmount)
        Me.Frame2.Controls.Add(Me.lblHeadOriginalAmount)
        Me.Frame2.Controls.Add(Me.imgOmitCoveringPayment)
        Me.Frame2.Controls.Add(Me.lblFromAccount)
        Me.Frame2.Controls.Add(Me.lblHeadFromAccount)
        Me.Frame2.Controls.Add(Me.lblHeadCurrency)
        Me.Frame2.Controls.Add(Me.lblRef3)
        Me.Frame2.Controls.Add(Me.lblHeadAmount)
        Me.Frame2.Controls.Add(Me.lblHeadRef1)
        Me.Frame2.Controls.Add(Me.lblHeadRef2)
        Me.Frame2.Controls.Add(Me.lblHeadRef3)
        Me.Frame2.Controls.Add(Me.lblHeadToAccount)
        Me.Frame2.Controls.Add(Me.lblNotifyGiroMail)
        Me.Frame2.Controls.Add(Me.lblNoOfInvoices)
        Me.Frame2.Controls.Add(Me.lblIncomingHeading)
        Me.Frame2.Controls.Add(Me.lblNotification)
        Me.Frame2.Controls.Add(Me.ImgMatched)
        Me.Frame2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.Frame2.Location = New System.Drawing.Point(8, 8)
        Me.Frame2.Name = "Frame2"
        Me.Frame2.Padding = New System.Windows.Forms.Padding(0)
        Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame2.Size = New System.Drawing.Size(540, 316)
        Me.Frame2.TabIndex = 8
        Me.Frame2.TabStop = False
        '
        'txtNotificationForPrint
        '
        Me.txtNotificationForPrint.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNotificationForPrint.Enabled = False
        Me.txtNotificationForPrint.Location = New System.Drawing.Point(214, 207)
        Me.txtNotificationForPrint.Multiline = True
        Me.txtNotificationForPrint.Name = "txtNotificationForPrint"
        Me.txtNotificationForPrint.Size = New System.Drawing.Size(46, 20)
        Me.txtNotificationForPrint.TabIndex = 72
        Me.txtNotificationForPrint.Visible = False
        '
        'lblCurrency
        '
        Me.lblCurrency.BackColor = System.Drawing.SystemColors.Window
        Me.lblCurrency.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCurrency.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblCurrency.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblCurrency.Location = New System.Drawing.Point(404, 175)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCurrency.Size = New System.Drawing.Size(111, 15)
        Me.lblCurrency.TabIndex = 71
        '
        'lblRef2
        '
        Me.lblRef2.BackColor = System.Drawing.SystemColors.Window
        Me.lblRef2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRef2.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblRef2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblRef2.Location = New System.Drawing.Point(404, 207)
        Me.lblRef2.Name = "lblRef2"
        Me.lblRef2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRef2.Size = New System.Drawing.Size(115, 30)
        Me.lblRef2.TabIndex = 70
        '
        'lblRef1
        '
        Me.lblRef1.BackColor = System.Drawing.SystemColors.Window
        Me.lblRef1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRef1.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblRef1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblRef1.Location = New System.Drawing.Point(404, 191)
        Me.lblRef1.Name = "lblRef1"
        Me.lblRef1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRef1.Size = New System.Drawing.Size(115, 15)
        Me.lblRef1.TabIndex = 69
        '
        'lblAmount
        '
        Me.lblAmount.BackColor = System.Drawing.SystemColors.Window
        Me.lblAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAmount.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblAmount.Location = New System.Drawing.Point(404, 158)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAmount.Size = New System.Drawing.Size(111, 15)
        Me.lblAmount.TabIndex = 68
        '
        'lblToAccount
        '
        Me.lblToAccount.BackColor = System.Drawing.SystemColors.Window
        Me.lblToAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblToAccount.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblToAccount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblToAccount.Location = New System.Drawing.Point(404, 141)
        Me.lblToAccount.Name = "lblToAccount"
        Me.lblToAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblToAccount.Size = New System.Drawing.Size(111, 15)
        Me.lblToAccount.TabIndex = 67
        '
        'lblDate
        '
        Me.lblDate.BackColor = System.Drawing.SystemColors.Window
        Me.lblDate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDate.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblDate.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblDate.Location = New System.Drawing.Point(404, 127)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDate.Size = New System.Drawing.Size(111, 17)
        Me.lblDate.TabIndex = 66
        '
        'lblHeadDate
        '
        Me.lblHeadDate.BackColor = System.Drawing.SystemColors.Window
        Me.lblHeadDate.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeadDate.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblHeadDate.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblHeadDate.Location = New System.Drawing.Point(313, 128)
        Me.lblHeadDate.Name = "lblHeadDate"
        Me.lblHeadDate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeadDate.Size = New System.Drawing.Size(90, 15)
        Me.lblHeadDate.TabIndex = 28
        Me.lblHeadDate.Text = "60077 - DATO:"
        '
        'cmdNRXNameAddress
        '
        Me.cmdNRXNameAddress.BackColor = System.Drawing.SystemColors.Control
        Me.cmdNRXNameAddress.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdNRXNameAddress.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNRXNameAddress.Location = New System.Drawing.Point(456, 32)
        Me.cmdNRXNameAddress.Name = "cmdNRXNameAddress"
        Me.cmdNRXNameAddress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdNRXNameAddress.Size = New System.Drawing.Size(73, 17)
        Me.cmdNRXNameAddress.TabIndex = 53
        Me.cmdNRXNameAddress.Text = "Endre"
        Me.cmdNRXNameAddress.UseVisualStyleBackColor = False
        Me.cmdNRXNameAddress.Visible = False
        '
        'cmdImportFile
        '
        Me.cmdImportFile.BackColor = System.Drawing.SystemColors.Control
        Me.cmdImportFile.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdImportFile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdImportFile.Location = New System.Drawing.Point(456, 8)
        Me.cmdImportFile.Name = "cmdImportFile"
        Me.cmdImportFile.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdImportFile.Size = New System.Drawing.Size(73, 17)
        Me.cmdImportFile.TabIndex = 50
        Me.cmdImportFile.Text = "Les inn fil"
        Me.cmdImportFile.UseVisualStyleBackColor = False
        Me.cmdImportFile.Visible = False
        '
        'txtNotification
        '
        Me.txtNotification.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNotification.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotification.Location = New System.Drawing.Point(4, 128)
        Me.txtNotification.Name = "txtNotification"
        Me.txtNotification.ReadOnly = True
        Me.txtNotification.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.txtNotification.Size = New System.Drawing.Size(308, 186)
        Me.txtNotification.TabIndex = 3
        Me.txtNotification.TabStop = False
        Me.txtNotification.Text = ""
        '
        'framePayer
        '
        Me.framePayer.BackColor = System.Drawing.SystemColors.Window
        Me.framePayer.Controls.Add(Me.lblPayersCC)
        Me.framePayer.Controls.Add(Me.lblNRXType)
        Me.framePayer.Controls.Add(Me.lblPayerName)
        Me.framePayer.Controls.Add(Me.lblPayersAdr1)
        Me.framePayer.Controls.Add(Me.lblPayersAdr2)
        Me.framePayer.Controls.Add(Me.lblPayersAdr3)
        Me.framePayer.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.framePayer.ForeColor = System.Drawing.SystemColors.WindowText
        Me.framePayer.Location = New System.Drawing.Point(5, 32)
        Me.framePayer.Name = "framePayer"
        Me.framePayer.Padding = New System.Windows.Forms.Padding(0)
        Me.framePayer.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.framePayer.Size = New System.Drawing.Size(260, 81)
        Me.framePayer.TabIndex = 14
        Me.framePayer.TabStop = False
        Me.framePayer.Text = "60076 - BETALERS NAVN, ADRESSE, POSTNR, STED"
        '
        'lblPayersCC
        '
        Me.lblPayersCC.BackColor = System.Drawing.SystemColors.Window
        Me.lblPayersCC.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayersCC.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayersCC.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPayersCC.Location = New System.Drawing.Point(232, 60)
        Me.lblPayersCC.Name = "lblPayersCC"
        Me.lblPayersCC.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayersCC.Size = New System.Drawing.Size(24, 13)
        Me.lblPayersCC.TabIndex = 55
        Me.lblPayersCC.Text = "CC"
        '
        'lblNRXType
        '
        Me.lblNRXType.BackColor = System.Drawing.SystemColors.Window
        Me.lblNRXType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNRXType.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNRXType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblNRXType.Location = New System.Drawing.Point(208, 11)
        Me.lblNRXType.Name = "lblNRXType"
        Me.lblNRXType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNRXType.Size = New System.Drawing.Size(47, 13)
        Me.lblNRXType.TabIndex = 54
        Me.lblNRXType.Text = "NRXType"
        Me.lblNRXType.Visible = False
        '
        'lblPayerName
        '
        Me.lblPayerName.BackColor = System.Drawing.SystemColors.Window
        Me.lblPayerName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayerName.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayerName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPayerName.Location = New System.Drawing.Point(5, 11)
        Me.lblPayerName.Name = "lblPayerName"
        Me.lblPayerName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayerName.Size = New System.Drawing.Size(247, 13)
        Me.lblPayerName.TabIndex = 18
        Me.lblPayerName.Text = "Payers name"
        '
        'lblPayersAdr1
        '
        Me.lblPayersAdr1.BackColor = System.Drawing.SystemColors.Window
        Me.lblPayersAdr1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayersAdr1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayersAdr1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPayersAdr1.Location = New System.Drawing.Point(5, 27)
        Me.lblPayersAdr1.Name = "lblPayersAdr1"
        Me.lblPayersAdr1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayersAdr1.Size = New System.Drawing.Size(247, 14)
        Me.lblPayersAdr1.TabIndex = 17
        Me.lblPayersAdr1.Text = "Payers adr1"
        '
        'lblPayersAdr2
        '
        Me.lblPayersAdr2.BackColor = System.Drawing.SystemColors.Window
        Me.lblPayersAdr2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayersAdr2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayersAdr2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPayersAdr2.Location = New System.Drawing.Point(5, 44)
        Me.lblPayersAdr2.Name = "lblPayersAdr2"
        Me.lblPayersAdr2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayersAdr2.Size = New System.Drawing.Size(247, 13)
        Me.lblPayersAdr2.TabIndex = 16
        Me.lblPayersAdr2.Text = "Payers adr2"
        '
        'lblPayersAdr3
        '
        Me.lblPayersAdr3.BackColor = System.Drawing.SystemColors.Window
        Me.lblPayersAdr3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPayersAdr3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayersAdr3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblPayersAdr3.Location = New System.Drawing.Point(5, 60)
        Me.lblPayersAdr3.Name = "lblPayersAdr3"
        Me.lblPayersAdr3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPayersAdr3.Size = New System.Drawing.Size(225, 13)
        Me.lblPayersAdr3.TabIndex = 15
        Me.lblPayersAdr3.Text = "Payers adr3"
        '
        'frameReceiver
        '
        Me.frameReceiver.BackColor = System.Drawing.SystemColors.Window
        Me.frameReceiver.Controls.Add(Me.cmbSISMOSetCode)
        Me.frameReceiver.Controls.Add(Me.lblReceiverName)
        Me.frameReceiver.Controls.Add(Me.lblReceiverAdr1)
        Me.frameReceiver.Controls.Add(Me.lblReceiverAdr2)
        Me.frameReceiver.Controls.Add(Me.lblReceiverAdr3)
        Me.frameReceiver.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.frameReceiver.ForeColor = System.Drawing.SystemColors.WindowText
        Me.frameReceiver.Location = New System.Drawing.Point(268, 32)
        Me.frameReceiver.Name = "frameReceiver"
        Me.frameReceiver.Padding = New System.Windows.Forms.Padding(0)
        Me.frameReceiver.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.frameReceiver.Size = New System.Drawing.Size(260, 81)
        Me.frameReceiver.TabIndex = 9
        Me.frameReceiver.TabStop = False
        Me.frameReceiver.Text = "60075 - MOTTAKERS NAVN, ADRESSE, POSTNR, STED"
        '
        'cmbSISMOSetCode
        '
        Me.cmbSISMOSetCode.BackColor = System.Drawing.SystemColors.Window
        Me.cmbSISMOSetCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbSISMOSetCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSISMOSetCode.Enabled = False
        Me.cmbSISMOSetCode.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.cmbSISMOSetCode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbSISMOSetCode.Location = New System.Drawing.Point(88, 56)
        Me.cmbSISMOSetCode.Name = "cmbSISMOSetCode"
        Me.cmbSISMOSetCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbSISMOSetCode.Size = New System.Drawing.Size(169, 22)
        Me.cmbSISMOSetCode.TabIndex = 55
        Me.cmbSISMOSetCode.Visible = False
        '
        'lblReceiverName
        '
        Me.lblReceiverName.BackColor = System.Drawing.SystemColors.Window
        Me.lblReceiverName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReceiverName.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceiverName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblReceiverName.Location = New System.Drawing.Point(5, 10)
        Me.lblReceiverName.Name = "lblReceiverName"
        Me.lblReceiverName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReceiverName.Size = New System.Drawing.Size(247, 13)
        Me.lblReceiverName.TabIndex = 13
        Me.lblReceiverName.Text = "Receivers name"
        '
        'lblReceiverAdr1
        '
        Me.lblReceiverAdr1.BackColor = System.Drawing.SystemColors.Window
        Me.lblReceiverAdr1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReceiverAdr1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceiverAdr1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblReceiverAdr1.Location = New System.Drawing.Point(5, 27)
        Me.lblReceiverAdr1.Name = "lblReceiverAdr1"
        Me.lblReceiverAdr1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReceiverAdr1.Size = New System.Drawing.Size(247, 14)
        Me.lblReceiverAdr1.TabIndex = 12
        Me.lblReceiverAdr1.Text = "Receivers adr1"
        '
        'lblReceiverAdr2
        '
        Me.lblReceiverAdr2.BackColor = System.Drawing.SystemColors.Window
        Me.lblReceiverAdr2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReceiverAdr2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceiverAdr2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblReceiverAdr2.Location = New System.Drawing.Point(5, 44)
        Me.lblReceiverAdr2.Name = "lblReceiverAdr2"
        Me.lblReceiverAdr2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReceiverAdr2.Size = New System.Drawing.Size(247, 14)
        Me.lblReceiverAdr2.TabIndex = 11
        Me.lblReceiverAdr2.Text = "Receivers adr2"
        '
        'lblReceiverAdr3
        '
        Me.lblReceiverAdr3.BackColor = System.Drawing.SystemColors.Window
        Me.lblReceiverAdr3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReceiverAdr3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceiverAdr3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblReceiverAdr3.Location = New System.Drawing.Point(5, 60)
        Me.lblReceiverAdr3.Name = "lblReceiverAdr3"
        Me.lblReceiverAdr3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReceiverAdr3.Size = New System.Drawing.Size(247, 13)
        Me.lblReceiverAdr3.TabIndex = 10
        Me.lblReceiverAdr3.Text = "Receivers adr3"
        '
        'imgMinus
        '
        Me.imgMinus.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgMinus.Image = CType(resources.GetObject("imgMinus.Image"), System.Drawing.Image)
        Me.imgMinus.Location = New System.Drawing.Point(168, 112)
        Me.imgMinus.Name = "imgMinus"
        Me.imgMinus.Size = New System.Drawing.Size(16, 16)
        Me.imgMinus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgMinus.TabIndex = 54
        Me.imgMinus.TabStop = False
        Me.imgMinus.Visible = False
        '
        'imgPlus
        '
        Me.imgPlus.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgPlus.Image = CType(resources.GetObject("imgPlus.Image"), System.Drawing.Image)
        Me.imgPlus.Location = New System.Drawing.Point(144, 112)
        Me.imgPlus.Name = "imgPlus"
        Me.imgPlus.Size = New System.Drawing.Size(16, 16)
        Me.imgPlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgPlus.TabIndex = 55
        Me.imgPlus.TabStop = False
        Me.imgPlus.Visible = False
        '
        'lblKID
        '
        Me.lblKID.BackColor = System.Drawing.SystemColors.Window
        Me.lblKID.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblKID.Enabled = False
        Me.lblKID.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblKID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblKID.Location = New System.Drawing.Point(380, 285)
        Me.lblKID.Name = "lblKID"
        Me.lblKID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblKID.Size = New System.Drawing.Size(150, 15)
        Me.lblKID.TabIndex = 63
        Me.lblKID.Visible = False
        '
        'lblHeadKID
        '
        Me.lblHeadKID.BackColor = System.Drawing.SystemColors.Window
        Me.lblHeadKID.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeadKID.Enabled = False
        Me.lblHeadKID.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeadKID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblHeadKID.Location = New System.Drawing.Point(313, 285)
        Me.lblHeadKID.Name = "lblHeadKID"
        Me.lblHeadKID.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeadKID.Size = New System.Drawing.Size(64, 17)
        Me.lblHeadKID.TabIndex = 62
        Me.lblHeadKID.Text = "60257 - KID:"
        Me.lblHeadKID.Visible = False
        '
        'lblVoucherNo
        '
        Me.lblVoucherNo.BackColor = System.Drawing.SystemColors.Window
        Me.lblVoucherNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVoucherNo.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVoucherNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblVoucherNo.Location = New System.Drawing.Point(10, 16)
        Me.lblVoucherNo.Name = "lblVoucherNo"
        Me.lblVoucherNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVoucherNo.Size = New System.Drawing.Size(117, 19)
        Me.lblVoucherNo.TabIndex = 57
        Me.lblVoucherNo.Text = "60239 - Voucher:"
        Me.lblVoucherNo.Visible = False
        '
        'lblOriginalAmount
        '
        Me.lblOriginalAmount.BackColor = System.Drawing.SystemColors.Window
        Me.lblOriginalAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblOriginalAmount.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblOriginalAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblOriginalAmount.Location = New System.Drawing.Point(447, 275)
        Me.lblOriginalAmount.Name = "lblOriginalAmount"
        Me.lblOriginalAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblOriginalAmount.Size = New System.Drawing.Size(90, 15)
        Me.lblOriginalAmount.TabIndex = 52
        Me.lblOriginalAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadOriginalAmount
        '
        Me.lblHeadOriginalAmount.AutoSize = True
        Me.lblHeadOriginalAmount.BackColor = System.Drawing.SystemColors.Window
        Me.lblHeadOriginalAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeadOriginalAmount.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblHeadOriginalAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblHeadOriginalAmount.Location = New System.Drawing.Point(313, 275)
        Me.lblHeadOriginalAmount.Name = "lblHeadOriginalAmount"
        Me.lblHeadOriginalAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeadOriginalAmount.Size = New System.Drawing.Size(116, 14)
        Me.lblHeadOriginalAmount.TabIndex = 51
        Me.lblHeadOriginalAmount.Text = "60215 - OPPR. BEL�P:"
        '
        'imgOmitCoveringPayment
        '
        Me.imgOmitCoveringPayment.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgOmitCoveringPayment.Image = CType(resources.GetObject("imgOmitCoveringPayment.Image"), System.Drawing.Image)
        Me.imgOmitCoveringPayment.Location = New System.Drawing.Point(0, 0)
        Me.imgOmitCoveringPayment.Name = "imgOmitCoveringPayment"
        Me.imgOmitCoveringPayment.Size = New System.Drawing.Size(305, 305)
        Me.imgOmitCoveringPayment.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgOmitCoveringPayment.TabIndex = 64
        Me.imgOmitCoveringPayment.TabStop = False
        Me.imgOmitCoveringPayment.Visible = False
        '
        'lblFromAccount
        '
        Me.lblFromAccount.BackColor = System.Drawing.SystemColors.Window
        Me.lblFromAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFromAccount.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblFromAccount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblFromAccount.Location = New System.Drawing.Point(419, 256)
        Me.lblFromAccount.Name = "lblFromAccount"
        Me.lblFromAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFromAccount.Size = New System.Drawing.Size(111, 15)
        Me.lblFromAccount.TabIndex = 43
        '
        'lblHeadFromAccount
        '
        Me.lblHeadFromAccount.AutoSize = True
        Me.lblHeadFromAccount.BackColor = System.Drawing.SystemColors.Window
        Me.lblHeadFromAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeadFromAccount.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblHeadFromAccount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblHeadFromAccount.Location = New System.Drawing.Point(313, 259)
        Me.lblHeadFromAccount.Name = "lblHeadFromAccount"
        Me.lblHeadFromAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeadFromAccount.Size = New System.Drawing.Size(109, 14)
        Me.lblHeadFromAccount.TabIndex = 42
        Me.lblHeadFromAccount.Text = "60090 - FRA KONTO:"
        '
        'lblHeadCurrency
        '
        Me.lblHeadCurrency.AutoSize = True
        Me.lblHeadCurrency.BackColor = System.Drawing.SystemColors.Window
        Me.lblHeadCurrency.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeadCurrency.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblHeadCurrency.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblHeadCurrency.Location = New System.Drawing.Point(313, 175)
        Me.lblHeadCurrency.Name = "lblHeadCurrency"
        Me.lblHeadCurrency.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeadCurrency.Size = New System.Drawing.Size(91, 14)
        Me.lblHeadCurrency.TabIndex = 40
        Me.lblHeadCurrency.Text = "60083 - VALUTA:"
        '
        'lblRef3
        '
        Me.lblRef3.BackColor = System.Drawing.SystemColors.Window
        Me.lblRef3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblRef3.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblRef3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblRef3.Location = New System.Drawing.Point(419, 240)
        Me.lblRef3.Name = "lblRef3"
        Me.lblRef3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblRef3.Size = New System.Drawing.Size(111, 15)
        Me.lblRef3.TabIndex = 39
        '
        'lblHeadAmount
        '
        Me.lblHeadAmount.AutoSize = True
        Me.lblHeadAmount.BackColor = System.Drawing.Color.Transparent
        Me.lblHeadAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeadAmount.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblHeadAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeadAmount.Location = New System.Drawing.Point(313, 159)
        Me.lblHeadAmount.Name = "lblHeadAmount"
        Me.lblHeadAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeadAmount.Size = New System.Drawing.Size(83, 14)
        Me.lblHeadAmount.TabIndex = 33
        Me.lblHeadAmount.Text = "60079 - BEL�P:"
        '
        'lblHeadRef1
        '
        Me.lblHeadRef1.AutoSize = True
        Me.lblHeadRef1.BackColor = System.Drawing.SystemColors.Window
        Me.lblHeadRef1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeadRef1.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblHeadRef1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblHeadRef1.Location = New System.Drawing.Point(313, 192)
        Me.lblHeadRef1.Name = "lblHeadRef1"
        Me.lblHeadRef1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeadRef1.Size = New System.Drawing.Size(119, 14)
        Me.lblHeadRef1.TabIndex = 32
        Me.lblHeadRef1.Text = "60080 - REFERANSE 1:"
        '
        'lblHeadRef2
        '
        Me.lblHeadRef2.AutoSize = True
        Me.lblHeadRef2.BackColor = System.Drawing.SystemColors.Window
        Me.lblHeadRef2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeadRef2.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblHeadRef2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblHeadRef2.Location = New System.Drawing.Point(313, 209)
        Me.lblHeadRef2.Name = "lblHeadRef2"
        Me.lblHeadRef2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeadRef2.Size = New System.Drawing.Size(119, 14)
        Me.lblHeadRef2.TabIndex = 31
        Me.lblHeadRef2.Text = "60081 - REFERANSE 2:"
        '
        'lblHeadRef3
        '
        Me.lblHeadRef3.AutoSize = True
        Me.lblHeadRef3.BackColor = System.Drawing.SystemColors.Window
        Me.lblHeadRef3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeadRef3.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblHeadRef3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblHeadRef3.Location = New System.Drawing.Point(313, 242)
        Me.lblHeadRef3.Name = "lblHeadRef3"
        Me.lblHeadRef3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeadRef3.Size = New System.Drawing.Size(119, 14)
        Me.lblHeadRef3.TabIndex = 30
        Me.lblHeadRef3.Text = "60082 - REFERANSE 3:"
        '
        'lblHeadToAccount
        '
        Me.lblHeadToAccount.AutoSize = True
        Me.lblHeadToAccount.BackColor = System.Drawing.SystemColors.Window
        Me.lblHeadToAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeadToAccount.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.lblHeadToAccount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblHeadToAccount.Location = New System.Drawing.Point(313, 142)
        Me.lblHeadToAccount.Name = "lblHeadToAccount"
        Me.lblHeadToAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeadToAccount.Size = New System.Drawing.Size(103, 14)
        Me.lblHeadToAccount.TabIndex = 29
        Me.lblHeadToAccount.Text = "60078 - TIL KONTO:"
        '
        'lblNotifyGiroMail
        '
        Me.lblNotifyGiroMail.BackColor = System.Drawing.SystemColors.Control
        Me.lblNotifyGiroMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNotifyGiroMail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNotifyGiroMail.Location = New System.Drawing.Point(316, 287)
        Me.lblNotifyGiroMail.Name = "lblNotifyGiroMail"
        Me.lblNotifyGiroMail.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNotifyGiroMail.Size = New System.Drawing.Size(221, 22)
        Me.lblNotifyGiroMail.TabIndex = 27
        Me.lblNotifyGiroMail.Text = "60096 - Se GiroMail / GiroFax"
        '
        'lblNoOfInvoices
        '
        Me.lblNoOfInvoices.BackColor = System.Drawing.SystemColors.Window
        Me.lblNoOfInvoices.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNoOfInvoices.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfInvoices.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblNoOfInvoices.Location = New System.Drawing.Point(426, 14)
        Me.lblNoOfInvoices.Name = "lblNoOfInvoices"
        Me.lblNoOfInvoices.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNoOfInvoices.Size = New System.Drawing.Size(99, 18)
        Me.lblNoOfInvoices.TabIndex = 26
        Me.lblNoOfInvoices.Visible = False
        '
        'lblIncomingHeading
        '
        Me.lblIncomingHeading.BackColor = System.Drawing.SystemColors.Window
        Me.lblIncomingHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblIncomingHeading.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncomingHeading.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblIncomingHeading.Location = New System.Drawing.Point(143, 14)
        Me.lblIncomingHeading.Name = "lblIncomingHeading"
        Me.lblIncomingHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblIncomingHeading.Size = New System.Drawing.Size(294, 19)
        Me.lblIncomingHeading.TabIndex = 20
        Me.lblIncomingHeading.Text = "60073 - MELDING OM KREDITERING"
        Me.lblIncomingHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblNotification
        '
        Me.lblNotification.BackColor = System.Drawing.SystemColors.Window
        Me.lblNotification.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNotification.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNotification.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblNotification.Location = New System.Drawing.Point(5, 116)
        Me.lblNotification.Name = "lblNotification"
        Me.lblNotification.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNotification.Size = New System.Drawing.Size(249, 17)
        Me.lblNotification.TabIndex = 19
        Me.lblNotification.Text = "60074 - BEL�PET GJELDER:"
        '
        'ImgMatched
        '
        Me.ImgMatched.BackColor = System.Drawing.Color.Transparent
        Me.ImgMatched.Cursor = System.Windows.Forms.Cursors.Default
        Me.ImgMatched.Image = CType(resources.GetObject("ImgMatched.Image"), System.Drawing.Image)
        Me.ImgMatched.InitialImage = CType(resources.GetObject("ImgMatched.InitialImage"), System.Drawing.Image)
        Me.ImgMatched.Location = New System.Drawing.Point(384, 2)
        Me.ImgMatched.Name = "ImgMatched"
        Me.ImgMatched.Size = New System.Drawing.Size(159, 104)
        Me.ImgMatched.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.ImgMatched.TabIndex = 65
        Me.ImgMatched.TabStop = False
        Me.ImgMatched.Visible = False
        '
        'frame_Matched
        '
        Me.frame_Matched.BackColor = System.Drawing.SystemColors.Window
        Me.frame_Matched.Controls.Add(Me.lblHowmatched)
        Me.frame_Matched.Controls.Add(Me.lblSelectedAmount)
        Me.frame_Matched.Controls.Add(Me.lblMatchedHeading)
        Me.frame_Matched.Cursor = System.Windows.Forms.Cursors.Default
        Me.frame_Matched.ForeColor = System.Drawing.SystemColors.WindowText
        Me.frame_Matched.Location = New System.Drawing.Point(338, 409)
        Me.frame_Matched.Name = "frame_Matched"
        Me.frame_Matched.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.frame_Matched.Size = New System.Drawing.Size(323, 115)
        Me.frame_Matched.TabIndex = 23
        Me.frame_Matched.Visible = False
        '
        'lblHowmatched
        '
        Me.lblHowmatched.BackColor = System.Drawing.SystemColors.Window
        Me.lblHowmatched.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHowmatched.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHowmatched.Location = New System.Drawing.Point(0, 4)
        Me.lblHowmatched.Name = "lblHowmatched"
        Me.lblHowmatched.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHowmatched.Size = New System.Drawing.Size(113, 17)
        Me.lblHowmatched.TabIndex = 58
        Me.lblHowmatched.Text = "lblHowMatched"
        '
        'lblSelectedAmount
        '
        Me.lblSelectedAmount.BackColor = System.Drawing.SystemColors.Window
        Me.lblSelectedAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSelectedAmount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSelectedAmount.Location = New System.Drawing.Point(8, 19)
        Me.lblSelectedAmount.Name = "lblSelectedAmount"
        Me.lblSelectedAmount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSelectedAmount.Size = New System.Drawing.Size(534, 20)
        Me.lblSelectedAmount.TabIndex = 25
        Me.lblSelectedAmount.Text = "60092 - Selected amount"
        '
        'lblMatchedHeading
        '
        Me.lblMatchedHeading.AutoSize = True
        Me.lblMatchedHeading.BackColor = System.Drawing.SystemColors.Window
        Me.lblMatchedHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMatchedHeading.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMatchedHeading.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblMatchedHeading.Location = New System.Drawing.Point(8, 0)
        Me.lblMatchedHeading.Name = "lblMatchedHeading"
        Me.lblMatchedHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMatchedHeading.Size = New System.Drawing.Size(222, 19)
        Me.lblMatchedHeading.TabIndex = 24
        Me.lblMatchedHeading.Text = "60091 - Avstemmingsforslag"
        '
        'ToolbarMatchManual
        '
        Me.ToolbarMatchManual.Location = New System.Drawing.Point(0, 24)
        Me.ToolbarMatchManual.Name = "ToolbarMatchManual"
        Me.ToolbarMatchManual.Size = New System.Drawing.Size(888, 25)
        Me.ToolbarMatchManual.TabIndex = 6
        Me.ToolbarMatchManual.TabStop = True
        '
        'imgGoToLastMatched
        '
        Me.imgGoToLastMatched.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgGoToLastMatched.Image = CType(resources.GetObject("imgGoToLastMatched.Image"), System.Drawing.Image)
        Me.imgGoToLastMatched.Location = New System.Drawing.Point(731, 136)
        Me.imgGoToLastMatched.Name = "imgGoToLastMatched"
        Me.imgGoToLastMatched.Size = New System.Drawing.Size(16, 16)
        Me.imgGoToLastMatched.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgGoToLastMatched.TabIndex = 65
        Me.imgGoToLastMatched.TabStop = False
        Me.imgGoToLastMatched.Visible = False
        '
        'imgFind7
        '
        Me.imgFind7.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgFind7.Image = CType(resources.GetObject("imgFind7.Image"), System.Drawing.Image)
        Me.imgFind7.Location = New System.Drawing.Point(780, 160)
        Me.imgFind7.Name = "imgFind7"
        Me.imgFind7.Size = New System.Drawing.Size(16, 16)
        Me.imgFind7.TabIndex = 66
        Me.imgFind7.TabStop = False
        Me.imgFind7.Visible = False
        '
        'imgFind6
        '
        Me.imgFind6.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgFind6.Image = CType(resources.GetObject("imgFind6.Image"), System.Drawing.Image)
        Me.imgFind6.Location = New System.Drawing.Point(764, 160)
        Me.imgFind6.Name = "imgFind6"
        Me.imgFind6.Size = New System.Drawing.Size(16, 16)
        Me.imgFind6.TabIndex = 67
        Me.imgFind6.TabStop = False
        Me.imgFind6.Visible = False
        '
        'imgFind5
        '
        Me.imgFind5.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgFind5.Image = CType(resources.GetObject("imgFind5.Image"), System.Drawing.Image)
        Me.imgFind5.Location = New System.Drawing.Point(748, 160)
        Me.imgFind5.Name = "imgFind5"
        Me.imgFind5.Size = New System.Drawing.Size(16, 16)
        Me.imgFind5.TabIndex = 68
        Me.imgFind5.TabStop = False
        Me.imgFind5.Visible = False
        '
        'imgFind4
        '
        Me.imgFind4.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgFind4.Image = CType(resources.GetObject("imgFind4.Image"), System.Drawing.Image)
        Me.imgFind4.Location = New System.Drawing.Point(732, 160)
        Me.imgFind4.Name = "imgFind4"
        Me.imgFind4.Size = New System.Drawing.Size(16, 16)
        Me.imgFind4.TabIndex = 69
        Me.imgFind4.TabStop = False
        Me.imgFind4.Visible = False
        '
        'ImgContinue
        '
        Me.ImgContinue.Cursor = System.Windows.Forms.Cursors.Default
        Me.ImgContinue.Image = CType(resources.GetObject("ImgContinue.Image"), System.Drawing.Image)
        Me.ImgContinue.Location = New System.Drawing.Point(717, 160)
        Me.ImgContinue.Name = "ImgContinue"
        Me.ImgContinue.Size = New System.Drawing.Size(16, 16)
        Me.ImgContinue.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.ImgContinue.TabIndex = 70
        Me.ImgContinue.TabStop = False
        Me.ImgContinue.Visible = False
        '
        'imgSave
        '
        Me.imgSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgSave.Image = CType(resources.GetObject("imgSave.Image"), System.Drawing.Image)
        Me.imgSave.Location = New System.Drawing.Point(712, 184)
        Me.imgSave.Name = "imgSave"
        Me.imgSave.Size = New System.Drawing.Size(16, 16)
        Me.imgSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgSave.TabIndex = 71
        Me.imgSave.TabStop = False
        Me.imgSave.Visible = False
        '
        'imgLockUser
        '
        Me.imgLockUser.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgLockUser.Image = CType(resources.GetObject("imgLockUser.Image"), System.Drawing.Image)
        Me.imgLockUser.Location = New System.Drawing.Point(661, 186)
        Me.imgLockUser.Name = "imgLockUser"
        Me.imgLockUser.Size = New System.Drawing.Size(32, 32)
        Me.imgLockUser.TabIndex = 72
        Me.imgLockUser.TabStop = False
        Me.imgLockUser.Visible = False
        '
        'imgOmit
        '
        Me.imgOmit.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgOmit.Enabled = False
        Me.imgOmit.Image = CType(resources.GetObject("imgOmit.Image"), System.Drawing.Image)
        Me.imgOmit.Location = New System.Drawing.Point(640, 184)
        Me.imgOmit.Name = "imgOmit"
        Me.imgOmit.Size = New System.Drawing.Size(15, 15)
        Me.imgOmit.TabIndex = 73
        Me.imgOmit.TabStop = False
        Me.imgOmit.Visible = False
        '
        'imgeMail
        '
        Me.imgeMail.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgeMail.Image = CType(resources.GetObject("imgeMail.Image"), System.Drawing.Image)
        Me.imgeMail.Location = New System.Drawing.Point(605, 180)
        Me.imgeMail.Name = "imgeMail"
        Me.imgeMail.Size = New System.Drawing.Size(16, 16)
        Me.imgeMail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgeMail.TabIndex = 74
        Me.imgeMail.TabStop = False
        Me.imgeMail.Visible = False
        '
        'imgPrintForm
        '
        Me.imgPrintForm.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgPrintForm.Image = CType(resources.GetObject("imgPrintForm.Image"), System.Drawing.Image)
        Me.imgPrintForm.Location = New System.Drawing.Point(585, 178)
        Me.imgPrintForm.Name = "imgPrintForm"
        Me.imgPrintForm.Size = New System.Drawing.Size(16, 16)
        Me.imgPrintForm.TabIndex = 75
        Me.imgPrintForm.TabStop = False
        Me.imgPrintForm.Visible = False
        '
        'imgClose
        '
        Me.imgClose.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgClose.Image = CType(resources.GetObject("imgClose.Image"), System.Drawing.Image)
        Me.imgClose.Location = New System.Drawing.Point(701, 159)
        Me.imgClose.Name = "imgClose"
        Me.imgClose.Size = New System.Drawing.Size(16, 16)
        Me.imgClose.TabIndex = 76
        Me.imgClose.TabStop = False
        Me.imgClose.Visible = False
        '
        'imgFind3
        '
        Me.imgFind3.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgFind3.Image = CType(resources.GetObject("imgFind3.Image"), System.Drawing.Image)
        Me.imgFind3.Location = New System.Drawing.Point(683, 159)
        Me.imgFind3.Name = "imgFind3"
        Me.imgFind3.Size = New System.Drawing.Size(16, 16)
        Me.imgFind3.TabIndex = 77
        Me.imgFind3.TabStop = False
        Me.imgFind3.Visible = False
        '
        'imgFind2
        '
        Me.imgFind2.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgFind2.Image = CType(resources.GetObject("imgFind2.Image"), System.Drawing.Image)
        Me.imgFind2.Location = New System.Drawing.Point(666, 159)
        Me.imgFind2.Name = "imgFind2"
        Me.imgFind2.Size = New System.Drawing.Size(16, 16)
        Me.imgFind2.TabIndex = 78
        Me.imgFind2.TabStop = False
        Me.imgFind2.Visible = False
        '
        'imgFind1
        '
        Me.imgFind1.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgFind1.Image = CType(resources.GetObject("imgFind1.Image"), System.Drawing.Image)
        Me.imgFind1.Location = New System.Drawing.Point(648, 159)
        Me.imgFind1.Name = "imgFind1"
        Me.imgFind1.Size = New System.Drawing.Size(16, 16)
        Me.imgFind1.TabIndex = 79
        Me.imgFind1.TabStop = False
        Me.imgFind1.Visible = False
        '
        'imgFindInvoice
        '
        Me.imgFindInvoice.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgFindInvoice.Image = CType(resources.GetObject("imgFindInvoice.Image"), System.Drawing.Image)
        Me.imgFindInvoice.Location = New System.Drawing.Point(622, 159)
        Me.imgFindInvoice.Name = "imgFindInvoice"
        Me.imgFindInvoice.Size = New System.Drawing.Size(16, 16)
        Me.imgFindInvoice.TabIndex = 80
        Me.imgFindInvoice.TabStop = False
        Me.imgFindInvoice.Visible = False
        '
        'imgFindCustomer
        '
        Me.imgFindCustomer.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgFindCustomer.Image = CType(resources.GetObject("imgFindCustomer.Image"), System.Drawing.Image)
        Me.imgFindCustomer.Location = New System.Drawing.Point(604, 159)
        Me.imgFindCustomer.Name = "imgFindCustomer"
        Me.imgFindCustomer.Size = New System.Drawing.Size(16, 16)
        Me.imgFindCustomer.TabIndex = 81
        Me.imgFindCustomer.TabStop = False
        Me.imgFindCustomer.Visible = False
        '
        'imgFindAmount
        '
        Me.imgFindAmount.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgFindAmount.Image = CType(resources.GetObject("imgFindAmount.Image"), System.Drawing.Image)
        Me.imgFindAmount.Location = New System.Drawing.Point(586, 159)
        Me.imgFindAmount.Name = "imgFindAmount"
        Me.imgFindAmount.Size = New System.Drawing.Size(16, 16)
        Me.imgFindAmount.TabIndex = 82
        Me.imgFindAmount.TabStop = False
        Me.imgFindAmount.Visible = False
        '
        'imgFront
        '
        Me.imgFront.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgFront.Image = CType(resources.GetObject("imgFront.Image"), System.Drawing.Image)
        Me.imgFront.Location = New System.Drawing.Point(710, 136)
        Me.imgFront.Name = "imgFront"
        Me.imgFront.Size = New System.Drawing.Size(16, 16)
        Me.imgFront.TabIndex = 83
        Me.imgFront.TabStop = False
        Me.imgFront.Visible = False
        '
        'imgDelete
        '
        Me.imgDelete.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgDelete.Image = CType(resources.GetObject("imgDelete.Image"), System.Drawing.Image)
        Me.imgDelete.Location = New System.Drawing.Point(692, 136)
        Me.imgDelete.Name = "imgDelete"
        Me.imgDelete.Size = New System.Drawing.Size(16, 16)
        Me.imgDelete.TabIndex = 84
        Me.imgDelete.TabStop = False
        Me.imgDelete.Visible = False
        '
        'imgUndo
        '
        Me.imgUndo.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgUndo.Image = CType(resources.GetObject("imgUndo.Image"), System.Drawing.Image)
        Me.imgUndo.Location = New System.Drawing.Point(675, 136)
        Me.imgUndo.Name = "imgUndo"
        Me.imgUndo.Size = New System.Drawing.Size(16, 16)
        Me.imgUndo.TabIndex = 85
        Me.imgUndo.TabStop = False
        Me.imgUndo.Visible = False
        '
        'imgTick
        '
        Me.imgTick.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgTick.Image = CType(resources.GetObject("imgTick.Image"), System.Drawing.Image)
        Me.imgTick.Location = New System.Drawing.Point(656, 136)
        Me.imgTick.Name = "imgTick"
        Me.imgTick.Size = New System.Drawing.Size(16, 16)
        Me.imgTick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgTick.TabIndex = 86
        Me.imgTick.TabStop = False
        Me.imgTick.Visible = False
        '
        'imgLast
        '
        Me.imgLast.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgLast.Image = CType(resources.GetObject("imgLast.Image"), System.Drawing.Image)
        Me.imgLast.Location = New System.Drawing.Point(637, 136)
        Me.imgLast.Name = "imgLast"
        Me.imgLast.Size = New System.Drawing.Size(16, 16)
        Me.imgLast.TabIndex = 87
        Me.imgLast.TabStop = False
        Me.imgLast.Visible = False
        '
        'imgNext
        '
        Me.imgNext.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgNext.Image = CType(resources.GetObject("imgNext.Image"), System.Drawing.Image)
        Me.imgNext.Location = New System.Drawing.Point(620, 136)
        Me.imgNext.Name = "imgNext"
        Me.imgNext.Size = New System.Drawing.Size(16, 16)
        Me.imgNext.TabIndex = 88
        Me.imgNext.TabStop = False
        Me.imgNext.Visible = False
        '
        'imgPrevious
        '
        Me.imgPrevious.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgPrevious.Image = CType(resources.GetObject("imgPrevious.Image"), System.Drawing.Image)
        Me.imgPrevious.Location = New System.Drawing.Point(603, 136)
        Me.imgPrevious.Name = "imgPrevious"
        Me.imgPrevious.Size = New System.Drawing.Size(16, 16)
        Me.imgPrevious.TabIndex = 89
        Me.imgPrevious.TabStop = False
        Me.imgPrevious.Visible = False
        '
        'imgFirst
        '
        Me.imgFirst.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgFirst.Image = CType(resources.GetObject("imgFirst.Image"), System.Drawing.Image)
        Me.imgFirst.Location = New System.Drawing.Point(586, 136)
        Me.imgFirst.Name = "imgFirst"
        Me.imgFirst.Size = New System.Drawing.Size(16, 16)
        Me.imgFirst.TabIndex = 90
        Me.imgFirst.TabStop = False
        Me.imgFirst.Visible = False
        '
        'lblSeek
        '
        Me.lblSeek.AutoSize = True
        Me.lblSeek.BackColor = System.Drawing.SystemColors.Control
        Me.lblSeek.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSeek.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeek.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSeek.Location = New System.Drawing.Point(578, 105)
        Me.lblSeek.Name = "lblSeek"
        Me.lblSeek.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSeek.Size = New System.Drawing.Size(61, 16)
        Me.lblSeek.TabIndex = 46
        Me.lblSeek.Text = "lblSeek"
        '
        'lblProgress
        '
        Me.lblProgress.BackColor = System.Drawing.SystemColors.Control
        Me.lblProgress.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProgress.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProgress.Location = New System.Drawing.Point(525, 553)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProgress.Size = New System.Drawing.Size(70, 20)
        Me.lblProgress.TabIndex = 45
        Me.lblProgress.Text = "100%"
        '
        'lblError
        '
        Me.lblError.BackColor = System.Drawing.SystemColors.Window
        Me.lblError.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblError.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblError.Location = New System.Drawing.Point(18, 560)
        Me.lblError.Name = "lblError"
        Me.lblError.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblError.Size = New System.Drawing.Size(403, 20)
        Me.lblError.TabIndex = 5
        '
        'cmdDemo
        '
        Me.cmdDemo.BackColor = System.Drawing.Color.SandyBrown
        Me.cmdDemo.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.cmdDemo.Location = New System.Drawing.Point(641, 290)
        Me.cmdDemo.Name = "cmdDemo"
        Me.cmdDemo.Size = New System.Drawing.Size(67, 47)
        Me.cmdDemo.TabIndex = 93
        Me.cmdDemo.Text = "Button1"
        Me.cmdDemo.UseVisualStyleBackColor = False
        '
        'txtDemoText
        '
        Me.txtDemoText.BackColor = System.Drawing.Color.Coral
        Me.txtDemoText.Location = New System.Drawing.Point(588, 554)
        Me.txtDemoText.Multiline = True
        Me.txtDemoText.Name = "txtDemoText"
        Me.txtDemoText.ReadOnly = True
        Me.txtDemoText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDemoText.Size = New System.Drawing.Size(119, 20)
        Me.txtDemoText.TabIndex = 94
        Me.txtDemoText.TabStop = False
        '
        'PictureDemoExit
        '
        Me.PictureDemoExit.Image = Global.BabelBank.My.Resources.Resources.application_exit
        Me.PictureDemoExit.Location = New System.Drawing.Point(643, 63)
        Me.PictureDemoExit.Name = "PictureDemoExit"
        Me.PictureDemoExit.Size = New System.Drawing.Size(16, 16)
        Me.PictureDemoExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureDemoExit.TabIndex = 96
        Me.PictureDemoExit.TabStop = False
        '
        'PictureFilterOn
        '
        Me.PictureFilterOn.Image = Global.BabelBank.My.Resources.Resources.filter_add
        Me.PictureFilterOn.Location = New System.Drawing.Point(840, 502)
        Me.PictureFilterOn.Name = "PictureFilterOn"
        Me.PictureFilterOn.Size = New System.Drawing.Size(14, 14)
        Me.PictureFilterOn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureFilterOn.TabIndex = 97
        Me.PictureFilterOn.TabStop = False
        '
        'PictureFilterOff
        '
        Me.PictureFilterOff.Image = Global.BabelBank.My.Resources.Resources.filter_delete
        Me.PictureFilterOff.Location = New System.Drawing.Point(860, 502)
        Me.PictureFilterOff.Name = "PictureFilterOff"
        Me.PictureFilterOff.Size = New System.Drawing.Size(14, 14)
        Me.PictureFilterOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureFilterOff.TabIndex = 98
        Me.PictureFilterOff.TabStop = False
        '
        'txtNote
        '
        Me.txtNote.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.txtNote.Location = New System.Drawing.Point(749, 290)
        Me.txtNote.Name = "txtNote"
        Me.txtNote.Size = New System.Drawing.Size(91, 46)
        Me.txtNote.TabIndex = 99
        Me.txtNote.Text = ""
        '
        'imgYellowPin
        '
        Me.imgYellowPin.BackColor = System.Drawing.Color.Transparent
        Me.imgYellowPin.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgYellowPin.Image = CType(resources.GetObject("imgYellowPin.Image"), System.Drawing.Image)
        Me.imgYellowPin.Location = New System.Drawing.Point(771, 194)
        Me.imgYellowPin.Name = "imgYellowPin"
        Me.imgYellowPin.Size = New System.Drawing.Size(16, 16)
        Me.imgYellowPin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgYellowPin.TabIndex = 100
        Me.imgYellowPin.TabStop = False
        Me.imgYellowPin.Visible = False
        '
        'ImageList2
        '
        Me.ImageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList2.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList2.TransparentColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        '
        'imgRed
        '
        Me.imgRed.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgRed.Image = CType(resources.GetObject("imgRed.Image"), System.Drawing.Image)
        Me.imgRed.Location = New System.Drawing.Point(815, 82)
        Me.imgRed.Name = "imgRed"
        Me.imgRed.Size = New System.Drawing.Size(20, 20)
        Me.imgRed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.imgRed.TabIndex = 101
        Me.imgRed.TabStop = False
        Me.imgRed.Visible = False
        '
        'imgGreen
        '
        Me.imgGreen.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgGreen.Image = CType(resources.GetObject("imgGreen.Image"), System.Drawing.Image)
        Me.imgGreen.Location = New System.Drawing.Point(815, 105)
        Me.imgGreen.Name = "imgGreen"
        Me.imgGreen.Size = New System.Drawing.Size(36, 36)
        Me.imgGreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.imgGreen.TabIndex = 102
        Me.imgGreen.TabStop = False
        Me.imgGreen.Visible = False
        '
        'imgBlue
        '
        Me.imgBlue.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgBlue.Image = CType(resources.GetObject("imgBlue.Image"), System.Drawing.Image)
        Me.imgBlue.Location = New System.Drawing.Point(815, 127)
        Me.imgBlue.Name = "imgBlue"
        Me.imgBlue.Size = New System.Drawing.Size(16, 16)
        Me.imgBlue.TabIndex = 103
        Me.imgBlue.TabStop = False
        Me.imgBlue.Visible = False
        '
        'imgYellow
        '
        Me.imgYellow.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgYellow.Image = CType(resources.GetObject("imgYellow.Image"), System.Drawing.Image)
        Me.imgYellow.Location = New System.Drawing.Point(815, 149)
        Me.imgYellow.Name = "imgYellow"
        Me.imgYellow.Size = New System.Drawing.Size(16, 16)
        Me.imgYellow.TabIndex = 104
        Me.imgYellow.TabStop = False
        Me.imgYellow.Visible = False
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(164, 528)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(560, 1)
        Me.lblLine1.TabIndex = 105
        Me.lblLine1.Text = "Label1"
        '
        'PrintDocument1
        '
        '
        'chkERPAll
        '
        Me.chkERPAll.BackColor = System.Drawing.SystemColors.Control
        Me.chkERPAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkERPAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkERPAll.Location = New System.Drawing.Point(717, 342)
        Me.chkERPAll.Name = "chkERPAll"
        Me.chkERPAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkERPAll.Size = New System.Drawing.Size(14, 15)
        Me.chkERPAll.TabIndex = 106
        Me.chkERPAll.UseVisualStyleBackColor = False
        '
        'frmMATCH_Manual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(888, 589)
        Me.Controls.Add(Me.chkERPAll)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.imgYellow)
        Me.Controls.Add(Me.imgBlue)
        Me.Controls.Add(Me.imgGreen)
        Me.Controls.Add(Me.imgRed)
        Me.Controls.Add(Me.imgYellowPin)
        Me.Controls.Add(Me.txtNote)
        Me.Controls.Add(Me.PictureFilterOff)
        Me.Controls.Add(Me.PictureFilterOn)
        Me.Controls.Add(Me.PictureDemoExit)
        Me.Controls.Add(Me.txtDemoText)
        Me.Controls.Add(Me.cmdDemo)
        Me.Controls.Add(Me.chkSpecialMark)
        Me.Controls.Add(Me.txtLBLSGCustomerNo)
        Me.Controls.Add(Me.txtSGCustomerNo)
        Me.Controls.Add(Me.chkSGRegion)
        Me.Controls.Add(Me.PictureeMail)
        Me.Controls.Add(Me.frameSeekResult)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.frameMATCH_Incoming)
        Me.Controls.Add(Me.frame_Matched)
        Me.Controls.Add(Me.ToolbarMatchManual)
        Me.Controls.Add(Me.imgGoToLastMatched)
        Me.Controls.Add(Me.imgFind7)
        Me.Controls.Add(Me.imgFind6)
        Me.Controls.Add(Me.imgFind5)
        Me.Controls.Add(Me.imgFind4)
        Me.Controls.Add(Me.ImgContinue)
        Me.Controls.Add(Me.imgSave)
        Me.Controls.Add(Me.imgLockUser)
        Me.Controls.Add(Me.imgOmit)
        Me.Controls.Add(Me.imgeMail)
        Me.Controls.Add(Me.imgPrintForm)
        Me.Controls.Add(Me.imgClose)
        Me.Controls.Add(Me.imgFind3)
        Me.Controls.Add(Me.imgFind2)
        Me.Controls.Add(Me.imgFind1)
        Me.Controls.Add(Me.imgFindInvoice)
        Me.Controls.Add(Me.imgFindCustomer)
        Me.Controls.Add(Me.imgFindAmount)
        Me.Controls.Add(Me.imgFront)
        Me.Controls.Add(Me.imgDelete)
        Me.Controls.Add(Me.imgUndo)
        Me.Controls.Add(Me.imgTick)
        Me.Controls.Add(Me.imgLast)
        Me.Controls.Add(Me.imgNext)
        Me.Controls.Add(Me.imgPrevious)
        Me.Controls.Add(Me.imgFirst)
        Me.Controls.Add(Me.lblSeek)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.MainMenu1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(4, 50)
        Me.Name = "frmMATCH_Manual"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "60072 - Manual Matching"
        Me.MainMenu1.ResumeLayout(False)
        Me.MainMenu1.PerformLayout()
        CType(Me.PictureeMail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.frameSeekResult.ResumeLayout(False)
        CType(Me.gridERP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.frameMATCH_Incoming.ResumeLayout(False)
        Me.Frame2.ResumeLayout(False)
        Me.Frame2.PerformLayout()
        Me.framePayer.ResumeLayout(False)
        Me.frameReceiver.ResumeLayout(False)
        CType(Me.imgMinus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgPlus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgOmitCoveringPayment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImgMatched, System.ComponentModel.ISupportInitialize).EndInit()
        Me.frame_Matched.ResumeLayout(False)
        Me.frame_Matched.PerformLayout()
        CType(Me.imgGoToLastMatched, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFind7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFind6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFind5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFind4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImgContinue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgSave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgLockUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgOmit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgeMail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgPrintForm, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFind3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFind2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFind1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFindInvoice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFindCustomer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFindAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFront, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgDelete, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgUndo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgTick, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgLast, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgNext, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgPrevious, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFirst, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureDemoExit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureFilterOn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureFilterOff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgYellowPin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgRed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgGreen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgBlue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgYellow, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gridERP As System.Windows.Forms.DataGridView
    Friend WithEvents cmdDemo As System.Windows.Forms.Button
    Friend WithEvents mnuPrintUnmatched As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtDemoText As System.Windows.Forms.TextBox
    Friend WithEvents mnuFilterSpecial As System.Windows.Forms.ToolStripMenuItem
    'Friend WithEvents gridMatched As MyDataGridView
    Friend WithEvents PictureDemoExit As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFilterOff As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFilterOn As System.Windows.Forms.PictureBox
    Friend WithEvents mnuPrintSpecial As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents lblCurrency As System.Windows.Forms.Label
    Public WithEvents lblRef2 As System.Windows.Forms.Label
    Public WithEvents lblRef1 As System.Windows.Forms.Label
    Public WithEvents lblAmount As System.Windows.Forms.Label
    Public WithEvents lblToAccount As System.Windows.Forms.Label
    Public WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents mnuPrintOmitted As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtNote As System.Windows.Forms.RichTextBox
    Public WithEvents imgYellowPin As System.Windows.Forms.PictureBox
    Friend WithEvents mnuNote As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents ImageList2 As System.Windows.Forms.ImageList
    Public WithEvents imgGreen As System.Windows.Forms.PictureBox
    Public WithEvents imgRed As System.Windows.Forms.PictureBox
    Public WithEvents imgYellow As System.Windows.Forms.PictureBox
    Public WithEvents imgBlue As System.Windows.Forms.PictureBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents txtNotificationForPrint As System.Windows.Forms.TextBox
    Public WithEvents chkERPAll As System.Windows.Forms.CheckBox
    Friend WithEvents mnuFilterProposed As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents lblPayersCC As System.Windows.Forms.Label
#End Region
End Class
