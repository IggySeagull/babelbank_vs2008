<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmVisma_ReportSetup
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public CommonDialogOpen As System.Windows.Forms.OpenFileDialog
    Public WithEvents cmdFileOpen As System.Windows.Forms.Button
    Public WithEvents txtReportPath As System.Windows.Forms.TextBox
    Public WithEvents lblReportPath As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVisma_ReportSetup))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.CommonDialogOpen = New System.Windows.Forms.OpenFileDialog
        Me.cmdFileOpen = New System.Windows.Forms.Button
        Me.txtReportPath = New System.Windows.Forms.TextBox
        Me.lblReportPath = New System.Windows.Forms.Label
        Me.cmdShow = New System.Windows.Forms.Button
        Me.cmdClose = New System.Windows.Forms.Button
        Me.cmdSave = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cmdFileOpen
        '
        Me.cmdFileOpen.BackColor = System.Drawing.SystemColors.Control
        Me.cmdFileOpen.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdFileOpen.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdFileOpen.Image = CType(resources.GetObject("cmdFileOpen.Image"), System.Drawing.Image)
        Me.cmdFileOpen.Location = New System.Drawing.Point(360, 48)
        Me.cmdFileOpen.Name = "cmdFileOpen"
        Me.cmdFileOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdFileOpen.Size = New System.Drawing.Size(24, 24)
        Me.cmdFileOpen.TabIndex = 5
        Me.cmdFileOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cmdFileOpen.UseVisualStyleBackColor = False
        '
        'txtReportPath
        '
        Me.txtReportPath.AcceptsReturn = True
        Me.txtReportPath.BackColor = System.Drawing.SystemColors.Window
        Me.txtReportPath.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtReportPath.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtReportPath.Location = New System.Drawing.Point(24, 51)
        Me.txtReportPath.MaxLength = 512
        Me.txtReportPath.Name = "txtReportPath"
        Me.txtReportPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtReportPath.Size = New System.Drawing.Size(325, 20)
        Me.txtReportPath.TabIndex = 0
        '
        'lblReportPath
        '
        Me.lblReportPath.BackColor = System.Drawing.SystemColors.Control
        Me.lblReportPath.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReportPath.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblReportPath.Location = New System.Drawing.Point(24, 32)
        Me.lblReportPath.Name = "lblReportPath"
        Me.lblReportPath.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReportPath.Size = New System.Drawing.Size(161, 14)
        Me.lblReportPath.TabIndex = 4
        Me.lblReportPath.Text = "35212 - Rapporter lagres i"
        '
        'cmdShow
        '
        Me.cmdShow.BackColor = System.Drawing.SystemColors.Control
        Me.cmdShow.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdShow.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdShow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdShow.Location = New System.Drawing.Point(36, 83)
        Me.cmdShow.Name = "cmdShow"
        Me.cmdShow.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdShow.Size = New System.Drawing.Size(100, 24)
        Me.cmdShow.TabIndex = 40
        Me.cmdShow.Text = "50500-Vis"
        Me.cmdShow.UseVisualStyleBackColor = False
        '
        'cmdClose
        '
        Me.cmdClose.BackColor = System.Drawing.SystemColors.Control
        Me.cmdClose.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClose.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdClose.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClose.Location = New System.Drawing.Point(240, 83)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClose.Size = New System.Drawing.Size(100, 24)
        Me.cmdClose.TabIndex = 39
        Me.cmdClose.Text = "35118-Lukk"
        Me.cmdClose.UseVisualStyleBackColor = False
        '
        'cmdSave
        '
        Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSave.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSave.Location = New System.Drawing.Point(138, 83)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSave.Size = New System.Drawing.Size(100, 24)
        Me.cmdSave.TabIndex = 38
        Me.cmdSave.Text = "55034-Lagre"
        Me.cmdSave.UseVisualStyleBackColor = False
        '
        'frmVisma_ReportSetup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(405, 124)
        Me.Controls.Add(Me.cmdShow)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.cmdFileOpen)
        Me.Controls.Add(Me.txtReportPath)
        Me.Controls.Add(Me.lblReportPath)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(8, 30)
        Me.Name = "frmVisma_ReportSetup"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "35213-Reportsetup"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents cmdShow As System.Windows.Forms.Button
    Public WithEvents cmdClose As System.Windows.Forms.Button
    Public WithEvents cmdSave As System.Windows.Forms.Button
#End Region 
End Class