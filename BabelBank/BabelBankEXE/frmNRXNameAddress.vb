Option Strict Off
Option Explicit On
Friend Class frmNRXNameAddress
	Inherits System.Windows.Forms.Form
	
	Public bStoreInfo As Boolean
	Private bChangesDone As Boolean
	Private sLocalName As String
	Private sLocalAdr1 As String
	Private sLocalAdr2 As String
	Private sLocalAdr3 As String
	Private sLocalZip As String
	Private sLocalCity As String
	Private iLocalType As Short
	Private iLocalCompanyType As Short
	Private bExit As Boolean
    'XNET 27.06.2011 - 2 new variables
    Public sSpecial As String
    Private sLocalGender As String

	'UPGRADE_WARNING: Event cmbType.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub cmbType_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbType.SelectedIndexChanged
		
        If Me.cmbType.SelectedIndex = 0 Then
            'XNET 27.06.2011 Added next IF
            If sSpecial = "NORSK_FOLKEHJELP" Then
                Me.cmbCompanyType.Enabled = False
                Me.cmbCompanyType.Visible = False
                Me.txtGender.Enabled = True
                Me.txtGender.Visible = True
                Me.lblCompanyType.Text = "Kj�nn:"
            Else
                Me.cmbCompanyType.Visible = False
                Me.lblCompanyType.Visible = False
            End If
        Else
            'XNET 27.06.2011 Added next IF
            If sSpecial = "NORSK_FOLKEHJELP" Then
                Me.cmbCompanyType.Enabled = True
                Me.cmbCompanyType.Visible = True
                Me.txtGender.Enabled = False
                Me.txtGender.Visible = False
                Me.lblCompanyType.Text = "Type bedrift:"
            Else
                Me.cmbCompanyType.Visible = True
                Me.lblCompanyType.Visible = True
            End If
        End If

    End Sub
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		
		'Check if chages are done
		bExit = True
		CheckIfChangesAreDone()
		If bExit = True Then
			Me.Hide()
		End If
		
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		
		bStoreInfo = True
		Me.Hide()
		
	End Sub
	
	Private Sub cmdUse_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUse.Click
		
		StoreLocalVariables()
		bStoreInfo = True
		
	End Sub
	
	
	
	Public Sub StoreLocalVariables()
		
		sLocalName = Trim(Me.txtName.Text)
		sLocalAdr1 = Trim(Me.txtAddress1.Text)
		sLocalAdr2 = Trim(Me.txtAddress2.Text)
		sLocalAdr3 = Trim(Me.txtAddress3.Text)
		sLocalZip = Trim(Me.txtZipCode.Text)
		sLocalCity = Trim(Me.txtCity.Text)
        'XNET - 27.06.2011
        sLocalGender = Trim$(Me.txtGender.Text)
        iLocalType = cmbType.SelectedIndex
		iLocalCompanyType = cmbCompanyType.SelectedIndex
		
	End Sub
	
	Private Sub ResetVariables()
		
		Me.txtName.Text = sLocalName
		Me.txtAddress1.Text = sLocalAdr1
		Me.txtAddress2.Text = sLocalAdr2
		Me.txtAddress3.Text = sLocalAdr3
		Me.txtZipCode.Text = sLocalZip
		Me.txtCity.Text = sLocalCity
        'XNET - 27.06.2011
        Me.txtGender.Text = sLocalGender
        cmbType.SelectedIndex = iLocalType
		cmbCompanyType.SelectedIndex = iLocalCompanyType
		
	End Sub
	
	
	Private Sub CheckIfChangesAreDone()
		Dim bChangesDone As Boolean
		Dim iResponse As Short
		
		bChangesDone = False
		
		If sLocalName <> Trim(Me.txtName.Text) Then
			bChangesDone = True
		End If
		If sLocalAdr1 <> Trim(Me.txtAddress1.Text) Then
			bChangesDone = True
		End If
		If sLocalAdr2 <> Trim(Me.txtAddress2.Text) Then
			bChangesDone = True
		End If
		If sLocalAdr3 <> Trim(Me.txtAddress3.Text) Then
			bChangesDone = True
		End If
		If sLocalZip <> Trim(Me.txtZipCode.Text) Then
			bChangesDone = True
		End If
		If sLocalCity <> Trim(Me.txtCity.Text) Then
			bChangesDone = True
		End If
        'XNET - 27.06.2011
        If sSpecial = "NORSK_FOLKEHJELP" Then
            If iLocalType <> cmbType.SelectedIndex Then
                bChangesDone = True
            End If
            If Me.cmbType.SelectedIndex = 0 Then
                If sLocalGender <> Trim$(Me.txtGender.Text) Then
                    bChangesDone = True
                End If
            Else
                If iLocalCompanyType <> cmbCompanyType.SelectedIndex Then
                    bChangesDone = True
                End If
            End If
        Else
            If iLocalType <> cmbType.SelectedIndex Then
                bChangesDone = True
            End If
            If iLocalCompanyType <> cmbCompanyType.SelectedIndex Then
                bChangesDone = True
            End If
        End If

        If bChangesDone Then
            iResponse = MsgBox("Informasjonen er endret." & vbCrLf & "�nsker du � avbryte uten � lagre informasjonen?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "INFORMASJON ENDRET")
            If iResponse = MsgBoxResult.Yes Then
                'Have to reset values to how they were before user cancelled
                ' Yhis means we shall store changed information
                ResetVariables()
                bExit = True
            Else
                'NOTHING TO DO
                bExit = False
            End If

        End If
		
		
	End Sub
	
	Private Sub frmNRXNameAddress_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		FormvbStyle(Me, "dollar.jpg", 550)
		FormLRSCaptions(Me)
		
	End Sub
	
	Private Sub txtAddress1_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAddress1.Enter
		
		txtAddress1.SelectionStart = 0
		txtAddress1.SelectionLength = Len(txtAddress1.Text)
		
	End Sub
	Private Sub txtAddress2_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAddress2.Enter
		
		txtAddress2.SelectionStart = 0
		txtAddress2.SelectionLength = Len(txtAddress2.Text)
		
	End Sub
	Private Sub txtAddress3_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAddress3.Enter
		
		txtAddress3.SelectionStart = 0
		txtAddress3.SelectionLength = Len(txtAddress3.Text)
		
	End Sub
	Private Sub txtZipCode_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtZipCode.Enter
		
		txtZipCode.SelectionStart = 0
		txtZipCode.SelectionLength = Len(txtZipCode.Text)
		
	End Sub
	Private Sub txtCity_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCity.Enter
		
		txtCity.SelectionStart = 0
		txtCity.SelectionLength = Len(txtCity.Text)
		
	End Sub
	
	Private Sub txtName_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtName.Enter
		
		txtName.SelectionStart = 0
		txtName.SelectionLength = Len(txtName.Text)
		
    End Sub
    'XNET - 27.06.2011
    Private Sub txtGender_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGender.Enter

        txtGender.SelectionStart = 0
        txtGender.SelectionLength = Len(txtGender.Text)

    End Sub
    'XNET - 27.06.2011
    Private Sub txtGender_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGender.Leave
        If txtGender.Text <> "I" And txtGender.Text <> "K" And txtGender.Text <> "M" Then
            MsgBox("Ugyldig verdi lagt inn som kj�nn." & vbCrLf & vbCrLf & "Gyldige verdier er:" & vbCrLf & "I = Kj�nn ikke speisfisert/Not specified" & vbCrLf & "K = Kvinne/Female" & vbCrLf & "M = Mann/Male", vbCritical + vbOKOnly, "Feil verdi")
            txtGender.Focus()
        End If
    End Sub
End Class
