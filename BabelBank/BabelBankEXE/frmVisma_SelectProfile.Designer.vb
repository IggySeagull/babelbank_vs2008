<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmVisma_SelectProfile
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents lstProfiles As System.Windows.Forms.ListBox
	Public WithEvents txtAccountNumber As System.Windows.Forms.TextBox
	Public WithEvents txtBankPartner As System.Windows.Forms.TextBox
	Public WithEvents txtClientNumber As System.Windows.Forms.TextBox
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lblAccount As System.Windows.Forms.Label
	Public WithEvents lblBankPartner As System.Windows.Forms.Label
	Public WithEvents lblClientNumber As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lstProfiles = New System.Windows.Forms.ListBox
        Me.txtAccountNumber = New System.Windows.Forms.TextBox
        Me.txtBankPartner = New System.Windows.Forms.TextBox
        Me.txtClientNumber = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblAccount = New System.Windows.Forms.Label
        Me.lblBankPartner = New System.Windows.Forms.Label
        Me.lblClientNumber = New System.Windows.Forms.Label
        Me.cmdSelect = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lstProfiles
        '
        Me.lstProfiles.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.lstProfiles.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstProfiles.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstProfiles.Location = New System.Drawing.Point(14, 136)
        Me.lstProfiles.Name = "lstProfiles"
        Me.lstProfiles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstProfiles.Size = New System.Drawing.Size(273, 95)
        Me.lstProfiles.TabIndex = 1
        '
        'txtAccountNumber
        '
        Me.txtAccountNumber.AcceptsReturn = True
        Me.txtAccountNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.txtAccountNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAccountNumber.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAccountNumber.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAccountNumber.Location = New System.Drawing.Point(166, 80)
        Me.txtAccountNumber.MaxLength = 0
        Me.txtAccountNumber.Name = "txtAccountNumber"
        Me.txtAccountNumber.ReadOnly = True
        Me.txtAccountNumber.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAccountNumber.Size = New System.Drawing.Size(121, 21)
        Me.txtAccountNumber.TabIndex = 8
        Me.txtAccountNumber.TabStop = False
        Me.txtAccountNumber.Text = "32011034567"
        '
        'txtBankPartner
        '
        Me.txtBankPartner.AcceptsReturn = True
        Me.txtBankPartner.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.txtBankPartner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBankPartner.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBankPartner.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBankPartner.Location = New System.Drawing.Point(166, 56)
        Me.txtBankPartner.MaxLength = 0
        Me.txtBankPartner.Name = "txtBankPartner"
        Me.txtBankPartner.ReadOnly = True
        Me.txtBankPartner.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBankPartner.Size = New System.Drawing.Size(121, 21)
        Me.txtBankPartner.TabIndex = 6
        Me.txtBankPartner.TabStop = False
        Me.txtBankPartner.Text = "1"
        '
        'txtClientNumber
        '
        Me.txtClientNumber.AcceptsReturn = True
        Me.txtClientNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.txtClientNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtClientNumber.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtClientNumber.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtClientNumber.Location = New System.Drawing.Point(166, 32)
        Me.txtClientNumber.MaxLength = 0
        Me.txtClientNumber.Name = "txtClientNumber"
        Me.txtClientNumber.ReadOnly = True
        Me.txtClientNumber.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtClientNumber.Size = New System.Drawing.Size(121, 21)
        Me.txtClientNumber.TabIndex = 4
        Me.txtClientNumber.TabStop = False
        Me.txtClientNumber.Text = "5"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(16, 112)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(137, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "35017-Profilgruppe"
        '
        'lblAccount
        '
        Me.lblAccount.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.lblAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAccount.Location = New System.Drawing.Point(16, 80)
        Me.lblAccount.Name = "lblAccount"
        Me.lblAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAccount.Size = New System.Drawing.Size(137, 13)
        Me.lblAccount.TabIndex = 7
        Me.lblAccount.Text = "60022 Kontonummer:"
        '
        'lblBankPartner
        '
        Me.lblBankPartner.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.lblBankPartner.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBankPartner.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBankPartner.Location = New System.Drawing.Point(16, 56)
        Me.lblBankPartner.Name = "lblBankPartner"
        Me.lblBankPartner.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBankPartner.Size = New System.Drawing.Size(137, 13)
        Me.lblBankPartner.TabIndex = 5
        Me.lblBankPartner.Text = "35001-Bankpartner:"
        '
        'lblClientNumber
        '
        Me.lblClientNumber.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.lblClientNumber.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblClientNumber.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblClientNumber.Location = New System.Drawing.Point(16, 32)
        Me.lblClientNumber.Name = "lblClientNumber"
        Me.lblClientNumber.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblClientNumber.Size = New System.Drawing.Size(137, 13)
        Me.lblClientNumber.TabIndex = 0
        Me.lblClientNumber.Text = "60021-Klientnummer:"
        '
        'cmdSelect
        '
        Me.cmdSelect.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSelect.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSelect.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSelect.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSelect.Location = New System.Drawing.Point(187, 237)
        Me.cmdSelect.Name = "cmdSelect"
        Me.cmdSelect.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSelect.Size = New System.Drawing.Size(100, 24)
        Me.cmdSelect.TabIndex = 12
        Me.cmdSelect.Text = "55018-Velg"
        Me.cmdSelect.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(81, 237)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(100, 24)
        Me.cmdCancel.TabIndex = 11
        Me.cmdCancel.Text = "55006-Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'frmVisma_SelectProfile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(312, 276)
        Me.Controls.Add(Me.cmdSelect)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.lstProfiles)
        Me.Controls.Add(Me.txtAccountNumber)
        Me.Controls.Add(Me.txtBankPartner)
        Me.Controls.Add(Me.txtClientNumber)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblAccount)
        Me.Controls.Add(Me.lblBankPartner)
        Me.Controls.Add(Me.lblClientNumber)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Location = New System.Drawing.Point(3, 25)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmVisma_SelectProfile"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "35003-Velg forbindelse"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents cmdSelect As System.Windows.Forms.Button
    Public WithEvents cmdCancel As System.Windows.Forms.Button
#End Region 
End Class