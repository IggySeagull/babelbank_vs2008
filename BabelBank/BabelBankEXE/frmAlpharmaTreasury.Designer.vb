<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmAlpharmaTreasury
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtText As System.Windows.Forms.TextBox
	Public WithEvents cmdContinue As System.Windows.Forms.Button
	Public WithEvents lstPayments As System.Windows.Forms.ListBox
	Public WithEvents lblText As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAlpharmaTreasury))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtText = New System.Windows.Forms.TextBox
		Me.cmdContinue = New System.Windows.Forms.Button
		Me.lstPayments = New System.Windows.Forms.ListBox
		Me.lblText = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.Text = "Endre melding til mottaker"
		Me.ClientSize = New System.Drawing.Size(658, 282)
		Me.Location = New System.Drawing.Point(4, 30)
		Me.ControlBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmAlpharmaTreasury"
		Me.txtText.AutoSize = False
		Me.txtText.Size = New System.Drawing.Size(529, 58)
		Me.txtText.Location = New System.Drawing.Point(12, 181)
		Me.txtText.Maxlength = 140
		Me.txtText.MultiLine = True
		Me.txtText.TabIndex = 1
		Me.txtText.AcceptsReturn = True
		Me.txtText.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtText.BackColor = System.Drawing.SystemColors.Window
		Me.txtText.CausesValidation = True
		Me.txtText.Enabled = True
		Me.txtText.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtText.HideSelection = True
		Me.txtText.ReadOnly = False
		Me.txtText.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtText.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtText.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtText.TabStop = True
		Me.txtText.Visible = True
		Me.txtText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtText.Name = "txtText"
		Me.cmdContinue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdContinue.Text = "Fortsett"
		Me.cmdContinue.Size = New System.Drawing.Size(99, 24)
		Me.cmdContinue.Location = New System.Drawing.Point(551, 247)
		Me.cmdContinue.TabIndex = 2
		Me.cmdContinue.TabStop = False
		Me.cmdContinue.BackColor = System.Drawing.SystemColors.Control
		Me.cmdContinue.CausesValidation = True
		Me.cmdContinue.Enabled = True
		Me.cmdContinue.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdContinue.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdContinue.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdContinue.Name = "cmdContinue"
		Me.lstPayments.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lstPayments.Size = New System.Drawing.Size(639, 133)
		Me.lstPayments.Location = New System.Drawing.Point(11, 11)
		Me.lstPayments.TabIndex = 0
		Me.lstPayments.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lstPayments.BackColor = System.Drawing.SystemColors.Window
		Me.lstPayments.CausesValidation = True
		Me.lstPayments.Enabled = True
		Me.lstPayments.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lstPayments.IntegralHeight = True
		Me.lstPayments.Cursor = System.Windows.Forms.Cursors.Default
		Me.lstPayments.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.lstPayments.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lstPayments.Sorted = False
		Me.lstPayments.TabStop = True
		Me.lstPayments.Visible = True
		Me.lstPayments.MultiColumn = False
		Me.lstPayments.Name = "lstPayments"
		Me.lblText.Text = "Melding til mottaker:"
		Me.lblText.Size = New System.Drawing.Size(283, 19)
		Me.lblText.Location = New System.Drawing.Point(13, 159)
		Me.lblText.TabIndex = 3
		Me.lblText.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblText.BackColor = System.Drawing.SystemColors.Control
		Me.lblText.Enabled = True
		Me.lblText.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblText.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblText.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblText.UseMnemonic = True
		Me.lblText.Visible = True
		Me.lblText.AutoSize = False
		Me.lblText.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblText.Name = "lblText"
		Me.Controls.Add(txtText)
		Me.Controls.Add(cmdContinue)
		Me.Controls.Add(lstPayments)
		Me.Controls.Add(lblText)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
