Option Strict Off
Option Explicit On

Friend Class frmMATCH_ManualSetup
	Inherits System.Windows.Forms.Form
	Dim bCanceled As Boolean
    Private Sub chkMatchARItem_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkMatchARItem.CheckStateChanged

        If chkMatchARItem.CheckState = 1 Then
            Me.txtERPID.Enabled = True
            Me.chkExportOnlyMacthed.Enabled = True
        Else
            Me.txtERPID.Enabled = False
            Me.chkExportOnlyMacthed.Enabled = False
        End If

    End Sub
    'XokNET - 08.02.2013 - New Sub
    Private Sub chkValidateMatching_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkvalidatematching.Click

        If chkValidateMatching.Checked Then
            Me.chkReportDeviations.Enabled = True
        Else
            Me.chkReportDeviations.Enabled = False
        End If

    End Sub

	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		bCanceled = True
		Me.Hide()
		
	End Sub
	
	Friend Function WasFormCanceled() As Boolean
		
		WasFormCanceled = bCanceled
		
	End Function

	Private Sub cmdFileSwap_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileSwap.Click
        ' Fill special form frmSwapInFiles
        Dim aFormatsAndProfiles(,) As Object
		Dim iFormatID As Short
		Dim i As Short
		Dim aSwapFileArray(,) As String
        Dim bEmptyArray As Boolean
		
        aSwapFileArray = RestoreSwapFiles("1", bEmptyArray, False)

		' will pr now return only one SwapFile, or Zero for new SwapFiles
		If Not bEmptyArray Then
            For i = 0 To UBound(aSwapFileArray, 2)
                frmSwapInFiles.txtName.Text = aSwapFileArray(0, i)
                frmSwapInFiles.txtFileName.Text = aSwapFileArray(1, i)
                ' find format
                iFormatID = Val(aSwapFileArray(2, i))
                frmSwapInFiles.chkAllowDiff.CheckState = bVal(aSwapFileArray(3, i))
                frmSwapInFiles.chkSwapAuto.CheckState = bVal(aSwapFileArray(4, i))
                frmSwapInFiles.txtDiffAccount.Text = aSwapFileArray(5, i)
                'Add accounttype here, must also change in the restore.. function
                frmSwapInFiles.cmbDiffType.Items.Clear()
                frmSwapInFiles.cmbDiffType.Items.Add(LRS(60279)) '"Kundereskontro 1"
                frmSwapInFiles.cmbDiffType.Items.Add(LRS(60280)) '"Hovedbokskonto 2"
                frmSwapInFiles.cmbDiffType.Items.Add(LRS(60281)) '"KID 3"
                frmSwapInFiles.cmbDiffType.Items.Add(LRS(60282)) '"Fra ERP 4"
                frmSwapInFiles.cmbDiffType.Items.Add(LRS(60283)) '"Don't adjust invoice"
                ' select correct type
                frmSwapInFiles.cmbDiffType.SelectedIndex = aSwapFileArray(6, i) - 1
                frmSwapInFiles.txtSQLName.Text = aSwapFileArray(7, i)
                frmSwapInFiles.chkRemoveAtExport.CheckState = bVal(aSwapFileArray(9, i))
                'frmSwapInFiles.cmbSelectSwapSetup.SelectedIndex = 0
                'Exit For 'To be removed when we may have more than 1 swap files
            Next i
		End If
		
		' Fill array with formats
        aFormatsAndProfiles = ArrayFormat(False, "", True, False)

        ' Empty listbox, in case we skip back and forth ..
		frmSwapInFiles.cmbFormat.Items.Clear()
		' fill listbox:
		For i = 0 To UBound(aFormatsAndProfiles, 2)
            frmSwapInFiles.cmbFormat.Items.Add((aFormatsAndProfiles(0, i) + Space(100) + Str(aFormatsAndProfiles(1, i))))
            If aFormatsAndProfiles(1, i) = iFormatID Then
                frmSwapInFiles.cmbFormat.SelectedIndex = frmSwapInFiles.cmbFormat.Items.Count - 1
            End If
		Next i
		
		' show form with setup for swapping info in one file with content from other file.
		FormSelectAllTextboxs(Me)
		VB6.ShowForm(frmSwapInFiles, 1, Me)
	End Sub
	
	Private Sub cmdOmitAccounts_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOmitAccounts.Click
        'Fill form frmOmitAccounts
        Dim i As Short
		Dim aOmitAccounts(,) As String
        Dim bEmptyArray As Boolean
        Dim frmOmitAccounts As New frmOmitAccounts

        aOmitAccounts = RestoreOmitAccounts("1", bEmptyArray) 'TODO: Hardcoded CompanyNo
		
		If Not bEmptyArray Then
            For i = 0 To UBound(aOmitAccounts, 2)
                If aOmitAccounts(1, i) = "True" Then
                    frmOmitAccounts.lstAutomaticMatching.Items.Add(aOmitAccounts(0, i))
                End If
                If aOmitAccounts(2, i) = "True" Then
                    frmOmitAccounts.lstAfterMatching.Items.Add(aOmitAccounts(0, i))
                End If
            Next i
		End If
		
        frmOmitAccounts.ShowDialog()
        'VB6.ShowForm(frmOmitAccounts, 1, Me)

        frmOmitAccounts.Dispose()
		
	End Sub
	
    Private Sub cmdChangeLabels_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdChangeLabels.Click
        Dim formLabels As New frmLabels

        formLabels.SetCompany_ID("1") 'TODO - Hardcoded CompanyID
        formLabels.ShowDialog()
        'VB6.ShowForm(frmOmitAccounts, 1, Me)

        formLabels.Dispose()

    End Sub
    Private Sub cmdPWStatistics_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPWStatistics.Click
        ' call form with password for Statistics Details
        frmPasswordStatistics.ShowDialog()
    End Sub
	
	Private Sub frmMATCH_ManualSetup_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Dim bJumpToNext As Boolean
		Dim bAskWhenRest As Boolean
		Dim sMatchedColumns As String
		Dim sColumnsToEnterData As String
		Dim sERPPaymentID As String
		Dim bManMatchFromERP As Boolean
		Dim bOmitPayments As Boolean
		Dim bDeductMatched As Boolean
		Dim bAllowDifferenceInCurrency As Boolean 'New 09.08.2007
		Dim i As Integer
		Dim bBckBeforeExport, bCreateBackup, bBckBeforeImport, bBckAfterExport As Boolean
		Dim sExportBackuppath As String
		Dim iExportBackupGenerations As Short
        Dim bExportOnlyMacthedPayments As Boolean
        Dim bValidateMatching As Boolean
        Dim bReportDeviations As Boolean
        Dim bUnDoDoubleMatched As Boolean
        Dim bUnDoAllDoubleMatched As Boolean

		' added 30.12.2009
		Dim bActivateStatistics As Boolean
		Dim sPWStatistics As String
		
        Dim sBBRET_CustomerNoLabel As String = ""
        Dim sBBRET_InvoiceNoLabel As String = ""
        Dim sBBRET_ClientNoLabel As String = ""
        Dim sBBRET_ExchangeRateLabel As String = ""
        Dim sBBRET_VendroNoLabel As String = ""
        Dim sBBRET_CurrencyLabel As String = ""
        Dim sBBRET_GeneralLedgerLabel As String = ""
        Dim sBBRET_VoucherNoLabel As String = ""
        Dim sBBRET_NameLabel As String = ""
        Dim sBBRET_FreetextLabel As String = ""
        Dim sBBRET_CurrencyAmountLabel As String = ""
        Dim sBBRET_Currency2Label As String = ""
        Dim sBBRET_DiscountLabel As String = ""
        Dim sBBRET_BackPaymentLabel As String = ""
        Dim sBBRET_MyFieldLabel As String = ""
        Dim sBBRET_MyField2Label As String = ""
        Dim sBBRET_MyField3Label As String = ""
        Dim sBBRET_Dim1Label As String = ""
        Dim sBBRET_Dim2Label As String = ""
        Dim sBBRET_Dim3Label As String = ""
        Dim sBBRET_Dim4Label As String = ""
        Dim sBBRET_Dim5Label As String = ""
        Dim sBBRET_Dim6Label As String = ""
        Dim sBBRET_Dim7Label As String = ""
        Dim sBBRET_Dim8Label As String = ""
        Dim sBBRET_Dim9Label As String = ""
        Dim sBBRET_Dim10Label As String = ""
        ' 27.01.2017
        Dim sHistoryDays As String = ""

        FormvbStyle(Me, "dollar.jpg")
		FormLRSCaptions(Me)

        sColumnsToEnterData = ""
        sERPPaymentID = ""
        sPWStatistics = ""

        If Not RetrieveLabels(Nothing, "1", sBBRET_CustomerNoLabel, sBBRET_InvoiceNoLabel, sBBRET_ClientNoLabel, _
                      sBBRET_VendroNoLabel, sBBRET_CurrencyLabel, sBBRET_ExchangeRateLabel, sBBRET_GeneralLedgerLabel, sBBRET_VoucherNoLabel, sBBRET_NameLabel, _
                      sBBRET_FreetextLabel, sBBRET_CurrencyAmountLabel, sBBRET_Currency2Label, sBBRET_DiscountLabel, sBBRET_BackPaymentLabel, sBBRET_MyFieldLabel, _
                      sBBRET_MyField2Label, sBBRET_MyField3Label, sBBRET_Dim1Label, sBBRET_Dim2Label, sBBRET_Dim3Label, sBBRET_Dim4Label, _
                      sBBRET_Dim5Label, sBBRET_Dim6Label, sBBRET_Dim7Label, sBBRET_Dim8Label, sBBRET_Dim9Label, sBBRET_Dim10Label) Then

        End If

        ' Return saved setupvalues from database:
        RestoreManualSetup(bJumpToNext, sMatchedColumns, sColumnsToEnterData, bOmitPayments, bDeductMatched, bAllowDifferenceInCurrency, sERPPaymentID, bManMatchFromERP, bExportOnlyMacthedPayments, bActivateStatistics, sPWStatistics, , , , , bValidateMatching, bReportDeviations, bUnDoDoubleMatched, bUnDoAllDoubleMatched, sHistoryDays)

        chkJumpToNext.CheckState = bVal(bJumpToNext)
        chkAskWhenRest.CheckState = bVal(bAskWhenRest)
        chkActiveOmitPayments.CheckState = bVal(bOmitPayments)
        Me.chkDeductPreviousMatched.CheckState = bVal(bDeductMatched)
        Me.chkUseReceivedAmount.CheckState = bVal(bAllowDifferenceInCurrency)
        If UseCommit(1) Then 'TODO: Hardcoded CompanyID
            Me.chkCommitTrans.CheckState = bVal(CBool("True"))
        Else
            Me.chkCommitTrans.CheckState = bVal(CBool("False"))
        End If
        If bUnDoDoubleMatched Then
            Me.ChkDoubleMatching.CheckState = CheckState.Checked ' bVal(CBool("True"))
        Else
            Me.ChkDoubleMatching.CheckState = CheckState.Unchecked ' bVal(CBool("False"))
        End If
        If bUnDoAllDoubleMatched Then
            Me.chkDoubleMatchingUndoAll.CheckState = CheckState.Checked ' bVal(CBool("True"))
        Else
            Me.chkDoubleMatchingUndoAll.CheckState = CheckState.Unchecked 'bVal(CBool("False"))
        End If

        ' added 30.12.2009
        Me.chkStatistics.CheckState = bVal(bActivateStatistics)
        ' added 05.01.2010
        frmPasswordStatistics.PWOld(sPWStatistics)

        'NBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBN!!!!!!!!!!!!!!!!!
        'IF YOU DO CHANGES HERE, CHECK ALSO
        ' BABELSETUP - FRMSQLSTATEMENTS - OPTVALIDATION_CLICK
        'NBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBNBN!!!!!!!!!!!!!!!!!

        ' Set which columns to show in sprMatched
        ' 12.07.2006 Added External Clientnumber
        lstMatchedColumns.Items.Add(sBBRET_ClientNoLabel) 'Clientno. /Klientnr.  1
        lstMatchedColumns.Items.Add(sBBRET_CustomerNoLabel) 'Customerno. /Kundenr.  2
        lstMatchedColumns.Items.Add(sBBRET_InvoiceNoLabel) 'Invoiceno. /Fakturanr.   3
        lstMatchedColumns.Items.Add(LRSCommon(41003)) 'Debit /Debet      4
        lstMatchedColumns.Items.Add(LRSCommon(41004)) 'Credit /Kredit     5
        lstMatchedColumns.Items.Add(sBBRET_CurrencyLabel) 'Currency /Valuta   6
        ' New 10.03.2004
        lstMatchedColumns.Items.Add(sBBRET_ExchangeRateLabel) 'Exch.rate /Val.kurs 8
        ' New 10.03.2004
        lstMatchedColumns.Items.Add(sBBRET_GeneralLedgerLabel) 'GL Account /H.Bok 7
        lstMatchedColumns.Items.Add(sBBRET_VendroNoLabel) 'Supplierno. /Levnr.  9
        lstMatchedColumns.Items.Add(sBBRET_VoucherNoLabel) 'Voucherno. /Bilagsnr.  10
        'New 19.12.2006
        lstMatchedColumns.Items.Add(sBBRET_NameLabel) 'Name /Navn   11
        'New 18.10.2007
        lstMatchedColumns.Items.Add(sBBRET_DiscountLabel) 'Rabatt   12
        lstMatchedColumns.Items.Add(sBBRET_FreetextLabel) 'Freetext /Fritekst   13
        ' 03.05.05
        ' New columns, for Lindorff
        lstMatchedColumns.Items.Add("Innbet.kode") '    14
        lstMatchedColumns.Items.Add("Rolle") '    15

        ' 25.01.06
        ' New columns, for NRX
        lstMatchedColumns.Items.Add("Not in use") 'MatchID    16
        lstMatchedColumns.Items.Add("Not in use") 'AkontoID    17
        lstMatchedColumns.Items.Add(sBBRET_MyFieldLabel) 'MyField    18

        lstMatchedColumns.Items.Add("Not in use")   ' 20 lColMatchedPartlyMatched
        lstMatchedColumns.Items.Add("Not in use")   ' 21 lColMatchedOriginalAmount
        lstMatchedColumns.Items.Add("Not in use")   ' 22 lColMatchedVerification

        'added 11.12.2014
        lstMatchedColumns.Items.Add(sBBRET_CurrencyAmountLabel)   ' Valutabel�p 22
        lstMatchedColumns.Items.Add(sBBRET_Currency2Label)   ' Valuta2     23
        lstMatchedColumns.Items.Add(sBBRET_Dim1Label)             ' Dim 1       24
        lstMatchedColumns.Items.Add(sBBRET_Dim2Label)             ' Dim 2       25
        lstMatchedColumns.Items.Add(sBBRET_Dim3Label)             ' Dim 3       26
        lstMatchedColumns.Items.Add(sBBRET_Dim4Label)             ' Dim 4       27
        lstMatchedColumns.Items.Add(sBBRET_Dim5Label)             ' Dim 5       28
        lstMatchedColumns.Items.Add(sBBRET_Dim6Label)             ' Dim 6       29
        lstMatchedColumns.Items.Add(sBBRET_Dim7Label)             ' Dim 7       30
        lstMatchedColumns.Items.Add(sBBRET_Dim8Label)             ' Dim 8       31
        lstMatchedColumns.Items.Add(sBBRET_Dim9Label)             ' Dim 9       32
        lstMatchedColumns.Items.Add(sBBRET_Dim10Label)            ' Dim 10      33
        '26.03.2015
        lstMatchedColumns.Items.Add(sBBRET_MyField2Label)            ' MyField2      34
        lstMatchedColumns.Items.Add(sBBRET_MyField3Label)            ' MyField3      35
        lstMatchedColumns.Items.Add(sBBRET_BackPaymentLabel)            ' BackpaymentAccount      36


        For i = 1 To Len(sMatchedColumns)
            If Mid(sMatchedColumns, i, 1) = "1" Then
                lstMatchedColumns.SetItemChecked(i - 1, True)
            Else
                lstMatchedColumns.SetItemChecked(i - 1, False)
            End If
        Next i

        lstMatchedColumns.SelectedIndex = -1 'No item selected

        ' Set which columns to show in sprMatched
        ' 12.07.2006 Added External Clientnumber
        lstColumnsEnterSequence.Items.Add(sBBRET_ClientNoLabel) 'Clientno. /Klientnr.  1
        lstColumnsEnterSequence.Items.Add(sBBRET_CustomerNoLabel) 'CustomerNo  2
        lstColumnsEnterSequence.Items.Add(sBBRET_InvoiceNoLabel) 'InvoiceNo   3
        lstColumnsEnterSequence.Items.Add((LRSCommon(41003))) 'Debit       4
        lstColumnsEnterSequence.Items.Add((LRSCommon(41004))) 'Credit      5
        lstColumnsEnterSequence.Items.Add(sBBRET_CurrencyLabel) 'Currency    6
        lstColumnsEnterSequence.Items.Add(sBBRET_ExchangeRateLabel) 'ExchangeRate  8
        lstColumnsEnterSequence.Items.Add(sBBRET_GeneralLedgerLabel) 'Ledger Account 7
        lstColumnsEnterSequence.Items.Add(sBBRET_VendroNoLabel) 'SupplierNo  9
        lstColumnsEnterSequence.Items.Add(sBBRET_VoucherNoLabel) 'VoucherNo   10
        'New 19.12.2006
        lstColumnsEnterSequence.Items.Add(sBBRET_NameLabel) 'Name /Navn   11
        lstColumnsEnterSequence.Items.Add(sBBRET_DiscountLabel) 'Discount /Rabatt   12
        lstColumnsEnterSequence.Items.Add(sBBRET_FreetextLabel) 'Freetext    13
        ' 03.05.05
        ' New columns, for Lindorff
        lstColumnsEnterSequence.Items.Add("Innbet.kode") '    14
        lstColumnsEnterSequence.Items.Add("Rolle") '    15
        ' 25.01.06
        ' New columns, for NRX
        lstColumnsEnterSequence.Items.Add("Not in use") 'MatchID    16
        lstColumnsEnterSequence.Items.Add("Not in use") 'AkontoID    17
        lstColumnsEnterSequence.Items.Add(sBBRET_MyFieldLabel) 'MyField    18

        lstColumnsEnterSequence.Items.Add("Not in use")   ' 20 lColMatchedPartlyMatched
        lstColumnsEnterSequence.Items.Add("Not in use")   ' 21 lColMatchedOriginalAmount
        lstColumnsEnterSequence.Items.Add("Not in use")   ' 22 lColMatchedVerification

        'added 11.12.2014
        lstColumnsEnterSequence.Items.Add(sBBRET_CurrencyAmountLabel)   ' Valutabel�p 22
        lstColumnsEnterSequence.Items.Add(sBBRET_Currency2Label)   ' Valuta2     23
        lstColumnsEnterSequence.Items.Add(sBBRET_Dim1Label)             ' Dim 1       24
        lstColumnsEnterSequence.Items.Add(sBBRET_Dim2Label)             ' Dim 2       25
        lstColumnsEnterSequence.Items.Add(sBBRET_Dim3Label)             ' Dim 3       26
        lstColumnsEnterSequence.Items.Add(sBBRET_Dim4Label)             ' Dim 4       27
        lstColumnsEnterSequence.Items.Add(sBBRET_Dim5Label)             ' Dim 5       28
        lstColumnsEnterSequence.Items.Add(sBBRET_Dim6Label)             ' Dim 6       29
        lstColumnsEnterSequence.Items.Add(sBBRET_Dim7Label)             ' Dim 7       30
        lstColumnsEnterSequence.Items.Add(sBBRET_Dim8Label)             ' Dim 8       31
        lstColumnsEnterSequence.Items.Add(sBBRET_Dim9Label)             ' Dim 9       32
        lstColumnsEnterSequence.Items.Add(sBBRET_Dim10Label)            ' Dim 10      33
        'Added 26.03.2015
        lstColumnsEnterSequence.Items.Add(sBBRET_MyField2Label)             ' MyField2       34
        lstColumnsEnterSequence.Items.Add(sBBRET_MyField3Label)            ' MyField3      35
        lstColumnsEnterSequence.Items.Add(sBBRET_BackPaymentLabel)            ' BackpaymentAccount      35

        For i = 1 To Len(sColumnsToEnterData)
            If Mid(sColumnsToEnterData, i, 1) = "1" Then
                lstColumnsEnterSequence.SetItemChecked(i - 1, True)
            Else
                lstColumnsEnterSequence.SetItemChecked(i - 1, False)
            End If
        Next i

        lstColumnsEnterSequence.SelectedIndex = -1 'No item selected

        FormSelectAllTextboxs(Me) ' Set selection for all textboxes

        'Add info about backup of the BB database
        sExportBackuppath = ""
        GetBBExportBackupInfo(bCreateBackup, bBckBeforeImport, bBckBeforeExport, bBckAfterExport, sExportBackuppath, iExportBackupGenerations)

        txtERPID.Text = Trim(sERPPaymentID)
        chkMatchARItem.CheckState = bVal(bManMatchFromERP)
        'XokNET - 08.02.2013 - Added next 2
        chkValidateMatching.Checked = bVal(bValidateMatching)
        chkReportDeviations.Checked = bVal(bReportDeviations)

        Me.chkExportOnlyMacthed.CheckState = bVal(bExportOnlyMacthedPayments)
        If chkMatchARItem.CheckState = 1 Then
            Me.txtERPID.Enabled = True
            Me.chkExportOnlyMacthed.Enabled = True
        Else
            Me.txtERPID.Enabled = False
            Me.chkExportOnlyMacthed.Enabled = False
        End If

        'XokNET - 08.02.2013 - Added next IF
        If chkValidateMatching.Checked Then
            Me.chkReportDeviations.Enabled = True
        Else
            Me.chkReportDeviations.Enabled = False
        End If
        If Me.ChkDoubleMatching.Checked Then
            Me.chkDoubleMatchingUndoAll.Enabled = True
        Else
            Me.chkDoubleMatchingUndoAll.Enabled = False
        End If
        Me.txtHistoryDays.Text = sHistoryDays
        bCanceled = True

    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Dim sMatchedColumns As String
        Dim sColumnsToEnterData As String
        Dim i As Integer
        Dim bSave As Boolean

        sColumnsToEnterData = ""
        sMatchedColumns = ""
        bSave = True

        ' Check which columns are selected in listbox;
        For i = 0 To lstMatchedColumns.Items.Count - 1
            If lstMatchedColumns.GetItemChecked(i) Then
                sMatchedColumns = sMatchedColumns & "1"
                If lstColumnsEnterSequence.GetItemChecked(i) Then
                    sColumnsToEnterData = sColumnsToEnterData & "1"
                Else
                    sColumnsToEnterData = sColumnsToEnterData & "0"
                End If
            Else
                sMatchedColumns = sMatchedColumns & "0"
                'Can't select an item to enter data if its not shown in sprMatched
                sColumnsToEnterData = sColumnsToEnterData & "0"
            End If
        Next i

        If bSave Then
            SaveManualSetup(chkJumpToNext.Checked, sMatchedColumns, sColumnsToEnterData, txtERPID.Text, chkMatchARItem.Checked, Me.chkExportOnlyMacthed.Checked, chkActiveOmitPayments.Checked, Me.chkDeductPreviousMatched.Checked, Me.chkUseReceivedAmount.Checked, Me.chkStatistics.Checked, frmPasswordStatistics.PWNew, Me.chkCommitTrans.Checked, Me.chkValidateMatching.Checked, Me.chkReportDeviations.Checked, Me.txtHistoryDays.Text) 'XNET - 08.02.2013 - Added last 2
        End If

        frmPasswordStatistics.Close()

        bCanceled = False
        'Me.Hide

    End Sub
    'Public Function SaveManualSetup(ByRef bJumpToNext As Boolean, ByRef sMatchedColumns As String, ByRef sColumnsToEnterData As String, ByRef sPWStatistics As String, ByRef bValidateMatching As Boolean, ByRef bReportDeviations As Boolean) As Boolean
    Public Function SaveManualSetup(ByRef bJumpToNext As Boolean, ByRef sMatchedColumns As String, ByRef sColumnsToEnterData As String, ByRef sERPPaymentID As String, ByRef bManMatchFromERP As Boolean, ByRef bExportOnlyMatchedPayments As Boolean, ByRef bOmitPayments As Boolean, ByRef bDeductMatched As Boolean, ByRef bAllowDifferenceInCurrency As Boolean, ByRef bActivateStatistics As Boolean, ByRef sPWStatistics As String, ByRef bUseCommit As Boolean, ByRef bValidateMatching As Boolean, ByRef bReportDeviations As Boolean, ByRef sHistoryDays As String) As Boolean
        ' Save values to database
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bReturnValue As Boolean = True


        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            ' First, check if there is a record in ManualSetup:
            sMySQL = "Select * FROM MATCH_Manual"

            oMyDal.SQL = sMySQL
            If oMyDal.Reader_Execute() Then
                If oMyDal.Reader_HasRows Then
                    ' One record present, update:
                    sMySQL = "UPDATE MATCH_Manual SET JumpToNext = "
                    sMySQL = sMySQL & Str(bJumpToNext)
                    sMySQL = sMySQL & ", ActivateOmit = " & Str(bOmitPayments)
                    sMySQL = sMySQL & ", DeductMatched = " & Str(bDeductMatched)
                    sMySQL = sMySQL & ", AllowDifferenceInCurrency = " & Str(bAllowDifferenceInCurrency)
                    sMySQL = sMySQL & ", ActivateStatistics = " & Str(bActivateStatistics)
                    sMySQL = sMySQL & ", PWStatistics = '" & sPWStatistics & "' "
                    sMySQL = sMySQL & ", UseCommitTrans = " & Str(bUseCommit)
                    'XokNET - 08.02.2013 - Added next 2
                    sMySQL = sMySQL & ", ValidateMatching = " & Str(bValidateMatching) & " "
                    sMySQL = sMySQL & ", ReportDeviations = " & Str(bReportDeviations)
                    If sHistoryDays = "" Then
                        ' default
                        sHistoryDays = "0"
                    End If
                    sMySQL = sMySQL & ", HistoryDays = " & sHistoryDays
                    sMySQL = sMySQL & " WHERE Company_ID = 1" 'TODO - Hardcoded CompanyID

                    oMyDal.SQL = sMySQL
                    If oMyDal.ExecuteNonQuery Then
                        If oMyDal.RecordsAffected = 1 Then
                            bReturnValue = True
                        Else
                            Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                        End If
                    Else
                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                    End If

                    sMySQL = "UPDATE MATCH_Manual SET MatchedColumns = "
                    sMySQL = sMySQL & "'" & sMatchedColumns & "'"
                    sMySQL = sMySQL & ", ColumnsToEnterData = '" & sColumnsToEnterData & "'"
                    sMySQL = sMySQL & " WHERE Company_ID = 1" 'TODO - Hardcoded CompanyID
                    oMyDal.SQL = sMySQL
                    If oMyDal.ExecuteNonQuery Then
                        If oMyDal.RecordsAffected = 1 Then
                            bReturnValue = True
                        Else
                            Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                        End If
                    Else
                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                    End If
                Else
                    ' No records in ManualSetup, Insert
                    sMySQL = "INSERT INTO Match_Manual(Company_ID, Match_Id, In_Top, In_Left, In_Height, "
                    sMySQL = sMySQL & "In_Width , Seek_Top, Seek_Left, Seek_Height, Seek_Width, "
                    sMySQL = sMySQL & "Matched_Top, Matched_Left, Matched_Height, Matched_Width, AccountRestAmount, "
                    sMySQL = sMySQL & "JumpToNext, ActivateOmit, DeductMatched, AllowDifferenceInCurrency, ActivateStatistics, PWStatistics, UseCommitTrans, MatchedColumns, ColumnsToEnterData, HistoryDays) "
                    sMySQL = sMySQL & "VALUES(1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " 'TODO - Hardcoded CompanyID
                    sMySQL = sMySQL & Str(bJumpToNext) & ", " & Str(bOmitPayments) & ", " & Str(bDeductMatched) & ", " & Str(bAllowDifferenceInCurrency) & ", " & Str(bActivateStatistics) & ", '" & sPWStatistics & "', " & Str(bUseCommit) & ", '" & sMatchedColumns & " ', '" & sColumnsToEnterData & " ', '" & sHistoryDays & "')"
                    oMyDal.SQL = sMySQL
                    If oMyDal.ExecuteNonQuery Then
                        If oMyDal.RecordsAffected = 1 Then
                            bReturnValue = True
                        Else
                            Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                        End If
                    Else
                        Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
                    End If
                End If
            Else

                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)

            End If

            sMySQL = "UPDATE Company SET ERPPaymentID = "
            sMySQL = sMySQL & "'" & sERPPaymentID & "', ManMatchFromERP = "
            sMySQL = sMySQL & bManMatchFromERP & ", ExportOnlyMatchedPayments = "
            sMySQL = sMySQL & bExportOnlyMatchedPayments
            sMySQL = sMySQL & " WHERE Company_ID = 1" 'TODO - Hardcoded CompanyID
            oMyDal.SQL = sMySQL
            If oMyDal.ExecuteNonQuery Then
                If oMyDal.RecordsAffected = 1 Then
                    bReturnValue = True
                Else
                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If
            sMySQL = "UPDATE Company SET "
            'sMySQL = "UPDATE Company SET CreateBackup = "
            'If Me.chkMakeCopyBeforeExport.CheckState = System.Windows.Forms.CheckState.Checked Then
            '    sMySQL = sMySQL & "True, "
            'Else
            '    sMySQL = sMySQL & "False, "
            'End If
            'sMySQL = sMySQL & "BckBeforeImport = "
            'If Me.chkBeforeImport.CheckState = System.Windows.Forms.CheckState.Checked Then
            '    sMySQL = sMySQL & "True, "
            'Else
            '    sMySQL = sMySQL & "False, "
            'End If
            'sMySQL = sMySQL & "BckBeforeExport = "
            'If Me.chkBeforeExport.CheckState = System.Windows.Forms.CheckState.Checked Then
            '    sMySQL = sMySQL & "True, "
            'Else
            '    sMySQL = sMySQL & "False, "
            'End If
            'sMySQL = sMySQL & "BckAfterExport = "
            'If Me.chkAfterExport.CheckState = System.Windows.Forms.CheckState.Checked Then
            '    sMySQL = sMySQL & "True, "
            'Else
            '    sMySQL = sMySQL & "False, "
            'End If
            'sMySQL = sMySQL & "ExportBackuppath = '" & Me.txtPathForBackup.Text
            'sMySQL = sMySQL & "', ExportBackupGenerations = " & Trim(Str(Me.cmbNoOfBackups.SelectedIndex + 1))

            sMySQL = sMySQL & " UnDoDoubleMatched = "
            If Me.ChkDoubleMatching.CheckState = System.Windows.Forms.CheckState.Checked Then
                sMySQL = sMySQL & "True, "
            Else
                sMySQL = sMySQL & "False, "
            End If
            sMySQL = sMySQL & "UnDoAllDoubleMatched = "
            If Me.chkDoubleMatchingUndoAll.CheckState = System.Windows.Forms.CheckState.Checked Then
                sMySQL = sMySQL & "True "
            Else
                sMySQL = sMySQL & "False "
            End If
            sMySQL = sMySQL & " WHERE Company_ID = 1" 'TODO - Hardcoded CompanyID
            oMyDal.SQL = sMySQL
            If oMyDal.ExecuteNonQuery Then
                If oMyDal.RecordsAffected = 1 Then
                    bReturnValue = True
                Else
                    Throw New Exception("No records affected" & vbCrLf & oMyDal.ErrorMessage)
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oMyDal.ErrorMessage)
            End If

        Catch ex As Exception

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

            Throw New Exception("Function: SaveManualSetup" & vbCrLf & ex.Message)

        End Try

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If
        Me.Hide()
        SaveManualSetup = bReturnValue

    End Function

	
	'UPGRADE_WARNING: Event lstColumnsEnterSequence.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub lstColumnsEnterSequence_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstColumnsEnterSequence.SelectedIndexChanged
		Dim i As Short
		
		i = lstColumnsEnterSequence.SelectedIndex
		
		'If i = 16 Or i = 17 Then 'Match_ID or Akonto_ID
		' Changed 11.09.2009
		If i = 15 Or i = 16 Then 'Match_ID or Akonto_ID
			'Theese fields are not valid
			lstMatchedColumns.SetItemChecked(i, False)
			MsgBox(LRS(62029), MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
		ElseIf i = -1 Then 
			'Nothing to do. No item selected
		Else
			'Validate that if a column is selected in lstColumnsEnterSequence, the corresponding
			' column in lstMatchedColumns must be checked
			If lstColumnsEnterSequence.GetItemChecked(i) = True Then
				If lstMatchedColumns.GetItemChecked(i) = True Then
					'OK
				Else
					lstColumnsEnterSequence.SetItemChecked(i, False)
					MsgBox(LRS(60207), MsgBoxStyle.OKOnly + MsgBoxStyle.Information, LRS(60208))
				End If
			End If
		End If
		
	End Sub
	
	'UPGRADE_WARNING: Event lstMatchedColumns.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub lstMatchedColumns_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstMatchedColumns.SelectedIndexChanged
		Dim i As Short
		
		i = lstMatchedColumns.SelectedIndex
		
		'If i = 16 Or i = 17 Then 'Match_ID or Akonto_ID
		' Changed 11.09.2009
		If i = 15 Or i = 16 Then 'Match_ID or Akonto_ID
			'Theese fields are not valid
			lstMatchedColumns.SetItemChecked(i, False)
			MsgBox(LRS(62029), MsgBoxStyle.OKOnly + MsgBoxStyle.Information)
		ElseIf i = -1 Then 
			'Nothing to do. No item selected
		Else
			'Validate that if a column is selected in lstColumnsEnterSequence, the corresponding
			' column in lstMatchedColumns must be checked
			If lstMatchedColumns.GetItemChecked(i) = False Then
				lstColumnsEnterSequence.SetItemChecked(i, False)
			End If
		End If
		
	End Sub

    Private Sub cmdRightClick_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRightClick.Click
        Dim frmRightClick As New frmRightClick

        frmRightClick.SetCompany_ID("1") 'TODO - Hardcoded CompanyID
        frmRightClick.ShowDialog()

        frmRightClick.Dispose()

    End Sub

    Private Sub ChkDoubleMatching_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChkDoubleMatching.Click

        If ChkDoubleMatching.CheckState = CheckState.Checked Then
            Me.chkDoubleMatchingUndoAll.Enabled = True
        Else
            Me.chkDoubleMatchingUndoAll.Enabled = False
            Me.chkDoubleMatchingUndoAll.CheckState = CheckState.Unchecked
        End If

    End Sub
End Class
