Option Strict Off
Option Explicit On

Friend Class frmVisma_LogView
	Inherits System.Windows.Forms.Form
	Private Sub frmVisma_LogView_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		
		FormLRSCaptions(Me)
        FormVismaStyle(Me)
        Me.txtLog.SelectionStart = Me.txtLog.Text.Length

	End Sub
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        Me.Hide()
    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Hide()
    End Sub
End Class