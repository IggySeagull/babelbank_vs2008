<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmVisma_ConvertDatabaseProgress
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents prgProgress As System.Windows.Forms.ProgressBar
	Public WithEvents txtProgress As System.Windows.Forms.TextBox
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.prgProgress = New System.Windows.Forms.ProgressBar
        Me.txtProgress = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'prgProgress
        '
        Me.prgProgress.Location = New System.Drawing.Point(16, 64)
        Me.prgProgress.Name = "prgProgress"
        Me.prgProgress.Size = New System.Drawing.Size(497, 25)
        Me.prgProgress.TabIndex = 1
        '
        'txtProgress
        '
        Me.txtProgress.AcceptsReturn = True
        Me.txtProgress.BackColor = System.Drawing.SystemColors.Window
        Me.txtProgress.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtProgress.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtProgress.Location = New System.Drawing.Point(16, 24)
        Me.txtProgress.MaxLength = 0
        Me.txtProgress.Name = "txtProgress"
        Me.txtProgress.ReadOnly = True
        Me.txtProgress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtProgress.Size = New System.Drawing.Size(497, 25)
        Me.txtProgress.TabIndex = 0
        '
        'frmVisma_ConvertDatabaseProgress
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(532, 106)
        Me.ControlBox = False
        Me.Controls.Add(Me.prgProgress)
        Me.Controls.Add(Me.txtProgress)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(8, 30)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmVisma_ConvertDatabaseProgress"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.Text = "ConvertX->Visma Payment (BabelBank)"
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class