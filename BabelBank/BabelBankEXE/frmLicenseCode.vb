Option Strict Off
Option Explicit On
Friend Class frmLicenseCode
	Inherits System.Windows.Forms.Form
	
	Public bRetValue As Boolean
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		Me.Hide()
		bRetValue = True
	End Sub
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
		bRetValue = False
	End Sub
	
	Private Sub frmLicenseCode_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		FormvbStyle(Me, "")
	End Sub
End Class
