<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmCustom4
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents txtCustom As System.Windows.Forms.TextBox
	Public WithEvents lblCustom As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCustom4))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.cmdCancel = New System.Windows.Forms.Button
		Me.cmdOK = New System.Windows.Forms.Button
		Me.txtCustom = New System.Windows.Forms.TextBox
		Me.lblCustom = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.Text = "Custom4"
		Me.ClientSize = New System.Drawing.Size(489, 135)
		Me.Location = New System.Drawing.Point(4, 30)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmCustom4"
		Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdCancel.Text = "&Cancel"
		Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
		Me.cmdCancel.Location = New System.Drawing.Point(396, 102)
		Me.cmdCancel.TabIndex = 2
		Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.cmdCancel.CausesValidation = True
		Me.cmdCancel.Enabled = True
		Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdCancel.TabStop = True
		Me.cmdCancel.Name = "cmdCancel"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "&OK"
		Me.AcceptButton = Me.cmdOK
		Me.cmdOK.Size = New System.Drawing.Size(81, 25)
		Me.cmdOK.Location = New System.Drawing.Point(308, 102)
		Me.cmdOK.TabIndex = 1
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.txtCustom.AutoSize = False
		Me.txtCustom.Size = New System.Drawing.Size(289, 19)
		Me.txtCustom.Location = New System.Drawing.Point(188, 54)
		Me.txtCustom.Maxlength = 100
		Me.txtCustom.TabIndex = 0
		Me.txtCustom.AcceptsReturn = True
		Me.txtCustom.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCustom.BackColor = System.Drawing.SystemColors.Window
		Me.txtCustom.CausesValidation = True
		Me.txtCustom.Enabled = True
		Me.txtCustom.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCustom.HideSelection = True
		Me.txtCustom.ReadOnly = False
		Me.txtCustom.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCustom.MultiLine = False
		Me.txtCustom.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCustom.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCustom.TabStop = True
		Me.txtCustom.Visible = True
		Me.txtCustom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtCustom.Name = "txtCustom"
		Me.lblCustom.Text = "Label1"
		Me.lblCustom.Size = New System.Drawing.Size(161, 41)
		Me.lblCustom.Location = New System.Drawing.Point(10, 54)
		Me.lblCustom.TabIndex = 3
		Me.lblCustom.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCustom.BackColor = System.Drawing.SystemColors.Control
		Me.lblCustom.Enabled = True
		Me.lblCustom.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCustom.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCustom.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCustom.UseMnemonic = True
		Me.lblCustom.Visible = True
		Me.lblCustom.AutoSize = False
		Me.lblCustom.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCustom.Name = "lblCustom"
		Me.Controls.Add(cmdCancel)
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(txtCustom)
		Me.Controls.Add(lblCustom)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
