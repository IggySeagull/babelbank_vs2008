Option Strict Off
Option Explicit On
' Johannes# 
Module TreatJanP
    Dim oBabelFile As vbBabel.BabelFile
    Dim oBatches As vbBabel.Batches
    Dim oBatch As vbBabel.Batch
    Dim oPayment As vbBabel.Payment
    Dim oInvoice As vbBabel.Invoice
    Dim oFreeText As vbBabel.Freetext
    Dim oProfile As vbBabel.Profile
    Dim oFilesetup As vbBabel.FileSetup
    Dim oClient As vbBabel.Client
    Dim oaccount As vbBabel.Account
    Public Function TreatTeekay_UK_Salary(ByRef oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' UK GBP-payments will go to TBI, as BACS - One profile for this!
        '----------------------------------------------------------------
        Dim sDebetSWIFTAddress As String
        Dim sCountryCode As String
        Dim lRunning As Integer

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If CDbl(oPayment.StatusCode) = 0 Then
                            ' Remove leading / from account
                            If Left(oPayment.E_Account, 1) = "/" Then
                                oPayment.E_Account = Mid(oPayment.E_Account, 2)
                            End If

                            ' 23.02.2009 - midlertidig ta vare p� opprinnelig kontonr til mottaker f�r vi gj�r noe med dem
                            '                oPayment.ExtraD3 = oPayment.E_Account
                            '
                            '                ' Pick Swift from FromAccount (must be set unde ClientSetup)
                            '                sDebetSWIFTAddress = ""
                            '                FindAccSWIFTAddr oPayment.I_Account, oBabelFiles.VB_Profile, oPayment.VB_FilenameOut_ID, sDebetSWIFTAddress
                            '                sCountryCode = Mid$(sDebetSWIFTAddress, 5, 2)
                            '
                            '                oPayment.I_CountryCode = sCountryCode
                            '                oPayment.BANK_I_SWIFTCode = sDebetSWIFTAddress
                            '                oPayment.BANK_I_BranchType = BankBranchType.SWIFT

                            ' 19.03.2009
                            ' Must change from own reference 000000000 to running number so DnBNOR London can
                            ' have less manuall work!
                            lRunning = lRunning + 1
                            If Trim(oPayment.REF_Own) = "0000000000" Then
                                oPayment.REF_Own = DateToString(Date.Today) & PadLeft(Trim(Str(lRunning)), 5, "0")
                            End If

                            ' Testing IBANN
                            ' If not valid IBANN, then report, and exclude from paymentfile
                            If Left(oPayment.E_Account, 2) = "GB" And oPayment.MON_InvoiceCurrency = "GBP" And Not IsIBANNumber((oPayment.E_Account), True, False) Then
                                oPayment.Exported = True
                                'oPayment.StatusCode = "20" ' Feilkode 20 - "Debet kontonummer ikke gyldig."
                                oPayment.StatusCode = "19"   ' Feilkode 19 - "Kredit kontonummer ikke gyldig."
                                oBabelFile.RejectsExists = True

                                ' First, check if IBANN starts with GB and OK IBANN and GBP, if so; SAL (BACS)
                                ' Otherwise, GEN
                            ElseIf Left(oPayment.E_Account, 2) = "GB" And oPayment.MON_InvoiceCurrency = "GBP" And IsIBANNumber((oPayment.E_Account), True, False) Then
                                ' Must deduct BBAN account and sortcode from IBAN
                                ' UK IBAN:
                                ' (22) IBAN format: GBkk BBBB SSSS SSCC CCCC CC
                                ' B = alphabetical bank code, S = sort code (often a specific branch), C = account No.
                                oPayment.PayType = "S"
                                oPayment.DnBNORTBIPayType = "SAL"

                                ' adust account;
                                oPayment.BANK_BranchNo = Mid(oPayment.E_Account, 9, 6)
                                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode
                                ' put BBAN in Account, last 8 digits
                                oPayment.E_Account = Right(oPayment.E_Account, 8)
                                oPayment.BANK_AccountCountryCode = "GB"


                                ' Will result in SAL-payment. Must adjust some info
                                oPayment.E_Name = Left(oPayment.E_Name, 18)
                                oPayment.E_Adr1 = ""
                                oPayment.E_Adr2 = ""
                                oPayment.E_Adr3 = ""
                                For Each oInvoice In oPayment.Invoices
                                    oInvoice.REF_Own = Trim(oPayment.REF_Own)
                                    If oInvoice.Freetexts.Count = 0 Then
                                        oFreeText = oInvoice.Freetexts.Add
                                        oFreeText.Text = "WAGES"
                                    End If
                                Next oInvoice
                            Else
                                ' Treat as GEN, using Swift + AccountNo
                                oPayment.PayType = "I"
                                oPayment.DnBNORTBIPayType = "GEN"
                                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT
                                ' Teekay will pay all charges ??????
                                '''''''oPayment.MON_ChargeMeDomestic = True
                                '''''''oPayment.MON_ChargeMeAbroad = True

                                oPayment.BANK_AccountCountryCode = Mid(oPayment.BANK_SWIFTCode, 5, 2)

                                ' added 09.03.2009
                                ' if GEN, do not put anything into opayment.BANK_BranchNo
                                oPayment.BANK_BranchNo = ""

                            End If
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatTeekay_UK_Salary = True

    End Function
    ' XokNET 05.02.2013 Added function - take whole function
    Public Function TreatTeekay_UK_SalaryReturn(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Redo from BACS (Mass-payments) to Int, to give a better returnfile
        '----------------------------------------------------------------
        Dim sDebetSWIFTAddress As String
        Dim sCountryCode As String
        Dim lRunning As Long
        Dim sValueDate As String
        Dim sEffektueringsRef As String
        Dim i As Integer
        Dim iLoop As Integer
        Dim sOldRefOwn As String
        Dim oTempInvoice As vbBabel.Invoice

        Try

            sValueDate = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.PayType = "I" ' Will give BETFOR01-04 in returnfiles

                        '- 01.03.2013
                        ' We will also need an ownref on BETFOR01, which is the same one as in the first BETFOR22 (BETFOR21/22 in return from TelepayPlus)
                        oPayment.REF_Own = oPayment.Invoices(1).REF_Own

                        ' - 19.03.2013
                        ' Valuedate on first payment only, because we have one BETFOR21 only, an several BETFOR22 in returnfile from DNB
                        ' Pick valuedate from first payment, and use that one
                        If oPayment.DATE_Value <> "19900101" Then
                            ' not empty
                            sValueDate = oPayment.DATE_Value
                        Else
                            ' empty valuedate
                            If Not EmptyString(sValueDate) Then
                                oPayment.DATE_Value = sValueDate
                            Else
                                ' we have no valuedate from previous payments, use paymentdate
                                oPayment.DATE_Value = oPayment.DATE_Payment
                            End If
                        End If
                        ' We may also need accountamount if not filled in. This is for UK domestics, no currencyexchange
                        ' XNET 29.11.2013 COmmented If, ad create new If 5 lines below
                        'If oPayment.MON_AccountAmount = 0 Then
                        If oPayment.MON_InvoiceCurrency = "GBP" Then
                            oPayment.MON_AccountAmount = oPayment.MON_TransferredAmount
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.MON_AccountAmount = 0 Then
                                    oInvoice.MON_AccountAmount = oInvoice.MON_TransferredAmount
                                End If
                            Next
                        End If
                        'End If
                        ' There is effektueringsref only on BETFOR21, not BETFOR22, take that to the others
                        If Not EmptyString(oPayment.REF_Bank1) Then
                            sEffektueringsRef = oPayment.REF_Bank1
                        Else
                            ' fill it in
                            oPayment.REF_Bank1 = sEffektueringsRef
                        End If
                    Next
                Next
            Next

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches

                    For i = 1 To oBatch.Payments.Count

                        ' 30.04.2013 -  we will need to organize invoicepayments with same OwnRef into one common Payment,
                        '               to be able to return in same setting as outgoing file from Agresso
                        ' Check if this payment has same ownref as the previous one

                        If Not oBatch.Payments(i).Exported Then
                            sOldRefOwn = oBatch.Payments(i).REF_Own
                            iLoop = 0
                            Do
                                iLoop = iLoop + 1
                                If i + iLoop > oBatch.Payments.Count Then
                                    ' must update amounts also on payment
                                    oBatch.Payments(i).MON_InvoiceAmount = 0
                                    oBatch.Payments(i).MON_TransferredAmount = 0
                                    oBatch.Payments(i).MON_AccountAmount = 0
                                    For Each oInvoice In oBatch.Payments(i).Invoices
                                        oBatch.Payments(i).MON_InvoiceAmount = oBatch.Payments(i).MON_InvoiceAmount + oInvoice.MON_InvoiceAmount
                                        oBatch.Payments(i).MON_TransferredAmount = oBatch.Payments(i).MON_TransferredAmount + oInvoice.MON_TransferredAmount
                                        oBatch.Payments(i).MON_AccountAmount = oBatch.Payments(i).MON_AccountAmount + oInvoice.MON_AccountAmount
                                    Next
                                    Exit Do
                                End If

                                If Left$(oBatch.Payments(i + iLoop).REF_Own, 15) = Left$(sOldRefOwn, 15) Then
                                    ' find invoice, and lift it "up" to current payment
                                    ' there can be max 1 invoice pr payment, the way we have organized them
                                    oTempInvoice = New vbBabel.Invoice
                                    oTempInvoice = oBatch.Payments(i + iLoop).Invoices(1)
                                    ' must "reindex
                                    oTempInvoice.Index = iLoop + 1
                                    ' add to "original" payment
                                    oTempInvoice = oBatch.Payments(i).Invoices.VB_AddWithObject(oTempInvoice)
                                    ' set payment as exported
                                    oBatch.Payments(i + iLoop).Exported = True
                                Else

                                    ' must update amounts on payment
                                    oBatch.Payments(i).MON_InvoiceAmount = 0
                                    oBatch.Payments(i).MON_TransferredAmount = 0
                                    oBatch.Payments(i).MON_AccountAmount = 0
                                    For Each oInvoice In oBatch.Payments(i).Invoices
                                        oBatch.Payments(i).MON_InvoiceAmount = oBatch.Payments(i).MON_InvoiceAmount + oInvoice.MON_InvoiceAmount
                                        oBatch.Payments(i).MON_TransferredAmount = oBatch.Payments(i).MON_TransferredAmount + oInvoice.MON_TransferredAmount
                                        oBatch.Payments(i).MON_AccountAmount = oBatch.Payments(i).MON_AccountAmount + oInvoice.MON_AccountAmount
                                    Next
                                    Exit Do
                                End If
                            Loop
                        End If

                    Next i

                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatTeekay_UK_SalaryReturn = True

    End Function
    Public Function TreatTeekay_UK(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' UK GBP-payments will go to Connect, as BACS - One profile for this!
        '----------------------------------------------------------------
        Dim sDebetSWIFTAddress As String
        Dim sCountryCode As String

        Try

            For Each oBabelFile In oBabelFiles
                ' XokNET 06.11.2013 - Run only once;
                ' added next IF, next line, and Endif
                If Not oBabelFile.Version = "99" Then
                    oBabelFile.Version = "99"
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If oPayment.StatusCode = 0 Then
                                ' Remove leading / from account
                                If Left$(oPayment.E_Account, 1) = "/" Then
                                    oPayment.E_Account = Mid$(oPayment.E_Account, 2)
                                End If

                                ' Do not test for valid IBAN - let DNB take that
                                If Left$(oPayment.E_Account, 2) = "GB" And _
                                    oPayment.MON_InvoiceCurrency = "GBP" Then
                                    ' Must deduct BBAN account and sortcode from IBAN
                                    ' UK IBAN:
                                    ' (22) IBAN format: GBkk BBBB SSSS SSCC CCCC CC
                                    ' B = alphabetical bank code, S = sort code (often a specific branch), C = account No.
                                    oPayment.PayType = "M"
                                    oPayment.DnBNORTBIPayType = "ACH"

                                    ' adust account;
                                    oPayment.BANK_BranchNo = Mid$(oPayment.E_Account, 9, 6)
                                    oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode
                                    ' put BBAN in Account, last 8 digits
                                    oPayment.E_Account = Right$(oPayment.E_Account, 8)
                                    oPayment.BANK_AccountCountryCode = "GB"

                                Else
                                    ' Treat as GEN, using Swift + AccountNo
                                    oPayment.PayType = "I"
                                    oPayment.DnBNORTBIPayType = "GEN"
                                    oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT

                                    oPayment.BANK_AccountCountryCode = Mid$(oPayment.BANK_SWIFTCode, 5, 2)
                                End If
                            End If
                        Next
                    Next
                End If  'If Not oBabelFile.Version = "99" Then
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatTeekay_UK = True

    End Function

    Public Function TreatLindorff_TNT(ByRef oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Extract invoicenumbers from Freetext
        ' Export on special format for Lindorff, both KIDs and extracted invoice payments
        ' -------------------------------------------------------------------------------
        Dim bReturnValue As Boolean
        Dim bKIDTransaction As Boolean
        Dim aInvDesc(,) As String
        Dim lCounter As Integer
        Dim sOldAccountNo, sI_Account As String
        Dim bAccountFound As Boolean

        Try

            bReturnValue = True


            'Retrieve Invoicenumbers from freetext
            ' So far: Only if one possible invoiceno


            sOldAccountNo = ""

            'Retrieve invoicenumber
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        'Find the client
                        If oPayment.I_Account <> sOldAccountNo Then
                            If oPayment.VB_ProfileInUse Then
                                sI_Account = oPayment.I_Account
                                bAccountFound = False
                                For Each oFilesetup In oBabelFile.VB_Profile.FileSetups
                                    For Each oClient In oFilesetup.Clients
                                        For Each oaccount In oClient.Accounts
                                            If sI_Account = oaccount.Account Then
                                                sOldAccountNo = oPayment.I_Account
                                                bAccountFound = True

                                                Exit For
                                            End If
                                        Next oaccount
                                        If bAccountFound Then Exit For
                                    Next oClient
                                    If bAccountFound Then Exit For
                                Next oFilesetup
                            End If
                            If bAccountFound Then
                                'Retrieve description - Hardcoded Company, Client and DB Profile_ID
                                aInvDesc = FindDescription(oBabelFile.VB_Profile.Company_ID, (oClient.Client_ID), (oClient.DBProfile_ID))
                            Else
                                Err.Raise(34234, "TreatLindorff_TNT", "Kontonummer " & sI_Account & " er ukjent for BabelBank." & vbCrLf & "Legg inn kontonummeret under 'Avstemming' - 'Klient'" & vbCrLf & vbCrLf & "BabelBank avbrytes!")
                            End If
                        End If
                        lCounter = 0
                        For lCounter = 1 To UBound(aInvDesc, 2)
                            ' Hvis vi har delt i 2 ulike M�nstertyper, blir de aInvDesc(1, og aInvDesc(2, !!!!
                            If Len(aInvDesc(0, lCounter)) > 0 Then '
                                oPayment.MATCH_NoOfLeadingZeros = 0
                                oPayment.MATCH_InvoiceDescription = aInvDesc(0, lCounter)
                                oPayment.MATCH_ExtractInvoiceNo()
                            End If
                            If Len(aInvDesc(1, lCounter)) > 0 Then '
                                oPayment.MATCH_NoOfLeadingZeros = 0
                                oPayment.MATCH_InvoiceDescription = aInvDesc(1, lCounter)
                                oPayment.MATCH_ExtractInvoiceNo()
                            End If
                        Next lCounter

                        ' can only handle those with one possible and one invoice for unstructured
                        If oPayment.MATCH_PossibleNumberOfInvoices = 1 And oPayment.Invoices.Count = 1 Then
                            oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched
                            For Each oInvoice In oPayment.Invoices
                                If Len(oPayment.MATCH_InvoiceNumber) = 6 Then
                                    oPayment.MATCH_InvoiceNumber = "00" & oPayment.MATCH_InvoiceNumber
                                ElseIf Len(oPayment.MATCH_InvoiceNumber) = 15 Then
                                    ' KID as text
                                    'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.MATCH_InvoiceNumber. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    oInvoice.Unique_Id = oPayment.MATCH_InvoiceNumber
                                End If
                                'UPGRADE_WARNING: Couldn't resolve default property of object oPayment.MATCH_InvoiceNumber. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                oInvoice.InvoiceNo = oPayment.MATCH_InvoiceNumber
                                oInvoice.MATCH_Matched = True
                            Next oInvoice
                        ElseIf oPayment.MATCH_PossibleNumberOfInvoices = oPayment.Invoices.Count Then
                            ' possible structured invoices
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.TypeOfStructuredInfo = vbBabel.BabelFiles.TypeOfStructuredInfo.StructureTypeInvoice Then
                                    If Len(oInvoice.InvoiceNo) > 0 Then ' may have some with no invoice
                                        If Len(oInvoice.InvoiceNo) = 6 Then
                                            oInvoice.InvoiceNo = "00" & oInvoice.InvoiceNo
                                        End If
                                        oInvoice.MATCH_Matched = True
                                    End If
                                End If
                                If oInvoice.TypeOfStructuredInfo = vbBabel.BabelFiles.TypeOfStructuredInfo.StructureTypeKID Then
                                    If Len(oInvoice.Unique_Id) > 0 Then ' may have some with no kid
                                        ' oInvoice.Unique_ID is filled with KID !
                                        oInvoice.MATCH_Matched = True
                                    End If
                                End If
                            Next oInvoice
                            oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched
                        Else
                            oPayment.MATCH_Reset()
                        End If

                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatLindorff_TNT = bReturnValue

    End Function
    ' XNET 98.11.2010 - new function
    Public Function TreatCominor(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Extract Sj�f�rnr og oppgj�rsnr from Freetext
        ' Export on special format for Cominor
        ' The exportfile will be imported into AccountControl

        ' Sj�f�rnr i som @9#####@
        ' Trekk ut oppgj�rsnr manuelt, da de st�r som 22-24, 22 - 24 o.l.
        ' -------------------------------------------------------------------------------
        Dim bReturnValue As Boolean
        Dim sTxt As String
        Dim sTmp As String
        Dim lCounter As Long

        Try

            bReturnValue = True

            'Retrieve invoicenumber
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        sTxt = ""
                        For Each oInvoice In oPayment.Invoices
                            For Each oFreeText In oInvoice.Freetexts
                                sTxt = sTxt & Trim$(oFreeText.Text)
                            Next oFreeText
                        Next oInvoice
                        ' Extract info pr payment:
                        ' Sj�f�rnr, @9#####@
                        lCounter = 0
                        Do
                            lCounter = lCounter + 1
                            If Mid$(sTxt, lCounter, 2) = " 9" Then
                                ' 5 digits after 9, and then space or end ?
                                If IsNumeric(Mid$(sTxt, lCounter + 1, 6)) And _
                                (Mid$(sTxt, lCounter + 7, 1) = " " Or Len(sTxt) = lCounter + 6) Then
                                    ' Yes, possible sj�f�rnr
                                    ' extract sj�f�rnr and put into Invoiceno
                                    oPayment.MATCH_InvoiceNumber = Mid$(sTxt, lCounter + 1, 6)
                                    oPayment.Structured = True
                                    Exit Do
                                End If
                            End If
                            If lCounter > Len(sTxt) - 6 Then
                                Exit Do
                            End If
                        Loop

                        '------------------------------
                        ' Oppgj�rnsnr
                        ' Extract info pr payment:
                        ' Oppgj�rsnr, 04-06
                        '------------------------------
                        lCounter = 0
                        Do
                            lCounter = lCounter + 1
                            ' 1-2, 1 - 2, 9-15, 9 - 15, 20-22. 20 - 22, 99-104, 99 - 104, 110-114, 110 - 114, etc
                            ' or 1, 5,  12, 112, etc (space on each side)
                            If Mid$(sTxt, lCounter, 1) = "-" Then
                                ' first with -
                                If lCounter > 3 Then
                                    sTmp = Mid$(sTxt, lCounter - 4, 10)
                                Else
                                    sTmp = Mid$(sTxt, 1, 10)
                                End If
                                sTmp = Replace(sTmp, " ", "")
                                ' keep digits and -
                                sTmp = CheckForValidCharacters(sTmp, False, False, False, True, "-")
                                ' may have leading -, remove
                                If Left$(sTmp, 1) = "-" Then
                                    sTmp = Mid$(sTmp, 2)
                                End If
                                If sTmp <> "-" Then
                                    ' that's it !
                                    ' extract oppgj�rsnr and put into Invoiceno
                                    oPayment.MATCH_MyField = sTmp
                                    oPayment.Structured = True
                                End If
                                ' try more - Exit Do
                            End If

                            ' then space,number,space
                            If (lCounter = 1 And IsNumeric(Left$(sTxt, 1))) Or Mid$(sTxt, lCounter, 1) = " " Then
                                ' next digit?
                                If IsNumeric(Mid$(sTxt, lCounter + 1, 1)) Then
                                    ' next space?
                                    If Mid$(sTxt, lCounter + 2, 1) = " " Then
                                        ' yes, one digit nr
                                        sTmp = Trim$(Mid$(sTmp, lCounter, 2))
                                        ' extract oppgj�rsnr and put into Invoiceno
                                        oPayment.MATCH_MyField = sTmp
                                        oPayment.Structured = True
                                        Exit Do
                                    Else
                                        ' another numeric ?
                                        If IsNumeric(Mid$(sTxt, lCounter + 2, 1)) Then
                                            If Mid$(sTxt, lCounter + 3, 1) = " " Then
                                                ' yes, two digit nr
                                                sTmp = Trim$(Mid$(sTxt, lCounter, 3))
                                                ' extract oppgj�rsnr and put into Invoiceno
                                                oPayment.MATCH_MyField = sTmp
                                                oPayment.Structured = True
                                                Exit Do
                                            Else
                                                ' another numeric ?
                                                If IsNumeric(Mid$(sTxt, lCounter + 3, 1)) Then
                                                    If Mid$(sTxt, lCounter + 4, 1) = " " Then
                                                        ' yes, three digit nr
                                                        sTmp = Trim$(Mid$(sTxt, lCounter, 4))
                                                        ' extract oppgj�rsnr and put into Invoiceno
                                                        oPayment.MATCH_MyField = sTmp
                                                        oPayment.Structured = True
                                                        Exit Do
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If

                            If lCounter > Len(sTxt) Then
                                Exit Do
                            End If
                        Loop

                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatCominor = bReturnValue

    End Function
    ' XNET 09.11.2010 New function
    '    Public Function TreatTools(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
    '        ' Fill in clientnumbers based on length of KID for one special client/account

    '        On Error GoTo ToolsErr

    '        For Each oBabelFile In oBabelFiles
    '            For Each oBatch In oBabelFile.Batches
    '                For Each oPayment In oBatch.Payments
    '                    For Each oInvoice In oPayment.Invoices
    '                        ' Visma Business 9 digit KID
    '                        ' SAP (Rogaland Jernvare) 8 digit KID
    '                        If Len(Trim$(oInvoice.Unique_Id)) = 9 Then
    '                            oPayment.VB_ClientNo = "VIS"
    '                        Else
    '                            oPayment.VB_ClientNo = "SAP"
    '                        End If
    '                    Next
    '                Next
    '            Next
    '        Next


    '        TreatTools = True
    '        Exit Function

    'ToolsErr:
    '        TreatTools = False

    '    End Function
    Public Function TreatTools(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal sFilenameOut As String) As Boolean
        ' Fill in clientnumbers based on length of KID for one special client/account

        ' Spit file in VIS/SAP
        ' Prepare special report for totals for VIS and SAP
        ' Prepare array for use in rp_951_Array5
        Dim lCounter As Long
        Dim aDoc(,) As Object
        Dim dSAPTotal As Double
        Dim dVISTotal As Double
        Dim oReport As vbBabel.Report
        Dim oBabelReport As vbBabel.BabelReport
        Dim sTmp As String

        Try

            dSAPTotal = 0
            dVISTotal = 0

            lCounter = 0
            ReDim aDoc(5, 0)
            aDoc(0, lCounter) = "Splittede OCR bunter"
            aDoc(1, lCounter) = "&M"

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    dVISTotal = 0
                    dSAPTotal = 0

                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(5, lCounter)
                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(5, lCounter)
                    aDoc(0, lCounter) = "&BDato:"
                    aDoc(1, lCounter) = oBatch.DATE_Production
                    aDoc(3, lCounter) = ""
                    aDoc(4, lCounter) = ""

                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(5, lCounter)
                    aDoc(0, lCounter) = "&BOpprinnelig bunt:"
                    aDoc(1, lCounter) = ""
                    aDoc(3, lCounter) = ""
                    aDoc(4, lCounter) = "&B" & "&R" & Format(oBatch.MON_TransferredAmount / 100, "##,##0.00")

                    For Each oPayment In oBatch.Payments
                        For Each oInvoice In oPayment.Invoices
                            ' Visma Business 9 digit KID
                            ' SAP (Rogaland Jernvare) 8 digit KID
                            If Len(Trim$(oInvoice.Unique_Id)) = 9 Then
                                oPayment.VB_ClientNo = "VIS"
                                dVISTotal = dVISTotal + oPayment.MON_TransferredAmount
                            Else
                                oPayment.VB_ClientNo = "SAP"
                                dSAPTotal = dSAPTotal + oPayment.MON_TransferredAmount
                            End If
                        Next
                    Next
                    ' batch completed; report VIS and SAP
                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(5, lCounter)
                    aDoc(0, lCounter) = ""
                    aDoc(1, lCounter) = "Visma bunt:"
                    aDoc(2, lCounter) = "       -"
                    aDoc(3, lCounter) = oBatch.REF_Bank
                    aDoc(4, lCounter) = "&R" & Format(dVISTotal / 100, "##,##0.00")

                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(5, lCounter)
                    aDoc(0, lCounter) = ""
                    aDoc(1, lCounter) = "SAP bunt:"
                    aDoc(2, lCounter) = "       -"
                    aDoc(3, lCounter) = oBatch.REF_Bank
                    aDoc(4, lCounter) = "&R" & Format(dSAPTotal / 100, "##,##0.00")

                Next
            Next


            ' Send to report 951
            '-------------------
            'oReport = CreateObject ("vbbabel.Report")
            'oBabelReport = CreateObject ("vbbabel.BabelReport")
            oReport = New vbBabel.Report
            oBabelReport = New vbBabel.BabelReport
            oReport.ReportNo = "951"  'Special report rp_951_Array5, with 5 fields = 5 array elements
            oReport.Heading = "OCR bunter"
            oReport.Preview = True
            oReport.Printer = False
            oReport.ExportType = "PDF"
            sTmp = Left$(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\OCRReport_" & Date.Today & ".pdf"
            oReport.ExportFilename = sTmp

            oReport.DetailLevel = 1
            'oBabelReport.ReportArray = aDoc
            'oBabelReport.Report = oReport
            'oBabelReport.BabelFiles = oBabelFiles
            'oBabelReport.Start()

            oReport = Nothing
            oBabelReport = Nothing

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatTools = True

    End Function

    ' XNET 03.12.2010 - added function, will have another name later
    Public Function TreatRieber(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        '----------------------------------------------------------------
        Dim sCountryCode As String
        Dim sTmp As String
        Dim sOldAccountNo As String
        Dim sDebetSWIFTAddress As String

        For Each oBabelFile In oBabelFiles
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    ' find debitaccounts countrycode:
                    If sOldAccountNo <> Trim$(oPayment.I_Account) Then
                        sOldAccountNo = Trim$(oPayment.I_Account)
                        sDebetSWIFTAddress = ""
                        FindAccSWIFTAddr(oPayment.I_Account, oBabelFiles.VB_Profile, oPayment.VB_FilenameOut_ID, sDebetSWIFTAddress)
                    End If

                    Select Case Mid$(sDebetSWIFTAddress, 5, 2)
                        Case "DK"
                            ' do nothing ?
                        Case "SE"
                            ' redo to domestic if both to and from Sweden
                            ' Use E_Countrycode, because we do not know bank_countrycode
                            If Left$(oPayment.E_Account, 2) = "SE" Or oPayment.E_CountryCode = "SE" Then
                                ' if IBAN, reduce to BBAN
                                If IsIBANNumber(oPayment.E_Account, False) Then
                                    ' TODO add SE to next function
                                    ExtractInfoFromIBAN(oPayment, False)
                                End If
                                oPayment.PayType = "D"
                            End If
                        Case "DE"
                            ' do nothing ?
                        Case Else
                            ' should not happen, but do nothing
                            sTmp = sTmp
                    End Select

                    sOldAccountNo = oPayment.I_Account
                Next oPayment
            Next oBatch
        Next oBabelFile

        TreatRieber = True

    End Function
    ' XNET 14.01.2011 New function
    Public Function TreatBergenEnergiPaymul(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFormat_ID As Integer) As Boolean
        ' Construct Ownreference based on Reference (20) + Amount (9) + Sequence (6)
        Dim lSequence As Long
        Dim sTmp As String

        Try

            lSequence = oBabelFiles.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        lSequence = lSequence + 1

                        If lSequence > 999999 Then
                            lSequence = 0
                        End If

                        ' start with reference, which is already stored in Ref_Own, pad to 20
                        sTmp = PadRight(Trim$(oPayment.REF_Own), 20, " ")
                        ' then add amount, 9 digits
                        sTmp = sTmp & PadRight(Trim(Str(oPayment.MON_InvoiceAmount)), 9, " ")
                        ' last, add sequence, 6 digits
                        sTmp = sTmp & PadLeft(Trim$(Str(lSequence)), 6, "0")

                        oPayment.REF_Own = sTmp
                    Next
                Next
                ' save sequence
                oBabelFile.VB_Profile.FileSetups(iFormat_ID).SeqNoTotal = lSequence
                oBabelFile.VB_Profile.Status = 2
                oBabelFile.VB_Profile.FileSetups(iFormat_ID).Status = 1

            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatBergenEnergiPaymul = True

    End Function
    ' XokNET 07.03.2011 New function
    Public Function TreatMikiTravel(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFormat_ID As Integer) As Boolean
        ' For returnfiles, redo from SEK, DKK, EUR to NOK, and recalculate amount
        ' based on setup
        Dim nSEKRate As Double
        Dim nDKKRate As Double
        Dim nEURRate As Double

        Try

            'SEK -NOK = 0.85
            'DKK -NOK = 1.1
            'EUR -NOK = 8
            ' Rates are set in CustomSetup1, 2 , 3
            nSEKRate = ConvertToAmount(oBabelFiles.VB_Profile.Custom1Value, "", ",.") / 100
            nDKKRate = ConvertToAmount(oBabelFiles.VB_Profile.Custom2Value, "", ",.") / 100
            nEURRate = ConvertToAmount(oBabelFiles.VB_Profile.Custom3Value, "", ",.") / 100

            If nSEKRate = 0 Then
                nSEKRate = 0.85
            End If
            If nDKKRate = 0 Then
                nDKKRate = 1.1
            End If
            If nEURRate = 0 Then
                nEURRate = 8
            End If



            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    If oBatch.StatusCode > 0 Then
                        For Each oPayment In oBatch.Payments
                            If oPayment.MON_InvoiceCurrency = "SEK" Then
                                oPayment.MON_InvoiceAmount = oPayment.MON_InvoiceAmount * nSEKRate
                                For Each oInvoice In oPayment.Invoices
                                    oInvoice.MON_InvoiceAmount = oInvoice.MON_InvoiceAmount * nSEKRate
                                    oInvoice.MON_AccountAmount = oInvoice.MON_InvoiceAmount
                                Next
                            End If
                            If oPayment.MON_InvoiceCurrency = "DKK" Then
                                oPayment.MON_InvoiceAmount = oPayment.MON_InvoiceAmount * nDKKRate
                                For Each oInvoice In oPayment.Invoices
                                    oInvoice.MON_InvoiceAmount = oInvoice.MON_InvoiceAmount * nDKKRate
                                    oInvoice.MON_AccountAmount = oInvoice.MON_InvoiceAmount
                                Next
                            End If
                            If oPayment.MON_InvoiceCurrency = "EUR" Then
                                oPayment.MON_InvoiceAmount = oPayment.MON_InvoiceAmount * nEURRate
                                For Each oInvoice In oPayment.Invoices
                                    oInvoice.MON_InvoiceAmount = oInvoice.MON_InvoiceAmount * nEURRate
                                    oInvoice.MON_AccountAmount = oInvoice.MON_InvoiceAmount
                                Next
                            End If

                            ' redo from currency to NOK
                            If oPayment.MON_InvoiceCurrency = "SEK" Or _
                               oPayment.MON_InvoiceCurrency = "DKK" Or _
                               oPayment.MON_InvoiceCurrency = "EUR" Then
                                oPayment.MON_AccountAmount = oPayment.MON_InvoiceAmount
                                oPayment.MON_InvoiceCurrency = "NOK"
                                oPayment.MON_TransferCurrency = "NOK"
                                oPayment.MON_LocalExchRate = 0
                            End If
                        Next
                    End If
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatMikiTravel = True

    End Function

    ' XokNET 15.03.2011 - ...
    ' Added function for BKK/Fjordkraft
    Public Function TreatBKK_SplitBasedOnKID(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFileSetup_ID As Integer, ByVal oProfile As vbBabel.Profile, Optional ByVal sSpecial As String = "") As Boolean

        Dim oERPDal As vbBabel.DAL = Nothing
        Dim bReturnValue As Boolean = True
        Dim sMySetupSQL As String
        Dim sMySQL As String
        Dim sClientNo As String
        Dim sAccountNo As String
        Dim lCompanyNo As Long
        Dim aQueryArray(,,) As String
        Dim lCounter As Long
        Dim aDoc(,) As Object
        Dim nFjordkraftAmount As Double = 0
        Dim nTotalFjordKraftAmount As Double = 0
        Dim nTotalAmount As Double = 0
        Dim sSpecialReportPath As String
        Dim oBabelReport As vbBabel.BabelReport
        Dim oReport As vbBabel.Report
        Dim sTmp As String
        Dim bRunReport As Boolean = False

        lCompanyNo = 1 'TODO - Hardcoded CompanyID

        Try

            aQueryArray = GetERPDBInfo(1, 5)

            For lCounter = 0 To UBound(aQueryArray, 3)
                Select Case UCase(aQueryArray(0, 4, lCounter))

                    Case "SPLITTEKID"
                        sMySetupSQL = Trim$(aQueryArray(0, 0, lCounter))
                End Select
            Next lCounter

            If Not EmptyString(sMySetupSQL) Then

                ' connect to erp system
                oERPDal = New vbBabel.DAL
                oERPDal.TypeOfMatching = vbBabel.DAL.MatchingType.Automatic
                oERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                oERPDal.CreateArrayForReturnvalues(aQueryArray)
                oERPDal.Company_ID = lCompanyNo
                oERPDal.DBProfile_ID = 1 'TODO - Hardcoded DBProfile_ID
                If Not oERPDal.ConnectToDB() Then
                    Throw New System.Exception(oERPDal.ErrorMessage)
                End If

                ' Prepare array for use in rp_951_Array5
                lCounter = 0
                ReDim aDoc(5, 0)
                aDoc(0, lCounter) = "OCR bunter"
                aDoc(1, lCounter) = "&M"

                For Each oBabelFile In oBabelFiles
                    sSpecial = oBabelFile.Special
                    For Each oBatch In oBabelFile.Batches

                        ' XNET 04.05.2011 - added next 5 lines (3 If's, and more changes !!!!!)
                        If oBatch.Payments.Count > 0 Then
                            If IsOCR(oBatch.Payments.Item(1).PayCode) Then
                                If (oBabelFile.Special = "BKK" And oBatch.Payments.Item(1).I_Account = "52010661641") Or _
                                    oBabelFile.Special = "SPLITKID" Or oBabelFile.Special = "SKAGERAK ENERGI" Then
                                    bRunReport = True

                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(5, lCounter)
                                    ' blank line between each bunt
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(5, lCounter)
                                    aDoc(0, lCounter) = "&BOpprinnelig bunt:"
                                    aDoc(1, lCounter) = ""
                                    aDoc(3, lCounter) = "&B" & oBatch.REF_Bank
                                    aDoc(4, lCounter) = "&B" & "&R" & Format(oBatch.MON_TransferredAmount / 100, "##,##0.00")
                                    nTotalAmount = nTotalAmount + oBatch.MON_TransferredAmount
                                    nFjordkraftAmount = 0 ' How much in this batch is Fjordkraft?

                                    For Each oPayment In oBatch.Payments
                                        ' not for all accounts

                                        ' ----- UNCOMMENT NEXT LINE IN PRODUCTION !!!
                                        'If oPayment.I_Account = "52010661641" Then
                                        'If IsOCR(oPayment.PayCode) Then
                                        ' replace sql with real unique_ID
                                        If sSpecial <> "SKAGERAK ENERGI" Then
                                            sMySQL = Replace(sMySetupSQL, "BB_InvoiceIdentifier", oPayment.Invoices(1).Unique_Id)
                                            ' run the sql
                                            oERPDal.SQL = sMySQL
                                            If oERPDal.Reader_ReadERPRecord() Then
                                                If oERPDal.Reader_HasRows Then
                                                    Do While oERPDal.Reader_ReadRecord
                                                        ' use clientno to update payment
                                                        sClientNo = oERPDal.ClientNo
                                                        oPayment.VB_ClientNo = sClientNo
                                                        ' replace accountno to Fjordkrafts accountno.
                                                        ' Set account, as "'50010199999' as BBRET_AccountNo" in SQL
                                                        sAccountNo = oERPDal.String1 'rsERP.Fields("BBRET_AccountNo") - In the SQL BBRET_AccountNo must be replaced with BBRET_String1
                                                        oPayment.I_Account = sAccountNo
                                                        nFjordkraftAmount = nFjordkraftAmount + oPayment.MON_TransferredAmount
                                                        nTotalFjordKraftAmount = nTotalFjordKraftAmount + oPayment.MON_TransferredAmount
                                                        Exit Do
                                                    Loop
                                                Else
                                                    sMySQL = sMySQL ' slett etter test
                                                End If
                                            Else
                                                Throw New Exception(LRSCommon(45002) & oERPDal.ErrorMessage)
                                            End If
                                        End If
                                    Next
                                    ' batch completed; report BKK and Fjordkraft
                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(5, lCounter)
                                    aDoc(0, lCounter) = ""
                                    If oBabelFile.Special = "SKAGERAK ENERGI" Then
                                        aDoc(1, lCounter) = "Rest Skagerak Nett bunt:"
                                    Else
                                        aDoc(1, lCounter) = "Rest BKK bunt:"
                                    End If
                                    aDoc(2, lCounter) = "       -"
                                    aDoc(3, lCounter) = oBatch.REF_Bank
                                    aDoc(4, lCounter) = "&R" & Format((oBatch.MON_TransferredAmount / 100 - nFjordkraftAmount / 100), "##,##0.00")

                                    lCounter = lCounter + 1
                                    ReDim Preserve aDoc(5, lCounter)
                                    aDoc(0, lCounter) = ""
                                    If oBabelFile.Special = "SKAGERAK ENERGI" Then
                                        aDoc(1, lCounter) = "&GRest Skagerak Fiber bunt:"
                                    Else
                                        aDoc(1, lCounter) = "&GOverf�rt Fjordkraft"
                                    End If

                                    aDoc(2, lCounter) = "       -"
                                    aDoc(3, lCounter) = "&G" & oBatch.REF_Bank
                                    aDoc(4, lCounter) = "&G" & "&R" & Format(nFjordkraftAmount / 100, "##,##0.00")

                                End If ' If (oBabelFile.Special = "BKK" And oBatch.Payments.Item(1).I_Account = "52010661641") Or oBabelFile.Special = "SPLITKID" Then
                            End If ' If IsOCR(oBatch.Payments.Item(1).PayCode) Then
                        End If ' If oBatch.Payments.Count > 0 Then

                    Next
                Next

                lCounter = lCounter + 1
                ReDim Preserve aDoc(5, lCounter)
                lCounter = lCounter + 1
                ReDim Preserve aDoc(5, lCounter)

                aDoc(1, lCounter) = "&S"
                aDoc(0, lCounter) = "Totalbel�p OCR    " & Format(nTotalAmount / 100, "##,##0.00")

                lCounter = lCounter + 1
                ReDim Preserve aDoc(5, lCounter)
                aDoc(1, lCounter) = "&S"
                If sSpecial = "SKAGERAK ENERGI" Then
                    aDoc(1, lCounter) = "Totalt Skagerak Fiber " & Format(nTotalFjordKraftAmount / 100, "##,##0.00")
                Else
                    aDoc(0, lCounter) = "Totalt Fjordkraft    " & Format(nTotalFjordKraftAmount / 100, "##,##0.00")
                End If


                bReturnValue = True

                If bRunReport Then 'XNET - 04.05.2011 Added
                    ' Send to report 951
                    '-------------------
                    oReport = New vbBabel.Report  '23.05.2017 CreateObject ("vbbabel.Report")
                    oBabelReport = New vbBabel.BabelReport  '23.05.2017 CreateObject ("vbbabel.BabelReport")
                    oReport.ReportNo = "951"  'Special report rp_951_Array5, with 5 fields = 5 array elements
                    oReport.Heading = "OCR bunter"
                    oReport.Preview = False
                    oReport.Printer = False

                    ' to file; if in use set folder/filename in Custom1Value
                    If Not EmptyString(oProfile.Custom1Value) Then
                        sSpecialReportPath = oProfile.Custom1Value
                        oReport.ExportType = "PDF"
                        oReport.ExportFilename = ReplaceDateTimeStamp(sSpecialReportPath)
                    End If

                    oReport.DetailLevel = 1
                    ' TODO: Call reports the new way !!
                    'oBabelReport.ReportArray = aDoc
                    'oBabelReport.Report = oReport
                    'oBabelReport.BabelFiles = oBabelFiles
                    'oBabelReport.Start()

                    oReport = Nothing
                    oBabelReport = Nothing
                End If

                ' reset reportno
                'oReport.ReportNo = "950"
                'oReport.ExportFilename = sSpecialReportPath

            Else
                TreatBKK_SplitBasedOnKID = True
            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally
            If Not oERPDal Is Nothing Then
                oERPDal.Close()
                oERPDal = Nothing
            End If

        End Try

        Return bReturnValue

    End Function
    Public Function TreatNHST_UK(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' NHST
        ' From Visma
        ' Imports IBANN + BIC, redo to Sortcode + BBAN
        '----------------------------------------------
        Dim sDebetSWIFTAddress As String
        Dim sCountryCode As String

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.StatusCode = 0 Then

                            ' Pick Swift from FromAccount (must be set under ClientSetup)
                            sDebetSWIFTAddress = ""
                            FindAccSWIFTAddr(oPayment.I_Account, oBabelFiles.VB_Profile, oPayment.VB_FilenameOut_ID, sDebetSWIFTAddress)
                            sCountryCode = Mid$(sDebetSWIFTAddress, 5, 2)

                            oPayment.I_CountryCode = sCountryCode
                            oPayment.BANK_I_SWIFTCode = sDebetSWIFTAddress
                            oPayment.BANK_I_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT

                            If oPayment.MON_InvoiceCurrency <> "GBP" Then
                                ' force error in frmCorrect
                                oPayment.MON_InvoiceCurrency = "XXX"

                                ' Testing IBANN
                            ElseIf Not IsIBANNumber(oPayment.E_Account, True, False) Then
                                ' Then check if sortcode and BBAN; (new 09.06.2011)
                                If Len(oPayment.BANK_BranchNo) = 6 And oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode Then
                                    ' AccountNo len = 8 ?
                                    If Len(oPayment.E_Account) = 8 Then
                                        oPayment.BANK_AccountCountryCode = "GB"
                                        oPayment.BANK_CountryCode = "GB"
                                    Else
                                        ' Force error in frmCorrect
                                        oPayment.E_Account = "-" & oPayment.E_Account
                                    End If
                                Else
                                    ' Force error in frmCorrect
                                    oPayment.E_Account = "-" & oPayment.E_Account
                                End If


                                ' First, check if IBANN starts with GB and OK IBANN and GBP, if so; ACH (BACS)
                                ' Otherwise, GEN
                            ElseIf Left$(oPayment.E_Account, 2) = "GB" And _
                                oPayment.MON_InvoiceCurrency = "GBP" And IsIBANNumber(oPayment.E_Account, True, False) Then
                                ' Must deduct BBAN account and sortcode from IBAN
                                ' UK IBAN:
                                ' (22) IBAN format: GBkk BBBB SSSS SSCC CCCC CC
                                ' B = alphabetical bank code, S = sort code (often a specific branch), C = account No.

                                ' adjust account;
                                oPayment.BANK_BranchNo = Mid$(oPayment.E_Account, 9, 6)
                                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode
                                ' put BBAN in Account, last 8 digits
                                oPayment.E_Account = Right$(oPayment.E_Account, 8)
                                oPayment.BANK_AccountCountryCode = "GB"
                                oPayment.BANK_CountryCode = "GB"

                            End If

                            ' XokNET 06.05.2012 Sometimes they use UK as countrycode receiver
                            If oPayment.E_CountryCode = "UK" Then
                                oPayment.E_CountryCode = "GB"
                            End If

                            oPayment.PayType = "M"
                            oPayment.DnBNORTBIPayType = "ACH"

                            ' Will result in ACH-payment. Must adjust some info
                            oPayment.E_Name = Left$(oPayment.E_Name, 18)
                            oPayment.E_Adr1 = ""
                            oPayment.E_Adr2 = ""
                            oPayment.E_Adr3 = ""

                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatNHST_UK = True

    End Function
    Public Function TreatNHST_UK_Return(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' NHST
        ' Returnfile to Visma
        '----------------------------------------------
        For Each oBabelFile In oBabelFiles
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    oPayment.PayCode = "150"
                    oPayment.PayType = "I"
                Next oPayment
            Next oBatch
        Next oBabelFile

        TreatNHST_UK_Return = True

    End Function
    ' XNET 27.05.2011 new function
    ' XOKNET 13.10.2015 added G3-handling
    Public Function TreatNHST_SG(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFilesetup_ID As Integer) As Boolean
        ' NHST
        ' From Visma
        ' Redo bank+branch, by removing "-"
        '----------------------------------------------
        Dim sDebetSWIFTAddress As String
        Dim sCountryCode As String

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.StatusCode = 0 Then

                            ' 13.05.2020
                            ' OCBC bank in Singapore can have only one paymentdate in file.
                            ' Redo all dates to next bankdate
                            oPayment.DATE_Payment = DateToString(GetBankday(Today, "SG", 1))

                            ' Pick Swift from FromAccount (must be set under ClientSetup)
                            sDebetSWIFTAddress = ""
                            FindAccSWIFTAddr(oPayment.I_Account, oBabelFiles.VB_Profile, oPayment.VB_FilenameOut_ID, sDebetSWIFTAddress)
                            sCountryCode = Mid$(sDebetSWIFTAddress, 5, 2)

                            oPayment.I_CountryCode = sCountryCode
                            oPayment.BANK_I_SWIFTCode = sDebetSWIFTAddress
                            oPayment.BANK_I_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT


                            ' First, check if SGD
                            If oPayment.MON_InvoiceCurrency <> "SGD" Then
                                'oPayment.Exported = True
                                'oPayment.StatusCode = "35"   '"Feil valutasort"
                                'oBabelFile.RejectsExists = True
                                oPayment.MON_InvoiceCurrency = "XXX"  ' Must "force" a currencyerror
                            End If

                            ' Redo bankcode-bankbranch
                            oPayment.PayType = "D"
                            oPayment.BANK_AccountCountryCode = "SG"
                            oPayment.BANK_CountryCode = "SG"

                            ' 26.02.2019 - Changed to OCBC bank SG - 140 long names!
                            'oPayment.E_Name = Left$(oPayment.E_Name, 20)  ' max 20 in Singapore
                            oPayment.E_Adr1 = ""
                            oPayment.E_Adr2 = ""
                            oPayment.E_Adr3 = ""

                            ' 13.10.2015
                            ' Added for DNB Singapore G3
                            If oPayment.DATE_Payment > "20151122" Then
                                ' G3 is ruling !
                                ' Copy Ref_Own to EndToEndRef
                                oPayment.REF_EndToEnd = oPayment.REF_Own
                                ' set SUPP as Purposecode, if not given from BB_Setup
                                If EmptyString(oPayment.PurposeCode) Then
                                    ' find default PurposeCode from Filesetup
                                    oPayment.PurposeCode = oBabelFiles.VB_Profile.FileSetups(iFilesetup_ID).StandardPurposeCode
                                    If EmptyString(oPayment.PurposeCode) Then
                                        'oPayment.PurposeCode = "SUPP"
                                        ' 01.03.2019 - for OCBC bank, default to OTHR
                                        oPayment.PurposeCode = "OTHR"
                                    End If
                                End If
                            Else
                                ' before G3 - remove SWIFT
                                ' remove SWIFT, not in use
                                oPayment.BANK_SWIFTCode = ""
                                oPayment.BANK_BranchNo = Replace(oPayment.BANK_BranchNo, "-", "")
                                oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.MEPS
                            End If

                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatNHST_SG = True

    End Function
    Public Function TreatNHST_US(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' NHST
        ' From Visma
        ' For Check payments - redo from CHECK in accountno
        '--------------------------------------------------
        Dim sTmp As String
        Dim i As Integer

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.StatusCode = 0 Then

                            If oPayment.E_Account = "CHECK" Or oPayment.E_Account = "A CHEQUE" Then
                                oPayment.E_Account = ""
                                oPayment.Cheque = vbBabel.BabelFiles.ChequeType.SendToReceiver

                                ' XNET 21.01.2013/12.02.2013 - problems with name on checks in TelepayPlus.
                                ' Move name from Adr2 till City
                                If EmptyString(oPayment.E_City) Then
                                    ' 12.02.2013 - If E_Adr3 is used, then the "zip/city/state part" is in E_Adr3, otherwise E_Adr2
                                    If Not EmptyString(oPayment.E_Adr3) Then
                                        oPayment.E_City = oPayment.E_Adr3
                                        oPayment.E_Adr3 = ""
                                    Else
                                        oPayment.E_City = oPayment.E_Adr2
                                        oPayment.E_Adr2 = ""
                                    End If
                                    '12.02.2013 - put zip at end, like from "10523ELMSFORD, NY" to "ELMSFORD, NY 10523"
                                    ' find all digits and -, and move from start to end
                                    sTmp = ""
                                    For i = 1 To Len(oPayment.E_City)
                                        If IsNumeric(Mid$(oPayment.E_City, i, 1)) Or Mid$(oPayment.E_City, i, 1) = "-" Then
                                            sTmp = sTmp & Mid$(oPayment.E_City, i, 1)
                                        Else
                                            ' no more digits, exit
                                            Exit For
                                        End If
                                    Next i
                                    ' remove the digits part from e_City
                                    oPayment.E_City = Replace(oPayment.E_City, sTmp, "")
                                    ' add digits to end
                                    oPayment.E_City = oPayment.E_City & " " & sTmp

                                End If
                                ' XNET 06.05.2012 - Either Check or ACH for NHST US
                            Else
                                oPayment.DnBNORTBIPayType = "ACH"
                            End If

                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatNHST_US = True

    End Function
    Public Function TreatNHST_LonnXML(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFilesetup_ID As Integer) As Boolean
        ' NHST - Salary, common file for all countries, all formats
        ' From Unit4 - Travel&Expence (.csv-file), and Salary (Telepay file)
        ' Split into two;
        ' DNB Pain.001 XML
        ' OCBC bank Giro format
        '-------------------------------------------------------------------
        Dim sDebetSWIFTAddress As String
        Dim sCountryCode As String
        Dim oTempBatch As vbBabel.Batch
        Dim i As Integer, j As Integer
        Dim oTempPayment As vbBabel.Payment
        Dim oNewbatch As vbBabel.Batch
        Dim oNewPayment As vbBabel.Payment
        Dim iPaymentIndex As Long

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.StatusCode = 0 Then

                            Select Case oPayment.BANK_CountryCode

                                Case "GB"
                                    ' Pick Swift from FromAccount (must be set under ClientSetup)
                                    sDebetSWIFTAddress = ""
                                    FindAccSWIFTAddr(oPayment.I_Account, oBabelFiles.VB_Profile, oPayment.VB_FilenameOut_ID, sDebetSWIFTAddress)
                                    sCountryCode = Mid$(sDebetSWIFTAddress, 5, 2)

                                    oPayment.I_CountryCode = sCountryCode
                                    oPayment.BANK_I_SWIFTCode = sDebetSWIFTAddress
                                    oPayment.BANK_I_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT

                                    If sCountryCode = "GB" Then
                                        oPayment.BANK_I_Domestic = True
                                        If oPayment.MON_InvoiceCurrency <> "GBP" Then
                                            ' Force error in frmCorrect
                                            oPayment.MON_InvoiceCurrency = "XXX"
                                            ' Testing IBANN
                                        ElseIf Not IsIBANNumber(oPayment.E_Account, True, False) Then
                                            ' NHST has set some of the sortcodes as 12-34-56
                                            oPayment.BANK_BranchNo = Replace(oPayment.BANK_BranchNo, "-", "")

                                            ' Then check if sortcode and BBAN; (new 09.06.2011)
                                            If Len(oPayment.BANK_BranchNo) = 6 Then
                                                ' AccountNo len = 8 ?
                                                If Len(oPayment.E_Account) = 8 Then
                                                    oPayment.BANK_AccountCountryCode = "GB"
                                                    oPayment.BANK_CountryCode = "GB"
                                                    ' because of 12-34-56 in sortcode, the import does not treat it as sortcode
                                                    oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode
                                                Else
                                                    ' Force error in frmCorrect
                                                    oPayment.E_Account = ""
                                                End If
                                            Else
                                                ' Force error in frmCorrect
                                                oPayment.E_Account = ""
                                            End If
                                            oPayment.DnBNORTBIPayType = "SAL"

                                            ' First, check if IBANN starts with GB and OK IBANN and GBP, if so; ACH (BACS)
                                            ' Otherwise, GEN
                                        ElseIf Left$(oPayment.E_Account, 2) = "GB" And _
                                            oPayment.MON_InvoiceCurrency = "GBP" And IsIBANNumber(oPayment.E_Account, True, False) Then
                                            ' Must deduct BBAN account and sortcode from IBAN
                                            ' UK IBAN:
                                            ' (22) IBAN format: GBkk BBBB SSSS SSCC CCCC CC
                                            ' B = alphabetical bank code, S = sort code (often a specific branch), C = account No.

                                            ' adjust account;
                                            oPayment.BANK_BranchNo = Mid$(oPayment.E_Account, 9, 6)
                                            oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode
                                            ' put BBAN in Account, last 8 digits
                                            oPayment.E_Account = Right$(oPayment.E_Account, 8)
                                            oPayment.BANK_AccountCountryCode = "GB"
                                            oPayment.BANK_CountryCode = "GB"

                                        End If

                                        ' 22.04.2022  removed the below line for new T&E solution
                                        'oPayment.PayType = "S"
                                        'oPayment.DnBNORTBIPayType = "SAL"
                                        oPayment.VoucherType = "TP+"

                                        ' Will result in ACH-payment. Must adjust some info
                                        oPayment.E_Name = Left$(oPayment.E_Name, 18)
                                        oPayment.E_Adr1 = ""
                                        oPayment.E_Adr2 = ""
                                        oPayment.E_Adr3 = ""
                                    Else
                                        ' debitaccount not in GB
                                        oPayment.VoucherType = "TP"
                                        oPayment.BANK_I_Domestic = False
                                    End If

                                Case "US"
                                    ' Pick Swift from FromAccount (must be set under ClientSetup)
                                    sDebetSWIFTAddress = ""
                                    FindAccSWIFTAddr(oPayment.I_Account, oBabelFiles.VB_Profile, oPayment.VB_FilenameOut_ID, sDebetSWIFTAddress)
                                    sCountryCode = Mid$(sDebetSWIFTAddress, 5, 2)

                                    If sCountryCode = "US" Then
                                        oPayment.BANK_I_Domestic = True

                                        oPayment.I_CountryCode = sCountryCode
                                        oPayment.BANK_I_SWIFTCode = sDebetSWIFTAddress
                                        oPayment.BANK_I_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT
                                        If Not EmptyString(oPayment.E_Account) Then
                                            ' XNET 22.12.2011 - changed for Personsaccount
                                            'oPayment.E_Account = oPayment.E_Account & "P"
                                            oPayment.E_AccountSuffix = "PPD"
                                            oPayment.DnBNORTBIPayType = "ACH"
                                        End If
                                        ' Must also mark this as PPD in batch
                                        oBatch.Version = "PPD"

                                        ' First, check if USD
                                        If oPayment.MON_InvoiceCurrency <> "USD" Then
                                            oPayment.MON_InvoiceCurrency = "XXX"  ' Must "force" a currencyerror
                                        End If

                                        oPayment.PayType = "D"
                                        oPayment.VoucherType = "Nacha"

                                        ' Will result in ACH-payment. Must adjust some info
                                        oPayment.E_Name = Left$(oPayment.E_Name, 18)
                                        oPayment.E_Adr1 = ""
                                        oPayment.E_Adr2 = ""
                                        oPayment.E_Adr3 = ""
                                        oPayment.E_Adr3 = ""

                                        ' Must also add days to Nacha-payments, if set in setup
                                        oPayment.DATE_Payment = DateToString(GetBankday(StringToDate(oPayment.DATE_Payment), "US", oBabelFiles.VB_Profile.FileSetups(iFilesetup_ID).AddDays))
                                        ' if .Date_Payment is and old, date, it is no use in adding DelDays to an old date.
                                        ' Must first test if it is old!
                                        If oPayment.DATE_Payment < DateToString(GetBankday(Date.Today, "US", oBabelFiles.VB_Profile.FileSetups(iFilesetup_ID).AddDays)) Then
                                            ' must add to todays date!!!
                                            oPayment.DATE_Payment = DateToString(GetBankday(Date.Today, "US", oBabelFiles.VB_Profile.FileSetups(iFilesetup_ID).AddDays))
                                        End If
                                    Else
                                        ' debitaccount not in US
                                        oPayment.VoucherType = "TP"
                                        oPayment.BANK_I_Domestic = False
                                    End If

                                Case "SG"
                                    ' Pick Swift from FromAccount (must be set under ClientSetup)
                                    sDebetSWIFTAddress = ""
                                    FindAccSWIFTAddr(oPayment.I_Account, oBabelFiles.VB_Profile, oPayment.VB_FilenameOut_ID, sDebetSWIFTAddress)
                                    sCountryCode = Mid$(sDebetSWIFTAddress, 5, 2)

                                    If sCountryCode = "SG" Then
                                        oPayment.BANK_I_Domestic = True

                                        oPayment.I_CountryCode = sCountryCode
                                        oPayment.BANK_I_SWIFTCode = sDebetSWIFTAddress
                                        oPayment.BANK_I_BranchType = vbBabel.BabelFiles.BankBranchType.MEPS


                                        ' First, check if SGD
                                        If oPayment.MON_InvoiceCurrency <> "SGD" Then
                                            oPayment.MON_InvoiceCurrency = "XXX"  ' Must "force" a currencyerror
                                        End If

                                        If oPayment.BANK_CountryCode = "SG" And oPayment.MON_InvoiceCurrency = "SGD" Then
                                            ' Copy Ref_Own to EndToEndRef
                                            oPayment.REF_EndToEnd = Trim(oPayment.REF_Own)
                                            ' set SUPP as Purposecode, if not given from BB_Setup
                                            If EmptyString(oPayment.PurposeCode) Then
                                                ' find default PurposeCode from Filesetup
                                                oPayment.PurposeCode = oBabelFiles.VB_Profile.FileSetups(iFilesetup_ID).StandardPurposeCode
                                                If EmptyString(oPayment.PurposeCode) Then
                                                    oPayment.PurposeCode = "SALA"
                                                End If
                                            End If
                                            ' Use BIC, not Bankcode
                                            oPayment.BANK_BranchNo = ""
                                            oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT
                                        End If
                                        oPayment.BANK_AccountCountryCode = "SG"
                                        oPayment.BANK_CountryCode = "SG"

                                        oPayment.PayType = "S"
                                        oPayment.DnBNORTBIPayType = "OCB"   '"SAL"
                                        ' OCBC bank Singapore
                                        oPayment.VoucherType = "OCB"

                                        oPayment.E_Name = Left$(oPayment.E_Name, 20)  ' max 20 in Singapore
                                        oPayment.E_Adr1 = ""
                                        oPayment.E_Adr2 = ""
                                        oPayment.E_Adr3 = ""
                                    Else
                                        ' debitaccount not in SG
                                        oPayment.VoucherType = "TP"
                                        oPayment.BANK_I_Domestic = False
                                    End If

                                Case Else
                                    ' Telepay, do nothing
                                    oPayment.VoucherType = "TP"
                                    ' Must test if it is blank before we default to NO !!!
                                    If EmptyString(oPayment.BANK_CountryCode) Then
                                        oPayment.BANK_CountryCode = "NO"
                                    End If
                                    ' her m� vi v�re forsiktige - er �gs� med BETFOR21/23 for skatteoverf�ring
                                    If oPayment.PayType = "S" Then
                                        oPayment.DnBNORTBIPayType = "SAL"
                                    End If
                                    oPayment.VoucherType = "TBI"
                                    FindAccSWIFTAddr(oPayment.I_Account, oBabelFiles.VB_Profile, oPayment.VB_FilenameOut_ID, sDebetSWIFTAddress)
                                    oPayment.BANK_I_SWIFTCode = sDebetSWIFTAddress
                                    sCountryCode = Mid$(sDebetSWIFTAddress, 5, 2)
                                    oPayment.I_CountryCode = sCountryCode
                                    ' 12.04.2019 - moved here from higher up
                                    If sCountryCode <> oPayment.BANK_CountryCode Then
                                        oPayment.BANK_I_Domestic = False
                                    End If
                            End Select

                        End If  'If oPayment.StatusCode = 0 Then
                    Next
                Next
            Next

            ' XNET 30.01.2012-
            ' To be able to have correct output in Pain001, we must have separate batches pr
            ' - each new account
            ' - shift between Bank_I_Domestic = False / True

            ' First, put all into one Batch pr BabelFile
            For Each oBabelFile In oBabelFiles
                iPaymentIndex = 0
                i = 0
                If oBabelFile.Batches.Count > 1 Then
                    ' copy first batch to a new batch
                    oTempBatch = oBabelFile.Batches(1)
                    oNewbatch = New vbBabel.Batch
                    ' Copy from first batch to last, new batch
                    CopyBatchObject(oTempBatch, oNewbatch)

                    ' copy each payment from batch 2 and upwards to newly added batch
                    For i = 2 To oBabelFile.Batches.Count
                        oBatch = oBabelFile.Batches.Item(i)
                        For Each oPayment In oBatch.Payments
                            oTempPayment = oPayment
                            ' Add payment to new batch
                            oNewPayment = oNewbatch.Payments.Add
                            ' Copy payment
                            CopyPaymentObject(oTempPayment, oNewPayment)
                            iPaymentIndex = iPaymentIndex + 1
                            oNewPayment.Index = iPaymentIndex
                        Next oPayment
                    Next i
                    ' then remove all but last batch
                    For i = oBabelFile.Batches.Count To 1 Step -1
                        oBabelFile.Batches.Remove(i)
                    Next i
                    ' then add the new batch to the end of oBabelFiles
                    oNewbatch = oBabelFile.Batches.VB_AddWithObject(oNewbatch)

                End If

                ' then "reindex" the batch and payments inside
                i = 0
                For Each oBatch In oBabelFile.Batches
                    i = i + 1
                    oBatch.Index = i
                    oBatch.ReIndexPayments()
                Next oBatch
            Next oBabelFile

            ' Now we have one batch pr babelfile
            'Organize into one "Int"-batch, and one for others
            For Each oBabelFile In oBabelFiles
                oTempBatch = oBabelFile.Batches(1)
                oNewbatch = New vbBabel.Batch

                ' Copy from first batch to second
                CopyBatchObject(oTempBatch, oNewbatch)
                oNewbatch = oBabelFile.Batches.VB_AddWithObject(oNewbatch)

                ' delete Int from first batch
                For i = oBabelFile.Batches(1).Payments.Count To 1 Step -1
                    oPayment = oBabelFile.Batches(1).Payments(i)
                    If oPayment.BANK_I_Domestic = False Then
                        oBabelFile.Batches(1).Payments.Remove(i)
                    End If
                Next i
                ' delete Dom from second batch
                For i = oBabelFile.Batches(2).Payments.Count To 1 Step -1
                    oPayment = oBabelFile.Batches(2).Payments(i)
                    If oPayment.BANK_I_Domestic = True Then
                        oBabelFile.Batches(2).Payments.Remove(i)
                    End If
                Next i

                ' then "reindex" the batch and payments inside
                i = 0
                For Each oBatch In oBabelFile.Batches
                    i = i + 1
                    oBatch.Index = i
                    j = 0
                    oBatch.ReIndexPayments()
                Next oBatch
            Next oBabelFile

            ' 23.03.2022 - remove empty batches
            For Each oBabelFile In oBabelFiles
                For i = oBabelFile.Batches.Count To 1 Step -1
                    If oBabelFile.Batches(i).Payments.Count = 0 Then
                        oBabelFile.Batches.Remove(i)
                    End If
                Next
            Next oBabelFile


        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatNHST_LonnXML = True

    End Function

    Public Function TreatNHST_Lonn(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFilesetup_ID As Integer) As Boolean
        ' NHST - UNit4 L�nn, for SG Only
        '----------------------------------------------
        Dim sDebetSWIFTAddress As String
        Dim sCountryCode As String
        Dim oTempBatch As vbBabel.Batch
        Dim i As Integer, j As Integer
        Dim oTempPayment As vbBabel.Payment
        Dim oNewbatch As vbBabel.Batch
        Dim oNewPayment As vbBabel.Payment
        Dim iPaymentIndex As Long

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.StatusCode = 0 Then

                            ' ALWAYS SG payments, from OCBC bank
                            ' Pick Swift from FromAccount (must be set under ClientSetup)
                            sDebetSWIFTAddress = ""
                            FindAccSWIFTAddr(oPayment.I_Account, oBabelFiles.VB_Profile, oPayment.VB_FilenameOut_ID, sDebetSWIFTAddress)
                            sCountryCode = Mid$(sDebetSWIFTAddress, 5, 2)

                            If sCountryCode = "SG" Then
                                ' XNET 30.01.2011
                                oPayment.BANK_I_Domestic = True

                                oPayment.I_CountryCode = sCountryCode
                                oPayment.BANK_I_SWIFTCode = sDebetSWIFTAddress
                                oPayment.BANK_I_BranchType = vbBabel.BabelFiles.BankBranchType.MEPS

                                ' First, check if SGD
                                If oPayment.MON_InvoiceCurrency <> "SGD" Then
                                    oPayment.MON_InvoiceCurrency = "XXX"  ' Must "force" a currencyerror
                                End If

                                ' 08.04.2022 - always oPayment.BANK_CountryCode = "SG" - overrun if set incorrectly in Unit4
                                'If oPayment.BANK_CountryCode = "SG" And oPayment.MON_InvoiceCurrency = "SGD" Then
                                If oPayment.MON_InvoiceCurrency = "SGD" Then

                                    ' Copy Ref_Own to EndToEndRef
                                    oPayment.REF_EndToEnd = Trim(oPayment.REF_Own)
                                    ' set SUPP as Purposecode, if not given from BB_Setup
                                    If EmptyString(oPayment.PurposeCode) Then
                                        ' find default PurposeCode from Filesetup
                                        oPayment.PurposeCode = oBabelFiles.VB_Profile.FileSetups(iFilesetup_ID).StandardPurposeCode
                                        If EmptyString(oPayment.PurposeCode) Then
                                            oPayment.PurposeCode = "SALA"
                                        End If
                                    End If
                                    ' 08.12.2015, added next
                                    ' Use BIC, not Bankcode
                                    oPayment.BANK_BranchNo = ""
                                    oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT

                                End If
                                oPayment.BANK_AccountCountryCode = "SG"
                                oPayment.BANK_CountryCode = "SG"

                                oPayment.PayType = "S"
                                oPayment.DnBNORTBIPayType = "OCB"   '"SAL"
                                oPayment.VoucherType = "OCB"

                                oPayment.E_Name = Left$(oPayment.E_Name, 20)  ' max 20 in Singapore
                                oPayment.E_Adr1 = ""
                                oPayment.E_Adr2 = ""
                                oPayment.E_Adr3 = ""

                                ' added 27.04.2022
                            ElseIf sCountryCode = "GB" Then
                                oPayment.PayType = "S"
                                oPayment.DnBNORTBIPayType = "ACH"
                                oPayment.BANK_I_Domestic = True
                            ElseIf sCountryCode = "NO" Then 'ny 29.04.2022
                                oPayment.PayType = "I"
                                oPayment.DnBNORTBIPayType = "GEN"
                                oPayment.BANK_I_Domestic = False
                            Else
                                ' debitaccount not in SG, GB
                                oPayment.VoucherType = "TP"
                                oPayment.BANK_I_Domestic = False
                            End If

                        End If  'If oPayment.StatusCode = 0 Then
                    Next
                Next
            Next

            ' XNET 30.01.2012-
            ' To be able to have correct output in TelepayPlus, we must have separate batches pr
            ' - each new account
            ' - shift between Bank_I_Domestic = False / True

            ' First, put all into one Batch pr BabelFile
            For Each oBabelFile In oBabelFiles
                iPaymentIndex = 0
                i = 0
                If oBabelFile.Batches.Count > 1 Then
                    ' copy first batch to a new batch
                    oTempBatch = oBabelFile.Batches(1)
                    oNewbatch = New vbBabel.Batch
                    ' Copy from first batch to last, new batch
                    CopyBatchObject(oTempBatch, oNewbatch)

                    ' copy each payment from batch 2 and upwards to newly added batch
                    For i = 2 To oBabelFile.Batches.Count
                        oBatch = oBabelFile.Batches.Item(i)
                        For Each oPayment In oBatch.Payments
                            oTempPayment = oPayment
                            ' Add payment to new batch
                            oNewPayment = oNewbatch.Payments.Add
                            ' Copy payment
                            CopyPaymentObject(oTempPayment, oNewPayment)
                            iPaymentIndex = iPaymentIndex + 1
                            oNewPayment.Index = iPaymentIndex
                        Next oPayment
                    Next i
                    ' then remove all but last batch
                    For i = oBabelFile.Batches.Count To 1 Step -1
                        oBabelFile.Batches.Remove(i)
                    Next i
                    ' then add the new batch to the end of oBabelFiles
                    oNewbatch = oBabelFile.Batches.VB_AddWithObject(oNewbatch)

                End If

                ' then "reindex" the batch and payments inside
                i = 0
                For Each oBatch In oBabelFile.Batches
                    i = i + 1
                    oBatch.Index = i
                    oBatch.ReIndexPayments()
                Next oBatch
            Next oBabelFile

            ' Now we have one batch pr babelfile
            'Organize into one "Int"-batch, and one for others
            For Each oBabelFile In oBabelFiles
                oTempBatch = oBabelFile.Batches(1)
                oNewbatch = New vbBabel.Batch

                ' Copy from first batch to second
                CopyBatchObject(oTempBatch, oNewbatch)
                oNewbatch = oBabelFile.Batches.VB_AddWithObject(oNewbatch)

                ' delete Int from first batch
                For i = oBabelFile.Batches(1).Payments.Count To 1 Step -1
                    oPayment = oBabelFile.Batches(1).Payments(i)
                    If oPayment.BANK_I_Domestic = False Then
                        oBabelFile.Batches(1).Payments.Remove(i)
                    End If
                Next i
                ' delete Dom from second batch
                For i = oBabelFile.Batches(2).Payments.Count To 1 Step -1
                    oPayment = oBabelFile.Batches(2).Payments(i)
                    If oPayment.BANK_I_Domestic = True Then
                        oBabelFile.Batches(2).Payments.Remove(i)
                    End If
                Next i

                ' then "reindex" the batch and payments inside
                i = 0
                For Each oBatch In oBabelFile.Batches
                    i = i + 1
                    oBatch.Index = i
                    j = 0
                    oBatch.ReIndexPayments()
                Next oBatch
            Next oBabelFile

            ' 23.03.2022 - remove empty batches
            For Each oBabelFile In oBabelFiles
                For i = oBabelFile.Batches.Count To 1 Step -1
                    If oBabelFile.Batches(i).Payments.Count = 0 Then
                        oBabelFile.Batches.Remove(i)
                    End If
                Next
            Next oBabelFile

            Dim nBatchTotal As Double = 0
            ' 02.05.2022 - recalculate batch amounts
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    nBatchTotal = 0
                    For Each oPayment In oBatch.Payments
                        nBatchTotal = nBatchTotal + oPayment.MON_InvoiceAmount
                    Next oPayment
                    oBatch.MON_InvoiceAmount=nBatchTotal
                Next oBatch
            Next oBabelFile

            '---------------------
            ' 02.05.2022
            ' Lag grunnlag for rapport med delsum pr valuta.
            ' Gj�r dette i en egen oBabelFiles, hvor vi har en oBabelFile, med en oBatch pr valuta
            Dim oNewBabelFiles As vbBabel.BabelFiles
            Dim oNewBabel As vbBabel.BabelFile
            Dim oNewBatches As vbBabel.Batches
            'Dim oNewBatch As vbBabel.Batch
            Dim oNewPayments As vbBabel.Payments
            'Dim oNewPayment As vbBabel.Payment
            Dim sCurrency As String = ""
            Dim bFoundBatch As Boolean = False

            oNewBabelFiles = New vbBabel.BabelFiles
            oNewBabelFiles.VB_Profile = oBabelFiles.VB_Profile
            oNewBabel = CopyBabelFileObject(oBabelFile)
            oNewBabelFiles.VB_AddWithObject(oNewBabel)
            oNewBabel.Batches = Nothing

            oNewBatches = New vbBabel.Batches
            oNewBabel.Batches = oNewBatches
            'oNewBabelFiles.VB_AddWithObject(oNewBabel)

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        sCurrency = oPayment.MON_InvoiceCurrency

                        ' check if we have a batch with this currency:
                        bFoundBatch = False
                        For Each oNewbatch In oNewBabel.Batches
                            If oNewbatch.Payments(1).MON_InvoiceCurrency = sCurrency Then
                                ' we already have a batch with this currency
                                ' add to total
                                oNewbatch.Payments(1).MON_InvoiceAmount = oNewbatch.Payments(1).MON_InvoiceAmount + oPayment.MON_InvoiceAmount
                                oNewbatch.MON_InvoiceAmount = oNewbatch.Payments(1).MON_InvoiceAmount  ' set batch amount = payments amount (only one payment in batch)
                                oNewbatch.MON_BalanceIN = oNewbatch.MON_BalanceIN + 1  ' store no of payments here
                                bFoundBatch = True
                                Exit For
                            End If
                        Next

                        If Not bFoundBatch Then
                            ' create a new batch for this currency
                            oNewbatch = New vbBabel.Batch
                            CopyBatchObject(oBatch, oNewbatch)  '????

                            oNewbatch.Payments = Nothing

                            oNewBabel.Batches.VB_AddWithObject(oNewbatch)

                            oNewPayments = New vbBabel.Payments
                            oNewbatch.Payments = oNewPayments

                            oNewPayment = oNewbatch.Payments.Add()
                            'oNewPayment = New vbBabel.Payment
                            CopyPaymentObject(oPayment, oNewPayment)
                            oNewPayment.PayCode = "601"
                            oNewbatch.MON_InvoiceAmount = oNewPayment.MON_InvoiceAmount
                            oNewbatch.MON_BalanceIN = 1  ' store no of payments here

                        End If
                    Next
                Next
            Next

            ' call on rp_001
            '28.04.2020 - Added to create charges report
            Dim oBabelReport As vbBabel.BabelReport

            oBabelReport = New vbBabel.BabelReport
            oBabelReport.Special = "NHST_LONN"
            oBabelReport.BabelFiles = oNewBabelFiles
            oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.Inside_SpecialRoutines
            oBabelReport.ReportNumber = "001"

            If Not oBabelReport.RunReports(iFilesetup_ID) Then

            End If
            '---

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatNHST_Lonn = True

    End Function

    Public Function TreatSplitBatchInDomesticAndInt(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Split into Domestic and Int batches, for correct Telepayexport
        '--------------------------------------------------------------------------------------
        Dim oTempBatch As vbBabel.Batch
        Dim i As Integer, j As Integer
        Dim oTempPayment As vbBabel.Payment
        Dim oNewbatch As vbBabel.Batch

        'Organize into one "Int"-batch, and one for others
        For Each oBabelFile In oBabelFiles
            oTempBatch = oBabelFile.Batches(1)
            oNewbatch = New vbBabel.Batch

            ' Copy from first batch to second
            CopyBatchObject(oTempBatch, oNewbatch)
            oNewbatch = oBabelFile.Batches.VB_AddWithObject(oNewbatch)

            ' delete Int from first batch
            For i = oBabelFile.Batches(1).Payments.Count To 1 Step -1
                oPayment = oBabelFile.Batches(1).Payments(i)
                If oPayment.PayType = "I" Then
                    oBabelFile.Batches(1).Payments.Remove(i)
                End If
            Next i
            ' delete Dom from second batch
            For i = oBabelFile.Batches(2).Payments.Count To 1 Step -1
                oPayment = oBabelFile.Batches(2).Payments(i)
                'If oPayment.PayType = "D" Then
                ' 02.04.2014 Changed next If, to take care of also "S" and "M"
                If oPayment.PayType <> "I" Then
                    oBabelFile.Batches(2).Payments.Remove(i)
                End If
            Next i

            ' 19.07.2016 - Check if empty batches, if so remove
            For i = oBabelFile.Batches.Count To 1 Step -1
                If oBabelFile.Batches(i).Payments.Count = 0 Then
                    oBabelFile.Batches.Remove(i)
                End If
            Next
        Next

        TreatSplitBatchInDomesticAndInt = True
        Exit Function

    End Function
    Public Function TreatSplitBatchInMassAndDomestic(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Split into masspayments and "ordinary" Domestic, for correct Telepayexport
        '--------------------------------------------------------------------------------------
        Dim oTempBatch As vbBabel.Batch
        Dim i As Long
        Dim oTempPayment As vbBabel.Payment
        Dim oNewbatch As vbBabel.Batch

        'Organize into one "Mass"-batch, and one for others
        For Each oBabelFile In oBabelFiles
            oTempBatch = oBabelFile.Batches(1)
            oNewbatch = New vbBabel.Batch

            ' Copy from first batch to second
            CopyBatchObject(oTempBatch, oNewbatch)
            oNewbatch = oBabelFile.Batches.VB_AddWithObject(oNewbatch)
            ' remove all payments from new batch;
            oNewbatch.Payments = Nothing
            oNewbatch.Payments = New vbBabel.Payments

            ' At this stage, we have one batch (1) with all payments,
            ' and one batch (2) with no payments
            ' Move all NON Mass from 1 to 2
            ' delete NON Mass/Sal from first batch
            For i = oBabelFile.Batches(1).Payments.Count To 1 Step -1
                oPayment = oBabelFile.Batches(1).Payments(i)
                'If oBabelFile.Batches(1).Payments(l).PayType <> "M" And oBabelFile.Batches(1).Payments(l).PayType <> "S" Then
                If oPayment.PayType <> "M" And oPayment.PayType <> "S" Then
                    'oTempPayment = oPayment
                    oPayment = oBabelFile.Batches(2).Payments.VB_AddWithObject(oBabelFile.Batches(1).Payments(i))
                    oBabelFile.Batches(1).Payments.Remove(i)
                End If
            Next i
            '' delete Mass and Sal from second batch
            'For i = oBabelFile.Batches(2).Payments.Count To 1 Step -1
            '    oPayment = oBabelFile.Batches(2).Payments(i)
            '    If Not (oPayment.PayType = "M" Or oPayment.PayType = "S") Then
            '        oBabelFile.Batches(2).Payments.Remove(i)
            '    End If
            'Next i

            ' REINDEX Payments in batch(1) and (2)
            oBabelFile.Batches(1).ReIndexPayments()
            oBabelFile.Batches(2).ReIndexPayments()
        Next

        TreatSplitBatchInMassAndDomestic = True
        Exit Function

    End Function
    ' XNET 09.09.2011 Hurray !-
    ' Prepare Cremulexport for DnV US
    Public Function TreatDnV_US_Cremul(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        '--------------------------------------------------------------------------------------
        Dim oTempBatch As vbBabel.Batch
        Dim i As Integer, j As Integer
        Dim oTempPayment As vbBabel.Payment
        Dim oNewbatch As vbBabel.Batch
        Dim bStructured As Boolean
        Dim nInvoiceRemoved As Double
        Dim nBabelTotal As Double
        Dim nBatchTotal As Double
        Dim nPaymentTotal As Double
        Dim nInvoiceTotal As Double



        ' TESTING:
        '------------
        'Dim nNoOfBabels As Long
        'Dim nNoOfBatches As Long
        'Dim nNoOfPayments As Long
        'Dim nNoOfInvoices As Long
        'Dim nBabelTotal As Double
        'Dim nBatchTotal As Double
        'Dim nPaymentTotal As Double
        'Dim nInvoiceTotal As Double
        'Dim nInvoiceRemoved As Double
        '
        'For Each oBabelFile In oBabelFiles
        '    nNoOfBabels = nNoOfBabels + 1
        '    nBabelTotal = nBabelTotal + oBabelFile.MON_InvoiceAmount
        '    For Each oBatch In oBabelFile.Batches
        '        nNoOfBatches = nNoOfBatches + 1
        '        nBatchTotal = nBatchTotal + oBatch.MON_InvoiceAmount
        '        For Each oPayment In oBatch.Payments
        '        nNoOfPayments = nNoOfPayments + 1
        '            nPaymentTotal = nPaymentTotal + oPayment.MON_InvoiceAmount
        '            For Each oInvoice In oPayment.Invoices
        '                nNoOfInvoices = nNoOfInvoices + 1
        '                nInvoiceTotal = nInvoiceTotal + oInvoice.MON_InvoiceAmount
        '            Next
        '        Next
        '    Next
        'Next
        '

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    bStructured = True
                    For Each oPayment In oBatch.Payments
                        nInvoiceTotal = 0

                        ' To be able to report correctly, we have imported "double" set of invoices for some payments
                        ' The "real" ones, have MATCH_Original = True,
                        ' the "fake" ones, have MATCH_Original = False, These have fixed InvoiceNo's where we can find them,
                        ' and better structured texts
                        ' Use them if
                        '   - InvoiceNo is filled in
                        '   - Amount on invoices totals to amount on payment
                        For i = oPayment.Invoices.Count To 1 Step -1
                            ' count totals for Match_Original = False, and where we have an invoiceno (not worth without invoiceno)
                            If oPayment.Invoices.Item(i).MATCH_Original = False And Not EmptyString(oPayment.Invoices.Item(i).InvoiceNo) Then
                                nInvoiceTotal = nInvoiceTotal + oPayment.Invoices.Item(i).MON_InvoiceAmount
                            End If
                        Next i

                        If nInvoiceTotal > 0 And nInvoiceTotal = oPayment.MON_InvoiceAmount Then
                            ' OK, the total for Match_Original = False adds up to oPayment.MON_InvoiceAmount, use Match_Original = False
                            For i = oPayment.Invoices.Count To 1 Step -1
                                If oPayment.Invoices.Item(i).MATCH_Original = True Then
                                    ' delete the "True"
                                    nInvoiceRemoved = nInvoiceRemoved + oPayment.Invoices.Item(i).MON_InvoiceAmount
                                    oPayment.Invoices.Remove(i)
                                End If
                            Next i
                        Else
                            ' the "False" does not match on amount, then use the original = True
                            For i = oPayment.Invoices.Count To 1 Step -1
                                If oPayment.Invoices.Item(i).MATCH_Original = False Then
                                    ' delete the "False"
                                    nInvoiceRemoved = nInvoiceRemoved + oPayment.Invoices.Item(i).MON_InvoiceAmount
                                    oPayment.Invoices.Remove(i)
                                End If
                            Next i
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile



            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    'bStructured = True
                    'For Each oPayment In oBatch.Payments
                    For i = oBatch.Payments.Count To 1 Step -1
                        oPayment = oBatch.Payments.Item(i)
                        bStructured = True
                        For j = oPayment.Invoices.Count To 1 Step -1
                            ' take away the exported ones
                            If oPayment.Exported Then
                                nInvoiceRemoved = nInvoiceRemoved + oPayment.Invoices.Item(j).MON_InvoiceAmount
                                oPayment.Invoices.Remove(j)
                            Else
                                ' If we have been able to extract invoiceno or lockboxno, then set payment as structured
                                If EmptyString(oPayment.Invoices.Item(j).InvoiceNo) Then
                                    bStructured = False
                                End If
                            End If

                        Next j
                        ' If we have been able to extract invoiceno or lockboxno, then set payment as structured
                        ' If all invoices are structured, then;
                        If bStructured And oPayment.Invoices.Count > 0 Then
                            oPayment.PayCode = "629"
                        End If
                        ' XokNET 19.03.2012 - added paytype to front of E_Name, to have it appear at the start of NAD++PL
                        ' Put ACH, LBX or WT in front of name
                        If oPayment.DnBNORTBIPayType = "MT940" Then
                            oPayment.E_Name = "WT-" & oPayment.E_Name
                        Else
                            oPayment.E_Name = oPayment.DnBNORTBIPayType & "-" & oPayment.E_Name
                        End If

                    Next i
                    If oPayment.Exported And i > 0 Then
                        ' remove
                        oBatch.Payments.Remove(i)
                    End If
                Next
            Next

            ' XokNET 25.11.2011 - remove 0-payments (like DirectDebits)
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For i = oBatch.Payments.Count To 1 Step -1
                        oPayment = oBatch.Payments.Item(i)
                        If oPayment.MON_InvoiceAmount = 0 Then
                            ' remove
                            oBatch.Payments.Remove(i)
                        End If
                    Next i
                Next
            Next

            'Organize into one batch pr payment
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    If oBatch.Payments.Count > 1 Then
                        ' do so only if we have more than one payment in batch
                        ' create as many batches as payments
                        For i = 2 To oBatch.Payments.Count
                            oTempBatch = oBatch
                            oNewbatch = New vbBabel.Batch

                            ' Copy from first batch to second
                            CopyBatchObject(oTempBatch, oNewbatch)
                            oNewbatch = oBabelFile.Batches.VB_AddWithObject(oNewbatch)

                            ' delete all payments BUT "i" from second batch
                            For j = oNewbatch.Payments.Count To 1 Step -1
                                If j <> i Then
                                    oNewbatch.Payments.Remove(j)
                                Else
                                    ' update batchtotals
                                    oNewbatch.MON_InvoiceAmount = oNewbatch.Payments.Item(j).MON_InvoiceAmount
                                    oNewbatch.MON_TransferredAmount = oNewbatch.Payments.Item(j).MON_TransferredAmount
                                End If
                            Next j
                        Next i
                        ' Then from first batch (original), delete all but payment(1)
                        For j = oBatch.Payments.Count To 1 Step -1
                            If j <> 1 Then
                                oBatch.Payments.Remove(j)
                            Else
                                ' update batchtotals
                                oNewbatch.MON_InvoiceAmount = oNewbatch.Payments.Item(1).MON_InvoiceAmount
                                oNewbatch.MON_TransferredAmount = oNewbatch.Payments.Item(1).MON_TransferredAmount
                            End If
                        Next j
                    End If
                Next oBatch
            Next oBabelFile


            ' Update amounts on each level
            nBabelTotal = 0
            For Each oBabelFile In oBabelFiles
                nBatchTotal = 0
                For Each oBatch In oBabelFile.Batches
                    nPaymentTotal = 0
                    For Each oPayment In oBatch.Payments
                        nInvoiceTotal = 0
                        i = 0
                        For Each oInvoice In oPayment.Invoices
                            ' reindex Index in Invoice, since we may have deleted some
                            i = i + 1
                            oInvoice.Index = i
                            nInvoiceTotal = nInvoiceTotal + (oInvoice.MON_InvoiceAmount - oInvoice.MON_DiscountAmount)
                            ' XokNET 21.11.2011 - we do not seem to use MON_DiscountAmount when producing Cremulfiles. Must deduct here-
                            oInvoice.MON_InvoiceAmount = oInvoice.MON_InvoiceAmount - oInvoice.MON_DiscountAmount
                            ' If no freetext, add a dummy freetext
                            If oInvoice.Freetexts.Count = 0 Then
                                oFreeText = oInvoice.Freetexts.Add
                                oFreeText.Text = "-"
                            End If
                            oInvoice.MATCH_Original = True
                        Next
                        ' XokNET 08.12.2011 - do not update paymentamount if Invoiceamount = 0. This may happen when error in production in DNB
                        ' added And nInvoiceTotal > 0
                        If oPayment.Invoices.Count > 0 And nInvoiceTotal > 0 Then
                            oPayment.MON_InvoiceAmount = nInvoiceTotal
                            oPayment.MON_TransferredAmount = nInvoiceTotal
                            nPaymentTotal = nPaymentTotal + oPayment.MON_InvoiceAmount
                        Else
                            nPaymentTotal = nPaymentTotal + oPayment.MON_InvoiceAmount
                        End If
                    Next
                    oBatch.MON_InvoiceAmount = nPaymentTotal
                    oBatch.MON_TransferredAmount = nPaymentTotal
                    nBatchTotal = nBatchTotal + oBatch.MON_InvoiceAmount
                Next
                oBabelFile.MON_InvoiceAmount = nBatchTotal
                oBabelFile.MON_TransferredAmount = nBatchTotal
                nBabelTotal = nBabelTotal + oBabelFile.MON_InvoiceAmount
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatDnV_US_Cremul = True

    End Function
    ' Special treatment for DnB's testing of ISO20022, import from Excel
    Public Function TreatDnB_Excel_ISO20022(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean

        Try

            For Each oBabelFile In oBabelFiles
                ' add date to <MsgId> (column AM)
                oBabelFile.EDI_MessageNo = oBabelFile.EDI_MessageNo & "_" & Format(Now, "MMddhhmmmm")
                For Each oBatch In oBabelFile.Batches
                    ' -   <PmtInfId> from Excel, add MMddhhmm+l�penr 5. L�penr is added in export of ISO20022
                    oBatch.REF_Own = oBatch.REF_Own & "_" & Format(Now, "MMddhhmm")
                    For Each oPayment In oBatch.Payments
                        '-   <InstrId> Bruk det som er fyllt inn + hent fra <PmtInfId>, og legg p� l�penr
                        oPayment.REF_Own = oPayment.REF_Own & "_" & oBatch.REF_Own & "_" & Format(oPayment.Index, "#######") & "_<InstrId>"
                        '-   <EndToEndId> Bruk det som er fyllt inn + hent fra <PmtInfId>, og legg p� l�penr
                        oPayment.REF_EndToEnd = oPayment.REF_EndToEnd & "_" & oBatch.REF_Own & "_" & Format(oPayment.Index, "#######") & "_<EndToEndId>"
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatDnB_Excel_ISO20022 = True


    End Function
    Public Function TreatKraftKultur(ByRef oBabelFiles As vbBabel.BabelFiles) As Boolean
        '
        ' Redo from *blablabla to Referencepayments
        Dim bIsReference As Boolean

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' check for referencepayments, Finland only
                        If oPayment.MON_InvoiceCurrency = "EUR" Then
                            If Not EmptyString(oPayment.E_Account) Then
                                If oPayment.E_Account.Substring(0, 2) = "FI" Then
                                    bIsReference = True
                                    For Each oInvoice In oPayment.Invoices
                                        ' either set with * as start of freetext (probably not in use)
                                        For Each oFreeText In oInvoice.Freetexts
                                            If Left$(oFreeText.Text, 1) = "*" Then
                                                ' remove *
                                                'oFreeText.Text = Mid$(oFreeText.Text, 2)
                                                'oPayment.PayCode = "301" ' Reference - assumes reference for all in one payment !
                                                'oInvoice.Unique_Id = Trim$(oFreeText.Text)
                                                'oFreeText.Text = ""

                                                'OK
                                            Else
                                                ' OR - set in freetext, but with no sign saying that this is reference
                                                ' We must see if it qualifies as reference by checking the controldigitelse
                                                If IsModulusReference(Trim$(oFreeText.Text)) Then
                                                    'OK
                                                Else
                                                    bIsReference = False
                                                    Exit For
                                                End If
                                            End If
                                        Next
                                    Next

                                    'If it is a reference reformat the to structured info
                                    If bIsReference Then
                                        For Each oInvoice In oPayment.Invoices
                                            ' either set with * as start of freetext (probably not in use)
                                            For Each oFreeText In oInvoice.Freetexts
                                                If Left$(oFreeText.Text, 1) = "*" Then
                                                    ' remove *
                                                    oFreeText.Text = Mid$(oFreeText.Text, 2)
                                                    oPayment.PayCode = "301" ' Reference - assumes reference for all in one payment !
                                                    oInvoice.Unique_Id = Trim$(oFreeText.Text)
                                                    oFreeText.Text = ""
                                                Else
                                                    ' OR - set in freetext, but with no sign saying that this is reference
                                                    ' We must see if it qualifies as reference by checking the controldigitelse
                                                    oPayment.PayCode = "301" ' Reference - assumes reference for all in one payment !
                                                    oInvoice.Unique_Id = Trim$(oFreeText.Text)
                                                    oFreeText.Text = ""
                                                End If
                                            Next
                                        Next
                                    End If

                                End If
                            End If
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatKraftKultur = True

    End Function
    'XNET 09.05.2012 Added TreatREC_SG
    Public Function TreatREC_SG(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Add On Behalf of to text, for Int payments

        Dim i As Integer
        Dim sTmp As String

        Try

            ' M� TESTES !!!!!!!!!
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.PayType = "I" Then
                            For Each oInvoice In oPayment.Invoices
                                For i = 1 To oInvoice.Freetexts.Count
                                    If i = 1 Then
                                        ' In oInvoice.MyField har vi lagret enten REC Module eller Rec Cells
                                        oInvoice.Freetexts(i).Text = "On behalf of " & oInvoice.MyField & " " & oInvoice.Freetexts(1).Text
                                    End If
                                Next i
                            Next
                        End If
                    Next
                Next
            Next
            'M� HUSKE � teste for DETTE !!!! det b�r v�re riktig, da REC Module eller REC Cell ligger f�rst i texten
            '"If the file is from REC Cell, they want the name: On behalf of REC Module", should read
            '"If the file is from REC Cell, they want the name: On behalf of REC Cell"

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatREC_SG = True

    End Function
    ' XNET 24.05.2012 - added function for R�hneSelmer
    Public Function TreatRohneSelmer(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        '
        ' Special handling of Divisjon - fill in from ClientSetup

        Dim bRetValue As Boolean
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean

        Try

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                bAccountFound = False
                For Each oBatch In oBabelFile.Batches
                    sOldAccountNo = "zzzzzzzzzzzzzzzzzzzz"
                    bAccountFound = False
                    For Each oPayment In oBatch.Payments
                        If oPayment.I_Account <> sOldAccountNo Then
                            sOldAccountNo = oPayment.I_Account
                            For Each oFilesetup In oProfile.FileSetups
                                For Each oClient In oFilesetup.Clients
                                    For Each oaccount In oClient.Accounts
                                        If oaccount.Account = oPayment.I_Account Then
                                            bAccountFound = True
                                            ' redo batch / divisjon
                                            oBatch.I_Branch = oClient.Division
                                            Exit For
                                        End If
                                    Next oaccount
                                    If bAccountFound Then
                                        Exit For
                                    End If
                                Next oClient
                                If bAccountFound Then
                                    Exit For
                                End If
                            Next oFilesetup
                            If Not bAccountFound Then
                                ' did not find account in setup!
                                Err.Raise(10001, "TreatRohneSelmer", "Finner ikke konto " & oPayment.I_Account & " i klientoppsettet." & vbCrLf & vbCrLf & "Kontakt Visual Banking!")
                                bRetValue = False
                            Else
                                Exit For
                            End If
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatRohneSelmer = bRetValue

    End Function
    ' XNET 24.05.2012 - added function for R�hneSelmerReturn
    Public Function TreatRohneSelmerReturn(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        '
        ' Special handling for returnfiles - set Foretaksnr back to original, and blank division

        Dim bRetValue As Boolean
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean

        Try

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                bAccountFound = False
                For Each oBatch In oBabelFile.Batches
                    sOldAccountNo = "zzzzzzzzzzzzzzzzzzzz"
                    bAccountFound = False
                    For Each oPayment In oBatch.Payments
                        If oPayment.I_Account <> sOldAccountNo Then
                            sOldAccountNo = oPayment.I_Account
                            For Each oFilesetup In oProfile.FileSetups
                                For Each oClient In oFilesetup.Clients
                                    For Each oaccount In oClient.Accounts
                                        If oaccount.Account = oPayment.I_Account Then
                                            bAccountFound = True
                                            ' reset enterpriseno to original, found in cientsetup
                                            oBatch.I_EnterpriseNo = oClient.ClientNo  ' clientno = original enterpriseno in telepayfile
                                            ' blank batch / divisjon
                                            oBatch.I_Branch = ""
                                            Exit For
                                        End If
                                    Next oaccount
                                    If bAccountFound Then
                                        Exit For
                                    End If
                                Next oClient
                                If bAccountFound Then
                                    Exit For
                                End If
                            Next oFilesetup
                            If Not bAccountFound Then
                                ' did not find account in setup!
                                Err.Raise(10001, "TreatRohneSelmerReturn", "Finner ikke konto " & oPayment.I_Account & " i klientoppsettet." & vbCrLf & vbCrLf & "Kontakt Visual Banking!")
                                bRetValue = False
                            Else
                                Exit For
                            End If
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatRohneSelmerReturn = bRetValue

    End Function
    Public Function TreatThuleSalaryXML(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Change debit BIC and Debit account for salaryfiles from Thule
        Dim NODTemp As System.Xml.XmlElement
        Dim NODTemp2 As System.Xml.XmlElement
        Dim XMLDoc As System.Xml.XmlDocument
        Dim nsMgr As System.Xml.XmlNamespaceManager
        Dim oBabelFile As vbBabel.BabelFile
        Dim sI_Account As String
        Dim bAccountFound As Boolean
        Dim sConvertedAccount As String
        Dim sDebetSwift As String

        Try

            For Each oBabelFile In oBabelFiles
                ' find account from debitinfo in XML-file;
                XMLDoc = oBabelFile.GenericXML

                nsMgr = New System.Xml.XmlNamespaceManager(XMLDoc.NameTable)
                nsMgr.AddNamespace("ISO", XMLDoc.DocumentElement.NamespaceURI)

                ' Remove Issr-tag;
                ' ----------------
                NODTemp = XMLDoc.SelectSingleNode("//ISO:Document/ISO:CstmrCdtTrfInitn/ISO:GrpHdr/ISO:InitgPty/ISO:Id/ISO:OrgId/ISO:Othr", nsMgr)
                NODTemp2 = XMLDoc.SelectSingleNode("//ISO:Document/ISO:CstmrCdtTrfInitn/ISO:GrpHdr/ISO:InitgPty/ISO:Id/ISO:OrgId/ISO:Othr/ISO:Issr", nsMgr)
                NODTemp.RemoveChild(NODTemp2)

                ' Find debitaccount, and replace
                ' ------------------------------
                NODTemp = XMLDoc.SelectSingleNode("//ISO:Document/ISO:CstmrCdtTrfInitn/ISO:PmtInf/ISO:DbtrAcct/ISO:Id/ISO:IBAN", nsMgr)
                sI_Account = NODTemp.InnerText

                For Each oFilesetup In oBabelFile.VB_Profile.FileSetups
                    For Each oClient In oFilesetup.Clients
                        For Each oaccount In oClient.Accounts
                            If sI_Account = oaccount.Account Then
                                sConvertedAccount = Trim(oaccount.ConvertedAccount)
                                sDebetSwift = Trim(oaccount.SWIFTAddress)

                                bAccountFound = True
                                Exit For
                            End If
                        Next oaccount
                        If bAccountFound Then Exit For
                    Next oClient
                    If bAccountFound Then Exit For
                Next oFilesetup

                If Not bAccountFound Then
                    MsgBox("AccountNumber " & sI_Account & " is unknown to BabelBank." & vbCrLf & "Please add the accountnumber in BabelBank's Clientsetup'" & vbCrLf & vbCrLf & "BabelBank terminates!", MsgBoxStyle.Exclamation, "Account not found!")
                    Err.Raise(34234, "TreatThuleSalaryXML", "AccountNumber " & sI_Account & " is unknown to BabelBank." & vbCrLf & "Please add the accountnumber in BabelBank's Clientsetup'" & vbCrLf & vbCrLf & "BabelBank terminates!")
                End If

                ' found account in setup - replace accountno;
                ' -------------------------------------------
                NODTemp.InnerText = sConvertedAccount

                ' replace debit swift;
                ' -------------------
                NODTemp = XMLDoc.SelectSingleNode("//ISO:Document/ISO:CstmrCdtTrfInitn/ISO:PmtInf/ISO:DbtrAgt/ISO:FinInstnId/ISO:BIC", nsMgr)
                NODTemp.InnerText = sDebetSwift
            Next
            XMLDoc = Nothing
            TreatThuleSalaryXML = True
        Catch ex As Exception
            TreatThuleSalaryXML = False
        End Try

    End Function
    Public Function TreatThuleGermany(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Replace illegal characters for Thule Germany
        Dim oBabelFile As vbBabel.BabelFile
        Dim sIllegalCharactersReplaced As String
        Try

            For Each oBabelFile In oBabelFiles
                oBabelFile.GenericText = CheckForValidCharacters(oBabelFile.GenericText, True, True, False, True, ".-/+,", False, sIllegalCharactersReplaced, False)
                If Len(sIllegalCharactersReplaced) > 0 Then
                    sIllegalCharactersReplaced = sIllegalCharactersReplaced
                End If
            Next

            TreatThuleGermany = True

        Catch ex As Exception
            TreatThuleGermany = False
        End Try

    End Function
    ' XOKNET 10.05.2013 added TreatAndresenBil
    Public Function TreatAndresenBil(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Import "Styringsfil"
        ' Use data from styringsfil to replace e_account, and add to dDate_Payment
        ' --------------------------------------------------------------------------
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim iCounter As Integer
        Dim bFound As Boolean
        Dim sLine As String
        Dim aStyringsfilContent(,) As String

        ReDim aStyringsfilContent(3, 0)

        Try

            oFs = New Scripting.FileSystemObject  ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")
            If Not EmptyString(oProfile.Custom1Value) Then

                If Not oFs.FileExists(oProfile.Custom1Value) Then
                    Err.Raise(13457, "TreatAndresenData", "Kan ikke finne filen " & oProfile.Custom1Value & vbCrLf & "som benyttes for � finne riktige kontonumre og betalingsdatoer!")
                End If

                oFile = oFs.OpenTextFile(oProfile.Custom1Value, Scripting.IOMode.ForReading)

                iCounter = -1
                Do
                    If oFile.AtEndOfStream Then
                        Exit Do
                    End If

                    iCounter = iCounter + 1
                    ReDim Preserve aStyringsfilContent(3, iCounter)

                    sLine = oFile.ReadLine
                    aStyringsfilContent(0, iCounter) = Trim$(Left$(sLine, 8)) ' "internal" account
                    aStyringsfilContent(1, iCounter) = Replace(Replace(Trim$(Mid$(sLine, 11, 13)), ".", ""), " ", "") ' "real" bankaccount
                    aStyringsfilContent(2, iCounter) = Trim$(Mid$(sLine, 26, 2)) ' days to add
                    aStyringsfilContent(3, iCounter) = Trim$(Mid$(sLine, 31, 30)) ' receivers name

                Loop

                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            ' replace accountno based on "lookup"
                            bFound = False
                            For iCounter = 0 To UBound(aStyringsfilContent, 2)
                                If oPayment.E_Account = aStyringsfilContent(0, iCounter) Then
                                    ' yes, found correct one
                                    ' replace with real accountno
                                    oPayment.E_Account = aStyringsfilContent(1, iCounter)
                                    ' replace date
                                    oPayment.DATE_Payment = DateToString(DateAdd(DateInterval.Day, Val(aStyringsfilContent(2, iCounter)), StringToDate(oPayment.DATE_Payment)))
                                    bFound = True
                                    Exit For
                                End If
                            Next iCounter
                            If Not bFound Then
                                Err.Raise(13457, "TreatAndresenData", "Finner ikke " & vbCrLf & oPayment.E_Account & " " & oPayment.E_Name & vbCrLf & " i styringsfilen!")
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabelFile
            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally

            oFile.Close()
            oFile = Nothing
            oFs = Nothing

        End Try

        TreatAndresenBil = True

    End Function
    Public Function TreatKerstin(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' 13.08.2013
        ' Special handling for OCR for Kerstins Regnskapskontor
        ' La kun Fakturanr v�re igjen i KID-feltet, slik at hun f�r en enkel rapport � punche fra

        Dim bRetValue As Boolean

        Try
            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        For Each oInvoice In oPayment.Invoices
                            ' trekk ut fakturanr, og legg p� Fakt:
                            ' fakturanr er slik: XXfffffk - XX er fast, fakturanr 5 siffer, + kontrollsiffer
                            If Not EmptyString(oInvoice.Unique_Id) Then
                                oInvoice.Unique_Id = "Fakturanr. " & oInvoice.Unique_Id.Substring(2, 5)
                            End If
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception


            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try


        TreatKerstin = bRetValue

    End Function
    Public Function TreatOsloKommuneHelse(ByRef oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Les inn Telepay returfil i collections
        ' Les inn kontoinformasjonsfil som filstreng
        ' Bytt ut navnedata i kontoinformasjonsfil, basert p� Telepayreferanser funnet i Telepay returfilen
        ' Skriv endret kontoinformasjonsfil
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sFilenameIn As String
        Dim sPathIn As String
        Dim sFileIn As String
        Dim sOldFileContent As String
        Dim sNewFileContent As String
        Dim sLine As String
        Dim sTelepayRef As String
        Dim sRefOwn As String
        Dim sName As String
        Dim sSection As String
        Dim sTemp As String
        Dim bFound As Boolean
        Dim lPos As Long
        'Dim sSingleFile As String

        Try

            ' Find filenamein from Custom1Value
            sFilenameIn = Trim(oProfile.Custom1Value)
            oFs = New Scripting.FileSystemObject  '22.05.2017 CreateObject ("Scripting.FileSystemObject")
            sPathIn = Left(sFilenameIn, InStrRev(sFilenameIn, "\") - 1)
            sFileIn = Mid(sFilenameIn, InStrRev(sFilenameIn, "\") + 1)

            For Each sSingleFile As String In My.Computer.FileSystem.GetFiles(sPathIn, FileIO.SearchOption.SearchTopLevelOnly, sFileIn)  ', FileIO.SearchOption.SearchAllSubDirectories, sFileIn)

                oFile = oFs.OpenTextFile(sSingleFile, Scripting.IOMode.ForReading)

                sOldFileContent = oFile.ReadAll()
                ' traverse through filecontent, line by line (line a 80 chars, + CR/LF
                Do
                    If Len(sOldFileContent) < 80 Then
                        Exit Do
                    End If
                    If Len(sOldFileContent) = 80 Then
                        ' last line (AH100....)
                        sLine = sOldFileContent.Substring(0, 80)
                        sOldFileContent = ""
                        'sNewFileContent = sNewFileContent & sLine

                    Else

                        sLine = sOldFileContent.Substring(0, 82)
                        'sOldFileContent = sOldFileContent.Substring(82)
                    End If

                    If sLine.Substring(0, 8) = "940SWI02" Then
                        ' record including telepayref
                        '940SWI020095893542021                 25 13150100687                        28 0
                        '0000/0061 1206060606D  000000001936000                    //                7974
                        '10OVERF I382835               20120606                                          
                        ' Telepayref is 382835 pos 10 in 3rd line
                        ' We will use the 5 last digits from this one

                        'sNewFileContent = sNewFileContent & sLine

                        ' Extract a string from this SWI02 until next SWI02
                        sSection = ExtractSWI02Block(sOldFileContent)

                        ' read line 2 in 940SWI02
                        'sLine = sSection.Substring(0, 82)
                        'sOldFileContent = sOldFileContent.Substring(82)

                        'sNewFileContent = sNewFileContent & sLine
                        ' read line 3 in 940SWI02
                        'sLine = sOldFileContent.Substring(0, 82)
                        'sOldFileContent = sOldFileContent.Substring(82)
                        'sNewFileContent = sNewFileContent & sLine

                        ' find Telepayref in SWI02
                        sTelepayRef = sSection.Substring(174, 5)

                        ' try to find this ref in the Telepay Returnfile
                        sRefOwn = ""
                        bFound = False
                        For Each oBabelFile In oBabelFiles
                            For Each oBatch In oBabelFile.Batches
                                For Each oPayment In oBatch.Payments
                                    ' Vi sjekker de 5 siste i Telepayref/kontoref
                                    If oPayment.Payment_ID.Substring(1, 5) = sTelepayRef Then
                                        bFound = True
                                        ' Vi m� plukke egenref fra BETFOR23
                                        ' Hvis det er flere BETFOR23, m� vi splitte opp en kontorecord
                                        'sRefOwn = Trim(oPayment.REF_Own)
                                        For Each oInvoice In oPayment.Invoices
                                            '  replace with ownref from each oInvoice;
                                            ' Egenrefens 2 f�rste elementer skal med;
                                            ' Fra 2705/11-2013-328/27.08.2013/Her skal vi plukke ut 2705/11-2013-328
                                            'sRefOwn = xDelim(oInvoice.REF_Own, "/", 1) & "/" & xDelim(oInvoice.REF_Own, "/", 2)
                                            ' 18.10.2013 - take three elements!
                                            sRefOwn = xDelim(oInvoice.REF_Own, "/", 1) & "/" & xDelim(oInvoice.REF_Own, "/", 2) & "/" & xDelim(oInvoice.REF_Own, "/", 3)
                                            sTemp = sSection

                                            ' replace in SWI02-block (holding SWI02, 03, 05, 07);
                                            Do
                                                sLine = sTemp.Substring(0, 82)
                                                sTemp = sTemp.Substring(82)
                                                If sLine.Substring(0, 8) = "940SWI02" Then
                                                    ' Change amount in SWI02, in case we have several BETFOR23
                                                    ' Amount is in pos 104-118
                                                    ' Line 1
                                                    sNewFileContent = sNewFileContent & sLine
                                                    ' read line 2 in 940SWI02
                                                    sLine = sTemp.Substring(0, 82)
                                                    sTemp = sTemp.Substring(82)
                                                    ' Replace amount, pos 24-38 in line 1
                                                    sLine = sLine.Substring(0, 23) & PadLeft(LTrim(Str(oInvoice.MON_InvoiceAmount)), 15, "0") & sLine.Substring(38, 44)
                                                    sNewFileContent = sNewFileContent & sLine

                                                    ' 18.10.2013 - not sure if Telepayref is used or not for matching
                                                    ' First; Try to remove Telepayref in SWI02; pos 173,6
                                                    ' read line 3 in 940SWI02
                                                    sLine = sTemp.Substring(0, 82)
                                                    sTemp = sTemp.Substring(82)
                                                    ' Replace Telepayref with blank
                                                    sLine = sLine.Substring(0, 8) & Space(7) & sLine.Substring(15, 67)
                                                    sNewFileContent = sNewFileContent & sLine

                                                ElseIf sLine.Substring(0, 8) = "940SWI03" Then
                                                    ' in SWI03 we will find the name which will be replaced by internal ref
                                                    '940SWI030095893542021                 25 13150100687                        28 0
                                                    '0000/0086 1Pingu Pingu                     Lierstranda 99                       
                                                    sNewFileContent = sNewFileContent & sLine
                                                    ' read line 2 in 940SWI03
                                                    sLine = sTemp.Substring(0, 82)
                                                    sTemp = sTemp.Substring(82)

                                                    If sLine.Substring(10, 1) = "1" Then
                                                        ' namerecord, replace
                                                        If sRefOwn <> "" Then
                                                            sLine = sLine.Substring(0, 11) & PadRight(sRefOwn, 31, " ") & sLine.Substring(42, 40)
                                                            ' Anonymize the address as well
                                                            sLine = sLine.Substring(0, 43) & StrDup(30, "X") & sLine.Substring(73, 9)
                                                        End If
                                                    End If
                                                    sNewFileContent = sNewFileContent & sLine

                                                ElseIf sLine.Substring(0, 8) = "940SWI07" Then
                                                    ' in SWI07 we may find name as well, as a "freetext"
                                                    '940SWI070095893542021                 25 13150100687                        28 0
                                                    '0000/0086 TIL Hindi Hasan Adan                                                  
                                                    sNewFileContent = sNewFileContent & sLine
                                                    ' read line 2 in 940SWI07
                                                    sLine = sTemp.Substring(0, 82)
                                                    sTemp = sTemp.Substring(82)

                                                    'If sLine.Substring(10, 3) = "TIL" Then
                                                    ' this MAY be a namerecord, replace anyway
                                                    If sRefOwn <> "" Then
                                                        sLine = sLine.Substring(0, 10) & PadRight("M/" & sRefOwn, 70, " ") & vbCrLf
                                                        ' needs refown only once! thus, blank it
                                                        sRefOwn = ""
                                                    Else
                                                        sLine = sLine.Substring(0, 10) & Space(70) & vbCrLf
                                                    End If
                                                    'End If
                                                    sNewFileContent = sNewFileContent & sLine

                                                Else
                                                    sNewFileContent = sNewFileContent & sLine
                                                End If
                                                If Len(sTemp) < 82 Then
                                                    Exit Do
                                                End If
                                                'sTemp = sTemp.Substring(82)
                                            Loop
                                        Next oInvoice
                                    End If
                                Next oPayment
                                If sRefOwn <> "" Then
                                    Exit For
                                End If
                            Next oBatch
                            If sRefOwn <> "" Then
                                Exit For
                            End If
                        Next oBabelFile
                        If Not bFound Then
                            ' did not find match on paymentref -
                            ' must then add section without any changes
                            ' one exception;
                            ' For Utbetalingsanvisninger, the text "TELEBANK GIROUTB" will be followed by name
                            ' we can try to blank the name
                            'lPos = InStr(sSection, "TELEBANK GIROUTB")
                            'If lPos > 0 Then
                            ' write 40 blanks from "TELEBANK GIROUTB"
                            'sSection = sSection.Substring(0, lPos + 15) & Space(25) & sSection.Substring(lPos + 40, sSection.Length - lPos - 40)
                            'lPos = 0
                            'End If
                            sNewFileContent = sNewFileContent & sSection
                        End If
                        ' reduce string with this SWI02;
                        sOldFileContent = sOldFileContent.Substring(sSection.Length)
                    Else
                        ' did not find a match in Telepayfile -
                        ' must then just add to exportcontent for kontofile
                        sNewFileContent = sNewFileContent & sLine
                        sOldFileContent = sOldFileContent.Substring(82)
                    End If
                Loop
                If Not oFile Is Nothing Then
                    oFile.Close()
                    oFile = Nothing
                End If

                ' then create a new, edited, kontoinfo file;
                ' alter the extension to Anonym
                sSingleFile = Replace(sSingleFile, ".", ".Anonym")
                sSingleFile = Left(sSingleFile, InStr(sSingleFile, ".")) & "Anonym"
                ' Delete if exists
                If oFs.FileExists(sSingleFile) Then
                    oFs.DeleteFile(sSingleFile)
                End If
                oFile = oFs.OpenTextFile(sSingleFile, Scripting.IOMode.ForWriting, True)
                oFile.Write(sNewFileContent)
                oFile.Close()
                oFile = Nothing

            Next sSingleFile


        Catch ex As Exception


            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally


            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If
            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        TreatOsloKommuneHelse = True

    End Function
    Private Function ExtractSWI02Block(ByVal sFilestring As String) As String
        ' return a string starting with 940SWI02 until next 940SWI02
        If InStr(10, sFilestring, "940SWI02") > 0 Then
            ExtractSWI02Block = Left(sFilestring, InStr(10, sFilestring, "940SWI02") - 1)
        Else
            ' must be end of file;
            ExtractSWI02Block = sFilestring
        End If
    End Function
    Public Function TreatDSS_LonnSAP(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFileSetup_ID As Integer) As Boolean
        ' Check accountnumbers and report errors

        Dim oReport As vbBabel.Report
        Dim oBabelReport As vbBabel.BabelReport
        Dim bErrorMarked
        Dim j As Integer

        Try


            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        If Not IsModulus11(oPayment.E_Account) Then
                            bErrorMarked = True
                            oPayment.ToSpecialReport = True
                            oPayment.Invoices(1).ToSpecialReport = True
                        End If
                    Next
                Next
            Next
            If bErrorMarked Then
                ' activate report, call report 912, FilteredAndDeleted
                'UPGRADE_WARNING: Couldn't resolve default property of object oBabelFiles.VB_Profile.FileSetups. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                oFilesetup = oBabelFiles.VB_Profile.FileSetups(iFileSetup_ID)
                For j = 1 To oFilesetup.Reports.Count
                    If oFilesetup.Reports(j).ReportNo = 3 Then ' Specialreport

                        oBabelReport = New vbBabel.BabelReport
                        oBabelReport.Special = oBabelFiles.Item(1).Special
                        oBabelReport.BabelFiles = oBabelFiles
                        oBabelReport.ReportOnSelectedItems = True
                        oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.Inside_SpecialRoutines
                        If Not oBabelReport.RunReports(oFilesetup.FileSetup_ID) Then
                            j = 0
                        End If

                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

                        Exit For
                    End If
                Next j
            End If

        Catch ex As Exception


            TreatDSS_LonnSAP = False

        End Try

        TreatDSS_LonnSAP = True

    End Function
    Public Function TreatDedicareLonn(ByRef oBabelFiles As vbBabel.BabelFiles, ByVal iFileSetup_ID As Integer) As Boolean
        ' 18.12.2013
        ' Import Excelfile with Ans.nr and swedish accountno, and complete the imported info with bankaccount
        ' 14.04.2014: Added Denmark

        Dim aAccounts() As String
        Dim sAnsNr As String
        Dim sAccountNo As String
        Dim appExcel As Microsoft.Office.Interop.Excel.Application
        Dim oXLWBook As Microsoft.Office.Interop.Excel.Workbook
        Dim oExcelSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim lRowNo As Integer
        Dim lEmptyRows As Integer
        Dim i As Integer
        Dim bFound As Boolean
        Dim bMissingAccounts As Boolean
        Dim oReport As vbBabel.Report
        Dim oBabelReport As vbBabel.BabelReport
        Dim j As Integer

        Try
            bMissingAccounts = False


            ' filename in oBabelFiles.VB_Profile.Custom1Value
            If Dir(oBabelFiles.VB_Profile.Custom1Value) <> "" Then

                appExcel = New Microsoft.Office.Interop.Excel.Application
                ' Disable all alerts
                appExcel.DisplayAlerts = False
                oXLWBook = appExcel.Workbooks.Open(oBabelFiles.VB_Profile.Custom1Value, , True)
                oExcelSheet = oXLWBook.Worksheets(1) ' er i ark 1
                lRowNo = 1 ' one Headingline
                lEmptyRows = 0
                Do
                    lRowNo = lRowNo + 1
                    '       A           B          C        D
                    'Ansattnummer	Etternavn	Fornavn	Kontonummer
                    '51301	        Abelsson	Katrin	83 140 032 553 950
                    sAnsNr = Trim(oExcelSheet.Cells._Default(lRowNo, 1).Value)
                    sAccountNo = Trim(Replace(oExcelSheet.Cells._Default(lRowNo, 4).Value, " ", ""))
                    sAccountNo = Replace(Replace(sAccountNo, ".", ""), "-", "")

                    If Len(sAnsNr) > 0 Then
                        'add to array
                        If Array_IsEmpty(aAccounts) Then
                            ReDim aAccounts(0)
                        Else
                            ReDim Preserve aAccounts(UBound(aAccounts) + 1)
                        End If
                        aAccounts(UBound(aAccounts)) = sAnsNr & ";" & sAccountNo
                    Else
                        If oExcelSheet.Cells._Default(lRowNo, 1).Value = "" Then
                            lEmptyRows = lEmptyRows + 1
                        End If
                    End If
                    ' tell opp tomme linjer, og hopp ut
                    If lEmptyRows > 20 Then
                        Exit Do
                    End If
                Loop
                ' array filled!
                ' Close Excelfile
                oXLWBook.Close()
                oXLWBook = Nothing

            End If

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        bFound = False

                        For i = 0 To UBound(aAccounts)
                            ' Ans.nr from Kontantliste in oPayment.Ref_own
                            If oPayment.REF_Own = xDelim(aAccounts(i), ";", 1) Then
                                oPayment.E_Account = xDelim(aAccounts(i), ";", 2)
                                bFound = True
                                Exit For
                            End If
                        Next i
                        If Not bFound Then
                            oPayment.Invoices(1).Unique_Id = "Konto ikke funnet"
                            oPayment.ToSpecialReport = True
                            bMissingAccounts = True
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile



            If bMissingAccounts Then
                ' present report of missing accounts, and stop

                'oReport = CreateObject ("vbbabel.Report")
                'oBabelReport = CreateObject ("vbbabel.BabelReport")
                'oReport.ReportNo = "003"
                'oReport.Heading = "OCR bunter"
                'oReport.Preview = False
                'oReport.Printer = False

                '' to file; if in use set folder/filename in Custom1Value
                'If Not EmptyString(oProfile.Custom1Value) Then
                '    sSpecialReportPath = oProfile.Custom1Value
                '    oReport.ExportType = "PDF"
                '    oReport.ExportFilename = ReplaceDateTimeStamp(sSpecialReportPath)
                'End If

                'oReport.DetailLevel = 1

                oFilesetup = oBabelFiles.VB_Profile.FileSetups(iFileSetup_ID)
                For j = 1 To oFilesetup.Reports.Count
                    If oFilesetup.Reports(j).ReportNo = 3 Then ' Specialreport

                        oBabelReport = New vbBabel.BabelReport
                        oBabelReport.Special = oBabelFiles.Item(1).Special
                        oBabelReport.BabelFiles = oBabelFiles
                        oBabelReport.ReportOnSelectedItems = True
                        oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.Inside_SpecialRoutines
                        If Not oBabelReport.RunReports(oFilesetup.FileSetup_ID) Then
                            j = 0
                        End If

                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

                        Exit For
                    End If
                Next j

                oReport = Nothing
                oBabelReport = Nothing

                MsgBox("Det finnes mottakere som mangler konto. Se rapport for disse." & vbCrLf & "BabelBank avbrytes", MsgBoxStyle.Critical)
                End
            End If

        Catch ex As Exception


            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatDedicareLonn = True

    End Function
    Public Function TreatGassco_Return(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFileSetup_ID As Integer) As Boolean
        ' Redo accountno for returnfiles

        Dim oReport As vbBabel.Report
        Dim oBabelReport As vbBabel.BabelReport
        Dim bErrorMarked
        Dim j As Integer

        On Error GoTo TreatGassco_ReturnErr


        For Each oBabelFile In oBabelFiles
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    If Len(Trim(oPayment.I_Account)) = 8 Then
                        ' add the blanks at the end of accountno, to make it easy for SAP to detect correct accountno
                        oPayment.I_Account = Trim(oPayment.I_Account) & "   "
                        ' To have BETOR01/02/03/04 and TPRI
                        oPayment.BANK_I_Domestic = False
                        oPayment.PayType = "I"
                    End If
                Next
            Next
        Next


        TreatGassco_Return = True
        Exit Function

TreatGassco_ReturnErr:
        TreatGassco_Return = False

    End Function
    Public Function TreatCredicare_OCR(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        '26.03.2014
        ' Import OCR
        ' Save AccountNo and SaksNr to table ExternalBankAccounts in BabelBanks database
        Dim bRetValue As Boolean
        Dim sSaksNummer As String
        Dim sBankkonto As String
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim bFound As Boolean = False
        Dim aAccountsToOmitFromMatching(,) As String
        Dim lCounter As Long
        Dim bSaveThisOne As Boolean

        Try

            '05.08.2010 - Added functionality to omit accounts from automatic matching
            aAccountsToOmitFromMatching = RestoreOmitAccounts("1", True)

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        bSaveThisOne = True
                        If Not aAccountsToOmitFromMatching Is Nothing Then
                            ' do not handle accounts set under "Avstemming", "Oppsett avstemming", "Utelat konti"
                            For lCounter = 0 To UBound(aAccountsToOmitFromMatching, 2)
                                If aAccountsToOmitFromMatching(2, lCounter) = "True" Then
                                    If aAccountsToOmitFromMatching(0, lCounter) = oPayment.E_Account Then
                                        bSaveThisOne = False  ' do not update this one
                                        Exit For
                                    End If
                                End If
                            Next lCounter
                        End If

                        ' do not save empty accounts or accounts with 9 in 5th pos (hjelpekonti)
                        If EmptyString(oPayment.E_Account) Or oPayment.E_Account = "00000000000" Or oPayment.E_Account.Substring(4, 1) = "9" Then
                            bSaveThisOne = False  ' do not update this one
                        End If

                        If bSaveThisOne Then
                            For Each oInvoice In oPayment.Invoices
                                ' trekk ut saksnr
                                ' fakturanr er slik: 010000050203098 XXXXXXXsssssssk - X er fast, saksnr 7 siffer, + kontrollsiffer
                                'If Not EmptyString(oInvoice.Unique_Id) Then
                                ' changed If 30.05.2014
                                If Len(oInvoice.Unique_Id) > 13 Then
                                    sSaksNummer = oInvoice.Unique_Id.Substring(7, 7) ' Find from pos 8 in KID
                                    sBankkonto = oPayment.E_Account
                                    ' Lagre til databaase
                                    ' Hvis finnes fra f�r, Update, eller Insert
                                    ' Finnes saksnr fra f�r?
                                    sMySQL = "SELECT * FROM ExternalBankAccounts WHERE Saksnummer = '" & sSaksNummer & "'"
                                    oMyDal.SQL = sMySQL
                                    bFound = False

                                    If oMyDal.Reader_Execute() Then

                                        Do While oMyDal.Reader_ReadRecord
                                            ' We found an instance of Saksnummer in the table, UPDATE
                                            bFound = True
                                        Loop

                                        If bFound Then
                                            sMySQL = "UPDATE ExternalBankAccounts SET Kontonummer = '" & sBankkonto & "' WHERE Saksnummer = '" & sSaksNummer & "'"
                                        Else
                                            sMySQL = "INSERT INTO ExternalBankAccounts(Saksnummer, Kontonummer) VALUES ('" & sSaksNummer & "', '" & sBankkonto & "')"
                                        End If

                                        oMyDal.SQL = sMySQL
                                        oMyDal.ExecuteNonQuery()
                                    Else
                                        bRetValue = False
                                        Throw New Exception("Function: TreatCredicare_OCR" & vbCrLf & "Problemer med oppdatering av kontonummer.")
                                    End If
                                End If
                            Next oInvoice
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

        End Try

        TreatCredicare_OCR = bRetValue

    End Function
    Public Function TreatCredicare_Telepay(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        '26.03.2014
        ' Import Telepay
        ' Redo from Utbetalingsanvisning til med kontonummer, basert p� oppslag med Saksnummer i tabellen ExternalBankAccounts
        Dim bRetValue As Boolean
        Dim sSaksNummer As String
        Dim sBankkonto As String
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sTxt As String

        Try

            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        For Each oInvoice In oPayment.Invoices
                            ' trekk ut saksnr
                            ' kun for poster med fritekst "For mye betalt p� sak nr 9988558"
                            If oInvoice.Freetexts.Count > 0 Then
                                If Not EmptyString(oInvoice.Freetexts(1).Text) Then
                                    sTxt = oInvoice.Freetexts(1).Text
                                    If sTxt.Contains("For mye betalt p� sak nr") Then
                                        sSaksNummer = Trim(sTxt.Replace("For mye betalt p� sak nr", ""))
                                        If sSaksNummer.Length >= 7 Then
                                            sSaksNummer = sSaksNummer.Substring(0, 7)

                                            If sSaksNummer.Length = 7 And IsNumeric(sSaksNummer) Then
                                                ' Finn i databaase
                                                sMySQL = "SELECT Kontonummer FROM ExternalBankAccounts WHERE Saksnummer = '" & sSaksNummer & "'"
                                                oMyDal.SQL = sMySQL

                                                If oMyDal.Reader_Execute() Then

                                                    Do While oMyDal.Reader_ReadRecord
                                                        ' We found an instance of Saksnummer in the table
                                                        sBankkonto = oMyDal.Reader_GetString("Kontonummer")
                                                        ' add accountno to "Telepay"
                                                        oPayment.E_Account = sBankkonto
                                                    Loop

                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

        End Try

        TreatCredicare_Telepay = bRetValue

    End Function
    'Public Function TreatGjensidigeUtbytte(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
    '    ' Have to sort payments to avoid a mix of BETFOR22 and BETFOR23
    '    For Each oBabelFile In oBabelFiles
    '        oBabelFile.SortCriteria1 = "PAYCODE" ' KID-payments has K in prefix
    '        oBabelFile.SortPayment()
    '    Next oBabelFile
    '    TreatGjensidigeUtbytte = True
    'End Function
    ' XNET 31.10.2012 Added next function
    Public Function TreatFalckCollection(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Replace creationdate for Falck Coldebitorfiles
        ' Find date in Excel

        ' 07.10.2008 - to open excelfile with countrycodes
        Dim aConvertTable(,) As String
        Dim appExcel As Microsoft.Office.Interop.Excel.Application
        Dim oXLWBook As Microsoft.Office.Interop.Excel.Workbook
        Dim oExcelSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim lRowNo As Long
        Dim lEmptyRows As Long
        Dim i As Long
        Dim sFilename As String
        Dim sCustomerNo As String
        Dim sStatus As String
        Dim sTmp As String

        Try
            sFilename = oBabelFiles.VB_Profile.Custom1Value

            If oBabelFiles(1).StatusCode <> "00" Then
                If Dir(sFilename) <> vbNullString Then
                    sStatus = "Klar for � �pne Excel avtalefil"
                    appExcel = New Microsoft.Office.Interop.Excel.Application
                    ' Disable all alerts
                    appExcel.DisplayAlerts = False
                    oXLWBook = appExcel.Workbooks.Open(getlongpath(sFilename), , True)
                    oExcelSheet = oXLWBook.Worksheets(1)  ' er i ark 1
                    sStatus = "Etter �pning av Excel avtalefil"
                    lRowNo = 1 ' Headingline
                    lEmptyRows = 0
                    Do
                        lRowNo = lRowNo + 1
                        ' Customer No. fra kolonne A (1)
                        ' Coll.Method fra kolonne D (4)
                        ' Activation date fra kolonne F (6)
                        ' Status fra kolonne I (9)

                        'add to array
                        If Array_IsEmpty(aConvertTable) Then
                            ReDim aConvertTable(3, 0)
                        Else
                            ReDim Preserve aConvertTable(3, UBound(aConvertTable, 2) + 1)
                        End If

                        If oExcelSheet.Cells(lRowNo, 1).Value <> vbNullString Then
                            aConvertTable(0, UBound(aConvertTable, 2)) = oExcelSheet.Cells(lRowNo, 1).Value ' Customerno
                            aConvertTable(1, UBound(aConvertTable, 2)) = oExcelSheet.Cells(lRowNo, 4).Value ' Coll.method
                            aConvertTable(2, UBound(aConvertTable, 2)) = oExcelSheet.Cells(lRowNo, 6).Value ' Activation date
                            aConvertTable(3, UBound(aConvertTable, 2)) = oExcelSheet.Cells(lRowNo, 9).Value ' status
                        Else
                            lEmptyRows = lEmptyRows + 1
                        End If
                        ' tell opp tomme linjer, og hopp ut
                        If lEmptyRows > 20 Then
                            Exit Do
                        End If
                    Loop

                    sStatus = "Etter gjennomgang, f�r lukking av  Excel avtalefil"
                    oExcelSheet = Nothing
                    oXLWBook.Close()
                    oXLWBook = Nothing
                    sStatus = "Etter lukking av Excel avtalefil"


                    For Each oBabelFile In oBabelFiles
                        For Each oBatch In oBabelFile.Batches
                            For Each oPayment In oBatch.Payments

                                ' Forutsett 1 invoice
                                sCustomerNo = oPayment.Invoices(1).CustomerNo

                                ' se om vi finner kundenr i arrayen
                                For i = 0 To UBound(aConvertTable, 2)
                                    ' kan v�re flere oppf�ringer pr kunde. m� finne den som har coll.method 11000100, og skal st� OK i Status
                                    If aConvertTable(0, i) = sCustomerNo And aConvertTable(1, i) = "11000100" And aConvertTable(3, i) = "OK" Then
                                        ' vi er p� riktig plass. hent ut dato
                                        sTmp = aConvertTable(2, i)  'dd.mm.yyyy
                                        oPayment.DATE_Payment = Right$(sTmp, 4) & Mid$(sTmp, 4, 2) & Mid$(sTmp, 1, 2)
                                        oPayment.DATE_Value = oPayment.DATE_Payment
                                        Exit For
                                    End If
                                Next i

                            Next oPayment
                        Next oBatch
                    Next oBabelFile

                Else
                    ' Finds no Excelfile
                    MsgBox("AVBRYTER" & vbCrLf & "Finner ikke filen " & sFilename)
                    TreatFalckCollection = False
                    Exit Function
                End If
            End If

        Catch ex As Exception

            TreatFalckCollection = False

            'If Not oBabelFile Is Nothing Then
            '    Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            'Else
            '    Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            'End If

        End Try

        TreatFalckCollection = True

    End Function
    ' XNET 14.12.2012 - added function for F&H (part of Jernia)
    Public Function Treat_FandH(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        '
        ' Fill in Statebank code

        Dim bRetValue As Boolean
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sI_CountryCodeDebitBank As String
        Dim sText As String

        Try
            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    sOldAccountNo = "zzzzzzzzzzzzzzzzzzzz"
                    bAccountFound = False
                    For Each oPayment In oBatch.Payments
                        If oPayment.I_Account <> sOldAccountNo Then
                            sOldAccountNo = oPayment.I_Account
                            For Each oFilesetup In oProfile.FileSetups
                                For Each oClient In oFilesetup.Clients
                                    For Each oaccount In oClient.Accounts
                                        If oaccount.Account = oPayment.I_Account Then
                                            bAccountFound = True
                                            ' Find countrycode debitaccount
                                            If Not EmptyString(oaccount.DebitAccountCountryCode) Then
                                                sI_CountryCodeDebitBank = oaccount.DebitAccountCountryCode
                                                oPayment.BANK_AccountCountryCode = sI_CountryCodeDebitBank
                                            Else
                                                sI_CountryCodeDebitBank = Mid$(oaccount.SWIFTAddress, 5, 2)
                                            End If
                                            Exit For
                                        End If
                                    Next oaccount
                                    If bAccountFound Then
                                        Exit For
                                    End If
                                Next oClient
                                If bAccountFound Then
                                    Exit For
                                End If
                            Next oFilesetup
                            If Not bAccountFound Then
                                ' did not find account in setup!
                                Err.Raise(10001, "TreatF&H", "Finner ikke konto " & oPayment.I_Account & " i klientoppsettet." & vbCrLf & vbCrLf & "Kontakt Visual Banking!")
                                bRetValue = False
                            End If
                        End If
                        sText = ""
                        For Each oInvoice In oPayment.Invoices
                            If oPayment.PayType = "I" Then
                                If sI_CountryCodeDebitBank = "NO" Then
                                    If EmptyString(oInvoice.STATEBANK_Code) Then
                                        oInvoice.STATEBANK_Code = "14"
                                    End If
                                    If EmptyString(oInvoice.STATEBANK_Text) Then
                                        oInvoice.STATEBANK_Text = "Salg/kj�p varer"
                                    End If
                                    ' We also need a name for receiver in Telepay
                                    If EmptyString(oPayment.E_Name) Then
                                        oPayment.E_Name = "NN"
                                    End If
                                Else
                                    If EmptyString(oInvoice.STATEBANK_Code) Then
                                        oInvoice.STATEBANK_Code = "101"
                                    End If
                                End If

                                ' 12.04.2013 - we need to put as much freetext as possible into one freetext, to be able to fill the one freetextline in Telepay Int.
                                For Each oFreeText In oInvoice.Freetexts
                                    sText = sText & " " & oFreeText.Text
                                    ' remove freetext
                                    oFreeText.Text = ""
                                Next oFreeText
                                ' XokNET  30.03.2015 - added next If/End If
                                If oInvoice.Freetexts.Count > 0 Then
                                    ' then, add all freetext into first freetext
                                    oInvoice.Freetexts.Item(1).Text = Trim(sText)
                                End If

                            Else
                                ' domestic
                                If sI_CountryCodeDebitBank = "NO" Then
                                    ' We need a name for receiver in Telepay
                                    If EmptyString(oPayment.E_Name) Then
                                        oPayment.E_Name = "NN"
                                    End If
                                End If

                            End If
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception


            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_FandH = bRetValue

    End Function
    ' XNET 26.02.2013 - added function  Jernia
    Public Function Treat_Jernia(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        '
        ' Redo from unstructured to structured
        ' Remove / in front of account
        ' Fill in Statebank code if blank or 01, fill in 14, and statebank text

        Dim bRetValue As Boolean
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sI_CountryCodeDebitBank As String
        Dim i As Integer
        Dim sTmp As String

        Try

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        ' if receivers accountno starts with /, remove
                        If Left$(oPayment.E_Account, 1) = "/" Then
                            oPayment.E_Account = Mid$(oPayment.E_Account, 2)
                        End If

                        For Each oInvoice In oPayment.Invoices
                            If oPayment.PayType = "I" Then
                                ' Statebankcode: if blank or 01, replace with 14
                                If EmptyString(oInvoice.STATEBANK_Code) Or oInvoice.STATEBANK_Code = "01" Then
                                    oInvoice.STATEBANK_Code = "14"
                                    oInvoice.STATEBANK_Text = "Salg/kj�p varer"
                                End If
                                If EmptyString(oInvoice.STATEBANK_Text) Then
                                    oInvoice.STATEBANK_Text = "Salg/kj�p varer"
                                End If

                            Else
                                ' domestic
                                ' If structured (invoiceno or kid present), remove freetext
                                If Trim(oInvoice.InvoiceNo) = "" And Trim(oInvoice.Unique_Id) = "" And Trim(oInvoice.CustomerNo) = "" Then
                                    ' create structured based on freetext
                                    sTmp = ""
                                    For Each oFreeText In oInvoice.Freetexts
                                        sTmp = sTmp & oFreeText.Text & " "
                                    Next oFreeText
                                    ' move to invoiceno
                                    oInvoice.InvoiceNo = Left$(sTmp, 20) ' max 20 chars
                                End If
                                ' remove freetext, both for structured and unstructured
                                If oInvoice.Freetexts.Count > 0 Then
                                    For i = oInvoice.Freetexts.Count To 1 Step -1
                                        oInvoice.Freetexts.Remove(i)
                                    Next i
                                End If
                            End If
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Return bRetValue

    End Function
    ' XNET 26.02.2013 - added function  TreatDolphin
    Public Function Treat_Dolphin(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        '
        ' Redo from charges, to Dolphin pay all charges
        Dim bRetValue As Boolean

        Try

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        'oPayment.MON_ChargeMeAbroad = True
                        'oPayment.MON_ChargeMeDomestic = True

                        oPayment.MON_ChargeMeAbroad = False
                        oPayment.MON_ChargeMeDomestic = True

                        '21.12.2015 - Laget spesielt for at Dolphin midlertidig skal kunne betale fra DNB London
                        ' 16.12.2016 - Fjernet testen under for DNB London - alt betales n� fra Singapore
                        'If oPayment.BANK_CountryCode = "GB" Then
                        '    ' redo to bacs
                        '    'oPayment.BANK_SWIFTCode = ""
                        '    oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode
                        '    oPayment.DnBNORTBIPayType = "SAL"
                        '    oPayment.PayType = "S"
                        '    ' Redo from IBAN to BBAN
                        '    If Len(oPayment.E_Account) > 8 And Left(oPayment.E_Account, 2) = "GB" Then
                        '        oPayment.E_Account = Right(oPayment.E_Account, 8)
                        '    End If
                        'End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception


            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Dolphin = bRetValue

    End Function
    
    ' XNET 26.02.2013 - added function  Jernia
    Public Function Treat_AonReturn(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        '
        ' AON: Remove Mottaksretur (Status = "01")
        Dim bRetValue As Boolean
        Dim i As Long
        Dim j As Long

        Try

            bRetValue = True

            For j = oBabelFiles.Count To 1 Step -1
                oBabelFile = oBabelFiles(j)
                For i = oBabelFile.Batches.Count To 1 Step -1
                    If oBabelFile.Batches(i).Payments.Count > 0 Then
                        If oBabelFile.Batches(i).Payments(1).StatusCode = "01" Then
                            ' this batch is "mottak", remove;
                            oBabelFile.Batches.Remove(i)
                        End If
                    End If
                Next i
                ' remove empty BabelFiles
                If oBabelFile.Batches.Count = 0 Then
                    oBabelFiles.Remove(j)
                End If
            Next j


        Catch ex As Exception


            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_AonReturn = bRetValue

    End Function
    Public Function Treat_Krifon(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        '
        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' Check if FIK71,73,75 or Girokort 04 or 15
                        ' Redo to correct data FIK-data
                        ' E_Account holds both FIK-/Giro-type and kreditornumber
                        ' 71*86571218
                        sTmp = oPayment.E_Account.Substring(0, 3)
                        If sTmp = "71*" Or sTmp = "73*" Or sTmp = "75*" Or sTmp = "04*" Or sTmp = "15*" Then
                            ' FIK or Girokort, redo accountno
                            oPayment.E_Account = oPayment.E_Account.Substring(3)
                            oPayment.PayCode = "301"
                            ' Then redo all invoices;
                            ' "KID"-field holds BetalingsID, we need to add FIK-/Giro-type
                            For Each oInvoice In oPayment.Invoices
                                'oInvoice.Unique_Id = sTmp.Substring(0, 2) & oInvoice.Freetexts(1).Text.Trim
                                oInvoice.Unique_Id = Left(sTmp, 2) & Trim(oInvoice.Freetexts(1).Text)
                            Next

                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile


        Catch ex As Exception


            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Krifon = bRetValue


    End Function

    Public Function Treat_Torm(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Redo danish payments from Int to Dom
        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.BANK_CountryCode = "DK" Then
                            oPayment.PayType = "D"
                            ' no extended message to receiver
                            oPayment.PayCode = "250"
                            ' Put receivers name as ownref
                            oPayment.REF_Own = Left(oPayment.E_Name, 30)
                            If oPayment.Invoices.Count > 0 Then
                                oPayment.Invoices(1).REF_Own = Left(oPayment.E_Name, 30)
                            End If
                            ' Text on receivers statement;
                            'oPayment.Text_E_Statement = "WAGES " & Year(Now).ToString & Right("0" & Month(Now).ToString, 2) ' + period, but need to find that one somewhere
                            ' Pick text to receiver from filename, without suffix
                            sTmp = oBabelFile.FilenameIn
                            sTmp = sTmp.Substring(InStrRev(sTmp, "\"))  ' Filpart with suffix
                            ' 01.04.2015 added If test, in case of no extenstion
                            If InStr(sTmp, ".") > 0 Then
                                sTmp = sTmp.Substring(0, InStr(sTmp, ".") - 1).ToUpper
                            End If

                            oPayment.Text_E_Statement = sTmp

                            ' accountno is a combination of branchno and e_account from imported file
                            oPayment.E_Account = oPayment.BANK_BranchNo & oPayment.E_Account
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Torm = bRetValue

    End Function
    
    Public Function TreatAerogulf_UpdateSendStatus(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        '09.01.2015
        ' After Telepayfiles are produced, update status to �Processing for Payment� in Ramcobase

        ' 23.04.2015 Changes in Ramcosystem/update
        ' Statuscolumn is now File_Process_status
        ' Voucher_No is unique pr payment

        ' Status Code	Status Description	Remarks
        ' -----------------------------------------
        ' P	    Pending	While inserting the data in the table, the default value is "P" by Ramco. Third party software should get these records for processing.
        ' PR	Processing	If the record got for processing, then Third-party software, update the value for the column as "PR"
        ' OH	On Hold	After processing the record, Third-Party software update the column as "OH" if it is "On Hold". 
        ' RJ	Rejected	After processing the record, Third-Party software update the column as "RJ" if it is "Rejected". 
        ' PD	Paid	After processing the record, Third-Party software update the column as "PD" if it is "Paid". 

        Dim bRetValue As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sMyOrigSQL As String = ""
        Dim bFound As Boolean = False
        Dim sRefOwn As String
        Dim aQueryArray(,,) As String
        Dim lCounter As Integer
        Dim sString As String
        Dim sProvider As String
        Dim sUID As String
        Dim sPWD As String
        Dim sRefOwnPartOne, sRefOwnPartTwo As String

        Try

            aQueryArray = GetERPDBInfo(oBabelFiles.VB_Profile.Company_ID, 1)

            For lCounter = 0 To UBound(aQueryArray, 2)
                ' find correct Update
                If aQueryArray(0, 4, lCounter).ToUpper = "AEROGULF_UPDATESENDSTATUS" Then
                    sMyOrigSQL = aQueryArray(0, 0, lCounter).ToUpper
                    Exit For
                End If
            Next lCounter


            If EmptyString(sMyOrigSQL) Then
                Err.Raise(45001, "TreatAerogulf_UpdateSendStatus", LRSCommon(45001))
                'As above
            End If

            oMyDal = New vbBabel.DAL
            sProvider = aQueryArray(0, 0, 0) ' Holds p�loggingsstreng, set in Profile
            sUID = aQueryArray(0, 1, 0)
            sPWD = aQueryArray(0, 2, 0)
            ' Change UID/PWD:
            ' Fill in UserID and Password
            ' 28.08.2014 do not ucase ! Removed in next 2 lines
            sProvider = Replace(sProvider, "=UID", "=" & sUID)
            sProvider = Replace(sProvider, "=PWD", "=" & sPWD)


            oMyDal = New vbBabel.DAL
            oMyDal.Provider = aQueryArray(0, 9, 0)
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
            oMyDal.Connectionstring = sProvider
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' 06.02.2015 go to invoicelevel
                        For Each oInvoice In oPayment.Invoices

                            sRefOwn = oInvoice.REF_Own.Trim
                            ' 06.02.2015 - new changes for Aerogulf - have first part of refown for VoucherNo "APPV-000014-2015", BUT
                            '              due to spacelimits (30 chars in BETFOR23), we have removed APPV- or APPPV-
                            '              If APPPV, then we have P as start of RefOwn

                            ' added 01.09.2015 to have no errors if empty ref_own
                            If Len(sRefOwn) > 0 Then
                                If sRefOwn.Substring(0, 1) = "P" Then
                                    sRefOwnPartOne = "APPPV-" & xDelim(sRefOwn, ";", 1).Substring(1)
                                Else
                                    sRefOwnPartOne = "APPV-" & xDelim(sRefOwn, ";", 1)
                                End If
                                sRefOwnPartTwo = xDelim(sRefOwn, ";", 2)
                                sMySQL = sMyOrigSQL.Replace("BB_REFOWN", sRefOwnPartOne)

                                oMyDal.SQL = sMySQL
                                oMyDal.ExecuteNonQuery()
                            End If
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

        End Try

        TreatAerogulf_UpdateSendStatus = bRetValue

    End Function
    Public Function TreatAerogulf_UpdateReturnStatus(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        '09.01.2015
        ' After Telepayfiles are produced, update status to �Processing for Payment� in Ramcobase

        ' 23.04.2015 Changes in Ramcosystem/update
        ' Statuscolumn is now File_Process_status
        ' Voucher_No is unique pr payment

        ' Status Code	Status Description	Remarks
        ' -----------------------------------------
        ' P	    Pending	While inserting the data in the table, the default value is "P" by Ramco. Third party software should get these records for processing.
        ' PR	Processing	If the record got for processing, then Third-party software, update the value for the column as "PR"
        ' OH	On Hold	After processing the record, Third-Party software update the column as "OH" if it is "On Hold". 
        ' RJ	Rejected	After processing the record, Third-Party software update the column as "RJ" if it is "Rejected". 
        ' PD	Paid	After processing the record, Third-Party software update the column as "PD" if it is "Paid". 

        Dim bRetValue As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMyConfirmSQL As String = ""
        Dim sMyRejectSQL As String = ""
        Dim sMyHoldSQL As String = ""
        Dim sMySQL As String
        Dim bFound As Boolean = False
        Dim sRefOwn As String
        Dim aQueryArray(,,) As String
        Dim lCounter As Integer
        Dim sString As String
        Dim sProvider As String
        Dim sUID As String
        Dim sPWD As String
        Dim sRefOwnPartOne, sRefOwnPartTwo As String
        Dim sDateValue As String = ""

        ' 16.08.2021 - Dim moved here
        Dim nMON_AccountExchangerate As Double = 0
        Dim nMON_accountAmount As Double = 0
        Dim nMON_accountAmountPrInvoice As Double = 0
        Dim nMON_TotalCalculatedAccountAmount As Double = 0

        Try
            '---test---

            aQueryArray = GetERPDBInfo(oBabelFiles.VB_Profile.Company_ID, 1)

            For lCounter = 0 To UBound(aQueryArray, 3)
                ' find correct Update
                If aQueryArray(0, 4, lCounter).ToUpper = "AEROGULF_UPDATECONFIRMSTATUS" Then
                    sMyConfirmSQL = aQueryArray(0, 0, lCounter)  ' .ToUpper
                End If
                If aQueryArray(0, 4, lCounter).ToUpper = "AEROGULF_UPDATEREJECTSTATUS" Then
                    sMyRejectSQL = aQueryArray(0, 0, lCounter).ToUpper
                End If
                ' New SQL for updating to Hold
                If aQueryArray(0, 4, lCounter).ToUpper = "AEROGULF_UPDATEHOLDSTATUS" Then
                    sMyHoldSQL = aQueryArray(0, 0, lCounter).ToUpper
                End If
            Next lCounter


            If EmptyString(sMyConfirmSQL) Then
                Err.Raise(45001, "TreatAerogulf_UpdateReturnStatus", LRSCommon(45001))
                'As above
            End If

            oMyDal = New vbBabel.DAL
            sProvider = aQueryArray(0, 0, 0) ' Holds p�loggingsstreng, set in Profile
            sUID = aQueryArray(0, 1, 0)
            sPWD = aQueryArray(0, 2, 0)
            ' Change UID/PWD:
            ' Fill in UserID and Password
            ' 28.08.2014 do not ucase ! Removed in next 2 lines
            sProvider = Replace(sProvider, "=UID", "=" & sUID)
            sProvider = Replace(sProvider, "=PWD", "=" & sPWD)


            oMyDal = New vbBabel.DAL
            oMyDal.Provider = aQueryArray(0, 9, 0)
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
            oMyDal.Connectionstring = sProvider
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' 16.08.2021 added next
                        nMON_TotalCalculatedAccountAmount = 0
                        '06.02.2015 changed to Invoicelevel
                        For Each oInvoice In oPayment.Invoices
                            sRefOwn = oInvoice.REF_Own.Trim
                            ' 06.02.2015 - new changes for Aerogulf - have first part of refown for VoucherNo "APPV-000014-2015", BUT
                            '              due to spacelimits (30 chars in BETFOR23), we have removed APPV- or APPPV-
                            '              If APPPV, then we have P as start of RefOwn
                            If sRefOwn.Substring(0, 1) = "P" Then
                                sRefOwnPartOne = "APPPV-" & xDelim(sRefOwn, ";", 1).Substring(1)
                            Else
                                sRefOwnPartOne = "APPV-" & xDelim(sRefOwn, ";", 1)
                            End If
                            sRefOwnPartTwo = xDelim(sRefOwn, ";", 2)


                            ' 08.01.2015 -
                            ' if more than one error, then prioritize error other than error in KID
                            'If Val(oInvoice.StatusCode) = 17 Or Val(oPayment.StatusCode) = 17 Then
                            If Val(oPayment.StatusCode) <> 17 And Val(oPayment.StatusCode) > 2 Then
                                sMySQL = sMyRejectSQL
                                sMySQL = sMySQL.Replace("BB_REFOWN", sRefOwnPartOne)

                            ElseIf Val(oInvoice.StatusCode) = 17 Or Val(oPayment.StatusCode) = 17 Then

                                ' Error in KID - Update the actual payment only; VoucherNo and Credit Document
                                sMySQL = sMyHoldSQL
                                '23.04.2015 Same behaviour as for other rejects
                                'sMySQL = sMySQL.Replace("BB_REFOWN_PART1", sRefOwnPartOne)
                                'sMySQL = sMySQL.Replace("BB_REFOWN_PART2", sRefOwnPartTwo)
                                sMySQL = sMySQL.Replace("BB_REFOWN", sRefOwnPartOne)
                            ElseIf Val(oInvoice.StatusCode) > 2 Or Val(oPayment.StatusCode) > 2 Then
                                sMySQL = sMyRejectSQL
                                sMySQL = sMySQL.Replace("BB_REFOWN", sRefOwnPartOne)
                            ElseIf oInvoice.StatusCode = "02" Then
                                sMySQL = sMyConfirmSQL
                                sMySQL = sMySQL.Replace("BB_REFOWN", sRefOwnPartOne)
                                ' 07.04.2021
                                ' Also update payment date
                                sDateValue = oPayment.DATE_Value
                                sDateValue = Left(sDateValue, 4) & "-" & Mid(sDateValue, 5, 2) & "-" & Right(sDateValue, 2)
                                sMySQL = sMySQL.Replace("BB_DATEVALUE", sDateValue)
                                ' 26.05.2021
                                ' UPDATE  rp_intg_eft_payment_dtl SET File_Process_status = 'PD', act_pay_date = 'BB_DATEVALUE', exchange_rate = BBRET_MON_ACCOUNTEXCHRATE, base_amount = BBRET_TRANSFERREDAMOUNT WHERE voucher_no = 'BB_REFOWN'
                                '
                                ' oPayment.MON_AccountExchRate  
                                '   - sendes med som del av SQL for � oppdatere kursinfo til feltet exchange_rate
                                '     exchange_rate: length 9, 5 decimals. NOTE: Imported from Telepay with 8 decimals !!!
                                '     Use BB_MON_AccountExchRate as variable in SQL
                                ' oPayment.MON_AccountAmount
                                '   - sendes som en del av SQL for � oppdatere belastet bel�p i Ramco databasen, til feltet new_base_amount
                                '     Use BB_TransferredAmount as variable in SQL
                                ' 27.05.2021 - endret til AccountAmount
                                ' 16.08.2021
                                ' MAJOR change:
                                ' We need to calculate AccountAmount PR INVOICE, otherwise the total account amount for the payment
                                ' is set on each invoice, and this leads to major errors in Ramco
                                nMON_AccountExchangerate = Math.Round(oPayment.MON_AccountExchRate, 5)
                                'removed 16.08.2021 nMON_accountAmount = Math.Round(oPayment.MON_AccountAmount / 100, 2)
                                ' 16.08.2021
                                ' For NOK we have no exc.rate. 
                                ' 17.08.2021 - Keeps it as 0, making Ramco calculating the base amount !!!!!
                                'If nMON_AccountExchangerate = 0 Then
                                '    nMON_AccountExchangerate = 1
                                'End If

                                ' 16.08.2021 - calculate account amount pr invoice as paid amount x exchange rate
                                nMON_accountAmountPrInvoice = oInvoice.MON_InvoiceAmount * nMON_AccountExchangerate
                                ' 16.08.2021 - add to a total which can be used to control account amount pr invoice to total account amount (on payment)
                                nMON_TotalCalculatedAccountAmount = nMON_TotalCalculatedAccountAmount + nMON_accountAmountPrInvoice

                                ' er dette en sikker test p� siste invoice?
                                If oInvoice.Index = oPayment.Invoices.Count Then
                                    ' 17.08.2021 - for NOK, do not campare.
                                    If nMON_TotalCalculatedAccountAmount <> 0 Then
                                        If Math.Round(nMON_TotalCalculatedAccountAmount, 0) <> oPayment.MON_AccountAmount Then
                                            ' adjust last invoice account amount with difference, so total of account amounts pr invoice = payments account amount
                                            nMON_accountAmountPrInvoice = nMON_accountAmountPrInvoice + (oPayment.MON_AccountAmount - Math.Round(nMON_TotalCalculatedAccountAmount, 2))
                                            nMON_TotalCalculatedAccountAmount = nMON_TotalCalculatedAccountAmount + (oPayment.MON_AccountAmount - Math.Round(nMON_TotalCalculatedAccountAmount, 2))
                                        End If
                                    End If
                                End If
                                sMySQL = sMySQL.Replace("BB_MON_ACCOUNTEXCHRATE", Replace(nMON_AccountExchangerate.ToString, ",", "."))  ' Bruk desimalpunktum ?
                                'sMySQL = sMySQL.Replace("BB_TRANSFERREDAMOUNT", Replace(nMON_accountAmount.ToString, ",", "."))  ' Bruk desimalpunktum ?
                                ' 16.08.2021 - use calculated accountAmount pr invoice
                                nMON_accountAmountPrInvoice = Math.Round(nMON_accountAmountPrInvoice / 100, 2)
                                sMySQL = sMySQL.Replace("BB_TRANSFERREDAMOUNT", Replace(nMON_accountAmountPrInvoice.ToString, ",", "."))  ' Bruk desimalpunktum ?

                            Else
                                ' Do not update 1. return (status = 01)
                                sMySQL = ""
                            End If
                            If Not EmptyString(sMySQL) Then
                                oMyDal.SQL = sMySQL
                                oMyDal.ExecuteNonQuery()
                            End If
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally

            If Not oMyDal Is Nothing Then
                oMyDal.Close()
                oMyDal = Nothing
            End If

        End Try

        TreatAerogulf_UpdateReturnStatus = bRetValue

    End Function
    ' XokNET 21.10.2014
    Public Function Treat_Franks(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Imports Nachafile - Telepay as export
        ' Detect KID
        ' Set to NOK
        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' 02.12.2014 - we have no account from Nacha;
                        If Trim$(oPayment.I_Account) = "" Then
                            oPayment.I_Account = "00000000000"
                        End If

                        oPayment.MON_InvoiceCurrency = "NOK"
                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.Freetexts.Count > 0 Then
                                If Left$(oInvoice.Freetexts(1).Text, 3) = "KID" Then
                                    oInvoice.Unique_Id = Trim(Mid$(oInvoice.Freetexts(1).Text, 4))
                                    oInvoice.Freetexts(1).Text = ""
                                End If
                            End If
                        Next
                        oPayment.PayType = "D"
                        '02.12.2014
                        oPayment.BANK_CountryCode = "NO"
                        oPayment.DnBNORTBIPayType = "GEN"
                        oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.NoBranchType


                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Franks = bRetValue

    End Function
    ' XokNET 31.12.2014 Added function - take whole function
    Public Function TreatPlatou_US_Return(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Redo from TelepayPlus to Int, to give a better returnfile,
        ' We may have Domestic returns in TP+, needs to set Int in Telepay
        '----------------------------------------------------------------
        Dim sDebetSWIFTAddress As String
        Dim sCountryCode As String
        Dim lRunning As Long
        Dim sValueDate As String
        Dim sEffektueringsRef As String
        Dim i As Integer
        Dim iLoop As Integer
        Dim sOldRefOwn As String
        Dim oTempInvoice As vbBabel.Invoice

        Try

            sValueDate = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.PayType <> "I" Then
                            oPayment.PayType = "I" ' Will give BETFOR01-04 in returnfiles

                            ''''            '- 01.03.2013
                            ''''            ' We will also need an ownref on BETFOR01, which is the same one as in the first BETFOR22 (BETFOR21/22 in return from TelepayPlus)
                            ''''''''''''''            oPayment.REF_Own = oPayment.Invoices(1).REF_Own
                            ''''
                            ''''            ' - 19.03.2013
                            ''''            ' Valuedate on first payment only, because we have one BETFOR21 only, an several BETFOR22 in returnfile from DNB
                            ''''            ' Pick valuedate from first payment, and use that one
                            ''''            If oPayment.DATE_Value <> "19900101" Then
                            ''''                ' not empty
                            ''''                sValueDate = oPayment.DATE_Value
                            ''''            Else
                            ''''                ' empty valuedate
                            ''''                If Not EmptyString(sValueDate) Then
                            ''''                   oPayment.DATE_Value = sValueDate
                            ''''                Else
                            ''''                    ' we have no valuedate from previous payments, use paymentdate
                            ''''                    oPayment.DATE_Value = oPayment.DATE_Payment
                            ''''                End If
                            ''''            End If


                            ' We may also need accountamount if not filled in. This is for US domestics, no currencyexchange
                            ' XNET 29.11.2013 COmmented If, ad create new If 5 lines below
                            'If oPayment.MON_AccountAmount = 0 Then
                            If oPayment.MON_InvoiceCurrency = "USD" Then
                                If oPayment.MON_TransferredAmount = 0 Then
                                    oPayment.MON_TransferredAmount = oPayment.MON_InvoiceAmount
                                End If
                                If oPayment.MON_AccountAmount = 0 Then
                                    oPayment.MON_AccountAmount = oPayment.MON_TransferredAmount
                                End If
                                For Each oInvoice In oPayment.Invoices
                                    If oInvoice.MON_AccountAmount = 0 Then
                                        oInvoice.MON_AccountAmount = oInvoice.MON_InvoiceAmount
                                    End If
                                    If oInvoice.MON_TransferredAmount = 0 Then
                                        oInvoice.MON_TransferredAmount = oInvoice.MON_InvoiceAmount
                                    End If

                                Next
                            End If
                            'End If
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatPlatou_US_Return = True

    End Function

    Public Function Treat_LMTieto(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Redo from IBAN to BBAN for receivers account for LM Tietopalvelut Oy 
        Dim bRetValue As Boolean

        Try

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If IsIBANNumber(oPayment.E_Account, False) Then
                            oPayment.E_Account = ExtractAccountFromIBAN(oPayment.E_Account)
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_LMTieto = bRetValue
    End Function

    Public Function TreatOdfjellDrilling_UK(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' UK GBP-payments will go to Connect, as BACS - One profile for this!
        '----------------------------------------------------------------
        Dim sDebetSWIFTAddress As String
        Dim sCountryCode As String

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        ' Sortcode is imported in Swift-field
                        ' Redo to bank_branch


                        oPayment.PayType = "M"
                        oPayment.DnBNORTBIPayType = "ACH"

                        ' Sortcode is in bankname
                        oPayment.BANK_BranchNo = Mid$(oPayment.BANK_Name, 3)  ' remove leading SC
                        oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode

                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatOdfjellDrilling_UK = True

    End Function
    Public Function TreatJerniaBAX(ByRef oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' 10.10.2015
        ' Import Excelfile with BAX nr
        ' Add BAX nr into a generic textfile

        Dim appExcel As Microsoft.Office.Interop.Excel.Application
        Dim oXLWBook As Microsoft.Office.Interop.Excel.Workbook
        Dim oExcelSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim lRowNo As Integer = 0
        Dim lEmptyRows As Integer = 0
        Dim bFound As Boolean = False
        Dim sAvdNo As String = ""
        Dim sBAXNo As String = ""
        Dim aBAXNOs As String() = Nothing
        Dim sTmp As String = ""
        Dim sGenericImportedText As String = ""
        Dim sGenericExportText As String = ""
        Dim sLine As String = ""
        Dim i As Integer = 0

        Try

            ' filename in oBabelFiles.VB_Profile.Custom1Value
            If Dir(oBabelFiles.VB_Profile.Custom1Value) <> "" Then

                appExcel = New Microsoft.Office.Interop.Excel.Application
                ' Disable all alerts
                appExcel.DisplayAlerts = False
                oXLWBook = appExcel.Workbooks.Open(oBabelFiles.VB_Profile.Custom1Value, , True)
                oExcelSheet = oXLWBook.Worksheets(1) ' er i ark 1
                lRowNo = 1 ' one Headingline
                lEmptyRows = 0
                Do
                    lRowNo = lRowNo + 1
                    '    A           B          C        D
                    '   BAX    Lagersted
                    '   220970	280
                    '   222566	280
                    '   230777	280
                    '-------------
                    sBAXNo = Trim(oExcelSheet.Cells._Default(lRowNo, 1).Value)                      ' A BAX
                    sAvdNo = Trim(Replace(oExcelSheet.Cells._Default(lRowNo, 2).Value, " ", ""))    ' B Avd.no/Lagersted

                    If Len(sBAXNo) > 0 Then
                        'add to array
                        If Array_IsEmpty(aBAXNOs) Then
                            ReDim aBAXNOs(0)
                        Else
                            ReDim Preserve aBAXNOs(UBound(aBAXNOs) + 1)
                        End If
                        aBAXNOs(UBound(aBAXNOs)) = sBAXNo & ";" & sAvdNo
                    Else
                        If oExcelSheet.Cells._Default(lRowNo, 1).Value = "" Then
                            lEmptyRows = lEmptyRows + 1
                        End If
                    End If
                    ' tell opp tomme linjer, og hopp ut
                    If lEmptyRows > 20 Then
                        Exit Do
                    End If
                Loop
                ' array filled!
                '-------------
                ' Close Excelfile
                oXLWBook.Close()
                oXLWBook = Nothing

            End If


            ' Add BAXNo into each line in textfile, in pos 151
            For Each oBabelFile In oBabelFiles
                ' find text from file as one long string
                sGenericImportedText = oBabelFile.GenericText

                ' traverse through whole file, line by line
                Do While sGenericImportedText.Length > 50
                    ' Find one complete line from top
                    sLine = Left(sGenericImportedText, InStr(sGenericImportedText, vbCrLf) + 1)
                    ' Remove the found line
                    sGenericImportedText = Mid(sGenericImportedText, InStr(sGenericImportedText, vbCrLf) + 2)

                    sBAXNo = ""
                    ' Find Avd.no from line
                    sAvdNo = sLine.Substring(19, 3)
                    bFound = False
                    For i = 0 To UBound(aBAXNOs)
                        ' Find BAX from Array
                        If sAvdNo = xDelim(aBAXNOs(i), ";", 2) Then
                            sBAXNo = xDelim(aBAXNOs(i), ";", 1).PadRight(6, " ")
                            bFound = True
                            Exit For
                        End If
                    Next i
                    If Not bFound Then
                        sBAXNo = "UKJENT"
                    End If

                    ' insert BAXNo into textline
                    sLine = Left(sLine, 150) & sBAXNo & Mid(sLine, 157)
                    ' add to exporttext
                    sGenericExportText = sGenericExportText & sLine
                Loop

                ' Finally; add exporttext into oBabelFile
                oBabelFile.GenericText = sGenericExportText

            Next oBabelFile



            'If bMissingAccounts Then
            '    ' present report of missing accounts, and stop

            '    'oReport = CreateObject ("vbbabel.Report")
            '    'oBabelReport = CreateObject ("vbbabel.BabelReport")
            '    'oReport.ReportNo = "003"
            '    'oReport.Heading = "OCR bunter"
            '    'oReport.Preview = False
            '    'oReport.Printer = False

            '    '' to file; if in use set folder/filename in Custom1Value
            '    'If Not EmptyString(oProfile.Custom1Value) Then
            '    '    sSpecialReportPath = oProfile.Custom1Value
            '    '    oReport.ExportType = "PDF"
            '    '    oReport.ExportFilename = ReplaceDateTimeStamp(sSpecialReportPath)
            '    'End If

            '    'oReport.DetailLevel = 1

            '    oFilesetup = oBabelFiles.VB_Profile.FileSetups(iFileSetup_ID)
            '    For j = 1 To oFilesetup.Reports.Count
            '        If oFilesetup.Reports(j).ReportNo = 3 Then ' Specialreport

            '            oBabelReport = New vbBabel.BabelReport
            '            oBabelReport.Special = oBabelFiles.Item(1).Special
            '            oBabelReport.BabelFiles = oBabelFiles
            '            oBabelReport.ReportOnSelectedItems = True
            '            oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.Inside_SpecialRoutines
            '            If Not oBabelReport.RunReports(oFilesetup.FileSetup_ID) Then
            '                j = 0
            '            End If

            '            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

            '            Exit For
            '        End If
            '    Next j

            '    oReport = Nothing
            '    oBabelReport = Nothing

            '    MsgBox("Det finnes mottakere som mangler konto. Se rapport for disse." & vbCrLf & "BabelBank avbrytes", MsgBoxStyle.Critical)
            '    End
            'End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatJerniaBAX = True

    End Function
    Public Function TreatSplitPaymentTypes(ByVal oBabelFiles As vbBabel.BabelFiles, ByRef iFileSetup_ID As Short) As Boolean
        ' Splitt Telepayfiler i flere filer, basert p� betalingstyper
        ' Starter med � splitte i innland og utland.
        ' M� OPPRETTE KLIENTER
        ' Bruk en fast profil med klientnr
        '----------------------------------------------------------------
        Dim sDebetSWIFTAddress As String
        Dim si_CountryCode As String
        Dim sOldAccountNo As String = ""
        Dim bAccountFound As Boolean = False
        Dim sClientNo As String = ""
        Dim sI_BIC As String

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' test Country
                        If sOldAccountNo <> oPayment.I_Account Then
                            bAccountFound = False
                            For Each oClient In oBabelFiles.VB_Profile.Filesetups(iFileSetup_ID).Clients
                                For Each oaccount In oClient.Accounts
                                    If oPayment.I_Account = oaccount.Account Then
                                        sOldAccountNo = oPayment.I_Account
                                        sClientNo = oClient.ClientNo
                                        sI_BIC = Trim(oaccount.SWIFTAddress)
                                        bAccountFound = True

                                        Exit For
                                    End If
                                Next oaccount
                                If bAccountFound Then Exit For
                            Next oClient
                        End If

                        If Not bAccountFound Then
                            Throw New Exception("Konto " & oPayment.I_Account & " finnes ikke i klientoppsettet.")
                        End If
                        If EmptyString(sI_BIC) Then
                            Throw New Exception("Finner ingen SWIFT-kode p� konto " & oPayment.I_Account & ".")
                        End If

                        ' Vi har noen case med blanke tegn i IBAN-konti, som vi kan fjerne, i test
                        oPayment.E_Account = oPayment.E_Account.Replace(" ", "")
                        ' Da kjenner vi BIC p� debetkonto
                        ' MEN - vi m� teste mer p� betalingstype;
                        Select Case sI_BIC.Substring(4, 2)  ' lankode i pos 5,2
                            Case "NO"
                                If IsSEPAPayment(oPayment, sI_BIC) Then
                                    If oPayment.PayType = "S" Then
                                        ' SEPA salary 4603
                                        oPayment.VB_ClientNo = "0003"
                                    Else
                                        ' SEPA credit    
                                        oPayment.VB_ClientNo = "0001"
                                    End If
                                Else
                                    If oPayment.PayType = "S" Then
                                        ' Domestic salary 4703
                                        oPayment.VB_ClientNo = "4703"
                                    ElseIf oPayment.PayType = "M" Then
                                        ' Without advice
                                        oPayment.VB_ClientNo = "4701"
                                    ElseIf oPayment.E_Account = "" Then
                                        ' Moneyorder, no account
                                        oPayment.VB_ClientNo = "4710"
                                    ElseIf oPayment.PayType = "I" Then
                                        ' International
                                        oPayment.VB_ClientNo = "0004"
                                    Else
                                        ' domestic Norway, to account
                                        oPayment.VB_ClientNo = "4701"
                                    End If
                                End If
                            Case "DE"
                                ' SEPA
                                If IsSEPAPayment(oPayment, sI_BIC) Then
                                    If oPayment.PayType = "S" Then
                                        ' SEPA salary 4603
                                        oPayment.VB_ClientNo = "0003"
                                    Else
                                        ' SEPA credit    
                                        oPayment.VB_ClientNo = "0001"
                                    End If

                                Else
                                    ' International, not SEPA
                                    oPayment.VB_ClientNo = "0004"
                                End If

                            Case "SE"
                                ' alle SEPA-er har vi satt fra SE
                                If IsSEPAPayment(oPayment, sI_BIC) Then
                                    If oPayment.PayType = "S" Then
                                        ' SEPA salary 4603
                                        oPayment.VB_ClientNo = "0003"
                                    Else
                                        ' SEPA credit    
                                        oPayment.VB_ClientNo = "0001"
                                    End If

                                Else
                                    If oPayment.PayType = "I" Then
                                        ' International, not SEPA
                                        oPayment.VB_ClientNo = "0004"

                                        ' Swedish domestic
                                    ElseIf oPayment.PayType = "S" Then
                                        ' Domestic salary 4603
                                        oPayment.VB_ClientNo = "4603"
                                    ElseIf oPayment.PayType = "M" Then
                                        ' Without advice
                                        oPayment.VB_ClientNo = "4602"
                                    ElseIf oPayment.E_Account = "" Then
                                        ' Moneyorder, no account
                                        oPayment.VB_ClientNo = "4610"
                                    ElseIf oPayment.PayType = "I" Then
                                        ' International
                                        oPayment.VB_ClientNo = "0004"
                                    ElseIf IsOutgoingKIDPayment(oPayment.PayCode) Then
                                        oPayment.VB_ClientNo = "4601"
                                    Else
                                        ' domestic Sweden, to account
                                        oPayment.VB_ClientNo = "4602"

                                    End If
                                End If

                            Case "FI"
                                ' Kan v�re SEPA fra Finland
                                If IsSEPAPayment(oPayment, sI_BIC) Then
                                    If oPayment.PayType = "S" Then
                                        ' SEPA salary 4603
                                        oPayment.VB_ClientNo = "0003"
                                    Else
                                        ' SEPA credit    
                                        oPayment.VB_ClientNo = "0001"
                                    End If

                                Else
                                    If oPayment.PayType = "I" Then
                                        ' International, not SEPA
                                        oPayment.VB_ClientNo = "0004"

                                        ' Finland domestic
                                    ElseIf oPayment.PayType = "S" Then
                                        ' Domestic salary 3503
                                        oPayment.VB_ClientNo = "3503"
                                    ElseIf oPayment.PayType = "M" Then
                                        ' Without advice
                                        oPayment.VB_ClientNo = "3501"
                                    ElseIf oPayment.E_Account = "" Then
                                        ' Moneyorder, no account
                                        oPayment.VB_ClientNo = "3510"
                                    ElseIf oPayment.PayType = "I" Then
                                        ' International
                                        oPayment.VB_ClientNo = "0004"
                                    Else
                                        ' domestic Finland, to account
                                        oPayment.VB_ClientNo = "3502"

                                    End If
                                End If

                            Case "DK"
                                ' Kan v�re SEPA fra Danmark
                                If IsSEPAPayment(oPayment, sI_BIC) Then
                                    If oPayment.PayType = "S" Then
                                        ' SEPA salary 0003
                                        oPayment.VB_ClientNo = "0003"
                                    Else
                                        ' SEPA credit    
                                        oPayment.VB_ClientNo = "0001"
                                    End If

                                Else

                                    If oPayment.PayType = "I" Then
                                        ' International, not SEPA
                                        oPayment.VB_ClientNo = "0004"

                                        'Denmark domestic
                                    ElseIf IsOutgoingKIDPayment(oPayment.PayCode) Then
                                        Select Case oPayment.Invoices(1).Unique_Id.Substring(0, 2)
                                            Case "71"
                                                oPayment.VB_ClientNo = "4571"
                                            Case "73"
                                                oPayment.VB_ClientNo = "4573"
                                            Case "75"
                                                oPayment.VB_ClientNo = "4575"
                                            Case "01"
                                                oPayment.VB_ClientNo = "4501"
                                            Case "04"
                                                oPayment.VB_ClientNo = "4504"
                                        End Select
                                    ElseIf oPayment.PayType = "S" Then
                                        ' Domestic salary 4503
                                        oPayment.VB_ClientNo = "4503"
                                    ElseIf oPayment.PayType = "M" Then
                                        ' Without advice, common - domestic to account
                                        oPayment.VB_ClientNo = "4701"
                                    ElseIf oPayment.E_Account = "" Then
                                        ' Moneyorder, no account
                                        oPayment.VB_ClientNo = "4510"
                                    ElseIf oPayment.PayType = "I" Then
                                        ' International
                                        oPayment.VB_ClientNo = "0004"
                                    Else
                                        ' domestic Denmark, set as Common to account
                                        oPayment.VB_ClientNo = "4701"

                                    End If
                                End If

                            Case "GB"
                                If oPayment.PayType = "S" Then
                                    ' Domestic salary 4403
                                    oPayment.VB_ClientNo = "4403"
                                ElseIf oPayment.PayType = "M" Then
                                    'BACS
                                    oPayment.VB_ClientNo = "4401"
                                ElseIf oPayment.PayType = "I" Then
                                    ' International
                                    oPayment.VB_ClientNo = "0004"
                                    ' added 01.06.2016
                                ElseIf oPayment.Priority Then
                                    ' set as CHAPS
                                    oPayment.VB_ClientNo = "4402"
                                Else
                                    ' set default as chaps, but redo to Mass
                                    'BACS
                                    oPayment.VB_ClientNo = "4401"
                                    oPayment.PayType = "M"

                                End If


                        End Select

                        ' If no clientno set, set as 4701, Common to account
                        If oPayment.VB_ClientNo = "" Then
                            oPayment.VB_ClientNo = "4701"
                        End If
                        If oPayment.VB_ClientNo = "9999" Then
                            ' feil !
                            oPayment.VB_ClientNo = "9999"
                        End If


                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatSplitPaymentTypes = True

    End Function
    Public Function TreatThorellRevisjon(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' ThorellRevisjon
        ' Import Bankgiro, remove leading zeros from recievers acc
        '----------------------------------------------------------------
        Dim sDebetSWIFTAddress As String
        Dim sCountryCode As String

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.E_Account = Right(oPayment.E_Account, 11)
                        ' redo to NOK
                        oPayment.MON_InvoiceCurrency = "NOK"
                        oPayment.MON_TransferCurrency = "NOK"

                        ' * in first pos of freetext to say it's a KID (OCR-payment)
                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.Freetexts.Count > 0 Then
                                ' assume KID in first freetextline
                                If Left(oInvoice.Freetexts(1).Text, 1) = "*" Then
                                    oInvoice.Unique_Id = Trim(Mid(oInvoice.Freetexts(1).Text, 2))
                                    oPayment.PayCode = "301"
                                    oInvoice.Freetexts(1).Text = ""
                                End If
                            End If
                        Next
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatThorellRevisjon = True

    End Function
    Public Function TreatVinghog(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFilesetup_ID As Integer) As Boolean

        ' Add REf_Own to freetext, to be able to handle returnfiles in MT940
        '-------------------------------------------------------------------

        Dim sRef_Own As String = ""
        Dim oFreeText As vbBabel.Freetext
        Dim bFirstInvoice As Boolean = False

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        bFirstInvoice = True
                        For Each oInvoice In oPayment.Invoices

                            ' first, redo from structured info to freetext
                            ' this to be able to return good info in MT940-returnfiles
                            If oInvoice.Freetexts.Count = 0 And Len(oInvoice.InvoiceNo) > 0 Then
                                '    ' create a freetext
                                oFreeText = oInvoice.Freetexts.Add
                                oFreeText.Text = oInvoice.InvoiceNo
                            End If
                            ' Blank InvoiceNo
                            oInvoice.InvoiceNo = ""

                            sRef_Own = oInvoice.REF_Own.Trim
                            If bFirstInvoice Then
                                If EmptyString(oInvoice.Unique_Id) Then
                                    ' this ref will be found in MT940 :86: somewhere
                                    oPayment.Ref_Own_BabelBankGenerated = "BB(" & sRef_Own & ")"
                                    oPayment.REF_Own = "BB(" & sRef_Own & ")"
                                    ' We will need to put this first reference into freetext if not KID-transaction
                                    ' Lets also try the same for KID, to see if freetext may be transferred;
                                    If oInvoice.Freetexts.Count = 0 Then
                                        oFreeText = oInvoice.Freetexts.Add
                                    End If
                                    oInvoice.Freetexts(1).Text = "BB(" & sRef_Own & ") " & oInvoice.Freetexts(1).Text
                                Else
                                    ' KID
                                    ' put KID as part of ref own?
                                    'oPayment.Ref_Own_BabelBankGenerated = oInvoice.Unique_Id
                                    ' 10.11.2016 KID er ikke unik!!!!! M� bringe inn bel�p som del av unik referanse
                                    oPayment.Ref_Own_BabelBankGenerated = oInvoice.Unique_Id & "/" & Trim(Str(oPayment.MON_InvoiceAmount))
                                End If

                                bFirstInvoice = False
                            End If

                        Next
                    Next
                Next
            Next

            ' Then call general function to save data to BabelBanks database;
            TreatISO20022(oBabelFiles, iFilesetup_ID, False)

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatVinghog = True

    End Function
    Public Function TreatVinghogReturn(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFilesetup_ID As Integer) As Boolean
        ' Imported MT940, try to pick info from :86: record to recreate Telepay returnfiles
        ' ----------------------------------------------------------------------------------

        Dim sRef_Own As String = ""
        Dim oFreeText As vbBabel.Freetext
        Dim bFirstInvoice As Boolean = False
        Dim sFreetext As String
        Dim sTmp As String = ""
        Dim i As Integer = 0
        Dim j As Integer = 0

        Try

            ' 27.06.2016
            ' for "long" payments, payments with muth freetextinfo, or many KIDs, the payment is splitted into several
            ' "payments" in the MT940 returnfile. The additional splitted payments have 0,00 in amountfield
            ' Because of this, we will need to merge the 0,00 payments to the previous payment
            ' Hver betaling = en batch, s� vi m� til forrige batch
            For Each oBabelFile In oBabelFiles
                For i = 1 To oBabelFile.Batches.Count
                    oBatch = oBabelFile.Batches(i)
                    If oBatch.Payments(1).MON_InvoiceAmount = 0 And i > 1 Then
                        oPayment = oBatch.Payments(1)
                        ' find freetext for 0-amount payment
                        sFreetext = ""
                        For Each oInvoice In oPayment.Invoices
                            For Each oFreeText In oInvoice.Freetexts
                                sFreetext = sFreetext & oFreeText.Text
                            Next
                        Next
                        ' then add this freetext to previous payment, which is not set Exported = True
                        ' use length 40 pr freetext
                        For j = 1 To 999

                            If oBabelFile.Batches(i - j).Payments(1).Exported = False Then
                                Exit For
                            End If
                        Next j
                        oInvoice = oBabelFile.Batches(i - j).Payments(1).Invoices.Add
                        Do
                            If sFreetext.Length = 0 Then
                                Exit Do
                            End If
                            oFreeText = oInvoice.Freetexts.Add
                            oFreeText.Text = Mid(sFreetext, 1, 40)
                            sFreetext = Mid(sFreetext, 41)
                        Loop
                        ' set 0,00 payment as exported
                        oBabelFile.Batches(i).Payments(1).Exported = True
                    End If
                Next i
            Next oBabelFile

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    oBatch.REF_Own = "BB"
                    For Each oPayment In oBatch.Payments
                        If oPayment.Exported = False Then
                            sFreetext = ""
                            For Each oInvoice In oPayment.Invoices
                                For Each oFreeText In oInvoice.Freetexts
                                    sFreetext = sFreetext & oFreeText.Text
                                Next
                            Next
                            ' try to extract the own_ref part from freetext
                            ' it's set as BB(nnnnn)
                            If InStr(sFreetext, "BB(") > 0 Then
                                sTmp = Mid(sFreetext, InStr(sFreetext, "BB("))
                                sTmp = Left(sTmp, InStr(sTmp, ")"))

                                ' set into ref_own
                                oPayment.REF_Own = sTmp
                                ' Mark as filepayments (ala SEB)
                                oPayment.REF_Bank1 = "STOESI"
                            ElseIf InStr(sFreetext, "Betaling med KID") > 0 Then
                                ' If paid with KID, then take it that it origins from BabelBank
                                ' We will need first KID as reference for all payments inside same invoice;

                                ' OSL 16041931041Betaling med KID0252000001161546 NOK 1520.0021ABELBANK7752173932PILARO AS
                                ' KID will be after 02520 (hopefully), up to next space


                                'sTmp = Mid(sFreetext, InStr(sFreetext, "Betaling med KID") + 16)
                                ' 29.04.2021 
                                ' Seems like SEB in March 2021 has started adding 0252021 in front of KID, instead of "02520" text as startpoint. we have much freetext before first real "Betaling med KID"
                                'If InStr(sFreetext, "0252021") > 1 Then
                                '    sTmp = Mid(sFreetext, InStr(sFreetext, "Betaling med KID0252021") + 18)
                                'Else
                                '    ' before some time i March 2021
                                '    sTmp = Mid(sFreetext, InStr(sFreetext, "Betaling med KID02520") + 16)
                                'End If
                                ' 02.06.2021 - remove the 025?20-part (CGA+ZKA)
                                sTmp = Mid(sFreetext, InStr(sFreetext, "Betaling med KID025?20") + 22)
                                ' 02.06.2021 - we can have one more element in the ZKA, f.eks. ?21, if so, remove
                                If Left(sTmp, 1) = "?" Then
                                    sTmp = Mid(sTmp, 4)
                                End If

                                ' should now have "0252000001161546 NOK 1520.0021ABELBANK7752173932PILARO AS"
                                'sTmp = Mid(sTmp, 6)

                                ' probably have 00001161546 NOK 1520.0021ABELBANK7752173932PILARO AS
                                sTmp = xDelim(sTmp, " ", 1)
                                ' now we have the KID 00001161546
                                ' Put KID into REF_own, to use as lookup in BabelBanks database
                                'oPayment.REF_Own = sTmp

                                ' 22.11.2016 - have from now on added /+ amount
                                sTmp = sTmp & "/" & Trim(oPayment.MON_InvoiceAmount.ToString)
                                oPayment.REF_Own = sTmp

                                ' 10.11.2016 - have from now on added /+ amount

                                ' Mark as filepayments (ala SEB)
                                oPayment.REF_Bank1 = "STOESI"

                                Else
                                    ' not generated in BabelBank, remove
                                    oPayment.Exported = True
                                End If
                                ' for � sl� opp i BabelBankGenerateEndToEndRef i Treat_ISO20022_Return:
                                oPayment.VB_Profile.EndToEndRefFromBabelBank = True
                        End If 'If oPayment.Exported = False Then
                    Next oPayment
                Next oBatch
            Next oBabelFile

            ' Then call general function to restore data to BabelBanks database;
            TreatISO20022_Return(oBabelFiles, iFilesetup_ID)

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatVinghogReturn = True

    End Function
    ' Santander needs a .CSV-file with the same payments as in Matched_Report (Report_504)
    Public Function Santander_CSV_Matched(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile, ByVal oClient As vbBabel.Client) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sString As String
        Dim sFreetext As String
        Dim sBabelBankText As String
        Dim sFilename As String

        Dim oBabelFile As vbbabel.BabelFile
        Dim oBatch As vbbabel.Batch
        Dim oPayment As vbbabel.Payment
        Dim oInvoice As vbbabel.Invoice
        Dim oFreetext As vbBabel.Freetext
        Dim bReturnvalue As Boolean

        Try

            oFs = New Scripting.FileSystemObject  '22.05.2017CreateObject ("Scripting.FileSystemObject")

            ' Name of exportfile in Setupmenu, Custom2Value
            If Not EmptyString(oProfile.Custom2Value) Then

                sFilename = Trim(ReplaceDateTimeStamp(oProfile.Custom2Value))
                ' sett inn klientnavn i filen
                sFilename = Replace(sFilename, ".", "_" & Trim(oClient.Name) & ".")
                oFile = oFs.OpenTextFile(sFilename, Scripting.IOMode.ForWriting, True)

                ' Heading;
                sString = "Navn;L�nenummer;KID;Kontonummer;Bel�p;Dato;Adresse;Poststed;Santanderkonto;Arkivref;Blankettref;Kontoref;Bilagsnummer;Avstemt av;Bel�pet gjelder;BabelBank melding"
                oFile.WriteLine(sString)
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            ' 26-27.04.2010 - ASSUMES ONE ACCOUNT ONLY pr client for Santander !!!!!
                            If oPayment.I_Account = oClient.Accounts.Item(1).Account Then

                                If oPayment.MATCH_Matched <> vbBabel.BabelFiles.MatchStatus.Matched Then
                                    For Each oInvoice In oPayment.Invoices
                                        If oInvoice.MATCH_Matched And oInvoice.MATCH_Final Then
                                            sString = ""
                                            sString = sString & oPayment.E_Name & ";"               ' A Navn
                                            sString = sString & oInvoice.InvoiceNo & AddCDV10digit(oInvoice.InvoiceNo) & ";"            ' B L�nenummer
                                            sString = sString & "'" & oInvoice.MATCH_ID & ";"       ' C KID
                                            sString = sString & oPayment.E_Account & ";"            ' D Betalers konto
                                            sString = sString & Format(oInvoice.MON_InvoiceAmount / 100, "##,##0.00") & ";" ' E Bel�p
                                            sString = sString & oPayment.DATE_Payment & ";"         ' F Dato
                                            sString = sString & oPayment.E_Adr1 & ";"            ' G Adresse
                                            sString = sString & oPayment.E_Zip & " " & oPayment.E_City & ";"    ' H Poststed
                                            sString = sString & oPayment.I_Account & ";"            ' I Mottakerkonto
                                            sString = sString & oPayment.REF_Bank2 & ";"            ' J Arkivref
                                            sString = sString & oPayment.REF_Bank1 & ";"            ' K Blankettref
                                            sString = sString & oBatch.REF_Bank & ";"             ' L Kontoref
                                            sString = sString & Trim(oPayment.VoucherNo) & ";"            ' M Bilagsnummer
                                            sString = sString & Replace(Replace(oPayment.MATCH_HowMatched, vbCr, ""), vbLf, "") & ";" ' N Hvordan avstemt

                                            sFreetext = ""
                                            sBabelBankText = ""

                                            For Each oFreetext In oInvoice.Freetexts
                                                If oFreetext.Qualifier > 2 Then
                                                    sBabelBankText = sBabelBankText & oFreetext.Text
                                                Else
                                                    sFreetext = sFreetext & oFreetext.Text
                                                End If
                                            Next
                                            sString = sString & sFreetext & ";"                     ' O Melding
                                            sString = sString & "'" & sBabelBankText                ' P Melding fra BabelBank
                                            oFile.WriteLine(sString)
                                        End If


                                    Next oInvoice
                                End If

                            End If
                        Next oPayment
                    Next oBatch
                Next oBabelFile
                oFile.Close()
                oFile = Nothing
                oFs = Nothing
                bReturnvalue = True

            Else
                bReturnvalue = False
                Throw New Exception("Angi navn for .csv eksportfil i Oppsettmenyen.")
            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Return bReturnvalue

    End Function
    Public Function TreatFalck_OCR(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Imports OCR, Exports MT940 - must redo some data
        ' ----------------------------------------------------------------------------------

        Dim sRef_Own As String = ""
        Dim oFreeText As vbBabel.Freetext
        Dim bFirstInvoice As Boolean = False
        Dim sFreetext As String
        Dim sTmp As String = ""

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    ' have no batch ref, add one

                    For Each oPayment In oBatch.Payments
                        oPayment.REF_Bank1 = "NTRFNONREF//OSL"

                        'sFreetext = ""
                        For Each oInvoice In oPayment.Invoices
                            oFreeText = oInvoice.Freetexts.Add
                            oFreeText.Text = "DOM. INCOMING TRANSFER"
                            'For Each oFreeText In oInvoice.Freetexts
                            'sFreetext = sFreetext & oFreeText.Text
                            'Next
                        Next
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatFalck_OCR = True

    End Function
    Public Function TreatFalck_Return(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Imports Telepay Avregningsreturn, Exports MT940 - must redo some data
        ' ----------------------------------------------------------------------------------

        Dim sRef_Own As String = ""
        Dim oFreeText As vbBabel.Freetext
        Dim bFirstInvoice As Boolean = False
        Dim sFreetext As String
        Dim sTmp As String = ""

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    ' have no batch ref, add one

                    For Each oPayment In oBatch.Payments
                        If oPayment.Invoices.Count > 0 Then
                            oPayment.REF_Bank1 = "NLOC" & Trim(oPayment.Invoices(1).REF_Own).ToLower & "//"
                        End If
                        oPayment.REF_Bank2 = "905579451"

                        'For Each oInvoice In oPayment.Invoices
                        '    oFreeText = oInvoice.Freetexts.Add
                        '    oFreeText.Text = "DOM. INCOMING TRANSFER"
                        '    'For Each oFreeText In oInvoice.Freetexts
                        '    'sFreetext = sFreetext & oFreeText.Text
                        '    'Next
                        'Next
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatFalck_Return = True

    End Function
    Public Function Treat_ISO20022_Leroy(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Imports Telepay Exports Pain.001
        ' Catch Konserninterne - set with Statebankcode = 98
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        ' For accountconverting, we can't use the normal way, as it will be "redone" for 
                        ' returnfiles in ImportExport, as we save the "shortaccount"
                        ' That's why we need to do accountconverting here
                        ' remove leading zeroes
                        oPayment.I_Account = RemoveLeadingCharacters(oPayment.I_Account, "0")

                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.STATEBANK_Code = "98" Then
                                oPayment.ToOwnAccount = True
                                oPayment.PayCode = "403"  ' To own account
                            End If
                        Next
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_ISO20022_Leroy = True

    End Function
    Public Function Treat_Nacha_Leroy(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Imports Telepay Exports Nacha
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        ' Because of both Nachafiles and pain.001 we will need two clients in BabelBank setup.
                        ' Have to "cheat" with accounts due to this, and must redo here
                        ' Like redo from 00013392001 to dummyaccount 99999999
                        ' AccountNo is not used in Nachafiles, it's just to hit correct client
                        If Len(oPayment.I_Account) = 11 And oPayment.I_Account.Substring(0, 3) = "000" Then
                            oPayment.I_Account = "99999999"
                        End If
                        oPayment.BANK_I_SWIFTCode = "DNBAUS33"
                        oPayment.PayType = "D"

                        ' 07.06.2016
                        ' Legg alltid dato til morgendagens dato for Nachapostene
                        oPayment.DATE_Payment = DateToString(DateAdd(DateInterval.Day, 1, Date.Today))

                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Nacha_Leroy = True

    End Function
    Public Function Treat_ACHUS_Leroy(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' 29.06.2018 - Imports Telepay Exports Pain.001
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        oPayment.BANK_I_SWIFTCode = "DNBAUS33"

                        ' For files to bank- redo from I to M
                        oPayment.PayType = "M"  ' masstrans to get ACH
                        ' 07.06.2016
                        ' Legg alltid dato til morgendagens dato for Nachapostene
                        'oPayment.DATE_Payment = DateToString(DateAdd(DateInterval.Day, 1, Date.Today))

                        ' must skip BIC, to be able to use FedWire in the exportfile;
                        oPayment.BANK_SWIFTCode = ""
                        oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.Fedwire
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_ACHUS_Leroy = True

    End Function
    Public Function Treat_ACHUS_Leroy_Return(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' for returnfiles from bank, reset to I
                        oPayment.PayType = "I"
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_ACHUS_Leroy_Return = True

    End Function
    Public Function Treat_ISO20022_Leroy_Return(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Redo from domestic to int for Ler�y US Inc, to be able to produce Telepay Int (BETFOR01-04)
        ' ----------------------------------------------------------------------------------

        For Each oBabelFile In oBabelFiles
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    ' endret 28.05.2016
                    oPayment.PayType = "I"
                Next
            Next
        Next

        Treat_ISO20022_Leroy_Return = True

    End Function
    Public Function TreatSalmarSpecialReport(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFileSetup_ID As Integer) As Boolean
        ' Report for payments to wrong accounts

        Dim oReport As vbBabel.Report
        Dim oBabelReport As vbBabel.BabelReport
        Dim bErrorMarked
        Dim j As Integer

        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim bReturnValue As Boolean


        Try

            oFilesetup = oBabelFiles.VB_Profile.FileSetups(iFileSetup_ID)
            For j = 1 To oFilesetup.Reports.Count
                If oFilesetup.Reports(j).ReportNo = 3 Then ' Specialreport

                    oBabelReport = New vbBabel.BabelReport
                    oBabelReport.Special = "SALMAR_FEIL"
                    oBabelReport.BabelFiles = oBabelFiles
                    oBabelReport.ReportOnSelectedItems = True
                    oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.Inside_SpecialRoutines
                    If Not oBabelReport.RunReports(oFilesetup.FileSetup_ID) Then
                        j = 0
                    End If

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

                    Exit For
                End If
            Next j
            bReturnValue = True

        Catch ex As Exception

            bReturnValue = False

        End Try

        Return bReturnValue

    End Function
    Public Function TreatSalmarRedoOCR(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Change from OCR-payment to manual payment - to be able to take OCRs through "normal" matching

        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oFreetext As vbBabel.Freetext
        Dim bReturnValue As Boolean
        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If IsOCR(oPayment.PayCode) Then
                            oPayment.PayCode = "601"
                            For Each oInvoice In oPayment.Invoices
                                oFreetext = oInvoice.Freetexts.Add
                                oFreetext.Text = "KID: " & oInvoice.Unique_Id
                                oInvoice.Unique_Id = ""
                            Next
                        End If
                    Next
                Next

            Next

            bReturnValue = True

        Catch ex As Exception

            bReturnValue = False

        End Try

        Return bReturnValue

    End Function
    Public Function Treat_Ramudden_NO(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Domestic Lev.bet fil as input. Domestic NOK payments only
        ' Use # as KID marker
        ' merge payments to be able to handle creditnotes
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        'If oPayment.MON_InvoiceCurrency = "NOK" Then
                        '    oPayment.PayType = "D"
                        'End If
                        oPayment.MON_InvoiceCurrency = "NOK"
                        oPayment.MON_TransferCurrency = "NOK"
                        oPayment.PayType = "D"

                        ' 29.05.2019 - change e_account, from like 9044000001185706 to 90441185706
                        If Len(oPayment.E_Account) = 16 Then
                            oPayment.E_Account = Left(oPayment.E_Account, 4) & Mid(oPayment.E_Account, 10, 7)
                        End If
                        ' added 08.12.2016
                        If EmptyString(oPayment.E_Name) Then
                            ' when importing LB innlandsfil, there is no name to recceiver
                            ' SEB needs this
                            oPayment.E_Name = "Mottaker"
                        End If
                        ' added 15.08.2016
                        ' if freetext starts with *, use as KID
                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.Freetexts.Count > 0 Then
                                ' check only first freetext
                                If Left(oInvoice.Freetexts(1).Text, 1) = "#" Then
                                    oInvoice.Unique_Id = Mid(oInvoice.Freetexts(1).Text, 2).Trim
                                    oInvoice.Freetexts(1).Text = ""
                                    oPayment.PayCode = "301"   ' OCR
                                End If
                            End If
                        Next
                    Next
                Next
                '''oBabelFile.Visma_Merge(True)
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Ramudden_NO = True

    End Function
    Public Function Treat_Ramudden_FI(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Domestic Lev.bet fil as input. Domestic EUR payments only
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.MON_InvoiceCurrency = "EUR"
                        oPayment.MON_TransferCurrency = "EUR"
                        'oPayment.PayType = "D"

                        oBatch.VB_Profile.CompanyName = "RAMUDDEN OY"  ' used in Pain.001 file
                        ' added 08.12.2016
                        If EmptyString(oPayment.E_Name) Then
                            ' when importing LB innlandsfil, there is no name to recceiver
                            ' SEB needs this
                            oPayment.E_Name = "Receiver"
                        End If

                        ' Redo from Structured to Unstructured, as we do not know if Reference or not
                        For Each oInvoice In oPayment.Invoices
                            If Not EmptyString(oInvoice.Unique_Id) Then
                                oFreeText = oInvoice.Freetexts.Add
                                oFreeText.Text = oInvoice.Unique_Id
                                oInvoice.Unique_Id = ""
                            End If
                        Next
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Ramudden_FI = True

    End Function
    Public Function Treat_Ramudden_EE(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Domestic Lev.bet fil as input. Domestic EUR payments only
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.MON_InvoiceCurrency = "EUR"
                        oPayment.MON_TransferCurrency = "EUR"
                        'oPayment.PayType = "D"

                        oBatch.VB_Profile.CompanyName = "RAMUDDEN O�"  ' used in Pain.001 file
                        ' added 08.12.2016
                        If EmptyString(oPayment.E_Name) Then
                            ' when importing LB innlandsfil, there is no name to recceiver
                            ' SEB needs this
                            oPayment.E_Name = "Receiver"
                        End If

                        ' Redo from Structured to Unstructured, as we do not know if Reference or not
                        For Each oInvoice In oPayment.Invoices
                            If Not EmptyString(oInvoice.Unique_Id) Then
                                oFreeText = oInvoice.Freetexts.Add
                                oFreeText.Text = oInvoice.Unique_Id
                                oInvoice.Unique_Id = ""
                            End If
                        Next
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Ramudden_EE = True

    End Function
    Public Function Treat_Ramudden_NO_Pain(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Domestic Lev.bet fil as input. Domestic NOK payments only
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' 04.04.2022
                        ' Also foreign currency, use currency from importfil
                        '----------------------------------------------------
                        'oPayment.MON_InvoiceCurrency = "NOK"
                        'oPayment.MON_TransferCurrency = "NOK"
                        'oPayment.PayType = "D"

                        oBatch.VB_Profile.CompanyName = "RAMUDDEN AS"  ' used in Pain.001 file
                        ' added 08.12.2016
                        If EmptyString(oPayment.E_Name) Then
                            ' when importing LB innlandsfil, there is no name to recceiver
                            ' SEB needs this
                            oPayment.E_Name = "Receiver"
                        End If

                        ' 04.04.2022 - make sure KID is used for NOK payments only
                        If oPayment.MON_InvoiceCurrency = "NOK" Then
                            ' if freetext starts with #, use as KID
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.Freetexts.Count > 0 Then
                                    ' check only first freetext
                                    If Left(oInvoice.Freetexts(1).Text, 1) = "#" Then
                                        oInvoice.Unique_Id = Mid(oInvoice.Freetexts(1).Text, 2).Trim
                                        oInvoice.Freetexts(1).Text = ""
                                        oPayment.PayCode = "301"   ' OCR
                                    End If
                                End If
                                ' Or if structured invoice (INV) starts with #, make KID
                                If Left(oInvoice.InvoiceNo, 1) = "#" Then
                                    oInvoice.Unique_Id = Mid(oInvoice.InvoiceNo, 2).Trim
                                    oInvoice.InvoiceNo = ""
                                    oPayment.PayCode = "301"   ' OCR
                                End If
                            Next
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Ramudden_NO_Pain = True

    End Function
    Public Function Treat_Ramudden_Return(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFileSetup_ID As Integer) As Boolean
        ' Find converted accountnumber
        ' ----------------------------------------------------------------------------------

        Dim sOldAccountNo As String = ""
        Dim sI_Account As String = ""

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' 03.09.2019
                        ' Create Lev.bet return file on domestic format
                        oPayment.PayType = "D"
                        If oPayment.I_Account <> sOldAccountNo Then
                            sI_Account = oPayment.I_Account
                            oFilesetup = oBabelFiles.VB_Profile.FileSetups(iFileSetup_ID)
                            For Each oClient In oFilesetup.Clients
                                For Each oaccount In oClient.Accounts
                                    If sI_Account = oaccount.Account Then
                                        sOldAccountNo = oPayment.I_Account
                                        sI_Account = Trim(oaccount.ConvertedAccount)
                                        Exit For
                                    End If
                                Next oaccount
                            Next oClient
                        End If
                        oPayment.I_Account = sI_Account  ' converted account

                        ' 03.09.2019
                        ' Using REF_EndToEnd from Camt.054D
                        ' In the WriteLevBet returnformat, we need to have this in oInvoice.Ref_Own
                        ' Hopefully only one invoice pr payment
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.REF_Own = oPayment.REF_EndToEnd
                        Next
                        If oPayment.Invoices.Count > 1 Then
                            'MsgBox("Mer enn en faktura pr betaling")
                        End If

                        ' 21.10.2019 - Redo to SEK for returnfile BG
                        oPayment.MON_InvoiceCurrency = "SEK"
                        oPayment.MON_TransferCurrency = "SEK"

                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Ramudden_Return = True

    End Function
    Public Function Treat_Presskontakterna_NO(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Domestic Lev.bet fil as input. Domestic NOK payments only
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        'If oPayment.MON_InvoiceCurrency = "NOK" Then
                        '    oPayment.PayType = "D"
                        'End If
                        oPayment.MON_InvoiceCurrency = "NOK"
                        oPayment.MON_TransferCurrency = "NOK"
                        oPayment.PayType = "D"

                        ' 26.11.2019 - added possibility for KID with * in front
                        For Each oInvoice In oPayment.Invoices
                            If Left(oInvoice.InvoiceNo, 1) = "*" Then
                                oInvoice.Unique_Id = Mid(oInvoice.InvoiceNo, 2)
                                oInvoice.InvoiceNo = ""
                                oPayment.PayCode = "301"  'OCR
                            End If
                        Next

                        ' 29.05.2019 - change e_account, from like 9044000001185706 to 90441185706
                        If Len(oPayment.E_Account) = 16 Then
                            oPayment.E_Account = Left(oPayment.E_Account, 4) & Mid(oPayment.E_Account, 10, 7)
                        End If
                    Next
                Next

                '''oBabelFile.Visma_Merge(True)
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Presskontakterna_NO = True

    End Function
    Public Function Treat_Presskontakterna_DK(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Domestic Lev.bet fil as input. Domestic DKK payments only
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        'If oPayment.MON_InvoiceCurrency = "NOK" Then
                        '    oPayment.PayType = "D"
                        'End If
                        oPayment.MON_InvoiceCurrency = "DKK"
                        oPayment.MON_TransferCurrency = "DKK"
                        oPayment.PayType = "D"

                        ' 29.05.2019 - change e_account, from like 9044000001185706 to 90441185706
                        'If Len(oPayment.E_Account) = 16 Then
                        '    oPayment.E_Account = Left(oPayment.E_Account, 4) & Mid(oPayment.E_Account, 10, 7)
                        'End If

                        ' 27.11.2019 - added possibility for FIK with * in front
                        For Each oInvoice In oPayment.Invoices
                            If Left(oInvoice.InvoiceNo, 1) = "*" Then
                                ' 10.12.2019 - remove / as it is added in WritePain001
                                oInvoice.Unique_Id = Replace(Mid(oInvoice.InvoiceNo, 2), "/", "")
                                oInvoice.InvoiceNo = ""
                                oPayment.PayCode = "371"  'FIK71
                            End If
                        Next

                        ' 24.10.2019
                        ' Test for FIK payments
                        If Len(oPayment.E_Account) = 8 Then
                            ' assume FIK71 - BetalingsID as structured invoiceno, put into Unique ID
                            If oPayment.Invoices.Count > 0 Then
                                ' 15 length BetalingsID
                                If Len(oPayment.Invoices(1).Unique_Id) = 15 Then
                                    For Each oInvoice In oPayment.Invoices
                                        oInvoice.Unique_Id = "71" & oInvoice.Unique_Id
                                        oInvoice.InvoiceNo = ""
                                    Next
                                    ' set as FIK71
                                    oPayment.PayCode = "371"
                                End If
                            End If
                        End If
                    Next
                Next
                '''oBabelFile.Visma_Merge(True)
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Presskontakterna_DK = True

    End Function
    Public Function Treat_Presskontakterna_Return(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFileSetup_ID As Integer) As Boolean
        ' Find converted accountnumber
        ' ----------------------------------------------------------------------------------

        Dim sOldAccountNo As String = ""
        Dim sI_Account As String = ""

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' 03.09.2019
                        ' Create Lev.bet return file on domestic format
                        oPayment.PayType = "D"
                        If oPayment.I_Account <> sOldAccountNo Then
                            sI_Account = oPayment.I_Account
                            oFilesetup = oBabelFiles.VB_Profile.FileSetups(iFileSetup_ID)
                            For Each oClient In oFilesetup.Clients
                                For Each oaccount In oClient.Accounts
                                    If sI_Account = oaccount.Account Then
                                        sOldAccountNo = oPayment.I_Account
                                        sI_Account = Trim(oaccount.ConvertedAccount)
                                        Exit For
                                    End If
                                Next oaccount
                            Next oClient
                        End If
                        oPayment.I_Account = sI_Account  ' converted account

                        ' 03.09.2019
                        ' Using REF_EndToEnd from Camt.054D
                        ' In the WriteLevBet returnformat, we need to have this in oInvoice.Ref_Own
                        ' Hopefully only one invoice pr payment
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.REF_Own = oPayment.REF_EndToEnd
                        Next
                        If oPayment.Invoices.Count > 1 Then
                            MsgBox("Mer enn en faktura pr betaling")
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Presskontakterna_Return = True

    End Function
    Public Function Treat_TeekayISO20022(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Redo bankcharges to SHA for SEPA payments
        ' AND Force US ACH (Nacha) payments from ACH to Wire, by setting as priority (20.11.2018)
        ' ----------------------------------------------------------------------------------
        Dim sI_SWIFT As String = ""
        Dim bAccountFound As Boolean = False

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        'If IsSEPAPayment(oPayment, , False) Then
                        ' 05.01.2018 - SHAR for all currencies within EU_EAA area
                        If IsEU_EEAPayment(oPayment, , False) Then
                            oPayment.MON_ChargeMeAbroad = False
                            oPayment.MON_ChargeMeDomestic = True
                        End If

                        '06.11.2017
                        'For Singapore domestic, the payments will be marked as G3 Mass, with Purposecode SUPP
                        If oPayment.BANK_CountryCode = "SG" And oPayment.MON_InvoiceCurrency = "SGD" And oPayment.MON_TransferCurrency = "SGD" Then
                            oPayment.PurposeCode = "SUPP"
                            oPayment.PayCode = "D"
                        End If

                        ' 20.11.2018 - added next If to force nacha to be Wire
                        bAccountFound = False
                        If oPayment.BANK_CountryCode = "US" And oPayment.MON_TransferCurrency = "USD" Then
                            ' Check if US debit account;
                            For Each oFilesetup In oBabelFile.VB_Profile.FileSetups
                                For Each oClient In oFilesetup.Clients
                                    For Each oaccount In oClient.Accounts
                                        If oPayment.I_Account = oaccount.Account Then
                                            sI_SWIFT = Trim(oaccount.SWIFTAddress)
                                            bAccountFound = True
                                            Exit For
                                        End If
                                    Next oaccount
                                    If bAccountFound Then Exit For
                                Next oClient
                                If bAccountFound Then Exit For
                            Next oFilesetup
                            If Left(sI_SWIFT, 6) = "DNBAUS" Then
                                ' Removed 10.12.2018 - Wire is not Priority (SDVA)
                                'oPayment.Priority = True
                                ' 10.12.2018 - Instead add a new PaymentType =281 Wire US
                                oPayment.PayCode = "281"
                            End If
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_TeekayISO20022 = True

    End Function
    Public Function TreatSG_Outgoing(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' added 30.12.2016
        ' to handle BIC with errors for SG_OUTGOING
        ' - sjekk om BIC er 8 eller 11 tegn
        ' - Hvis mindre enn 8 legg til �X��er s� det blir 8
        ' - Hvis 9 eller 10 trim ned det som ligger i aq fila til 8
        ' - Hvis flere enn 11 trim ned til 11
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.BANK_SWIFTCode = oPayment.BANK_SWIFTCode.ToUpper  ' always uppercase
                        If oPayment.BANK_SWIFTCode.Length > 0 Then
                            If oPayment.BANK_SWIFTCode.Length <> 8 And oPayment.BANK_SWIFTCode.Length <> 11 Then
                                ' change Swift to be able to pass it through the syntax check
                                If oPayment.BANK_SWIFTCode.Length < 8 Then
                                    oPayment.BANK_SWIFTCode = Left(oPayment.BANK_SWIFTCode & "XXXXXXXXXX", 8)
                                ElseIf oPayment.BANK_SWIFTCode.Length < 11 Then
                                    oPayment.BANK_SWIFTCode = Left(oPayment.BANK_SWIFTCode, 8)
                                ElseIf oPayment.BANK_SWIFTCode.Length > 11 Then
                                    oPayment.BANK_SWIFTCode = Left(oPayment.BANK_SWIFTCode, 11)
                                End If
                            End If
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatSG_Outgoing = True

    End Function

    Public Function TreatBodoBBL(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal sFilenameOut As String) As Boolean
        ' Fill in clientnumbers based on length of KID for one special client/account
        ' Used to split OCR-files for Bod� BBL, based on KID-length
        ' One time job for Keanette Skau, DNB

        ' Spit file in KID_20/KID713
        ' Prepare special report for totals for VIS and SAP
        ' Prepare array for use in rp_951_Array5
        Dim lCounter As Long
        Dim aDoc(,) As Object
        Dim dKID713Total As Double
        Dim dKID_20Total As Double
        Dim oReport As vbBabel.Report
        Dim oBabelReport As vbBabel.BabelReport
        Dim sTmp As String
        Dim bReturnValue As Boolean

        Try

            dKID713Total = 0
            dKID_20Total = 0

            lCounter = 0
            ReDim aDoc(5, 0)
            aDoc(0, lCounter) = "Splittede OCR bunter"
            aDoc(1, lCounter) = "&M"

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    dKID_20Total = 0
                    dKID713Total = 0

                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(5, lCounter)
                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(5, lCounter)
                    aDoc(0, lCounter) = "&BDato:"
                    aDoc(1, lCounter) = oBatch.DATE_Production
                    aDoc(3, lCounter) = ""
                    aDoc(4, lCounter) = ""

                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(5, lCounter)
                    aDoc(0, lCounter) = "&BOpprinnelig bunt:"
                    aDoc(1, lCounter) = ""
                    aDoc(3, lCounter) = ""
                    aDoc(4, lCounter) = "&B" & "&R" & Format(oBatch.MON_TransferredAmount / 100, "##,##0.00")

                    For Each oPayment In oBatch.Payments
                        For Each oInvoice In oPayment.Invoices
                            If Len(Trim$(oInvoice.Unique_Id)) = 20 Then
                                oPayment.VB_ClientNo = "KID_20"
                                dKID_20Total = dKID_20Total + oPayment.MON_TransferredAmount
                            Else
                                oPayment.VB_ClientNo = "KID713"
                                dKID713Total = dKID713Total + oPayment.MON_TransferredAmount
                            End If
                        Next
                        ' alltid mottakerkonto 15033393897
                        oPayment.I_Account = "15033393897"
                    Next
                    ' batch completed; report VIS and SAP
                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(5, lCounter)
                    aDoc(0, lCounter) = ""
                    aDoc(1, lCounter) = "Visma bunt:"
                    aDoc(2, lCounter) = "       -"
                    aDoc(3, lCounter) = oBatch.REF_Bank
                    aDoc(4, lCounter) = "&R" & Format(dKID_20Total / 100, "##,##0.00")

                    lCounter = lCounter + 1
                    ReDim Preserve aDoc(5, lCounter)
                    aDoc(0, lCounter) = ""
                    aDoc(1, lCounter) = "SAP bunt:"
                    aDoc(2, lCounter) = "       -"
                    aDoc(3, lCounter) = oBatch.REF_Bank
                    aDoc(4, lCounter) = "&R" & Format(dKID713Total / 100, "##,##0.00")

                Next
            Next


            ' Send to report 951
            '-------------------
            'oReport = CreateObject ("vbbabel.Report")
            'oBabelReport = CreateObject ("vbbabel.BabelReport")
            'oReport.ReportNo = "951"  'Special report rp_951_Array5, with 5 fields = 5 array elements
            'oReport.Heading = "OCR bunter"
            'oReport.Preview = True
            'oReport.Printer = False
            'oReport.ExportType = "PDF"
            'sTmp = Left$(sFilenameOut, InStrRev(sFilenameOut, "\") - 1) & "\OCRReport_" & Date.Today & ".pdf"
            'oReport.ExportFilename = sTmp

            'oReport.DetailLevel = 1
            ''oBabelReport.ReportArray = aDoc
            'oBabelReport.Report = oReport
            'oBabelReport.BabelFiles = oBabelFiles
            'oBabelReport.Start()

            oReport = Nothing
            oBabelReport = Nothing

            bReturnValue = True

        Catch ex As Exception

            bReturnValue = False

        End Try

        Return bReturnValue
    End Function
    Public Function TreatKlaveness(ByRef oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' added 21.02.2017
        ' check if //BL or //FW and redo
        ' use IBAN/BIC only for //BL
        ' BIC only for //FW ???

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If Mid(oPayment.BANK_Name, 1, 2) = "//" Then
                            Select Case oPayment.BANK_CountryCode
                                Case "US"
                                    ' skip fedwire
                                    ' //FW in name, shift rest "up" from bank_adr
                                    ' but do this only if swift is set
                                    If Not EmptyString(oPayment.BANK_SWIFTCode) Then
                                        oPayment.BANK_Name = oPayment.BANK_Adr1
                                        oPayment.BANK_Adr1 = oPayment.BANK_Adr2
                                        oPayment.BANK_Adr2 = oPayment.BANK_Adr3
                                        oPayment.BANK_Adr3 = ""
                                        oPayment.BANK_BranchNo = ""
                                        oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT
                                    End If

                                Case "DE" 'BLZ
                                    ' Skip blz, keep bic and iban
                                    ' //BL in name, shift rest "up" from bank_adr
                                    oPayment.BANK_Name = oPayment.BANK_Adr1
                                    oPayment.BANK_Adr1 = oPayment.BANK_Adr2
                                    oPayment.BANK_Adr2 = oPayment.BANK_Adr3
                                    oPayment.BANK_Adr3 = ""
                                    oPayment.BANK_BranchNo = ""
                                    oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SWIFT

                            End Select

                        End If 'If InStr(oPayment.BANK_Name, "//") > 0 Then
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatKlaveness = True

    End Function

    Public Function Treat_SkagerakISO20022(ByRef oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' added 29.03.2017
        ' change from NKR to NOK

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        ' kontonummer mottaker er de 4 f�rste + de 7 siste siffer fra LB filen
                        ' 27.08.2018 -
                        ' av en eller annen grunn g�r det feil hos Skagerak - vi har derfor fjernet de tre linjene under.
                        '' kontonummer mottaker er de 4 f�rste + de 7 siste siffer fra LB filen
                        'If oPayment.E_Account <> "" Then
                        '    oPayment.E_Account = Left(oPayment.E_Account, 4) & Right(oPayment.E_Account, 7)
                        'End If
                        If oPayment.MON_InvoiceCurrency = "NKR" Then
                            oPayment.MON_InvoiceCurrency = "NOK"
                            oPayment.MON_TransferCurrency = "NOK"
                        End If
                        ' og for returfiler, gj�r om tilbake til NKR
                        If oPayment.StatusCode <> "00" Then
                            If oPayment.MON_InvoiceCurrency = "NOK" Then
                                oPayment.MON_InvoiceCurrency = "NKR"
                                oPayment.MON_TransferCurrency = "NKR"
                            End If
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_SkagerakISO20022 = True

    End Function
    Public Sub TreatScandicInnbetalingExport(ByVal oBabelFiles As vbBabel.BabelFiles)

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched Then
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.MATCH_Final Then
                                    If oInvoice.MATCH_ID = "X" Or Left(oInvoice.MATCH_ID, 2) = "15" Or Left(oInvoice.MATCH_ID, 4) = "9999" Then
                                        ' 29.03.2017 - Teller (1503, 1507) og Bankaxept (1504) f�res n� rett til hovedbok som AutoBooking
                                        If oInvoice.MATCH_ID <> "1503" And oInvoice.MATCH_ID <> "1504" And oInvoice.MATCH_ID <> "1507" Then
                                            If oInvoice.MATCH_ID = "X" Or Left(oInvoice.MATCH_ID, 4) = "9999" Then
                                                oInvoice.InvoiceNo = ""
                                                oInvoice.MATCH_ID = ""
                                            End If
                                            ' 03.02.2017 - remove also for aKonto
                                            If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                                                oInvoice.InvoiceNo = ""
                                                oInvoice.MATCH_ID = ""
                                            End If
                                        End If
                                    End If
                                    ' 31.03.2017 - added next for aKonto
                                    ' do not use Match_Id for aKonto, gives problems in exportfile
                                    If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                                        oInvoice.MATCH_ID = ""
                                    End If
                                End If
                            Next
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

    End Sub
    Public Function Treat_ScandicNattsafe(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' redo NATTSAFE payments.
        ' find Ref_Bank2, and take date from here into Date_Value
        Dim stmp As String = ""

        Try
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.Freetexts.Count > 0 Then
                                If oInvoice.Freetexts(1).Text = "NATTSAFE" Then
                                    If oPayment.REF_Bank2.Length = 9 And IsNumeric(oPayment.REF_Bank2) Then
                                        stmp = oPayment.REF_Bank2
                                        ' add to freetext

                                        oInvoice.Freetexts(1).Text = oInvoice.Freetexts(1).Text & " " & Left(stmp, 3) & " " & Mid(stmp, 4, 2) & "." & Mid(stmp, 6, 2) & "." & "20" & Mid(stmp, 8, 2)
                                    End If
                                End If
                            End If
                        Next
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch
            ' if problems with dateformat in Ref_Bank2 probably gets here.
            ' just resume without any errorwarning

        End Try

        Treat_ScandicNattsafe = True

    End Function
    Public Function Treat_FaroeBACS(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' IF BACS, then redo from IBAN to sortcode/BBAN

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If Left(oPayment.E_Account, 2) = "GB" And oPayment.MON_InvoiceCurrency = "GBP" And Len(oPayment.I_Account) = 8 Then

                            ' Must deduct BBAN account and sortcode from IBAN
                            ' UK IBAN:
                            ' (22) IBAN format: GBkk BBBB SSSS SSCC CCCC CC
                            ' B = alphabetical bank code, S = sort code (often a specific branch), C = account No.
                            oPayment.PayType = "M"
                            oPayment.DnBNORTBIPayType = "ACH"

                            ' adust account;
                            oPayment.BANK_BranchNo = Mid$(oPayment.E_Account, 9, 6)
                            oPayment.BANK_BranchType = vbBabel.BabelFiles.BankBranchType.SortCode
                            ' put BBAN in Account, last 8 digits
                            oPayment.E_Account = Right$(oPayment.E_Account, 8)
                            oPayment.BANK_AccountCountryCode = "GB"
                            oPayment.BANK_SWIFTCode = ""

                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_FaroeBACS = True
    End Function
    Public Function Treat_RemoveLBXACH_FromMT940(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim l As Long = 0
        Dim i As Integer = 0
        Dim nTotalInvoices As Double = 0

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For l = oBatch.Payments.Count To 1 Step -1
                        ' For ACH, these can be structured, if so, remove original, and set to structured paytype = 629
                        ' Check if we have more than one invoice, and oInvoice.Extra1 = "RMR"
                        ' and do a control on total against the first (original) invoice
                        If oBatch.Payments(l).DnBNORTBIPayType = "ACH" Then
                            If oBatch.Payments(l).Invoices.Count > 1 Then
                                If oBatch.Payments(l).Invoices(2).Extra1 = "RMR" Then
                                    oPayment = oBatch.Payments(l)
                                    nTotalInvoices = 0
                                    For i = 2 To oPayment.Invoices.Count
                                        nTotalInvoices = nTotalInvoices + oPayment.Invoices(i).MON_InvoiceAmount
                                    Next
                                    If nTotalInvoices = oPayment.Invoices(1).MON_InvoiceAmount Then
                                        ' Ok, total of structured invoices match the amount on original invoice.
                                        ' remove first invoice
                                        oPayment.Invoices.Remove(1)
                                        oPayment.PayCode = "629"
                                    End If

                                End If
                            End If
                        End If

                        If Not (oBatch.Payments(l).DnBNORTBIPayType = "ACH" Or oBatch.Payments(l).DnBNORTBIPayType = "LBX") Then
                            If oBatch.Payments(l).PayCode = "660" Then
                                ' Lockbox=660
                                ' ACH = 661
                                oBatch.Payments.Remove(l)

                            ElseIf oBatch.Payments(l).PayCode = "661" Then
                                ' also, test if CHR as part of freetext
                                If oBatch.Payments(l).Invoices(1).Freetexts.Count > 0 Then
                                    If InStr(oBatch.Payments(l).Invoices(1).Freetexts(1).Text, "CHR") > 0 Then
                                        ' ACH = 661
                                        oBatch.Payments.Remove(l)
                                    End If
                                End If
                            End If

                        End If

                    Next
                Next
            Next

            For Each oBabelFile In oBabelFiles
                ' remove empty batches
                For i = oBabelFile.Batches.Count To 1 Step -1
                    If oBabelFile.Batches(i).Payments.Count = 0 Then
                        oBabelFile.Batches.Remove(i)
                    End If
                Next
            Next

            For Each oBabelFile In oBabelFiles
                ' reindex
                ' then "reindex" the batch and payments inside
                i = 0
                For Each oBatch In oBabelFile.Batches
                    i = i + 1
                    oBatch.Index = i
                    oBatch.ReIndexPayments()
                Next oBatch

            Next

            ' 11.08.2017
            ' added next sequence
            ' StormGeo have problems with spaces in freetext, which are added by our MT940 import
            ' Remove double info for invoices like 17-03-12345
            Dim sTmp1 As String = ""
            Dim sTmp2 As String = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        For Each oInvoice In oPayment.Invoices
                            For Each oFreeText In oInvoice.Freetexts
                                ' remove spaces from freetext
                                If Not EmptyString(oFreeText.Text) Then
                                    ' remove spaces if numbers or - on each side of space
                                    sTmp1 = oFreeText.Text
                                    sTmp2 = ""
                                    For l = 1 To sTmp1.Length - 2
                                        If sTmp1.Substring(l, 1) = " " Then
                                            If IsNumeric(sTmp1.Substring(l - 1, 1)) Or sTmp1.Substring(l - 1, 1) = "-" Then
                                                If IsNumeric(sTmp1.Substring(l + 1, 1)) Or sTmp1.Substring(l + 1, 1) = "-" Then
                                                    ' remove that space
                                                    sTmp2 = oFreeText.Text.Remove(l, 1)
                                                End If
                                            End If
                                        End If
                                    Next
                                End If
                                If Not EmptyString(sTmp2) Then
                                    oFreeText.Text = sTmp2
                                End If
                            Next
                        Next
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_RemoveLBXACH_FromMT940 = True
    End Function
    Public Function Treat_VeidekkeIndustri(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFilesetup_ID As Integer) As Boolean
        ' 08.05.2017
        ' change accountno for EUR og USD payments

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.StatusCode = "00" Then
                            If oPayment.I_Account = "70500642198" Or oPayment.I_Account = "NO7670500642198" Or oPayment.I_Account = "NO4115062964984" Then
                                ' 25.11.2019 - NO4115062964984 is used in test only ????
                                If oPayment.MON_InvoiceCurrency = "USD" Then
                                    oPayment.I_Account = "12500544153"
                                End If
                                If oPayment.MON_InvoiceCurrency = "EUR" Then
                                    oPayment.I_Account = "50150447620"
                                End If
                            End If
                        Else
                            ' returnfiles
                            'If oPayment.MON_InvoiceCurrency = "USD" And oPayment.I_Account = "12500544153" Then
                            '    oPayment.I_Account = "70500642198"
                            'End If
                            'If oPayment.MON_InvoiceCurrency = "EUR" And oPayment.I_Account = "50150447620" Then
                            '    oPayment.I_Account = "70500642198"
                            'End If

                        End If
                    Next
                Next
            Next

            ' Then call general function to save data to BabelBanks database;
            TreatISO20022(oBabelFiles, iFilesetup_ID, True)

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_VeidekkeIndustri = True
    End Function

    Public Function Treat_StormGeoInc(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' 10.08.2017
        ' StormGeo have problems ablut same invoicenumber comes twice in freetext, and makes automatching difficult.
        ' Remove double info for invoices like 17-03-12345
        Dim sTmp1 As String = ""
        Dim sTmp2 As String = ""
        Dim l As Long

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        For Each oInvoice In oPayment.Invoices
                            For Each oFreeText In oInvoice.Freetexts
                                ' remove spaces from freetext
                                If Not EmptyString(oFreeText.Text) Then
                                    ' remove spaces if numbers or - on each side of space
                                    sTmp1 = oFreeText.Text
                                    sTmp2 = ""
                                    For l = 1 To sTmp1.Length - 1
                                        If sTmp1.Substring(l, 1) = " " Then
                                            If IsNumeric(sTmp1.Substring(l - 1, 1)) Or sTmp1.Substring(l, 1) = "-" Then
                                                If IsNumeric(sTmp1.Substring(l + 1, 1)) Or sTmp1.Substring(l + 1, 1) = "-" Then
                                                    ' remove that space
                                                    sTmp2 = oFreeText.Text.Remove(l, 1)
                                                End If
                                            End If
                                        End If
                                    Next

                                End If
                                oFreeText.Text = sTmp2
                            Next
                        Next
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_StormGeoInc = True
    End Function
    Public Function Treat_Flyktninghjelpen_Autogiro(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFileSetup_ID As Integer) As Boolean
        ' 21.09.2017
        ' change from 14 len kid to 8 len for use in Autogiro Sweden
        Dim oFileSetup As vbBabel.FileSetup
        oFileSetup = oBabelFiles.VB_Profile.FileSetups(iFileSetup_ID)

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        For Each oInvoice In oPayment.Invoices
                            ' 02.01.2018 - remove leading zeros
                            oInvoice.Unique_Id = RemoveLeadingCharacters(oInvoice.Unique_Id, "0")
                            ' 30.10.2017 - Save complete KID, to be used in reference field in Bankgirofile
                            oInvoice.InvoiceNo = oInvoice.Unique_Id

                            If oInvoice.Unique_Id.Length >= 8 Then
                                ' In Autogirofiles, the KID is righjustified - take last 8!
                                If oInvoice.Unique_Id.Substring(0, 8) = "00000000" Then
                                    oInvoice.Unique_Id = Right(oInvoice.Unique_Id, 8)
                                    ' For Autogiro, redo from paycode Autogiro to OCR
                                    oPayment.PayCode = "515"  ' OCR from Autogiro
                                Else
                                    ' BG Max, keep first 8 in KID;
                                    ' 03.11.2017 - Keep all 14
                                    'oInvoice.Unique_Id = oInvoice.Unique_Id.Substring(0, 8)
                                End If
                            End If

                            If oFileSetup.Format_ID = vbBabel.BabelFiles.FileType.OCR Then
                                ' 07.11.2017
                                ' set date to 28th in current month for all payments for files TO Bankgirot
                                oPayment.DATE_Payment = DateToString(DateSerial(Year(Now), Month(Now), "28"))
                            End If

                            ' 08.12.2017 
                            ' From Sweden we can receive both autogiro and bgmax.
                            ' If autogiro, redo to Avtalegiro, because the ERP at Flyktninghjelpen can not receive Autogiro in OCR-files (NY01)
                            If oPayment.PayCode = "611" Then
                                oPayment.PayCode = "670"
                            End If

                        Next

                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Flyktninghjelpen_Autogiro = True
    End Function
    Public Function Treat_IKEAKundeUtbetalinger(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFileSetup_ID As Integer) As Boolean
        ' 22.09.2017
        ' IKEA Returbetalinger til kunder
        ' - Sjekk kontonr for CDV
        ' - lage rapport for de med feil
        ' - fjerne de med feil
        ' - lag kontroll for om fil er lest inn f�r (beregn stikktall for innhold i filen)

        Dim oReport As vbBabel.Report
        Dim oBabelReport As vbBabel.BabelReport
        Dim bErrorMarked As Boolean = False
        Dim j As Integer = 0
        Dim bx As Boolean = False

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        If Not IsModulus11(oPayment.E_Account) Then
                            bErrorMarked = True
                            oPayment.ToSpecialReport = True
                            oPayment.Invoices(1).ToSpecialReport = True
                            ' do not export errormarked
                            oPayment.Exported = True
                        End If

                    Next
                Next
                ' create messageId to be stored in ImportedFiles. can be used to check for doubleimport
                oBabelFile.EDI_MessageNo = CreateMessageReference(oBabelFile)
                bx = CheckForDoubleImport(oBabelFile.VB_Profile.Company_ID, oBabelFile.VB_FileSetupID, oBabelFile.EDI_MessageNo, oBabelFile.BankByCode, oBabelFile.ImportFormat, oBabelFile.FilenameIn, False, False, Format(Now, "yyyyMMdd"))
                If Not bx Then
                    'The file has been previously imported

                    If MsgBox(Replace(Replace(LRS(16005, oBabelFile.EDI_MessageNo, Format(Now, "yyyyMMdd"), oBabelFile.FilenameIn), "%4", VB6.Format(StringToDate(oBabelFile.DATE_Production), "Long date")), "%5", VB6.Format(oBabelFile.MON_InvoiceAmount / 100, "##,##0.00")) & vbCrLf & vbCrLf & LRS(16006), MsgBoxStyle.YesNo + MsgBoxStyle.Question + MsgBoxStyle.DefaultButton2, LRS(16007)) = MsgBoxResult.Yes Then
                        'A file with identifier %1 was imported %2.
                        'Filename: %3
                        'Creation date of importfile: %4
                        'Total amount on importfile: %5
                        '
                        'Do You want to continue the import?
                        'OK, continue the import
                        Treat_IKEAKundeUtbetalinger = True
                    Else
                        Treat_IKEAKundeUtbetalinger = False
                        Exit Function
                    End If
                Else
                    Treat_IKEAKundeUtbetalinger = True
                End If

            Next

            If bErrorMarked Then
                ' activate report, call report 003, with Specialsetup
                oFilesetup = oBabelFiles.VB_Profile.FileSetups(iFileSetup_ID)
                For j = 1 To oFilesetup.Reports.Count
                    If oFilesetup.Reports(j).ReportNo = 3 Then ' Specialreport

                        oBabelReport = New vbBabel.BabelReport
                        oBabelReport.Special = oBabelFiles.Item(1).Special
                        oBabelReport.BabelFiles = oBabelFiles
                        oBabelReport.ReportOnSelectedItems = True
                        oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.Inside_SpecialRoutines
                        If Not oBabelReport.RunReports(oFilesetup.FileSetup_ID) Then
                            j = 0
                        End If

                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

                        Exit For
                    End If
                Next j
            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_IKEAKundeUtbetalinger = True

    End Function
    Public Function TreatNHSTInnbetalingerClientNo(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' 25.09.2017
        ' pre process before export, by changing vb_clientno based on content of MyField
        '
        'ClientNo	Code    Name
        'F14003	    TW          Trade Winds                                       
        'F14006     UP          Upstream
        'F14010	                NHST Asia                                         
        'F14015	    IFCO+flere  Intrafish Media                                   
        'F14032     RE          Recharge

        Dim sCode As String = ""
        Dim sClientNo As String = ""

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' 14.11.2017 - added next If
                        sClientNo = oPayment.VB_ClientNo
                        If sClientNo = "F14003" Or sClientNo = "F14006" Or sClientNo = "F14010" Or sClientNo = "F14015" Or sClientNo = "F14032" Then
                            For Each oInvoice In oPayment.Invoices
                                If xDelim(oInvoice.MyField, "|", 1) = "INFOSOFT" Then
                                    sCode = xDelim(oInvoice.MyField, "|", 2)

                                    ' set payments clientno based on companycode in MyField (companycode is fetched from Infosoft)
                                    Select Case sCode
                                        Case "TW"
                                            sClientNo = "F14003"
                                        Case "UP"
                                            sClientNo = "F14006"
                                        Case "IFCO", "IFNO", "FFI", "FNI", "SFP", "SID"
                                            sClientNo = "F14015"
                                        Case "RE"
                                            sClientNo = "F14032"
                                        Case Else
                                            ' rest to F14015
                                            sClientNo = "F14015"
                                    End Select
                                    ' assumes that in one payment, ALL invoices belong to same Company (client)
                                    ' 03.10.2019 - that is not true !!!!!!!!!
                                    oPayment.VB_ClientNo = sClientNo
                                    Exit For  ' take code from 1st invoice with Infosoft-productcode, and exit. assume that the rest are the same

                                End If
                            Next
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatNHSTInnbetalingerClientNo = True
    End Function
    ' 25.10.2017 New function
    Public Function Treat_ButikkDrift_Teller(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Fill in clientnumbers based on MerchantID
        Dim sClientNo As String = ""
        Dim i As Integer = 0
        Dim sTmp As String = ""
        Dim bReturnValue As Boolean

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    Select Case oBatch.I_Branch
                        Case "5096821"  ' Voss
                            sClientNo = "100"
                        Case "5107693"  ' KTSTavanger
                            sClientNo = "400"
                        Case "5096813"  ' Vestby
                            sClientNo = "600"
                        Case "5096730"  ' Trysil
                            sClientNo = "1200"
                    End Select
                    For Each oPayment In oBatch.Payments
                        oPayment.VB_ClientNo = sClientNo
                        'oPayment.I_Account = sClientNo & "00"
                        ' 08.12.2017 - add ClientNo to name;
                        oPayment.E_Name = oPayment.E_Name & "  - Avd. " & sClientNo & " - " & oBatch.I_Branch
                        ' Friser tekst, for � f� plass til mer i Manuell
                        For Each oInvoice In oPayment.Invoices
                            ' sett Qualifier = 5, to hide from Manual for some textlines;
                            For i = 1 To oInvoice.Freetexts.Count
                                If i = 1 Or i = 3 Or i = 4 Or i = 5 Or i = 7 Then
                                    oInvoice.Freetexts(i).Qualifier = 5
                                End If
                                If i = 2 Then
                                    '30.11.2018 - some invoices with 6 ony freetexts
                                    If oInvoice.Freetexts.Count > 6 Then
                                        ' Add SchemeName to start (VI, MS, etc)
                                        oInvoice.Freetexts(7).Text = Replace(oInvoice.Freetexts(7).Text, "CardType:", "").Trim
                                        Select Case Left(oInvoice.Freetexts(7).Text, 2)
                                            Case "VI"
                                                oInvoice.Freetexts(i).Text = "VISA  " & " " & oInvoice.Freetexts(2).Text & " " & oInvoice.Freetexts(5).Text
                                            Case "MC"
                                                oInvoice.Freetexts(i).Text = "MASTER" & " " & oInvoice.Freetexts(2).Text & " " & oInvoice.Freetexts(5).Text
                                            Case "EU"
                                                oInvoice.Freetexts(i).Text = "EURO  " & " " & oInvoice.Freetexts(2).Text & " " & oInvoice.Freetexts(5).Text
                                            Case "AM"
                                                oInvoice.Freetexts(i).Text = "AMEX  " & " " & oInvoice.Freetexts(2).Text & " " & oInvoice.Freetexts(5).Text
                                            Case Else
                                                oInvoice.Freetexts(i).Text = oInvoice.Freetexts(7).Text & " " & oInvoice.Freetexts(2).Text & " " & oInvoice.Freetexts(5).Text
                                        End Select
                                    End If
                                End If
                                If i = 6 Then
                                    ' added next if 01.10.2018
                                    If oInvoice.Freetexts.Count > 6 Then
                                        oInvoice.Freetexts(i).Text = Replace(oInvoice.Freetexts(i).Text, "PurchaseDate", "Dato/tid")
                                        ' 08.12.2017 - We have problems when Teller is one day late.
                                        ' Must then adjust paymentdate according to date in specification;
                                        sTmp = oInvoice.Freetexts(i).Text
                                        sTmp = Mid(sTmp, InStr(sTmp, "Dato/tid: ") + 10) ' find date after Dato/tid
                                        sTmp = Replace(Left(sTmp, 10), "-", "")   ' take datepart, and remove -
                                        If oPayment.DATE_Value <> sTmp Then
                                            ' swap date
                                            ' 03.01.2018 - DO NOT SWAP DATES. Det �delegger mer enn det gavner!!!
                                            'oPayment.DATE_Value = sTmp
                                        End If

                                        ' 08.12.2017:  2 spaces before time (time is important in manual)
                                        oInvoice.Freetexts(i).Text = Replace(oInvoice.Freetexts(i).Text, "T", "  ")
                                    End If
                                End If
                            Next
                        Next

                        ' Kan vi sortere Invoice etter bel�p?
                        Dim aTempSort() As Object
                        Dim oTempPayment As vbBabel.Payment
                        'Dim oInvoice As vbBabel.Invoice
                        Dim oTempInvoice As vbBabel.Invoice
                        Dim nCounter As Integer
                        Dim nOrigIndex As Integer


                        ' Fill array aTempSort with indexes and amounts;
                        ReDim aTempSort(oPayment.Invoices.Count - 1)
                        nCounter = -1
                        For Each oInvoice In oPayment.Invoices
                            nCounter = nCounter + 1
                            aTempSort(nCounter) = PadLeft(oInvoice.MON_InvoiceAmount.ToString, 15, "0") & "|" & oInvoice.Index.ToString
                        Next
                        ' Do the actual sorting of the array
                        ' ----------------------------------
                        If aTempSort.GetUpperBound(0) > 0 Then
                            ' sort array with built in sorting
                            aTempSort.Sort(aTempSort)
                            'If bSortDescending1 Then
                            '    ' Descended sorting - only on first element !
                            '    aTempSort.Reverse(aTempSort)
                            'End If
                        End If

                        oTempPayment = New vbBabel.Payment
                        nCounter = 0
                        ' Add to tempbatch all payments
                        For Each oInvoice In oPayment.Invoices
                            oTempInvoice = oTempPayment.Invoices.VB_AddWithObject(oInvoice)
                        Next oInvoice

                        ' Faster way to empty oPayment.Invoices
                        oPayment.Invoices = Nothing
                        oPayment.Invoices = New vbBabel.Invoices

                        If oTempPayment.Invoices.Count > 0 Then
                            ' Reorganize original payment according to array aTempSort
                            For nCounter = 0 To aTempSort.GetUpperBound(0)
                                nOrigIndex = xDelim(aTempSort(nCounter), "|", 2)  ' Index from Invoice as second part of array
                                oTempInvoice = oTempPayment.Invoices(nOrigIndex)
                                ' Must update index according to new sequence
                                oTempInvoice.Index = nCounter + 1
                                oInvoice = oPayment.Invoices.VB_AddWithObject(oTempInvoice)
                            Next nCounter
                        End If
                        nCounter = 0

                    Next
                Next
            Next
            bReturnValue = True

        Catch ex As Exception

            bReturnValue = False
        End Try

        Return bReturnValue

    End Function
    ' 10.11.2017 New function
    ' 07.10.2021 changed name of function, and added POP treatment
    Public Function TreatAeroGulf_Send(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Change to charges OUROUR for USD payments
        Dim bReturnValue As Boolean

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.MON_InvoiceCurrency = "USD" Then
                            ' change charges to OUROUR
                            oPayment.MON_ChargeMeAbroad = True
                            oPayment.MON_ChargeMeDomestic = True
                        End If

                        ' 07.10.2021
                        ' If present, add POP code to first invoice in each payment
                        ' Fetched from Ramco field Userdef_1, and used for payments to Malaysia
                        If Not EmptyString(oPayment.ExtraD1) Then
                            If oPayment.Invoices.Count > 0 Then
                                oPayment.Invoices(1).Freetexts(1).Text = oPayment.ExtraD1 & " / " & oPayment.Invoices(1).Freetexts(1).Text
                            End If
                        End If

                    Next
                Next
            Next

            bReturnValue = True

        Catch ex As Exception

            bReturnValue = False

        End Try

        Return bReturnValue

    End Function
    Public Function Treat_Nettleie_TilOCR(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFileSetup_ID As Integer) As Boolean
        ' For str�msalgselskaper som m� betale nettleie med OCR pr kunde
        ' Import DirRem
        ' Export DirRem AND OCR

        Dim bReturn As Boolean = True
        Dim i As Integer, j As Integer
        Dim aAccountsList() As String  ' List with accounts to exclude from DirRem, and transfer to OCR
        Dim nTotalAmount As Double = 0
        Dim oTempPayment As vbBabel.Payment
        Dim oNewPayment As vbBabel.Payment
        Dim bFound As Boolean = False

        Try

            For Each oBabelFile In oBabelFiles
                For Each oFilesetup In oBabelFile.VB_Profile.FileSetups
                    For Each oClient In oFilesetup.Clients
                        For Each oaccount In oClient.Accounts

                            ' sjekk om kontoen er lagt inn f�r;
                            bFound = False
                            If Not aAccountsList Is Nothing Then
                                For i = 0 To aAccountsList.GetUpperBound(0)
                                    If aAccountsList(i) = oaccount.Account Then
                                        bFound = True
                                        Exit For
                                    End If
                                Next
                            End If
                            If Not bFound Then
                                If aAccountsList Is Nothing Then
                                    ReDim aAccountsList(0)
                                Else
                                    ReDim Preserve aAccountsList(aAccountsList.GetUpperBound(0) + 1)
                                End If
                                aAccountsList(aAccountsList.GetUpperBound(0)) = oaccount.Account
                            End If
                        Next oaccount
                    Next oClient
                Next oFilesetup
            Next
            ' gjenst�r:
            ' - lage samlebetalingspost for de som skal til OCR

            For i = 0 To aAccountsList.GetUpperBound(0)
                ' Run through each account in aAccountslist
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches

                        For Each oPayment In oBatch.Payments
                            nTotalAmount = 0
                            ' mark payments which will not be exported to DirRem file;
                            If oPayment.E_Account = aAccountsList(i) And oPayment.MATCH_MyField <> "DIRREM" Then  ' do not concider the already totalled payments
                                ' set .Cancel = True to exclude them from DirRem export
                                oPayment.Cancel = True
                                oPayment.MATCH_MyField = "OCR"
                                oPayment.MON_TransferredAmount = oPayment.MON_InvoiceAmount
                                nTotalAmount = nTotalAmount + oPayment.MON_InvoiceAmount
                                For Each oInvoice In oPayment.Invoices
                                    oInvoice.MON_TransferredAmount = oInvoice.MON_InvoiceAmount
                                Next
                                ' add new ClientNo to this payment. Clients must be set in ClientSetup
                                oClient = FindClient(oBabelFile.VB_Profile, iFileSetup_ID, oPayment.E_Account, "", 1)
                                oPayment.VB_ClientNo = oClient.ClientNo
                            End If
                            If nTotalAmount > 0 Then
                                ' add a totalpayment for all OCR-payments for this account, in this batch
                                oTempPayment = oPayment
                                ' Add payment to new batch
                                oNewPayment = oBatch.Payments.Add
                                ' Copy payment
                                CopyPaymentObject(oTempPayment, oNewPayment)
                                oNewPayment.Index = oPayment.Index + 1
                                'oPayment = oBatch.Payments.VB_AddWithObject(oNewPayment)
                                oNewPayment.MON_InvoiceAmount = nTotalAmount
                                oNewPayment.MON_TransferredAmount = nTotalAmount
                                oNewPayment.MATCH_MyField = "DIRREM"
                                oNewPayment.Cancel = False
                                ' remove all invoices except first
                                For j = oPayment.Invoices.Count To 2 Step -1
                                    oNewPayment.Invoices.Remove(j)
                                Next
                                ' update first invoice;
                                oNewPayment.Invoices(1).MON_InvoiceAmount = nTotalAmount
                                oNewPayment.Invoices(1).MON_TransferredAmount = nTotalAmount
                                ' Due to "tvungen KID" - keep first Unique_ID ?????

                            End If
                        Next
                    Next
                Next

            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Nettleie_TilOCR = bReturn

    End Function
    Public Function Treat_Fjellinjen_Vipps(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Change running number (in Ref_Own) from Vipps OCR
        Dim sTmp As String
        Dim bReturnValue As Boolean

        Try
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        sTmp = DateToString(Date.Today)
                        ' last part is running number
                        oPayment.REF_Own = Left(oPayment.REF_Own, 5) & Right(sTmp, 5)
                    Next
                Next
            Next

            bReturnValue = True
        Catch ex As Exception

            bReturnValue = False

        End Try

        Return bReturnValue

    End Function
    Public Function Treat_Text_To_Name(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Move from Text_E_Statement to E_Name
        Dim sTmp As String
        Dim bReturnValue As Boolean

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.E_Name = "" Then
                            oPayment.E_Name = oPayment.Text_E_Statement
                        End If
                    Next
                Next
            Next

            bReturnValue = True

        Catch ex As Exception

            bReturnValue = False

        End Try

        Return bReturnValue

    End Function
    ' XNET 26.02.2013 - added function  Jernia
    Public Function Treat_Skeidar(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        '
        ' Redo from unstructured to structured

        Dim bRetValue As Boolean
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sI_CountryCodeDebitBank As String
        Dim i As Integer
        Dim sTmp As String

        Try

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        For Each oInvoice In oPayment.Invoices
                            ' If structured (invoiceno or kid present), remove freetext
                            If Trim(oInvoice.InvoiceNo) = "" And Trim(oInvoice.Unique_Id) = "" And Trim(oInvoice.CustomerNo) = "" Then
                                ' create structured based on freetext
                                sTmp = ""
                                For Each oFreeText In oInvoice.Freetexts
                                    sTmp = sTmp & oFreeText.Text & " "
                                Next oFreeText

                                ' extract InvoiceNo - from f.ex. INVOICE: TRV0102865, DATE: 10.09.2017
                                If InStr(sTmp, "INVOICE:") > 0 Then
                                    sTmp = xDelim(sTmp, ",", 1)
                                    sTmp = Replace(sTmp, "INVOICE:", "").Trim
                                End If
                                ' move to invoiceno
                                oInvoice.InvoiceNo = Left$(sTmp, 35) ' default max len = 35
                                oPayment.Structured = True
                            ElseIf oPayment.PayType = "I" And EmptyString(oInvoice.Unique_Id) = False Then
                                ' for International, Skeidar may have set KID! - redo to structured InvoiceNo
                                oInvoice.InvoiceNo = oInvoice.Unique_Id
                                oInvoice.Unique_Id = ""
                                oPayment.Structured = True
                            End If
                            ' remove freetext, both for structured and unstructured
                            If oInvoice.Freetexts.Count > 0 Then
                                For i = oInvoice.Freetexts.Count To 1 Step -1
                                    oInvoice.Freetexts.Remove(i)
                                Next i
                            End If
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Skeidar = bRetValue

    End Function

    Public Function Treat_Karona(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Imports svensk lev.betfil  - Telepay as export
        ' Redo to dometsic
        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        If oPayment.MON_InvoiceCurrency = "NOK" Then
                            oPayment.PayType = "D"
                            oPayment.BANK_CountryCode = "NO"
                        End If
                        ' kontonummer mottaker kan v�re for langt, begrens til 11
                        If oPayment.PayType = "D" Then
                            oPayment.E_Account = Left(oPayment.E_Account, 11)
                        End If

                        ' * i starten av melding = KID,
                        ' ellers strukturert fakturanr
                        For Each oInvoice In oPayment.Invoices
                            If Left(oInvoice.Freetexts(1).Text, 1) = "*" Then
                                oInvoice.Unique_Id = Mid(oInvoice.Freetexts(1).Text, 2).Trim
                            Else
                                oInvoice.InvoiceNo = oInvoice.Freetexts(1).Text.Trim
                            End If
                            oInvoice.Freetexts(1).Text = ""
                        Next
                    Next oPayment
                Next oBatch

            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Karona = bRetValue

    End Function
    Public Function Treat_Lindorff_DK_Cremul(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Imports danske Cremul - As Generic text
        ' Change FIK 19 digits with 4 leading zeros to 15 digits, without leading zeros
        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                ' manipulate generic text;
                sTmp = oBabelFile.GenericText

                ' traverse through the generic text, and replace "0000" with ""
                ' this is for DOC+999+0000048267241000552:
                ' But need to take away all crlfs first
                sTmp = sTmp.Replace(vbCr, "")
                sTmp = sTmp.Replace(vbLf, "")

                ' then replace
                sTmp = sTmp.Replace("DOC+999+0000", "DOC+999+")

                ' cut in 80/80 with crlf between

                ' then out into object
                oBabelFile.GenericText = sTmp

            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Lindorff_DK_Cremul = bRetValue

    End Function

    Public Function Treat_QSC(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Imports svensk lev.betfil  - Telepay as export (QSC - McDonalds)
        ' Is a domestic LB file, no name for receiver
        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.MON_InvoiceCurrency = "" Then
                            oPayment.MON_InvoiceCurrency = "NOK"
                            oPayment.BANK_CountryCode = "NO"
                            oPayment.PayType = "D"
                        End If
                        If oPayment.MON_InvoiceCurrency = "SEK" And oPayment.PayType = "D" Then
                            oPayment.MON_InvoiceCurrency = "NOK"
                            oPayment.BANK_CountryCode = "NO"
                        End If
                        ' kontonummer mottaker kan v�re for langt, begrens til 11
                        ' bruk 4 f�rste + 7 siste i kontonummer
                        If oPayment.PayType = "D" Then
                            oPayment.E_Account = Left(oPayment.E_Account, 4) & Right(oPayment.E_Account, 7)
                        End If

                        ' Ingen navn p� mottaker, legg egenref:
                        If oPayment.E_Name = "" Then
                            'oPayment.E_Name = "Mottaker: " & oPayment.E_Account
                            oPayment.E_Name = "Ref: " & oPayment.REF_Own
                        End If

                        ' * i starten av melding = KID,
                        ' ellers strukturert fakturanr
                        For Each oInvoice In oPayment.Invoices
                            If Left(oInvoice.Freetexts(1).Text, 1) = "*" Then
                                oInvoice.Unique_Id = Mid(oInvoice.Freetexts(1).Text, 2).Trim
                            Else
                                oInvoice.InvoiceNo = oInvoice.Freetexts(1).Text.Trim
                            End If
                            oInvoice.Freetexts(1).Text = ""
                        Next

                        '23.06.2021 - Talked with Anna-Carin Bladin, do not merged. Not even creditnotes.
                        'oPayment.MergeCriteria = oPayment.I_Account & oPayment.MON_InvoiceCurrency & oPayment.Invoices(1).SupplierNo & oPayment.BANK_SWIFTCode & oPayment.BANK_BranchNo & Chr(34) & "/d" & oPayment.DATE_Payment
                        'oPayment.MergeThis = True
                    Next oPayment
                Next oBatch

                '23.06.2021 - Talked with Anna-Carin Bladin, do not merged. Not even creditnotes.
                '' added 31.05.2018
                'oBabelFile.Visma_Merge(True)
                '' her f�r vi merget creditnotes dersom LB filesn produksjonsdato = forfallsdato for en "positiv" betaling
                'oBabelFile.Visma_MergeCreditNotes()

            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_QSC = bRetValue

    End Function
    Public Function TreatTelenorGlobalSplit(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Fill in clientnumbers based on content in Ref_Own

        On Error GoTo TelenorErr

        For Each oBabelFile In oBabelFiles
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    ' Supplier payments start with 2
                    ' Customer payments start with 1 or 7
                    If Left(oPayment.REF_Own, 1) = "1" Or Left(oPayment.REF_Own, 1) = "7" Then
                        oPayment.VB_ClientNo = "CUSTOMER"
                    Else
                        ' if not starting with 1 or 7, will be classified as Supplier !!! supplier payments most common ?
                        oPayment.VB_ClientNo = "SUPPLIER"
                    End If
                Next
            Next
        Next

        TreatTelenorGlobalSplit = True
        Exit Function

TelenorErr:
        TreatTelenorGlobalSplit = False

    End Function
    Public Function Treat_ABAXInvoiceNo(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' For ABAX Sweden, structured invoicenos can consist of more info than invoiceno
        ' Try to trim down to invoiceno
        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        For Each oInvoice In oPayment.Invoices
                            'If Mid(oInvoice.InvoiceNo, 7, 1) = " " Then
                            '    oInvoice.InvoiceNo = Left(oInvoice.InvoiceNo, 6)
                            'ElseIf Mid(oInvoice.InvoiceNo, 8, 1) = " " Then
                            '    oInvoice.InvoiceNo = Left(oInvoice.InvoiceNo, 7)
                            'End If
                            ' 12.10.2020 - if len 7, then cut to 6, to real invoice no
                            ' 21.10.2020 - det kan ogs� v�re tekst i .InvoiceNo - fjern f�rst dette
                            oInvoice.InvoiceNo = KeepNumericsOnly(oInvoice.InvoiceNo)
                            ' behold s� kun de 6 f�rste
                            oInvoice.InvoiceNo = Left(oInvoice.InvoiceNo, 6)
                            'If Len(oInvoice.InvoiceNo) = 7 Then
                            '    oInvoice.InvoiceNo = Left(oInvoice.InvoiceNo, 6)
                            'End If
                        Next
                    Next oPayment
                Next oBatch

            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_ABAXInvoiceNo = bRetValue

    End Function
    Public Function TreatDNBHakon(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Change from freetext incoming payment to OCR-payment
        ' H�kon Belt for NAV 22.10.2018

        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oFreetext As vbBabel.Freetext
        Dim bReturnValue As Boolean
        Dim sTmpAcc As String = ""
        Dim i As Long = 0
        Dim lCounter As Long = 0

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        ' Eksporter KUN for konto 82760100435 og 
                        If oPayment.E_Account = "82760100435" Or oPayment.E_Account = "82760300256" Then
                            oPayment.PayCode = "510"

                            ' add clientno, as the 3 last from account
                            oPayment.VB_ClientNo = Right(oPayment.E_Account, 3)
                            ' vi m� gj�re om fra E_Account til I_Account , og omvendt!
                            sTmpAcc = oPayment.E_Account
                            oPayment.E_Account = oPayment.I_Account
                            oPayment.I_Account = sTmpAcc

                            For Each oInvoice In oPayment.Invoices
                                oInvoice.Unique_Id = oInvoice.InvoiceNo
                                If oInvoice.Freetexts.Count > 0 Then
                                    oInvoice.Freetexts(1).Text = ""
                                Else
                                    oPayment.PayCode = "510"  ' skal ikke komme hit !!!!
                                End If
                            Next
                        Else
                            oPayment.Exported = True
                            lCounter = lCounter + 1
                        End If
                    Next

                    ' Delete the "exported" payments;
                    For i = oBatch.Payments.Count To 1 Step -1
                        oPayment = oBatch.Payments(i)
                        If oPayment.Exported = True Then
                            oBatch.Payments.Remove(i)
                        End If
                    Next i
                    ' then "reindex" the batch and payments inside
                    oBatch.ReIndexPayments()
        
                Next

            Next
            bReturnValue = True

        Catch ex As Exception

            bReturnValue = False

        End Try

        Return bReturnValue

    End Function
    Public Function Treat_ABAX_SwedenOutgoing(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Imports Telepay domestic - exports Pain.001
        ' redo from NOK to SEK
        ' test bankgiro, postgiro, bankaccounts

        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.MON_InvoiceCurrency = "SEK"
                        oPayment.MON_TransferCurrency = "SEK"
                        oPayment.BANK_CountryCode = "SE"
                        oPayment.PayType = "D"

                        ' Determine type of account;
                        ' strip leading 0
                        oPayment.E_Account = Replace(RemoveLeadingCharacters(oPayment.E_Account, "0"), "-", "")
                        If Len(oPayment.E_Account) > 8 Then
                            oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount
                            ' HOW TO CHECK IF POSTGIRO ?????
                        Else
                            oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_ABAX_SwedenOutgoing = bRetValue

    End Function
    Public Function Treat_ABAX_FinlandOutgoing(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Imports Telepay domestic - exports Pain.001
        ' redo from NOK to EUR

        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.MON_InvoiceCurrency = "EUR"
                        oPayment.MON_TransferCurrency = "EUR"
                        oPayment.BANK_CountryCode = "FI"
                        oPayment.PayType = "D"
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_ABAX_FinlandOutgoing = bRetValue

    End Function
    Public Function Treat_ABAX_PolandOutgoing(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Imports Telepay domestic - exports Pain.001
        ' Remove BIC if = X

        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.BANK_SWIFTCode = "X" Then
                            oPayment.BANK_SWIFTCode = ""
                        End If
                        If oPayment.BANK_SWIFTCodeCorrBank = "X" Then
                            oPayment.BANK_SWIFTCodeCorrBank = ""
                        End If

                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_ABAX_PolandOutgoing = bRetValue

    End Function
    Public Function Treat_ABAX_DenmarkOutgoing(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Imports Telepay domestic - exports Pain.001
        ' redo from NOK to DKK
        ' always domestic
        ' check for FIK, and split correctly

        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        ' 08.06.2020:
                        ' Also International payments from Denmark.
                        ' Must decide which ones are domestic and int.
                        'oPayment.MON_InvoiceCurrency = "DKK"
                        'oPayment.MON_TransferCurrency = "DKK"
                        'oPayment.BANK_CountryCode = "DK"
                        If oPayment.MON_InvoiceCurrency = "DKK" And oPayment.BANK_CountryCode = "DK" Then
                            oPayment.PayType = "D"
                        Else
                            oPayment.PayType = "I"
                        End If

                        ' FIK or not?
                        If oPayment.PayType = "D" Then  ' 08.06.2020 added If
                            For Each oInvoice In oPayment.Invoices
                                If Len(oInvoice.Unique_Id) > 0 Then
                                    sTmp = oInvoice.Unique_Id
                                    oPayment.E_Account = SplitFI(sTmp, 1)
                                    oInvoice.Unique_Id = SplitFI(sTmp, 2)

                                    ' 03.04.2019 - Must set paycode = FIK !!!
                                    Select Case Left(oInvoice.Unique_Id, 2)
                                        Case "04"
                                            oPayment.PayCode = "304"
                                        Case "71"
                                            oPayment.PayCode = "371"
                                        Case "73"
                                            oPayment.PayCode = "373"
                                        Case "75"
                                            oPayment.PayCode = "375"
                                    End Select
                                End If
                            Next
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_ABAX_DenmarkOutgoing = bRetValue

    End Function
    Public Function Treat_ABAX_DenmarkOutgoingReturn(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' 18.11.2020 - Must redo returnfiles to Int, to create Betfor01-04
        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.PayType = "I"
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_ABAX_DenmarkOutgoingReturn = bRetValue

    End Function
    Public Function Treat_NavamedicOutgoing(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Imports Telepay - exports Pain.001
        ' If Sweden domestic, test bankgiro, postgiro, bankaccounts
        ' Remove spaces in account no

        Dim bRetValue As Boolean
        Dim sTmp As String

        Dim oTempInvoices As vbBabel.Invoices
        Dim oTmpPayment As vbBabel.Payment
        Dim iInvoiceCounter As Integer = 0
        Dim iTmpInvoiceCounter As Integer = 0

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.E_Account = Replace(oPayment.E_Account, " ", "")
                        oPayment.PayTypeSetInImport = False

                        If oPayment.BANK_CountryCode = "SE" And oPayment.MON_InvoiceCurrency = "SEK" Then
                            If IsPaymentDomestic(oPayment) Then
                                ' Determine type of account;
                                ' First- test for - in next last as Plusgiro (1234567-8)
                                If Mid(oPayment.E_Account, Len(oPayment.E_Account) - 1) = "-" Then
                                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_PlusGiro
                                End If
                                ' strip leading 0 and -
                                oPayment.E_Account = Replace(RemoveLeadingCharacters(oPayment.E_Account, "0"), "-", "")
                                If Len(oPayment.E_Account) > 8 Then
                                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankaccount
                                Else
                                    oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_Bankgiro
                                    ' Check for Postgiro, by checking Melding til mottaker
                                    ' 23.12.2020
                                    ' Check if freetext starts with P-, then Postgiro
                                    If Not oPayment.Invoices(1).Freetexts Is Nothing Then
                                        If oPayment.Invoices(1).Freetexts.Count > 0 Then
                                            If Left(oPayment.Invoices(1).Freetexts(1).Text, 2) = "P-" Then
                                                oPayment.E_AccountType = vbBabel.BabelFiles.AccountType.SE_PlusGiro
                                                oPayment.Invoices(1).Freetexts(1).Text = Mid(oPayment.Invoices(1).Freetexts(1).Text, 3)
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        If oPayment.BANK_CountryCode = "DK" And oPayment.MON_InvoiceCurrency = "DKK" Then
                            ' check for FIK71.
                            For Each oInvoice In oPayment.Invoices
                                If Len(oInvoice.Unique_Id) > 20 And Left(oInvoice.Unique_Id, 3) = "+71" Then
                                    ' +71<001387446004285+86446987<
                                    ' strip Shit
                                    oInvoice.Unique_Id = KeepNumericsOnly(oInvoice.Unique_Id)
                                    oInvoice.Unique_Id = Left(oInvoice.Unique_Id, 17)
                                    oPayment.PayCode = 371
                                End If
                            Next

                            ' split into several payments if there are more than one FIK in a payment;
                            If IsFIK(oPayment.PayCode) And oPayment.Invoices.Count > 1 And oPayment.MATCH_MyField3 <> "KIDsplit" Then
                                oTempInvoices = oPayment.Invoices
                                'oPayment.Invoices = Nothing
                                'oPayment.MATCH_MyField3 = "KIDorig"  ' set a tmp-mark on involved payments
                                oTmpPayment = Nothing
                                ' first add new payments as many as there are invoices
                                For iInvoiceCounter = oTempInvoices.Count To 2 Step -1
                                    oTmpPayment = New vbBabel.Payment
                                    CopyPaymentObject(oPayment, oTmpPayment)
                                    'oTmpPayment = Payments.Add
                                    oTmpPayment = oBatch.Payments.VB_AddWithObject(oTmpPayment)
                                    oTmpPayment.MATCH_MyField3 = "KIDsplit"  ' set a tmp-mark on involved payments
                                    'oTmpPayment = Nothing

                                    ' keep one invoice only
                                    ' do not treat the first one
                                    For iTmpInvoiceCounter = oTmpPayment.Invoices.Count To 1 Step -1
                                        ' mark for later delete all invoices except from the one we treat in the outer for/next
                                        If iInvoiceCounter <> iTmpInvoiceCounter Then
                                            'oTmpPayment.Invoices.Remove(iTmpInvoiceCounter)
                                            oTmpPayment.Invoices(iTmpInvoiceCounter).Cancel = True
                                        Else
                                            ' update amounts on "newly added" payment
                                            oTmpPayment.MON_InvoiceAmount = oTmpPayment.Invoices(iTmpInvoiceCounter).MON_InvoiceAmount
                                            oTmpPayment.MON_TransferredAmount = oTmpPayment.Invoices(iTmpInvoiceCounter).MON_TransferredAmount

                                        End If
                                    Next
                                Next

                                ' then mark to delete all invoices apart from the first one in "original" FIK payment
                                For iTmpInvoiceCounter = oPayment.Invoices.Count To 2 Step -1
                                    oPayment.Invoices(iTmpInvoiceCounter).Cancel = True
                                Next

                            End If
                        End If
                    Next oPayment

                    ' Finally, delete all invoices marked with Cancel = True
                    For Each oPayment In oBatch.Payments
                        If oPayment.BANK_CountryCode = "DK" And oPayment.MON_InvoiceCurrency = "DKK" Then
                            If IsFIK(oPayment.PayCode) Then
                                For iInvoiceCounter = oPayment.Invoices.Count To 1 Step -1
                                    If oPayment.Invoices(iInvoiceCounter).Cancel Then
                                        oPayment.Invoices.Remove(iInvoiceCounter)
                                    End If
                                Next
                                ' update amounts on all FIK payments
                                oPayment.MON_InvoiceAmount = oPayment.Invoices(1).MON_InvoiceAmount
                                oPayment.MON_TransferredAmount = oPayment.Invoices(1).MON_TransferredAmount
                            End If
                        End If
                    Next
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_NavamedicOutgoing = bRetValue

    End Function
    'Public Function Treat_Navamedic_SwedenOutgoingReturn(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
    '    ' 18.11.2020 - Must redo returnfiles to Int, to create Betfor01-04
    '    Dim bRetValue As Boolean
    '    Dim sTmp As String

    '    Try

    '        bRetValue = True
    '        sTmp = ""

    '        For Each oBabelFile In oBabelFiles
    '            For Each oBatch In oBabelFile.Batches
    '                For Each oPayment In oBatch.Payments
    '                    ' NO - 06.01.2021 - Probably going to use BETFOR21/23
    '                    'oPayment.PayType = "I"
    '                Next oPayment
    '            Next oBatch
    '        Next oBabelFile

    '    Catch ex As Exception

    '        If Not oBabelFile Is Nothing Then
    '            Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
    '        Else
    '            Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
    '        End If

    '    End Try

    '    Treat_Navamedic_SwedenOutgoingReturn = bRetValue

    'End Function
    Public Function Treat_Pelagia(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' 04.12.2018 - from Ole Chr. Wigestrand, Pelagia:
        ' For USD and JPY incoming payments, use ProductionDate from Cremul as payment date

        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.MON_InvoiceCurrency = "JPY" Then
                            ' for JPY, pick date from ProductionDate - DTM+137
                            'oPayment.DATE_Payment = oBabelFile.DATE_Production
                            ' 11.09.2019: With Camt.054 always use Date_Payment
                            If oPayment.DATE_Payment <> "19900101" Then
                                oPayment.DATE_Value = oPayment.DATE_Payment
                            End If
                        End If
                        ' 11.09.2019 - for Camt.054, no special treat for USD
                        'If oPayment.MON_InvoiceCurrency = "USD" Then
                        '    ' For USD, pick date from DTM+193 in SEQ
                        '    oPayment.DATE_Payment = oPayment.DATE_Initiated
                        '    oPayment.DATE_Value = oPayment.DATE_Payment
                        'End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Pelagia = bRetValue

    End Function
    Public Function Treat_GeorgJensen(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' 17.12.2018
        ' For UK and SE payments, we have very limited space for notification of receiver.
        ' Remove first 9 positions ("Invoice:,")
        Dim bRetValue As Boolean = True

        Try
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.BANK_CountryCode = "SE" Or oPayment.BANK_CountryCode = "GB" Then
                            For Each oInvoice In oPayment.Invoices
                                For Each oFreeText In oInvoice.Freetexts
                                    If Len(oFreeText.Text) > 10 Then
                                        oFreeText.Text = Mid(oFreeText.Text, 10).Trim
                                    End If
                                Next
                            Next
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_GeorgJensen = bRetValue

    End Function
    Public Function Treat_InfoTjenester(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' 20.02.2019
        ' For Invoice > 423500, redo OCR-payments to "fake account" 5124.00.02538
        Dim bRetValue As Boolean = True
        Dim sKID As String = ""

        Try
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' for OCR only!
                        If IsOCR(oPayment.PayCode) Then
                            ' only for old bankaccount - 5124.05.02538 = Infotjenester Klient 100
                            ' Fake account 5124.00.02538 = Capitech - klient 300
                            If oPayment.I_Account = "51240502538" Then
                                For Each oInvoice In oPayment.Invoices
                                    If Len(oInvoice.Unique_Id) = 13 Then
                                        sKID = Mid(oInvoice.Unique_Id, 7, 6)
                                        If Val(sKID) >= 423500 Then
                                            ' redo to fake account
                                            oPayment.I_Account = "51240002538"
                                            oPayment.I_Client = "400"       ' Fake client must be 400
                                            oPayment.VB_ClientNo = "400"    ' Fake client must be 400
                                        Else
                                            sKID = sKID
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

            ' Then we will need reorganizer batches.
            ' Must have a new batch with the fake account, and keep the rest in batches as is
            Dim oTempBatch As vbBabel.Batch
            Dim i As Integer, j As Integer, k As Integer
            Dim oTempPayment As vbBabel.Payment
            Dim oNewbatch As vbBabel.Batch

            'Organize into one "fake"-batch, and keep the others
            For Each oBabelFile In oBabelFiles

                For k = oBabelFile.Batches.Count To 1 Step -1
                    oTempBatch = oBabelFile.Batches(k)
                    oNewbatch = New vbBabel.Batch

                    ' Copy from all batches to new, fake ones
                    CopyBatchObject(oTempBatch, oNewbatch)
                    oNewbatch.Version = "Fake"  ' to know which ones a fake and real
                    oNewbatch = oBabelFile.Batches.VB_AddWithObject(oNewbatch)
                Next k

                ' delete "fake" from other batches (all except last batch)
                For j = oBabelFile.Batches.Count To 1 Step -1
                    oBatch = oBabelFile.Batches(j)
                    If oBatch.Version <> "Fake" Then
                        For i = oBatch.Payments.Count To 1 Step -1
                            oPayment = oBatch.Payments(i)
                            If oPayment.I_Account = "51240002538" Then
                                oBabelFile.Batches(j).Payments.Remove(i)
                            Else
                                i = i
                            End If
                        Next i
                    End If
                Next j

                ' delete "non fake" from new, "fake" batches
                For j = oBabelFile.Batches.Count To 1 Step -1
                    oBatch = oBabelFile.Batches(j)
                    If oBatch.Version = "Fake" Then
                        For i = oBatch.Payments.Count To 1 Step -1
                            oPayment = oBatch.Payments(i)
                            If oPayment.I_Account <> "51240002538" Then
                                oBabelFile.Batches(j).Payments.Remove(i)
                            Else
                                i = i
                            End If
                        Next i
                    End If
                Next j

                ' 19.07.2016 - Check if empty batches, if so remove
                For i = oBabelFile.Batches.Count To 1 Step -1
                    If oBabelFile.Batches(i).Payments.Count = 0 Then
                        oBabelFile.Batches.Remove(i)
                    End If
                Next
            Next


        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_InfoTjenester = bRetValue

    End Function
    Public Function TreatKrohneOCR(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal sFilenameOut As String) As Boolean
        ' Fill in clientnumbers based on length of KID for one special client/account

        ' Spit file in ERPLN and NAV
        '        Dim lCounter As Long
        Dim dERPLNTotal As Double
        Dim dNAVTotal As Double
        Dim sTmp As String

        Try

            dERPLNTotal = 0
            dNAVTotal = 0


            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    dNAVTotal = 0
                    dERPLNTotal = 0



                    For Each oPayment In oBatch.Payments
                        For Each oInvoice In oPayment.Invoices
                            ' NAV 8 digit KID
                            ' ERPLN 12 digit KID
                            If Len(Trim$(oInvoice.Unique_Id)) = 8 Then
                                oPayment.VB_ClientNo = "NAV"
                                dNAVTotal = dNAVTotal + oPayment.MON_TransferredAmount
                            Else
                                oPayment.VB_ClientNo = "ERP"
                                dERPLNTotal = dERPLNTotal + oPayment.MON_TransferredAmount
                            End If
                        Next
                    Next
                Next
            Next


        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatKrohneOCR = True

    End Function
    Public Function Treat_ActiveBrandsAdyenXML(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim i As Integer
        Dim sResponse As String

        Dim nMarkupNC As Double = 0
        Dim nInterchangeNC As Double = 0
        Dim nScheemeFees As Double = 0
        Dim nCommissionNC As Double = 0
        Dim bActiveBrandsUS As Boolean = False ' 13.11.2020
        Dim oSwapPayment As vbBabel.Payment
        Dim oSwapBatch As vbBabel.Batch
        Dim oSwapBabelFile As vbBabel.BabelFile
        Dim sTmpFilenameOutID As String = ""
        Dim sTmpDate As String = ""

        ' ask for currency
        If oBabelFiles(1).VB_Profile.CompanyName = "Active Brands US" Or oBabelFiles(1).VB_Profile.CompanyName = "ECom USA" Then
            sResponse = "USD"
            bActiveBrandsUS = True
        Else
            sResponse = "NOK"
            bActiveBrandsUS = False
        End If

        Do While True
            sResponse = InputBox("Oppgi �nsket valuta for import", "Valuta", sResponse).ToUpper
            ' 21.02.2022 - �pnet for � behandle alle valutaer samlet
            If sResponse = "ALL" Or sResponse = "NOK" Or sResponse = "GBP" Or sResponse = "SEK" Or sResponse = "DKK" Or sResponse = "USD" Or sResponse = "EUR" Then
                ' OK valuta
                Exit Do
            Else
                ' 12.05.2022
                ' Hvis bruker trykker Cancel, er sResponse=blank
                If sResponse = "" Then
                    ' hopp ut av funksjon
                    Treat_ActiveBrandsAdyenXML = False
                    Exit Function
                Else
                    MsgBox("Feil valutakode oppgitt: " & sResponse & vbCrLf & "Pr�v p� nytt!", vbOKOnly, "Feil valuta")
                End If
            End If
        Loop


        For Each oBabelFile In oBabelFiles
            For Each oBatch In oBabelFile.Batches
                For i = oBatch.Payments.Count To 1 Step -1
                    If sResponse <> "ALL" Then
                        If oBatch.Payments(i).MON_TransferCurrency <> sResponse Then
                            oBatch.Payments.Remove(i)
                        End If
                    End If
                Next i
            Next oBatch
        Next oBabelFile

        ' 06.01.2021 
        ' Sum up charges for Adyen direct import of XML files
        ' Add new payments with these charges
        ' This is similar to what we do in SwapFileChangeInvoiceInfo
        'For Each oSwapBabelFile In oBabelFiles
        '    For Each oSwapBatch In oSwapBabelFile.Batches

        '        For i = oSwapBatch.Payments.Count To 1 Step -1
        '            ' total up fees
        '            nMarkupNC = nMarkupNC + oSwapBatch.Payments(i).MON_LocalAmount
        '            nInterchangeNC = nInterchangeNC + oSwapBatch.Payments(i).MON_EuroAmount
        '            nScheemeFees = nScheemeFees + oSwapBatch.Payments(i).MON_ChargesAmount
        '            nCommissionNC = nCommissionNC + oSwapBatch.Payments(i).MON_AccountAmount
        '            oSwapBatch.Payments(i).MON_LocalAmount = 0
        '            oSwapBatch.Payments(i).MON_EuroAmount = 0
        '            oSwapBatch.Payments(i).MON_ChargesAmount = 0
        '            sTmpFilenameOutID = oSwapBatch.Payments(i).VB_FilenameOut_ID
        '            sTmpDate = oSwapBatch.Payments(i).DATE_Payment
        '        Next

        '        ' then add upto four new payments for these charges;
        '        If nMarkupNC > 0 Then
        '            oSwapPayment = oSwapBatch.Payments.Add()
        '            oSwapPayment.VB_Profile = oBabelFiles.VB_Profile
        '            oSwapPayment.ImportFormat = vbBabel.BabelFiles.FileType.Adyen_XML  'iImportFormat
        '            oSwapPayment.StatusCode = "02"
        '            oSwapPayment.E_Name = "Adyen MarkupNC"
        '            oSwapPayment.I_Account = "ADYEN"
        '            oSwapPayment.MON_InvoiceAmount = nMarkupNC * -1
        '            oSwapPayment.MON_TransferredAmount = nMarkupNC * -1
        '            oSwapPayment.MON_InvoiceCurrency = sResponse
        '            oSwapPayment.MON_TransferCurrency = sResponse
        '            oSwapPayment.DATE_Payment = sTmpDate

        '            oSwapPayment.PayType = "D"
        '            oSwapPayment.PayCode = "629"
        '            oSwapPayment.VB_FilenameOut_ID = sTmpFilenameOutID
        '            oSwapPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched

        '            oInvoice = oSwapPayment.Invoices.Add
        '            oInvoice.VB_Profile = oBabelFiles.VB_Profile
        '            oInvoice.MON_InvoiceAmount = nMarkupNC * -1
        '            oInvoice.MON_TransferredAmount = nMarkupNC * -1
        '            'oInvoice.MATCH_ID = "7781"
        '            ' 13.11.2020 - utvidet til ogs� � behandle Active Brands US
        '            If bActiveBrandsUS Then
        '                oInvoice.MATCH_ID = "80401"
        '            Else
        '                oInvoice.MATCH_ID = "7781"
        '            End If
        '            oInvoice.MATCH_Final = True
        '            oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL
        '            oInvoice.MATCH_Matched = True
        '            oInvoice.MATCH_Original = True
        '            oInvoice.Dim10 = "MarkupNC"

        '            oFreeText = oInvoice.Freetexts.Add
        '            oFreeText.Text = "MarkupNC"

        '        End If

        '        If nInterchangeNC > 0 Then
        '            oSwapPayment = oSwapBatch.Payments.Add()
        '            oSwapPayment.VB_Profile = oBabelFiles.VB_Profile
        '            oSwapPayment.ImportFormat = vbBabel.BabelFiles.FileType.Adyen_XML
        '            oSwapPayment.StatusCode = "02"
        '            oSwapPayment.E_Name = "Adyen InterchangeNC"
        '            oSwapPayment.I_Account = "ADYEN"
        '            oSwapPayment.MON_InvoiceAmount = nInterchangeNC * -1
        '            oSwapPayment.MON_TransferredAmount = nInterchangeNC * -1
        '            oSwapPayment.DATE_Payment = sTmpDate
        '            oSwapPayment.MON_InvoiceCurrency = sResponse
        '            oSwapPayment.MON_TransferCurrency = sResponse
        '            oSwapPayment.PayType = "D"
        '            oSwapPayment.PayCode = "629"
        '            oSwapPayment.VB_FilenameOut_ID = sTmpFilenameOutID
        '            oSwapPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched

        '            oInvoice = oSwapPayment.Invoices.Add
        '            oInvoice.VB_Profile = oBabelFiles.VB_Profile
        '            oInvoice.MON_InvoiceAmount = nInterchangeNC * -1
        '            oInvoice.MON_TransferredAmount = nInterchangeNC * -1
        '            'oInvoice.MATCH_ID = "7781"
        '            ' 13.11.2020 - utvidet til ogs� � behandle Active Brands US
        '            If bActiveBrandsUS Then
        '                oInvoice.MATCH_ID = "80401"
        '            Else
        '                oInvoice.MATCH_ID = "7781"
        '            End If
        '            oInvoice.MATCH_Final = True
        '            oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL
        '            oInvoice.MATCH_Matched = True
        '            oInvoice.MATCH_Original = True
        '            oInvoice.Dim10 = "InterchangeNC"

        '            oFreeText = oInvoice.Freetexts.Add
        '            oFreeText.Text = "InterchangeNC"
        '        End If

        '        If nScheemeFees > 0 Then
        '            oSwapPayment = oSwapBatch.Payments.Add()
        '            oSwapPayment.VB_Profile = oBabelFiles.VB_Profile
        '            oSwapPayment.ImportFormat = vbBabel.BabelFiles.FileType.Adyen_XML
        '            oSwapPayment.StatusCode = "02"
        '            oSwapPayment.E_Name = "Adyen ScheemeFeesNC"
        '            oSwapPayment.I_Account = "ADYEN"
        '            oSwapPayment.MON_InvoiceAmount = nScheemeFees * -1
        '            oSwapPayment.MON_TransferredAmount = nScheemeFees * -1
        '            oSwapPayment.DATE_Payment = sTmpDate
        '            oSwapPayment.MON_InvoiceCurrency = sResponse
        '            oSwapPayment.MON_TransferCurrency = sResponse
        '            oSwapPayment.PayType = "D"
        '            oSwapPayment.PayCode = "629"
        '            oSwapPayment.VB_FilenameOut_ID = sTmpFilenameOutID
        '            oSwapPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched

        '            oInvoice = oSwapPayment.Invoices.Add
        '            oInvoice.VB_Profile = oBabelFiles.VB_Profile
        '            oInvoice.MON_InvoiceAmount = nScheemeFees * -1
        '            oInvoice.MON_TransferredAmount = nScheemeFees * -1
        '            'oInvoice.MATCH_ID = "7781"
        '            ' 13.11.2020 - utvidet til ogs� � behandle Active Brands US
        '            If bActiveBrandsUS Then
        '                oInvoice.MATCH_ID = "80401"
        '            Else
        '                oInvoice.MATCH_ID = "7781"
        '            End If
        '            oInvoice.MATCH_Final = True
        '            oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL
        '            oInvoice.MATCH_Matched = True
        '            oInvoice.MATCH_Original = True
        '            oInvoice.Dim10 = "ScheemeFeesNC"

        '            oFreeText = oInvoice.Freetexts.Add
        '            oFreeText.Text = "ScheemeFeesNC"
        '        End If

        '        If nCommissionNC > 0 Then
        '            oSwapPayment = oSwapBatch.Payments.Add()
        '            oSwapPayment.VB_Profile = oBabelFiles.VB_Profile
        '            oSwapPayment.ImportFormat = vbBabel.BabelFiles.FileType.Adyen_XML
        '            oSwapPayment.StatusCode = "02"
        '            oSwapPayment.E_Name = "Adyen CommissionNC"
        '            oSwapPayment.I_Account = "ADYEN"
        '            oSwapPayment.MON_InvoiceAmount = nCommissionNC * -1
        '            oSwapPayment.MON_TransferredAmount = nCommissionNC * -1
        '            oSwapPayment.DATE_Payment = sTmpDate
        '            oSwapPayment.MON_InvoiceCurrency = sResponse
        '            oSwapPayment.MON_TransferCurrency = sResponse
        '            oSwapPayment.PayType = "D"
        '            oSwapPayment.PayCode = "629"
        '            oSwapPayment.VB_FilenameOut_ID = sTmpFilenameOutID
        '            oSwapPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched

        '            oInvoice = oSwapPayment.Invoices.Add
        '            oInvoice.VB_Profile = oBabelFiles.VB_Profile
        '            oInvoice.MON_InvoiceAmount = nCommissionNC * -1
        '            oInvoice.MON_TransferredAmount = nCommissionNC * -1
        '            'oInvoice.MATCH_ID = "7781"
        '            ' 13.11.2020 - utvidet til ogs� � behandle Active Brands US
        '            If bActiveBrandsUS Then
        '                oInvoice.MATCH_ID = "80401"
        '            Else
        '                oInvoice.MATCH_ID = "7781"
        '            End If
        '            oInvoice.MATCH_Final = True
        '            oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL
        '            oInvoice.MATCH_Matched = True
        '            oInvoice.MATCH_Original = True
        '            oInvoice.Dim10 = "CommissionNC"

        '            oFreeText = oInvoice.Freetexts.Add
        '            oFreeText.Text = "CommissionNC"
        '        End If
        '    Next oSwapBatch
        'Next oSwapBabelFile

        Treat_ActiveBrandsAdyenXML = True

    End Function

    Public Function Treat_RagnSellsDomestic(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' 15.01.2020
        ' FI and SE domestic payments, from Telepay OR TBIO
        ' Redo to domestic payment.
        ' ON HOLD: What to do with bankgiro/plusgiro?
        ' Unstructured only?
        Dim bRetValue As Boolean = True

        Try
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.PayType = "D"
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_RagnSellsDomestic = bRetValue

    End Function
    Public Function Treat_Active_Lisenstelling(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile, ByVal iFileSetup_ID As Integer) As Boolean
        ' tell opp fakturaforbruk for ActiveBrands
        ' plukk ut alle som har F; i recordstart

        Dim bRetValue As Boolean = True
        Dim sErr As String = ""
        Dim lCounterF As Long = 0
        Dim lCounterK As Long = 0
        Dim sTxt As String = ""

        Try
            For Each oBabelFile In oBabelFiles
                sTxt = oBabelFile.GenericText

                ' tell opp antall F;
                lCounterF = lCounterF + Len(sTxt) - Len(Replace(sTxt, "F;", ""))
                lCounterK = lCounterK + Len(sTxt) - Len(Replace(sTxt, "K;", ""))
            Next oBabelFile
            ' teller 2 chars for hver (F; eller K;
            lCounterF = lCounterF / 2
            lCounterK = lCounterK / 2

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        ' Resultat:
        ' - Teller 1 023  (F; = 943)  I Babel:  1 364
        ' - Camt  11 167  (F; = 1997) I Babel: 23 084
        ' - Adyen 36 222  (F; = 1069) I Babel: 49 511
        ' ------------------------------------------
        ' Total   48 412                      

        Treat_Active_Lisenstelling = bRetValue

    End Function

    Public Function Treat_RagnSellsPG(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile, ByVal iFileSetup_ID As Integer) As Boolean
        ' 24.01.2020
        ' NO and SE debits, Int payments (imported from Nordea PG file)
        ' Treat NorgesBank koder, if debit in Norway
        Dim bRetValue As Boolean = True
        Dim sErr As String = ""

        Try
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If Mid(oPayment.BANK_I_SWIFTCode, 5, 2) = "NO" Then
                            For Each oInvoice In oPayment.Invoices
                                oInvoice.STATEBANK_Code = TranslateFromRiksbankCodesToNorgesBankCodes(oInvoice.STATEBANK_Code)
                                If EmptyOrNullString(oInvoice.STATEBANK_Text) Then
                                    oInvoice.STATEBANK_Text = ValutaRegistreringsTekst(oInvoice.STATEBANK_Code, "NO")
                                End If
                            Next oInvoice
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile


        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_RagnSellsPG = bRetValue

    End Function
    Public Function Treat_RagnSellsBG(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile, ByVal iFileSetup_ID As Integer) As Boolean
        ' 07.04.2020 TEST ONLY
        ' Lagt til funksjonalitet for � kontrollere betalinger, og slette disse dersom det er feil p� dem
        ' Det skal lages en rapport over de feilmarkerte
        ' Hva kontrolleres?
        Dim oReport As vbBabel.Report
        Dim oBabelReport As vbBabel.BabelReport
        Dim bErrorMarked As Boolean = False
        Dim j As Integer = 0
        '        Dim bx As Boolean = False
        Dim sErr As String = ""
        Dim bRetValue As Boolean = True

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        ' Start controls;
                        ' ---------------
                        '-	Om mottakerkonto er angitt (forutsetter da at ingen betalinger skal betales med Check/Money order)
                        If Len(oPayment.E_Account) < 5 Then
                            bErrorMarked = True
                            oPayment.ToSpecialReport = True
                            oPayment.Invoices(1).ToSpecialReport = True
                            ' do not export errormarked
                            oPayment.Exported = True
                            ' add text to report;
                            oFreeText = oPayment.Invoices(1).Freetexts.Add
                            oFreeText.Text = "FEL�RSAK: " & "Modtagarens kontonummer saknas."
                        Else
                            '-	Mottakerkonto lengde, gjelder for noen land, ikke alle
                            ' Sjekk IBAN
                            If IsIBANNumber(oPayment.E_Account, True, False, sErr) = 1 Then  ' hva slags retur f�r vi ved feil i iban?
                                bErrorMarked = True
                                oPayment.ToSpecialReport = True
                                oPayment.Invoices(1).ToSpecialReport = True
                                ' do not export errormarked
                                oPayment.Exported = True
                                ' add text to report;
                                oFreeText = oPayment.Invoices(1).Freetexts.Add
                                oFreeText.Text = sErr
                            Else
                                ' sjekk Norge
                                If oPayment.BANK_CountryCode = "NO" Then
                                    If Len(oPayment.E_Account) <> 11 Then
                                        bErrorMarked = True
                                        oPayment.ToSpecialReport = True
                                        oPayment.Invoices(1).ToSpecialReport = True
                                        ' do not export errormarked
                                        oPayment.Exported = True
                                        ' add text to report;
                                        oFreeText = oPayment.Invoices(1).Freetexts.Add
                                        oFreeText.Text = LRS("Fel lengd p� norsk kontonummer")
                                    End If
                                End If

                            End If
                        End If

                        '-	Mottaker BIC; Syntax sjekk (lengde, i hvilke posisjoner det er tillatt med bokstaver og tall, sjekke spesialtegn)
                        If Len(oPayment.BANK_SWIFTCode) > 0 Then
                            If ValidateBankID(vbBabel.BabelFiles.BankBranchType.SWIFT, oPayment.BANK_SWIFTCode, False, sErr) Then
                                bErrorMarked = True
                                oPayment.ToSpecialReport = True
                                oPayment.Invoices(1).ToSpecialReport = True
                                ' do not export errormarked
                                oPayment.Exported = True
                                ' add text to report;
                                oFreeText = oPayment.Invoices(1).Freetexts.Add
                                oFreeText.Text = sErr
                            End If
                        End If

                        '-	Vi kan ogs� sjekke dato, at denne er satt fram i tid, og ikke �for langt� fram i tid
                        If DateDiff("d", StringToDate(oPayment.DATE_Payment), Today) > 60 Then
                            bErrorMarked = True
                            oPayment.ToSpecialReport = True
                            oPayment.Invoices(1).ToSpecialReport = True
                            ' do not export errormarked
                            oPayment.Exported = True
                            ' add text to report;
                            oFreeText = oPayment.Invoices(1).Freetexts.Add
                            oFreeText.Text = "Betalningsdato mer enn 60 dager fram�t i tid"
                        End If

                        '-	I noen land kan vi kontrollere KID (Norge), FIK (Danmark), etc.
                        ' DISKUTER OM VI SKAL AVVISE P� DETTE !!!!!!!!
                        For Each oInvoice In oPayment.Invoices
                            If Not EmptyString(oInvoice.Unique_Id) Then
                                If oPayment.BANK_CountryCode = "NO" Then
                                    If IsModulus11(oInvoice.Unique_Id) = False And IsModulus10(oInvoice.Unique_Id) = False Then
                                        bErrorMarked = True
                                        oPayment.ToSpecialReport = True
                                        oInvoice.ToSpecialReport = True
                                        oInvoice.ToSpecialReport = True
                                        ' do not export errormarked
                                        oPayment.Exported = True
                                        ' add text to report;
                                        oFreeText = oInvoice.Freetexts.Add
                                        oFreeText.Text = "Felaktig KID"
                                    End If

                                    Dim sKortart As String = ""
                                    Dim sKreditor As String = ""
                                    Dim sBetalerIdent As String = ""
                                    If oPayment.BANK_CountryCode = "DK" Then
                                        If ValidateFI(oInvoice.Unique_Id, sKortart, sBetalerIdent, sKreditor, vbBabel.BabelFiles.FileType.Pain001, sErr) Then
                                            bErrorMarked = True
                                            oPayment.ToSpecialReport = True
                                            oInvoice.ToSpecialReport = True
                                            oPayment.Invoices(1).ToSpecialReport = True
                                            ' do not export errormarked
                                            oPayment.Exported = True
                                            ' add text to report;
                                            oFreeText = oInvoice.Freetexts.Add
                                            oFreeText.Text = sErr
                                        End If
                                    End If
                                    If oPayment.BANK_CountryCode = "FI" Then
                                        If ValidateReference(oInvoice.Unique_Id, sErr) Then
                                            bErrorMarked = True
                                            oPayment.ToSpecialReport = True
                                            oInvoice.ToSpecialReport = True
                                            oPayment.Invoices(1).ToSpecialReport = True
                                            ' do not export errormarked
                                            oPayment.Exported = True
                                            ' add text to report;
                                            oFreeText = oInvoice.Freetexts.Add
                                            oFreeText.Text = sErr
                                        End If
                                    End If

                                End If
                            End If

                        Next oInvoice

                    Next
                Next
            Next

            If bErrorMarked Then
                ' activate report, call report 003, with Specialsetup
                oFilesetup = oBabelFiles.VB_Profile.FileSetups(iFileSetup_ID)
                For j = 1 To oFilesetup.Reports.Count
                    If oFilesetup.Reports(j).ReportNo = 3 And oFilesetup.Reports(j).ReportWhen = vbBabel.BabelFiles.ReportTime.Inside_SpecialRoutines Then ' Specialreport

                        oBabelReport = New vbBabel.BabelReport
                        oBabelReport.Special = oBabelFiles.Item(1).Special
                        oBabelReport.BabelFiles = oBabelFiles
                        oBabelReport.ReportOnSelectedItems = True
                        oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.Inside_SpecialRoutines
                        If Not oBabelReport.RunReports(oFilesetup.FileSetup_ID) Then
                            j = 0
                        End If

                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

                        Exit For
                    End If
                Next j
            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_RagnSellsBG = bRetValue

    End Function
    Public Function Treat_VideotelFasterPayments(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' 04.02.2020
        ' GB Faster payments; Need to set as Priority
        Dim bRetValue As Boolean = True

        Try
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.Priority = True
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_VideotelFasterPayments = bRetValue

    End Function
    Public Function Treat_DNB_Autolease(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Redo from text to FIK/Girokort

        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' Check if FIK71 or Girokort 04
                        ' Redo to correct data FIK-data

                        For Each oInvoice In oPayment.Invoices
                            ' FIK 71, in e_account (and freetext):
                            ' 71 200208522347480 88659562
                            If oInvoice.Freetexts.Count = 1 Then
                                sTmp = oInvoice.Freetexts(1).Text.Trim
                                If Len(sTmp) = 27 And Left(sTmp, 2) = "71" And Mid(sTmp, 3, 1) = " " And Mid(sTmp, 19, 1) = " " Then
                                    ' this is FIK71
                                    oPayment.PayCode = "371"
                                    ' put 71+BetalingsId into UniqueID
                                    oInvoice.Unique_Id = "71" & Mid(sTmp, 4, 15)
                                    ' put Kreditorno into e_account
                                    oPayment.E_Account = Mid(sTmp, 20, 8)
                                    ' blank text
                                    oInvoice.Freetexts(1).Text = ""
                                End If
                                ' 04.09.2020
                                ' added FIK75
                                'Length: Kortartcode (2) + Betalerident (16) + Creditorno. (8)
                                ' 75 3234613104883345 84549177 
                                If Len(sTmp) = 28 And Left(sTmp, 2) = "75" And Mid(sTmp, 3, 1) = " " And Mid(sTmp, 20, 1) = " " Then
                                    ' this is FIK75
                                    oPayment.PayCode = "375"
                                    ' put 71+BetalingsId into UniqueID
                                    oInvoice.Unique_Id = "75" & Mid(sTmp, 4, 16)
                                    ' put Kreditorno into e_account
                                    oPayment.E_Account = Mid(sTmp, 21, 8)
                                    ' blank text
                                    oInvoice.Freetexts(1).Text = ""
                                End If

                                ' 04 0083053724109862 8016313
                                ' Len girocard 04 from 26-28. 2+16+6,7,8 (plus 2 spaces when Autolease are using them in freetext BETFOR04
                                If Left(sTmp, 2) = "04" And Mid(sTmp, 3, 1) = " " And Mid(sTmp, 20, 1) = " " Then
                                    ' this is GIRO04
                                    oPayment.PayCode = "304"
                                    ' put 04+BetalingsId into UniqueID
                                    oInvoice.Unique_Id = "04" & Mid(sTmp, 4, 16)
                                    ' put Kreditorno into e_account (can be 6,7 or 8 length
                                    oPayment.E_Account = Mid(sTmp, 20, 8).Trim
                                    ' blank text
                                    oInvoice.Freetexts(1).Text = ""
                                End If
                            End If
                        Next
                    Next oPayment
                Next oBatch
            Next oBabelFile


        Catch ex As Exception


            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_DNB_Autolease = bRetValue


    End Function
    Public Function Treat_HoeghAutoliners_SingaporeOutgoing(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' set Int or Domestic
        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.MON_InvoiceCurrency = "SGD" And oPayment.BANK_CountryCode = "SG" Then
                            oPayment.PayType = "D"
                        Else
                            oPayment.PayType = "I"
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_HoeghAutoliners_SingaporeOutgoing = bRetValue

    End Function
    Public Function Treat_VegFinans_Regnr(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Import list of Regnr from Excel file
        ' Setup path/fielname for Excelfile in Oppsett/Brukertilpasset meny_1
        Dim bRetValue As Boolean
        Dim sTmp As String
        Dim bFound As Boolean = False

        Dim aRegNos(100000) As String
        Dim appExcel As Microsoft.Office.Interop.Excel.Application
        Dim oXLWBook As Microsoft.Office.Interop.Excel.Workbook
        Dim oExcelSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim lRowNo As Integer
        Dim lEmptyRows As Integer
        Dim i As Integer

        bRetValue = True
        sTmp = ""

        If Dir(Trim(oBabelFiles.VB_Profile.Custom1Value)) <> "" Then

            appExcel = New Microsoft.Office.Interop.Excel.Application
            ' Disable all alerts
            appExcel.DisplayAlerts = False
            oXLWBook = appExcel.Workbooks.Open(Trim(oBabelFiles.VB_Profile.Custom1Value), , True)
            oExcelSheet = oXLWBook.Worksheets(1) ' er i ark 1
            lRowNo = 1 ' one Headingline
            lEmptyRows = 0
            Do
                lRowNo = lRowNo + 1
                '       A
                'Regnr
                'DN57976
                sTmp = Trim(oExcelSheet.Cells._Default(lRowNo, 1).Value)
                If Len(sTmp) > 0 Then
                    'add to array
                    'If Array_IsEmpty(aRegNos) Then
                    '    ReDim aRegNos(0)
                    'Else
                    '    ReDim Preserve aRegNos(UBound(aRegNos) + 1)
                    'End If
                    ' aRegNos already dimensioned !!!
                    'aRegNos(UBound(aRegNos)) = sTmp

                    aRegNos(lRowNo) = sTmp
                Else
                    If oExcelSheet.Cells._Default(lRowNo, 1).Value = "" Then
                        lEmptyRows = lEmptyRows + 1
                    End If
                End If
                ' tell opp tomme linjer, og hopp ut
                If lEmptyRows > 20 Then
                    Exit Do
                End If
            Loop
            ' array filled!
            ' Close Excelfile
            oXLWBook.Close()
            oXLWBook = Nothing

            ' cut array at no of lines;
            ReDim Preserve aRegNos(lRowNo + 1)

        End If

        For Each oBabelFile In oBabelFiles
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    For Each oInvoice In oPayment.Invoices
                        ' Regnr er lagret i oInvoice.InvoiceNo
                        bFound = False
                        For i = 0 To UBound(aRegNos)
                            ' Ans.nr from Kontantliste in oPayment.Ref_own
                            If oInvoice.InvoiceNo = aRegNos(i) Then
                                bFound = True
                                Exit For
                            End If
                        Next i
                        If Not bFound Then
                            ' do not export those where can't find Regno
                            oPayment.Exported = True
                        End If
                    Next
                Next oPayment
            Next oBatch
        Next oBabelFile

        Treat_VegFinans_Regnr = bRetValue

    End Function
    Public Function Treat_BergansDEIncoming(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Redo from KID to InvoiceNo

        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True
            sTmp = ""

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments

                        For Each oInvoice In oPayment.Invoices
                            If Len(oInvoice.Unique_Id) > 0 Then
                                sTmp = oInvoice.Unique_Id
                                oInvoice.Unique_Id = ""
                                oInvoice.InvoiceNo = sTmp
                                oPayment.PayCode = "150"
                            End If
                        Next
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_BergansDEIncoming = bRetValue

    End Function
    Public Function Treat_Moelven_Industrier(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Imports MT101 Exports Pain.001
        ' Catch Konserninterne - Always for GB and DK
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        'If (oPayment.BANK_AccountCountryCode = "GB" And oPayment.BANK_CountryCode = "GB") Or (oPayment.BANK_AccountCountryCode = "DK" And oPayment.BANK_CountryCode = "DK") Then
                        If (Left(oPayment.I_Account, 2) = "GB" And oPayment.BANK_CountryCode = "GB") Or (Left(oPayment.I_Account, 2) = "DK" And oPayment.BANK_CountryCode = "DK") Then
                            oPayment.ToOwnAccount = True
                            oPayment.PayCode = "403"  ' To own account
                            oPayment.PayType = "D"
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_Moelven_Industrier = True

    End Function
    Public Function TreatVOKKS_Telepay(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Imports Cremul exports Telepay
        ' Redo from incoming to outgoing payments
        ' ----------------------------------------------------------------------------------

        Dim i As Long = 0
        Dim j As Long = 0

        Try

            For Each oBabelFile In oBabelFiles
                oBabelFile.StatusCode = "00"
                For Each oBatch In oBabelFile.Batches
                    oBatch.StatusCode = "00"
                    For j = oBatch.Payments.Count To 1 Step -1
                        oPayment = oBatch.Payments(j)
                        oPayment.PayCode = "301"
                        oPayment.StatusCode = "00"

                        ' Sett konti her:
                        ' Frakonto
                        oPayment.I_Account = "15066815584"
                        ' Tilkonto
                        oPayment.E_Account = "15035246204"
                        If EmptyString(oPayment.E_Name) Then
                            oPayment.E_Name = "N/A"
                        End If

                        For i = oPayment.Invoices.Count To 1 Step -1
                            oPayment.Invoices(i).StatusCode = "00"

                            ' remove all payments with KID len 15, starting with 72
                            If Len(oPayment.Invoices(i).Unique_Id) = 15 And Left(oPayment.Invoices(i).Unique_Id, 2) = "72" Then
                                oPayment.Invoices.Remove(i)
                                ' 16.12.2021 - Fjern ogs� de uten KID
                            ElseIf EmptyString(oPayment.Invoices(i).Unique_Id) Then
                                oPayment.Invoices.Remove(i)
                            End If
                        Next
                        ' reset
                        oPayment.MON_InvoiceAmount = 0
                        oPayment.MON_TransferredAmount = 0
                        oPayment.AddInvoiceAmountOnPayment()
                        oPayment.AddTransferredAmountOnPayment()

                        If oPayment.Invoices.Count = 0 Then
                            oBatch.Payments.Remove(j)
                        End If
                    Next
                    ' reset
                    oBatch.MON_InvoiceAmount = 0
                    oBatch.MON_TransferredAmount = 0
                    oBatch.AddInvoiceAmountOnBatch()
                    oBatch.AddTransferredAmountOnBatch()
                Next
                'reset 
                oBabelFile.MON_InvoiceAmount = 0
                oBabelFile.MON_TransferredAmount = 0
                oBabelFile.AddInvoiceAmountOnBabelFile()
                oBabelFile.AddTransferredAmountOnBabelFile()
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatVOKKS_Telepay = True

    End Function
    Public Function TreatVOKKS_Cremul(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Imports Cremul exports Cremul
        ' Remove invoices with wrong KID
        ' ----------------------------------------------------------------------------------

        Dim i As Long = 0
        Dim j As Long = 0

        Try

            For Each oBabelFile In oBabelFiles
                oBabelFile.StatusCode = "00"
                For Each oBatch In oBabelFile.Batches
                    oBatch.StatusCode = "00"
                    For j = oBatch.Payments.Count To 1 Step -1
                        oPayment = oBatch.Payments(j)

                        For i = oPayment.Invoices.Count To 1 Step -1
                            ' remove all payments which does NOT have KID len 15, starting with 72
                            If Not (Len(oPayment.Invoices(i).Unique_Id) = 15 And Left(oPayment.Invoices(i).Unique_Id, 2) = "72") Then
                                ' 16.12.2021 - ikke slett de uten KID
                                If Len(oPayment.Invoices(i).Unique_Id) = 0 Then
                                    ' do not remove !!!!
                                    j = j
                                Else
                                    oPayment.Invoices.Remove(i)
                                End If
                            End If

                        Next
                        ' reset
                        oPayment.MON_InvoiceAmount = 0
                        oPayment.MON_TransferredAmount = 0
                        oPayment.AddInvoiceAmountOnPayment()
                        oPayment.AddTransferredAmountOnPayment()

                        If oPayment.Invoices.Count = 0 Then
                            oBatch.Payments.Remove(j)
                        End If
                    Next
                    ' reset
                    oBatch.MON_InvoiceAmount = 0
                    oBatch.MON_TransferredAmount = 0
                    oBatch.AddInvoiceAmountOnBatch()
                    oBatch.AddTransferredAmountOnBatch()
                Next
                'reset 
                oBabelFile.MON_InvoiceAmount = 0
                oBabelFile.MON_TransferredAmount = 0
                oBabelFile.AddInvoiceAmountOnBabelFile()
                oBabelFile.AddTransferredAmountOnBabelFile()
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatVOKKS_Cremul = True

    End Function
    Public Function Treat_EuroFinans(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' Has Valutaficka.
        ' Must "cheat" by setting account to 1234567USD, 1234567EUR, etc, to make currency match account
        ' ----------------------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.MON_InvoiceCurrency <> "SEK" Then
                            oPayment.I_Account = Trim(oPayment.I_Account) & oPayment.MON_InvoiceCurrency
                        End If

                        ' In Catm054 import we end up with paycode = 629 (OCR), which is not good for matching.
                        ' Temp. redo to 601
                        oPayment.PayCode = "601"

                        ' Hvis ultimate debtor er satt, bruk denne:
                        If Not EmptyString(oPayment.UltimateE_Name) Then
                            oPayment.E_Name = oPayment.UltimateE_Name
                            oPayment.E_Adr1 = oPayment.UltimateE_Adr1
                            oPayment.E_Adr2 = oPayment.UltimateE_Adr2
                            oPayment.E_Adr3 = oPayment.UltimateE_Adr3
                            oPayment.E_City = oPayment.UltimateE_City
                            oPayment.E_Zip = oPayment.UltimateE_Zip
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_EuroFinans = True

    End Function


    ' lese inn XML partially ?
    'Imports System.Xml
    'Imports System.Xml.Linq
    'Module Module1
    Const FILENAME As String = "C:\largexml.xml"
    'Sub Main()
    '    Dim reader As XmlReader = XmlReader.Create(FILENAME)
    '    While Not reader.EOF
    '        If reader.Name <> "ns" Then
    '            reader.ReadToFollowing("ns")
    '        End If
    '        If Not reader.EOF Then
    '            Dim ns As XElement = XElement.ReadFrom(reader)
    '        End If
    '    End While

    'End Sub

    'End Module
    Public Function TreatNemoq(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Imports Bankgiro innland Exports Pain.001
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If EmptyString(oPayment.E_Name) Then
                            oPayment.E_Name = "Fake name"
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatNemoq = True

    End Function
    Public Function TreatColliCare_DK(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' Telepay to Pain.001
        ' Check for FIK71
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.BANK_CountryCode = "DK" Then
                            oPayment.PayType = "D"
                        End If
                        For Each oInvoice In oPayment.Invoices
                            ' check StateBankText if starts with +71
                            If Left(oInvoice.STATEBANK_Text, 3) = "+71" Then
                                oInvoice.Unique_Id = Mid(oInvoice.STATEBANK_Text, 2, 2) & Mid(oInvoice.STATEBANK_Text, 5, 15)
                                oPayment.E_Account = Left(oInvoice.Freetexts(1).Text, 20).Trim  ' find creditorno in meldingstekst
                                oPayment.PayCode = "371"  ' FIK 71
                            End If
                        Next
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatColliCare_DK = True

    End Function
    Public Function TreatIceland_Priority(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' set payment to SameDay (Priority = True)
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.Priority = True
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatIceland_Priority = True

    End Function
End Module
