Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic

Friend Class frmDebug
	Inherits System.Windows.Forms.Form
	
	Private Const VER_PLATFORM_WIN32s As Short = 0
	Private Const VER_PLATFORM_WIN32_WINDOWS As Short = 1
	Private Const VER_PLATFORM_WIN32_NT As Short = 2
	
	' 05.08.2008 - Commentet, not in use!!
	'Private Type OSVERSIONINFOEX
	'  OSVSize            As Long
	'  dwVerMajor        As Long
	'  dwVerMinor         As Long
	'  dwBuildNumber      As Long
	'  PlatformID         As Long
	'  szCSDVersion       As String * 128
	'  wServicePackMajor  As Integer
	'  wServicePackMinor  As Integer
	'  wSuiteMask         As Integer
	'  wProductType       As Byte
	'  wReserved          As Byte
	'End Type

    'my type for holding the retrieved info
    Private Structure RGB_WINVER
        Dim PlatformID As Integer
        Dim VersionName As String
        Dim VersionNo As String
        Dim ServicePack As String
        Dim BuildNo As String
    End Structure

    Dim sInfoString As String
    Private Sub cmdDatabase_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDatabase.Click
        ' Collect information about BabelBanks database;
        Dim oDAL As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim sCLRVersion As String
        Dim fs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sPath As String
        Dim sVersion As String

        Me.Picture1.Visible = False

        On Error GoTo errorINI
        ' INI-file in use ?
        If RunTime() Then   'DISKUTERNOTRUNTIME Skal vi ha en felles mappe?
            If Len(Dir(My.Application.Info.DirectoryPath & "\BabelBank.ini")) > 0 Then
                sInfoString = "BabelBank INI-file: " & My.Application.Info.DirectoryPath & "\BabelBank.ini" & vbCrLf
            Else
                sInfoString = "No INI-file in use" & vbCrLf
            End If
        Else
            If Len(Dir("c:\projects\BabelBank\BabelBank.ini")) > 0 Then
                sInfoString = "BabelBank INI-file: c:\projects\BabelBank\Profiles\BabelBank.ini" & vbCrLf
            Else
                sInfoString = "No INI-file in use" & vbCrLf
            End If
        End If


        On Error GoTo 0
        On Error GoTo errorFindDatabase
        ' Can we find BabelBanks database?
        If Len(Dir(BB_DatabasePath)) = 0 Then
            sInfoString = sInfoString & "NB! Can not find BabelBank's database " & BB_DatabasePath() & vbCrLf
            Me.Picture1.Visible = True ' Exlamationmark on
        Else
            sInfoString = sInfoString & "BabelBank database: " & BB_DatabasePath() & vbCrLf
        End If

        On Error GoTo 0
        On Error GoTo errorReadOnly

        ' Is database readonly ?
        If (GetAttr(BB_DatabasePath) And FileAttribute.ReadOnly) > 0 Then
            sInfoString = sInfoString & vbCrLf & "NB! BabelBank database is READONLY. " & vbCrLf
            sInfoString = sInfoString & "This will prevent Babelbank from opening the database correctly." & vbCrLf & vbCrLf
            Me.Picture1.Visible = True ' Exlamationmark on
        End If

        On Error GoTo 0
        On Error GoTo errorTestWrite
        ' Try to create a file - and test error
        'fs = CreateObject ("Scripting.FileSystemObject")
        fs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
        ' Find databasepath, filename not included
        sPath = VB.Left(BB_DatabasePath, InStrRev(BB_DatabasePath, "\") - 1)
        oFile = fs.OpenTextFile(sPath & "\xzcvTestfile.txt", Scripting.IOMode.ForAppending, True)
        oFile.Write("Hello BabelBank!")

        If Not oFile Is Nothing Then
            oFile.Close()
            oFile = Nothing
        End If

        If Not fs Is Nothing Then
            fs = Nothing
        End If

        On Error GoTo 0
        On Error GoTo errorTestDelete
        If Len(Dir(sPath & "\xzcvTestfile.txt")) > 0 Then
            Kill(sPath & "\xzcvTestfile.txt")
        End If

        On Error GoTo errorFramework
        ' .Net Framework
        sCLRVersion = System.Environment.Version.ToString
        'Environment.Version returns CLR version only. (Does this mean that we don't see if servicepacks are installed
        sInfoString = sInfoString & "CLR version: " & sCLRVersion & vbCrLf
        On Error GoTo 0

        On Error GoTo errorOpeningDatabase
        ' Check if possible to open database
        oDAL = New vbBabel.DAL
        oDAL.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
        If Not oDAL.ConnectToDB() Then
            Err.Raise(1000, , oDAL.ErrorMessage)
            Throw New System.Exception(oDAL.ErrorMessage)
        End If
        On Error GoTo 0

        On Error GoTo errorSelecting
        sMySQL = "Select VersionNo From Version"
        oDAL.SQL = sMySQL
        If oDAL.Reader_Execute() Then
            Do While oDAL.Reader_ReadRecord
                sVersion = oDAL.Reader_GetString("VersionNo")
            Loop
        Else
            Err.Raise(1000, , oDAL.ErrorMessage)
        End If

        sInfoString = sInfoString & "Databaseversjon: " & sVersion & vbCrLf

        ' Present info in form:
        Me.txtInfoString.Text = sInfoString

        If Not oDAL Is Nothing Then
            oDAL.Close()
            oDAL = Nothing
        End If

        Exit Sub

errorINI:
        MsgBox("Error when trying to find BabelBank's INI-file", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Debug info")
        Resume Next

errorFindDatabase:
        MsgBox("Error when trying to find BabelBank's database", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Debug info")
        Resume Next

errorReadOnly:
        MsgBox("Error when trying to check if BabelBank's database is ReadOnly", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Debug info")
        Me.Picture1.Visible = True ' Exlamationmark on
        Resume Next

errorTestWrite:
        Me.Picture1.Visible = True ' Exlamationmark on
        MsgBox("Error when trying to write to database folder. Please check permissions for user " & Environ("Username"), MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Debug info")
        sInfoString = sInfoString & vbCrLf & "NB! No permission to write to " & sPath
        Resume Next

errorTestDelete:
        Me.Picture1.Visible = True ' Exlamationmark on
        MsgBox("Error when trying to delete from database folder. Please check permissions!", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Debug info")
        sInfoString = sInfoString & vbCrLf & "NB! No permission to delete from " & sPath
        Resume Next

errorFramework:
        Me.Picture1.Visible = True ' Exlamationmark on
        MsgBox("Error testing .NET framework!", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Debug info")
        Resume Next

errorOpeningDatabase:
        Me.Picture1.Visible = True ' Exlamationmark on
        MsgBox("Error opening database" & vbCrLf & Err.Description, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Debug info")
        sInfoString = sInfoString & vbCrLf & "NB! Error opening database " & BB_DatabasePath()
        Resume Next

errorSelecting:
        Me.Picture1.Visible = True ' Exlamationmark on
        MsgBox("Error finding versionno from database" & vbCrLf & Err.Description, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Debug info")
        sInfoString = sInfoString & vbCrLf & "NB! Error finding versionno from database " & BB_DatabasePath()
        Resume Next


    End Sub
    Private Sub cmdDLL_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDLL.Click

        ' �rsta er faen ikke en by

        On Error GoTo errorDLL
        Me.Picture1.Visible = False

        ' Find versioninfo of our .dlls
        sInfoString = ""
        'If RunTime() Then   'DISKUTERNOTRUNTIME Dette blir feil i 2017
        sInfoString = sInfoString & System.Diagnostics.FileVersionInfo.GetVersionInfo(My.Application.Info.DirectoryPath & "\BabelBank.exe").ToString & vbCrLf
        sInfoString = sInfoString & System.Diagnostics.FileVersionInfo.GetVersionInfo(My.Application.Info.DirectoryPath & "\vbbabel.dll").ToString & vbCrLf
        sInfoString = sInfoString & System.Diagnostics.FileVersionInfo.GetVersionInfo(My.Application.Info.DirectoryPath & "\babelSetup.dll").ToString & vbCrLf
        'Else
        'sInfoString = System.Diagnostics.FileVersionInfo.GetVersionInfo("C:\Projects.net\BabelBank\BabelBankEXE\bin\BabelBankExe.exe").ToString & vbCrLf
        'sInfoString = sInfoString & System.Diagnostics.FileVersionInfo.GetVersionInfo("C:\Projects.net\BabelBank\Bin\vbbabel.dll").ToString & vbCrLf
        'sInfoString = sInfoString & System.Diagnostics.FileVersionInfo.GetVersionInfo("C:\Projects.net\BabelBank\BabelSetup\bin\BabelSetup.dll").ToString & vbCrLf
        'End If
        ' Present info in form:
        Me.txtInfoString.Text = sInfoString
        Exit Sub

errorDLL:
        MsgBox("Error retreving versioninfo for DLL's", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Debug info")
        Resume Next

    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.Hide()
    End Sub
    Private Sub cmdSystemInfo_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSystemInfo.Click
        ' Find systeminfo like operating system version

        On Error GoTo errorSysInfo
        Me.Picture1.Visible = False

        sInfoString = ""

        ' Get the Environment.OSVersion.
        sInfoString = sInfoString & "Windows version: " & My.Computer.Info.OSFullName & vbCrLf
        sInfoString = sInfoString & "Windows version number: " & My.Computer.Info.OSVersion & vbCrLf
        sInfoString = sInfoString & "Windows build: " & Environment.OSVersion.Version.Build.ToString & vbCrLf
        sInfoString = sInfoString & "Windows servicepack " & Environment.OSVersion.Version.Revision.ToString & vbCrLf
        sInfoString = sInfoString & "Windows platform ID " & My.Computer.Info.OSPlatform & vbCrLf
        'Environment.Version returns CLR version only. 
        sInfoString = sInfoString & "CLR version: " & System.Environment.Version.ToString & vbCrLf
        On Error GoTo 0

        ' Present info in form:
        Me.txtInfoString.Text = sInfoString

        Exit Sub

errorSysInfo:
        MsgBox("Error retreving system information", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Debug info")
        Resume Next


    End Sub

    Private Sub frmDebug_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load

        ' Must be done before we show eMail form
        FormvbStyle(Me, "dollar.jpg")
        FormLRSCaptions(Me)

    End Sub

End Class
