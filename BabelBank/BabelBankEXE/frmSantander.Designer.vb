<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSantander
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdSearch As System.Windows.Forms.Button
	Public WithEvents optBOSF As System.Windows.Forms.RadioButton
	Public WithEvents optVILE As System.Windows.Forms.RadioButton
	Public WithEvents frameSystem As System.Windows.Forms.GroupBox
	Public WithEvents lstResult As System.Windows.Forms.ListBox
	Public WithEvents lblResultNumber As System.Windows.Forms.Label
	Public WithEvents lblResultLastUsed As System.Windows.Forms.Label
	Public WithEvents lblResultAccount As System.Windows.Forms.Label
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents cmdClose As System.Windows.Forms.Button
	Public WithEvents txtOrgPersnr As System.Windows.Forms.TextBox
	Public WithEvents txtRegnr As System.Windows.Forms.TextBox
	Public WithEvents txtSaksnummer As System.Windows.Forms.TextBox
	Public WithEvents lblRegnr As System.Windows.Forms.Label
	Public WithEvents lblOrgPersnr As System.Windows.Forms.Label
	Public WithEvents lblSaksnummer As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSantander))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.cmdSearch = New System.Windows.Forms.Button
		Me.frameSystem = New System.Windows.Forms.GroupBox
		Me.optBOSF = New System.Windows.Forms.RadioButton
		Me.optVILE = New System.Windows.Forms.RadioButton
		Me.Frame1 = New System.Windows.Forms.GroupBox
		Me.lstResult = New System.Windows.Forms.ListBox
		Me.lblResultNumber = New System.Windows.Forms.Label
		Me.lblResultLastUsed = New System.Windows.Forms.Label
		Me.lblResultAccount = New System.Windows.Forms.Label
		Me.cmdClose = New System.Windows.Forms.Button
		Me.txtOrgPersnr = New System.Windows.Forms.TextBox
		Me.txtRegnr = New System.Windows.Forms.TextBox
		Me.txtSaksnummer = New System.Windows.Forms.TextBox
		Me.lblRegnr = New System.Windows.Forms.Label
		Me.lblOrgPersnr = New System.Windows.Forms.Label
		Me.lblSaksnummer = New System.Windows.Forms.Label
		Me.frameSystem.SuspendLayout()
		Me.Frame1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.Text = "Santander - Hent kontonummer"
		Me.ClientSize = New System.Drawing.Size(444, 385)
		Me.Location = New System.Drawing.Point(8, 28)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmSantander"
		Me.cmdSearch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdSearch.Text = "&S�k"
		Me.cmdSearch.Size = New System.Drawing.Size(73, 25)
		Me.cmdSearch.Location = New System.Drawing.Point(280, 352)
		Me.cmdSearch.TabIndex = 5
		Me.cmdSearch.BackColor = System.Drawing.SystemColors.Control
		Me.cmdSearch.CausesValidation = True
		Me.cmdSearch.Enabled = True
		Me.cmdSearch.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdSearch.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdSearch.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdSearch.TabStop = True
		Me.cmdSearch.Name = "cmdSearch"
		Me.frameSystem.Text = "System"
		Me.frameSystem.Size = New System.Drawing.Size(393, 41)
		Me.frameSystem.Location = New System.Drawing.Point(40, 48)
		Me.frameSystem.TabIndex = 11
		Me.frameSystem.BackColor = System.Drawing.SystemColors.Control
		Me.frameSystem.Enabled = True
		Me.frameSystem.ForeColor = System.Drawing.SystemColors.ControlText
		Me.frameSystem.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.frameSystem.Visible = True
		Me.frameSystem.Padding = New System.Windows.Forms.Padding(0)
		Me.frameSystem.Name = "frameSystem"
		Me.optBOSF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optBOSF.Text = "BOSF"
		Me.optBOSF.Size = New System.Drawing.Size(113, 17)
		Me.optBOSF.Location = New System.Drawing.Point(176, 16)
		Me.optBOSF.TabIndex = 1
		Me.optBOSF.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optBOSF.BackColor = System.Drawing.SystemColors.Control
		Me.optBOSF.CausesValidation = True
		Me.optBOSF.Enabled = True
		Me.optBOSF.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optBOSF.Cursor = System.Windows.Forms.Cursors.Default
		Me.optBOSF.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optBOSF.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optBOSF.TabStop = True
		Me.optBOSF.Checked = False
		Me.optBOSF.Visible = True
		Me.optBOSF.Name = "optBOSF"
		Me.optVILE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optVILE.Text = "VILE"
		Me.optVILE.Size = New System.Drawing.Size(129, 17)
		Me.optVILE.Location = New System.Drawing.Point(16, 16)
		Me.optVILE.TabIndex = 0
		Me.optVILE.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optVILE.BackColor = System.Drawing.SystemColors.Control
		Me.optVILE.CausesValidation = True
		Me.optVILE.Enabled = True
		Me.optVILE.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optVILE.Cursor = System.Windows.Forms.Cursors.Default
		Me.optVILE.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optVILE.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optVILE.TabStop = True
		Me.optVILE.Checked = False
		Me.optVILE.Visible = True
		Me.optVILE.Name = "optVILE"
		Me.Frame1.Text = "Resultat"
		Me.Frame1.Size = New System.Drawing.Size(401, 153)
		Me.Frame1.Location = New System.Drawing.Point(32, 184)
		Me.Frame1.TabIndex = 10
		Me.Frame1.BackColor = System.Drawing.SystemColors.Control
		Me.Frame1.Enabled = True
		Me.Frame1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Visible = True
		Me.Frame1.Padding = New System.Windows.Forms.Padding(0)
		Me.Frame1.Name = "Frame1"
		Me.lstResult.Size = New System.Drawing.Size(369, 98)
		Me.lstResult.Location = New System.Drawing.Point(16, 44)
		Me.lstResult.TabIndex = 12
		Me.lstResult.TabStop = False
		Me.lstResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lstResult.BackColor = System.Drawing.SystemColors.Window
		Me.lstResult.CausesValidation = True
		Me.lstResult.Enabled = True
		Me.lstResult.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lstResult.IntegralHeight = True
		Me.lstResult.Cursor = System.Windows.Forms.Cursors.Default
		Me.lstResult.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.lstResult.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lstResult.Sorted = False
		Me.lstResult.Visible = True
		Me.lstResult.MultiColumn = False
		Me.lstResult.Name = "lstResult"
		Me.lblResultNumber.Text = "Antall ganger brukt"
		Me.lblResultNumber.Size = New System.Drawing.Size(105, 17)
		Me.lblResultNumber.Location = New System.Drawing.Point(232, 24)
		Me.lblResultNumber.TabIndex = 15
		Me.lblResultNumber.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblResultNumber.BackColor = System.Drawing.SystemColors.Control
		Me.lblResultNumber.Enabled = True
		Me.lblResultNumber.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblResultNumber.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblResultNumber.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblResultNumber.UseMnemonic = True
		Me.lblResultNumber.Visible = True
		Me.lblResultNumber.AutoSize = False
		Me.lblResultNumber.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblResultNumber.Name = "lblResultNumber"
		Me.lblResultLastUsed.Text = "Dato sist brukt"
		Me.lblResultLastUsed.Size = New System.Drawing.Size(105, 17)
		Me.lblResultLastUsed.Location = New System.Drawing.Point(120, 24)
		Me.lblResultLastUsed.TabIndex = 14
		Me.lblResultLastUsed.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblResultLastUsed.BackColor = System.Drawing.SystemColors.Control
		Me.lblResultLastUsed.Enabled = True
		Me.lblResultLastUsed.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblResultLastUsed.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblResultLastUsed.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblResultLastUsed.UseMnemonic = True
		Me.lblResultLastUsed.Visible = True
		Me.lblResultLastUsed.AutoSize = False
		Me.lblResultLastUsed.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblResultLastUsed.Name = "lblResultLastUsed"
		Me.lblResultAccount.Text = "Bankkontonr"
		Me.lblResultAccount.Size = New System.Drawing.Size(89, 17)
		Me.lblResultAccount.Location = New System.Drawing.Point(16, 24)
		Me.lblResultAccount.TabIndex = 13
		Me.lblResultAccount.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblResultAccount.BackColor = System.Drawing.SystemColors.Control
		Me.lblResultAccount.Enabled = True
		Me.lblResultAccount.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblResultAccount.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblResultAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblResultAccount.UseMnemonic = True
		Me.lblResultAccount.Visible = True
		Me.lblResultAccount.AutoSize = False
		Me.lblResultAccount.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblResultAccount.Name = "lblResultAccount"
		Me.cmdClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdClose.Text = "&Lukk"
		Me.cmdClose.Size = New System.Drawing.Size(73, 25)
		Me.cmdClose.Location = New System.Drawing.Point(360, 352)
		Me.cmdClose.TabIndex = 6
		Me.cmdClose.BackColor = System.Drawing.SystemColors.Control
		Me.cmdClose.CausesValidation = True
		Me.cmdClose.Enabled = True
		Me.cmdClose.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdClose.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdClose.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdClose.TabStop = True
		Me.cmdClose.Name = "cmdClose"
		Me.txtOrgPersnr.AutoSize = False
		Me.txtOrgPersnr.Size = New System.Drawing.Size(134, 19)
		Me.txtOrgPersnr.Location = New System.Drawing.Point(200, 126)
		Me.txtOrgPersnr.TabIndex = 3
		Me.txtOrgPersnr.AcceptsReturn = True
		Me.txtOrgPersnr.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtOrgPersnr.BackColor = System.Drawing.SystemColors.Window
		Me.txtOrgPersnr.CausesValidation = True
		Me.txtOrgPersnr.Enabled = True
		Me.txtOrgPersnr.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtOrgPersnr.HideSelection = True
		Me.txtOrgPersnr.ReadOnly = False
		Me.txtOrgPersnr.Maxlength = 0
		Me.txtOrgPersnr.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtOrgPersnr.MultiLine = False
		Me.txtOrgPersnr.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtOrgPersnr.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtOrgPersnr.TabStop = True
		Me.txtOrgPersnr.Visible = True
		Me.txtOrgPersnr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtOrgPersnr.Name = "txtOrgPersnr"
		Me.txtRegnr.AutoSize = False
		Me.txtRegnr.Size = New System.Drawing.Size(134, 19)
		Me.txtRegnr.Location = New System.Drawing.Point(200, 150)
		Me.txtRegnr.TabIndex = 4
		Me.txtRegnr.AcceptsReturn = True
		Me.txtRegnr.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtRegnr.BackColor = System.Drawing.SystemColors.Window
		Me.txtRegnr.CausesValidation = True
		Me.txtRegnr.Enabled = True
		Me.txtRegnr.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtRegnr.HideSelection = True
		Me.txtRegnr.ReadOnly = False
		Me.txtRegnr.Maxlength = 0
		Me.txtRegnr.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtRegnr.MultiLine = False
		Me.txtRegnr.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtRegnr.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtRegnr.TabStop = True
		Me.txtRegnr.Visible = True
		Me.txtRegnr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtRegnr.Name = "txtRegnr"
		Me.txtSaksnummer.AutoSize = False
		Me.txtSaksnummer.Size = New System.Drawing.Size(134, 19)
		Me.txtSaksnummer.Location = New System.Drawing.Point(200, 102)
		Me.txtSaksnummer.TabIndex = 2
		Me.txtSaksnummer.AcceptsReturn = True
		Me.txtSaksnummer.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSaksnummer.BackColor = System.Drawing.SystemColors.Window
		Me.txtSaksnummer.CausesValidation = True
		Me.txtSaksnummer.Enabled = True
		Me.txtSaksnummer.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSaksnummer.HideSelection = True
		Me.txtSaksnummer.ReadOnly = False
		Me.txtSaksnummer.Maxlength = 0
		Me.txtSaksnummer.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSaksnummer.MultiLine = False
		Me.txtSaksnummer.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSaksnummer.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSaksnummer.TabStop = True
		Me.txtSaksnummer.Visible = True
		Me.txtSaksnummer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtSaksnummer.Name = "txtSaksnummer"
		Me.lblRegnr.Text = "Regnummer:"
		Me.lblRegnr.Size = New System.Drawing.Size(89, 15)
		Me.lblRegnr.Location = New System.Drawing.Point(40, 152)
		Me.lblRegnr.TabIndex = 9
		Me.lblRegnr.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblRegnr.BackColor = System.Drawing.SystemColors.Control
		Me.lblRegnr.Enabled = True
		Me.lblRegnr.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblRegnr.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblRegnr.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblRegnr.UseMnemonic = True
		Me.lblRegnr.Visible = True
		Me.lblRegnr.AutoSize = False
		Me.lblRegnr.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblRegnr.Name = "lblRegnr"
		Me.lblOrgPersnr.Text = "Organisasjons-/Personnummer:"
		Me.lblOrgPersnr.Size = New System.Drawing.Size(153, 15)
		Me.lblOrgPersnr.Location = New System.Drawing.Point(40, 128)
		Me.lblOrgPersnr.TabIndex = 8
		Me.lblOrgPersnr.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblOrgPersnr.BackColor = System.Drawing.SystemColors.Control
		Me.lblOrgPersnr.Enabled = True
		Me.lblOrgPersnr.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblOrgPersnr.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblOrgPersnr.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblOrgPersnr.UseMnemonic = True
		Me.lblOrgPersnr.Visible = True
		Me.lblOrgPersnr.AutoSize = False
		Me.lblOrgPersnr.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblOrgPersnr.Name = "lblOrgPersnr"
		Me.lblSaksnummer.Text = "Saksnummer:"
		Me.lblSaksnummer.Size = New System.Drawing.Size(89, 15)
		Me.lblSaksnummer.Location = New System.Drawing.Point(40, 104)
		Me.lblSaksnummer.TabIndex = 7
		Me.lblSaksnummer.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblSaksnummer.BackColor = System.Drawing.SystemColors.Control
		Me.lblSaksnummer.Enabled = True
		Me.lblSaksnummer.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblSaksnummer.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblSaksnummer.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblSaksnummer.UseMnemonic = True
		Me.lblSaksnummer.Visible = True
		Me.lblSaksnummer.AutoSize = False
		Me.lblSaksnummer.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblSaksnummer.Name = "lblSaksnummer"
		Me.Controls.Add(cmdSearch)
		Me.Controls.Add(frameSystem)
		Me.Controls.Add(Frame1)
		Me.Controls.Add(cmdClose)
		Me.Controls.Add(txtOrgPersnr)
		Me.Controls.Add(txtRegnr)
		Me.Controls.Add(txtSaksnummer)
		Me.Controls.Add(lblRegnr)
		Me.Controls.Add(lblOrgPersnr)
		Me.Controls.Add(lblSaksnummer)
		Me.frameSystem.Controls.Add(optBOSF)
		Me.frameSystem.Controls.Add(optVILE)
		Me.Frame1.Controls.Add(lstResult)
		Me.Frame1.Controls.Add(lblResultNumber)
		Me.Frame1.Controls.Add(lblResultLastUsed)
		Me.Frame1.Controls.Add(lblResultAccount)
		Me.frameSystem.ResumeLayout(False)
		Me.Frame1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
