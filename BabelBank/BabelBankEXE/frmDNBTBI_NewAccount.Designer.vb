﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDNBTBI_NewAccount
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdOK = New System.Windows.Forms.Button
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.lstClients = New System.Windows.Forms.ListBox
        Me.lblAccountNo = New System.Windows.Forms.Label
        Me.lblHead = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(186, 321)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 6
        Me.cmdOK.Text = "&Ok"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(262, 321)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.CmdCancel.TabIndex = 7
        Me.CmdCancel.Text = "55006 &Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'lstClients
        '
        Me.lstClients.FormattingEnabled = True
        Me.lstClients.Location = New System.Drawing.Point(29, 115)
        Me.lstClients.Name = "lstClients"
        Me.lstClients.Size = New System.Drawing.Size(306, 186)
        Me.lstClients.TabIndex = 4
        '
        'lblAccountNo
        '
        Me.lblAccountNo.AutoSize = True
        Me.lblAccountNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountNo.Location = New System.Drawing.Point(29, 87)
        Me.lblAccountNo.Name = "lblAccountNo"
        Me.lblAccountNo.Size = New System.Drawing.Size(205, 20)
        Me.lblAccountNo.TabIndex = 5
        Me.lblAccountNo.Text = "accountNo: 9999999999"
        '
        'lblHead
        '
        Me.lblHead.Location = New System.Drawing.Point(29, 52)
        Me.lblHead.Name = "lblHead"
        Me.lblHead.Size = New System.Drawing.Size(319, 30)
        Me.lblHead.TabIndex = 3
        Me.lblHead.Text = "60404-New account found. Connect account to client by selecting client in the lis" & _
            "t below."
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 315)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(350, 1)
        Me.lblLine1.TabIndex = 80
        Me.lblLine1.Text = "Label1"
        '
        'frmDNBTBI_NewAccount
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(376, 355)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.lstClients)
        Me.Controls.Add(Me.lblAccountNo)
        Me.Controls.Add(Me.lblHead)
        Me.Name = "frmDNBTBI_NewAccount"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents CmdCancel As System.Windows.Forms.Button
    Friend WithEvents lstClients As System.Windows.Forms.ListBox
    Friend WithEvents lblAccountNo As System.Windows.Forms.Label
    Friend WithEvents lblHead As System.Windows.Forms.Label
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
End Class
