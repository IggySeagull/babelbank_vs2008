Option Strict Off
Option Explicit On

Friend Class frmOmitAccounts
	Inherits System.Windows.Forms.Form
	
	Private bSave As Boolean
    Private oDal As vbBabel.DAL
    Private sErrorString As String
	Private bUpdateDone As Boolean
	Private bGetTheHellOut As Boolean
	
	
	Private Sub cmdAdd_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAdd.Click
		Dim bAccountExists As Boolean
		Dim iCounter As Short
		Dim sAccountNo As String
		Dim bUpdateTable As Boolean
		Dim bInsert As Boolean
		Dim bAutoMatching As Boolean
		Dim bAfterMatching As Boolean
		
		bUpdateTable = False
		bInsert = True
		bAutoMatching = False
		bAfterMatching = False
		
		If EmptyString((Me.txtAccount.Text)) Then
			MsgBox(LRS(48021), MsgBoxStyle.Information + MsgBoxStyle.OKOnly, LRS(48022))
			'MsgBox "You must enter an accountnumber in the textbox to the left.", vbOKOnly, "Account missing"
		Else
			sAccountNo = Trim(Me.txtAccount.Text)
			
			bAccountExists = False
			For iCounter = 0 To Me.lstAutomaticMatching.Items.Count - 1
				Me.lstAutomaticMatching.SelectedIndex = iCounter
				If sAccountNo = Me.lstAutomaticMatching.Text Then
					If Me.chkAutoMatching.CheckState = System.Windows.Forms.CheckState.Checked Then
						MsgBox(LRS(48023), MsgBoxStyle.Information + MsgBoxStyle.OKOnly, LRS(48024))
						'MsgBox "The account is already stated to be omitted from automatic matching.", vbInformation + vbOKOnly, "Account exists"
						bAccountExists = True
					Else
						bAccountExists = True
						bInsert = False
					End If
					Exit For
				End If
			Next iCounter
			If Not bAccountExists Then
				If Me.chkAutoMatching.CheckState = System.Windows.Forms.CheckState.Checked Then
					Me.lstAutomaticMatching.Items.Add((sAccountNo))
					bUpdateTable = True
					bAutoMatching = True
				End If
			End If
			
			bAccountExists = False
			For iCounter = 0 To Me.lstAfterMatching.Items.Count - 1
				Me.lstAfterMatching.SelectedIndex = iCounter
				If sAccountNo = Me.lstAfterMatching.Text Then
					If Me.chkAfterMatching.CheckState = System.Windows.Forms.CheckState.Checked Then
						MsgBox(LRS(48025), MsgBoxStyle.Information + MsgBoxStyle.OKOnly, LRS(48024))
						'MsgBox "The account is already stated to be omitted from aftermatching.", vbInformation + vbOKOnly, "Account exists"
						bAccountExists = True
					Else
						bAccountExists = True
						bInsert = False
					End If
				End If
			Next iCounter
			If Not bAccountExists Then
				If Me.chkAfterMatching.CheckState = System.Windows.Forms.CheckState.Checked Then
					Me.lstAfterMatching.Items.Add((sAccountNo))
					bUpdateTable = True
					bAfterMatching = True
				End If
			End If
			
			If Me.chkAutoMatching.CheckState = System.Windows.Forms.CheckState.UnChecked And Me.chkAfterMatching.CheckState = System.Windows.Forms.CheckState.UnChecked Then
				MsgBox(LRS(48026), MsgBoxStyle.Information + MsgBoxStyle.OKOnly, LRS(48027))
				'MsgBox "You have to choose in which module of BabelBank to omit the account.", vbOKOnly, "Pick a module"
			Else
				If bUpdateTable Then
					DoTheUpdate(sAccountNo, bInsert, bAutoMatching, bAfterMatching)
                    Me.txtAccount.Text = ""
                End If
            End If
        End If

    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click

        If bUpdateDone Then
            If MsgBox(LRS(48020), MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, Replace(LRS(55006), "&", "")) = MsgBoxResult.Yes Then
                oDal.TEST_Rollback()
                'oBBConn.RollbackTrans
                bGetTheHellOut = True
                Me.Close()
            Else
                'Nada to do
            End If
        Else
            bGetTheHellOut = True
            Me.Close()
        End If

    End Sub
    Private Sub frmOmitAccounts_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason

        If bGetTheHellOut Then
            'Unload Me
        Else
            If bUpdateDone Then
                If MsgBox(LRS(48020), MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, Replace(LRS(55006), "&", "")) = MsgBoxResult.Yes Then
                    oDal.TEST_Rollback()
                    Me.Close()
                Else
                    Cancel = 1
                End If
            Else
                Me.Close()
            End If
        End If
        eventArgs.Cancel = Cancel
    End Sub
	
	Private Sub cmdDeleteAfter_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDeleteAfter.Click
		Dim sMySQL As String
		Dim sAccountNo As String
		Dim bDelete As Boolean
		Dim iCounter As Short
		
		sAccountNo = Me.lstAfterMatching.Text
		
		If Not EmptyString(sAccountNo) Then
			bDelete = True
			For iCounter = 0 To Me.lstAutomaticMatching.Items.Count - 1
				Me.lstAutomaticMatching.SelectedIndex = iCounter
				If sAccountNo = Me.lstAutomaticMatching.Text Then
					bDelete = False
					Exit For
				End If
			Next iCounter
			
			If bDelete Then
				sMySQL = "DELETE FROM OmitAccounts WHERE Company_ID = 1 AND AccountNo = '" & sAccountNo & "'"
            Else
                sMySQL = "UPDATE OmitAccounts SET After = False WHERE Company_ID = 1 AND AccountNo = '" & sAccountNo & "'"
            End If

            oDal.SQL = sMySQL

            If oDal.ExecuteNonQuery Then
                If oDal.RecordsAffected = 1 Then

                Else
                    Throw New Exception("No records affected" & vbCrLf & oDal.ErrorMessage)
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oDal.ErrorMessage)
            End If

			Me.lstAfterMatching.Items.RemoveAt((Me.lstAfterMatching.SelectedIndex))
		End If
		
	End Sub
	
	Private Sub cmdDeleteAuto_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDeleteAuto.Click
		Dim sMySQL As String
		Dim sAccountNo As String
		Dim bDelete As Boolean
		Dim iCounter As Short
		
		sAccountNo = Me.lstAutomaticMatching.Text
		
		If Not EmptyString(sAccountNo) Then
			bDelete = True
			For iCounter = 0 To Me.lstAfterMatching.Items.Count - 1
				Me.lstAfterMatching.SelectedIndex = iCounter
				If sAccountNo = Me.lstAfterMatching.Text Then
					bDelete = False
					Exit For
				End If
			Next iCounter
			
			If bDelete Then
				sMySQL = "DELETE FROM OmitAccounts WHERE Company_ID = 1 AND AccountNo = '" & sAccountNo & "'"
                bUpdateDone = True
			Else
				sMySQL = "UPDATE OmitAccounts SET Automatic = False WHERE Company_ID = 1 AND AccountNo = '" & sAccountNo & "'"
                bUpdateDone = True
			End If
			
            oDal.SQL = sMySQL

            If oDal.ExecuteNonQuery Then
                If oDal.RecordsAffected = 1 Then

                Else
                    Throw New Exception("No records affected" & vbCrLf & oDal.ErrorMessage)
                End If
            Else
                Throw New Exception(LRSCommon(45002) & vbCrLf & oDal.ErrorMessage)
            End If

            Me.lstAutomaticMatching.Items.RemoveAt((Me.lstAutomaticMatching.SelectedIndex))
		End If
		
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		
        oDal.TEST_CommitTrans()
		bGetTheHellOut = True
		Me.Close()
		
	End Sub
	
	Private Sub cmdUse_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUse.Click
		
        oDal.TEST_CommitTrans()
        oDal.TEST_BeginTrans()
		
		bUpdateDone = False
		
	End Sub
	
	Private Sub frmOmitAccounts_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim ctl As System.Windows.Forms.Control
        Try
            FormvbStyle(Me, "dollar.jpg")
            FormLRSCaptions(Me)

            For Each ctl In Me.Controls
                If TypeName(ctl) = "PictureBox" Then
                    If ctl.Left = 0 And ctl.Top = 0 Then
                        ' 28.12.2010 Top-image, "VB-band" is positioned at 0,0 - this is the best test I have found so far !
                        Me.lblHeading.Location = New Point(180, 18)
                        Me.lblHeading.BackColor = Color.Transparent
                        Me.lblHeading.Parent = ctl
                    End If
                End If
            Next

            oDal = New vbBabel.DAL
            oDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
            oDal.TEST_UseCommitFromOutside = True
            If Not oDal.ConnectToDB() Then
                Throw New System.Exception(oDal.ErrorMessage)
            End If
            oDal.TEST_BeginTrans()

        Catch ex As Exception

            If Not oDal Is Nothing Then
                oDal.Close()
                oDal = Nothing
            End If

            Throw New Exception("Function: frmOmitAccounts.Load" & vbCrLf & ex.Message)

        End Try
        'oBBConn.BeginTrans()

        bUpdateDone = False
        bGetTheHellOut = False

    End Sub
    Private Sub txtAccount_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAccount.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000

        If KeyCode = System.Windows.Forms.Keys.Return Then
            cmdAdd_Click(cmdAdd, New System.EventArgs())
        End If

    End Sub
	Private Sub DoTheUpdate(ByRef sAccountNo As String, ByRef bInsert As Boolean, ByRef bAutoMatching As Boolean, ByRef bAfterMatching As Boolean)
		Dim sMySQL As String
		
		If bInsert Then
			sMySQL = "INSERT INTO OmitAccounts (Company_ID, AccountNo, Automatic, After) VALUES (1, '"
			sMySQL = sMySQL & sAccountNo & "', " & Str(bAutoMatching) & ", " & Str(bAfterMatching) & ")"
		Else
			sMySQL = "UPDATE OmitAccounts SET "
			If bAutoMatching Then
				If bAfterMatching Then
					sMySQL = sMySQL & "Automatic = True AND After = True "
				Else
					sMySQL = sMySQL & "Automatic = True "
				End If
			ElseIf bAfterMatching Then 
				sMySQL = sMySQL & "After = True "
			Else
				Exit Sub
			End If
			sMySQL = sMySQL & "WHERE Company_ID = 1 AND AccountNo = '" & sAccountNo & "'"
			
		End If
		
        oDal.SQL = sMySQL

        If oDal.ExecuteNonQuery Then
            If oDal.RecordsAffected = 1 Then

            Else
                Throw New Exception("No records affected" & vbCrLf & oDal.ErrorMessage)
            End If
        Else
            Throw New Exception(LRSCommon(45002) & vbCrLf & oDal.ErrorMessage)
        End If

		bUpdateDone = True
		
	End Sub
End Class
