<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmPasswordStatistics
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtPWConfirm As System.Windows.Forms.TextBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents txtPWNew As System.Windows.Forms.TextBox
	Public WithEvents txtPWOld As System.Windows.Forms.TextBox
	Public WithEvents lblConfirmPW As System.Windows.Forms.Label
    Public WithEvents lblNewPW As System.Windows.Forms.Label
	Public WithEvents lblOldPW As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtPWConfirm = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.txtPWNew = New System.Windows.Forms.TextBox
        Me.txtPWOld = New System.Windows.Forms.TextBox
        Me.lblConfirmPW = New System.Windows.Forms.Label
        Me.lblNewPW = New System.Windows.Forms.Label
        Me.lblOldPW = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtPWConfirm
        '
        Me.txtPWConfirm.AcceptsReturn = True
        Me.txtPWConfirm.BackColor = System.Drawing.SystemColors.Window
        Me.txtPWConfirm.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPWConfirm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPWConfirm.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtPWConfirm.Location = New System.Drawing.Point(382, 163)
        Me.txtPWConfirm.MaxLength = 0
        Me.txtPWConfirm.Name = "txtPWConfirm"
        Me.txtPWConfirm.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPWConfirm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPWConfirm.Size = New System.Drawing.Size(107, 20)
        Me.txtPWConfirm.TabIndex = 2
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(416, 200)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 6
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(336, 200)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 5
        Me.cmdCancel.Text = "55006 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'txtPWNew
        '
        Me.txtPWNew.AcceptsReturn = True
        Me.txtPWNew.BackColor = System.Drawing.SystemColors.Window
        Me.txtPWNew.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPWNew.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPWNew.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtPWNew.Location = New System.Drawing.Point(382, 136)
        Me.txtPWNew.MaxLength = 0
        Me.txtPWNew.Name = "txtPWNew"
        Me.txtPWNew.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPWNew.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPWNew.Size = New System.Drawing.Size(107, 20)
        Me.txtPWNew.TabIndex = 1
        '
        'txtPWOld
        '
        Me.txtPWOld.AcceptsReturn = True
        Me.txtPWOld.BackColor = System.Drawing.SystemColors.Window
        Me.txtPWOld.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPWOld.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPWOld.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.txtPWOld.Location = New System.Drawing.Point(382, 109)
        Me.txtPWOld.MaxLength = 0
        Me.txtPWOld.Name = "txtPWOld"
        Me.txtPWOld.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPWOld.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPWOld.Size = New System.Drawing.Size(107, 20)
        Me.txtPWOld.TabIndex = 0
        '
        'lblConfirmPW
        '
        Me.lblConfirmPW.BackColor = System.Drawing.SystemColors.Control
        Me.lblConfirmPW.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblConfirmPW.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblConfirmPW.Location = New System.Drawing.Point(184, 163)
        Me.lblConfirmPW.Name = "lblConfirmPW"
        Me.lblConfirmPW.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblConfirmPW.Size = New System.Drawing.Size(193, 17)
        Me.lblConfirmPW.TabIndex = 7
        Me.lblConfirmPW.Text = "62056 - Confirm password"
        '
        'lblNewPW
        '
        Me.lblNewPW.BackColor = System.Drawing.SystemColors.Control
        Me.lblNewPW.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNewPW.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNewPW.Location = New System.Drawing.Point(184, 136)
        Me.lblNewPW.Name = "lblNewPW"
        Me.lblNewPW.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNewPW.Size = New System.Drawing.Size(193, 17)
        Me.lblNewPW.TabIndex = 4
        Me.lblNewPW.Text = "62055 - New password"
        '
        'lblOldPW
        '
        Me.lblOldPW.BackColor = System.Drawing.SystemColors.Control
        Me.lblOldPW.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblOldPW.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblOldPW.Location = New System.Drawing.Point(184, 109)
        Me.lblOldPW.Name = "lblOldPW"
        Me.lblOldPW.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblOldPW.Size = New System.Drawing.Size(193, 17)
        Me.lblOldPW.TabIndex = 3
        Me.lblOldPW.Text = "62054 - Old password"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 192)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(475, 1)
        Me.lblLine1.TabIndex = 82
        Me.lblLine1.Text = "Label1"
        '
        'frmPasswordStatistics
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(500, 228)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.txtPWConfirm)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.txtPWNew)
        Me.Controls.Add(Me.txtPWOld)
        Me.Controls.Add(Me.lblConfirmPW)
        Me.Controls.Add(Me.lblNewPW)
        Me.Controls.Add(Me.lblOldPW)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmPasswordStatistics"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "55036 - Password statistics"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
