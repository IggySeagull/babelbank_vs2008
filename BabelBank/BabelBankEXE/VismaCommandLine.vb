Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic

'<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Visma versjonen lages i Visual Studio 2017, IKKE i VS2008 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
'<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< NB! INGEN ENDRINGER SKAL GJ�RES I DENNE KLASSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
'<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Visma versjonen lages i Visual Studio 2017, IKKE i VS2008 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
'******************************************************************************************************************************
Friend Class VismaCommandLine
	Dim OnBits(31) As Integer
	Dim bExport As Boolean
	Dim bImport As Boolean
	Dim sActiveCompany As String
	Dim sSystemDatabase As String
	Dim sBBDatabase As String
	Dim sUser As String
	Dim sUID As String
	Dim sPWD As String
	Dim sActivePaymentLine As String
	Dim sVersion As String
	Dim sServer As String
	Dim sInstance As String
	' added 21.05.2014
	Dim sODBCName As String
	' Extracts the different elements of the commandline passed from Visma Business
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	'Private Sub Class_Initialize_Renamed()
	'	' Nye kall fra Visma;
	'	' 24.06.2013 alle kall fra Visma Business har n� VB; foran det vi hadde f�r
	'	'VB;/U;F9998;vbsys; system;sa;Visma123;EddieBr-w7;CurrentVersion;;7
	'	'VB;/D;F10001;vbsys;"system";"sa";"visma";"VSWN-SC-OSL-025";;67
	'	'VB;/D;F9998;vbsys;system;sa;Visma123;EddieBr-w7;CurrentVersion
	'	'VB;/S;F9998;vbsys;system;sa;Visma123
	'	If xDelim(VB.Command(), ";", 2) = "/U" Then
	'		bExport = True '/U = Upload
	'	End If
	'	If xDelim(VB.Command(), ";", 2) = "/D" Then
	'		bImport = True '/D = Download
	'	End If
	'	sActiveCompany = xDelim(VB.Command(), ";", 3, Chr(34))
	'	'MsgBox "sActiveCompany " & sActiveCompany
	'	sSystemDatabase = xDelim(VB.Command(), ";", 4, Chr(34))
	'	'MsgBox "sSystemDatabase " & sSystemDatabase
	'	sBBDatabase = "BabelBank_" & sSystemDatabase
	'	'MsgBox "sBBDatabase " & sBBDatabase

	'	sUser = xDelim(VB.Command(), ";", 5, Chr(34))
	'	'MsgBox "sUser " & sUser
	'	sUID = xDelim(VB.Command(), ";", 6, Chr(34))
	'	'MsgBox "sUID " & sUID
	'	sPWD = xDelim(VB.Command(), ";", 7, Chr(34))
	'	'MsgBox "sPWD " & sPWD
	'	sServer = xDelim(VB.Command(), ";", 8, Chr(34))
	'	'MsgBox "sServer " & sServer
	'	sInstance = xDelim(VB.Command(), ";", 9, Chr(34))
	'	'MsgBox "sInstance " & sInstance
	'	' 05.07.2013 - det er lagt p� enda en parameter fra VBus, version, pos 10, som gj�r at
	'	' activepaymentline kommer som 11
	'	sVersion = xDelim(VB.Command(), ";", 10, Chr(34))
	'	'MsgBox "sVersion" & sVersion
	'	sActivePaymentLine = xDelim(VB.Command(), ";", 11, Chr(34))
	'	'MsgBox "sActivePaymentLine " & sActivePaymentLine

	'	' added 21.05.2014
	'	sODBCName = xDelim(VB.Command(), ";", 12, Chr(34))
	'	'MsgBox "sODBCName " & sODBCName

	'       If Not EmptyString(sODBCName) Then

	'           'test, scramble;
	'           'sPWD = Scramble("Visma123")
	'           'sPWD = Scramble("xxxxxx")   '3113A352B6B626B625B6E6

	'           ' if value passed in parameter 12, ODBCName, then password, parameter 7, is scrambled
	'           ' Unscramble
	'           ' REMOVE NEXT
	'           'MsgBox "(TEST) ODBC = " & sODBCName & vbCrLf & "PWD = " & sPWD & vbCrLf & "We need to unscramble"
	'           ' Lagt inn 29.08.2014, for � forhindre unscrambling av blanke passord ved Win NT autentisering

	'           If Not EmptyString(sPWD) Then

	'               'MsgBox("sPWD har en verdi f�r unscramble: " & sPWD)
	'               'sPWD = Unscramble(sPWD)
	'               'sPWD = VismaUnsrcambler.Descramble(sPWD)
	'               'sPWD = vismaunscambler.descramble(sPWD)

	'               '13.09.2016 Uses new Descambler from Mattias H�gstr�m in Visma
	'               sPWD = Visma.Business.Core.Interop.BabelStream.Read(sPWD)



	'               'Dim e1 = "66A69686F6E6D6C63626B1A39385B19B"
	'               'Dim e2 = String.Empty
	'               'Dim e3 = "41A7B2D3E29227B3734353C7B282F2932353C7B2F333A2F7B3E23383E3E3F287B2F333E7B373E353C2F337B343D7B3A7B353429363A377B2B3A28282C34293F6A214"
	'               'Dim d1 = Visma.Business.Core.Interop.BabelStream.Read(e1)
	'               'Dim d2 = Visma.Business.Core.Interop.BabelStream.Read(e2)
	'               'Dim d3 = Visma.Business.Core.Interop.BabelStream.Read(e3)

	'               'If Date.Today = StringToDate("20160829") Then
	'               'sPWD = "visma123@"
	'               'sPWD = "Cehegin*9886"
	'               'End If

	'               'MsgBox("sPWD har en verdi etter unscramble: " & sPWD)
	'           End If

	'           'MsgBox "Unscrambled PWD " & sPWD
	'           'MsgBox "(TEST) Unscrambled PWD = " & sPWD
	'       End If

	'End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	Public ReadOnly Property IsExport() As Boolean
		Get
			IsExport = bExport
		End Get
	End Property
	Public ReadOnly Property IsImport() As Boolean
		Get
			IsImport = bImport
		End Get
	End Property
	Public ReadOnly Property ActiveCompany() As String
		Get
			ActiveCompany = sActiveCompany
		End Get
	End Property
	Public ReadOnly Property SystemDatabase() As String
		Get
			SystemDatabase = sSystemDatabase
		End Get
	End Property
	Public ReadOnly Property BBDatabase() As String
		Get
			BBDatabase = sBBDatabase
		End Get
	End Property
	Public ReadOnly Property User() As String
		Get
			User = sUser
		End Get
	End Property
	Public ReadOnly Property UID() As String
		Get
			UID = sUID
		End Get
	End Property
	Public ReadOnly Property PWD() As String
		Get
			PWD = sPWD
		End Get
	End Property
	Public ReadOnly Property ActivePaymentLine() As String
		Get
			ActivePaymentLine = sActivePaymentLine
		End Get
	End Property
	Public ReadOnly Property Version() As String
		Get
			Version = sVersion
		End Get
	End Property
	Public ReadOnly Property Server() As String
		Get
			Server = sServer
		End Get
	End Property
	Public ReadOnly Property Instance() As String
		Get
			Instance = sInstance
		End Get
	End Property
	' added 21.05.2014
	Public ReadOnly Property ODBCName() As String
		Get
			ODBCName = sODBCName
		End Get
	End Property
	
	' 21.05.2014
	' Rest of module is to support UnScrambling of sPWD (parameter 7)
	Public Sub MakeOnBits()
		Dim j As Short
		Dim v As Integer
		For j = 0 To 30
			v = v + (2 ^ j)
			OnBits(j) = v
		Next j
		OnBits(j) = v + &H80000000
		
	End Sub
	Public Function LShiftLong(ByVal Value As Integer, ByVal Shift As Short) As Integer
		MakeOnBits()
		If (Value And (2 ^ (31 - Shift))) Then GoTo OverFlow
		LShiftLong = (CShort(Value And OnBits(31 - Shift)) * (2 ^ Shift))
		Exit Function
OverFlow: 
		LShiftLong = (CShort(Value And OnBits(31 - (Shift + 1))) * (2 ^ (Shift))) Or &H80000000
	End Function
	Public Function RShiftLong(ByVal Value As Integer, ByVal Shift As Short) As Integer
		Dim hi As Integer
		Dim OnBits(31) As Integer
		MakeOnBits()
		If (Value And &H80000000) Then hi = &H40000000
		
		RShiftLong = (Value And &H7FFFFFFE) \ (2 ^ Shift)
		RShiftLong = (RShiftLong Or (hi \ (2 ^ (Shift - 1))))
	End Function
	'UPGRADE_NOTE: str was upgraded to str_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Function HexStringToByteArray(ByRef str_Renamed As String) As Byte()
		Dim i, counter As Short
		Dim byteArr() As Byte
		If str_Renamed <> vbNullString Then
			ReDim byteArr((Len(str_Renamed) / 2) - 1)
			For i = 1 To Len(str_Renamed) Step 2
				byteArr(counter) = CDbl(Val("&H" & Mid(str_Renamed, i, 2)))
				counter = counter + 1
			Next i
			HexStringToByteArray = VB6.CopyArray(byteArr)
		End If
	End Function
	
	'UPGRADE_NOTE: str was upgraded to str_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Function Scramble(ByRef str_Renamed As String) As String
		Scramble = ""
		
		Dim iLen As Short
		iLen = Len(str_Renamed)
		
		If (iLen = 0) Then
			Exit Function
		End If
		
		Dim byteStream() As Byte
		ReDim byteStream((iLen + 3 - 1))
		
		Dim checksum As Integer
		Dim codeByte As Short
		codeByte = LShiftLong(iLen, 4) And &HFF
		checksum = 0
		
		Dim byteCode() As Byte
		'UPGRADE_ISSUE: Constant vbFromUnicode was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
        'UPGRADE_TODO: Code was upgraded to use System.Text.UnicodeEncoding.Unicode.GetBytes() which may not have the same behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="93DD716C-10E3-41BE-A4A8-3BA40157905B"'

        ' VISMA_TODO - sjekk med Visma hvordan vi b�r gj�re dette !
        'byteCode = System.Text.UnicodeEncoding.Unicode.GetBytes(StrConv(str_Renamed, vbFromUnicode))
        byteCode = System.Text.UnicodeEncoding.Unicode.GetBytes(StrConv(str_Renamed, 0))
		
		Dim i As Short
		Dim c As Byte
		
		For i = 0 To UBound(byteCode)
			
			c = byteCode(i)
			codeByte = codeByte Or RShiftLong(c, 4)
			byteStream(i) = codeByte
			checksum = checksum + codeByte
			codeByte = LShiftLong(c, 4) And &HFF
		Next 
		codeByte = codeByte Or RShiftLong(iLen And &HF0, 4)
		checksum = checksum + codeByte
		byteStream(i) = codeByte
		
		i = i + 1
		codeByte = RShiftLong(checksum, 8)
		byteStream(i) = codeByte
		
		i = i + 1
		codeByte = checksum And &HFF
		byteStream(i) = codeByte
		
		Dim s As String
		s = ""
		For i = 0 To UBound(byteStream)
			byteStream(i) = byteStream(i) Xor &HB5
			s = s & Hex(byteStream(i))
		Next 
		
		Scramble = s
		
	End Function
	'UPGRADE_NOTE: str was upgraded to str_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Function Unscramble(ByRef str_Renamed As String) As String
		Unscramble = ""
		Dim iLen As Short
		iLen = Len(str_Renamed)
		If ((iLen Mod 2) <> 0) Then
			Exit Function
		End If
		
		Dim byteCode() As Byte
		byteCode = HexStringToByteArray(str_Renamed)
		
		Dim byteCodeLen As Short
		byteCodeLen = iLen / 2
		
		Dim i As Short
		
		For i = 0 To byteCodeLen - 1 Step 1
			byteCode(i) = byteCode(i) Xor &HB5
		Next i
		
		Dim internalLen As Integer
		
		internalLen = RShiftLong(byteCode(0), 4) Or (LShiftLong(byteCode(byteCodeLen - 3), 4) And &HFF)
		
		Dim storedChecksum As Integer
		storedChecksum = (LShiftLong(byteCode(byteCodeLen - 2), 8) And &HFF00) Or byteCode(byteCodeLen - 1)
		
		
		If (internalLen <> (byteCodeLen - 3)) Then
			Exit Function
		End If
		
		
		
		Dim byteStream() As Byte
		'UPGRADE_WARNING: Lower bound of array byteStream was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		ReDim byteStream(internalLen)
		
		
		Dim checksum As Integer
		checksum = byteCode(0)
		Dim codeByte As Short
		
		codeByte = LShiftLong(byteCode(0), 4) And &HFF
		Dim n As Short
		For n = 1 To internalLen - 1 Step 1
			codeByte = codeByte Or (RShiftLong(byteCode(n), 4) And &HFF)
			checksum = checksum + byteCode(n)
			byteStream(n) = codeByte
			codeByte = LShiftLong(byteCode(n), 4) And &HFF
		Next n
		
		codeByte = codeByte Or (RShiftLong(byteCode(n), 4) And &HFF)
		checksum = checksum + byteCode(n)
		byteStream(n) = codeByte
		
		'If (checksum = storedChecksum) Then
		'UPGRADE_ISSUE: Constant vbUnicode was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
        'VISMA_TODO - sjekk med Visma hvordan vi b�r gj�re dette
        'Unscramble = StrConv(System.Text.UnicodeEncoding.Unicode.GetString(byteStream), vbUnicode)
        Unscramble = StrConv(System.Text.UnicodeEncoding.Unicode.GetString(byteStream), 0)
		
		'End If
		
    End Function

	'============================================================================================================
	' DENNE ER FRA Visual Studio 2017, og er den som brukes. Vi bruker IKKE NOE fra Visual Studio 2008 for Visma
	'============================================================================================================
	Private Sub Class_Initialize_Renamed()

		Dim keyPattern1 As String = "448C9344" ' Keep safe
		Dim KeyPattern2 As String = "BCEF-4EC9-AAE2-49716BE5193A"  ' Keep safe
		Dim bWinAuth As Boolean = False
		Dim bBeforeVismaBusiness1410 As Boolean = False

		' 22.09.2021 - NY, uten ODBC, uten bruker og passord
		' --------------------------------------------------
		'VB;/S;F0001;vbsys;deprecated;deprecated;JP_ASUS\;CurrentVersion;16.1.0;;

		' Nye kall fra og med Visma Business 14.10 (05.11.2019)
		'VB;/S;F0001;vbsys; system;deprecated;deprecated;JP_ASUS\;CurrentVersion;9.2.3042;;VismaBusiness

		' Gamle kall fra Visma;
		' 24.06.2013 alle kall fra Visma Business har n� VB; foran det vi hadde f�r
		'VB;/S;F0001;vbsys; system;system;30D3228363A6A69685B6B2;JP_ASUS\;CurrentVersion;9.2.3042;;VismaBusiness
		'VB;/U;F0001;vbsys; system;system;30D3228363A6A69685B6B2;JP_ASUS\;CurrentVersion;9.2.3042;;VismaBusiness
		'VB;/D;F0001;vbsys; system;system;30D3228363A6A69685B6B2;JP_ASUS\;CurrentVersion;9.2.3042;;VismaBusiness
		'sUID = VB.Command()

		If xDelim(VB.Command(), ";", 2) = "/U" Then
			bExport = True '/U = Upload
		End If
		If xDelim(VB.Command(), ";", 2) = "/D" Then
			bImport = True '/D = Download
		End If
		sActiveCompany = xDelim(VB.Command(), ";", 3, Chr(34))
		sSystemDatabase = xDelim(VB.Command(), ";", 4, Chr(34))
		sBBDatabase = "BabelBank_" & sSystemDatabase
		sUser = xDelim(VB.Command(), ";", 5, Chr(34))
		sUID = xDelim(VB.Command(), ";", 6, Chr(34))
		sPWD = xDelim(VB.Command(), ";", 7, Chr(34))
		sServer = xDelim(VB.Command(), ";", 8, Chr(34))
		sInstance = xDelim(VB.Command(), ";", 9, Chr(34))
		sVersion = xDelim(VB.Command(), ";", 10, Chr(34))
		sActivePaymentLine = xDelim(VB.Command(), ";", 11, Chr(34))
		sODBCName = xDelim(VB.Command(), ";", 12, Chr(34))

		' 22.09.2021 - No ODBC from VismaBusiness 16.
		'If Not EmptyString(sODBCName) Then
		If sPWD <> "" Then
			' 30.10.2019 - From Visma Business 14, we can't use the Descrambler
			' from VB 14.10 and on, PWD in parameterstring is replaced by "deprecated"
			If sUID = "deprecated" Then
				'' Visma Business 14.10 or higher
				' find UID in BabelBankEXE.config
				sUID = ""
				sUID = System.Configuration.ConfigurationManager.AppSettings("VismaSQLUID")
				sPWD = ""
				' Find encrypted password in BabelBankEXE.config
				sPWD = System.Configuration.ConfigurationManager.AppSettings("VismaSQLPW")

				' F�rste gang s� har vi ikke noe lagret passord - og da m� vi sp�rre allerede her!!!!
				If System.Configuration.ConfigurationManager.AppSettings("VismaSQLWINAUTH") <> "TRUE" Then
					bWinAuth = False
					If EmptyString(sPWD) Then
                        'frmVisma_SQLPassword.ShowDialog()
						' Find UID in BabelBankEXE.config
						sUID = System.Configuration.ConfigurationManager.AppSettings("VismaSQLUID")
						' Find encrypted password in BabelBankEXE.config
						sPWD = System.Configuration.ConfigurationManager.AppSettings("VismaSQLPW")
						If System.Configuration.ConfigurationManager.AppSettings("VismaSQLWINAUTH") = "TRUE" Then
							bWinAuth = True
						Else
							bWinAuth = False
						End If
					End If
				Else
					bWinAuth = True
				End If
			Else
				' 19.08.2020 - added next lines
				' this is Visma Business 13.10 or earlier
				bBeforeVismaBusiness1410 = True
			End If

			If Not bWinAuth Then
				' decrypt password
				If bBeforeVismaBusiness1410 Then
					' 19.08.2020 - Use old descrambler from Mattias H�gstr�m to decrypt the password passed from commandline
					'MsgBox("Kryptert passord fra kommandolinje" & vbNewLine & sPWD)
					sPWD = Visma.Business.Core.Interop.BabelStream.Read(sPWD)
					'MsgBox("Ukryptert passord" & vbNewLine & sPWD)
				Else
                    'sPWD = Visma1410.TempNamespace.Backward.Match(sPWD, keyPattern1, KeyPattern2, System.Text.RegularExpressions.RegexOptions.Singleline)
				End If
			End If

		End If
		'End If

	End Sub
End Class