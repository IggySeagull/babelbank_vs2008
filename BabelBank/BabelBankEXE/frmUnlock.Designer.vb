<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmUnlock
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdRefresh As System.Windows.Forms.Button
	Public WithEvents lstUsers As System.Windows.Forms.ListBox
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents lblCustom As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdRefresh = New System.Windows.Forms.Button
        Me.lstUsers = New System.Windows.Forms.ListBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblCustom = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdRefresh
        '
        Me.cmdRefresh.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRefresh.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdRefresh.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdRefresh.Location = New System.Drawing.Point(208, 251)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdRefresh.Size = New System.Drawing.Size(81, 25)
        Me.cmdRefresh.TabIndex = 4
        Me.cmdRefresh.Text = "55020-Refresh"
        Me.cmdRefresh.UseVisualStyleBackColor = False
        '
        'lstUsers
        '
        Me.lstUsers.BackColor = System.Drawing.SystemColors.Window
        Me.lstUsers.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstUsers.Enabled = False
        Me.lstUsers.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstUsers.Location = New System.Drawing.Point(210, 157)
        Me.lstUsers.Name = "lstUsers"
        Me.lstUsers.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstUsers.Size = New System.Drawing.Size(259, 82)
        Me.lstUsers.TabIndex = 3
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(388, 252)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.cmdCancel.TabIndex = 1
        Me.cmdCancel.Text = "55006-Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(298, 251)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "55021-&Unlock"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblCustom
        '
        Me.lblCustom.BackColor = System.Drawing.SystemColors.Control
        Me.lblCustom.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCustom.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCustom.Location = New System.Drawing.Point(207, 66)
        Me.lblCustom.Name = "lblCustom"
        Me.lblCustom.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCustom.Size = New System.Drawing.Size(268, 88)
        Me.lblCustom.TabIndex = 2
        Me.lblCustom.Text = "60000 - Explain what to do before unlocking"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 245)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(460, 1)
        Me.lblLine1.TabIndex = 88
        Me.lblLine1.Text = "Label1"
        '
        'frmUnlock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(482, 282)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdRefresh)
        Me.Controls.Add(Me.lstUsers)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lblCustom)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUnlock"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "60184 - Force unlock"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
