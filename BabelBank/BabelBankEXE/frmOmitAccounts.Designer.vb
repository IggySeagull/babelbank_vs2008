<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmOmitAccounts
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdDeleteAfter As System.Windows.Forms.Button
	Public WithEvents cmdDeleteAuto As System.Windows.Forms.Button
	Public WithEvents cmdAdd As System.Windows.Forms.Button
	Public WithEvents cmdUse As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents chkAfterMatching As System.Windows.Forms.CheckBox
	Public WithEvents chkAutoMatching As System.Windows.Forms.CheckBox
	Public WithEvents txtAccount As System.Windows.Forms.TextBox
	Public WithEvents lstAfterMatching As System.Windows.Forms.ListBox
	Public WithEvents lstAutomaticMatching As System.Windows.Forms.ListBox
    Public WithEvents lblAccount As System.Windows.Forms.Label
	Public WithEvents lblAfterMatching As System.Windows.Forms.Label
	Public WithEvents lblAutomaticMatching As System.Windows.Forms.Label
	Public WithEvents lblExplaination As System.Windows.Forms.Label
	Public WithEvents lblHeading As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdDeleteAfter = New System.Windows.Forms.Button
        Me.cmdDeleteAuto = New System.Windows.Forms.Button
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.cmdUse = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.chkAfterMatching = New System.Windows.Forms.CheckBox
        Me.chkAutoMatching = New System.Windows.Forms.CheckBox
        Me.txtAccount = New System.Windows.Forms.TextBox
        Me.lstAfterMatching = New System.Windows.Forms.ListBox
        Me.lstAutomaticMatching = New System.Windows.Forms.ListBox
        Me.lblAccount = New System.Windows.Forms.Label
        Me.lblAfterMatching = New System.Windows.Forms.Label
        Me.lblAutomaticMatching = New System.Windows.Forms.Label
        Me.lblExplaination = New System.Windows.Forms.Label
        Me.lblHeading = New System.Windows.Forms.Label
        Me.lblLine1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cmdDeleteAfter
        '
        Me.cmdDeleteAfter.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDeleteAfter.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDeleteAfter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDeleteAfter.Location = New System.Drawing.Point(392, 328)
        Me.cmdDeleteAfter.Name = "cmdDeleteAfter"
        Me.cmdDeleteAfter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDeleteAfter.Size = New System.Drawing.Size(73, 21)
        Me.cmdDeleteAfter.TabIndex = 15
        Me.cmdDeleteAfter.Text = "55043 - Delete"
        Me.cmdDeleteAfter.UseVisualStyleBackColor = False
        '
        'cmdDeleteAuto
        '
        Me.cmdDeleteAuto.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDeleteAuto.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdDeleteAuto.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDeleteAuto.Location = New System.Drawing.Point(200, 328)
        Me.cmdDeleteAuto.Name = "cmdDeleteAuto"
        Me.cmdDeleteAuto.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDeleteAuto.Size = New System.Drawing.Size(73, 21)
        Me.cmdDeleteAuto.TabIndex = 14
        Me.cmdDeleteAuto.Text = "55043 - Delete"
        Me.cmdDeleteAuto.UseVisualStyleBackColor = False
        '
        'cmdAdd
        '
        Me.cmdAdd.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAdd.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAdd.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAdd.Location = New System.Drawing.Point(488, 358)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAdd.Size = New System.Drawing.Size(73, 21)
        Me.cmdAdd.TabIndex = 5
        Me.cmdAdd.Text = "55042 - Add"
        Me.cmdAdd.UseVisualStyleBackColor = False
        '
        'cmdUse
        '
        Me.cmdUse.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUse.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdUse.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdUse.Location = New System.Drawing.Point(488, 424)
        Me.cmdUse.Name = "cmdUse"
        Me.cmdUse.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdUse.Size = New System.Drawing.Size(73, 21)
        Me.cmdUse.TabIndex = 8
        Me.cmdUse.Text = "55041 - Use"
        Me.cmdUse.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(408, 424)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(73, 21)
        Me.cmdOK.TabIndex = 7
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(328, 424)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(73, 21)
        Me.cmdCancel.TabIndex = 6
        Me.cmdCancel.Text = "55006 - Avbryt"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'chkAfterMatching
        '
        Me.chkAfterMatching.BackColor = System.Drawing.SystemColors.Control
        Me.chkAfterMatching.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkAfterMatching.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAfterMatching.Location = New System.Drawing.Point(368, 384)
        Me.chkAfterMatching.Name = "chkAfterMatching"
        Me.chkAfterMatching.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAfterMatching.Size = New System.Drawing.Size(161, 17)
        Me.chkAfterMatching.TabIndex = 4
        Me.chkAfterMatching.Text = "55039 - Aftermatching"
        Me.chkAfterMatching.UseVisualStyleBackColor = False
        '
        'chkAutoMatching
        '
        Me.chkAutoMatching.BackColor = System.Drawing.SystemColors.Control
        Me.chkAutoMatching.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkAutoMatching.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAutoMatching.Location = New System.Drawing.Point(200, 384)
        Me.chkAutoMatching.Name = "chkAutoMatching"
        Me.chkAutoMatching.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAutoMatching.Size = New System.Drawing.Size(161, 17)
        Me.chkAutoMatching.TabIndex = 3
        Me.chkAutoMatching.Text = "50708 - Automatching"
        Me.chkAutoMatching.UseVisualStyleBackColor = False
        '
        'txtAccount
        '
        Me.txtAccount.AcceptsReturn = True
        Me.txtAccount.BackColor = System.Drawing.SystemColors.Window
        Me.txtAccount.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAccount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAccount.Location = New System.Drawing.Point(320, 359)
        Me.txtAccount.MaxLength = 0
        Me.txtAccount.Name = "txtAccount"
        Me.txtAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAccount.Size = New System.Drawing.Size(153, 20)
        Me.txtAccount.TabIndex = 2
        '
        'lstAfterMatching
        '
        Me.lstAfterMatching.BackColor = System.Drawing.SystemColors.Window
        Me.lstAfterMatching.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstAfterMatching.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstAfterMatching.Location = New System.Drawing.Point(392, 200)
        Me.lstAfterMatching.Name = "lstAfterMatching"
        Me.lstAfterMatching.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstAfterMatching.Size = New System.Drawing.Size(174, 121)
        Me.lstAfterMatching.Sorted = True
        Me.lstAfterMatching.TabIndex = 1
        '
        'lstAutomaticMatching
        '
        Me.lstAutomaticMatching.BackColor = System.Drawing.SystemColors.Window
        Me.lstAutomaticMatching.Cursor = System.Windows.Forms.Cursors.Default
        Me.lstAutomaticMatching.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lstAutomaticMatching.Location = New System.Drawing.Point(200, 200)
        Me.lstAutomaticMatching.Name = "lstAutomaticMatching"
        Me.lstAutomaticMatching.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lstAutomaticMatching.Size = New System.Drawing.Size(174, 121)
        Me.lstAutomaticMatching.Sorted = True
        Me.lstAutomaticMatching.TabIndex = 0
        '
        'lblAccount
        '
        Me.lblAccount.BackColor = System.Drawing.SystemColors.Control
        Me.lblAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAccount.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAccount.Location = New System.Drawing.Point(200, 360)
        Me.lblAccount.Name = "lblAccount"
        Me.lblAccount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAccount.Size = New System.Drawing.Size(102, 17)
        Me.lblAccount.TabIndex = 13
        Me.lblAccount.Text = "55040 - Accountno"
        '
        'lblAfterMatching
        '
        Me.lblAfterMatching.BackColor = System.Drawing.SystemColors.Control
        Me.lblAfterMatching.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAfterMatching.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAfterMatching.Location = New System.Drawing.Point(392, 176)
        Me.lblAfterMatching.Name = "lblAfterMatching"
        Me.lblAfterMatching.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAfterMatching.Size = New System.Drawing.Size(153, 17)
        Me.lblAfterMatching.TabIndex = 12
        Me.lblAfterMatching.Text = "55039 - AfterMatching"
        '
        'lblAutomaticMatching
        '
        Me.lblAutomaticMatching.BackColor = System.Drawing.SystemColors.Control
        Me.lblAutomaticMatching.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAutomaticMatching.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAutomaticMatching.Location = New System.Drawing.Point(200, 176)
        Me.lblAutomaticMatching.Name = "lblAutomaticMatching"
        Me.lblAutomaticMatching.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAutomaticMatching.Size = New System.Drawing.Size(129, 17)
        Me.lblAutomaticMatching.TabIndex = 11
        Me.lblAutomaticMatching.Text = "50708 - AutoMatching"
        '
        'lblExplaination
        '
        Me.lblExplaination.BackColor = System.Drawing.SystemColors.Control
        Me.lblExplaination.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblExplaination.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblExplaination.Location = New System.Drawing.Point(208, 64)
        Me.lblExplaination.Name = "lblExplaination"
        Me.lblExplaination.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblExplaination.Size = New System.Drawing.Size(305, 97)
        Me.lblExplaination.TabIndex = 10
        Me.lblExplaination.Text = "55038 - Explain"
        '
        'lblHeading
        '
        Me.lblHeading.BackColor = System.Drawing.SystemColors.Control
        Me.lblHeading.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHeading.Location = New System.Drawing.Point(216, 16)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHeading.Size = New System.Drawing.Size(249, 17)
        Me.lblHeading.TabIndex = 9
        Me.lblHeading.Text = "55037 - Omit accounts"
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLine1.Location = New System.Drawing.Point(10, 412)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(565, 1)
        Me.lblLine1.TabIndex = 82
        Me.lblLine1.Text = "Label1"
        '
        'frmOmitAccounts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(584, 457)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdDeleteAfter)
        Me.Controls.Add(Me.cmdDeleteAuto)
        Me.Controls.Add(Me.cmdAdd)
        Me.Controls.Add(Me.cmdUse)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.chkAfterMatching)
        Me.Controls.Add(Me.chkAutoMatching)
        Me.Controls.Add(Me.txtAccount)
        Me.Controls.Add(Me.lstAfterMatching)
        Me.Controls.Add(Me.lstAutomaticMatching)
        Me.Controls.Add(Me.lblAccount)
        Me.Controls.Add(Me.lblAfterMatching)
        Me.Controls.Add(Me.lblAutomaticMatching)
        Me.Controls.Add(Me.lblExplaination)
        Me.Controls.Add(Me.lblHeading)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(8, 28)
        Me.Name = "frmOmitAccounts"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "55037 - Omit accounts"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
#End Region 
End Class
