Option Strict Off
Option Explicit On
Friend Class frmExportInfo
	Inherits System.Windows.Forms.Form
	Dim bStatus As Boolean
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		bStatus = True
		' Check for filename;
        If txtFilename.Text = "" Then
            MsgBox(LRS(60037), MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "BabelBank") '"Du m� oppgi eksportfilnavn!"
            bStatus = False
        End If
		If lstFormats.SelectedIndex < 0 Then
			MsgBox(LRS(17007), MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "BabelBank") ' Du m� velge eksportformat
			bStatus = False
		End If
		If bStatus Then
			Hide()
		End If
	End Sub
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		bStatus = False
		Hide()
	End Sub
	Private Sub cmdFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFileOpen.Click
		' Hent filnavn
		Dim sPath As String
		'Dim sFile As String
		
		' Bruk BrowseForFolders, via rutine fra VBNet ( i Utils)
		'sPath = FolderPicker(Me, LRS(60017))
		' spath avsluttes med end of string - fjern denne!
		'sPath = Left$(RTrim(sPath), Len(RTrim(sPath)) - 1)
		'Me.txtFilename.Text = sPath
		
		' Changed 18.10.05
		' using a new function where we can pass a path
        'sPath = FixPath((Me.txtFilename.Text))
        ' changed 31.12.2010
        sPath = BrowseForFilesOrFolders(Me.txtFilename.Text, Me, LRS(60017), True)
		
        ' If browseforfilesorfolders returns empty path, then keep the old one;
		If Len(sPath) > 0 Then
			Me.txtFilename.Text = sPath
		End If
		
	End Sub
	Public Function Status() As Boolean
		Status = bStatus
	End Function
	Private Sub frmExportInfo_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		FormvbStyle(Me, "dollar.jpg")
	End Sub
End Class
