<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmNRXNameAddress
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmbCompanyType As System.Windows.Forms.ComboBox
	Public WithEvents cmbType As System.Windows.Forms.ComboBox
	Public WithEvents txtAddress1 As System.Windows.Forms.TextBox
	Public WithEvents txtAddress2 As System.Windows.Forms.TextBox
	Public WithEvents txtAddress3 As System.Windows.Forms.TextBox
	Public WithEvents txtZipCode As System.Windows.Forms.TextBox
	Public WithEvents txtCity As System.Windows.Forms.TextBox
	Public WithEvents txtName As System.Windows.Forms.TextBox
	Public WithEvents CmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdUse As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents lblCompanyType As System.Windows.Forms.Label
	Public WithEvents lblCity As System.Windows.Forms.Label
	Public WithEvents lblType As System.Windows.Forms.Label
	Public WithEvents lblZipCode As System.Windows.Forms.Label
	Public WithEvents lblAddress3 As System.Windows.Forms.Label
	Public WithEvents lblAddress2 As System.Windows.Forms.Label
	Public WithEvents lblAddress1 As System.Windows.Forms.Label
	Public WithEvents lblName As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmbCompanyType = New System.Windows.Forms.ComboBox
        Me.cmbType = New System.Windows.Forms.ComboBox
        Me.txtAddress1 = New System.Windows.Forms.TextBox
        Me.txtAddress2 = New System.Windows.Forms.TextBox
        Me.txtAddress3 = New System.Windows.Forms.TextBox
        Me.txtZipCode = New System.Windows.Forms.TextBox
        Me.txtCity = New System.Windows.Forms.TextBox
        Me.txtName = New System.Windows.Forms.TextBox
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.cmdUse = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblCompanyType = New System.Windows.Forms.Label
        Me.lblCity = New System.Windows.Forms.Label
        Me.lblType = New System.Windows.Forms.Label
        Me.lblZipCode = New System.Windows.Forms.Label
        Me.lblAddress3 = New System.Windows.Forms.Label
        Me.lblAddress2 = New System.Windows.Forms.Label
        Me.lblAddress1 = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtGender = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'cmbCompanyType
        '
        Me.cmbCompanyType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbCompanyType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbCompanyType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbCompanyType.Location = New System.Drawing.Point(456, 184)
        Me.cmbCompanyType.Name = "cmbCompanyType"
        Me.cmbCompanyType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbCompanyType.Size = New System.Drawing.Size(91, 21)
        Me.cmbCompanyType.TabIndex = 7
        '
        'cmbType
        '
        Me.cmbType.BackColor = System.Drawing.SystemColors.Window
        Me.cmbType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbType.Location = New System.Drawing.Point(280, 184)
        Me.cmbType.Name = "cmbType"
        Me.cmbType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbType.Size = New System.Drawing.Size(81, 21)
        Me.cmbType.TabIndex = 6
        '
        'txtAddress1
        '
        Me.txtAddress1.AcceptsReturn = True
        Me.txtAddress1.BackColor = System.Drawing.SystemColors.Window
        Me.txtAddress1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAddress1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAddress1.Location = New System.Drawing.Point(280, 88)
        Me.txtAddress1.MaxLength = 0
        Me.txtAddress1.Name = "txtAddress1"
        Me.txtAddress1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAddress1.Size = New System.Drawing.Size(267, 19)
        Me.txtAddress1.TabIndex = 1
        '
        'txtAddress2
        '
        Me.txtAddress2.AcceptsReturn = True
        Me.txtAddress2.BackColor = System.Drawing.SystemColors.Window
        Me.txtAddress2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAddress2.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAddress2.Location = New System.Drawing.Point(280, 112)
        Me.txtAddress2.MaxLength = 0
        Me.txtAddress2.Name = "txtAddress2"
        Me.txtAddress2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAddress2.Size = New System.Drawing.Size(267, 19)
        Me.txtAddress2.TabIndex = 2
        '
        'txtAddress3
        '
        Me.txtAddress3.AcceptsReturn = True
        Me.txtAddress3.BackColor = System.Drawing.SystemColors.Window
        Me.txtAddress3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAddress3.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAddress3.Location = New System.Drawing.Point(280, 136)
        Me.txtAddress3.MaxLength = 0
        Me.txtAddress3.Name = "txtAddress3"
        Me.txtAddress3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAddress3.Size = New System.Drawing.Size(267, 19)
        Me.txtAddress3.TabIndex = 3
        '
        'txtZipCode
        '
        Me.txtZipCode.AcceptsReturn = True
        Me.txtZipCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtZipCode.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtZipCode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtZipCode.Location = New System.Drawing.Point(280, 160)
        Me.txtZipCode.MaxLength = 0
        Me.txtZipCode.Name = "txtZipCode"
        Me.txtZipCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtZipCode.Size = New System.Drawing.Size(32, 19)
        Me.txtZipCode.TabIndex = 4
        '
        'txtCity
        '
        Me.txtCity.AcceptsReturn = True
        Me.txtCity.BackColor = System.Drawing.SystemColors.Window
        Me.txtCity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCity.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCity.Location = New System.Drawing.Point(380, 160)
        Me.txtCity.MaxLength = 0
        Me.txtCity.Name = "txtCity"
        Me.txtCity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCity.Size = New System.Drawing.Size(166, 19)
        Me.txtCity.TabIndex = 5
        '
        'txtName
        '
        Me.txtName.AcceptsReturn = True
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtName.Location = New System.Drawing.Point(280, 64)
        Me.txtName.MaxLength = 0
        Me.txtName.Name = "txtName"
        Me.txtName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtName.Size = New System.Drawing.Size(267, 19)
        Me.txtName.TabIndex = 0
        '
        'CmdCancel
        '
        Me.CmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.CmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CmdCancel.Location = New System.Drawing.Point(376, 238)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CmdCancel.Size = New System.Drawing.Size(81, 25)
        Me.CmdCancel.TabIndex = 9
        Me.CmdCancel.Text = "&Avbryt"
        Me.CmdCancel.UseVisualStyleBackColor = False
        '
        'cmdUse
        '
        Me.cmdUse.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUse.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdUse.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdUse.Location = New System.Drawing.Point(464, 238)
        Me.cmdUse.Name = "cmdUse"
        Me.cmdUse.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdUse.Size = New System.Drawing.Size(81, 25)
        Me.cmdUse.TabIndex = 10
        Me.cmdUse.Text = "&Bruk"
        Me.cmdUse.UseVisualStyleBackColor = False
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(288, 238)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(81, 25)
        Me.cmdOK.TabIndex = 8
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblCompanyType
        '
        Me.lblCompanyType.BackColor = System.Drawing.SystemColors.Control
        Me.lblCompanyType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompanyType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompanyType.Location = New System.Drawing.Point(376, 187)
        Me.lblCompanyType.Name = "lblCompanyType"
        Me.lblCompanyType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompanyType.Size = New System.Drawing.Size(81, 17)
        Me.lblCompanyType.TabIndex = 18
        Me.lblCompanyType.Text = "Type bedrift:"
        '
        'lblCity
        '
        Me.lblCity.BackColor = System.Drawing.SystemColors.Control
        Me.lblCity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCity.Location = New System.Drawing.Point(328, 164)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCity.Size = New System.Drawing.Size(65, 17)
        Me.lblCity.TabIndex = 17
        Me.lblCity.Text = "Poststed:"
        '
        'lblType
        '
        Me.lblType.BackColor = System.Drawing.SystemColors.Control
        Me.lblType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblType.Location = New System.Drawing.Point(208, 187)
        Me.lblType.Name = "lblType"
        Me.lblType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblType.Size = New System.Drawing.Size(65, 17)
        Me.lblType.TabIndex = 16
        Me.lblType.Text = "Persontype:"
        '
        'lblZipCode
        '
        Me.lblZipCode.BackColor = System.Drawing.SystemColors.Control
        Me.lblZipCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblZipCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblZipCode.Location = New System.Drawing.Point(208, 164)
        Me.lblZipCode.Name = "lblZipCode"
        Me.lblZipCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblZipCode.Size = New System.Drawing.Size(49, 17)
        Me.lblZipCode.TabIndex = 15
        Me.lblZipCode.Text = "Postnr.:"
        '
        'lblAddress3
        '
        Me.lblAddress3.BackColor = System.Drawing.SystemColors.Control
        Me.lblAddress3.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAddress3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAddress3.Location = New System.Drawing.Point(208, 140)
        Me.lblAddress3.Name = "lblAddress3"
        Me.lblAddress3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAddress3.Size = New System.Drawing.Size(70, 17)
        Me.lblAddress3.TabIndex = 14
        Me.lblAddress3.Text = "Adresse:"
        '
        'lblAddress2
        '
        Me.lblAddress2.BackColor = System.Drawing.SystemColors.Control
        Me.lblAddress2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAddress2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAddress2.Location = New System.Drawing.Point(208, 115)
        Me.lblAddress2.Name = "lblAddress2"
        Me.lblAddress2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAddress2.Size = New System.Drawing.Size(70, 17)
        Me.lblAddress2.TabIndex = 13
        Me.lblAddress2.Text = "Adresse:"
        '
        'lblAddress1
        '
        Me.lblAddress1.BackColor = System.Drawing.SystemColors.Control
        Me.lblAddress1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAddress1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAddress1.Location = New System.Drawing.Point(208, 90)
        Me.lblAddress1.Name = "lblAddress1"
        Me.lblAddress1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAddress1.Size = New System.Drawing.Size(70, 17)
        Me.lblAddress1.TabIndex = 12
        Me.lblAddress1.Text = "C/O Adresse:"
        '
        'lblName
        '
        Me.lblName.BackColor = System.Drawing.SystemColors.Control
        Me.lblName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblName.Location = New System.Drawing.Point(208, 66)
        Me.lblName.Name = "lblName"
        Me.lblName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblName.Size = New System.Drawing.Size(70, 17)
        Me.lblName.TabIndex = 11
        Me.lblName.Text = "Navn:"
        '
        'txtGender
        '
        Me.txtGender.AcceptsReturn = True
        Me.txtGender.BackColor = System.Drawing.SystemColors.Window
        Me.txtGender.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtGender.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtGender.Location = New System.Drawing.Point(457, 211)
        Me.txtGender.MaxLength = 0
        Me.txtGender.Name = "txtGender"
        Me.txtGender.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtGender.Size = New System.Drawing.Size(32, 20)
        Me.txtGender.TabIndex = 19
        '
        'frmNRXNameAddress
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(561, 275)
        Me.Controls.Add(Me.txtGender)
        Me.Controls.Add(Me.cmbCompanyType)
        Me.Controls.Add(Me.cmbType)
        Me.Controls.Add(Me.txtAddress1)
        Me.Controls.Add(Me.txtAddress2)
        Me.Controls.Add(Me.txtAddress3)
        Me.Controls.Add(Me.txtZipCode)
        Me.Controls.Add(Me.txtCity)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.cmdUse)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lblCompanyType)
        Me.Controls.Add(Me.lblCity)
        Me.Controls.Add(Me.lblType)
        Me.Controls.Add(Me.lblZipCode)
        Me.Controls.Add(Me.lblAddress3)
        Me.Controls.Add(Me.lblAddress2)
        Me.Controls.Add(Me.lblAddress1)
        Me.Controls.Add(Me.lblName)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 30)
        Me.Name = "frmNRXNameAddress"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Endre navn og adrese"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents txtGender As System.Windows.Forms.TextBox
#End Region 
End Class
