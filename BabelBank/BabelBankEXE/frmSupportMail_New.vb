﻿Option Strict Off
Option Explicit On
Friend Class frmSupportMail_New
    Dim oProfile As vbBabel.Profile
    Dim oFileSetup As vbBabel.FileSetup
    Dim aFilenameData() As String 'Array to store information about selected Shortname

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Me.DialogResult = 0
        Me.Hide()
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Me.DialogResult = 1
        Me.Hide()
    End Sub
    Private Sub frmSupportMail_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        FormvbStyle(Me, "dollar.jpg")
        'FormvbStyle(Me, "")
    End Sub
    Sub FillListboxes()
        Dim iCountFilenames As Short

        lstSendProfiles.Items.Clear()
        lstReturnprofiles.Items.Clear()

        'We don't need the array aFilenameData

        iCountFilenames = 0
        For Each oFilesetup In oProfile.FileSetups
            ReDim Preserve aFilenameData(iCountFilenames)
            If oFilesetup.FromAccountingSystem = True And oFilesetup.FileSetup_ID <> oFilesetup.FileSetupOut Then
                ' Don't show outpart of profiles which has only returnattributes (no sendprofile)
                If Not (oFilesetup.FileNameIn1 = "" And oFilesetup.FileNameIn2 = "" And oFilesetup.FileNameIn3 = "") Then
                    'lstSendProfiles.Items.Add(New VB6.ListBoxItem(oFilesetup.ShortName, oFilesetup.FileSetup_ID))
                    ' 12.07.2019
                    lstSendProfiles.Items.Add(New _MyListBoxItem(oFilesetup.ShortName, oFilesetup.FileSetup_ID))
                    aFilenameData(iCountFilenames) = oFilesetup.ShortName
                End If
            Else
                ' Don't show outpart of profiles which has only sendattributes (no returnprofile)
                If Not (oFilesetup.FileNameIn1 = "" And oFilesetup.FileNameIn2 = "" And oFilesetup.FileNameIn3 = "") Then
                    'lstReturnprofiles.Items.Add(New VB6.ListBoxItem(oFilesetup.ShortName, oFilesetup.FileSetup_ID))
                    lstReturnprofiles.Items.Add(New _MyListBoxItem(oFilesetup.ShortName, oFilesetup.FileSetup_ID))
                    aFilenameData(iCountFilenames) = oFilesetup.ShortName
                End If
            End If
            iCountFilenames = iCountFilenames + 1
        Next oFilesetup

        lstSendProfiles.Refresh()
        lstReturnprofiles.Refresh()

    End Sub


End Class