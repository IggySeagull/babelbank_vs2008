Option Strict Off
Option Explicit On
' Johannes# 
Module TreatKjell
    Dim oBabelFile As vbBabel.BabelFile
    Dim oBatches As vbBabel.Batches
    Dim oBatch As vbBabel.Batch
    Dim oPayment As vbBabel.Payment
    Dim oInvoice As vbBabel.Invoice
    Dim oFreeText As vbBabel.Freetext
    Dim oProfile As vbBabel.Profile
    Dim oFilesetup As vbBabel.FileSetup
    Dim oClient As vbBabel.Client
    Dim oaccount As vbBabel.Account

    Private Const InternalStatusNOTSET = 0
    Private Const InternalStatusIGNORE = 1
    Private Const InternalStatusREPORT = 2
    Private Const InternalStatusSTOP = 3

    Public Function TreatRiisBilglass(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef iFileSetup_ID As Object) As Boolean
        Dim oReport As vbBabel.Report
        Dim oBabelReport As vbBabel.BabelReport
        Dim oProfile As vbBabel.Profile
        Dim sAccountNo As String
        Dim bAccountFound As Boolean
        Dim sClientNo As String
        Dim sSpecialReportPath As String
        Dim sOriginalHeading As String
        Dim sTmp As String
        Dim j As Integer

        sClientNo = ""

        Try

            sAccountNo = ""
            For Each oBabelFile In oBabelFiles
                oProfile = oBabelFile.VB_Profile
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If IsOCR((oPayment.PayCode)) Then
                            If sAccountNo <> oPayment.I_Account Then
                                sAccountNo = oPayment.I_Account
                                bAccountFound = False
                                sClientNo = ""
                                For Each oFilesetup In oBabelFile.VB_Profile.FileSetups
                                    For Each oClient In oFilesetup.Clients
                                        For Each oaccount In oClient.Accounts
                                            If sAccountNo = oaccount.Account Then
                                                bAccountFound = True
                                                Exit For
                                            End If
                                        Next oaccount
                                        If bAccountFound Then
                                            sClientNo = oClient.ClientNo
                                            Exit For
                                        End If
                                    Next oClient
                                    If bAccountFound Then
                                        Exit For
                                    End If
                                Next oFilesetup
                            End If
                            If Not bAccountFound Then
                                Err.Raise(6284, "TreatRiisBilglass", "Kontonummer " & oPayment.I_Account & " ikke funnet i klientoppsettet" & vbCrLf & "Vennligst legg inn dette i klientoppsettet i BabelBank." & vbCrLf & vbCrLf & "Program avbrytes!")
                            End If
                            For Each oInvoice In oPayment.Invoices
                                If Not EmptyString((oInvoice.Unique_Id)) Then
                                    If Mid(Trim(oInvoice.Unique_Id), 4, 3) <> sClientNo Then
                                        oPayment.ToSpecialReport = True
                                        Exit For
                                    End If
                                End If
                            Next oInvoice
                            If oPayment.ToSpecialReport Then
                                oPayment.ToSpecialReport = False
                            Else
                                oPayment.PayCode = "599"
                            End If
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

            ' Present OCR-report 004
            oFilesetup = oProfile.FileSetups(iFileSetup_ID)
            oBabelReport = New vbBabel.BabelReport
            oReport = New vbBabel.Report
            For j = 1 To oFilesetup.Reports.Count
                If oFilesetup.Reports(j).ReportNo = 4 Then ' 950 Special_Report
                    oReport = oFilesetup.Reports(j)
                    sOriginalHeading = oReport.Heading
                    oReport.Heading = "Innbetalt til feil konto"
                    sSpecialReportPath = oReport.ExportFilename
                    ' make it possible to save to file.
                    ' Use path set in SetupWizard, set as Folder for reports, and add filename
                    ' based on Header of report !!!
                    If Len(oReport.ExportType) > 0 Then
                        ' Manipulate name of report;
                        ' but save it first
                        sTmp = oReport.ExportFilename
                        sTmp = ReplaceDateTimeStamp(Left(sTmp, InStrRev(sTmp, "\")) & "Feilinnbetalt" & "_%YMD_hms%." & oReport.ExportType)
                        oReport.ExportFilename = sTmp
                    End If

                    'TODO: BABELREPORT
                    'oReport.DetailLevel = 1
                    'oBabelReport.Report = oReport
                    'oBabelReport.BabelFiles = oBabelFiles

                    'oBabelReport.Start()
                    'oReport.ExportFilename = sSpecialReportPath
                    'oReport.Heading = sOriginalHeading
                    'UPGRADE_NOTE: Object oBabelReport may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oBabelReport = Nothing
                    'UPGRADE_NOTE: Object oReport may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    oReport = Nothing

                    Exit For
                End If
            Next j

            ' reset reportflag
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.PayCode = "599" Then
                            oPayment.PayCode = "510"
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally

            If Not oReport Is Nothing Then
                oReport = Nothing
            End If
            If Not oBabelReport Is Nothing Then
                oBabelReport = Nothing
            End If

        End Try

        TreatRiisBilglass = True


        Err.Raise(Err.Number, "TreatRiisBilglass", Err.Description)
        TreatRiisBilglass = False

    End Function

    Public Function TreatOdinKID(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef oProfile As vbBabel.Profile, ByRef iFileSetup_ID As Short, ByRef bLog As Boolean, ByRef oBabelLog As vbLog.vbLogging, ByRef oOwnerForm As System.Windows.Forms.Form, ByRef bSilent As Boolean) As Boolean
        'This function moves the copies the oInvoice.Unique_Id to oInvoice.MATCH_ID for payments with KID
        '

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If IsOCR((oPayment.PayCode)) Or IsAutogiro((oPayment.PayCode)) Then
                            For Each oInvoice In oPayment.Invoices
                                If Not EmptyString((oInvoice.Unique_Id)) Then
                                    oInvoice.Unique_Id = Trim(oInvoice.Unique_Id)
                                    '25.08.2010 - Removed next line
                                    'oInvoice.MATCH_ID = oInvoice.Unique_Id
                                End If
                            Next oInvoice
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatOdinKID = True

    End Function
    Public Function TreatDekkmann_KID(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile, ByVal iFileSetup_ID As Integer) As Boolean
        'XNET - 22.10.2010 - To seperate KID-payments that should be sent to Dekkmann and not Procasso
        Dim sKID As String
        Dim bSendToDekkmann As Boolean
        Dim oBatchProcasso As vbBabel.Batch
        Dim oBatchDekkmann As vbBabel.Batch
        Dim oPaymentProcasso As vbBabel.Payment
        Dim oPaymentDekkmann As vbBabel.Payment
        Dim lBatchCount As Long
        Dim i As Integer
        Dim iNoOfBatches As Integer
        Dim nAmount As Double
        Dim sClientName As String

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If IsOCR(oPayment.PayCode) Then
                            bSendToDekkmann = True
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.MATCH_Final Then
                                    sKID = oInvoice.Unique_Id
                                    'XokNET 16.05.2011 - Added next IF
                                    If Len(Trim$(sKID)) = 25 Then
                                        If Val(Mid$(sKID, 15, 10)) < 2000024951 Then
                                            'OK - send to Dekkmann
                                        Else
                                            'Send to Procasso
                                            bSendToDekkmann = False
                                        End If
                                    Else
                                        'Send to Procasso, f.eks. purre KID
                                        bSendToDekkmann = False
                                    End If
                                End If
                            Next oInvoice
                            If bSendToDekkmann Then
                                oPayment.I_Account = "60490529988"
                            End If
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

            'Reorganize the data - Splitt in new batches
            For Each oBabelFile In oBabelFiles
                iNoOfBatches = oBabelFile.Batches.Count
                For i = 1 To iNoOfBatches
                    oBatch = oBabelFile.Batches.Item(i)
                    oBatchProcasso = New vbBabel.Batch
                    CopyBatchObject(oBatch, oBatchProcasso)

                    'Delete the original batches in the new Batch
                    For lBatchCount = oBatchProcasso.Payments.Count To 1 Step -1
                        oPaymentProcasso = oBatchProcasso.Payments.Item(lBatchCount)
                        ' Delete marked payment
                        oBatchProcasso.Payments.Remove(lBatchCount)
                    Next lBatchCount

                    oBatchProcasso = oBabelFile.Batches.VB_AddWithObject(oBatchProcasso)

                    oBatchDekkmann = New vbBabel.Batch
                    CopyBatchObject(oBatchProcasso, oBatchDekkmann)
                    oBatchDekkmann = oBabelFile.Batches.VB_AddWithObject(oBatchDekkmann)

                    For Each oPayment In oBatch.Payments
                        If oPayment.I_Account = "60490529988" Then
                            oPayment.VB_ClientNo = "DEK"
                            oPaymentDekkmann = oBatchDekkmann.Payments.VB_AddWithObject(oPayment)
                        Else
                            oPaymentProcasso = oBatchProcasso.Payments.VB_AddWithObject(oPayment)
                        End If
                    Next oPayment

                    'Mark the batches to be deleted
                    oBatch.BGMString = "DELETE"
                Next i
            Next oBabelFile

            'Delete the original batches and set the new clientname in payment
            For Each oBabelFile In oBabelFiles
                For lBatchCount = oBabelFile.Batches.Count To 1 Step -1
                    oBatch = oBabelFile.Batches.Item(lBatchCount)
                    ' Delete marked payments
                    If oBatch.BGMString = "DELETE" Or oBatch.Payments.Count = 0 Then
                        oBabelFile.Batches.Remove(lBatchCount)
                    Else
                        'Calculate a new totalamount on batch
                        nAmount = 0
                        For Each oPayment In oBatch.Payments
                            nAmount = nAmount + oPayment.MON_InvoiceAmount
                        Next oPayment
                        oBatch.MON_InvoiceAmount = nAmount
                        oBatch.MON_TransferredAmount = nAmount
                    End If
                Next lBatchCount
            Next oBabelFile

            'Reorganize the indexes
            For Each oBabelFile In oBabelFiles
                i = 0
                For Each oBatch In oBabelFile.Batches
                    i = i + 1
                    oBatch.Index = i
                Next oBatch
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatDekkmann_KID = True

    End Function

    ' XokNET 03.05.2013 - taken away most parameters, as they are not in use, and other tings
    Public Function OrganizeBatchesInDomesticAndOriginal(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean  ', oProfile As Profile, iFilesetup_ID As Integer, sSpecial As String) As Boolean
        ' special for DnBNOR DK, instead for TBI
        ' also used for Visma
        Dim bDomestic As Boolean
        Dim bInt As Boolean
        Dim oDOMBatch As vbBabel.Batch
        Dim oTempBatch As vbBabel.Batch
        Dim i As Long
        Dim iOrgBatch As Integer  ' added 06.07.2010
        Dim iAddedBatch As Integer ' added 06.07.2010

        Try

            ' if mix of domestic and international, must organize into two batches

            ' Wrong - problems if more than one batch originally -
            ' Corrected 06.07.2010
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    bDomestic = False
                    bInt = False

                    ' added 06.07.2010
                    If oBatch.BGMString <> "ADDED" Then
                        iOrgBatch = oBatch.Index  ' added 06.07.2010

                        For Each oPayment In oBatch.Payments
                            If oPayment.PayType = "I" Then
                                bInt = True
                            Else
                                bDomestic = True
                            End If
                        Next oPayment
                        If bDomestic And bInt Then
                            ' both domestic and international in same batch, must split in two
                            'Set oTempBatch = oBabelFile.Batches(1)
                            oTempBatch = oBabelFile.Batches(iOrgBatch)
                            ' changed 06.07.2010
                            oDOMBatch = New vbBabel.Batch
                            ' Copy from first batch to second
                            CopyBatchObject(oTempBatch, oDOMBatch)
                            oDOMBatch = oBabelFile.Batches.VB_AddWithObject(oDOMBatch)
                            ' added 06.07.2010
                            iAddedBatch = oDOMBatch.Index
                            oDOMBatch.BGMString = "ADDED"  ' To mark as not "original" added 06.07.2010
                            ' XNET 08.05.2013 added paytype
                            oDOMBatch.PayType = "D" 'D = Domestic

                            ' delete Domestic from original batch
                            'For i = oBabelFile.Batches.Item(1).Payments.Count To 1 Step -1
                            ' changed 06.07.2010
                            For i = oBabelFile.Batches.Item(iOrgBatch).Payments.Count To 1 Step -1
                                'Set oPayment = oBabelFile.Batches.Item(1).Payments.Item(i)
                                ' XOKNET 16.12.2010 BOMBS here - severe error - changed from (1) to (iOrgBatch) - KJELL sjekk dette !!!
                                oPayment = oBabelFile.Batches.Item(iOrgBatch).Payments.Item(i)
                                If oPayment.PayType <> "I" Then
                                    ' XOKNET 16.12.2010 BOMBS here - severe error - changed from (1) to (iOrgBatch) - KJELL sjekk dette !!!
                                    'oBabelFile.Batches.Item(1).Payments.Remove i
                                    oBabelFile.Batches.Item(iOrgBatch).Payments.Remove(i)
                                End If
                            Next i

                            ' delete non Domestic from added batch
                            'For i = oBabelFile.Batches.Item(2).Payments.Count To 1 Step -1
                            ' changed 06.07.2010
                            For i = oBabelFile.Batches.Item(iAddedBatch).Payments.Count To 1 Step -1
                                'Set oPayment = oBabelFile.Batches.Item(2).Payments.Item(i)
                                oPayment = oBabelFile.Batches.Item(iAddedBatch).Payments.Item(i)
                                ' changed 06.07.2010
                                If oPayment.PayType = "I" Then
                                    'oBabelFile.Batches.Item(2).Payments.Remove i
                                    ' changed 06.07.2010
                                    oBabelFile.Batches.Item(iAddedBatch).Payments.Remove(i)
                                End If
                            Next i
                        End If

                    End If  'If oBatch.BGMString <> "ADDED" Then
                Next oBatch
            Next oBabelFile

            'Add correct amounts to the batchlevel and remove "ADDED" from BGMString
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    oBatch.BGMString = vbNullString
                    oBatch.AddInvoiceAmountOnBatch()
                    ' XNET 14.05.2013
                    If oBatch.Payments.Count > 0 Then
                        oBatch.PayType = oBatch.Payments.Item(1).PayType  ' D=Domestic, I=International
                    End If
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        OrganizeBatchesInDomesticAndOriginal = True

    End Function

    'XNET - New function
    Public Function TreatHusleiepartner(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        'Split the OCR-file in two!
        'NEW client
        'KID length 21
        'KID length 25 and starts with 0000
        'Old client
        'The rest

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If IsOCR(oPayment.PayCode) Or oPayment.PayCode = "530" Or oPayment.PayCode = "531" Or oPayment.PayCode = "532" Then
                            For Each oInvoice In oPayment.Invoices
                                ' New and old accountingprogram
                                ' Kid len 15 = New, otherwise old system

                                If Len(Trim$(oInvoice.Unique_Id)) = 21 Then
                                    oPayment.VB_ClientNo = "NEW"
                                Else
                                    If Len(Trim$(oInvoice.Unique_Id)) = 25 Then
                                        If Left$(Trim$(oInvoice.Unique_Id), 4) = "0000" Then
                                            oPayment.VB_ClientNo = "NEW"
                                        Else
                                            oPayment.VB_ClientNo = "OLD"
                                        End If
                                    Else
                                        oPayment.VB_ClientNo = "OLD"
                                    End If
                                End If
                            Next
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatHusleiepartner = True

    End Function
    'XNET -17.12.2010 - New function
    Public Function TreatCardoEntrance(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        'Made for Cardo Entrance Solutions Norway As
        'The KID is stated in the REF_Own field on the invoice
        'This functions runs through the collections and check if a REF_Own is stated
        'If YES copy the content of REF_Own to KID and correct the paytype to 301
        Dim bAllInvoicesAreKID As Boolean

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        bAllInvoicesAreKID = True
                        For Each oInvoice In oPayment.Invoices
                            If EmptyString(oInvoice.REF_Own) Then
                                bAllInvoicesAreKID = False
                            End If
                        Next
                        If bAllInvoicesAreKID Then
                            oPayment.PayCode = "301"
                            For Each oInvoice In oPayment.Invoices
                                oInvoice.Unique_Id = Trim$(oInvoice.REF_Own)
                                'Remove structured info
                                oInvoice.InvoiceNo = ""
                                oInvoice.CustomerNo = ""
                                oInvoice.InvoiceDate = ""
                            Next oInvoice
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatCardoEntrance = True

    End Function
    'XNET -13.01.2011 - New function
    Public Function TreatCuveco(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        'Made for Cardo Entrance Solutions Norway As
        'The KID is stated in the REF_Own field on the invoice
        'This functions runs through the collections and check if a REF_Own is stated
        'If YES copy the content of REF_Own to KID and correct the paytype to 301
        Dim bAllInvoicesAreKID As Boolean

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.PayCode = "629" Then
                            bAllInvoicesAreKID = True
                            For Each oInvoice In oPayment.Invoices
                                If EmptyString(oInvoice.Unique_Id) Then
                                    oInvoice.Unique_Id = oInvoice.InvoiceNo
                                    'bAllInvoicesAreKID = False
                                End If
                            Next

                            If bAllInvoicesAreKID Then
                                oPayment.PayCode = "510"
                            End If
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatCuveco = True

    End Function
    Public Function TreatSG_Leasing(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        'Made for SG Finans Leasing
        'This is a test of functionality that may be implemented
        'Som specific types of KID should only be reported

        Dim bAllInvoicesAreKID As Boolean

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.Invoices.Count = 1 Then
                            If IsOCR(oPayment.PayCode) Then
                                bAllInvoicesAreKID = True

                                For Each oInvoice In oPayment.Invoices
                                    If Left$(oInvoice.Unique_Id, 4) = "1884" Then
                                        oPayment.Exported = True
                                        oPayment.ToSpecialReport = True
                                        oInvoice.ToSpecialReport = True
                                    End If
                                Next

                            End If
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatSG_Leasing = True

    End Function
    'XokNET - 23.06.2011 - New function
    Public Function TreatSGFinans_Leasing(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        'Made for SG Finans Leasing
        'this function is used To be able to export KID-transactions on structured payments to an OCR-file,
        ' but only to leave the total payment posistive.

        Dim nKIDAmount As Double
        Dim nNotKIDAmount As Double
        Dim nNegativeAmount As Double
        Dim nTotalNegativeAmount As Double
        Dim nLargestKIDAmount As Double
        Dim lCounter As Long
        Dim lCounter2 As Long
        Dim lArrayIndexToUse As Long
        Dim bDoTheExchange As Boolean
        Dim bAllInvoicesAreKID As Boolean
        Dim bAtLeastOneInvoiceAreKID As Boolean
        Dim oInvoice As vbBabel.Invoice
        Dim oFreetext As vbBabel.Freetext

        Dim nTotalAmountWithoutKID As Double

        Dim aInvoiceArray(,) As Double
        'Element 0, - Amount
        'Element 1, - InvoiceIndex
        'Element 2, - Marker, 0 = Do not change, 1 = Change to KID

        Try

            nTotalAmountWithoutKID = 0

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.PayCode = "629" Then 'Structured payments

                            If oPayment.MON_InvoiceAmount > 0 Then
                                lCounter = -1
                                nKIDAmount = 0
                                nNotKIDAmount = 0
                                nNegativeAmount = 0
                                bDoTheExchange = False
                                bAllInvoicesAreKID = False
                                bAtLeastOneInvoiceAreKID = False

                                ReDim aInvoiceArray(2, oPayment.Invoices.Count - 1)

                                For Each oInvoice In oPayment.Invoices

                                    lCounter = lCounter + 1
                                    aInvoiceArray(0, lCounter) = oInvoice.MON_InvoiceAmount
                                    aInvoiceArray(1, lCounter) = oInvoice.Index



                                    If oInvoice.MON_InvoiceAmount < 0 Then
                                        aInvoiceArray(2, lCounter) = 0
                                        nNegativeAmount = nNegativeAmount + oInvoice.MON_InvoiceAmount
                                    Else
                                        If EmptyString(oInvoice.Unique_Id) Then
                                            aInvoiceArray(2, lCounter) = 0
                                            nNotKIDAmount = nNotKIDAmount + oInvoice.MON_InvoiceAmount
                                        Else
                                            nKIDAmount = nKIDAmount + oInvoice.MON_InvoiceAmount
                                            aInvoiceArray(2, lCounter) = 1
                                        End If
                                    End If


                                    '                        If EmptyString(oInvoice.Unique_Id) Then
                                    '                            aInvoiceArray(2, lCounter) = 0
                                    '                            If oInvoice.MON_InvoiceAmount < 0 Then
                                    '                                nNegativeAmount = nNegativeAmount + oInvoice.MON_InvoiceAmount
                                    '                            Else
                                    '                                nNotKIDAmount = nNotKIDAmount + oInvoice.MON_InvoiceAmount
                                    '                            End If
                                    '                        Else
                                    '                            nKIDAmount = nKIDAmount + oInvoice.MON_InvoiceAmount
                                    '                            aInvoiceArray(2, lCounter) = 1
                                    '                        End If

                                Next

                                If nNotKIDAmount - (nNegativeAmount * -1) > 0 Then
                                    If nKIDAmount > 0 Then
                                        'OK, just change the KID-transactions
                                        bDoTheExchange = True
                                        bAtLeastOneInvoiceAreKID = True
                                    Else
                                        'No KID-transactions
                                        bDoTheExchange = False
                                        'Mark the payment as exported because there are no KID-transactions in the payment
                                        oPayment.Exported = True
                                        nTotalAmountWithoutKID = nTotalAmountWithoutKID + oPayment.MON_InvoiceAmount
                                    End If
                                ElseIf IsEqualAmount(oPayment.MON_InvoiceAmount, nKIDAmount) Then
                                    bAtLeastOneInvoiceAreKID = True
                                    bAllInvoicesAreKID = True
                                    bDoTheExchange = True
                                Else
                                    bDoTheExchange = True
                                    lCounter = 0
                                    nTotalNegativeAmount = nNotKIDAmount + nNegativeAmount

                                    ' and remove the Mark
                                    Do While nTotalNegativeAmount < 0
                                        'Loop through the array to find the largest amount that's marked as a KID,

                                        nLargestKIDAmount = 0
                                        lArrayIndexToUse = -1
                                        For lCounter = 0 To UBound(aInvoiceArray, 2)
                                            If aInvoiceArray(2, lCounter) = 1 Then 'Marked as KID
                                                If aInvoiceArray(0, lCounter) > nLargestKIDAmount Then
                                                    lArrayIndexToUse = lCounter
                                                    nLargestKIDAmount = aInvoiceArray(0, lCounter)
                                                End If
                                            End If
                                        Next lCounter

                                        'Do some test to prevent an eternal loop if something unforeseen has happend
                                        If nLargestKIDAmount = 0 Then
                                            bDoTheExchange = False
                                            Exit Do
                                        End If
                                        If lArrayIndexToUse = -1 Then
                                            bDoTheExchange = False
                                            Exit Do
                                        End If

                                        aInvoiceArray(2, lArrayIndexToUse) = 0 'Remove the mark on the entry in the Array with the largest amount

                                        nTotalNegativeAmount = nTotalNegativeAmount + aInvoiceArray(0, lArrayIndexToUse)

                                    Loop

                                End If 'If nNotKIDAmount - nNegativeAmount > 0 Then

                                If bDoTheExchange Then

                                    'Loop through the array and change the invoices according to the array
                                    For lCounter = 0 To UBound(aInvoiceArray, 2)
                                        oInvoice = oPayment.Invoices.Item(aInvoiceArray(1, lCounter))
                                        If aInvoiceArray(2, lCounter) = 1 Then
                                            oInvoice.MATCH_Matched = True
                                            oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice
                                            oInvoice.MATCH_ID = oInvoice.Unique_Id
                                            oInvoice.CustomerNo = "EXPORTED AS OCR"
                                            oInvoice.InvoiceNo = "EXPORTED"
                                            For lCounter2 = oInvoice.Freetexts.Count To 1 Step -1
                                                oInvoice.Freetexts.Remove(oInvoice.Freetexts.Item(lCounter2).Index)
                                            Next lCounter2
                                            oFreetext = oInvoice.Freetexts.Add
                                            oFreetext.Text = "Posten er eksportert!"
                                            oFreetext = oInvoice.Freetexts.Add
                                            oFreetext.Text = "Bel�p:  " & PadLeft(ConvertFromAmountToString(oPayment.Invoices.Item(aInvoiceArray(1, lCounter)).MON_InvoiceAmount, ".", ","), 18, " ") & " / "
                                            bAtLeastOneInvoiceAreKID = True
                                        Else
                                            oInvoice.MATCH_Matched = False
                                            oInvoice.Exported = True
                                            nTotalAmountWithoutKID = nTotalAmountWithoutKID + oInvoice.MON_InvoiceAmount
                                        End If
                                    Next lCounter

                                    If bAllInvoicesAreKID Then
                                        oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched
                                    Else
                                        If bAtLeastOneInvoiceAreKID Then
                                            oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.PartlyMatched
                                        Else
                                            oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.NotMatched
                                            oPayment.Exported = True
                                        End If
                                    End If

                                End If


                            Else
                                'Mark the payment as exported
                                oPayment.Exported = True
                                nTotalAmountWithoutKID = nTotalAmountWithoutKID + oPayment.MON_InvoiceAmount

                            End If 'If oPayment.MON_InvoiceAmount > 0 Then

                        ElseIf IsOCR(oPayment.PayCode) Or IsAutogiro(oPayment.PayCode) Then
                            'Mark all KID-payments as matched
                            oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched
                            For Each oInvoice In oPayment.Invoices
                                oInvoice.MATCH_Matched = True
                                oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice
                                oInvoice.MATCH_ID = oInvoice.Unique_Id
                            Next oInvoice
                        Else
                            oPayment.Exported = True
                            nTotalAmountWithoutKID = nTotalAmountWithoutKID + oPayment.MON_InvoiceAmount
                        End If 'If oPayment.PayCode = "240" Then
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatSGFinans_Leasing = True

    End Function
    'XNET - 14.12.2011 - Whole function
    Public Function TreatConectoGLFile(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal sFilenameOut As String, ByVal bGLBank As Boolean, ByVal iFilesetup_ID As Integer, ByVal oFilesetup As vbBabel.FileSetup) As Boolean
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim bReturnValue As Boolean
        Dim nTotalAmount As Double
        Dim oaccount As vbBabel.Account
        Dim oClient As vbBabel.Client
        Dim bExportTheBatch As Boolean
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sInterimAccount As String, sGLAccount As String
        Dim sLineToWrite As String
        Dim nTotalUpdayAmount As Double
        Dim sUpdayAccount As String
        Dim bAllInvoicesMatchedAgainstGL As Boolean
        Dim sFreetext As String
        Dim oBackup As vbBabel.BabelFileHandling
        Dim bBackup As Boolean = False

        ' sGLLogOnString holds P�loggingsstreng Oracle. Must change =UID and =PWD. Use same UserID and Password as
        ' set in BabelBanks standard database logon

        Try
            bReturnValue = False

            ' Export records to Visma Business with GL-posting
            ' Update database, do not export to file


            nTotalAmount = 0
            sOldAccountNo = ""
            sLineToWrite = ""

            If bGLBank Then
                sFilenameOut = AddTextToFilename(sFilenameOut, "_Bank", True, False)
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            Else
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            End If

            ' Start by header-record;

            If bGLBank Then
                For Each oBabelFile In oBabelFiles

                    For Each oBatch In oBabelFile.Batches

                        If oBatch.Payments.Count > 0 Then

                            'Must set oBatch.REF_Bank to 10 positions because we use it as Groupnumber (which must be exactly 10 positions) in the OCR-export
                            If Len(oBatch.REF_Bank) > 10 Then
                                oBatch.REF_Bank = Right(oBatch.REF_Bank, 10)
                            ElseIf Len(oBatch.REF_Bank) < 10 Then
                                oBatch.REF_Bank = PadRight(oBatch.REF_Bank, 10, "0")
                            End If

                            If sOldAccountNo <> oBatch.Payments.Item(1).I_Account Then
                                bAccountFound = False
                                For Each oClient In oFilesetup.Clients
                                    For Each oaccount In oClient.Accounts
                                        If oBatch.Payments.Item(1).I_Account = oaccount.Account Then
                                            sGLAccount = Trim$(oaccount.GLAccount)
                                            sInterimAccount = Trim$(oaccount.GLResultsAccount)
                                            sOldAccountNo = oBatch.Payments.Item(1).I_Account
                                            bAccountFound = True

                                            Exit For
                                        End If
                                    Next oaccount
                                    If bAccountFound Then
                                        Exit For
                                    End If
                                Next oClient
                                'XNET 26.01.2012 - removed next line
                                'If bAccountFound Then Exit For

                            End If

                            bBackup = True
                            sLineToWrite = vbNullString
                            sLineToWrite = "1," 'Transtype
                            ' Entry date, posteringsdato
                            'According to the formatdescription the dateformat should be DDMMYY
                            '14.09.2007 - Changed the entry date from Now() to oPayment.DATE_Payment
                            '04.03.2016 - Changed back to NOW() - Petronelle Beichmann
                            sLineToWrite = sLineToWrite & Format(Date.Today, "dd.MM.yyyy") & ","
                            'sLineToWrite = sLineToWrite & Format(StringToDate(oBatch.Payments.Item(1).DATE_Payment), "dd.MM.yyyy") & ","
                            'VoucherNo
                            'sLineToWrite = sLineToWrite & Trim$(oBatch.Payments.Item(1).VoucherNo) & ","
                            sLineToWrite = sLineToWrite & ","
                            'VoucherDate
                            sLineToWrite = sLineToWrite & Format(StringToDate(oBatch.Payments.Item(1).DATE_Payment), "dd.MM.yyyy") & ","
                            'VoucherTypeNo  - should be a variable
                            sLineToWrite = sLineToWrite & "11,"
                            'VoucherText
                            If Len(oBatch.REF_Bank) > 40 Then
                                sLineToWrite = sLineToWrite & Chr(34) & "Bankref.: " & Left$(oBatch.REF_Bank, 40) & Chr(34) & ","
                            Else
                                sLineToWrite = sLineToWrite & Chr(34) & "Bankref.: " & Trim$(oBatch.REF_Bank) & Chr(34) & ","
                            End If
                            'Dep.no/Avdelingsnummer
                            'XNET - 26.01.2012 - Changed line below
                            sLineToWrite = sLineToWrite & ","
                            'sLineToWrite = sLineToWrite & oClient.ClientNo & ","
                            'ProjectNo - not in use
                            sLineToWrite = sLineToWrite & "0,"
                            'Debit AccountNo
                            sLineToWrite = sLineToWrite & sGLAccount & ","
                            'CreditAccountNo
                            sLineToWrite = sLineToWrite & "0,"
                            'VAT Code
                            sLineToWrite = sLineToWrite & "0,"
                            'CurrencyNo
                            sLineToWrite = sLineToWrite & "0,"
                            'ExchangeRate
                            sLineToWrite = sLineToWrite & "0.000000,"
                            'ExchangeAmount
                            sLineToWrite = sLineToWrite & "0.000000,"
                            'Amount
                            sLineToWrite = sLineToWrite & ConvertFromAmountToString(Math.Abs(oBatch.MON_InvoiceAmount), , ".") & "0000,"
                            'ContraEntryNo/Motbilagsnummer
                            sLineToWrite = sLineToWrite & "0,"
                            'DueDate
                            sLineToWrite = sLineToWrite & Format(StringToDate(oBatch.Payments.Item(1).DATE_Payment), "dd.MM.yyyy") & ","
                            'ContraEntryChainNo/Kjedenummer
                            sLineToWrite = sLineToWrite & "0,"
                            'Quantity
                            sLineToWrite = sLineToWrite & "0.000000,"
                            'KID
                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & ","
                            'TaxClassNo/avgiftsklasse
                            sLineToWrite = sLineToWrite & "1,"
                            'SL Account
                            sLineToWrite = sLineToWrite & "0,"
                            'SuppliersInvoiceNo/lev.faktnr.
                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & ","
                            'Div fields.
                            sLineToWrite = sLineToWrite & "0,0,0,0,0,0,0"
                            oFile.WriteLine(sLineToWrite)

                            '**********************Write counterpost
                            sLineToWrite = vbNullString
                            sLineToWrite = "1," 'Transtype
                            ' Entry date, posteringsdato
                            'According to the formatdescription the dateformat should be DDMMYY
                            '14.09.2007 - Changed the entry date from Now() to oPayment.DATE_Payment
                            '04.03.2016 - Changed back to NOW() - Petronelle Beichmann
                            sLineToWrite = sLineToWrite & Format(Date.Today, "dd.MM.yyyy") & ","
                            'sLineToWrite = sLineToWrite & Format(StringToDate(oBatch.Payments.Item(1).DATE_Payment), "dd.MM.yyyy") & ","
                            'Old code
                            'sLineToWrite = sLineToWrite & Format(Now(), "DD.MM.YYYY") & ","
                            'VoucherNo
                            'sLineToWrite = sLineToWrite & Trim$(oPayment.VoucherNo) & ","
                            sLineToWrite = sLineToWrite & ","
                            'VoucherDate
                            sLineToWrite = sLineToWrite & Format(StringToDate(oBatch.Payments.Item(1).DATE_Payment), "dd.MM.yyyy") & ","
                            'VoucherTypeNo  - should be a variable
                            sLineToWrite = sLineToWrite & "11,"
                            'VoucherText
                            If Len(oBatch.REF_Bank) > 40 Then
                                sLineToWrite = sLineToWrite & Chr(34) & "Bankref.: " & Left$(oBatch.REF_Bank, 40) & Chr(34) & ","
                            Else
                                sLineToWrite = sLineToWrite & Chr(34) & "Bankref.: " & Trim$(oBatch.REF_Bank) & Chr(34) & ","
                            End If
                            'Dep.no/Avdelingsnummer
                            'XNET - 26.01.2012 - Changed line below
                            sLineToWrite = sLineToWrite & ","
                            'sLineToWrite = sLineToWrite & oClient.ClientNo & ","
                            'ProjectNo - not in use
                            sLineToWrite = sLineToWrite & "0,"
                            'Debit AccountNo
                            sLineToWrite = sLineToWrite & "0,"
                            'CreditAccountNo
                            'XNET - 26.01.2012 - Removed a zero from the line below
                            sLineToWrite = sLineToWrite & sInterimAccount & ","
                            'VAT Code
                            sLineToWrite = sLineToWrite & "0,"
                            'CurrencyNo
                            sLineToWrite = sLineToWrite & "0,"
                            'ExchangeRate
                            sLineToWrite = sLineToWrite & "0.000000,"
                            'ExchangeAmount
                            sLineToWrite = sLineToWrite & "0.000000,"
                            'Amount
                            sLineToWrite = sLineToWrite & ConvertFromAmountToString(Math.Round(Math.Abs(oBatch.MON_InvoiceAmount), 2), , ".") & "0000,"
                            'sLineToWrite = sLineToWrite & ConvertFromAmountToString(Abs(oInvoice.MON_InvoiceAmount), , ".") & "0000,"

                            'ContraEntryNo/Motbilagsnummer
                            sLineToWrite = sLineToWrite & "0,"
                            'DueDate
                            sLineToWrite = sLineToWrite & Format(StringToDate(oBatch.Payments.Item(1).DATE_Payment), "dd.MM.yyyy") & ","
                            'ContraEntryChainNo/Kjedenummer
                            sLineToWrite = sLineToWrite & "0,"
                            'Quantity
                            sLineToWrite = sLineToWrite & "0.000000,"
                            'KID
                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & ","
                            'TaxClassNo/avgiftsklasse
                            sLineToWrite = sLineToWrite & "1,"
                            'SL Account
                            sLineToWrite = sLineToWrite & "0,"
                            'SuppliersInvoiceNo/lev.faktnr.
                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & ","
                            'Div fields.
                            sLineToWrite = sLineToWrite & "0,0,0,0,0,0,0"

                            oFile.WriteLine(sLineToWrite)

                        End If 'If oBatch.Payments.Count > 0 Then
                    Next oBatch
                Next oBabelFile

            Else
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        If Len(oBatch.REF_Bank) > 10 Then
                            oBatch.REF_Bank = Right(oBatch.REF_Bank, 10)
                        ElseIf Len(oBatch.REF_Bank) < 10 Then
                            oBatch.REF_Bank = PadRight(oBatch.REF_Bank, 10, "0")
                        End If
                        For Each oPayment In oBatch.Payments
                            If oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched Then
                                If sOldAccountNo <> oBatch.Payments.Item(1).I_Account Then
                                    bAccountFound = False
                                    For Each oClient In oFilesetup.Clients
                                        For Each oaccount In oClient.Accounts
                                            If oBatch.Payments.Item(1).I_Account = oaccount.Account Then
                                                sGLAccount = Trim$(oaccount.GLAccount)
                                                sInterimAccount = Trim$(oaccount.GLResultsAccount)
                                                sOldAccountNo = oBatch.Payments.Item(1).I_Account
                                                bAccountFound = True

                                                Exit For
                                            End If
                                        Next oaccount
                                        If bAccountFound Then
                                            Exit For
                                        End If
                                    Next oClient
                                    'XNET 26.01.2012 - removed next line
                                    'If bAccountFound Then Exit For

                                End If

                                bAllInvoicesMatchedAgainstGL = True
                                For Each oInvoice In oPayment.Invoices
                                    If oInvoice.MATCH_Final Then
                                        If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                                            bBackup = True
                                            sLineToWrite = vbNullString
                                            sLineToWrite = "1," 'Transtype
                                            ' Entry date, posteringsdato
                                            'According to the formatdescription the dateformat should be DDMMYY
                                            '14.09.2007 - Changed the entry date from Now() to oPayment.DATE_Payment
                                            '04.03.2016 - Changed back to NOW() - Petronelle Beichmann
                                            sLineToWrite = sLineToWrite & Format(Date.Today, "dd.MM.yyyy") & ","
                                            'sLineToWrite = sLineToWrite & Format(StringToDate(oPayment.DATE_Payment), "dd.MM.yyyy") & ","
                                            'Old code
                                            'sLineToWrite = sLineToWrite & Format(Now(), "DD.MM.YYYY") & ","
                                            'VoucherNo
                                            sLineToWrite = sLineToWrite & ","
                                            'VoucherDate
                                            sLineToWrite = sLineToWrite & Format(StringToDate(oPayment.DATE_Payment), "dd.MM.yyyy") & ","
                                            'VoucherTypeNo  - should be a variable
                                            sLineToWrite = sLineToWrite & "11,"
                                            'VoucherText
                                            For Each oFreeText In oInvoice.Freetexts
                                                If oFreeText.Qualifier > 3 Then
                                                    sFreetext = sFreetext & oFreeText.Text
                                                End If
                                            Next oFreeText
                                            sFreetext = sFreetext & " - " & "Ref: " & oBatch.REF_Bank

                                            If Len(sFreetext) > 50 Then
                                                sLineToWrite = sLineToWrite & Chr(34) & Left$(sFreetext, 50) & Chr(34) & ","
                                            Else
                                                sLineToWrite = sLineToWrite & Chr(34) & sFreetext & Chr(34) & ","
                                            End If
                                            'Dep.no/Avdelingsnummer
                                            '08.12.2009 - Added next IF
                                            If InStr(1, oInvoice.MATCH_ID, "-", vbTextCompare) > 0 Then
                                                sLineToWrite = sLineToWrite & xDelim(oInvoice.MATCH_ID, "-", 2) & ","
                                            Else
                                                'XNET - 26.01.2012- Changed line below
                                                sLineToWrite = sLineToWrite & ","
                                                'sLineToWrite = sLineToWrite & oClient.ClientNo & ","
                                            End If
                                            'ProjectNo - not in use
                                            sLineToWrite = sLineToWrite & "0,"
                                            If oInvoice.MON_InvoiceAmount < 0 Then
                                                'Debit AccountNo
                                                '08.12.2009 - Added next IF
                                                If InStr(1, oInvoice.MATCH_ID, "-", vbTextCompare) > 0 Then
                                                    sLineToWrite = sLineToWrite & xDelim(oInvoice.MATCH_ID, "-", 1) & ","
                                                Else
                                                    sLineToWrite = sLineToWrite & oInvoice.MATCH_ID & ","
                                                End If
                                                'CreditAccountNo
                                                sLineToWrite = sLineToWrite & "0,"
                                            Else
                                                'DebitAccountNo
                                                sLineToWrite = sLineToWrite & "0,"
                                                'Credit AccountNo
                                                '08.12.2009 - Added next IF
                                                If InStr(1, oInvoice.MATCH_ID, "-", vbTextCompare) > 0 Then
                                                    sLineToWrite = sLineToWrite & xDelim(oInvoice.MATCH_ID, "-", 1) & ","
                                                Else
                                                    sLineToWrite = sLineToWrite & oInvoice.MATCH_ID & ","
                                                End If
                                            End If
                                            'VAT Code
                                            sLineToWrite = sLineToWrite & "0,"
                                            'CurrencyNo
                                            sLineToWrite = sLineToWrite & "0,"
                                            'ExchangeRate
                                            sLineToWrite = sLineToWrite & "0.000000,"
                                            'ExchangeAmount
                                            sLineToWrite = sLineToWrite & "0.000000,"
                                            'Amount
                                            sLineToWrite = sLineToWrite & ConvertFromAmountToString(Math.Round(Math.Abs(oInvoice.MON_InvoiceAmount), 2), , ".") & "0000,"
                                            'sLineToWrite = sLineToWrite & ConvertFromAmountToString(Abs(oInvoice.MON_InvoiceAmount), , ".") & "0000,"

                                            'ContraEntryNo/Motbilagsnummer
                                            sLineToWrite = sLineToWrite & "0,"
                                            'DueDate
                                            sLineToWrite = sLineToWrite & Format(StringToDate(oPayment.DATE_Payment), "dd.MM.yyyy") & ","
                                            'ContraEntryChainNo/Kjedenummer
                                            sLineToWrite = sLineToWrite & "0,"
                                            'Quantity
                                            sLineToWrite = sLineToWrite & "0.000000,"
                                            'KID
                                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & ","
                                            'TaxClassNo/avgiftsklasse
                                            sLineToWrite = sLineToWrite & "1,"
                                            'SL Account
                                            sLineToWrite = sLineToWrite & "0,"
                                            'SuppliersInvoiceNo/lev.faktnr.
                                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & ","
                                            'Div fields.
                                            sLineToWrite = sLineToWrite & "0,0,0,0,0,0,0"
                                            oFile.WriteLine(sLineToWrite)

                                            '*****************Write counterpost
                                            sFreetext = ""
                                            sLineToWrite = vbNullString
                                            sLineToWrite = "1," 'Transtype
                                            ' Entry date, posteringsdato
                                            'According to the formatdescription the dateformat should be DDMMYY
                                            '14.09.2007 - Changed the entry date from Now() to oPayment.DATE_Payment
                                            '04.03.2016 - Changed back to NOW() - Petronelle Beichmann
                                            sLineToWrite = sLineToWrite & Format(Date.Today, "dd.MM.yyyy") & ","
                                            'sLineToWrite = sLineToWrite & Format(StringToDate(oBatch.Payments.Item(1).DATE_Payment), "dd.MM.yyyy") & ","
                                            'Old code
                                            'sLineToWrite = sLineToWrite & Format(Now(), "DD.MM.YYYY") & ","
                                            'VoucherNo
                                            'sLineToWrite = sLineToWrite & Trim$(oPayment.VoucherNo) & ","
                                            sLineToWrite = sLineToWrite & ","
                                            'VoucherDate
                                            sLineToWrite = sLineToWrite & Format(StringToDate(oPayment.DATE_Payment), "dd.MM.yyyy") & ","
                                            'VoucherTypeNo  - should be a variable
                                            sLineToWrite = sLineToWrite & "11,"
                                            'VoucherText
                                            If Len(oBatch.REF_Bank) > 40 Then
                                                sLineToWrite = sLineToWrite & Chr(34) & "Bankref.: " & Left$(oBatch.REF_Bank, 40) & Chr(34) & ","
                                            Else
                                                sLineToWrite = sLineToWrite & Chr(34) & "Bankref.: " & Trim$(oBatch.REF_Bank) & Chr(34) & ","
                                            End If
                                            'Dep.no/Avdelingsnummer
                                            'XNET - 26.01.2012 - Changed line below
                                            sLineToWrite = sLineToWrite & ","
                                            'sLineToWrite = sLineToWrite & oClient.ClientNo & ","
                                            'ProjectNo - not in use
                                            sLineToWrite = sLineToWrite & "0,"
                                            If oInvoice.MON_InvoiceAmount > 0 Then
                                                'Debit AccountNo
                                                sLineToWrite = sLineToWrite & sInterimAccount & ","
                                                'CreditAccountNo
                                                sLineToWrite = sLineToWrite & "0,"
                                            Else
                                                'DebitAccountNo
                                                sLineToWrite = sLineToWrite & "0,"
                                                'Credit AccountNo
                                                '08.12.2009 - Added next IF
                                                sLineToWrite = sLineToWrite & sInterimAccount & ","
                                            End If
                                            'VAT Code
                                            sLineToWrite = sLineToWrite & "0,"
                                            'CurrencyNo
                                            sLineToWrite = sLineToWrite & "0,"
                                            'ExchangeRate
                                            sLineToWrite = sLineToWrite & "0.000000,"
                                            'ExchangeAmount
                                            sLineToWrite = sLineToWrite & "0.000000,"
                                            'Amount
                                            sLineToWrite = sLineToWrite & ConvertFromAmountToString(Math.Round(Math.Abs(oInvoice.MON_InvoiceAmount), 2), , ".") & "0000,"
                                            'sLineToWrite = sLineToWrite & ConvertFromAmountToString(Abs(oInvoice.MON_InvoiceAmount), , ".") & "0000,"

                                            'ContraEntryNo/Motbilagsnummer
                                            sLineToWrite = sLineToWrite & "0,"
                                            'DueDate
                                            sLineToWrite = sLineToWrite & Format(StringToDate(oPayment.DATE_Payment), "dd.MM.yyyy") & ","
                                            'ContraEntryChainNo/Kjedenummer
                                            sLineToWrite = sLineToWrite & "0,"
                                            'Quantity
                                            sLineToWrite = sLineToWrite & "0.000000,"
                                            'KID
                                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & ","
                                            'TaxClassNo/avgiftsklasse
                                            sLineToWrite = sLineToWrite & "1,"
                                            'SL Account
                                            sLineToWrite = sLineToWrite & "0,"
                                            'SuppliersInvoiceNo/lev.faktnr.
                                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & ","
                                            'Div fields.
                                            sLineToWrite = sLineToWrite & "0,0,0,0,0,0,0"

                                            oFile.WriteLine(sLineToWrite)

                                            nTotalUpdayAmount = nTotalUpdayAmount + oInvoice.MON_InvoiceAmount
                                            sUpdayAccount = oPayment.I_Account 'Only 1 account

                                        Else
                                            bAllInvoicesMatchedAgainstGL = False
                                        End If
                                    End If
                                Next oInvoice
                                If bAllInvoicesMatchedAgainstGL Then
                                    oPayment.Exported = True
                                End If
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabelFile

            End If 'If bGLBank Then

            'Store to DayTotals
            If Not bGLBank Then
                If nTotalUpdayAmount > 0 Then
                    dbUpDayTotals(False, DateToString(Now()), nTotalUpdayAmount, "Matched_GL", sUpdayAccount, oBabelFiles.VB_Profile.Company_ID)
                End If
            End If

            'Create backup
            If bBackup Then
                oBackup = New vbBabel.BabelFileHandling
                oBackup.BackupPath = oBabelFiles.VB_Profile.BackupPath
                oBackup.SourceFile = sFilenameOut
                oBackup.DeleteOriginalFile = False
                oBackup.FilesetupID = oFilesetup.FileSetup_ID
                oBackup.InOut = "O"
                'oBackup.FilenameNo = oFilesetup.FileNameOut1
                oBackup.Client = vbNullString
                oBackup.TheFilenameHasACounter = False

                bBackup = oBackup.CreateBackup()
                oBackup = Nothing
            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally

            If Not oaccount Is Nothing Then
                oaccount = Nothing
            End If
            If Not oClient Is Nothing Then
                oClient = Nothing
            End If
        End Try

        TreatConectoGLFile = True

    End Function
    Public Function TreatConectoGLFile_VismaB(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal sFilenameOut As String, ByVal bGLBank As Boolean, ByVal iFilesetup_ID As Integer, ByVal oFilesetup As vbBabel.FileSetup) As Boolean
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim bReturnValue As Boolean
        Dim nTotalAmount As Double
        Dim oaccount As vbBabel.Account
        Dim oClient As vbBabel.Client
        Dim bExportTheBatch As Boolean
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sInterimAccount As String, sGLAccount As String
        Dim sLineToWrite As String
        Dim nTotalUpdayAmount As Double = 0
        Dim sUpdayAccount As String
        Dim bAllInvoicesMatchedAgainstGL As Boolean
        Dim sFreetext As String
        Dim sSeparator As String = ";"
        Dim nVoucherNo As Double
        Dim sVoucherType As String
        Dim sAccountCurrency As String
        Dim bHeadersWritten As Boolean = False
        Dim oBackup As vbBabel.BabelFileHandling
        Dim bBackup As Boolean = False
        Dim sNxtVoNo As String
        '15.03.2016 - Added
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sSrNo As String
        Dim sSrcTp As String
        Dim sVoTp As String


        '********************************************************************************
        '***********THIS FUNCTION IS USED BY CONECTO AND CONFIDE ************************
        '********************************************************************************

        sSrNo = "8" 'OK
        sSrcTp = "31" 'OK
        sVoTp = "81" 'OK

        ' sGLLogOnString holds P�loggingsstreng Oracle. Must change =UID and =PWD. Use same UserID and Password as
        ' set in BabelBanks standard database logon

        Try

            bReturnValue = False

            ' Export records to Visma Business with GL-posting
            ' Update database, do not export to file


            nTotalAmount = 0
            sOldAccountNo = ""
            sLineToWrite = ""

            If bGLBank Then
                sFilenameOut = AddTextToFilename(sFilenameOut, "_Bank", True, False)
                '06.07.2016 - Added next line
                sFilenameOut = ReplaceDateTimeStamp(sFilenameOut, False)
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            Else
                '06.07.2016 - Added next line
                sFilenameOut = ReplaceDateTimeStamp(sFilenameOut, False)
                oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            End If

            ' Start by header-record;

            If bGLBank Then
                For Each oBabelFile In oBabelFiles

                    For Each oBatch In oBabelFile.Batches

                        If oBatch.Payments.Count > 0 Then

                            'Must set oBatch.REF_Bank to 10 positions because we use it as Groupnumber (which must be exactly 10 positions) in the OCR-export
                            If Len(oBatch.REF_Bank) > 10 Then
                                oBatch.REF_Bank = Right(oBatch.REF_Bank, 10)
                            ElseIf Len(oBatch.REF_Bank) < 10 Then
                                oBatch.REF_Bank = PadRight(oBatch.REF_Bank, 10, "0")
                            End If

                            If sOldAccountNo <> oBatch.Payments.Item(1).I_Account Then
                                bAccountFound = False
                                For Each oClient In oFilesetup.Clients
                                    For Each oaccount In oClient.Accounts
                                        If oBatch.Payments.Item(1).I_Account = oaccount.Account Then
                                            sGLAccount = Trim$(oaccount.GLAccount)
                                            sInterimAccount = Trim$(oaccount.GLResultsAccount)
                                            sOldAccountNo = oBatch.Payments.Item(1).I_Account
                                            bAccountFound = True
                                            sNxtVoNo = (CDbl(oClient.VoucherNo2) + 1).ToString
                                            sVoucherType = oClient.VoucherType
                                            sAccountCurrency = oaccount.CurrencyCode

                                            Exit For
                                        End If
                                    Next oaccount
                                    If bAccountFound Then
                                        Exit For
                                    End If
                                Next oClient
                                'XNET 26.01.2012 - removed next line
                                'If bAccountFound Then Exit For

                            End If

                            If Not bHeadersWritten Then
                                '15.03.2016 - Added Connection to DB and the use of VoNo
                                'Start by connecting to the DB
                                'oVismaDal = Conecto_ConnectToVismaBusiness(oBabelFile.VB_Profile.Company_ID)

                                'sNxtVoNo = Conecto_FindNextVoNo(oBabelFile.VB_Profile.Company_ID, oVismaDal)
                                'sNxtVoNo = "810001"
                                sLineToWrite = "@WaBnd ( Descr, SrNo, ValDt, SrcTp)"
                                oFile.WriteLine(sLineToWrite)
                                sLineToWrite = Chr(34) & "BabelBank " & Chr(34) & sSeparator & Chr(34) & sSrNo & Chr(34) & sSeparator & Chr(34) & Format(Date.Today, "yyyyMMdd") & Chr(34) & sSeparator & Chr(34) & sSrcTp & Chr(34)
                                'sLineToWrite = Chr(34) & "BabelBank " & Chr(34) & " " & Chr(34) & sVoucherSerial & Chr(34) & " " & Chr(34) & oBatch.DATE_Production & Chr(34) & " " & Chr(34) & "31" & Chr(34)
                                oFile.WriteLine(sLineToWrite)
                                ' Write headers
                                sLineToWrite = "@WaVo(VoNo,VoDt,ValDt,VoTp,Txt,DbAcNo,CrAcNo,CurAm,InvoNo,AGRef,DbTxCd,CrTxCd,Cur)"
                                oFile.WriteLine(sLineToWrite)
                                bBackup = True
                                bHeadersWritten = True
                            End If

                            sLineToWrite = ""
                            '1) VoucherNo, VoNo
                            sLineToWrite = sLineToWrite & Chr(34) & sNxtVoNo & Chr(34) & sSeparator
                            '2) VoucherDate, YYYYMMDD, VoDt
                            '04.03.2016 - Changed 04.03.2016
                            '07.04.2016 - Changed around VoucherDate and ValueDate
                            sLineToWrite = sLineToWrite & Chr(34) & VB6.Format(StringToDate(oBatch.Payments.Item(1).DATE_Payment), "YYYYMMDD") & Chr(34) & sSeparator
                            'sLineToWrite = sLineToWrite & Chr(34) & VB6.Format(StringToDate(oBatch.Payments.Item(1).DATE_Payment), "YYYYMMDD") & Chr(34) & sSeparator
                            '3) ValueDate, YYYYMMDD, ValDt
                            sLineToWrite = sLineToWrite & Chr(34) & Format(Date.Today, "yyyyMMdd") & Chr(34) & sSeparator
                            '4) VoucherType
                            sLineToWrite = sLineToWrite & Chr(34) & sVoTp & Chr(34) & sSeparator
                            '5) VoucherText
                            sLineToWrite = sLineToWrite & Chr(34) & Left("Bankref.: " & oBatch.REF_Bank, 40) & Chr(34) & sSeparator
                            '6) Debit AccountNo
                            sLineToWrite = sLineToWrite & Chr(34) & sGLAccount & Chr(34) & sSeparator
                            '7) CreditAccountNo
                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator
                            '8) Amount
                            sLineToWrite = sLineToWrite & Chr(34) & ConvertFromAmountToString(System.Math.Abs(oBatch.MON_InvoiceAmount), , ".") & Chr(34) & sSeparator
                            '9) ContraEntryNo/Motbilagsnummer, InvoNo
                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator
                            '10) Motpost, AGRef
                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator

                            ' use default vatcode for account (?)
                            ''11) DbTxCode
                            '12) CrTxCd
                            sLineToWrite = sLineToWrite & Chr(34) & "0" & Chr(34) & sSeparator & Chr(34) & "0" & Chr(34) & sSeparator

                            sLineToWrite = sLineToWrite & Chr(34) & sAccountCurrency
                            oFile.WriteLine(sLineToWrite)

                            '**********************Write counterpost
                            sLineToWrite = ""
                            '1) VoucherNo, VoNo
                            sLineToWrite = sLineToWrite & Chr(34) & sNxtVoNo & Chr(34) & sSeparator
                            '2) VoucherDate, YYYYMMDD, VoDt
                            '04.03.2016 - Changed 04.03.2016
                            '07.04.2016 - Changed around VoucherDate and ValueDate
                            sLineToWrite = sLineToWrite & Chr(34) & VB6.Format(StringToDate(oBatch.Payments.Item(1).DATE_Payment), "YYYYMMDD") & Chr(34) & sSeparator
                            'sLineToWrite = sLineToWrite & Chr(34) & VB6.Format(StringToDate(oBatch.Payments.Item(1).DATE_Payment), "YYYYMMDD") & Chr(34) & sSeparator
                            '3) ValueDate, YYYYMMDD, ValDt
                            sLineToWrite = sLineToWrite & Chr(34) & Format(Date.Today, "yyyyMMdd") & Chr(34) & sSeparator
                            '4) VoucherType
                            sLineToWrite = sLineToWrite & Chr(34) & sVoTp & Chr(34) & sSeparator
                            '5) VoucherText
                            sLineToWrite = sLineToWrite & Chr(34) & Left("Bankref.: " & oBatch.REF_Bank, 40) & Chr(34) & sSeparator
                            '6) Debit AccountNo
                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator
                            '7) CreditAccountNo
                            sLineToWrite = sLineToWrite & Chr(34) & sInterimAccount & Chr(34) & sSeparator
                            '8) Amount
                            sLineToWrite = sLineToWrite & Chr(34) & ConvertFromAmountToString(System.Math.Abs(oBatch.MON_InvoiceAmount), , ".") & Chr(34) & sSeparator
                            '9) ContraEntryNo/Motbilagsnummer, InvoNo
                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator
                            '10) Motpost, AGRef
                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator

                            ' use default vatcode for account (?)
                            ''11) DbTxCode
                            '12) CrTxCd
                            sLineToWrite = sLineToWrite & Chr(34) & "0" & Chr(34) & sSeparator & Chr(34) & "0" & Chr(34) & sSeparator

                            '17.11.2014 added currency
                            sLineToWrite = sLineToWrite & Chr(34) & sAccountCurrency
                            oFile.WriteLine(sLineToWrite)


                        End If 'If oBatch.Payments.Count > 0 Then
                    Next oBatch
                Next oBabelFile

            Else
                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches
                        If Len(oBatch.REF_Bank) > 10 Then
                            oBatch.REF_Bank = Right(oBatch.REF_Bank, 10)
                        ElseIf Len(oBatch.REF_Bank) < 10 Then
                            oBatch.REF_Bank = PadRight(oBatch.REF_Bank, 10, "0")
                        End If
                        For Each oPayment In oBatch.Payments
                            If oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched Then
                                If sOldAccountNo <> oBatch.Payments.Item(1).I_Account Then
                                    bAccountFound = False
                                    For Each oClient In oFilesetup.Clients
                                        For Each oaccount In oClient.Accounts
                                            If oBatch.Payments.Item(1).I_Account = oaccount.Account Then
                                                sGLAccount = Trim$(oaccount.GLAccount)
                                                sInterimAccount = Trim$(oaccount.GLResultsAccount)
                                                sOldAccountNo = oBatch.Payments.Item(1).I_Account
                                                bAccountFound = True
                                                sNxtVoNo = (CDbl(oClient.VoucherNo2) + 1).ToString
                                                'nVoucherNo = oClient.VoucherNo + 1
                                                sVoucherType = oClient.VoucherType
                                                sAccountCurrency = oaccount.CurrencyCode

                                                Exit For
                                            End If
                                        Next oaccount
                                        If bAccountFound Then
                                            Exit For
                                        End If
                                    Next oClient
                                    'XNET 26.01.2012 - removed next line
                                    'If bAccountFound Then Exit For

                                End If

                                bAllInvoicesMatchedAgainstGL = True
                                For Each oInvoice In oPayment.Invoices
                                    If oInvoice.MATCH_Final Then
                                        If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then

                                            'Added 11.08.2022
                                            oPayment.ToSpecialReport = True
                                            oInvoice.ToSpecialReport = True

                                            If Not bHeadersWritten Then
                                                '15.03.2016 - Added Connection to DB and the use of VoNo
                                                'Start by connecting to the DB
                                                'oVismaDal = Conecto_ConnectToVismaBusiness(oBabelFile.VB_Profile.Company_ID)

                                                'sNxtVoNo = Conecto_FindNextVoNo(oBabelFile.VB_Profile.Company_ID, oVismaDal)
                                                'sNxtVoNo = "810002"
                                                sLineToWrite = "@WaBnd ( Descr, SrNo, ValDt, SrcTp)"
                                                oFile.WriteLine(sLineToWrite)
                                                sLineToWrite = Chr(34) & "BabelBank " & Chr(34) & sSeparator & Chr(34) & sSrNo & Chr(34) & sSeparator & Chr(34) & Format(Date.Today, "yyyyMMdd") & Chr(34) & sSeparator & Chr(34) & sSrcTp & Chr(34)
                                                'sLineToWrite = Chr(34) & "BabelBank " & Chr(34) & " " & Chr(34) & sVoucherSerial & Chr(34) & " " & Chr(34) & oBatch.DATE_Production & Chr(34) & " " & Chr(34) & "31" & Chr(34)
                                                oFile.WriteLine(sLineToWrite)
                                                ' Write headers
                                                sLineToWrite = "@WaVo(VoNo,VoDt,ValDt,VoTp,Txt,DbAcNo,CrAcNo,CurAm,InvoNo,AGRef,DbTxCd,CrTxCd,Cur)"
                                                oFile.WriteLine(sLineToWrite)
                                                bBackup = True
                                                bHeadersWritten = True
                                            End If

                                            sLineToWrite = ""
                                            '1) VoucherNo, VoNo
                                            sLineToWrite = sLineToWrite & Chr(34) & sNxtVoNo & Chr(34) & sSeparator
                                            '2) VoucherDate, YYYYMMDD, VoDt
                                            '04.03.2016 - Changed 04.03.2016
                                            '07.04.2016 - Changed around VoucherDate and ValueDate
                                            sLineToWrite = sLineToWrite & Chr(34) & VB6.Format(StringToDate(oPayment.DATE_Payment), "YYYYMMDD") & Chr(34) & sSeparator
                                            'sLineToWrite = sLineToWrite & Chr(34) & VB6.Format(StringToDate(oPayment.DATE_Payment), "YYYYMMDD") & Chr(34) & sSeparator
                                            '3) ValueDate, YYYYMMDD, ValDt
                                            sLineToWrite = sLineToWrite & Chr(34) & Format(Date.Today, "yyyyMMdd") & Chr(34) & sSeparator
                                            '4) VoucherType
                                            sLineToWrite = sLineToWrite & Chr(34) & sVoTp & Chr(34) & sSeparator
                                            '5) VoucherText
                                            'VoucherText
                                            sFreetext = ""
                                            For Each oFreeText In oInvoice.Freetexts
                                                If oFreeText.Qualifier > 3 Then
                                                    sFreetext = sFreetext & oFreeText.Text
                                                End If
                                            Next oFreeText
                                            sFreetext = sFreetext & " - " & "Ref: " & oBatch.REF_Bank

                                            If Len(sFreetext) > 50 Then
                                                sLineToWrite = sLineToWrite & Chr(34) & Left$(sFreetext, 50) & Chr(34) & ","
                                            Else
                                                sLineToWrite = sLineToWrite & Chr(34) & sFreetext & Chr(34) & ","
                                            End If



                                            If oInvoice.MON_InvoiceAmount < 0 Then
                                                '6) Debit AccountNo
                                                sLineToWrite = sLineToWrite & Chr(34) & oInvoice.MATCH_ID.Trim & Chr(34) & sSeparator
                                                '7) CreditAccountNo
                                                sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator
                                            Else
                                                '6) Debit AccountNo
                                                sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator
                                                '7) CreditAccountNo
                                                sLineToWrite = sLineToWrite & Chr(34) & oInvoice.MATCH_ID.Trim & Chr(34) & sSeparator
                                            End If
                                            '8) Amount
                                            sLineToWrite = sLineToWrite & Chr(34) & ConvertFromAmountToString(System.Math.Abs(oInvoice.MON_InvoiceAmount), , ".") & Chr(34) & sSeparator
                                            '9) ContraEntryNo/Motbilagsnummer, InvoNo
                                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator
                                            '10) Motpost, AGRef
                                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator

                                            ' use default vatcode for account (?)
                                            ''11) DbTxCode
                                            '12) CrTxCd
                                            sLineToWrite = sLineToWrite & Chr(34) & "0" & Chr(34) & sSeparator & Chr(34) & "0" & Chr(34) & sSeparator

                                            sLineToWrite = sLineToWrite & Chr(34) & sAccountCurrency
                                            oFile.WriteLine(sLineToWrite)

                                            '**********************Write counterpost
                                            sLineToWrite = ""
                                            '1) VoucherNo, VoNo
                                            sLineToWrite = sLineToWrite & Chr(34) & sNxtVoNo & Chr(34) & sSeparator
                                            '2) VoucherDate, YYYYMMDD, VoDt
                                            '04.03.2016 - Changed 04.03.2016
                                            '07.04.2016 - Changed around VoucherDate and ValueDate
                                            sLineToWrite = sLineToWrite & Chr(34) & VB6.Format(StringToDate(oPayment.DATE_Payment), "YYYYMMDD") & Chr(34) & sSeparator
                                            'sLineToWrite = sLineToWrite & Chr(34) & VB6.Format(StringToDate(oPayment.DATE_Payment), "YYYYMMDD") & Chr(34) & sSeparator
                                            '3) ValueDate, YYYYMMDD, ValDt
                                            sLineToWrite = sLineToWrite & Chr(34) & Format(Date.Today, "yyyyMMdd") & Chr(34) & sSeparator
                                            '4) VoucherType
                                            sLineToWrite = sLineToWrite & Chr(34) & sVoTp & Chr(34) & sSeparator

                                            '5) VoucherText
                                            If Len(oBatch.REF_Bank) > 40 Then
                                                sLineToWrite = sLineToWrite & Chr(34) & Left("Bankref.: " & oBatch.REF_Bank, 40) & Chr(34) & sSeparator
                                            Else
                                                sLineToWrite = sLineToWrite & Chr(34) & "Bankref.: " & Trim$(oBatch.REF_Bank) & Chr(34) & sSeparator
                                            End If

                                            If oInvoice.MON_InvoiceAmount > 0 Then
                                                '6) Debit AccountNo
                                                sLineToWrite = sLineToWrite & Chr(34) & sInterimAccount & Chr(34) & sSeparator
                                                '7) CreditAccountNo
                                                sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator
                                            Else
                                                '6) Debit AccountNo
                                                sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator
                                                '7) CreditAccountNo
                                                sLineToWrite = sLineToWrite & Chr(34) & sInterimAccount & Chr(34) & sSeparator
                                            End If

                                            '8) Amount
                                            sLineToWrite = sLineToWrite & Chr(34) & ConvertFromAmountToString(System.Math.Abs(oInvoice.MON_InvoiceAmount), , ".") & Chr(34) & sSeparator
                                            '9) ContraEntryNo/Motbilagsnummer, InvoNo
                                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator
                                            '10) Motpost, AGRef
                                            sLineToWrite = sLineToWrite & Chr(34) & Chr(34) & sSeparator

                                            ' use default vatcode for account (?)
                                            ''11) DbTxCode
                                            '12) CrTxCd
                                            sLineToWrite = sLineToWrite & Chr(34) & "0" & Chr(34) & sSeparator & Chr(34) & "0" & Chr(34) & sSeparator

                                            '17.11.2014 added currency
                                            sLineToWrite = sLineToWrite & Chr(34) & sAccountCurrency
                                            oFile.WriteLine(sLineToWrite)

                                            nTotalUpdayAmount = nTotalUpdayAmount + oInvoice.MON_InvoiceAmount
                                            sUpdayAccount = oPayment.I_Account 'Only 1 account

                                        Else
                                            bAllInvoicesMatchedAgainstGL = False
                                        End If
                                    End If
                                Next oInvoice
                                If bAllInvoicesMatchedAgainstGL Then
                                    oPayment.Exported = True
                                End If
                            End If
                        Next oPayment
                    Next oBatch
                Next oBabelFile

            End If 'If bGLBank Then

            'Store to DayTotals
            If Not bGLBank Then
                If nTotalUpdayAmount > 0 Then
                    dbUpDayTotals(False, DateToString(Now()), nTotalUpdayAmount, "Matched_GL", sUpdayAccount, oBabelFiles.VB_Profile.Company_ID)
                End If
            End If

            If bHeadersWritten Then
                'Create backup
                If bBackup Then
                    oBackup = New vbBabel.BabelFileHandling
                    oBackup.BackupPath = oBabelFiles.VB_Profile.BackupPath
                    oBackup.SourceFile = sFilenameOut
                    oBackup.DeleteOriginalFile = False
                    oBackup.FilesetupID = oFilesetup.FileSetup_ID
                    oBackup.InOut = "O"
                    'oBackup.FilenameNo = oFilesetup.FileNameOut1
                    oBackup.Client = vbNullString
                    oBackup.TheFilenameHasACounter = False

                    bBackup = oBackup.CreateBackup()
                    oBackup = Nothing
                End If
                ' Update oClient.VoucherNo
                oClient.VoucherNo2 = sNxtVoNo
                ' save buntnummer
                oClient.Status = vbBabel.Profile.CollectionStatus.Changed
                oFilesetup.Status = vbBabel.Profile.CollectionStatus.ChangeUnder
                oBabelFiles.VB_Profile.Status = vbBabel.Profile.CollectionStatus.ChangeUnder
                oBabelFiles.VB_Profile.Save(1)
            End If

            If Not bGLBank Then
                'Added 11.08.2022
                'Run the restAPI
                'XNET - 09.02.2012 - Added next ElseIf
                If RunModhiAML_RestAPI(oBabelFiles) Then

                End If

            End If


        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally

            If Not oaccount Is Nothing Then
                oaccount = Nothing
            End If
            If Not oClient Is Nothing Then
                oClient = Nothing
            End If

        End Try

        TreatConectoGLFile_VismaB = True

    End Function
    Private Function Conecto_ConnectToVismaBusiness(ByVal lCompanyNo As Long) As vbBabel.DAL
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim aQueryArray(,,) As String
        Dim iOldDBProfileID As Integer
        Dim bDBProfileFound As Boolean
        Dim lCounter As Long
        Dim iERPArrayIndex As Integer
        Dim sConnectionString As String
        Dim sUID As String
        Dim sPWD As String

        Try

            'Build an array consisting of the ConnectionStrings and Query agianst the ERP-system.
            aQueryArray = GetERPDBInfo(lCompanyNo, 3)

            For lCounter = 0 To UBound(aQueryArray, 1)
                Select Case UCase(aQueryArray(lCounter, 4, 0))

                    Case "VISMA_BUSINESS"
                        sConnectionString = Trim$(aQueryArray(lCounter, 0, 0))
                        Exit For
                End Select
            Next lCounter

            sUID = aQueryArray(lCounter, 1, 0)
            sPWD = aQueryArray(lCounter, 2, 0)
            ' Change UID/PWD:
            ' Fill in UserID and Password
            sConnectionString = Replace(sConnectionString, "=UID", "=" & sUID, , , CompareMethod.Text)
            sConnectionString = Replace(sConnectionString, "=PWD", "=" & sPWD, , , CompareMethod.Text)

            ' Logon to Oracle:
            oVismaDal = New vbBabel.DAL
            'oERPDal.TypeOfMatching = vbBabel.DAL.MatchingType.Automatic 'TODO - do we need this?
            oVismaDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
            oVismaDal.Connectionstring = sConnectionString
            oVismaDal.Provider = aQueryArray(lCounter, 9, 0)
            oVismaDal.Company_ID = lCompanyNo
            oVismaDal.DBProfile_ID = 1 'TODO - Hardcoded DBProfile_ID
            If Not oVismaDal.ConnectToDB() Then
                Throw New System.Exception(oVismaDal.ErrorMessage)
            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Return oVismaDal

    End Function
    Private Function Conecto_FindNextVoNo(ByVal lCompanyNo As Long, ByVal oVismaDal As vbBabel.DAL) As Double
        Dim sMySQL As String
        Dim sReturnValue As String
        Dim lCounter As Long
        Dim aQueryArray(,,) As String

        Try

            'Build an array consisting of the ConnectionStrings and Query agianst the ERP-system.
            aQueryArray = GetERPDBInfo(lCompanyNo, 5)

            For lCounter = 0 To UBound(aQueryArray, 1)
                Select Case UCase(aQueryArray(lCounter, 2, 1))

                    Case "CONECTO_GETNEXTINVOICENO"
                        sMySQL = Trim$(aQueryArray(lCounter, 0, 1))
                        Exit For
                End Select
            Next lCounter

            oVismaDal.SQL = sMySQL
            If oVismaDal.Reader_Execute() Then

                If oVismaDal.Reader_HasRows Then
                    Do While oVismaDal.Reader_ReadRecord
                        sReturnValue = oVismaDal.Reader_GetString("VoNo", False)
                    Loop
                Else
                    Throw New Exception("BabelBank klarer ikke � utlede sist brukte bilagsnummer fra Visma Business.")
                End If
            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Return sReturnValue

    End Function
    Public Function TreatFjordkraft_Feilfakt(ByVal oBabelFiles As vbBabel.BabelFiles) As Integer
        Dim oMyERPDal As vbBabel.DAL = Nothing
        Dim lRecCount As Long
        Dim sMySetupSQL As String
        Dim sMySQL As String
        Dim sClientNo As String
        Dim sAccountNo As String
        Dim lCompanyNo As Long
        Dim sProvider As String
        Dim sUID As String
        Dim sPWD As String
        Dim aQueryArray(,,) As String
        Dim lCounter As Long
        Dim sTmp As String

        sProvider = ""
        sUID = ""
        sPWD = ""
        lCompanyNo = 1

        Try

            aQueryArray = GetERPDBInfo(1, 5)

            For lCounter = 0 To UBound(aQueryArray, 3)
                Select Case UCase(aQueryArray(0, 4, lCounter))

                    Case "KIDSJEKK"
                        sMySetupSQL = Trim$(aQueryArray(0, 0, lCounter))
                End Select
            Next lCounter

            If Not EmptyString(sMySetupSQL) Then

                ' connect to erp system
                'Build an array consisting of the ConnectionStrings and Query agianst the ERP-system.
                aQueryArray = GetERPDBInfo(lCompanyNo, 1)
                sProvider = aQueryArray(0, 0, 0)  ' Connectionstring
                sUID = aQueryArray(0, 1, 0)
                sPWD = aQueryArray(0, 2, 0)

                ' Fill in UserID and Password
                sProvider = Replace(UCase(sProvider), "=UID", "=" & sUID)
                sProvider = Replace(UCase(sProvider), "=PWD", "=" & sPWD)

                oMyERPDal = New vbBabel.DAL
                'oMyERPDal.Connectionstring = sProvider
                'oMyERPDal.Provider = vbBabel.DAL.ProviderType.Odbc
                oMyERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                oMyERPDal.Company_ID = lCompanyNo
                oMyERPDal.DBProfile_ID = 1
                If Not oMyERPDal.ConnectToDB() Then
                    Throw New System.Exception(oMyERPDal.ErrorMessage)
                End If

                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches

                        If oBatch.Payments.Count > 0 Then

                            For Each oPayment In oBatch.Payments

                                If IsOCR(oPayment.PayCode) Then

                                    If oPayment.Invoices.Count = 1 Then

                                        For Each oInvoice In oPayment.Invoices
                                            sMySQL = Replace(sMySetupSQL, "BB_MyText", oInvoice.Unique_Id, , , vbTextCompare)
                                            sMySQL = Replace(sMySQL, "BB_ClientNo", "VEAS", , , vbTextCompare)
                                            sMySQL = Replace(sMySQL, "BB_Amount", oInvoice.MON_InvoiceAmount.ToString, , , vbTextCompare)
                                            ' run the sql

                                            ' run the sql
                                            oMyERPDal.SQL = sMySQL
                                            If oMyERPDal.Reader_Execute() Then
                                                If oMyERPDal.Reader_HasRows Then
                                                    oPayment.I_Account = "15038640175"
                                                    oPayment.VB_ClientNo = "VEAS"
                                                End If
                                            End If

                                            'rsERP.Open(sMySQL, conERPConnection, adOpenStatic, adLockOptimistic)
                                            'If BB_RecordCount(rsERP, True) = 1 Then
                                            ' oInvoice.Unique_Id = rsERP!BBRET_MatchID
                                            'End If
                                            'rsERP.Close()
                                        Next oInvoice
                                    End If
                                End If
                            Next oPayment
                        End If
                    Next oBatch
                Next oBabelFile
            Else
                Throw New Exception("Fant ikke SQL-en med Spesial = KIDSJEKK")

            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Return True

    End Function
    Public Function TreatConecto_ConvertKIDToPredator(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        '25.08.2015 - When importing transform Procasso KID to Predator KID

        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim oClient As vbBabel.Client
        Dim oMyERPDal As vbBabel.DAL = Nothing
        'Dim rsERP As ADODB.Recordset
        Dim lRecCount As Long
        Dim sMySetupSQL As String
        Dim sMySQL As String
        Dim sClientNo As String
        Dim sAccountNo As String
        Dim lCompanyNo As Long
        Dim sProvider As String
        Dim sUID As String
        Dim sPWD As String
        Dim aQueryArray(,,) As String
        Dim lCounter As Long
        'Dim cnConnection As New ADODB.Connection
        Dim sTmp As String
        Dim sSpecial As String
        Dim bReturnValue As Boolean

        Try

            sProvider = ""
            sUID = ""
            sPWD = ""
            lCompanyNo = 1

            aQueryArray = GetERPDBInfo(1, 5)

            For lCounter = 0 To UBound(aQueryArray, 3)
                Select Case UCase(aQueryArray(0, 4, lCounter))

                    Case "SPLITTILPREDATOR"
                        sMySetupSQL = Trim$(aQueryArray(0, 0, lCounter))
                End Select
            Next lCounter

            If Not EmptyString(sMySetupSQL) Then

                ' connect to erp system
                'Build an array consisting of the ConnectionStrings and Query agianst the ERP-system.
                'aQueryArray = GetERPDBInfo(lCompanyNo, 1)
                'sProvider = "DSN=Procasso4;UID=UID;PWD=PWD"  ' Connectionstring
                'sUID = "Babelbank"
                'sPWD = "3Skalle123"


                '' Fill in UserID and Password
                'sProvider = Replace(sProvider, "=UID", "=" & sUID)
                'sProvider = Replace(sProvider, "=PWD", "=" & sPWD)

                oMyERPDal = New vbBabel.DAL
                'oMyERPDal.Connectionstring = sProvider
                'oMyERPDal.Provider = vbBabel.DAL.ProviderType.Odbc
                oMyERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                oMyERPDal.Company_ID = lCompanyNo
                oMyERPDal.DBProfile_ID = 1
                If Not oMyERPDal.ConnectToDB() Then
                    Throw New System.Exception(oMyERPDal.ErrorMessage)
                End If

                lCounter = 0

                For Each oBabelFile In oBabelFiles
                    sSpecial = oBabelFile.Special

                    For Each oBatch In oBabelFile.Batches

                        If oBatch.Payments.Count > 0 Then
                            If IsOCR(oBatch.Payments.Item(1).PayCode) Then   ' Report ALL, not only OCR !!!!

                                For Each oPayment In oBatch.Payments
                                    ' not for all accounts

                                    'Rutinen skal finne saksnummeret i KID-en, sp�rre mot PC om
                                    'denne saken er overf�rt til PD.
                                    'Hvis ja s� skal BabelBank enten hente ut informasjon fra feltet CaseMatterText AS PredatorKID i tabellen ECRef  i Praocasso for � lage
                                    'ny KID, eller koble seg p� PD for � finne ny KID p� grunnlag av gammel KID i Ref2
                                    'feltet (hvilken l�sning som velges vil avhenge av hvilken informasjon som
                                    'legges over i CREF-feltet i PC).
                                    'Saker som er overf�rt skal p�f�res ny KID
                                    ' Finn saksnummerdel i KID

                                    '                        If oPayment.MATCH_Matched > NotMatched Then
                                    '                            ' Vil ha flere invoices, en original og en ny matchInvoice
                                    For Each oInvoice In oPayment.Invoices
                                        If Not (EmptyString(oInvoice.Unique_Id)) Then

                                            If Left(oInvoice.Unique_Id, 5) = "01000" Then
                                                sTmp = Mid(oInvoice.Unique_Id, 8, 7)

                                                ' Can be 6 or 7 digits in Sak
                                                If Left(sTmp, 1) = "0" Then
                                                    sTmp = Mid(sTmp, 2)
                                                End If
                                                sMySQL = Replace(sMySetupSQL, "BB_InvoiceIdentifier", sTmp)

                                                ' run the sql
                                                oMyERPDal.SQL = sMySQL
                                                If oMyERPDal.Reader_Execute() Then
                                                    If oMyERPDal.Reader_HasRows Then
                                                        Do While oMyERPDal.Reader_ReadRecord
                                                            sTmp = oMyERPDal.Reader_GetString("PredatorKID")
                                                            oInvoice.Unique_Id = sTmp
                                                            oInvoice.MATCH_ID = sTmp
                                                        Loop
                                                    End If
                                                End If



                                                '                                        oInvoice.Extra1 = "TILPREDATOR"
                                                '                                        oPayment.ExtraD1 = "TILPREDATOR"
                                                '                                        nPredatorAmount = nPredatorAmount + oInvoice.MON_TransferredAmount
                                                '                                        nTotalPredatorAmount = nTotalPredatorAmount + oInvoice.MON_TransferredAmount
                                            End If

                                            '06.07.2016 - Added next IF
                                            If oInvoice.Unique_Id.Length = 15 Then
                                                If oInvoice.Unique_Id.Substring(0, 2) = "59" Then
                                                    oInvoice.Unique_Id = oInvoice.Unique_Id & "-"
                                                    oInvoice.MATCH_ID = oInvoice.Unique_Id
                                                End If
                                            End If
                                        End If
                                    Next oInvoice

                                    '                        End If
                                Next oPayment

                            End If ' If IsOCR(oBatch.Payments.Item(1).PayCode) Then
                        End If ' If oBatch.Payments.Count > 0 Then

                    Next oBatch

                Next oBabelFile

                bReturnValue = True

            Else
                bReturnValue = True
            End If


        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally

            If Not oMyERPDal Is Nothing Then
                oMyERPDal.Close()
                oMyERPDal = Nothing
            End If

        End Try

        Return bReturnValue

    End Function
    'XNET - 09.02.2012 - New Function
    Public Function TreatGLPostingsAndUpdayTotals(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal sSpecial As String) As Boolean
        'The function marks GL-Postings as exported
        'It may also be used to update DayTotals
        'This function is used by AKTIV_KAPITAL
        Dim bUseDayTotals As Boolean
        Dim aAccountArray(,) As String
        Dim aAmountArray() As Double
        Dim sOldAccountNo As String
        Dim iArrayCounter As Integer
        Dim iCounter As Integer
        Dim bFoundAccount As Boolean
        Dim bGLExist As Boolean
        Dim bAllInvoicesMatched As Boolean

        Try

            sOldAccountNo = ""

            If sSpecial = "AKTIV_KAPITAL" Or sSpecial = "LOWELL_NO" Then
                bUseDayTotals = True
            Else
                bUseDayTotals = False
            End If

            If bUseDayTotals Then
                'New code 21.12.2006
                aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)

                'Create an array equal to the client-array
                ReDim Preserve aAmountArray(UBound(aAccountArray, 2))
            End If

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        bGLExist = False
                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.MATCH_Final Then
                                If oInvoice.MATCH_Matched Then
                                    If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL Then
                                        oInvoice.Exported = True
                                        bGLExist = True
                                        If oPayment.I_Account <> sOldAccountNo Then
                                            If bUseDayTotals Then
                                                bFoundAccount = False
                                                For Each oFilesetup In oBatch.VB_Profile.FileSetups
                                                    For Each oClient In oFilesetup.Clients
                                                        For Each oaccount In oClient.Accounts
                                                            If oaccount.Account = oPayment.I_Account Then
                                                                'Find the correct account in the array
                                                                For iCounter = 0 To UBound(aAccountArray, 2)
                                                                    If oPayment.I_Account = aAccountArray(1, iCounter) Then
                                                                        iArrayCounter = iCounter
                                                                        bFoundAccount = True
                                                                        Exit For
                                                                    End If
                                                                Next iCounter
                                                                Exit For
                                                            End If
                                                        Next oaccount
                                                        If bFoundAccount Then
                                                            Exit For
                                                        End If
                                                    Next oClient
                                                    If bFoundAccount Then
                                                        Exit For
                                                    End If
                                                Next oFilesetup
                                                If bFoundAccount Then
                                                    'Add to totalamountexported
                                                    aAmountArray(iArrayCounter) = aAmountArray(iArrayCounter) + oInvoice.MON_InvoiceAmount
                                                Else
                                                    Throw New Exception("TreatGLPostingsAndUpdayTotals - Error during update totals for GL postings." & vbCrLf & "Can't find the account: " & oPayment.I_Account)
                                                End If
                                            End If 'If bUseDayTotals Then
                                        End If 'If oPayment.I_Account <> sOldAccountNo Then
                                    End If
                                End If
                            End If
                        Next oInvoice
                        If bGLExist Then
                            'Check if all invoices are exported
                            bAllInvoicesMatched = True
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.MATCH_Final Then
                                    If oInvoice.MATCH_Matched = False Then
                                        bAllInvoicesMatched = False
                                    End If
                                End If
                            Next oInvoice
                            If bAllInvoicesMatched Then
                                oPayment.Exported = True
                            End If
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

            '04.12.2008 - New added next IF
            If bUseDayTotals Then
                For iCounter = 0 To UBound(aAccountArray, 2)
                    dbUpDayTotals(False, DateToString(Now()), aAmountArray(iCounter), "MATCHED_GL", aAccountArray(1, iCounter), oBabelFiles.VB_Profile.Company_ID)
                Next iCounter
            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatGLPostingsAndUpdayTotals = True

    End Function
    Public Function TreatRoyal_C(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        'Change from masspayment to payment with message
        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.PayType = "D"
                        oPayment.PayType = "150"
                        For Each oInvoice In oPayment.Invoices
                            oFreeText = oInvoice.Freetexts.Add
                            oFreeText.Text = oInvoice.REF_Own
                        Next
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatRoyal_C = True

    End Function
    Public Function TreatGjensidige_OW_Rejected(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        'Change statuscode on the Batch-level and on the invoice-level.
        'Move the REF_Own from Payment to each Invoice
        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    'If oBatch.StatusCode = "99" Then
                    '    oBatch.StatusCode = "02"
                    'End If
                    For Each oPayment In oBatch.Payments
                        For Each oInvoice In oPayment.Invoices
                            oInvoice.REF_Own = oPayment.REF_Own
                            'If oInvoice.StatusCode = "99" Then
                            '    oInvoice.StatusCode = "02"
                            'End If
                        Next
                        If Not EmptyString(oPayment.REF_EndToEnd) Then
                            oPayment.REF_Own = oPayment.REF_EndToEnd
                        End If
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatGjensidige_OW_Rejected = True
        
    End Function
    'XokNET - 17.01.2013 - New Function
    Public Function TreatSpecialMatchingELKEM_MovexM3(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        ' For OCRSimulated, use a spcific set of AvtaleIDs, different from
        ' the original AvtaleIDs which are used for "real" OCR
        Dim oBabelFile As vbBabel.BabelFile
        Dim oBatch As vbBabel.Batch
        Dim oPayment As vbBabel.Payment
        Dim oInvoice As vbBabel.Invoice
        Dim bM3Exists As Boolean
        Dim bMOExists As Boolean
        Dim bBlankExists As Boolean
        Dim nM3Amount As Double
        Dim nMovexAmount As Double

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        bM3Exists = False
                        bMOExists = False
                        bBlankExists = False
                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.MATCH_Final Then
                                If oInvoice.MATCH_Matched = False Then
                                    bM3Exists = True
                                    oInvoice.MyField = "MO"
                                Else
                                    If oInvoice.MyField = "M3" Then
                                        bM3Exists = True
                                    ElseIf oInvoice.MyField = "MO" Then
                                        bMOExists = True
                                    ElseIf EmptyString(oInvoice.MyField) Then
                                        If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Or oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer Then
                                            Throw New Exception("No system is stated on an AR-posting. This is not allowed. " & vbCrLf & "Payers name: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, ".", ","))
                                        End If
                                        bBlankExists = True
                                    Else
                                        Throw New Exception("Wrong systemt is stated is posted in MyField. Valid systems are M3 or MO. " & vbCrLf & "Payers name: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, ".", ","))
                                    End If
                                End If
                            End If
                        Next oInvoice

                        If bBlankExists Then
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.MATCH_Final Then
                                    'If there are at least 1 posting against M3, post all other postings (GL) against M3
                                    'If no postings are done against M3 but at least 1 against MO post all other postings (GL) against MO
                                    'If no postings are doen against either M3 or MO, post against M3
                                    If EmptyString(oInvoice.MyField) Then
                                        If bMOExists Then
                                            oInvoice.MyField = "MO"
                                        ElseIf bM3Exists Then
                                            oInvoice.MyField = "M3"
                                        Else
                                            oInvoice.MyField = "MO"
                                        End If

                                    End If
                                End If
                            Next oInvoice
                        End If

                        'Check if all postings against a system is postiv
                        nM3Amount = 0
                        nMovexAmount = 0
                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.MATCH_Final Then
                                If oInvoice.MyField = "MO" Then
                                    nMovexAmount = nMovexAmount + oInvoice.MON_InvoiceAmount
                                Else 'M3
                                    nM3Amount = nM3Amount + oInvoice.MON_InvoiceAmount
                                End If
                            End If
                        Next oInvoice

                        If nMovexAmount < 0 Then
                            Throw New Exception("The total amount on the postings aginst Movex is negative. This is not allowed. " & vbCrLf & "Payers name: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, ".", ","))
                        End If
                        If nM3Amount < 0 Then
                            Throw New Exception("The total amount on the postings aginst M3 is negative. This is not allowed. " & vbCrLf & "Payers name: " & oPayment.E_Name & vbCrLf & "Amount: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, ".", ","))
                        End If

                    Next oPayment
                Next oBatch
            Next oBabelFile

            oInvoice = Nothing
            oPayment = Nothing
            oBatch = Nothing
            oBabelFile = Nothing

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatSpecialMatchingELKEM_MovexM3 = True

    End Function

    Public Function Treat_CreateReturn1Telepay(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        'XokNET - 02.04.2013 - To create returnfile 1 (mottak) in the Telepayformat
        '07.08.2019 - Removed code using MSXML2
        Dim docXMLParameters As New System.Xml.XmlDocument
        Dim nodParameter As System.Xml.XmlNode
        Dim oFs As Scripting.FileSystemObject
        Dim sXPathCommand As String
        Dim sTmp As String
        Dim sReference As String
        Dim sReferenceStringPart As String
        Dim lReferenceNumericPart As Long
        Dim nCounter As Double
        Dim bPaymentIsAlreadyErrorMarkes As Boolean
        Dim bMarkCompanyNoAsError As Boolean
        Dim bMarkFirstKIDPaymentAsError As Boolean
        Dim bMarkFirstPaymentAsErrorInAccount As Boolean
        Dim bMarkFirstIntPaymentAsErrorInSWIFT As Boolean

        Try

            '10 Feil i foretaksnummer. - Batch
            '17 Feil bruk av KID / ugyldig KID - Invoice
            '19 Kredit kontonummer ikke gyldig. - Payment
            '45 SWIFT-adresse feil utfylt - Payment
            'Sletting

            'BETFOR01
            'R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA

            'BETFOR02
            'R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA

            'BETFOR03
            'R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA

            'BETFOR04
            'R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA

            'BETFOR21
            'R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA

            'BETFOR22
            'R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA
            'R1/R2 L�PENUMMER 293-296 4 **NUMERISK

            'BETFOR23
            'R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA
            'R1/R2 L�PENUMMER 294-296 3***NUMERISK - OK

            'BETFOR99 (Only when domestic according to the documentation.

            bMarkCompanyNoAsError = False
            bMarkFirstKIDPaymentAsError = False
            bMarkFirstPaymentAsErrorInAccount = False
            bMarkFirstIntPaymentAsErrorInSWIFT = False


            oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")

            If RunTime() Then    'DISKUTERNOTRUNTIME Felles mappe?
                If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\ReturnFileInfo.xml") Then
                    Throw New Exception("Treat_CreateReturn1Telepay - Kan ikke finne filen ReturnFileInfo.xml, " & vbCrLf & "som benyttes til � legge p� returverdier fra bank.")
                Else
                    'docXMLParameters.async = False
                    docXMLParameters.Load(My.Application.Info.DirectoryPath & "\Mapping\ReturnFileInfo.xml")
                End If
            Else
                If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\ReturnFileInfo.xml") Then
                    Throw New Exception("Treat_CreateReturn1Telepay - Kan ikke finne filen ReturnFileInfo.xml, " & vbCrLf & "som benyttes til � legge p� returverdier fra bank.")
                Else
                    'docXMLParameters.async = False
                    docXMLParameters.Load(My.Application.Info.DirectoryPath & "\Mapping\ReturnFileInfo.xml")
                End If
            End If

            nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/REFERENCENUMBER/VALUE")
            If Not nodParameter Is Nothing Then
                sReference = nodParameter.InnerText
                FindNumericPartOfString(sReference, lReferenceNumericPart, sReferenceStringPart)
            End If

            nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/MARKCOMPANYNOASERROR/VALUE")
            If Not nodParameter Is Nothing Then
                sTmp = ""
                sTmp = nodParameter.InnerText
                If sTmp = "TRUE" Then
                    bMarkCompanyNoAsError = True
                End If
            End If

            nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/MARKFIRSTKIDPAYMENTASERROR/VALUE")
            If Not nodParameter Is Nothing Then
                sTmp = ""
                sTmp = nodParameter.InnerText
                If sTmp = "TRUE" Then
                    bMarkFirstKIDPaymentAsError = True
                End If
            End If

            nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/MARKFIRSTPAYMENTASERRORINACCOUNT/VALUE")
            If Not nodParameter Is Nothing Then
                sTmp = ""
                sTmp = nodParameter.InnerText
                If sTmp = "TRUE" Then
                    bMarkFirstPaymentAsErrorInAccount = True
                End If
            End If

            nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/MARKFIRSTIINTPAYMENTASERRORINSWIFT/VALUE")
            If Not nodParameter Is Nothing Then
                sTmp = ""
                sTmp = nodParameter.InnerText
                If sTmp = "TRUE" Then
                    bMarkFirstIntPaymentAsErrorInSWIFT = True
                End If
            End If

            nCounter = 0

            For Each oBabelFile In oBabelFiles
                If bMarkCompanyNoAsError Then
                    oBabelFile.StatusCode = "10"
                Else
                    oBabelFile.StatusCode = "01"
                End If
                For Each oBatch In oBabelFile.Batches
                    If bMarkCompanyNoAsError Then
                        oBatch.StatusCode = "10"
                    Else
                        oBatch.StatusCode = "01"
                    End If
                    For Each oPayment In oBatch.Payments
                        bPaymentIsAlreadyErrorMarkes = False
                        If bMarkCompanyNoAsError Then
                            oPayment.StatusCode = "10"
                            bPaymentIsAlreadyErrorMarkes = True
                        Else
                            If bMarkFirstPaymentAsErrorInAccount Then
                                oPayment.StatusCode = "19"
                                bMarkFirstPaymentAsErrorInAccount = False
                                bPaymentIsAlreadyErrorMarkes = True
                            Else
                                If bMarkFirstIntPaymentAsErrorInSWIFT Then
                                    If oPayment.PayType = "I" And Len(oPayment.BANK_SWIFTCode) > 0 Then
                                        oPayment.StatusCode = "45"
                                        bMarkFirstIntPaymentAsErrorInSWIFT = False
                                        bPaymentIsAlreadyErrorMarkes = True
                                    Else
                                        oPayment.StatusCode = "01"
                                    End If
                                Else
                                    oPayment.StatusCode = "01"
                                End If
                            End If
                        End If
                        oPayment.Payment_ID = sReferenceStringPart & Trim(Str(lReferenceNumericPart + nCounter))
                        nCounter = nCounter + 1
                        For Each oInvoice In oPayment.Invoices
                            If bMarkCompanyNoAsError Then
                                oInvoice.StatusCode = "10"
                            Else
                                If bMarkFirstKIDPaymentAsError And Not bPaymentIsAlreadyErrorMarkes Then
                                    If IsOutgoingKIDPayment(oPayment.PayCode) Then 'XNET 24.01.2014 - Changed from IsOCR to IsOutgoingKIDPayment
                                        oInvoice.StatusCode = "17"
                                        bMarkFirstKIDPaymentAsError = False
                                        bPaymentIsAlreadyErrorMarkes = True
                                    Else
                                        oInvoice.StatusCode = "01"
                                    End If
                                Else
                                    oInvoice.StatusCode = "01"
                                End If
                            End If
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_CreateReturn1Telepay = True

    End Function

    Public Function Treat_CreateReturn2Telepay(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        'XNET - 02.04.2013 - To create returnfile 2 (avregn) in the Telepayformat
        '07.08.2019 - Removed code using MSXML2
        Dim docXMLParameters As New System.Xml.XmlDocument
        Dim nodParameter As System.Xml.XmlNode
        Dim oFs As Scripting.FileSystemObject
        Dim sXPathCommand As String
        Dim sTmp As String
        Dim sReference As String
        Dim sReferenceStringPart As String
        Dim lReferenceNumericPart As Long
        Dim nExecutionRef1 As Double
        Dim nExecutionRef2 As Double
        Dim nCounter As Double
        Dim bPaymentIsAlreadyErrorMarkes As Boolean
        Dim bMarkCompanyNoAsError As Boolean
        Dim bMarkFirstKIDPaymentAsError As Boolean
        Dim bMarkFirstPaymentAsErrorInAccount As Boolean
        Dim bMarkFirstIntPaymentAsErrorInSWIFT As Boolean
        Dim sSelectString As String

        Dim sOldAccountNo As String
        Dim sI_Account As String
        Dim bAccountFound As Boolean
        Dim sAccountCurrency As String

        Try

            '10 Feil i foretaksnummer. - Batch
            '17 Feil bruk av KID / ugyldig KID - Invoice
            '19 Kredit kontonummer ikke gyldig. - Payment
            '45 SWIFT-adresse feil utfylt - Payment
            'Sletting

            'BETFOR01
            '**R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA
            '**R2 REELL KURS 191-202 12 NUMERISK
            '**R2 EFFEKTUERINGSREF.2 203-214 12 ALFA
            '*R2 BELASTET BEL�P 215-230 16 NUMERISK
            '**R2 OVERF�RT BEL�P 231-246 16 NUMERISK
            '**R2 EFFEKTUERINGSREF.1 252-257 6 NUMERISK (FELT ER P�KREVET)
            '**R2 VALUTERINGSDATO 266-271 6 NUMERISK, ��MMDD
            'R2 PROVISJON 272-280 9 NUMERISK - oPayment.MON_ChargesAmount
            '*R2 KURS MOT NOK 281-292 12 NUMERISK
            'R2 SLETTE�RSAK 293 1 ALFA - IKKE IMPLEMENTERT
            'R2 BESTILTOVERF�RTBEL�P294-309 16 NUMERISK - IKKE IMPLEMENTERT
            'R2 INFO.VEDR.PRISING 310 1 ALFA - IKKE IMPLEMENTERT

            'BETFOR02
            '**R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA

            'BETFOR03
            '**R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA

            'BETFOR04
            '**R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA
            'R2 SLETTE�RSAK 234 1 ALFA - IKKE IMPLEMENTERT
            '**R2/O/V L�PENUMMER 294-296 3 NUMERISK

            'BETFOR21
            '**R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA
            '**R2 TOTALBEL�P 269-283 15 *** NUMERISK
            '**R2 VALUTERINGSDATO 289-294 6 ��MMDD
            'R2 SLETTE�RSAK 301 1 ALFA - IKKE IMPLEMENTERT

            'BETFOR22
            '*R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA
            '*R1/R2 L�PENUMMER 293-296 4 **NUMERISK
            'R2 SLETTE�RSAK 297 1 ALFA - IKKE IMPLEMENTERT

            'BETFOR23
            '*R1/R2/O/V REFERANSENUMMER 75-80 6 ALFA
            '**R1/R2 L�PENUMMER 294-296 3***NUMERISK
            'R2 SLETTE�RSAK 297 1 ALFA - IKKE IMPLEMENTERT

            'BETFOR99 (Only when domestic according to the documentation.
            'R2 ANTALL OPPDRAG 85-88 4 NUMERISK - IKKE IMPLEMENTERT
            'R2 TOTALSUMFIL 89-103 15 MUMERISK - IKKE IMPLEMENTERT

            bMarkCompanyNoAsError = False
            bMarkFirstKIDPaymentAsError = False
            bMarkFirstPaymentAsErrorInAccount = False
            bMarkFirstIntPaymentAsErrorInSWIFT = False


            'oFs = CreateObject ("Scripting.FileSystemObject")
            oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")

            If RunTime() Then   ''DISKUTERNOTRUNTIME Felles mappe?
                If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\ReturnFileInfo.xml") Then
                    Throw New Exception("Treat_CreateReturn1Telepay - Kan ikke finne filen ReturnFileInfo.xml, " & vbCrLf & "som benyttes til � legge p� returverdier fra bank.")
                Else
                    'docXMLParameters.async = False
                    docXMLParameters.Load(My.Application.Info.DirectoryPath & "\Mapping\ReturnFileInfo.xml")
                End If
            Else
                If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\ReturnFileInfo.xml") Then
                    Throw New Exception("Treat_CreateReturn1Telepay - Kan ikke finne filen ReturnFileInfo.xml, " & vbCrLf & "som benyttes til � legge p� returverdier fra bank.")
                Else
                    'docXMLParameters.async = False
                    docXMLParameters.Load(My.Application.Info.DirectoryPath & "\Mapping\ReturnFileInfo.xml")
                End If
            End If

            nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/REFERENCENUMBER/VALUE")
            If Not nodParameter Is Nothing Then
                sReference = nodParameter.InnerText
                FindNumericPartOfString(sReference, lReferenceNumericPart, sReferenceStringPart)
            End If

            nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/EXECUTIONREF1/VALUE")
            If Not nodParameter Is Nothing Then
                nExecutionRef1 = Val(nodParameter.InnerText)
            Else
                nExecutionRef1 = 0
            End If

            nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/EXECUTIONREF2/VALUE")
            If Not nodParameter Is Nothing Then
                nExecutionRef2 = Val(nodParameter.InnerText)
            Else
                nExecutionRef2 = 0
            End If

            nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/MARKCOMPANYNOASERROR/VALUE")
            If Not nodParameter Is Nothing Then
                sTmp = ""
                sTmp = nodParameter.InnerText
                If sTmp = "TRUE" Then
                    bMarkCompanyNoAsError = True
                End If
            End If

            nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/MARKFIRSTKIDPAYMENTASERROR/VALUE")
            If Not nodParameter Is Nothing Then
                sTmp = ""
                sTmp = nodParameter.InnerText
                If sTmp = "TRUE" Then
                    bMarkFirstKIDPaymentAsError = True
                End If
            End If

            nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/MARKFIRSTPAYMENTASERRORINACCOUNT/VALUE")
            If Not nodParameter Is Nothing Then
                sTmp = ""
                sTmp = nodParameter.InnerText
                If sTmp = "TRUE" Then
                    bMarkFirstPaymentAsErrorInAccount = True
                End If
            End If

            nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/MARKFIRSTIINTPAYMENTASERRORINSWIFT/VALUE")
            If Not nodParameter Is Nothing Then
                sTmp = ""
                sTmp = nodParameter.InnerText
                If sTmp = "TRUE" Then
                    bMarkFirstIntPaymentAsErrorInSWIFT = True
                End If
            End If

            nCounter = 0

            For Each oBabelFile In oBabelFiles
                If bMarkCompanyNoAsError Then
                    oBabelFile.StatusCode = "10"
                Else
                    oBabelFile.StatusCode = "02"
                End If
                For Each oBatch In oBabelFile.Batches
                    If bMarkCompanyNoAsError Then
                        oBatch.StatusCode = "10"
                    Else
                        oBatch.StatusCode = "02"
                    End If
                    For Each oPayment In oBatch.Payments

                        'Find the account in the BB setup
                        If oPayment.I_Account <> sOldAccountNo Then
                            If oPayment.VB_ProfileInUse Then
                                sI_Account = oPayment.I_Account
                                bAccountFound = False
                                For Each oFilesetup In oBabelFile.VB_Profile.FileSetups
                                    For Each oClient In oFilesetup.Clients
                                        For Each oaccount In oClient.Accounts
                                            If sI_Account = oaccount.Account Then
                                                sOldAccountNo = oPayment.I_Account
                                                bAccountFound = True

                                                If Len(Trim(oaccount.CurrencyCode)) <> 3 Then
                                                    Throw New Exception("Treat_CreateReturn2Telepay - Kontonummer " & sI_Account & " har ikke angitt en valutakode." & vbCrLf & _
                                                    "Legg inn valutakode under 'Klient'" & vbCrLf & vbCrLf & _
                                                    "BabelBank avbrytes!")
                                                Else
                                                    sAccountCurrency = Trim(oaccount.CurrencyCode)
                                                End If
                                                Exit For
                                            End If
                                        Next oaccount
                                        If bAccountFound Then Exit For
                                    Next oClient
                                    If bAccountFound Then Exit For
                                Next oFilesetup
                            End If
                            If Not bAccountFound Then
                                Throw New Exception("Treat_CreateReturn2Telepay - Kontonummer " & sI_Account & " er ukjent for BabelBank." & vbCrLf & _
                                "Legg inn kontonummeret og valutakode under 'Klient'" & vbCrLf & vbCrLf & _
                                "BabelBank avbrytes!")
                            End If
                        End If

                        bPaymentIsAlreadyErrorMarkes = False
                        If bMarkCompanyNoAsError Then
                            oPayment.StatusCode = "10"
                            bPaymentIsAlreadyErrorMarkes = True
                        Else
                            If bMarkFirstPaymentAsErrorInAccount Then
                                oPayment.StatusCode = "19"
                                bMarkFirstPaymentAsErrorInAccount = False
                                bPaymentIsAlreadyErrorMarkes = True
                            Else
                                If bMarkFirstIntPaymentAsErrorInSWIFT Then
                                    If oPayment.PayType = "I" And Len(oPayment.BANK_SWIFTCode) > 0 Then
                                        oPayment.StatusCode = "45"
                                        bMarkFirstIntPaymentAsErrorInSWIFT = False
                                        bPaymentIsAlreadyErrorMarkes = True
                                    Else
                                        oPayment.StatusCode = "02"
                                    End If
                                Else
                                    oPayment.StatusCode = "02"
                                End If
                            End If
                        End If
                        If oPayment.StatusCode = "02" Then
                            oPayment.MON_AccountCurrency = sAccountCurrency
                            oPayment.MON_TransferredAmount = oPayment.MON_InvoiceAmount
                            oPayment.DATE_Value = Format(Now(), "yyyyMMdd")
                            If oPayment.PayType <> "I" Then
                                oPayment.MON_AccountAmount = oPayment.MON_InvoiceAmount
                            Else
                                'oPayment.MON_ChargesAmount = ??????
                                If Not nExecutionRef1 = 0 Then
                                    oPayment.REF_Bank1 = Trim(Str(nExecutionRef1))
                                    nExecutionRef1 = nExecutionRef1 + 1
                                End If
                                If Not nExecutionRef2 = 0 Then
                                    oPayment.REF_Bank2 = Trim(Str(nExecutionRef2))
                                    nExecutionRef2 = nExecutionRef2 + 1
                                End If


                                'Add exchangerate
                                If sAccountCurrency = "NOK" And oPayment.MON_InvoiceCurrency = "NOK" Then
                                    'Don't update actual exchangerate
                                    'If the account being debited is a NOK account and the currency of payment is NOK the field
                                    '   will not be updated.
                                    oPayment.MON_AccountAmount = oPayment.MON_InvoiceAmount

                                ElseIf sAccountCurrency <> "NOK" And sAccountCurrency = oPayment.MON_InvoiceCurrency Then
                                    'If the account being debited is denominated in a currency other than NOK and the currency of payment is the same currency the exchange rate will be against NOK.
                                    sSelectString = "//PARAMETER/EXCHANGERATE/NOK/" & sAccountCurrency
                                    'Set nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/ACTUALEXCHANGERATE/NOK")
                                    nodParameter = docXMLParameters.selectSingleNode(sSelectString)
                                    If Not nodParameter Is Nothing Then
                                        oPayment.MON_LocalExchRate = Val(nodParameter.InnerText)
                                        oPayment.MON_AccountExchRate = Val(nodParameter.InnerText)
                                    Else
                                        Throw New Exception("Treat_CreateReturn2Telepay - Mangler valutakurs mellom NOK og " & sAccountCurrency & vbCrLf & _
                                        "Legg inn valutakurs i XML-fil under tag EXCHANGERATE" & vbCrLf & vbCrLf & _
                                        "BabelBank avbrytes!")
                                    End If
                                    oPayment.MON_AccountAmount = oPayment.MON_InvoiceAmount

                                Else
                                    'If the currency of the account being debited is different from the currency of the payment the
                                    '   exchange rate between the two different currencies will be used.
                                    sSelectString = "//PARAMETER/EXCHANGERATE/" & sAccountCurrency & "/" & oPayment.MON_InvoiceCurrency
                                    'Set nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/ACTUALEXCHANGERATE/NOK")
                                    nodParameter = docXMLParameters.selectSingleNode(sSelectString)
                                    If Not nodParameter Is Nothing Then
                                        oPayment.MON_AccountExchRate = Val(nodParameter.InnerText)
                                    Else
                                        Throw New Exception("Treat_CreateReturn2Telepay - Mangler valutakurs mellom " & sAccountCurrency & " og " & oPayment.MON_InvoiceCurrency & vbCrLf & _
                                        "Legg inn valutakurs i XML-fil under tag EXCHANGERATE" & vbCrLf & vbCrLf & _
                                        "BabelBank avbrytes!")
                                    End If

                                    sSelectString = "//PARAMETER/EXCHANGERATE/NOK/" & sAccountCurrency
                                    'Set nodParameter = docXMLParameters.selectSingleNode("//PARAMETER/ACTUALEXCHANGERATE/NOK")
                                    nodParameter = docXMLParameters.selectSingleNode(sSelectString)
                                    If Not nodParameter Is Nothing Then
                                        oPayment.MON_LocalExchRate = Val(nodParameter.InnerText)
                                    Else
                                        Throw New Exception("Treat_CreateReturn2Telepay - Mangler valutakurs mellom NOK og " & sAccountCurrency & vbCrLf & _
                                        "Legg inn valutakurs i XML-fil under tag EXCHANGERATE" & vbCrLf & vbCrLf & _
                                        "BabelBank avbrytes!")
                                    End If
                                    If sAccountCurrency = "EUR" Or sAccountCurrency = "USD" Or sAccountCurrency = "GBP" Then
                                        If oPayment.MON_InvoiceCurrency = "EUR" Or oPayment.MON_InvoiceCurrency = "USD" Or oPayment.MON_InvoiceCurrency = "GBP" Then
                                            oPayment.MON_AccountAmount = Math.Round(oPayment.MON_InvoiceAmount * oPayment.MON_AccountExchRate, 0)
                                        Else
                                            oPayment.MON_AccountAmount = Math.Round(oPayment.MON_InvoiceAmount * oPayment.MON_AccountExchRate / 100, 0)
                                        End If
                                    Else
                                        If oPayment.MON_InvoiceCurrency = "EUR" Or oPayment.MON_InvoiceCurrency = "USD" Or oPayment.MON_InvoiceCurrency = "GBP" Then
                                            oPayment.MON_AccountAmount = Math.Round(oPayment.MON_InvoiceAmount * oPayment.MON_AccountExchRate, 0)
                                        Else
                                            oPayment.MON_AccountAmount = Math.Round(oPayment.MON_InvoiceAmount * oPayment.MON_AccountExchRate / 100, 0)
                                        End If
                                    End If

                                End If

                            End If
                        End If
                        oPayment.Payment_ID = sReferenceStringPart & Trim(Str(lReferenceNumericPart + nCounter))
                        nCounter = nCounter + 1
                        For Each oInvoice In oPayment.Invoices
                            If bMarkCompanyNoAsError Then
                                oInvoice.StatusCode = "10"
                            Else
                                If bMarkFirstKIDPaymentAsError And Not bPaymentIsAlreadyErrorMarkes Then
                                    If IsOutgoingKIDPayment(oPayment.PayCode) Then 'XNET 24.01.2014 - Changed from IsOCR to IsOutgoingKIDPayment
                                        oInvoice.StatusCode = "17"
                                        bMarkFirstKIDPaymentAsError = False
                                        bPaymentIsAlreadyErrorMarkes = True
                                    Else
                                        oInvoice.StatusCode = "02"
                                    End If
                                Else
                                    oInvoice.StatusCode = "02"
                                End If
                            End If
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_CreateReturn2Telepay = True

    End Function
    'XNET - 17.10.2013 - Added new function
    Public Function TreatSpecial_Santander_ValidateKID(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        'New function to validate KID for Santander
        Dim bValid As Boolean
        Dim sMessage As String

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.MATCH_Final Then
                                If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                                    bValid = True
                                    If Len(Trim(oInvoice.MATCH_ID)) <> 14 Then
                                        'This is the KID without checkdigit, the actual KID for Santader is 15 positions.
                                        bValid = False
                                    End If
                                    If Not vbIsNumeric(oInvoice.MATCH_ID, "0123456789") Then
                                        bValid = False
                                    End If
                                    If Not bValid Then
                                        sMessage = "Feil KID " & Trim(oInvoice.MATCH_ID) & " funnet p� en postering p� f�lgende innbetaling:" & vbCrLf
                                        sMessage = sMessage & "Betalers navn: " & oPayment.E_Name & vbCrLf
                                        sMessage = sMessage & "Innbetalt bel�p: " & ConvertFromAmountToString(oPayment.MON_InvoiceAmount, ".", ",") & vbCrLf
                                        sMessage = sMessage & "Betalers konto: " & oPayment.E_Account & vbCrLf
                                        sMessage = sMessage & "Poster bel�p p� feilKID: " & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, ".", ",") & vbCrLf & vbCrLf
                                        sMessage = sMessage & "Vennligst g� inn p� manuell avstemming og poster om innbetalingen!"
                                        MsgBox(sMessage, vbCritical + vbOKOnly, "FEIL KID P� POSTERING")

                                        TreatSpecial_Santander_ValidateKID = False
                                        Exit Function

                                    End If
                                End If
                            End If
                        Next
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatSpecial_Santander_ValidateKID = True

    End Function
    'XNET - 17.10.2013 - Added new function
    Public Function TreatFjordkraft_Test(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal sFilenameOut As String) As Boolean
        'New function to create a simple file for reverse doubleimported payments
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim s As String

        Try

            sFilenameOut = AddTextToFilename(sFilenameOut, "EKSPORT-", False, True)
            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        For Each oInvoice In oPayment.Invoices
                            s = oInvoice.Unique_Id & ";" & ConvertFromAmountToString(oInvoice.MON_InvoiceAmount, "", ".")
                            oFile.WriteLine(s)
                        Next
                    Next
                Next
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatFjordkraft_Test = True

    End Function

    Public Function TreatFjordkraft_OCR(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim bAllInvoicesMatched As Boolean
        Dim bAtLeastOneInvoiceMatched As Boolean
        Dim bAtLeastOnePaymentMatched As Boolean
        Dim sUniqueID As String
        Dim bMatched As Boolean
        Dim oNewBabelFiles As vbBabel.BabelFiles
        Dim oNewBabel As vbBabel.BabelFile
        Dim oNewBatch As vbBabel.Batch
        Dim oNewPayment As vbBabel.Payment
        Dim oNewBatches As vbBabel.Batches
        Dim oNewPayments As vbBabel.Payments
        Dim aQueryArray(,,) As String
        Dim iERPArrayIndex As Integer
        Dim bRemoveAccounts As Boolean = False
        Dim bUseParameters As Boolean = False
        Dim sTmp As String
        Dim sTmp2 As String
        Dim iPos As Integer
        Dim iCounter As Integer
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream

        Try

            '08.05.2017 - Added next lines for Fjordkraft_Egeninkasso
            aQueryArray = GetERPDBInfo(1, 3)
            If Not Array_IsEmpty(aQueryArray) Then
                For iERPArrayIndex = 0 To aQueryArray.GetUpperBound(2)
                    If aQueryArray(0, 2, iERPArrayIndex).ToUpper = "FJORDKRAFT_FJERNKONTI" Then
                        bRemoveAccounts = True
                    End If
                Next iERPArrayIndex
            End If

            '03.02.2022 - Added next lines to retrieve set from the SQL-setup
            aQueryArray = GetERPDBInfo(1, 1)
            If Not Array_IsEmpty(aQueryArray) Then
                For iERPArrayIndex = 0 To aQueryArray.GetUpperBound(2)
                    If aQueryArray(0, 2, iERPArrayIndex).ToUpper = "FJORDKRAFT_NY_EKKID" Then
                        'Example
                        'ACCOUNT=15065112814;START=5&ACCOUNT=DEFAULT;START=7
                        sTmp = aQueryArray(0, 0, iERPArrayIndex)
                        Exit For
                    End If
                Next iERPArrayIndex
            End If

            'aQueryArray(0, 0, 0) - Account
            'aQueryArray(0, 0, 1) - Startvalues in the KID
            If EmptyString(sTmp) Then
                ReDim aQueryArray(0, 1, 1)
                aQueryArray(0, 0, 0) = "15065112814"
                aQueryArray(0, 1, 0) = "5"
                aQueryArray(0, 0, 1) = "DEFAULT"
                aQueryArray(0, 1, 1) = "7"
                bUseParameters = False
            Else
                iCounter = -1
                Do While True
                    iCounter = iCounter + 1
                    sTmp2 = xDelim(sTmp, "&", iCounter + 1)
                    If Not EmptyString(sTmp2) Then
                        If iCounter = 0 Then
                            ReDim aQueryArray(0, 1, iCounter)
                        Else
                            ReDim Preserve aQueryArray(0, 1, iCounter)
                        End If
                        aQueryArray(0, 0, iCounter) = xDelim(sTmp2, ";", 1).Replace("ACCOUNT=", "")
                        aQueryArray(0, 1, iCounter) = xDelim(sTmp2, ";", 2).Replace("START=", "")
                    Else
                        Exit Do
                    End If
                Loop
                bUseParameters = True
            End If

            bAtLeastOnePaymentMatched = False

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches

                    For Each oPayment In oBatch.Payments

                        If IsOCR(oPayment.PayCode) Then 'TRONDHEIM KOMMUNE
                            bAllInvoicesMatched = True
                            bAtLeastOneInvoiceMatched = False

                            '03.02.2022 - Added next IF
                            If bUseParameters Then
                                For iCounter = 0 To aQueryArray.GetUpperBound(2)
                                    If oPayment.I_Account = aQueryArray(0, 0, iCounter) Or aQueryArray(0, 0, iCounter) = "DEFAULT" Then
                                        For Each oInvoice In oPayment.Invoices

                                            If Not EmptyString(oInvoice.Unique_Id) Then
                                                sUniqueID = oInvoice.Unique_Id.Trim
                                                If sUniqueID.Substring(0, 1) = aQueryArray(0, 1, iCounter) Then
                                                    If sUniqueID.Length = 10 Or sUniqueID.Length = 15 Then
                                                        oInvoice.MATCH_Matched = True
                                                        oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice
                                                        oInvoice.MATCH_ID = sUniqueID
                                                        bAtLeastOneInvoiceMatched = True
                                                    Else
                                                        If Not RunTime() Then
                                                            oFs = New Scripting.FileSystemObject ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")
                                                            oFile = oFs.OpenTextFile("C:\Slett\Fjordkraft\Feil KID.txt", Scripting.IOMode.ForAppending)
                                                            oFile.WriteLine(oPayment.E_Name & "-" & (oInvoice.MON_InvoiceAmount / 100).ToString & "-" & oInvoice.Unique_Id)
                                                            oFile.Close()
                                                            oFs = Nothing
                                                        End If
                                                        bAllInvoicesMatched = False
                                                    End If
                                                Else
                                                    If Not RunTime() Then
                                                        oFs = New Scripting.FileSystemObject ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")
                                                        oFile = oFs.OpenTextFile("C:\Slett\Fjordkraft\Feil KID.txt", Scripting.IOMode.ForAppending)
                                                        oFile.WriteLine(oPayment.E_Name & "-" & (oInvoice.MON_InvoiceAmount / 100).ToString & "-" & oInvoice.Unique_Id)
                                                        oFile.Close()
                                                        oFs = Nothing
                                                    End If
                                                    bAllInvoicesMatched = False
                                                End If
                                            Else
                                                If Not RunTime() Then
                                                    oFs = New Scripting.FileSystemObject ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")
                                                    oFile = oFs.OpenTextFile("C:\Slett\Fjordkraft\Feil KID.txt", Scripting.IOMode.ForAppending)
                                                    oFile.WriteLine(oPayment.E_Name & "-" & (oInvoice.MON_InvoiceAmount / 100).ToString & "-" & oInvoice.Unique_Id)
                                                    oFile.Close()
                                                    oFs = Nothing
                                                End If
                                                bAllInvoicesMatched = False
                                            End If
                                        Next oInvoice
                                        Exit For
                                    End If
                                Next iCounter
                            Else
                                '02.02.2022 -Added next If
                                If oPayment.I_Account = "15065112814" Then
                                    For Each oInvoice In oPayment.Invoices

                                        If Not EmptyString(oInvoice.Unique_Id) Then
                                            sUniqueID = oInvoice.Unique_Id.Trim
                                            If sUniqueID.Substring(0, 1) = "5" Then
                                                If sUniqueID.Length = 10 Or sUniqueID.Length = 15 Then
                                                    oInvoice.MATCH_Matched = True
                                                    oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice
                                                    oInvoice.MATCH_ID = sUniqueID
                                                    bAtLeastOneInvoiceMatched = True
                                                Else
                                                    If Not RunTime() Then
                                                        oFs = New Scripting.FileSystemObject ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")
                                                        oFile = oFs.OpenTextFile("C:\Slett\Fjordkraft\20220324\Feil KID.txt", Scripting.IOMode.ForAppending)
                                                        oFile.WriteLine(oPayment.E_Name & "-" & (oInvoice.MON_InvoiceAmount / 100).ToString & "-" & oInvoice.Unique_Id)
                                                        oFile.Close()
                                                        oFs = Nothing
                                                    End If
                                                    bAllInvoicesMatched = False
                                                End If
                                            Else
                                                If Not RunTime() Then
                                                    oFs = New Scripting.FileSystemObject ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")
                                                    oFile = oFs.OpenTextFile("C:\Slett\Fjordkraft\20220324\Feil KID.txt", Scripting.IOMode.ForAppending)
                                                    oFile.WriteLine(oPayment.E_Name & "-" & (oInvoice.MON_InvoiceAmount / 100).ToString & "-" & oInvoice.Unique_Id)
                                                    oFile.Close()
                                                    oFs = Nothing
                                                End If
                                                bAllInvoicesMatched = False
                                            End If

                                        Else
                                            If Not RunTime() Then
                                                oFs = New Scripting.FileSystemObject ' 22.05.2017 CreateObject ("Scripting.FileSystemObject")
                                                oFile = oFs.OpenTextFile("C:\Slett\Fjordkraft\20220324\Feil KID.txt", Scripting.IOMode.ForAppending)
                                                oFile.WriteLine(oPayment.E_Name & "-" & (oInvoice.MON_InvoiceAmount / 100).ToString & "-" & oInvoice.Unique_Id)
                                                oFile.Close()
                                                oFs = Nothing
                                            End If
                                            bAllInvoicesMatched = False
                                        End If

                                    Next oInvoice

                                Else
                                    For Each oInvoice In oPayment.Invoices

                                        If Not EmptyString(oInvoice.Unique_Id) Then
                                            sUniqueID = oInvoice.Unique_Id.Trim
                                            If sUniqueID.Substring(0, 1) = "7" Then
                                                If sUniqueID.Length = 10 Or sUniqueID.Length = 15 Then
                                                    oInvoice.MATCH_Matched = True

                                                    oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice
                                                    oInvoice.MATCH_ID = sUniqueID
                                                    bAtLeastOneInvoiceMatched = True
                                                Else
                                                    bAllInvoicesMatched = False
                                                End If
                                            Else
                                                bAllInvoicesMatched = False
                                            End If

                                        Else
                                            bAllInvoicesMatched = False
                                        End If

                                    Next oInvoice
                                End If
                            End If

                            If bAllInvoicesMatched Then
                                oPayment.MATCH_Reset()
                                'oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.PartlyMatched
                                'oPayment.MATCH_HowMatched = "Delpostert p� EnergiKonto-KID"
                                bMatched = False

                                'Added 08.05.2018
                                If bRemoveAccounts Then
                                    oPayment.E_Account = ""
                                End If
                            Else

                                If oNewBabelFiles Is Nothing Then
                                    oNewBabelFiles = New vbBabel.BabelFiles
                                    oNewBabelFiles.VB_Profile = oBabelFiles.VB_Profile
                                    oNewBabelFiles.MATCH_MatchOCR = True
                                End If
                                If oNewBabel Is Nothing Then
                                    oNewBabel = CopyBabelFileObject(oBabelFile)
                                    oNewBabel.Batches = Nothing

                                    oNewBatches = New vbBabel.Batches
                                    oNewBabel.Batches = oNewBatches

                                    oNewBabelFiles.VB_AddWithObject(oNewBabel)
                                End If
                                If oNewBatch Is Nothing Then
                                    oNewBatch = New vbBabel.Batch
                                    CopyBatchObject(oBatch, oNewBatch)

                                    oNewBatch.Payments = Nothing

                                    oNewPayments = New vbBabel.Payments
                                    oNewBatch.Payments = oNewPayments

                                    oNewBabel.Batches.VB_AddWithObject(oNewBatch)

                                End If
                                oNewPayment = New vbBabel.Payment
                                CopyPaymentObject(oPayment, oNewPayment)
                                oNewPayment.PayCode = "601"
                                For Each oInvoice In oNewPayment.Invoices
                                    If Not EmptyString(oInvoice.Unique_Id) Then
                                        oFreeText = oInvoice.Freetexts.Add
                                        oFreeText.Text = "KID: " & oInvoice.Unique_Id.Trim
                                    End If

                                Next oInvoice

                                oNewBatch.Payments.VB_AddWithObject(oNewPayment)

                                ''If the payment isn't matced set it as matched to be able to save it to the DB
                                'oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched
                                'oPayment.MATCH_HowMatched = "Avstemt p� KID"
                                oPayment.Exported = True
                                For Each oInvoice In oPayment.Invoices
                                    oInvoice.Exported = True
                                Next oInvoice
                                bAtLeastOnePaymentMatched = True
                                'bMatched = True
                            End If

                        Else
                            Exit For

                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

            If bAtLeastOnePaymentMatched Then

                'oNewBabelFiles.SetProfileInUndelyingCollections()

                If oNewBabelFiles.MATCH_SaveData(True, False, True) Then

                Else
                    Throw New Exception("En uspesifisert feil oppstod ved lagring av OCR-poster med feil KID.")
                End If

            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Return True


    End Function
    'XNET - 11.11.2013 - New function
    Public Function TreatBKK_Agresso_GLFile(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal sGLFileFixed As String, ByVal iFileSetup_ID As Integer, ByVal oFilesetup As vbBabel.FileSetup) As Boolean
        Dim oFs As New Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim oLocalFileSetup As vbBabel.FileSetup
        Dim oBackup As vbBabel.BabelFileHandling
        Dim bBackup As Boolean

        Dim sLineToWrite As String
        Dim sBuntPrefix As String
        Dim sPreSystem As String
        Dim sVoucherType As String
        Dim sCompanyCode As String
        Dim bPostGLBank As String
        Dim sPeriod As String
        Dim sBuntNummer As String
        Dim nTotalAmount As Double
        Dim bFoundRecords As Boolean
        Dim sOldAccountNo As String
        Dim sOldClientNo As String
        Dim bAccountFound As Boolean
        Dim aAccountArray(,) As String
        Dim aAmountArray() As Double
        Dim lArrayCounter As Long
        Dim bFileInitiated As Boolean
        Dim sGLAccount As String
        Dim sNoMatchAccount As String
        Dim sI_Account As String
        Dim lCounter As Long
        Dim sCurrency As String
        Dim sFilenameOut As String
        Dim bRecordWritten As Boolean
        Dim bAtLeastOneRecordWritten As Boolean
        Dim bFirstBabelFile As Boolean
        Dim sSpecialNoMatchAccountForAgresso_Lindorff As String = "15160"
        Dim sFilter As String = "E_ACCOUNT"
        Dim sFilterValue As String = "15038828875" 'Used for Egeninkasso from Lindorff
        Dim sFilterValue2 As String = "15038828876" 'Used for specificationfile from Lindorff

        Dim sFreetext As String
        Dim bx As Boolean
        Dim lVoucherDigit As Long
        Dim sAlphaPart As String
        'Dim nAmountToPostOnSpecialNoMatchAccount As Double = 0
        'Dim sCurrencyToPostOnSpecialNoMatchAccount As String = ""
        'Dim sDateToPostOnSpecialNoMatchAccount As String = ""

        Try

            sBuntPrefix = "BB"
            sPreSystem = "BB"
            sVoucherType = "BB"
            sCompanyCode = "" 'Found from accountsetup
            bPostGLBank = True
            sPeriod = ""
            sBuntNummer = sBuntPrefix & Format(Now(), "yyyyMMddHHmmss")
            sBuntPrefix = ""
            nTotalAmount = 0
            sGLAccount = ""
            sNoMatchAccount = ""
            bFoundRecords = False
            bAccountFound = False
            sOldAccountNo = ""
            sOldClientNo = ""
            bFileInitiated = False
            bRecordWritten = False
            bAtLeastOneRecordWritten = False
            bFirstBabelFile = True

            sFilenameOut = ReplaceDateTimeStamp(sGLFileFixed, False)

            oFile = oFs.OpenTextFile(sFilenameOut, Scripting.IOMode.ForAppending, True, 0)
            bFileInitiated = True

            'New code 21.12.2006
            aAccountArray = GetAccounts(oBabelFiles.VB_Profile.Company_ID)

            'Create an array equal to the client-array
            ReDim Preserve aAmountArray(UBound(aAccountArray, 2))

            '20.08.2018 - Sjekk om vi har spesialinnbetaling fra Lindorff (hovedstol)
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.E_Name.Trim = sFilterValue Or oPayment.E_Account.Trim = sFilterValue Then

                            sNoMatchAccount = vbNullString
                            bFoundRecords = False

                            If oPayment.VB_ProfileInUse Then
                                sI_Account = oPayment.I_Account
                                bAccountFound = False
                                For Each oLocalFileSetup In oBabelFile.VB_Profile.FileSetups
                                    For Each oClient In oLocalFileSetup.Clients
                                        For Each oaccount In oClient.Accounts
                                            If sI_Account = oaccount.Account Then
                                                sOldAccountNo = oPayment.I_Account

                                                bAccountFound = True

                                                If sOldClientNo <> oClient.ClientNo Then
                                                    'We have to find the last used sequenceno. in some cases
                                                    'First check if we use a profile
                                                    If oBatch.VB_ProfileInUse = True Then
                                                        '18.06.2017 - Changed the use of VoucherType for Fjordkraft, 
                                                        ' now prioritizing VoucherType2 (which is the one shown in the setup window).
                                                        If Not EmptyString(oClient.VoucherType2) Then
                                                            sVoucherType = Trim$(oClient.VoucherType2)
                                                        Else
                                                            If Not EmptyString(oClient.VoucherType) Then
                                                                sVoucherType = Trim$(oClient.VoucherType)
                                                            End If
                                                        End If
                                                    End If
                                                    sOldClientNo = oClient.ClientNo

                                                End If

                                                If Not EmptyString(oClient.Division) Then
                                                    sCompanyCode = Trim(oClient.Division)
                                                End If

                                                sGLAccount = Trim(oaccount.GLAccount)
                                                sNoMatchAccount = GetNoMatchAccount(oBabelFile.VB_Profile.Company_ID, oClient.Client_ID)

                                                '28.08.2017 - New way of retrieving Buntnummer
                                                If vbIsNumeric(oClient.VoucherNo.Trim, "0123456789") Then
                                                    sBuntNummer = Trim(Str(Val(oClient.VoucherNo) + 1))
                                                Else
                                                    'Retrieve the numeric part and add 1 to the series
                                                    'Added 15.08.2018
                                                    bx = FindNumericPartOfString(oClient.VoucherNo.Trim, lVoucherDigit, sAlphaPart)
                                                    sBuntNummer = sAlphaPart & (lVoucherDigit + 1).ToString
                                                End If

                                                'Find the correct client in the array
                                                For lCounter = 0 To UBound(aAccountArray, 2)
                                                    If oPayment.I_Account = aAccountArray(1, lCounter) Then
                                                        lArrayCounter = lCounter
                                                        Exit For
                                                    End If
                                                Next lCounter

                                                Exit For
                                            End If
                                        Next oaccount
                                        If bAccountFound Then Exit For
                                    Next oClient
                                    If bAccountFound Then Exit For
                                Next oLocalFileSetup
                            End If

                            If Not EmptyString(oPayment.DATE_Payment) Then
                                If oPayment.DATE_Payment.Length = 8 Then
                                    sPeriod = oPayment.DATE_Payment.Substring(0, 6)
                                End If
                            End If

                            'Find tekst on payment, should be used as bilakstekst in Agresso
                            sFreetext = ""
                            For Each oInvoice In oPayment.Invoices
                                For Each oFreeText In oInvoice.Freetexts
                                    If oFreeText.Text.Length > 8 Then
                                        If oFreeText.Text.Substring(0, 6).ToUpper = "BEL�P:" Then
                                            'Do not write this line
                                        Else
                                            sFreetext = sFreetext & oFreeText.Text
                                        End If
                                    Else
                                        sFreetext = sFreetext & oFreeText.Text
                                    End If
                                Next oFreeText
                            Next oInvoice

                            'Changed 05.12.2018
                            'Changed bilagstekst
                            sLineToWrite = WriteAgresso_BKKAgresso_GL(sBuntPrefix & sBuntNummer, sPreSystem, sVoucherType, sCompanyCode, sGLAccount, sFreetext, sPeriod, oPayment.MON_InvoiceCurrency, oPayment.MON_InvoiceAmount, oPayment.DATE_Value, False, " ", " ", " ", " ", " ", " ", " ")
                            oFile.WriteLine(sLineToWrite)
                            'Changed account and bilagstekst
                            sLineToWrite = WriteAgresso_BKKAgresso_GL(sBuntPrefix & sBuntNummer, sPreSystem, sVoucherType, sCompanyCode, sNoMatchAccount, sFreetext, sPeriod, oPayment.MON_InvoiceCurrency, oPayment.MON_InvoiceAmount * -1, oPayment.DATE_Value, False, " ", " ", " ", " ", " ", " ", " ")
                            oFile.WriteLine(sLineToWrite)
                            'Old code
                            'sLineToWrite = WriteAgresso_BKKAgresso_GL(sBuntPrefix & sBuntNummer, sPreSystem, sVoucherType, sCompanyCode, sGLAccount, oBatch.REF_Bank, sPeriod, oPayment.MON_InvoiceCurrency, oPayment.MON_InvoiceAmount, oPayment.DATE_Value, False, " ", " ", " ", " ", " ", " ", " ")
                            'oFile.WriteLine(sLineToWrite)
                            'sLineToWrite = WriteAgresso_BKKAgresso_GL(sBuntPrefix & sBuntNummer, sPreSystem, sVoucherType, sCompanyCode, sGLAccount, oBatch.REF_Bank, sPeriod, oPayment.MON_InvoiceCurrency, oPayment.MON_InvoiceAmount, oPayment.DATE_Value, False, " ", " ", " ", " ", " ", " ", " ")
                            'oFile.WriteLine(sLineToWrite)

                            sPeriod = ""
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

            If Not TreatFilter(oBabelFiles, iFileSetup_ID, False, sFilter, sFilterValue, "") Then

            End If

            For Each oBabelFile In oBabelFiles
                '11.06.2018 - Added next IF
                If oBabelFile.ImportFormat <> vbBabel.BabelFiles.FileType.Lindorff_Gebyr Then

                    '28.08.2017 - Commented the code below. New way of retrieving Buntnummer
                    'If bFirstBabelFile Then
                    '    bFirstBabelFile = False
                    '    sBuntNummer = Trim(Str(Val(oBabelFile.VB_Profile.Custom1Value) + 1))
                    'End If
                    For Each oBatch In oBabelFile.Batches
                        If oBatch.Payments.Count > 0 Then

                            oPayment = oBatch.Payments.Item(1)
                            sCurrency = oPayment.MON_InvoiceCurrency

                            'Find the client
                            'Must do this now because we want to check the accounttype of NoMatchAccount
                            If oPayment.I_Account <> sOldAccountNo Then

                                If bFoundRecords Then
                                    'LINDORFF()
                                    sLineToWrite = WriteAgresso_BKKAgresso_GL(sBuntPrefix & sBuntNummer, sPreSystem, sVoucherType, sCompanyCode, sNoMatchAccount, sFreetext, sPeriod, sCurrency, nTotalAmount * -1, oPayment.DATE_Value, False, " ", " ", " ", " ", " ", " ", " ")
                                    oFile.WriteLine(sLineToWrite)
                                End If

                                sNoMatchAccount = vbNullString
                                nTotalAmount = 0
                                bFoundRecords = False


                                If oPayment.VB_ProfileInUse Then
                                    sI_Account = oPayment.I_Account
                                    bAccountFound = False
                                    For Each oLocalFileSetup In oBabelFile.VB_Profile.FileSetups
                                        For Each oClient In oLocalFileSetup.Clients
                                            For Each oaccount In oClient.Accounts
                                                If sI_Account = oaccount.Account Then
                                                    sOldAccountNo = oPayment.I_Account

                                                    bAccountFound = True

                                                    If sOldClientNo <> oClient.ClientNo Then
                                                        'We have to find the last used sequenceno. in some cases
                                                        'First check if we use a profile
                                                        If oBatch.VB_ProfileInUse = True Then
                                                            '18.06.2017 - Changed the use of VoucherType for Fjordkraft, 
                                                            ' now prioritizing VoucherType2 (which is the one shown in the setup window).
                                                            If Not EmptyString(oClient.VoucherType2) Then
                                                                sVoucherType = Trim$(oClient.VoucherType2)
                                                            Else
                                                                If Not EmptyString(oClient.VoucherType) Then
                                                                    sVoucherType = Trim$(oClient.VoucherType)
                                                                End If
                                                            End If
                                                        End If
                                                        sOldClientNo = oClient.ClientNo

                                                        '28.08.2017 - New way of retrieving Buntnummer
                                                        If vbIsNumeric(oClient.VoucherNo.Trim, "0123456789") Then
                                                            sBuntNummer = Trim(Str(Val(oClient.VoucherNo) + 1))
                                                        Else
                                                            'Retrieve the numeric part and add 1 to the series
                                                            'Added 15.08.2018
                                                            bx = FindNumericPartOfString(oClient.VoucherNo.Trim, lVoucherDigit, sAlphaPart)
                                                            sBuntNummer = sAlphaPart & (lVoucherDigit + 1).ToString
                                                        End If


                                                        ' XOKNET 12.10.2012 - changes for Fjordkraft, regarding buntnummer
                                                        '  - find Buntnummer from Client
                                                        ' ----------------------------------------------
                                                        '03.01.2014  Not anymore, now retrieved from CustomerValue
                                                        'sBuntnummer = Trim(Str(Val(oClient.VoucherNo) + 1))

                                                        'This is not a good solution. The problem is that the the buntnummer (voucherno) already har been increased
                                                        ' with 1 because of this GL-Bank file is written, when we shall write the AR-postings. However this is done in the other filesetup (where we also find client) which
                                                        ' we don't hit here. We have no FielSetup_ID to find the correct one.
                                                    End If

                                                    If Not EmptyString(oClient.Division) Then
                                                        sCompanyCode = Trim(oClient.Division)
                                                    End If

                                                    sGLAccount = Trim(oaccount.GLAccount)
                                                    sNoMatchAccount = GetNoMatchAccount(oBabelFile.VB_Profile.Company_ID, oClient.Client_ID)

                                                    'Find the correct client in the array
                                                    For lCounter = 0 To UBound(aAccountArray, 2)
                                                        If oPayment.I_Account = aAccountArray(1, lCounter) Then
                                                            lArrayCounter = lCounter
                                                            Exit For
                                                        End If
                                                    Next lCounter

                                                    Exit For
                                                End If
                                            Next oaccount
                                            If bAccountFound Then Exit For
                                        Next oClient
                                        If bAccountFound Then Exit For
                                    Next oLocalFileSetup
                                End If
                            End If

                            '05.02.2018 - Added this code after receiving mail from BKK 
                            'Must be done here because we may write against the observationaccount above and that is info about the previous batch
                            If Not EmptyString(oPayment.DATE_Payment) Then
                                If oPayment.DATE_Payment.Length = 8 Then
                                    sPeriod = oPayment.DATE_Payment.Substring(0, 6)
                                End If
                            End If

                            '28.08.2018 - Added to write avregningsnummer for egeninkasso solution
                            sFreetext = ""
                            If oBabelFile.Special = "BKK_AGRESSO" Then
                                If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.CREMUL Then
                                    If oPayment.E_Name.ToUpper.Contains("FJORDKRAFT") Then
                                        For Each oInvoice In oPayment.Invoices
                                            For Each oFreeText In oInvoice.Freetexts
                                                If oFreeText.Text.ToUpper.Contains("LEVERAND�R") Then
                                                    sFreetext = sFreetext & oFreeText.Text.Trim
                                                End If
                                            Next oFreeText
                                        Next oInvoice
                                    End If
                                End If
                            End If
                            If Not sFreetext.Length > 0 Then
                                sFreetext = oBatch.REF_Bank
                            End If

                            sLineToWrite = WriteAgresso_BKKAgresso_GL(sBuntPrefix & sBuntNummer, sPreSystem, sVoucherType, sCompanyCode, sGLAccount, sFreetext, sPeriod, oPayment.MON_InvoiceCurrency, oBatch.MON_InvoiceAmount, oPayment.DATE_Value, False, " ", " ", " ", " ", " ", " ", " ")
                            oFile.WriteLine(sLineToWrite)
                            bFileInitiated = True
                            bFoundRecords = True
                            nTotalAmount = nTotalAmount + oBatch.MON_InvoiceAmount
                            aAmountArray(lArrayCounter) = aAmountArray(lArrayCounter) + oBatch.MON_InvoiceAmount
                            bRecordWritten = True
                            bAtLeastOneRecordWritten = True

                        End If
                    Next oBatch
                End If
            Next oBabelFile

            If bFoundRecords Then
                'LINDORFF()
                sLineToWrite = WriteAgresso_BKKAgresso_GL(sBuntPrefix & sBuntNummer, sPreSystem, sVoucherType, sCompanyCode, sNoMatchAccount, sFreetext, sPeriod, sCurrency, nTotalAmount * -1, oPayment.DATE_Value, False, " ", " ", " ", " ", " ", " ", " ")
                oFile.WriteLine(sLineToWrite)
            End If

            '04.12.2008 - New added next IF
            '19.09.2018: Removed next For ... Next, dbUpday on Imported payments is run for all cases in ImportExport.
            'This updated once more, created problems for Lindorff gebyr transactions for Fjordkraft.
            'For lCounter = 0 To UBound(aAccountArray, 2)
            '    dbUpDayTotals(False, DateToString(Now()), aAmountArray(lCounter), "IMPORTED", aAccountArray(1, lCounter), oBabelFiles.VB_Profile.Company_ID)
            'Next lCounter

            If Not bAtLeastOneRecordWritten Then

                If Not oFile Is Nothing Then
                    If bFileInitiated Then
                        oFile.Close()
                        bFileInitiated = False
                    End If
                    oFile = Nothing
                End If

                If Not oFs Is Nothing Then
                    oFs = Nothing
                End If

                oFs = New Scripting.FileSystemObject

                If oFs.FileExists(sFilenameOut) Then
                    oFs.DeleteFile(sFilenameOut, True)
                End If

            Else

                ' Update oClient.VoucherNo
                For Each oLocalFileSetup In oBabelFile.VB_Profile.FileSetups
                    For Each oClient In oLocalFileSetup.Clients
                        oClient.VoucherNo = sBuntNummer
                        oClient.Status = vbBabel.Profile.CollectionStatus.Changed
                    Next oClient
                    oLocalFileSetup.Status = vbBabel.Profile.CollectionStatus.ChangeUnder
                Next oLocalFileSetup
                ' save buntnummer
                oBabelFiles.VB_Profile.Status = vbBabel.Profile.CollectionStatus.ChangeUnder
                oBabelFiles.VB_Profile.Save(1)


                '' Update oClient.VoucherNo
                'oBabelFiles.VB_Profile.Custom1Value = sBuntNummer
                ''oClient.VoucherNo = sBuntnummer
                '' save buntnummer
                ''oClient.STATUS = Changed
                ''oFilesetup.STATUS = ChangeUnder
                'oBabelFiles.VB_Profile.STATUS = vbBabel.Profile.CollectionStatus.Changed
                'oBabelFiles.VB_Profile.Save(1)

                oBackup = New vbBabel.BabelFileHandling
                oBackup.BackupPath = oBabelFiles.VB_Profile.BackupPath
                oBackup.SourceFile = sFilenameOut
                oBackup.DeleteOriginalFile = False
                oBackup.FilesetupID = oFilesetup.FileSetup_ID
                oBackup.InOut = "O"
                'oBackup.FilenameNo = oFilesetup.FileNameOut1
                oBackup.Client = vbNullString
                oBackup.TheFilenameHasACounter = False

                bBackup = oBackup.CreateBackup()
                oBackup = Nothing
            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally

            If Not oFile Is Nothing Then
                If bFileInitiated Then
                    oFile.Close()
                End If
                oFile = Nothing
            End If
            If Not oFs Is Nothing Then
                oFs = Nothing
            End If

        End Try

        TreatBKK_Agresso_GLFile = True

    End Function
    'XokNET - 25.02.2014 - New function
    Public Function Treat_SGFinansBGMax(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        'This function split imported BGMax-files into 1 KID-batch and 1 batch for other payments.
        Dim oKIDBatch As vbBabel.Batch
        Dim oDummyBatch As vbBabel.Batch
        Dim iBabelCounter As Integer
        Dim iBatchCounter As Integer
        Dim iPaymentCounter As Integer
        Dim iBatchIndexCounter As Integer
        Dim nBatchInvoiceAmount As Double
        Dim nBatchTransferredAmount As Double

        Try

            'Mark all BGMax-payments as non-KID
            For Each oBabelFile In oBabelFiles
                If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.BgMax Then
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            If IsOCR(oPayment.PayCode) Then
                                For Each oInvoice In oPayment.Invoices
                                    If Not EmptyString(oInvoice.Unique_Id) Then
                                        oFreeText = oInvoice.Freetexts.Add
                                        oFreeText.Text = "ID: " & Trim(oInvoice.Unique_Id)
                                    End If
                                Next oInvoice
                            End If
                            oPayment.PayCode = "601"
                        Next oPayment
                    Next oBatch
                End If
            Next oBabelFile

            '
            '
            '
            'For iBabelCounter = oBabelFiles.Count To 1 Step -1
            '    Set oBabelFile = oBabelFiles(iBabelCounter)
            '
            '    If oBabelFile.ImportFormat = vbBabel.BgMax Then
            '        iBatchIndexCounter = oBabelFile.Batches.Count
            '        For iBatchCounter = oBabelFile.Batches.Count To 1 Step -1
            '            Set oBatch = oBabelFile.Batches(iBatchCounter)
            '
            '            Set oKIDBatch = CreateObject ("vbbabel.Batch")
            '            'Set oOtherBatch = CreateObject ("vbbabel.Batch")
            '            ' Copy from first batch to second
            '            CopyBatchObject oBatch, oKIDBatch
            '            'CopyBatchObject oBatch, oOtherBatch
            '            iBatchIndexCounter = iBatchIndexCounter + 1
            '            oKIDBatch.Index = iBatchIndexCounter
            '            'iBatchIndexCounter = iBatchIndexCounter + 1
            '            'oOtherBatch.Index = iBatchIndexCounter
            '
            '            'Remove non-KID payments from the KID-batch
            '            For iPaymentCounter = oKIDBatch.Payments.Count To 1 Step -1
            '                Set oPayment = oKIDBatch.Payments(iPaymentCounter)
            '                If IsOCR(oPayment.PayCode) Then
            '                    oKIDBatch.Payments.Remove iPaymentCounter
            '                End If
            '            Next iPaymentCounter
            '
            '            'Reindex KIDBatch
            '            iPaymentCounter = 0
            '            For Each oPayment In oKIDBatch.Payments
            '                iPaymentCounter = iPaymentCounter + 1
            '                oPayment.Index = iPaymentCounter
            '            Next oPayment
            '
            '            'Remove KID payments from the original batch
            '            For iPaymentCounter = oBatch.Payments.Count To 1 Step -1
            '                Set oPayment = oBatch.Payments(iPaymentCounter)
            '                If Not IsOCR(oPayment.PayCode) Then
            '                    oBatch.Payments.Remove iPaymentCounter
            '                End If
            '            Next iPaymentCounter
            '
            '            'Reindex KIDBatch
            '            iPaymentCounter = 0
            '            For Each oPayment In oBatch.Payments
            '                iPaymentCounter = iPaymentCounter + 1
            '                oPayment.Index = iPaymentCounter
            '            Next oPayment
            '
            ''            'Remove KID payments from the Other-batch
            ''            For iPaymentCounter = oOtherBatch.Payments.Count To 1 Step -1
            ''                Set oPayment = oOtherBatch.Payments(iPaymentCounter)
            ''                If Not IsOCR(oPayment.PayCode) Then
            ''                    oOtherBatch.Payments.Remove iPaymentCounter
            ''                End If
            ''            Next oPayment
            ''
            ''            'Reindex KIDBatch
            ''            iPaymentCounter = 0
            ''            For Each oPayment In oOtherBatch.Payments
            ''                iPaymentCounter = iPaymentCounter + 1
            ''                oPayment.Index = iPaymentCounter
            ''            Next oPayment
            ''
            ''            'Remove original batch
            ''            oBabelFile.Batches.Remove iBatchCounter
            '
            '            'Add new batches
            '            Set oDummyBatch = oBabelFile.Batches.VB_AddWithObject(oKIDBatch)
            '            'oBabelFile.Batches.VB_AddWithObject (oOtherBatch)
            '
            '        Next iBatchCounter
            '
            '        'Reindex the batches
            '        iBatchCounter = 0
            '        For Each oBatch In oBabelFile.Batches
            '            iBatchCounter = iBatchCounter + 1
            '            oBatch.Index = iBatchCounter
            '            nBatchInvoiceAmount = 0
            '            nBatchTransferredAmount = 0
            '            For Each oPayment In oBatch.Payments
            '                nBatchInvoiceAmount = nBatchInvoiceAmount + oPayment.MON_InvoiceAmount
            '                nBatchTransferredAmount = nBatchTransferredAmount + oPayment.MON_TransferredAmount
            '            Next oPayment
            '            oBatch.MON_InvoiceAmount = nBatchInvoiceAmount
            '            oBatch.MON_TransferredAmount = nBatchTransferredAmount
            '        Next oBatch
            '
            '    End If
            'Next iBabelCounter

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_SGFinansBGMax = True

    End Function
    Public Function Treat_SGFinansAggregateCamt053(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        'This function split imported BGMax-files into 1 KID-batch and 1 batch for other payments.
        Dim iBatchCounter As Integer
        Dim iPaymentCounter As Integer
        Dim iInvoiceCounter As Integer
        Dim nBatchInvoiceAmount As Double
        Dim nBatchTransferredAmount As Double
        Dim nPaymentInvoiceAmount As Double
        Dim nPaymentTransferredAmount As Double

        Try

            'For Camt053, used for blockfactoring, aggregate remove all batches and payments and use the 
            ' Babelfile-amount on batch and payment (they want to post the change in saldo)
            'If the change is negative, they will be removed in the filter-processing, at least I hope so)
            For Each oBabelFile In oBabelFiles
                If oBabelFile.ImportFormat = vbBabel.BabelFiles.FileType.Camt053 Then
                    nBatchInvoiceAmount = 0
                    nBatchTransferredAmount = 0
                    nPaymentInvoiceAmount = 0
                    nPaymentTransferredAmount = 0
                    'Delete all batches except for one
                    For iBatchCounter = oBabelFile.Batches.Count To 2 Step -1
                        oBatch = oBabelFile.Batches.Item(iBatchCounter)
                        nBatchInvoiceAmount = nBatchInvoiceAmount + oBatch.MON_InvoiceAmount
                        nBatchTransferredAmount = nBatchTransferredAmount + oBatch.MON_TransferredAmount
                        For Each oPayment In oBatch.Payments
                            nPaymentInvoiceAmount = nPaymentInvoiceAmount + oPayment.MON_InvoiceAmount
                            nPaymentTransferredAmount = nPaymentTransferredAmount + oPayment.MON_TransferredAmount
                        Next oPayment
                        oBabelFile.Batches.Remove(iBatchCounter)
                    Next iBatchCounter
                    If oBabelFile.Batches.Count = 1 Then
                        For Each oBatch In oBabelFile.Batches
                            nBatchInvoiceAmount = nBatchInvoiceAmount + oBatch.MON_InvoiceAmount
                            nBatchTransferredAmount = nBatchTransferredAmount + oBatch.MON_TransferredAmount
                            For iPaymentCounter = oBatch.Payments.Count To 2 Step -1
                                oPayment = oBatch.Payments.Item(iPaymentCounter)
                                nPaymentInvoiceAmount = nPaymentInvoiceAmount + oPayment.MON_InvoiceAmount
                                nPaymentTransferredAmount = nPaymentTransferredAmount + oPayment.MON_TransferredAmount
                                oBatch.Payments.Remove(iPaymentCounter)
                            Next iPaymentCounter
                            If oBatch.Payments.Count = 1 Then
                                'OK
                                For Each oPayment In oBatch.Payments
                                    nPaymentInvoiceAmount = nPaymentInvoiceAmount + oPayment.MON_InvoiceAmount
                                    nPaymentTransferredAmount = nPaymentTransferredAmount + oPayment.MON_TransferredAmount
                                Next oPayment
                            Else
                                Throw New Exception("BabelBank was not able to remove all payments from the Camt.053-file." & vbCrLf & "Filename: " & oBabelFile.FilenameIn)
                            End If
                        Next oBatch
                    ElseIf oBabelFile.Batches.Count = 0 Then
                        '26.06.2017 - Added this ElseIf to deal with empty files.
                        'This is OK the file was empty from the beginning
                    Else
                        Throw New Exception("BabelBank was not able to remove all batches from the Camt.053-file." & vbCrLf & "Filename: " & oBabelFile.FilenameIn)
                    End If

                    'Check the amounts
                    If Not IsEqualAmount(nBatchInvoiceAmount, nPaymentInvoiceAmount) Then
                        Throw New Exception("The invoiceamount on batch is not equal to the total amount of the payments." & vbCrLf & "Amount on batch: " & ConvertFromAmountToString(nBatchInvoiceAmount, " ", ",") & vbCrLf & "Amount on payment: " & ConvertFromAmountToString(nPaymentInvoiceAmount, " ", ","))
                    End If
                    If Not IsEqualAmount(nBatchTransferredAmount, nPaymentTransferredAmount) Then
                        Throw New Exception("The transferamount on batch is not equal to the total amount of the payments." & vbCrLf & "Amount on batch: " & ConvertFromAmountToString(nBatchTransferredAmount, " ", ",") & vbCrLf & "Amount on payment: " & ConvertFromAmountToString(nPaymentTransferredAmount, " ", ","))
                    End If
                    If Not IsEqualAmount(nBatchInvoiceAmount, oBabelFile.MON_InvoiceAmount) Then
                        Throw New Exception("The invoiceamount on the file is not equal to the total amount of the batches." & vbCrLf & "Amount on file: " & ConvertFromAmountToString(oBabelFile.MON_InvoiceAmount, " ", ",") & vbCrLf & "Amount on batch: " & ConvertFromAmountToString(nBatchInvoiceAmount, " ", ","))
                    End If
                    If Not IsEqualAmount(nBatchTransferredAmount, oBabelFile.MON_TransferredAmount) Then
                        Throw New Exception("The transferamount on the file is not equal to the total amount of the batches." & vbCrLf & "Amount on file: " & ConvertFromAmountToString(oBabelFile.MON_TransferredAmount, " ", ",") & vbCrLf & "Amount on batch: " & ConvertFromAmountToString(nBatchTransferredAmount, " ", ","))
                    End If
                    For Each oBatch In oBabelFile.Batches
                        oBatch.MON_InvoiceAmount = oBabelFile.MON_InvoiceAmount
                        oBatch.MON_TransferredAmount = oBabelFile.MON_TransferredAmount
                        For Each oPayment In oBatch.Payments
                            oPayment.MON_InvoiceAmount = oBatch.MON_InvoiceAmount
                            oPayment.MON_TransferredAmount = oBatch.MON_TransferredAmount
                            oPayment.MON_OriginallyPaidAmount = oPayment.MON_InvoiceAmount
                            oPayment.E_Name = "Saldoendring"
                            oPayment.E_CountryCode = "NO" '12.05.2020 - To avoid AML check
                            oPayment.PayType = "D"
                            oPayment.PayCode = "601"
                            For iInvoiceCounter = oPayment.Invoices.Count To 2 Step -1
                                'oInvoice = oPayment.Invoices.Item(iInvoiceCounter)
                                oPayment.Invoices.Remove(iInvoiceCounter)
                            Next iInvoiceCounter
                            If oPayment.Invoices.Count = 1 Then
                                'OK
                                For Each oInvoice In oPayment.Invoices
                                    oInvoice.MON_InvoiceAmount = oBabelFile.MON_InvoiceAmount
                                    oInvoice.MON_TransferredAmount = oBabelFile.MON_TransferredAmount
                                Next oInvoice
                            Else
                                Throw New Exception("BabelBank was not able to remove all invoices from the Camt.053-file." & vbCrLf & "Filename: " & oBabelFile.FilenameIn)
                            End If
                        Next oPayment
                    Next oBatch

                End If
                oBabelFile.ReIndexBatches()
                For Each oBatch In oBabelFile.Batches
                    oBatch.ReIndexPayments()
                    For Each oPayment In oBatch.Payments
                        oPayment.ReIndexInvoices()
                    Next oPayment
                Next oBatch
            Next oBabelFile

            'If Not RunTime() Then
            '    Dim oNewInvoice As vbBabel.Invoice
            '    Dim nDeviation As Double = 0
            '    Dim nRunningTotal As Double = 0
            '    Dim sNewKID As String
            '    Dim sClientNo As String
            '    For Each oBabelFile In oBabelFiles
            '        For Each oBatch In oBabelFile.Batches
            '            For Each oPayment In oBatch.Payments
            '                If oPayment.Invoices.Count = 1 Then
            '                    For iInvoiceCounter = oPayment.Invoices.Count To 1 Step -1
            '                        'oInvoice = oPayment.Invoices.Item(iInvoiceCounter)
            '                        oPayment.Invoices.Remove(iInvoiceCounter)
            '                    Next iInvoiceCounter

            '                    'For Each oInvoice In oPayment.Invoices


            '                    oNewInvoice = oPayment.Invoices.Add
            '                    'If nDeviation <> 0 And CDbl(oMyDal.Reader_GetString("BBRET_Amount")) + nDeviation > 0 Then
            '                    '    oNewInvoice.MON_InvoiceAmount = CDbl(oMyDal.Reader_GetString("BBRET_Amount")) + nDeviation
            '                    '    oNewInvoice.MON_TransferredAmount = oNewInvoice.MON_InvoiceAmount
            '                    '    nDeviation = 0
            '                    'Else
            '                    '    oNewInvoice.MON_InvoiceAmount = CDbl(oMyDal.Reader_GetString("BBRET_Amount"))
            '                    '    oNewInvoice.MON_TransferredAmount = oNewInvoice.MON_InvoiceAmount
            '                    'End If
            '                    nRunningTotal = nRunningTotal + oNewInvoice.MON_InvoiceAmount
            '                    oNewInvoice.MATCH_Final = True
            '                    oNewInvoice.MATCH_Matched = True
            '                    oNewInvoice.MATCH_Original = False
            '                    oNewInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice
            '                    'sNewKID = "94017" & PadLeft(oMyDal.Reader_GetString("BBRET_InvoiceNo"), 8, "0") & "00"
            '                    ' 10.10.2015
            '                    'sNewKID = "9" & sClientNo & PadLeft(oMyDal.Reader_GetString("BBRET_InvoiceNo"), 8, "0") & "00"
            '                    sNewKID = "9874653"
            '                    sNewKID = sNewKID & AddCDV10digit(sNewKID)
            '                    oNewInvoice.Unique_Id = sNewKID
            '                    oNewInvoice.StatusCode = "02"
            '                    oNewInvoice.VB_Profile = oPayment.VB_Profile


            '                    'Next oInvoice
            '                End If
            '            Next oPayment
            '        Next oBatch
            '    Next oBabelFile
            'End If


        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Return True

    End Function
    Public Function TreatSGFinansTemp(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String

        Dim bLOG As Boolean = True

        oFs = New Scripting.FileSystemObject
        oFile = oFs.OpenTextFile("C:\Slett\SG Finans Fact\Cut off CRE vs CAM\Sammenligning\Markerte poster.txt", Scripting.IOMode.ForWriting, True, 0)

        For Each oBabelFile In oBabelFiles
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    If oPayment.PayType = "I" Then
                        sLine = ""
                        sLine = PadLeft(oPayment.E_Name, 40, " ") & " - "
                        sLine = sLine & PadLeft(ConvertFromAmountToString(oPayment.MON_InvoiceAmount, ".", ","), 20, " ") & " - "
                        sLine = sLine & oPayment.DATE_Payment.ToString & " - "
                        sLine = sLine & oPayment.DATE_Value.ToString & " - "
                        oFile.WriteLine(sLine)
                    End If
                Next
            Next
        Next

        oFile.Close()
        oFile = Nothing
        oFs = Nothing

    End Function

    Public Function Treat_SGFinansPrepareKID(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim sKID As String
        Dim bAddKID As Boolean
        Dim bReturnValue As Boolean = True

        Dim oFs As Scripting.FileSystemObject
        Dim oFile As Scripting.TextStream
        Dim sLine As String

        Dim bLOG As Boolean = True

        If bLOG Then
            oFs = New Scripting.FileSystemObject
            oFile = oFs.OpenTextFile("C:\BabelBankPro\Innbetaling\Konvertert KID.txt", Scripting.IOMode.ForAppending, True, 0)
        End If

        For Each oBabelFile In oBabelFiles
            For Each oBatch In oBabelFile.Batches
                For Each oPayment In oBatch.Payments
                    If IsOCR((oPayment.PayCode)) Then

                        For Each oInvoice In oPayment.Invoices

                            sKID = Trim(oInvoice.Unique_Id)

                            ' 10.10.2015
                            ' Ref Morten Nissen Lie, SG
                            ' for KIDs starting with 940170 and 91004 will need to cut from 25 to 16, and then send to the old 16-kid-routine;
                            If Left(sKID, 5) = "91004" Or Left(sKID, 5) = "94017" Then
                                ' 09.11.2015 Noen er allerede p� 16 pos, ikke kutt disse
                                If Len(sKID) <> 16 Then
                                    sKID = Left$(sKID, 5) & Mid$(sKID, 15, 10)
                                    sKID = sKID & AddCDV10digit(sKID)
                                End If
                            End If

                            Select Case Len(sKID)
                                Case 15 'Remove first digit in invoice number, and add 00 after the invoice number and calculate the cdv digit
                                    sKID = Left(sKID, 5) & Mid(sKID, 7, 8) & "00"
                                    sKID = sKID & AddCDV10digit(sKID)
                                Case 16 'Nothing to do
                                    ' XOKNET 05.11.2015 sKID already set some lines abovce
                                    'sKID = Trim$(oInvoice.Unique_Id)

                                    'New code12.02.2008
                                    'To treat some KIDs that deviates from the standard
                                    If Left(sKID, 7) = "0139166" Then
                                        '9166 - Cargo Partner
                                        '0139166014223400 - 9916600422340000
                                        sKID = "99166" & PadLeft(Mid(sKID, 10, 6), 8, "0") & "00"
                                        sKID = sKID & AddCDV10digit(sKID)
                                        '23.10.2009 - From after instrudtions from Morten Nissen-Lie
                                    ElseIf Left(sKID, 7) = "9129099" Then
                                        'Old code - ElseIf Left$(sKID, 5) = "91290" Then
                                        '1290 - IFCO
                                        '9129099050297745 - 9129099050297000
                                        sKID = Left(sKID, 13) & "00"
                                        sKID = sKID & AddCDV10digit(sKID)
                                    ElseIf Left(sKID, 7) = "0109327" Then
                                        '1327 - Strandco
                                        '0109327491670003 - 9132700491670000
                                        sKID = "91327000" & Mid(sKID, 8, 5) & "00"
                                        sKID = sKID & AddCDV10digit(sKID)
                                        '28.20.2009 - Changed after instructions from Roar Gl�tta. The KID may also start with "1"
                                    ElseIf Left(sKID, 1) = "0" Or Left(sKID, 1) = "1" Then
                                        'Old code
                                        'ElseIf Left$(sKID, 1) = "0" Then
                                        If Mid(sKID, 6, 4) = "4779" Then
                                            '4779 - New Phone
                                            '0394947790954305 - 9477900095430000
                                            sKID = "94779" & PadLeft(Mid(sKID, 10, 6), 8, "0") & "00"
                                            sKID = sKID & AddCDV10digit(sKID)
                                        End If
                                        'ElseIf Left$(sKID, 6) = "940170" Then 'Special for Kristiansand. The code will be moved somewhere else
                                        ' XOKNET 10.10.2015 added 91004
                                    ElseIf Left$(sKID, 6) = "940170" Or Left$(sKID, 5) = "91004" Then 'Special for Kristiansand. The code will be moved somewhere else
                                        ' XOKNET 05.11.2015 - her m� vi ogs� gj�re om fra 25 til 16 KID for basisinnbetaling
                                        oInvoice.Unique_Id = sKID

                                        If SplitKIDperInvoiceForSGFinance(oInvoice.Unique_Id, oPayment) Then
                                            'OK
                                        Else
                                            'OK
                                        End If
                                        'This payment should mow be OK, so just exit.
                                        'Important, do not traverse through more invoices

                                        Exit For

                                    End If
                                Case 25 'Remove debtornumber and recalculate the cdv digit
                                    sKID = Left(sKID, 5) & Mid(sKID, 15, 10)
                                    sKID = sKID & AddCDV10digit(sKID)
                                Case Else
                                    'Err.Raise 77777, "Wr_AquariusOCR", "Ugyldig lengde p� KID" & vbCrLf & "KID: " & Trim$(oInvoice.Unique_Id)
                            End Select

                            If bLOG Then
                                If oInvoice.Unique_Id <> sKID Then
                                    sLine = "KOnvertert KID - Gammel KID:" & oInvoice.Unique_Id & " Ny KID:" & sKID & " " & oPayment.E_Name & " " & oPayment.MON_InvoiceAmount.ToString
                                    'oFile.WriteLine(sLine)
                                End If
                            End If

                            oInvoice.Unique_Id = sKID

                        Next oInvoice
                    End If
                Next oPayment
            Next oBatch
        Next oBabelFile

        If bLOG Then
            If Not oFile Is Nothing Then
                oFile.Close()
                oFile = Nothing
            End If

            If Not oFs Is Nothing Then
                oFs = Nothing
            End If
        End If

        Treat_SGFinansPrepareKID = bReturnValue

    End Function
    Private Function SplitKIDperInvoiceForSGFinance(ByRef sUnique_Id As String, ByRef oPayment As vbBabel.Payment) As Boolean
        Dim oMyDal As vbBabel.DAL = Nothing
        Dim sMySQL As String = ""
        Dim iRecordCounter As Integer = 0
        Dim oInvoice As vbBabel.Invoice
        Dim oNewInvoice As vbBabel.Invoice
        Dim aQueryArray(,,) As String
        Dim lCompanyNo As Integer
        Dim sProvider As String
        Dim sUID, sPWD As String
        Dim sMySQL1, sMyModifiedSQL1 As String
        Dim sMySQL2, sMyModifiedSQL2 As String
        Dim lCounter As Integer
        Dim nTotalAmount As Double
        Dim nRunningTotal As Double
        Dim bReturnValue As Boolean
        Dim bContinue As Boolean
        Dim sDocumentDate As String = ""
        Dim sCustomerNo As String = ""
        Dim sTempInvoiceNo As String = ""
        Dim sTempAmount As String
        Dim sNewKID As String
        Dim bNegativeAmountExist As Boolean
        Dim nDeviation As Double

        Dim bWriteInfo As Boolean
        Dim sInfoString As String
        Dim iInvoiceCounter As Integer

        '10.10.2015
        Dim sClientNo As String

        sClientNo = Mid(sUnique_Id, 2, 4)   ' 4017 or 1004

        sInfoString = "KID: " & sUnique_Id

        bReturnValue = False

        If oPayment.Invoices.Count = 1 Then

            lCompanyNo = 1 'TODO - Hardcoded CompanyNo
            ''Build an array consisting of the ConnectionStrings and Queries for aftermatcinh agianst the ERP-system.
            aQueryArray = GetERPDBInfo(lCompanyNo, 5)
            'sProvider = aQueryArray(0, 0, 0) ' Holds connectionstring
            'sUID = aQueryArray(0, 1, 0)
            'sPWD = aQueryArray(0, 2, 0)
            '' Change UID/PWD:
            '' Fill in UserID and Password
            'sProvider = Replace(sProvider, "=UID", "=" & sUID, , , CompareMethod.Text)
            'sProvider = Replace(sProvider, "=PWD", "=" & sPWD, , , CompareMethod.Text)
            '' Logon
            oMyDal = New vbBabel.DAL
            oMyDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
            oMyDal.Company_ID = lCompanyNo
            oMyDal.DBProfile_ID = 1 'TODO - Hardcoded DBProfile_ID
            If Not oMyDal.ConnectToDB() Then
                Throw New System.Exception(oMyDal.ErrorMessage)
            End If

            sMySQL1 = ""
            sMySQL2 = ""

            'sMySQL1 = "SELECT LI.DocumentDate AS BBRET_DocumentDate, AD.ClientNumber AS BBRET_ClientNo, AD.DebtorNumber AS BBRET_CustomerNo, LI.DocumentNumber AS BBRET_InvoiceNo, LI.BalanceDAC*100 as BBRET_Amount, LI.ActualBalanceDAC*100 as ActualRestAmount, LI.CurrencyCode as BBRET_Currency, LI.DueDate AS FFDato FROM LedgerItems LI inner join AgreementDetails_DEB AD ON AD.AgreementID = LI.DebtorAgreementID WHERE LI.Openitem = 1 AND LI.DocumentNumber = 'BB_InvoiceIdentifier' AND AD.ClientNumber = RIGHT('0000000' + '4017',7) ORDER BY AD.ClientNumber ASC, LI.DueDate ASC"
            'sMySQL2 = "SELECT AD.ClientNumber AS BBRET_ClientNo, AD.DebtorNumber AS BBRET_CustomerNo, LI.DocumentNumber AS BBRET_InvoiceNo, LI.BalanceDAC*100 as BBRET_Amount, LI.ActualBalanceDAC*100 as ActualRestAmount, LI.CurrencyCode as BBRET_Currency FROM LedgerItems LI inner join AgreementDetails_DEB AD ON AD.AgreementID = LI.DebtorAgreementID WHERE LI.Openitem = 1 AND AD.DebtorNumber = 'BB_CustomerNo' AND AD.ClientNumber = RIGHT('0000000' + '4017',7) AND LI.DocumentDate = CONVERT(datetime, 'BB_DocumentDate')"
            '10.10.2015
            sMySQL1 = "SELECT LI.DocumentDate AS BBRET_DocumentDate, AD.ClientNumber AS BBRET_ClientNo, AD.DebtorNumber AS BBRET_CustomerNo, LI.DocumentNumber AS BBRET_InvoiceNo, LI.BalanceDAC*100 as BBRET_Amount, LI.ActualBalanceDAC*100 as ActualRestAmount, LI.CurrencyCode as BBRET_Currency, LI.DueDate AS FFDato FROM LedgerItems LI inner join AgreementDetails_DEB AD ON AD.AgreementID = LI.DebtorAgreementID WHERE LI.Openitem = 1 AND LI.DocumentNumber = 'BB_InvoiceIdentifier' AND AD.ClientNumber = RIGHT('0000000' + '" & sClientNo & "',7) ORDER BY AD.ClientNumber ASC, LI.DueDate ASC"
            sMySQL2 = "SELECT AD.ClientNumber AS BBRET_ClientNo, AD.DebtorNumber AS BBRET_CustomerNo, LI.DocumentNumber AS BBRET_InvoiceNo, LI.BalanceDAC*100 as BBRET_Amount, LI.ActualBalanceDAC*100 as ActualRestAmount, LI.CurrencyCode as BBRET_Currency FROM LedgerItems LI inner join AgreementDetails_DEB AD ON AD.AgreementID = LI.DebtorAgreementID WHERE LI.Openitem = 1 AND AD.DebtorNumber = 'BB_CustomerNo' AND AD.ClientNumber = RIGHT('0000000' + '" & sClientNo & "',7) AND LI.DocumentDate = CONVERT(datetime, 'BB_DocumentDate')"
            nTotalAmount = 0
            nRunningTotal = 0

            For lCounter = 0 To UBound(aQueryArray, 3)

                If UCase(aQueryArray(0, 4, lCounter)) = "SAMLEFAKTURA1 9999" Then
                    'Name of the SQL's retrieve the duedate on the invoice of the KID.
                    sMySQL1 = aQueryArray(0, 0, lCounter)

                End If
                If UCase(aQueryArray(0, 4, lCounter)) = "SAMLEFAKTURA2 9999" Then
                    'Name of the SQL's to use for checking the samlefaktura.
                    sMySQL2 = aQueryArray(0, 0, lCounter)
                End If

            Next lCounter

            'If oPayment.E_Name.Trim = "EXPANDIA MODULER AS" Then
            '    MsgBox("Kommet til SG_KID")
            '    bShowMessage = True
            'End If

            If Not EmptyString(sMySQL1) And Not EmptyString(sMySQL2) Then
                If InStr(1, sMySQL2, "BBRET_Amount", CompareMethod.Text) Then

                    sMyModifiedSQL1 = Replace(sMySQL1, "BB_InvoiceIdentifier", RemoveLeadingCharacters(Mid(sUnique_Id, 7, 7), "0"), , , CompareMethod.Text)
                    sMyModifiedSQL1 = Replace(sMyModifiedSQL1, "BB_ClientNo", sClientNo, , , CompareMethod.Text)

                    sInfoString = sInfoString & " - " & RemoveLeadingCharacters(Mid(sUnique_Id, 7, 7), "0")

                    oMyDal.SQL = sMyModifiedSQL1
                    iRecordCounter = 0
                    bContinue = False
                    If oMyDal.Reader_Execute() Then
                        Do While oMyDal.Reader_ReadRecord
                            iRecordCounter = iRecordCounter + 1
                            If InStr(1, sMyModifiedSQL1, "BBRET_DocumentDate", CompareMethod.Text) > 0 Then
                                If InStr(1, sMyModifiedSQL1, "BBRET_CustomerNo", CompareMethod.Text) > 0 Then
                                    sDocumentDate = oMyDal.Reader_GetString("BBRET_DocumentDate")
                                    If Not oMyDal.Reader_GetString("BBRET_CustomerNo") = "" Then
                                        If Not sDocumentDate = "" Then
                                            'We need to retrieve the invoiceNo
                                            If InStr(1, sMySQL2, "BBRET_InvoiceNo", CompareMethod.Text) > 0 Then
                                                If Mid(sDocumentDate, 3, 1) = "." And Mid(sDocumentDate, 6, 1) = "." And Len(sDocumentDate) = 10 Then
                                                    sDocumentDate = Mid(sDocumentDate, 7, 4) & Mid(sDocumentDate, 4, 2) & Mid(sDocumentDate, 1, 2)
                                                    sInfoString = sInfoString & " - " & "Funnet en faktura med dato " & sDocumentDate & vbCrLf
                                                    sCustomerNo = oMyDal.Reader_GetString("BBRET_CustomerNo")
                                                    bContinue = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        Loop
                    Else
                        Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                    End If

                    If iRecordCounter > 1 Then
                        bContinue = False
                        sInfoString = sInfoString & vbCrLf & "AVBRYTER PROSESS! Funnet " & iRecordCounter.ToString & " poster."
                    End If

                    If bContinue Then
                        nTotalAmount = 0
                        bContinue = True
                        bNegativeAmountExist = False
                        iRecordCounter = 0

                        sMyModifiedSQL2 = Replace(sMySQL2, "BB_DocumentDate", sDocumentDate, , , CompareMethod.Text)
                        'sMyModifiedSQL2 = Replace(sMyModifiedSQL2, "BB_ClientNo", "4017", , , CompareMethod.Text)
                        ' 10.10.2015
                        sMyModifiedSQL2 = Replace(sMyModifiedSQL2, "BB_ClientNo", sClientNo, , , vbTextCompare)

                        sMyModifiedSQL2 = Replace(sMyModifiedSQL2, "BB_CustomerNo", sCustomerNo, , , CompareMethod.Text)
                        oMyDal.SQL = sMyModifiedSQL2

                        If oMyDal.Reader_Execute() Then
                            If oMyDal.Reader_HasRows Then
                                Do While oMyDal.Reader_ReadRecord
                                    iRecordCounter = iRecordCounter + 1
                                    sTempInvoiceNo = oMyDal.Reader_GetString("BBRET_InvoiceNo")
                                    sTempAmount = oMyDal.Reader_GetString("BBRET_Amount")

                                    If Not sTempInvoiceNo = "" Then
                                        If Not sTempAmount = "" Then
                                            nTotalAmount = nTotalAmount + CDbl(sTempAmount)
                                            '29.04.2020 - Removed this, allowing negative amounts
                                            'If CDbl(sTempAmount) < 0 Then
                                            '    bNegativeAmountExist = True
                                            '    bContinue = False
                                            '    Exit Do
                                            'End If
                                        Else
                                            bContinue = False
                                            Exit Do
                                        End If
                                    Else
                                        bContinue = False
                                        Exit Do
                                    End If
                                Loop
                            Else
                                sInfoString = sInfoString & "AVBRYTER PROSESS! Funnet 0 fakturaer p� denne dato."
                            End If
                        Else
                            Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                        End If

                        If iRecordCounter > 1 Then
                            sInfoString = sInfoString & "Funnet " & iRecordCounter.ToString & " fakturaer."
                        Else
                            bContinue = False
                        End If

                        '29.04.2020 -removed this
                        'If bNegativeAmountExist Then
                        '    sInfoString = sInfoString & vbCrLf & "AVBRYTER PROSESS! Samlefaktura inneholder kreditnota."
                        '    bContinue = False
                        'End If

                        nDeviation = 0
                        If Not IsEqualAmount(nTotalAmount, oPayment.MON_InvoiceAmount) Then

                            If System.Math.Abs(oPayment.MON_InvoiceAmount - nTotalAmount) < 100 Then
                                nDeviation = oPayment.MON_InvoiceAmount - nTotalAmount
                                nTotalAmount = oPayment.MON_InvoiceAmount
                                sInfoString = sInfoString & "�resavvik p� " & Trim(Str(nDeviation)) & " er godtatt."
                            End If

                        End If

                        If IsEqualAmount(nTotalAmount, oPayment.MON_InvoiceAmount) And iRecordCounter > 1 And bContinue Then
                            'OK, We've found the invoices
                            'Don't need to do anything if we have only 1 invoice

                            '07.04.2020 - Remove the invoice entirely
                            For iInvoiceCounter = oPayment.Invoices.Count To 1 Step -1
                                'oInvoice = oPayment.Invoices.Item(iInvoiceCounter)
                                oPayment.Invoices.Remove(iInvoiceCounter)
                            Next iInvoiceCounter

                            'Old code
                            ''Set the original invoce as not final
                            'For Each oInvoice In oPayment.Invoices
                            '    oInvoice.MATCH_Final = False
                            '    oInvoice.MATCH_Matched = False
                            'Next oInvoice

                            'Add the new invoces from the recordset
                            oMyDal.SQL = sMyModifiedSQL2

                            If oMyDal.Reader_Execute() Then
                                Do While oMyDal.Reader_ReadRecord

                                    oNewInvoice = oPayment.Invoices.Add
                                    If nDeviation <> 0 And CDbl(oMyDal.Reader_GetString("BBRET_Amount")) + nDeviation > 0 Then
                                        oNewInvoice.MON_InvoiceAmount = CDbl(oMyDal.Reader_GetString("BBRET_Amount")) + nDeviation
                                        oNewInvoice.MON_TransferredAmount = oNewInvoice.MON_InvoiceAmount
                                        nDeviation = 0
                                    Else
                                        oNewInvoice.MON_InvoiceAmount = CDbl(oMyDal.Reader_GetString("BBRET_Amount"))
                                        oNewInvoice.MON_TransferredAmount = oNewInvoice.MON_InvoiceAmount
                                    End If

                                    nRunningTotal = nRunningTotal + oNewInvoice.MON_InvoiceAmount
                                    oNewInvoice.MATCH_Final = True
                                    oNewInvoice.MATCH_Matched = False '07.04.2020 - True
                                    oNewInvoice.MATCH_Original = True '07.04.2020 - False
                                    oNewInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.OpenInvoice '07.04.2020 - vbBabel.BabelFiles.MatchType.MatchedOnInvoice
                                    'sNewKID = "94017" & PadLeft(oMyDal.Reader_GetString("BBRET_InvoiceNo"), 8, "0") & "00"
                                    ' 10.10.2015
                                    sNewKID = "9" & sClientNo & PadLeft(oMyDal.Reader_GetString("BBRET_InvoiceNo"), 8, "0") & "00"

                                    sNewKID = sNewKID & AddCDV10digit(sNewKID)
                                    oNewInvoice.Unique_Id = sNewKID
                                    oNewInvoice.StatusCode = "02"
                                    oNewInvoice.VB_Profile = oPayment.VB_Profile

                                Loop
                            Else
                                Throw New Exception(LRSCommon(45002) & oMyDal.ErrorMessage)
                            End If

                            'Just check if everything is OK
                            If IsEqualAmount(nRunningTotal, oPayment.MON_InvoiceAmount) Then
                                bReturnValue = True
                                sInfoString = sInfoString & vbCrLf & "VELLYKKET UTBYING!!!!! Innbetalt bel�p: " & oPayment.MON_InvoiceAmount.ToString & " Totalt fakturert bel�p: " & nTotalAmount.ToString
                            Else
                                sInfoString = sInfoString & vbCrLf & "AVBRYTER PROSESS! Sluttkontroll avsl�rer avvik. Opphever samlefaktura."
                                oPayment.MATCH_Undo()
                            End If

                        Else

                            If bNegativeAmountExist Then
                                'Don't write anything
                            ElseIf iRecordCounter = 1 Then
                                sInfoString = sInfoString & vbCrLf & "AVBRYTER PROSESS! Kun en faktura."
                            Else
                                sInfoString = sInfoString & vbCrLf & "AVBRYTER PROSESS! Bel�p stemmer ikke. Innbetalt bel�p: " & oPayment.MON_InvoiceAmount.ToString & " Totalt fakturert bel�p: " & nTotalAmount.ToString
                            End If

                            'bReturnValue = False

                        End If

                    End If 'If bContinue

                End If
            End If 'If Not EmptyString(sMySQL1) And Not EmptyString(sMySQL2) Then

            'If bShowMessage Then
            '    MsgBox("F�rste faktura: " & oPayment.Invoices.Item(1).MON_InvoiceAmount.ToString)
            '    MsgBox("Andre faktura: " & oPayment.Invoices.Item(2).MON_InvoiceAmount.ToString)
            '    MsgBox("Tredje faktura: " & oPayment.Invoices.Item(3).MON_InvoiceAmount.ToString)
            'End If

        Else

        End If 'If oInvoice.Count = 1 Then

        If Not oMyDal Is Nothing Then
            oMyDal.Close()
            oMyDal = Nothing
        End If

        'If Not RunTime Then
        '    Set oFile = oFs.OpenTextFile("C:\SG Finans\KID.txt", ForAppending, True, 0)
        '    oFile.WriteLine sInfoString & vbCrLf & vbCrLf
        '    oFile.Close
        '    Set oFile = Nothing
        '    Set oFs = Nothing
        'End If

        SplitKIDperInvoiceForSGFinance = bReturnValue

    End Function

    '16.11.2016 - Converted from VB6
    Public Function PrepareKIDConversionKREDINOR(ByVal oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim oMyERPDal As vbBabel.DAL = Nothing
        Dim lRecCount As Long
        Dim sMySetupSQL As String
        Dim sMySQL As String
        Dim sClientNo As String
        Dim sAccountNo As String
        Dim lCompanyNo As Long
        Dim sProvider As String
        Dim sUID As String
        Dim sPWD As String
        Dim aQueryArray(,,) As String
        Dim lCounter As Long
        Dim sTmp As String

        sProvider = ""
        sUID = ""
        sPWD = ""
        lCompanyNo = 1

        Try

            aQueryArray = GetERPDBInfo(1, 5)

            For lCounter = 0 To UBound(aQueryArray, 3)
                Select Case UCase(aQueryArray(0, 2, lCounter))

                    Case "KID_CONVERT"
                        sMySetupSQL = Trim$(aQueryArray(0, 0, lCounter))
                End Select
            Next lCounter

            If Not EmptyString(sMySetupSQL) Then

                ' connect to erp system
                'Build an array consisting of the ConnectionStrings and Query agianst the ERP-system.
                aQueryArray = GetERPDBInfo(lCompanyNo, 1)
                sProvider = aQueryArray(0, 0, 0)  ' Connectionstring
                sUID = aQueryArray(0, 1, 0)
                sPWD = aQueryArray(0, 2, 0)

                ' Fill in UserID and Password
                sProvider = Replace(UCase(sProvider), "=UID", "=" & sUID)
                sProvider = Replace(UCase(sProvider), "=PWD", "=" & sPWD)

                oMyERPDal = New vbBabel.DAL
                'oMyERPDal.Connectionstring = sProvider
                'oMyERPDal.Provider = vbBabel.DAL.ProviderType.Odbc
                oMyERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                oMyERPDal.Company_ID = lCompanyNo
                oMyERPDal.DBProfile_ID = 1
                If Not oMyERPDal.ConnectToDB() Then
                    Throw New System.Exception(oMyERPDal.ErrorMessage)
                End If

                For Each oBabelFile In oBabelFiles
                    For Each oBatch In oBabelFile.Batches

                        If oBatch.Payments.Count > 0 Then

                            For Each oPayment In oBatch.Payments

                                If IsOCR(oPayment.PayCode) Then

                                    For Each oInvoice In oPayment.Invoices
                                        sMySQL = Replace(sMySetupSQL, "BB_InvoiceIdentifier", oInvoice.Unique_Id, , , vbTextCompare)
                                        ' run the sql

                                        ' run the sql
                                        oMyERPDal.SQL = sMySQL
                                        If oMyERPDal.Reader_Execute() Then
                                            If oMyERPDal.Reader_HasRows Then
                                                Do While oMyERPDal.Reader_ReadRecord
                                                    sTmp = oMyERPDal.Reader_GetString("BBRET_MatchID")
                                                    oInvoice.Unique_Id = sTmp
                                                    'oInvoice.MATCH_ID = sTmp
                                                    Exit Do
                                                Loop
                                            End If
                                        End If

                                        'rsERP.Open(sMySQL, conERPConnection, adOpenStatic, adLockOptimistic)
                                        'If BB_RecordCount(rsERP, True) = 1 Then
                                        ' oInvoice.Unique_Id = rsERP!BBRET_MatchID
                                        'End If
                                        'rsERP.Close()
                                    Next oInvoice
                                End If
                            Next oPayment
                        End If
                    Next oBatch
                Next oBabelFile
            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        PrepareKIDConversionKREDINOR = True

    End Function
    Public Function Treat_SGFinansAML(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal bLOG As Boolean, ByVal oBabelLog As vbLog.vbLogging) As Boolean
        Dim oMyERPDal As vbBabel.DAL = Nothing
        Dim sMySetupSQL As String
        Dim sMySQL As String
        Dim lCompanyNo As Long
        Dim sProvider As String
        Dim sUID As String
        Dim sPWD As String
        Dim aQueryArray(,,) As String
        Dim lCounter As Long
        Dim lCounter2 As Long
        Dim sTmp As String '= "<SGEDW_Deliveries.dbo.ComplianceEntityList ListName='DefenceExlusionList' ListType='Company' SearchValue=' '/><SGEDW_Deliveries.dbo.ComplianceEntityList ListName='E S Watchlist Company' ListType=Company' SearchValue=' '/><SGEDW_Deliveries.dbo.ComplianceEntityList ListName='StopList Company' ListType='Company' SearchValue=' '/>"
        Dim sTmpNew As String = ""
        Dim sTmpLine As String = ""
        Dim iPos As Integer
        Dim bReturnValue As Boolean = False
        Dim sFilename As String = ""
        Dim bRunTheSQL As Boolean = True
        Dim iCounter As Integer
        Dim oFile As Scripting.TextStream
        Dim sContent As String
        Dim bContinue As Boolean
        Dim bWorkBookFound As Boolean = True

        Dim oFs As New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
        Dim XMLDoc As New System.Xml.XmlDocument
        Dim XMLSGInternalStatusDoc As New System.Xml.XmlDocument
        Dim nodeTransTypeList As System.Xml.XmlNodeList
        Dim nodeTransType As System.Xml.XmlNode
        Dim node As System.Xml.XmlNode

        Dim oExcelBook As Microsoft.Office.Interop.Excel.Workbook
        Dim oExcelSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim appExcel As Microsoft.Office.Interop.Excel.Application
        Dim lRowNo As Long
        Dim iEmptyRows As Integer
        Dim sCharactersToRemove As String = ",.-/ "

        Dim TransTypeDictionary As New System.Collections.Generic.Dictionary(Of String, String)
        Dim SGInternalStatusDictionary As New System.Collections.Generic.Dictionary(Of String, String) '10.03.2020 - Added code rearding SGInternalStatus
        Dim ClearedCompayDictionary As New System.Collections.Generic.Dictionary(Of String, String)
        Dim AcceptedTransactionDictionary As New System.Collections.Generic.Dictionary(Of String, String)
        Dim sKey As String
        Dim sValue As String
        'Dim pair As System.Collections.Generic.KeyValuePair(Of String, String)
        Dim sValidPayCode As String = ""
        Dim sSGInternalStatus As String
        Dim iFinalSGInternalStatus As Integer
        'Dim iSGInternalStatus As Integer
        Dim sSGInternalStatusAction As String
        Dim sValueInString As String
        Dim sValidValue As String

        Dim bAddItem As Boolean
        Dim sAcceptedType As String
        Dim sAcceptedText As String
        '04.11.2019 - Added next 7
        Dim sOld_I_Account As String = "XXX"
        Dim sAccountCurrencyCode As String = ""
        Dim oMyBBDal As vbBabel.DAL = Nothing
        Dim aAMLAccounts As String(,)
        Dim iAccountCounter As Integer
        Dim bAccountFound As Boolean = False
        Dim aArchiveArray As String()
        Dim sCountryCodeToUse As String
        Dim sClientCountryCode As String
        Dim sSpecial As String = ""
        Dim bOK As Boolean

        'Dim sTestString As String = "<t ListName=" & Chr(34) & "Country Risk Rating" & Chr(34) & " Match=" & Chr(34) & "Country" & Chr(34) & " SearchValue=" & Chr(34) & "RU" & Chr(34) & " SGInternalStatus=" & Chr(34) & "40" & Chr(34) & " CountryRiskRatingLevel=" & Chr(34) & "E&amp;S" & Chr(34) & "/><t ListName=" & Chr(34) & "StopList Company" & Chr(34) & " Match=" & Chr(34) & "Company" & Chr(34) & " SearchValue=" & Chr(34) & "Kjell" & Chr(34) & " SGInternalStatus=" & Chr(34) & "0" & Chr(34) & "/><t ListName=" & Chr(34) & "WorldCheck" & Chr(34) & " Match=" & Chr(34) & "BONDEVIK Kjell Magne Johannesson" & Chr(34) & " SearchValue=" & Chr(34) & "Kjell" & Chr(34) & "/><t ListName=" & Chr(34) & "WorldCheck" & Chr(34) & " Match=" & Chr(34) & "LAUGERUD GARCIA Kjell Eugenio" & Chr(34) & " SearchValue=" & Chr(34) & "Kjell" & Chr(34) & "/><t ListName=" & Chr(34) & "WorldCheck" & Chr(34) & " Match=" & Chr(34) & "KJELL PEDERSEN ENTREPRISE A/S " & Chr(34) & " SearchValue=" & Chr(34) & "Kjell" & Chr(34) & "/>"
        Dim sTestString As String

        Dim oBabelReport As vbBabel.BabelReport

        sTestString = "<t ListName=" & Chr(34) & "WorldCheck" & Chr(34) & " Match=" & Chr(34) & "PUTIN Vladimir Vladimirovich" & Chr(34) & " SearchValue=" & Chr(34) & "Vladimir Putin" & Chr(34) & " SGInternalStatus=" & Chr(34) & "20" & Chr(34) & " Category_Name=" & Chr(34) & "POLITICAL INDIVIDUAL" & Chr(34) & " SubCategory_Name=" & Chr(34) & "PEP" & Chr(34) & " FurtherInformation_Text=" & Chr(34) & "[AUSTRALIA SANCTIONS - DFAT-AS] No 6678 (Feb 2022 - addition). PRIMARY NAME: Vladimir Vladimirovich PUTIN. Date of Birth: 1952-10-07. Place of Birth: Saint Petersburg, Russia. Additional Information: President of the Russian Federation. [CANADA SANCTIONS " & Chr(34) & " ExternalSource_Text=" & Chr(34) & "http://er.ru/rubr.shtml?110085 http://fra.gov.ky/contents/page/1 http://gazeta.ua/ru/articles/politics/_zena-medvedchuka-rasskazala-pochemu-putin-stal-ih-kumom-foto/191192 http://grani.ru/Economy/m.103096.html http://kremlin.ru/news/15224 http://lenta.ru/" & Chr(34) & "/><t ListName=" & Chr(34) & "WorldCheck" & Chr(34) & " Match=" & Chr(34) & "PUTIN Vladimir Vladimirovich" & Chr(34) & " SearchValue=" & Chr(34) & "Vladimir Putin" & Chr(34) & " SGInternalStatus=" & Chr(34) & "30" & Chr(34) & " Category_Name=" & Chr(34) & "POLITICAL INDIVIDUAL" & Chr(34) & " SubCategory_Name=" & Chr(34) & "PEP" & Chr(34) & " FurtherInformation_Text=" & Chr(34) & "[AUSTRALIA SANCTIONS - DFAT-AS] No 6678 (Feb 2022 - addition). PRIMARY NAME: Vladimir Vladimirovich PUTIN. Date of Birth: 1952-10-07. Place of Birth: Saint Petersburg, Russia. Additional Information: President of the Russian Federation. [CANADA SANCTIONS " & Chr(34) & " ExternalSource_Text=" & Chr(34) & "http://er.ru/rubr.shtml?110085 http://fra.gov.ky/contents/page/1 http://gazeta.ua/ru/articles/politics/_zena-medvedchuka-rasskazala-pochemu-putin-stal-ih-kumom-foto/191192 http://grani.ru/Economy/m.103096.html http://kremlin.ru/news/15224 http://lenta.ru/" & Chr(34) & "/><t ListName=" & Chr(34) & "WorldCheck" & Chr(34) & " Match=" & Chr(34) & "Putin Vladimir Vladimirovich" & Chr(34) & " SearchValue=" & Chr(34) & "Vladimir Putin" & Chr(34) & " SGInternalStatus=" & Chr(34) & "20" & Chr(34) & " Category_Name=" & Chr(34) & "POLITICAL INDIVIDUAL" & Chr(34) & " SubCategory_Name=" & Chr(34) & "KJELL" & Chr(34) & " FurtherInformation_Text=" & Chr(34) & "[AUSTRALIA SANCTIONS - DFAT-AS] No 6678 (Feb 2022 - addition). PRIMARY NAME: Vladimir Vladimirovich PUTIN. Date of Birth: 1952-10-07. Place of Birth: Saint Petersburg, Russia. Additional Information: President of the Russian Federation. [CANADA SANCTIONS " & Chr(34) & " ExternalSource_Text=" & Chr(34) & "http://er.ru/rubr.shtml?110085 http://fra.gov.ky/contents/page/1 http://gazeta.ua/ru/articles/politics/_zena-medvedchuka-rasskazala-pochemu-putin-stal-ih-kumom-foto/191192 http://grani.ru/Economy/m.103096.html http://kremlin.ru/news/15224 http://lenta.ru/" & Chr(34) & "/>"
        '04.06.2020 - Removed some logging

        Try

            '10.02.2021 - Added this code
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If IsOCR(oPayment.PayCode) Then
                            For Each oInvoice In oPayment.Invoices
                                If Not EmptyString(oInvoice.Unique_Id) Then
                                    oPayment.ExtraD2 = oInvoice.Unique_Id.Trim
                                    Exit For
                                End If
                            Next oInvoice
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

            If Not oBabelFiles Is Nothing Then

                If oBabelFiles.VB_Profile.DisableAdminMenus = True Then 'Added 10.12.2020 - Added this IF to diable AML-check

                    bReturnValue = True

                Else

                    'oBabelLog.AddLogEvent("Kommet til spesialrutine.", 0)


                    'FOR TESTING PURPOSES:
                    'sTmp = "<SGEDW_Deliveries.dbo.ComplianceEntityList ListName=" & Chr(34) & "Country Risk Rating" & Chr(34) & " ListType=" & Chr(34) & "Country" & Chr(34) & " SearchValue=" & Chr(34) & "PL" & Chr(34) & " CountryRiskRatingLevel=" & Chr(34) & "MED-LOW" & Chr(34) & "/><SGEDW_Deliveries.dbo.ComplianceEntityList ListName=" & Chr(34) & "E S Watchlist Project" & Chr(34) & " ListType=" & Chr(34) & "Country" & Chr(34) & " SearchValue=" & Chr(34) & "PL" & Chr(34) & "/>"
                    'sTmp = "<ComplianceEntityList ListName=" & Chr(34) & "Country Risk Rating" & Chr(34) & " Match=" & Chr(34) & "Country" & Chr(34) & " SearchValue=" & Chr(34) & "PL" & Chr(34) & " CountryRiskRatingLevel=" & Chr(34) & "MED-LOW" & Chr(34) & "/><SGEDW_Deliveries.dbo.ComplianceEntityList ListName=" & Chr(34) & "E S Watchlist Project" & Chr(34) & " Match=" & Chr(34) & "Country" & Chr(34) & " SearchValue=" & Chr(34) & "PL" & Chr(34) & "/>"

                    'If Not EmptyString(sTmp) Then
                    '    If sTmp.Length > 2 Then
                    '        oInvoice = oPayment.Invoices.Item(oPayment.Invoices.Count)
                    '        sTmp = sTmp.Replace(Chr(34), "'")
                    '        sTmp = sTmp.Replace("<SGEDW_Deliveries.dbo.ComplianceEntityList ", String.Empty)
                    '        sTmp = sTmp.Replace("/>", String.Empty)

                    '        Do While True
                    '            If sTmp.Length > 3 Then
                    '                iPos = 0
                    '                iPos = sTmp.IndexOf("ListName", 3)
                    '                sTmpNew = String.Empty
                    '                If iPos > 0 Then
                    '                    sTmpNew = sTmp.Substring(0, iPos)
                    '                    sTmp = sTmp.Substring(iPos)
                    '                Else
                    '                    sTmpNew = sTmp
                    '                    sTmp = String.Empty
                    '                End If
                    '                If Not EmptyString(sTmpNew) Then
                    '                    oFreeText = oInvoice.Freetexts.Add
                    '                    oFreeText.Text = "TREFF I COMPLIANCE CHECKLIST"
                    '                    iPos = sTmpNew.IndexOf("ListType", 3)
                    '                    sTmpLine = String.Empty
                    '                    If iPos > 0 Then
                    '                        sTmpLine = sTmpNew.Substring(0, iPos)
                    '                        sTmpNew = sTmpNew.Substring(iPos)
                    '                        oFreeText = oInvoice.Freetexts.Add
                    '                        oFreeText.Text = sTmpLine
                    '                    End If

                    '                    iPos = sTmpNew.IndexOf("SearchValue", 3)
                    '                    sTmpLine = String.Empty
                    '                    If iPos > 0 Then
                    '                        sTmpLine = sTmpNew.Substring(0, iPos)
                    '                        sTmpNew = sTmpNew.Substring(iPos)
                    '                        oFreeText = oInvoice.Freetexts.Add
                    '                        oFreeText.Text = sTmpLine
                    '                    End If

                    '                    iPos = sTmpNew.IndexOf("CountryRiskRatingLevel", 3)
                    '                    sTmpLine = String.Empty
                    '                    If iPos > 0 Then
                    '                        sTmpLine = sTmpNew.Substring(0, iPos)
                    '                        sTmpNew = sTmpNew.Substring(iPos)
                    '                        oFreeText = oInvoice.Freetexts.Add
                    '                        oFreeText.Text = sTmpLine
                    '                    End If

                    '                    oFreeText = oInvoice.Freetexts.Add
                    '                    oFreeText.Text = sTmpNew
                    '                End If
                    '            Else
                    '                Exit Do
                    '            End If
                    '        Loop

                    '    End If
                    'End If



                    sProvider = ""
                    sUID = ""
                    sPWD = ""
                    lCompanyNo = 1

                    aQueryArray = GetERPDBInfo(1, 5)

                    For lCounter = 0 To UBound(aQueryArray, 3)
                        Select Case aQueryArray(0, 2, lCounter).ToUpper

                            Case "AML_PROCEDURE"
                                sMySetupSQL = aQueryArray(0, 0, lCounter).Trim.ToUpper

                        End Select
                    Next lCounter

                    If Not EmptyString(sMySetupSQL) And oBabelFiles.Count > 0 Then

                        '04.11.2019 - Added next lines to retrieve accounts to post postive not cleared payments
                        oMyBBDal = New vbBabel.DAL
                        oMyBBDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                        If Not oMyBBDal.ConnectToDB() Then
                            Throw New System.Exception(oMyBBDal.ErrorMessage)
                        End If

                        '18.05.2020 - Changed to LIKE
                        sMySQL = "SELECT Pattern, CurrencyCode FROM MATCH_DiffAccounts Where Company_ID = " & oBabelFiles.VB_Profile.Company_ID.ToString & " AND Name like '%AML IKKE KLARERT%'"
                        iAccountCounter = -1
                        oMyBBDal.SQL = sMySQL
                        If oMyBBDal.Reader_Execute() Then
                            If oMyBBDal.Reader_HasRows Then
                                Do While oMyBBDal.Reader_ReadRecord
                                    iAccountCounter = iAccountCounter + 1
                                    ReDim Preserve aAMLAccounts(1, iAccountCounter)
                                    aAMLAccounts(0, iAccountCounter) = oMyBBDal.Reader_GetString("Pattern")
                                    aAMLAccounts(1, iAccountCounter) = oMyBBDal.Reader_GetString("CurrencyCode")
                                Loop
                            Else
                                'lLastBabelfileID = 0
                            End If

                        Else
                            Throw New Exception(LRSCommon(45002) & oMyBBDal.ErrorMessage)
                        End If

                        '05.11.2019 - Changed to retrieve logoninfo from Archive
                        aArchiveArray = GetArchiveInfo(1)

                        'Connect to Archive-database
                        oMyERPDal = New vbBabel.DAL
                        oMyERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ArchiveDB
                        oMyERPDal.Company_ID = 1 'TODO: Hardcoded Company_ID
                        If Not oMyERPDal.ConnectToDB() Then
                            Throw New System.Exception(oMyERPDal.ErrorMessage)
                        End If

                        '' connect to erp system
                        ''Build an array consisting of the ConnectionStrings and Query agianst the ERP-system.
                        'aQueryArray = GetERPDBInfo(lCompanyNo, 1)
                        'sProvider = aQueryArray(0, 0, 0)  ' Connectionstring
                        'sUID = aQueryArray(0, 1, 0)
                        'sPWD = aQueryArray(0, 2, 0)

                        '' Fill in UserID and Password
                        'sProvider = Replace(UCase(sProvider), "=UID", "=" & sUID)
                        'sProvider = Replace(UCase(sProvider), "=PWD", "=" & sPWD)

                        'oMyERPDal = New vbBabel.DAL
                        ''oMyERPDal.Connectionstring = sProvider
                        ''oMyERPDal.Provider = vbBabel.DAL.ProviderType.Odbc
                        'oMyERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                        'oMyERPDal.Company_ID = lCompanyNo
                        'oMyERPDal.DBProfile_ID = 1

                        'If Not oMyERPDal.ConnectToDB() Then
                        '    Throw New System.Exception(oMyERPDal.ErrorMessage)
                        'End If

                        '05.11.2019 - The use of the XML-file has changed
                        'Before: The list contained transactiontypes which to control
                        'After: The list contains transactions which not to control
                        'Load the file for valid transaction codes
                        'oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
                        If RunTime() Then   'DISKUTERNOTRUNTIME Felles mappe (er felles allerede?)
                            If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml") Then
                                Throw New Exception("Treat_SGFinansAML - Kan ikke finne filen " & My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml, " & vbCrLf & "som benyttes til � se hvilke poster som skal valideres.")
                            Else

                                'oBabelLog.AddLogEvent("Funnet XML-filen: " & My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml", 0)

                                XMLDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml")

                            End If
                            '10.03.2020 - Added code rearding SGInternalStatus
                            If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml") Then
                                Throw New Exception("Treat_SGFinansAML - Kan ikke finne filen " & My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml, " & vbCrLf & "som benyttes til � se hvilke poster som skal valideres.")
                            Else

                                'oBabelLog.AddLogEvent("Funnet XML-filen: " & My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml", 0)

                                XMLSGInternalStatusDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml")

                            End If
                        Else
                            If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml") Then
                                Throw New Exception("Treat_SGFinansAML - Kan ikke finne filen " & My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml" & vbCrLf & "som benyttes til � se hvilke poster som skal valideres.")
                            Else
                                'XMLDoc.async = False
                                XMLDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml")
                            End If
                            If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml") Then
                                Throw New Exception("Treat_SGFinansAML - Kan ikke finne filen " & My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml" & vbCrLf & "som benyttes til � se hvilke poster som skal valideres.")
                            Else
                                'XMLDoc.async = False
                                XMLSGInternalStatusDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml")
                            End If
                        End If
                        '
                        TransTypeDictionary.Clear()
                        nodeTransTypeList = XMLDoc.SelectNodes("BabelBank_AML/ValidCodes/Code")
                        For Each nodeTransType In nodeTransTypeList
                            node = nodeTransType.SelectSingleNode("Cd")
                            If Not node Is Nothing Then
                                sValidPayCode = node.InnerText
                            End If
                            node = nodeTransType.SelectSingleNode("Fmly/Cd")
                            If Not node Is Nothing Then
                                sValidPayCode = sValidPayCode & "-" & node.InnerText
                            End If
                            node = nodeTransType.SelectSingleNode("Fmly/SubFmlyCd")
                            If Not node Is Nothing Then
                                sValidPayCode = sValidPayCode & "-" & node.InnerText
                            End If
                            If Not TransTypeDictionary.ContainsKey(sValidPayCode) Then
                                TransTypeDictionary.Add(sValidPayCode, sValidPayCode)
                            End If
                        Next nodeTransType

                        '10.03.2020 - Added code rearding SGInternalStatus
                        'Load SGInternalStatus in to list
                        SGInternalStatusDictionary.Clear()
                        nodeTransTypeList = XMLSGInternalStatusDoc.SelectNodes("BabelBank_AML/StatusCodes/Code")
                        For Each nodeTransType In nodeTransTypeList
                            node = nodeTransType.SelectSingleNode("Cd")
                            If Not node Is Nothing Then
                                sSGInternalStatus = node.InnerText
                            End If
                            node = nodeTransType.SelectSingleNode("Action")
                            If Not node Is Nothing Then
                                sSGInternalStatusAction = node.InnerText
                            End If
                            If Not SGInternalStatusDictionary.ContainsKey(sSGInternalStatus) Then
                                SGInternalStatusDictionary.Add(sSGInternalStatus, sSGInternalStatusAction)
                            End If
                        Next nodeTransType

                        'If Not RunTime() Then
                        AcceptedTransactionDictionary.Clear()
                        nodeTransTypeList = XMLSGInternalStatusDoc.SelectNodes("BabelBank_AML/AcceptedInfo/Valid")
                        For Each nodeTransType In nodeTransTypeList
                            bAddItem = True
                            node = nodeTransType.SelectSingleNode("Type")
                            If Not node Is Nothing Then
                                If Not EmptyString(node.InnerText) Then
                                    sAcceptedType = node.InnerText
                                Else
                                    bAddItem = False
                                    sAcceptedType = ""
                                End If
                            Else
                                bAddItem = False
                                sAcceptedType = ""
                            End If
                            If bAddItem Then
                                sAcceptedText = ""
                                nodeTransTypeList = nodeTransType.SelectNodes("Values/Value")
                                iCounter = 0
                                For Each node In nodeTransTypeList
                                    iCounter = iCounter + 1
                                    If Not node Is Nothing Then
                                        If Not EmptyString(node.InnerText) Then
                                            If iCounter = 1 Then
                                                sAcceptedText = node.InnerText
                                            Else
                                                sAcceptedText = sAcceptedText & ";" & node.InnerText
                                            End If
                                        Else
                                            If EmptyString(sAcceptedText) Then
                                                bAddItem = False
                                            Else
                                                'OK, keep the sAcceptedText
                                            End If
                                        End If
                                    Else
                                        If EmptyString(sAcceptedText) Then
                                            bAddItem = False
                                        Else
                                            'OK, keep the sAcceptedText
                                        End If
                                    End If
                                Next node
                            End If
                            If bAddItem Then
                                If Not AcceptedTransactionDictionary.ContainsKey(sAcceptedText) Then
                                    AcceptedTransactionDictionary.Add(sAcceptedText, sAcceptedType)
                                End If
                            End If
                        Next nodeTransType

                        'End If

                        If 1 = 2 Then '1 = 1 To use csv-file, 1 = 2 to use xlsx-file 
                            If RunTime() Then 'DISKUTERNOTRUNTIME Felles mappe?
                                If Not oFs.FileExists(oBabelFiles.VB_Profile.Custom1Value) Then
                                    Throw New Exception("Treat_SGFinansAML - Kan ikke finne filen " & oBabelFiles.VB_Profile.Custom1Value & vbCrLf & "som benyttes til � sjekke ut gokjente debitorer.")
                                Else

                                    'oBabelLog.AddLogEvent("Funnet XML-filen!", 0)

                                    'oBabelLog.AddLogEvent("TXT-filen inneholder: " & sContent, 0)
                                    oFile = oFs.OpenTextFile(My.Application.Info.DirectoryPath & "\Mapping\Godkjente debitorer.xlsx", Scripting.IOMode.ForReading, True, 0)
                                    sContent = oFile.ReadLine
                                    XMLDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml")

                                End If
                            Else
                                If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\Godkjente debitorer.csv") Then
                                    Throw New Exception("Treat_SGFinansAML - Kan ikke finne filen " & My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml" & vbCrLf & "som benyttes til � sjekke ut gokjente debitorer.")
                                Else
                                    'XMLDoc.async = False
                                    XMLDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\Godkjente debitorer.csv")
                                End If
                            End If


                        Else
                            'Load the file with cleared payors
                            appExcel = New Microsoft.Office.Interop.Excel.Application
                            appExcel.DisplayAlerts = False

                            sFilename = ""
                            sFilename = System.Configuration.ConfigurationManager.AppSettings("SGF_PathGodkjenteDebitorer")
                            If EmptyString(sFilename) Then
                                Throw New Exception("Treat_SGFinansAML: Det er ikke angitt riktig n�kkel i BabelBank sin konfigurasjonsfil for fil for godkjente debitorer." & vbCrLf & vbCrLf & "N�kkel skal v�re: " & "SGF_PathGodkjenteDebitorer")
                            ElseIf Not oFs.FileExists(sFilename) Then
                                bWorkBookFound = False

                                'Throw New Exception("Treat_SGFinansAML: Kan ikke finne filen " & sFilename & vbCrLf & " som benyttes for godkjente debitorer.")

                            Else
                                bWorkBookFound = True
                                '12.07.2022 - Changed parameter ReadOnly from True to False
                                '  This solved an error after a patch upgrade of Excel at Nordea Finance
                                oExcelBook = appExcel.Workbooks.Open(getlongpath(sFilename), , False)
                            End If

                            If bWorkBookFound Then
                                'lRowNo = "KOKO"

                                oExcelSheet = oExcelBook.Worksheets(1)

                                ClearedCompayDictionary.Clear()
                                lRowNo = 4
                                iEmptyRows = 0
                                Do While lRowNo < 10000
                                    sTmp = oExcelSheet.Cells(lRowNo, 10).Value
                                    If EmptyString(sTmp) Then
                                        If iEmptyRows > 200 Then
                                            Exit Do
                                        Else
                                            iEmptyRows = iEmptyRows + 1
                                        End If
                                    Else
                                        sTmp = sTmp.ToUpper.Trim
                                        '20.05.2020 - Added next line
                                        sTmp = RemoveCharacters(sTmp, sCharactersToRemove)
                                        If Not ClearedCompayDictionary.ContainsKey(sTmp) Then
                                            ClearedCompayDictionary.Add(sTmp, sTmp)
                                        End If
                                    End If
                                    '28.11.2018 - Also checking column K (Debtor name (Alternativt))
                                    sTmp = oExcelSheet.Cells(lRowNo, 11).Value
                                    If EmptyString(sTmp) Then
                                    Else
                                        sTmp = sTmp.ToUpper.Trim
                                        '20.05.2020 - Added next line
                                        sTmp = RemoveCharacters(sTmp, sCharactersToRemove)
                                        If Not ClearedCompayDictionary.ContainsKey(sTmp) Then
                                            ClearedCompayDictionary.Add(sTmp, sTmp)
                                        End If
                                    End If

                                    lRowNo = lRowNo + 1
                                Loop
                            End If
                        End If

                        'FOR TESTING!!! - MAY BE USED TO TEST AGAINST GODKJENTE DEBITORER
                        'If Not RunTime() Then
                        '    For Each oBabelFile In oBabelFiles
                        '        For Each oBatch In oBabelFile.Batches
                        '            For Each oPayment In oBatch.Payments
                        '                If Not EmptyString(oPayment.E_Name) Then
                        '                    sTmp = RemoveCharacters(oPayment.E_Name, sCharactersToRemove)
                        '                    If ClearedCompayDictionary.ContainsKey(sTmp.ToUpper.Trim) Then
                        '                        If oPayment.ExtraD4 = "1" Then
                        '                            oPayment.ExtraD4 = "2"
                        '                        Else
                        '                            oPayment.ExtraD4 = "4"
                        '                        End If
                        '                    Else
                        '                        oPayment.ExtraD4 = "0"
                        '                    End If
                        '                End If
                        '            Next oPayment
                        '        Next oBatch
                        '    Next oBabelFile
                        'End If

                        'sTmp = "CF ZARYA LTD"
                        ''sTmp = "CF ZARYA Ltd"
                        'If ClearedCompayDictionary.ContainsKey(sTmp) Then
                        '    sTmp = sTmp.Trim
                        'Else
                        '    sTmp = sTmp.Trim
                        'End If

                        For Each oBabelFile In oBabelFiles
                            sSpecial = oBabelFile.Special
                            sFilename = oBabelFile.FilenameIn
                            For Each oBatch In oBabelFile.Batches

                                For Each oPayment In oBatch.Payments

                                    '02.11.2019 - Removed all code regarding valid transactionscodes stated in a XML-file
                                    'If Not EmptyString(oPayment.Visma_StatusCode) Then

                                    If oPayment.I_Account <> sOld_I_Account Then
                                        For Each oFilesetup In oBatch.VB_Profile.FileSetups
                                            For Each oClient In oFilesetup.Clients
                                                For Each oaccount In oClient.Accounts
                                                    If oaccount.Account = oPayment.I_Account Or oaccount.ConvertedAccount = oPayment.I_Account Then

                                                        sOld_I_Account = oPayment.I_Account
                                                        sAccountCurrencyCode = oaccount.CurrencyCode
                                                        sClientCountryCode = oaccount.DebitAccountCountryCode

                                                    End If
                                                Next oaccount
                                            Next oClient
                                        Next oFilesetup
                                    End If

                                    sCountryCodeToUse = ""

                                    If Not EmptyString(oPayment.Visma_StatusCode) Then
                                        If TransTypeDictionary.ContainsKey(oPayment.Visma_StatusCode) Then
                                            'Valid transactiontype
                                            sCountryCodeToUse = sClientCountryCode

                                        End If
                                    Else
                                        'MsgBox("oPayment.Visma_StatusCode er tom")
                                    End If

                                    If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20220713" Then
                                        sCountryCodeToUse = "NO"
                                    End If

                                    bRunTheSQL = False

                                    If EmptyString(oPayment.E_Name) Then
                                        '07.11.2019 - Removed code below
                                        'If oPayment.PayType <> "I" And oPayment.MON_InvoiceCurrency = "NOK" Then
                                        '    bRunTheSQL = False
                                        '    sMySQL = sMySetupSQL.Replace("BB_NAME", " ")
                                        'Else
                                        '    sMySQL = sMySetupSQL.Replace("BB_NAME", " ") ' Replace(sMySetupSQL, "BB_Name", " ", , , vbTextCompare)
                                        'End If
                                        sMySQL = sMySetupSQL.Replace("BB_NAME", " ")
                                    ElseIf oPayment.E_Name.Trim = "-" Then
                                        '06.01.2021 - Added this ElseIf after Teams-meeting, Name = "-" created an error 
                                        sMySQL = sMySetupSQL.Replace("BB_NAME", " ")
                                    ElseIf oPayment.E_Name.Trim = "." Then
                                        '06.01.2021 - Added this ElseIf after error in EQ DK, Name = "." created an error 
                                        sMySQL = sMySetupSQL.Replace("BB_NAME", " ")
                                    Else
                                        sMySQL = sMySetupSQL.Replace("BB_NAME", oPayment.E_Name.Replace("'", String.Empty).Trim) '.Replace(Chr(34), String.Empty).Trim)
                                        bRunTheSQL = True
                                    End If

                                    sTmp = ""
                                    If Not EmptyString(oPayment.E_Adr1) Then
                                        sTmp = oPayment.E_Adr1.Trim
                                    End If
                                    If Not EmptyString(oPayment.E_Adr2) Then
                                        sTmp = sTmp & " " & oPayment.E_Adr2.Trim
                                    End If
                                    If Not EmptyString(oPayment.E_Adr3) Then
                                        sTmp = sTmp & " " & oPayment.E_Adr3.Trim
                                    End If
                                    If EmptyString(sTmp) Then
                                        sTmp = " "
                                    Else
                                        sTmp = sTmp.Trim
                                        sTmp = sTmp.Replace("'", String.Empty)
                                        bRunTheSQL = True
                                    End If

                                    sMySQL = sMySQL.Replace("BB_ADDRESS", sTmp)

                                    sTmp = ""
                                    If EmptyString(oPayment.E_Name) Or oPayment.E_Name = "-" Or oPayment.E_Name = "." Then
                                        oPayment.ExtraD4 = "3"
                                        If EmptyString(oPayment.E_CountryCode) Then
                                            If Not EmptyString(sCountryCodeToUse) Then
                                                sTmp = sCountryCodeToUse
                                            End If
                                        End If
                                        bRunTheSQL = True
                                    ElseIf EmptyString(oPayment.E_CountryCode) Then
                                        'Added 07.11.2019
                                        'To add accountcountrycode to the transactions that
                                        ' have an approved transactiontype in the XML-file. 

                                        If EmptyString(sCountryCodeToUse) Then
                                            oPayment.ExtraD4 = "3"
                                            'MsgBox("sCountryCodeToUse er blank " & oPayment.E_Name)

                                        Else
                                            sTmp = sCountryCodeToUse
                                            'MsgBox("sCountryCodeToUse er IKKE blank. sTmp har verdien: " & sTmp)
                                            oPayment.ExtraD4 = "0"
                                        End If
                                        'If EmptyString(oPayment.Visma_StatusCode) Then
                                        '    oPayment.ExtraD4 = "3"
                                        'Else
                                        '    If oPayment.Visma_StatusCode.Length > 3 Then
                                        '        If oPayment.Visma_StatusCode.Substring(0, 4) = "DMCT" Then
                                        '            oPayment.ExtraD4 = "0"
                                        '        Else
                                        '            oPayment.ExtraD4 = "3"
                                        '        End If
                                        '    Else
                                        '        oPayment.ExtraD4 = "3"
                                        '    End If
                                        'End If
                                        bRunTheSQL = True
                                    Else
                                        If oPayment.E_CountryCode.Trim.Length = 2 Then
                                            sTmp = oPayment.E_CountryCode.Trim
                                            oPayment.ExtraD4 = "0" 'Added 15.11 If SQL returns nothing and the payor is cleared it would get ExtraD4 4
                                            bRunTheSQL = True
                                        Else
                                            sTmp = " "
                                            bRunTheSQL = True
                                            oPayment.ExtraD4 = "3"
                                        End If
                                    End If
                                    sMySQL = sMySQL.Replace("BB_COUNTRYCODE", sTmp)

                                    'If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20200130" Then
                                    '    If oPayment.E_Name = "PETTERI HIRVONEN" Then
                                    'MsgBox("SQL-en som skal kj�res: " & sMySQL)
                                    '    End If
                                    'End If

                                    ' run the sql

                                    sSGInternalStatus = ""
                                    If bRunTheSQL Then

                                        'If EmptyString(oPayment.E_CountryCode) Then
                                        '    MsgBox("Skal kj�re f�lgende SQL: " & sMySQL)
                                        'End If

                                        oMyERPDal.SQL = sMySQL
                                        bOK = True
                                        If oMyERPDal.Reader_Execute() Then
                                            If oMyERPDal.Reader_HasRows Then
                                                'MsgBox("Her fant vi noe muffens gitt.")

                                                'If EmptyString(oPayment.E_CountryCode) Then
                                                'MsgBox("Her er det noe muffens: " & sMySQL)
                                                'End If
                                                '25.09.2017 - " CountryRiskRatingLevel=" & Chr(34) & "MED-LOW" & Chr(34) & "/>

                                                'Definitions of ExtraD4:
                                                ' 0 = OK
                                                ' 1 = Positiv treff, ikke klarert
                                                ' 2 = Positiv treff, betaler klarert (ligger i Excel-fil)
                                                ' 3 = Ukjent land, ikke klarert
                                                ' 4 = Ukjent land, klarert betaler (ligger i Excel-fil)


                                                'oPayment.ExtraD4 = "1" '30.01.2020 - Moved further down
                                                iCounter = 0

                                                Do While oMyERPDal.Reader_ReadRecord
                                                    iCounter = iCounter + 1

                                                    sTmp = oMyERPDal.Reader_GetField(0)

                                                    If Not RunTime() And Format(Date.Today, "dd.MM.yyyy") = "08.08.2022" Then
                                                        'If Not RunTime() Then  'NOTRUNTIME
                                                        sTmp = sTestString
                                                    End If

                                                    '30.01.2020 - Added this IF because of a change in the precedure Compliance_Check
                                                    'If everything is OK it will now return an empty string.
                                                    If IsDBNull(sTmp) Then

                                                        'OK - No problem with this payment

                                                    ElseIf EmptyString(sTmp) Then

                                                        'OK - No problem with this payment

                                                    Else

                                                        '10.03.2020 - Added code rearding SGInternalStatus
                                                        bContinue = True
                                                        iPos = 0
                                                        iFinalSGInternalStatus = 0
                                                        sSGInternalStatus = ""
                                                        'iSGInternalStatus = 0
                                                        If Not EmptyString(sTmp) Then
                                                            '13.07.2022 - New code to retrieve the 'worst' SGInternalStatus
                                                            ' The returnstring may contain several matched
                                                            Do While True
                                                                iPos = iPos + 1
                                                                If sTmp.Substring(iPos).Contains("SGInternalStatus") Then
                                                                    iPos = iPos + sTmp.Substring(iPos).IndexOf("SGInternalStatus")
                                                                    sSGInternalStatus = sTmp.Substring(iPos + 18, 2)
                                                                    sSGInternalStatus = sSGInternalStatus.Replace(Chr(34), "")
                                                                    If SGInternalStatusDictionary.ContainsKey(sSGInternalStatus) Then
                                                                        sValue = SGInternalStatusDictionary(sSGInternalStatus)
                                                                    End If

                                                                    Select Case sValue.ToUpper

                                                                        Case "IGNORE"
                                                                            If iFinalSGInternalStatus = InternalStatusNOTSET Then
                                                                                iFinalSGInternalStatus = InternalStatusIGNORE
                                                                            End If

                                                                        Case "REPORT"
                                                                            If iFinalSGInternalStatus < InternalStatusSTOP Then
                                                                                iFinalSGInternalStatus = InternalStatusREPORT
                                                                            End If

                                                                        Case "STOP"
                                                                            iFinalSGInternalStatus = InternalStatusSTOP
                                                                            Exit Do

                                                                        Case Else
                                                                            iFinalSGInternalStatus = InternalStatusSTOP
                                                                            Exit Do

                                                                    End Select
                                                                Else
                                                                    Exit Do
                                                                End If
                                                            Loop

                                                            If iFinalSGInternalStatus = InternalStatusNOTSET Then
                                                                bContinue = False
                                                            ElseIf iFinalSGInternalStatus = InternalStatusIGNORE Then
                                                                bContinue = False
                                                            ElseIf iFinalSGInternalStatus = InternalStatusSTOP Then
                                                                bContinue = True
                                                            Else
                                                                bContinue = True
                                                                For Each kvp As System.Collections.Generic.KeyValuePair(Of String, String) In AcceptedTransactionDictionary

                                                                    sKey = kvp.Key 'PEP
                                                                    sValue = kvp.Value 'SubCategory_Name
                                                                    iPos = 0

                                                                    Do While True
                                                                        iPos = iPos + 1
                                                                        If sTmp.Substring(iPos).Contains(sValue) Then
                                                                            sValueInString = ""
                                                                            iPos = iPos + sTmp.Substring(iPos).IndexOf(sValue)
                                                                            sValueInString = sTmp.Substring(iPos + sValue.Length + 2, sTmp.Substring(iPos + sValue.Length + 2).IndexOf(Chr(34)))

                                                                            lCounter2 = 0

                                                                            Do While True

                                                                                lCounter2 = lCounter2 + 1
                                                                                sValidValue = xDelim(sKey, ";", lCounter2)

                                                                                If Not EmptyString(sValidValue) Then
                                                                                    If sValueInString = sValidValue Then
                                                                                        bContinue = False
                                                                                        Exit Do
                                                                                    Else
                                                                                        bContinue = True
                                                                                    End If
                                                                                Else
                                                                                    Exit Do
                                                                                End If
                                                                            Loop

                                                                            'If one of the returned 'lines/reasons' do not contain the correct info, then report the payment
                                                                            'May not be correct
                                                                            If bContinue Then
                                                                                Exit Do
                                                                            End If
                                                                        Else
                                                                            Exit Do
                                                                        End If
                                                                    Loop

                                                                Next
                                                            End If

                                                            'If RunTime() Then
                                                            '    If sTmp.Contains("SubCategory_Name=" & Chr(34) & "PEP" & Chr(34)) Then
                                                            '        'MsgBox("CHR(34)OKOKOK Innbetaling fra: " & oPayment.E_Name & " inneholder PEP." & vbCrLf & "stmp: " & sTmp)
                                                            '        bContinue = False
                                                            '        'sTmpLine = sTmpNew.Substring(0, iPos)
                                                            '        'sTmpNew = sTmpNew.Substring(iPos)
                                                            '    Else
                                                            '        'MsgBox("CHR(34)Innbetaling fra: " & oPayment.E_Name & " inneholder IKKE PEP." & vbCrLf & "stmp: " & sTmp)
                                                            '    End If
                                                            'End If

                                                        End If

                                                        If bContinue Then

                                                            bOK = False
                                                            '30.01.2020 - Moved here
                                                            oPayment.ExtraD4 = "1"

                                                            oPayment.ToSpecialReport = True
                                                            For Each oInvoice In oPayment.Invoices
                                                                oInvoice.ToSpecialReport = True
                                                            Next oInvoice

                                                            'If EmptyString(oPayment.E_CountryCode) Then
                                                            '    MsgBox("Feilmelding: " & sTmp)
                                                            'End If

                                                            If oPayment.Invoices.Count > 0 Then

                                                                If Not EmptyString(sTmp) Then
                                                                    If sTmp.Length > 2 Then
                                                                        oInvoice = oPayment.Invoices.Item(oPayment.Invoices.Count)
                                                                        sTmp = sTmp.Replace(Chr(34), "'")
                                                                        sTmp = sTmp.Replace("<SGEDW_Deliveries.dbo.ComplianceEntityList ", String.Empty)
                                                                        sTmp = sTmp.Replace("<ComplianceEntityList ", String.Empty)
                                                                        'sTmp = sTmp.Replace("<t ", String.Empty) 'Added 22.06.2022
                                                                        sTmp = sTmp.Replace("/>", String.Empty)

                                                                        Do While True
                                                                            If sTmp.Length > 3 Then
                                                                                iPos = 0
                                                                                iPos = sTmp.IndexOf("ListName", 3) 'Changed 22.06.2022, from 3 to 0
                                                                                sTmpNew = String.Empty
                                                                                If iPos > 0 Then 'Changed 22.06.2022, from 0 to -1
                                                                                    sTmpNew = sTmp.Substring(0, iPos)
                                                                                    sTmp = sTmp.Substring(iPos)
                                                                                Else
                                                                                    sTmpNew = sTmp
                                                                                    sTmp = String.Empty
                                                                                End If
                                                                                If Not EmptyString(sTmpNew) Then
                                                                                    If iCounter = 1 Then
                                                                                        iCounter = iCounter + 1 'Added 22.06.2022
                                                                                        oFreeText = oInvoice.Freetexts.Add
                                                                                        oFreeText.Text = "TREFF I COMPLIANCE CHECKLIST"
                                                                                    End If
                                                                                    iPos = sTmpNew.IndexOf("ListType", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        oFreeText = oInvoice.Freetexts.Add
                                                                                        oFreeText.Text = sTmpLine
                                                                                    End If

                                                                                    'Added 12.09.2017
                                                                                    iPos = sTmpNew.IndexOf("Match=", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        oFreeText = oInvoice.Freetexts.Add
                                                                                        oFreeText.Text = sTmpLine
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("SearchValue", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("CountryRiskRatingLevel", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("SGInternalStatus", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("Category_Name", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("SubCategory_Name", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("FurtherInformation_Text", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("ExternalSource_Text", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    If Not EmptyString(sTmpNew) Then
                                                                                        If sTmpNew <> "<t " Then

                                                                                            Do While Not EmptyString(sTmpNew)
                                                                                                If EmptyString(sTmpNew) Then
                                                                                                    Exit Do
                                                                                                ElseIf sTmpNew.Length > 47 Then
                                                                                                    oFreeText = oInvoice.Freetexts.Add
                                                                                                    oFreeText.Text = sTmpNew.Substring(0, 47)
                                                                                                    sTmpNew = sTmpNew.Substring(47)
                                                                                                Else
                                                                                                    oFreeText = oInvoice.Freetexts.Add
                                                                                                    oFreeText.Text = sTmpNew
                                                                                                    sTmpNew = ""
                                                                                                End If
                                                                                            Loop
                                                                                        End If
                                                                                    End If
                                                                                End If
                                                                            Else
                                                                                Exit Do
                                                                            End If
                                                                        Loop

                                                                    End If
                                                                End If

                                                                '' 10.08.2017
                                                                '' NOT TESTED
                                                                '' Add info about Cdtr to be shown in the freetext part of report;
                                                                'If Not EmptyString(oPayment.I_Name) Then
                                                                '    oFreeText = oInvoice.Freetexts.Add
                                                                '    oFreeText.Text = "Kreditor:"
                                                                '    oFreeText = oInvoice.Freetexts.Add
                                                                '    oFreeText.Text = oPayment.I_Name
                                                                '    If Not EmptyString(oPayment.I_Adr1) Then
                                                                '        oFreeText.Text = oPayment.I_Adr1
                                                                '    End If
                                                                '    If Not EmptyString(oPayment.I_Adr2) Then
                                                                '        oFreeText.Text = oPayment.I_Adr2
                                                                '    End If
                                                                '    If Not EmptyString(oPayment.I_Adr3) Then
                                                                '        oFreeText.Text = oPayment.I_Adr3
                                                                '    End If
                                                                '    If Not EmptyString(oPayment.I_Zip) Then
                                                                '        oFreeText.Text = oPayment.I_Zip
                                                                '    End If
                                                                '    If Not EmptyString(oPayment.I_City) Then
                                                                '        oFreeText.Text = oPayment.I_City
                                                                '    End If
                                                                'End If

                                                            End If 'If oPayment.Invoices.Count > 0 Then
                                                        End If 'If bContinue Then

                                                    End If
                                                    Exit Do
                                                Loop

                                            Else

                                                'OK - No problem with this payment

                                                '30.01.2020 - Old code before introducing the variable bOK
                                                ''Added - 25.08.2017
                                                'If EmptyString(oPayment.E_Name) Then
                                                '    oPayment.ExtraD4 = "3"
                                                'ElseIf EmptyString(oPayment.E_CountryCode) Then
                                                '    If EmptyString(sCountryCodeToUse) Then
                                                '        oPayment.ExtraD4 = "3"
                                                '    Else
                                                '        oPayment.ExtraD4 = "0"
                                                '    End If
                                                'Else
                                                '    If oPayment.E_CountryCode.Trim.Length = 2 Then
                                                '        'OK
                                                '    Else
                                                '        oPayment.ExtraD4 = "3"
                                                '    End If
                                                'End If

                                                ' ''04.11.2019 - Changed code below
                                                ''If oPayment.ExtraD4 = "3" Then
                                                ''    If EmptyString(oPayment.Visma_StatusCode) Then
                                                ''        oPayment.ExtraD4 = "3"
                                                ''    Else
                                                ''        If oPayment.Visma_StatusCode.Length > 3 Then
                                                ''            If oPayment.Visma_StatusCode.Substring(0, 4) = "DMCT" Then
                                                ''                oPayment.ExtraD4 = "0"
                                                ''            Else
                                                ''                oPayment.ExtraD4 = "3"
                                                ''            End If
                                                ''        Else
                                                ''            oPayment.ExtraD4 = "3"
                                                ''        End If
                                                ''    End If
                                                ''End If

                                            End If
                                        Else
                                            Throw New Exception(oMyERPDal.ErrorNumber.ToString & ": " & oMyERPDal.ErrorMessage)
                                        End If

                                        '30.01.2020 - Added this IF
                                        If bOK Then

                                            'Added - 25.08.2017
                                            If EmptyString(oPayment.E_Name) Or oPayment.E_Name = "-" Or oPayment.E_Name = "." Then
                                                '26.01.2021 - Added - and . see comment above
                                                oPayment.ExtraD4 = "3"
                                            ElseIf EmptyString(oPayment.E_CountryCode) Then
                                                If EmptyString(sCountryCodeToUse) Then
                                                    oPayment.ExtraD4 = "3"
                                                Else
                                                    oPayment.ExtraD4 = "0"
                                                End If
                                            Else
                                                If oPayment.E_CountryCode.Trim.Length = 2 Then
                                                    'OK
                                                Else
                                                    oPayment.ExtraD4 = "3"

                                                End If
                                            End If

                                            ''04.11.2019 - Changed code below
                                            'If oPayment.ExtraD4 = "3" Then
                                            '    If EmptyString(oPayment.Visma_StatusCode) Then
                                            '        oPayment.ExtraD4 = "3"
                                            '    Else
                                            '        If oPayment.Visma_StatusCode.Length > 3 Then
                                            '            If oPayment.Visma_StatusCode.Substring(0, 4) = "DMCT" Then
                                            '                oPayment.ExtraD4 = "0"
                                            '            Else
                                            '                oPayment.ExtraD4 = "3"
                                            '            End If
                                            '        Else
                                            '            oPayment.ExtraD4 = "3"
                                            '        End If
                                            '    End If
                                            'End If

                                        End If

                                    End If
                                    If Not EmptyString(oPayment.E_Name) Then

                                        If Not oPayment.E_Name = "-" Then

                                            If Not oPayment.E_Name = "." Then

                                                If oPayment.ExtraD4 = "0" Then
                                                    'These payments are OK, do not check anymore
                                                Else
                                                    '20.05.2020 - Added next line
                                                    sTmp = RemoveCharacters(oPayment.E_Name, sCharactersToRemove)

                                                    If ClearedCompayDictionary.ContainsKey(sTmp.ToUpper.Trim) Then

                                                        If oPayment.ExtraD4 = "1" Then
                                                            oPayment.ExtraD4 = "2"
                                                        Else
                                                            oPayment.ExtraD4 = "4"
                                                        End If

                                                    End If
                                                End If

                                            End If
                                        End If

                                    End If

                                    '10.03.2020 - Added code rearding SGInternalStatus
                                    If oPayment.ExtraD4 = "1" Or oPayment.ExtraD4 = "3" Then

                                        'MsgBox("Har funnet en som skal rapporteres!")

                                        If SGInternalStatusDictionary.ContainsKey(sSGInternalStatus) Then
                                            If SGInternalStatusDictionary(sSGInternalStatus) = "IGNORE" Then
                                                'Everything is OK, no problem with the payment

                                            ElseIf SGInternalStatusDictionary(sSGInternalStatus) = "REPORT" Then
                                                oPayment.ExtraD4 = "2"

                                            ElseIf SGInternalStatusDictionary(sSGInternalStatus) = "STOP" Then
                                                'No change. remove it by posting it to correct account

                                            End If
                                        End If
                                    End If

                                    '28.05.2020 - Added next section after mail from Jan-Kjetil
                                    'Do not stop zero-payments
                                    If IsEqualAmount(oPayment.MON_InvoiceAmount, 0) Then
                                        If oPayment.ExtraD4 = "1" Or oPayment.ExtraD4 = "3" Or oPayment.ExtraD4 = "4" Then
                                            oPayment.ExtraD4 = "2"
                                        End If
                                    End If

                                    ''Else
                                    ''    oPayment.ExtraD4 = "0"
                                    'End If 'If TransTypeDictionary.ContainsKey(oPayment.Visma_StatusCode) Then
                                    'End If 'If Not EmptyString(oPayment.Visma_StatusCode) Then

                                    '04.11.2019 - Added new code to post payments that have a positive match and aren't cleared
                                    If oPayment.ExtraD4 = "1" Or oPayment.ExtraD4 = "3" Then

                                        'MsgBox("Fortsatt feil p� denne!")

                                        oPayment.PurposeCode = "AML"

                                        bAccountFound = False
                                        For iAccountCounter = 0 To aAMLAccounts.GetUpperBound(1)
                                            If aAMLAccounts(1, iAccountCounter) = oPayment.MON_InvoiceCurrency Then
                                                bAccountFound = True
                                                Exit For
                                            End If
                                        Next iAccountCounter

                                        If Not bAccountFound Then
                                            Throw New Exception("Det er ikke angitt noen AML-posteringskonto for valutaen: " & oPayment.MON_InvoiceCurrency)
                                        ElseIf EmptyString(aAMLAccounts(0, iAccountCounter)) Then
                                            Throw New Exception("Det er ikke angitt noen posteringskonto p� AML-kontoen for valutaen: " & oPayment.MON_InvoiceCurrency)
                                        Else
                                            '05.06.2020 - Added next IF
                                            If sSpecial = "SG FINANS" Then
                                                If oPayment.MON_OriginallyPaidCurrency <> oPayment.MON_InvoiceCurrency Then
                                                    oPayment.MON_AccountExchRate = 0
                                                    oPayment.MON_LocalExchRate = 0
                                                    If oPayment.Invoices.Count > 0 Then
                                                        oFreeText = oPayment.Invoices.Item(1).Freetexts.Add()
                                                        oFreeText.Text = "Oppr. bel�p: " & oPayment.MON_OriginallyPaidCurrency & " " & ConvertFromAmountToString(oPayment.MON_OriginallyPaidAmount, ".", ",")
                                                        oFreeText.Qualifier = 4
                                                    End If
                                                    oPayment.MON_OriginallyPaidCurrency = oPayment.MON_InvoiceCurrency
                                                    oPayment.MON_OriginallyPaidAmount = oPayment.MON_InvoiceAmount
                                                End If
                                            End If

                                            If oPayment.Invoices.Count = 1 Then

                                                'MsgBox("Invoices.Count = 1")


                                                For Each oInvoice In oPayment.Invoices
                                                    If sSpecial = "SGF_EQ" Then
                                                        'oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL '2
                                                        '18.05.2020 - Changed to MatchedOnCustomer
                                                        oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer
                                                        oInvoice.MATCH_ID = aAMLAccounts(0, iAccountCounter)
                                                        '18.05.2020 - Added next 3 lines
                                                        oInvoice.CustomerNo = aAMLAccounts(0, iAccountCounter)
                                                        oInvoice.MyField = "1"
                                                        oInvoice.MyField2 = "1"
                                                    Else
                                                        oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer '2
                                                        If sSpecial = "NORDEAFINANCE_APTIC" Then
                                                            oInvoice.MATCH_ID = aAMLAccounts(0, iAccountCounter)
                                                        Else
                                                            oInvoice.MATCH_ID = "KJELL"
                                                        End If
                                                        oInvoice.MATCH_ClientNo = xDelim(aAMLAccounts(0, iAccountCounter), "/", 1)
                                                        oInvoice.CustomerNo = xDelim(aAMLAccounts(0, iAccountCounter), "/", 3)
                                                        oInvoice.MyField = xDelim(aAMLAccounts(0, iAccountCounter), "/", 2) & "-" 'AgreementNumber
                                                        '05.06.2020 - Added next 2 lines
                                                        oPayment.MON_AccountExchRate = 0
                                                        oPayment.MON_LocalExchRate = 0
                                                    End If
                                                    oInvoice.MATCH_Matched = True
                                                Next oInvoice
                                            Else
                                                For Each oInvoice In oPayment.Invoices
                                                    oInvoice.MATCH_Final = False
                                                Next oInvoice

                                                oPayment.Invoices.Add()
                                                oInvoice.MATCH_Original = False
                                                oInvoice.MATCH_Final = True
                                                oInvoice.MON_InvoiceAmount = oPayment.MON_InvoiceAmount
                                                oInvoice.MON_TransferredAmount = oPayment.MON_TransferredAmount
                                                oInvoice.StatusCode = oPayment.StatusCode
                                                oInvoice.VB_Profile = oPayment.VB_Profile
                                                If sSpecial = "SGF_EQ" Then
                                                    'oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL '2
                                                    '18.05.2020 - Changed to MatchedOnCustomer
                                                    oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer
                                                    oInvoice.MATCH_ID = aAMLAccounts(0, iAccountCounter)
                                                    '18.05.2020 - Added next 3 lines
                                                    oInvoice.CustomerNo = aAMLAccounts(0, iAccountCounter)
                                                    oInvoice.MATCH_Matched = True
                                                    oInvoice.MyField = "1"
                                                    oInvoice.MyField2 = "1"
                                                Else
                                                    oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer '2
                                                    oInvoice.MATCH_Matched = True
                                                    oInvoice.MATCH_ClientNo = xDelim(aAMLAccounts(0, iAccountCounter), "/", 1)
                                                    oInvoice.CustomerNo = xDelim(aAMLAccounts(0, iAccountCounter), "/", 3)
                                                    oInvoice.MyField = xDelim(aAMLAccounts(0, iAccountCounter), "/", 2) & "-" 'AgreementNumber
                                                    If sSpecial = "NORDEAFINANCE_APTIC" Then
                                                        oInvoice.MATCH_ID = aAMLAccounts(0, iAccountCounter)
                                                    Else
                                                        oInvoice.MATCH_ID = "KJELL"
                                                    End If
                                                End If

                                            End If

                                            oPayment.MATCH_HowMatched = "Fjernet AML-sjekk"
                                            oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched
                                        End If
                                    End If

                                Next oPayment
                            Next oBatch
                        Next oBabelFile
                        bReturnValue = True
                    Else
                        bReturnValue = False
                    End If


                    '28.04.2020 - Added to create charges report
                    oBabelReport = New vbBabel.BabelReport
                    oBabelReport.LoggingActivated = bLOG
                    oBabelReport.BabelLog = oBabelLog
                    oBabelReport.Special = sSpecial
                    oBabelReport.BabelFiles = oBabelFiles
                    oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.After_Autogiro_Export
                    'oBabelReport.OwnerForm = oOwnerForm
                    oBabelReport.BBIsInSilentMode()

                    If Not oBabelReport.RunReports(oFilesetup.FileSetup_ID) Then

                    End If

                    oBabelReport = Nothing

                End If

            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally
            'If Not oExcelSheet Is Nothing Then
            '    'releaseObject(oExcelSheet)
            '    oExcelSheet = Nothing
            'End If
            'If Not oExcelBook Is Nothing Then
            '    oExcelBook.Close()
            '    'releaseObject(oExcelBook)
            '    oExcelBook = Nothing
            'End If
            'If Not appExcel Is Nothing Then
            '    appExcel.Quit()
            '    'releaseObject(appExcel)
            '    appExcel = Nothing

            '    GC.Collect()
            '    GC.WaitForPendingFinalizers()
            '    GC.Collect()
            '    GC.WaitForPendingFinalizers()
            'End If

        End Try

        Return bReturnValue


    End Function
    Public Function DELETE_WHENPRODOK_Treat_SGFinansAML(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal bLOG As Boolean, ByVal oBabelLog As vbLog.vbLogging) As Boolean
        Dim oMyERPDal As vbBabel.DAL = Nothing
        Dim sMySetupSQL As String
        Dim sMySQL As String
        Dim lCompanyNo As Long
        Dim sProvider As String
        Dim sUID As String
        Dim sPWD As String
        Dim aQueryArray(,,) As String
        Dim lCounter As Long
        Dim sTmp As String '= "<SGEDW_Deliveries.dbo.ComplianceEntityList ListName='DefenceExlusionList' ListType='Company' SearchValue=' '/><SGEDW_Deliveries.dbo.ComplianceEntityList ListName='E S Watchlist Company' ListType=Company' SearchValue=' '/><SGEDW_Deliveries.dbo.ComplianceEntityList ListName='StopList Company' ListType='Company' SearchValue=' '/>"
        Dim sTmpNew As String = ""
        Dim sTmpLine As String = ""
        Dim iPos As Integer
        Dim bReturnValue As Boolean = False
        Dim sFilename As String = ""
        Dim bRunTheSQL As Boolean = True
        Dim iCounter As Integer
        Dim oFile As Scripting.TextStream
        Dim sContent As String
        Dim bContinue As Boolean
        Dim bWorkBookFound As Boolean = True

        Dim oFs As New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
        Dim XMLDoc As New System.Xml.XmlDocument
        Dim XMLSGInternalStatusDoc As New System.Xml.XmlDocument
        Dim nodeTransTypeList As System.Xml.XmlNodeList
        Dim nodeTransType As System.Xml.XmlNode
        Dim node As System.Xml.XmlNode

        Dim oExcelBook As Microsoft.Office.Interop.Excel.Workbook
        Dim oExcelSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim appExcel As Microsoft.Office.Interop.Excel.Application
        Dim lRowNo As Long
        Dim iEmptyRows As Integer
        Dim sCharactersToRemove As String = ",.-/ "

        Dim TransTypeDictionary As New System.Collections.Generic.Dictionary(Of String, String)
        Dim SGInternalStatusDictionary As New System.Collections.Generic.Dictionary(Of String, String) '10.03.2020 - Added code rearding SGInternalStatus
        Dim ClearedCompayDictionary As New System.Collections.Generic.Dictionary(Of String, String)
        Dim AcceptedTransactionDictionary As New System.Collections.Generic.Dictionary(Of String, String)
        'Dim pair As System.Collections.Generic.KeyValuePair(Of String, String)
        Dim sValidPayCode As String = ""
        Dim sSGInternalStatus As String
        Dim sSGInternalStatusAction As String
        Dim bAddItem As Boolean
        Dim sAcceptedType As String
        Dim sAcceptedText As String
        '04.11.2019 - Added next 7
        Dim sOld_I_Account As String = "XXX"
        Dim sAccountCurrencyCode As String = ""
        Dim oMyBBDal As vbBabel.DAL = Nothing
        Dim aAMLAccounts As String(,)
        Dim iAccountCounter As Integer
        Dim bAccountFound As Boolean = False
        Dim aArchiveArray As String()
        Dim sCountryCodeToUse As String
        Dim sClientCountryCode As String
        Dim sSpecial As String = ""
        Dim bOK As Boolean

        'Dim sTestString As String = "<t ListName=" & Chr(34) & "Country Risk Rating" & Chr(34) & " Match=" & Chr(34) & "Country" & Chr(34) & " SearchValue=" & Chr(34) & "RU" & Chr(34) & " SGInternalStatus=" & Chr(34) & "40" & Chr(34) & " CountryRiskRatingLevel=" & Chr(34) & "E&amp;S" & Chr(34) & "/><t ListName=" & Chr(34) & "StopList Company" & Chr(34) & " Match=" & Chr(34) & "Company" & Chr(34) & " SearchValue=" & Chr(34) & "Kjell" & Chr(34) & " SGInternalStatus=" & Chr(34) & "0" & Chr(34) & "/><t ListName=" & Chr(34) & "WorldCheck" & Chr(34) & " Match=" & Chr(34) & "BONDEVIK Kjell Magne Johannesson" & Chr(34) & " SearchValue=" & Chr(34) & "Kjell" & Chr(34) & "/><t ListName=" & Chr(34) & "WorldCheck" & Chr(34) & " Match=" & Chr(34) & "LAUGERUD GARCIA Kjell Eugenio" & Chr(34) & " SearchValue=" & Chr(34) & "Kjell" & Chr(34) & "/><t ListName=" & Chr(34) & "WorldCheck" & Chr(34) & " Match=" & Chr(34) & "KJELL PEDERSEN ENTREPRISE A/S " & Chr(34) & " SearchValue=" & Chr(34) & "Kjell" & Chr(34) & "/>"
        Dim sTestString As String

        Dim oBabelReport As vbBabel.BabelReport

        sTestString = "<t ListName=" & Chr(34) & "WorldCheck" & Chr(34) & " Match=" & Chr(34) & "PUTIN Vladimir Vladimirovich" & Chr(34) & " SearchValue=" & Chr(34) & "Vladimir Putin" & Chr(34) & " SGInternalStatus=" & Chr(34) & "20" & Chr(34) & " Category_Name=" & Chr(34) & "POLITICAL INDIVIDUAL" & Chr(34) & " SubCategory_Name=" & Chr(34) & "PEP" & Chr(34) & " FurtherInformation_Text=" & Chr(34) & "[AUSTRALIA SANCTIONS - DFAT-AS] No 6678 (Feb 2022 - addition). PRIMARY NAME: Vladimir Vladimirovich PUTIN. Date of Birth: 1952-10-07. Place of Birth: Saint Petersburg, Russia. Additional Information: President of the Russian Federation. [CANADA SANCTIONS " & Chr(34) & " ExternalSource_Text=" & Chr(34) & "http://er.ru/rubr.shtml?110085 http://fra.gov.ky/contents/page/1 http://gazeta.ua/ru/articles/politics/_zena-medvedchuka-rasskazala-pochemu-putin-stal-ih-kumom-foto/191192 http://grani.ru/Economy/m.103096.html http://kremlin.ru/news/15224 http://lenta.ru/" & Chr(34) & "/><t ListName=" & Chr(34) & "WorldCheck" & Chr(34) & " Match=" & Chr(34) & "PUTIN Vladimir Vladimirovich" & Chr(34) & " SearchValue=" & Chr(34) & "Vladimir Putin" & Chr(34) & " SGInternalStatus=" & Chr(34) & "20" & Chr(34) & " Category_Name=" & Chr(34) & "POLITICAL INDIVIDUAL" & Chr(34) & " SubCategory_Name=" & Chr(34) & "PEP" & Chr(34) & " FurtherInformation_Text=" & Chr(34) & "[AUSTRALIA SANCTIONS - DFAT-AS] No 6678 (Feb 2022 - addition). PRIMARY NAME: Vladimir Vladimirovich PUTIN. Date of Birth: 1952-10-07. Place of Birth: Saint Petersburg, Russia. Additional Information: President of the Russian Federation. [CANADA SANCTIONS " & Chr(34) & " ExternalSource_Text=" & Chr(34) & "http://er.ru/rubr.shtml?110085 http://fra.gov.ky/contents/page/1 http://gazeta.ua/ru/articles/politics/_zena-medvedchuka-rasskazala-pochemu-putin-stal-ih-kumom-foto/191192 http://grani.ru/Economy/m.103096.html http://kremlin.ru/news/15224 http://lenta.ru/" & Chr(34) & "/><t ListName=" & Chr(34) & "WorldCheck" & Chr(34) & " Match=" & Chr(34) & "Putin Vladimir Vladimirovich" & Chr(34) & " SearchValue=" & Chr(34) & "Vladimir Putin" & Chr(34) & " SGInternalStatus=" & Chr(34) & "20" & Chr(34) & " Category_Name=" & Chr(34) & "POLITICAL INDIVIDUAL" & Chr(34) & " SubCategory_Name=" & Chr(34) & "PEP" & Chr(34) & " FurtherInformation_Text=" & Chr(34) & "[AUSTRALIA SANCTIONS - DFAT-AS] No 6678 (Feb 2022 - addition). PRIMARY NAME: Vladimir Vladimirovich PUTIN. Date of Birth: 1952-10-07. Place of Birth: Saint Petersburg, Russia. Additional Information: President of the Russian Federation. [CANADA SANCTIONS " & Chr(34) & " ExternalSource_Text=" & Chr(34) & "http://er.ru/rubr.shtml?110085 http://fra.gov.ky/contents/page/1 http://gazeta.ua/ru/articles/politics/_zena-medvedchuka-rasskazala-pochemu-putin-stal-ih-kumom-foto/191192 http://grani.ru/Economy/m.103096.html http://kremlin.ru/news/15224 http://lenta.ru/" & Chr(34) & "/>"
        '04.06.2020 - Removed some logging

        Try

            '10.02.2021 - Added this code
            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If IsOCR(oPayment.PayCode) Then
                            For Each oInvoice In oPayment.Invoices
                                If Not EmptyString(oInvoice.Unique_Id) Then
                                    oPayment.ExtraD2 = oInvoice.Unique_Id.Trim
                                    Exit For
                                End If
                            Next oInvoice
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

            If Not oBabelFiles Is Nothing Then

                If oBabelFiles.VB_Profile.DisableAdminMenus = True Then 'Added 10.12.2020 - Added this IF to diable AML-check

                    bReturnValue = True

                Else

                    'oBabelLog.AddLogEvent("Kommet til spesialrutine.", 0)


                    'FOR TESTING PURPOSES:
                    'sTmp = "<SGEDW_Deliveries.dbo.ComplianceEntityList ListName=" & Chr(34) & "Country Risk Rating" & Chr(34) & " ListType=" & Chr(34) & "Country" & Chr(34) & " SearchValue=" & Chr(34) & "PL" & Chr(34) & " CountryRiskRatingLevel=" & Chr(34) & "MED-LOW" & Chr(34) & "/><SGEDW_Deliveries.dbo.ComplianceEntityList ListName=" & Chr(34) & "E S Watchlist Project" & Chr(34) & " ListType=" & Chr(34) & "Country" & Chr(34) & " SearchValue=" & Chr(34) & "PL" & Chr(34) & "/>"
                    'sTmp = "<ComplianceEntityList ListName=" & Chr(34) & "Country Risk Rating" & Chr(34) & " Match=" & Chr(34) & "Country" & Chr(34) & " SearchValue=" & Chr(34) & "PL" & Chr(34) & " CountryRiskRatingLevel=" & Chr(34) & "MED-LOW" & Chr(34) & "/><SGEDW_Deliveries.dbo.ComplianceEntityList ListName=" & Chr(34) & "E S Watchlist Project" & Chr(34) & " Match=" & Chr(34) & "Country" & Chr(34) & " SearchValue=" & Chr(34) & "PL" & Chr(34) & "/>"

                    'If Not EmptyString(sTmp) Then
                    '    If sTmp.Length > 2 Then
                    '        oInvoice = oPayment.Invoices.Item(oPayment.Invoices.Count)
                    '        sTmp = sTmp.Replace(Chr(34), "'")
                    '        sTmp = sTmp.Replace("<SGEDW_Deliveries.dbo.ComplianceEntityList ", String.Empty)
                    '        sTmp = sTmp.Replace("/>", String.Empty)

                    '        Do While True
                    '            If sTmp.Length > 3 Then
                    '                iPos = 0
                    '                iPos = sTmp.IndexOf("ListName", 3)
                    '                sTmpNew = String.Empty
                    '                If iPos > 0 Then
                    '                    sTmpNew = sTmp.Substring(0, iPos)
                    '                    sTmp = sTmp.Substring(iPos)
                    '                Else
                    '                    sTmpNew = sTmp
                    '                    sTmp = String.Empty
                    '                End If
                    '                If Not EmptyString(sTmpNew) Then
                    '                    oFreeText = oInvoice.Freetexts.Add
                    '                    oFreeText.Text = "TREFF I COMPLIANCE CHECKLIST"
                    '                    iPos = sTmpNew.IndexOf("ListType", 3)
                    '                    sTmpLine = String.Empty
                    '                    If iPos > 0 Then
                    '                        sTmpLine = sTmpNew.Substring(0, iPos)
                    '                        sTmpNew = sTmpNew.Substring(iPos)
                    '                        oFreeText = oInvoice.Freetexts.Add
                    '                        oFreeText.Text = sTmpLine
                    '                    End If

                    '                    iPos = sTmpNew.IndexOf("SearchValue", 3)
                    '                    sTmpLine = String.Empty
                    '                    If iPos > 0 Then
                    '                        sTmpLine = sTmpNew.Substring(0, iPos)
                    '                        sTmpNew = sTmpNew.Substring(iPos)
                    '                        oFreeText = oInvoice.Freetexts.Add
                    '                        oFreeText.Text = sTmpLine
                    '                    End If

                    '                    iPos = sTmpNew.IndexOf("CountryRiskRatingLevel", 3)
                    '                    sTmpLine = String.Empty
                    '                    If iPos > 0 Then
                    '                        sTmpLine = sTmpNew.Substring(0, iPos)
                    '                        sTmpNew = sTmpNew.Substring(iPos)
                    '                        oFreeText = oInvoice.Freetexts.Add
                    '                        oFreeText.Text = sTmpLine
                    '                    End If

                    '                    oFreeText = oInvoice.Freetexts.Add
                    '                    oFreeText.Text = sTmpNew
                    '                End If
                    '            Else
                    '                Exit Do
                    '            End If
                    '        Loop

                    '    End If
                    'End If



                    sProvider = ""
                    sUID = ""
                    sPWD = ""
                    lCompanyNo = 1

                    aQueryArray = GetERPDBInfo(1, 5)

                    For lCounter = 0 To UBound(aQueryArray, 3)
                        Select Case aQueryArray(0, 2, lCounter).ToUpper

                            Case "AML_PROCEDURE"
                                sMySetupSQL = aQueryArray(0, 0, lCounter).Trim.ToUpper

                        End Select
                    Next lCounter

                    If Not EmptyString(sMySetupSQL) And oBabelFiles.Count > 0 Then

                        '04.11.2019 - Added next lines to retrieve accounts to post postive not cleared payments
                        oMyBBDal = New vbBabel.DAL
                        oMyBBDal.TypeOfDB = vbBabel.DAL.DatabaseType.BBDB
                        If Not oMyBBDal.ConnectToDB() Then
                            Throw New System.Exception(oMyBBDal.ErrorMessage)
                        End If

                        '18.05.2020 - Changed to LIKE
                        sMySQL = "SELECT Pattern, CurrencyCode FROM MATCH_DiffAccounts Where Company_ID = " & oBabelFiles.VB_Profile.Company_ID.ToString & " AND Name like '%AML IKKE KLARERT%'"
                        iAccountCounter = -1
                        oMyBBDal.SQL = sMySQL
                        If oMyBBDal.Reader_Execute() Then
                            If oMyBBDal.Reader_HasRows Then
                                Do While oMyBBDal.Reader_ReadRecord
                                    iAccountCounter = iAccountCounter + 1
                                    ReDim Preserve aAMLAccounts(1, iAccountCounter)
                                    aAMLAccounts(0, iAccountCounter) = oMyBBDal.Reader_GetString("Pattern")
                                    aAMLAccounts(1, iAccountCounter) = oMyBBDal.Reader_GetString("CurrencyCode")
                                Loop
                            Else
                                'lLastBabelfileID = 0
                            End If

                        Else
                            Throw New Exception(LRSCommon(45002) & oMyBBDal.ErrorMessage)
                        End If

                        '05.11.2019 - Changed to retrieve logoninfo from Archive
                        aArchiveArray = GetArchiveInfo(1)

                        'Connect to Archive-database
                        oMyERPDal = New vbBabel.DAL
                        oMyERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ArchiveDB
                        oMyERPDal.Company_ID = 1 'TODO: Hardcoded Company_ID
                        If Not oMyERPDal.ConnectToDB() Then
                            Throw New System.Exception(oMyERPDal.ErrorMessage)
                        End If

                        '' connect to erp system
                        ''Build an array consisting of the ConnectionStrings and Query agianst the ERP-system.
                        'aQueryArray = GetERPDBInfo(lCompanyNo, 1)
                        'sProvider = aQueryArray(0, 0, 0)  ' Connectionstring
                        'sUID = aQueryArray(0, 1, 0)
                        'sPWD = aQueryArray(0, 2, 0)

                        '' Fill in UserID and Password
                        'sProvider = Replace(UCase(sProvider), "=UID", "=" & sUID)
                        'sProvider = Replace(UCase(sProvider), "=PWD", "=" & sPWD)

                        'oMyERPDal = New vbBabel.DAL
                        ''oMyERPDal.Connectionstring = sProvider
                        ''oMyERPDal.Provider = vbBabel.DAL.ProviderType.Odbc
                        'oMyERPDal.TypeOfDB = vbBabel.DAL.DatabaseType.ERPDB
                        'oMyERPDal.Company_ID = lCompanyNo
                        'oMyERPDal.DBProfile_ID = 1

                        'If Not oMyERPDal.ConnectToDB() Then
                        '    Throw New System.Exception(oMyERPDal.ErrorMessage)
                        'End If

                        '05.11.2019 - The use of the XML-file has changed
                        'Before: The list contained transactiontypes which to control
                        'After: The list contains transactions which not to control
                        'Load the file for valid transaction codes
                        'oFs = New Scripting.FileSystemObject  '23.05.2017 CreateObject ("Scripting.Filesystemobject")
                        If RunTime() Then   'DISKUTERNOTRUNTIME Felles mappe (er felles allerede?)
                            If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml") Then
                                Throw New Exception("Treat_SGFinansAML - Kan ikke finne filen " & My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml, " & vbCrLf & "som benyttes til � se hvilke poster som skal valideres.")
                            Else

                                'oBabelLog.AddLogEvent("Funnet XML-filen: " & My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml", 0)

                                XMLDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml")

                            End If
                            '10.03.2020 - Added code rearding SGInternalStatus
                            If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml") Then
                                Throw New Exception("Treat_SGFinansAML - Kan ikke finne filen " & My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml, " & vbCrLf & "som benyttes til � se hvilke poster som skal valideres.")
                            Else

                                'oBabelLog.AddLogEvent("Funnet XML-filen: " & My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml", 0)

                                XMLSGInternalStatusDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml")

                            End If
                        Else
                            If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml") Then
                                Throw New Exception("Treat_SGFinansAML - Kan ikke finne filen " & My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml" & vbCrLf & "som benyttes til � se hvilke poster som skal valideres.")
                            Else
                                'XMLDoc.async = False
                                XMLDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml")
                            End If
                            If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml") Then
                                Throw New Exception("Treat_SGFinansAML - Kan ikke finne filen " & My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml" & vbCrLf & "som benyttes til � se hvilke poster som skal valideres.")
                            Else
                                'XMLDoc.async = False
                                XMLSGInternalStatusDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\SGInternalStatus.xml")
                            End If
                        End If
                        '
                        TransTypeDictionary.Clear()
                        nodeTransTypeList = XMLDoc.SelectNodes("BabelBank_AML/ValidCodes/Code")
                        For Each nodeTransType In nodeTransTypeList
                            node = nodeTransType.SelectSingleNode("Cd")
                            If Not node Is Nothing Then
                                sValidPayCode = node.InnerText
                            End If
                            node = nodeTransType.SelectSingleNode("Fmly/Cd")
                            If Not node Is Nothing Then
                                sValidPayCode = sValidPayCode & "-" & node.InnerText
                            End If
                            node = nodeTransType.SelectSingleNode("Fmly/SubFmlyCd")
                            If Not node Is Nothing Then
                                sValidPayCode = sValidPayCode & "-" & node.InnerText
                            End If
                            If Not TransTypeDictionary.ContainsKey(sValidPayCode) Then
                                TransTypeDictionary.Add(sValidPayCode, sValidPayCode)
                            End If
                        Next nodeTransType

                        '10.03.2020 - Added code rearding SGInternalStatus
                        'Load SGInternalStatus in to list
                        SGInternalStatusDictionary.Clear()
                        nodeTransTypeList = XMLSGInternalStatusDoc.SelectNodes("BabelBank_AML/StatusCodes/Code")
                        For Each nodeTransType In nodeTransTypeList
                            node = nodeTransType.SelectSingleNode("Cd")
                            If Not node Is Nothing Then
                                sSGInternalStatus = node.InnerText
                            End If
                            node = nodeTransType.SelectSingleNode("Action")
                            If Not node Is Nothing Then
                                sSGInternalStatusAction = node.InnerText
                            End If
                            If Not SGInternalStatusDictionary.ContainsKey(sSGInternalStatus) Then
                                SGInternalStatusDictionary.Add(sSGInternalStatus, sSGInternalStatusAction)
                            End If
                        Next nodeTransType

                        If Not RunTime() Then
                            AcceptedTransactionDictionary.Clear()
                            nodeTransTypeList = XMLSGInternalStatusDoc.SelectNodes("BabelBank_AML/AcceptedInfo/Valid")
                            For Each nodeTransType In nodeTransTypeList
                                bAddItem = True
                                node = nodeTransType.SelectSingleNode("Type")
                                If Not node Is Nothing Then
                                    If Not EmptyString(node.InnerText) Then
                                        sAcceptedType = node.InnerText
                                    Else
                                        bAddItem = False
                                        sAcceptedType = ""
                                    End If
                                Else
                                    bAddItem = False
                                    sAcceptedType = ""
                                End If
                                node = nodeTransType.SelectSingleNode("Text")
                                If Not node Is Nothing Then
                                    If Not EmptyString(node.InnerText) Then
                                        sAcceptedText = node.InnerText
                                    Else
                                        bAddItem = False
                                        sAcceptedText = ""
                                    End If
                                Else
                                    bAddItem = False
                                    sAcceptedText = ""
                                End If
                                If bAddItem Then
                                    If Not AcceptedTransactionDictionary.ContainsKey(sAcceptedType) Then
                                        AcceptedTransactionDictionary.Add(sAcceptedType, sAcceptedText)
                                    End If
                                End If
                            Next nodeTransType

                            'Dim pair As KeyValuePair(Of Integer, Integer) =
                            'New KeyValuePair(Of Integer, Integer)(5, 8)

                            'Dim kvp As System.Collections.Generic.KeyValuePair(Of String, System.Collections.Generic.List(Of String)) = KeyValuePair(Of String, List(Of String))()

                            For Each kvp As System.Collections.Generic.KeyValuePair(Of String, String) In AcceptedTransactionDictionary
                                'For Each kvp As System.Collections.Generic.KeyValuePair(Of String, System.Collections.Generic.List(Of String)) In AcceptedTransactionDictionary

                                'sKey = kvp.Key 'SubCategory_Name
                                'sValue = kvp.Value 'PEP

                                'Dim rows As String = kvp.Value
                                'For Each item As String In rows
                                '    MessageBox.Show(item.ToString)
                                'Next
                            Next

                        End If


                        If 1 = 2 Then '1 = 1 To use csv-file, 1 = 2 to use xlsx-file 
                            If RunTime() Then 'DISKUTERNOTRUNTIME Felles mappe?
                                If Not oFs.FileExists(oBabelFiles.VB_Profile.Custom1Value) Then
                                    Throw New Exception("Treat_SGFinansAML - Kan ikke finne filen " & oBabelFiles.VB_Profile.Custom1Value & vbCrLf & "som benyttes til � sjekke ut gokjente debitorer.")
                                Else

                                    'oBabelLog.AddLogEvent("Funnet XML-filen!", 0)

                                    'oBabelLog.AddLogEvent("TXT-filen inneholder: " & sContent, 0)
                                    oFile = oFs.OpenTextFile(My.Application.Info.DirectoryPath & "\Mapping\Godkjente debitorer.xlsx", Scripting.IOMode.ForReading, True, 0)
                                    sContent = oFile.ReadLine
                                    XMLDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml")

                                End If
                            Else
                                If Not oFs.FileExists(My.Application.Info.DirectoryPath & "\Mapping\Godkjente debitorer.csv") Then
                                    Throw New Exception("Treat_SGFinansAML - Kan ikke finne filen " & My.Application.Info.DirectoryPath & "\Mapping\SGF_AML_TransTypesToValidate.xml" & vbCrLf & "som benyttes til � sjekke ut gokjente debitorer.")
                                Else
                                    'XMLDoc.async = False
                                    XMLDoc.Load(My.Application.Info.DirectoryPath & "\Mapping\Godkjente debitorer.csv")
                                End If
                            End If


                        Else
                            'Load the file with cleared payors
                            appExcel = New Microsoft.Office.Interop.Excel.Application
                            appExcel.DisplayAlerts = False

                            sFilename = ""
                            sFilename = System.Configuration.ConfigurationManager.AppSettings("SGF_PathGodkjenteDebitorer")
                            If EmptyString(sFilename) Then
                                Throw New Exception("Treat_SGFinansAML: Det er ikke angitt riktig n�kkel i BabelBank sin konfigurasjonsfil for fil for godkjente debitorer." & vbCrLf & vbCrLf & "N�kkel skal v�re: " & "SGF_PathGodkjenteDebitorer")
                            ElseIf Not oFs.FileExists(sFilename) Then
                                bWorkBookFound = False

                                'Throw New Exception("Treat_SGFinansAML: Kan ikke finne filen " & sFilename & vbCrLf & " som benyttes for godkjente debitorer.")

                            Else
                                bWorkBookFound = True
                                '12.07.2022 - Changed parameter ReadOnly from True to False
                                '  This solved an error after a patch upgrade of Excel at Nordea Finance
                                oExcelBook = appExcel.Workbooks.Open(getlongpath(sFilename), , False)
                            End If

                            If bWorkBookFound Then
                                'lRowNo = "KOKO"

                                oExcelSheet = oExcelBook.Worksheets(1)

                                ClearedCompayDictionary.Clear()
                                lRowNo = 4
                                iEmptyRows = 0
                                Do While lRowNo < 10000
                                    sTmp = oExcelSheet.Cells(lRowNo, 10).Value
                                    If EmptyString(sTmp) Then
                                        If iEmptyRows > 200 Then
                                            Exit Do
                                        Else
                                            iEmptyRows = iEmptyRows + 1
                                        End If
                                    Else
                                        sTmp = sTmp.ToUpper.Trim
                                        '20.05.2020 - Added next line
                                        sTmp = RemoveCharacters(sTmp, sCharactersToRemove)
                                        If Not ClearedCompayDictionary.ContainsKey(sTmp) Then
                                            ClearedCompayDictionary.Add(sTmp, sTmp)
                                        End If
                                    End If
                                    '28.11.2018 - Also checking column K (Debtor name (Alternativt))
                                    sTmp = oExcelSheet.Cells(lRowNo, 11).Value
                                    If EmptyString(sTmp) Then
                                    Else
                                        sTmp = sTmp.ToUpper.Trim
                                        '20.05.2020 - Added next line
                                        sTmp = RemoveCharacters(sTmp, sCharactersToRemove)
                                        If Not ClearedCompayDictionary.ContainsKey(sTmp) Then
                                            ClearedCompayDictionary.Add(sTmp, sTmp)
                                        End If
                                    End If

                                    lRowNo = lRowNo + 1
                                Loop
                            End If
                        End If

                        'FOR TESTING!!! - MAY BE USED TO TEST AGAINST GODKJENTE DEBITORER
                        'If Not RunTime() Then
                        '    For Each oBabelFile In oBabelFiles
                        '        For Each oBatch In oBabelFile.Batches
                        '            For Each oPayment In oBatch.Payments
                        '                If Not EmptyString(oPayment.E_Name) Then
                        '                    sTmp = RemoveCharacters(oPayment.E_Name, sCharactersToRemove)
                        '                    If ClearedCompayDictionary.ContainsKey(sTmp.ToUpper.Trim) Then
                        '                        If oPayment.ExtraD4 = "1" Then
                        '                            oPayment.ExtraD4 = "2"
                        '                        Else
                        '                            oPayment.ExtraD4 = "4"
                        '                        End If
                        '                    Else
                        '                        oPayment.ExtraD4 = "0"
                        '                    End If
                        '                End If
                        '            Next oPayment
                        '        Next oBatch
                        '    Next oBabelFile
                        'End If

                        'sTmp = "CF ZARYA LTD"
                        ''sTmp = "CF ZARYA Ltd"
                        'If ClearedCompayDictionary.ContainsKey(sTmp) Then
                        '    sTmp = sTmp.Trim
                        'Else
                        '    sTmp = sTmp.Trim
                        'End If

                        For Each oBabelFile In oBabelFiles
                            sSpecial = oBabelFile.Special
                            sFilename = oBabelFile.FilenameIn
                            For Each oBatch In oBabelFile.Batches

                                For Each oPayment In oBatch.Payments

                                    '02.11.2019 - Removed all code regarding valid transactionscodes stated in a XML-file
                                    'If Not EmptyString(oPayment.Visma_StatusCode) Then

                                    If oPayment.I_Account <> sOld_I_Account Then
                                        For Each oFilesetup In oBatch.VB_Profile.FileSetups
                                            For Each oClient In oFilesetup.Clients
                                                For Each oaccount In oClient.Accounts
                                                    If oaccount.Account = oPayment.I_Account Or oaccount.ConvertedAccount = oPayment.I_Account Then

                                                        sOld_I_Account = oPayment.I_Account
                                                        sAccountCurrencyCode = oaccount.CurrencyCode
                                                        sClientCountryCode = oaccount.DebitAccountCountryCode

                                                    End If
                                                Next oaccount
                                            Next oClient
                                        Next oFilesetup
                                    End If

                                    sCountryCodeToUse = ""

                                    If Not EmptyString(oPayment.Visma_StatusCode) Then
                                        If TransTypeDictionary.ContainsKey(oPayment.Visma_StatusCode) Then
                                            'Valid transactiontype
                                            sCountryCodeToUse = sClientCountryCode

                                        End If
                                    Else
                                        'MsgBox("oPayment.Visma_StatusCode er tom")
                                    End If

                                    If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20220622" Then
                                        sCountryCodeToUse = "NO"
                                    End If

                                    bRunTheSQL = False

                                    If EmptyString(oPayment.E_Name) Then
                                        '07.11.2019 - Removed code below
                                        'If oPayment.PayType <> "I" And oPayment.MON_InvoiceCurrency = "NOK" Then
                                        '    bRunTheSQL = False
                                        '    sMySQL = sMySetupSQL.Replace("BB_NAME", " ")
                                        'Else
                                        '    sMySQL = sMySetupSQL.Replace("BB_NAME", " ") ' Replace(sMySetupSQL, "BB_Name", " ", , , vbTextCompare)
                                        'End If
                                        sMySQL = sMySetupSQL.Replace("BB_NAME", " ")
                                    ElseIf oPayment.E_Name.Trim = "-" Then
                                        '06.01.2021 - Added this ElseIf after Teams-meeting, Name = "-" created an error 
                                        sMySQL = sMySetupSQL.Replace("BB_NAME", " ")
                                    ElseIf oPayment.E_Name.Trim = "." Then
                                        '06.01.2021 - Added this ElseIf after error in EQ DK, Name = "." created an error 
                                        sMySQL = sMySetupSQL.Replace("BB_NAME", " ")
                                    Else
                                        sMySQL = sMySetupSQL.Replace("BB_NAME", oPayment.E_Name.Replace("'", String.Empty).Trim) '.Replace(Chr(34), String.Empty).Trim)
                                        bRunTheSQL = True
                                    End If

                                    sTmp = ""
                                    If Not EmptyString(oPayment.E_Adr1) Then
                                        sTmp = oPayment.E_Adr1.Trim
                                    End If
                                    If Not EmptyString(oPayment.E_Adr2) Then
                                        sTmp = sTmp & " " & oPayment.E_Adr2.Trim
                                    End If
                                    If Not EmptyString(oPayment.E_Adr3) Then
                                        sTmp = sTmp & " " & oPayment.E_Adr3.Trim
                                    End If
                                    If EmptyString(sTmp) Then
                                        sTmp = " "
                                    Else
                                        sTmp = sTmp.Trim
                                        sTmp = sTmp.Replace("'", String.Empty)
                                        bRunTheSQL = True
                                    End If

                                    sMySQL = sMySQL.Replace("BB_ADDRESS", sTmp)

                                    sTmp = ""
                                    If EmptyString(oPayment.E_Name) Or oPayment.E_Name = "-" Or oPayment.E_Name = "." Then
                                        oPayment.ExtraD4 = "3"
                                        If EmptyString(oPayment.E_CountryCode) Then
                                            If Not EmptyString(sCountryCodeToUse) Then
                                                sTmp = sCountryCodeToUse
                                            End If
                                        End If
                                        bRunTheSQL = True
                                    ElseIf EmptyString(oPayment.E_CountryCode) Then
                                        'Added 07.11.2019
                                        'To add accountcountrycode to the transactions that
                                        ' have an approved transactiontype in the XML-file. 

                                        If EmptyString(sCountryCodeToUse) Then
                                            oPayment.ExtraD4 = "3"
                                            'MsgBox("sCountryCodeToUse er blank " & oPayment.E_Name)

                                        Else
                                            sTmp = sCountryCodeToUse
                                            'MsgBox("sCountryCodeToUse er IKKE blank. sTmp har verdien: " & sTmp)
                                            oPayment.ExtraD4 = "0"
                                        End If
                                        'If EmptyString(oPayment.Visma_StatusCode) Then
                                        '    oPayment.ExtraD4 = "3"
                                        'Else
                                        '    If oPayment.Visma_StatusCode.Length > 3 Then
                                        '        If oPayment.Visma_StatusCode.Substring(0, 4) = "DMCT" Then
                                        '            oPayment.ExtraD4 = "0"
                                        '        Else
                                        '            oPayment.ExtraD4 = "3"
                                        '        End If
                                        '    Else
                                        '        oPayment.ExtraD4 = "3"
                                        '    End If
                                        'End If
                                        bRunTheSQL = True
                                    Else
                                        If oPayment.E_CountryCode.Trim.Length = 2 Then
                                            sTmp = oPayment.E_CountryCode.Trim
                                            oPayment.ExtraD4 = "0" 'Added 15.11 If SQL returns nothing and the payor is cleared it would get ExtraD4 4
                                            bRunTheSQL = True
                                        Else
                                            sTmp = " "
                                            bRunTheSQL = True
                                            oPayment.ExtraD4 = "3"
                                        End If
                                    End If
                                    sMySQL = sMySQL.Replace("BB_COUNTRYCODE", sTmp)

                                    'If Format(Date.Today, "yyyyMMdd").Substring(0, 8) = "20200130" Then
                                    '    If oPayment.E_Name = "PETTERI HIRVONEN" Then
                                    'MsgBox("SQL-en som skal kj�res: " & sMySQL)
                                    '    End If
                                    'End If

                                    ' run the sql

                                    sSGInternalStatus = ""
                                    If bRunTheSQL Then

                                        'If EmptyString(oPayment.E_CountryCode) Then
                                        '    MsgBox("Skal kj�re f�lgende SQL: " & sMySQL)
                                        'End If

                                        oMyERPDal.SQL = sMySQL
                                        bOK = True
                                        If oMyERPDal.Reader_Execute() Then
                                            If oMyERPDal.Reader_HasRows Then
                                                'MsgBox("Her fant vi noe muffens gitt.")

                                                'If EmptyString(oPayment.E_CountryCode) Then
                                                'MsgBox("Her er det noe muffens: " & sMySQL)
                                                'End If
                                                '25.09.2017 - " CountryRiskRatingLevel=" & Chr(34) & "MED-LOW" & Chr(34) & "/>

                                                'Definitions of ExtraD4:
                                                ' 0 = OK
                                                ' 1 = Positiv treff, ikke klarert
                                                ' 2 = Positiv treff, betaler klarert (ligger i Excel-fil)
                                                ' 3 = Ukjent land, ikke klarert
                                                ' 4 = Ukjent land, klarert betaler (ligger i Excel-fil)


                                                'oPayment.ExtraD4 = "1" '30.01.2020 - Moved further down
                                                iCounter = 0

                                                Do While oMyERPDal.Reader_ReadRecord
                                                    iCounter = iCounter + 1

                                                    sTmp = oMyERPDal.Reader_GetField(0)

                                                    If Not RunTime() And Format(Date.Today, "dd.MM.yyyy") = "29.06.2022" Then
                                                        'If Not RunTime() Then  'NOTRUNTIME
                                                        sTmp = sTestString
                                                    End If

                                                    '30.01.2020 - Added this IF because of a change in the precedure Compliance_Check
                                                    'If everything is OK it will now return an empty string.
                                                    If IsDBNull(sTmp) Then

                                                        'OK - No problem with this payment

                                                    ElseIf EmptyString(sTmp) Then

                                                        'OK - No problem with this payment

                                                    Else

                                                        '10.03.2020 - Added code rearding SGInternalStatus
                                                        bContinue = True
                                                        If Not EmptyString(sTmp) Then
                                                            If sTmp.Contains("SGInternalStatus") Then
                                                                iPos = sTmp.IndexOf("SGInternalStatus")
                                                                sSGInternalStatus = sTmp.Substring(iPos + 18, 2)
                                                                sSGInternalStatus = sSGInternalStatus.Replace(Chr(34), "")
                                                                If SGInternalStatusDictionary.ContainsKey(sSGInternalStatus) Then
                                                                    If SGInternalStatusDictionary(sSGInternalStatus) = "IGNORE" Then
                                                                        'Everything is OK, no problem with the payment
                                                                        bContinue = False
                                                                    End If
                                                                End If
                                                            End If

                                                            '28.06.2022 - Tempcode to remove payments with SubCategory_Name='PEP'
                                                            'If sTmp.Contains("SubCategory_Name='PEP'") Then
                                                            '    MsgBox("OKOKOK Innbetaling fra: " & oPayment.E_Name & " inneholder PEP." & vbCrLf & "stmp: " & sTmp)
                                                            '    bContinue = False
                                                            '    'sTmpLine = sTmpNew.Substring(0, iPos)
                                                            '    'sTmpNew = sTmpNew.Substring(iPos)
                                                            'Else
                                                            '    MsgBox("Innbetaling fra: " & oPayment.E_Name & " inneholder IKKE PEP." & vbCrLf & "stmp: " & sTmp)
                                                            'End If
                                                            '28.06.2022 - Tempcode to remove payments with SubCategory_Name='PEP'
                                                            'MsgBox("Skal sjekke: " & "SubCategory_Name=" & Chr(34) & "PEP" & Chr(34))

                                                            If Not RunTime() Then
                                                                If sTmp.Contains("SubCategory_Name=" & Chr(34) & "PEP" & Chr(34)) Then
                                                                    'MsgBox("CHR(34)OKOKOK Innbetaling fra: " & oPayment.E_Name & " inneholder PEP." & vbCrLf & "stmp: " & sTmp)
                                                                    bContinue = False
                                                                    'sTmpLine = sTmpNew.Substring(0, iPos)
                                                                    'sTmpNew = sTmpNew.Substring(iPos)
                                                                Else
                                                                    'MsgBox("CHR(34)Innbetaling fra: " & oPayment.E_Name & " inneholder IKKE PEP." & vbCrLf & "stmp: " & sTmp)
                                                                End If
                                                            End If

                                                        End If

                                                        If bContinue Then

                                                            bOK = False
                                                            '30.01.2020 - Moved here
                                                            oPayment.ExtraD4 = "1"

                                                            oPayment.ToSpecialReport = True
                                                            For Each oInvoice In oPayment.Invoices
                                                                oInvoice.ToSpecialReport = True
                                                            Next oInvoice

                                                            'If EmptyString(oPayment.E_CountryCode) Then
                                                            '    MsgBox("Feilmelding: " & sTmp)
                                                            'End If

                                                            If oPayment.Invoices.Count > 0 Then

                                                                If Not EmptyString(sTmp) Then
                                                                    If sTmp.Length > 2 Then
                                                                        oInvoice = oPayment.Invoices.Item(oPayment.Invoices.Count)
                                                                        sTmp = sTmp.Replace(Chr(34), "'")
                                                                        sTmp = sTmp.Replace("<SGEDW_Deliveries.dbo.ComplianceEntityList ", String.Empty)
                                                                        sTmp = sTmp.Replace("<ComplianceEntityList ", String.Empty)
                                                                        'sTmp = sTmp.Replace("<t ", String.Empty) 'Added 22.06.2022
                                                                        sTmp = sTmp.Replace("/>", String.Empty)

                                                                        Do While True
                                                                            If sTmp.Length > 3 Then
                                                                                iPos = 0
                                                                                iPos = sTmp.IndexOf("ListName", 3) 'Changed 22.06.2022, from 3 to 0
                                                                                sTmpNew = String.Empty
                                                                                If iPos > 0 Then 'Changed 22.06.2022, from 0 to -1
                                                                                    sTmpNew = sTmp.Substring(0, iPos)
                                                                                    sTmp = sTmp.Substring(iPos)
                                                                                Else
                                                                                    sTmpNew = sTmp
                                                                                    sTmp = String.Empty
                                                                                End If
                                                                                If Not EmptyString(sTmpNew) Then
                                                                                    If iCounter = 1 Then
                                                                                        iCounter = iCounter + 1 'Added 22.06.2022
                                                                                        oFreeText = oInvoice.Freetexts.Add
                                                                                        oFreeText.Text = "TREFF I COMPLIANCE CHECKLIST"
                                                                                    End If
                                                                                    iPos = sTmpNew.IndexOf("ListType", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        oFreeText = oInvoice.Freetexts.Add
                                                                                        oFreeText.Text = sTmpLine
                                                                                    End If

                                                                                    'Added 12.09.2017
                                                                                    iPos = sTmpNew.IndexOf("Match=", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        oFreeText = oInvoice.Freetexts.Add
                                                                                        oFreeText.Text = sTmpLine
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("SearchValue", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("CountryRiskRatingLevel", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("SGInternalStatus", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("Category_Name", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("SubCategory_Name", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("FurtherInformation_Text", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    iPos = sTmpNew.IndexOf("ExternalSource_Text", 3)
                                                                                    sTmpLine = String.Empty
                                                                                    If iPos > 0 Then
                                                                                        sTmpLine = sTmpNew.Substring(0, iPos)
                                                                                        sTmpNew = sTmpNew.Substring(iPos)
                                                                                        Do While Not EmptyString(sTmpLine)
                                                                                            If EmptyString(sTmpLine) Then
                                                                                                Exit Do
                                                                                            ElseIf sTmpLine.Length > 47 Then
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine.Substring(0, 47)
                                                                                                sTmpLine = sTmpLine.Substring(47)
                                                                                            Else
                                                                                                oFreeText = oInvoice.Freetexts.Add
                                                                                                oFreeText.Text = sTmpLine
                                                                                                sTmpLine = ""
                                                                                            End If
                                                                                        Loop
                                                                                    End If

                                                                                    If Not EmptyString(sTmpNew) Then
                                                                                        If sTmpNew <> "<t " Then

                                                                                            Do While Not EmptyString(sTmpNew)
                                                                                                If EmptyString(sTmpNew) Then
                                                                                                    Exit Do
                                                                                                ElseIf sTmpNew.Length > 47 Then
                                                                                                    oFreeText = oInvoice.Freetexts.Add
                                                                                                    oFreeText.Text = sTmpNew.Substring(0, 47)
                                                                                                    sTmpNew = sTmpNew.Substring(47)
                                                                                                Else
                                                                                                    oFreeText = oInvoice.Freetexts.Add
                                                                                                    oFreeText.Text = sTmpNew
                                                                                                    sTmpNew = ""
                                                                                                End If
                                                                                            Loop
                                                                                        End If
                                                                                    End If
                                                                                End If
                                                                            Else
                                                                                Exit Do
                                                                            End If
                                                                        Loop

                                                                    End If
                                                                End If

                                                                '' 10.08.2017
                                                                '' NOT TESTED
                                                                '' Add info about Cdtr to be shown in the freetext part of report;
                                                                'If Not EmptyString(oPayment.I_Name) Then
                                                                '    oFreeText = oInvoice.Freetexts.Add
                                                                '    oFreeText.Text = "Kreditor:"
                                                                '    oFreeText = oInvoice.Freetexts.Add
                                                                '    oFreeText.Text = oPayment.I_Name
                                                                '    If Not EmptyString(oPayment.I_Adr1) Then
                                                                '        oFreeText.Text = oPayment.I_Adr1
                                                                '    End If
                                                                '    If Not EmptyString(oPayment.I_Adr2) Then
                                                                '        oFreeText.Text = oPayment.I_Adr2
                                                                '    End If
                                                                '    If Not EmptyString(oPayment.I_Adr3) Then
                                                                '        oFreeText.Text = oPayment.I_Adr3
                                                                '    End If
                                                                '    If Not EmptyString(oPayment.I_Zip) Then
                                                                '        oFreeText.Text = oPayment.I_Zip
                                                                '    End If
                                                                '    If Not EmptyString(oPayment.I_City) Then
                                                                '        oFreeText.Text = oPayment.I_City
                                                                '    End If
                                                                'End If

                                                            End If 'If oPayment.Invoices.Count > 0 Then
                                                        End If 'If bContinue Then

                                                    End If
                                                    Exit Do
                                                Loop

                                            Else

                                                'OK - No problem with this payment

                                                '30.01.2020 - Old code before introducing the variable bOK
                                                ''Added - 25.08.2017
                                                'If EmptyString(oPayment.E_Name) Then
                                                '    oPayment.ExtraD4 = "3"
                                                'ElseIf EmptyString(oPayment.E_CountryCode) Then
                                                '    If EmptyString(sCountryCodeToUse) Then
                                                '        oPayment.ExtraD4 = "3"
                                                '    Else
                                                '        oPayment.ExtraD4 = "0"
                                                '    End If
                                                'Else
                                                '    If oPayment.E_CountryCode.Trim.Length = 2 Then
                                                '        'OK
                                                '    Else
                                                '        oPayment.ExtraD4 = "3"
                                                '    End If
                                                'End If

                                                ' ''04.11.2019 - Changed code below
                                                ''If oPayment.ExtraD4 = "3" Then
                                                ''    If EmptyString(oPayment.Visma_StatusCode) Then
                                                ''        oPayment.ExtraD4 = "3"
                                                ''    Else
                                                ''        If oPayment.Visma_StatusCode.Length > 3 Then
                                                ''            If oPayment.Visma_StatusCode.Substring(0, 4) = "DMCT" Then
                                                ''                oPayment.ExtraD4 = "0"
                                                ''            Else
                                                ''                oPayment.ExtraD4 = "3"
                                                ''            End If
                                                ''        Else
                                                ''            oPayment.ExtraD4 = "3"
                                                ''        End If
                                                ''    End If
                                                ''End If

                                            End If
                                        Else
                                            Throw New Exception(oMyERPDal.ErrorNumber.ToString & ": " & oMyERPDal.ErrorMessage)
                                        End If

                                        '30.01.2020 - Added this IF
                                        If bOK Then

                                            'Added - 25.08.2017
                                            If EmptyString(oPayment.E_Name) Or oPayment.E_Name = "-" Or oPayment.E_Name = "." Then
                                                '26.01.2021 - Added - and . see comment above
                                                oPayment.ExtraD4 = "3"
                                            ElseIf EmptyString(oPayment.E_CountryCode) Then
                                                If EmptyString(sCountryCodeToUse) Then
                                                    oPayment.ExtraD4 = "3"
                                                Else
                                                    oPayment.ExtraD4 = "0"
                                                End If
                                            Else
                                                If oPayment.E_CountryCode.Trim.Length = 2 Then
                                                    'OK
                                                Else
                                                    oPayment.ExtraD4 = "3"

                                                End If
                                            End If

                                            ''04.11.2019 - Changed code below
                                            'If oPayment.ExtraD4 = "3" Then
                                            '    If EmptyString(oPayment.Visma_StatusCode) Then
                                            '        oPayment.ExtraD4 = "3"
                                            '    Else
                                            '        If oPayment.Visma_StatusCode.Length > 3 Then
                                            '            If oPayment.Visma_StatusCode.Substring(0, 4) = "DMCT" Then
                                            '                oPayment.ExtraD4 = "0"
                                            '            Else
                                            '                oPayment.ExtraD4 = "3"
                                            '            End If
                                            '        Else
                                            '            oPayment.ExtraD4 = "3"
                                            '        End If
                                            '    End If
                                            'End If

                                        End If

                                    End If
                                    If Not EmptyString(oPayment.E_Name) Then

                                        If Not oPayment.E_Name = "-" Then

                                            If Not oPayment.E_Name = "." Then

                                                If oPayment.ExtraD4 = "0" Then
                                                    'These payments are OK, do not check anymore
                                                Else
                                                    '20.05.2020 - Added next line
                                                    sTmp = RemoveCharacters(oPayment.E_Name, sCharactersToRemove)

                                                    If ClearedCompayDictionary.ContainsKey(sTmp.ToUpper.Trim) Then

                                                        If oPayment.ExtraD4 = "1" Then
                                                            oPayment.ExtraD4 = "2"
                                                        Else
                                                            oPayment.ExtraD4 = "4"
                                                        End If

                                                    End If
                                                End If

                                            End If
                                        End If

                                    End If

                                    '10.03.2020 - Added code rearding SGInternalStatus
                                    If oPayment.ExtraD4 = "1" Or oPayment.ExtraD4 = "3" Then

                                        'MsgBox("Har funnet en som skal rapporteres!")

                                        If SGInternalStatusDictionary.ContainsKey(sSGInternalStatus) Then
                                            If SGInternalStatusDictionary(sSGInternalStatus) = "IGNORE" Then
                                                'Everything is OK, no problem with the payment

                                            ElseIf SGInternalStatusDictionary(sSGInternalStatus) = "REPORT" Then
                                                oPayment.ExtraD4 = "2"

                                            ElseIf SGInternalStatusDictionary(sSGInternalStatus) = "STOP" Then
                                                'No change. remove it by posting it to correct account

                                            End If
                                        End If
                                    End If

                                    '28.05.2020 - Added next section after mail from Jan-Kjetil
                                    'Do not stop zero-payments
                                    If IsEqualAmount(oPayment.MON_InvoiceAmount, 0) Then
                                        If oPayment.ExtraD4 = "1" Or oPayment.ExtraD4 = "3" Or oPayment.ExtraD4 = "4" Then
                                            oPayment.ExtraD4 = "2"
                                        End If
                                    End If

                                    ''Else
                                    ''    oPayment.ExtraD4 = "0"
                                    'End If 'If TransTypeDictionary.ContainsKey(oPayment.Visma_StatusCode) Then
                                    'End If 'If Not EmptyString(oPayment.Visma_StatusCode) Then

                                    '04.11.2019 - Added new code to post payments that have a positive match and aren't cleared
                                    If oPayment.ExtraD4 = "1" Or oPayment.ExtraD4 = "3" Then

                                        'MsgBox("Fortsatt feil p� denne!")

                                        oPayment.PurposeCode = "AML"

                                        bAccountFound = False
                                        For iAccountCounter = 0 To aAMLAccounts.GetUpperBound(1)
                                            If aAMLAccounts(1, iAccountCounter) = oPayment.MON_InvoiceCurrency Then
                                                bAccountFound = True
                                                Exit For
                                            End If
                                        Next iAccountCounter

                                        If Not bAccountFound Then
                                            Throw New Exception("Det er ikke angitt noen AML-posteringskonto for valutaen: " & oPayment.MON_InvoiceCurrency)
                                        ElseIf EmptyString(aAMLAccounts(0, iAccountCounter)) Then
                                            Throw New Exception("Det er ikke angitt noen posteringskonto p� AML-kontoen for valutaen: " & oPayment.MON_InvoiceCurrency)
                                        Else
                                            '05.06.2020 - Added next IF
                                            If sSpecial = "SG FINANS" Then
                                                If oPayment.MON_OriginallyPaidCurrency <> oPayment.MON_InvoiceCurrency Then
                                                    oPayment.MON_AccountExchRate = 0
                                                    oPayment.MON_LocalExchRate = 0
                                                    If oPayment.Invoices.Count > 0 Then
                                                        oFreeText = oPayment.Invoices.Item(1).Freetexts.Add()
                                                        oFreeText.Text = "Oppr. bel�p: " & oPayment.MON_OriginallyPaidCurrency & " " & ConvertFromAmountToString(oPayment.MON_OriginallyPaidAmount, ".", ",")
                                                        oFreeText.Qualifier = 4
                                                    End If
                                                    oPayment.MON_OriginallyPaidCurrency = oPayment.MON_InvoiceCurrency
                                                    oPayment.MON_OriginallyPaidAmount = oPayment.MON_InvoiceAmount
                                                End If
                                            End If

                                            If oPayment.Invoices.Count = 1 Then

                                                'MsgBox("Invoices.Count = 1")


                                                For Each oInvoice In oPayment.Invoices
                                                    If sSpecial = "SGF_EQ" Then
                                                        'oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL '2
                                                        '18.05.2020 - Changed to MatchedOnCustomer
                                                        oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer
                                                        oInvoice.MATCH_ID = aAMLAccounts(0, iAccountCounter)
                                                        '18.05.2020 - Added next 3 lines
                                                        oInvoice.CustomerNo = aAMLAccounts(0, iAccountCounter)
                                                        oInvoice.MyField = "1"
                                                        oInvoice.MyField2 = "1"
                                                    Else
                                                        oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer '2
                                                        If sSpecial = "NORDEAFINANCE_APTIC" Then
                                                            oInvoice.MATCH_ID = aAMLAccounts(0, iAccountCounter)
                                                        Else
                                                            oInvoice.MATCH_ID = "KJELL"
                                                        End If
                                                        oInvoice.MATCH_ClientNo = xDelim(aAMLAccounts(0, iAccountCounter), "/", 1)
                                                        oInvoice.CustomerNo = xDelim(aAMLAccounts(0, iAccountCounter), "/", 3)
                                                        oInvoice.MyField = xDelim(aAMLAccounts(0, iAccountCounter), "/", 2) & "-" 'AgreementNumber
                                                        '05.06.2020 - Added next 2 lines
                                                        oPayment.MON_AccountExchRate = 0
                                                        oPayment.MON_LocalExchRate = 0
                                                    End If
                                                    oInvoice.MATCH_Matched = True
                                                Next oInvoice
                                            Else
                                                For Each oInvoice In oPayment.Invoices
                                                    oInvoice.MATCH_Final = False
                                                Next oInvoice

                                                oPayment.Invoices.Add()
                                                oInvoice.MATCH_Original = False
                                                oInvoice.MATCH_Final = True
                                                oInvoice.MON_InvoiceAmount = oPayment.MON_InvoiceAmount
                                                oInvoice.MON_TransferredAmount = oPayment.MON_TransferredAmount
                                                oInvoice.StatusCode = oPayment.StatusCode
                                                oInvoice.VB_Profile = oPayment.VB_Profile
                                                If sSpecial = "SGF_EQ" Then
                                                    'oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnGL '2
                                                    '18.05.2020 - Changed to MatchedOnCustomer
                                                    oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer
                                                    oInvoice.MATCH_ID = aAMLAccounts(0, iAccountCounter)
                                                    '18.05.2020 - Added next 3 lines
                                                    oInvoice.CustomerNo = aAMLAccounts(0, iAccountCounter)
                                                    oInvoice.MATCH_Matched = True
                                                    oInvoice.MyField = "1"
                                                    oInvoice.MyField2 = "1"
                                                Else
                                                    oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnCustomer '2
                                                    oInvoice.MATCH_Matched = True
                                                    oInvoice.MATCH_ClientNo = xDelim(aAMLAccounts(0, iAccountCounter), "/", 1)
                                                    oInvoice.CustomerNo = xDelim(aAMLAccounts(0, iAccountCounter), "/", 3)
                                                    oInvoice.MyField = xDelim(aAMLAccounts(0, iAccountCounter), "/", 2) & "-" 'AgreementNumber
                                                    If sSpecial = "NORDEAFINANCE_APTIC" Then
                                                        oInvoice.MATCH_ID = aAMLAccounts(0, iAccountCounter)
                                                    Else
                                                        oInvoice.MATCH_ID = "KJELL"
                                                    End If
                                                End If

                                            End If

                                            oPayment.MATCH_HowMatched = "Fjernet AML-sjekk"
                                            oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.Matched
                                        End If
                                    End If

                                Next oPayment
                            Next oBatch
                        Next oBabelFile
                        bReturnValue = True
                    Else
                        bReturnValue = False
                    End If


                    '28.04.2020 - Added to create charges report
                    oBabelReport = New vbBabel.BabelReport
                    oBabelReport.LoggingActivated = bLOG
                    oBabelReport.BabelLog = oBabelLog
                    oBabelReport.Special = sSpecial
                    oBabelReport.BabelFiles = oBabelFiles
                    oBabelReport.VB_ReportTime = vbBabel.BabelFiles.ReportTime.After_Autogiro_Export
                    'oBabelReport.OwnerForm = oOwnerForm
                    oBabelReport.BBIsInSilentMode()

                    If Not oBabelReport.RunReports(oFilesetup.FileSetup_ID) Then

                    End If

                    oBabelReport = Nothing

                End If

            End If

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        Finally
            'If Not oExcelSheet Is Nothing Then
            '    'releaseObject(oExcelSheet)
            '    oExcelSheet = Nothing
            'End If
            'If Not oExcelBook Is Nothing Then
            '    oExcelBook.Close()
            '    'releaseObject(oExcelBook)
            '    oExcelBook = Nothing
            'End If
            'If Not appExcel Is Nothing Then
            '    appExcel.Quit()
            '    'releaseObject(appExcel)
            '    appExcel = Nothing

            '    GC.Collect()
            '    GC.WaitForPendingFinalizers()
            '    GC.Collect()
            '    GC.WaitForPendingFinalizers()
            'End If

        End Try

        Return bReturnValue


    End Function
    'Code to try to release Excel
    'Private Sub releaseObject(ByVal obj As Object)
    '    Try
    '        System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
    '        obj = Nothing
    '    Catch ex As Exception
    '        obj = Nothing
    '    Finally
    '        GC.Collect()
    '    End Try
    'End Sub
    Public Function TreatVingcard_Payment(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal iFileSetup_ID As Integer) As Boolean
        Dim sOldAccountNo As String
        Dim bAccountFound As Boolean
        Dim sClientNo As String
        Dim sSpecialReportPath As String
        Dim sOriginalHeading As String
        Dim sTmp As String
        Dim j As Long
        Dim sI_Swift As String
        Dim bAllPaymentsHasKID As Boolean
        Dim sText As String
        Dim iInvoiceCounter As Integer
        Dim iCounter As Integer
        Dim iPos As Integer
        Dim iPos2 As Integer
        Dim aInvoiceInfo() As String

        Try

            sOldAccountNo = vbNullString
            sI_Swift = vbNullString
            For Each oBabelFile In oBabelFiles
                oProfile = oBabelFile.VB_Profile
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If sOldAccountNo <> oPayment.I_Account Then
                            sOldAccountNo = oPayment.I_Account
                            bAccountFound = False
                            sClientNo = vbNullString
                            For Each oFilesetup In oBabelFile.VB_Profile.FileSetups
                                For Each oClient In oFilesetup.Clients
                                    For Each oaccount In oClient.Accounts
                                        If sOldAccountNo = oaccount.Account Then
                                            sI_Swift = oaccount.SWIFTAddress
                                            bAccountFound = True
                                            Exit For
                                        End If
                                    Next
                                    If bAccountFound Then
                                        Exit For
                                    End If
                                Next
                                If bAccountFound Then
                                    Exit For
                                End If
                            Next
                        End If
                        If Not bAccountFound Then
                            Throw New Exception("TreatRiisBilglass - Kontonummer " & oPayment.I_Account & " ikke funnet i klientoppsettet" & vbCrLf & _
                                "Vennligst legg inn dette i klientoppsettet i BabelBank." & vbCrLf & vbCrLf & "Program avbrytes!")
                        End If
                        If Mid(sI_Swift, 5, 2) = "SE" Then
                            oPayment.PayTypeSetInImport = False

                            If IsPaymentDomestic(oPayment, sI_Swift) Then
                                bAllPaymentsHasKID = True
                                For Each oInvoice In oPayment.Invoices
                                    If Not EmptyString(oInvoice.Unique_Id) Then
                                        'OK, it is a KID
                                    Else
                                        sText = ""
                                        For Each oFreeText In oInvoice.Freetexts
                                            sText = sText & oFreeText.Text
                                        Next oFreeText
                                        If vbIsNumeric(Left(sText, 25), " 01234567890") Then
                                            If vbIsNumeric(Left(sText, 5), "0123456789") Then
                                                'OK, it is a KID
                                            Else
                                                bAllPaymentsHasKID = False
                                            End If
                                        Else
                                            bAllPaymentsHasKID = False
                                        End If
                                    End If
                                    If Not bAllPaymentsHasKID Then
                                        Exit For
                                    End If
                                Next oInvoice
                                If bAllPaymentsHasKID Then
                                    oPayment.PayCode = "601"

                                    'ReDim aInvoiceInfo(3, oPayment.Invoices.Count)
                                    'iCounter = -1
                                    For Each oInvoice In oPayment.Invoices
                                        'Move information to an array
                                        'iCounter = iCounter + 1
                                        For Each oFreeText In oInvoice.Freetexts
                                            oInvoice.Unique_Id = Trim(Left(oFreeText.Text, 25))
                                            Exit For
                                        Next oFreeText
                                        'aInvoiceInfo(0, iCounter) = oInvoice.Unique_Id
                                        'aInvoiceInfo(1, iCounter) = oInvoice.ImportFormat
                                        'aInvoiceInfo(2, iCounter) = Trim(Str(oInvoice.MON_InvoiceAmount))
                                        'aInvoiceInfo(3, iCounter) = oInvoice.REF_Own
                                    Next oInvoice

                                    'Remove all old text
                                    'For iCounter = oPayment.Invoices.Count To 1 Step -1
                                    '    oPayment.Invoices.Remove (iCounter)
                                    'Next iCounter

                                    'Add new invoices
                                    'For iCounter2 = 0 To iCounter
                                    '    Set oInvoice = oPayment.Invoices.Add
                                    '    oInvoice.Unique_Id = aInvoiceInfo(0, iCounter)
                                    '    oInvoice.ImportFormat = aInvoiceInfo(1, iCounter)
                                    '    oInvoice.MON_InvoiceAmount = Val(aInvoiceInfo(2, iCounter))
                                    '    oInvoice.MON_TransferredAmount = oInvoice.MON_TransferredAmount
                                    '    oInvoice.REF_Own = aInvoiceInfo(3, iCounter)
                                    'Next iCounter2

                                Else
                                    iInvoiceCounter = 0
                                    sText = ""
                                    For Each oInvoice In oPayment.Invoices
                                        iInvoiceCounter = iInvoiceCounter + 1
                                        iPos = 0
                                        iPos2 = 0
                                        sText = ""
                                        For Each oFreeText In oInvoice.Freetexts
                                            sText = sText & oFreeText.Text
                                        Next oFreeText
                                        If iInvoiceCounter = 1 Then
                                            iPos = InStr(1, sText, "DATE:", vbTextCompare)
                                            If iPos > 0 Then
                                                sText = Left(sText, iPos - 1)
                                            Else
                                                Exit For
                                            End If
                                        Else
                                            iPos = InStr(1, sText, "INVOICE:", vbTextCompare)
                                            iPos2 = InStr(1, sText, "DATE:", vbTextCompare)
                                            If iPos > 0 And iPos2 > 0 Then
                                                sText = Trim(Mid(sText, iPos + 8, iPos2 - (iPos + 8)))
                                            Else
                                                Exit For
                                            End If
                                        End If
                                        'Remove all old text
                                        For iCounter = oInvoice.Freetexts.Count To 1 Step -1
                                            oInvoice.Freetexts.Remove(iCounter)
                                        Next iCounter
                                        'Add the new one
                                        oFreeText = oInvoice.Freetexts.Add
                                        oFreeText.Text = sText
                                    Next oInvoice
                                End If
                            End If
                            oPayment.PayTypeSetInImport = True

                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        TreatVingcard_Payment = True

    End Function
    Public Function TreatSkuld(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        Dim bAllInvoicesHaveKID As Boolean = True

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        bAllInvoicesHaveKID = True
                        For Each oInvoice In oPayment.Invoices
                            If EmptyString(oInvoice.Unique_Id) Then
                                bAllInvoicesHaveKID = False
                            End If
                        Next oInvoice
                        If bAllInvoicesHaveKID Then
                            oPayment.PayCode = "301"
                        End If

                        ' 07.11.2014 Long receivers names where name/address is split by }
                        If oPayment.E_Adr1.Contains("}") Then
                            ' we have a | in address1, redo name/address
                            oPayment.E_Name = oPayment.E_Name & oPayment.E_Adr1.Substring(0, oPayment.E_Adr1.IndexOf("}"))
                            oPayment.E_Adr1 = oPayment.E_Adr1.Substring(oPayment.E_Adr1.IndexOf("}") + 1)   ' Keep whatever is behind |
                        End If

                        ' 26.06.2015
                        ' Skuld has ; (semicolon) as part of Ref_Own.
                        ' This is not an allowed sign in ISO20022 - so we need to replace it, with : (colon)
                        oPayment.REF_Own = Replace(oPayment.REF_Own, ";", ":")
                    Next oPayment
                Next oBatch
            Next oBabelFile

            Return True

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

    End Function
    Public Function TreatSkuld_Return(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' 26.06.2015 - added this function to redo from : to ; in Ref_Own

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        ' Skuld has ; (semicolon) as part of Ref_Own.
                        ' When creating pain.001 we have changed from ; to : as this is not an allowed sign in ISO20022 
                        ' - so we need to replace : with ; to recreate original Ref_Own
                        oPayment.REF_Own = Replace(oPayment.REF_Own, ":", ";")
                    Next oPayment
                Next oBatch
            Next oBabelFile

            Return True

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

    End Function
    Public Function Treat_SGF_EQ(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        Dim bAllInvoicesHaveKID As Boolean = True
        Dim sMergeCriteria As String

        'NBNBNBNB! Kj�res f�r TreatFilter, det m� den for at merging skal fungere (bruk av paymentIndex fungerer ikke n�r enkelte betalinger er fjernet i TreatFilter).

        Try

            For Each oBabelFile In oBabelFiles
                If oBabelFile.BankByCode = vbBabel.BabelFiles.Bank.DanskeBank_SE Then

                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            sMergeCriteria = ""
                            sMergeCriteria = oPayment.I_Account
                            sMergeCriteria = sMergeCriteria & oPayment.MON_InvoiceCurrency
                            sMergeCriteria = sMergeCriteria & oPayment.E_Account
                            sMergeCriteria = sMergeCriteria & oPayment.E_Name
                            sMergeCriteria = sMergeCriteria & oPayment.e_OrgNo
                            sMergeCriteria = sMergeCriteria & oBatch.Index.ToString
                            sMergeCriteria = sMergeCriteria & Chr(34) & "/d" & oPayment.DATE_Payment
                            oPayment.MergeCriteria = sMergeCriteria
                            oPayment.MATCH_DifferenceExact = True
                            If oPayment.MON_InvoiceAmount < 0 Then
                                sMergeCriteria = sMergeCriteria
                            End If
                        Next oPayment
                    Next oBatch
                    oBabelFile.Visma_MergeCreditNotes()
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            oPayment.MergeCriteria = ""
                        Next oPayment
                    Next oBatch
                End If
            Next oBabelFile

            Return True

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

    End Function
    Public Function TreatRedcross_UndoMatchedOnMalgruppe(ByRef oBabelFiles As vbBabel.BabelFiles) As Boolean
        Dim bAllInvoicesMatchedOnMalgruppe As Boolean = True

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        bAllInvoicesMatchedOnMalgruppe = True
                        For Each oInvoice In oPayment.Invoices
                            If oInvoice.MATCH_Final = True Then
                                If oInvoice.MATCH_ID = oPayment.ExtraD3 Then
                                    If EmptyString(oInvoice.CustomerNo) Then
                                        'OK
                                    Else
                                        bAllInvoicesMatchedOnMalgruppe = False
                                        Exit For
                                    End If
                                Else
                                    bAllInvoicesMatchedOnMalgruppe = False
                                    Exit For
                                End If
                            End If
                        Next oInvoice
                        'End If
                        If bAllInvoicesMatchedOnMalgruppe Then
                            oPayment.MATCH_Reset()
                            oPayment.MATCH_Undo()
                            For Each oInvoice In oPayment.Invoices
                                If oInvoice.MATCH_Final Then
                                    oInvoice.MATCH_Matched = False
                                End If
                            Next oInvoice
                        Else
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

    End Function
    Public Function TreatVismaCollectors_Inkasso(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef iERPArrayIndex As Integer, ByRef sSpecial As String) As Boolean
        Dim aQueryArray(,,) As String
        Dim sSQLToUseInTheFunction As String
        Dim sTemp As String = String.Empty
        Dim aClients() As String = Nothing
        Dim iCounter As Integer = -1
        Dim iCounter2 As Integer = -1
        Dim iInvoiceCounter As Integer
        Dim iStart As Integer = 0
        Dim iStop As Integer = 0
        Dim bAtLeastOneInvoiceUnmatched As Boolean = False
        Dim bAllInvoicesUnmatched As Boolean = True

        Try

            'Build an array consisting of the ConnectionStrings and Query agianst the ERP-system.
            aQueryArray = GetERPDBInfo(1, 3) 'TODO: Hardkoded CompanyNo

            For iCounter = 1 To UBound(aQueryArray, 3)
                'Check if we have a query stated
                If Len(aQueryArray(iERPArrayIndex, 0, iCounter)) > 0 Then

                    'Find the type of matching
                    ' 11=CallAFunctionToRunSQL
                    Select Case aQueryArray(iERPArrayIndex, 1, iCounter)

                        Case "11"
                            If aQueryArray(iERPArrayIndex, 2, iCounter) = "VISMAC_INKASSO" Then
                                sSQLToUseInTheFunction = aQueryArray(iERPArrayIndex, 0, iCounter)
                            End If

                        Case Else

                    End Select

                End If
            Next iCounter


            If Not EmptyString(sSQLToUseInTheFunction) Then
                'This is used to undo posting agains invoices marked as 'Sent for collection/Inkasso) for specific clients
                'The clients are stated in the SQL like this %171, 262%
                sTemp = sSQLToUseInTheFunction

                iStart = InStr(sTemp, "%", CompareMethod.Text) ' first %
                iStart = sTemp.IndexOf("%") 'First %, zero-based
                iStop = InStr(iStart + 1, sTemp, "%", CompareMethod.Text) ' last %
                iStop = sTemp.IndexOf("%", iStart + 1)

                If iStart > -1 And iStop > 0 Then
                    sTemp = sSQLToUseInTheFunction.Substring(iStart + 1, iStop - iStart - 1)
                End If

                iCounter2 = sTemp.Length - Replace(sTemp, ",", String.Empty).Length + 1 'Number of clients

                If iCounter2 > -1 Then

                    ReDim aClients(iCounter2 - 1)
                    For iCounter = 0 To iCounter2 - 1
                        aClients(iCounter) = xDelim(sTemp, ",", iCounter + 1).Trim
                    Next iCounter

                    For Each oBabelFile In oBabelFiles
                        For Each oBatch In oBabelFile.Batches
                            For Each oPayment In oBatch.Payments
                                bAtLeastOneInvoiceUnmatched = False
                                bAllInvoicesUnmatched = True

                                For iInvoiceCounter = oPayment.Invoices.Count To 1 Step -1 'Each oInvoice In oPayment.Invoices
                                    oInvoice = oPayment.Invoices.Item(iInvoiceCounter)
                                    If oInvoice.MATCH_Final Then
                                        If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                                            'Undo some specific postings
                                            If oInvoice.MyField = "7" Or oInvoice.MyField = "8" Then 'Marked as Collection/Inkasso
                                                'Just do this for some specific clients
                                                If Array.IndexOf(aClients, oPayment.VB_ClientNo) > -1 Then

                                                    'If oPayment.VB_ClientNo = "901" Or oPayment.VB_ClientNo = "171" Or oPayment.VB_ClientNo = "262" Or oPayment.VB_ClientNo = "516" Then
                                                    bAtLeastOneInvoiceUnmatched = True

                                                    If oInvoice.MATCH_Original Then
                                                        'Unmatch the payment
                                                        oInvoice.MATCH_ID = String.Empty
                                                        oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.OpenInvoice
                                                        If oInvoice.MATCH_Original = False Then
                                                            oInvoice.InvoiceNo = String.Empty
                                                        End If
                                                        oInvoice.CustomerNo = String.Empty
                                                        oInvoice.MyField = String.Empty
                                                        oInvoice.MATCH_ClientNo = ""
                                                    Else
                                                        'Remove the payment
                                                        oPayment.Invoices.Remove(iInvoiceCounter)
                                                    End If
                                                Else
                                                    bAllInvoicesUnmatched = False
                                                End If
                                            Else
                                                bAllInvoicesUnmatched = False
                                            End If
                                        Else
                                            bAllInvoicesUnmatched = False
                                        End If
                                    End If
                                Next iInvoiceCounter
                                If bAtLeastOneInvoiceUnmatched Then
                                    If bAllInvoicesUnmatched Then
                                        oPayment.MATCH_Reset()
                                        oPayment.MATCH_Undo()
                                    Else
                                        oPayment.MATCH_Matched = vbBabel.BabelFiles.MatchStatus.PartlyMatched
                                    End If
                                End If
                            Next oPayment
                        Next oBatch
                    Next oBabelFile
                End If
            End If

            Return True

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

    End Function
    Public Function TreatFrankMohnReturn(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' For SEBISO20022 for Frank MOhn
        ' Recalculate exchangerate, and move to oLocalExchrate
        Dim bAllInvoicesHaveKID As Boolean = True

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.MON_AccountExchRate > 0 Then
                            ' FM needs corrected rate, x 100
                            ' 07.12.2015 BUT only for SEK, DKK, JPY
                            If oPayment.MON_InvoiceCurrency = "DKK" Or oPayment.MON_InvoiceCurrency = "SEK" Or oPayment.MON_InvoiceCurrency = "JPY" Or oPayment.MON_InvoiceCurrency = "CHF" Then
                                oPayment.MON_AccountExchRate = oPayment.MON_AccountExchRate * 100
                            End If

                            ' FM needs currencyrate in KursMotNOK in Telepay returnfile
                            oPayment.MON_LocalExchRate = oPayment.MON_AccountExchRate
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Return True

    End Function
    Public Function SplitCamt_054() As Boolean

        Dim XMLDocRead As System.Xml.XmlDocument
        Dim XMLDocWrite As System.Xml.XmlDocument
        Dim nsMgr As System.Xml.XmlNamespaceManager
        Dim root As System.Xml.XmlNode
        Dim nodeList As System.Xml.XmlNodeList
        Dim node As System.Xml.XmlNode
        Dim nodeTemp As System.Xml.XmlNode
        Dim sFilenameIn As String = "C:\Slett\Slett\SKULD_Babelbank_camt054_R2 NOKUSD.xml"
        Dim aAccounts As String()
        Dim sAccount As String
        Dim iCounter As Integer = 1
        Dim bSaveTheXML As Boolean

        Try

            ReDim aAccounts(2)
            aAccounts(0) = "97501510142"
            aAccounts(1) = "TULL"
            aAccounts(2) = "97501510126"
            sFilenameIn = "C:\Slett\Gjensidige\20150326\ZZ_GF_NO_payment.15032516322265.xml"

            XMLDocRead = New System.Xml.XmlDocument

            'XMLDocRead.Load(sFilenameIn)

            For iCounter = 0 To aAccounts.GetUpperBound(0)
                bSaveTheXML = False
                XMLDocWrite = New System.Xml.XmlDocument
                XMLDocWrite.Load(sFilenameIn)

                nsMgr = New System.Xml.XmlNamespaceManager(XMLDocWrite.NameTable)
                nsMgr.AddNamespace("ISO", XMLDocWrite.DocumentElement.NamespaceURI)

                root = XMLDocWrite.DocumentElement

                nodeList = XMLDocWrite.SelectNodes("/ISO:Document/ISO:BkToCstmrDbtCdtNtfctn/ISO:Ntfctn", nsMgr)

                For Each node In nodeList
                    sAccount = GetInnerText(node, "ISO:Acct/ISO:Id/ISO:Othr/ISO:Id", nsMgr)
                    If sAccount = aAccounts(iCounter) Then
                        'Keep the node
                        bSaveTheXML = True
                    Else
                        'Delete the node
                        nodeTemp = node.ParentNode.RemoveChild(node)
                    End If
                Next node

                If bSaveTheXML Then
                    XMLDocWrite.Save("C:\Slett\Slett\" & aAccounts(iCounter) & ".xml")
                End If

                XMLDocWrite = Nothing
            Next iCounter

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

    End Function

    '    Imports System
    'Imports System.Windows.Forms
    'Imports Progress.Open4GL.Proxy

    'Namespace AppServer

    'Module CallAppserverK90BabelbankproPro
    Friend Function getSakerForBabelbank(ByVal sSearchString As String, ByVal sAppserverConnectionString As String) As Integer
        'BRUK! Verdi for appserver UTV : "AppServerDC://ulven.kredinor.no:51601"
        'Verdi for appserver TEST: "AppServerDC://aixutv.kredinor.no:51601"
        'Verdi for appserver PROD: "AppServerDC://kredinor.no:51701"

        'Dim AppserverConnectionString As String = "AppServerDC://ulven.kredinor.no:51601"
        Dim id As Int16 = 0
        Dim sErrorNo As String = "0"

        Try
            'Using connection As New Global.Progress.Open4GL.Proxy.Connection(sAppserverConnectionString, "", "", "")
            '    connection.SessionModel = 1
            '    sErrorNo = "100"

            '    Using openAo As New Global.Progress.Open4GL.Proxy.OpenAppObject(connection, "")
            '        '                    Dim verdi As String = "Saksnr=153471417"
            '        Dim parms As New Global.Progress.Open4GL.Proxy.[ParamArray](2)
            '        'Dim parms As New [ParamArray](2)
            '        '                    parms.AddCharacter(0, verdi, Global.Progress.Open4GL.Proxy.ParamArrayMode.INPUT)
            '        sErrorNo = "200"
            '        parms.AddCharacter(0, sSearchString, Global.Progress.Open4GL.Proxy.ParamArrayMode.INPUT)
            '        'MsgBox("Lagt inn INPUT parameter: " & sSearchString)
            '        parms.AddInteger(1, id, Global.Progress.Open4GL.Proxy.ParamArrayMode.OUTPUT)

            '        sErrorNo = "300"
            '        openAo.RunProc("babelbankpro_get.p", parms)
            '        sErrorNo = "400"

            '        id = Convert.ToInt16(parms.GetOutputParameter(1))
            '        sErrorNo = "500"
            '    End Using
            '    connection.ReleaseConnection()
            'End Using
            'MessageBox.Show("id=" + id.ToString)
        Catch ex As Exception

            Throw New Exception("Function: getSakerForBabelBank" & vbCrLf & sErrorNo & ex.Message)
            'MessageBox.Show("Error :" + ex.Message)
        End Try

        'MsgBox("I slutten av ny rutine.")
        Return id

    End Function
    Public Function Treat_GjensidigeISO20022(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal sFileSetupID As String) As Boolean
        ' Redo bankcharges to SHA for payments in SEPA countries
        ' ----------------------------------------------------------------------------------

        Try

            For Each oBabelFile In oBabelFiles
                'I have an assumption that if the imported file has the property
                ' FromAccountingSystem = True, it has to be an outgoing paymentfile from Gjensidige to the bank
                If oBabelFile.VB_Profile.FileSetups(CDbl(sFileSetupID)).FromAccountingSystem Then
                    For Each oBatch In oBabelFile.Batches
                        For Each oPayment In oBatch.Payments
                            'SHAR for all currencies within EU_EAA area
                            If IsEU_EEAPayment(oPayment, , False) Then
                                oPayment.MON_ChargeMeAbroad = False
                                oPayment.MON_ChargeMeDomestic = True
                            End If

                        Next
                    Next
                End If
            Next

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_GjensidigeISO20022 = True

    End Function
    Public Function Treat_NRX_Camt054(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        'Don't export OCR-payments
        'Just export to account 8200.06.10174
        Dim bRetValue As Boolean

        Try

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If oPayment.I_Account = "82000610174" Then
                            If IsOCR(oPayment.PayCode) Then
                                oPayment.Exported = True
                            End If
                        Else
                            oPayment.Exported = True
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_NRX_Camt054 = bRetValue

    End Function
    Public Function Treat_GjensidigeGauda(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        ' 28.03.2019 - added this function to redo from : to ; in Ref_Own
        Dim bExchangeDone As Boolean
        Dim bContinue As Boolean
        Dim sFreeText As String
        Dim sKID As String
        Dim iFreeTextCounter As Integer
        Dim sContractNo As String
        Dim bFoundContractNo As Boolean

        Try

            For Each oBabelFile In oBabelFiles
                oBabelFile.IDENT_Sender = ""
                For Each oBatch In oBabelFile.Batches
                    sContractNo = ""
                    bFoundContractNo = False
                    For Each oFilesetup In oBatch.VB_Profile.FileSetups
                        For Each oClient In oFilesetup.Clients
                            For Each oaccount In oClient.Accounts
                                If oaccount.Account = oBatch.Payments.Item(1).I_Account Then
                                    sContractNo = oaccount.ContractNo
                                    If Len(sContractNo) > 0 Then
                                        bFoundContractNo = True

                                        Exit For
                                    End If
                                End If
                                If bFoundContractNo Then
                                    Exit For
                                End If
                            Next oaccount
                            If bFoundContractNo Then
                                Exit For
                            End If
                        Next oClient

                        If bFoundContractNo Then
                            Exit For
                        End If
                    Next oFilesetup
                    If bFoundContractNo Then
                        oBatch.REF_Own = sContractNo
                    Else
                        Throw New Exception("35023: Ingen avtale-ID oppgitt for konto: oBatch.Payments.Item(1).I_Account." & vbCrLf & "Kan ikke lage korrekt OCR-fil." & vbLf & "Oppgi dette under valget klienter i BabelBank.")
                    End If
                    For Each oPayment In oBatch.Payments
                        bContinue = False
                        If oPayment.Invoices.Count = 1 Then
                            For Each oInvoice In oPayment.Invoices
                                sFreeText = ""
                                For Each oFreeText In oInvoice.Freetexts
                                    If oFreeText.Text.Length > 6 Then
                                        If oFreeText.Text.Substring(0, 4) = "KID:" Then
                                            'Not interesteing, added by BB
                                        ElseIf oFreeText.Text.Substring(0, 6).ToUpper = "BEL�P:" Then
                                            'Not interesteing, added by BB
                                        ElseIf oFreeText.Text.Substring(0, 7).ToUpper = "AMOUNT:" Then
                                            'Not interesteing, added by BB
                                        Else
                                            sFreeText = sFreeText & oFreeText.Text
                                        End If
                                    Else
                                        sFreeText = sFreeText & oFreeText.Text
                                    End If
                                Next oFreeText
                            Next oInvoice
                        End If
                        'Try to find KID
                        bContinue = False
                        If Not EmptyString(sFreeText) Then
                            If sFreeText.Length > 25 Then
                                If sFreeText.Substring(0, 8).ToUpper = "V�R REF:" Then
                                    sKID = sFreeText.Substring(sFreeText.Length - 12, 12) 'Last 12 positions
                                    sKID = "00" & sKID
                                    If vbIsNumeric(sKID, "0123456789") Then
                                        'If IsModulus10(sKID) Then
                                        sKID = sKID & AddCDV10digit(sKID)
                                        bContinue = True
                                        'End If
                                    End If
                                End If
                            End If
                        End If
                        If bContinue Then
                            oPayment.PayCode = "510"
                            oPayment.Invoices.Item(1).Unique_Id = sKID
                            For Each oInvoice In oPayment.Invoices
                                For iFreeTextCounter = oInvoice.Freetexts.Count To 1 Step -1
                                    oInvoice.Freetexts.Remove(iFreeTextCounter)
                                Next iFreeTextCounter
                            Next oInvoice
                            oPayment.REF_Own = RemoveCharacters(oPayment.REF_Own, "abcdefghijklmnopqrstuvwxyz���ABCDEFGHIJKLMNOPQRSTUVWXYZ���,. :()-\/?*")
                            oPayment.REF_Bank1 = oPayment.REF_Own
                        Else
                            oPayment.PayCode = 601
                            oPayment.Exported = True
                            For Each oInvoice In oPayment.Invoices
                                oInvoice.Exported = True
                            Next oInvoice
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

            Return True

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

    End Function
    Public Function Treat_HelseVestTeller(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean
        'Don't export OCR-payments
        'Just export to account 8200.06.10174
        Dim bRetValue As Boolean
        Dim sTmp As String

        Try

            bRetValue = True

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        oPayment.PayCode = "629"
                        ' test to get other SubFmlyCd
                        oPayment.PayCode = "610"
                        For Each oInvoice In oPayment.Invoices
                            sTmp = oInvoice.Unique_Id
                        Next oInvoice
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_HelseVestTeller = bRetValue
    End Function
    Public Function Treat_FreudenbergSalary(ByVal oBabelFiles As vbBabel.BabelFiles, ByVal oProfile As vbBabel.Profile) As Boolean

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    For Each oPayment In oBatch.Payments
                        If Not EmptyString(oPayment.E_Account) Then
                            If oPayment.E_Account.Trim.Length = 11 Then
                                oPayment.E_Account = CalculateIBAN(oPayment.E_Account.Trim, "NO")
                            End If
                        End If
                    Next oPayment
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_FreudenbergSalary = True
    End Function
    Public Function Treat_ForskningraadetStructured(ByRef oBabelFiles As vbBabel.BabelFiles, ByRef oProfile As vbBabel.Profile) As Boolean
        'The function is used to convert structured transactions to KID-transactions
        '  if all invoices within a batch are either KID or Invoice/creditnote.

        Dim bConvert As Boolean

        Try

            For Each oBabelFile In oBabelFiles
                For Each oBatch In oBabelFile.Batches
                    bConvert = True
                    For Each oPayment In oBatch.Payments
                        If oPayment.PayCode = "629" Then
                            For Each oInvoice In oPayment.Invoices
                                If Not EmptyString(oInvoice.Unique_Id) Then
                                    If oInvoice.Unique_Id.Length = 10 Then
                                        If IsModulus10(oInvoice.Unique_Id) Then
                                            'OK
                                        Else
                                            bConvert = False
                                        End If
                                    Else
                                        bConvert = False
                                    End If
                                ElseIf Not EmptyString(oInvoice.InvoiceNo) Then
                                    'Validate the invoiceno
                                    If oInvoice.InvoiceNo.Length = 10 Then
                                        'It may be a KID
                                        If IsModulus10(oInvoice.InvoiceNo) Then
                                            'OK
                                        Else
                                            bConvert = False
                                        End If
                                    ElseIf oInvoice.InvoiceNo.Length = 9 Then
                                        If oInvoice.InvoiceNo.Substring(0, 1) = "8" Then
                                            'OK - An invoice
                                        Else
                                            bConvert = False
                                        End If
                                    Else
                                        bConvert = False
                                    End If

                                Else
                                    bConvert = False
                                End If
                            Next oInvoice
                        Else
                            bConvert = False
                        End If

                        If Not bConvert Then Exit For

                    Next oPayment

                    If bConvert Then
                        For Each oPayment In oBatch.Payments
                            oPayment.PayCode = "510"
                            For Each oInvoice In oPayment.Invoices
                                If Not EmptyString(oInvoice.Unique_Id) Then
                                    'OK
                                ElseIf Not EmptyString(oInvoice.InvoiceNo) Then
                                    If oInvoice.InvoiceNo.Length = 10 Then
                                        oInvoice.Unique_Id = oInvoice.InvoiceNo
                                        oInvoice.InvoiceNo = ""
                                    ElseIf oInvoice.InvoiceNo.Length = 9 Then
                                        oInvoice.Unique_Id = oInvoice.InvoiceNo & AddCDV10digit(oInvoice.InvoiceNo)
                                        oInvoice.InvoiceNo = ""
                                    End If
                                Else
                                    bConvert = False
                                End If
                            Next oInvoice

                        Next oPayment

                    End If
                Next oBatch
            Next oBabelFile

        Catch ex As Exception

            If Not oBabelFile Is Nothing Then
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex, oPayment, "", oBabelFile.FilenameIn) ' - Testing Try ... Catch
            Else
                Throw New vbBabel.Payment.PaymentException(ex.Message, ex) ' - Testing Try ... Catch
            End If

        End Try

        Treat_ForskningraadetStructured = True

    End Function
    Friend Function Sergel_CheckMatching(ByRef oLocalPayment As vbBabel.Payment) As Boolean
        Dim sClientNo As String = "" 'Kreditornr.
        Dim bReturnValue As Boolean = True

        For Each oInvoice In oLocalPayment.Invoices
            If oInvoice.MATCH_Final Then
                If oInvoice.MATCH_MatchType = vbBabel.BabelFiles.MatchType.MatchedOnInvoice Then
                    If Not EmptyString(oInvoice.MATCH_ClientNo) Then
                        If sClientNo = "" Then
                            sClientNo = oInvoice.MATCH_ClientNo.Trim
                        Else
                            If sClientNo = oInvoice.MATCH_ClientNo.Trim Then
                                'OK - identical kreditor
                            Else
                                'Matched agianst several kreditorer, return False
                                bReturnValue = False
                                Exit For
                            End If
                        End If
                    End If
                End If
            End If
        Next oInvoice

        Return bReturnValue

    End Function

End Module
