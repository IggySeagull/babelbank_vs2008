Option Strict Off
Option Explicit On
Friend Class frmVisma_About
    Inherits System.Windows.Forms.Form
    Private Sub frmVisma_About_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim oVismaDal As vbBabel.DAL = Nothing
        Dim sMySQL As Object
        Dim sUser As Object
        Dim sLogPath As String
        Dim oVismaCon As vbBabel.DAL
        Dim sSystemDatabase As String
        Dim sBBDatabase As String
        Dim sErrorString As String
        Dim sUID As String
        Dim sPWD As String
        Dim sInstance As String
        Dim oVismaCommandLine As New VismaCommandLine
        Dim sDBVersion As String
        Dim sTmp As String = ""

        sSystemDatabase = oVismaCommandLine.SystemDatabase
        sBBDatabase = oVismaCommandLine.BBDatabase
        sUser = oVismaCommandLine.User
        sUID = oVismaCommandLine.UID
        sPWD = oVismaCommandLine.PWD
        sInstance = oVismaCommandLine.Instance
        oVismaCommandLine = Nothing

        oVismaDal = ConnectToVismaBusinessDB(False, sErrorString, True)
        ' Find logpath from EftInf or Windows programdata
        sLogPath = Visma_RetrieveProgramDataBabelBank(oVismaDal, sSystemDatabase) & "\Log.txt"

        FormLRSCaptions(Me)
        FormVismaStyle(Me)

        ' Fill in;
        'o BabelBank
        'o   BabelBank versjonsnr
        'o   Database version number
        'o   BabelBank installert i
        'o   BabelBank logfile

        ' Find database versionNo
        sMySQL = "SELECT bbDBVersion FROM " & sBBDatabase & ".dbo.bbSetup"
        oVismaDal.SQL = sMySQL

        If oVismaDal.Reader_Execute() Then

            oVismaDal.Reader_ReadRecord()

            sErrorString = "frmVisma_About, find bbDBVersion."
            sDBVersion = oVismaDal.Reader_GetString("bbDBVersion")

            sTmp = "Visma Payment (BabelBank) from Visma / Visual Banking" & vbCrLf
            sTmp = sTmp & LRS(60045) & " " & Replace(System.Diagnostics.FileVersionInfo.GetVersionInfo(My.Application.Info.DirectoryPath & "\BabelBank.exe").ProductVersion, ".0.", ".") & vbCrLf
            sTmp = sTmp & LRS(35284) & " " & sDBVersion & vbCrLf
            sTmp = sTmp & "Visma Payment (BabelBank) " & My.Application.Info.DirectoryPath & vbCrLf

            sTmp = sTmp & LRS(60364) & " " & sLogPath & vbCrLf & vbCrLf
            Me.txtAbout.Text = sTmp
            Me.txtAbout.Select(Me.txtAbout.Text.Length, 0)
        End If
    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Hide()
    End Sub
End Class