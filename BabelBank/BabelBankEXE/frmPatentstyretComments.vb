﻿Imports System.Windows.Forms

Public Class frmPatentstyretComments
    Dim bInitialPresentation As Boolean = True
    Dim aExternalTexts As String()
    Dim aInternalTexts As String()
    Dim bAddedFromListbox As Boolean = False
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub
    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
    Public Sub InitialMode(ByVal b As Boolean)
        bInitialPresentation = b
    End Sub
    Private Sub frmPatentstyretComments_Load()
        'bInitialPresentation = True
    End Sub
    Public Sub ExternalTexts(ByVal a As String())
        aExternalTexts = a
    End Sub
    Public Sub InternalTexts(ByVal a As String())
        aInternalTexts = a
    End Sub
    Private Sub cmbExternal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbExternal.SelectedIndexChanged
        ' fill in the standardtext at top of txtExternal
        If Not bInitialPresentation Then
            'Me.txtExternal.Text = Environ("Username") & " " & VB6.Format(Date.Now, "yyyy.mm.dd") & "-" & VB6.Format(Date.Now, "hh:mm:ss") & vbCrLf & cmbExternal.SelectedItem & vbCrLf & Me.txtExternal.Text
            bAddedFromListbox = True  ' if false, will loop constantly in _textChanged
            Me.txtExternal.Text = Environ("Username") & " " & VB6.Format(Date.Now, "yyyy.mm.dd") & "-" & VB6.Format(Date.Now, "hh:mm:ss") & vbCrLf & aExternalTexts(Me.cmbExternal.SelectedIndex) & vbCrLf & Me.txtExternal.Text

        End If
    End Sub
    Private Sub cmbInternal_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbInternal.SelectedIndexChanged
        ' fill in the standardtext at top of txtInternal
        If Not bInitialPresentation Then
            'Me.txtInternal.Text = Environ("Username") & " " & VB6.Format(Date.Now, "yyyy.mm.dd") & "-" & VB6.Format(Date.Now, "hh:mm:ss") & vbCrLf & cmbInternal.SelectedItem & vbCrLf & Me.txtInternal.Text
            bAddedFromListbox = True  ' if false, will loop constantly in _textChanged
            Me.txtInternal.Text = Environ("Username") & " " & VB6.Format(Date.Now, "yyyy.mm.dd") & "-" & VB6.Format(Date.Now, "hh:mm:ss") & vbCrLf & aInternalTexts(Me.cmbInternal.SelectedIndex) & vbCrLf & Me.txtInternal.Text
            'Me.txtInternal.Text = aInternalTexts(Me.cmbInternal.SelectedIndex) & vbCrLf & Me.txtInternal.Text
        End If
    End Sub

    Private Sub txtExternal_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtExternal.Leave
        bInitialPresentation = False
    End Sub
    'Private Sub txtExternal_MouseClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtExternal.MouseClick
    '    bInitialPresentation = False
    'End Sub
    Private Sub txtExternal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExternal.TextChanged
        ' add textline at top of txtBox if cursor is in first position
        Dim lTopLength As Long = 0
        Dim sTxt As String = Me.txtExternal.Text

        If Not bAddedFromListbox Then
            If Me.txtExternal.SelectionStart < 2 And Not bInitialPresentation Then
                ' add a line at top;
                bInitialPresentation = True  ' if false, will loop constantly
                Me.txtExternal.Text = Environ("Username") & " " & VB6.Format(Date.Now, "yyyy.mm.dd") & "-" & VB6.Format(Date.Now, "hh:mm:ss") & vbCrLf & sTxt
                lTopLength = Len(Environ("Username") & " " & VB6.Format(Date.Now, "yyyy.mm.dd") & "-" & VB6.Format(Date.Now, "hh:mm:ss") & vbCrLf)
                ' set cursor after newly inserted line at top
                Me.txtExternal.SelectionStart = lTopLength + 1
            End If
            'bInitialPresentation = False
        End If
        bAddedFromListbox = False
    End Sub

    Private Sub txtInternal_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInternal.Leave
        bInitialPresentation = False
    End Sub
    'Private Sub txtInternal_MouseClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInternal.MouseClick
    '    bInitialPresentation = False
    'End Sub
    Private Sub txtInternal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInternal.TextChanged
        ' add textline at top of txtBox if cursor is in first position
        Dim lTopLength As Long = 0
        Dim sTxt As String = Me.txtInternal.Text

        If Not bAddedFromListbox Then
            If Me.txtInternal.SelectionStart < 2 And Not bInitialPresentation Then
                ' add a line at top;
                bInitialPresentation = True
                Me.txtInternal.Text = Environ("Username") & " " & VB6.Format(Date.Now, "yyyy.mm.dd") & "-" & VB6.Format(Date.Now, "hh:mm:ss") & vbCrLf & sTxt
                lTopLength = Len(Environ("Username") & " " & VB6.Format(Date.Now, "yyyy.mm.dd") & "-" & VB6.Format(Date.Now, "hh:mm:ss") & vbCrLf)
                ' set cursor after newly inserted line at top
                Me.txtInternal.SelectionStart = lTopLength + 1
            End If
        End If
        bAddedFromListbox = False
    End Sub

End Class
