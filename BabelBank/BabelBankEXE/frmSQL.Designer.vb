﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSQL
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtQuery = New System.Windows.Forms.TextBox
        Me.cmdSELECT = New System.Windows.Forms.Button
        Me.lblHeading = New System.Windows.Forms.Label
        Me.cmdClose = New System.Windows.Forms.Button
        Me.cmdOthers = New System.Windows.Forms.Button
        Me.gridResult = New System.Windows.Forms.DataGridView
        CType(Me.gridResult, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtQuery
        '
        Me.txtQuery.Location = New System.Drawing.Point(202, 125)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(859, 107)
        Me.txtQuery.TabIndex = 28
        '
        'cmdSELECT
        '
        Me.cmdSELECT.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdSELECT.Location = New System.Drawing.Point(629, 401)
        Me.cmdSELECT.Name = "cmdSELECT"
        Me.cmdSELECT.Size = New System.Drawing.Size(110, 23)
        Me.cmdSELECT.TabIndex = 27
        Me.cmdSELECT.Text = "SELECT"
        '
        'lblHeading
        '
        Me.lblHeading.AutoSize = True
        Me.lblHeading.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeading.Location = New System.Drawing.Point(207, 68)
        Me.lblHeading.Name = "lblHeading"
        Me.lblHeading.Size = New System.Drawing.Size(145, 16)
        Me.lblHeading.TabIndex = 26
        Me.lblHeading.Text = "15038-Legg inn SQL"
        '
        'cmdClose
        '
        Me.cmdClose.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdClose.Location = New System.Drawing.Point(990, 401)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(67, 23)
        Me.cmdClose.TabIndex = 25
        Me.cmdClose.Text = "50115 - Close"
        '
        'cmdOthers
        '
        Me.cmdOthers.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdOthers.Location = New System.Drawing.Point(743, 401)
        Me.cmdOthers.Name = "cmdOthers"
        Me.cmdOthers.Size = New System.Drawing.Size(110, 23)
        Me.cmdOthers.TabIndex = 24
        Me.cmdOthers.Text = "50114-Others"
        '
        'gridResult
        '
        Me.gridResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridResult.Location = New System.Drawing.Point(202, 262)
        Me.gridResult.Name = "gridResult"
        Me.gridResult.Size = New System.Drawing.Size(859, 122)
        Me.gridResult.TabIndex = 48
        '
        'frmSQL
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1093, 440)
        Me.Controls.Add(Me.gridResult)
        Me.Controls.Add(Me.txtQuery)
        Me.Controls.Add(Me.cmdSELECT)
        Me.Controls.Add(Me.lblHeading)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.cmdOthers)
        Me.Name = "frmSQL"
        Me.Text = "SQL"
        CType(Me.gridResult, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtQuery As System.Windows.Forms.TextBox
    Friend WithEvents cmdSELECT As System.Windows.Forms.Button
    Friend WithEvents lblHeading As System.Windows.Forms.Label
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents cmdOthers As System.Windows.Forms.Button
    Friend WithEvents gridResult As System.Windows.Forms.DataGridView
End Class
