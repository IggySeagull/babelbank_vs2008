<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmFileExists
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdTerminate As System.Windows.Forms.Button
	Public WithEvents cmdOverwrite As System.Windows.Forms.Button
	Public WithEvents cmdAppend As System.Windows.Forms.Button
	Public WithEvents lblResult As System.Windows.Forms.Label
	Public WithEvents lblText As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmFileExists))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.cmdTerminate = New System.Windows.Forms.Button
		Me.cmdOverwrite = New System.Windows.Forms.Button
		Me.cmdAppend = New System.Windows.Forms.Button
		Me.lblResult = New System.Windows.Forms.Label
		Me.lblText = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Filename exists"
		Me.ClientSize = New System.Drawing.Size(323, 132)
		Me.Location = New System.Drawing.Point(3, 22)
		Me.ControlBox = False
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmFileExists"
		Me.cmdTerminate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdTerminate.Text = "Terminate"
		Me.cmdTerminate.Size = New System.Drawing.Size(80, 25)
		Me.cmdTerminate.Location = New System.Drawing.Point(224, 96)
		Me.cmdTerminate.TabIndex = 0
		Me.cmdTerminate.BackColor = System.Drawing.SystemColors.Control
		Me.cmdTerminate.CausesValidation = True
		Me.cmdTerminate.Enabled = True
		Me.cmdTerminate.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdTerminate.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdTerminate.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdTerminate.TabStop = True
		Me.cmdTerminate.Name = "cmdTerminate"
		Me.cmdOverwrite.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOverwrite.Text = "OverWrite"
		Me.cmdOverwrite.Size = New System.Drawing.Size(80, 25)
		Me.cmdOverwrite.Location = New System.Drawing.Point(120, 96)
		Me.cmdOverwrite.TabIndex = 2
		Me.cmdOverwrite.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOverwrite.CausesValidation = True
		Me.cmdOverwrite.Enabled = True
		Me.cmdOverwrite.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOverwrite.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOverwrite.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOverwrite.TabStop = True
		Me.cmdOverwrite.Name = "cmdOverwrite"
		Me.cmdAppend.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdAppend.Text = "Append"
		Me.cmdAppend.Size = New System.Drawing.Size(80, 25)
		Me.cmdAppend.Location = New System.Drawing.Point(16, 96)
		Me.cmdAppend.TabIndex = 1
		Me.cmdAppend.BackColor = System.Drawing.SystemColors.Control
		Me.cmdAppend.CausesValidation = True
		Me.cmdAppend.Enabled = True
		Me.cmdAppend.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdAppend.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdAppend.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdAppend.TabStop = True
		Me.cmdAppend.Name = "cmdAppend"
		Me.lblResult.Size = New System.Drawing.Size(25, 17)
		Me.lblResult.Location = New System.Drawing.Point(288, 72)
		Me.lblResult.TabIndex = 4
		Me.lblResult.Visible = False
		Me.lblResult.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblResult.BackColor = System.Drawing.SystemColors.Control
		Me.lblResult.Enabled = True
		Me.lblResult.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblResult.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblResult.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblResult.UseMnemonic = True
		Me.lblResult.AutoSize = False
		Me.lblResult.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblResult.Name = "lblResult"
		Me.lblText.Size = New System.Drawing.Size(296, 65)
		Me.lblText.Location = New System.Drawing.Point(16, 16)
		Me.lblText.TabIndex = 3
		Me.lblText.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblText.BackColor = System.Drawing.SystemColors.Control
		Me.lblText.Enabled = True
		Me.lblText.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblText.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblText.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblText.UseMnemonic = True
		Me.lblText.Visible = True
		Me.lblText.AutoSize = False
		Me.lblText.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblText.Name = "lblText"
		Me.Controls.Add(cmdTerminate)
		Me.Controls.Add(cmdOverwrite)
		Me.Controls.Add(cmdAppend)
		Me.Controls.Add(lblResult)
		Me.Controls.Add(lblText)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
