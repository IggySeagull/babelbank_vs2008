<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmLicenseCode
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtLicenseCode4 As System.Windows.Forms.TextBox
	Public WithEvents txtLicenseCode3 As System.Windows.Forms.TextBox
	Public WithEvents txtLicenseCode2 As System.Windows.Forms.TextBox
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents txtLicenseCode1 As System.Windows.Forms.TextBox
	Public WithEvents txtName As System.Windows.Forms.TextBox
	Public WithEvents txtNumber As System.Windows.Forms.TextBox
	Public WithEvents lblLicenseCode As System.Windows.Forms.Label
	Public WithEvents lblLicenseName As System.Windows.Forms.Label
	Public WithEvents lblNumber As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLicenseCode))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtLicenseCode4 = New System.Windows.Forms.TextBox
		Me.txtLicenseCode3 = New System.Windows.Forms.TextBox
		Me.txtLicenseCode2 = New System.Windows.Forms.TextBox
		Me.cmdCancel = New System.Windows.Forms.Button
		Me.cmdOK = New System.Windows.Forms.Button
		Me.txtLicenseCode1 = New System.Windows.Forms.TextBox
		Me.txtName = New System.Windows.Forms.TextBox
		Me.txtNumber = New System.Windows.Forms.TextBox
		Me.lblLicenseCode = New System.Windows.Forms.Label
		Me.lblLicenseName = New System.Windows.Forms.Label
		Me.lblNumber = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.Text = "LicenseCode"
		Me.ClientSize = New System.Drawing.Size(443, 262)
		Me.Location = New System.Drawing.Point(4, 23)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmLicenseCode"
		Me.txtLicenseCode4.AutoSize = False
		Me.txtLicenseCode4.Size = New System.Drawing.Size(67, 19)
		Me.txtLicenseCode4.Location = New System.Drawing.Point(348, 163)
		Me.txtLicenseCode4.Maxlength = 7
		Me.txtLicenseCode4.TabIndex = 6
		Me.txtLicenseCode4.AcceptsReturn = True
		Me.txtLicenseCode4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLicenseCode4.BackColor = System.Drawing.SystemColors.Window
		Me.txtLicenseCode4.CausesValidation = True
		Me.txtLicenseCode4.Enabled = True
		Me.txtLicenseCode4.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLicenseCode4.HideSelection = True
		Me.txtLicenseCode4.ReadOnly = False
		Me.txtLicenseCode4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLicenseCode4.MultiLine = False
		Me.txtLicenseCode4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLicenseCode4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLicenseCode4.TabStop = True
		Me.txtLicenseCode4.Visible = True
		Me.txtLicenseCode4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLicenseCode4.Name = "txtLicenseCode4"
		Me.txtLicenseCode3.AutoSize = False
		Me.txtLicenseCode3.Size = New System.Drawing.Size(67, 19)
		Me.txtLicenseCode3.Location = New System.Drawing.Point(279, 163)
		Me.txtLicenseCode3.Maxlength = 7
		Me.txtLicenseCode3.TabIndex = 5
		Me.txtLicenseCode3.AcceptsReturn = True
		Me.txtLicenseCode3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLicenseCode3.BackColor = System.Drawing.SystemColors.Window
		Me.txtLicenseCode3.CausesValidation = True
		Me.txtLicenseCode3.Enabled = True
		Me.txtLicenseCode3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLicenseCode3.HideSelection = True
		Me.txtLicenseCode3.ReadOnly = False
		Me.txtLicenseCode3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLicenseCode3.MultiLine = False
		Me.txtLicenseCode3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLicenseCode3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLicenseCode3.TabStop = True
		Me.txtLicenseCode3.Visible = True
		Me.txtLicenseCode3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLicenseCode3.Name = "txtLicenseCode3"
		Me.txtLicenseCode2.AutoSize = False
		Me.txtLicenseCode2.Size = New System.Drawing.Size(67, 19)
		Me.txtLicenseCode2.Location = New System.Drawing.Point(210, 163)
		Me.txtLicenseCode2.Maxlength = 7
		Me.txtLicenseCode2.TabIndex = 4
		Me.txtLicenseCode2.AcceptsReturn = True
		Me.txtLicenseCode2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLicenseCode2.BackColor = System.Drawing.SystemColors.Window
		Me.txtLicenseCode2.CausesValidation = True
		Me.txtLicenseCode2.Enabled = True
		Me.txtLicenseCode2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLicenseCode2.HideSelection = True
		Me.txtLicenseCode2.ReadOnly = False
		Me.txtLicenseCode2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLicenseCode2.MultiLine = False
		Me.txtLicenseCode2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLicenseCode2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLicenseCode2.TabStop = True
		Me.txtLicenseCode2.Visible = True
		Me.txtLicenseCode2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLicenseCode2.Name = "txtLicenseCode2"
		Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CancelButton = Me.cmdCancel
		Me.cmdCancel.Text = "&Cancel"
		Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
		Me.cmdCancel.Location = New System.Drawing.Point(224, 218)
		Me.cmdCancel.TabIndex = 8
		Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.cmdCancel.CausesValidation = True
		Me.cmdCancel.Enabled = True
		Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdCancel.TabStop = True
		Me.cmdCancel.Name = "cmdCancel"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "&OK"
		Me.cmdOK.Size = New System.Drawing.Size(81, 25)
		Me.cmdOK.Location = New System.Drawing.Point(138, 218)
		Me.cmdOK.TabIndex = 7
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.txtLicenseCode1.AutoSize = False
		Me.txtLicenseCode1.Size = New System.Drawing.Size(67, 22)
		Me.txtLicenseCode1.Location = New System.Drawing.Point(141, 163)
		Me.txtLicenseCode1.Maxlength = 7
		Me.txtLicenseCode1.TabIndex = 3
		Me.txtLicenseCode1.AcceptsReturn = True
		Me.txtLicenseCode1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLicenseCode1.BackColor = System.Drawing.SystemColors.Window
		Me.txtLicenseCode1.CausesValidation = True
		Me.txtLicenseCode1.Enabled = True
		Me.txtLicenseCode1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLicenseCode1.HideSelection = True
		Me.txtLicenseCode1.ReadOnly = False
		Me.txtLicenseCode1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLicenseCode1.MultiLine = False
		Me.txtLicenseCode1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLicenseCode1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLicenseCode1.TabStop = True
		Me.txtLicenseCode1.Visible = True
		Me.txtLicenseCode1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLicenseCode1.Name = "txtLicenseCode1"
		Me.txtName.AutoSize = False
		Me.txtName.Size = New System.Drawing.Size(224, 19)
		Me.txtName.Location = New System.Drawing.Point(140, 98)
		Me.txtName.TabIndex = 2
		Me.txtName.AcceptsReturn = True
		Me.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtName.BackColor = System.Drawing.SystemColors.Window
		Me.txtName.CausesValidation = True
		Me.txtName.Enabled = True
		Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtName.HideSelection = True
		Me.txtName.ReadOnly = False
		Me.txtName.Maxlength = 0
		Me.txtName.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtName.MultiLine = False
		Me.txtName.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtName.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtName.TabStop = True
		Me.txtName.Visible = True
		Me.txtName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtName.Name = "txtName"
		Me.txtNumber.AutoSize = False
		Me.txtNumber.Size = New System.Drawing.Size(52, 19)
		Me.txtNumber.Location = New System.Drawing.Point(140, 67)
		Me.txtNumber.Maxlength = 5
		Me.txtNumber.TabIndex = 1
		Me.txtNumber.AcceptsReturn = True
		Me.txtNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNumber.BackColor = System.Drawing.SystemColors.Window
		Me.txtNumber.CausesValidation = True
		Me.txtNumber.Enabled = True
		Me.txtNumber.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNumber.HideSelection = True
		Me.txtNumber.ReadOnly = False
		Me.txtNumber.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNumber.MultiLine = False
		Me.txtNumber.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNumber.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNumber.TabStop = True
		Me.txtNumber.Visible = True
		Me.txtNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtNumber.Name = "txtNumber"
		Me.lblLicenseCode.Text = "Licensecode"
		Me.lblLicenseCode.Size = New System.Drawing.Size(126, 19)
		Me.lblLicenseCode.Location = New System.Drawing.Point(8, 162)
		Me.lblLicenseCode.TabIndex = 10
		Me.lblLicenseCode.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLicenseCode.BackColor = System.Drawing.SystemColors.Control
		Me.lblLicenseCode.Enabled = True
		Me.lblLicenseCode.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblLicenseCode.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLicenseCode.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLicenseCode.UseMnemonic = True
		Me.lblLicenseCode.Visible = True
		Me.lblLicenseCode.AutoSize = False
		Me.lblLicenseCode.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLicenseCode.Name = "lblLicenseCode"
		Me.lblLicenseName.Text = "LicenseName"
		Me.lblLicenseName.Size = New System.Drawing.Size(126, 19)
		Me.lblLicenseName.Location = New System.Drawing.Point(8, 98)
		Me.lblLicenseName.TabIndex = 9
		Me.lblLicenseName.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLicenseName.BackColor = System.Drawing.SystemColors.Control
		Me.lblLicenseName.Enabled = True
		Me.lblLicenseName.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblLicenseName.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLicenseName.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLicenseName.UseMnemonic = True
		Me.lblLicenseName.Visible = True
		Me.lblLicenseName.AutoSize = False
		Me.lblLicenseName.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLicenseName.Name = "lblLicenseName"
		Me.lblNumber.Text = "Licensenumber"
		Me.lblNumber.Size = New System.Drawing.Size(126, 19)
		Me.lblNumber.Location = New System.Drawing.Point(8, 66)
		Me.lblNumber.TabIndex = 0
		Me.lblNumber.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblNumber.BackColor = System.Drawing.SystemColors.Control
		Me.lblNumber.Enabled = True
		Me.lblNumber.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblNumber.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblNumber.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblNumber.UseMnemonic = True
		Me.lblNumber.Visible = True
		Me.lblNumber.AutoSize = False
		Me.lblNumber.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblNumber.Name = "lblNumber"
		Me.Controls.Add(txtLicenseCode4)
		Me.Controls.Add(txtLicenseCode3)
		Me.Controls.Add(txtLicenseCode2)
		Me.Controls.Add(cmdCancel)
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(txtLicenseCode1)
		Me.Controls.Add(txtName)
		Me.Controls.Add(txtNumber)
		Me.Controls.Add(lblLicenseCode)
		Me.Controls.Add(lblLicenseName)
		Me.Controls.Add(lblNumber)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class
